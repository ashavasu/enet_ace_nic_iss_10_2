/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipsnmpif.c,v 1.10 2016/02/27 10:14:48 siva Exp $
 *
 * Description:Network management routines for IP module.   
 *             Includes IP stats, config, static routes,    
 *             dynamic, routes, etc                       
 *
 *******************************************************************/
#include "ipinc.h"
#include "fsmpipcli.h"

/****************************************************************************
 Function    :  nmhGetFsIpInLengthErrors
 Input       :  The Indices

                The Object
                retValIpInLengthErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpInLengthErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpInLengthErrors (UINT4 *pu4RetValIpInLengthErrors)
#else
INT1
nmhGetFsIpInLengthErrors (pu4RetValIpInLengthErrors)
     UINT4              *pu4RetValIpInLengthErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpInLengthErrors = pIpCxt->Ip_stats.u4In_len_err;
    return SNMP_SUCCESS;        /* Less length                       */
}

/****************************************************************************
 Function    :  nmhGetFsIpInCksumErrors
 Input       :  The Indices

                The Object
                retValIpInCksumErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInCksumErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpInCksumErrors (UINT4 *pu4RetValIpInCksumErrors)
#else
INT1
nmhGetFsIpInCksumErrors (pu4RetValIpInCksumErrors)
     UINT4              *pu4RetValIpInCksumErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    /* Checksum problem */
    *pu4RetValIpInCksumErrors = pIpCxt->Ip_stats.u4In_cksum_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpInVersionErrors
 Input       :  The Indices

                The Object
                retValIpInVersionErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInVersionErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpInVersionErrors (UINT4 *pu4RetValIpInVersionErrors)
#else
INT1
nmhGetFsIpInVersionErrors (pu4RetValIpInVersionErrors)
     UINT4              *pu4RetValIpInVersionErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* Version mismatch */
    *pu4RetValIpInVersionErrors = pIpCxt->Ip_stats.u4In_ver_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpInTTLErrors
 Input       :  The Indices

                The Object
                retValIpInTTLErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInTTLErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpInTTLErrors (UINT4 *pu4RetValIpInTTLErrors)
#else
INT1
nmhGetFsIpInTTLErrors (pu4RetValIpInTTLErrors)
     UINT4              *pu4RetValIpInTTLErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    /* Time to live expired */
    *pu4RetValIpInTTLErrors = pIpCxt->Ip_stats.u4In_ttl_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpInOptionErrors
 Input       :  The Indices

                The Object
                retValIpInOptionErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInOptionErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpInOptionErrors (UINT4 *pu4RetValIpInOptionErrors)
#else
INT1
nmhGetFsIpInOptionErrors (pu4RetValIpInOptionErrors)
     UINT4              *pu4RetValIpInOptionErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /*Problems in options */
    *pu4RetValIpInOptionErrors = pIpCxt->Ip_stats.u4In_opt_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpInBroadCasts
 Input       :  The Indices

                The Object
                retValIpInBroadCasts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpInBroadCasts ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpInBroadCasts (UINT4 *pu4RetValIpInBroadCasts)
#else
INT1
nmhGetFsIpInBroadCasts (pu4RetValIpInBroadCasts)
     UINT4              *pu4RetValIpInBroadCasts;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* Total broadcast pkts received     */
    *pu4RetValIpInBroadCasts = pIpCxt->Ip_stats.u4In_bcasts;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpOutGenErrors
 Input       :  The Indices

                The Object
                retValIpOutGenErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SNMP_SUCCESS or SNMP_SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpOutGenErrors ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpOutGenErrors (UINT4 *pu4RetValIpOutGenErrors)
#else
INT1
nmhGetFsIpOutGenErrors (pu4RetValIpOutGenErrors)
     UINT4              *pu4RetValIpOutGenErrors;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    /* Iface DOWN , Not forwarding etc   */
    *pu4RetValIpOutGenErrors = pIpCxt->Ip_stats.u4Out_gen_err;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpOptProcEnable
 Input       :  The Indices

                The Object 
                testValIpOptProcEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpOptProcEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpOptProcEnable (UINT4 *pu4ErrorCode, INT4 i4TestValIpOptProcEnable)
#else
INT1
nmhTestv2FsIpOptProcEnable (pu4ErrorCode, i4TestValIpOptProcEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpOptProcEnable;
#endif
{
    UINT4               u4ContextId = 0;

    if ((i4TestValIpOptProcEnable != IP_OPT_PROC_ENABLE) &&
        (i4TestValIpOptProcEnable != IP_OPT_PROC_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (NULL == UtilIpGetCxt (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpOptProcEnable
 Input       :  The Indices

                The Object 
                setValIpOptProcEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpOptProcEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpOptProcEnable (INT4 i4SetValIpOptProcEnable)
#else
INT1
nmhSetFsIpOptProcEnable (i4SetValIpOptProcEnable)
     INT4                i4SetValIpOptProcEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    if ((i4SetValIpOptProcEnable == IP_OPT_PROC_ENABLE) ||
        (i4SetValIpOptProcEnable == IP_OPT_PROC_DISABLE))
    {
        IP_CFG_SET_OPT_PROC_ENABLE (pIpCxt, (INT1) i4SetValIpOptProcEnable);
        IncMsrForFsIpv4GlobalTable ((INT4) u4ContextId, i4SetValIpOptProcEnable,
                                    FsMIFsIpOptProcEnable,
                                    (sizeof (FsMIFsIpOptProcEnable) /
                                     sizeof (UINT4)), FALSE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsIpOptProcEnable
 Input       :  The Indices

                The Object 
                retValIpOptProcEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpOptProcEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpOptProcEnable (INT4 *pi4RetValIpOptProcEnable)
#else
INT1
nmhGetFsIpOptProcEnable (pi4RetValIpOptProcEnable)
     INT4               *pi4RetValIpOptProcEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpOptProcEnable = ((INT4) IP_CFG_GET_OPT_PROC_ENABLE (pIpCxt));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IpNumMultipath
 Input       :  The Indices

                The Object
                testValIpNumMultipath
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpNumMultipath ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpNumMultipath (UINT4 *pu4ErrorCode, INT4 i4TestValIpNumMultipath)
#else
INT1
nmhTestv2FsIpNumMultipath (pu4ErrorCode, i4TestValIpNumMultipath)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpNumMultipath;
#endif
{
    UINT4               u4ContextId = 0;

    if ((i4TestValIpNumMultipath < 1) ||
        (i4TestValIpNumMultipath > MAX_NUM_MULTIPATH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (NULL == UtilIpGetCxt (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpNumMultipath
 Input       :  The Indices

                The Object
                setValIpNumMultipath
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpNumMultipath ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpNumMultipath (INT4 i4SetValIpNumMultipath)
#else
INT1
nmhSetFsIpNumMultipath (i4SetValIpNumMultipath)
     INT4                i4SetValIpNumMultipath;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    IP_CFG_SET_NUM_MULTIPATH (pIpCxt, (UINT1) i4SetValIpNumMultipath);
    IncMsrForFsIpv4GlobalTable ((INT4) u4ContextId, i4SetValIpNumMultipath,
                                FsMIFsIpNumMultipath,
                                (sizeof (FsMIFsIpNumMultipath) /
                                 sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpNumMultipath
 Input       :  The Indices

                The Object
                retValIpNumMultipath
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpNumMultipath ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpNumMultipath (INT4 *pi4RetValIpNumMultipath)
#else
INT1
nmhGetFsIpNumMultipath (pi4RetValIpNumMultipath)
     INT4               *pi4RetValIpNumMultipath;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpNumMultipath = (INT4) IP_CFG_GET_NUM_MULTIPATH (pIpCxt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpLoadShareEnable
 Input       :  The Indices

                The Object
                testValIpLoadShareEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2IpLoadShareEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpLoadShareEnable (UINT4 *pu4ErrorCode,
                              INT4 i4TestValIpLoadShareEnable)
#else
INT1
nmhTestv2FsIpLoadShareEnable (pu4ErrorCode, i4TestValIpLoadShareEnable)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValIpLoadShareEnable;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if ((i4TestValIpLoadShareEnable != IP_LOADSHARE_ENABLE) &&
        (i4TestValIpLoadShareEnable != IP_LOADSHARE_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (NULL == UtilIpGetCxt (u4ContextId))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpLoadShareEnable
 Input       :  The Indices

                The Object
                setValIpLoadShareEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetIpLoadShareEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpLoadShareEnable (INT4 i4SetValIpLoadShareEnable)
#else
INT1
nmhSetFsIpLoadShareEnable (i4SetValIpLoadShareEnable)
     INT4                i4SetValIpLoadShareEnable;

#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }

    IP_CFG_SET_LOADSHARE_ENABLE (pIpCxt, (INT1) i4SetValIpLoadShareEnable);
    IncMsrForFsIpv4GlobalTable ((INT4) u4ContextId, i4SetValIpLoadShareEnable,
                                FsMIFsIpLoadShareEnable,
                                (sizeof (FsMIFsIpLoadShareEnable) /
                                 sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpLoadShareEnable
 Input       :  The Indices

                The Object
                retValIpLoadShareEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetIpLoadShareEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpLoadShareEnable (INT4 *pi4RetValIpLoadShareEnable)
#else
INT1
nmhGetFsIpLoadShareEnable (pi4RetValIpLoadShareEnable)
     INT4               *pi4RetValIpLoadShareEnable;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpLoadShareEnable = IP_CFG_GET_LOADSHARE_ENABLE (pIpCxt);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpProxyArpSubnetOption
 Input       :  The Indices

                The Object 
                retValFsIpProxyArpSubnetOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsIpProxyArpSubnetOption (INT4 *pi4RetValFsIpProxyArpSubnetOption)
#else
INT1
nmhGetFsIpProxyArpSubnetOption (pi4RetValFsIpProxyArpSubnetOption)
     INT4               *pi4RetValFsIpProxyArpSubnetOption;
#endif
{
    if (IpvxUtilGetFsMIStdIpProxyArpSubnetOption 
        (pi4RetValFsIpProxyArpSubnetOption) == IP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpProxyArpSubnetOption
 Input       :  The Indices

                The Object 
                setValFsIpProxyArpSubnetOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsIpProxyArpSubnetOption (INT4 i4SetValFsIpProxyArpSubnetOption)
#else
INT1
nmhSetFsIpProxyArpSubnetOption (i4SetValFsIpProxyArpSubnetOption)
     INT4                i4SetValFsIpProxyArpSubnetOption;
#endif
{
    if (IpvxUtilSetFsMIStdIpProxyArpSubnetOption
        (i4SetValFsIpProxyArpSubnetOption)   == IP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpProxyArpSubnetOption
 Input       :  The Indices

                The Object 
                testValFsIpProxyArpSubnetOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsIpProxyArpSubnetOption (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsIpProxyArpSubnetOption)
#else
INT1
nmhTestv2FsIpProxyArpSubnetOption (pu4ErrorCode,
                                   i4TestValFsIpProxyArpSubnetOption)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsIpProxyArpSubnetOption;
#endif
{
if (IpvxUtilTestv2FsMIStdIpProxyArpSubnetOption (pu4ErrorCode, i4TestValFsIpProxyArpSubnetOption) == IP_FAILURE)
{
    return SNMP_FAILURE;
}
return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpProxyArpSubnetOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpProxyArpSubnetOption (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpLoadShareEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpLoadShareEnable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpNumMultipath
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpNumMultipath (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpOptProcEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpOptProcEnable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
