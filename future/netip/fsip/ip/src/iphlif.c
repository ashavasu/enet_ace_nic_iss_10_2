/**************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iphlif.c,v 1.7 2013/07/04 13:10:59 siva Exp $
 *
 * Description: Routines provided to higher layers. 
 *
 **************************************************************************/

#include "ipinc.h"

/***************************************************************************
 *                                                                         *
 *     Function Name : IpEnquePktToIpFromHL                                *
 *                                                                         *
 *     Description   : The function whill be called by NON-MI functions.   *
 *                     So default context id is filled in the packet's     *
 *                     data and then enqueued to IP. This function is      *
 *                     retained for back ward compatability. All new HL    *
 *                     protocol should use the function                    *
 *                     IpEnquePktToIpFromHLWithCxtId                       *
 *                                                                         *
 *     Input(s)      : pPkt        : Pointer to Packet.                    *
 *                     pIpSendParms: Pointer to the structure consisting   *
 *                                   of information used by IP, while      *
 *                                   filling up the IP header.             *
 *     Output(s)     : None.                                               *
 *                                                                         *
 *     Returns       : IP_SUCCESS/IP_FAILURE.                              *
 *                                                                         *
 ***************************************************************************/
/**** $$TRACE_PROCEDURE_NAME = IpEnquePktToIpFromHL ****/
/**** $$TRACE_PROCEDURE_LEVEL = MAIN ****/
#ifdef __STDC__
PUBLIC INT4
IpEnquePktToIpFromHL (tIP_BUF_CHAIN_HEADER * pPkt)
#else
PUBLIC INT4
IpEnquePktToIpFromHL (pPkt)
     tIP_BUF_CHAIN_HEADER *pPkt;
#endif
{
    tIpParms           *pIpParms = NULL;
    t_IP_SEND_PARMS    *pIpSendParams = NULL;
    t_ICMP_MSG_PARMS   *pIcmpParams = NULL;

    pIpParms = (tIpParms *) IP_GET_MODULE_DATA_PTR (pPkt);
    if (pIpParms == NULL)
    {
        return IP_FAILURE;
    }

    switch (pIpParms->u1Cmd)
    {
        case IP_LAYER4_DATA:
            pIpSendParams = (t_IP_SEND_PARMS *) pIpParms;
            pIpSendParams->u4ContextId = IP_DEFAULT_CONTEXT;
            break;
        case IP_ICMP_DATA:
            pIcmpParams = (t_ICMP_MSG_PARMS *) pIpParms;
            pIcmpParams->u4ContextId = IP_DEFAULT_CONTEXT;
            break;
        default:
            break;

    }

    return (IpEnquePktToIpFromHLWithCxtId (pPkt));
}

/***************************************************************************
 *                                                                         *
 *     Function Name : IpEnquePktToIpFromHLWithCxtId                       *
 *                                                                         *
 *     Description   : The function which is used to enqueue the OSPF pdu  *
 *                     to FutureIP through the Common Fwd Agt.             *
 *                                                                         *
 *     Input(s)      : pPkt        : Pointer to Packet.                    *
 *                     pIpSendParms: Pointer to the structure consisting   *
 *                                   of information used by IP, while      *
 *                                   filling up the IP header.             *
 *     Output(s)     : None.                                               *
 *                                                                         *
 *     Returns       : IP_SUCCESS/IP_FAILURE.                              *
 *                                                                         *
 ***************************************************************************/
/**** $$TRACE_PROCEDURE_NAME = IpEnquePktToIpFromHL ****/
/**** $$TRACE_PROCEDURE_LEVEL = MAIN ****/
#ifdef __STDC__
PUBLIC INT4
IpEnquePktToIpFromHLWithCxtId (tIP_BUF_CHAIN_HEADER * pPkt)
#else
PUBLIC INT4
IpEnquePktToIpFromHLWithCxtId (pPkt)
     tIP_BUF_CHAIN_HEADER *pPkt;
#endif
{
    if (OsixQueSend (gIpGlobalInfo.IpPktQId, (UINT1 *) &pPkt, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        IP_RELEASE_BUF (pPkt, FALSE);
        return IP_FAILURE;
    }
    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_PACKET_ARRIVAL_EVENT);
    return IP_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : IpJoinMcastGroup                                           *
 *                                                                           *
 * Description  : This procedure calls the IP module to join the specified   *
 *                multicast group in  the specified interface                *
 *                                                                           *
 * Input        :  pMcastGroupAddr  : multicast group address                *
 *                 u4IfIndex      : ifIndex of the interface                 *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : VOID                                                       *
 *                                                                           *
 *****************************************************************************/
/**** $$TRACE_PROCEDURE_NAME = IpJoinMcastGroup ****/
/**** $$TRACE_PROCEDURE_LEVEL = MAIN ****/

#ifdef __STDC__
PUBLIC VOID
IpJoinMcastGroup (UINT4 *pMcastGroupAddr, UINT4 u4IfIndex)
#else
PUBLIC VOID
IpJoinMcastGroup (pMcastGroupAddr, u4IfIndex)
     UINT4              *pMcastGroupAddr;
     UINT4               u4IfIndex;
#endif
{

    /* This fuction is called from OSPF to register for the reception of 
     * mcast data packets for a particular group */
    UNUSED_PARAM (pMcastGroupAddr);
    UNUSED_PARAM (u4IfIndex);
}

/*****************************************************************************
 *                                                                           *
 * Function     : IpLeaveMcastGroup                                          *
 *                                                                           *
 * Description  : This procedure calls the IP module to leave the specified  *
 *                multicast group in  the specified interface                *
 *                                                                           *
 * Input        :  pMcastGroupAddr : multicast group address                 *
 *                 u4IfIndex        : ifIndex of the interface               *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : VOID                                                       *
 *                                                                           *
 *****************************************************************************/
/**** $$TRACE_PROCEDURE_NAME = IpLeaveMcastGroup ****/
/**** $$TRACE_PROCEDURE_LEVEL = MAIN ****/

#ifdef __STDC__
PUBLIC VOID
IpLeaveMcastGroup (UINT4 *pMcastGroupAddr, UINT4 u4IfIndex)
#else /*  */
PUBLIC VOID
IpLeaveMcastGroup (pMcastGroupAddr, u4IfIndex)
     UINT4              *pMcastGroupAddr;
     UINT4               u4IfIndex;
#endif
{
    /* This fuction is called from OSPF to register for not recieving 
     * mcast data packets for a particular group */

    UNUSED_PARAM (pMcastGroupAddr);
    UNUSED_PARAM (u4IfIndex);
}
