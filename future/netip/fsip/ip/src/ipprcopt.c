/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipprcopt.c,v 1.14 2014/02/24 11:27:55 siva Exp $
 *
 * Description: IP Options processing functions. 
 *
 *******************************************************************/
#include "ipinc.h"

/**************************************************************************/

     /**** $$TRACE_MODULE_NAME    = IP_FWD ****/
     /**** $$TRACE_SUB_MODULE_NAME = MAIN   ****/

/*
 * This procedure is called by IpInputProcessPkt.This is called before 
 * forwarding a packet to other machine/higher layers of this node.
 * The procedure calles IpGetRouteInCxt    to get a route in some cases.
 * The calling procedure is expected to supply the pointer and can use
 * same route for further processing.
 * The procedure calls icmp_output in case of any error.
 * If the datagram was addressed to us and the destination is now changed
 * according to source route option, then the calling procedure is expected
 * to reset flag ( indicating for this node ) with the knowledge of non null
 * route pointer.
 * The procedure returns IP_SUCCESS or IP_FAILURE.
 */
INT4
ip_process_options_InCxt (tIpCxt * pIpCxt, t_IP * pIp, INT1 *i1For_this_node,
                          tIP_BUF_CHAIN_HEADER * pBuf, UINT2 *pu2pRt_port,
                          UINT4 *u4pRt_dest)
{
    t_ICMP              Icmp;
    INT4                i4Temp = 0, i4Opt = 0, i4tmp = 0;
    UINT4               u4Local_addr = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4CxtId = pIpCxt->u4ContextId;
    INT2                i2Opt_len = 0;
    INT2                i2Cur_opt_ptr = 0;
    UINT2               u2pPort = 0;
    UINT2               u2RouterAlertValue = 0;
    UINT4               u4TempAddr = 0;

    while ((i4Temp < IP_MAX_BYTES ((pIp->u2Olen), IP_MAX_OPT_LEN))
           && ((i4Opt = IP_OPT_NUMBER (pIp->au1Options[i4Temp])) != IP_OPT_EOL))
    {
        if (i4Temp == (IP_MAX_OPT_LEN - 1))
        {
            return IP_FAILURE;
        }

        i2Opt_len =
            (INT2) ((i4Opt ==
                     IP_OPT_NOP) ? 1 : (UINT1) (pIp->au1Options[i4Temp + 1]));

        if ((UINT2) (i4Temp + i2Opt_len) > pIp->u2Olen)
        {
            return IP_FAILURE;
        }

        if (i4Opt == IP_OPT_NOP)
        {
            i4Temp += i2Opt_len;
            continue;
        }

        if (i2Opt_len < 2)
        {
          /**** $$TRACE_LOG (INTMD, "Wrong Option length\n"); ****/
            return IP_FAILURE;
        }

        if (IP_OPT_CLASS (pIp->au1Options[i4Temp]) !=
            IP_OPT_DATAGRAM_NETWORK_CONTROL)
        {
            if (!((i4Opt == IP_OPT_TSTAMP) &&
                  (IP_OPT_CLASS (pIp->au1Options[i4Temp]) == IP_OPT_DEBUG)))
            {
           /**** $$TRACE_LOG (INTMD, "Not Supported Options\n");****/
                i4Temp += i2Opt_len;
                continue;
            }
        }

        if ((i4Temp + 1 >= IP_MAX_OPT_LEN) ||
            ((i4Temp + IP_TWO) >= IP_MAX_OPT_LEN) ||
            ((i4Temp + IP_THREE) >= IP_MAX_OPT_LEN) ||
            ((i4Temp + IP_FOUR) >= IP_MAX_OPT_LEN))
        {
            return IP_FAILURE;
        }

        switch (i4Opt)
        {
            case IP_OPT_SSROUTE:
              /**** $$TRACE_LOG (INTMD, "IP_OPT_SSROUTE\n");****/

                if (*i1For_this_node != IP_FOR_THIS_NODE)
                {
                    goto Route_error;    /* Strict routing-not for this node and we
                                         * have the packet.!!
                                         */
                    /* Fall through */
                }

            case IP_OPT_LSROUTE:
              /**** $$TRACE_LOG (INTMD, "IP_OPT_LSROUTE\n");****/

                /* Ignore this options if we are not in path */
                if (*i1For_this_node != IP_FOR_THIS_NODE)
                {
                    break;
                }

                i2Cur_opt_ptr = (UINT1) pIp->au1Options[i4Temp + 2];

                /* If the pointer is greater than the length, the source route
                 * is empty (and the recorded route full) and normal routing
                 * is used.
                 */
                if (i2Cur_opt_ptr >= i2Opt_len)    /* Don't record */
                    break;

                if ((i2Opt_len - i2Cur_opt_ptr + 1) < IP_FOUR)
                    goto Opt_error;

                /* The pointer is relative to this option, and the
                 * smallest legal value for the pointer is 4.
                 */
                /* The address in the source route replaces the address in the
                 * destination address field, and the recorded route address
                 * replaces the source address just used, and pointer is
                 * increased by four.
                 */

                if ((i2Cur_opt_ptr % IP_FOUR) != 0)
                {
                    goto Opt_error;
                }

                if (((i4Temp + i2Cur_opt_ptr - 1) < 0)
                    || ((i4Temp + i2Cur_opt_ptr - 1) >= IP_MAX_OPT_LEN))

                {
                    return IP_FAILURE;
                }
                if (((i4Temp + IP_TWO) <= 0)
                    || ((i4Temp + IP_TWO) >= IP_MAX_OPT_LEN))
                {
                    return IP_FAILURE;
                }

                MEMCPY (&u4TempAddr,
                        &(pIp->au1Options[i4Temp + i2Cur_opt_ptr - 1]),
                        sizeof (UINT4));
                pIp->u4Dest = IP_NTOHL (u4TempAddr);

                if (IpGetRouteInCxt (u4CxtId, pIp->u4Dest, pIp->u4Src,
                                     pIp->u1Proto, pIp->u1Tos,
                                     pu2pRt_port, u4pRt_dest) != IP_FAILURE)
                {
                    if (IpGetSrcAddressOnInterface (*pu2pRt_port,
                                                    pIp->u4Dest,
                                                    &u4Local_addr) ==
                        IP_FAILURE)
                    {
                        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, ALL_FAILURE_TRC,
                                         IP_NAME,
                                         "Getting source IP for destination failed %x\n",
                                         pIp->u4Dest);
                    }
                }
                else
                {
                    goto Route_error;
                }
                if ((i4Opt == IP_OPT_SSROUTE) && (*u4pRt_dest != pIp->u4Dest))
                {
                    goto Opt_error;    /* Not a locally connected net
                                     * Some other Gw is involved which is not
                                     * in user specied list.
                                     */
                }
                u4TempAddr = IP_HTONL (u4Local_addr);
                MEMCPY (&(pIp->au1Options[i4Temp + i2Cur_opt_ptr - 1]),
                        &u4TempAddr, sizeof (UINT4));
                pIp->au1Options[i4Temp + IP_TWO] =
                    (UINT1) (pIp->au1Options[i4Temp + IP_TWO] + IP_FOUR);

                if (IP_CFG_GET_FORWARDING (pIpCxt) == IP_FORW_DISABLE)
                {
                    IP4SYS_INC_IN_FORW_DGRAMS (u4CxtId);
                    IP4IF_INC_IN_FORW_DGRAMS (*pu2pRt_port);
                }
                break;

            case IP_OPT_RROUTE:
                i2Cur_opt_ptr = (UINT1) pIp->au1Options[i4Temp + IP_TWO];

                /* If the route data area is already full (the pointer exceeds the
                 * length) the datagram is forwarded without inserting the
                 * address into the recorded route.  If there is some room but not
                 * enough room for a full address to be inserted, the original
                 * datagram is considered to be in error and is discarded.
                 */
                if ((i2Cur_opt_ptr >= i2Opt_len) ||
                    ((i2Opt_len - i2Cur_opt_ptr + 1) < IP_FOUR))
                {
                    if (((i2Opt_len - i2Cur_opt_ptr + 1) == 0) ||
                        (*i1For_this_node == IP_FOR_THIS_NODE))
                    {
                        break;
                    }
                    goto Opt_error;
                }
                if ((IpGetRouteInCxt (u4CxtId, pIp->u4Dest, pIp->u4Src,
                                      pIp->u1Proto, pIp->u1Tos, &u2pPort,
                                      &u4IpAddr)) != IP_FAILURE)
                {
                    if (IpGetSrcAddressOnInterface (u2pPort,
                                                    u4IpAddr,
                                                    &u4Local_addr) ==
                        IP_FAILURE)
                    {
                        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, ALL_FAILURE_TRC,
                                         IP_NAME,
                                         "Getting source IP for destination failed %x\n",
                                         pIp->u4Dest);
                    }
                }
                else if (ipif_is_our_address_InCxt (u4CxtId, pIp->u4Dest) !=
                         FALSE)
                {
                    u4Local_addr = pIp->u4Dest;
                }
                else
                {
                    goto Route_error;
                }
                /*
                 * The recorded route address is the internet module's own
                 * internet address as known in the environment into which this
                 * datagram is being forwarded.
                 */
                if (((i4Temp + i2Cur_opt_ptr - 1) < 0)
                    || ((i4Temp + i2Cur_opt_ptr - 1) >= IP_MAX_OPT_LEN))
                {
                    return IP_FAILURE;
                }
                if (((i4Temp + IP_TWO) <= 0)
                    || ((i4Temp + IP_TWO) >= IP_MAX_OPT_LEN))
                {
                    return IP_FAILURE;
                }

                {
                    UINT4               u4TmpAddr = 0;
                    u4TmpAddr = IP_HTONL (u4Local_addr);
                    MEMCPY (&(pIp->au1Options[i4Temp + i2Cur_opt_ptr - 1]),
                            &u4TmpAddr, sizeof (UINT4));
                }
                pIp->au1Options[i4Temp + IP_TWO] =
                    (UINT1) (pIp->au1Options[i4Temp + IP_TWO] + IP_FOUR);
                break;

            case IP_OPT_TSTAMP:

                i2Cur_opt_ptr = (UINT1) pIp->au1Options[i4Temp + IP_TWO];
                if (i2Cur_opt_ptr >= i2Opt_len)
                {
                    /* If there is no space then increment overflow count */

                    if (IP_OPT_GET_TS_FLG (pIp->au1Options[i4Temp + IP_THREE])
                        == IP_OPT_TS_SPECIFIED_ADDR)
                    {
                        break;
                    }
                    if ((IP_OPT_GET_TS_OVF
                         (*(UINT1 *) &pIp->au1Options[i4Temp + IP_THREE]) ==
                         0x0f))
                    {
                        if (*i1For_this_node == IP_FOR_THIS_NODE)
                        {
                            break;
                        }
                        goto Opt_error;
                    }
                    else
                        (pIp->au1Options[i4Temp + IP_THREE]) =
                            (UINT1) (pIp->au1Options[i4Temp + IP_THREE] + 0x10);
                }
                i4tmp = i2Opt_len - i2Cur_opt_ptr + 1;

                /*   
                 * If there is some room but not enough room for a
                 * full timestamp to be inserted, or the overflow
                 * count itself overflows, the original datagram is
                 * considered to be in error and is discarded. In
                 * either case an ICMP parameter problem message may
                 * be sent to the source host.
                 *       - RFC 791
                 */
                if (i4tmp == 0)
                {
                    break;
                }

                switch (IP_OPT_GET_TS_FLG (pIp->au1Options[i4Temp + IP_THREE]))
                {

                    case IP_OPT_TS_TSTAMP_ONLY:

                        if (i4tmp < IP_FOUR)
                        {
                            goto Opt_error;
                        }
                        if (((i4Temp + i2Cur_opt_ptr - 1) < 0)
                            || ((i4Temp + i2Cur_opt_ptr - 1) >= IP_MAX_OPT_LEN))
                        {
                            return IP_FAILURE;
                        }
                        if (((i4Temp + IP_TWO) <= 0)
                            || ((i4Temp + IP_TWO) >= IP_MAX_OPT_LEN))
                        {
                            return IP_FAILURE;
                        }

                        {
                            UINT4               u4Ticks = 0;
                            OsixGetSysTime (&u4Ticks);
                            u4Ticks = IP_HTONL (u4Ticks | 0x80000000L);
                            MEMCPY (&pIp->
                                    au1Options[i4Temp + i2Cur_opt_ptr - 1],
                                    &u4Ticks, sizeof (UINT4));
                        }
                        pIp->au1Options[i4Temp + IP_TWO] =
                            (UINT1) (pIp->au1Options[i4Temp + IP_TWO] +
                                     IP_FOUR);
                        break;

                    case IP_OPT_TS_ADDR_TSTAMP:

                        if (i4tmp < 8)
                        {
                            goto Opt_error;
                        }
                        if ((IpGetRouteInCxt (u4CxtId, pIp->u4Dest, pIp->u4Src,
                                              pIp->u1Proto, pIp->u1Tos,
                                              &u2pPort,
                                              &u4IpAddr)) != IP_FAILURE)
                        {
                            if (IpGetSrcAddressOnInterface (u2pPort,
                                                            u4IpAddr,
                                                            &u4Local_addr) ==
                                IP_FAILURE)
                            {
                                IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC,
                                                 ALL_FAILURE_TRC, IP_NAME,
                                                 "Getting source IP for destination failed %x\n",
                                                 pIp->u4Dest);
                            }
                        }
                        else if (ipif_is_our_address_InCxt
                                 (u4CxtId, pIp->u4Dest) != FALSE)
                        {
                            u4Local_addr = pIp->u4Dest;
                        }
                        else
                        {
                            goto Route_error;
                        }
                        if (((i4Temp + i2Cur_opt_ptr - 1) < 0) ||
                            ((i4Temp + i2Cur_opt_ptr + IP_FOUR - 1) >=
                             IP_MAX_OPT_LEN))
                        {
                            return IP_FAILURE;
                        }

                        if (((i4Temp + IP_TWO) <= 0) ||
                            ((i4Temp + IP_TWO) >= IP_MAX_OPT_LEN))
                        {
                            return IP_FAILURE;
                        }

                        {
                            UINT4               u4Ticks = 0;
                            OsixGetSysTime (&u4Ticks);
                            u4Ticks = IP_HTONL (u4Ticks | 0x80000000L);
                            u4Local_addr = IP_HTONL (u4Local_addr);
                            MEMCPY (&
                                    (pIp->
                                     au1Options[i4Temp + i2Cur_opt_ptr - 1]),
                                    &u4Local_addr, sizeof (UINT4));
                            MEMCPY (&
                                    (pIp->
                                     au1Options[i4Temp + i2Cur_opt_ptr +
                                                IP_FOUR - 1]), &u4Ticks,
                                    sizeof (UINT4));
                        }
                        pIp->au1Options[i4Temp + IP_TWO] =
                            (UINT1) (pIp->au1Options[i4Temp + IP_TWO] + 8);
                        break;

                    case IP_OPT_TS_SPECIFIED_ADDR:

                        if (i4tmp < 8)
                        {
                            goto Opt_error;
                        }

                        /* Put timestamp only if we are in the list */
                        if ((IpGetRouteInCxt (u4CxtId, pIp->u4Dest, pIp->u4Src,
                                              pIp->u1Proto, pIp->u1Tos,
                                              &u2pPort,
                                              &u4IpAddr)) != IP_FAILURE)
                        {
                            if (IpGetSrcAddressOnInterface (u2pPort,
                                                            u4IpAddr,
                                                            &u4Local_addr) ==
                                IP_FAILURE)
                            {
                                IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC,
                                                 ALL_FAILURE_TRC, IP_NAME,
                                                 "Getting source IP for destination failed %x\n",
                                                 pIp->u4Dest);
                            }
                        }
                        else if (ipif_is_our_address_InCxt
                                 (u4CxtId, pIp->u4Dest) != FALSE)
                        {
                            u4Local_addr = pIp->u4Dest;
                        }
                        else
                            goto Route_error;

                        if (((i4Temp + i2Cur_opt_ptr - 1) < 0) ||
                            ((i4Temp + i2Cur_opt_ptr + IP_FOUR - 1) >=
                             IP_MAX_OPT_LEN))
                        {
                            return IP_FAILURE;
                        }

                        if (((i4Temp + IP_TWO) <= 0) ||
                            ((i4Temp + IP_TWO) >= IP_MAX_OPT_LEN))
                        {
                            return IP_FAILURE;
                        }

                        MEMCPY (&u4TempAddr,
                                &(pIp->au1Options[i4Temp + i2Cur_opt_ptr - 1]),
                                sizeof (UINT4));
                        if (ipif_is_our_address_InCxt
                            (u4CxtId, IP_NTOHL (u4TempAddr)) == TRUE)
                        {
                            UINT4               u4Ticks = 0;
                            OsixGetSysTime (&u4Ticks);
                            u4Ticks = IP_HTONL (u4Ticks | 0x80000000L);

                            MEMCPY (&pIp->
                                    au1Options[i4Temp + i2Cur_opt_ptr +
                                               IP_FOUR - 1], &u4Ticks,
                                    sizeof (UINT4));
                            pIp->au1Options[i4Temp + IP_TWO] =
                                (UINT1) (pIp->au1Options[i4Temp + IP_TWO] + 8);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case IP_OPT_STREAM_IDENT:

                break;
            case IP_OPT_SECURITY:

                break;
            case IP_OPT_ROUTERALERT:

                u2RouterAlertValue = (UINT2) pIp->au1Options[i4Temp + IP_TWO];
                if (!u2RouterAlertValue)
                {
                    *i1For_this_node = IP_FOR_THIS_NODE;
                }
                break;

            default:
                break;
        }
        i4Temp += i2Opt_len;
    }

    return IP_SUCCESS;

  Opt_error:

    Icmp.i1Type = ICMP_PARAM_PROB;
    Icmp.i1Code = ICMP_PARAM_ERR_AT_PTR;
    Icmp.args.u1Pointer = (UINT1) (IP_HDR_LEN + i4Temp);
    Icmp.u4ContextId = pIpCxt->u4ContextId;

    icmp_error_msg (pBuf, &Icmp);

    return IP_FAILURE;

  Route_error:

    Icmp.i1Type = ICMP_DEST_UNREACH;
    Icmp.u4ContextId = pIpCxt->u4ContextId;
    Icmp.args.u4Unused = 0;

    pIpCxt->Ip_stats.u4Out_no_routes++;
    if ((ipif_is_our_address_InCxt (u4CxtId, pIp->u4Src) == TRUE) ||
        (pIp->u4Src == IP_ANY_ADDR))
    {
        IP4SYS_INC_OUT_NO_ROUTES (u4CxtId);
        IP4IF_INC_OUT_NO_ROUTES (*pu2pRt_port);
    }
    else
    {
        IP4SYS_INC_IN_NO_ROUTES (u4CxtId);
        IP4IF_INC_IN_NO_ROUTES (*pu2pRt_port);
    }

    if ((i4Opt == IP_OPT_LSROUTE) || (i4Opt == IP_OPT_SSROUTE))
    {
        Icmp.i1Code = ICMP_ROUTE_FAIL;
        icmp_error_msg (pBuf, &Icmp);
    }
    else
    {
        Icmp.i1Code = ICMP_NET_UNREACH;
        icmp_error_msg (pBuf, &Icmp);
    }

    return IP_FAILURE;
}
