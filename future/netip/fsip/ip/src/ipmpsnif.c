/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmpsnif.c,v 1.12 2013/07/04 13:10:59 siva Exp $
 *
 * Description:This contains SNMP low level routines for     
 *             IP Common Routing Table                   
 *
 *******************************************************************/
#include "ipinc.h"
#include "rtminc.h"
#include "fsmpipcli.h"
#include "rtm.h"
/* **************************************************************************
 * *********************G L O B A L  V A R I A B L E S **********************
 * **************************************************************************
 */

/* LOW LEVEL Routines for Table : FsIpCommonRoutingTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIpCommonRoutingTable
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsIpCommonRoutingTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsIpCommonRoutingTable (UINT4 u4FsIpRouteDest,
                                                UINT4 u4FsIpRouteMask,
                                                INT4 i4FsIpRouteTos,
                                                UINT4 u4FsIpRouteNextHop,
                                                INT4 i4FsIpRouteProto)
#else
INT1
nmhValidateIndexInstanceFsIpCommonRoutingTable (u4FsIpRouteDest,
                                                u4FsIpRouteMask, i4FsIpRouteTos,
                                                u4FsIpRouteNextHop,
                                                i4FsIpRouteProto)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
#endif
{

    /* Protocol value can be any one of the protocol listed in rfc 2096 */

    if ((i4FsIpRouteProto <= 0) || (i4FsIpRouteProto > MAX_ROUTING_PROTOCOLS))
    {
        return (SNMP_FAILURE);
    }

    if (IP_IS_ADDR_CLASS_D (u4FsIpRouteDest) ||
        IP_IS_ADDR_CLASS_E (u4FsIpRouteDest) ||
        IP_IS_ADDR_CLASS_D (u4FsIpRouteNextHop) ||
        ((u4FsIpRouteDest & u4FsIpRouteMask) != u4FsIpRouteDest))
    {
        return SNMP_FAILURE;
    }

    if (i4FsIpRouteTos < 0)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIpCommonRoutingTable
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsIpCommonRoutingTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsIpCommonRoutingTable (UINT4 *pu4FsIpRouteDest,
                                        UINT4 *pu4FsIpRouteMask,
                                        INT4 *pi4FsIpRouteTos,
                                        UINT4 *pu4FsIpRouteNextHop,
                                        INT4 *pi4FsIpRouteProto)
#else
INT1
nmhGetFirstIndexFsIpCommonRoutingTable (pu4FsIpRouteDest, pu4FsIpRouteMask,
                                        pi4FsIpRouteTos, pu4FsIpRouteNextHop,
                                        pi4FsIpRouteProto)
     UINT4              *pu4FsIpRouteDest;
     UINT4              *pu4FsIpRouteMask;
     INT4               *pi4FsIpRouteTos;
     UINT4              *pu4FsIpRouteNextHop;
     INT4               *pi4FsIpRouteProto;
#endif
{
    INT4                i4OutCome = 0;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = 0;
    RtQuery.u4DestinationSubnetMask = 0;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_EXACT_DEST;
    RtQuery.u4ContextId = u4ContextId;

    /* Expecting RTM_PROT_LOCK is already taken before calling this fn */
    if (RtmUtilIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        *pu4FsIpRouteDest = 0;
        *pu4FsIpRouteMask = 0;
        *pi4FsIpRouteTos = (INT4) NetIpRtInfo.u4Tos;
        *pu4FsIpRouteNextHop = NetIpRtInfo.u4NextHop;
        *pi4FsIpRouteProto = NetIpRtInfo.u2RtProto;
        return (SNMP_SUCCESS);
    }
    else
    {
        /* Default route is not present. */
        i4OutCome =
            (INT4) (nmhGetNextIndexFsIpCommonRoutingTable
                    (0, pu4FsIpRouteDest, 0, pu4FsIpRouteMask, 0,
                     pi4FsIpRouteTos, 0, pu4FsIpRouteNextHop, 0,
                     pi4FsIpRouteProto));
        if (i4OutCome == SNMP_SUCCESS)
        {
            return (SNMP_SUCCESS);
        }
    }

    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIpCommonRoutingTable
 Input       :  The Indices
                FsIpRouteDest
                nextFsIpRouteDest
                FsIpRouteMask
                nextFsIpRouteMask
                FsIpRouteTos
                nextFsIpRouteTos
                FsIpRouteNextHop
                nextFsIpRouteNextHop
                FsIpRouteProto
                nextFsIpRouteProto
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsIpCommonRoutingTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsIpCommonRoutingTable (UINT4 u4FsIpRouteDest,
                                       UINT4 *pu4NextFsIpRouteDest,
                                       UINT4 u4FsIpRouteMask,
                                       UINT4 *pu4NextFsIpRouteMask,
                                       INT4 i4FsIpRouteTos,
                                       INT4 *pi4NextFsIpRouteTos,
                                       UINT4 u4FsIpRouteNextHop,
                                       UINT4 *pu4NextFsIpRouteNextHop,
                                       INT4 i4FsIpRouteProto,
                                       INT4 *pi4NextFsIpRouteProto)
#else
INT1
nmhGetNextIndexFsIpCommonRoutingTable (u4FsIpRouteDest, pu4NextFsIpRouteDest,
                                       u4FsIpRouteMask, pu4NextFsIpRouteMask,
                                       i4FsIpRouteTos, pi4NextFsIpRouteTos,
                                       u4FsIpRouteNextHop,
                                       pu4NextFsIpRouteNextHop,
                                       i4FsIpRouteProto, pi4NextFsIpRouteProto)
     UINT4               u4FsIpRouteDest;
     UINT4              *pu4NextFsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     UINT4              *pu4NextFsIpRouteMask;
     INT4                i4FsIpRouteTos;
     INT4               *pi4NextFsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     UINT4              *pu4NextFsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4NextFsIpRouteProto;
#endif
{
    tNetIpv4RtInfo      InRtInfo;
    tNetIpv4RtInfo      OutRtInfo;
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    MEMSET (&InRtInfo, 0, sizeof (tNetIpv4RtInfo));
    InRtInfo.u4DestNet = u4FsIpRouteDest;
    InRtInfo.u4DestMask = u4FsIpRouteMask;
    InRtInfo.u4Tos = (UINT4) i4FsIpRouteTos;
    InRtInfo.u4NextHop = u4FsIpRouteNextHop;
    InRtInfo.u2RtProto = (UINT2) i4FsIpRouteProto;
    InRtInfo.u4ContextId = u4ContextId;

    /* Expecting RTM_PROT_LOCK is already taken before calling this fn */
    if (RtmUtilGetNextFwdTableRtEntry (&InRtInfo, &OutRtInfo) == IP_SUCCESS)
    {
        *pu4NextFsIpRouteDest = OutRtInfo.u4DestNet;
        *pu4NextFsIpRouteMask = OutRtInfo.u4DestMask;
        *pi4NextFsIpRouteTos = (INT4) OutRtInfo.u4Tos;
        *pu4NextFsIpRouteNextHop = OutRtInfo.u4NextHop;
        *pi4NextFsIpRouteProto = OutRtInfo.u2RtProto;
        return (SNMP_SUCCESS);
    }
    return (SNMP_FAILURE);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIpRouteProtoInstanceId
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                retValFsIpRouteProtoInstanceId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpRouteProtoInstanceId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRouteProtoInstanceId (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                                INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                                INT4 i4FsIpRouteProto,
                                INT4 *pi4RetValFsIpRouteProtoInstanceId)
#else
INT1
nmhGetFsIpRouteProtoInstanceId (u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto,
                                pi4RetValFsIpRouteProtoInstanceId)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4RetValFsIpRouteProtoInstanceId;
#endif
{
/* Once Instantiation comes to true and routes from different instants exists, this
 * routine has to be filled.
 */
    UNUSED_PARAM (u4FsIpRouteDest);
    UNUSED_PARAM (u4FsIpRouteMask);
    UNUSED_PARAM (i4FsIpRouteTos);
    UNUSED_PARAM (u4FsIpRouteNextHop);
    UNUSED_PARAM (i4FsIpRouteProto);
    *pi4RetValFsIpRouteProtoInstanceId = 0;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpRouteIfIndex
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                retValFsIpRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpRouteIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRouteIfIndex (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                        INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                        INT4 i4FsIpRouteProto, INT4 *pi4RetValFsIpRouteIfIndex)
#else
INT1
nmhGetFsIpRouteIfIndex (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                        u4FsIpRouteNextHop, i4FsIpRouteProto,
                        pi4RetValFsIpRouteIfIndex)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4RetValFsIpRouteIfIndex;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, pi4RetValFsIpRouteIfIndex,
                                IPFWD_ROUTE_IFINDEX) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }

    *pi4RetValFsIpRouteIfIndex =
        (INT4) gIpGlobalInfo.Ipif_config[*pi4RetValFsIpRouteIfIndex].
        InterfaceId.u4IfIndex;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIpRouteType
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                retValFsIpRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpRouteType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRouteType (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                     INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                     INT4 i4FsIpRouteProto, INT4 *pi4RetValFsIpRouteType)
#else
INT1
nmhGetFsIpRouteType (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                     u4FsIpRouteNextHop, i4FsIpRouteProto,
                     pi4RetValFsIpRouteType)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4RetValFsIpRouteType;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, pi4RetValFsIpRouteType,
                                IPFWD_ROUTE_TYPE) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIpRouteAge
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                retValFsIpRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRouteAge (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                    INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                    INT4 i4FsIpRouteProto, INT4 *pi4RetValFsIpRouteAge)
#else
INT1
nmhGetFsIpRouteAge (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                    u4FsIpRouteNextHop, i4FsIpRouteProto, pi4RetValFsIpRouteAge)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4RetValFsIpRouteAge;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, pi4RetValFsIpRouteAge,
                                IPFWD_ROUTE_AGE) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpRouteNextHopAS
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                retValFsIpRouteNextHopAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpRouteNextHopAS ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRouteNextHopAS (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                          INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                          INT4 i4FsIpRouteProto,
                          INT4 *pi4RetValFsIpRouteNextHopAS)
#else
INT1
nmhGetFsIpRouteNextHopAS (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                          u4FsIpRouteNextHop, i4FsIpRouteProto,
                          pi4RetValFsIpRouteNextHopAS)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4RetValFsIpRouteNextHopAS;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();
    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, pi4RetValFsIpRouteNextHopAS,
                                IPFWD_ROUTE_NEXTHOPAS) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpRouteMetric1
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                retValFsIpRouteMetric1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpRouteMetric1 ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRouteMetric1 (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                        INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                        INT4 i4FsIpRouteProto, INT4 *pi4RetValFsIpRouteMetric1)
#else
INT1
nmhGetFsIpRouteMetric1 (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                        u4FsIpRouteNextHop, i4FsIpRouteProto,
                        pi4RetValFsIpRouteMetric1)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4RetValFsIpRouteMetric1;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId, u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, pi4RetValFsIpRouteMetric1,
                                IPFWD_ROUTE_METRIC1) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpRoutePreference
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                retValFsIpRoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpRoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRoutePreference (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                           INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                           INT4 i4FsIpRouteProto,
                           INT4 *pi4RetValFsIpRoutePreference)
#else
INT1
nmhGetFsIpRoutePreference (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                           u4FsIpRouteNextHop, i4FsIpRouteProto,
                           pi4RetValFsIpRoutePreference)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4RetValFsIpRoutePreference;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, pi4RetValFsIpRoutePreference,
                                IPFWD_ROUTE_PREFERENCE) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIpRouteStatus
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                retValFsIpRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsIpRouteStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsIpRouteStatus (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                       INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                       INT4 i4FsIpRouteProto, INT4 *pi4RetValFsIpRouteStatus)
#else
INT1
nmhGetFsIpRouteStatus (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                       u4FsIpRouteNextHop, i4FsIpRouteProto,
                       pi4RetValFsIpRouteStatus)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4               *pi4RetValFsIpRouteStatus;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, pi4RetValFsIpRouteStatus,
                                IPFWD_ROUTE_ROW_STATUS) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIpRouteIfIndex
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                setValFsIpRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpRouteIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRouteIfIndex (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                        INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                        INT4 i4FsIpRouteProto, INT4 i4SetValFsIpRouteIfIndex)
#else
INT1
nmhSetFsIpRouteIfIndex (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                        u4FsIpRouteNextHop, i4FsIpRouteProto,
                        i4SetValFsIpRouteIfIndex)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4SetValFsIpRouteIfIndex;

#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblSetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, &i4SetValFsIpRouteIfIndex,
                                IPFWD_ROUTE_IFINDEX) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }

    IncMsrForFsIpv4CommonRoutingTable (u4ContextId, u4FsIpRouteDest,
                                       u4FsIpRouteMask, i4FsIpRouteTos,
                                       u4FsIpRouteNextHop, i4FsIpRouteProto,
                                       i4SetValFsIpRouteIfIndex,
                                       FsMIFsIpRouteIfIndex,
                                       (sizeof (FsMIFsIpRouteIfIndex) /
                                        sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpRouteType
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                setValFsIpRouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpRouteType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRouteType (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                     INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                     INT4 i4FsIpRouteProto, INT4 i4SetValFsIpRouteType)
#else
INT1
nmhSetFsIpRouteType (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                     u4FsIpRouteNextHop, i4FsIpRouteProto,
                     i4SetValFsIpRouteType)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4SetValFsIpRouteType;

#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblSetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, &i4SetValFsIpRouteType,
                                IPFWD_ROUTE_TYPE) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    IncMsrForFsIpv4CommonRoutingTable (u4ContextId, u4FsIpRouteDest,
                                       u4FsIpRouteMask, i4FsIpRouteTos,
                                       u4FsIpRouteNextHop, i4FsIpRouteProto,
                                       i4SetValFsIpRouteType,
                                       FsMIFsIpRouteType,
                                       (sizeof (FsMIFsIpRouteType) /
                                        sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIpRouteNextHopAS
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                setValFsIpRouteNextHopAS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpRouteNextHopAS ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRouteNextHopAS (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                          INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                          INT4 i4FsIpRouteProto,
                          INT4 i4SetValFsIpRouteNextHopAS)
#else
INT1
nmhSetFsIpRouteNextHopAS (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                          u4FsIpRouteNextHop, i4FsIpRouteProto,
                          i4SetValFsIpRouteNextHopAS)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4SetValFsIpRouteNextHopAS;

#endif
{
    UNUSED_PARAM (u4FsIpRouteDest);
    UNUSED_PARAM (u4FsIpRouteMask);
    UNUSED_PARAM (i4FsIpRouteTos);
    UNUSED_PARAM (u4FsIpRouteNextHop);
    UNUSED_PARAM (i4FsIpRouteProto);
    UNUSED_PARAM (i4SetValFsIpRouteNextHopAS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIpRoutePreference
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                setValFsIpRoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpRoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRoutePreference (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                           INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                           INT4 i4FsIpRouteProto,
                           INT4 i4SetValFsIpRoutePreference)
#else
INT1
nmhSetFsIpRoutePreference (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                           u4FsIpRouteNextHop, i4FsIpRouteProto,
                           i4SetValFsIpRoutePreference)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4SetValFsIpRoutePreference;

#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblSetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, &i4SetValFsIpRoutePreference,
                                IPFWD_ROUTE_PREFERENCE) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    IncMsrForFsIpv4CommonRoutingTable (u4ContextId, u4FsIpRouteDest,
                                       u4FsIpRouteMask, i4FsIpRouteTos,
                                       u4FsIpRouteNextHop, i4FsIpRouteProto,
                                       i4SetValFsIpRoutePreference,
                                       FsMIFsIpRoutePreference,
                                       (sizeof (FsMIFsIpRoutePreference) /
                                        sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIpRouteStatus
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                setValFsIpRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsIpRouteStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsIpRouteStatus (UINT4 u4FsIpRouteDest, UINT4 u4FsIpRouteMask,
                       INT4 i4FsIpRouteTos, UINT4 u4FsIpRouteNextHop,
                       INT4 i4FsIpRouteProto, INT4 i4SetValFsIpRouteStatus)
#else
INT1
nmhSetFsIpRouteStatus (u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
                       u4FsIpRouteNextHop, i4FsIpRouteProto,
                       i4SetValFsIpRouteStatus)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4SetValFsIpRouteStatus;

#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblSetObjectInCxt (u4ContextId,
                                u4FsIpRouteDest, u4FsIpRouteMask,
                                i4FsIpRouteTos, u4FsIpRouteNextHop,
                                i4FsIpRouteProto, &i4SetValFsIpRouteStatus,
                                IPFWD_ROUTE_ROW_STATUS) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    IncMsrForFsIpv4CommonRoutingTable (u4ContextId, u4FsIpRouteDest,
                                       u4FsIpRouteMask, i4FsIpRouteTos,
                                       u4FsIpRouteNextHop, i4FsIpRouteProto,
                                       i4SetValFsIpRouteStatus,
                                       FsMIFsIpRouteStatus,
                                       (sizeof (FsMIFsIpRouteStatus) /
                                        sizeof (UINT4)), TRUE);

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIpRouteIfIndex
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                testValFsIpRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpRouteIfIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpRouteIfIndex (UINT4 *pu4ErrorCode, UINT4 u4FsIpRouteDest,
                           UINT4 u4FsIpRouteMask, INT4 i4FsIpRouteTos,
                           UINT4 u4FsIpRouteNextHop, INT4 i4FsIpRouteProto,
                           INT4 i4TestValFsIpRouteIfIndex)
#else
INT1
nmhTestv2FsIpRouteIfIndex (pu4ErrorCode, u4FsIpRouteDest, u4FsIpRouteMask,
                           i4FsIpRouteTos, u4FsIpRouteNextHop, i4FsIpRouteProto,
                           i4TestValFsIpRouteIfIndex)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4TestValFsIpRouteIfIndex;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4FsIpRouteDest, u4FsIpRouteMask,
                                 i4FsIpRouteTos, u4FsIpRouteNextHop,
                                 i4FsIpRouteProto, &i4TestValFsIpRouteIfIndex,
                                 IPFWD_ROUTE_IFINDEX,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpRouteType
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                testValFsIpRouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpRouteType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpRouteType (UINT4 *pu4ErrorCode, UINT4 u4FsIpRouteDest,
                        UINT4 u4FsIpRouteMask, INT4 i4FsIpRouteTos,
                        UINT4 u4FsIpRouteNextHop, INT4 i4FsIpRouteProto,
                        INT4 i4TestValFsIpRouteType)
#else
INT1
nmhTestv2FsIpRouteType (pu4ErrorCode, u4FsIpRouteDest, u4FsIpRouteMask,
                        i4FsIpRouteTos, u4FsIpRouteNextHop, i4FsIpRouteProto,
                        i4TestValFsIpRouteType)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4TestValFsIpRouteType;
#endif
{
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (i4FsIpRouteProto);

    if ((i4TestValFsIpRouteType < OTHERS) || (i4TestValFsIpRouteType > REMOTE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (RtmApiCheckInvalidRtExists
        (u4ContextId, u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
         u4FsIpRouteNextHop) == RTM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpRouteNextHopAS
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                testValFsIpRouteNextHopAS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpRouteNextHopAS ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpRouteNextHopAS (UINT4 *pu4ErrorCode, UINT4 u4FsIpRouteDest,
                             UINT4 u4FsIpRouteMask, INT4 i4FsIpRouteTos,
                             UINT4 u4FsIpRouteNextHop, INT4 i4FsIpRouteProto,
                             INT4 i4TestValFsIpRouteNextHopAS)
#else
INT1
nmhTestv2FsIpRouteNextHopAS (pu4ErrorCode, u4FsIpRouteDest, u4FsIpRouteMask,
                             i4FsIpRouteTos, u4FsIpRouteNextHop,
                             i4FsIpRouteProto, i4TestValFsIpRouteNextHopAS)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4TestValFsIpRouteNextHopAS;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4FsIpRouteDest, u4FsIpRouteMask,
                                 i4FsIpRouteTos, u4FsIpRouteNextHop,
                                 i4FsIpRouteProto, &i4TestValFsIpRouteNextHopAS,
                                 IPFWD_ROUTE_NEXTHOPAS,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsIpRoutePreference
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                testValFsIpRoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpRoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpRoutePreference (UINT4 *pu4ErrorCode, UINT4 u4FsIpRouteDest,
                              UINT4 u4FsIpRouteMask, INT4 i4FsIpRouteTos,
                              UINT4 u4FsIpRouteNextHop, INT4 i4FsIpRouteProto,
                              INT4 i4TestValFsIpRoutePreference)
#else
INT1
nmhTestv2FsIpRoutePreference (pu4ErrorCode, u4FsIpRouteDest, u4FsIpRouteMask,
                              i4FsIpRouteTos, u4FsIpRouteNextHop,
                              i4FsIpRouteProto, i4TestValFsIpRoutePreference)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4TestValFsIpRoutePreference;
#endif
{
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (i4FsIpRouteProto);

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if ((i4TestValFsIpRoutePreference < 0) ||
        (i4TestValFsIpRoutePreference > IP_PREFERENCE_INFINITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RtmApiCheckInvalidRtExists
        (u4ContextId, u4FsIpRouteDest, u4FsIpRouteMask, i4FsIpRouteTos,
         u4FsIpRouteNextHop) == RTM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIpRouteStatus
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto

                The Object 
                testValFsIpRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsIpRouteStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsIpRouteStatus (UINT4 *pu4ErrorCode, UINT4 u4FsIpRouteDest,
                          UINT4 u4FsIpRouteMask, INT4 i4FsIpRouteTos,
                          UINT4 u4FsIpRouteNextHop, INT4 i4FsIpRouteProto,
                          INT4 i4TestValFsIpRouteStatus)
#else
INT1                nmhTestv2FsIpRouteStatus (pu4ErrorCode, u4FsIpRouteDest,
                                              u4FsIpRouteMask,
                                              UINT4 *pu4ErrorCode;
                                              i4FsIpRouteTos,
                                              u4FsIpRouteNextHop,
                                              i4FsIpRouteProto,
                                              i4TestValFsIpRouteStatus)
     UINT4               u4FsIpRouteDest;
     UINT4               u4FsIpRouteMask;
     INT4                i4FsIpRouteTos;
     UINT4               u4FsIpRouteNextHop;
     INT4                i4FsIpRouteProto;
     INT4                i4TestValFsIpRouteStatus;
#endif
{
    UINT4               u4ContextId = 0;

    u4ContextId = (UINT4) IpvxGetCurrContext ();

    if (IpFwdTblTestObjectInCxt (u4ContextId,
                                 u4FsIpRouteDest, u4FsIpRouteMask,
                                 i4FsIpRouteTos, u4FsIpRouteNextHop,
                                 i4FsIpRouteProto, &i4TestValFsIpRouteStatus,
                                 IPFWD_ROUTE_ROW_STATUS,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {

        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIpCommonRoutingTable
 Input       :  The Indices
                FsIpRouteDest
                FsIpRouteMask
                FsIpRouteTos
                FsIpRouteNextHop
                FsIpRouteProto
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpCommonRoutingTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
