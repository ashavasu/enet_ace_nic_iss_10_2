/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsipcli.c,v 1.30 2016/10/21 13:43:51 siva Exp $
 *
 * Description: Action routines of IP module specific commands
 *
 *******************************************************************/
#ifndef __FSIPCLI_C__
#define __FSIPCLI_C__

#include "ipinc.h"
#include "ipcli.h"
#include "vcm.h"
#include "ip.h"
#include "isscli.h"

extern CONST CHR1  *StdipCliErrString[];
INT4
cli_process_ip_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[IP_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT1              *pu1IpCxtName = NULL;
    UINT4               u4VcId = VCM_INVALID_VC;

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Fourth argument is always ContextName */
    pu1IpCxtName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     * Store IP_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == IP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    CliRegisterLock (CliHandle, IpFwdProtocolLock, IpFwdProtocolUnlock);
    IPFWD_PROT_LOCK ();

    if (pu1IpCxtName != NULL)
    {
        if (VcmIsVrfExist (pu1IpCxtName, &u4VcId) == VCM_FALSE)
        {
            i4RetStatus = CLI_FAILURE;
        }
        else
        {
            i4RetStatus = CLI_SUCCESS;
        }
    }
    else
    {
        u4VcId = IP_DEFAULT_CONTEXT;
    }
    if (i4RetStatus == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF\r\n");
        CliUnRegisterLock (CliHandle);
        IPFWD_PROT_UNLOCK ();
        return CLI_FAILURE;
    }
    /*Set Context Id, which will be used by the following
       configuration commands */

    if (UtilIpSetCxt (u4VcId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF\r\n");
        CliUnRegisterLock (CliHandle);
        IPFWD_PROT_UNLOCK ();
        return CLI_FAILURE;
    }

    switch (u4Command)
    {
        case CLI_IP_SHOW_PMTU:
            if (pu1IpCxtName == NULL)
            {
                u4VcId = VCM_INVALID_VC;
            }

            i4RetStatus = ShowIpPmtuInCxt (CliHandle, u4VcId);
            break;

        case CLI_IP_REDIRECTS:
            i4RetStatus = IpSetIcmpRedirects (CliHandle,
                                              (INT4) ICMP_SEND_REDIRECT_ENABLE);
            break;

        case CLI_IP_NO_REDIRECTS:
            i4RetStatus = IpSetIcmpRedirects (CliHandle,
                                              (INT4)
                                              ICMP_SEND_REDIRECT_DISABLE);
            break;

        case CLI_IP_UNREACHABLES:
            i4RetStatus = IpSetIcmpUnreachables (CliHandle,
                                                 (INT4)
                                                 ICMP_SEND_UNREACHABLE_ENABLE);
            break;

        case CLI_IP_NO_UNREACHABLES:
            i4RetStatus = IpSetIcmpUnreachables (CliHandle,
                                                 (INT4)
                                                 ICMP_SEND_UNREACHABLE_DISABLE);
            break;

        case CLI_IP_MASK_REPLY:
            i4RetStatus = IpSetIcmpMaskReply (CliHandle,
                                              (INT4) ICMP_NMASK_REPLY_ENABLE);
            break;

        case CLI_IP_NO_MASK_REPLY:
            i4RetStatus = IpSetIcmpMaskReply (CliHandle,
                                              (INT4) ICMP_NMASK_REPLY_DISABLE);
            break;

        case CLI_IP_ECHO_REPLY:
            i4RetStatus = IpSetIcmpEchoReply (CliHandle,
                                              (INT4) ICMP_ECHO_REPLY_ENABLE);
            break;

        case CLI_IP_NO_ECHO_REPLY:
            i4RetStatus = IpSetIcmpEchoReply (CliHandle,
                                              (INT4) ICMP_ECHO_REPLY_DISABLE);
            break;

        case CLI_IP_ROUTING:
            i4RetStatus = IpSetRouting (CliHandle, (INT4) IP_FORW_ENABLE);
            break;

        case CLI_IP_NO_ROUTING:
            i4RetStatus = IpSetRouting (CliHandle, (INT4) IP_FORW_DISABLE);
            break;

        case CLI_IP_MAXIMUM_PATHS:
            /* args[0] -  maximum paths value
             */
            i4RetStatus = IpSetMaximumPaths (CliHandle, *(INT4 *) args[0]);
            break;

        case CLI_IP_NO_MAXIMUM_PATHS:
            i4RetStatus = IpSetMaximumPaths (CliHandle,
                                             (INT4) DEF_NUM_MULTIPATH);
            break;

        case CLI_IP_DEFAULT_TTL:
            /* args[0] - default TTL value
             */
            i4RetStatus = IpSetDefaultTTL (CliHandle, *(INT4 *) args[0]);
            break;

        case CLI_IP_NO_DEFAULT_TTL:
            i4RetStatus = IpSetDefaultTTL (CliHandle, (INT4) IP_DEF_TTL);
            break;

        case CLI_IP_AGGREGATE_ROUTE:
            /* args[0] - number of aggregate routes
             */
            i4RetStatus = IpSetAggregateRoutes (CliHandle, *(INT4 *) args[0]);
            break;

        case CLI_IP_NO_AGGREGATE_ROUTE:
            i4RetStatus = IpSetAggregateRoutes (CliHandle,
                                                (INT4) IP_DEF_AGGREGATE_ROUTES);
            break;

        case CLI_IP_TRAFFIC_SHARE:
            i4RetStatus = IpSetTrafficShare (CliHandle,
                                             (INT4) IP_LOADSHARE_ENABLE);
            break;

        case CLI_IP_NO_TRAFFIC_SHARE:
            i4RetStatus = IpSetTrafficShare (CliHandle,
                                             (INT4) IP_LOADSHARE_DISABLE);
            break;

        case CLI_IP_PATH_MTU_DISCOVER:
            i4RetStatus = IpSetPathMtuDiscover (CliHandle, (INT4) PMTU_ENABLED);
            break;

        case CLI_IP_NO_PATH_MTU_DISCOVER:
            i4RetStatus =
                IpSetPathMtuDiscover (CliHandle, (INT4) PMTU_DISABLED);
            break;

        case CLI_IP_PATH_MTU_ADD:
            i4RetStatus = IpPathMtuAdd (CliHandle, *args[0],
                                        *(INT4 *) args[1], *(INT4 *) args[2]);
            break;

        case CLI_IP_PATH_MTU_DEL:
            i4RetStatus = IpPathMtuDel (CliHandle, *args[0], *(INT4 *) args[1]);
            break;

        case CLI_IP_DIRECTED_BROADCAST:
            /* Validating of Interface for ATM interface type 
             * was done previously, should I retain it
             */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = IpSetDirectedBcast (CliHandle,
                                              (INT4) IPIF_DRTD_BCAST_FWD_ENABLE,
                                              i4IfaceIndex);
            break;

        case CLI_IP_NO_DIRECTED_BROADCAST:
            /* Validating of Interface for ATM interface type 
             * was done previously, should I retain it
             */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = IpSetDirectedBcast (CliHandle,
                                              (INT4)
                                              IPIF_DRTD_BCAST_FWD_DISABLE,
                                              i4IfaceIndex);
            break;

        default:
            CliPrintf (CliHandle, "\r% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }

    /*Reset Context */
    UtilIpReleaseCxt ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_IP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", StdipCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);
    IPFWD_PROT_UNLOCK ();

    return i4RetStatus;
}

INT4
cli_process_rarp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[IP_CLI_MAX_ARGS];
    INT1                i1argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4CmdType = 0;
    UINT1               au1IfName[IF_PREFIX_LEN];

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfaceIndex = va_arg (ap, INT4);

    /* Walk through the rest of the arguments and store in args array. 
     * Store IP_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == IP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    CliRegisterLock (CliHandle, ArpProtocolLock, ArpProtocolUnLock);
    ARP_PROT_LOCK ();

    switch (u4Command)
    {
        case CLI_IP_SHOW_RARP:
            i4RetStatus = IpShowRarp (CliHandle);
            break;

        case CLI_IP_RARP_CLIENT_REQUEST:
            /* args[0] - CmdType for ip rarp client request command
             * args[1] - If CmdType is RARP_CLIENT_REQ_INTERVAL then timeout 
             *              interval
             *           else if CmdType is RARP_CLIENT_REQ_RETRIES then retries
             */
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            if (u4CmdType == RARP_CLIENT_REQ_INTERVAL)
            {
                i4RetStatus =
                    IpSetRarpReqTimeout (CliHandle, *(INT4 *) args[1]);
            }
            else if (u4CmdType == RARP_CLIENT_REQ_RETRIES)
            {
                i4RetStatus =
                    IpSetRarpReqRetries (CliHandle, *(INT4 *) args[1]);
            }
            break;

        case CLI_IP_NO_RARP_CLIENT_REQUEST:
            /* args[0] - CmdType for ip rarp client request command
             */
            u4CmdType = CLI_PTR_TO_U4 (args[0]);
            if (u4CmdType == RARP_CLIENT_REQ_INTERVAL)
            {
                i4RetStatus = IpSetRarpReqTimeout (CliHandle,
                                                   (INT4) IP_DEF_REQ_INTERVAL);
            }
            else if (u4CmdType == RARP_CLIENT_REQ_RETRIES)
            {
                i4RetStatus = IpSetRarpReqRetries (CliHandle,
                                                   (INT4) IP_DEF_REQ_RETRIES);
            }
            break;

        case CLI_IP_RARP_CLIENT:

            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (CfaGetInterfaceNameFromIndex
                ((UINT4) i4IfaceIndex, au1IfName) == OSIX_FAILURE)
            {
                CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
                CliUnRegisterLock (CliHandle);
                ARP_PROT_UNLOCK ();
                return (CLI_FAILURE);
            }

            if (cli_get_if_type (au1IfName) != CFA_ENET)
            {
                CLI_SET_ERR (CLI_IP_RARP_ERR);
                CliUnRegisterLock (CliHandle);
                ARP_PROT_UNLOCK ();
                return (CLI_FAILURE);
            }

            i4RetStatus = IpSetRarpClientEnable (CliHandle, i4IfaceIndex);
            break;

        case CLI_IP_NO_RARP_CLIENT:

            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = IpSetRarpClientDisable (CliHandle, i4IfaceIndex);
            break;

        default:
            CliPrintf (CliHandle, "\r% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_IP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", StdipCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);
    ARP_PROT_UNLOCK ();

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpShowRarp                                             */
/*                                                                           */
/* Description      : This function is invoked to display RARP configuration */
/*                    and RARP statistics                                    */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpShowRarp (tCliHandle CliHandle)
{
    INT4                i4RarpClientMaxEntries = 0;
    INT4                i4RarpClientTimeOut = 0;
    INT4                i4RarpClientPktsDiscarded = 0;

    nmhGetFsRarpClientMaxRetries (&i4RarpClientMaxEntries);
    nmhGetFsRarpClientRetransmissionTimeout (&i4RarpClientTimeOut);
    nmhGetFsRarpClientPktsDiscarded ((UINT4 *) &i4RarpClientPktsDiscarded);

    CliPrintf (CliHandle, "\r\nRARP Configurations:\r\n");
    CliPrintf (CliHandle, "--------------------\r\n");

    CliPrintf (CliHandle,
               " Maximum number of RARP request retransmission retries is %d\r\n",
               i4RarpClientMaxEntries);

    CliPrintf (CliHandle,
               " RARP request retransmission timeout is %d seconds\r\n",
               i4RarpClientTimeOut);

    CliPrintf (CliHandle, "\r\nRARP Statistics:\r\n");
    CliPrintf (CliHandle, "----------------\r\n");

    CliPrintf (CliHandle, " %d responses discarded\r\n\r\n",
               i4RarpClientPktsDiscarded);

    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : IpSetIcmpRedirects
*  Description   : Calls Low level Set Routine to enable or disable
*                  ICMP Redirect Messages.
*
*  Input(s)      : 1. tCliHandle - CliContext ID
*                  2. i4RedirectStatus - CLI_ENABLE / CLI_DISABLE
*  Output(s)     : None
*  Return Values : CLI_SUCCESS / CLI_FAILURE
**********************************************************************/
INT4
IpSetIcmpRedirects (tCliHandle CliHandle, INT4 i4RedirectStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIcmpSendRedirectEnable (&u4ErrorCode,
                                           i4RedirectStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsIcmpSendRedirectEnable (i4RedirectStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : IpSetIcmpUnreachables
*  Description   : Calls Low level Set Routine to enable or disable
*                  ICMP Unreachable Messages.
*
*  Input(s)      : 1. tCliHandle - CliContext ID
*                  2. i4UnreachableStatus - CLI_ENABLE / CLI_DISABLE
*  Output(s)     : None
*  Return Values : CLI_SUCCESS / CLI_FAILURE
**********************************************************************/
INT4
IpSetIcmpUnreachables (tCliHandle CliHandle, INT4 i4UnreachableStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIcmpSendUnreachableEnable (&u4ErrorCode,
                                              i4UnreachableStatus)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsIcmpSendUnreachableEnable (i4UnreachableStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : IpSetIcmpEchoReply
*  Description   : Calls Low level Set Routine to enable or disable
*                  ICMP Echo Reply Messages.
*
*  Input(s)      : 1. tCliHandle - CliContext ID
*                  2. i4EchoReplyStatus - CLI_ENABLE / CLI_DISABLE
*  Output(s)     : None
*  Return Values : CLI_SUCCESS / CLI_FAILURE
**********************************************************************/
INT4
IpSetIcmpEchoReply (tCliHandle CliHandle, INT4 i4EchoReplyStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIcmpSendEchoReplyEnable (&u4ErrorCode,
                                            i4EchoReplyStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsIcmpSendEchoReplyEnable (i4EchoReplyStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : IpSetIcmpMaskReply
*  Description   : Calls Low level Set Routine to enable or disable
*                  ICMP Mask Reply Messages.
*
*  Input(s)      : 1. tCliHandle - CliContext ID
*                  2. i4MaskReplyStatus - CLI_ENABLE / CLI_DISABLE
*  Output(s)     : None
*  Return Values : CLI_SUCCESS / CLI_FAILURE
**********************************************************************/
INT4
IpSetIcmpMaskReply (tCliHandle CliHandle, INT4 i4MaskReplyStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIcmpNetMaskReplyEnable (&u4ErrorCode,
                                           i4MaskReplyStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsIcmpNetMaskReplyEnable (i4MaskReplyStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetMaximumPaths                                      */
/*                                                                           */
/* Description      : This function is invoked to set the maximum number of  */
/*                    multi paths                                            */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4MaximumPaths - maximum paths value to set         */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetMaximumPaths (tCliHandle CliHandle, INT4 i4MaximumPaths)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIpNumMultipath (&u4ErrorCode,
                                   i4MaximumPaths) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsIpNumMultipath (i4MaximumPaths) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetRarpReqTimeout                                    */
/*                                                                           */
/* Description      : This function is invoked to set the RARP client request*/
/*                    retransmission timeout                                 */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4ReqTimeout - request timeout value to set         */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetRarpReqTimeout (tCliHandle CliHandle, INT4 i4ReqTimeout)
{

    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);

    if ((nmhTestv2FsRarpClientRetransmissionTimeout
         (&u4ErrCode, i4ReqTimeout)) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    nmhSetFsRarpClientRetransmissionTimeout (i4ReqTimeout);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetRarpReqRetries                                    */
/*                                                                           */
/* Description      : This function is invoked to set the RARP client request*/
/*                    retries                                                */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4ReqRetries - request retries value to set         */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetRarpReqRetries (tCliHandle CliHandle, INT4 i4ReqRetries)
{

    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);
    if ((nmhTestv2FsRarpClientMaxRetries
         (&u4ErrCode, i4ReqRetries)) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    nmhSetFsRarpClientMaxRetries (i4ReqRetries);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetAggregateRoutes                                   */
/*                                                                           */
/* Description      : This function is invoked to set number of aggregate    */
/*                    routes                                                 */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4AggregateRoutes - Number of aggregate routes      */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetAggregateRoutes (tCliHandle CliHandle, INT4 i4AggregateRoutes)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsNoOfAggregatedRoutes (&u4ErrorCode, i4AggregateRoutes) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsNoOfAggregatedRoutes (i4AggregateRoutes);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetTrafficShare                                      */
/*                                                                           */
/* Description      : This function is invoked to enable or disable load     */
/*                    sharing                                                */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4TrafficShareStatus - CLI_ENABLE / CLI_DISABLE     */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetTrafficShare (tCliHandle CliHandle, INT4 i4TrafficShareStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIpLoadShareEnable (&u4ErrorCode, i4TrafficShareStatus)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsIpLoadShareEnable (i4TrafficShareStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetPathMtuDiscover                                   */
/*                                                                           */
/* Description      : This function is invoked to enable or disable path mtu */
/*                    discovery                                              */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4PathDiscoverStatus - CLI_ENABLE / CLI_DISABLE     */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetPathMtuDiscover (tCliHandle CliHandle, INT4 i4PathDiscoverStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIpEnablePMTUD (&u4ErrorCode, i4PathDiscoverStatus)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsIpEnablePMTUD (i4PathDiscoverStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetDirectedBcast                                     */
/*                                                                           */
/* Description      : This function is invoked to enable or disable directed */
/*                    broadcast forwarding on a given interface              */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4BcastStatus - CLI_ENABLE / CLI_DISABLE            */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */

/*****************************************************************************/

INT4
IpSetDirectedBcast (tCliHandle CliHandle, INT4 i4BcastStatus, INT4 i4IfaceIndex)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIpifDrtBcastFwdingEnable
        (&u4ErrorCode, i4IfaceIndex, i4BcastStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsIpifDrtBcastFwdingEnable (i4IfaceIndex, i4BcastStatus)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_SUCCESS);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetRarpClientEnable                                  */
/*                                                                           */
/* Description      : This function is invoked to enable RARP client         */
/*                    on a given interface                                   */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IfaceIndex - Interface index                      */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetRarpClientEnable (tCliHandle CliHandle, INT4 i4IfaceIndex)
{
    UINT2               u2Port = 0;

    UNUSED_PARAM (CliHandle);

    if (IpifGetPortFromIfIndex ((UINT4) i4IfaceIndex, &u2Port) == IP_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    RarpConfigTable.u1ClientStatus = RARP_CLIENT_STATUS_ENABLE;
    RarpConfigTable.u1ServerStatus = RARP_SERVER_STATUS_DISABLE;

    ARP_PROT_UNLOCK ();

    RarpClientSendRequest ((UINT2) i4IfaceIndex);

    ARP_PROT_LOCK ();
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : IpSetRarpClientDisable                                 */
/*                                                                           */
/* Description      : This function is invoked to enable RARP client         */
/*                    on a given interface                                   */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                    2. i4IfaceIndex - Interface index                      */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
IpSetRarpClientDisable (tCliHandle CliHandle, INT4 i4IfaceIndex)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4IfaceIndex);

    RarpConfigTable.u1ClientStatus = RARP_CLIENT_STATUS_DISABLE;
    RarpConfigTable.u1ServerStatus = RARP_SERVER_STATUS_DISABLE;

    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliIpPathMtuAdd 
*  Description   : calls the LowLevel Routines for adding an entry into 
*                  path MTU table.
*                  
*  Input(s)      : pCliPmtu- Structure filled up with Path Mtu Destination,
*                  path Mtu Type Of Service. 
*  Output(s)     : pRespMsg -filled with error Message ,if any.
*  Return Values :  
*********************************************************************/
INT4
IpPathMtuAdd (tCliHandle CliHandle, UINT4 u4FsIpPmtuDest,
              INT4 i4FsIpPmtuTos, INT4 i4FsIpPathMtu)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4PmtuGblStatus = 0;

    nmhGetFsIpEnablePMTUD (&i4PmtuGblStatus);

    if (i4PmtuGblStatus != PMTU_ENABLED)
    {
        CliPrintf (CliHandle,
                   "%%Path Mtu Discovery Should be Enabled First\r\n");
        return CLI_FAILURE;

    }
    if (nmhGetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                   &i4RowStatus) == SNMP_FAILURE)
    {
        /*No Row Exists With This Instance Create One */
        if (nmhTestv2FsIpPmtuEntryStatus (&u4ErrorCode, u4FsIpPmtuDest,
                                          i4FsIpPmtuTos,
                                          IPFWD_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_RESOURCE_UNAVAILABLE)
            {
                CLI_SET_ERR (CLI_IP_PMTU_MAX_ENTRIES_ERR);
            }
            return CLI_FAILURE;
        }

        if (nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                       IPFWD_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /*Row exists for the destination go on to Set the Params */
        if (i4RowStatus == IPFWD_ACTIVE)
        {
            if (nmhTestv2FsIpPmtuEntryStatus (&u4ErrorCode, u4FsIpPmtuDest,
                                              i4FsIpPmtuTos,
                                              IPFWD_NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                           IPFWD_NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    if (nmhSetFsIpPmtuDisc (u4FsIpPmtuDest, i4FsIpPmtuTos,
                            (INT4) IP_ENABLE_STATUS) == SNMP_FAILURE)
    {
        nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                   IPFWD_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIpPathMtu (&u4ErrorCode, u4FsIpPmtuDest,
                              i4FsIpPmtuTos, i4FsIpPathMtu) == SNMP_FAILURE)
    {
        /*destroy the Entry Created Here */
        nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                   IPFWD_DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsIpPathMtu (u4FsIpPmtuDest, i4FsIpPmtuTos,
                           i4FsIpPathMtu) == SNMP_FAILURE)
    {
        nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                   IPFWD_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                   IPFWD_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                   IPFWD_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : CliIpPathMtuDel 
*  Description   : Calls the Lowlevel Routines for deletion of an Entry
*                  from Path MTU Table.
*                  
*  Input(s)      : IpCliPmtu-structure filled up with indices(ie Destination,
*                  Type of service) . 
*  Output(s)     : pRespMsg - filled up with the error message ,if any.
*  Return Values : . 
*********************************************************************/
INT4
IpPathMtuDel (tCliHandle CliHandle, UINT4 u4FsIpPmtuDest, INT4 i4FsIpPmtuTos)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsIpPmtuEntryStatus (&u4ErrorCode, u4FsIpPmtuDest,
                                      i4FsIpPmtuTos,
                                      IPFWD_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIpPmtuEntryStatus (u4FsIpPmtuDest, i4FsIpPmtuTos,
                                   IPFWD_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
ShowIpPmtuInCxt (tCliHandle CliHandle, UINT4 u4VcId)
{
    CHR1               *pc1Address = NULL;
    UINT4               u4PmtuIpAddr = 0;
    UINT4               u4CurrentIpAddr = 0;
    UINT4               u4ShowAllCxt = FALSE;
    INT4                i4CurrentVcId = 0;
    INT4                i4PmtuTos = 0, i4RetVal = 0;
    INT4                i4CurrentTos = 0;
    INT4                i4PathMtu = 0;
    INT4                i4PmtuDisc = 0;
    INT4                i4VcId = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    CliPrintf (CliHandle, "\r\n    Ip Path MTU Table\r\n");

    CliPrintf (CliHandle, "    -----------------\r\n");

    CliPrintf (CliHandle, "\r\nVrf Name       Destination  TOS   PMTU \r\n");
    CliPrintf (CliHandle, "--------       -----------  ---   ---- \r\n");
    i4VcId = (INT4) u4VcId;

    if (u4VcId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
        i4RetVal = nmhGetFirstIndexFsMIFsIpPathMtuTable
            (&i4VcId, &u4PmtuIpAddr, &i4PmtuTos);
    }
    else
    {
        i4CurrentVcId = i4VcId;
        i4RetVal = nmhGetNextIndexFsMIFsIpPathMtuTable
            (i4CurrentVcId, &i4VcId, u4CurrentIpAddr,
             &u4PmtuIpAddr, i4CurrentTos, &i4PmtuTos);
        if (i4CurrentVcId != i4VcId)
        {
            return SNMP_FAILURE;
        }
    }

    while (i4RetVal == SNMP_SUCCESS)
    {
        VcmGetAliasName ((UINT4) i4VcId, au1ContextName);
        nmhGetFsMIFsIpPathMtu (i4VcId, u4PmtuIpAddr, i4PmtuTos, &i4PathMtu);

        nmhGetFsMIFsIpPmtuDisc (i4VcId, u4PmtuIpAddr, i4PmtuTos, &i4PmtuDisc);

        CLI_CONVERT_IPADDR_TO_STR (pc1Address, u4PmtuIpAddr);

        CliPrintf (CliHandle, "\n");

        CliPrintf (CliHandle, "%-15s", au1ContextName);
        CliPrintf (CliHandle, "%-13s", pc1Address);
        CliPrintf (CliHandle, "\t%-6d", i4PmtuTos);
        CliPrintf (CliHandle, "\t%-6d\n", i4PathMtu);

        i4CurrentVcId = i4VcId;

        u4CurrentIpAddr = u4PmtuIpAddr;

        i4CurrentTos = i4PmtuTos;

        i4RetVal = nmhGetNextIndexFsMIFsIpPathMtuTable
            (i4CurrentVcId, &i4VcId, u4CurrentIpAddr,
             &u4PmtuIpAddr, i4CurrentTos, &i4PmtuTos);

        if ((i4CurrentVcId != i4VcId) && (u4ShowAllCxt == FALSE))
        {
            /*Finished displaying entries belonging to one VRF */
            break;
        }
    }                            /*end of while */

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : FsIpShowRunningConfigInCxt                         */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the FSIP  Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4Module - flag                                    */
/*                        i4ContextId - VRF Id                               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
FsIpShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Module,
                            INT4 i4ContextId)
{
    UINT1 u1TempBangStatus = FALSE;
    UINT1 u1BangStatus = FALSE;

    CliRegisterLock (CliHandle, IpFwdProtocolLock, IpFwdProtocolUnlock);
    IPFWD_PROT_LOCK ();

    FsIpShowRunningConfigScalarsInCxt (CliHandle, i4ContextId);
    FsIpShowRunningConfigTablesInCxt (CliHandle, i4ContextId);

    CliGetBangStatus(CliHandle, &u1BangStatus);
    if (u1BangStatus == TRUE)
    {
        u1TempBangStatus = TRUE;
    }
    CliSetBangStatus (CliHandle, FALSE);

    if (u4Module == ISS_IP_SHOW_RUNNING_CONFIG)
    {
        FsIpShowRunningConfigInterfaceInCxt (CliHandle, i4ContextId, u4Module);
    }

    if (u1TempBangStatus == TRUE)
    {
        CliSetBangStatus (CliHandle, TRUE);
    }
    IPFWD_PROT_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FsIpShowRunningConfigScalarsInCxt                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Ip for    */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FsIpShowRunningConfigScalarsInCxt (tCliHandle CliHandle, INT4 i4ContextId)
{
    INT4                i4RetVal = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               u1BangStatus = FALSE;

    if (i4ContextId != IP_DEFAULT_CONTEXT)
    {
        /*Get context name from context Id */
        VcmGetAliasName ((UINT4) i4ContextId, au1ContextName);
    }
    nmhGetFsMIFsIcmpSendRedirectEnable (i4ContextId, &i4RetVal);
    if (i4RetVal != ICMP_SEND_REDIRECT_ENABLE)
    {
        u1BangStatus = TRUE;


        if (i4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "no ip redirects\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "no ip redirects vrf %s\r\n", au1ContextName);
        }
    }

    nmhGetFsMIFsIcmpSendUnreachableEnable (i4ContextId, &i4RetVal);
    if (i4RetVal != ICMP_SEND_UNREACHABLE_ENABLE)
    {   
        u1BangStatus = TRUE;

        if (i4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "no ip unreachables\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "no ip unreachables vrf %s\r\n",
                       au1ContextName);
        }
    }

    nmhGetFsMIFsIcmpNetMaskReplyEnable (i4ContextId, &i4RetVal);
    if (i4RetVal != ICMP_NMASK_REPLY_ENABLE)
    { 
        u1BangStatus = TRUE;
        if (i4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "no ip mask-reply\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "no ip mask-reply vrf %s\r\n",
                       au1ContextName);
        }
    }

    nmhGetFsMIFsIcmpSendEchoReplyEnable (i4ContextId, &i4RetVal);
    if (i4RetVal != ICMP_ECHO_REPLY_ENABLE)
    {
        u1BangStatus = TRUE;
        if (i4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "no ip echo-reply\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "no ip echo-reply vrf %s\r\n",
                       au1ContextName);
        }
    }

    nmhGetFsMIFsIpNumMultipath (i4ContextId, &i4RetVal);
    if (i4RetVal != DEF_NUM_MULTIPATH)
    {
        u1BangStatus = TRUE;
        if (i4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "maximum-paths %d\r\n", i4RetVal);
        }
        else
        {
            CliPrintf (CliHandle, "maximum-paths vrf %s %d\r\n", au1ContextName,
                       i4RetVal);
        }
    }

    nmhGetFsMIFsRarpClientRetransmissionTimeout (&i4RetVal);
    if (i4RetVal != IP_DEF_REQ_INTERVAL)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, "ip rarp client request interval %d\r\n",
                   i4RetVal);
    }

    nmhGetFsMIFsRarpClientMaxRetries (&i4RetVal);
    if (i4RetVal != IP_DEF_REQ_RETRIES)
    {
        u1BangStatus = TRUE;
        CliPrintf (CliHandle, "ip rarp client request retries %d\r\n",
                   i4RetVal);
    }

    nmhGetFsMIFsIpLoadShareEnable (i4ContextId, &i4RetVal);
    if (i4RetVal != IP_LOADSHARE_DISABLE)
    {
        u1BangStatus = TRUE;
        if (i4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "traffic-share\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "traffic-share vrf %s\r\n", au1ContextName);
        }
    }

    nmhGetFsMIFsIpEnablePMTUD (i4ContextId, &i4RetVal);
    if (i4RetVal != PMTU_DISABLED)
    {
        u1BangStatus = TRUE;
        if (i4ContextId == IP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, "ip path mtu discover\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "ip vrf %s path mtu discover\r\n",
                       au1ContextName);
        }
    }
    if(u1BangStatus == TRUE)
    {
        CliSetBangStatus(CliHandle, TRUE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FsIpShowRunningConfigTablesInCxt                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Ip for    */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4ContextId - VRF Id                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FsIpShowRunningConfigTablesInCxt (tCliHandle CliHandle, INT4 i4ContextId)
{
    INT4                i4RetVal = 0;
    INT4                i4PmtuTos = 0;
    INT4                i4NextPmtuTos = 0;
    INT4                i4PathMtu = 0;
    INT4                i4PmtuDisc = 0;
    CHR1               *pu1Address = NULL;
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT4               u4PmtuIpAddr = 0;
    UINT4               u4NextPmtuIpAddr = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextContextId = 0;
    UINT1               u1BangStatus = FALSE;


    MEMSET (au1Address, 0, MAX_ADDR_LEN);

    pu1Address = (CHR1 *) & au1Address[0];

    while (nmhGetNextIndexFsMIFsIpPathMtuTable
           (i4ContextId, &i4NextContextId,
            u4PmtuIpAddr, &u4NextPmtuIpAddr,
            i4PmtuTos, &i4NextPmtuTos) != SNMP_FAILURE)
    {
        if (i4ContextId != i4NextContextId)
        {
            break;
            /*no more entries for this context */
        }
        nmhGetFsMIFsIpPmtuEntryStatus (i4NextContextId, u4NextPmtuIpAddr,
                                       i4NextPmtuTos, &i4RetVal);
        if (i4RetVal == IPFWD_ACTIVE)
        {
            u1BangStatus = TRUE;
            MEMSET (au1Address, 0, MAX_ADDR_LEN);
            nmhGetFsMIFsIpPathMtu (i4NextContextId, u4NextPmtuIpAddr,
                                   i4NextPmtuTos, &i4PathMtu);

            nmhGetFsMIFsIpPmtuDisc (i4NextContextId, u4NextPmtuIpAddr,
                                    i4NextPmtuTos, &i4PmtuDisc);

            CLI_CONVERT_IPADDR_TO_STR (pu1Address, u4NextPmtuIpAddr);

            if (i4ContextId == IP_DEFAULT_CONTEXT)
            {
                CliPrintf (CliHandle, "ip path mtu");
            }
            else
            {
                /*Get context name from context Id */
                VcmGetAliasName ((UINT4) i4ContextId, au1ContextName);
                CliPrintf (CliHandle, "ip path mtu vrf %s", au1ContextName);
            }
            CliPrintf (CliHandle, " %s ", pu1Address);
            CliPrintf (CliHandle, " %d ", i4NextPmtuTos);
            u4PagingStatus =
                (UINT4) CliPrintf (CliHandle, " %d\r\n", i4PathMtu);

            if (u4PagingStatus == CLI_FAILURE)
            {
                /*User pressed 'q' at more prompt, no more print required, exit */
                break;
            }
        }
        i4ContextId = i4NextContextId;
        u4PmtuIpAddr = u4NextPmtuIpAddr;
        i4PmtuTos = i4NextPmtuTos;

    }                            /*end of while */
    if(u1BangStatus == TRUE)
    {
        CliSetBangStatus(CliHandle, TRUE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FsIpShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current ip configuration    */
/*                        for interface                                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index - Specified interface index                */
/*                        for configuration                                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
FsIpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4BcastStatus = 0;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];

    pi1InterfaceName = &ai1InterfaceName[0];
    MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaCliConfGetIfName ((UINT4) i4Index, pi1InterfaceName);
    CliRegisterLock (CliHandle, IpFwdProtocolLock, IpFwdProtocolUnlock);
    IPFWD_PROT_LOCK ();
    if (nmhValidateIndexInstanceFsMIFsIpifTable (i4Index) == SNMP_SUCCESS)
    {

        if (nmhGetFsMIFsIpifDrtBcastFwdingEnable (i4Index, &i4BcastStatus) ==
            SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            CliPrintf (CliHandle, "!\r\n");
            return CLI_FAILURE;
        }

        if (i4BcastStatus != IPIF_DRTD_BCAST_FWD_DISABLE)
        {
            CliPrintf (CliHandle, "interface %s\r\n", pi1InterfaceName);
            CliPrintf (CliHandle, " ip directed-broadcast\r\n");
            CliPrintf (CliHandle, "!\r\n");
        }
        
    }

    IPFWD_PROT_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : FsIpShowRunningConfigInterfaceInCxt                */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects in Ip for    */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4ContextId - VRF Id                               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
INT4
FsIpShowRunningConfigInterfaceInCxt (tCliHandle CliHandle, INT4 i4ContextId,
                                     UINT4 u4Module)
{
    INT1                ai1InterfaceName[CFA_MAX_PORT_NAME_LENGTH];
    INT1               *pi1InterfaceName = NULL;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextContextId = 0;

    pi1InterfaceName = &ai1InterfaceName[0];

    if (nmhGetFirstIndexFsMIFsIpifTable (&i4NextIfIndex) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFsMIFsIpifContextId (i4NextIfIndex, &i4NextContextId);
            if (i4NextContextId != i4ContextId)
            {
                i4IfIndex = i4NextIfIndex;
                continue;
            }
            MEMSET (ai1InterfaceName, 0, CFA_MAX_PORT_NAME_LENGTH);

            CfaCliConfGetIfName ((UINT4) i4NextIfIndex, pi1InterfaceName);

            if (u4Module != ISS_IP_SHOW_RUNNING_CONFIG)
            {
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "interface %s\r\n",
                                       pi1InterfaceName);
            }
            IPFWD_PROT_UNLOCK ();
            CliUnRegisterLock (CliHandle);

            InterfaceShowRunningConfig (CliHandle, i4NextIfIndex, u4Module);

            CliRegisterLock (CliHandle, IpFwdProtocolLock, IpFwdProtocolUnlock);
            IPFWD_PROT_LOCK ();
            u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r\n");
            i4IfIndex = i4NextIfIndex;

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                break;
            }
        }
        while (nmhGetNextIndexFsMIFsIpifTable (i4IfIndex, &i4NextIfIndex)
               != SNMP_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : FsIpShowInfoInCxt                                      */
/*                                                                           */
/* Description      : This function is invoked to display FSIP configuration */
/*                    parameters                                             */
/*                                                                           */
/*                                                                           */
/* Input Parameters : 1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/

INT4
FsIpShowInfoInCxt (tCliHandle CliHandle, INT4 i4VcId)
{
    INT4                i4GetVal = 0;
    tIpCliInfo          IpCliInfo;

    nmhGetFsMIFsIcmpSendRedirectEnable (i4VcId, &i4GetVal);
    IpCliInfo.IcmpStats.i4IcmpSendRedirectEnable = i4GetVal;

    nmhGetFsMIFsIcmpSendUnreachableEnable (i4VcId, &i4GetVal);
    IpCliInfo.IcmpStats.i4IcmpSendUnreachEnable = i4GetVal;

    nmhGetFsMIFsIcmpSendEchoReplyEnable (i4VcId, &i4GetVal);
    IpCliInfo.IcmpStats.i4IcmpSendEchoReplyEnable = i4GetVal;

    nmhGetFsMIFsIcmpNetMaskReplyEnable (i4VcId, &i4GetVal);
    IpCliInfo.IcmpStats.i4IcmpSendNetmaskReplyEnable = i4GetVal;

    nmhGetFsNoOfAggregatedRoutes (&i4GetVal);
    IpCliInfo.IpStats.i4AggRoutes = i4GetVal;

    nmhGetFsMIFsIpNumMultipath (i4VcId, &i4GetVal);
    IpCliInfo.IpStats.i4Multipath = i4GetVal;

    nmhGetFsMIFsIpLoadShareEnable (i4VcId, &i4GetVal);
    IpCliInfo.IpStats.i4IpLoadShareEnable = i4GetVal;

    nmhGetFsMIFsIpEnablePMTUD (i4VcId, &i4GetVal);
    IpCliInfo.IpStats.i4IpEnablePMTUD = i4GetVal;

    if (IpCliInfo.IcmpStats.i4IcmpSendRedirectEnable
        == ICMP_SEND_REDIRECT_ENABLE)
    {
        CliPrintf (CliHandle, " ICMP redirects are always sent\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " ICMP redirects are never sent\r\n");
    }

    if (IpCliInfo.IcmpStats.i4IcmpSendUnreachEnable
        == ICMP_SEND_UNREACHABLE_ENABLE)
    {
        CliPrintf (CliHandle, " ICMP unreachables are always sent\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " ICMP unreachables are never sent\r\n");
    }

    if (IpCliInfo.IcmpStats.i4IcmpSendEchoReplyEnable == ICMP_ECHO_REPLY_ENABLE)
    {
        CliPrintf (CliHandle, " ICMP echo replies are always sent\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " ICMP echo replies are never sent\r\n");
    }

    if (IpCliInfo.IcmpStats.i4IcmpSendNetmaskReplyEnable
        == ICMP_NMASK_REPLY_ENABLE)
    {
        CliPrintf (CliHandle, " ICMP mask replies are always sent\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " ICMP mask replies are never sent\r\n");
    }

    CliPrintf (CliHandle, " Number of aggregate routes is %d\r\n",
               IpCliInfo.IpStats.i4AggRoutes);

    CliPrintf (CliHandle, " Number of multi-paths is %d\r\n",
               IpCliInfo.IpStats.i4Multipath);

    if (IpCliInfo.IpStats.i4IpLoadShareEnable == IP_LOADSHARE_ENABLE)
    {
        CliPrintf (CliHandle, " Load sharing is enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Load sharing is disabled\r\n");
    }

    if (IpCliInfo.IpStats.i4IpEnablePMTUD == PMTU_ENABLED)
    {
        CliPrintf (CliHandle, " Path MTU discovery is enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Path MTU discovery is disabled\r\n");
    }
    return CLI_SUCCESS;
}
#endif
