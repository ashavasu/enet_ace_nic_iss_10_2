/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iptskmgr.c,v 1.71 2017/11/14 07:31:12 siva Exp $
 *
 * Description: Task related functions are handled here. 
 *
 *******************************************************************/
#ifndef _IPMAIN_C
#define _IPMAIN_C

#include "ipinc.h"
#ifdef MRI_WANTED
#include "mri.h"
#endif
#ifdef SNMP_2_WANTED
#include "fsipwr.h"
#endif

/**************************************************************************/

/**** $$TRACE_MODULE_NAME     = IP_FWD     ****/
/**** $$TRACE_SUB_MODULE_NAME = TXRX       ****/
/**** $$TRACE_ENABLE  ****/

/********************************************************************/
/******************** IP TSK ROUTINES *******************************/
/********************************************************************/

/**** $$TRACE_PROCEDURE_NAME  = IP_FWD_Process_Timer_Expiry  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD                     ****/
/*-------------------------------------------------------------------+
 *
 * Function           : IP_FWD_Process_Timer_Expiry
 *
 * Input(s)           : value
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action :
 * This procedure is invoked when the event indicating a timer expiry occurs.
 * This procedure finds the expired timers and invokes the corresponding timer
 * routines.
 *
+-------------------------------------------------------------------*/
VOID
IP_FWD_Process_Timer_Expiry (VOID)
{
    tTMO_APP_TIMER     *pList_head = NULL;
    tTMO_APP_TIMER     *pTimer = NULL;
    UINT1               u1Id = 0;

    /* Take a Protocol Lock Here */
    IPFWD_PROT_LOCK ();

    IP_GET_EXPIRED_TIMERS (gIpGlobalInfo.IpTimerListId, &pList_head);

    while (pList_head != NULL)
    {
        pTimer = pList_head;
        pList_head = IP_NEXT_TIMER (gIpGlobalInfo.IpTimerListId);

        u1Id = ((t_IP_TIMER *) pTimer)->u1Id;

        switch (u1Id)
        {
            case IP_REASM_TIMER_ID:
                ip_handle_reasm_timer_expiry ((t_IP_TIMER *) pTimer);
                break;

            case IRDP_ADVERT_TIMER_ID:
            case IRDP_SOLICIT_TIMER_ID:
            case IRDP_ROUTER_LIST_TIMER_ID:
            {
                IrdpTimerExpiryHandler ((t_IP_TIMER *) pTimer);
                break;
            }

            case IP_PMTU_TIMER_ID:
            {

                PmtuTimerExpiryHandler ((t_IP_TIMER *) pTimer);
                break;
            }

            default:
                IP_TRC (IP_MOD_TRC, ALL_FAILURE_TRC, IP_NAME,
                        "Expired timer's identity not known.\n");
                break;
        }

    }

    /* Unlock it */
    IPFWD_PROT_UNLOCK ();
}

/**** $$TRACE_PROCEDURE_NAME  = IP_TSK_Initialize_Protocol  ****/
/**** $$TRACE_PROCEDURE_LEVEL = MAIN                        ****/

/*-------------------------------------------------------------------+
 *
 * Function           : IP_TSK_Initialize_Protocol
 *
 * Input(s)           : u1_RoutingModule
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action :
 * IP Router initialization
 *
+-------------------------------------------------------------------*/
INT4
IP_TSK_Initialize_Protocol (VOID)
{
/* ISS */
    /* Create a Semaphore of Ipif Table in order to provide a data struture 
     * lock */
    if (OsixCreateSem ((const UINT1 *) IP_DATA_SEMA4, SELF, OSIX_GLOBAL,
                       &gIpGlobalInfo.IpDataSemId) != OSIX_SUCCESS)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Failure in creating the semaphore for data structure in IP\n");
        return IP_FAILURE;
    }

    /* Create a Semaphore of Protocol in order to provide a protocol lock */

    if (OsixCreateSem ((const UINT1 *) IP_PROT_SEMA4, SELF, OSIX_GLOBAL,
                       &gIpGlobalInfo.IpProtSemId) != OSIX_SUCCESS)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Failure in creating the task semaphore for IP\n");
        return IP_FAILURE;
    }

    if (OsixCreateSem ((const UINT1 *) IP_CXT_SEMA4, SELF, OSIX_GLOBAL,
                       &gIpGlobalInfo.IpCxtSemId) != OSIX_SUCCESS)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Failure in creating the semaphore for IP context in IP\n");
        return IP_FAILURE;
    }

    if (OsixCreateSem ((const UINT1 *) IP_HLREG_SEMA4, SELF, OSIX_GLOBAL,
                       &gIpGlobalInfo.IpHLRegSemId) != OSIX_SUCCESS)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Failure in creating the semaphore for HL Registration\n");
        return IP_FAILURE;
    }

    gIpGlobalInfo.u1ProxyArp_SubnetOption = ARP_PROXY_ENABLE;
    /* end of if to see if semaphore is created */
    /* Protocol Registration Table Initialization */
    IpInitRegTable ();

    /* Multi cast protocol registration for receiving multicast pkts from IP */
    IpInitMCastRegTable ();

#ifdef NPAPI_WANTED
    IpFsNpIpInit ();
#endif
    if (Ip_Mem_Init () == IP_FAILURE)
    {
        return (IP_FAILURE);
    }

    /* Register with VCM for context create and delete indication */
    if (IpRegisterWithVcm () == IP_FAILURE)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Failure in registering with VCM \n");
        return IP_FAILURE;
    }

    ipif_init ();

    /* Create the global RBTree to whold the statically configured PMTUs and 
     * dynamically learnt PMTU entries.
     */
    gIpGlobalInfo.PmtuTblRoot = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tPmtuInfo, PmtuEntryRBNode), PmtuRBTreeCmpFn);

/*Dummy compilation switch*/
#ifdef V4OV6_WANTED
    Ipv4ov6TunlInit ();
#endif

    if (IpCreateContext (IP_DEFAULT_CONTEXT) == IP_FAILURE)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Failure in creating default context in IP\n");
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/**** $$TRACE_PROCEDURE_NAME  = IPTaskMain  ****/
/**** $$TRACE_PROCEDURE_LEVEL = MAIN        ****/

 /***************************************************************************/
 /*                                                                         */
 /*  Function Name: IPFWDTaskMain                                           */
 /*                                                                         */
 /*  Description  : This is main task of IP_FWD task.                       */
 /*                 It continually waits for the events.                    */
 /*                 when event arrives, it calls different functions to     */
 /*                 handle the event.                                       */
 /*                                                                         */
 /*  Input(s)     : pDummy - Dummy pointer to remove warnings               */
 /*                                                                         */
 /*  Output(s)    : None.                                                   */
 /*                                                                         */
 /*                                                                         */
 /*  Returns      : NA.                                                     */
 /*                                                                         */
 /***************************************************************************/

#ifdef __STDC__

EXPORT VOID
IPFWDTaskMain (INT1 *pDummy)
#else

EXPORT VOID
IPFWDTaskMain ()
     INT1               *pDummy;
#endif
{
    UINT4               u4EventMask = 0;
    UINT4               u4RetVal = 0;

    if (OsixTskIdSelf (&gIpGlobalInfo.IpTaskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        IP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (IP_TSK_Initialize_Protocol () != IP_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        IP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    u4RetVal = IP_CREATE_TMR_LIST ((const UINT1 *) IPFW_TASK_NAME,
                                   IPFWD_TIMER_EXPIRY_EVENT,
                                   &gIpGlobalInfo.IpTimerListId);

    if (OsixQueCrt ((UINT1 *) IPFW_PACKET_ARRIVAL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    IPFW_PACKET_ARRIVAL_Q_DEPTH, &gIpGlobalInfo.IpPktQId)
        != OSIX_SUCCESS)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Fatal error in creating IP process's queue.\n");
        /* Indicate the status of initialization to the main routine */
        IP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) IPFW_CFA_INTERFACE_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    FsIPSizingParams[MAX_IP_IPIF_QUE_DEPTH_SIZING_ID].
                    u4PreAllocatedUnits,
                    &gIpGlobalInfo.IpCfaIfQId) != OSIX_SUCCESS)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Fatal error in creating IP CFA Interface queue.\n");
        /* Indicate the status of initialization to the main routine */
        IP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    IrdpInitialize ();

    CfaNotifyIpUp ();
#ifdef MRI_WANTED
    /* MRI module does not have a task. It executes in CLI/SNMP 
       context only. So Initialize the MRI module 
     */
    MriInit ();
#endif
    UNUSED_PARAM (pDummy);
    /* Waiting for Signal from CFA to proceed */
    /* CFA sends this signal if atleast one interface has a valid IP address */

    /* Indicate the status of initialization to the main routine */
    IP_INIT_COMPLETE (OSIX_SUCCESS);
#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
/*Dummy compilation switch*/
#ifdef V4OV6_WANTED
    RegisterFSV4OV6 ();
#endif
    RegisterFSIP ();
    RegisterFSMPIP ();

    /*
     * Remove this #ifdef when deleting the deprecated objects at 
     * standard ip mib to support backward comp.
     * This RegisterSTDIP is replaced by RegisterSTDIPvx in futrure/netipvx 
     */
#ifdef REMOVE_ON_STDIP_DEPR_OBJ_DELETE
    RegisterSTDIP ();
#endif /* REMOVE_ON_STDIP_DEPR_OBJ_DELETE */

#endif
    /* To add route in Linux kernel using Netlink socket,
     * if DHCP is Linux IP and ISS is compiled for Fsip */
#ifdef DHCP_LNXIP_WANTED
    RtmSubTaskInit ();
#endif
    FOREVER
    {
        OsixEvtRecv (gIpGlobalInfo.IpTaskId,
                     IPFWD_TIMER_EXPIRY_EVENT | IPFWD_PACKET_ARRIVAL_EVENT |
                     IPFWD_CFA_INTERFACE_EVENT | IPFWD_VCM_CONTEXT_EVENT |
                     IPFW_VCM_INTERFACE_EVENT | IPFWD_IPV4_ENABLE_EVENT |
                     IPFWD_PROXYARP_ADMIN_STATUS_EVENT,
                     IPFW_EVENT_WAIT_FLAGS, &u4EventMask);

        if (u4EventMask & IPFWD_TIMER_EXPIRY_EVENT)
        {
            IP_FWD_Process_Timer_Expiry ();
        }

        if (u4EventMask & IPFWD_PACKET_ARRIVAL_EVENT)
        {
            IP_FWD_Process_Pkt_Arrival_Event ();
        }
        if (u4EventMask & IPFWD_CFA_INTERFACE_EVENT)
        {
            IPProcessMsgQue ();
        }
        if (u4EventMask & IPFWD_VCM_CONTEXT_EVENT)
        {
            IPProcessMsgQue ();
        }
        if (u4EventMask & IPFW_VCM_INTERFACE_EVENT)
        {
            IPProcessMsgQue ();
        }
        if (u4EventMask & IPFWD_IPV4_ENABLE_EVENT)
        {
            IPProcessMsgQue ();
        }
        if (u4EventMask & IPFWD_PROXYARP_ADMIN_STATUS_EVENT)
        {
            IPProcessMsgQue ();
        }

    }
    UNUSED_PARAM (u4RetVal);
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function Name: ipChangeIfaceOperStatus                                 */
 /*                                                                         */
 /*  Description  : Extracts the interface from pBuf.                       */
 /*                 changes the oper status of the interface accordingly.   */
 /*                 notifies higher layer applications.                     */
 /*                                                                         */
 /*  Input(s)     : pBuf     - Pointer to the buffer.                       */
 /*                 u4Status - Status of the interface.                     */
 /*                                                                         */
 /*  Output(s)    : None.                                                   */
 /*                                                                         */
 /*                                                                         */
 /*  Returns      : None.                                                   */
 /*                                                                         */
 /***************************************************************************/

VOID
ipChangeIfaceOperStatus (UINT2 u2Port, UINT4 u4Status)
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4BitMap = 0;
    UINT1               u1OperStatus = 0;
    UINT4               u4IfaceIndex = 0;
    UINT1               u1Ipv4EnableStatus = IPVX_IPV4_ENABLE_STATUS_DOWN;
    tArpQMsg            ArpQMsg;
    tNetIpv4IfInfo      IpIntfInfo;
    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));

    /* New CFA sets the logical index in u2_SubReferenceNum */
    /* Take a Data Structure Lock */

    if (u2Port >= IPIF_MAX_LOGICAL_IFACES)
    {
        return;
    }
    IPFWD_DS_LOCK ();
    u1OperStatus = gIpGlobalInfo.Ipif_config[u2Port].u1Oper;
    u4IfaceIndex = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    u1Ipv4EnableStatus = gIpGlobalInfo.Ipif_config[u2Port].u1Ipv4EnableStatus;
    pIpCxt = gIpGlobalInfo.Ipif_config[u2Port].pIpCxt;
    IPFWD_DS_UNLOCK ();

    if (NULL == pIpCxt)
    {
        return;
    }

    if (u1OperStatus != (UINT1) u4Status)
    {

        /* set the interface oper status and mark it to inform others
         * IP need not know the ADMIN status. updating admin status just to
         * be backward compatible.
         */
        IPIF_CONFIG_SET_OPER_STATUS (u2Port, (UINT1) u4Status);
        IPIF_CONFIG_SET_ADMIN_STATUS (u2Port, (UINT1) u4Status);

        /* If Ipv4EnableStatus is = Enabled (True) send the Notification 
         * to HL otherwise return */

        if (u1Ipv4EnableStatus == IPVX_IPV4_ENABLE_STATUS_UP)
        {

            u4BitMap |= OPER_STATE;
            if (u4Status == IPIF_OPER_DISABLE)
            {
                IP_CXT_TRC_ARG1 (pIpCxt->u4ContextId, IP_MOD_TRC,
                                 CONTROL_PLANE_TRC, IP_NAME,
                                 "oper status changed to down for interface %d\n",
                                 u4IfaceIndex);

                IrdpDisableInterface (u2Port);
            }
            else
            {
                IP_CXT_TRC_ARG1 (pIpCxt->u4ContextId, IP_MOD_TRC,
                                 CONTROL_PLANE_TRC, IP_NAME,
                                 "oper status changed to up for interface %d\n",
                                 u4IfaceIndex);

                IrdpEnableInterface (u2Port);

                if (u1OperStatus == CFA_IF_UP)
                {
                    MEMSET (&IpIntfInfo, 0, sizeof (tNetIpv4IfInfo));
                    if (NetIpv4GetIfInfo (u2Port, &IpIntfInfo) ==
                        NETIPV4_SUCCESS)
                    {
                        if (IpIntfInfo.u4Addr != ZERO)
                        {
                            ArpQMsg.u4MsgType = CFA_GRAT_ARP_REQ;
                            ArpQMsg.u4EncapType = CFA_ENCAP_ENETV2;
                            ArpQMsg.pBuf = NULL;
                            ArpQMsg.u2Port = u2Port;
                            ArpQMsg.u4IpAddr = IpIntfInfo.u4Addr;

                            if (ArpEnqueuePkt (&ArpQMsg) != ARP_SUCCESS)
                            {
                                IP_CXT_TRC_ARG1 (pIpCxt->u4ContextId,
                                                 IP_MOD_TRC, CONTROL_PLANE_TRC,
                                                 IP_NAME,
                                                 "IpIf: Gratituous Arp Sending is Failed\n",
                                                 u4IfaceIndex);
                                return;
                            }
                        }
                    }
                }

            }

            /* Reflects the status of interfaces on the routes whose gateway can be
             * reached on those interface. 
             */
            RtmProcessRouteForIfStatChange (u2Port, (INT1) u4Status);

            /* notify other protocols who has registered */
            IpIfStateChngNotify (u2Port, u4BitMap);
        }
    }
    IP_CFG_SET_IF_TBL_CHANGE_TIME ();
}

/**** $$TRACE_PROCEDURE_NAME  =  fs_ip_init ****/
/**** $$TRACE_PROCEDURE_LEVEL =  LOW    ****/
/*-------------------------------------------------------------------+
 * Function           : fs_ip_init_InCxt
 *
 * Input(s)           : u4ContextId - context id
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Initialization routine for IP for a particular context. Sets up IP timer 
 * list and the reassembly list in the context. Initializes the Ip_cfg structure 
 * with defaults in the context
 *
+-------------------------------------------------------------------*/
INT4
fs_ip_init_InCxt (tIpCxt * pIpCxt)
{

    MEMSET (&pIpCxt->Ip_cfg, 0, sizeof (t_IP_CFG));
    MEMSET (&pIpCxt->Ip_stats, 0, sizeof (t_IP_STATS));

    /* Initialise IP Configuration Structure for default context */
    pIpCxt->Ip_cfg.pIpCxt = pIpCxt;
    pIpCxt->Ip_stats.pIpCxt = pIpCxt;
    IP_CFG_SET_TTL (pIpCxt, IP_DEF_TTL);
    IP_CFG_SET_REASM_TIME (pIpCxt, IP_DEF_REASM_TIME);
    IP_CFG_SET_FORWARDING (pIpCxt, IP_FORW_ENABLE);
    IP_CFG_SET_PROPAGATE (pIpCxt, IP_PROP_ENABLE);
    IP_CFG_SET_ROUTING_PROTOCOL (pIpCxt, IP_ROUTING_PROTO_NONE);
    IP_CFG_SET_OPT_PROC_ENABLE (pIpCxt, IP_OPT_PROC_ENABLE);
    IP_CFG_SET_NUM_MULTIPATH (pIpCxt, DEF_NUM_MULTIPATH);
    IP_CFG_SET_LOADSHARE_ENABLE (pIpCxt, IP_LOADSHARE_DISABLE);

    ipfrag_init_InCxt (pIpCxt);

    pIpCxt->u2PmtuStatusFlag = PMTU_DISABLED;
    pIpCxt->u2PmtuCfgEntryAge = PMTU_DEF_AGE;

    /* Intialisate the Aggregate Route Table */
    if (IpAggrRtInitInCxt (pIpCxt->u4ContextId) == IP_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : Ip_Mem_Init
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Memory initialization routine.
 *
+-------------------------------------------------------------------*/
INT4
Ip_Mem_Init (VOID)
{
    INT4                i4Ret = OSIX_FAILURE;
    i4Ret = IpSizingMemCreateMemPools ();
    if (i4Ret == OSIX_FAILURE)
    {
        IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                "Mem pool allocation for IpIfPool failed.\n");
        return IP_FAILURE;
    }
    gIpGlobalInfo.Ip_mems.IpCxtPoolId = IPMemPoolIds[MAX_IP_CONTEXTS_SIZING_ID];
    gIpGlobalInfo.Ip_mems.ReasmPoolId =
        IPMemPoolIds[MAX_IP_REASM_LISTS_SIZING_ID];
    gIpGlobalInfo.Ip_mems.FragPoolId = IPMemPoolIds[MAX_IP_FRAG_PKTS_SIZING_ID];
    gIpGlobalInfo.Ip_mems.IfacePoolId =
        IPMemPoolIds[MAX_IP_IPIF_QUE_DEPTH_SIZING_ID];
    gIpGlobalInfo.Ip_mems.IpIfPoolId =
        IPMemPoolIds[MAX_IP_BROADCAST_Q_DEPTH_SIZING_ID];
    gIpGlobalInfo.Ip_mems.PmtuPoolId =
        IPMemPoolIds[MAX_IP_PMTU_ENTRIES_SIZING_ID];
    gIpGlobalInfo.Ip_mems.IpIfInfoPoolId =
        IPMemPoolIds[MAX_IP_LOGICAL_IFACES_SIZING_ID];
    gIpGlobalInfo.Ip_mems.IpIfStatPoolId =
        IPMemPoolIds[MAX_IP_IF_STATS_SIZING_ID];
    gIpGlobalInfo.Ip_mems.AggRtPoolId =
        IPMemPoolIds[MAX_IP_AGG_ROUTES_SIZING_ID];
    gIpGlobalInfo.Ip_mems.AggAdvRtPoolId =
        IPMemPoolIds[MAX_IP_CIDR_EXPL_ROUTES_SIZING_ID];
#ifdef TUNNEL_WANTED
    gIpGlobalInfo.Ip_mems.IpTnlIfPoolId =
        IPMemPoolIds[MAX_IP_TUNNEL_INTERFACES_SIZING_ID];
#endif

    IP_TRC (IP_MOD_TRC, INIT_SHUT_TRC, IP_NAME,
            "Mem pool initialization complete.\n");
    return IP_SUCCESS;

}

/*-------------------------------------------------------------------+
 *
 * Function           : IpTxDatagram
 *
 * Input(s)           : pBuf, u2Port, u4Dest, u1TxType
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action :
 * Hand over the packet to CFA. If it is ethernet, then call ARP to resolve 
 * the next hop mac address.
 *
+-------------------------------------------------------------------*/
INT4
IpTxDatagram (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, UINT4 u4Dest,
              UINT1 u1TxType, UINT2 u2Len)
{

    tArpQMsg            ArpQMsg;
    tIpCxt             *pIpCxt = NULL;
    INT4                i4RetVal = IP_SUCCESS;
    UINT4               u4IfIndex = 0;
    UINT4               u4UnnumAssocIPIf = 0;
    UINT4               u4CxtId = 0;
    UINT1               u1IfType = 0;
    UINT1               u1ArpReqFlag = TRUE;
    UINT1               u1EncapType = CFA_ENCAP_NONE;
    UINT1               au1HwAddr[IP_MAX_PHYS_LENGTH];
    t_IP_HEADER         IpHdr;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4SrcIp = 0;
    UINT4               u4CfaInIfIndex = 0;
    UINT4               u4CfaOutIfIndex = 0;
    INT4                i4SrcAvailable = IP_SUCCESS;

    pIpCxt = UtilIpGetCxtFromIpPortNum (u2Port);
    if (NULL == pIpCxt)
    {
        return IP_FAILURE;
    }
    u4CxtId = pIpCxt->u4ContextId;

    IP4SYS_INC_OUT_OCTETS (u2Len, u4CxtId);
    IP4IF_INC_OUT_OCTETS (u2Port, u2Len);

    IP4SYS_INC_OUT_TRANSMITS (u4CxtId);
    IP4IF_INC_OUT_TRANSMITS (u2Port);

    IPFWD_DS_LOCK ();

    u4IfIndex = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    u1IfType = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType;

    /* Unlock it */
    IPFWD_DS_UNLOCK ();

#ifdef TUNNEL_WANTED
    if (u1IfType == IP_TUNNEL_INTERFACE_TYPE)
    {
        /* Extract the IP header information */
        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpHdr, 0,
                                       CFA_IP_HDR_LEN) < CFA_IP_HDR_LEN)
        {
            IP_CXT_TRC (pIpCxt->u4ContextId, IP_MOD_TRC,
                        INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                        "Extracting IP Header failed \r\n");
            return IP_FAILURE;
        }
        return (Ip4TunlSend (&IpHdr, pBuf, u2Port));
    }
#endif

    /* This the check is introduced so as to avoid the multicast 
     * and broadcast packets being dropped */
    if ((u1TxType == IP_UCAST) || (u1TxType == IP_FOR_THIS_NODE))
    {
        /* Extract the IP header information */
        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpHdr, 0,
                                       CFA_IP_HDR_LEN) < CFA_IP_HDR_LEN)
        {
            IP_CXT_TRC (pIpCxt->u4ContextId, IP_MOD_TRC,
                        INIT_SHUT_TRC | ALL_FAILURE_TRC, IP_NAME,
                        "Extracting IP Header failed \r\n");
            return IP_FAILURE;
        }

        /* Dont trigger route installation to Hardware for self originated
         * packets */
        u4SrcIp = OSIX_HTONL (IpHdr.u4Src);

        NetIpv4GetCfaIfIndexFromPort ((UINT4) u2Port, &u4CfaOutIfIndex);

        /* We don't install routes added over the OOB interface into
         * Hardware.So we should software forward the packets
         * Same in the case when NAT is enabled. When the packets are to
         * be forwarded on the port on which NAT is enabled, the packets must be
         * translated. So we should do software forwarding. Same when a packet with
         * source port having NAT enabled. (Next check after deriving source route)*/
        if ((CfaIsMgmtPort (u4CfaOutIfIndex) != TRUE)
            && (CFA_CHECK_IF_NAT_ENABLE (u4CfaOutIfIndex) != NAT_ENABLE)
            && (u1IfType != CFA_PPP))
        {
            if (NetIpv4IfIsOurAddressInCxt (u4CxtId, u4SrcIp) !=
                NETIPV4_SUCCESS)
            {
                MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
                MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

                RtQuery.u4DestinationIpAddress = u4SrcIp;
                RtQuery.u4DestinationSubnetMask = IP_DEF_NET_MASK;
                RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
                RtQuery.u4ContextId = u4CxtId;

                if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
                {
                    i4SrcAvailable = IP_FAILURE;
                }

                if (i4SrcAvailable == IP_SUCCESS)
                {
                    /* Checking if the source was the OOB interface. 
                     * If Yes, we do software forwarding */
                    if (NetIpv4GetCfaIfIndexFromPort
                        (NetIpRtInfo.u4RtIfIndx,
                         &u4CfaInIfIndex) == NETIPV4_FAILURE)
                    {
                        return IP_FAILURE;
                    }
                }
                /* If source route info is not available dont drop the packet */
                if ((i4SrcAvailable == IP_FAILURE)
                    || ((CfaIsMgmtPort (u4CfaInIfIndex) != TRUE)
                        && (CFA_CHECK_IF_NAT_ENABLE (u4CfaInIfIndex) !=
                            NAT_ENABLE)))
                {
                    i4RetVal = ipResolveDestEnetAddr (u4CfaOutIfIndex, u1TxType,
                                                      u4Dest, au1HwAddr,
                                                      &(u1ArpReqFlag),
                                                      &u1EncapType);
                    if (i4RetVal != IP_SUCCESS)
                    {
#ifdef ARP_WANTED
                        CfaGetIfUnnumAssocIPIf (u4CfaOutIfIndex,
                                                &u4UnnumAssocIPIf);
                        if (0 != u4UnnumAssocIPIf)
                        {
                            i4RetVal = IP_SUCCESS;
                            /*This is a unnumbered interface use peer mac address as the destination */

                            CfaGetIfUnnumPeerMac (u4CfaOutIfIndex, au1HwAddr);
                        }
                        else
                        {
                            /* Trigger ARP resolution for installation to H/W */
                            /* In case of Target, IP unicast packet in the slow path is given 
                             * to ARP module to trigger installation of route/ARP entries */
                            MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
                            ArpQMsg.u4MsgType = IP_PKT_FOR_RESOLVE;
                            ArpQMsg.u2Protocol = CFA_ENET_IPV4;
                            ArpQMsg.u4EncapType = CFA_ENCAP_ENETV2;
                            ArpQMsg.pBuf = pBuf;
                            ArpQMsg.u2Port = u2Port;

                            if (ArpEnqueueIpPkt (&ArpQMsg) != ARP_SUCCESS)
                            {
                                IP_CXT_TRC (pIpCxt->u4ContextId, IP_MOD_TRC,
                                            INIT_SHUT_TRC | ALL_FAILURE_TRC,
                                            IP_NAME,
                                            "IP Packet eneque to ARP failed \r\n");
                                return IP_FAILURE;
                            }
                            return IP_SUCCESS;
                        }
#endif
                    }
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                    else
                    {
                        /* Remove the Entry created in the DLF Hash Table */
                        IpFsCfaHwRemoveIpNetRcvdDlfInHash (u4CxtId, u4Dest,
                                                           0xffffffff);
                    }
#endif
                }
            }
        }
    }
    UNUSED_PARAM (u2Len);

    if ((u1IfType == IP_ENET_PHYS_INTERFACE_TYPE) ||
        (u1IfType == IP_OTHER_PHYS_INTERFACE_TYPE) ||
        (u1IfType == IP_LAGG_INTERFACE_TYPE) ||
        (u1IfType == IP_L3IPVLAN_PHYS_INTERFACE_TYPE) ||
        (u1IfType == IP_PSEUDO_WIRE_INTERFACE_TYPE) ||
        (u1IfType == IP_L3SUBIF_TYPE)
#ifdef WGS_WANTED
        || (u1IfType == IP_L2VLAN_PHYS_INTERFACE_TYPE)
#endif
        )
    {
        /* u1ArpReqFlag is not needed. But 
         * removing that would result in extra changes in code.
         * hence left over for pack2
         */
        IPIF_CONFIG_GET_ENCAP_TYPE (u2Port, u1EncapType);

        i4RetVal =
            ipResolveDestEnetAddr (u4IfIndex, u1TxType, u4Dest, au1HwAddr,
                                   &(u1ArpReqFlag), &u1EncapType);
        if (i4RetVal != IP_SUCCESS)
        {

            CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4UnnumAssocIPIf);
            if (0 != u4UnnumAssocIPIf)
            {
                i4RetVal = IP_SUCCESS;
                /*This is a unnumbered interface use peer mac address as the destination */

                CfaGetIfUnnumPeerMac (u4IfIndex, au1HwAddr);
            }
            else
            {
                IP_CXT_TRC_ARG2 (pIpCxt->u4ContextId, IP_MOD_TRC,
                                 CONTROL_PLANE_TRC, IP_NAME,
                                 "MAC addr to be resolved for %x, on interface %d.\n",
                                 u4Dest, u4IfIndex);

                IP_PKT_DUMP (IP_MOD_TRC, DUMP_TRC, IP_NAME, pBuf, u2Len,
                             "IP pkt enqd to ARP module.\n");
                MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
                ArpQMsg.u4MsgType = IP_PKT_FOR_RESOLVE;
                ArpQMsg.u2Protocol = CFA_ENET_IPV4;
                ArpQMsg.u4EncapType = CFA_ENCAP_ENETV2;
                ArpQMsg.pBuf = pBuf;
                ArpQMsg.u2Port = u2Port;
                i4RetVal = ArpEnqueueIpPkt (&ArpQMsg);

                return i4RetVal;
            }

        }
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
        else
        {
            /* Remove the Entry created in the DLF Hash Table */
            IpFsCfaHwRemoveIpNetRcvdDlfInHash (u4CxtId, u4Dest, 0xffffffff);
        }
#endif
    }
/*Dummy compilation switch*/
#ifdef V4OV6_WANTED
    else if (u1IfType == IP_V4OVERV6_TUNNEL_INTERFACE_TYPE)
    {
        return (Ipv4ov6TunlSend (u2Port, u2Len, pBuf));
    }
#endif
    if (i4RetVal == IP_SUCCESS)
    {
        IP_PKT_DUMP (IP_MOD_TRC, DUMP_TRC, IP_NAME, pBuf, u2Len,
                     "IP pkt enqd to LL module.\n");

        i4RetVal = CfaHandlePktFromIp (pBuf, u4IfIndex, u4Dest, au1HwAddr,
                                       IP_PROTOCOL, u1TxType, u1EncapType);
        if (i4RetVal != CFA_SUCCESS)
        {
            i4RetVal = IP_FAILURE;
        }
    }
    return i4RetVal;
}

VOID
IPProcessMsgQue ()
{
    tIfaceQMsg         *pIfaceQMsg = NULL;

    while (OsixQueRecv (gIpGlobalInfo.IpCfaIfQId, (UINT1 *) &pIfaceQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pIfaceQMsg == NULL)
        {
            return;
        }

        /* Take a Protocol Lock Here */
        IPFWD_PROT_LOCK ();

        switch (pIfaceQMsg->u1MsgType)
        {
            case IPIF_CREATE_INTERFACE:
            {
                IpRegisterNetworkInterface (pIfaceQMsg);
                break;
            }

            case IPIF_UPDATE_INTERFACE:
            {
                IpNetworkInterfaceConfig (pIfaceQMsg);
                break;
            }

            case IPIF_DELETE_INTERFACE:
            {
                /* Utility function internally takes the lock */
                IPFWD_PROT_UNLOCK ();
                IpClearInterfaceInfo (pIfaceQMsg->u2IfIndex);
                IPFWD_PROT_LOCK ();
                IpDeRegisterNetworkInterface (pIfaceQMsg->u2Port);
                break;
            }

            case IP_INTERFACE_DOWN:
            {
                ipChangeIfaceOperStatus (pIfaceQMsg->u2Port, IPIF_OPER_DISABLE);
                break;
            }

            case IP_INTERFACE_UP:
            {
                ipChangeIfaceOperStatus (pIfaceQMsg->u2Port, IPIF_OPER_ENABLE);
                break;
            }

            case IP_CONTEXT_DELETE:
            {
                IpClearContextInfo (pIfaceQMsg->u4ContextId);
                IpDeleteContext (pIfaceQMsg->u4ContextId);
                break;
            }

            case IP_INTERFACE_MAP:
            {
                IpHandleIfaceMapping (pIfaceQMsg->u4ContextId,
                                      pIfaceQMsg->u2Port);
                break;
            }

            case IP_INTERFACE_UNMAP:
            {
                /* Utility function internally takes the lock */
                IPFWD_PROT_UNLOCK ();
                IpClearInterfaceInfo (pIfaceQMsg->u2IfIndex);
                IPFWD_PROT_LOCK ();
                IpHandleIfaceMapping (IP_DEFAULT_CONTEXT, pIfaceQMsg->u2Port);

                break;
            }
            case IP_SEC_CREATE:
            {
                IpIfSecAddrChange (pIfaceQMsg, IF_SECIP_CREATED);
                break;
            }
            case IP_SEC_DELETE:
            {
                IpIfSecAddrChange (pIfaceQMsg, IF_SECIP_DELETED);
                break;
            }
            case IPV4_ENABLE:
            {
                IPvxSetIpv4IfEnableStatus (pIfaceQMsg->u2IfIndex,
                                           IPVX_IPV4_ENABLE_STATUS_UP);
                break;
            }
            case IPV4_DISABLE:
            {
                IPvxSetIpv4IfEnableStatus (pIfaceQMsg->u2IfIndex,
                                           IPVX_IPV4_ENABLE_STATUS_DOWN);
                break;
            }
            case IP_PROXYARP_ADMIN_STATUS_ENABLE:
            {
                IPvxSetProxyArpAdminStatus (pIfaceQMsg->u2IfIndex,
                                            ARP_PROXY_ENABLE);
                break;
            }
            case IP_PROXYARP_ADMIN_STATUS_DISABLE:
            {
                IPvxSetProxyArpAdminStatus (pIfaceQMsg->u2IfIndex,
                                            ARP_PROXY_DISABLE);
                break;
            }
            default:
                break;
        }

        IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.IfacePoolId, pIfaceQMsg);

        /* UnLock it */
        IPFWD_PROT_UNLOCK ();
    }
}

VOID
IP_FWD_Process_Pkt_Arrival_Event ()
{
    tMcastIpSendParams *pIpMcastParams = NULL;
    /* For multicast data and control packets */
    tIpBuf             *pBuf = NULL;
    tIpParms           *pIpParms = NULL;
    tIpCxt             *pIpCxt = NULL;
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
    t_IP_HEADER         TmpIpHdr;
    t_IP_HEADER        *pIpHdr = NULL;
    INT4                i4RetVal = 0;
    UINT4               u4Dest = 0;
#endif
    UINT4               u4NoOfQueuedMsgs = IP_QUE_DEF_PKTS_PER_EVENT;

    if (OsixQueNumMsg (gIpGlobalInfo.IpPktQId, &u4NoOfQueuedMsgs)
        == OSIX_FAILURE)
    {
        u4NoOfQueuedMsgs = IP_QUE_DEF_PKTS_PER_EVENT;
    }

    while (OsixQueRecv (gIpGlobalInfo.IpPktQId, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        u4NoOfQueuedMsgs--;

        pIpParms = (tIpParms *) IP_GET_MODULE_DATA_PTR (pBuf);

        /* Take a Protocol Lock Here */
        IPFWD_PROT_LOCK ();

        switch (pIpParms->u1Cmd)
        {
            case IP_LAYER2_DATA:
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                pIpHdr = (t_IP_HEADER *) (VOID *) IP_GET_DATA_PTR_IF_LINEAR
                    (pBuf, 0, IP_HDR_LEN);

                if (pIpHdr == NULL)
                {
                    /* The header is not contiguous in the buffer */
                    pIpHdr = &TmpIpHdr;
                    /* Copy the header */
                    IP_COPY_FROM_BUF (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
                }
                u4Dest = OSIX_NTOHL (pIpHdr->u4Dest);

                i4RetVal = IpInputProcessPkt (pIpParms->u2Port, pBuf);
                if (i4RetVal != IP_SUCCESS)
                {
                    /* Remove the Entry created in the DLF Hash Table */
                    IpFsCfaHwRemoveIpNetRcvdDlfInHash (pIpParms->u4ContextId,
                                                       u4Dest, 0xffffffff);
                }
#else
                IpInputProcessPkt (pIpParms->u2Port, pBuf);
#endif

                break;

            case IP_LAYER4_DATA:
            {
                t_IP_SEND_PARMS    *pParms = NULL;

                pParms = (t_IP_SEND_PARMS *) pIpParms;

                pIpCxt = UtilIpGetCxt (pParms->u4ContextId);
                if (NULL == pIpCxt)
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    IPFWD_PROT_UNLOCK ();
                    return;
                }

                ip_send_InCxt (pIpCxt, pParms->u4Src, pParms->u4Dest,
                               pParms->u1Proto, pParms->u1Tos, pParms->u1Ttl,
                               pBuf, pParms->u2Len, pParms->u2Id, pParms->u1Df,
                               pParms->u1Olen, pParms->u2Port);
                break;
            }

            case IP_ICMP_DATA:
            {
                t_ICMP_MSG_PARMS   *pParms = NULL;
                t_ICMP              Icmp;
                pParms = (t_ICMP_MSG_PARMS *) pIpParms;

                if (pParms->i1Error_or_request == FALSE)
                {
                    Icmp.i1Type = pParms->Msg.Err.i1Type;
                    Icmp.i1Code = pParms->Msg.Err.i1Code;
                    Icmp.u4ContextId = pParms->u4ContextId;
                    icmp_error_msg (pBuf, &Icmp);
                    IP_RELEASE_BUF (pBuf, FALSE);

                }
                else
                {
                    Icmp.i1Type = pParms->Msg.Err.i1Type;
                    Icmp.i1Code = pParms->Msg.Err.i1Code;
                    Icmp.u4ContextId = pParms->u4ContextId;
                    if ((Icmp.i1Type == ICMP_DEST_UNREACH) &&
                        (Icmp.i1Code == ICMP_FRAG_NEEDED))
                    {
                        ICMP_NH_MTU (Icmp) = (UINT2) pParms->Msg.Err.u4Parm1;
                    }
                    else
                    {
                        Icmp.args.u4Unused = 0;
                    }
                    icmp_error_msg (pBuf, &Icmp);
                    IP_RELEASE_BUF (pBuf, FALSE);
                }
                break;
            }
#ifdef SLI_WANTED
            case IP_RAW_DATA:
            {
                t_IP                Ip;
#if ((!defined NPAPI_WANTED) || (!defined CFA_WANTED))
                INT4                i4RetVal = 0;
#endif
                INT4                i4Port = 0;
                tPmtuInfo          *pPmtuEntry = NULL;
                pIpCxt = UtilIpGetCxt (pIpParms->u4ContextId);
                if (NULL == pIpCxt)
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    IPFWD_PROT_UNLOCK ();
                    return;
                }

                i4RetVal = IpExtractHdr (&Ip, pBuf);

                if ((i4RetVal == IP_SUCCESS) && (Ip.u4Dest != IP_ANY_ADDR))
                {
                    IP_PKT_DUMP (IP_MOD_TRC, DUMP_TRC, IP_NAME, pBuf,
                                 Ip.u2Len,
                                 "IP (raw) pkt Rx from application.\n");

                    IP4SYS_INC_OUT_REQUESTS (pIpCxt->u4ContextId);
                    if (Ip.u2Id == 0)
                    {
                        Ip.u2Id = (UINT2) pIpCxt->Ip_stats.u4Out_requests;
                        IP_PKT_ASSIGN_ID (pBuf, Ip.u2Id);
                    }
                    if (pIpCxt->u2PmtuStatusFlag == PMTU_ENABLED)
                    {
                        pPmtuEntry =
                            PmtuLookupEntryInCxt (pIpCxt, Ip.u4Dest, Ip.u1Tos);

                        if (pPmtuEntry != NULL)
                        {
                            if (pPmtuEntry->u1PmtuDiscFlag & IPPMTUDISC_DO)
                            {
                                pPmtuEntry->u1PmtuDiscFlag |=
                                    IPPMTUDISC_ALLOW_FRAG;
                            }
                        }

                        Ip.u2Fl_offs |= IP_DF;
                    }
                    IP_PKT_ASSIGN_FLAGS (pBuf, Ip.u2Fl_offs);

                    if (Ip.u1Ttl == 0)
                    {
                        Ip.u1Ttl = IP_CFG_GET_TTL (pIpCxt->u4ContextId);
                        IP_PKT_ASSIGN_TTL (pBuf, Ip.u1Ttl);
                    }

                    IP_PKT_ASSIGN_CKSUM (pBuf, 0);
                    Ip.u2Cksum = IpCalcCheckSum (pBuf,
                                                 (UINT4) (IP_HDR_LEN +
                                                          Ip.u2Olen), 0);
                    IP_PKT_ASSIGN_CKSUM (pBuf, Ip.u2Cksum);
                    if (IP_IS_ADDR_CLASS_D (Ip.u4Dest))
                    {
                        i4Port = pIpParms->u2Port;
                    }
                    else
                    {
                        i4Port = ipif_get_handle_from_addr_InCxt
                            (pIpCxt->u4ContextId, Ip.u4Src);
                    }

                    IP4IF_INC_OUT_REQUESTS ((UINT2) i4Port);
                    IpOutputProcessPktInCxt (pIpCxt, pBuf, &Ip, (UINT2) i4Port);
                }
                else
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                }
            }
                break;
#endif
            case IP_MCAST_DATA_PACKET_FROM_MRM:
                /*
                 * For data packets from RTM, IP expects that the IP header
                 * has been already filled. Just duplicate the packet and
                 * send it on multiple interfaces.
                 */

                pIpMcastParams = (tMcastIpSendParams *)
                    IP_GET_MODULE_DATA_PTR (pBuf);
                pIpCxt = UtilIpGetCxt (IP_DEFAULT_CONTEXT);
                if (pIpCxt == NULL)
                {
                    UtlShMemFreeOifList (pIpMcastParams->pu4OIfList);
                    IP_RELEASE_BUF (pBuf, FALSE);
                }
                else
                {
                    if (IpSendMcastPktInCxt (pIpCxt, pBuf,
                                             pIpMcastParams) == IP_FAILURE)
                    {
                        UtlShMemFreeOifList (pIpMcastParams->pu4OIfList);
                        IP_RELEASE_BUF (pBuf, FALSE);
                    }
                }

                break;

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                IpMbsmUpdateLCStatus (pBuf, pIpParms->u1Cmd);
                IP_RELEASE_BUF (pBuf, FALSE);
                break;
#endif
            default:
                IP_RELEASE_BUF (pBuf, FALSE);
                break;
        }

        /* Unlock it */
        IPFWD_PROT_UNLOCK ();

        /* To process only the requests currently in queue.
         * The events queued after the recv will be taken in next turn.
         * */
        if (u4NoOfQueuedMsgs == 0)
        {
            OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_PACKET_ARRIVAL_EVENT);
            break;
        }
    }
}

#endif
