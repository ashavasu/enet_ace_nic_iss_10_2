
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iptunnel.c,v 1.2 2013/07/04 13:11:58 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/*
 *----------------------------------------------------------------------------- 
 *    FILE NAME                    :    iptunnel.c
 *    PRINCIPAL AUTHOR             :    Aricent Inc.
 *
 *    SUBSYSTEM NAME               :    IP
 *
 *    MODULE NAME                  :    FSIP Submodule
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains routines for
 *                                      transmitting IPv4 datagrams through
 *                                      tunnels.
 *
 *----------------------------------------------------------------------------- 
 */
#include "ipinc.h"
#ifdef TUNNEL_WANTED

/******************************************************************************
 * DESCRIPTION : This function is called from Lower Layer to intimate any
 *               changes in Tunnel Parameters.
 *
 * INPUTS      : u4IfIndex, u4TnlDir, u4TnlDirFlag, u4EncapOption, u4EncapLimit,
 *               u4Mask
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP_SUCCESS/IP_FAILURE
 *               
 *
 * NOTES       : 
 ******************************************************************************/

INT4
Ip4TunlIfUpdate (UINT4 u4IfIndex, UINT4 u4Mask, tIpTunlIf * pIpTnlInfo)
{
    tIpCxt             *pIpCxt = NULL;
    UINT2               u2Port = 0;

    IPFWD_DS_LOCK ();
    for (u2Port = 0; u2Port < IPIF_MAX_LOGICAL_IFACES; u2Port++)
    {
        if ((gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex ==
             u4IfIndex))
        {

            break;
        }
    }
    if (u2Port >= IPIF_MAX_LOGICAL_IFACES)
    {
        IPFWD_DS_UNLOCK ();
        return IP_FAILURE;
    }

    pIpCxt = gIpGlobalInfo.Ipif_config[u2Port].pIpCxt;
    if (pIpCxt == NULL)
    {
        IPFWD_DS_UNLOCK ();
        return IP_FAILURE;
    }

    if (gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u1_InterfaceType !=
        IP_TUNNEL_INTERFACE_TYPE)
    {
        IP_CXT_TRC_ARG1 (pIpCxt->u4ContextId, IP_MOD_TRC, CONTROL_PLANE_TRC,
                         IP_NAME,
                         "IPIF: Ip4TunlIfUpdate: Not Tunnel Interface %d\n",
                         u4IfIndex);

        IPFWD_DS_UNLOCK ();
        return IP_FAILURE;
    }

    if (u4Mask & IP_TNL_CHG_MASK_DIR)
    {
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u1TunlDir =
            pIpTnlInfo->u1TunlDir;
    }

    if (u4Mask & IP_TNL_CHG_MASK_DIR_FLAG)
    {
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u1TunlFlag =
            pIpTnlInfo->u1TunlFlag;
    }

    if (u4Mask & IP_TNL_CHG_MASK_ENCAP_OPT)
    {
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u1EncapFlag =
            pIpTnlInfo->u1EncapFlag;
    }

    if (u4Mask & IP_TNL_CHG_MASK_ENCAP_LMT)
    {
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u1TunlEncaplmt =
            pIpTnlInfo->u1TunlEncaplmt;
    }

    if (u4Mask & IP_TNL_CHG_MASK_END_POINT)
    {
        /*  Get all Tunnel params from CFA tunnel table */
        IpGetTunnelParams (u4IfIndex,
                           gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf);

        /* Set IP address as per notification */
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u4tunlSrc =
            pIpTnlInfo->u4tunlSrc;
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u4tunlDst =
            pIpTnlInfo->u4tunlDst;
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u1TunlType =
            pIpTnlInfo->u1TunlType;
    }

    if (u4Mask & IP_TNL_CHG_MASK_HOP_LIMIT)
    {
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u2HopLimit =
            pIpTnlInfo->u2HopLimit;
    }

    IPFWD_DS_UNLOCK ();
    return IP_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This routine is called to send out a IP datagram on a
 *               Tunnel interface.
 *
 * INPUTS      : pIpHdr - Ip Header Info of the packet. 
 *               pBuf   - CRUBuffer.
 *               u2Port.
 *
 * OUTPUTS     : None
 *
 * RETURNS     : IP_SUCCESS/IP_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4
Ip4TunlSend (t_IP_HEADER * pIpHdr, tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port)
{
    tTnlPktTxInfo       TnlPktInfo;
    UINT4               u4Mtu = 0;
    UINT1               u1Df = 0;

    MEMSET (&TnlPktInfo, 0, sizeof (tTnlPktTxInfo));

    TnlPktInfo.pBuf = pBuf;
    TnlPktInfo.u4AddrType = ADDR_TYPE_IPV4;
    TnlPktInfo.u2Len = (UINT2) IP_GET_BUF_LENGTH (pBuf);

    IPFWD_DS_LOCK ();
    TnlPktInfo.u4IfIndex =
        (UINT4) gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    TnlPktInfo.u4ContextId =
        gIpGlobalInfo.Ipif_config[u2Port].pIpCxt->u4ContextId;
    u4Mtu = gIpGlobalInfo.Ipif_config[u2Port].u4Mtu;
    if (gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u1TunlType ==
        IPV6_GRE_TUNNEL)
    {
        TnlPktInfo.u2Proto = CFA_ENET_IPV4;
    }
    TnlPktInfo.TnlDestAddr.Ip4TnlAddr =
        gIpGlobalInfo.Ipif_config[u2Port].pIpTunlIf->u4tunlDst;
    IPFWD_DS_UNLOCK ();

    if ((u4Mtu > IPIF_DEF_MTU) && (TnlPktInfo.u2Len < u4Mtu))
    {
        u1Df = IP_DF_BIT;
    }

    TnlPktInfo.IpHdr.u2Len = (UINT2) IP_GET_BUF_LENGTH (pBuf);
    /* In case of GRE Tunnel this length
     * value has to be recalculated. */
    TnlPktInfo.IpHdr.u2Id = pIpHdr->u2Id;
    TnlPktInfo.IpHdr.u1Df = u1Df;
    TnlPktInfo.IpHdr.u1Ttl = pIpHdr->u1Ttl;
    TnlPktInfo.IpHdr.u1Tos = pIpHdr->u1Tos;
    TnlPktInfo.IpHdr.u1Proto = pIpHdr->u1Proto;    /* In case of GRE Tunnel this
                                                 * value needs to be updated. */
    TnlPktInfo.IpHdr.u1Olen = (UINT1) IP_OLEN (pIpHdr->u1Ver_hdrlen);

    if (CfaTnlMgrTxFrame (&TnlPktInfo) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function gets the parameters associated with the given
 *               tunnel interface and updates the given Tunnel interface
 *               structure.
 *
 * INPUTS      : u4IfIndex - Index pointing to the Tunnel Interface.
 *
 * OUTPUTS     : pIpTunlIf - Updated Tunnel Interface Structure. 
 *
 * RETURNS     : IP_SUCCESS/IP_FAILURE
 *
 ******************************************************************************/
INT4
IpGetTunnelParams (UINT4 u4IfIndex, tIpTunlIf * pIpTunlIf)
{
    tTnlIfEntry         TnlIfEntry;

    if (CfaGetTnlEntryFromIfIndex (u4IfIndex, &TnlIfEntry) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }
    pIpTunlIf->u1TunlType = (UINT1) TnlIfEntry.u4EncapsMethod;
    pIpTunlIf->u1TunlFlag = TnlIfEntry.u1DirFlag;
    pIpTunlIf->u1TunlDir = TnlIfEntry.u1Direction;
    pIpTunlIf->u4tunlSrc = TnlIfEntry.LocalAddr.Ip4TnlAddr;
    pIpTunlIf->u4tunlDst = TnlIfEntry.RemoteAddr.Ip4TnlAddr;
    pIpTunlIf->u1TunlEncaplmt = (UINT1) TnlIfEntry.u4EncapLmt;
    pIpTunlIf->u1EncapFlag = TnlIfEntry.u1EncapOption;
    pIpTunlIf->u2HopLimit = TnlIfEntry.u2HopLimit;
    return IP_SUCCESS;
}
#endif /* TUNNEL_WANTED */
