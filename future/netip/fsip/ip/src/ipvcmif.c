/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipvcmif.c,v 1.13 2014/02/24 11:27:55 siva Exp $
 *
 * Description:This file contains the various interfactions     
 *             of IP module with VCM. This includes context creation
 *             and deletion in IP and various event handling from VCM
 *
 *******************************************************************/

#include "ipinc.h"
#include "iss.h"

/*********************************************************************
 *
 * Function           : IpCreateContext
 *
 * Description        : This function creates a context in IP for the specified 
 *                      context id.
 *
 * Input(s)           : u4ContextId
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 *
*********************************************************************/
INT4
IpCreateContext (UINT4 u4ContextId)
{
    tIpCxt             *pIpCxt = NULL;
    INT4                i4Var = 0;

    IPFWD_CXT_LOCK ();

    if (u4ContextId >= IP_SIZING_CONTEXT_COUNT)
    {
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }
    /* Allocate memory for context structure */
    pIpCxt = (tIpCxt *) MemAllocMemBlk (gIpGlobalInfo.Ip_mems.IpCxtPoolId);

    if (NULL == pIpCxt)
    {
        /* Memory allocation failed. */
        IP_CXT_TRC (u4ContextId, IP_MOD_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                    IP_NAME, "IP Context mem alloc failed  \r\n");
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }

    MEMSET (pIpCxt, 0, sizeof (tIpCxt));

    pIpCxt->u4ContextId = u4ContextId;
    gIpGlobalInfo.apIpCxt[u4ContextId] = pIpCxt;

    if (fs_ip_init_InCxt (pIpCxt) == IP_FAILURE)
    {
        gIpGlobalInfo.apIpCxt[u4ContextId] = NULL;
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IpCxtPoolId,
                            (UINT1 *) pIpCxt);
        IP_CXT_TRC (u4ContextId, IP_MOD_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                    IP_NAME, "fs_ip_init_InCxt failed  \r\n");
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }

    /* Initialise the protocol registration table */
    for (i4Var = 0; i4Var < IP_MAX_PROTOCOLS; i4Var++)
    {
        pIpCxt->au1RegTable[i4Var] = DEREGISTER;
    }

    if (Ip_Icmp_Cfg_Init_InCxt (pIpCxt) == IP_FAILURE)
    {
        gIpGlobalInfo.apIpCxt[u4ContextId] = NULL;
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IpCxtPoolId,
                            (UINT1 *) pIpCxt);
        IP_CXT_TRC (u4ContextId, IP_MOD_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                    IP_NAME, "Ip_Icmp_Cfg_Init_InCxt failed  \r\n");
        IPFWD_CXT_UNLOCK ();
        return IP_FAILURE;
    }

    IpCidrCfgInitInCxt (pIpCxt);

    IP_CXT_TRC (u4ContextId, IP_MOD_TRC, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                IP_NAME, "IP context successfully created \r\n");
    IPFWD_CXT_UNLOCK ();
    return IP_SUCCESS;
}

/*********************************************************************
 *
 * Function           : IpDeleteContext
 *
 * Description        : This function delets the specified context from IP. 
 *
 * Input(s)           : u4ContextId
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 *
*********************************************************************/
VOID
IpDeleteContext (UINT4 u4ContextId)
{
    tIpCxt             *pIpCxt = gIpGlobalInfo.apIpCxt[u4ContextId];
    t_IP_REASM         *pReasmEntry = NULL;
    tPmtuInfo          *pPmtuEntry = NULL;
    tPmtuInfo          *pPmtuNextEntry = NULL;
    tPmtuInfo           DummyPmtuEntry;

    IPFWD_CXT_LOCK ();
    if (NULL == pIpCxt)
    {
        IPFWD_CXT_UNLOCK ();
        return;
    }

    MEMSET (&DummyPmtuEntry, 0, sizeof (tPmtuInfo));

    if (TMO_DLL_Count (&pIpCxt->Reasm_list) != 0)
    {
        /* Scan the list and delete all the reassembly nodes. */
        pReasmEntry = (t_IP_REASM *) TMO_DLL_First (&pIpCxt->Reasm_list);
        while (NULL != pReasmEntry)
        {
            ip_free_reasm (pReasmEntry);
            pReasmEntry = (t_IP_REASM *) TMO_DLL_First (&pIpCxt->Reasm_list);
        }
    }

    DummyPmtuEntry.pIpCxt = pIpCxt;
    if (pIpCxt->u2PmtuStatusFlag == PMTU_ENABLED)
    {
        /* PMTU is enabled in the context. Delete all the static and dynamic
         * PMTU entries.
         */
        pPmtuEntry = RBTreeGetNext (gIpGlobalInfo.PmtuTblRoot, &DummyPmtuEntry,
                                    NULL);
        while (pPmtuEntry != NULL)
        {
            pPmtuNextEntry = RBTreeGetNext (gIpGlobalInfo.PmtuTblRoot,
                                            pPmtuEntry, NULL);
            if (pPmtuEntry->pIpCxt->u4ContextId != u4ContextId)
            {
                /* All entries with the specified context id have been scanned. */
                break;
            }
            RBTreeRemove (gIpGlobalInfo.PmtuTblRoot, pPmtuEntry);
            MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.PmtuPoolId,
                                (UINT1 *) pPmtuEntry);
            pPmtuEntry = pPmtuNextEntry;
        }

        gIpGlobalInfo.u1PmtuEnableCount--;
        if (gIpGlobalInfo.u1PmtuEnableCount == 0)
        {
            /* PMTU was enabled only in this context. So stop the timers.
             */
            IP_STOP_TIMER (gIpGlobalInfo.IpTimerListId, &PMTU_GARBAGE_TIMER);
            IP_STOP_TIMER (gIpGlobalInfo.IpTimerListId, &PMTU_INCR_TIMER);
        }
    }
    gIpGlobalInfo.apIpCxt[u4ContextId] = NULL;
    MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IpCxtPoolId, (UINT1 *) pIpCxt);
    IPFWD_CXT_UNLOCK ();
    return;
}

/*********************************************************************
 *
 * Function           : IpCxtChngHdlr
 *
 * Description        : This function handles the context create and delete     
 *                      indication from VCM and post event to IP task
 *
 * Input(s)           : u4ContextId
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 *
*********************************************************************/
VOID
IpCxtChngHdlr (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1CxtChngType)
{
    tIfaceQMsg         *pIfaceQMsg = NULL;
    UNUSED_PARAM (u4IfIndex);

    pIfaceQMsg =
        (tIfaceQMsg *) MemAllocMemBlk (gIpGlobalInfo.Ip_mems.IfacePoolId);

    if (NULL == pIfaceQMsg)
    {
        IP_CXT_TRC (u4ContextId, IP_MOD_TRC,
                    INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC, IP_NAME,
                    "pIfaceQMsg mem alloc failed \r\n");
        return;
    }

    pIfaceQMsg->u4ContextId = u4ContextId;

    if (u1CxtChngType == VCM_CONTEXT_CREATE)
    {
        IPFWD_PROT_LOCK ();
        IpCreateContext (u4ContextId);
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IfacePoolId,
                            (UINT1 *) pIfaceQMsg);
        IPFWD_PROT_UNLOCK ();
        return;
    }
    else
    {
        pIfaceQMsg->u1MsgType = IP_CONTEXT_DELETE;
    }

    if (OsixQueSend (gIpGlobalInfo.IpCfaIfQId, (UINT1 *) &pIfaceQMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        IP_CXT_TRC (u4ContextId, IP_MOD_TRC, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                    IP_NAME, "OsixQueSend failed \r\n");
        MemReleaseMemBlock (gIpGlobalInfo.Ip_mems.IfacePoolId,
                            (UINT1 *) pIfaceQMsg);
        return;
    }

    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_VCM_CONTEXT_EVENT);
    return;
}

/*********************************************************************
 *
 * Function           : IpHandleIfaceMapping
 *
 * Description        : This function handles IP interface to context 
 *                      mapping. Interface mapping will be treated as 
 *                      interface unmapping from default context and mapping
 *                      to the specified context. Interface unmapping will
 *                      be treated as interface deletion and mapping as interace
 *                      creation in IP. This function takes care of indication
 *                      all HL protocols about the interface deletion ( unmapping
 *                      from default context). Interface creation indication 
 *                      is normally not given to protocols.
 *                      
 * Input(s)           : u4NewCxtId - context id to which the interface is newly
 *                      getting mapped.
 *                      u2Port - the interface ip port number 
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 *
*********************************************************************/
INT4
IpHandleIfaceMapping (UINT4 u4NewCxtId, UINT2 u2Port)
{

    tIpCxt             *pIpCxt = NULL;
    tIpCxt             *pNewIpCxt = NULL;

    if (u2Port >= IPIF_MAX_LOGICAL_IFACES)
    {
        return IP_FAILURE;
    }

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (NULL == pIpCxt)
    {
        return IP_FAILURE;
    }

    pNewIpCxt = UtilIpGetCxt (u4NewCxtId);
    if (NULL == pNewIpCxt)
    {
        return IP_FAILURE;
    }

    if ((pIpCxt->u4ContextId != IP_DEFAULT_CONTEXT) &&
        (u4NewCxtId != IP_DEFAULT_CONTEXT))
    {
        return IP_FAILURE;
    }

    /* Delete static routes associated with this interface  */
    RtmActOnStaticRoutesForIfDeleteInCxt (pIpCxt->u4ContextId, (UINT4) u2Port);

    IpIfStateChngNotify (u2Port, IFACE_DELETED);
    if (pIpCxt->u4ContextId == IP_DEFAULT_CONTEXT)
    {
        IrdpDeleteInterface (u2Port);
    }
    IpIfStChgNotify (u2Port, IFACE_DELETED);

    IPIF_SET_IFACE_STATUS (u2Port, UNNUMBERED);
    gIpGlobalInfo.Ipif_config[u2Port].pIpCxt = pNewIpCxt;
    gIpGlobalInfo.Ipif_glbtab[u2Port].pIpCxt = pNewIpCxt;

    IP_CFG_SET_IF_TBL_CHANGE_TIME ();
    return IP_SUCCESS;
}
