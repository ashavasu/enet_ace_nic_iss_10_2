/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipprcfrg.c,v 1.17 2014/05/28 12:19:01 siva Exp $
 *
 * Description:Consists of  fragment reassembly functions   
 *             Not needed when running as a standalone      
 *             gateway.                                     
 *
 *******************************************************************/
#include "ipinc.h"

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/
INT4 ip_construct_hdr_for_frags PROTO ((t_IP * pIp_org, t_IP * pIp_frag));
static t_IP_REASM  *ip_lookup_reasm_InCxt PROTO ((tIpCxt * pIpCxt, t_IP * pIp));
static t_IP_REASM  *ip_creat_reasm_InCxt PROTO ((tIpCxt * pIpCxt, t_IP * pIp));
static t_IP_FRAG   *ip_new_frag PROTO ((UINT2 u2Start_offset,
                                        UINT2 u2End_offset,
                                        tIP_BUF_CHAIN_HEADER * pBuf));
static VOID ip_free_frag PROTO ((t_IP_FRAG * pFrag));

/**************************************************************************/

     /**** $$TRACE_MODULE_NAME     = IP_FWD ****/
     /**** $$TRACE_SUB_MODULE_NAME = FRAG   ****/

/**** $$TRACE_PROCEDURE_NAME  =  ipfrag_init ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
/*-------------------------------------------------------------------+
 * Function           : ipfrag_init_InCxt
 *
 * Input(s)           : pIpCxt - IP context pointer
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action :
 *
 * Initialize the ip reassembly list in the specified ip context.            
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ipfrag_init_InCxt (tIpCxt * pIpCxt)
#else

INT4
ipfrag_init_InCxt (pIpCxt)
     tIpCxt             *pIpCxt;
#endif
{
    IP_DLL_Init ((tIP_DLL *) & (pIpCxt->Reasm_list));
    return IP_SUCCESS;
}

/**** $$TRACE_PROCEDURE_NAME  = ipReassemble  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/

/* -------------------------------------------------------------------+
 * Function           : ipReassemble
 *
 * Input(s)           : pIp, pBuf, u2Port
 *
 * Output(s)          : pIp->u2Len if complete datagram.
 *
 * Returns            : NULL - If buffer is fragment
 *            Buffer pointer - pointer to complete packet.
 *
 * Action :
 * Processes IP datagram fragments in the context to which u2Port belongs.
 * If incoming datagram is complete, returns the same.
 * else if it is a fragment and completes a datagram, returns pointer
 *      completed datagram
 * else put the fragment in the context's reassembly list in the 
 * proper place for reassembly and returns NULL.
+-------------------------------------------------------------------*/
#ifdef __STDC__

tIP_BUF_CHAIN_HEADER *
ipReassemble (t_IP * pIp, tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port)
#else
tIP_BUF_CHAIN_HEADER *
ipReassemble (pIp, pBuf, u2Port)
     t_IP               *pIp;    /* IP header, host byte order          */
     tIP_BUF_CHAIN_HEADER *pBuf;    /* The fragment itself       */
     UINT2               u2Port;    /* Interface on which it was received  */
#endif
{
    t_IP_REASM         *pReasm = NULL;
    t_IP_FRAG          *pLast_frag = NULL, *pNext_frag = NULL, *pTmp_frag =
        NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4BufLen = 0;
    UINT4               u4CxtId = 0;
    UINT2               u2TmpOffset = 0;
    UINT2               u2Start_offset = 0;    /* Index of first byte in fragment     */
    UINT2               u2End_offset = 0;    /* Index of first byte beyond fragment */
    INT1                i1More_flag = 0;    /* 1 if not last fragment, 0 otherwise */

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (pIpCxt == NULL)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return NULL;
    }

    u4CxtId = pIpCxt->u4ContextId;

    /* Convert to bytes */
    u2Start_offset = (UINT2) IP_OFF_IN_BYTES (pIp->u2Fl_offs);
    u2End_offset = (UINT2) (u2Start_offset + pIp->u2Len -
                            (IP_HDR_LEN + pIp->u2Olen));
    i1More_flag = (pIp->u2Fl_offs & IP_MF) ? TRUE : FALSE;

    if ((u2Start_offset == 0) && (i1More_flag == FALSE))    /* Complete pkt received */
    {
        /* Already a Complete Packet */
        return pBuf;
    }
    u4BufLen = IP_GET_BUF_LENGTH (pBuf);
    if (u4BufLen > pIp->u2Len)
    {
        pBuf = CRU_BUF_Delete_BufChainAtEnd (pBuf, (u4BufLen - pIp->u2Len));
        if (pBuf == NULL)
        {
            return NULL;
        }
    }
    IP4SYS_INC_REASM_REQDS (u4CxtId);
    IP4IF_INC_REASM_REQDS (u2Port);

    if (u2End_offset > IPIF_GLBTAB_REASM_MAX_SIZE (u2Port))
    {
        /* This is exceeding the maximum reassembly limit
         * If reassembly as up for this datagram let it timeout
         * on its own.  Else it is some unwanted pkt belonging to
         * old discarded reassembly.
         */
        IP4SYS_INC_REASM_FAILS (u4CxtId);
        IP4IF_INC_REASM_FAILS (u2Port);

        IP_RELEASE_BUF (pBuf, FALSE);
        /* Fragment size is high */
        return NULL;
    }

    /* Remove IP header portion from all fragments except first */
    if (u2Start_offset != 0)
    {
        IP_BUF_MOVE_VALID_OFFSET (pBuf, (UINT4) (IP_HDR_LEN + pIp->u2Olen));
    }

    if ((pReasm = ip_lookup_reasm_InCxt (pIpCxt, pIp)) == NULL)
    {
        /* First fragment; create new reassembly descriptor */
        if ((pReasm = ip_creat_reasm_InCxt (pIpCxt, pIp)) == NULL)
        {
            /* No space for descriptor, drop fragment */
            IP_RELEASE_BUF (pBuf, FALSE);
        /**** No space for descriptor */

            IP4SYS_INC_REASM_FAILS (u4CxtId);
            IP4IF_INC_REASM_FAILS (u2Port);

            return NULL;
        }
        /* Adding fragment timer IP_CFG_GET_REASM_TIME() */
        IP_START_TIMER (gIpGlobalInfo.IpTimerListId,
                        &(pReasm->Timer.Timer_node),
                        IP_CFG_GET_REASM_TIME (pIpCxt));

    }
    else
    {
        /* Keep restarting timer as long as we keep getting fragments */

        if ((pReasm->u2Len != 0) && (u2Start_offset > pReasm->u2Len))
        {
            /*
             *  IP has received a segment whose Offset is more than the size of
             *  the datagram. Hence discard the datagram silently.
             */
            IP_RELEASE_BUF (pBuf, FALSE);
            return NULL;
        }
        TmrStopTimer (gIpGlobalInfo.IpTimerListId, &pReasm->Timer.Timer_node);
        IP_START_TIMER (gIpGlobalInfo.IpTimerListId, &pReasm->Timer.Timer_node,
                        IP_CFG_GET_REASM_TIME (pIpCxt));
    }

    /*
     * If this is the last fragment, we now know how long the
     * entire datagram is; record it
     */

    if (i1More_flag == FALSE)
    {
        pReasm->u2Len = u2End_offset;
    }

    /*
     * Set pNext_frag to the first fragment which begins after us,
     * and pLast_frag to the last fragment which begins before us.
     */
    pLast_frag = NULL;

    IP_DLL_Scan (&(pReasm->Frag_list), pNext_frag, t_IP_FRAG *)
    {
        if (pNext_frag->u2Start_offset > u2Start_offset)
        {
            /* Current fragment should be placed in first in the list */
            break;                /* Next frag offset greater than ours */
        }
        else if ((pNext_frag->u2Start_offset == u2Start_offset) &&
                 (u2Start_offset == 0))
        {

            UINT2               u2Tmp_olen = 0;

       /** Two fragment Having Offset 0 (Case if the first fragment having
           complete Option Part
           This case Occurs when MTU is 64 and Ip header has maximum Option

          In this case the fragment having Option len large will be
          Considered as first fragment **/

            IP_PKT_GET_HLEN (pNext_frag->pBuf, u2Tmp_olen);
            u2Tmp_olen = (UINT2) (IP_OLEN (u2Tmp_olen));
            if (u2Tmp_olen >= pIp->u2Olen)
            {
                IP_BUF_MOVE_VALID_OFFSET (pBuf,
                                          (UINT4) (IP_HDR_LEN + pIp->u2Olen));
            }
            else
            {
                IP_BUF_MOVE_VALID_OFFSET (pNext_frag->pBuf,
                                          (UINT4) (IP_HDR_LEN + u2Tmp_olen));
            }
        }
        pLast_frag = pNext_frag;
        /* Previous Frag Found */
    }

    /* Check for overlap with preceeding fragment */
    if ((pLast_frag != NULL) && (u2Start_offset < pLast_frag->u2End_offset))
    {
        /* Strip overlap from new fragment */

        u2TmpOffset = (UINT2) (pLast_frag->u2End_offset - u2Start_offset);

        if (IP_GET_BUF_LENGTH (pBuf) <= u2TmpOffset)
        {
            IP_RELEASE_BUF (pBuf, FALSE);
            /* Duplicate packet, deleting it */
            return NULL;        /* Nothing left */
        }

        /* Deleting bytes at start of cur fragment */

        IP_BUF_MOVE_VALID_OFFSET (pBuf, u2TmpOffset);

        u2Start_offset = (UINT2) (u2Start_offset + u2TmpOffset);
    }

    /* Look for overlap with succeeding segments */
    for (; pNext_frag != NULL; pNext_frag = pTmp_frag)
    {
        /* Save in case we delete fp */

        pTmp_frag = (t_IP_FRAG *) IP_DLL_Next (&pReasm->Frag_list,
                                               &pNext_frag->Link);

        if (pNext_frag->u2Start_offset >= u2End_offset)
        {
            break;                /* Past our end */
        }

        /*
         * Trim the front of this entry; if nothing is
         * left, remove it.
         */
        u2TmpOffset = (UINT2) (u2End_offset - pNext_frag->u2Start_offset);

        IP_BUF_MOVE_VALID_OFFSET (pNext_frag->pBuf, u2TmpOffset);

        if (IP_GET_BUF_LENGTH (pNext_frag->pBuf) == 0)
        {
            /* superseded; delete from list */
            IP_DLL_Delete (&pReasm->Frag_list, &pNext_frag->Link);
            ip_free_frag (pNext_frag);
        }
        else
        {
            pNext_frag->u2Start_offset = u2End_offset;
            break;
        }
    }
    /*
     * Lastfrag now points, as before, to the fragment before us;
     * pNext_frag points at the next fragment. Check to see if we can
     * join to either or both fragments.
     */
    u2TmpOffset = IP_FRAG_INSERT;

    if ((pLast_frag != NULL) && (pLast_frag->u2End_offset == u2Start_offset))
    {
        u2TmpOffset |= IP_FRAG_APPEND;
    }

    if ((pNext_frag != NULL) && (pNext_frag->u2Start_offset == u2End_offset))
    {
        u2TmpOffset |= IP_FRAG_PREPEND;
    }

    switch (u2TmpOffset)
    {
        case IP_FRAG_INSERT:
            /* Insert new desc between pLast_frag and pNext_frag */

            if (IP_DLL_Count (&pReasm->Frag_list) == IP_DEF_MAX_FRAGS)
            {
                /*
                 * NOTE: At this position, there is no point in continuing with
                 *       reassembly since u have already dropped a fragment.
                 *       But we can't delete it bcos if we do it a new reassembly
                 *       will be started for the fragments following this.
                 */
                IP_RELEASE_BUF (pBuf, FALSE);
                return NULL;
            }

            pTmp_frag = ip_new_frag (u2Start_offset, u2End_offset, pBuf);
            if (pTmp_frag == NULL)
            {
                return NULL;
            }
            if (pLast_frag != NULL)
            {
                IP_DLL_Insert (&pReasm->Frag_list, &pLast_frag->Link,
                               &pTmp_frag->Link);
            }
            else
            {
                IP_DLL_Add (&pReasm->Frag_list, &pTmp_frag->Link);
            }
            break;

        case IP_FRAG_APPEND:
            /* Append to pLast_frag */
            IP_CONCAT_BUFS (pLast_frag->pBuf, pBuf);
            pLast_frag->u2End_offset = u2End_offset;    /* Extend forward */
            break;

        case IP_FRAG_PREPEND:
            /* Prepend to pNext_frag */
            IP_CONCAT_BUFS (pBuf, pNext_frag->pBuf);
            pNext_frag->pBuf = pBuf;
            pNext_frag->u2Start_offset = u2Start_offset;    /* Extend backward */
            break;

        case (IP_FRAG_APPEND | IP_FRAG_PREPEND):
            /* Consolidate by appending this fragment and pNext_frag
             * to pLast_frag and removing the pNext_frag descriptor
             */
            IP_CONCAT_BUFS (pLast_frag->pBuf, pBuf);
            IP_CONCAT_BUFS (pLast_frag->pBuf, pNext_frag->pBuf);
            pNext_frag->pBuf = NULL;
            pLast_frag->u2End_offset = pNext_frag->u2End_offset;

            /* Finally unlink and delete the now unneeded pNext_frag */
            IP_DLL_Delete (&(pReasm->Frag_list), &pNext_frag->Link);
            ip_free_frag (pNext_frag);
            break;
    }
    pTmp_frag = (t_IP_FRAG *) IP_DLL_First (&(pReasm->Frag_list));

    if ((pTmp_frag != NULL) && (pTmp_frag->u2Start_offset == 0) &&
        (pReasm->u2Len != 0) && (pTmp_frag->u2End_offset == pReasm->u2Len))
    {
        /*
         * We've got a complete datagram, so extract it from the
         * reassembly buffer and pass it on.
         */
        pBuf = pTmp_frag->pBuf;
        pTmp_frag->pBuf = NULL;

        /* Tell IP the entire length */
        pIp->u2Len = (UINT2) (pReasm->u2Len + (IP_HDR_LEN + pIp->u2Olen));

        pIp->u2Cksum =
            IpCalcCheckSum (pBuf, (UINT4) (IP_HDR_LEN + pIp->u2Olen), 0);
        ip_put_hdr (pBuf, pIp, TRUE);

        ip_free_reasm (pReasm);

        IP4SYS_INC_REASM_OKS (u4CxtId);
        IP4IF_INC_REASM_OKS (u2Port);

        /* Frag Reasm Ok */
        return pBuf;
    }
    else
    {
        /* Frag Processing Done, still more frags */
        return NULL;
    }
}

/*
 * Search reassembly list.
 */

/**** $$TRACE_PROCEDURE_NAME  = ip_lookup_reasm  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/

/*-------------------------------------------------------------------+
 *
 * Function           : ip_lookup_reasm_InCxt
 *
 * Input(s)           : pIpCxt, pIp
 *
 * Output(s)          : None
 *
 * Returns            : pointer - if found
 *                      NULL    - if not found.
 *
 * Action :
 * Looks up reassembly queue of the specified context for a datagram undergoing
 * reassembly matching pIp 
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

static t_IP_REASM  *
ip_lookup_reasm_InCxt (tIpCxt * pIpCxt, t_IP * pIp)
#else
static t_IP_REASM  *
ip_lookup_reasm_InCxt (pIpCxt, pIp)
     tIpCxt             *pIpCxt;
     t_IP               *pIp;    /*  Structure to lookup */
#endif
{
    t_IP_REASM         *pReasm = NULL;

    IP_DLL_Scan (&pIpCxt->Reasm_list, pReasm, t_IP_REASM *)
    {
        if ((pIp->u4Src == pReasm->u4Src) && (pIp->u4Dest == pReasm->u4Dest)
            && (pIp->u1Proto == pReasm->u1Proto) && (pIp->u2Id == pReasm->u2Id))
        {
            return pReasm;
        }
    }
    return NULL;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_creat_reasm_InCxt
 *
 * Input(s)           : pIpCxt, pIp
 *
 * Output(s)          : None
 *
 * Returns            : pointer to reassembly structure if IP_SUCCESS;
 *                      NULL if failure
 * Action :
 * Creates a reassembly descriptor and puts at head of reassembly list
 * of the specified context. The procedure also initializes the fragment list 
 * for this reassembly.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

static t_IP_REASM  *
ip_creat_reasm_InCxt (tIpCxt * pIpCxt, t_IP * pIp)
#else
static t_IP_REASM  *
ip_creat_reasm_InCxt (pIpCxt, pIp)
     tIpCxt             *pIpCxt;
     t_IP               *pIp;    /*  Source, dest, id, proto information */
#endif
{
    t_IP_REASM         *pReasm = NULL;
    UINT2               u2Port = 0;

    if (IP_DLL_Count (&(pIpCxt->Reasm_list)) == IP_DEF_MAX_REASM_LISTS)
    {
        /*
         * NOTE: An exception or a Count of No. of Datagrams dropped due to
         *       too many Datagrams arrived for reassembly can be had here.
         */

        /*
         * Reassembly wasn't taken up at all. Hence should be considered as
         * reassembly failure due to memory shortage.
         */
        IP4SYS_INC_REASM_FAILS (pIpCxt->u4ContextId);
        /* Find the corresponding interface index */
        if ((u2Port = (UINT2) ipif_get_handle_from_addr_InCxt
             (pIpCxt->u4ContextId, pIp->u4Dest)) != (UINT2) IP_FAILURE)
        {
            IP4IF_INC_REASM_FAILS (u2Port);
        }

        return NULL;
    }

    if ((pReasm = (t_IP_REASM *) IP_ALLOCATE_MEM_BLOCK
         (gIpGlobalInfo.Ip_mems.ReasmPoolId)) != NULL)
    {
        pReasm->pIpCxt = pIpCxt;
        pReasm->u4Src = pIp->u4Src;
        pReasm->u4Dest = pIp->u4Dest;
        pReasm->u2Id = pIp->u2Id;
        pReasm->u1Proto = pIp->u1Proto;
        pReasm->u2Len = 0;

        pReasm->Timer.u1Id = IP_REASM_TIMER_ID;
        pReasm->Timer.pArg = (VOID *) pReasm;

        IP_DLL_Add (&(pIpCxt->Reasm_list), &(pReasm->Link));

        /* Initialize the fragment list for this reassembly process */
        IP_DLL_Init (&(pReasm->Frag_list));

        return pReasm;
    }
    else
    {
        IP4SYS_INC_REASM_FAILS (pIpCxt->u4ContextId);
        /* Find the corresponding interface index */
        if ((u2Port = (UINT2) ipif_get_handle_from_addr_InCxt
             (pIpCxt->u4ContextId, pIp->u4Dest)) != (UINT2) IP_FAILURE)
        {
            IP4IF_INC_REASM_FAILS (u2Port);
        }

        return NULL;
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_free_reasm
 *
 * Input(s)           : pReasm
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action :
 * Free all resources associated with a reassembly descriptor.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
VOID
ip_free_reasm (t_IP_REASM * pReasm)
#else
VOID
ip_free_reasm (pReasm)
     t_IP_REASM         *pReasm;    /* CR : Fragment structure to free */
#endif
{
    t_IP_FRAG          *pFrag = NULL;
    tIpCxt             *pIpCxt = pReasm->pIpCxt;

    IP_STOP_TIMER (gIpGlobalInfo.IpTimerListId, &pReasm->Timer.Timer_node);

    /* Remove from list of reassembly descriptors */
    IP_DLL_Delete (&(pIpCxt->Reasm_list), &pReasm->Link);

    /* Free any fragments on list, starting at beginning */
    while ((pFrag = (t_IP_FRAG *) IP_DLL_First (&pReasm->Frag_list)))
    {
        if (pFrag->pBuf != NULL)
        {
            IP_RELEASE_BUF (pFrag->pBuf, FALSE);
        }

        /* Should have deleted from reassembly list before freeing it. */
        IP_DLL_Delete (&pReasm->Frag_list, &pFrag->Link);
        IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.FragPoolId, pFrag);
    }

    IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.ReasmPoolId, pReasm);
}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_new_frag
 *
 * Input(s)           : u2Start_offset, u2End_offset, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : pointer to new fragment if IP_SUCCESS;
 *                      NULL if failure
 *
 * Action :
 * Create a fragment.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
static t_IP_FRAG   *
ip_new_frag (UINT2 u2Start_offset, UINT2 u2End_offset,
             tIP_BUF_CHAIN_HEADER * pBuf)
#else
static t_IP_FRAG   *
ip_new_frag (u2Start_offset, u2End_offset, pBuf)
     UINT2               u2Start_offset;    /* Offset of this fragment */
     UINT2               u2End_offset;    /* Last frag offset        */
     tIP_BUF_CHAIN_HEADER *pBuf;    /* Data                */
#endif
{
    t_IP_FRAG          *pFrag = NULL;

    if ((pFrag = (t_IP_FRAG *)
         IP_ALLOCATE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.FragPoolId)) == NULL)
    {
        /* Drop fragment */
        IP_RELEASE_BUF (pBuf, FALSE);
        return NULL;
    }
    pFrag->pBuf = pBuf;
    pFrag->u2Start_offset = u2Start_offset;
    pFrag->u2End_offset = u2End_offset;

    return pFrag;
}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_free_frag
 *
 * Input(s)           : pFrag
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action :
 * Delete a fragment, return next one on queue.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

static VOID
ip_free_frag (t_IP_FRAG * pFrag)
#else
static VOID
ip_free_frag (pFrag)
     t_IP_FRAG          *pFrag;    /* CR : Fragment to free */
#endif
{
    if (pFrag->pBuf != NULL)
    {
        IP_RELEASE_BUF (pFrag->pBuf, FALSE);
    }
    IP_RELEASE_MEM_BLOCK (gIpGlobalInfo.Ip_mems.FragPoolId, pFrag);

}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_fragment_and_send
 *
 * Input(s)           : u2Port, pIp, pBuf, u4Gw, u1Flag ,u4MTU
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 * This procedure fragments the given message to send over an interface.
 * It copies appropriate flags into fragments and calls interface send
 * routine to send it over the interface.
 *
+-------------------------------------------------------------------*/
/**** $$TRACE_PROCEDURE_NAME  = ip_fragment_and_send  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD  ****/
#ifdef __STDC__

INT4
ip_fragment_and_send (UINT2 u2Port, t_IP * pIp,
                      tIP_BUF_CHAIN_HEADER * pBuf, UINT4 u4Gw, UINT1 u1Flag,
                      UINT4 u4MTU)
#else
INT4
ip_fragment_and_send (u2Port, pIp, pBuf, u4Gw, u1Flag, u4MTU)
     UINT2               u2Port;    /*  Interface to send it        */
     t_IP               *pIp;    /* IP header of this msg       */
     tIP_BUF_CHAIN_HEADER *pBuf;    /*  Message buffer to send      */
     UINT4               u4Gw;    /*  Next Hop to send            */
     UINT1               u1Flag;    /*  Broadcast or Multicast flag */
#endif
{
    t_IP                Ip_hdr_for_frag;
    tIP_BUF_CHAIN_HEADER *pTmp_buf = NULL;
    tIP_BUF_CHAIN_HEADER *pFrag_data = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4IfaceIndex = 0;
    INT2                i2Org_mf = 0;    /* Temp to store original more flag */
    INT2                i2Ip_hlen = 0;
    INT2                i2Org_df = 0;
    UINT2               u2Offset = 0;
    UINT2               u2Frag_data_size = 0;    /* Size of this fragment's data     */
    UINT2               u2Len = 0;
    UINT2               u2OrgLen = 0;    /* added for trace and debug */
    INT1                i1First_frag = TRUE, i1Last_frag = FALSE;

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();
    u4IfaceIndex = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    if (pIpCxt == NULL)
    {
        return IP_FAILURE;
    }

    pIpCxt->Ip_stats.u4Out_frag_pkts++;
    IPIF_INC_OUT_FRAG_PKTS (u2Port);

    u2OrgLen = pIp->u2Len;
    i2Ip_hlen = (INT2) IP_TOTAL_HDR_LEN (pIp);
    u2Len = (UINT2) (pIp->u2Len - (UINT2) i2Ip_hlen);    /* Data length only */
    u2Offset = (UINT2) (IP_OFF_IN_BYTES (pIp->u2Fl_offs));
    i2Org_df = (INT2) pIp->u2Fl_offs;
    i2Org_mf = pIp->u2Fl_offs & IP_MF;    /* Save original MF flag */

    while (u2Len != 0)            /* As long as there's data left */
    {
        pIp->u2Fl_offs = (UINT2) (IP_OFF_IN_HDR_FORMAT (u2Offset));

        /* Makefile Changes - comparison btwn signed and unsigned */
        if ((UINT2) (u2Len + i2Ip_hlen) <= u4MTU)
        {
            /* Last fragment; send all that remains */
            u2Frag_data_size = u2Len;
            pIp->u2Fl_offs |= (UINT2) i2Org_mf;    /* Pass original MF flag */
            pIp->u2Fl_offs |= (UINT2) i2Org_df;
            i1Last_frag = TRUE;
        }
        else
        {
            /* More to come, so send multpIple of 8 bytes */
            u2Frag_data_size = (UINT2) (((INT2) u4MTU - (i2Ip_hlen)) & 0xfff8);
            pIp->u2Fl_offs |= IP_MF;
            pIp->u2Fl_offs |= (UINT2) i2Org_df;
        }

        pIp->u2Len = (UINT2) (u2Frag_data_size + (UINT2) i2Ip_hlen);

        /* Put IP header back on */
        if (i1First_frag == FALSE)
        {
            pTmp_buf = IP_ALLOCATE_BUF ((UINT4) (IP_MAC_HDR_LEN + pIp->u2Olen),
                                        IP_MAC_HDR_LEN - IP_HDR_LEN);
            if (pTmp_buf == NULL)
            {
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            ip_put_hdr (pTmp_buf, pIp, TRUE);
            /*
             * Extract data from original buffer and link it with our
             * header.
             */
            pFrag_data = pBuf;

            if (i1Last_frag == FALSE)
            {
                if (IP_FRAGMENT_BUF (pFrag_data, u2Frag_data_size, &pBuf) !=
                    CRU_SUCCESS)
                {
                    IP_RELEASE_BUF (pFrag_data, FALSE);
                    IP_RELEASE_BUF (pTmp_buf, FALSE);
                    return (IP_FAILURE);
                }
            }
            else
            {
                pBuf = NULL;
            }
            if (pFrag_data != NULL)
            {
                IP_CONCAT_BUFS (pTmp_buf, pFrag_data);
            }
        }
        else
        {
            /*
             * Extract IP header  and data from front end and send it.
             */
            pTmp_buf = pBuf;
            if (IP_FRAGMENT_BUF
                (pTmp_buf, (UINT4) (u2Frag_data_size + i2Ip_hlen),
                 &pBuf) != CRU_SUCCESS)
            {
                IP_RELEASE_BUF (pTmp_buf, FALSE);
                return (IP_FAILURE);
            }
            ip_put_hdr (pTmp_buf, pIp, TRUE);
        }

        /* Now the datagram to send is in pTmp_buf; Add frag specific info */
        IP_PKT_ASSIGN_LEN (pTmp_buf, pIp->u2Len);
        IP_PKT_ASSIGN_FLAGS (pTmp_buf, pIp->u2Fl_offs);

        pIpCxt->Ip_stats.u4Out_pkts++;

        IP4SYS_INC_OUT_FRAG_CREATES (pIpCxt->u4ContextId);
        IP4IF_INC_OUT_FRAG_CREATES (u2Port);

        IP_CXT_TRC_ARG5 (pIpCxt->u4ContextId, IP_MOD_TRC, DATA_PATH_TRC,
                         IP_NAME,
                         "s = %x, d = %x, frag %d / %d sending on interface %d.\n",
                         pIp->u4Src, pIp->u4Dest, pIp->u2Len, u2OrgLen,
                         u4IfaceIndex);

        if (IpTxDatagram (pTmp_buf, u2Port, u4Gw, u1Flag, pIp->u2Len) ==
            IP_FAILURE)
        {
            /* Free all our extra data */
            if (pTmp_buf != pBuf)
            {
                IP_RELEASE_BUF (pTmp_buf, FALSE);
            }
            if (pBuf != NULL)
            {
                IP_RELEASE_BUF (pBuf, FALSE);
            }
            return IP_FAILURE;
        }

        u2Offset = (UINT2) (u2Offset + u2Frag_data_size);    /* Each time offset gets updated */
        u2Len = (UINT2) (u2Len - u2Frag_data_size);

        if (i1First_frag == TRUE)
        {
            i1First_frag = FALSE;
            /* If there are no options dont waste time in copying */
            if (pIp->u2Olen != 0)
            {
                /*
                 * After the first fragment, should remove those
                 * options that aren't supposed to be copied on fragmentation
                 */
                if (ip_construct_hdr_for_frags (pIp, &Ip_hdr_for_frag)
                    == IP_FAILURE)
                {
                    /* Free all our extra data */
                    if (pTmp_buf != pBuf)
                    {
                        IP_RELEASE_BUF (pTmp_buf, FALSE);
                    }
                    if (pBuf != NULL)
                    {
                        IP_RELEASE_BUF (pBuf, FALSE);
                    }
                    return IP_FAILURE;
                }
                pIp = &(Ip_hdr_for_frag);
                i2Ip_hlen = (INT2) IP_TOTAL_HDR_LEN (pIp);
            }
        }                        /* first frag */
    }                            /* while length */
    IP4SYS_INC_OUT_FRAG_OKS (pIpCxt->u4ContextId);
    IP4IF_INC_OUT_FRAG_OKS (u2Port);
    return IP_SUCCESS;
}

/**** $$TRACE_PROCEDURE_NAME  = ip_construct_hdr_for_frags  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/

/*-------------------------------------------------------------------+
 *
 * Function           : ip_construct_hdr_for_frags
 *
 * Input(s)           : pIp_org, pIp_frag
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 * This procedure takes in two t_IP header type structures.
 * It constructs the IP header for the fragments depending on options.
 * finally it updates i1Olen field in fragment IP header structure.
 * This is called from ip_fragment_and_send procedure.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ip_construct_hdr_for_frags (t_IP * pIp_org, t_IP * pIp_frag)
#else

INT4
ip_construct_hdr_for_frags (pIp_org, pIp_frag)
     t_IP               *pIp_org;
     t_IP               *pIp_frag;
#endif
{
    UINT2               u2opt = 0;
    UINT2               u2Cur_olen = 0;
    UINT2               u2OptLen = 0;
    UINT2               u2PadLen = 0;

    /**** $$TRACE_LOG (ENTRY, "Org %x To %x\n", pIp_org, pIp_frag);****/
    MEMCPY (pIp_frag, pIp_org, sizeof (t_IP));
    MEMSET (pIp_frag->au1Options, 0, sizeof (pIp_frag->au1Options));

    /**** $$TRACE_LOG (INTMD, "Original Length %d but u2opt %d\n",
                       pIp_org->u2Olen, u2opt);****/
    while ((u2opt < (IP_MAX_BYTES ((pIp_org->u2Olen), IP_MAX_OPT_LEN))) &&
           IP_OPT_NUMBER (pIp_org->au1Options[u2opt]) != IP_OPT_EOL)
    {
        if (u2opt == (IP_MAX_OPT_LEN - 1))
        {
            /* Max option length can be 40 which means pIpau1Options[39] should 
             * be IP_OPT_EOL. Here it is not IP_OPT_EOL. So error
             */
            return IP_FAILURE;
        }

        u2Cur_olen =
            (UINT2) ((pIp_org->au1Options[u2opt] ==
                      IP_OPT_NOP) ? 1 : pIp_org->au1Options[u2opt + 1]);
       /**** $$TRACE_LOG (INTMD, "Len %d starts at %d char %d\n",
                     u2Cur_olen, u2opt, pIp_org->au1Options[u2opt]);****/
        if (IP_COPY_OPTION_TO_FRAG (pIp_org->au1Options[u2opt]) != FALSE)
        {
            if ((u2Cur_olen >= IP_MAX_OPT_LEN) || (u2OptLen >= IP_MAX_OPT_LEN))
            {
                return IP_FAILURE;
            }
            MEMCPY (&(pIp_frag->au1Options[u2OptLen]),
                    &(pIp_org->au1Options[u2opt]), u2Cur_olen);
            u2OptLen = (UINT2) (u2OptLen + u2Cur_olen);
        }
        u2opt = (UINT2) (u2opt + u2Cur_olen);
    }
    if (u2OptLen >= IP_MAX_OPT_LEN)
    {
        return IP_FAILURE;
    }

    /* pad the option for word alignment of IP header */
    u2PadLen = (UINT2) (IP_FOUR - (u2OptLen % IP_FOUR));
    switch (u2PadLen)
    {
        case 1:                /* EOL option */
            pIp_frag->au1Options[u2OptLen++] = IP_OPT_EOL;
            break;
        case 2:
            pIp_frag->au1Options[u2OptLen++] = IP_OPT_NOP;
            if (u2OptLen > IP_MAX_OPT_LEN - 1)
                return IP_FAILURE;
            pIp_frag->au1Options[u2OptLen++] = IP_OPT_EOL;
            break;
        case 3:
            pIp_frag->au1Options[u2OptLen++] = IP_OPT_NOP;
            if (u2OptLen > IP_MAX_OPT_LEN - 1)
                return IP_FAILURE;
            pIp_frag->au1Options[u2OptLen++] = IP_OPT_NOP;
            if (u2OptLen > IP_MAX_OPT_LEN - 1)
                return IP_FAILURE;
            pIp_frag->au1Options[u2OptLen++] = IP_OPT_EOL;
            break;
        default:
            break;
    }
    pIp_frag->u2Olen = u2OptLen;
    return IP_SUCCESS;
    /**** $$TRACE_LOG (EXIT, "Ok Handled\n");****/
}

/**** $$TRACE_PROCEDURE_NAME  = ip_handle_reasm_timer_expiry  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
/*-------------------------------------------------------------------+
 * Function           : ip_handle_reasm_timer_expiry
 *
 * Input(s)           : pTimer
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action :
 * This is called from IP_FWD_Handle_Timer_Expiry procedure.
 * Deletes the Reassembly list in the timer structure.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

VOID
ip_handle_reasm_timer_expiry (t_IP_TIMER * pTimer)
#else

VOID
ip_handle_reasm_timer_expiry (pTimer)
     t_IP_TIMER         *pTimer;
#endif
{
    t_IP_REASM         *pReasm = NULL;
    t_IP_FRAG          *pFrag = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT2               u2Port = 0;

    pReasm = pTimer->pArg;
    pIpCxt = pReasm->pIpCxt;

    if (NULL == pIpCxt)
    {
        return;
    }
   /**** $$TRACE_LOG (ENTRY,
            "Removing Reassembly for Src %08x, Dest %08x, IP Id %d, Proto %d\n",
             pReasm->u4Src, pReasm->u4Dest, pReasm->u2Id, pReasm->u1Proto);****/

    pFrag = (t_IP_FRAG *) IP_DLL_First (&pReasm->Frag_list);
    if ((pFrag != NULL) && (pFrag->u2Start_offset == 0))
    {
        t_ICMP              Icmp;
        /*
         * First fragment is available, now you can send ICMP error message
         */
        MEMSET (&Icmp, 0, sizeof (t_ICMP));
        Icmp.i1Type = ICMP_TIME_EXCEED;
        Icmp.i1Code = ICMP_FRAG_EXCEED;
        Icmp.args.u4Unused = 0;
        Icmp.u4ContextId = pIpCxt->u4ContextId;

        icmp_error_msg (pFrag->pBuf, &Icmp);
    }

    IP4SYS_INC_REASM_FAILS (pIpCxt->u4ContextId);
    /* Find the corresponding ip port */
    if ((u2Port = (UINT2) ipif_get_handle_from_addr_InCxt
         (pIpCxt->u4ContextId, pReasm->u4Dest)) != (UINT2) IP_FAILURE)
    {
        IP4IF_INC_REASM_FAILS (u2Port);
    }

    ip_free_reasm (pReasm);
    pIpCxt->Ip_stats.u4Reasm_timeout++;

}
