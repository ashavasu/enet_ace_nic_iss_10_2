/* $Id: fsipwr.c,v 1.8 2013/07/04 13:10:59 siva Exp $*/
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "arp.h"
#include "fssnmp.h"
#include "fsipwr.h"
#include "fsiplow.h"
#include "fsipdb.h"

VOID
RegisterFSIP ()
{
    SNMPRegisterMib (&fsipOID, &fsipEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsipOID, (const UINT1 *) "futureip");
}

INT4
FsIpInLengthErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsIpInLengthErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpInCksumErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpInCksumErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpInVersionErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpInVersionErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpInTTLErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpInTTLErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpInOptionErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpInOptionErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpInBroadCastsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpInBroadCasts (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpOutGenErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpOutGenErrors (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpOptProcEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpOptProcEnable (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpOptProcEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpOptProcEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpOptProcEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpOptProcEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpNumMultipathTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpNumMultipath (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpNumMultipathSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpNumMultipath (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpNumMultipathGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpNumMultipath (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpLoadShareEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhTestv2FsIpLoadShareEnable (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpLoadShareEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpLoadShareEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpLoadShareEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpLoadShareEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpEnablePMTUDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpEnablePMTUD (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpEnablePMTUDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpEnablePMTUD (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpEnablePMTUDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpEnablePMTUD (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuEntryAgeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpPmtuEntryAge (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuEntryAgeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpPmtuEntryAge (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuEntryAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpPmtuEntryAge (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuTableSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpPmtuTableSize (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuTableSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpPmtuTableSize (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuTableSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpPmtuTableSize (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpTraceConfigAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpTraceConfigAdminStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue);
    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhSetFsIpTraceConfigAdminStatus
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpTraceConfigAdminStatus
        (pMultiIndex->pIndex[0].u4_ULongValue, &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMaxTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpTraceConfigMaxTTL (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMaxTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsIpTraceConfigMaxTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMaxTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsIpTraceConfigMaxTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMinTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpTraceConfigMinTTL (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMinTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsIpTraceConfigMinTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMinTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsIpTraceConfigMinTTL (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpTraceConfigOperStatus
        (pMultiIndex->pIndex[0].u4_ULongValue, &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpTraceConfigTimeout (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal =
        nmhSetFsIpTraceConfigTimeout (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsIpTraceConfigTimeout (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMtuTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpTraceConfigMtu (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMtuSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhSetFsIpTraceConfigMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceConfigMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpTraceConfigMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpTraceHopCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpTraceIntermHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpTraceIntermHop (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &pMultiData->u4_ULongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceReachTime1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpTraceReachTime1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceReachTime2Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpTraceReachTime2 (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpTraceReachTime3Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpTraceTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        UDP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpTraceReachTime3 (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &pMultiData->i4_SLongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddrTabIfaceIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpAddressTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpAddrTabIfaceId (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddrTabAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpAddressTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpAddrTabAdvertiseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpAddrTabAdvertise (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddrTabAdvertiseSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpAddrTabAdvertise (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddrTabAdvertiseGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpAddressTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpAddrTabAdvertise (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddrTabPreflevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpAddrTabPreflevel (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddrTabPreflevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpAddrTabPreflevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddrTabPreflevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpAddressTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpAddrTabPreflevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddrTabStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpAddressTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpAddrTabStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstIfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpRtrLstIface (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstIfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpRtrLstIface (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstIfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRtrLstIface (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpRtrLstPreflevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpRtrLstPreflevel (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstPreflevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpRtrLstPreflevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstPreflevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRtrLstPreflevel (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstStaticGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRtrLstStatic (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpRtrLstStatus (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpRtrLstStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpRtrLstTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRtrLstStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuDestinationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpPathMtuTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpPmtuTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpPathMtuTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpPathMtuTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpPathMtu (pu4Error,
                                     pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPathMtuSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpPathMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPathMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpPathMtuTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpPathMtu (pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuDiscTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpPmtuDisc (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuDiscSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpPmtuDisc (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuDiscGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpPathMtuTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpPmtuDisc (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuEntryStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpPmtuEntryStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuEntryStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpPmtuEntryStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuEntryStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpPathMtuTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpPmtuEntryStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpRouteMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpRouteTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[2].i4_SLongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpRouteNextHopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {

        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[3].u4_ULongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpRouteProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[4].i4_SLongValue;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpRouteProtoInstanceIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRouteProtoInstanceId
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue, &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpRouteIfIndex (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetFsIpRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpRouteType (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetFsIpRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiIndex->pIndex[1].u4_ULongValue,
                                    pMultiIndex->pIndex[2].i4_SLongValue,
                                    pMultiIndex->pIndex[3].u4_ULongValue,
                                    pMultiIndex->pIndex[4].i4_SLongValue,
                                    &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRouteAge (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiIndex->pIndex[1].u4_ULongValue,
                                   pMultiIndex->pIndex[2].i4_SLongValue,
                                   pMultiIndex->pIndex[3].u4_ULongValue,
                                   pMultiIndex->pIndex[4].i4_SLongValue,
                                   &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteNextHopASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpRouteNextHopAS (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[2].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[3].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[4].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteNextHopASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetFsIpRouteNextHopAS (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteNextHopASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRouteNextHopAS (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteMetric1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRouteMetric1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRoutePreferenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpRoutePreference (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[4].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRoutePreferenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetFsIpRoutePreference (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRoutePreferenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRoutePreference (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiIndex->pIndex[4].i4_SLongValue,
                                          &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpRouteStatus (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhSetFsIpRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpCommonRoutingTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].i4_SLongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiIndex->pIndex[4].i4_SLongValue,
                                      &pMultiData->i4_SLongValue);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpifMaxReasmSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpifMaxReasmSize (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifMaxReasmSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpifMaxReasmSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifMaxReasmSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpifMaxReasmSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifIcmpRedirectEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpifIcmpRedirectEnable (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifIcmpRedirectEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpifIcmpRedirectEnable
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifIcmpRedirectEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpifIcmpRedirectEnable
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifDrtBcastFwdingEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpifDrtBcastFwdingEnable (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifDrtBcastFwdingEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpifDrtBcastFwdingEnable
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifDrtBcastFwdingEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIpifDrtBcastFwdingEnable
        (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpProxyArpSubnetOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsIpProxyArpSubnetOption (&(pMultiData->i4_SLongValue));

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpProxyArpSubnetOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhSetFsIpProxyArpSubnetOption (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpProxyArpSubnetOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhTestv2FsIpProxyArpSubnetOption (pu4Error,
                                                  pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpProxyArpSubnetOptionDep (UINT4 *pu4Error,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIpProxyArpSubnetOption (pu4Error,
                                                 pSnmpIndexList, pSnmpvarbinds);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifProxyArpAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpifProxyArpAdminStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifProxyArpAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsIpifProxyArpAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifProxyArpAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal =
        nmhGetFsIpifProxyArpAdminStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));
    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifLocalProxyArpAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsIpifLocalProxyArpAdminStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsIpifLocalProxyArpAdminStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsIpifLocalProxyArpAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsIpifLocalProxyArpAdminStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIpifTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIpifLocalProxyArpAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsIcmpSendRedirectEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIcmpSendRedirectEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendRedirectEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIcmpSendRedirectEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendRedirectEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpSendRedirectEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendUnreachableEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIcmpSendUnreachableEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendUnreachableEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIcmpSendUnreachableEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendUnreachableEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpSendUnreachableEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendEchoReplyEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIcmpSendEchoReplyEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendEchoReplyEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIcmpSendEchoReplyEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendEchoReplyEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpSendEchoReplyEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpNetMaskReplyEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIcmpNetMaskReplyEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpNetMaskReplyEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIcmpNetMaskReplyEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpNetMaskReplyEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpNetMaskReplyEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpTimeStampReplyEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIcmpTimeStampReplyEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpTimeStampReplyEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIcmpTimeStampReplyEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpTimeStampReplyEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpTimeStampReplyEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpInDomainNameRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpInDomainNameRequests (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpInDomainNameReplyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpInDomainNameReply (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpOutDomainNameRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpOutDomainNameRequests (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpOutDomainNameReplyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpOutDomainNameReply (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpDirectQueryEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIcmpDirectQueryEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpDirectQueryEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIcmpDirectQueryEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpDirectQueryEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpDirectQueryEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsDomainNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsDomainName (pu4Error, pMultiData->pOctetStrValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsDomainNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsDomainName (pMultiData->pOctetStrValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsDomainNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsDomainName (pMultiData->pOctetStrValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsTimeToLiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsTimeToLive (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsTimeToLiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsTimeToLive (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsTimeToLiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsTimeToLive (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpInSecurityFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpInSecurityFailures (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpOutSecurityFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpOutSecurityFailures (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendSecurityFailuresEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIcmpSendSecurityFailuresEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendSecurityFailuresEnableSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsIcmpSendSecurityFailuresEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendSecurityFailuresEnableGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpSendSecurityFailuresEnable
        (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpRecvSecurityFailuresEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIcmpRecvSecurityFailuresEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpRecvSecurityFailuresEnableSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhSetFsIcmpRecvSecurityFailuresEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpRecvSecurityFailuresEnableGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIcmpRecvSecurityFailuresEnable
        (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsUdpInNoCksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    UNUSED_PARAM (pMultiIndex);
    i4RetVal = nmhGetFsUdpInNoCksum (&pMultiData->u4_ULongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsUdpInIcmpErrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    UDP_PROT_LOCK ();

    i4RetVal = nmhGetFsUdpInIcmpErr (&pMultiData->u4_ULongValue);
    UDP_PROT_UNLOCK ();

    return i4RetVal;
}

INT4
FsUdpInErrCksumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    UDP_PROT_LOCK ();

    i4RetVal = nmhGetFsUdpInErrCksum (&pMultiData->u4_ULongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsUdpInBcastGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    UDP_PROT_LOCK ();

    i4RetVal = nmhGetFsUdpInBcast (&pMultiData->u4_ULongValue);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsCidrAggAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsCidrAggTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsCidrAggAddressMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsCidrAggTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsCidrAggStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsCidrAggStatus (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsCidrAggStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsCidrAggStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsCidrAggStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsCidrAggTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsCidrAggStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsCidrAdvertAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsCidrAdvertTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsCidrAdvertAddressMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsCidrAdvertTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsCidrAdvertStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsCidrAdvertStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsCidrAdvertStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsCidrAdvertStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsCidrAdvertStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsCidrAdvertTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsCidrAdvertStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpInAdvertisementsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIrdpInAdvertisements (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpInSolicitationsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIrdpInSolicitations (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpOutAdvertisementsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIrdpOutAdvertisements (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpOutSolicitationsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIrdpOutSolicitations (&pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpSendAdvertisementsEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIrdpSendAdvertisementsEnable
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpSendAdvertisementsEnableSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIrdpSendAdvertisementsEnable (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpSendAdvertisementsEnableGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhGetFsIrdpSendAdvertisementsEnable (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfIfNumGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIrdpIfConfSubrefGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIrdpIfConfAdvertisementAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIrdpIfConfAdvertisementAddress (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfAdvertisementAddressSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIrdpIfConfAdvertisementAddress
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfAdvertisementAddressGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIrdpIfConfAdvertisementAddress
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfMaxAdvertisementIntervalTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIrdpIfConfMaxAdvertisementInterval (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfMaxAdvertisementIntervalSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIrdpIfConfMaxAdvertisementInterval
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfMaxAdvertisementIntervalGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIrdpIfConfMaxAdvertisementInterval
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfMinAdvertisementIntervalTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIrdpIfConfMinAdvertisementInterval (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              i4_SLongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              i4_SLongValue,
                                                              pMultiData->
                                                              i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfMinAdvertisementIntervalSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIrdpIfConfMinAdvertisementInterval
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfMinAdvertisementIntervalGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIrdpIfConfMinAdvertisementInterval
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfAdvertisementLifetimeTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIrdpIfConfAdvertisementLifetime (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfAdvertisementLifetimeSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIrdpIfConfAdvertisementLifetime
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfAdvertisementLifetimeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIrdpIfConfAdvertisementLifetime
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfPerformRouterDiscoveryTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIrdpIfConfPerformRouterDiscovery (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            i4_SLongValue,
                                                            pMultiData->
                                                            i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfPerformRouterDiscoverySet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIrdpIfConfPerformRouterDiscovery
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfPerformRouterDiscoveryGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIrdpIfConfPerformRouterDiscovery
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfSolicitationAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIrdpIfConfSolicitationAddress (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiIndex->pIndex[1].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfSolicitationAddressSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIrdpIfConfSolicitationAddress
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfSolicitationAddressGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsIrdpIfConfTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        IPFWD_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsIrdpIfConfSolicitationAddress
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue, &pMultiData->u4_ULongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientRetransmissionTimeoutTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsRarpClientRetransmissionTimeout
        (pu4Error, pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientRetransmissionTimeoutSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetFsRarpClientRetransmissionTimeout
        (pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientRetransmissionTimeoutGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhGetFsRarpClientRetransmissionTimeout
        (&pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientMaxRetriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsRarpClientMaxRetries
        (pu4Error, pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientMaxRetriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetFsRarpClientMaxRetries (pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientMaxRetriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhGetFsRarpClientMaxRetries (&pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientPktsDiscardedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhGetFsRarpClientPktsDiscarded (&pMultiData->u4_ULongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal =
        nmhTestv2FsRarpServerStatus (pu4Error, pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetFsRarpServerStatus (pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhGetFsRarpServerStatus (&pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerPktsDiscardedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhGetFsRarpServerPktsDiscarded (&pMultiData->u4_ULongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerTableMaxEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsRarpServerTableMaxEntries
        (pu4Error, pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerTableMaxEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetFsRarpServerTableMaxEntries (pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerTableMaxEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    ARP_PROT_LOCK ();

    i4RetVal = nmhGetFsRarpServerTableMaxEntries (&pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsHardwareAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsRarpServerDatabaseTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            (size_t) pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    ARP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsHardwareAddrLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsHardwareAddrLen (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsHardwareAddrLenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetFsHardwareAddrLen (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsHardwareAddrLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsRarpServerDatabaseTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsHardwareAddrLen (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsProtocolAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsProtocolAddress (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiData->u4_ULongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsProtocolAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetFsProtocolAddress (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->u4_ULongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsProtocolAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsRarpServerDatabaseTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsProtocolAddress (pMultiIndex->pIndex[0].pOctetStrValue,
                                        &pMultiData->u4_ULongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsEntryStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhTestv2FsEntryStatus (pu4Error,
                                       pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsEntryStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhSetFsEntryStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsEntryStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    if (nmhValidateIndexInstanceFsRarpServerDatabaseTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        ARP_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetFsEntryStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                    &pMultiData->i4_SLongValue);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfStaticRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhTestv2FsNoOfStaticRoutes (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfStaticRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsNoOfStaticRoutes (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfStaticRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsNoOfStaticRoutes (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfAggregatedRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsNoOfAggregatedRoutes
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfAggregatedRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsNoOfAggregatedRoutes (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfAggregatedRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsNoOfAggregatedRoutes (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsNoOfRoutes (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsNoOfRoutes (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsNoOfRoutes (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfReassemblyListsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsNoOfReassemblyLists
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfReassemblyListsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsNoOfReassemblyLists (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfReassemblyListsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsNoOfReassemblyLists (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfFragmentsPerListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsNoOfFragmentsPerList
        (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfFragmentsPerListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsNoOfFragmentsPerList (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfFragmentsPerListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsNoOfFragmentsPerList (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpGlobalDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhTestv2FsIpGlobalDebug (pu4Error, pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpGlobalDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhSetFsIpGlobalDebug (pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpGlobalDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;
    UNUSED_PARAM (pMultiIndex);
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhGetFsIpGlobalDebug (&pMultiData->i4_SLongValue);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexFsRarpServerDatabaseTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    tSNMP_OCTET_STRING_TYPE *p1fsHardwareAddress;
    ARP_PROT_LOCK ();

    p1fsHardwareAddress = pNextMultiIndex->pIndex[0].pOctetStrValue;    /* not-accessible */
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRarpServerDatabaseTable (p1fsHardwareAddress)
            == SNMP_FAILURE)
        {
            ARP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRarpServerDatabaseTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             p1fsHardwareAddress) == SNMP_FAILURE)
        {
            ARP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    ARP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsIrdpIfConfTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    INT4                i4fsIrdpIfConfIfNum;
    INT4                i4fsIrdpIfConfSubref;
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIrdpIfConfTable (&i4fsIrdpIfConfIfNum,
                                               &i4fsIrdpIfConfSubref)
            == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIrdpIfConfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4fsIrdpIfConfIfNum,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4fsIrdpIfConfSubref) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4fsIrdpIfConfIfNum;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4fsIrdpIfConfSubref;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCidrAdvertTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4fsCidrAdvertAddress;
    UINT4               u4fsCidrAdvertAddressMask;
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCidrAdvertTable (&u4fsCidrAdvertAddress,
                                               &u4fsCidrAdvertAddressMask)
            == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCidrAdvertTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4fsCidrAdvertAddress,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4fsCidrAdvertAddressMask) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4fsCidrAdvertAddress;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4fsCidrAdvertAddressMask;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsCidrAggTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4fsCidrAggAddress;
    UINT4               u4fsCidrAggAddressMask;
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsCidrAggTable (&u4fsCidrAggAddress,
                                            &u4fsCidrAggAddressMask)
            == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsCidrAggTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4fsCidrAggAddress,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4fsCidrAggAddressMask) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4fsCidrAggAddress;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4fsCidrAggAddressMask;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsIpifTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    INT4                i4fsIpifIndex;
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpifTable (&i4fsIpifIndex) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpifTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4fsIpifIndex) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4fsIpifIndex;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsIpCommonRoutingTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4fsIpRouteDest;
    UINT4               u4fsIpRouteMask;
    INT4                i4fsIpRouteTos;
    UINT4               u4fsIpRouteNextHop;
    INT4                i4fsIpRouteProto;

    RTM_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpCommonRoutingTable (&u4fsIpRouteDest,
                                                    &u4fsIpRouteMask,
                                                    &i4fsIpRouteTos,
                                                    &u4fsIpRouteNextHop,
                                                    &i4fsIpRouteProto)
            == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpCommonRoutingTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4fsIpRouteDest,
             pFirstMultiIndex->pIndex[1].u4_ULongValue, &u4fsIpRouteMask,
             pFirstMultiIndex->pIndex[2].i4_SLongValue, &i4fsIpRouteTos,
             pFirstMultiIndex->pIndex[3].u4_ULongValue, &u4fsIpRouteNextHop,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &i4fsIpRouteProto) == SNMP_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4fsIpRouteDest;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4fsIpRouteMask;
    pNextMultiIndex->pIndex[2].i4_SLongValue = i4fsIpRouteTos;
    pNextMultiIndex->pIndex[3].u4_ULongValue = u4fsIpRouteNextHop;
    pNextMultiIndex->pIndex[4].i4_SLongValue = i4fsIpRouteProto;

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsIpPathMtuTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4fsIpPmtuDestination;
    INT4                i4fsIpPmtuTos;
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpPathMtuTable (&u4fsIpPmtuDestination,
                                              &i4fsIpPmtuTos) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpPathMtuTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4fsIpPmtuDestination,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4fsIpPmtuTos) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4fsIpPmtuDestination;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4fsIpPmtuTos;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsIpRtrLstTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4fsIpRtrLstAddress;
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpRtrLstTable (&u4fsIpRtrLstAddress)
            == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpRtrLstTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4fsIpRtrLstAddress) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4fsIpRtrLstAddress;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsIpAddressTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4fsIpAddrTabAddress;
    IPFWD_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpAddressTable (&u4fsIpAddrTabAddress)
            == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpAddressTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4fsIpAddrTabAddress) == SNMP_FAILURE)
        {
            IPFWD_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4fsIpAddrTabAddress;

    IPFWD_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsIpTraceTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4fsIpTraceDest;
    INT4                i4fsIpTraceHopCount;
    UDP_PROT_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpTraceTable (&u4fsIpTraceDest,
                                            &i4fsIpTraceHopCount)
            == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpTraceTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue, &u4fsIpTraceDest,
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &i4fsIpTraceHopCount) == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4fsIpTraceDest;
    pNextMultiIndex->pIndex[1].i4_SLongValue = i4fsIpTraceHopCount;
    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsIpTraceConfigTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    UINT4               u4fsIpTraceConfigDest;
    UDP_PROT_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIpTraceConfigTable (&u4fsIpTraceConfigDest)
            == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIpTraceConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &u4fsIpTraceConfigDest) == SNMP_FAILURE)
        {
            UDP_PROT_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].u4_ULongValue = u4fsIpTraceConfigDest;
    UDP_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsIpOptProcEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIpOptProcEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpNumMultipathDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsIpNumMultipath (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpLoadShareEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIpLoadShareEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;

}

INT4
FsIpEnablePMTUDDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsIpEnablePMTUD (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuEntryAgeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsIpPmtuEntryAge (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPmtuTableSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIpPmtuTableSize
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;

}

INT4
FsIpTraceConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    UDP_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIpTraceConfigTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    UDP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpAddressTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsIpAddressTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpRtrLstTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsIpRtrLstTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpPathMtuTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsIpPathMtuTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpCommonRoutingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    RTM_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIpCommonRoutingTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpifTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIpifTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendRedirectEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIcmpSendRedirectEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendUnreachableEnableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIcmpSendUnreachableEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendEchoReplyEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIcmpSendEchoReplyEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpNetMaskReplyEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIcmpNetMaskReplyEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpTimeStampReplyEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIcmpTimeStampReplyEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpDirectQueryEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIcmpDirectQueryEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsDomainNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsDomainName (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsTimeToLiveDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsTimeToLive (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpSendSecurityFailuresEnableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIcmpSendSecurityFailuresEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIcmpRecvSecurityFailuresEnableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIcmpRecvSecurityFailuresEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsCidrAggTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsCidrAggTable (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsCidrAdvertTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsCidrAdvertTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpSendAdvertisementsEnableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIrdpSendAdvertisementsEnable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIrdpIfConfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsIrdpIfConfTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientRetransmissionTimeoutDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhDepv2FsRarpClientRetransmissionTimeout
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpClientMaxRetriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhDepv2FsRarpClientMaxRetries
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhDepv2FsRarpServerStatus
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerTableMaxEntriesDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhDepv2FsRarpServerTableMaxEntries
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsRarpServerDatabaseTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    ARP_PROT_LOCK ();

    i4RetVal = nmhDepv2FsRarpServerDatabaseTable
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    ARP_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfStaticRoutesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsNoOfStaticRoutes
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfAggregatedRoutesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsNoOfAggregatedRoutes
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfRoutesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsNoOfRoutes (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfReassemblyListsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsNoOfReassemblyLists
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsNoOfFragmentsPerListDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal = nmhDepv2FsNoOfFragmentsPerList
        (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}

INT4
FsIpGlobalDebugDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    INT4                i4RetVal;
    IPFWD_PROT_LOCK ();

    i4RetVal =
        nmhDepv2FsIpGlobalDebug (pu4Error, pSnmpIndexList, pSnmpvarbinds);

    IPFWD_PROT_UNLOCK ();
    return i4RetVal;
}
