/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipfwdutl.c,v 1.15 2014/11/15 12:57:24 siva Exp $
 *
 * Description:This file contains the Utility functions of the  
 *             IP forwarding module                            
 *
 *******************************************************************/
#include "ipinc.h"

/*****************************************************************************/
/* Function Name     :  IpCompareBcastAddr                                   */
/*                                                                           */
/* Description       :  This function is used to check whether the broadcast */
/*                      address passed matches with default broadcast address*/
/*                      or the specific  interface broadcat address          */
/*                                                                           */
/* Input(s)          :  1. u4Addr - The Broadcast address which is to be     */
/*                                  compared.                                */
/*                      2. pAddrRec- The address record pointer              */
/*                                                                           */
/* Output(s)         :  None                                                 */
/*                                                                           */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                    */
/*****************************************************************************/
/*             $$TRACE_PROCEDURE_NAME  = IpCompareBcastAddr                  */
/*             $$TRACE_PROCEDURE_LEVEL = INTMD                               */
/* ***************************************************************************/
INT4
IpCompareBcastAddr (UINT4 u4Addr, UINT4 u4IpAddr, UINT4 u4BcastAddr)
{
    UINT4               u4DefaultAddr = 0;

   /**** $$TRACE_LOG (ENTRY, "Addr %x Braodcast %x\n", u4Addr, 
                      pIpAddrRec->u4Bcast_addr); ****/

    if ((u4DefaultAddr = IpGetDefaultBroadcastAddr (u4IpAddr)) == u4Addr)
    {
      /**** $$TRACE_LOG (EXIT, "Matching the default Bcast Address of the 
                         received interface \n"); ****/
        return IP_SUCCESS;
    }
    else
    {
        return ((u4Addr == u4BcastAddr) ? IP_SUCCESS : IP_FAILURE);
    }
}

/*****************************************************************************/
/* Function Name     :  IpVerifyGetAddrTypeInCxt                             */
/*                                                                           */
/* Description       : This function first verifies the address passed as    */
/*                     parameter to the function based on the Flag which     */
/*                     also passed as parameter.This verification is done    */
/*                     both values of the Flag (IP_ADDR_DEST - address to    */
/*                     checked is for Destination address ,IP_ADDR_SRC -     */
/*                     address to be checked is for Source address. If the   */
/*                     address is CLASS-E or greater than CLASS-E address    */
/*                     range then IP_FAILURE is returned else if the Flag says  */
/*                     that the addressto be verified is Source address      */
/*                     then additional check is made whether the souce       */
/*                     address is Multicast or greater than CLASS-E which    */
/*                     also checks for broadcast address . After verify the  */
/*                     address the Classifcation of valid address is made .  */
/*                     The return the address type based on the following    */
/*                     check . If the address matched any of the router's    */
/*                     interface address or If the address is Multicast      */
/*                     address or If the address is Class-A,Class-B or       */
/*                     Class-C or If it is a broadcast address.              */
/*                                                                           */
/*                                                                           */
/* Global Variables  : None                                                  */
/* Affected                                                                  */
/*                                                                           */
/* Input(s)          :  pIp - The IP header structure                        */
/*                      u1Flag - The Flag indication whether the IP address  */
/*                                 passed is Source or Destination address   */
/*                                                                           */
/* Output(s)         : None                                                  */
/*                                                                           */
/* Result            : IP_FOR_THIS_NODE | IP_UCAST | IP_MCAST | IP_BCAST |   */
/*                       IP_FAILURE                                             */
/*****************************************************************************/
/*             $$TRACE_PROCEDURE_NAME  = IpVerifyGetAddrType                 */
/*             $$TRACE_PROCEDURE_LEVEL = INTMD                               */
/* ***************************************************************************/
INT4
IpVerifyGetAddrTypeInCxt (UINT4 u4ContextId, t_IP * pIp, UINT1 u1Flag)
{
    UINT4               u4Addr = 0;
    UINT4               u4Port = 0;
    UINT1               u1AdminStatus = 0;
    UINT1               u1OperStatus = 0;

    if (u1Flag == IP_ADDR_SRC)
    {
        u4Addr = pIp->u4Src;
    }
    else
    {
        u4Addr = pIp->u4Dest;
    }

    /*    SOurce address and Destination address cannot be loopback 
     *    address - ANVL FIX
     */

    if (((pIp->u4Src & 0xff000000) == LOOPBACK_ADDRESS) ||
        ((pIp->u4Dest & 0xff000000) == LOOPBACK_ADDRESS))
    {
        return IP_ADDR_LOOP_BACK;    /* Support for Default Loopback Address */
    }

    /* Validate the Source or Destination Address */
    if ((IP_IS_ADDR_CLASS_E (u4Addr) == TRUE) && (u4Addr != IP_GEN_BCAST_ADDR))
    {
        return IP_FAILURE;
    }
    else
    {
        if ((u1Flag == IP_ADDR_SRC) && (IP_ADDRESS_ABOVE_CLASS_C (u4Addr)))
        {
            /* source address > Class C address */
            return IP_FAILURE;
        }
    }

    u4Port = (UINT4) ipif_get_handle_from_addr_InCxt (u4ContextId, pIp->u4Dest);

    /* Check for our Address */
    if ((ipif_is_our_address_InCxt (u4ContextId, pIp->u4Dest) == TRUE) &&
        (u4Port != IPIF_INVALID_INDEX))
    {
        IPIF_CONFIG_GET_ADMIN (u4Port, u1AdminStatus);
        IPIF_CONFIG_GET_OPER (u4Port, u1OperStatus);

        if ((u1AdminStatus != 0) && (u1OperStatus == IPIF_OPER_ENABLE))
        {
            return IP_FOR_THIS_NODE;
        }
    }

    /* Check for Multicast */
    if (IP_IS_ADDR_CLASS_D (pIp->u4Dest) == TRUE)
    {
        return IP_MCAST;
    }

    if ((IP_IS_ADDR_CLASS_A (pIp->u4Dest)) ||
        (IP_IS_ADDR_CLASS_B (pIp->u4Dest)) ||
        (IP_IS_ADDR_CLASS_C (pIp->u4Dest)))
    {

        /* 
         * Check whether the Dest matchs with any one of our Broadcast address
         * else classify it as Unicast i.e it can be unicast or net directed
         * broadcast. Net directed bcast will get resolved only at the last hop
         * for all the other it will be unicast.
         */
        if (IpifIsBroadCastLocalInCxt (u4ContextId, pIp->u4Dest) == IP_SUCCESS)
        {
            return IP_BCAST;
        }
        return IP_UCAST;
    }

    /* Limited broadcast (255.255.255.255) */
    return IP_BCAST;
}

/*****************************************************************************/
/* Function Name     :  IpVerifyTtl                                          */
/*                                                                           */
/* Description       :  The function checks for TTL value in the IP header   */
/*                      if the value is equal to zero then the datagrams is  */
/*                      discarded , ICMP Deatination Unreachable message     */
/*                      is generated ,and statistics is updated, else        */
/*                      the checksum is updated in the IP header.The ICMP    */
/*                      error message is not generated for Multicast and     */
/*                      Broadcast datagrams.                                 */
/*                                                                           */
/* Global Vaiables   : Ipstats - IP statistics variable                      */
/*                                                                           */
/* Input(s)          :  1. pIp - IP header pointer containing the IP header  */
/*                      2. u1Flag - This flag determines the mode of         */
/*                                  reception ( Unicast,Multicast or         */
/*                                  Broadcast                                */
/*                      3. u2Port - The received port index                  */
/*                      4. pBuf   - The IP Buffer pointer                   */
/*                                                                           */
/* Output(s)         :   None                                                */
/*                                                                           */
/* Returns           :  IP_SUCCESS | IP_FAILURE                                    */
/*****************************************************************************/
/*             $$TRACE_PROCEDURE_NAME  = IpVerifyTtl                         */
/*             $$TRACE_PROCEDURE_LEVEL = INTMD                               */
/* ***************************************************************************/
INT4
IpVerifyTtl (t_IP * pIp, INT1 i1Flag, UINT2 u2Port, tIP_BUF_CHAIN_HEADER * pBuf)
{
    t_IP_HEADER        *pIpHdr = NULL;
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4Sum = 0;
    UINT4               u4CxtId = 0;

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (NULL == pIpCxt)
    {
        return IP_FAILURE;
    }
    u4CxtId = pIpCxt->u4ContextId;

    if (pIp->u1Ttl <= 1)
    {
        pIpCxt->Ip_stats.u4In_ttl_err++;
        IPIF_INC_IN_TTL_ERR (u2Port);

        IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
        IP4IF_INC_IN_HDR_ERRS (u2Port);

        IP_CXT_TRC_ARG3 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "s = %x, d = %x, len %d, dropped (time exceeded).\n",
                         pIp->u4Src, pIp->u4Dest, pIp->u2Len);

        if ((i1Flag == IP_FOR_THIS_NODE) || (i1Flag == IP_UCAST))
        {

            t_ICMP              Icmp;
            Icmp.i1Type = ICMP_TIME_EXCEED;
            Icmp.i1Code = ICMP_TTL_EXCEED;
            Icmp.u4ContextId = u4CxtId;
            Icmp.args.u4Unused = 0;

            icmp_error_msg (pBuf, &Icmp);
            return IP_FAILURE;
        }
    }

    /* 
     * Checksum udpdaton 
     */
/* ANVL - TTL was not reduced in the packet */

    {
        t_IP_HEADER         IpHdr;
        pIpHdr =
            (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
            (pBuf, 0, IP_HDR_LEN);

        if (pIpHdr == NULL)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IpHdr, 0, IP_HDR_LEN);
            IpHdr.u1Ttl--;
            u4Sum = (UINT4) (IP_NTOHS (IpHdr.u2Cksum) + 0x100);
            IpHdr.u2Cksum = (UINT2) IP_HTONS (u4Sum + (u4Sum >> IP_SIXTEEN));
            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IpHdr, 0, IP_HDR_LEN);
            return IP_SUCCESS;
        }
        pIpHdr->u1Ttl--;
        u4Sum = (UINT4) (IP_NTOHS (pIpHdr->u2Cksum) + 0x100);
        pIpHdr->u2Cksum = (UINT2) IP_HTONS (u4Sum + (u4Sum >> IP_SIXTEEN));
    }

    return IP_SUCCESS;
}

/*****************************************************************************/
/* Function Name     :  IpForwardCheckInterface                              */
/*                                                                           */
/* Description       :  This Function checks the Admin and Oper status       */
/*                      of the given logical Port and also checks whether    */
/*                      the system is acting as a gateway.If any failure     */
/*                      condition of this checking happens a ICMP error      */
/*                      message is generated                                 */
/*                                                                           */
/* Global Variables  : Ip_stats - The IP statistics variable                 */
/*                                                                           */
/* Inputs            : u2Rt_port - forwarding logical port Index             */
/*                     pIp       - Pointer to IP header                      */
/*                     i1Flag    - Flag indication the destination address   */
/*                                  Type .                                   */
/*                     pBuf      - The IP Buffer pointer                    */
/*                                                                           */
/* Outputs           : None                                                  */
/*                                                                           */
/* Result            :  IP_SUCCESS | IP_FAILURE                                    */
/*****************************************************************************/
/*             $$TRACE_PROCEDURE_NAME  = IpForwardCheckInterface             */
/*             $$TRACE_PROCEDURE_LEVEL = INTMD                               */
/*****************************************************************************/
   /* Makefile Changes - unused parameter pIp removed it */
INT4
IpForwardCheckInterface (UINT2 u2Rt_port, INT1 i1Flag,
                         tIP_BUF_CHAIN_HEADER * pBuf)
{

    t_ICMP              Icmp;
    tIpCxt             *pIpCxt = NULL;
    UINT1               u1InterfaceType = 0;
    UINT1               u1IpFwdEnable = 0;

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Rt_port);
    if (NULL == pIpCxt)
    {
        return IP_FAILURE;
    }

    /* Take a Data Structure Lock */
    IPFWD_DS_LOCK ();

    u1InterfaceType = gIpGlobalInfo.Ipif_config[u2Rt_port].
        InterfaceId.u1_InterfaceType;
    u1IpFwdEnable = gIpGlobalInfo.Ipif_config[u2Rt_port].u1IpForwardingEnable;

    /* Unlock it */
    IPFWD_DS_UNLOCK ();

    if (u1InterfaceType == IP_IF_TYPE_INVALID)
    {
        return IP_FAILURE;
    }

    if ((ipif_is_ok_to_forward (u2Rt_port) == FALSE) ||
        (IP_CFG_GET_FORWARDING (pIpCxt) != IP_FORW_ENABLE) ||
        (u1IpFwdEnable == IPFORWARD_DISABLE))
    {

        /* Forwarding is disabled or this interface is not up */
        IPIF_INC_OUT_GEN_ERR (u2Rt_port);
        pIpCxt->Ip_stats.u4Out_gen_err++;

        if ((i1Flag == IP_BCAST) || (i1Flag == IP_MCAST))
        {
            return IP_FAILURE;
        }
        /*
         * ICMP error message is not sent for IP_MCAST and IP_BCAST
         */

        Icmp.i1Type = ICMP_DEST_UNREACH;
        Icmp.i1Code = ICMP_NET_UNREACH;
        Icmp.args.u4Unused = 0;
        Icmp.u4ContextId = pIpCxt->u4ContextId;

        icmp_error_msg (pBuf, &Icmp);
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : IpCalcCheckSum                                         */
/*  Description     : Routine to calculate  checksum.                        */
/*                    The checksum field is the 16 bit one's complement of   */
/*                    the one's   complement sum of all 16 bit words in the  */
/*                    header.                                                */
/* Input(s)         : pBuf         - pointer to buffer with data over        */
/*                                   which checksum is to be calculated      */
/*                    u4Size       - size of data in the buffer              */
/*                    u4Offset     - offset from which the data starts       */
/*  Output(s)       : None.                                                  */
/*  Returns         : The Checksum                                           */
/*              THE INPUT  BUFFER IS ASSUMED TO BE IN NETWORK BYTE ORDER     */
/*              AND RETURN VALUE  WILL BE IN HOST ORDER , SO APPLICATION     */
/*              HAS TO CONVERT THE RETURN VALUE APPROPRIATELY                */
/*****************************************************************************/
EXPORT UINT2
IpCalcCheckSum (tIpBuf * pBuf, UINT4 u4Size, UINT4 u4Offset)
{
#define TMP_ARRAY_SIZE   (16 * 2)    /* Should be a multiple of 2 */

    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1               u1Byte = 0;
    UINT1              *pu1Buf = NULL;
    UINT1               au1TmpArray[TMP_ARRAY_SIZE];
    UINT4               u4TmpSize = 0;

    pu1Buf = IP_GET_DATA_PTR_IF_LINEAR (pBuf, u4Offset, u4Size);

    if (pu1Buf == NULL)
    {
        while (u4Size > TMP_ARRAY_SIZE)
        {
            u4TmpSize = TMP_ARRAY_SIZE;
            pu1Buf = IP_GET_DATA_PTR_IF_LINEAR (pBuf, u4Offset, u4TmpSize);
            if (pu1Buf == NULL)
            {
                IP_COPY_FROM_BUF (pBuf, au1TmpArray, u4Offset, u4TmpSize);
                pu1Buf = au1TmpArray;
            }

            while (u4TmpSize > 0)
            {
                u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
                u4Sum += u2Tmp;
                pu1Buf += sizeof (UINT2);
                u4TmpSize -= sizeof (UINT2);
            }

            u4Size -= TMP_ARRAY_SIZE;
            u4Offset += TMP_ARRAY_SIZE;
        }

        while (u4Size > 1)
        {
            IP_COPY_FROM_BUF (pBuf, &u2Tmp, u4Offset, sizeof (UINT2));
            u4Sum += u2Tmp;
            u4Size -= sizeof (UINT2);
            u4Offset += sizeof (UINT2);
        }

        if (u4Size == 1)
        {
            IP_COPY_FROM_BUF (pBuf, &u1Byte, u4Offset, sizeof (UINT1));
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = u1Byte;
            u4Sum += u2Tmp;
        }
    }
    else
    {
        while (u4Size > 1)
        {
            u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
            u4Sum += u2Tmp;
            pu1Buf += sizeof (UINT2);
            u4Size -= sizeof (UINT2);
        }
        if (u4Size == 1)
        {
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = *pu1Buf;
            u4Sum += u2Tmp;
        }
    }

    u4Sum = (u4Sum >> IP_SIXTEEN) + (u4Sum & IP_BIT_ALL);
    u4Sum += (u4Sum >> IP_SIXTEEN);
    u2Tmp = (UINT2) ~(u4Sum);

    return (IP_NTOHS (u2Tmp));
}

/* returns the nukber of multipaths */
INT4
IpCfgGetNumMultipathInCxt (tIpCxt * pIpCxt)
{
    return pIpCxt->Ip_cfg.u1NumOfMultiPath;
}

/*-------------------------------------------------------------------+
 * Function           : IpGetDefaultBroadcastAddr
 *
 * Input(s)           : u4Host_Addr
 *
 * Output(s)          : None.
 *
 * Returns            : Default broadcast address
 *
 * Action :
 * Gets the default broadcast address for the host depending
 * on the class to which the address belongs.
 *
+-------------------------------------------------------------------*/
UINT4
IpGetDefaultBroadcastAddr (UINT4 u4Host_Addr)
{
    UINT4               u4Addr = 0;

  /**** $$TRACE_LOG (ENTRY, "For Addr %x\n", u4Host_Addr);****/

    if (IP_IS_ADDR_CLASS_A (u4Host_Addr))
    {
     /**** $$TRACE_LOG (EXIT, "Class A\n");****/
        u4Addr = (IP_CLASS_A_DEF_BROADCAST_MASK
                  | (IP_CLASS_A_DEF_MASK & u4Host_Addr));
    }
    else if (IP_IS_ADDR_CLASS_B (u4Host_Addr))
    {
     /**** $$TRACE_LOG (EXIT, "Class B\n");****/
        u4Addr = (IP_CLASS_B_DEF_BROADCAST_MASK
                  | (IP_CLASS_B_DEF_MASK & u4Host_Addr));
    }
    else if (IP_IS_ADDR_CLASS_C (u4Host_Addr))
    {
     /**** $$TRACE_LOG (EXIT, "Class C\n");****/
        u4Addr = (IP_CLASS_C_DEF_BROADCAST_MASK
                  | (IP_CLASS_C_DEF_MASK & u4Host_Addr));
    }
    else
    {
     /**** $$TRACE_LOG (EXIT, "Class NO\n");****/
        u4Addr = 0x00000000;
    }
    return u4Addr;
}

/*-------------------------------------------------------------------+
 * Function           : IpGetDefaultNetmask
 *
 * Input(s)           : u4Host_Addr
 *
 * Output(s)          : None.
 *
 * Returns            : Net mask
 *
 * Action :
 * Gets the default net mask for the address class (A, B or C).
 *
+-------------------------------------------------------------------*/
UINT4
IpGetDefaultNetmask (UINT4 u4Host_Addr)
{
    UINT4               u4Addr = 0;
  /**** $$TRACE_LOG (ENTRY, "For Addr %x\n", u4Host_Addr);****/

    if (IP_IS_ADDR_CLASS_A (u4Host_Addr))
    {
     /**** $$TRACE_LOG (EXIT, "Class A\n");****/
        u4Addr = IP_CLASS_A_DEF_MASK;
    }
    else if (IP_IS_ADDR_CLASS_B (u4Host_Addr))
    {

     /**** $$TRACE_LOG (EXIT, "Class B\n");****/
        u4Addr = IP_CLASS_B_DEF_MASK;
    }
    else if (IP_IS_ADDR_CLASS_C (u4Host_Addr))
    {
     /**** $$TRACE_LOG (EXIT, "Class C\n");****/
        u4Addr = IP_CLASS_C_DEF_MASK;
    }
    else
    {
     /**** $$TRACE_LOG (EXIT, "Class NO\n");****/
        u4Addr = IP_DEF_NET_MASK;
    }
    return u4Addr;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpExtractHdr
 *
 * Input(s)           : pIp, pBuf
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action :
 * This routine is used to get the IP header in the packet.
 *
+-------------------------------------------------------------------*/
/**********************************************************************/
/****   $$TRACE_PROCEDURE_NAME  =  IpExtractHdr                  ****/
/****   $$TRACE_PROCEDURE_LEVEL =  INTMD                           ****/
/**********************************************************************/
INT4
IpExtractHdr (t_IP * pIp, tIP_BUF_CHAIN_HEADER * pBuf)
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;

    UINT1               u1Tmp = 0;
    UINT1              *pu1Options = NULL;
    UINT4               u4TempSrc = 0, u4TempDest = 0;

    pIpHdr = (t_IP_HEADER *) (VOID *) IP_GET_DATA_PTR_IF_LINEAR
        (pBuf, 0, IP_HDR_LEN);

    if (pIpHdr == NULL)
    {

        /* The header is not contiguous in the buffer */

        pIpHdr = &TmpIpHdr;

        /* Copy the header */
        IP_COPY_FROM_BUF (pBuf, (UINT1 *) pIpHdr, 0, IP_HDR_LEN);
    }

    u1Tmp = pIpHdr->u1Ver_hdrlen;

    pIp->u1Version = (UINT1) (IP_VERS (u1Tmp));
    pIp->u1Hlen = (UINT1) ((u1Tmp & IP_HEADER_LEN_MASK) << IP_TWO);
    pIp->u1Tos = pIpHdr->u1Tos;
    pIp->u2Len = CRU_NTOHS (pIpHdr->u2Totlen);

    if ((pIp->u1Hlen < IP_HDR_LEN) || (pIp->u2Len < pIp->u1Hlen))
    {

        IP_TRC_ARG1 (IP_MOD_TRC, ALL_FAILURE_TRC | DATA_PATH_TRC, IP_NAME,
                     "Discarding pkt rcvd with header length %d.\n",
                     pIp->u1Hlen);

        return IP_FAILURE;
    }

    pIp->u2Olen = (UINT2) (IP_OLEN (u1Tmp));
    pIp->u2Id = CRU_NTOHS (pIpHdr->u2Id);
    pIp->u2Fl_offs = CRU_NTOHS (pIpHdr->u2Fl_offs);
    pIp->u1Ttl = pIpHdr->u1Ttl;
    pIp->u1Proto = pIpHdr->u1Proto;
    pIp->u2Cksum = CRU_NTOHS (pIpHdr->u2Cksum);

    MEMCPY (&u4TempSrc, &pIpHdr->u4Src, sizeof (UINT4));
    MEMCPY (&u4TempDest, &pIpHdr->u4Dest, sizeof (UINT4));

    u4TempSrc = CRU_NTOHL (u4TempSrc);
    u4TempDest = CRU_NTOHL (u4TempDest);

    MEMCPY (&pIp->u4Src, &u4TempSrc, sizeof (UINT4));
    MEMCPY (&pIp->u4Dest, &u4TempDest, sizeof (UINT4));

    if (pIp->u2Olen)
    {
        pu1Options = IP_GET_DATA_PTR_IF_LINEAR (pBuf, IP_HDR_LEN, pIp->u2Olen);
        if (pu1Options == NULL)
        {
            IP_COPY_FROM_BUF (pBuf, pIp->au1Options, IP_HDR_LEN, pIp->u2Olen);
        }
        else
        {
            MEMCPY (pIp->au1Options, pu1Options, pIp->u2Olen);
        }
    }

    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpVerifyAddr
 *
 * Input(s)           : u4Addr, u1Flag
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 * Verifies if an address is a valid source/destination address.
+-------------------------------------------------------------------*/
INT4
IpVerifyAddr (UINT4 u4Addr, UINT1 u1Flag)
{
    if ((IP_IS_ADDR_CLASS_E (u4Addr) == TRUE) && (u4Addr != IP_GEN_BCAST_ADDR))
                                       /** This is Not an IP addr type */
        return IP_FAILURE;
    else
    {
        if (u1Flag == IP_ADDR_SRC)
        {
            if ((IP_IS_ADDR_CLASS_D (u4Addr) != FALSE) || (u4Addr > 0xf8000000))
                                    /** This is Not an IP addr type */
                return IP_FAILURE;
        }
        return IP_SUCCESS;
    }
}

 /***************************************************************************/
 /*                                                                         */
 /*  Function Name: ipResolveDestEnetAddr                                   */
 /*                                                                         */
 /*  Description  : resolves Destination Ethernet hardware address.         */
 /*                                                                         */
 /*  Input(s)     : u4IfIndex - Interface index
    u1BroadCastFlag - Flag indicating type of address.      */
 /*                 u4Dest          - Detination address.                   */
 /*                 pu1HwAddr       - Pointer to the hardware address.      */
 /*                                                                         */
 /*  Output(s)    : pu1ArpReqFlag   - Pointer to the flag indicating        */
 /*                                   arp request is possible or not.       */
 /*                                                                         */
 /*  Returns      : IP_SUCCESS - If destination is resolved.                */
 /*               : IP_FAILURE - Otherwise.                                 */
 /*                                                                         */
 /***************************************************************************/
 /**** $$TRACE_PROCEDURE_NAME  =  ipResolveDestEnetAddr  ****/
 /**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/
INT4
ipResolveDestEnetAddr (UINT4 u4IfIndex, UINT1 u1BroadCastFlag, UINT4 u4Dest,
                       UINT1 *pu1HwAddr, UINT1 *pu1ArpReqFlag,
                       UINT1 *pu1EncapType)
{
 /**** $$TRACE_LOG (ENTRY, "Entering ipResolveDestEnetAddr"); ****/

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1EncapType);
    switch (u1BroadCastFlag)
    {

        case IP_MCAST:
        {
            UINT4               u4Mcast = 0;

            pu1HwAddr[0] = 0x01;    /* directly putting h/w address here */
            pu1HwAddr[1] = 0x00;
            u4Mcast = u4Dest & (UINT4) 0xffffff;
            u4Mcast = u4Mcast | (UINT4) 0x5e000000;
            u4Mcast = IP_HTONL (u4Mcast);
            MEMCPY (&pu1HwAddr[IP_TWO], &u4Mcast, sizeof (UINT4));
            break;
        }

        case IP_BCAST:
        {

            MEMSET (pu1HwAddr, IP_BYTE_ALL_ONE, CFA_ENET_ADDR_LEN);
            break;
        }

        case IP_FOR_THIS_NODE:
        case IP_UCAST:
        {
            INT1                i1Status = 0;
            i1Status =
                ArpResolveWithIndex (u4IfIndex, u4Dest, (INT1 *) pu1HwAddr,
                                     pu1EncapType);

            if (i1Status == ARP_SUCCESS)
            {
                return (IP_SUCCESS);
            }

            return (IP_FAILURE);    /* physical address is not found */
        }

        default:
        {
            /* should not be here */
            *pu1ArpReqFlag = FALSE;
            return (IP_FAILURE);    /* physical address is not found */
        }

    }

    return (IP_SUCCESS);
}

