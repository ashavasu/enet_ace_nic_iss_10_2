/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipmbsm.c,v 1.10 2015/06/17 04:46:29 siva Exp $
 *                  
 *
 *******************************************************************/

#ifdef MBSM_WANTED
#include "ipinc.h"
#include "ipnp.h"
#include "dhcp.h"
#define PROTOCOL_ENABLED 1
#ifndef IPVX_ADDR_FMLY_IPV4
#define IPVX_ADDR_FMLY_IPV4 1
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : IpFwdMbsmUpdateCardStatus                                  */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the Line card          */
/*                change status to the IP FWD Task                           */
/*                                                                           */
/*                                                                           */
/* Input        : pProtoMsg - Contains the Slot and Port Information         */
/*                i1Status  - Line card Up/Down status                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

INT4
IpFwdMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIpParms           *pParams = NULL;

    pBuf = CRU_BUF_Allocate_MsgBufChain ((sizeof (tMbsmProtoMsg)), 0);
    if (pBuf == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP, "CRU Buf Allocation Failed\n");
        return MBSM_FAILURE;
    }

    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pProtoMsg, 0,
                               sizeof (tMbsmProtoMsg));

    pParams = (tIpParms *) IP_GET_MODULE_DATA_PTR (pBuf);
    pParams->u1Cmd = (UINT1) i4Event;

    if (OsixQueSend (gIpGlobalInfo.IpPktQId, (UINT1 *) &pBuf, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP, "Send to IP Q failed\n");
        return MBSM_FAILURE;
    }
    OsixEvtSend (gIpGlobalInfo.IpTaskId, IPFWD_PACKET_ARRIVAL_EVENT);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpMbsmUpdateIfaceTable                                     */
/*                                                                           */
/* Description  : Updates the Interface table in the NP based on the         */
/*                Line card status received                                  */
/*                                                                           */
/*                                                                           */
/* Input        : pBuf - Buffer containing the protocol message information  */
/*                u1Status - Line card Status (UP/DOWN)                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
IpMbsmUpdateLCStatus (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmProtoAckMsg    protoAckMsg;
    tMbsmSlotInfo      *pSlotInfo = NULL;

    /* Perform CRU Buf Copy of the pBuf to Structure tMbsmProtoMsg */
    if ((CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
                                    sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                  "CRU Buf Copy From Buf Chain Failed\n");
        return;
    }

    pSlotInfo = &(ProtoMsg.MbsmSlotInfo);

    if (0 == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (u1Cmd == MBSM_MSG_CARD_INSERT)
        {
            IpMbsmInitProtocols (pSlotInfo);

        }
    }
#ifdef VRRP_WANTED
    if (VrrpIpIfHandleCardAttach (pSlotInfo, IPVX_ADDR_FMLY_IPV4) ==
        VRRP_NOT_OK)
    {
        return;
    }
#endif

    /* Send an acknowledgment to the MBSM module to inform the completion
     * of updation of interfaces in the NP */
    protoAckMsg.i4RetStatus = i4RetStatus;
    protoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
    protoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;

    MbsmSendAckFromProto (&protoAckMsg);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpNpMbsmInitProtocols                                      */
/*                                                                           */
/* Description  : Enables the protocols registered                           */
/*                                                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
IpMbsmInitProtocols (tMbsmSlotInfo * pSlotInfo)
{

    IpFsNpMbsmIpInit (pSlotInfo);

#ifdef DHCP_RLY_WANTED

    /*Since Dhcp relay task is Dynamic, we are initialising the NP programming 
       from here */

    if (DhcpIsRlyEnabledInAnyCxt() == PROTOCOL_ENABLED)
    {
        if (IpFsNpMbsmDhcpRlyInit (pSlotInfo) != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                      "Card Insertion: FsNpDhcpRlyInit Failed \n");
            /* Need to return */
            return;
        }
    }
#endif

#ifdef DHCP_SRV_WANTED

    /*Since Dhcp Server task is Dynamic, we are initialising the NP programming 
       from here */

    if (DhcpIsSrvEnabled () == PROTOCOL_ENABLED)
    {
        if (IpFsNpMbsmDhcpSrvInit (pSlotInfo) != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                      "Card Insertion: FsNpDhcpSrvInit Failed \n");
            /* Need to return */
            return;
        }
    }
#endif

}

#endif
