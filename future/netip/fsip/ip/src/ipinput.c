/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipinput.c,v 1.28 2015/03/14 11:44:42 siva Exp $
 *
 * Description:This file contains the Input processing of       
 *             packets received from lower layer and takes the 
 *             decision for delivering to higher layer and/or   
 *             to forwarding module .                          
 *
 *******************************************************************/

#include "ipinc.h"
#include "iss.h"

/*****************************************************************************/
/* Function Name     :  IpInputProcessPkt                                    */
/*                                                                           */
/* Description       :  This function is called whenever a datagram has been */
/*                      received from the Lower layers.This does the intial  */
/*                      header validation,finds the IP address type(Unicast, */
/*                      Multicast,Broadcast),does the option processing,does */
/*                      the checking for Local delivery and then calls the   */
/*                      appropriate function based on the IP address type    */
/*                      to take the forwarding decision                      */
/*                                                                           */
/* Input(s)          :  1. u2Port - The logical port (at IP level) through   */
/*                                  which the datagram has been received     */
/*                      2. pBuf   - The Buffer pointer containing the IP     */
/*                                  header with data                         */
/* Output(s)         :                                                       */
/*                                                                           */
/* Returns           :  SUCCESS | FAILURE                                    */
/*****************************************************************************/
#ifdef __STDC__
INT4
IpInputProcessPkt (UINT2 u2Port, tIP_BUF_CHAIN_HEADER * pBuf)
#else
INT4
IpInputProcessPkt (u2Port, pBuf)
     UINT2               u2Port;
     tIP_BUF_CHAIN_HEADER *pBuf;
#endif
{
    t_IP                Ip;
    tIfConfigRecord     IfConfigRecord;
    tIpParms           *pIpParms = NULL;
    tIpCxt             *pIpCxt = NULL;
    INT4                i4HLDelvStatus;
#ifdef SLI_WANTED
    INT4                i4SliEnqStatus = 0;
#endif
#ifdef VRRP_WANTED
    tIPvXAddr           IpAddr;
    INT4                i4VrId = 0;
    UINT4               u4TempAddr = 0;
#endif
    UINT4               u4Rt_gw = 0;
    UINT4               u4IpAddr = 0, u4NegMask = 0;
    UINT4               u4IfaceIndex = 0;
    UINT4               u4BcastAddr = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4CxtId = 0;
    UINT2               u2Rt_port = 0;
    INT1                i1Flag = 0;

#ifdef VRRP_WANTED
    IPVX_ADDR_CLEAR (&IpAddr);
#endif

    pIpCxt = UtilIpGetCxtFromIpPortNum (u2Port);
    if (NULL == pIpCxt)
    {
        IP_TRC (IP_MOD_TRC, MGMT_TRC, IP_NAME, "IP Context not present\n");
        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }
    u4CxtId = pIpCxt->u4ContextId;

    IP4SYS_INC_IN_RECEVES (u4CxtId);
    IP4IF_INC_IN_RECEVES (u2Port);

    IP4SYS_INC_IN_OCTETS ((IP_GET_BUF_LENGTH (pBuf)), u4CxtId);
    IP4IF_INC_IN_OCTETS (u2Port, (IP_GET_BUF_LENGTH (pBuf)));

    if (IpInputHeaderValidateExtract (pBuf, &Ip, u2Port) == IP_FAILURE)
    {

        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                         "Releasing buffer %x.\n", pBuf);

        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    IP_PKT_DUMP (IP_MOD_TRC, DUMP_TRC, IP_NAME, pBuf, Ip.u2Len,
                 "Rcvd IP packet from LL.\n");
    if (IpGetIfConfigRecord (u2Port, &IfConfigRecord) == IP_FAILURE)
    {
        IP_CXT_TRC (u4CxtId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                    "IP Interface Not Present\n");
        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    if (Ip.u4Src != 0)
    {
        /* Identify the IP address to be used w.r.t to the Source of 
         * the given packet */
        if (IpGetSrcAddressOnInterface (u2Port, Ip.u4Src,
                                        &u4IpAddress) == IP_FAILURE)
        {
            IP_CXT_TRC (u4CxtId, IP_MOD_TRC, MGMT_TRC,
                        IP_NAME, "No matching address on the Interface..\r\n");
        }

        if (IpIfGetAddressInfoInCxt (pIpCxt->u4ContextId, u4IpAddress,
                                     &u4NetMask, &u4BcastAddr,
                                     &u4IfaceIndex) == IP_SUCCESS)
        {
            IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, MGMT_TRC, IP_NAME,
                             "IpIfGetAddressInfo failed for %x\n", u4IpAddress);
        }
    }
    else
    {
        u4IpAddress = IfConfigRecord.u4Addr;
        u4BcastAddr = IfConfigRecord.u4Bcast_addr;
    }

    IPFWD_DS_LOCK ();
    u4IfaceIndex = gIpGlobalInfo.Ipif_config[u2Port].InterfaceId.u4IfIndex;
    IPFWD_DS_UNLOCK ();

    u4NegMask = ~IpGetDefaultNetmask (u4IpAddress);

    if (Ip.u4Src != IP_ANY_ADDR)
    {
        u4IpAddr = Ip.u4Src & u4NegMask;

        /* The Host prefix in the source address should not be zero 
         * also the host prefix should not be all '1s'
         */

        if (u4NetMask != IP_ADDR_31BIT_MASK)
        {
            if ((u4IpAddr == IP_ANY_ADDR) ||
                (u4IpAddr == ~IpGetDefaultNetmask (u4IpAddress)))
            {

                IP_CXT_TRC_ARG4 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                                 "s=%x(interface %d), d=%x, "
                                 "len %d dropped (invalid src addr)\n",
                                 Ip.u4Src, u4IfaceIndex, Ip.u4Dest, Ip.u2Len);

                IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                                 "Releasing buffer %x.\n", pBuf);

                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
        }
    }

    /*Destination and sourde address is cheked for link local range
     *If source is in link local range and destination addr is not in link
     * local range or vice-versa,the packet is dropped*/

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if ((IS_LINK_LOCAL_ADDR (Ip.u4Src)) || (IS_LINK_LOCAL_ADDR (Ip.u4Dest)))
        {
            if (!((IS_LINK_LOCAL_ADDR (Ip.u4Src)) &&
                  (IS_LINK_LOCAL_ADDR (Ip.u4Dest))))
            {
                IP_CXT_TRC_ARG4 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                                 "s=%x(interface %d), d=%x, "
                                 "len %d dropped (Accessing link local network)\n",
                                 Ip.u4Src, u4IfaceIndex, Ip.u4Dest, Ip.u2Len);

                IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                                 "Releasing buffer %x.\n", pBuf);
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
        }
    }
    /*Adding self ping support for default Loopback address */
    if (((i1Flag = (INT1) IpVerifyGetAddrTypeInCxt (pIpCxt->u4ContextId, &Ip,
                                                    IP_ADDR_SRC)) == IP_FAILURE)
        || (i1Flag == IP_ADDR_LOOP_BACK))
    {
        IP4SYS_INC_IN_ADDR_ERRS (u4CxtId);
        IP4IF_INC_IN_ADDR_ERRS (u2Port);

        IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
        IP4IF_INC_IN_HDR_ERRS (u2Port);

        IP_CXT_TRC_ARG4 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "s=%x(interface %d), d=%x, len %d "
                         "dropped (invalid src/dest addr)\n",
                         Ip.u4Src, u4IfaceIndex, Ip.u4Dest, Ip.u2Len);

        IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                         "Releasing buffer %x.\n", pBuf);

        IP_RELEASE_BUF (pBuf, FALSE);
        return IP_FAILURE;
    }

    if ((i1Flag == IP_UCAST) && (u4NetMask != IP_ADDR_31BIT_MASK))
    {
        u4IpAddr = Ip.u4Dest & u4NegMask;
        if (u4IpAddr == IP_ANY_ADDR)
        {
            IP_CXT_TRC_ARG4 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "s=%x(interface %d), d=%x, "
                             "len %d dropped (invalid dst addr)\n", Ip.u4Src,
                             u4IfaceIndex, Ip.u4Dest, Ip.u2Len);

            IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                             "Releasing buffer %x.\n", pBuf);

            IP_RELEASE_BUF (pBuf, FALSE);
            return IP_FAILURE;
        }
    }

#ifdef VRRP_WANTED
    /* Check whether VRRP is enabled on the interface . If so
     * VRRP State :
     *       MASTER - 
     *            1. Respond for packets with interface IP address (Owner)
     *            2. Process Multicast packets
     *            3. Forward all other unicast packets.
     *
     *       BACKUP -
     *            1. Respond to packets with interface IP address
     *            2. Process Multicast packets.
     *            3. Drop all other packets.
     */

    if (u4CxtId == IP_DEFAULT_CONTEXT)
    {
        if (VrrpEnabledOnInterface ((INT4) u4IfaceIndex) == 0)
        {
            if (i1Flag == IP_UCAST)
            {
                /* It is not addressed to the local ip address.
                   It may be a Virtual Ip Address. 

                   If Virtual Ip Address, if the accept mode 
                   is true, this router is master and not owning
                   the address, should respond to the packet. This
                   is handled by adding IP address to the secondary
                   IP address list. 

                   If Virtual IP address, if the accept mode is false,
                   this router is not master or not owning the address
                   should drop the packet. This is handled here. */

                u4TempAddr = OSIX_HTONL (Ip.u4Dest);
                IPVX_ADDR_INIT_FROMV4 (IpAddr, u4TempAddr);

                if (VrrpGetVridFromAssocIpAddress (u4IfaceIndex,
                                                   IpAddr, &i4VrId)
                    == VRRP_NOT_OK)
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    return IP_FAILURE;
                }
            }
        }
    }
#endif /* VRRP_WANTED */

#if defined (MPLS_WANTED) || (MPLS_RSVPTE_WANTED)
    if (i1Flag == IP_UCAST)
    {
        UINT2               u2DestUdpPort;
        if (Ip.u1Proto == UDP_PTCL)
        {
            /* Get the Destination UDP Port */
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &u2DestUdpPort,
                                       Ip.u1Hlen + sizeof (UINT2),
                                       sizeof (UINT2));
            u2DestUdpPort = OSIX_NTOHS (u2DestUdpPort);
            if (u2DestUdpPort == RSVP_DEST_PORT)
            {
                i1Flag = IP_FOR_THIS_NODE;
            }
        }
    }
#endif

    /* 
     * Option Processing is done only for Unicast IP address pkts
     */
    if (IP_CFG_GET_OPT_PROC_ENABLE (pIpCxt) == IP_OPT_PROC_ENABLE)
    {
        if ((Ip.u2Olen > 0) && (i1Flag != IP_MCAST) && (i1Flag != IP_BCAST))
        {
            if (ip_process_options_InCxt
                (pIpCxt, &Ip, &i1Flag, pBuf, &u2Rt_port,
                 &u4Rt_gw) == IP_FAILURE)
            {

                pIpCxt->Ip_stats.u4In_opt_err++;
                IP4SYS_INC_IN_ADDR_ERRS (u4CxtId);
                IP4IF_INC_IN_ADDR_ERRS (u2Port);

                IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
                IP4IF_INC_IN_HDR_ERRS (u2Port);

                IP_CXT_TRC_ARG1 (u4CxtId, IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                                 "Releasing buffer %x.\n", pBuf);
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            ip_put_hdr (pBuf, &Ip, TRUE);
        }
    }

    pIpParms = (tIpParms *) IP_GET_MODULE_DATA_PTR (pBuf);

    switch (i1Flag)
    {

        case IP_FOR_THIS_NODE:
        {
            if ((pIpParms->u1LinkType == LINK_MCAST) ||
                (pIpParms->u1LinkType == LINK_BCAST))
            {
                /* this condition is from RFC 1812 Section 5.3.4
                 * if packet is unicast ip addr and link layer multicast/broadcast
                 * silently  dicard the packet.
                 */

                IP_TRC_ARG4 (IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "s=%x(interface %d), d=%x, "
                             "len %d dropped (link layer bcast/mcast)",
                             Ip.u4Src, u4IfaceIndex, Ip.u4Dest, Ip.u2Len);

                IP_TRC_ARG1 (IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                             "Releasing buffer %x.\n", pBuf);

                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }

            if (u4Rt_gw == 0)
            {
#ifdef SLI_WANTED
                i4SliEnqStatus =
                    SliEnqueueIpv4RawPacketInCxt (pIpCxt->u4ContextId,
                                                  Ip.u1Proto, Ip.u2Len,
                                                  Ip.u4Src, Ip.u4Dest, pBuf);
#endif

                IP_TRC_ARG4 (IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "s = %x, d = %x (local) len %d, "
                             "rcvd on interface %d.\n",
                             Ip.u4Src, Ip.u4Dest, Ip.u2Len, u4IfaceIndex);

                i4HLDelvStatus = IpDeliverHigherLayerProtocol (pBuf, &Ip,
                                                               (UINT1) i1Flag,
                                                               u2Port);

                if (i4HLDelvStatus == IP_FAILURE)
                {
#ifdef SLI_WANTED
                    if (i4SliEnqStatus == SLI_FAILURE)
#endif
                    {
                        IpHandleUnKnownProtosInCxt (pIpCxt, pBuf, u2Port,
                                                    i1Flag);
                    }
                    IP_RELEASE_BUF (pBuf, FALSE);
                }
                return IP_SUCCESS;
            }
        }

            /* 
             * Fall through for source routeing.
             * This case of the u1Flag is executed if a Unicast IP packet has been
             * received and which is not addressed to the router interfaces and also 
             * forced by options
             */

        case IP_UCAST:
        {
            if ((pIpParms->u1LinkType == LINK_MCAST) ||
                (pIpParms->u1LinkType == LINK_BCAST) ||
                (Ip.u4Src == IP_ANY_ADDR))
            {

                /* this condition is from RFC 1812 Section 5.3.4
                 * if packet is unicast ip addr and link layer multicast/broadcast
                 * silently dicard the packet.
                 */

                IP_TRC_ARG4 (IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "s=%x(interface %d), d=%x, "
                             "len %d dropped (link layer bcast/mcast).\n",
                             Ip.u4Src, u4IfaceIndex, Ip.u4Dest, Ip.u2Len);

                IP_TRC_ARG1 (IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                             "Releasing buffer %x.\n", pBuf);

                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }

            if (IpForward (pBuf, &Ip, i1Flag, u2Rt_port,
                           u4Rt_gw, u2Port) == IP_FAILURE)
            {
                IP_TRC_ARG1 (IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                             "Releasing buffer %x.\n", pBuf);
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            break;
        }

        case IP_MCAST:
        {

            /* as per RFC 1812 section 5.3.4, we should forward only if
             * link layer destination address is  multicast/broadcast
             * however, here, we will not accept for ourselves also.
             */

            IP4SYS_INC_IN_MCAST_OCTETS (Ip.u2Len, u4CxtId);
            IP4IF_INC_IN_MCAST_OCTETS (u2Port, Ip.u2Len);

            IP4SYS_INC_IN_MCAST_PKT (u4CxtId);
            IP4IF_INC_IN_MCAST_PKT (u2Port);

            if (pIpParms->u1LinkType == LINK_UCAST)
            {

                IP_TRC_ARG4 (IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "Discarding s=%x, d=%x, len %d "
                             "rcvd on interface %d as link layer unicast.\n",
                             Ip.u4Src, Ip.u4Dest, Ip.u2Len, u4IfaceIndex);

                IP_TRC_ARG1 (IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                             "Releasing buffer %x.\n", pBuf);
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            else if (IpInputPrcsMcastPkt (pBuf, &Ip, i1Flag, u2Port) ==
                     IP_FAILURE)
            {

                IP_TRC_ARG1 (IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                             "Releasing buffer %x.\n", pBuf);
                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            break;
        }

        case IP_BCAST:
        {
            if ((Ip.u4Dest == IP_GEN_BCAST_ADDR) ||
                (IpCompareBcastAddr (Ip.u4Dest, u4IpAddress, u4BcastAddr)
                 == IP_SUCCESS))
            {
                IP4SYS_INC_IN_BCAST_PKT (u4CxtId);
                IP4IF_INC_IN_BCAST_PKT (u2Port);
#ifdef SLI_WANTED
                i4SliEnqStatus =
                    SliEnqueueIpv4RawPacketInCxt (pIpCxt->u4ContextId,
                                                  Ip.u1Proto, Ip.u2Len,
                                                  Ip.u4Src, Ip.u4Dest, pBuf);
#endif
                IP_TRC_ARG4 (IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                             "s = %x, d = %x, len %d, rcvd on interface %d.\n",
                             Ip.u4Src, Ip.u4Dest, Ip.u2Len, u4IfaceIndex);

                i4HLDelvStatus = IpDeliverHigherLayerProtocol (pBuf, &Ip,
                                                               (UINT1) i1Flag,
                                                               u2Port);
                if (i4HLDelvStatus == IP_FAILURE)
                {
#ifdef SLI_WANTED
                    if (i4SliEnqStatus == SLI_FAILURE)
#endif
                    {
                        IpHandleUnKnownProtosInCxt (pIpCxt, pBuf, u2Port,
                                                    i1Flag);
                    }
                    IP_RELEASE_BUF (pBuf, FALSE);
                }
                return IP_SUCCESS;
            }

            if ((Ip.u4Src == IP_ANY_ADDR) ||
                (IpForwardBcast (pBuf, &Ip, u2Port) == IP_FAILURE))
            {

                IP_TRC_ARG1 (IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                             "Releasing buffer %x.\n", pBuf);

                IP_RELEASE_BUF (pBuf, FALSE);
                return IP_FAILURE;
            }
            break;
        }
        default:
            IP_TRC_ARG1 (IP_MOD_TRC, BUFFER_TRC, IP_NAME,
                         "Releasing buffer %x.\n", pBuf);
            IP_RELEASE_BUF (pBuf, FALSE);
            return IP_FAILURE;
            /* no break here as we are directly returning */
    }
    return IP_SUCCESS;
}

/*****************************************************************************/
/* Function Name     :  IpInputHeaderValidateExtract                         */
/*                                                                           */
/* Description       :  This function does header validation and extraction  */
/*                      of the IP header from the IP buffer.The header      */
/*                      validation involves the following steps              */
/*                      1. Check for data size from the packet received from */
/*                      the link layer.If the data length is less than       */
/*                      default IP header size (20 Octets) then the packet   */
/*                      is discarded.                                        */
/*                      2. The Extraction of the header is done              */
/*                      3. If the IP version number field in the IP header   */
/*                      is not equal to four then the packet is discarded.   */
/*                      4. The checksum is calculated .                      */
/*                                                                           */
/*                                                                           */
/* Global Variables  :  None                                                 */
/*                                                                           */
/* Input(s)          :  pBuf     - IP Buffer pointer containing the IP      */
/*                                 header                                    */
/*                      pIp  - pointer containing the IP header              */
/*                      u2Port - The received port index                     */
/* Output(s)         :  None                                                 */
/*                                                                           */
/* Returns           :  IP_SUCCESS | IP_FAILURE                              */
/*****************************************************************************/
#ifdef __STDC__
INT4
IpInputHeaderValidateExtract (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp,
                              UINT2 u2Port)
#else
INT4
IpInputHeaderValidateExtract (pBuf, pIp, u2Port)
     tIP_BUF_CHAIN_HEADER *pBuf;
     t_IP               *pIp;
     UINT2               u2Port;
#endif
{
    tIpCxt             *pIpCxt = NULL;
    UINT4               u4IfaceIndex = 0;
    UINT4               u4CxtId = 0;
    UINT2               u2Len = 0;
#if ((defined (VXLAN_WANTED)) && (defined (NPAPI_WANTED)))
    INT4               i4VxlanStatus = VXLAN_FAILURE;
#endif

    pIpCxt = UtilIpGetCxtFromIpPortNum ((UINT4) u2Port);
    if (pIpCxt == NULL)
    {
        return IP_FAILURE;
    }
    u4CxtId = pIpCxt->u4ContextId;

    u2Len = (UINT2) IP_GET_BUF_LENGTH (pBuf);
    IPIF_CONFIG_GET_IF_INDEX (u2Port, u4IfaceIndex);

    if (u2Len < IP_HDR_LEN)
    {

        pIpCxt->Ip_stats.u4In_len_err++;
        IPIF_INC_IN_LEN_ERR (u2Port);

        IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
        IP4IF_INC_IN_HDR_ERRS (u2Port);

        IP4SYS_INC_IN_TRUNCATED_PKTS (u4CxtId);
        IP4IF_INC_IN_TRUNCATED_PKTS (u2Port);

        IP_CXT_TRC_ARG2 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "Discarding pkt of size %d, recvd on interface %d.\n",
                         u2Len, u4IfaceIndex);
        return IP_FAILURE;
    }

    if (IpExtractHdr (pIp, pBuf) == IP_FAILURE)
    {
        pIpCxt->Ip_stats.u4In_len_err++;
        IPIF_INC_IN_LEN_ERR (u2Port);

        IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
        IP4IF_INC_IN_HDR_ERRS (u2Port);
        return IP_FAILURE;
    }

    /* Verify version  */
    if (pIp->u1Version != IP_VERSION_4)
    {
        pIpCxt->Ip_stats.u4In_ver_err++;
        IPIF_INC_IN_VER_ERR (u2Port);

        IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
        IP4IF_INC_IN_HDR_ERRS (u2Port);

        IP_CXT_TRC_ARG5 (u4CxtId, IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "s=%x(interface %d), d=%x, len %d "
                         "dropped (invalid version %d)\n",
                         pIp->u4Src, u4IfaceIndex, pIp->u4Dest,
                         pIp->u2Len, pIp->u1Version);

        return IP_FAILURE;
    }

#if ((defined (VXLAN_WANTED)) && (defined (NPAPI_WANTED)))
    i4VxlanStatus = VxlanPortCheckMcastVxlanPkt (pBuf, TRUE);
#endif

    /* Received Length of IP Header + Payload should be equal to
     * Total Length field of IP Header. 
     * 
     * Minimum size of packet received on ethernet driver is 60 bytes of 
     * which ethernet header size is 14 bytes. So the above condition is
     * applicable only if we have received a minimum IP payload of 46 bytes.*/
    if ((u2Len > IP_MIN_PAYLOAD_LEN)
#if ((defined (VXLAN_WANTED)) && (defined (NPAPI_WANTED)))
            && (i4VxlanStatus == VXLAN_FAILURE)
#endif
       )
    {
        if (u2Len != pIp->u2Len)
        {
            pIpCxt->Ip_stats.u4In_len_err++;
            IPIF_INC_IN_LEN_ERR (u2Port);

            IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
            IP4IF_INC_IN_HDR_ERRS (u2Port);

            IP4SYS_INC_IN_TRUNCATED_PKTS (u4CxtId);
            IP4IF_INC_IN_TRUNCATED_PKTS (u2Port);

            IP_TRC_ARG2 (IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "Discarding pkt of size %d, recvd on interface %d.\n",
                         u2Len, u4IfaceIndex);

            return IP_FAILURE;
        }
    }
    else
    {
        /* Changed for RPort support in PIM */
        /* When the Packet size is less than IP_MIN_PAYLOAD_LEN, then
         * the CRU Buf valid byte count is changed to the actual
         * packet length */

        if (u2Len >= pIp->u2Len)
        {
            pBuf->pFirstValidDataDesc->u4_ValidByteCount = pIp->u2Len;
            pBuf->pFirstValidDataDesc->u4_FreeByteCount +=
                (UINT4) (u2Len - pIp->u2Len);
        }
        else
        {
            pIpCxt->Ip_stats.u4In_len_err++;
            IPIF_INC_IN_LEN_ERR (u2Port);

            IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
            IP4IF_INC_IN_HDR_ERRS (u2Port);

            IP4SYS_INC_IN_TRUNCATED_PKTS (u4CxtId);
            IP4IF_INC_IN_TRUNCATED_PKTS (u2Port);

            IP_TRC_ARG2 (IP_MOD_TRC, DATA_PATH_TRC, IP_NAME,
                         "Discarding pkt of size %d, recvd on interface %d.\n",
                         u2Len, u4IfaceIndex);

            return IP_FAILURE;
        }
    }

    /* Verify checksum */
    if (IpCalcCheckSum (pBuf, (UINT4) (IP_HDR_LEN + pIp->u2Olen), 0) !=
        IP_VALID_CHECKSUM)
    {
        pIpCxt->Ip_stats.u4In_cksum_err++;
        IPIF_INC_IN_CKSUM_ERR (u2Port);

        IP4SYS_INC_IN_HDR_ERRS (u4CxtId);
        IP4IF_INC_IN_HDR_ERRS (u2Port);

        IP_CXT_TRC_ARG4 (u4CxtId, IP_MOD_TRC, ALL_FAILURE_TRC | DATA_PATH_TRC,
                         IP_NAME,
                         "s=%x(interface %d), d=%x, len %d "
                         "dropped (bad checksum).\n", pIp->u4Src, u4IfaceIndex,
                         pIp->u4Dest, pIp->u2Len);

        return IP_FAILURE;
    }
    return IP_SUCCESS;

}
