/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ippduflt.c,v 1.4 2010/11/12 05:06:19 prabuc Exp $
 *
 * Description:This file contains the routines for filter   
 *             implementation for IP. The routines here are 
 *             used by forwarding module to find whether to 
 *             filter out the datagram.                   
 *
 *******************************************************************/
#include "ipinc.h"

/**************  V A R I A B L E   D E C L A R A T I O N S  ***************/
tIP_SLL             Ip_filt_list[IP_FILT_MAX_LISTS];    /* Filter list table */
UINT1               u1Ip_filtering = IP_FILTERING_DISABLE;    /* Enable/Disable Filtering */

/**************************************************************************/

/**** $$TRACE_MODULE_NAME     = IP_FWD    ****/
/**** $$TRACE_SUB_MODULE_NAME = FILTER    ****/

/**** $$TRACE_PROCEDURE_NAME  = ip_filt_lookup  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD           ****/

/*-------------------------------------------------------------------+
 *
 * Function           : ip_filt_lookup
 *
 * Input(s)           : i4List, u4Src, u4Dest
 *
 * Output(s)          : None
 *
 * Returns            : IP_FILT_BLOCK if the entry is to be filtered out
 *                      IP_FILT_ALLOW otherwise.
 *
 * Action :
 * This procedure is called from IP before forwarding a packet.
 * The procedure verifies if this packet is to be filtered out.
 * It looks at all the entries in the given list by applying
 * the mask.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ip_filt_lookup (INT4 i4List, UINT4 u4Src, UINT4 u4Dest)
#else
INT4
ip_filt_lookup (i4List, u4Src, u4Dest)
     INT4                i4List;    /* List used by this interface 0 -- No list */
     UINT4               u4Src;    /* Source address of the pkt in test        */
     UINT4               u4Dest;    /* Destination address of the pkt in test   */
#endif
{
    t_IP_FILTER        *pIp_filter = NULL;

    /* If filtering is not enabled or no list is assigned to
     * this interface then allow the pkt.
     */

    if ((u1Ip_filtering == IP_FILTERING_DISABLE) || (i4List == 0) ||
        (i4List > IP_FILT_MAX_LISTS))
    {
        return IP_FILT_ALLOW;
    }

    IP_SLL_Scan (&Ip_filt_list[i4List - 1], pIp_filter, t_IP_FILTER *)
    {
        /*
         * If an entry is found in our list then return action from that.
         */
        if (((u4Src & pIp_filter->u4Src_mask) == pIp_filter->u4Src) &&
            ((u4Dest & pIp_filter->u4Dest_mask) == pIp_filter->u4Dest))
        {
            return ((INT4) pIp_filter->u2Status);
        }
    }
    return IP_FILT_ALLOW;        /* This node is not in our list */
}

/*-------------------------------------------------------------------+
 *
 * Function           : ip_filt_init
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS
 *
 * Action :
 * Initializes the singly linked list for filters
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

INT4
ip_filt_init (VOID)
#else
INT4
ip_filt_init ()
#endif
{
    INT2                i2Count = 0;

    for (i2Count = 0; i2Count < IP_FILT_MAX_LISTS; i2Count++)
    {
        IP_SLL_Init (&(Ip_filt_list[i2Count]));
    }
    return IP_SUCCESS;
}

/**** $$TRACE_PROCEDURE_NAME  =  cmp_filt_nodes ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW    ****/

/*-------------------------------------------------------------------+
 *
 * Function           : cmp_filt_nodes
 *
 * Input(s)           : u4Src1, u4Dest1, u4Src2, u4Dest2
 *
 * Output(s)          : None
 *
 * Returns            : ONE_EQ_TWO, ONE_GT_TWO, ONE_LT_TWO
 *
 * Action :
 * This procedure compares two filter entries in the order of (u4Src, u4Dest).
 * Returns ONE_EQ_TWO , ONE_GT_TWO or ONE_LT_TWO depending on result.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
INT4
cmp_filt_nodes (UINT4 u4Src1, UINT4 u4Dest1, UINT4 u4Src2, UINT4 u4Dest2)
#else
INT4
cmp_filt_nodes (u4Src1, u4Dest1, u4Src2, u4Dest2)
     UINT4               u4Src1;    /* Src and Dest of first entry  */
     UINT4               u4Dest1;
     UINT4               u4Src2;    /* Src and Dest of second entry */
     UINT4               u4Dest2;
#endif
{

    if (u4Src1 > u4Src2)
    {
        return ONE_GT_TWO;
    }
    if (u4Src1 < u4Src2)
    {
        return ONE_LT_TWO;
    }

    /* source addresses are equal */
    if (u4Dest1 > u4Dest2)
    {
        return ONE_GT_TWO;
    }
    if (u4Dest1 < u4Dest2)
    {
        return ONE_LT_TWO;
    }
    return ONE_EQ_TWO;
}

/**** $$TRACE_PROCEDURE_NAME  = ip_filt_add  ****/
/**** $$TRACE_PROCEDURE_LEVEL = INTMD        ****/

/*-------------------------------------------------------------------+
 *
 * Function           : ip_filt_add
 *
 * Input(s)           : i4List, u4Src, u4Dest, u4Src_mask, u4Dest_mask, u2Status
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 * This procedure is used to add/delete an entry from filter list.
 * Each list is maintained in the increasing order of (u4Src.u4Dest).
 * So the entire table can be looked as a sorted list with
 * (i4List.u4Src.u4Dest) as key. This scheme helps in fast extraction
 * for network management purposes.
 * The u2Status parameter of IP_FILT_INVALID deletes an entry.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__
INT4
ip_filt_add (INT4 i4List, UINT4 u4Src, UINT4 u4Dest, UINT4 u4Src_mask,
             UINT4 u4Dest_mask, UINT2 u2Status)
#else
INT4
ip_filt_add (i4List, u4Src, u4Dest, u4Src_mask, u4Dest_mask, u2Status)
     INT4                i4List;    /* List to add this entry        */
     UINT4               u4Src;    /*  Source address of the entry  */
     UINT4               u4Dest;    /*   Dest address of the entry   */
     UINT4               u4Src_mask;    /* Source mask to be applied     */
     UINT4               u4Dest_mask;    /* Dest   mask to be applied     */
     UINT2               u2Status;    /* Status of this node           */
#endif
{
    t_IP_FILTER        *pIp_filter = NULL, *pCur_filter = NULL;
    INT4                i4Count = 0, i4Flag = 0;

    if ((i4List == 0) || (i4List > IP_FILT_MAX_LISTS))
    {
        return IP_FAILURE;        /* Invalid list */
    }

    if (u2Status != IP_FILT_INVALID)    /* We dont need it for deletion */
    {

        /* This is not a deletion ; Allocate and update new entry */
        if ((pCur_filter = IP_FILT_MALLOC ()) == NULL)
        {
            return IP_FAILURE;
        }

        pCur_filter->u4Src = u4Src;
        pCur_filter->u4Src_mask = u4Src_mask;
        pCur_filter->u4Dest = u4Dest;
        pCur_filter->u4Dest_mask = u4Dest_mask;
        pCur_filter->u2Status = u2Status;
    }

    if (IP_SLL_First (&Ip_filt_list[i4List - 1]) == NULL)
    {
        if (u2Status == IP_FILT_INVALID)
        {
            return IP_FAILURE;
        }

        IP_SLL_Add (&Ip_filt_list[i4List - 1], &(pCur_filter->Link));
        return IP_SUCCESS;
    }

    IP_SLL_Scan (&Ip_filt_list[i4List - 1], pIp_filter, t_IP_FILTER *)
    {
        i4Count++;                /* Number of nodes in the list */

        i4Flag = cmp_filt_nodes (u4Src, u4Dest, pIp_filter->u4Src,
                                 pIp_filter->u4Dest);
        if (((i4Flag == ONE_LT_TWO) && (u2Status != IP_FILT_INVALID))
            || (i4Flag == ONE_EQ_TWO))
        {
            break;
        }
    }

    if (pIp_filter == NULL)        /* This node is greater than all other nodes */
    {
        if (u2Status == IP_FILT_INVALID)    /* No entry to delete */
        {
            return IP_FAILURE;
        }

        /* Put this entry at the end */
        IP_SLL_Insert (&Ip_filt_list[i4List - 1],
                       IP_SLL_Last (&Ip_filt_list[i4List - 1]),
                       &(pCur_filter->Link));
        return IP_SUCCESS;
    }
    else
    {
        if (i4Flag == ONE_EQ_TWO)
        {
            if (u2Status == IP_FILT_INVALID)
            {
                IP_SLL_Delete (&Ip_filt_list[i4List - 1], &pIp_filter->Link);

                IP_FILTER_FREE (pIp_filter);
            }
            else
            {
                IP_SLL_Replace (&Ip_filt_list[i4List - 1],
                                &(pIp_filter->Link), &(pCur_filter->Link));
            }
            return IP_SUCCESS;
        }
    }

    /* This is not last entry and was not equal to any otheer entries */
    IP_SLL_Insert (&Ip_filt_list[i4List - 1],
                   IP_SLL_Previous (&Ip_filt_list[i4List - 1],
                                    &pIp_filter->Link), &(pCur_filter->Link));

    return IP_SUCCESS;
}

/**** $$TRACE_PROCEDURE_NAME  = ip_filt_get_entry  ****/
/**** $$TRACE_PROCEDURE_LEVEL = LOW                ****/

/*-------------------------------------------------------------------+
 *
 * Function           : ip_filt_get_entry
 *
 * Input(s)           : i4List, u4Src, u4Dest
 *
 * Output(s)          : None
 *
 * Returns            : NULL, pointer
 *
 * Action :
 * Routine to get a filter node corresponding to the key parameters.
 * Returns pointer to the node if found, NULL otherwise.
 *
+-------------------------------------------------------------------*/
#ifdef __STDC__

t_IP_FILTER        *
ip_filt_get_entry (INT4 i4List, UINT4 u4Src, UINT4 u4Dest)
#else
t_IP_FILTER        *
ip_filt_get_entry (i4List, u4Src, u4Dest)
     INT4                i4List;    /* List Number          */
     UINT4               u4Src;    /* Source address       */
     UINT4               u4Dest;    /* Destination address  */
#endif
{
    t_IP_FILTER        *pIp_filter = NULL;

    if ((i4List == 0) || (i4List > IP_FILT_MAX_LISTS))
    {
        return NULL;            /* Invalid list */
    }

    IP_SLL_Scan (&Ip_filt_list[i4List - 1], pIp_filter, t_IP_FILTER *)
    {
        if ((pIp_filter->u4Src == u4Src) && (pIp_filter->u4Dest == u4Dest))
        {
            return (pIp_filter);
        }
    }

    return NULL;                /* Not found */
}
