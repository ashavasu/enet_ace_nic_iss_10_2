/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: netip.c,v 1.27 2015/10/26 13:43:32 siva Exp $
 *
 * Description: Standardised Ip Interface with HL protocols.
 *
 *******************************************************************/

#include "ipinc.h"

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfInfo
 *
 * Description      :   This function provides the Interface related
 *                      Information for a given IfIndex.
 *
 * Inputs           :   u4IfIndex
 *
 * Outputs          :   pNetIpIfInfo - If-Info for the given IfIndex.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE
 *
 *******************************************************************************
 */

INT4
NetIpv4GetIfInfo (UINT4 u4IfIndex, tNetIpv4IfInfo * pNetIpIfInfo)
{
    UINT4               u4CfaIfIndex = 0;

    tIfConfigRecord     IfConfigRecord;

    if (!pNetIpIfInfo)
    {
        return (NETIPV4_FAILURE);
    }

    if (IpGetIfConfigRecord ((UINT2) u4IfIndex, &IfConfigRecord) == IP_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    pNetIpIfInfo->u4IfIndex = u4IfIndex;
    pNetIpIfInfo->u4Mtu = IfConfigRecord.u4Mtu;
    pNetIpIfInfo->u4Addr = IfConfigRecord.u4Addr;
    pNetIpIfInfo->u4NetMask = IfConfigRecord.u4Net_mask;
    pNetIpIfInfo->u4BcastAddr = IfConfigRecord.u4Bcast_addr;
    pNetIpIfInfo->u4IfSpeed = IfConfigRecord.u4IfSpeed;
    pNetIpIfInfo->u4RoutingProtocol = IfConfigRecord.u2Routing_Protocol;
    pNetIpIfInfo->u4Admin = IfConfigRecord.u1Admin;
    pNetIpIfInfo->u4Oper = IfConfigRecord.u1Oper;
    pNetIpIfInfo->u4IfType = IfConfigRecord.u1InterfaceType;
    pNetIpIfInfo->u1EncapType = IfConfigRecord.u1EncapType;
    pNetIpIfInfo->u4ContextId = IfConfigRecord.u4ContextId;
    pNetIpIfInfo->u1ProxyArpAdminStatus = IfConfigRecord.u1ProxyArpAdminStatus;
    pNetIpIfInfo->u1LocalProxyArpStatus = IfConfigRecord.u1LocalProxyArpStatus;

    if (NetIpv4GetCfaIfIndexFromPort (u4IfIndex,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    /* Ip doesn't store the IfName in it's Iface Record. So calling Cfa 
     * Function to get the IfName from the CFA IfIndex. */
    if (CfaGetInterfaceNameFromIndex (u4CfaIfIndex,
                                      pNetIpIfInfo->au1IfName) == OSIX_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }
    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetFirstIfInfoInCxt
 *
 * Description      :   This function provides the First Entry in the If Config
 *                      Record. 
 *
 * Inputs           :   u4ContextId
 *
 * Outputs          :   pNetIpIfInfo - First If Config Record.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 * 
 *******************************************************************************
 */

INT4
NetIpv4GetFirstIfInfoInCxt (UINT4 u4ContextId, tNetIpv4IfInfo * pNetIpIfInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (NetIpv4GetFirstIfInfo (pNetIpIfInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetFirstIfInfo
 *
 * Description      :   This function provides the First Entry in the If Config
 *                      Record. 
 *
 * Inputs           :   None.
 *
 * Outputs          :   pNetIpIfInfo - First If Config Record.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 * 
 *******************************************************************************
 */

INT4
NetIpv4GetFirstIfInfo (tNetIpv4IfInfo * pNetIpIfInfo)
{
    UINT2               u2Port = 0;

    if (pNetIpIfInfo == NULL)
    {
        return (NETIPV4_FAILURE);
    }
    if (IpifGetFirstPort (&u2Port) == IP_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    return (NetIpv4GetIfInfo ((UINT4) u2Port, pNetIpIfInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetNextIfInfoInCxt
 *
 * Description      :   This function returns the Next If Config Record for a
 *                      given IfIndex.
 *
 * Inputs           :   u4IfIndex, u4ContextId
 *
 * Outputs          :   pNextNetIpIfInfo - If-Info of the Next Interface Index.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetNextIfInfoInCxt (UINT4 u4ContextId,
                           UINT4 u4IfIndex, tNetIpv4IfInfo * pNextNetIpIfInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (NetIpv4GetNextIfInfo (u4IfIndex, pNextNetIpIfInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetNextIfInfo
 *
 * Description      :   This function returns the Next If Config Record for a
 *                      given IfIndex.
 *
 * Inputs           :   u4IfIndex
 *
 * Outputs          :   pNextNetIpIfInfo - If-Info of the Next Interface Index.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetNextIfInfo (UINT4 u4IfIndex, tNetIpv4IfInfo * pNextNetIpIfInfo)
{
    UINT2               u2Port = 0, u2NextPort = 0;
    UINT4               u4NextIfIndex = 0;

    if (pNextNetIpIfInfo == NULL)
    {
        return (NETIPV4_FAILURE);
    }

    u2Port = (UINT2) u4IfIndex;

    if (IpGetNextPort (u2Port, &u2NextPort) == IP_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    u4NextIfIndex = (UINT4) u2NextPort;

    return (NetIpv4GetIfInfo (u4NextIfIndex, pNextNetIpIfInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfIndexFromAddr
 *
 * Description      :   This function returns the IfIndex for a given Ip
 *                      Address.
 *
 * Inputs           :   u4IpAddr
 *
 * Outputs          :   pu4IfIndex - IfIndex for the given Ip Address.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetIfIndexFromAddr (UINT4 u4IpAddr, UINT4 *pu4IfIndex)
{
    INT4                i4Port = 0;

    if ((i4Port = ipif_get_handle_from_addr_InCxt (IP_DEFAULT_CONTEXT,
                                                   u4IpAddr)) == IP_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    *pu4IfIndex = (UINT4) i4Port;

    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfIndexFromAddrInCxt
 *
 * Description      :   This function returns the IfIndex for a given Ip
 *                      Address in the specified context
 *
 * Inputs           :   u4ContextId, u4IpAddr
 *
 * Outputs          :   pu4IfIndex - IfIndex for the given Ip Address.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetIfIndexFromAddrInCxt (UINT4 u4ContextId, UINT4 u4IpAddr,
                                UINT4 *pu4IfIndex)
{
    INT4                i4Port = 0;

    if ((i4Port = ipif_get_handle_from_addr_InCxt (u4ContextId,
                                                   u4IpAddr)) == IP_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    *pu4IfIndex = (UINT4) i4Port;

    return (NETIPV4_SUCCESS);
}

 /******************************************************************************
 * Function Name    :   NetIpv4LeakRoute
 *
 * Description      :   This function, depending on the value of u1CmdType Adds
 *                      (or) Deletes (or) modifies the given Route Information
 *                      stored in IpFwdTable accordingly.
 *
 * Inputs           :   u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetRtInfo - Route to be Added | Deleted | Modified.
 *
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv4LeakRoute (UINT1 u1CmdType, tNetIpv4RtInfo * pNetRtInfo)
{
    INT4                i4RetVal = NETIPV4_FAILURE;

    i4RetVal = RtmIpv4LeakRoute (u1CmdType, pNetRtInfo);

    if (i4RetVal == IP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

 /******************************************************************************
 * Function Name    :   NetIpv4RegisterHigherLayerProtocol
 *
 * Description      :   This function is used by Higher Layer Protocols to
 *                      register with Ip for Receiving their packets or for
 *                      getting notification on IF changes or Route Changes.
 *
 * Inputs           :   pRegInfo - Information about the registering protocol
 *                                 and the call back functions.
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4RegisterHigherLayerProtocol (tNetIpRegInfo * pRegInfo)
{
    tIpCxt             *pIpCxt = NULL;

    IPFWD_CXT_LOCK ();
    pIpCxt = UtilIpGetCxt (pRegInfo->u4ContextId);
    if (NULL == pIpCxt)
    {
        IPFWD_CXT_UNLOCK ();
        return NETIPV4_FAILURE;
    }

    if (gIpGlobalInfo.i1RegTableUp != REGUP)
    {
        IPFWD_CXT_UNLOCK ();
        return (NETIPV4_FAILURE);
    }
    if (pRegInfo->u1ProtoId >= IP_MAX_PROTOCOLS)
    {
        IPFWD_CXT_UNLOCK ();
        return (NETIPV4_FAILURE);
    }

    if ((pIpCxt->au1RegTable[pRegInfo->u1ProtoId] == REGISTER) &&
         (pRegInfo->u1ProtoId != SNOOP_ID))
    {
        IPFWD_CXT_UNLOCK ();
        return (NETIPV4_FAILURE);
    }

    pIpCxt->au1RegTable[pRegInfo->u1ProtoId] = REGISTER;

    IP_HLREG_LOCK ();

    if (gIpGlobalInfo.aHLProtoRegFn[pRegInfo->u1ProtoId].u2RegCount == 0)
    {
        /* No context have registered for this protocol. So set the reg functions.
         */
        if (pRegInfo->u2InfoMask & NETIPV4_IFCHG_REQ)
        {
            gIpGlobalInfo.aHLProtoRegFn[pRegInfo->u1ProtoId].pIfStChng =
                (VOID (*)(tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4BitMap))
                pRegInfo->pIfStChng;
        }
        if (pRegInfo->u2InfoMask & NETIPV4_ROUTECHG_REQ)
        {
            gIpGlobalInfo.aHLProtoRegFn[pRegInfo->u1ProtoId].pRtChng =
                (VOID (*)(tNetIpv4RtInfo *pNetIpRtInfo, tNetIpv4RtInfo *pNetIpRtInfo1, UINT1 u1CmdType))
                pRegInfo->pRtChng;
        }
        if (pRegInfo->u2InfoMask & NETIPV4_PROTO_PKT_REQ)
        {
            gIpGlobalInfo.aHLProtoRegFn[pRegInfo->u1ProtoId].pProtoPktRecv =
                (VOID (*)(tIP_BUF_CHAIN_HEADER *, UINT2, UINT4, tIP_INTERFACE,
                          UINT1)) pRegInfo->pProtoPktRecv;
        }
    }
#if defined (VRRP_WANTED) && defined (NPAPI_WANTED)
    if (pRegInfo->u1ProtoId == VRRPMODULE)
    {
        FsNpIpv4VrrpInstallFilter ();
    }
#endif

    gIpGlobalInfo.aHLProtoRegFn[pRegInfo->u1ProtoId].u2RegCount++;

    IP_HLREG_UNLOCK ();

    IpActOnProtoRegDeregInCxt (pRegInfo->u4ContextId, pRegInfo->u1ProtoId);
    IPFWD_CXT_UNLOCK ();

    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetFirstFwdTableRouteEntry
 *
 * Description      :   This function provides the First Route Entry present in
 *                      the Ip Forwarding Table.
 *
 * Inputs           :   None.
 *
 * Outputs          :   pNetRtInfo - First Route Entry of the Ip Forwarding
 *                      Table.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetFirstFwdTableRouteEntry (tNetIpv4RtInfo * pNetRtInfo)
{
    INT4                i4RetVal = NETIPV4_FAILURE;

    i4RetVal = NetIpv4GetFirstFwdTableRouteEntryInCxt (IP_DEFAULT_CONTEXT,
                                                       pNetRtInfo);
    return i4RetVal;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetFirstFwdTableRouteEntryInCxt
 *
 * Description      :   This function provides the First Route Entry present in
 *                      the Ip Forwarding Table.
 *
 * Inputs           :   None.
 *
 * Outputs          :   pNetRtInfo - First Route Entry of the Ip Forwarding
 *                      Table.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetFirstFwdTableRouteEntryInCxt (UINT4 u4CxtId,
                                        tNetIpv4RtInfo * pNetRtInfo)
{

    tRtInfo             RtInfo;
    tRtInfo             NextRtInfo;
    INT4                i4RetVal = NETIPV4_FAILURE;

    if (pNetRtInfo == NULL)
    {
        return (NETIPV4_FAILURE);
    }

    MEMSET (&RtInfo, 0, sizeof (tRtInfo));
    MEMSET (&NextRtInfo, 0, sizeof (tRtInfo));

    /*  Calling IpGetNextRtEntry() with DestAddr and Mask as Zero
     *  returns the First Route Entry in the Ip Forwarding Table
     */

    i4RetVal = RtmNetIpv4GetNextBestRtEntryInCxt (u4CxtId, RtInfo, &NextRtInfo);
    if (i4RetVal == IP_SUCCESS)
    {
        pNetRtInfo->u4DestNet = NextRtInfo.u4DestNet;
        pNetRtInfo->u4DestMask = NextRtInfo.u4DestMask;
        pNetRtInfo->u4Tos = NextRtInfo.u4Tos;
        pNetRtInfo->u4NextHop = NextRtInfo.u4NextHop;
        pNetRtInfo->u4RtIfIndx = NextRtInfo.u4RtIfIndx;
        pNetRtInfo->u4RtAge = NextRtInfo.u4RtAge;
        pNetRtInfo->u4RtNxtHopAs = NextRtInfo.u4RtNxtHopAS;
        pNetRtInfo->i4Metric1 = NextRtInfo.i4Metric1;
        pNetRtInfo->u4RowStatus = NextRtInfo.u4RowStatus;
        pNetRtInfo->u2RtType = NextRtInfo.u2RtType;
        pNetRtInfo->u2RtProto = NextRtInfo.u2RtProto;
        pNetRtInfo->u4RouteTag = NextRtInfo.u4RouteTag;

        return (NETIPV4_SUCCESS);
    }
    return (NETIPV4_FAILURE);
}

 /******************************************************************************
 * Function Name    :   NetIpv4DeRegisterHigherLayerProtocol 
 *
 * Description      :   This function De-Registers the Protocol specified by
 *                      the Application from default context.
 *
 * Inputs           :   u1ProtoId - Protocol Identifier.
 *
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4DeRegisterHigherLayerProtocol (UINT1 u1ProtoId)
{
    return (NetIpv4DeRegisterHigherLayerProtocolInCxt (IP_DEFAULT_CONTEXT,
                                                       u1ProtoId));
}

 /******************************************************************************
 * Function Name    :   NetIpv4DeRegisterHigherLayerProtocolInCxt 
 *
 * Description      :   This function De-Registers the Protocol specified by
 *                      the Application from the specified context.
 *
 * Inputs           :   u1ProtoId - Protocol Identifier.
 *
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4DeRegisterHigherLayerProtocolInCxt (UINT4 u4ContextId, UINT1 u1ProtoId)
{

    tIpCxt             *pIpCxt = NULL;

    if (u1ProtoId >= IP_MAX_PROTOCOLS)
    {
        return (NETIPV4_FAILURE);
    }

    pIpCxt = UtilIpGetCxt (u4ContextId);
    IPFWD_CXT_LOCK ();
    if (NULL != pIpCxt)
    {

        if (pIpCxt->au1RegTable[u1ProtoId] == DEREGISTER)
        {
            IPFWD_CXT_UNLOCK ();
            return (NETIPV4_SUCCESS);
        }

        pIpCxt->au1RegTable[u1ProtoId] = DEREGISTER;
    }

    IP_HLREG_LOCK ();

    gIpGlobalInfo.aHLProtoRegFn[u1ProtoId].u2RegCount--;
    if (0 == gIpGlobalInfo.aHLProtoRegFn[u1ProtoId].u2RegCount)
    {
        /* Protocol from all the context have deregistered. So reset
         * the function pointers.
         */
        gIpGlobalInfo.aHLProtoRegFn[u1ProtoId].pIfStChng = NULL;
        gIpGlobalInfo.aHLProtoRegFn[u1ProtoId].pRtChng = NULL;
        gIpGlobalInfo.aHLProtoRegFn[u1ProtoId].pProtoPktRecv = NULL;
    }

#if defined (VRRP_WANTED) && defined (NPAPI_WANTED)
    if (u1ProtoId == VRRPMODULE)
    {
        FsNpIpv4VrrpRemoveFilter ();
    }
#endif

    IP_HLREG_UNLOCK ();

    IPFWD_CXT_UNLOCK ();
    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetNextFwdTableRouteEntry
 *
 * Description      :   This function returns the Next Route Entry in the Ip
 *                      Forwarding Table for a given Route Entry in a specific
 *                      context.
 *
 * Inputs           :   pNetRtInfo - Route Entry for which the Next Entry is
 *                      required.
 *
 * Outputs          :   pNextNetRtInfo - Next Route Entry in the Ip Forwarding
 *                      Table.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetNextFwdTableRouteEntry (tNetIpv4RtInfo * pNetRtInfo,
                                  tNetIpv4RtInfo * pNextNetRtInfo)
{

    tRtInfo             RtInfo;
    tRtInfo             NextRtInfo;
    INT4                i4RetVal = NETIPV4_FAILURE;
    UINT4               u4ContextId = 0;

    if ((pNetRtInfo == NULL) || (pNextNetRtInfo == NULL))
    {
        return (NETIPV4_FAILURE);
    }

    u4ContextId = pNetRtInfo->u4ContextId;
    MEMSET (&RtInfo, 0, sizeof (tRtInfo));
    MEMSET (&NextRtInfo, 0, sizeof (tRtInfo));

    RtInfo.u4DestNet = pNetRtInfo->u4DestNet;
    RtInfo.u4DestMask = pNetRtInfo->u4DestMask;
    RtInfo.u4Tos = pNetRtInfo->u4Tos;
    RtInfo.u4NextHop = pNetRtInfo->u4NextHop;
    RtInfo.u4RtIfIndx = pNetRtInfo->u4RtIfIndx;
    RtInfo.u4RtAge = pNetRtInfo->u4RtAge;
    RtInfo.u4RtNxtHopAS = pNetRtInfo->u4RtNxtHopAs;
    RtInfo.i4Metric1 = pNetRtInfo->i4Metric1;
    RtInfo.u4RowStatus = pNetRtInfo->u4RowStatus;
    RtInfo.u2RtType = pNetRtInfo->u2RtType;
    RtInfo.u2RtProto = pNetRtInfo->u2RtProto;
    RtInfo.u4RouteTag = pNetRtInfo->u4RouteTag;

    i4RetVal = RtmNetIpv4GetNextBestRtEntryInCxt (pNetRtInfo->u4ContextId,
                                                  RtInfo, &NextRtInfo);
    if (i4RetVal == IP_SUCCESS)
    {
        pNextNetRtInfo->u4DestNet = NextRtInfo.u4DestNet;
        pNextNetRtInfo->u4DestMask = NextRtInfo.u4DestMask;
        pNextNetRtInfo->u4Tos = NextRtInfo.u4Tos;
        pNextNetRtInfo->u4NextHop = NextRtInfo.u4NextHop;
        pNextNetRtInfo->u4RtIfIndx = NextRtInfo.u4RtIfIndx;
        pNextNetRtInfo->u4RtAge = NextRtInfo.u4RtAge;
        pNextNetRtInfo->u4RtNxtHopAs = NextRtInfo.u4RtNxtHopAS;
        pNextNetRtInfo->i4Metric1 = NextRtInfo.i4Metric1;
        pNextNetRtInfo->u4RowStatus = NextRtInfo.u4RowStatus;
        pNextNetRtInfo->u2RtType = NextRtInfo.u2RtType;
        pNextNetRtInfo->u2RtProto = NextRtInfo.u2RtProto;
        pNextNetRtInfo->u4RouteTag = NextRtInfo.u4RouteTag;
        pNextNetRtInfo->u4ContextId = u4ContextId;

        return (NETIPV4_SUCCESS);
    }
    return (NETIPV4_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIpForwarding
 *
 * Description      :   This function returns the IP forwarding flag
 *
 * Inputs           :   None 
 *
 * Outputs          :   pu1IpForward -  Global Ip Forwarding flag
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIpForwarding (UINT1 *pu1IpForward)
{
    return (NetIpv4GetIpForwardingInCxt (IP_DEFAULT_CONTEXT, pu1IpForward));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIpForwardingInCxt
 *
 * Description      :   This function returns the IP forwarding flag in the 
 *                      specified context
 *
 * Inputs           :   None 
 *
 * Outputs          :   pu1IpForward -  Ip Forwarding flag in the context
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIpForwardingInCxt (UINT4 u4ContextId, UINT1 *pu1IpForward)
{

    tIpCxt             *pIpCxt = NULL;

    IPFWD_CXT_LOCK ();

    pIpCxt = UtilIpGetCxt (u4ContextId);
    if (NULL == pIpCxt)
    {
        IPFWD_CXT_UNLOCK ();
        return NETIPV4_FAILURE;
    }

    *pu1IpForward = (UINT1) IP_CFG_GET_FORWARDING (pIpCxt);
    IPFWD_CXT_UNLOCK ();
    return (NETIPV4_SUCCESS);
}

/*******************************************************************************
 * Function Name    :   NetIpv4GetIfIndexFromName
 *
 * Description      :   This function returns the IfIndex for a given IfName.
 *
 * Inputs           :   pu1IfName  - IfName.
 *
 * Outputs          :   pu4IfIndex - IfIndex for the given IfName.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */

INT4
NetIpv4GetIfIndexFromName (UINT1 *pu1IfName, UINT4 *pu4IfIndex)
{
    return (NetIpv4GetIfIndexFromNameInCxt (IP_DEFAULT_CONTEXT, pu1IfName,
                                            pu4IfIndex));
}

/*******************************************************************************
 * Function Name    :   NetIpv4GetIfIndexFromNameInCxt
 *
 * Description      :   This function returns the IfIndex for a given IfName.
 *
 * Inputs           :   u4L2ContextId - Layer 2 context Id for the Interface
 *                  :   pu1IfName  - IfName.
 *
 * Outputs          :   pu4IfIndex - IfIndex for the given IfName.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */

INT4
NetIpv4GetIfIndexFromNameInCxt (UINT4 u4L2ContextId,
                                UINT1 *pu1IfName, UINT4 *pu4IfIndex)
{
    INT4                i4RetVal = OSIX_FAILURE;
    UINT2               u2Port = 0;
    UINT4               u4Index = 0;

    i4RetVal =
        (INT4) CfaGetInterfaceIndexFromNameInCxt (u4L2ContextId, pu1IfName,
                                                  &u4Index);

    if (i4RetVal == (INT4) OSIX_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    if (IpifGetPortFromIfIndex ((UINT2) u4Index, &u2Port) == IP_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    *pu4IfIndex = (UINT4) u2Port;
    return (NETIPV4_SUCCESS);

}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfCount
 *
 * Description      :   This function returns the number of active interfaces
 *
 * Inputs           :   None.
 *
 * Outputs          :   pu4IfCount - Number of interfaces.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */

INT4
NetIpv4GetIfCount (UINT4 *pu4IfCount)
{
    IpifGetIfaceEntryCount (pu4IfCount);
    return NETIPV4_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetHighestIpAddr
 *
 * Description      :   This function returns the highest IP Address among the
 *                      active V4 Interface address.
 *
 * Inputs           :   None.
 *
 * Outputs          :   pu4IfCount - Number of interfaces.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */

INT4
NetIpv4GetHighestIpAddr (UINT4 *pu4IpAddress)
{
    NetIpv4GetHighestIpAddrInCxt (IP_DEFAULT_CONTEXT, pu4IpAddress);
    return NETIPV4_SUCCESS;
}

/******************************************************************************
 * Function Name    :   NetIpv4GetHighestIpAddrInCxt
 *
 * Description      :   This function returns the highest IP Address among the
 *                      active V4 Interfaces mapped to the specified context
 *
 * Inputs           :   None.
 *
 * Outputs          :   u4ContextId - the context id
 *                      pu4IpAddress - The highest ip address in the context
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */

INT4
NetIpv4GetHighestIpAddrInCxt (UINT4 u4ContextId, UINT4 *pu4IpAddress)
{
    IpifGetHighIpAddressInCxt (u4ContextId, pu4IpAddress);
    return NETIPV4_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetRoute
 *
 * Description      :   This function provides the Route (either Exact Route or
 *                      Best route) for a given destination and Mask based on
 *                      the incoming request.
 *
 * Inputs           :   pRtQuery - Infomation about the route to be
 *                                 retrieved.
 *
 * Outputs          :   pNetIpRtInfo - Information about the requested route.
 *
 * Return Value     :   NETIPV4_SUCCESS - if the route is present.
 *                      NETIPV4_FAILURE - if the route is not present.
 *
 ******************************************************************************
 */

INT4
NetIpv4GetRoute (tRtInfoQueryMsg * pRtQuery, tNetIpv4RtInfo * pNetIpRtInfo)
{
    if ((pRtQuery == NULL) || (pNetIpRtInfo == NULL))
    {
        return (NETIPV4_FAILURE);
    }

    if (RtmNetIpv4GetRoute (pRtQuery, pNetIpRtInfo) == IP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetPortFromIfIndex
 * *                     Provides the IP Port Number Corresponding to IfIndex
 *
 * Input(s)           : u4IfIndex - CFA IfIndex
 * Output(s)          : IP Port Number.
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Provides the IP Port Number Corresponding to IfIndex
------------------------------------------------------------------- */
INT4
NetIpv4GetPortFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4IfIndex)
{
    UINT2               u2Port;
    if (IpGetPortFromIfIndex (u4IfIndex, &u2Port) == IP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    *pu4IfIndex = (UINT4) u2Port;
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IfIsOurAddress
 *                      Checks whether IP belongs to Local Interface mapped
 *                      to default context.                       
 *
 * Input(s)           : u4IpAddress - Ip Address
 * Output(s)          : None.
                                                                                                                             
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4IfIsOurAddress (UINT4 u4IpAddress)
{
    return (NetIpv4IfIsOurAddressInCxt (IP_DEFAULT_CONTEXT, u4IpAddress));
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IfIsOurAddressInCxt
 *                      Checks whether IP belongs to Local Interface mapped to 
 *                      the specified contexts
 *
 * Input(s)           : u4ContextId - Context id
 *                      u4IpAddress - Ip Address
 * Output(s)          : None.
                                                                                                                             
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface mapped to the
 *                      specified context
------------------------------------------------------------------- */
INT4
NetIpv4IfIsOurAddressInCxt (UINT4 u4ContextId, UINT4 u4IpAddress)
{
    if (IpifIsOurAddressInCxt (u4ContextId, u4IpAddress) == FALSE)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IsLoopbackAddress
 *                      Checks the IP Address is a Loopback Address
 *
 * Input(s)           : u4IpAddress - Ip Address
 * Output(s)          : None.
 *
 * Returns            : NETIPV4_SUCCESS - If u4IpAddress is Loopback
 *                                        address
 *                      NETIPV4_FAILURE - If u4IpAddress is not
 *                                        Loopback address
 * Action             : Checks the IP Address is a Loopback Address
------------------------------------------------------------------- */
INT4
NetIpv4IsLoopbackAddress (UINT4 u4IpAddress)
{
    UINT4               u4Port = 0;
    UINT4               u4CfaIfIndex = 0;
    tCfaIfInfo          CfaIfInfo;

    MEMSET ((UINT1 *) &CfaIfInfo, 0, sizeof (CfaIfInfo));

    if (NetIpv4GetIfIndexFromAddr (u4IpAddress, &u4Port) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    if (NetIpv4GetCfaIfIndexFromPort (u4Port, &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetCfaIfIndexFromPort
 * *                   Provides the CFA IfIndex Corresponding to IP Port No
 *
 * Input(s)           : u4Port - IP Port Number
 * Output(s)          : CFA Interface Index
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Provides CFA IFIndex Corresponding to IP Port No
------------------------------------------------------------------- */
INT4
NetIpv4GetCfaIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    *pu4IfIndex = IpGetIfIndexFromPort ((UINT2) u4Port);

    if (*pu4IfIndex == 0)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetSrcAddrToUseForDest
 * *                     Provides the Source address for a given
 *                       destination address in the default context
 *
 * Input(s)           : u4Dest - destination address
 * Output(s)          : *4pSrc_addr_to_use
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             :  Provides the Source address for a given
 *                       destination address
------------------------------------------------------------------- */

INT4
NetIpv4GetSrcAddrToUseForDest (UINT4 u4Dest, UINT4 *u4pSrc_addr_to_use)
{
    return (NetIpv4GetSrcAddrToUseForDestInCxt (IP_DEFAULT_CONTEXT, u4Dest,
                                                u4pSrc_addr_to_use));
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetSrcAddrToUseForDestInCxt
 * *                     Provides the Source address for a given
 *                       destination address in the specified context.
 *
 * Input(s)           :  u4ContextId - the context id
 *                       u4Dest - destination address
 * Output(s)          : *4pSrc_addr_to_use
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             :  Provides the Source address for a given
 *                       destination address in the specified context
------------------------------------------------------------------- */

INT4
NetIpv4GetSrcAddrToUseForDestInCxt (UINT4 u4ContextId, UINT4 u4Dest,
                                    UINT4 *u4pSrc_addr_to_use)
{
    if (ip_src_addr_to_use_for_dest_InCxt (u4ContextId, u4Dest,
                                           u4pSrc_addr_to_use) == IP_SUCCESS)
    {
        return NETIPV4_SUCCESS;
    }
    return NETIPV4_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IpIsLocalNet
 * *                     Checks whether IP belongs to Local Network
 *
 * Input(s)           : u4IpAddress - Ip Address
 * Output(s)          : None.

 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4IpIsLocalNet (UINT4 u4IpAddress)
{
    return (NetIpv4IpIsLocalNetInCxt (IP_DEFAULT_CONTEXT, u4IpAddress));
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IpIsLocalNetInCxt
 *                      Checks whether IP belongs to Local Network in the
 *                      the specified context.
 * 
 * Input(s)           : u4IpAddress - Ip Address
 * Output(s)          : None.

 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface mapped to 
 *                      the specified context.
------------------------------------------------------------------- */
INT4
NetIpv4IpIsLocalNetInCxt (UINT4 u4ContextId, UINT4 u4IpAddress)
{
    if (IpIsLocalNetInCxt (u4ContextId, u4IpAddress) == IP_SUCCESS)
    {
        return NETIPV4_SUCCESS;
    }
    return NETIPV4_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IpIsLocalNetOnInterface
 *                      Checks whether IP belongs to Local Network in the
 *                      the specified interface.
 * 
 * Input(s)           : u4IfIndex - CFA Index
 * Output(s)          : u4IpAddress - Ip Address

 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Network in
 *                      the specified interface.
------------------------------------------------------------------- */
INT4
NetIpv4IpIsLocalNetOnInterface (UINT4 u4IfIndex, UINT4 u4IpAddress)
{
    IPFWD_PROT_LOCK ();

    if (IpIsLocalNetOnInterface (u4IfIndex, u4IpAddress) == IP_SUCCESS)
    {
        IPFWD_PROT_UNLOCK ();
        return NETIPV4_SUCCESS;
    }
    IPFWD_PROT_UNLOCK ();
    return NETIPV4_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           :  NetIpv4GetNextSecondaryAddress
 *                     
 *Description         : Get the next secondary ip address over the interface
                        if the given ip adddress is primary ip address,firsr
                        secondary ip address is returned from the function
 *
 * Input(s)           : u2Port      - Ip Interface Index 
 *                      u4IpAddress - Ip Address
 *
 * Output(s)          : *pu4NextIpAddr - Next Secondary address
 *                      *pu4NetMask    - Mask associated with the secondary
 *                                       address
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4GetNextSecondaryAddress (UINT2 u2Port, UINT4 u4IpAddr,
                                UINT4 *pu4NextIpAddr, UINT4 *pu4NetMask)
{
    if (IpIfGetNextSecondaryAddress (u2Port, u4IpAddr,
                                     pu4NextIpAddr, pu4NetMask) == IP_SUCCESS)
    {
        return NETIPV4_SUCCESS;
    }
    return NETIPV4_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           :  NetIpv4GetCxtId
 *                     
 *Description         : This functions gets the context id associated with the 
                        the given IP Interface Index
 *
 * Input(s)           : u4IfIndex     - IP Port Number 
 *
 * Output(s)          : *pu4CxtId - Context id of the interface
 *                                   having port number as u4IfIndex
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : This fucntion calls VcmGetContextInfoFromIpIfIndex
 *                      to get the context id associated with u4IfIndex 
------------------------------------------------------------------- */
INT4
NetIpv4GetCxtId (UINT4 u4IfIndex, UINT4 *pu4CxtId)
{
    if ((VcmGetContextInfoFromIpIfIndex (u4IfIndex, pu4CxtId)) == VCM_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           :  IpHandlePathStatusChange
 *
 *Description         : This functions gets the IpNbrInfo from BFD 
 *                      inorder to clear the route entry from the table 
 *                      and clears the arp cache.
 *
 * Input(s)           : u4ContextId - Context id of the interface
 *                      pIpNbrInfo  - Next hop details to clear 
 *                                    route and arp cache.
 * Output(s)          : NONE
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */

INT1
IpHandlePathStatusChange (UINT4 u4ContextId, tClientNbrIpPathInfo * pIpNbrInfo)
{
    UINT4               u4NextHopAddr = 0;
    UINT1               u1Operation = ARP_DELETION;
    UINT4               u4NetMask;
    UINT4               u4Port = 0;
    tArpQMsg            ArpQMsg;
    tNetIpv4IfInfo      NetIfInfo;
    INT1                i1flag = ARP_STATIC_NOT_IN_SERVICE;
    /*Statically configured 
       roure is not in use */

    if (pIpNbrInfo == NULL)
    {
        return NETIPV4_FAILURE;
    }

    MEMCPY (&u4NextHopAddr, &pIpNbrInfo->NbrAddr, sizeof (u4NextHopAddr));
    u4NextHopAddr = OSIX_HTONL (u4NextHopAddr);
    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
    MEMSET ((UINT1 *) &NetIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetPortFromIfIndex (pIpNbrInfo->u4IfIndex, &u4Port) !=
        NETIPV4_SUCCESS)
    {
        return NETIPV4_FAILURE;
    }
    /* Get Netmask from IfIndex */
    if (NetIpv4GetIfInfo (u4Port, &NetIfInfo) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    else
    {
        u4NetMask = NetIfInfo.u4NetMask;
    }

    /* Informing Rtm module to delete ARP
     * entry that is previously learned through the gateway
     */
    if (RtmApiHandlePRTRoutesForArpUpdationInCxt (u4ContextId,
                                                  u4NextHopAddr, u1Operation,
                                                  i1flag) == IP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    /* Enque the Nexthop address to ARP que to delete ARP cache */
    ArpQMsg.u4MsgType = ARP_STATIC_RT_CHANGE;
    ArpQMsg.u2Port = (UINT2) u4Port;
    ArpQMsg.u4IpAddr = u4NextHopAddr;
    ArpQMsg.u4NetMask = u4NetMask;
    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

#ifdef VRRP_WANTED
/*-------------------------------------------------------------------+
 * Function           : NetIpv4CreateVrrpInterface
 *                     
 * Description        : Enables VRRP capability for the IP interface or
 *                      adds address as virtual address to the IP
 *                      interface.
 *
 * Input(s)           : pVrrpNwIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : None
 *
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv4CreateVrrpInterface (tVrrpNwIntf * pVrrpNwIntf)
{
    UINT4               u4IpAddr = 0;
    UINT4               u4SecondaryIpAddr = 0;
    UINT4               u4NetMask = 0xffffffff;

    MEMCPY ((UINT1 *) &u4SecondaryIpAddr, pVrrpNwIntf->IpvXAddr.au1Addr,
            IPVX_IPV4_ADDR_LEN);
    u4SecondaryIpAddr = OSIX_NTOHL (u4SecondaryIpAddr);

    MEMCPY ((UINT1 *) &u4IpAddr, pVrrpNwIntf->IntfAddr.au1Addr,
            IPVX_IPV4_ADDR_LEN);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_SECONDARY_CREATE)
    {
        if (IpIfSetSecondaryAddressInfo (pVrrpNwIntf->u4Port,
                                         u4SecondaryIpAddr, u4NetMask,
                                         CREATE_AND_GO) == IP_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_CREATE)
    {

#ifdef NPAPI_WANTED
        if (IpFsNpVrrpHwProgram (VRRP_NP_CREATE_INTERFACE,
                                 pVrrpNwIntf) == FNP_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
#endif
    }
    else
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4DeleteVrrpInterface
 *                     
 * Description        : Disables VRRP capability for the IP interface or
 *                      deletes virtual address from the IP interface.
 *
 * Input(s)           : pVrrpNwIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : None
 *
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv4DeleteVrrpInterface (tVrrpNwIntf * pVrrpNwIntf)
{
    UINT4               u4IpAddr = 0;
    UINT4               u4SecondaryIpAddr = 0;
    UINT4               u4NetMask = 0xffffffff;

    MEMCPY ((UINT1 *) &u4SecondaryIpAddr, pVrrpNwIntf->IpvXAddr.au1Addr,
            IPVX_IPV4_ADDR_LEN);
    u4SecondaryIpAddr = OSIX_NTOHL (u4SecondaryIpAddr);

    MEMCPY ((UINT1 *) &u4IpAddr, pVrrpNwIntf->IntfAddr.au1Addr,
            IPVX_IPV4_ADDR_LEN);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_SECONDARY_DELETE)
    {
        if (IpIfSetSecondaryAddressInfo (pVrrpNwIntf->u4Port,
                                         u4SecondaryIpAddr, u4NetMask,
                                         DESTROY) == IP_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_DELETE)
    {
#ifdef NPAPI_WANTED
        if (IpFsNpVrrpHwProgram (VRRP_NP_DELETE_INTERFACE,
                                 pVrrpNwIntf) == FNP_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
#endif
    }
    else
    {
        return NETIPV4_FAILURE;
    }

    return (NETIPV4_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetVrrpInterface
 *            
 * Description        : Retrieves VRRP information corresponding to
 *                      IP interface from NP.
 *
 *                      This NetIp API also handles the below actions
 *                            1. To Change Network action.
 *                            2. To Check Primary IP against Associated
 *                               IP.
 *                      The above actions are only applicable for
 *                      Linux IP.
 *
 * Input(s)           : pVrrpNwInIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : pVrrpNwOutIntf   - Pointer to VRRP Interface
 *                                       Information.
 *
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv4GetVrrpInterface (tVrrpNwIntf * pVrrpNwInIntf,
                         tVrrpNwIntf * pVrrpNwOutIntf)
{

    if ((pVrrpNwInIntf->b1IsChgNwActionReqd == TRUE) ||
        (pVrrpNwInIntf->b1IsCheckIpReqd == TRUE))
    {
        return NETIPV4_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (IpFsNpVrrpHwProgram (VRRP_NP_GET_INTERFACE,
                             pVrrpNwInIntf) == FNP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    MEMCPY (pVrrpNwOutIntf, pVrrpNwInIntf, sizeof (tVrrpNwIntf));
#else
    UNUSED_PARAM (pVrrpNwInIntf);
    UNUSED_PARAM (pVrrpNwOutIntf);
#endif

    return NETIPV4_SUCCESS;
}
#endif
