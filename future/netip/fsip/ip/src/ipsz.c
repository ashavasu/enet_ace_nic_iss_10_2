/* $Id: ipsz.c,v 1.5 2013/11/29 11:04:14 siva Exp $*/
#define _IPSZ_C
#include "ipinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
IpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsIPSizingParams[i4SizingId].u4StructSize,
                                     FsIPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(IPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            IpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
IpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsIPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, IPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
IpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < IP_MAX_SIZING_ID; i4SizingId++)
    {
        if (IPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (IPMemPoolIds[i4SizingId]);
            IPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
