/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ippmtud.h,v 1.5 2011/10/25 09:53:31 siva Exp $
 *
 * Description: Data structures used for IP PMTU
 *
 *******************************************************************/

/***************** TYPE DEFINITIONS ****************/

/* PMTU structure for Hash Table */
typedef struct tPmtuInfo
{
    tRBNode         PmtuEntryRBNode;
    tOsixSysTime    PmtuTstamp;       /* Timestamp for deleting non-active 
                                       * entries 
                                       */
    tIpCxt         *pIpCxt;           /* Pointer to ip context strucutre */
    UINT4           u4PmtuDest;       /* Destination IP address of the path */
    UINT4           u4PMtu;           /* MTU of the path */
    UINT1           u1PmtuDiscFlag;   /* Enable / Disable PMTU for this entry */
    UINT1           u1PmtuRowStatus;  /* Rowstatus for creation of the row */
    UINT1           u1PmtuTos;        /* IP Type Of Service */
    UINT1           u1PmtuAge;        /* Time counter for detecting PMTU 
                                       * increase 
                                       */ 
} tPmtuInfo;

/***************************************************/

/*************** CONSTANT DEFINITIONS **************/

#define   PMTU_MAX_DEF_ENTRIES        10
#define   PMTU_MIN_MTU                68
#define   PMTU_MAX_MTU             65535
#define   PMTU_DEF_AGE               600    /* 10 minutes */
#define   PMTU_SCAN_INTVL             60    /* 1 minute */
#define   PMTU_GRBG_CLXN_INTVL      7200    /* 2 Hrs */
#define   PMTU_INFINITY             0xff
#define   PMTU_ENABLED                 1
#define   PMTU_DISABLED                2
#define   PMTU_GRBG_TIMER              1
#define   PMTU_AGEING_TIMER            2

#define   PMTU_TABLE_MAX_SIZE       65535   
#define   PMTU_TABLE_MIN_SIZE          1   

#define   PMTU_ACTIVE                  1
#define   PMTU_NOT_IN_SERVICE          2
#define   PMTU_NOT_READY               3
#define   PMTU_CREATE_N_GO             4
#define   PMTU_CREATE_N_WAIT           5
#define   PMTU_DESTROY                 6

#define   IP_PMTU_PLATEAU_SIZE         9

#define   PMTU_MIN_ENTRY_AGE           5
#define   PMTU_MAX_ENTRY_AGE           255

/***************************************************/

/***************** MACRO DEFINITIONS ***************/
#define   PMTU_GARBAGE_TIMER         gIpGlobalInfo.PmtuGarbageTimer.Timer_node 
#define   PMTU_INCR_TIMER            gIpGlobalInfo.PmtuAgeingTimer.Timer_node  

/***************************************************
               Function Prototypes
***************************************************/

tPmtuInfo *  PmtuLookupEntryInCxt PROTO ((tIpCxt *pIpCxt, UINT4, UINT1));
VOID PmtuTimerExpiryHandler PROTO ((t_IP_TIMER *));
INT4 PmtuInitInCxt PROTO ((tIpCxt *pIpCxt));
INT4 PmtuShutInCxt PROTO ((tIpCxt *pIpCxt));
VOID PmtuDeleteEntry PROTO ((tPmtuInfo *));
INT4 PmtuRBTreeCmpFn PROTO ((tRBElem *pElem1, tRBElem *pElem2));
UINT2 PmtuEstimateMtu PROTO ((UINT2));


