/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipfrgtyp.h,v 1.3 2009/08/24 13:11:37 prabuc Exp $
 *
 * Description:All structure, macro and constant definitions
 *             used IP fragmentation                        
 *                               
 *******************************************************************/
#ifndef   __IP_IPFRAG_H__
#define   __IP_IPFRAG_H__

#include "iptmrdfs.h"

#define   IP_MIN_REASM_TIME    30
#define   IP_DEF_REASM_TIME    30
#define   IP_MAX_REASM_TIME    50

/*
 * Reassembly queue contains information regarding all datagrams which
 * are undergoing reassembly process.  It is arrenged as a linked list
 * in which each entry corresponds to one datagram.  Each entry will have
 * a pointer to fragment list belonging to that datagram.  The reassmbly
 * queue entry is freed, once the entire datagram is reconstructed.
 */

typedef struct
{
    tIP_DLL_NODE  Link;
    t_IP_TIMER    Timer;            /* To implement reassembly timeout */
    tIP_DLL       Frag_list;        /* List of fragments collected so far */
    tIpCxt       *pIpCxt;           /* Pointer to ip context structure */
    UINT4         u4Src;            /* These fields uniquely identify a 
                                     * datagram 
                                     */
    UINT4         u4Dest;
    UINT1         u1Proto;
    UINT1         u1AlignmentByte;  /* Byte for word alignment */
    UINT2         u2AlignmentByte;  /* Byte for word alignment */
    UINT2         u2Id;
    UINT2         u2Len;            /* Entire datagram length, if known */
} t_IP_REASM;

/*
 * A linked list of fragments is maintained with each of the reassembly
 * queues during the process of reassembly. Once the list is complete
 * i.e. all the fragments are received, the queue entry is freed.
 */
typedef struct
{
    tIP_DLL_NODE          Link;
                          
    tIP_BUF_CHAIN_HEADER  *pBuf;           /* Data buffer associated with this 
                                            * fragment 
                                            */
    UINT2                 u2Start_offset;  /* Begining and end offsets of this
                                            * fragment 
                                            */
    UINT2                 u2End_offset;
} t_IP_FRAG;

/* Used by fragment reassembly scheme */
#define   IP_FRAG_INSERT     0
#define   IP_FRAG_APPEND     1
#define   IP_FRAG_PREPEND    2

/*
 * Max. Number of Datagrams that are allowed to undergo reassembly.
 */
#define   IP_MAX_FRAGMENTS_IN_DATAGRAM       30

#define   IP_COPY_OPTION_TO_FRAG(u1Code)    (u1Code & 0x80)

#define IP_MAX_BYTES(NoBytes, MAX) (((NoBytes) <= (MAX))?(NoBytes):(MAX))
#endif            /*__IP_IPFRAG_H__*/
