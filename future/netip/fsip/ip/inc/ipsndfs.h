/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipsndfs.h,v 1.3 2015/06/25 12:15:56 siva Exp $
 *
 * Description: IP Management related constants
 *
 *******************************************************************/
#ifndef   __IP_IPMGMT_H__
#define   __IP_IPMGMT_H__

/*
 * This information is shared between routing protocol and IP.
 * When there is a request for some entry in dynamic route, a single
 * procedure is called and the variable index is passed as argument.
 */
#define   IP_MGMT_RT_GET_PORT_IF_INDEX    1    /* Interface index  */
#define   IP_MGMT_RT_GET_PORT_SUBREF      2    /* Subreferenece    */
#define   IP_MGMT_RT_GET_GW               3    /* Next hop address */
#define   IP_MGMT_RT_GET_METRIC           4    /* Primary metric   */
#define   IP_MGMT_RT_GET_TYPE             5    /* Type of the route */
#define   IP_MGMT_RT_GET_PROTO            6    /* Route protocol   */
#define   IP_MGMT_RT_GET_TIME             7    /* Time since change */

/*
 * These values confirm to the definition of MIB2 Route Table of 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * These are the values taken by the Type field of t_ST_RT_INFO
 */
#define   IP_STATIC_INVALID         0x10

#define   IP_STATIC_MIN_METRIC1        1
#define   IP_STATIC_MAX_METRIC1       15

#define   IP_STAT_UP                   1
#define   IP_STAT_DOWN                 2
#define   IP_STAT_DOWN_ALL_IFACE       3

    /*
     * This information is shared between routing protocol and IP and
     * corresponds to the ipRouteTable defined in MIB-2.
     * When there is a request for some entry in dynamic route, a single
     * procedure is called and the variable index is passed as argument.
     */

#define   IP_MGMT_RT_IF_INDEX     1
#define   IP_MGMT_RT_METRIC1      2
#define   IP_MGMT_RT_METRIC2      3
#define   IP_MGMT_RT_METRIC3      4
#define   IP_MGMT_RT_METRIC4      5
#define   IP_MGMT_RT_GW           6
#define   IP_MGMT_RT_TYPE         7    /* Primary metric   */
#define   IP_MGMT_RT_TIME         8    /* Type of the route */
#define   IP_MGMT_RT_MASK         9    /* Route protocol   */
#define   IP_MGMT_RT_METRIC5     10


#define  IP_QUE_DEF_PKTS_PER_EVENT   100

#endif            /*__IP_IPMGMT_H__*/
