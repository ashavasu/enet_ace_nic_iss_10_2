/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipprotos.h,v 1.31 2016/03/04 11:04:31 siva Exp $
 *
 * Description: Contains Prototypes required for IP/RIP module.
 *
 *******************************************************************/
#include "udppdudf.h"
#include "ipprmdfs.h"
#include "ipcfaif.h"

#ifdef NPAPI_WANTED
#include "ipnp.h"
#endif

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/
INT4 ip_send_InCxt PROTO ((tIpCxt *pIpCxt, UINT4 u4Src, UINT4 u4Dest,
                     UINT1 u1Proto, UINT1 u1Tos,
                     UINT1 u1Ttl,
                     tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT2 u2Len, UINT2 u2Id,
                     UINT1 u1Df, UINT2 u2Olen, UINT2 u2Rt_port));
INT4 IpSendMcastPktInCxt PROTO ((tIpCxt *pIpCxt, tIP_BUF_CHAIN_HEADER * pBuf, 
                                 tMcastIpSendParams * pIpMcastParams));
INT4 ip_validate_net_mask PROTO ((UINT4 u4Addr, UINT4 u4Value));
INT4 ip_validate_bcast_addr PROTO ((UINT4 u4Addr, UINT4 u4Value));
tCRU_BUF_CHAIN_HEADER *CRU_BUF_UnLink_BufChains
PROTO ((tCRU_BUF_CHAIN_HEADER * p_msg1));
INT4 IP_Are_Interface_IDs_Equal
PROTO ((tCRU_INTERFACE * pIfId1, tCRU_INTERFACE * pIfId2));
UINT1 ip_reverse_src_route_option
PROTO ((UINT1 * pu1_DstOpt, UINT1 * pu1_SrcOpt, UINT4 u4Src, UINT4 * pu4Dest));
INT4 IpDuplicateBuffer PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                        tCRU_BUF_CHAIN_HEADER ** pDupBuf, UINT4 u4PktSize));


#ifdef __IP_IPREG_H__

/*  ipreg.c   */

VOID IpInitRegTable PROTO ((VOID));
VOID IpInitMCastRegTable PROTO ((VOID));
INT1 IpRegStatus PROTO ((UINT1 u1ProtocolId,
                         FUNCPTR * pAppRcv,
                         FUNCPTR * pIfStChg, FUNCPTR * pRtChg));
VOID IpIfStChgNotify PROTO ((UINT4 u4Port, UINT4 u4BitMap));

VOID IpActOnProtoRegDeregInCxt PROTO ((UINT4 u4ContextId, UINT1 u1ProtocolId));

VOID IpIfStateChngNotify (UINT2 u2Port, UINT4 u4BitMap);
VOID IpIfSecAddrChange (tIfaceQMsg * pIfaceQMsg, UINT4 u4BitMap);
INT4 IpRegisterWithVcm (VOID);
VOID IpCxtChngHdlr (UINT4, UINT4, UINT1);

#endif

#ifdef __IP_IP_H__
/*  ipmcast.c  */
INT1 IpInputPrcsMcastPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                           t_IP * pIp, INT1 i1Flag, UINT2 u2Port));
#endif

/* iphlif.c */

#ifdef    __IP_IP_H__

INT4 IpDeliverHigherLayerProtocol PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                          t_IP * pIp,
                                          UINT1 u1Flag, UINT2 u2Port));

VOID  IpHandleUnKnownProtosInCxt (tIpCxt *pIpCxt, tIP_BUF_CHAIN_HEADER *pBuf, 
                                  UINT2 u2Port, INT1  i1Flag);
INT4 ip_process_options_InCxt PROTO ((tIpCxt *pIpCxt, t_IP * pIp, 
                                      INT1 * i1For_this_node,
                                tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT2 * u2pRt_port, UINT4 * u4pRt_dest));

#endif              /*__IP_IP_H__*/

VOID IP_FWD_Process_Timer_Expiry PROTO ((VOID));
VOID IPProcessMsgQue PROTO ((VOID));
VOID IP_FWD_Process_Pkt_Arrival_Event PROTO ((VOID));

INT4 IpHandleIfaceMapping PROTO ((UINT4, UINT2));

#ifdef  __IP_IP_H__



UINT2 ip_update_cksum PROTO ((t_IP * pIp, UINT4 u4Value));

#endif              /*__IP_IP_H__*/

VOID iptask_icmp_mask_reply_handler_InCxt PROTO ((tIpCxt *pIpCxt, 
                                                  tCRU_BUF_CHAIN_HEADER * pBuf,
                                                  UINT4 u4Ip_src,
                                                  UINT4 u4Ip_dest, INT2 i2Icmp_Id,
                                                  INT2 i2Seq));
INT4 iptask_icmp_tcp_in_cxt PROTO ((UINT4 u4Context, tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4Src, INT1 i1Type, INT1 i1Code));

INT4 ip_mem_failure PROTO ((VOID));

INT4 IpTxDatagram PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                          UINT2 u2Port, UINT4 u4Dest,
                          UINT1 u1TxType, UINT2 u2Len));
#ifdef TUNNEL_WANTED
INT4 Ip4TunlSend PROTO ((t_IP_HEADER * pIpHdr, tCRU_BUF_CHAIN_HEADER * pBuf, 
                         UINT2 u2Port));
#endif

/************* E X T E R N   D E C L A R A T I O N S  ************/


#ifdef  __IP_IP_H__
tCRU_BUF_CHAIN_HEADER *ipReassemble PROTO ((t_IP * pIp,
                                            tCRU_BUF_CHAIN_HEADER * pBuf,
                                            UINT2 u2Port));
INT4 ip_fragment_and_send PROTO ((UINT2 u2Port, t_IP * pIp,
                                  tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 u4Gw, UINT1 u1Flag,UINT4 u4MTU));
#endif              /*__IP_IP_H__*/

#ifdef  __IP_IPFRAG_H__
VOID ip_free_reasm PROTO ((t_IP_REASM * pReasm));
#endif              /*__IP_IPFRAG_H__*/

INT4 ipfrag_init_InCxt PROTO ((tIpCxt *pIpCxt));
INT4 fs_ip_init_InCxt PROTO ((tIpCxt *pIpCxt));

#ifdef   __IP_IPTIMER_H__
VOID ip_handle_reasm_timer_expiry PROTO ((t_IP_TIMER * pTimer));
#endif              /*__IP_IPTIMER_H__*/

INT4 ipResolveDestEnetAddr PROTO ((UINT4, UINT1, UINT4, UINT1 *, UINT1 *, 
                                   UINT1 *));

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/


INT4 ip_filt_mgmt_list_test PROTO ((INT4 i4List,
                                    UINT4 u4Src, UINT4 u4Dest, INT4 i4Value));


INT4 ip_filt_mgmt_basic_tests PROTO ((INT4 i4List, UINT4 u4Src, UINT4 u4Dest));

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/

UINT1 IpifGetInterfaceType PROTO ((UINT2 u2Port));
VOID ipif_init PROTO ((VOID));
INT4 ipif_get_handle_from_addr_InCxt PROTO ((UINT4 u4ContextId, UINT4 u4Addr));
INT4 ipif_is_ok_to_forward PROTO ((UINT2 u2Port));
INT4 ipif_is_our_address PROTO ((UINT4 u4Addr));
INT4 ipif_is_our_address_InCxt PROTO ((UINT4 u4ContextId, UINT4 u4Addr));
VOID ipifConvertIpIfToCRU PROTO ((INT4, INT4, tCRU_INTERFACE *));
INT4 ipifConvertIfIdFromCRU PROTO ((tCRU_INTERFACE));
INT4 ipifConvertSubrefFromCRU PROTO ((tCRU_INTERFACE));

UINT4 ipif_mgmt_iftab_get_entry PROTO ((tCRU_INTERFACE InterfaceId));

INT1 nmhValidateIndexInstanceIpIfTable PROTO ((INT4, INT4));
INT1 nmhGetFirstIndexIpIfTable PROTO ((INT4 *, INT4 *));
INT1 nmhGetNextIndexIpIfTable PROTO ((INT4, INT4 *, INT4, INT4 *));


/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/

VOID IpGetMaskFromAddr PROTO ((UINT4 u4DestNet, UINT4 *pu4DestMask));

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/

#ifdef  __IP_TRACE_H__

INT4 trace_send PROTO ((t_TRACE * pTrace));
INT4 trace_end_timeout PROTO ((t_TRACE * pTrace));

#endif              /*__IP_TRACE_H__*/

VOID trace_init_rec PROTO ((VOID));

INT4 trace_input PROTO ((UINT2 u2Port,
                         tCRU_BUF_CHAIN_HEADER * pBuf,
                         INT2 i2Len, UINT4 u4IpAddr, UINT2 u2Iden));
INT4 trace_error PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                         INT1 i1Type, INT1 i1Code, UINT4 u4IpAddr));

INT4 trace_info_get_next_index PROTO ((UINT4 u4Dest, INT2 i2Hop,
                                       UINT4 * u4pDest_ret, INT2 * i2pHop_ret));

/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/
/*************** IP forward ***************************************/
/*  ipoutput.c */

#ifdef __IP_IP_H__
INT4 IpOutputProcessPktInCxt PROTO ((tIpCxt *pIpCxt, tCRU_BUF_CHAIN_HEADER * pBuf,
                                t_IP * pIp, UINT2 u2Rt_port));

INT4 IpOutputForwardInCxt PROTO ((tIpCxt *pIpCxt, tCRU_BUF_CHAIN_HEADER * pBuf, 
                                  t_IP * pIp));

INT4 IpOutputMcastForward PROTO ((UINT2 u2Rt_port,
                                  tCRU_BUF_CHAIN_HEADER * pBuf, t_IP * pIp));

INT4 IpOutputBcastForwardInCxt PROTO ((tIpCxt *pIpCxt, UINT2 u2Rt_port,
                                  tCRU_BUF_CHAIN_HEADER * pBuf, t_IP * pIp));

INT4 IpOutput PROTO ((UINT2 u2Rt_port,
                      UINT4 u4Rt_gw,
                      t_IP * pIp, tCRU_BUF_CHAIN_HEADER * pBuf, INT1 i1Flag));

/* ipinput.c */
INT4 IpInputProcessPkt PROTO ((UINT2 u2Port, tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 IpVerifyGetAddrTypeInCxt PROTO ((UINT4 u4ContextId, t_IP * pIp, 
                                      UINT1 u1Flag));

INT4 IpInputHeaderValidateExtract PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                          t_IP * pIp, UINT2 u2Port));

/* ipfwdutl.c*/
INT4 IpVerifyTtl PROTO ((t_IP * pIp,
                         INT1 i1Flag,
                         UINT2 u2Port, tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 IpForwardCheckInterface PROTO ((UINT2 u2Rt_port,
                                     INT1 i1Flag,
                                     tCRU_BUF_CHAIN_HEADER * pBuf));
UINT2 IpCalcCheckSum ARG_LIST ((tIpBuf * pBuf, UINT4 u4Size, UINT4 u4Offset));

INT4 IpReleaseProtInfo ARG_LIST ((VOID * pProtInfo));
/* ipfwd.c */
INT4 IpForward PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                       t_IP * pIp, INT1 i1Flag,
                       UINT2 u2Rt_port, UINT4 u4Rt_gw, UINT2 u2Port));

/* Initialisation funtion of a Test application - ECHO_CLIENT */

INT4 IpForwardBcast PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                            t_IP * pIp, UINT2 u2Port));

INT4 IpCfgGetNumMultipathInCxt PROTO((tIpCxt *pIpCxt));

#endif

#ifdef __IP_IPIF_H__
INT4 IpCompareBcastAddr PROTO ((UINT4 u4Addr, 
                                UINT4 u4IpAddr, UINT4 u4BcastAddress));

UINT4
ipif_get_tab_index (tIP_INTERFACE InterfaceId);
#endif

INT4 IpAggrRtInitInCxt PROTO ((UINT4 u4ContextId));
VOID IpCidrCfgInitInCxt PROTO ((tIpCxt *pIpCxt));

INT4 IpCidrCompareIndics PROTO ((UINT4 u4Addr1,
                                 UINT4 u4Mask1, UINT4 u4Addr2, UINT4 u4Mask2));

/*   start of RARP Module mappings */
INT1      RarpSendPktBelow
PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, UINT1 * pHwAddr));

INT1 GetProtocolAddress PROTO ((UINT1 * pHwAddr, UINT4 * pAddr, UINT1 u1Len));

#ifdef _rarptdfs_h

INT4 RarpExtractHeader PROTO ((tRarpPacket *, tCRU_BUF_CHAIN_HEADER *));
VOID RarpPutHeader PROTO ((tRarpPacket *, tCRU_BUF_CHAIN_HEADER *));
INT1 RarpServerHandler PROTO ((tRarpPacket *, UINT2));
INT1 RarpClientHandler PROTO ((tRarpPacket *, UINT2));
tServerAddrNode *CheckThisIndex PROTO ((tSNMP_OCTET_STRING_TYPE *));
#endif /*  rarptdfs */

/* Prototypes added for system resizing */

VOID      IpInitGblResizingParameters (VOID);

INT4      IpHandlePktFromCfa
PROTO ((tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port, UINT1 u1LinkType));
INT4
IpRegisterNetworkInterface PROTO ((tIfaceQMsg *pIfaceQMsg));

VOID IpDeRegisterNetworkInterface PROTO ((UINT2 u2Port));
VOID IpNetworkInterfaceConfig PROTO ((tIfaceQMsg *pIfaceQMsg));


/*************   P R O T O T Y P E    D E C L A R A T I O N S  ************/

VOID PmtuReleaseEntry PROTO ((tTMO_HASH_NODE *));
VOID      ipChangeIfaceOperStatus (UINT2 u2Port, UINT4 u4Status);
INT4      Ip_Mem_Init (VOID);



/* Prototypes for Utility Functions */
INT1
IpGetNextPort (UINT2 u2Port, UINT2 *pu2Port);
INT4
IpifGetPortFromIfIndex (UINT4 u4IfIndex, UINT2 *pu2Port);
UINT4 IpGetIfIndexFromPort            (UINT2 u2Port);

VOID  ip_put_hdr(tIP_BUF_CHAIN_HEADER *pBuf, t_IP *pIp, INT1 i1flag);

INT4 IpFwdDataLock (VOID);
INT4 IpFwdDataUnlock (VOID);
INT4 IpFwdCxtLock (VOID);
INT4 IpFwdCxtUnlock (VOID);
INT4 IpHLRegLock (VOID);
INT4 IpHLRegUnLock (VOID);

VOID ipifipIfaceConfigInit PROTO ((UINT2 u2Port));
INT4 ipifipIfaceAllocError PROTO ((UINT2 u2Index));

INT4 IpGetSrcAddressOnInterface PROTO ((UINT2   u2Port, UINT4 u4Dest, 
                                UINT4 *pu4SrcIp));

/* Get the information associated with the given  IP Address */
INT4 IpIfGetAddressInfo PROTO ((UINT4 u4IpAddr, UINT4 *pu4NetMask, 
                                UINT4 *pu4BcastAddr,UINT4 *pu4IfIndex));

INT4 IpIfGetAddressInfoInCxt PROTO ((UINT4 u4ContextId, UINT4 u4IpAddr, 
                                     UINT4 *pu4NetMask, UINT4 *pu4BcastAddr,
                                     UINT4 *pu4IfIndex));

INT4 IpIsLocalNetOnInterface PROTO ((UINT4 u4IfIndex, UINT4 u4Addr));

INT4 IpIfGetNextSecondaryAddress PROTO ((UINT2   u2Port, UINT4 u4IpAddr,
                                UINT4 *pu4NextIpAddr, UINT4 *pu4NetMask));
INT4 IpCreateContext PROTO ((UINT4 u4ContextId));

VOID IpDeleteContext PROTO ((UINT4 u4ContextId));

tIpCxt * UtilIpGetCxt PROTO ((UINT4 u4ContextId));


tIpCxt * UtilIpGetCxtFromIpPortNum PROTO ((UINT4 u4IpPort));

INT4 UtilIpVcmIsVcExist PROTO ((UINT4 u4ContextId));

INT1 IncMsrForFsIpv4GlobalTable PROTO ((INT4 i4ContextId, INT4 i4SetVal, 
                                        UINT4 *pu4ObjectId, UINT4 u4OIdLen, 
                                        UINT1 IsRowStatus));


INT1 IncMsrForFsIpv4Scalars (INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen);

INT1 IncMsrForFsIpv4TraceConfigTable (UINT4 u4IpTraceConfigDest,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OIdLen, UINT1 IsRowStatus);

INT1 IncMsrForFsIpv4RtrLstTable      (UINT4 u4RtrLstAddress,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OIdLen, UINT1 IsRowStatus);

INT1 IncMsrForFsIpv4AddressTable      (UINT4 u4TabAddress,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OIdLen, UINT1 IsRowStatus);

INT1 IncMsrForFsIpv4PathMtuTable     (INT4 i4ContextId, UINT4 u4PmtuDestination,
                                 INT4 i4PmtuTos,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OIdLen, UINT1 IsRowStatus);

INT1 IncMsrForFsIpv4CommonRoutingTable  (UINT4 i4ContextId, UINT4 u4RouteDest,
                                    UINT4 u4RouteMask, INT4 i4RouteTos,
                                    UINT4 u4RouteNextHop, INT4 RouteProto,
                                    INT4 i4SetVal, UINT4 *pu4ObjectId,
                                    UINT4 u4OIdLen, UINT1 IsRowStatus);

INT1 IncMsrForFsIpv4IpifTable (INT4 i4IpifIndex,
                        INT4 i4SetVal, UINT4 *pu4ObjectId,
                        UINT4 u4OIdLen, UINT1 IsRowStatus);

INT1 IncMsrForIpv4IcmpGlobalTable (INT4 i4ContextId, CHR1 cDatatype,
                            VOID *pSetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                            UINT1 IsRowStatus);

INT1 IncMsrForFsIpv4CidrAggTable     (INT4 i4ContextId, UINT4 u4AggAddress,
                                 UINT4 u4AddressMask,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OIdLen, UINT1 IsRowStatus);


INT1 IncMsrForFsIpv4CidrAdvertTable  (INT4 i4ContextId, UINT4 u4AdvertAddress,
                                 UINT4 u4AdvertAddressMask,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OIdLen, UINT1 IsRowStatus);


INT1 IncMsrForFsIpv4IrdpIfConfTable (INT4 i4IrdpIfConfIfNum, INT4 IrdpIfConfSubref,
                              CHR1 cDatatype, VOID *pSetVal,
                              UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                              UINT1 IsRowStatus);

INT1 IncMsrForIpv4RarpServerDatabaseTable (tSNMP_OCTET_STRING_TYPE * pHardwareAddress,
                            CHR1 cDatatype,
                            VOID *pSetVal, UINT4 *pu4ObjectId, UINT4 u4OIdLen,
                            UINT1 IsRowStatus);

UINT4 IpUtilGetTraceFlag PROTO ((UINT4 u4IpCxtId));

extern VOID PingEnableDebug PROTO ((UINT4));

INT4 IpClearInterfaceInfo PROTO ((UINT4 u4IfIndex));

INT4 IpClearContextInfo PROTO ((UINT4 u4ContextId));
#ifdef TUNNEL_WANTED

INT4
IpGetTunnelParams PROTO ((UINT4 u4IfIndex, tIpTunlIf * pIpTunlIf));
INT4 Ip4TunlIfUpdate PROTO  ((UINT4 u4IfIndex, UINT4 u4Mask, tIpTunlIf * pIpTnlInfo));
#endif
INT4
Ip4TnlIfUpdateNotifyFromCfa PROTO ((UINT4 u4IfIndex, tTnlIfInfo * pCfaTnlIfInfo));
INT4 Ipv4IsLoopbackAddress PROTO ((UINT4 u4ContextId, UINT4 u4IpAddress));
INT1
IpSetFsIpProxyArpSubnetOption ARG_LIST((INT4 ));
INT1
IpTestv2FsIpProxyArpSubnetOption ARG_LIST((UINT4 *  ,INT4 ));
INT1
IpGetFsIpProxyArpSubnetOption ARG_LIST((INT4 *));

#ifdef DHCP_LNXIP_WANTED
VOID    RtmSubTaskInit PROTO ((VOID));
#endif
