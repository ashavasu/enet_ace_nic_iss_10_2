/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpipdb.h,v 1.5 2010/07/27 07:22:20 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPIPDB_H
#define _FSMPIPDB_H

UINT1 FsMIFsIpGlobalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFsIpTraceConfigTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIFsIpTraceTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFsIpAddressTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIFsIpRtrLstTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIFsIpPathMtuTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFsIpCommonRoutingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIFsIpifTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFsIcmpGlobalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFsUdpGlobalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFsIpCidrAggTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIFsCidrAdvertTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIFsIrdpIfConfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIFsRarpServerDatabaseTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};

UINT4 fsmpip [] ={1,3,6,1,4,1,29601,2,38};
tSNMP_OID_TYPE fsmpipOID = {9, fsmpip};


UINT4 FsMIFsIpGlobalDebug [ ] ={1,3,6,1,4,1,29601,2,38,1};
UINT4 FsMIFsIpInLengthErrors [ ] ={1,3,6,1,4,1,29601,2,38,2,1,1};
UINT4 FsMIFsIpInCksumErrors [ ] ={1,3,6,1,4,1,29601,2,38,2,1,2};
UINT4 FsMIFsIpInVersionErrors [ ] ={1,3,6,1,4,1,29601,2,38,2,1,3};
UINT4 FsMIFsIpInTTLErrors [ ] ={1,3,6,1,4,1,29601,2,38,2,1,4};
UINT4 FsMIFsIpInOptionErrors [ ] ={1,3,6,1,4,1,29601,2,38,2,1,5};
UINT4 FsMIFsIpInBroadCasts [ ] ={1,3,6,1,4,1,29601,2,38,2,1,6};
UINT4 FsMIFsIpOutGenErrors [ ] ={1,3,6,1,4,1,29601,2,38,2,1,7};
UINT4 FsMIFsIpOptProcEnable [ ] ={1,3,6,1,4,1,29601,2,38,2,1,8};
UINT4 FsMIFsIpNumMultipath [ ] ={1,3,6,1,4,1,29601,2,38,2,1,9};
UINT4 FsMIFsIpLoadShareEnable [ ] ={1,3,6,1,4,1,29601,2,38,2,1,10};
UINT4 FsMIFsIpEnablePMTUD [ ] ={1,3,6,1,4,1,29601,2,38,2,1,11};
UINT4 FsMIFsIpPmtuEntryAge [ ] ={1,3,6,1,4,1,29601,2,38,2,1,12};
UINT4 FsMIFsIpContextDebug [ ] ={1,3,6,1,4,1,29601,2,38,2,1,13};
UINT4 FsMIFsIpTraceConfigDest [ ] ={1,3,6,1,4,1,29601,2,38,3,1,1};
UINT4 FsMIFsIpTraceConfigAdminStatus [ ] ={1,3,6,1,4,1,29601,2,38,3,1,2};
UINT4 FsMIFsIpTraceConfigMaxTTL [ ] ={1,3,6,1,4,1,29601,2,38,3,1,3};
UINT4 FsMIFsIpTraceConfigMinTTL [ ] ={1,3,6,1,4,1,29601,2,38,3,1,4};
UINT4 FsMIFsIpTraceConfigOperStatus [ ] ={1,3,6,1,4,1,29601,2,38,3,1,5};
UINT4 FsMIFsIpTraceConfigTimeout [ ] ={1,3,6,1,4,1,29601,2,38,3,1,6};
UINT4 FsMIFsIpTraceConfigMtu [ ] ={1,3,6,1,4,1,29601,2,38,3,1,7};
UINT4 FsMIFsIpTraceDest [ ] ={1,3,6,1,4,1,29601,2,38,4,1,1};
UINT4 FsMIFsIpTraceHopCount [ ] ={1,3,6,1,4,1,29601,2,38,4,1,2};
UINT4 FsMIFsIpTraceIntermHop [ ] ={1,3,6,1,4,1,29601,2,38,4,1,3};
UINT4 FsMIFsIpTraceReachTime1 [ ] ={1,3,6,1,4,1,29601,2,38,4,1,4};
UINT4 FsMIFsIpTraceReachTime2 [ ] ={1,3,6,1,4,1,29601,2,38,4,1,5};
UINT4 FsMIFsIpTraceReachTime3 [ ] ={1,3,6,1,4,1,29601,2,38,4,1,6};
UINT4 FsMIFsIpAddrTabAddress [ ] ={1,3,6,1,4,1,29601,2,38,5,1,1};
UINT4 FsMIFsIpAddrTabIfaceId [ ] ={1,3,6,1,4,1,29601,2,38,5,1,2};
UINT4 FsMIFsIpAddrTabAdvertise [ ] ={1,3,6,1,4,1,29601,2,38,5,1,3};
UINT4 FsMIFsIpAddrTabPreflevel [ ] ={1,3,6,1,4,1,29601,2,38,5,1,4};
UINT4 FsMIFsIpAddrTabStatus [ ] ={1,3,6,1,4,1,29601,2,38,5,1,5};
UINT4 FsMIFsIpRtrLstIface [ ] ={1,3,6,1,4,1,29601,2,38,6,1,1};
UINT4 FsMIFsIpRtrLstAddress [ ] ={1,3,6,1,4,1,29601,2,38,6,1,2};
UINT4 FsMIFsIpRtrLstPreflevel [ ] ={1,3,6,1,4,1,29601,2,38,6,1,3};
UINT4 FsMIFsIpRtrLstStatic [ ] ={1,3,6,1,4,1,29601,2,38,6,1,4};
UINT4 FsMIFsIpRtrLstStatus [ ] ={1,3,6,1,4,1,29601,2,38,6,1,5};
UINT4 FsMIFsIpPmtuDestination [ ] ={1,3,6,1,4,1,29601,2,38,7,1,1};
UINT4 FsMIFsIpPmtuTos [ ] ={1,3,6,1,4,1,29601,2,38,7,1,2};
UINT4 FsMIFsIpPathMtu [ ] ={1,3,6,1,4,1,29601,2,38,7,1,3};
UINT4 FsMIFsIpPmtuDisc [ ] ={1,3,6,1,4,1,29601,2,38,7,1,4};
UINT4 FsMIFsIpPmtuEntryStatus [ ] ={1,3,6,1,4,1,29601,2,38,7,1,5};
UINT4 FsMIFsIpRouteDest [ ] ={1,3,6,1,4,1,29601,2,38,8,1,1};
UINT4 FsMIFsIpRouteMask [ ] ={1,3,6,1,4,1,29601,2,38,8,1,2};
UINT4 FsMIFsIpRouteTos [ ] ={1,3,6,1,4,1,29601,2,38,8,1,3};
UINT4 FsMIFsIpRouteNextHop [ ] ={1,3,6,1,4,1,29601,2,38,8,1,4};
UINT4 FsMIFsIpRouteProto [ ] ={1,3,6,1,4,1,29601,2,38,8,1,5};
UINT4 FsMIFsIpRouteProtoInstanceId [ ] ={1,3,6,1,4,1,29601,2,38,8,1,6};
UINT4 FsMIFsIpRouteIfIndex [ ] ={1,3,6,1,4,1,29601,2,38,8,1,7};
UINT4 FsMIFsIpRouteType [ ] ={1,3,6,1,4,1,29601,2,38,8,1,8};
UINT4 FsMIFsIpRouteAge [ ] ={1,3,6,1,4,1,29601,2,38,8,1,9};
UINT4 FsMIFsIpRouteNextHopAS [ ] ={1,3,6,1,4,1,29601,2,38,8,1,10};
UINT4 FsMIFsIpRouteMetric1 [ ] ={1,3,6,1,4,1,29601,2,38,8,1,11};
UINT4 FsMIFsIpRoutePreference [ ] ={1,3,6,1,4,1,29601,2,38,8,1,12};
UINT4 FsMIFsIpRouteStatus [ ] ={1,3,6,1,4,1,29601,2,38,8,1,13};
UINT4 FsMIFsIpifIndex [ ] ={1,3,6,1,4,1,29601,2,38,9,1,1};
UINT4 FsMIFsIpifMaxReasmSize [ ] ={1,3,6,1,4,1,29601,2,38,9,1,2};
UINT4 FsMIFsIpifIcmpRedirectEnable [ ] ={1,3,6,1,4,1,29601,2,38,9,1,3};
UINT4 FsMIFsIpifDrtBcastFwdingEnable [ ] ={1,3,6,1,4,1,29601,2,38,9,1,4};
UINT4 FsMIFsIpifContextId [ ] ={1,3,6,1,4,1,29601,2,38,9,1,5};
UINT4 FsMIFsIpifProxyArpAdminStatus [ ] ={1,3,6,1,4,1,29601,2,38,9,1,6};
UINT4 FsMIFsIpifLocalProxyArpAdminStatus [ ] ={1,3,6,1,4,1,29601,2,38,9,1,7};
UINT4 FsMIFsIcmpSendRedirectEnable [ ] ={1,3,6,1,4,1,29601,2,38,10,1,1};
UINT4 FsMIFsIcmpSendUnreachableEnable [ ] ={1,3,6,1,4,1,29601,2,38,10,1,2};
UINT4 FsMIFsIcmpSendEchoReplyEnable [ ] ={1,3,6,1,4,1,29601,2,38,10,1,3};
UINT4 FsMIFsIcmpNetMaskReplyEnable [ ] ={1,3,6,1,4,1,29601,2,38,10,1,4};
UINT4 FsMIFsIcmpTimeStampReplyEnable [ ] ={1,3,6,1,4,1,29601,2,38,10,1,5};
UINT4 FsMIFsIcmpInDomainNameRequests [ ] ={1,3,6,1,4,1,29601,2,38,10,1,6};
UINT4 FsMIFsIcmpInDomainNameReply [ ] ={1,3,6,1,4,1,29601,2,38,10,1,7};
UINT4 FsMIFsIcmpOutDomainNameRequests [ ] ={1,3,6,1,4,1,29601,2,38,10,1,8};
UINT4 FsMIFsIcmpOutDomainNameReply [ ] ={1,3,6,1,4,1,29601,2,38,10,1,9};
UINT4 FsMIFsIcmpDirectQueryEnable [ ] ={1,3,6,1,4,1,29601,2,38,10,1,10};
UINT4 FsMIFsIcmpDomainName [ ] ={1,3,6,1,4,1,29601,2,38,10,1,11};
UINT4 FsMIFsIcmpTimeToLive [ ] ={1,3,6,1,4,1,29601,2,38,10,1,12};
UINT4 FsMIFsIcmpInSecurityFailures [ ] ={1,3,6,1,4,1,29601,2,38,10,1,13};
UINT4 FsMIFsIcmpOutSecurityFailures [ ] ={1,3,6,1,4,1,29601,2,38,10,1,14};
UINT4 FsMIFsIcmpSendSecurityFailuresEnable [ ] ={1,3,6,1,4,1,29601,2,38,10,1,15};
UINT4 FsMIFsIcmpRecvSecurityFailuresEnable [ ] ={1,3,6,1,4,1,29601,2,38,10,1,16};
UINT4 FsMIFsUdpInNoCksum [ ] ={1,3,6,1,4,1,29601,2,38,11,1,1};
UINT4 FsMIFsUdpInIcmpErr [ ] ={1,3,6,1,4,1,29601,2,38,11,1,2};
UINT4 FsMIFsUdpInErrCksum [ ] ={1,3,6,1,4,1,29601,2,38,11,1,3};
UINT4 FsMIFsUdpInBcast [ ] ={1,3,6,1,4,1,29601,2,38,11,1,4};
UINT4 FsMIFsIpCidrAggAddress [ ] ={1,3,6,1,4,1,29601,2,38,12,1,1};
UINT4 FsMIFsIpCidrAggAddressMask [ ] ={1,3,6,1,4,1,29601,2,38,12,1,2};
UINT4 FsMIFsIpCidrAggStatus [ ] ={1,3,6,1,4,1,29601,2,38,12,1,3};
UINT4 FsMIFsCidrAdvertAddress [ ] ={1,3,6,1,4,1,29601,2,38,13,1,1};
UINT4 FsMIFsCidrAdvertAddressMask [ ] ={1,3,6,1,4,1,29601,2,38,13,1,2};
UINT4 FsMIFsCidrAdvertStatus [ ] ={1,3,6,1,4,1,29601,2,38,13,1,3};
UINT4 FsMIFsIrdpInAdvertisements [ ] ={1,3,6,1,4,1,29601,2,38,14};
UINT4 FsMIFsIrdpInSolicitations [ ] ={1,3,6,1,4,1,29601,2,38,15};
UINT4 FsMIFsIrdpOutAdvertisements [ ] ={1,3,6,1,4,1,29601,2,38,16};
UINT4 FsMIFsIrdpOutSolicitations [ ] ={1,3,6,1,4,1,29601,2,38,17};
UINT4 FsMIFsIrdpSendAdvertisementsEnable [ ] ={1,3,6,1,4,1,29601,2,38,18};
UINT4 FsMIFsIrdpIfConfIfNum [ ] ={1,3,6,1,4,1,29601,2,38,19,1,1};
UINT4 FsMIFsIrdpIfConfSubref [ ] ={1,3,6,1,4,1,29601,2,38,19,1,2};
UINT4 FsMIFsIrdpIfConfAdvertisementAddress [ ] ={1,3,6,1,4,1,29601,2,38,19,1,3};
UINT4 FsMIFsIrdpIfConfMaxAdvertisementInterval [ ] ={1,3,6,1,4,1,29601,2,38,19,1,4};
UINT4 FsMIFsIrdpIfConfMinAdvertisementInterval [ ] ={1,3,6,1,4,1,29601,2,38,19,1,5};
UINT4 FsMIFsIrdpIfConfAdvertisementLifetime [ ] ={1,3,6,1,4,1,29601,2,38,19,1,6};
UINT4 FsMIFsIrdpIfConfPerformRouterDiscovery [ ] ={1,3,6,1,4,1,29601,2,38,19,1,7};
UINT4 FsMIFsIrdpIfConfSolicitationAddress [ ] ={1,3,6,1,4,1,29601,2,38,19,1,8};
UINT4 FsMIFsRarpClientRetransmissionTimeout [ ] ={1,3,6,1,4,1,29601,2,38,20};
UINT4 FsMIFsRarpClientMaxRetries [ ] ={1,3,6,1,4,1,29601,2,38,21};
UINT4 FsMIFsRarpClientPktsDiscarded [ ] ={1,3,6,1,4,1,29601,2,38,22};
UINT4 FsMIFsRarpServerStatus [ ] ={1,3,6,1,4,1,29601,2,38,23};
UINT4 FsMIFsRarpServerPktsDiscarded [ ] ={1,3,6,1,4,1,29601,2,38,24};
UINT4 FsMIFsRarpServerTableMaxEntries [ ] ={1,3,6,1,4,1,29601,2,38,25};
UINT4 FsMIFsHardwareAddress [ ] ={1,3,6,1,4,1,29601,2,38,26,1,1};
UINT4 FsMIFsHardwareAddrLen [ ] ={1,3,6,1,4,1,29601,2,38,26,1,2};
UINT4 FsMIFsProtocolAddress [ ] ={1,3,6,1,4,1,29601,2,38,26,1,3};
UINT4 FsMIFsEntryStatus [ ] ={1,3,6,1,4,1,29601,2,38,26,1,4};
UINT4 FsMIFsIpProxyArpSubnetOption [ ] ={1,3,6,1,4,1,29601,2,38,27};


tMbDbEntry fsmpipMibEntry[]= {

{{10,FsMIFsIpGlobalDebug}, NULL, FsMIFsIpGlobalDebugGet, FsMIFsIpGlobalDebugSet, FsMIFsIpGlobalDebugTest, FsMIFsIpGlobalDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIFsIpInLengthErrors}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpInLengthErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpInCksumErrors}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpInCksumErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpInVersionErrors}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpInVersionErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpInTTLErrors}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpInTTLErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpInOptionErrors}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpInOptionErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpInBroadCasts}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpInBroadCastsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpOutGenErrors}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpOutGenErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpOptProcEnable}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpOptProcEnableGet, FsMIFsIpOptProcEnableSet, FsMIFsIpOptProcEnableTest, FsMIFsIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIpNumMultipath}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpNumMultipathGet, FsMIFsIpNumMultipathSet, FsMIFsIpNumMultipathTest, FsMIFsIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpGlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIFsIpLoadShareEnable}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpLoadShareEnableGet, FsMIFsIpLoadShareEnableSet, FsMIFsIpLoadShareEnableTest, FsMIFsIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpGlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIFsIpEnablePMTUD}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpEnablePMTUDGet, FsMIFsIpEnablePMTUDSet, FsMIFsIpEnablePMTUDTest, FsMIFsIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpGlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIFsIpPmtuEntryAge}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpPmtuEntryAgeGet, FsMIFsIpPmtuEntryAgeSet, FsMIFsIpPmtuEntryAgeTest, FsMIFsIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpGlobalTableINDEX, 1, 0, 0, "10"},

{{12,FsMIFsIpContextDebug}, GetNextIndexFsMIFsIpGlobalTable, FsMIFsIpContextDebugGet, FsMIFsIpContextDebugSet, FsMIFsIpContextDebugTest, FsMIFsIpGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpTraceConfigDest}, GetNextIndexFsMIFsIpTraceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpTraceConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpTraceConfigAdminStatus}, GetNextIndexFsMIFsIpTraceConfigTable, FsMIFsIpTraceConfigAdminStatusGet, FsMIFsIpTraceConfigAdminStatusSet, FsMIFsIpTraceConfigAdminStatusTest, FsMIFsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpTraceConfigTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIpTraceConfigMaxTTL}, GetNextIndexFsMIFsIpTraceConfigTable, FsMIFsIpTraceConfigMaxTTLGet, FsMIFsIpTraceConfigMaxTTLSet, FsMIFsIpTraceConfigMaxTTLTest, FsMIFsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpTraceConfigTableINDEX, 1, 0, 0, "15"},

{{12,FsMIFsIpTraceConfigMinTTL}, GetNextIndexFsMIFsIpTraceConfigTable, FsMIFsIpTraceConfigMinTTLGet, FsMIFsIpTraceConfigMinTTLSet, FsMIFsIpTraceConfigMinTTLTest, FsMIFsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpTraceConfigTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIpTraceConfigOperStatus}, GetNextIndexFsMIFsIpTraceConfigTable, FsMIFsIpTraceConfigOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFsIpTraceConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpTraceConfigTimeout}, GetNextIndexFsMIFsIpTraceConfigTable, FsMIFsIpTraceConfigTimeoutGet, FsMIFsIpTraceConfigTimeoutSet, FsMIFsIpTraceConfigTimeoutTest, FsMIFsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpTraceConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpTraceConfigMtu}, GetNextIndexFsMIFsIpTraceConfigTable, FsMIFsIpTraceConfigMtuGet, FsMIFsIpTraceConfigMtuSet, FsMIFsIpTraceConfigMtuTest, FsMIFsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpTraceConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpTraceDest}, GetNextIndexFsMIFsIpTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpTraceHopCount}, GetNextIndexFsMIFsIpTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpTraceIntermHop}, GetNextIndexFsMIFsIpTraceTable, FsMIFsIpTraceIntermHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIFsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpTraceReachTime1}, GetNextIndexFsMIFsIpTraceTable, FsMIFsIpTraceReachTime1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpTraceReachTime2}, GetNextIndexFsMIFsIpTraceTable, FsMIFsIpTraceReachTime2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpTraceReachTime3}, GetNextIndexFsMIFsIpTraceTable, FsMIFsIpTraceReachTime3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpAddrTabAddress}, GetNextIndexFsMIFsIpAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpAddrTabIfaceId}, GetNextIndexFsMIFsIpAddressTable, FsMIFsIpAddrTabIfaceIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpAddrTabAdvertise}, GetNextIndexFsMIFsIpAddressTable, FsMIFsIpAddrTabAdvertiseGet, FsMIFsIpAddrTabAdvertiseSet, FsMIFsIpAddrTabAdvertiseTest, FsMIFsIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpAddrTabPreflevel}, GetNextIndexFsMIFsIpAddressTable, FsMIFsIpAddrTabPreflevelGet, FsMIFsIpAddrTabPreflevelSet, FsMIFsIpAddrTabPreflevelTest, FsMIFsIpAddressTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpAddressTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIpAddrTabStatus}, GetNextIndexFsMIFsIpAddressTable, FsMIFsIpAddrTabStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFsIpAddressTableINDEX, 2, 0, 1, NULL},

{{12,FsMIFsIpRtrLstIface}, GetNextIndexFsMIFsIpRtrLstTable, FsMIFsIpRtrLstIfaceGet, FsMIFsIpRtrLstIfaceSet, FsMIFsIpRtrLstIfaceTest, FsMIFsIpRtrLstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpRtrLstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpRtrLstAddress}, GetNextIndexFsMIFsIpRtrLstTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpRtrLstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpRtrLstPreflevel}, GetNextIndexFsMIFsIpRtrLstTable, FsMIFsIpRtrLstPreflevelGet, FsMIFsIpRtrLstPreflevelSet, FsMIFsIpRtrLstPreflevelTest, FsMIFsIpRtrLstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpRtrLstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpRtrLstStatic}, GetNextIndexFsMIFsIpRtrLstTable, FsMIFsIpRtrLstStaticGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIFsIpRtrLstTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpRtrLstStatus}, GetNextIndexFsMIFsIpRtrLstTable, FsMIFsIpRtrLstStatusGet, FsMIFsIpRtrLstStatusSet, FsMIFsIpRtrLstStatusTest, FsMIFsIpRtrLstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpRtrLstTableINDEX, 1, 0, 1, NULL},

{{12,FsMIFsIpPmtuDestination}, GetNextIndexFsMIFsIpPathMtuTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpPathMtuTableINDEX, 3, 0, 0, NULL},

{{12,FsMIFsIpPmtuTos}, GetNextIndexFsMIFsIpPathMtuTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFsIpPathMtuTableINDEX, 3, 0, 0, NULL},

{{12,FsMIFsIpPathMtu}, GetNextIndexFsMIFsIpPathMtuTable, FsMIFsIpPathMtuGet, FsMIFsIpPathMtuSet, FsMIFsIpPathMtuTest, FsMIFsIpPathMtuTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpPathMtuTableINDEX, 3, 0, 0, NULL},

{{12,FsMIFsIpPmtuDisc}, GetNextIndexFsMIFsIpPathMtuTable, FsMIFsIpPmtuDiscGet, FsMIFsIpPmtuDiscSet, FsMIFsIpPmtuDiscTest, FsMIFsIpPathMtuTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpPathMtuTableINDEX, 3, 0, 0, NULL},

{{12,FsMIFsIpPmtuEntryStatus}, GetNextIndexFsMIFsIpPathMtuTable, FsMIFsIpPmtuEntryStatusGet, FsMIFsIpPmtuEntryStatusSet, FsMIFsIpPmtuEntryStatusTest, FsMIFsIpPathMtuTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpPathMtuTableINDEX, 3, 0, 1, NULL},

{{12,FsMIFsIpRouteDest}, GetNextIndexFsMIFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteMask}, GetNextIndexFsMIFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteTos}, GetNextIndexFsMIFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteNextHop}, GetNextIndexFsMIFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteProto}, GetNextIndexFsMIFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteProtoInstanceId}, GetNextIndexFsMIFsIpCommonRoutingTable, FsMIFsIpRouteProtoInstanceIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteIfIndex}, GetNextIndexFsMIFsIpCommonRoutingTable, FsMIFsIpRouteIfIndexGet, FsMIFsIpRouteIfIndexSet, FsMIFsIpRouteIfIndexTest, FsMIFsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteType}, GetNextIndexFsMIFsIpCommonRoutingTable, FsMIFsIpRouteTypeGet, FsMIFsIpRouteTypeSet, FsMIFsIpRouteTypeTest, FsMIFsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteAge}, GetNextIndexFsMIFsIpCommonRoutingTable, FsMIFsIpRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, "0"},

{{12,FsMIFsIpRouteNextHopAS}, GetNextIndexFsMIFsIpCommonRoutingTable, FsMIFsIpRouteNextHopASGet, FsMIFsIpRouteNextHopASSet, FsMIFsIpRouteNextHopASTest, FsMIFsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, "0"},

{{12,FsMIFsIpRouteMetric1}, GetNextIndexFsMIFsIpCommonRoutingTable, FsMIFsIpRouteMetric1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, "-1"},

{{12,FsMIFsIpRoutePreference}, GetNextIndexFsMIFsIpCommonRoutingTable, FsMIFsIpRoutePreferenceGet, FsMIFsIpRoutePreferenceSet, FsMIFsIpRoutePreferenceTest, FsMIFsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIFsIpRouteStatus}, GetNextIndexFsMIFsIpCommonRoutingTable, FsMIFsIpRouteStatusGet, FsMIFsIpRouteStatusSet, FsMIFsIpRouteStatusTest, FsMIFsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpCommonRoutingTableINDEX, 6, 0, 1, NULL},

{{12,FsMIFsIpifIndex}, GetNextIndexFsMIFsIpifTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpifMaxReasmSize}, GetNextIndexFsMIFsIpifTable, FsMIFsIpifMaxReasmSizeGet, FsMIFsIpifMaxReasmSizeSet, FsMIFsIpifMaxReasmSizeTest, FsMIFsIpifTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpifIcmpRedirectEnable}, GetNextIndexFsMIFsIpifTable, FsMIFsIpifIcmpRedirectEnableGet, FsMIFsIpifIcmpRedirectEnableSet, FsMIFsIpifIcmpRedirectEnableTest, FsMIFsIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpifDrtBcastFwdingEnable}, GetNextIndexFsMIFsIpifTable, FsMIFsIpifDrtBcastFwdingEnableGet, FsMIFsIpifDrtBcastFwdingEnableSet, FsMIFsIpifDrtBcastFwdingEnableTest, FsMIFsIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpifContextId}, GetNextIndexFsMIFsIpifTable, FsMIFsIpifContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIFsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpifProxyArpAdminStatus}, GetNextIndexFsMIFsIpifTable, FsMIFsIpifProxyArpAdminStatusGet, FsMIFsIpifProxyArpAdminStatusSet, FsMIFsIpifProxyArpAdminStatusTest, FsMIFsIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpifTableINDEX, 1, 0, 0, "2"},

{{12,FsMIFsIpifLocalProxyArpAdminStatus}, GetNextIndexFsMIFsIpifTable, FsMIFsIpifLocalProxyArpAdminStatusGet, FsMIFsIpifLocalProxyArpAdminStatusSet, FsMIFsIpifLocalProxyArpAdminStatusTest, FsMIFsIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpifTableINDEX, 1, 0, 0, "2"},

{{12,FsMIFsIcmpSendRedirectEnable}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpSendRedirectEnableGet, FsMIFsIcmpSendRedirectEnableSet, FsMIFsIcmpSendRedirectEnableTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIcmpSendUnreachableEnable}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpSendUnreachableEnableGet, FsMIFsIcmpSendUnreachableEnableSet, FsMIFsIcmpSendUnreachableEnableTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIcmpSendEchoReplyEnable}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpSendEchoReplyEnableGet, FsMIFsIcmpSendEchoReplyEnableSet, FsMIFsIcmpSendEchoReplyEnableTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIcmpNetMaskReplyEnable}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpNetMaskReplyEnableGet, FsMIFsIcmpNetMaskReplyEnableSet, FsMIFsIcmpNetMaskReplyEnableTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIcmpTimeStampReplyEnable}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpTimeStampReplyEnableGet, FsMIFsIcmpTimeStampReplyEnableSet, FsMIFsIcmpTimeStampReplyEnableTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIcmpInDomainNameRequests}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpInDomainNameRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIcmpInDomainNameReply}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpInDomainNameReplyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIcmpOutDomainNameRequests}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpOutDomainNameRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIcmpOutDomainNameReply}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpOutDomainNameReplyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIcmpDirectQueryEnable}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpDirectQueryEnableGet, FsMIFsIcmpDirectQueryEnableSet, FsMIFsIcmpDirectQueryEnableTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsMIFsIcmpDomainName}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpDomainNameGet, FsMIFsIcmpDomainNameSet, FsMIFsIcmpDomainNameTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIcmpTimeToLive}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpTimeToLiveGet, FsMIFsIcmpTimeToLiveSet, FsMIFsIcmpTimeToLiveTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIcmpInSecurityFailures}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpInSecurityFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIcmpOutSecurityFailures}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpOutSecurityFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIcmpSendSecurityFailuresEnable}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpSendSecurityFailuresEnableGet, FsMIFsIcmpSendSecurityFailuresEnableSet, FsMIFsIcmpSendSecurityFailuresEnableTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsIcmpRecvSecurityFailuresEnable}, GetNextIndexFsMIFsIcmpGlobalTable, FsMIFsIcmpRecvSecurityFailuresEnableGet, FsMIFsIcmpRecvSecurityFailuresEnableSet, FsMIFsIcmpRecvSecurityFailuresEnableTest, FsMIFsIcmpGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIcmpGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsMIFsUdpInNoCksum}, GetNextIndexFsMIFsUdpGlobalTable, FsMIFsUdpInNoCksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsUdpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsUdpInIcmpErr}, GetNextIndexFsMIFsUdpGlobalTable, FsMIFsUdpInIcmpErrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsUdpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsUdpInErrCksum}, GetNextIndexFsMIFsUdpGlobalTable, FsMIFsUdpInErrCksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsUdpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsUdpInBcast}, GetNextIndexFsMIFsUdpGlobalTable, FsMIFsUdpInBcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIFsUdpGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsIpCidrAggAddress}, GetNextIndexFsMIFsIpCidrAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpCidrAggTableINDEX, 3, 0, 0, NULL},

{{12,FsMIFsIpCidrAggAddressMask}, GetNextIndexFsMIFsIpCidrAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsIpCidrAggTableINDEX, 3, 0, 0, NULL},

{{12,FsMIFsIpCidrAggStatus}, GetNextIndexFsMIFsIpCidrAggTable, FsMIFsIpCidrAggStatusGet, FsMIFsIpCidrAggStatusSet, FsMIFsIpCidrAggStatusTest, FsMIFsIpCidrAggTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIpCidrAggTableINDEX, 3, 0, 1, NULL},

{{12,FsMIFsCidrAdvertAddress}, GetNextIndexFsMIFsCidrAdvertTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsCidrAdvertTableINDEX, 3, 0, 0, NULL},

{{12,FsMIFsCidrAdvertAddressMask}, GetNextIndexFsMIFsCidrAdvertTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIFsCidrAdvertTableINDEX, 3, 0, 0, NULL},

{{12,FsMIFsCidrAdvertStatus}, GetNextIndexFsMIFsCidrAdvertTable, FsMIFsCidrAdvertStatusGet, FsMIFsCidrAdvertStatusSet, FsMIFsCidrAdvertStatusTest, FsMIFsCidrAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsCidrAdvertTableINDEX, 3, 0, 1, NULL},

{{10,FsMIFsIrdpInAdvertisements}, NULL, FsMIFsIrdpInAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIFsIrdpInSolicitations}, NULL, FsMIFsIrdpInSolicitationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIFsIrdpOutAdvertisements}, NULL, FsMIFsIrdpOutAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIFsIrdpOutSolicitations}, NULL, FsMIFsIrdpOutSolicitationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIFsIrdpSendAdvertisementsEnable}, NULL, FsMIFsIrdpSendAdvertisementsEnableGet, FsMIFsIrdpSendAdvertisementsEnableSet, FsMIFsIrdpSendAdvertisementsEnableTest, FsMIFsIrdpSendAdvertisementsEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsMIFsIrdpIfConfIfNum}, GetNextIndexFsMIFsIrdpIfConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFsIrdpIfConfTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIrdpIfConfSubref}, GetNextIndexFsMIFsIrdpIfConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIFsIrdpIfConfTableINDEX, 2, 0, 0, NULL},

{{12,FsMIFsIrdpIfConfAdvertisementAddress}, GetNextIndexFsMIFsIrdpIfConfTable, FsMIFsIrdpIfConfAdvertisementAddressGet, FsMIFsIrdpIfConfAdvertisementAddressSet, FsMIFsIrdpIfConfAdvertisementAddressTest, FsMIFsIrdpIfConfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIFsIrdpIfConfTableINDEX, 2, 0, 0, "3758096385"},

{{12,FsMIFsIrdpIfConfMaxAdvertisementInterval}, GetNextIndexFsMIFsIrdpIfConfTable, FsMIFsIrdpIfConfMaxAdvertisementIntervalGet, FsMIFsIrdpIfConfMaxAdvertisementIntervalSet, FsMIFsIrdpIfConfMaxAdvertisementIntervalTest, FsMIFsIrdpIfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIrdpIfConfTableINDEX, 2, 0, 0, "600"},

{{12,FsMIFsIrdpIfConfMinAdvertisementInterval}, GetNextIndexFsMIFsIrdpIfConfTable, FsMIFsIrdpIfConfMinAdvertisementIntervalGet, FsMIFsIrdpIfConfMinAdvertisementIntervalSet, FsMIFsIrdpIfConfMinAdvertisementIntervalTest, FsMIFsIrdpIfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIrdpIfConfTableINDEX, 2, 0, 0, "450"},

{{12,FsMIFsIrdpIfConfAdvertisementLifetime}, GetNextIndexFsMIFsIrdpIfConfTable, FsMIFsIrdpIfConfAdvertisementLifetimeGet, FsMIFsIrdpIfConfAdvertisementLifetimeSet, FsMIFsIrdpIfConfAdvertisementLifetimeTest, FsMIFsIrdpIfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsIrdpIfConfTableINDEX, 2, 0, 0, "1800"},

{{12,FsMIFsIrdpIfConfPerformRouterDiscovery}, GetNextIndexFsMIFsIrdpIfConfTable, FsMIFsIrdpIfConfPerformRouterDiscoveryGet, FsMIFsIrdpIfConfPerformRouterDiscoverySet, FsMIFsIrdpIfConfPerformRouterDiscoveryTest, FsMIFsIrdpIfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsIrdpIfConfTableINDEX, 2, 0, 0, "1"},

{{12,FsMIFsIrdpIfConfSolicitationAddress}, GetNextIndexFsMIFsIrdpIfConfTable, FsMIFsIrdpIfConfSolicitationAddressGet, FsMIFsIrdpIfConfSolicitationAddressSet, FsMIFsIrdpIfConfSolicitationAddressTest, FsMIFsIrdpIfConfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIFsIrdpIfConfTableINDEX, 2, 0, 0, "3758096386"},

{{10,FsMIFsRarpClientRetransmissionTimeout}, NULL, FsMIFsRarpClientRetransmissionTimeoutGet, FsMIFsRarpClientRetransmissionTimeoutSet, FsMIFsRarpClientRetransmissionTimeoutTest, FsMIFsRarpClientRetransmissionTimeoutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "100"},

{{10,FsMIFsRarpClientMaxRetries}, NULL, FsMIFsRarpClientMaxRetriesGet, FsMIFsRarpClientMaxRetriesSet, FsMIFsRarpClientMaxRetriesTest, FsMIFsRarpClientMaxRetriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{10,FsMIFsRarpClientPktsDiscarded}, NULL, FsMIFsRarpClientPktsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIFsRarpServerStatus}, NULL, FsMIFsRarpServerStatusGet, FsMIFsRarpServerStatusSet, FsMIFsRarpServerStatusTest, FsMIFsRarpServerStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsMIFsRarpServerPktsDiscarded}, NULL, FsMIFsRarpServerPktsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIFsRarpServerTableMaxEntries}, NULL, FsMIFsRarpServerTableMaxEntriesGet, FsMIFsRarpServerTableMaxEntriesSet, FsMIFsRarpServerTableMaxEntriesTest, FsMIFsRarpServerTableMaxEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsMIFsHardwareAddress}, GetNextIndexFsMIFsRarpServerDatabaseTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIFsRarpServerDatabaseTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsHardwareAddrLen}, GetNextIndexFsMIFsRarpServerDatabaseTable, FsMIFsHardwareAddrLenGet, FsMIFsHardwareAddrLenSet, FsMIFsHardwareAddrLenTest, FsMIFsRarpServerDatabaseTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIFsRarpServerDatabaseTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsProtocolAddress}, GetNextIndexFsMIFsRarpServerDatabaseTable, FsMIFsProtocolAddressGet, FsMIFsProtocolAddressSet, FsMIFsProtocolAddressTest, FsMIFsRarpServerDatabaseTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIFsRarpServerDatabaseTableINDEX, 1, 0, 0, NULL},

{{12,FsMIFsEntryStatus}, GetNextIndexFsMIFsRarpServerDatabaseTable, FsMIFsEntryStatusGet, FsMIFsEntryStatusSet, FsMIFsEntryStatusTest, FsMIFsRarpServerDatabaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIFsRarpServerDatabaseTableINDEX, 1, 0, 1, NULL},

{{10,FsMIFsIpProxyArpSubnetOption}, NULL, FsMIFsIpProxyArpSubnetOptionGet, FsMIFsIpProxyArpSubnetOptionSet, FsMIFsIpProxyArpSubnetOptionTest, FsMIFsIpProxyArpSubnetOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData fsmpipEntry = { 112, fsmpipMibEntry };
#endif /* _FSMPIPDB_H */

