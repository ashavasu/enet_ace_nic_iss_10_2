
#ifndef _FSIPWRAP_H 
#define _FSIPWRAP_H 
VOID RegisterFSIP(VOID);
INT4 FsIpInLengthErrorsGet (tSnmpIndex *, tRetVal *);
INT4 FsIpInCksumErrorsGet (tSnmpIndex *, tRetVal *);
INT4 FsIpInVersionErrorsGet (tSnmpIndex *, tRetVal *);
INT4 FsIpInTTLErrorsGet (tSnmpIndex *, tRetVal *);
INT4 FsIpInOptionErrorsGet (tSnmpIndex *, tRetVal *);
INT4 FsIpInBroadCastsGet (tSnmpIndex *, tRetVal *);
INT4 FsIpOutGenErrorsGet (tSnmpIndex *, tRetVal *);
INT4 FsIpOptProcEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpOptProcEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIpOptProcEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIpOptProcEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIpNumMultipathTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpNumMultipathSet (tSnmpIndex *, tRetVal *);
INT4 FsIpNumMultipathGet (tSnmpIndex *, tRetVal *);
INT4 FsIpNumMultipathDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIpLoadShareEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpLoadShareEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIpLoadShareEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIpLoadShareEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIpEnablePMTUDTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpEnablePMTUDSet (tSnmpIndex *, tRetVal *);
INT4 FsIpEnablePMTUDGet (tSnmpIndex *, tRetVal *);
INT4 FsIpEnablePMTUDDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIpPmtuEntryAgeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuEntryAgeSet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuEntryAgeGet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuEntryAgeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIpPmtuTableSizeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuTableSizeSet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuTableSizeGet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuTableSizeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 FsIpProxyArpSubnetOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsIpProxyArpSubnetOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsIpProxyArpSubnetOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpProxyArpSubnetOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 FsIpTraceConfigDestGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigAdminStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigAdminStatusSet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigAdminStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMaxTTLTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMaxTTLSet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMaxTTLGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMinTTLTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMinTTLSet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMinTTLGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigOperStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigTimeoutTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigTimeoutSet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigTimeoutGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMtuTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMtuSet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigMtuGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsIpTraceDestGet (tSnmpIndex *, tRetVal *);





INT4 FsIpTraceHopCountGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceIntermHopGet (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceReachTime1Get (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceReachTime2Get (tSnmpIndex *, tRetVal *);
INT4 FsIpTraceReachTime3Get (tSnmpIndex *, tRetVal *);
INT4 FsIpAddrTabIfaceIdGet (tSnmpIndex *, tRetVal *);
INT4 FsIpAddrTabAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsIpAddrTabAdvertiseTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpAddrTabAdvertiseSet (tSnmpIndex *, tRetVal *);
INT4 FsIpAddrTabAdvertiseGet (tSnmpIndex *, tRetVal *);
INT4 FsIpAddrTabPreflevelTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpAddrTabPreflevelSet (tSnmpIndex *, tRetVal *);
INT4 FsIpAddrTabPreflevelGet (tSnmpIndex *, tRetVal *);
INT4 FsIpAddressTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 FsIpAddrTabStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstIfaceTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstIfaceSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstIfaceGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstPreflevelTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstPreflevelSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstPreflevelGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstStaticGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstStatusSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRtrLstTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 FsIpPmtuDestinationGet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuTosGet (tSnmpIndex *, tRetVal *);
INT4 FsIpPathMtuTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpPathMtuSet (tSnmpIndex *, tRetVal *);
INT4 FsIpPathMtuGet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuDiscTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuDiscSet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuDiscGet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuEntryStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuEntryStatusSet (tSnmpIndex *, tRetVal *);
INT4 FsIpPmtuEntryStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsIpPathMtuTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 FsIpRoutingProtoIdGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRoutingProtoPrefTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRoutingProtoPrefSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRoutingProtoPrefGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteDestGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteMaskGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteTosGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteNextHopGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteProtoGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteProtoInstanceIdGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteIfIndexTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRouteIfIndexSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteIfIndexGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteTypeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRouteTypeSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteTypeGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteAgeGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteNextHopASTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRouteNextHopASSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteNextHopASGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteMetric1Get (tSnmpIndex *, tRetVal *);
INT4 FsIpRoutePreferenceTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRoutePreferenceSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRoutePreferenceGet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpRouteStatusSet (tSnmpIndex *, tRetVal *);
INT4 FsIpRouteStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsIpCommonRoutingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 FsIpifIndexGet (tSnmpIndex *, tRetVal *);
INT4 FsIpifMaxReasmSizeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpifMaxReasmSizeSet (tSnmpIndex *, tRetVal *);
INT4 FsIpifMaxReasmSizeGet (tSnmpIndex *, tRetVal *);
INT4 FsIpifIcmpRedirectEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpifIcmpRedirectEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIpifIcmpRedirectEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIpifDrtBcastFwdingEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpifDrtBcastFwdingEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIpifDrtBcastFwdingEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIpifProxyArpAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIpifProxyArpAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIpifProxyArpAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpifLocalProxyArpAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsIpifLocalProxyArpAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsIpifLocalProxyArpAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);

INT4 FsIpifTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 FsIcmpSendRedirectEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendRedirectEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendRedirectEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendUnreachableEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendUnreachableEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendUnreachableEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendEchoReplyEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendEchoReplyEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendEchoReplyEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpNetMaskReplyEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcmpNetMaskReplyEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpNetMaskReplyEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpTimeStampReplyEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcmpTimeStampReplyEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpTimeStampReplyEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpInDomainNameRequestsGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpInDomainNameReplyGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpOutDomainNameRequestsGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpOutDomainNameReplyGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpDirectQueryEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcmpDirectQueryEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpDirectQueryEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsDomainNameTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDomainNameSet (tSnmpIndex *, tRetVal *);
INT4 FsDomainNameGet (tSnmpIndex *, tRetVal *);
INT4 FsTimeToLiveTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTimeToLiveSet (tSnmpIndex *, tRetVal *);
INT4 FsTimeToLiveGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpInSecurityFailuresGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpOutSecurityFailuresGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendSecurityFailuresEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendSecurityFailuresEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendSecurityFailuresEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpRecvSecurityFailuresEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIcmpRecvSecurityFailuresEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpRecvSecurityFailuresEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIcmpSendRedirectEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcmpSendUnreachableEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcmpSendEchoReplyEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcmpNetMaskReplyEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcmpTimeStampReplyEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcmpDirectQueryEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDomainNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTimeToLiveDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcmpSendSecurityFailuresEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsIcmpRecvSecurityFailuresEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);










INT4 FsUdpInNoCksumGet (tSnmpIndex *, tRetVal *);
INT4 FsUdpInIcmpErrGet (tSnmpIndex *, tRetVal *);
INT4 FsUdpInErrCksumGet (tSnmpIndex *, tRetVal *);
INT4 FsUdpInBcastGet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAggAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAggAddressMaskGet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAggStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsCidrAggStatusSet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAggStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAggTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 FsCidrAdvertAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAdvertAddressMaskGet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAdvertStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsCidrAdvertStatusSet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAdvertStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsCidrAdvertTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 FsIrdpInAdvertisementsGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpInSolicitationsGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpOutAdvertisementsGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpOutSolicitationsGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpSendAdvertisementsEnableTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIrdpSendAdvertisementsEnableSet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpSendAdvertisementsEnableGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpSendAdvertisementsEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 FsIrdpIfConfIfNumGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfSubrefGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfAdvertisementAddressTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfAdvertisementAddressSet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfAdvertisementAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfMaxAdvertisementIntervalTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfMaxAdvertisementIntervalSet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfMaxAdvertisementIntervalGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfMinAdvertisementIntervalTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfMinAdvertisementIntervalSet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfMinAdvertisementIntervalGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfAdvertisementLifetimeTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfAdvertisementLifetimeSet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfAdvertisementLifetimeGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfPerformRouterDiscoveryTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfPerformRouterDiscoverySet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfPerformRouterDiscoveryGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfSolicitationAddressTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfSolicitationAddressSet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfSolicitationAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsIrdpIfConfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 FsRarpClientRetransmissionTimeoutTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRarpClientRetransmissionTimeoutSet (tSnmpIndex *, tRetVal *);
INT4 FsRarpClientRetransmissionTimeoutGet (tSnmpIndex *, tRetVal *);
INT4 FsRarpClientRetransmissionTimeoutDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRarpClientMaxRetriesTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRarpClientMaxRetriesSet (tSnmpIndex *, tRetVal *);
INT4 FsRarpClientMaxRetriesGet (tSnmpIndex *, tRetVal *);
INT4 FsRarpClientPktsDiscardedGet (tSnmpIndex *, tRetVal *);
INT4 FsRarpServerStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRarpClientMaxRetriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 FsRarpServerStatusSet (tSnmpIndex *, tRetVal *);
INT4 FsRarpServerStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsRarpServerPktsDiscardedGet (tSnmpIndex *, tRetVal *);
INT4 FsRarpServerTableMaxEntriesTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsRarpServerTableMaxEntriesSet (tSnmpIndex *, tRetVal *);
INT4 FsRarpServerTableMaxEntriesGet (tSnmpIndex *, tRetVal *);
INT4 FsRarpServerStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsRarpServerTableMaxEntriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 FsHardwareAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsHardwareAddrLenTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsHardwareAddrLenSet (tSnmpIndex *, tRetVal *);
INT4 FsHardwareAddrLenGet (tSnmpIndex *, tRetVal *);
INT4 FsProtocolAddressTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsProtocolAddressSet (tSnmpIndex *, tRetVal *);
INT4 FsProtocolAddressGet (tSnmpIndex *, tRetVal *);
INT4 FsEntryStatusTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsEntryStatusSet (tSnmpIndex *, tRetVal *);
INT4 FsEntryStatusGet (tSnmpIndex *, tRetVal *);
INT4 FsRarpServerDatabaseTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 FsNoOfStaticRoutesTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNoOfStaticRoutesSet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfStaticRoutesGet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfAggregatedRoutesTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNoOfAggregatedRoutesSet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfAggregatedRoutesGet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfRoutesTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNoOfRoutesSet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfRoutesGet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfReassemblyListsTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNoOfReassemblyListsSet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfReassemblyListsGet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfFragmentsPerListTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNoOfFragmentsPerListSet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfFragmentsPerListGet (tSnmpIndex *, tRetVal *);
INT4 FsNoOfStaticRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsNoOfAggregatedRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsNoOfRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsNoOfReassemblyListsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsNoOfFragmentsPerListDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);





INT4 FsNoOfMcastGroupsTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsNoOfMcastGroupsSet (tSnmpIndex *, tRetVal *);
INT4 FsIpGlobalDebugTest ( UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsIpGlobalDebugSet (tSnmpIndex *, tRetVal *);
INT4 FsIpGlobalDebugGet (tSnmpIndex *, tRetVal *);
INT4 FsIpGlobalDebugDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

 /*  GetNext Function Prototypes */

INT4 GetNextIndexFsRarpServerDatabaseTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIrdpIfConfTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsCidrAdvertTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsCidrAggTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIpifTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIpCommonRoutingTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIpRoutePrefTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIpPathMtuTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIpRtrLstTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIpAddressTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIpTraceTable( tSnmpIndex *, tSnmpIndex *);

INT4 GetNextIndexFsIpTraceConfigTable( tSnmpIndex *, tSnmpIndex *);

#ifdef IP6_WANTED
VOID RegisterFSV4OV6 (VOID);
#endif
    
#endif
