/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iptmrdfs.h,v 1.3 2007/02/01 14:57:53 iss Exp $
 *
 * Description: The header file for IP timer module.
 *
 *******************************************************************/

#ifndef   __IP_IPTIMER_H__
#define   __IP_IPTIMER_H__

#define   IP_REASM_TIMER_ID         2    /* Indicate Reassembly Timer  */
#define   IP_CACHE_TIMER_ID        50    /* Indicate Route Cache Timer */
#define   IP_PMTU_TIMER_ID         14

#endif            /*__IP_IPTIMER_H__*/
