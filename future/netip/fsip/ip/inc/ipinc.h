/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipinc.h,v 1.18 2014/10/10 11:59:15 siva Exp $
 *
 * Description: Common includes of IP
 *
 *******************************************************************/
#ifndef   __IP_INC_H
#define   __IP_INC_H

#include "lr.h"
#include "cruport.h"
#include "tmoport.h"
#include "other.h"
#include "cfa.h"
#include "ip.h"
#include "arp.h"
#include "vcm.h"
#include "iptmrdfs.h"
#include "ipport.h"
#include "ippdudfs.h"
#include "ipfrgtyp.h"
#include "ipprmdfs.h"
#include "ipifdfs.h"
#include "ipreg.h"
#include "ipflttyp.h"
#include "snmctdfs.h"
#include "iptrace.h"
#include "ipifrtif.h"
#include "ipcfaif.h"
#include "icmptyps.h"
#include "irdpdefn.h"
#include "irdptdfs.h"
#include "irdpprot.h"
#include "ipcidrcf.h"
#include "ipsndfs.h"
#include "snmccons.h"
#include "udpport.h"
#include "icmpprot.h"
#include "ippmtud.h"
#include "ipcfaif.h"
#include "ipprotos.h"
#include "rarpdefn.h"
#include "rarptdfs.h"
#include "ipifutls.h"
#include "ipextn.h"
#include "fssocket.h"
#include "fsipwr.h"
#include "fsiplow.h"
#include "fsmpipwr.h"
#include "fsmpiplw.h"
#include "iptdfs.h"
#include "ipglob.h"
#include "rtm.h"
#include "utlshinc.h"
#include "vrrp.h"

#ifdef NPAPI_WANTED
#include "ipnpwr.h"
#endif /* NPAPI_WANTED */

#include "ipvx.h"
#include "ip4ipvx.h"
#include "ipsz.h"
#ifdef VXLAN_WANTED
#include "fsvxlan.h"
#endif
#endif /* __IP_INC_H */
