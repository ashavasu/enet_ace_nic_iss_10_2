
 /* $Id: fsiplow.h,v 1.10 2016/02/27 10:14:47 siva Exp $*/
INT1
nmhGetFsIpInLengthErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsIpInCksumErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsIpInVersionErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsIpInTTLErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsIpInOptionErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsIpInBroadCasts ARG_LIST((UINT4 *));

INT1
nmhGetFsIpOutGenErrors ARG_LIST((UINT4 *));

INT1
nmhGetFsIpOptProcEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIpNumMultipath ARG_LIST((INT4 *));

INT1
nmhGetFsIpLoadShareEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIpEnablePMTUD ARG_LIST((INT4 *));

INT1
nmhGetFsIpPmtuEntryAge ARG_LIST((INT4 *));

INT1
nmhGetFsIpPmtuTableSize ARG_LIST((INT4 *));

INT1
nmhGetFsIpProxyArpSubnetOption ARG_LIST((INT4 *));

INT1
nmhGetFsIpifLocalProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
IpGetFsIpifLocalProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));




/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpOptProcEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIpNumMultipath ARG_LIST((INT4 ));

INT1
nmhSetFsIpLoadShareEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIpEnablePMTUD ARG_LIST((INT4 ));

INT1
nmhSetFsIpPmtuEntryAge ARG_LIST((INT4 ));

INT1
nmhSetFsIpPmtuTableSize ARG_LIST((INT4 ));

INT1
nmhSetFsIpProxyArpSubnetOption ARG_LIST((INT4 ));

INT1
nmhSetFsIpifLocalProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
IpSetFsIpifLocalProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));



/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpOptProcEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIpNumMultipath ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIpLoadShareEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIpEnablePMTUD ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIpPmtuEntryAge ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIpPmtuTableSize ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIpProxyArpSubnetOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIpifLocalProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
IpTestv2FsIpifLocalProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpOptProcEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIpNumMultipath ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIpLoadShareEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIpEnablePMTUD ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIpPmtuEntryAge ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIpPmtuTableSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIpProxyArpSubnetOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpTraceConfigTable. */
INT1
nmhValidateIndexInstanceFsIpTraceConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpTraceConfigTable  */

INT1
nmhGetFirstIndexFsIpTraceConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpTraceConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpTraceConfigAdminStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpTraceConfigMaxTTL ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpTraceConfigMinTTL ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpTraceConfigOperStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpTraceConfigTimeout ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpTraceConfigMtu ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpTraceConfigAdminStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIpTraceConfigMaxTTL ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIpTraceConfigMinTTL ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIpTraceConfigTimeout ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIpTraceConfigMtu ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpTraceConfigAdminStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIpTraceConfigMaxTTL ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIpTraceConfigMinTTL ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIpTraceConfigTimeout ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIpTraceConfigMtu ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpTraceConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpTraceTable. */
INT1
nmhValidateIndexInstanceFsIpTraceTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpTraceTable  */

INT1
nmhGetFirstIndexFsIpTraceTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpTraceTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpTraceIntermHop ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsIpTraceReachTime1 ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpTraceReachTime2 ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpTraceReachTime3 ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsIpAddressTable. */
INT1
nmhValidateIndexInstanceFsIpAddressTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpAddressTable  */

INT1
nmhGetFirstIndexFsIpAddressTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpAddressTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpAddrTabIfaceId ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpAddrTabAdvertise ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpAddrTabPreflevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpAddrTabStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpAddrTabAdvertise ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIpAddrTabPreflevel ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpAddrTabAdvertise ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIpAddrTabPreflevel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpRtrLstTable. */
INT1
nmhValidateIndexInstanceFsIpRtrLstTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpRtrLstTable  */

INT1
nmhGetFirstIndexFsIpRtrLstTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpRtrLstTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpRtrLstIface ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpRtrLstPreflevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpRtrLstStatic ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsIpRtrLstStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpRtrLstIface ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIpRtrLstPreflevel ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsIpRtrLstStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpRtrLstIface ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIpRtrLstPreflevel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsIpRtrLstStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpRtrLstTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpPathMtuTable. */
INT1
nmhValidateIndexInstanceFsIpPathMtuTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpPathMtuTable  */

INT1
nmhGetFirstIndexFsIpPathMtuTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpPathMtuTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpPathMtu ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpPmtuDisc ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpPmtuEntryStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpPathMtu ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsIpPmtuDisc ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsIpPmtuEntryStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpPathMtu ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIpPmtuDisc ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIpPmtuEntryStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpPathMtuTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpCommonRoutingTable. */
INT1
nmhValidateIndexInstanceFsIpCommonRoutingTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpCommonRoutingTable  */

INT1
nmhGetFirstIndexFsIpCommonRoutingTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpCommonRoutingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpRouteProtoInstanceId ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpRouteAge ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpRoutePreference ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsIpRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsIpRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsIpRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsIpRoutePreference ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsIpRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpRouteIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIpRouteType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIpRouteNextHopAS ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIpRoutePreference ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIpRouteStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpCommonRoutingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIpifTable. */
INT1
nmhValidateIndexInstanceFsIpifTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIpifTable  */

INT1
nmhGetFirstIndexFsIpifTable ARG_LIST((INT4 *));

INT1
IpGetFirstIndexFsIpifTable ARG_LIST((INT4 *));


/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIpifTable ARG_LIST((INT4 , INT4 *));

INT1
IpGetNextIndexFsIpifTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpifMaxReasmSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIpifIcmpRedirectEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIpifDrtBcastFwdingEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIpifProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
IpGetFsIpifProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpifMaxReasmSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIpifIcmpRedirectEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIpifDrtBcastFwdingEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIpifProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
IpSetFsIpifProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpifMaxReasmSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIpifIcmpRedirectEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIpifDrtBcastFwdingEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIpifProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
IpTestv2FsIpifProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpifTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIcmpSendRedirectEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpSendUnreachableEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpSendEchoReplyEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpNetMaskReplyEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpTimeStampReplyEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpInDomainNameRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpInDomainNameReply ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpOutDomainNameRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpOutDomainNameReply ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpDirectQueryEnable ARG_LIST((INT4 *));

INT1
nmhGetFsDomainName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTimeToLive ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpInSecurityFailures ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpOutSecurityFailures ARG_LIST((UINT4 *));

INT1
nmhGetFsIcmpSendSecurityFailuresEnable ARG_LIST((INT4 *));

INT1
nmhGetFsIcmpRecvSecurityFailuresEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIcmpSendRedirectEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpSendUnreachableEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpSendEchoReplyEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpNetMaskReplyEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpTimeStampReplyEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpDirectQueryEnable ARG_LIST((INT4 ));

INT1
nmhSetFsDomainName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsTimeToLive ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpSendSecurityFailuresEnable ARG_LIST((INT4 ));

INT1
nmhSetFsIcmpRecvSecurityFailuresEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIcmpSendRedirectEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpSendUnreachableEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpSendEchoReplyEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpNetMaskReplyEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpTimeStampReplyEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpDirectQueryEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsDomainName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsTimeToLive ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpSendSecurityFailuresEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIcmpRecvSecurityFailuresEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIcmpSendRedirectEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpSendUnreachableEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpSendEchoReplyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpNetMaskReplyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpTimeStampReplyEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpDirectQueryEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDomainName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTimeToLive ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpSendSecurityFailuresEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIcmpRecvSecurityFailuresEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsUdpInNoCksum ARG_LIST((UINT4 *));

INT1
nmhGetFsUdpInIcmpErr ARG_LIST((UINT4 *));

INT1
nmhGetFsUdpInErrCksum ARG_LIST((UINT4 *));

INT1
nmhGetFsUdpInBcast ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsCidrAggTable. */
INT1
nmhValidateIndexInstanceFsCidrAggTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsCidrAggTable  */

INT1
nmhGetFirstIndexFsCidrAggTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCidrAggTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCidrAggStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCidrAggStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCidrAggStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCidrAggTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsCidrAdvertTable. */
INT1
nmhValidateIndexInstanceFsCidrAdvertTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsCidrAdvertTable  */

INT1
nmhGetFirstIndexFsCidrAdvertTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsCidrAdvertTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsCidrAdvertStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsCidrAdvertStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsCidrAdvertStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsCidrAdvertTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIgmpGlobalStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIgmpGlobalStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIgmpInterfaceTable  */

INT1
nmhGetFirstIndexFsIgmpInterfaceTable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIrdpInAdvertisements ARG_LIST((UINT4 *));

INT1
nmhGetFsIrdpInSolicitations ARG_LIST((UINT4 *));

INT1
nmhGetFsIrdpOutAdvertisements ARG_LIST((UINT4 *));

INT1
nmhGetFsIrdpOutSolicitations ARG_LIST((UINT4 *));

INT1
nmhGetFsIrdpSendAdvertisementsEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIrdpSendAdvertisementsEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIrdpSendAdvertisementsEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIrdpSendAdvertisementsEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIrdpIfConfTable. */
INT1
nmhValidateIndexInstanceFsIrdpIfConfTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIrdpIfConfTable  */

INT1
nmhGetFirstIndexFsIrdpIfConfTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIrdpIfConfTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIrdpIfConfAdvertisementAddress ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsIrdpIfConfMaxAdvertisementInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIrdpIfConfMinAdvertisementInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIrdpIfConfAdvertisementLifetime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIrdpIfConfPerformRouterDiscovery ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIrdpIfConfSolicitationAddress ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIrdpIfConfAdvertisementAddress ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsIrdpIfConfMaxAdvertisementInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIrdpIfConfMinAdvertisementInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIrdpIfConfAdvertisementLifetime ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIrdpIfConfPerformRouterDiscovery ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIrdpIfConfSolicitationAddress ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIrdpIfConfAdvertisementAddress ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsIrdpIfConfMaxAdvertisementInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIrdpIfConfMinAdvertisementInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIrdpIfConfAdvertisementLifetime ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIrdpIfConfPerformRouterDiscovery ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIrdpIfConfSolicitationAddress ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIrdpIfConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRarpClientRetransmissionTimeout ARG_LIST((INT4 *));

INT1
nmhGetFsRarpClientMaxRetries ARG_LIST((INT4 *));

INT1
nmhGetFsRarpClientPktsDiscarded ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRarpClientRetransmissionTimeout ARG_LIST((INT4 ));

INT1
nmhSetFsRarpClientMaxRetries ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRarpClientRetransmissionTimeout ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRarpClientMaxRetries ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRarpClientRetransmissionTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRarpClientMaxRetries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRarpServerStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRarpServerPktsDiscarded ARG_LIST((UINT4 *));

INT1
nmhGetFsRarpServerTableMaxEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRarpServerStatus ARG_LIST((INT4 ));

INT1
nmhSetFsRarpServerTableMaxEntries ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRarpServerStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRarpServerTableMaxEntries ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRarpServerStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRarpServerTableMaxEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRarpServerDatabaseTable. */
INT1
nmhValidateIndexInstanceFsRarpServerDatabaseTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsRarpServerDatabaseTable  */

INT1
nmhGetFirstIndexFsRarpServerDatabaseTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRarpServerDatabaseTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsHardwareAddrLen ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsProtocolAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsHardwareAddrLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsProtocolAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsHardwareAddrLen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsProtocolAddress ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsEntryStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRarpServerDatabaseTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsNoOfStaticRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsNoOfAggregatedRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsNoOfRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsNoOfReassemblyLists ARG_LIST((INT4 *));

INT1
nmhGetFsNoOfFragmentsPerList ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsNoOfStaticRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfAggregatedRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfReassemblyLists ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfFragmentsPerList ARG_LIST((INT4 ));

INT1
nmhSetFsNoOfMcastGroups ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsNoOfStaticRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNoOfAggregatedRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNoOfRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNoOfReassemblyLists ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsNoOfFragmentsPerList ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsNoOfStaticRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNoOfAggregatedRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNoOfRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNoOfReassemblyLists ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsNoOfFragmentsPerList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIpGlobalDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIpGlobalDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIpGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIpGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
