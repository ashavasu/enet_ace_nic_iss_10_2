/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpiplw.h,v 1.4 2010/07/27 07:22:20 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpGlobalDebug ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpGlobalDebug ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpGlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIpGlobalTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpGlobalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpGlobalTable  */

INT1
nmhGetFirstIndexFsMIFsIpGlobalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpGlobalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpInLengthErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIpInCksumErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIpInVersionErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIpInTTLErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIpInOptionErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIpInBroadCasts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIpOutGenErrors ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIpOptProcEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpNumMultipath ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpLoadShareEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpEnablePMTUD ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpPmtuEntryAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpContextDebug ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpOptProcEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpNumMultipath ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpLoadShareEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpEnablePMTUD ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpPmtuEntryAge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpContextDebug ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpOptProcEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpNumMultipath ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpLoadShareEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpEnablePMTUD ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpPmtuEntryAge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpContextDebug ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIpTraceConfigTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpTraceConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpTraceConfigTable  */

INT1
nmhGetFirstIndexFsMIFsIpTraceConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpTraceConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpTraceConfigAdminStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpTraceConfigMaxTTL ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpTraceConfigMinTTL ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpTraceConfigOperStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpTraceConfigTimeout ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpTraceConfigMtu ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpTraceConfigAdminStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsIpTraceConfigMaxTTL ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsIpTraceConfigMinTTL ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsIpTraceConfigTimeout ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsIpTraceConfigMtu ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpTraceConfigAdminStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpTraceConfigMaxTTL ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpTraceConfigMinTTL ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpTraceConfigTimeout ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpTraceConfigMtu ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpTraceConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIpTraceTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpTraceTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpTraceTable  */

INT1
nmhGetFirstIndexFsMIFsIpTraceTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpTraceTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpTraceIntermHop ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIpTraceReachTime1 ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpTraceReachTime2 ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpTraceReachTime3 ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIFsIpAddressTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpAddressTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpAddressTable  */

INT1
nmhGetFirstIndexFsMIFsIpAddressTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpAddressTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpAddrTabIfaceId ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpAddrTabAdvertise ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpAddrTabPreflevel ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpAddrTabStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpAddrTabAdvertise ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIFsIpAddrTabPreflevel ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpAddrTabAdvertise ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpAddrTabPreflevel ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpAddressTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIpRtrLstTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpRtrLstTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpRtrLstTable  */

INT1
nmhGetFirstIndexFsMIFsIpRtrLstTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpRtrLstTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpRtrLstIface ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRtrLstPreflevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRtrLstStatic ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRtrLstStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpRtrLstIface ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsIpRtrLstPreflevel ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMIFsIpRtrLstStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpRtrLstIface ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpRtrLstPreflevel ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpRtrLstStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpRtrLstTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIpPathMtuTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpPathMtuTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpPathMtuTable  */

INT1
nmhGetFirstIndexFsMIFsIpPathMtuTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpPathMtuTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpPathMtu ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpPmtuDisc ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpPmtuEntryStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpPathMtu ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpPmtuDisc ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpPmtuEntryStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpPathMtu ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpPmtuDisc ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpPmtuEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpPathMtuTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIpCommonRoutingTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpCommonRoutingTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpCommonRoutingTable  */

INT1
nmhGetFirstIndexFsMIFsIpCommonRoutingTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpCommonRoutingTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpRouteProtoInstanceId ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRouteIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRouteType ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRouteAge ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRouteNextHopAS ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRouteMetric1 ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRoutePreference ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpRouteStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpRouteIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpRouteType ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpRouteNextHopAS ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpRoutePreference ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpRouteStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpRouteType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpRouteNextHopAS ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpRoutePreference ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpRouteStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpCommonRoutingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIpifTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpifTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpifTable  */

INT1
nmhGetFirstIndexFsMIFsIpifTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpifTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpifMaxReasmSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpifIcmpRedirectEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpifDrtBcastFwdingEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpifContextId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpifProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIpifLocalProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpifMaxReasmSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpifIcmpRedirectEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpifDrtBcastFwdingEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpifProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIpifLocalProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpifMaxReasmSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpifIcmpRedirectEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpifDrtBcastFwdingEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpifLocalProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIpifProxyArpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpifTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIcmpGlobalTable. */
INT1
nmhValidateIndexInstanceFsMIFsIcmpGlobalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIcmpGlobalTable  */

INT1
nmhGetFirstIndexFsMIFsIcmpGlobalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIcmpGlobalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIcmpSendRedirectEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIcmpSendUnreachableEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIcmpSendEchoReplyEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIcmpNetMaskReplyEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIcmpTimeStampReplyEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIcmpInDomainNameRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIcmpInDomainNameReply ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIcmpOutDomainNameRequests ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIcmpOutDomainNameReply ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIcmpDirectQueryEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIcmpDomainName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIFsIcmpTimeToLive ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIcmpInSecurityFailures ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIcmpOutSecurityFailures ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIcmpSendSecurityFailuresEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIFsIcmpRecvSecurityFailuresEnable ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIcmpSendRedirectEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIcmpSendUnreachableEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIcmpSendEchoReplyEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIcmpNetMaskReplyEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIcmpTimeStampReplyEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIcmpDirectQueryEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIcmpDomainName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIFsIcmpTimeToLive ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIcmpSendSecurityFailuresEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIFsIcmpRecvSecurityFailuresEnable ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIcmpSendRedirectEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIcmpSendUnreachableEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIcmpSendEchoReplyEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIcmpNetMaskReplyEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIcmpTimeStampReplyEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIcmpDirectQueryEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIcmpDomainName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIFsIcmpTimeToLive ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIcmpSendSecurityFailuresEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIcmpRecvSecurityFailuresEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIcmpGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsUdpGlobalTable. */
INT1
nmhValidateIndexInstanceFsMIFsUdpGlobalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsUdpGlobalTable  */

INT1
nmhGetFirstIndexFsMIFsUdpGlobalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsUdpGlobalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsUdpInNoCksum ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsUdpInIcmpErr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsUdpInErrCksum ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIFsUdpInBcast ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMIFsIpCidrAggTable. */
INT1
nmhValidateIndexInstanceFsMIFsIpCidrAggTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIpCidrAggTable  */

INT1
nmhGetFirstIndexFsMIFsIpCidrAggTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIpCidrAggTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpCidrAggStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpCidrAggStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpCidrAggStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpCidrAggTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsCidrAdvertTable. */
INT1
nmhValidateIndexInstanceFsMIFsCidrAdvertTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsCidrAdvertTable  */

INT1
nmhGetFirstIndexFsMIFsCidrAdvertTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsCidrAdvertTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsCidrAdvertStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsCidrAdvertStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsCidrAdvertStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsCidrAdvertTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIrdpInAdvertisements ARG_LIST((UINT4 *));

INT1
nmhGetFsMIFsIrdpInSolicitations ARG_LIST((UINT4 *));

INT1
nmhGetFsMIFsIrdpOutAdvertisements ARG_LIST((UINT4 *));

INT1
nmhGetFsMIFsIrdpOutSolicitations ARG_LIST((UINT4 *));

INT1
nmhGetFsMIFsIrdpSendAdvertisementsEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIrdpSendAdvertisementsEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIrdpSendAdvertisementsEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIrdpSendAdvertisementsEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsIrdpIfConfTable. */
INT1
nmhValidateIndexInstanceFsMIFsIrdpIfConfTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIFsIrdpIfConfTable  */

INT1
nmhGetFirstIndexFsMIFsIrdpIfConfTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsIrdpIfConfTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIrdpIfConfAdvertisementAddress ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsMIFsIrdpIfConfMaxAdvertisementInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIrdpIfConfMinAdvertisementInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIrdpIfConfAdvertisementLifetime ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIrdpIfConfPerformRouterDiscovery ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIFsIrdpIfConfSolicitationAddress ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIrdpIfConfAdvertisementAddress ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsMIFsIrdpIfConfMaxAdvertisementInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIrdpIfConfMinAdvertisementInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIrdpIfConfAdvertisementLifetime ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIrdpIfConfPerformRouterDiscovery ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIFsIrdpIfConfSolicitationAddress ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIrdpIfConfAdvertisementAddress ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsMIFsIrdpIfConfMaxAdvertisementInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIrdpIfConfMinAdvertisementInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIrdpIfConfAdvertisementLifetime ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIrdpIfConfPerformRouterDiscovery ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIFsIrdpIfConfSolicitationAddress ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIrdpIfConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsRarpClientRetransmissionTimeout ARG_LIST((INT4 *));

INT1
nmhGetFsMIFsRarpClientMaxRetries ARG_LIST((INT4 *));

INT1
nmhGetFsMIFsRarpClientPktsDiscarded ARG_LIST((UINT4 *));

INT1
nmhGetFsMIFsRarpServerStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIFsRarpServerPktsDiscarded ARG_LIST((UINT4 *));

INT1
nmhGetFsMIFsRarpServerTableMaxEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsRarpClientRetransmissionTimeout ARG_LIST((INT4 ));

INT1
nmhSetFsMIFsRarpClientMaxRetries ARG_LIST((INT4 ));

INT1
nmhSetFsMIFsRarpServerStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMIFsRarpServerTableMaxEntries ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsRarpClientRetransmissionTimeout ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIFsRarpClientMaxRetries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIFsRarpServerStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIFsRarpServerTableMaxEntries ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsRarpClientRetransmissionTimeout ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIFsRarpClientMaxRetries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIFsRarpServerStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIFsRarpServerTableMaxEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIFsRarpServerDatabaseTable. */
INT1
nmhValidateIndexInstanceFsMIFsRarpServerDatabaseTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIFsRarpServerDatabaseTable  */

INT1
nmhGetFirstIndexFsMIFsRarpServerDatabaseTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIFsRarpServerDatabaseTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsHardwareAddrLen ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIFsProtocolAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIFsEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsHardwareAddrLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIFsProtocolAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIFsEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsHardwareAddrLen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIFsProtocolAddress ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIFsEntryStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsRarpServerDatabaseTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIFsIpProxyArpSubnetOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIFsIpProxyArpSubnetOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIFsIpProxyArpSubnetOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIFsIpProxyArpSubnetOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
