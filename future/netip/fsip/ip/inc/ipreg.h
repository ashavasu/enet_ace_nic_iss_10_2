/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipreg.h,v 1.6 2015/10/26 13:43:28 siva Exp $
 *
 * Description: Application Registered parameters
 *
 *******************************************************************/
#ifndef __IP_IPREG_H__
#define __IP_IPREG_H__

#include "ipifdfs.h"
#include "ip.h"

#define   DEREGISTER            0
#define   REGISTER              1
#define   REGDOWN               0
#define   REGUP                 1
#define   HCT_UPDATE                   5
#define   HCT_DELETE                   6

#define   FUNCPTR    UINT4

#ifdef IPOA_WANTED
#define   IP_ATM_PHYS_INTERFACE_TYPE   CRU_ATM_PHYS_INTERFACE_TYPE 
#define   IP_ATM_HARDWARE_ADDR_LEN     CRU_ATM_HARDWARE_ADDR_LEN   
#endif

/*
 * This structre stores the various parameters registered by applications
 * while registering their protocol identifiers with IP.
 */

typedef struct _RegTblEntry
{
    VOID (*pAppRcv) (tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len, UINT2 u2Port,
                     tIP_INTERFACE InterfaceId, UINT1 u1Flag);
    VOID (*pIfStChg) (UINT4 u4Port, UINT4 u4BitMap);
    VOID (*pRtChg) (tRtInfo * pRtEntry, UINT4 u4BitMap);
    UINT2           u2RegCount;
    UINT2           u2AlignByte;    
} tRegTblEntry;

typedef struct _MCastProtoRegTblEntry
{
    VOID (*pAppRcv) (tIP_BUF_CHAIN_HEADER * pBuf);
    UINT1     u1RegFlag;
    UINT1     u1AlignmentByte;    /* 3 bytes to ensure a 4-byte boundary */
    UINT2     u2AlignmentByte;
} tMCastProtoRegTblEntry;

typedef struct _LowerLayerRegTblEntry
{
    UINT4     u4NumInterfaces;
              INT4 (*pLowerLayerWrite) (tIP_BUF_CHAIN_HEADER * pBuf);
              INT4 (*pGetHwAddr) (VOID);
    UINT1     u1InterfaceType;
    UINT1     u1RegFlag;
    UINT2     u2AlignmentByte;
} tLowerLayerRegTblEntry;

typedef struct _HLProtoRegnTbl
{
    VOID (*pIfStChng)     (tNetIpv4IfInfo *pNetIpIfInfo, UINT4 u4BitMap);
    VOID (*pRtChng)       (tNetIpv4RtInfo *pNetIpRtInfo, tNetIpv4RtInfo *pNetIpRtInfo1, UINT1 u1CmdType);
    VOID (*pProtoPktRecv) (tIP_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len,
                           UINT4 u4IfIndex, tIP_INTERFACE InterfaceId,
                           UINT1 u1Flag);
    UINT2                  u2RegCount;
    UINT2                  u2AlignByte;    
} tHLProtoRegnTbl;

extern tLowerLayerRegTblEntry aLowerLayerRegTable[IP_MAX_HW_TYPES];

#endif /* __IP_IPREG_H__ */
