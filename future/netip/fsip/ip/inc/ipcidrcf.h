/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipcidrcf.h,v 1.3 2009/08/24 13:11:37 prabuc Exp $
 *
 * Description: This File contains the CIDR configuration 
 *              information.
 *
 *******************************************************************/

#ifndef __IP_CIDR_H__
#define __IP_CIDR_H__

/* *************************************************************************
 * This structure maintains the Explicitly advertised routes information.  *
 * This maintains the status of each entry                                 *
 * *************************************************************************
 */

typedef struct _CidrExplRts
{
    tTMO_DLL_NODE NextExplRtNode;
    UINT4  u4ExplRtAddr;     /* The Explicitly advertised route prefix */
    UINT4  u4ExplRtMask;     /* The Explicitly advertised route's supernet  */ 
    INT1   i1ExplRtStatus;   /* The status for this entry */
    UINT1  u1AlignmentByte;  /* The Byte for Word Alignment */
    UINT2  u2AlignmentByte;  /* The Byte for Word Alignment */
} tCidrExplRts;
           
/* *************************************************************************
 * This structure maintains the Aggregate routes information.This maintains*
 * the status of each entry                                                *
 * *************************************************************************
 */

typedef struct _CidrAggrRtInfo
{
    tTMO_DLL_NODE NextAggRtNode;
    UINT4         u4AggrRtAddr;                /* The Aggregate route for the 
                                                * domain 
                                                */
    UINT4         u4AggrRtMask;                /* The Supernet mask for the 
                                                * domain 
                                                */
    tTMO_DLL      ExplRtList;

    UINT2         u2NumExpRts;                 /* The number of Valid 
                                                * Explicilty addvertised
                                                */ 
    UINT1         u1AggrRtStatus;              /* The Aggregate address status 
                                                * of this entry  
                                                */
    UINT1         u1AlignmentByte;             /* The Byte for Word Alignment */
} tCidrAggrRtInfo;

/* **************************************************************************
 * This structure maintains the CIDR Configuration Information, the list of *
 * aggregate routes for our domain                                          *
 * **************************************************************************
 */

/* Changed for system resizing */
typedef struct _CidrCfgInfo
{
    tCidrAggrRtInfo  *aAggrRts;    /* This holds the all the   
                                    * list of aggregate routes  
                                    */
} tCidrCfgInfo;

/* ***************************************************************************
 * This is a common structure used to retrieve the routing Info from TRIE    *
 * the order of the structure is very important and none shuld be changed    *
 * This is as per RFC 2096.                                                  *
 * We have added u4Gw and have not implemented RouteInfo desrcibed in RFC    *
 * 2096.                                                                     *
 * ***************************************************************************
 */
/* ************************************************************************
 * Macros for enabling and disabling the CIDR Global configuration        *
 * structure status                                                       *
 * ************************************************************************
 */

#define   IP_CIDR_AGGR_ENABLE     1
#define   IP_CIDR_AGGR_DISABLE    2

#define   IP_CIDR_EXPL_ENABLE     1
#define   IP_CIDR_EXPL_DISABLE    2


/* ************************************************************************
 * Macros for accesing the CIDR Global configuration structure            *
 * ************************************************************************
 */

#define  IP_CIDR_GET_AGGR_ADDR(u1AggIndex)   \
            gCidrGlbCfg.aAggrRts[u1AggIndex].u4AggrRtAddr

#define  IP_CIDR_GET_AGGR_MASK(u1AggIndex)   \
            gCidrGlbCfg.aAggrRts[u1AggIndex].u4AggrRtMask

#define  IP_CIDR_GET_AGGR_STATUS(u1AggIndex)  \
            gCidrGlbCfg.aAggrRts[u1AggIndex].u1AggrRtStatus

#define  IP_CIDR_GET_EXPL_ADDR(u1AggIndex, u1ExplIndex)   \
            gCidrGlbCfg.aAggrRts[u1AggIndex].aExplRts[u1ExplIndex].u4ExplRtAddr

#define  IP_CIDR_GET_EXPL_MASK(u1AggIndex, u1ExplIndex)   \
            gCidrGlbCfg.aAggrRts[u1AggIndex].aExplRts[u1ExplIndex].u4ExplRtMask

#define  IP_CIDR_GET_EXPL_STATUS(u1AggIndex, u1ExplIndex) \
           gCidrGlbCfg.aAggrRts[u1AggIndex].aExplRts[u1ExplIndex].i1ExplRtStatus

/* ************************************************************************
 * Macros for setting  the CIDR Global configuration structure            *
 * ************************************************************************
 */
#define  IP_CIDR_SET_AGGR_ADDR(u1AggIndex, u4Addr) \
             gCidrGlbCfg.aAggrRts[u1AggIndex].u4AggrRtAddr=u4Addr

#define  IP_CIDR_SET_AGGR_MASK(u1AggIndex, u4Mask) \
            gCidrGlbCfg.aAggrRts[u1AggIndex].u4AggrRtMask=u4Mask

#define  IP_CIDR_SET_AGGR_STATUS(u1AggIndex, u1Status) \
            gCidrGlbCfg.aAggrRts[u1AggIndex].u1AggrRtStatus=u1Status

#define  IP_CIDR_SET_EXPL_ADDR(u1AggIndex, u1ExplIndex, u4Addr)  \
           gCidrGlbCfg.aAggrRts[u1AggIndex].aExplRts[u1ExplIndex].u4ExplRtAddr \
           =u4Addr

#define  IP_CIDR_SET_EXPL_MASK(u1AggIndex, u1ExplIndex, u4Mask)  \
           gCidrGlbCfg.aAggrRts[u1AggIndex].aExplRts[u1ExplIndex].u4ExplRtMask \
           =u4Mask

#define  IP_CIDR_SET_EXPL_STATUS(u1AggIndex, u1ExplIndex, u1Status)  \
           gCidrGlbCfg.aAggrRts[u1AggIndex].aExplRts[u1ExplIndex] \
           .i1ExplRtStatus=u1Status

#define IP_CIDR_INC_EXPL_RTS(u1AggIndex) \
           gCidrGlbCfg.aAggrRts[u1AggIndex].u2NumExpRts++

#define IP_CIDR_DEC_EXPL_RTS(u1AggIndex) \
           gCidrGlbCfg.aAggrRts[u1AggIndex].u2NumExpRts--

/* ***********************************************************************
 * This set of Macros is used in RIP                                     *
 * ***********************************************************************
 */

#define THIS_AGGR_RT_IS_ENABLED(u4Indx) \
(gCidrGlbCfg.aAggrRts[(u4Indx)].u1AggrRtStatus == 1)

#define THIS_EXPL_RT_IS_ENABLED(u4AggIndx,u4ExplIndx) \
(gCidrGlbCfg.aAggrRts[(u4AggIndx)].aExplRts[(u4ExplIndx)].i1ExplRtStatus == 1)

#define AGGR_RT_MASK(u4Indx) gCidrGlbCfg.aAggrRts[(u4Indx)].u4AggrRtMask

#define AGGR_RT_ADDR(u4Indx) gCidrGlbCfg.aAggrRts[(u4Indx)].u4AggrRtAddr

#endif
