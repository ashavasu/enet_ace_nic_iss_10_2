/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipflttyp.h,v 1.2 2007/02/01 14:57:53 iss Exp $
 *
 * Description: IP filtering header file
 *
 *******************************************************************/
#ifndef   __IP_IPFILT_H__
#define   __IP_IPFILT_H__

#define   IP_FILT_MAX_LISTS    10

/*
 * Filter entry status.
 */

#define   IP_FILT_ALLOW                     2
#define   IP_FILT_BLOCK                     1
#define   IP_FILT_INVALID                   3

#define   IP_FILTERING_DISABLE              2
#define   IP_FILTERING_ENABLE               1

#define   IP_FILT_MIN_LIST_SIZE             1
#define   IP_FILT_DEF_LIST_SIZE            10
#define   IP_FILT_MAX_LIST_SIZE           100

#define   IP_FILT_DEF_SRC_MASK     0xffffffff
#define   IP_FILT_DEF_DEST_MASK    0xffffffff
#define   IP_FILT_DEF_STATUS                1

#endif            /*__IP_IPFILT_H__*/
