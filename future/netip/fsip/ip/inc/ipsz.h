/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipsz.h,v 1.3 2011/08/30 07:11:03 siva Exp $
 *
 * Description: This file contains mempool creation/deletion prototype
 *              declarations in netip module.
 *********************************************************************/
enum {
    MAX_IP_AGG_ROUTES_SIZING_ID,
    MAX_IP_BROADCAST_Q_DEPTH_SIZING_ID,
    MAX_IP_CIDR_EXPL_ROUTES_SIZING_ID,
    MAX_IP_CONTEXTS_SIZING_ID,
    MAX_IP_FRAG_PKTS_SIZING_ID,
    MAX_IP_IF_STATS_SIZING_ID,
    MAX_IP_IPIF_QUE_DEPTH_SIZING_ID,
    MAX_IP_LOGICAL_IFACES_SIZING_ID,
    MAX_IP_PMTU_ENTRIES_SIZING_ID,
    MAX_IP_REASM_LISTS_SIZING_ID,
#ifdef TUNNEL_WANTED
    MAX_IP_TUNNEL_INTERFACES_SIZING_ID,
#endif
    IP_MAX_SIZING_ID
};


#ifdef  _IPSZ_C
tMemPoolId IPMemPoolIds[ IP_MAX_SIZING_ID];
INT4  IpSizingMemCreateMemPools(VOID);
VOID  IpSizingMemDeleteMemPools(VOID);
INT4  IpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _IPSZ_C  */
extern tMemPoolId IPMemPoolIds[ ];
extern INT4  IpSizingMemCreateMemPools(VOID);
extern VOID  IpSizingMemDeleteMemPools(VOID);
extern INT4  IpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _IPSZ_C  */


#ifdef  _IPSZ_C
tFsModSizingParams FsIPSizingParams [] = {
{ "tCidrAggrRtInfo", "MAX_IP_AGG_ROUTES", sizeof(tCidrAggrRtInfo),MAX_IP_AGG_ROUTES, MAX_IP_AGG_ROUTES,0 },
{ "t_IF_LIST_NODE", "MAX_IP_BROADCAST_Q_DEPTH", sizeof(t_IF_LIST_NODE),MAX_IP_BROADCAST_Q_DEPTH, MAX_IP_BROADCAST_Q_DEPTH,0 },
{ "tCidrExplRts", "MAX_IP_CIDR_EXPL_ROUTES", sizeof(tCidrExplRts),MAX_IP_CIDR_EXPL_ROUTES, MAX_IP_CIDR_EXPL_ROUTES,0 },
{ "tIpCxt", "MAX_IP_CONTEXTS", sizeof(tIpCxt),MAX_IP_CONTEXTS, MAX_IP_CONTEXTS,0 },
{ "t_IP_FRAG", "MAX_IP_FRAG_PKTS", sizeof(t_IP_FRAG),MAX_IP_FRAG_PKTS, MAX_IP_FRAG_PKTS,0 },
{ "t_IP_IF_STAT", "MAX_IP_IF_STATS", sizeof(t_IP_IF_STAT),MAX_IP_IF_STATS, MAX_IP_IF_STATS,0 },
{ "tIfaceQMsg", "MAX_IP_IPIF_QUE_DEPTH", sizeof(tIfaceQMsg),MAX_IP_IPIF_QUE_DEPTH, MAX_IP_IPIF_QUE_DEPTH,0 },
{ "t_IP_IFACE_INFO", "MAX_IP_LOGICAL_IFACES", sizeof(t_IP_IFACE_INFO),MAX_IP_LOGICAL_IFACES, MAX_IP_LOGICAL_IFACES,0 },
{ "tPmtuInfo", "MAX_IP_PMTU_ENTRIES", sizeof(tPmtuInfo),MAX_IP_PMTU_ENTRIES, MAX_IP_PMTU_ENTRIES,0 },
{ "t_IP_REASM", "MAX_IP_REASM_LISTS", sizeof(t_IP_REASM),MAX_IP_REASM_LISTS, MAX_IP_REASM_LISTS,0 },
#ifdef TUNNEL_WANTED
{ "tIpTunlIf", "MAX_IP_TUNNEL_INTERFACES", sizeof(tIpTunlIf),MAX_IP_TUNNEL_INTERFACES, MAX_IP_TUNNEL_INTERFACES,0 },
#endif
{"\0","\0",0,0,0,0}
};
#else  /*  _IPSZ_C  */
extern tFsModSizingParams FsIPSizingParams [];
#endif /*  _IPSZ_C  */


