/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsipdb.h,v 1.9 2010/07/27 07:22:20 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSIPDB_H
#define _FSIPDB_H

UINT1 FsIpTraceConfigTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsIpTraceTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIpAddressTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsIpRtrLstTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsIpPathMtuTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIpCommonRoutingTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsIpifTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsCidrAggTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsCidrAdvertTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsIrdpIfConfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsRarpServerDatabaseTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};

UINT4 fsip [] ={1,3,6,1,4,1,2076,2};
tSNMP_OID_TYPE fsipOID = {8, fsip};


UINT4 FsIpInLengthErrors [ ] ={1,3,6,1,4,1,2076,2,1,1};
UINT4 FsIpInCksumErrors [ ] ={1,3,6,1,4,1,2076,2,1,2};
UINT4 FsIpInVersionErrors [ ] ={1,3,6,1,4,1,2076,2,1,3};
UINT4 FsIpInTTLErrors [ ] ={1,3,6,1,4,1,2076,2,1,4};
UINT4 FsIpInOptionErrors [ ] ={1,3,6,1,4,1,2076,2,1,5};
UINT4 FsIpInBroadCasts [ ] ={1,3,6,1,4,1,2076,2,1,6};
UINT4 FsIpOutGenErrors [ ] ={1,3,6,1,4,1,2076,2,1,7};
UINT4 FsIpOptProcEnable [ ] ={1,3,6,1,4,1,2076,2,1,9};
UINT4 FsIpNumMultipath [ ] ={1,3,6,1,4,1,2076,2,1,10};
UINT4 FsIpLoadShareEnable [ ] ={1,3,6,1,4,1,2076,2,1,11};
UINT4 FsIpEnablePMTUD [ ] ={1,3,6,1,4,1,2076,2,1,12};
UINT4 FsIpPmtuEntryAge [ ] ={1,3,6,1,4,1,2076,2,1,13};
UINT4 FsIpPmtuTableSize [ ] ={1,3,6,1,4,1,2076,2,1,14};
UINT4 FsIpProxyArpSubnetOption [ ] ={1,3,6,1,4,1,2076,2,1,15};
UINT4 FsIpTraceConfigDest [ ] ={1,3,6,1,4,1,2076,2,1,16,1,1};
UINT4 FsIpTraceConfigAdminStatus [ ] ={1,3,6,1,4,1,2076,2,1,16,1,2};
UINT4 FsIpTraceConfigMaxTTL [ ] ={1,3,6,1,4,1,2076,2,1,16,1,3};
UINT4 FsIpTraceConfigMinTTL [ ] ={1,3,6,1,4,1,2076,2,1,16,1,4};
UINT4 FsIpTraceConfigOperStatus [ ] ={1,3,6,1,4,1,2076,2,1,16,1,5};
UINT4 FsIpTraceConfigTimeout [ ] ={1,3,6,1,4,1,2076,2,1,16,1,6};
UINT4 FsIpTraceConfigMtu [ ] ={1,3,6,1,4,1,2076,2,1,16,1,7};
UINT4 FsIpTraceDest [ ] ={1,3,6,1,4,1,2076,2,1,17,1,1};
UINT4 FsIpTraceHopCount [ ] ={1,3,6,1,4,1,2076,2,1,17,1,2};
UINT4 FsIpTraceIntermHop [ ] ={1,3,6,1,4,1,2076,2,1,17,1,3};
UINT4 FsIpTraceReachTime1 [ ] ={1,3,6,1,4,1,2076,2,1,17,1,4};
UINT4 FsIpTraceReachTime2 [ ] ={1,3,6,1,4,1,2076,2,1,17,1,5};
UINT4 FsIpTraceReachTime3 [ ] ={1,3,6,1,4,1,2076,2,1,17,1,6};
UINT4 FsIpAddrTabIfaceId [ ] ={1,3,6,1,4,1,2076,2,1,18,1,1};
UINT4 FsIpAddrTabAddress [ ] ={1,3,6,1,4,1,2076,2,1,18,1,2};
UINT4 FsIpAddrTabAdvertise [ ] ={1,3,6,1,4,1,2076,2,1,18,1,3};
UINT4 FsIpAddrTabPreflevel [ ] ={1,3,6,1,4,1,2076,2,1,18,1,4};
UINT4 FsIpAddrTabStatus [ ] ={1,3,6,1,4,1,2076,2,1,18,1,5};
UINT4 FsIpRtrLstIface [ ] ={1,3,6,1,4,1,2076,2,1,19,1,1};
UINT4 FsIpRtrLstAddress [ ] ={1,3,6,1,4,1,2076,2,1,19,1,2};
UINT4 FsIpRtrLstPreflevel [ ] ={1,3,6,1,4,1,2076,2,1,19,1,3};
UINT4 FsIpRtrLstStatic [ ] ={1,3,6,1,4,1,2076,2,1,19,1,4};
UINT4 FsIpRtrLstStatus [ ] ={1,3,6,1,4,1,2076,2,1,19,1,5};
UINT4 FsIpPmtuDestination [ ] ={1,3,6,1,4,1,2076,2,1,20,1,1};
UINT4 FsIpPmtuTos [ ] ={1,3,6,1,4,1,2076,2,1,20,1,2};
UINT4 FsIpPathMtu [ ] ={1,3,6,1,4,1,2076,2,1,20,1,3};
UINT4 FsIpPmtuDisc [ ] ={1,3,6,1,4,1,2076,2,1,20,1,4};
UINT4 FsIpPmtuEntryStatus [ ] ={1,3,6,1,4,1,2076,2,1,20,1,5};
UINT4 FsIpRouteDest [ ] ={1,3,6,1,4,1,2076,2,1,22,1,1};
UINT4 FsIpRouteMask [ ] ={1,3,6,1,4,1,2076,2,1,22,1,2};
UINT4 FsIpRouteTos [ ] ={1,3,6,1,4,1,2076,2,1,22,1,3};
UINT4 FsIpRouteNextHop [ ] ={1,3,6,1,4,1,2076,2,1,22,1,4};
UINT4 FsIpRouteProto [ ] ={1,3,6,1,4,1,2076,2,1,22,1,5};
UINT4 FsIpRouteProtoInstanceId [ ] ={1,3,6,1,4,1,2076,2,1,22,1,6};
UINT4 FsIpRouteIfIndex [ ] ={1,3,6,1,4,1,2076,2,1,22,1,7};
UINT4 FsIpRouteType [ ] ={1,3,6,1,4,1,2076,2,1,22,1,8};
UINT4 FsIpRouteAge [ ] ={1,3,6,1,4,1,2076,2,1,22,1,9};
UINT4 FsIpRouteNextHopAS [ ] ={1,3,6,1,4,1,2076,2,1,22,1,10};
UINT4 FsIpRouteMetric1 [ ] ={1,3,6,1,4,1,2076,2,1,22,1,11};
UINT4 FsIpRoutePreference [ ] ={1,3,6,1,4,1,2076,2,1,22,1,12};
UINT4 FsIpRouteStatus [ ] ={1,3,6,1,4,1,2076,2,1,22,1,13};
UINT4 FsIpifIndex [ ] ={1,3,6,1,4,1,2076,2,1,23,1,1};
UINT4 FsIpifMaxReasmSize [ ] ={1,3,6,1,4,1,2076,2,1,23,1,2};
UINT4 FsIpifIcmpRedirectEnable [ ] ={1,3,6,1,4,1,2076,2,1,23,1,3};
UINT4 FsIpifDrtBcastFwdingEnable [ ] ={1,3,6,1,4,1,2076,2,1,23,1,4};
UINT4 FsIpifProxyArpAdminStatus [ ] ={1,3,6,1,4,1,2076,2,1,23,1,5};
UINT4 FsIpifLocalProxyArpAdminStatus [ ] ={1,3,6,1,4,1,2076,2,1,23,1,6};
UINT4 FsIcmpSendRedirectEnable [ ] ={1,3,6,1,4,1,2076,2,3,1};
UINT4 FsIcmpSendUnreachableEnable [ ] ={1,3,6,1,4,1,2076,2,3,2};
UINT4 FsIcmpSendEchoReplyEnable [ ] ={1,3,6,1,4,1,2076,2,3,3};
UINT4 FsIcmpNetMaskReplyEnable [ ] ={1,3,6,1,4,1,2076,2,3,4};
UINT4 FsIcmpTimeStampReplyEnable [ ] ={1,3,6,1,4,1,2076,2,3,5};
UINT4 FsIcmpInDomainNameRequests [ ] ={1,3,6,1,4,1,2076,2,3,6};
UINT4 FsIcmpInDomainNameReply [ ] ={1,3,6,1,4,1,2076,2,3,7};
UINT4 FsIcmpOutDomainNameRequests [ ] ={1,3,6,1,4,1,2076,2,3,8};
UINT4 FsIcmpOutDomainNameReply [ ] ={1,3,6,1,4,1,2076,2,3,9};
UINT4 FsIcmpDirectQueryEnable [ ] ={1,3,6,1,4,1,2076,2,3,10};
UINT4 FsDomainName [ ] ={1,3,6,1,4,1,2076,2,3,11};
UINT4 FsTimeToLive [ ] ={1,3,6,1,4,1,2076,2,3,12};
UINT4 FsIcmpInSecurityFailures [ ] ={1,3,6,1,4,1,2076,2,3,13};
UINT4 FsIcmpOutSecurityFailures [ ] ={1,3,6,1,4,1,2076,2,3,14};
UINT4 FsIcmpSendSecurityFailuresEnable [ ] ={1,3,6,1,4,1,2076,2,3,15};
UINT4 FsIcmpRecvSecurityFailuresEnable [ ] ={1,3,6,1,4,1,2076,2,3,16};
UINT4 FsUdpInNoCksum [ ] ={1,3,6,1,4,1,2076,2,4,1};
UINT4 FsUdpInIcmpErr [ ] ={1,3,6,1,4,1,2076,2,4,2};
UINT4 FsUdpInErrCksum [ ] ={1,3,6,1,4,1,2076,2,4,3};
UINT4 FsUdpInBcast [ ] ={1,3,6,1,4,1,2076,2,4,4};
UINT4 FsCidrAggAddress [ ] ={1,3,6,1,4,1,2076,2,5,1,1,1};
UINT4 FsCidrAggAddressMask [ ] ={1,3,6,1,4,1,2076,2,5,1,1,2};
UINT4 FsCidrAggStatus [ ] ={1,3,6,1,4,1,2076,2,5,1,1,3};
UINT4 FsCidrAdvertAddress [ ] ={1,3,6,1,4,1,2076,2,5,2,1,1};
UINT4 FsCidrAdvertAddressMask [ ] ={1,3,6,1,4,1,2076,2,5,2,1,2};
UINT4 FsCidrAdvertStatus [ ] ={1,3,6,1,4,1,2076,2,5,2,1,3};
UINT4 FsIrdpInAdvertisements [ ] ={1,3,6,1,4,1,2076,2,8,1};
UINT4 FsIrdpInSolicitations [ ] ={1,3,6,1,4,1,2076,2,8,2};
UINT4 FsIrdpOutAdvertisements [ ] ={1,3,6,1,4,1,2076,2,8,3};
UINT4 FsIrdpOutSolicitations [ ] ={1,3,6,1,4,1,2076,2,8,4};
UINT4 FsIrdpSendAdvertisementsEnable [ ] ={1,3,6,1,4,1,2076,2,8,5};
UINT4 FsIrdpIfConfIfNum [ ] ={1,3,6,1,4,1,2076,2,8,6,1,1};
UINT4 FsIrdpIfConfSubref [ ] ={1,3,6,1,4,1,2076,2,8,6,1,2};
UINT4 FsIrdpIfConfAdvertisementAddress [ ] ={1,3,6,1,4,1,2076,2,8,6,1,3};
UINT4 FsIrdpIfConfMaxAdvertisementInterval [ ] ={1,3,6,1,4,1,2076,2,8,6,1,4};
UINT4 FsIrdpIfConfMinAdvertisementInterval [ ] ={1,3,6,1,4,1,2076,2,8,6,1,5};
UINT4 FsIrdpIfConfAdvertisementLifetime [ ] ={1,3,6,1,4,1,2076,2,8,6,1,6};
UINT4 FsIrdpIfConfPerformRouterDiscovery [ ] ={1,3,6,1,4,1,2076,2,8,6,1,7};
UINT4 FsIrdpIfConfSolicitationAddress [ ] ={1,3,6,1,4,1,2076,2,8,6,1,8};
UINT4 FsRarpClientRetransmissionTimeout [ ] ={1,3,6,1,4,1,2076,2,9,1};
UINT4 FsRarpClientMaxRetries [ ] ={1,3,6,1,4,1,2076,2,9,2};
UINT4 FsRarpClientPktsDiscarded [ ] ={1,3,6,1,4,1,2076,2,9,3};
UINT4 FsRarpServerStatus [ ] ={1,3,6,1,4,1,2076,2,10,1};
UINT4 FsRarpServerPktsDiscarded [ ] ={1,3,6,1,4,1,2076,2,10,2};
UINT4 FsRarpServerTableMaxEntries [ ] ={1,3,6,1,4,1,2076,2,10,3};
UINT4 FsHardwareAddress [ ] ={1,3,6,1,4,1,2076,2,10,4,1,1};
UINT4 FsHardwareAddrLen [ ] ={1,3,6,1,4,1,2076,2,10,4,1,2};
UINT4 FsProtocolAddress [ ] ={1,3,6,1,4,1,2076,2,10,4,1,3};
UINT4 FsEntryStatus [ ] ={1,3,6,1,4,1,2076,2,10,4,1,4};
UINT4 FsNoOfStaticRoutes [ ] ={1,3,6,1,4,1,2076,2,16,1};
UINT4 FsNoOfAggregatedRoutes [ ] ={1,3,6,1,4,1,2076,2,16,2};
UINT4 FsNoOfRoutes [ ] ={1,3,6,1,4,1,2076,2,16,3};
UINT4 FsNoOfReassemblyLists [ ] ={1,3,6,1,4,1,2076,2,16,4};
UINT4 FsNoOfFragmentsPerList [ ] ={1,3,6,1,4,1,2076,2,16,5};
UINT4 FsIpGlobalDebug [ ] ={1,3,6,1,4,1,2076,2,17,1};


tMbDbEntry fsipMibEntry[]= {

{{10,FsIpInLengthErrors}, NULL, FsIpInLengthErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIpInCksumErrors}, NULL, FsIpInCksumErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIpInVersionErrors}, NULL, FsIpInVersionErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIpInTTLErrors}, NULL, FsIpInTTLErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIpInOptionErrors}, NULL, FsIpInOptionErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIpInBroadCasts}, NULL, FsIpInBroadCastsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIpOutGenErrors}, NULL, FsIpOutGenErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIpOptProcEnable}, NULL, FsIpOptProcEnableGet, FsIpOptProcEnableSet, FsIpOptProcEnableTest, FsIpOptProcEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsIpNumMultipath}, NULL, FsIpNumMultipathGet, FsIpNumMultipathSet, FsIpNumMultipathTest, FsIpNumMultipathDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsIpLoadShareEnable}, NULL, FsIpLoadShareEnableGet, FsIpLoadShareEnableSet, FsIpLoadShareEnableTest, FsIpLoadShareEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsIpEnablePMTUD}, NULL, FsIpEnablePMTUDGet, FsIpEnablePMTUDSet, FsIpEnablePMTUDTest, FsIpEnablePMTUDDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsIpPmtuEntryAge}, NULL, FsIpPmtuEntryAgeGet, FsIpPmtuEntryAgeSet, FsIpPmtuEntryAgeTest, FsIpPmtuEntryAgeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{10,FsIpPmtuTableSize}, NULL, FsIpPmtuTableSizeGet, FsIpPmtuTableSizeSet, FsIpPmtuTableSizeTest, FsIpPmtuTableSizeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{10,FsIpProxyArpSubnetOption}, NULL, FsIpProxyArpSubnetOptionGet, FsIpProxyArpSubnetOptionSet, FsIpProxyArpSubnetOptionTest, FsIpProxyArpSubnetOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsIpTraceConfigDest}, GetNextIndexFsIpTraceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpTraceConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsIpTraceConfigAdminStatus}, GetNextIndexFsIpTraceConfigTable, FsIpTraceConfigAdminStatusGet, FsIpTraceConfigAdminStatusSet, FsIpTraceConfigAdminStatusTest, FsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpTraceConfigTableINDEX, 1, 0, 0, "1"},

{{12,FsIpTraceConfigMaxTTL}, GetNextIndexFsIpTraceConfigTable, FsIpTraceConfigMaxTTLGet, FsIpTraceConfigMaxTTLSet, FsIpTraceConfigMaxTTLTest, FsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpTraceConfigTableINDEX, 1, 0, 0, "15"},

{{12,FsIpTraceConfigMinTTL}, GetNextIndexFsIpTraceConfigTable, FsIpTraceConfigMinTTLGet, FsIpTraceConfigMinTTLSet, FsIpTraceConfigMinTTLTest, FsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpTraceConfigTableINDEX, 1, 0, 0, "1"},

{{12,FsIpTraceConfigOperStatus}, GetNextIndexFsIpTraceConfigTable, FsIpTraceConfigOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIpTraceConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsIpTraceConfigTimeout}, GetNextIndexFsIpTraceConfigTable, FsIpTraceConfigTimeoutGet, FsIpTraceConfigTimeoutSet, FsIpTraceConfigTimeoutTest, FsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpTraceConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsIpTraceConfigMtu}, GetNextIndexFsIpTraceConfigTable, FsIpTraceConfigMtuGet, FsIpTraceConfigMtuSet, FsIpTraceConfigMtuTest, FsIpTraceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpTraceConfigTableINDEX, 1, 0, 0, NULL},

{{12,FsIpTraceDest}, GetNextIndexFsIpTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsIpTraceHopCount}, GetNextIndexFsIpTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsIpTraceIntermHop}, GetNextIndexFsIpTraceTable, FsIpTraceIntermHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsIpTraceReachTime1}, GetNextIndexFsIpTraceTable, FsIpTraceReachTime1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsIpTraceReachTime2}, GetNextIndexFsIpTraceTable, FsIpTraceReachTime2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsIpTraceReachTime3}, GetNextIndexFsIpTraceTable, FsIpTraceReachTime3Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpTraceTableINDEX, 2, 0, 0, NULL},

{{12,FsIpAddrTabIfaceId}, GetNextIndexFsIpAddressTable, FsIpAddrTabIfaceIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpAddressTableINDEX, 1, 0, 0, NULL},

{{12,FsIpAddrTabAddress}, GetNextIndexFsIpAddressTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpAddressTableINDEX, 1, 0, 0, NULL},

{{12,FsIpAddrTabAdvertise}, GetNextIndexFsIpAddressTable, FsIpAddrTabAdvertiseGet, FsIpAddrTabAdvertiseSet, FsIpAddrTabAdvertiseTest, FsIpAddressTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpAddressTableINDEX, 1, 0, 0, NULL},

{{12,FsIpAddrTabPreflevel}, GetNextIndexFsIpAddressTable, FsIpAddrTabPreflevelGet, FsIpAddrTabPreflevelSet, FsIpAddrTabPreflevelTest, FsIpAddressTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpAddressTableINDEX, 1, 0, 0, NULL},

{{12,FsIpAddrTabStatus}, GetNextIndexFsIpAddressTable, FsIpAddrTabStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIpAddressTableINDEX, 1, 0, 1, NULL},

{{12,FsIpRtrLstIface}, GetNextIndexFsIpRtrLstTable, FsIpRtrLstIfaceGet, FsIpRtrLstIfaceSet, FsIpRtrLstIfaceTest, FsIpRtrLstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpRtrLstTableINDEX, 1, 0, 0, NULL},

{{12,FsIpRtrLstAddress}, GetNextIndexFsIpRtrLstTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpRtrLstTableINDEX, 1, 0, 0, NULL},

{{12,FsIpRtrLstPreflevel}, GetNextIndexFsIpRtrLstTable, FsIpRtrLstPreflevelGet, FsIpRtrLstPreflevelSet, FsIpRtrLstPreflevelTest, FsIpRtrLstTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpRtrLstTableINDEX, 1, 0, 0, NULL},

{{12,FsIpRtrLstStatic}, GetNextIndexFsIpRtrLstTable, FsIpRtrLstStaticGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIpRtrLstTableINDEX, 1, 0, 0, NULL},

{{12,FsIpRtrLstStatus}, GetNextIndexFsIpRtrLstTable, FsIpRtrLstStatusGet, FsIpRtrLstStatusSet, FsIpRtrLstStatusTest, FsIpRtrLstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpRtrLstTableINDEX, 1, 0, 1, NULL},

{{12,FsIpPmtuDestination}, GetNextIndexFsIpPathMtuTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpPathMtuTableINDEX, 2, 0, 0, NULL},

{{12,FsIpPmtuTos}, GetNextIndexFsIpPathMtuTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIpPathMtuTableINDEX, 2, 0, 0, NULL},

{{12,FsIpPathMtu}, GetNextIndexFsIpPathMtuTable, FsIpPathMtuGet, FsIpPathMtuSet, FsIpPathMtuTest, FsIpPathMtuTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpPathMtuTableINDEX, 2, 0, 0, NULL},

{{12,FsIpPmtuDisc}, GetNextIndexFsIpPathMtuTable, FsIpPmtuDiscGet, FsIpPmtuDiscSet, FsIpPmtuDiscTest, FsIpPathMtuTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpPathMtuTableINDEX, 2, 0, 0, NULL},

{{12,FsIpPmtuEntryStatus}, GetNextIndexFsIpPathMtuTable, FsIpPmtuEntryStatusGet, FsIpPmtuEntryStatusSet, FsIpPmtuEntryStatusTest, FsIpPathMtuTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpPathMtuTableINDEX, 2, 0, 1, NULL},

{{12,FsIpRouteDest}, GetNextIndexFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteMask}, GetNextIndexFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteTos}, GetNextIndexFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteNextHop}, GetNextIndexFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteProto}, GetNextIndexFsIpCommonRoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteProtoInstanceId}, GetNextIndexFsIpCommonRoutingTable, FsIpRouteProtoInstanceIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteIfIndex}, GetNextIndexFsIpCommonRoutingTable, FsIpRouteIfIndexGet, FsIpRouteIfIndexSet, FsIpRouteIfIndexTest, FsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteType}, GetNextIndexFsIpCommonRoutingTable, FsIpRouteTypeGet, FsIpRouteTypeSet, FsIpRouteTypeTest, FsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteAge}, GetNextIndexFsIpCommonRoutingTable, FsIpRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpCommonRoutingTableINDEX, 5, 0, 0, "0"},

{{12,FsIpRouteNextHopAS}, GetNextIndexFsIpCommonRoutingTable, FsIpRouteNextHopASGet, FsIpRouteNextHopASSet, FsIpRouteNextHopASTest, FsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpCommonRoutingTableINDEX, 5, 0, 0, "0"},

{{12,FsIpRouteMetric1}, GetNextIndexFsIpCommonRoutingTable, FsIpRouteMetric1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIpCommonRoutingTableINDEX, 5, 0, 0, "-1"},

{{12,FsIpRoutePreference}, GetNextIndexFsIpCommonRoutingTable, FsIpRoutePreferenceGet, FsIpRoutePreferenceSet, FsIpRoutePreferenceTest, FsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpCommonRoutingTableINDEX, 5, 0, 0, NULL},

{{12,FsIpRouteStatus}, GetNextIndexFsIpCommonRoutingTable, FsIpRouteStatusGet, FsIpRouteStatusSet, FsIpRouteStatusTest, FsIpCommonRoutingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpCommonRoutingTableINDEX, 5, 0, 1, NULL},

{{12,FsIpifIndex}, GetNextIndexFsIpifTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsIpifMaxReasmSize}, GetNextIndexFsIpifTable, FsIpifMaxReasmSizeGet, FsIpifMaxReasmSizeSet, FsIpifMaxReasmSizeTest, FsIpifTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsIpifIcmpRedirectEnable}, GetNextIndexFsIpifTable, FsIpifIcmpRedirectEnableGet, FsIpifIcmpRedirectEnableSet, FsIpifIcmpRedirectEnableTest, FsIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsIpifDrtBcastFwdingEnable}, GetNextIndexFsIpifTable, FsIpifDrtBcastFwdingEnableGet, FsIpifDrtBcastFwdingEnableSet, FsIpifDrtBcastFwdingEnableTest, FsIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpifTableINDEX, 1, 0, 0, NULL},

{{12,FsIpifProxyArpAdminStatus}, GetNextIndexFsIpifTable, FsIpifProxyArpAdminStatusGet, FsIpifProxyArpAdminStatusSet, FsIpifProxyArpAdminStatusTest, FsIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpifTableINDEX, 1, 0, 0, "2"},

{{12,FsIpifLocalProxyArpAdminStatus}, GetNextIndexFsIpifTable, FsIpifLocalProxyArpAdminStatusGet, FsIpifLocalProxyArpAdminStatusSet, FsIpifLocalProxyArpAdminStatusTest, FsIpifTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIpifTableINDEX, 1, 0, 0, "2"},

{{10,FsIcmpSendRedirectEnable}, NULL, FsIcmpSendRedirectEnableGet, FsIcmpSendRedirectEnableSet, FsIcmpSendRedirectEnableTest, FsIcmpSendRedirectEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsIcmpSendUnreachableEnable}, NULL, FsIcmpSendUnreachableEnableGet, FsIcmpSendUnreachableEnableSet, FsIcmpSendUnreachableEnableTest, FsIcmpSendUnreachableEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsIcmpSendEchoReplyEnable}, NULL, FsIcmpSendEchoReplyEnableGet, FsIcmpSendEchoReplyEnableSet, FsIcmpSendEchoReplyEnableTest, FsIcmpSendEchoReplyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsIcmpNetMaskReplyEnable}, NULL, FsIcmpNetMaskReplyEnableGet, FsIcmpNetMaskReplyEnableSet, FsIcmpNetMaskReplyEnableTest, FsIcmpNetMaskReplyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsIcmpTimeStampReplyEnable}, NULL, FsIcmpTimeStampReplyEnableGet, FsIcmpTimeStampReplyEnableSet, FsIcmpTimeStampReplyEnableTest, FsIcmpTimeStampReplyEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsIcmpInDomainNameRequests}, NULL, FsIcmpInDomainNameRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIcmpInDomainNameReply}, NULL, FsIcmpInDomainNameReplyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIcmpOutDomainNameRequests}, NULL, FsIcmpOutDomainNameRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIcmpOutDomainNameReply}, NULL, FsIcmpOutDomainNameReplyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIcmpDirectQueryEnable}, NULL, FsIcmpDirectQueryEnableGet, FsIcmpDirectQueryEnableSet, FsIcmpDirectQueryEnableTest, FsIcmpDirectQueryEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsDomainName}, NULL, FsDomainNameGet, FsDomainNameSet, FsDomainNameTest, FsDomainNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsTimeToLive}, NULL, FsTimeToLiveGet, FsTimeToLiveSet, FsTimeToLiveTest, FsTimeToLiveDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIcmpInSecurityFailures}, NULL, FsIcmpInSecurityFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIcmpOutSecurityFailures}, NULL, FsIcmpOutSecurityFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIcmpSendSecurityFailuresEnable}, NULL, FsIcmpSendSecurityFailuresEnableGet, FsIcmpSendSecurityFailuresEnableSet, FsIcmpSendSecurityFailuresEnableTest, FsIcmpSendSecurityFailuresEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsIcmpRecvSecurityFailuresEnable}, NULL, FsIcmpRecvSecurityFailuresEnableGet, FsIcmpRecvSecurityFailuresEnableSet, FsIcmpRecvSecurityFailuresEnableTest, FsIcmpRecvSecurityFailuresEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsUdpInNoCksum}, NULL, FsUdpInNoCksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsUdpInIcmpErr}, NULL, FsUdpInIcmpErrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsUdpInErrCksum}, NULL, FsUdpInErrCksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsUdpInBcast}, NULL, FsUdpInBcastGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsCidrAggAddress}, GetNextIndexFsCidrAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsCidrAggTableINDEX, 2, 0, 0, NULL},

{{12,FsCidrAggAddressMask}, GetNextIndexFsCidrAggTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsCidrAggTableINDEX, 2, 0, 0, NULL},

{{12,FsCidrAggStatus}, GetNextIndexFsCidrAggTable, FsCidrAggStatusGet, FsCidrAggStatusSet, FsCidrAggStatusTest, FsCidrAggTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCidrAggTableINDEX, 2, 0, 1, NULL},

{{12,FsCidrAdvertAddress}, GetNextIndexFsCidrAdvertTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsCidrAdvertTableINDEX, 2, 0, 0, NULL},

{{12,FsCidrAdvertAddressMask}, GetNextIndexFsCidrAdvertTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsCidrAdvertTableINDEX, 2, 0, 0, NULL},

{{12,FsCidrAdvertStatus}, GetNextIndexFsCidrAdvertTable, FsCidrAdvertStatusGet, FsCidrAdvertStatusSet, FsCidrAdvertStatusTest, FsCidrAdvertTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsCidrAdvertTableINDEX, 2, 0, 1, NULL},

{{10,FsIrdpInAdvertisements}, NULL, FsIrdpInAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIrdpInSolicitations}, NULL, FsIrdpInSolicitationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIrdpOutAdvertisements}, NULL, FsIrdpOutAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIrdpOutSolicitations}, NULL, FsIrdpOutSolicitationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIrdpSendAdvertisementsEnable}, NULL, FsIrdpSendAdvertisementsEnableGet, FsIrdpSendAdvertisementsEnableSet, FsIrdpSendAdvertisementsEnableTest, FsIrdpSendAdvertisementsEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsIrdpIfConfIfNum}, GetNextIndexFsIrdpIfConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIrdpIfConfTableINDEX, 2, 0, 0, NULL},

{{12,FsIrdpIfConfSubref}, GetNextIndexFsIrdpIfConfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIrdpIfConfTableINDEX, 2, 0, 0, NULL},

{{12,FsIrdpIfConfAdvertisementAddress}, GetNextIndexFsIrdpIfConfTable, FsIrdpIfConfAdvertisementAddressGet, FsIrdpIfConfAdvertisementAddressSet, FsIrdpIfConfAdvertisementAddressTest, FsIrdpIfConfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsIrdpIfConfTableINDEX, 2, 0, 0, "3758096385"},

{{12,FsIrdpIfConfMaxAdvertisementInterval}, GetNextIndexFsIrdpIfConfTable, FsIrdpIfConfMaxAdvertisementIntervalGet, FsIrdpIfConfMaxAdvertisementIntervalSet, FsIrdpIfConfMaxAdvertisementIntervalTest, FsIrdpIfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIrdpIfConfTableINDEX, 2, 0, 0, "600"},

{{12,FsIrdpIfConfMinAdvertisementInterval}, GetNextIndexFsIrdpIfConfTable, FsIrdpIfConfMinAdvertisementIntervalGet, FsIrdpIfConfMinAdvertisementIntervalSet, FsIrdpIfConfMinAdvertisementIntervalTest, FsIrdpIfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIrdpIfConfTableINDEX, 2, 0, 0, "450"},

{{12,FsIrdpIfConfAdvertisementLifetime}, GetNextIndexFsIrdpIfConfTable, FsIrdpIfConfAdvertisementLifetimeGet, FsIrdpIfConfAdvertisementLifetimeSet, FsIrdpIfConfAdvertisementLifetimeTest, FsIrdpIfConfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIrdpIfConfTableINDEX, 2, 0, 0, "1800"},

{{12,FsIrdpIfConfPerformRouterDiscovery}, GetNextIndexFsIrdpIfConfTable, FsIrdpIfConfPerformRouterDiscoveryGet, FsIrdpIfConfPerformRouterDiscoverySet, FsIrdpIfConfPerformRouterDiscoveryTest, FsIrdpIfConfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIrdpIfConfTableINDEX, 2, 0, 0, "1"},

{{12,FsIrdpIfConfSolicitationAddress}, GetNextIndexFsIrdpIfConfTable, FsIrdpIfConfSolicitationAddressGet, FsIrdpIfConfSolicitationAddressSet, FsIrdpIfConfSolicitationAddressTest, FsIrdpIfConfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsIrdpIfConfTableINDEX, 2, 0, 0, "3758096386"},

{{10,FsRarpClientRetransmissionTimeout}, NULL, FsRarpClientRetransmissionTimeoutGet, FsRarpClientRetransmissionTimeoutSet, FsRarpClientRetransmissionTimeoutTest, FsRarpClientRetransmissionTimeoutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "100"},

{{10,FsRarpClientMaxRetries}, NULL, FsRarpClientMaxRetriesGet, FsRarpClientMaxRetriesSet, FsRarpClientMaxRetriesTest, FsRarpClientMaxRetriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "4"},

{{10,FsRarpClientPktsDiscarded}, NULL, FsRarpClientPktsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRarpServerStatus}, NULL, FsRarpServerStatusGet, FsRarpServerStatusSet, FsRarpServerStatusTest, FsRarpServerStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRarpServerPktsDiscarded}, NULL, FsRarpServerPktsDiscardedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRarpServerTableMaxEntries}, NULL, FsRarpServerTableMaxEntriesGet, FsRarpServerTableMaxEntriesSet, FsRarpServerTableMaxEntriesTest, FsRarpServerTableMaxEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsHardwareAddress}, GetNextIndexFsRarpServerDatabaseTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRarpServerDatabaseTableINDEX, 1, 0, 0, NULL},

{{12,FsHardwareAddrLen}, GetNextIndexFsRarpServerDatabaseTable, FsHardwareAddrLenGet, FsHardwareAddrLenSet, FsHardwareAddrLenTest, FsRarpServerDatabaseTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRarpServerDatabaseTableINDEX, 1, 0, 0, NULL},

{{12,FsProtocolAddress}, GetNextIndexFsRarpServerDatabaseTable, FsProtocolAddressGet, FsProtocolAddressSet, FsProtocolAddressTest, FsRarpServerDatabaseTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsRarpServerDatabaseTableINDEX, 1, 0, 0, NULL},

{{12,FsEntryStatus}, GetNextIndexFsRarpServerDatabaseTable, FsEntryStatusGet, FsEntryStatusSet, FsEntryStatusTest, FsRarpServerDatabaseTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRarpServerDatabaseTableINDEX, 1, 0, 1, NULL},

{{10,FsNoOfStaticRoutes}, NULL, FsNoOfStaticRoutesGet, FsNoOfStaticRoutesSet, FsNoOfStaticRoutesTest, FsNoOfStaticRoutesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsNoOfAggregatedRoutes}, NULL, FsNoOfAggregatedRoutesGet, FsNoOfAggregatedRoutesSet, FsNoOfAggregatedRoutesTest, FsNoOfAggregatedRoutesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsNoOfRoutes}, NULL, FsNoOfRoutesGet, FsNoOfRoutesSet, FsNoOfRoutesTest, FsNoOfRoutesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsNoOfReassemblyLists}, NULL, FsNoOfReassemblyListsGet, FsNoOfReassemblyListsSet, FsNoOfReassemblyListsTest, FsNoOfReassemblyListsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsNoOfFragmentsPerList}, NULL, FsNoOfFragmentsPerListGet, FsNoOfFragmentsPerListSet, FsNoOfFragmentsPerListTest, FsNoOfFragmentsPerListDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIpGlobalDebug}, NULL, FsIpGlobalDebugGet, FsIpGlobalDebugSet, FsIpGlobalDebugTest, FsIpGlobalDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsipEntry = { 116, fsipMibEntry };
#endif /* _FSIPDB_H */

