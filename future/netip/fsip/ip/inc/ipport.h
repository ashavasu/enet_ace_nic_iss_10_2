/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipport.h,v 1.10 2013/07/04 13:10:57 siva Exp $
 *
 * Description: Definitions that have to be changed when porting
 *
 *******************************************************************/
#ifndef   __IP_IPPORT_H__
#define   __IP_IPPORT_H__

#include "ipifdfs.h"

#define   IPFW_PACKET_ARRIVAL_Q_NAME               "IPQ"
#define   IPFW_CFA_INTERFACE_Q_NAME                "IFQ"
#define   IPFW_TASK_NAME                           "IPFW"

/*************************Comm with Higher layer *********************/
#define   IPGETFWDIFLIST    IgmpGetFwdIfList

#define   IP_DEF_NO_OF_INTERFACES              15
#define   IP_DEF_NO_OF_RIP_PEERS               60

/* MAX and MIN Values */
#define   IP_MIN_STATIC_ROUTES            20
#define   IP_MIN_AGGREGATED_ROUTES         5
#define   IP_MAX_REASSEMBLY_LISTS        150
#define   IP_MAX_FRAGMENTS_PER_LIST      300
#define   MAX_ADDR_LEN                    32
typedef tTimerListId tIpTmrListId;
typedef tTmrAppTimer tIpTimer;

#define   IP_TMR_SUCCESS                TMR_SUCCESS
#define   IP_TMR_FAILURE                TMR_FAILURE

#define   IP_TIMER_DATA(pTimer)    (pTimer)->u4Data

#define IP_CREATE_TMR_LIST(au1TaskName, u4Event, pTmrListId)  \
   TmrCreateTimerList((au1TaskName), (u4Event), NULL, (pTmrListId))

#define IP_START_TIMER(TimerListId, pTmrRef, u4Duration)  \
   TmrStartTimer((TimerListId), (pTmrRef), \
           ((UINT4)(u4Duration) * LR_NUM_OF_SYS_TIME_UNITS_IN_A_SEC))

#define IP_STOP_TIMER(TimerListId, pTmrRef)  \
   TmrStopTimer((TimerListId), (pTmrRef))

#define IP_RESIZE_TIMER(TimerListId, pTmrRef, u4Duration)   \
   TmrResizeTimer((TimerListId), (pTmrRef), (u4Duration))

#define IP_RESTART_TIMER(TimerListId, pTmrRef, u4Duration)   \
   {\
      TmrStopTimer((TimerListId), (pTmrRef));   \
      TmrStartTimer((TimerListId), (pTmrRef), \
           LR_NUM_OF_SYS_TIME_UNITS_IN_A_SEC * (u4Duration));   \
   }

#define IP_GET_EXPIRED_TIMERS(TimerListId, ppTmrRef)             \
   TmrGetExpiredTimers((TimerListId), (ppTmrRef))

#define IP_NEXT_TIMER(TimerListId)             \
   TmrGetNextExpiredTimer((TimerListId))

/************************ IP TO ARP ******************************/

#define IP_SET_HWADDR_VALID_FLAG(u4_iface_num, u1Flag) \
           (aEnet[u4_iface_num].u1HwAddrValidFlag = u1Flag)

#define IP_GET_PHYSICAL_ADDR(u1RegTabIndx, u1IfIndex, pu1HwAddr, pu1HwLength) \
        aLowerLayerRegTable[u1RegTabIndx].pGetHwAddr ((UINT1) (u1IfIndex), \
              (pu1HwAddr), (pu1HwLength))

#define IP_LOWER_LAYER_ADDRESS(u2Index) \
        IPIF_PHYS_ADDR(u2Index)

#define   IPFW_TASK_PRIORITY                 30
#define   IPFW_TASK_STACK_SIZE           0x8000
#define   IPFW_PACKET_ARRIVAL_Q_DEPTH    3000
#define   IPFW_CFA_INTERFACE_Q_DEPTH        1000
#define   IPFW_INTERFACE_Q_DEPTH            100
#define   IPFW_BROADCAST_Q_DEPTH            10
#define   IPFW_TASK_MODE                OSIX_DEFAULT_TASK_MODE 
#define   IPFW_CFA_PKT_ARRIVAL_Q_NAME   "IPCQ"                 
#define   IPFW_PACKET_ARRIVAL_Q_MODE    OSIX_GLOBAL            
#define   IPFW_CFA_INTERFACE_Q_MODE    OSIX_GLOBAL            

#define  CALLOC_IP_FRAG(size)                MEM_CALLOC(size, 1, t_IP_FRAG)
#define  FREE_IP_FRAG(pFrag)                 MEM_FREE(pFrag)

#define  CALLOC_IP_REASM(size)               MEM_CALLOC(size, 1, t_IP_REASM)
#define  FREE_IP_REASM(pReasm)               MEM_FREE(pReasm)

#define  CALLOC_IF_LIST_NODE(size)           MEM_CALLOC(size,1,t_IF_LIST_NODE)
#define  IP_FREE_BCAST_NODE(pIf_node)        MEM_FREE (pIf_node)

#define  CALLOC_MCAST_NODE(size)             MEM_CALLOC(size, 1, t_IP_MCAST)
#define  FREE_MCAST_NODE(pMcast)             MEM_FREE(pMcast)

#define  CALLOC_STATIC_TAB_LIST_ENTRY(size)  MEM_CALLOC(size, 1, t_ST_RT_TAB)
#define  FREE_STATIC_TAB_LIST_ENTRY(pStRt)   MEM_FREE(pStRt)

#define   IP_RT_CACHE_SEM_NAME      "IPRT"                
#define   IP_RT_CACHE_SEM_NODE_ID   SELF                  
#define   IP_RT_CACHE_SEM_COUNT     1                     
#define   IP_RT_CACHE_SEM_FLAGS     OSIX_DEFAULT_SEM_MODE 

/**********************IP to RIP **********************/
#endif            /*__IP_IPPORT_H__*/
