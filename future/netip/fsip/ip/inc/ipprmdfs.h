/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipprmdfs.h,v 1.4 2010/08/05 12:24:21 prabuc Exp $
 *
 * Description: Intertask Procedure parameters.
 *
 *******************************************************************/
#ifndef   __IP_IPPARMS_H__
#define   __IP_IPPARMS_H__

#define  IP_PARMS_ALLOC_IP_SEND_PARMS(pIpSendParms)             \
         IP_ALLOCATE_MEM_BLOCK(Ip_mems.ParmPoolId, &(pIpSendParms))

#define  IP_PARMS_RELEASE_IP_SEND_PARMS(pParms)\
         IP_RELEASE_MEM_BLOCK(Ip_mems.ParmPoolId, (pParms))

/*
 * This buffer is used to pass the parameters to the udp send routine.
 * See udptask.c, riptask.c
 */

#define  IP_PARMS_RELEASE_UDP_SEND_PARMS(pParms)\
         IP_RELEASE_MEM_BLOCK(Ip_mems.ParmPoolId, (pParms))

#define  IP_PARMS_ALLOC_IP_TO_HLMS_PARMS(pIpHlParms)               \
         IP_ALLOCATE_MEM_BLOCK(Ip_mems.ParmPoolId, &(pIpHlParms))

#define  IP_PARMS_RELEASE_IP_TO_HLMS_PARMS(pParms)\
         IP_RELEASE_MEM_BLOCK(Ip_mems.ParmPoolId, (pParms))

#define  IP_PARMS_ALLOC_UDP_TO_APP_MSG_PARMS(pUdpAppParms)          \
         IP_ALLOCATE_MEM_BLOCK(Ip_mems.ParmPoolId, &(pUdpAppParms))

#define  IP_PARMS_RELEASE_UDP_TO_APP_MSG_PARMS(pParms)\
         IP_RELEASE_MEM_BLOCK(Ip_mems.ParmPoolId, (pParms))

#define  IP_PARMS_ALLOC_ICMP_ERR_TO_HLMS_PARMS(pIcmpParms)          \
         IP_ALLOCATE_MEM_BLOCK(Ip_mems.ParmPoolId, &(pIcmpParms))

#define  IP_PARMS_RELEASE_ICMP_ERR_TO_HLMS_PARMS(pParms)\
         IP_RELEASE_MEM_BLOCK(Ip_mems.ParmPoolId, (pParms))

/*
 * When a high level protocol wants to send an ICMP message it does so by
 * sending the buffer along with these parameters.
 */
typedef struct
{
    UINT1     u1Cmd;
    INT1      i1Error_or_request; /* TRUE if Error ; FALSE  Otherwise */
    INT2      i2Align;            /* For 4 Byte Alignment */
    UINT4     u4ContextId;
    union
    {
        struct
        {
            UINT4     u4Parm1;
            UINT4     u4Parm2;
            INT1      i1Type;
            INT1      i1Code;
            INT2      i2Align;  /* For 4 Byte Alignment */
        } Err;
        struct
        {
            UINT4     u4Dest;
            UINT4     u4Parm1;
            UINT4     u4Parm2;
            UINT1     u1Tos;
            INT1      i1Type;
            INT1      i1Code;
            INT1      i1Align;  /* FOr 4 Byte Alignment */
        } Req;
    } Msg;
} t_ICMP_MSG_PARMS;

#define  IP_PARMS_ALLOC_ICMP_MSG_PARMS(pIcmpParms)               \
         IP_ALLOCATE_MEM_BLOCK(Ip_mems.ParmPoolId, &(pIcmpParms))

/*
 * These are the possible parameter exchanges between the IP tasks.
 * A module calling a procedure in other module should fill up the
 * parameter structure and assign the user_data field with the
 * pointer to the structure.  At the receiving end task file the parameters
 * are extracted and the appropriate procedures are called according
 * to source module ID and the destination module ID.
 *
 * The parameter structures contain only those which are not found in
 * the msg_info field of our tIP_BUF_CHAIN_HEADER.  If a module needs some fields
 * that are defined in the msg_info, the task file of the module gets it
 * from there.
 *
 * These message parameters are relevent only for those procedures which
 * need to be executed in the receiving task's context.  All other procedures
 * are called directly with the appropriate parameters.
 *
 * Logically a sending module should allocate a buffer for these parameter
 * message and the receiving task should release it after extracting the
 * parameters.  However the modules may use the existing unused msg_info space
 * instead of allocating and releasing.
 */
typedef union t_IP_TASK_PARMS_UNION
{
    t_IP_SEND_PARMS           Ip_send;           /* Send data to IP */
    t_UDP_SEND_PARMS          Udp_send;          /* Send data to UDP */
    t_IP_TO_HLMS_PARMS        Ip_to_hlm;         /* IP to high level */
    t_UDP_TO_APP_MSG_PARMS    Udp_to_app_msg;    /* Udp to Applications */
    t_ICMP_ERR_TO_HLMS_PARMS  Icmp_err_to_hlms;  /* Icmp error to Udp,TCP */
    t_ICMP_MSG_PARMS          Icmp_msg;          /* Icmp msg from hlms */
} t_IP_TASKS_PARMS_UNION;


#endif            /*__IP_IPPARMS_H__*/
