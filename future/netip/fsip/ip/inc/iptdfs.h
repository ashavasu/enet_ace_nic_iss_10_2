/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: iptdfs.h,v 1.5 2013/09/07 10:38:46 siva Exp $
 *
 * Description: Definitions used for IP-CFA Interface
 *
 *******************************************************************/

#ifndef   __IP_IPTDFS_H__
#define   __IP_IPTDFS_H__

struct sIpCxt
{
    t_IP_CFG             Ip_cfg;
    t_IP_STATS           Ip_stats;
    t_ICMP_ERR           Icmp_err;
    t_ICMP_STATS         Icmp_stats;
    t_ICMP_CFG           Icmp_cfg;
    tIP_DLL              Reasm_list;
    tIP_DLL              CidrAggRtList;
    INT4                 i4TimeToLive;
    UINT4                u4ContextId;
    UINT4                u4IpDbg;
    UINT2                u2PmtuStatusFlag;
    UINT2                u2PmtuCfgEntryAge;
    UINT1                au1DomainName [ICMP_MAX_DOMAIN_NAME_SIZE];
    UINT1                au1RegTable [IP_MAX_PROTOCOLS];
    UINT2                u2Pad;
};

typedef struct sIpGlobalInfo
{
    t_IP_MEMORY              Ip_mems;
    t_IP_TRAPS               Ip_traps;
    tIpTmrListId             IpTimerListId;
    tOsixSemId               IpDataSemId;
    tOsixSemId               IpProtSemId;
    tOsixSemId               IpCxtSemId;
    tOsixSemId               IpHLRegSemId;
    tOsixQId                 IpPktQId;
    tOsixQId                 IpCfaIfQId;
    tOsixTaskId              IpTaskId;
    tRegTblEntry             aProtoRegFn [IP_MAX_PROTOCOLS];
    tHLProtoRegnTbl          aHLProtoRegFn [IP_MAX_PROTOCOLS];
    tMCastProtoRegTblEntry   aMCastProtoRegTable [IP_MAX_PROTOCOLS];
    tLowerLayerRegTblEntry   aLowerLayerRegTable [IP_MAX_HW_TYPES];
    t_IP_IFACE_RECORD        Ipif_glbtab [IPIF_MAX_LOGICAL_IFACES + 1];
    t_IPIF_CONFIG            Ipif_config [IPIF_MAX_LOGICAL_IFACES + 1];
    tRBTree                  PmtuTblRoot;
    t_IP_TIMER               PmtuGarbageTimer;
    t_IP_TIMER               PmtuAgeingTimer;
    tIpCxt                  *apIpCxt [SYS_DEF_MAX_NUM_CONTEXTS];
    tIpCxt                  *pIpCxt;
    UINT4                    u4Ip4IfTableLastChange;
    UINT4                    u4IpDbg;
    UINT2                    u2FirstIpifIndex;
    INT1                     i1RegTableUp;
    UINT1                    u1PmtuEnableCount;
    UINT1                    u1AggRtEntryCount;
    UINT1                    u1ProxyArp_SubnetOption; /* Proxy ARP Subnet Check Enable/Disable Flag */
    UINT1                    u1Reserved[2];
}tIpGlobalInfo;

#endif
