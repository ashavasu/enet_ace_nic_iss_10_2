/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipcfaif.h,v 1.11 2014/08/23 11:57:55 siva Exp $
 *
 * Description: Definitions used for IP-CFA Interface 
 *
 *******************************************************************/
#ifndef  _IP_CFA_IF_H
#define _IP_CFA_IF_H

#define   IP_ETABLEFULL               -2
#define   IP_EMEMFAIL                 -3

#define   ADD_MULTI                    1
#define   DEL_MULTI                    3

#define   ALL_MULTI                    5
#define   PROCEED_SIGNAL      0x00000008
#define   IP_ENET_ADDR_LEN             6

/* IP Minimum Payload len over which IP header + payload length is validated against 
 * Total length in IP Header. */
#define   IP_MIN_PAYLOAD_LEN  CFA_ENET_MIN_UNTAGGED_FRAME_SIZE - CFA_ENET_V2_HEADER_SIZE


typedef struct _IPFWDIfaceQMsg {
  UINT4  u4Address;      
  UINT4  u4Netmask;      
  UINT4  u4BcastAddr;      
  UINT4  u4Mtu;      
  UINT4  u4IfSpeed;      
  UINT4  u4PeerAddr;      
  UINT4  u4ContextId;
  UINT2  u2Port;
  UINT2  u2IfIndex;      
  UINT2  u2VlanId;      
  UINT1  u1MsgType;
  UINT1  u1IfaceType;      
  UINT1  u1Persistence;      
  UINT1  u1EncapType;    
  UINT1  u1CxtChgType;
  UINT1  u1Pad;
}tIfaceQMsg;
#endif
