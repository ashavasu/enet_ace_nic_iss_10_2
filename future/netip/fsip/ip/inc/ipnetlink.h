/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ipnetlink.h,v 1.1 2016/03/04 11:09:19 siva Exp $
 *
 * Description: Contains Prototypes and Typedefs for Netlink Route.
 *
 *******************************************************************/

#ifndef   __IP_NETLINK_H__
#define   __IP_NETLINK_H__

#include "fssocket.h"
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#define REQ_BUFFER_SIZE      1024
#define   RT_FS_PROTO  20
#define FSIP_INET_AFI_IPV4  1
#define RTM_CHECK_ACK 2

/* Message structure. */
typedef struct message
{
   INT4                key;
   const char         *str;
} tLip6Message;

/* Socket interface to kernel */
typedef struct _netlinksock
{
    INT4                i4Sock;
    struct sockaddr_nl  snl;
}tNetLinkSock;

typedef struct _request
{
        struct nlmsghdr     NLMsgHdr;
        struct rtmsg        RtmMsg;
        UINT1               u1Buf[REQ_BUFFER_SIZE];
}tRequest;

INT4 RtmNetIpv4LeakRoute (UINT1 u1NetIpv4RtCmd , tRtInfo *pRtInfo);
INT4 NetLinkRouteModify (INT4 i4Cmd, tRtInfo * pRtInfo);
INT4 RtmNetlinkSockInit (tNetLinkSock * pNLSocket, UINT4 u4Groups);
INT4 RtmNetlinkGetRoute (tNetLinkSock * pNLSocket);
INT4    RtmIpMaskToPrefixLen (UINT4 u4Mask);
INT4    RtmIpMaskToPrefixLen (UINT4 u4Mask);
INT4    RtmNetlinkAddRtAttribute (struct rtattr *pRtAttr, INT4 i4MaxLen,
                                            INT4 i4Type, VOID *pData, INT4 i4Len);

extern INT4 RtmAddAttribute (struct nlmsghdr *n, INT4 maxlen, INT4 type,void *data, INT4 alen);
VOID    RtmSubTaskInit (VOID);
INT4    RtmSendToNetLink (struct nlmsghdr *n, tNetLinkSock *nl, UINT2 u2Afi);
INT4    RtmNetlinkSockInit (tNetLinkSock *, UINT4 u4Groups);
INT4    RtmNetlinkParseInfo (UINT1 u1InfoFlag , tNetLinkSock *nl,
                                     UINT2 u2Afi);
#endif


