/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: iptrace.h,v 1.8 2011/10/25 09:53:30 siva Exp $
 *
 * Description: IP Trace macros 
 *
 *******************************************************************/

#ifndef _IP_TRACE_H
#define _IP_TRACE_H

#define   IP_DBG_FLAG   gIpGlobalInfo.u4IpDbg 

#ifdef TRACE_WANTED

#define   IP_NAME       "IP"     
#define   RARP_NAME     "RARP"
#define   ICMP_NAME     "ICMP"   
#define   BOOTP_NAME    "BOOTP"  
#define   UDP_NAME      "UDP"    
#if MBSM_WANTED
#define   MBSM_IP       "MbsmIp"   
#endif


#define   IP_MOD_TRC       0x00010000
#define   ICMP_MOD_TRC     0x00020000    /* includes IRDP, PING */
#define   BOOTP_MOD_TRC    0x00200000    /* Includes BOOTP/TFTP */
#define   TRCRT_MOD_TRC    0x00400000
#define   UDP_MOD_TRC      0x00800000
#define   RARP_MOD_TRC     0x00100000

 /* General Macros */

#define   IP_CXT_DBG_FLAG(CxtId) IpUtilGetTraceFlag(CxtId)    
#define   CONTEXT_INFO(x, CxtId) "Context-%d:" x, CxtId

#define IP_PKT_DUMP( cmod, mask,  mod, pBuf, Length, fmt) \
        { if ((IP_DBG_FLAG & cmod) == cmod) {\
                     UtlTrcLog(IP_DBG_FLAG, mask , mod, fmt); \
             if ((IP_DBG_FLAG & mask) == DUMP_TRC)\
              DumpPkt(pBuf, Length); \
                  }\
        }

#define IP_CXT_PKT_DUMP(CxtId, cmod, mask,  mod, pBuf, Length, fmt) \
        { if ((IP_CXT_DBG_FLAG(CxtId) & cmod) == cmod) {\
                     UtlTrcLog(IP_CXT_DBG_FLAG(CxtId), mask , mod, CONTEXT_INFO(fmt, CxtId)); \
             if ((IP_CXT_DBG_FLAG(CxtId) & mask) == DUMP_TRC)\
              DumpPkt(pBuf, Length); \
                  }\
        }

#define IP_TRC(cmod, mask,mod,fmt) \
                { if ((IP_DBG_FLAG & cmod) == cmod) {\
                     UtlTrcLog(IP_DBG_FLAG, \
                                  mask,               \
                                  mod,         \
                                  fmt); \
                  }\
                }

#define IP_CXT_TRC(CxtId, cmod, mask,mod,fmt) \
                { if ((IP_CXT_DBG_FLAG(CxtId) & cmod) == cmod) {\
                     UtlTrcLog(IP_CXT_DBG_FLAG(CxtId), \
                                  mask,               \
                                  mod,         \
                                  CONTEXT_INFO(fmt, CxtId)); \
                  }\
                }

#define IP_TRC_ARG1(cmod,  mask,  mod, fmt, arg1) \
              { if ((IP_DBG_FLAG & cmod) == cmod) {\
           UtlTrcLog(IP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1); \
                }\
              }
#define IP_CXT_TRC_ARG1(CxtId, cmod,  mask,  mod, fmt, arg1) \
              { if ((IP_CXT_DBG_FLAG (CxtId) & cmod) == cmod) {\
           UtlTrcLog(IP_CXT_DBG_FLAG (CxtId),    \
            mask,                \
            mod,            \
            CONTEXT_INFO(fmt, CxtId),                \
            arg1); \
                }\
              }

#define IP_TRC_ARG2( cmod, mask, mod, fmt,arg1,arg2) \
              { if ((IP_DBG_FLAG & cmod) == cmod) {\
            UtlTrcLog(IP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1,                \
            arg2);\
                }\
             }

#define IP_CXT_TRC_ARG2(CxtId, cmod, mask, mod, fmt,arg1,arg2) \
              { if ((IP_CXT_DBG_FLAG(CxtId) & cmod) == cmod) {\
            UtlTrcLog(IP_CXT_DBG_FLAG(CxtId),    \
            mask,                \
            mod,            \
            CONTEXT_INFO(fmt,CxtId),                \
            arg1,                \
            arg2);\
                }\
             }

#define IP_TRC_ARG3( cmod, mask, mod, fmt,arg1,arg2,arg3) \
              { if ((IP_DBG_FLAG & cmod) == cmod) {\
            UtlTrcLog(IP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1,                \
            arg2,                \
            arg3);\
                }\
             }

#define IP_CXT_TRC_ARG3(CxtId, cmod, mask, mod, fmt,arg1,arg2,arg3) \
              { if ((IP_CXT_DBG_FLAG(CxtId) & cmod) == cmod) {\
            UtlTrcLog(IP_CXT_DBG_FLAG(CxtId),    \
            mask,                \
            mod,            \
            CONTEXT_INFO(fmt,CxtId),                \
            arg1,                \
            arg2,                \
            arg3);\
                }\
             }

#define IP_TRC_ARG4( cmod, mask, mod, fmt,arg1,arg2,arg3,arg4) \
              { if ((IP_DBG_FLAG & cmod) == cmod) {\
            UtlTrcLog(IP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4);\
                }\
             }

#define IP_CXT_TRC_ARG4(CxtId, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4) \
              { if ((IP_CXT_DBG_FLAG(CxtId) & cmod) == cmod) {\
            UtlTrcLog(IP_CXT_DBG_FLAG(CxtId),    \
            mask,                \
            mod,            \
            CONTEXT_INFO(fmt,CxtId),                \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4);\
                }\
             }

#define IP_TRC_ARG5( cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5) \
              { if ((IP_DBG_FLAG & cmod) == cmod) {\
            UtlTrcLog(IP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4,                \
            arg5);\
                }\
             }

#define IP_CXT_TRC_ARG5(CxtId, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5) \
              { if ((IP_CXT_DBG_FLAG(CxtId) & cmod) == cmod) {\
            UtlTrcLog(IP_CXT_DBG_FLAG(CxtId),    \
            mask,                \
            mod,            \
            CONTEXT_INFO(fmt,CxtId),                \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4,                \
            arg5);\
                }\
             }

#define IP_TRC_ARG6( cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6) \
              { if ((IP_DBG_FLAG & cmod) == cmod) {\
            UtlTrcLog(IP_DBG_FLAG,    \
            mask,                \
            mod,            \
            fmt,                \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4,                \
            arg5,                \
            arg6);\
                }\
             }

#define IP_CXT_TRC_ARG6(CxtId, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6) \
              { if ((IP_CXT_DBG_FLAG (CxtId) & cmod) == cmod) {\
            UtlTrcLog(IP_CXT_DBG_FLAG (CxtId),    \
            mask,                \
            mod,            \
            CONTEXT_INFO(fmt,CxtId),                \
            arg1,                \
            arg2,                \
            arg3,                \
            arg4,                \
            arg5,                \
            arg6);\
                }\
             }

#else

#define IP_PKT_DUMP( cmod, mask,  mod, pBuf, Length, fmt)
#define IP_TRC( cmod, mask, mod, fmt)
#define IP_TRC_ARG1( cmod, mask, mod, fmt,arg1)
#define IP_TRC_ARG2( cmod, mask, mod, fmt,arg1,arg2)
#define IP_TRC_ARG3( cmod, mask, mod, fmt,arg1,arg2,arg3)
#define IP_TRC_ARG4( cmod, mask, mod, fmt,arg1,arg2,arg3,arg4)
#define IP_TRC_ARG5( cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5)
#define IP_TRC_ARG6( cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#define IP_CXT_PKT_DUMP( CxtId, cmod, mask,  mod, pBuf, Length, fmt)
#define IP_CXT_TRC( CxtId, cmod, mask, mod, fmt)
#define IP_CXT_TRC_ARG1( CxtId, cmod, mask, mod, fmt,arg1)
#define IP_CXT_TRC_ARG2( CxtId, cmod, mask, mod, fmt,arg1,arg2)
#define IP_CXT_TRC_ARG3( CxtId, cmod, mask, mod, fmt,arg1,arg2,arg3)
#define IP_CXT_TRC_ARG4( CxtId, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4)
#define IP_CXT_TRC_ARG5( CxtId, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5)
#define IP_CXT_TRC_ARG6( CxtId, cmod, mask, mod, fmt,arg1,arg2,arg3,arg4,arg5,arg6)

#endif /* DEBUG_IP */
#endif /* _IP_TRACE_H_ */
