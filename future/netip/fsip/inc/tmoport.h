/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmoport.h,v 1.6 2015/03/30 06:14:14 siva Exp $
 *
 * Description: Contains Functions and macros related to TMO.
 *
 *******************************************************************/

#ifndef _TMOPORT_H
#define _TMOPORT_H

#define   IP_LOWER_DOWN                       CRU_DOWN                       
#define   LR_NUM_OF_SYS_TIME_UNITS_IN_A_SEC   SYS_NUM_OF_TIME_UNITS_IN_A_SEC 
#define   IPFW_EVENT_WAIT_FLAGS               (OSIX_WAIT | OSIX_EV_ANY)      

#define   IPFW_EVENT_WAIT_TIMEOUT             0
#define   IPFWD_TIMER_EXPIRY_EVENT            2
#define   IPFWD_CFA_INTERFACE_EVENT           8
#define   IPFWD_VCM_CONTEXT_EVENT             4 
#define   IPFW_VCM_INTERFACE_EVENT           16
#define   IPFWD_IPV4_ENABLE_EVENT            12
#define   IPFWD_PROXYARP_ADMIN_STATUS_EVENT  32

#define   UDP_ERROR         IP_FAILURE      
#define   UDP_OK            IP_SUCCESS      
#define   UDP_NOT_OK        IP_FAILURE      

    /****** End of UDP MODULE MACROS ********/

        /******* Start of TRCRT MODULE MACROS ********/
#define   UDP_SLL_Add(pList,pNode)        TMO_SLL_Add(pList,pNode)      
#define   UDP_SLL_Count(pList)            TMO_SLL_Count(pList)          
#define   UDP_SLL_Nth(pList,u4_NodeNum)   TMO_SLL_Nth(pList,u4_NodeNum) 
#define   UDP_SLL_Delete(pList,pNode)     TMO_SLL_Delete(pList,pNode)   
#define   UDP_SLL_Clear(pList)            TMO_SLL_Clear(pList)          
#define   UDP_SLL_Init(pList)             TMO_SLL_Init(pList)           
#define   tUDP_SLL_NODE                   tTMO_SLL_NODE                 
#define   tUDP_SLL                        tTMO_SLL                      

       /******* End of TRCRT MODULE MACROS ********/

#define   IP_MALLOC(u2Size, UINT1)   MEM_MALLOC(u2Size, UINT1) 
#define   IP_FREE(pu1Base)           MEM_FREE(pu1Base)         
/******************  IP     MACROS         ************************/
#define   IP_OK                     IP_SUCCESS               
#define   IP_NOT_OK                 IP_FAILURE               
#define   tIP_SLL                   tTMO_SLL                
#define   IP_SLL_Next(x,y)          TMO_SLL_Next(x,y)
#define   IP_SLL_Scan(x, y, z)      TMO_SLL_Scan(x, y, z)    
#define   IP_SLL_Init(x)            TMO_SLL_Init(x)          
#define   IP_SLL_First(x)           TMO_SLL_First(x)         
#define   IP_SLL_Add(x, y)          TMO_SLL_Add(x, y)        
#define   IP_SLL_Insert(x, y, z)    TMO_SLL_Insert(x, y, z)  
#define   IP_SLL_Last(x)            TMO_SLL_Last(x)          
#define   IP_SLL_Delete(x, y)       TMO_SLL_Delete(x, y)     
#define   IP_SLL_Replace(x, y, z)   TMO_SLL_Replace(x, y, z) 
#define   IP_SLL_Previous(x, y)     TMO_SLL_Previous(x, y)   
#define   tIP_DLL                   tTMO_DLL                 
#define   tIP_DLL_NODE              tTMO_DLL_NODE            
#define   IP_DLL_Init(x)            TMO_DLL_Init(x)          
#define   IP_DLL_Scan(x, y, z)      TMO_DLL_Scan(x, y, z)    
#define   IP_DLL_Next(x, y)         TMO_DLL_Next(x, y)       
#define   IP_DLL_Delete(x, y)       TMO_DLL_Delete(x, y)     
#define   IP_DLL_Count(x)           TMO_DLL_Count(x)         
#define   IP_DLL_Insert(x, y, z)    TMO_DLL_Insert(x, y, z)  
#define   IP_DLL_First(x)           TMO_DLL_First(x)         
#define   IP_DLL_Add(x, y)          TMO_DLL_Add(x, y)        
#define   IP_SLL_Count(x)           TMO_SLL_Count(x)         
#define   IP_SLL_Get(x)             TMO_SLL_Get(x)           
#define   IP_DLL_Init_Node(x)       TMO_DLL_Init_Node(x)     
#define   IP_DLL_Last(x)            TMO_DLL_Last(x)          
#define   IP_ERROR                  IP_FAILURE               
/******************************************************************/

/*****   Start of RARP Macros  ******/
#define RARP_SLL_Scan(pList, pNode, TypeCast) \
                  TMO_SLL_Scan(pList, pNode, TypeCast)

#define   RARP_SLL_Init(pList)            TMO_SLL_Init(pList)          
#define   RARP_SLL_Count(pList)           TMO_SLL_Count(pList)         
#define   RARP_SLL_Add(pList, pNode)      TMO_SLL_Add(pList, pNode)    
#define   RARP_SLL_Delete(pList, pNode)   TMO_SLL_Delete(pList, pNode) 
#define   RARP_SLL_Init_Node(pNode)       TMO_SLL_Init_Node(pNode)     
#define   RARP_SLL_First(pList)           TMO_SLL_First(pList)         
#define   RARP_SLL_Next(pList, pNode)     TMO_SLL_Next(pList, pNode)   
#define   tRARP_SLL                       tTMO_SLL                     
#define   tRARP_SLL_NODE                  tTMO_SLL_NODE                
#define   RARP_FREE(Ptr)                  MEM_FREE(Ptr)                
/***** End of RARP macros ******/

#define   tBOOTP_SLL                 tTMO_SLL            
#define   tBOOTP_SLL_NODE            tTMO_SLL_NODE       
#define   BOOTP_SLL_INIT(pList)      TMO_SLL_Init(pList) 
#define   BOOTP_SLL_SCAN             TMO_SLL_Scan        
#define   BOOTP_SLL_COUNT            TMO_SLL_Count       
#define   BOOTP_SLL_FIRST            TMO_SLL_First       
#define   BOOTP_SLL_NEXT             TMO_SLL_Next        
#define   BOOTP_SLL_ADD              TMO_SLL_Add         
#define   BOOTP_SLL_DELETE           TMO_SLL_Delete      
 
#define   TFTP_WRITE_DATA_TO_FILE    FileWrite
#define   TFTP_READ_DATA_FROM_FILE   FileRead
#define   FILE_OPEN                  FileOpen
#define   FILE_CLOSE                 FileClose

#define TFTP_START_TIMER(TimerListId, pTmrRef, u4Duration)             \
   TmrStartTimer((TimerListId), (pTmrRef), (u4Duration))

#define TFTP_STOP_TIMER(TimerListId, pTmrRef)             \
   TmrStopTimer((TimerListId), (pTmrRef))

#define TFTP_GET_EXPIRED_TIMERS(TimerListId, ppTmrRef)             \
   TmrGetExpiredTimers((TimerListId), (ppTmrRef))

#define TFTP_NEXT_TIMER(TimerListId)             \
   TmrGetNextExpiredTimer(TimerListId)

#define   BOOTP_MALLOC(Size, Type)   MEM_MALLOC(Size, Type)  
#define   BOOTP_FREE(Ptr)            MEM_FREE(Ptr)           
#define   BOOTP_MAX_INTERFACES       IPIF_MAX_LOGICAL_IFACES 
#define   GET_SYSTEM_NAME            nmhGetSysName           

#endif /*_TMOPORT_H*/
