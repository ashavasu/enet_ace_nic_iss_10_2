/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: other.h,v 1.6 2009/10/12 15:18:27 prabuc Exp $
 *
 * Description:   Other global definitions 
 *
 *******************************************************************/
#ifndef   __IP_OTHER_H__
#define   __IP_OTHER_H__

#define   IP_PROTOCOL                     0x0800
#define   IP_PROTOCOL_ADDR_LEN                 4
#define   IP_ETH_BCAST_ADDR       0xffffffffffff

#define   ICMP_PTCL                            1
#define   IGMP_PTCL                            2
#define   TCP_PTCL                             6
#define   UDP_PTCL                            17

#define   OSPF_ALL_SPF_ROUTERS        0xE0000005
#define   OSPF_ALL_D_ROUTERS          0xE0000006

#define   RIP_2_ALL_ROUTERS           0xE0000009

#define   UDP_PING_SUB_MODULE               0x50
#define   IP_ICMP_SUB_MODULE                0x51
#define   IP_TRACE_MODULE                   0x52

#define   FOREVER                      for (;;)           
#define   CRU_BUF_NEXT_MESSAGE(pBuf)   (pBuf->pNextChain) 

#define   PKT_TYPE_IP     1

#define   IP_TCP_SUCCESS     0    /* TCP_SUCCESS */
#define   IP_TCP_FAILURE    -1    /* TCP_FAILURE */

#define   IP_TCP_RECV(pIfMsg)        /* IP_TCP_FAILURE */ TcpRecv(pIfMsg)

#define   IP_OSPF_SUCCESS     0    /* OSPF_SUCCESS */
#define   IP_OSPF_FAILURE    -1    /* OSPF_FAILURE */

#define   IP_OSPF_RECV(pIfMsg)    OspfRecv(pIfMsg)

#define   APPLICATION_PROTOCOLS    8
#define   IP_PROTOCOL_ID           2

#define IP_INIT_COMPLETE(u4Status)	lrInitComplete(u4Status)

#if defined (MPLS_WANTED) || (MPLS_RSVPTE_WANTED)   
#define RSVP_DEST_PORT          1698   
#endif 

#define   IP_TIMESTAMP_VAL      0x80000000L

#define   IP_OPTIONS_LEN        44

#endif          /*__IP_OTHER_H__*/
