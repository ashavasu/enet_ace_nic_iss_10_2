/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cruport.h,v 1.11 2016/10/03 10:34:43 siva Exp $
 *
 * Description: Equivalent CRU function/macro calls.
 *
 *******************************************************************/
#ifndef   __IP_CRUPORT_H__
#define   __IP_CRUPORT_H__

#define   IP_MAC_HDR_LEN              34
#define   IP_MAX_HW_TYPES             10
#define   IP_BUF_SUCCESS     CRU_SUCCESS

#define IP_CONCAT_BUFS(pBuf1, pBuf2)                             \
   CRU_BUF_Concat_MsgBufChains((pBuf1), (pBuf2))

#define IP_FRAGMENT_BUF(pBuf, u4Offset, ppFragBuf)               \
   CRU_BUF_Fragment_BufChain((pBuf), (u4Offset), (ppFragBuf))

#define IP_PREPEND_BUF(pBuf, pSrc, u4Size)                       \
   CRU_BUF_Prepend_BufChain((pBuf), (pSrc), (u4Size))

#define IP_BUF_MOVE_BACK_VALID_OFFSET(pBuf, u4Size)              \
   CRU_BUF_Prepend_BufChain((pBuf), NULL, (u4Size))

#define IP_DUPLICATE_BUF(pBuf)                                   \
   CRU_BUF_Duplicate_BufChain((pBuf))

#define IP_LINK_BUFS(pBuf1, pBuf2)                               \
   CRU_BUF_Link_BufChains((pBuf1), (pBuf2))

#define IP_UNLINK_BUFS(pBuf)                                     \
   CRU_BUF_UnLink_BufChain((pBuf))

#define IP_GET_DATA_PTR_IF_LINEAR(pBuf, u4Offset, u4NumBytes)    \
   CRU_BUF_Get_DataPtr_IfLinear((pBuf), (u4Offset), (u4NumBytes))

#define IP_DELETE_BUF_AT_END(pBuf, u4Size)                       \
   CRU_BUF_Delete_BufChainAtEnd((pBuf), (u4Size))

/* Interface ID Macros */

#define IP_GET_MODULE_DATA(pMsg)                       \
   CRU_BUF_Get_ModuleData((pMsg))

#define IP_GET_SRC_MODULE_ID(pMsg)                     \
   CRU_BUF_Get_SourceModuleId((pMsg))

#define IP_GET_DST_MODULE_ID(pMsg)                     \
   CRU_BUF_Get_DestinModuleId((pMsg))

#define IP_GET_IFACE_ID(pMsg)                          \
   CRU_BUF_Get_InterfaceId((pMsg))

#define IP_SET_SRC_MODULE_ID(pMsg, u1ModuleId)         \
   CRU_BUF_Set_SourceModuleId((pMsg), (u1ModuleId))

#define IP_SET_DST_MODULE_ID(pMsg, u1ModuleId)         \
   CRU_BUF_Set_DestinModuleId((pMsg), (u1ModuleId))

#define IP_SET_IFACE_ID(pMsg, tInterface)              \
   CRU_BUF_Set_InterfaceId((pMsg), (tInterface))

#define IP_GET_IFACE_TYPE(tInterface)                  \
   CRU_BUF_Get_Interface_Type((tInterface))

#define IP_GET_IFACE_NUM(tInterface)                   \
   CRU_BUF_Get_Interface_Num((tInterface))

#define IP_GET_IF_INDEX(tInterface)                    \
   CRU_BUF_Get_IfIndex((tInterface))

#define IP_GET_IFACE_SUBREF(tInterface)                \
   CRU_BUF_Get_Interface_SubRef((tInterface))

#define IP_SET_IFACE_TYPE(tInterface)                  \
   CRU_BUF_Set_Interface_Type((tInterface))

#define IP_SET_IFACE_NUM(tInterface, IfNum)                   \
   CRU_BUF_Set_Interface_Num((tInterface, IfNum))

#define IP_SET_IF_INDEX(tInterface, IfIndex)                   \
   CRU_BUF_Set_IfIndex(tInterface, IfIndex)

#define IP_SET_IFACE_SUBREF(tInterface)                \
   CRU_BUF_Set_Interface_SubRef((tInterface))

/* This is updated to avoid multiple definitions in TELNET - Ramki */
#define   TMO_HTONL    IP_HTONL
#define   TMO_HTONS    IP_HTONS
#define   TMO_NTOHL    IP_NTOHL
#define   TMO_NTOHS    IP_NTOHS

#define   CRU_BUF_CONCAT_MSGS           IP_CONCAT_BUFS           
#define   CRU_BUF_COPY_FROM_MSG         IP_COPY_FROM_BUF         
#define   CRU_BUF_COPY_TO_MSG           IP_COPY_TO_BUF           
#define   CRU_BUF_DUP_MSG               IP_DUPLICATE_BUF         
#define   CRU_BUF_FRAG_MSG              IP_FRAGMENT_BUF          
#define   CRU_BUF_DELETE_MSG_AT_START   IP_BUF_MOVE_VALID_OFFSET 
#define   CRU_BUF_DELETE_MSG_AT_END     IP_DELETE_BUF_AT_END     
#define   CRU_BUF_LINK_MSG              IP_LINK_BUFS             
#define   CRU_BUF_UNLINK_MSG            IP_UNLINK_BUFS           

#define   CRU_BUF_GET_MSG            IP_ALLOCATE_BUF               
#define   CRU_BUF_RELEASE_MSG        IP_RELEASE_BUF                
#define   IP_RELEASE_BUFFER(pBuf)    IP_RELEASE_BUF((pBuf), FALSE) 
#define   IP_GET_DATA_LENGTH         IP_GET_BUF_LENGTH             

/* command related */
#define   FROM_CFA_TO_IP                    1

#define   IP_PACKET_FROM_HIGHER_LAYER       3
#define   PHYSICAL_INTERFACE_DOWN           4
#define   PHYSICAL_INTERFACE_UP             5
#define   ICMP_PACKET_FROM_HIGHER_LAYER     6
#define   ICMP_ERROR_FROM_HIGHER_LAYER      7
#define   FROM_IP_TO_APPLNS                 8
#define   FROM_APPLN_TO_IP                  9
#define   FROM_IP_TO_TRCRT                 10
#define   FROM_ICMP_TO_TRCRT_ERR           11
#define   FROM_ICMP_TO_APPLN               12
#define   FROM_RAW_SLI_MODULE              14
#define   LIMITED_BROADCAST_ADDRESS        0xFFFFFFFF

#ifdef DHCPC_WANTED
#define   FROM_DHCPC_TO_CFA                15
#endif
#define   IP_MAX_ENET_IFACES               IPIF_MAX_LOGICAL_IFACES 


/* Taken from CRU coz. used in CFA_entry_exists, no idea y */
#define   CRU_SRN_DLCI    0x05    /*** FRMRLY DLCI       ***/
#define   CRU_SRN_INV     0x07    /*** Invalid SubReferen***/

 /* though, related to physical interface it is define in CRU */

 /* should not use CRU chained buffer size less  than this */
#define   IP_MIN_SIZE_BUFFER    4

/* for link layer */
#define   IP_LOWER_UP   CRU_UP            
#define   LINK_UCAST    1
#define   LINK_MCAST    2
#define   LINK_BCAST    3

#define IP_SET_CMD(pBuf, u2Cmd)\
        CRU_BMC_Set_Command(pBuf, u2Cmd)

#define UDP_SET_CMD(pBuf, u2Cmd)\
        CRU_BMC_Set_Command(pBuf, u2Cmd)

#define IP_GET_PHYS_ADVT_TYPE(pBuf) \
        CRU_BMC_Get_McastFlag(pBuf)

#define IP_SET_LOGICAL_PORT(pBuf, u4IfIndex) \
        CRU_BMC_Set_logicalIndex(pBuf, u4IfIndex)

#define IP_GET_LOGICAL_PORT(pBuf) \
        CRU_BMC_Get_LogicalIndex(pBuf)

#define IP_SET_PROTOCOL(pBuf, u2ProtId) \
        CRU_BMC_Set_Protocol(pBuf, u2ProtId)

#define IP_ALLOC_BUFFER(u4Size, u4OffSet)\
        CRU_BUF_Allocate_MsgBufChain (u4Size, u4OffSet)

#define IP_SET_INTERFACE(pBuf, iFace) \
        CRU_BMC_Set_InterfaceId(pBuf, iFace);

#define IP_IS_IFID_EQUAL(pIfId1,pIfId2) \
              IP_Are_Interface_IDs_Equal(pIfId1,pIfId2)

#define   IP_MEM_SUCCESS    MEM_SUCCESS
#define   IP_MEM_FAILURE    MEM_FAILURE

/* Macros for accessing the IfMsg structures */
#define   IP_IFMSG_MSG_TYPE(pIfMsg)     (pIfMsg)->u2MsgType     
#define   IP_IFMSG_PRIMITIVE(pIfMsg)    (pIfMsg)->u2Primitive   
#define   IP_IFMSG_PROTO_ID(pIfMsg)     (pIfMsg)->u4ProtocolId  
#define   IP_IFMSG_DIRECTION(pIfMsg)    (pIfMsg)->u2Direction   
#define   IP_IFMSG_IFID(pIfMsg)         (pIfMsg)->IfId          
#define   IP_IFMSG_DATA(pIfMsg)         (pIfMsg)->pData         
#define   IP_IFMSG_DATALEN(pIfMsg)      (pIfMsg)->u4DataLen     
#define   IP_IFMSG_PROTO_INFO(pIfMsg)   (pIfMsg)->pProtocolInfo 
#define   IP_IFMSG_REL_PROTO(pIfMsg)    (pIfMsg)->ReleaseFunc   

#define   IP_IFMSG_SUCCESS    IFMSG_SUCCESS  
#define   IP_IFMSG_FAILURE    IFMSG_FAILURE  

#define   IP_IFMSG_DIR_DOWN   IFMSG_DIR_DOWN 
#define   IP_IFMSG_DIR_UP     IFMSG_DIR_UP   

#define   IP_ALLOCATE_IF_MSG(ppIfMsg)    IfMsgAllocateIfMsg((ppIfMsg))

#define IP_RELEASE_IF_MSG(pIfMsg)                              \
   if (IP_IFMSG_PROTO_INFO(pIfMsg)) {                          \
      IP_IFMSG_REL_PROTO(pIfMsg)(IP_IFMSG_PROTO_INFO(pIfMsg)); \
   }                                                           \
   IfMsgReleaseIfMsg(pIfMsg);

#define IP_ALLOCATE_PROT_INFO(ppProt) \
   IP_ALLOCATE_MEM_BLOCK(Ip_mems.ParmPoolId, (ppProt))

/* Interface Message Types */
#define   IP_MSG_DATA       IFMSG_TYPE_DATA
#define   IP_MSG_CONTROL    IFMSG_TYPE_CTRL

/* Control message primitives */
#define   IP_IF_STAT_CHANGE     3
#define   IP_IF_ROUTE_CHANGE    4
#define   IP_LAYER2_DATA        5
#define   IP_APP_DATA           8
#define   ETH_ADDR_LEN          6

/****** Start of UDP MODULE's Mappings  ***********/
#define   tUDP_BUF_CHAIN_HEADER   tCRU_BUF_CHAIN_HEADER 
#define   tUDP_INTERFACE          tCRU_INTERFACE        

#define  IP_BMC_Set_ReadOffsetZero(pBufChain)  \
           CRU_BMC_Set_ReadOffsetZero(pBufChain)

/****** End of UDP MODULE's Mappings ********/

/******* Start of TRCRT MODULE Mappings  ******/
#define  UDP_BUF_GET_MSG(u4_Size,u4_ValidDataOffset)   \
           CRU_BUF_Allocate_MsgBufChain(u4_Size,u4_ValidDataOffset)

#define  UDP_BUF_Set_CharInBufChain(pBuf,u1CharValue,u4Offset,u4RepeatCount)  \
          CRU_BUF_Set_CharInBufChain(pBuf,u1CharValue,u4Offset,u4RepeatCount)

      /******** End of TRCRT MODULE Mappings   *****/

#define   CRU_MAX_ENCAPS_ADDRESS_LENGTH    22



/******************  IP     MACROS         ************************/

#define   IP_LOWER_MODULE                   CRU_COMMON_FORWARD_MODULE        
#define   IP_OSPF_TASK_MAIN_FUNC()          CRU_OSPF_TASK_MAIN_FUNC()        
#define   IP_TSRV_MAIN_TASK_FUNC()          CRU_TSRV_MAIN_TASK_FUNC()        
#define   IP_DUMP_INC_ALL_PKT(x, y)         CRU_IP_DUMP_INC_ALL_PKT(x, y)    
#define   IP_BUF_RELEASE_MSG(pBuf, FALSE)   CRU_BUF_RELEASE_MSG(pBuf, FALSE) 
#define   IP_PROTOCOL_FOR_LOWER_LAYER       CRU_IP                           
#define   IP_FWD_TASK_ID                    CRU_IP_FWD_TASK_ID 
#define   IP_GET_INTERFACE_NUM(InterfaceId)  \
          CRU_GET_INTERFACE_NUM(InterfaceId)
#define   IP_GET_INTERFACE_TYPE(InterfaceId) \
          CRU_GET_INTERFACE_TYPE(InterfaceId)
#define   IP_GET_INTERFACE_VCNUM(InterfaceId) \
          CRU_GET_INTERFACE_VCNUM(InterfaceId)
#define   IP_BUF_UnLink_BufChains(p_msg1)   CRU_BUF_UnLink_BufChains(p_msg1) 
#define   IP_IS_INTERFACE_ID_INVALID(x)     CRU_IS_INTERFACE_ID_INVALID(x)   

#define   IP_Compare_IfIds(x, y) \
          (IP_GET_IF_INDEX((x)) - IP_GET_IF_INDEX((y)))

#define   IP_L3SUBIF_TYPE                 IP_IF_TYPE_L3SUBIF
#define   IP_ENET_TYPE                    CRU_ENET_IP_TYPE              
#define   IP_BUF_SET_READ_OFFSET(x, y)    CRU_BUF_SET_READ_OFFSET(x, y) 
#define   IP_MEM_CREATE_BUFFER_POOL       CRU_MEM_CREATE_BUFFER_POOL    
#define   IP_IS_ENET_INTERFACE(x)         CRU_IS_ENET_INTERFACE(x)      
#define   IP_SRN_DLCI                     CRU_SRN_DLCI                  
#define   IP_SRN_INV                      CRU_SRN_INV                   
#define   IP_ENET_PHYS_INTERFACE_TYPE     IP_IF_TYPE_ETHERNET           
#define   IP_OTHER_PHYS_INTERFACE_TYPE     IP_IF_TYPE_OTHER
#define   IP_L2VLAN_PHYS_INTERFACE_TYPE   IP_IF_TYPE_L2VLAN
#define   IP_L3IPVLAN_PHYS_INTERFACE_TYPE IP_IF_TYPE_L3IPVLAN
#define   IP_LAGG_INTERFACE_TYPE          IP_IF_TYPE_LAGG
#define   IP_PSEUDO_WIRE_INTERFACE_TYPE   IP_IF_TYPE_PSEUDO_WIRE
#define   IP_ENET_TYPE_ENCAPSULATION      CRU_ENET_TYPE_ENCAPSULATION   
#define   IP_MAX_ENET_ADDR_LEN            CRU_MAX_ENET_ADDR_LEN         
#define   IP_FRMRL_PHYS_INTERFACE_TYPE    CRU_FRMRL_PHYS_INTERFACE_TYPE 
#define   IP_WAN_ENCAPSULATION            CRU_WAN_ENCAPSULATION         
#define   IP_X25_PHYS_INTERFACE_TYPE      CRU_X25_PHYS_INTERFACE_TYPE   
#define   IP_BUF_MSG_PARAMS(pBuf)         CRU_BUF_MSG_PARAMS(pBuf)      

#ifdef SLI_WANTED
#define  IP_PKT_ASSIGN_ID(pBuf,u2Id) \
         {\
            UINT2 u2Val;\
            u2Val = IP_HTONS((u2Id));\
            IP_COPY_TO_BUF (pBuf, &u2Val, IP_PKT_OFF_ID, sizeof(UINT2));\
         }
#endif

#define   MALLOC_IP_RT_CACHE_ENTRY(pu1Blk)  \
          IP_ALLOCATE_MEM_BLOCK(Ip_mems.IpRtChPoolId, &pu1Blk)

#define   IP_RT_CACHE_FREE(pRt) \
          IP_RELEASE_MEM_BLOCK(Ip_mems.IpRtChPoolId, (pRt))

#define   TRACE_INFO_CALLOC(size)      MEM_CALLOC(size,1,t_TRACE_INFO) 
#define   TRACE_INFO_FREE(pTrc_info)   MEM_FREE(pTrc_info)             

#define   IP_FILT_MALLOC()          \
          MEM_MALLOC(sizeof(t_IP_FILTER), t_IP_FILTER)
#define   IP_FILTER_FREE(pIp_filter)      \
          MEM_FREE(pIp_filter)

#define   MALLOC(a)   MEM_MALLOC(a, VOID) 

/******************************************************************/

    /***** Start of IPIF Macros ******/

#define   IP_PPP_PHYS_INTERFACE_TYPE   IP_IF_TYPE_PPP 

#define   IPIF_INVALIDATE_IF_ID(pIfId)\
          (IP_GET_IFACE_TYPE((*pIfId)) = IP_IF_TYPE_INVALID)

#define   IPIF_IS_INVALID_ID(IfId) \
          (IP_GET_IFACE_TYPE((IfId)) == IP_IF_TYPE_INVALID)

#define   CALLOC_IPIF_ADDR()\
          MEM_CALLOC(sizeof (t_IP_ADDR_RECORD), 1, t_IP_ADDR_RECORD)

#define   CALLOC_IPIF_INFO()\
          MEM_CALLOC(sizeof (t_IP_IFACE_INFO), 1, t_IP_IFACE_INFO)

#define   CALLOC_IPIF_STAT()\
          MEM_CALLOC(sizeof (t_IP_IF_STAT), 1, t_IP_IF_STAT)

#define   FREE_IPIF_ADDR(pIp)     MEM_FREE(pIp);   pIp = NULL;   
#define   FREE_IPIF_INFO(pIf)     MEM_FREE(pIf);   pIf = NULL;   
#define   FREE_IPIF_STAT(pStat)   MEM_FREE(pStat); pStat = NULL; 

   /****** End of IPIF Macros *****/

    /***** Start of PING Macros ******/

   /****** End of PING Macros *****/

/* UTILITY MACROS */
/* These three changes are done to overcome the compilation error got in Debian
 * Machines.
 */
#ifdef OFFSET
#undef OFFSET
#endif
#define   OFFSET(x,y)    ((UINT4)(&(((x *)0)->y)))
#ifdef IN_CLASSD
#undef IN_CLASSD
#endif
#define   IN_CLASSD(a)    ((((long unsigned int) (a)) & 0xf0000000) == 0xe0000000)

/*  Start of RARP Module mappings */

#define   tRARP_BUF_CHAIN_HEADER           tCRU_BUF_CHAIN_HEADER         
#define   RARP_RELEASE_BUFFER(pBuf)        IP_RELEASE_BUF((pBuf), FALSE) 
#define   RARP_BUF_GET_MSG                 IP_ALLOCATE_BUF               
#define   RARP_MAX_ENCAPS_ADDRESS_LENGTH   CRU_MAX_ENCAPS_ADDRESS_LENGTH 
#define   tRARP_INTERFACE                  tCRU_INTERFACE                

typedef tMemPoolId tBootpMemPoolId;

#define   BOOTP_MEM_SUCCESS   MEM_SUCCESS 
#define   BOOTP_MEM_FAILURE   MEM_FAILURE 

#endif          /*__IP_CRUPORT_H__*/
