/* $Id: fspingwr.c,v 1.4 2014/11/03 12:29:40 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "fspinglw.h"
# include  "fspingwr.h"
# include  "fspingdb.h"
# include "pinginc.h"

INT4
GetNextIndexFsPingTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSPING ()
{
    SNMPRegisterMibWithLock (&fspingOID, &fspingEntry, PingLock, PingUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fspingOID, (const UINT1 *) "fsping");
}

VOID
UnRegisterFSPING ()
{
    SNMPUnRegisterMib (&fspingOID, &fspingEntry);
    SNMPDelSysorEntry (&fspingOID, (const UINT1 *) "fsping");
}

INT4
FsPingDestGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingDest (pMultiIndex->pIndex[0].i4_SLongValue,
                              &(pMultiData->u4_ULongValue)));

}

INT4
FsPingTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsPingTriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingTries (pMultiIndex->pIndex[0].i4_SLongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
FsPingDataSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingDataSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsPingStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                &(pMultiData->i4_SLongValue)));

}

INT4
FsPingSendCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingSendCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsPingAverageTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingAverageTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsPingMaxTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingMaxTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsPingMinTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingMinTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
FsPingSuccessesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingSuccesses (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->u4_ULongValue)));

}

INT4
FsPingEntryStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPingEntryStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4 FsPingHostNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsPingTable(
                pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsPingHostName(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->pOctetStrValue));

}

INT4
FsPingDestSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPingDest (pMultiIndex->pIndex[0].i4_SLongValue,
                              pMultiData->u4_ULongValue));

}

INT4
FsPingTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPingTimeout (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->i4_SLongValue));

}

INT4
FsPingTriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPingTries (pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiData->i4_SLongValue));

}

INT4
FsPingDataSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPingDataSize (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsPingEntryStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPingEntryStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4 FsPingHostNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsPingHostName(
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->pOctetStrValue));

}

INT4
FsPingDestTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsPingDest (pu4Error,
                                 pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiData->u4_ULongValue));

}

INT4
FsPingTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    return (nmhTestv2FsPingTimeout (pu4Error,
                                    pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
FsPingTriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2FsPingTries (pu4Error,
                                  pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsPingDataSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsPingDataSize (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsPingEntryStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsPingEntryStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4 FsPingHostNameTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2FsPingHostName(pu4Error,
                pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->pOctetStrValue));

}

INT4
FsPingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPingTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
