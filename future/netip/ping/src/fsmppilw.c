/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmppilw.c,v 1.4 2014/11/03 12:29:40 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmppilw.h"
# include  "fspinglw.h"
# include  "pinginc.h"

/* LOW LEVEL Routines for Table : FsMIPingTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsMIPingTable
Input       :  The Indices
FsMIPingIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIPingTable (INT4 i4FsMIPingIndex)
{
    if (nmhValidateIndexInstanceFsPingTable (i4FsMIPingIndex) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsMIPingTable
Input       :  The Indices
FsMIPingIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIPingTable (INT4 *pi4FsMIPingIndex)
{
    if (nmhGetFirstIndexFsPingTable (pi4FsMIPingIndex) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetNextIndexFsMIPingTable
Input       :  The Indices
FsMIPingIndex
nextFsMIPingIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIPingTable (INT4 i4FsMIPingIndex, INT4 *pi4NextFsMIPingIndex)
{
    if (nmhGetNextIndexFsPingTable (i4FsMIPingIndex,
                                    pi4NextFsMIPingIndex) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsMIPingDest
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingDest
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingDest (INT4 i4FsMIPingIndex, UINT4 *pu4RetValFsMIPingDest)
{
    if (nmhGetFsPingDest (i4FsMIPingIndex, pu4RetValFsMIPingDest) !=
        SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingContextId
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingContextId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingContextId (INT4 i4FsMIPingIndex, INT4 *pi4RetValFsMIPingContextId)
{
    t_PING             *pPing;

    if ((pPing = PingGetInstance (i4FsMIPingIndex)) != NULL)
    {
        *pi4RetValFsMIPingContextId = (INT4) pPing->u4ContextId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFsMIPingTimeout
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingTimeout
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingTimeout (INT4 i4FsMIPingIndex, INT4 *pi4RetValFsMIPingTimeout)
{
    if (nmhGetFsPingTimeout (i4FsMIPingIndex,
                             pi4RetValFsMIPingTimeout) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingTries
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingTries
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingTries (INT4 i4FsMIPingIndex, INT4 *pi4RetValFsMIPingTries)
{
    if (nmhGetFsPingTries (i4FsMIPingIndex, pi4RetValFsMIPingTries) !=
        SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingDataSize
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingDataSize
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingDataSize (INT4 i4FsMIPingIndex, INT4 *pi4RetValFsMIPingDataSize)
{
    if (nmhGetFsPingDataSize (i4FsMIPingIndex,
                              pi4RetValFsMIPingDataSize) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingStatus
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingStatus (INT4 i4FsMIPingIndex, INT4 *pi4RetValFsMIPingStatus)
{
    if (nmhGetFsPingStatus (i4FsMIPingIndex, pi4RetValFsMIPingStatus) !=
        SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingSendCount
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingSendCount
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingSendCount (INT4 i4FsMIPingIndex, INT4 *pi4RetValFsMIPingSendCount)
{
    if (nmhGetFsPingSendCount (i4FsMIPingIndex,
                               pi4RetValFsMIPingSendCount) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingAverageTime
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingAverageTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingAverageTime (INT4 i4FsMIPingIndex,
                           INT4 *pi4RetValFsMIPingAverageTime)
{
    if (nmhGetFsPingAverageTime (i4FsMIPingIndex,
                                 pi4RetValFsMIPingAverageTime) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingMaxTime
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingMaxTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingMaxTime (INT4 i4FsMIPingIndex, INT4 *pi4RetValFsMIPingMaxTime)
{
    if (nmhGetFsPingMaxTime (i4FsMIPingIndex,
                             pi4RetValFsMIPingMaxTime) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingMinTime
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingMinTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingMinTime (INT4 i4FsMIPingIndex, INT4 *pi4RetValFsMIPingMinTime)
{
    if (nmhGetFsPingMinTime (i4FsMIPingIndex,
                             pi4RetValFsMIPingMinTime) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingSuccesses
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingSuccesses
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingSuccesses (INT4 i4FsMIPingIndex,
                         UINT4 *pu4RetValFsMIPingSuccesses)
{
    if (nmhGetFsPingSuccesses (i4FsMIPingIndex,
                               pu4RetValFsMIPingSuccesses) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhGetFsMIPingEntryStatus
Input       :  The Indices
FsMIPingIndex

The Object 
retValFsMIPingEntryStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMIPingEntryStatus (INT4 i4FsMIPingIndex,
                           INT4 *pi4RetValFsMIPingEntryStatus)
{
    if (nmhGetFsPingEntryStatus (i4FsMIPingIndex,
                                 pi4RetValFsMIPingEntryStatus) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetFsMIPingHostName
 Input       :  The Indices
                FsMIPingIndex

                The Object
                retValFsMIPingHostName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIPingHostName(INT4 i4FsMIPingIndex , 
                       tSNMP_OCTET_STRING_TYPE * pRetValFsMIPingHostName)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhGetFsPingHostName (i4FsMIPingIndex,
                                     pRetValFsMIPingHostName);

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsMIPingDest
Input       :  The Indices
FsMIPingIndex

The Object 
setValFsMIPingDest
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIPingDest (INT4 i4FsMIPingIndex, UINT4 u4SetValFsMIPingDest)
{
    if (nmhSetFsPingDest (i4FsMIPingIndex, u4SetValFsMIPingDest) !=
        SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhSetFsMIPingContextId
Input       :  The Indices
FsMIPingIndex

The Object 
setValFsMIPingContextId
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIPingContextId (INT4 i4FsMIPingIndex, INT4 i4SetValFsMIPingContextId)
{
    t_PING             *pPing;

    if ((pPing = PingGetInstance (i4FsMIPingIndex)) != NULL)
    {
        /* Ping Entry can not be updated when Ping is in progress */
        if (pPing->i1Oper != (INT1) PING_OPER_IN_PROGRESS)
        {
            pPing->u4ContextId = (UINT4) i4SetValFsMIPingContextId;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhSetFsMIPingTimeout
Input       :  The Indices
FsMIPingIndex

The Object 
setValFsMIPingTimeout
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIPingTimeout (INT4 i4FsMIPingIndex, INT4 i4SetValFsMIPingTimeout)
{
    if (nmhSetFsPingTimeout (i4FsMIPingIndex,
                             i4SetValFsMIPingTimeout) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhSetFsMIPingTries
Input       :  The Indices
FsMIPingIndex

The Object 
setValFsMIPingTries
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIPingTries (INT4 i4FsMIPingIndex, INT4 i4SetValFsMIPingTries)
{
    if (nmhSetFsPingTries (i4FsMIPingIndex, i4SetValFsMIPingTries) !=
        SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhSetFsMIPingDataSize
Input       :  The Indices
FsMIPingIndex

The Object 
setValFsMIPingDataSize
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIPingDataSize (INT4 i4FsMIPingIndex, INT4 i4SetValFsMIPingDataSize)
{
    if (nmhSetFsPingDataSize (i4FsMIPingIndex,
                              i4SetValFsMIPingDataSize) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhSetFsMIPingEntryStatus
Input       :  The Indices
FsMIPingIndex

The Object 
setValFsMIPingEntryStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMIPingEntryStatus (INT4 i4FsMIPingIndex,
                           INT4 i4SetValFsMIPingEntryStatus)
{
    if (nmhSetFsPingEntryStatus (i4FsMIPingIndex,
                                 i4SetValFsMIPingEntryStatus) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhSetFsMIPingHostName
 Input       :  The Indices
                FsMIPingIndex

                The Object
                setValFsMIPingHostName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIPingHostName(INT4 i4FsMIPingIndex , 
                            tSNMP_OCTET_STRING_TYPE *pSetValFsMIPingHostName)
{

    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhSetFsPingHostName (i4FsMIPingIndex,
                                     pSetValFsMIPingHostName);

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsMIPingDest
Input       :  The Indices
FsMIPingIndex

The Object 
testValFsMIPingDest
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIPingDest (UINT4 *pu4ErrorCode,
                       INT4 i4FsMIPingIndex, UINT4 u4TestValFsMIPingDest)
{
    if (nmhTestv2FsPingDest (pu4ErrorCode,
                             i4FsMIPingIndex,
                             u4TestValFsMIPingDest) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhTestv2FsMIPingContextId
Input       :  The Indices
FsMIPingIndex

The Object 
testValFsMIPingContextId
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIPingContextId (UINT4 *pu4ErrorCode,
                            INT4 i4FsMIPingIndex,
                            INT4 i4TestValFsMIPingContextId)
{
    t_PING             *pPing;

    if ((pPing = PingGetInstance (i4FsMIPingIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Ping Entry can not be updated when Ping is in progress */
    if (pPing->i1Oper == (INT1) PING_OPER_IN_PROGRESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PingVcmIsVRExist ((UINT4) i4TestValFsMIPingContextId) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsMIPingTimeout
Input       :  The Indices
FsMIPingIndex

The Object 
testValFsMIPingTimeout
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIPingTimeout (UINT4 *pu4ErrorCode,
                          INT4 i4FsMIPingIndex, INT4 i4TestValFsMIPingTimeout)
{
    if (nmhTestv2FsPingTimeout (pu4ErrorCode,
                                i4FsMIPingIndex,
                                i4TestValFsMIPingTimeout) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhTestv2FsMIPingTries
Input       :  The Indices
FsMIPingIndex

The Object 
testValFsMIPingTries
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIPingTries (UINT4 *pu4ErrorCode,
                        INT4 i4FsMIPingIndex, INT4 i4TestValFsMIPingTries)
{
    if (nmhTestv2FsPingTries (pu4ErrorCode,
                              i4FsMIPingIndex,
                              i4TestValFsMIPingTries) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhTestv2FsMIPingDataSize
Input       :  The Indices
FsMIPingIndex

The Object 
testValFsMIPingDataSize
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIPingDataSize (UINT4 *pu4ErrorCode, INT4 i4FsMIPingIndex,
                           INT4 i4TestValFsMIPingDataSize)
{
    if (nmhTestv2FsPingDataSize (pu4ErrorCode,
                                 i4FsMIPingIndex,
                                 i4TestValFsMIPingDataSize) != SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
Function    :  nmhTestv2FsMIPingEntryStatus
Input       :  The Indices
FsMIPingIndex

The Object 
testValFsMIPingEntryStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsMIPingEntryStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIPingIndex,
                              INT4 i4TestValFsMIPingEntryStatus)
{
    if (nmhTestv2FsPingEntryStatus (pu4ErrorCode,
                                    i4FsMIPingIndex,
                                    i4TestValFsMIPingEntryStatus) !=
        SNMP_FAILURE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsMIPingHostName
 Input       :  The Indices
                FsMIPingIndex

                The Object
                testValFsMIPingHostName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIPingHostName(UINT4 *pu4ErrorCode , 
                          INT4 i4FsMIPingIndex , 
                          tSNMP_OCTET_STRING_TYPE *pTestValFsMIPingHostName)
{
    INT1                i1RetVal = SNMP_FAILURE;

    i1RetVal = nmhTestv2FsPingHostName (pu4ErrorCode,
                                        i4FsMIPingIndex,
                                        pTestValFsMIPingHostName);
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsMIPingTable
Input       :  The Indices
FsMIPingIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsMIPingTable (UINT4 *pu4ErrorCode,
                       tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
