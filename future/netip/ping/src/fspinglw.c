/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspinglw.c,v 1.12 2015/05/20 13:23:11 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fspinglw.h"
#include   "pinginc.h"

PRIVATE VOID        PingClearStatistics (t_PING * pPing);
UINT1 gau1ZeroPingHostName[DNS_MAX_QUERY_LEN];
/* LOW LEVEL Routines for Table : FsPingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPingTable
 Input       :  The Indices
                FsPingIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPingTable (INT4 i4FsPingIndex)
{
    if (i4FsPingIndex < 0)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPingTable
 Input       :  The Indices
                FsPingIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPingTable (INT4 *pi4FsPingIndex)
{
    /* Get the Entry with Index Greater than -1,
       Index range starts from 0 */
    if (nmhGetNextIndexFsPingTable (PING_INVALID_ID,
                                    pi4FsPingIndex) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPingTable
 Input       :  The Indices
                FsPingIndex
                nextFsPingIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPingTable (INT4 i4FsPingIndex, INT4 *pi4NextFsPingIndex)
{
    t_PING             *pPing = NULL;
    INT2                i2Id = 0;
    INT4                i4NextIndex = i4FsPingIndex;
    INT4                i4Found = FALSE;

    PING_SCAN_INSTANCE_TABLE (pPing, i2Id)
    {
        if (i4Found == FALSE)
        {
            if (pPing->i4PingIndex > i4FsPingIndex)
            {
                i4NextIndex = pPing->i4PingIndex;
                i4Found = TRUE;
            }
        }
        else
        {
            if ((pPing->i4PingIndex > i4FsPingIndex) &&
                (pPing->i4PingIndex < i4NextIndex))
            {
                i4NextIndex = pPing->i4PingIndex;
            }
        }
    }

    /* If found the lexographically higher one,
     * Return the same */
    if (i4Found == TRUE)
    {
        *pi4NextFsPingIndex = i4NextIndex;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPingDest
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingDest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingDest (INT4 i4FsPingIndex, UINT4 *pu4RetValFsPingDest)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pu4RetValFsPingDest = pPing->u4Dest;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingTimeout
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingTimeout (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingTimeout)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingTimeout = (INT4) pPing->i2Timeout;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingTries
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingTries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingTries (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingTries)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingTries = (INT4) pPing->i2MaxTries;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingDataSize
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingDataSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingDataSize (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingDataSize)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingDataSize = (INT4) pPing->i2DataSize;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingStatus
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingStatus (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingStatus)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingStatus = (INT4) pPing->i1Oper;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingSendCount
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingSendCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingSendCount (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingSendCount)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingSendCount = (INT4) pPing->i2SentCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingAverageTime
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingAverageTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingAverageTime (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingAverageTime)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingAverageTime = (INT4) pPing->u2AvgTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingMaxTime
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingMaxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingMaxTime (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingMaxTime)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingMaxTime = (INT4) pPing->u2MaxTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingMinTime
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingMinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingMinTime (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingMinTime)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingMinTime = (INT4) pPing->u2MinTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingSuccesses
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingSuccesses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingSuccesses (INT4 i4FsPingIndex, UINT4 *pu4RetValFsPingSuccesses)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pu4RetValFsPingSuccesses = (UINT4) pPing->i2Successes;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingEntryStatus
 Input       :  The Indices
                FsPingIndex

                The Object 
                retValFsPingEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPingEntryStatus (INT4 i4FsPingIndex, INT4 *pi4RetValFsPingEntryStatus)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        *pi4RetValFsPingEntryStatus = (INT4) pPing->i4RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPingHostName
 Input       :  The Indices
                FsPingIndex

                The Object
                retValFsPingHostName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsPingHostName(INT4 i4FsPingIndex , 
                     tSNMP_OCTET_STRING_TYPE * pRetValFsPingHostName)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        STRNCPY (pRetValFsPingHostName->pu1_OctetList,
                 pPing->au1HostName, STRLEN (pPing->au1HostName));

        pRetValFsPingHostName->i4_Length = (INT4) STRLEN(pPing->au1HostName);

        pRetValFsPingHostName->pu1_OctetList[STRLEN(pPing->au1HostName)] = '\0';

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPingDest
 Input       :  The Indices
                FsPingIndex

                The Object 
                setValFsPingDest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPingDest (INT4 i4FsPingIndex, UINT4 u4SetValFsPingDest)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        /* Ping Entry can not be updated when Ping is in progress */
        if (pPing->i1Oper != (INT1) PING_OPER_IN_PROGRESS)
        {
            pPing->u4Dest = u4SetValFsPingDest;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPingTimeout
 Input       :  The Indices
                FsPingIndex

                The Object 
                setValFsPingTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPingTimeout (INT4 i4FsPingIndex, INT4 i4SetValFsPingTimeout)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        /* Ping Entry can not be updated when Ping is in progress */
        if (pPing->i1Oper != (INT1) PING_OPER_IN_PROGRESS)
        {
            pPing->i2Timeout = (INT2) i4SetValFsPingTimeout;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPingTries
 Input       :  The Indices
                FsPingIndex

                The Object 
                setValFsPingTries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPingTries (INT4 i4FsPingIndex, INT4 i4SetValFsPingTries)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        /* Ping Entry can not be updated when Ping is in progress */
        if (pPing->i1Oper != (INT1) PING_OPER_IN_PROGRESS)
        {
            pPing->i2MaxTries = (INT2) i4SetValFsPingTries;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPingDataSize
 Input       :  The Indices
                FsPingIndex

                The Object 
                setValFsPingDataSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPingDataSize (INT4 i4FsPingIndex, INT4 i4SetValFsPingDataSize)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        /* Ping Entry can not be updated when Ping is in progress */
        if (pPing->i1Oper != (INT1) PING_OPER_IN_PROGRESS)
        {
            pPing->i2DataSize = (INT2) i4SetValFsPingDataSize;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPingEntryStatus
 Input       :  The Indices
                FsPingIndex

                The Object 
                setValFsPingEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPingEntryStatus (INT4 i4FsPingIndex, INT4 i4SetValFsPingEntryStatus)
{
    t_PING             *pPing = NULL;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = 0;

    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));

    switch (i4SetValFsPingEntryStatus)
    {
        case CREATE_AND_WAIT:

            pPing = PingCreate ();
            if (pPing == NULL)
            {
                return SNMP_FAILURE;
            }
            pPing->i4PingIndex = i4FsPingIndex;
            pPing->i2AppId = SNMP_PING_APP_ID;
            pPing->i4RowStatus = NOT_READY;
            KW_FALSEPOSITIVE_FIX (pPing->SemId);
            return SNMP_SUCCESS;

        case ACTIVE:

            if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
            {
                return SNMP_FAILURE;
            }

            /* Destination IP should be set for a Active Ping Instance */
            MEMSET (gau1ZeroPingHostName, 0, DNS_MAX_QUERY_LEN);

            if ((pPing->u4Dest == 0)  && (MEMCMP(pPing->au1HostName, 
                                                 gau1ZeroPingHostName, 
                                                 DNS_MAX_QUERY_LEN)))
            {
                return SNMP_FAILURE;
            }

            if (i4SetValFsPingEntryStatus == pPing->i4RowStatus)
            {
                return SNMP_SUCCESS;
            }

            /* Reset the Statistics */
            PingClearStatistics (pPing);

            pPing->i4RowStatus = i4SetValFsPingEntryStatus;


            if (pPing->u4Dest == 0)
            {
                /* here is 0(2nd argument) denotes its a blocking call*/
                i4RetVal = FsUtlIPvXResolveHostName (pPing->au1HostName, 
                                                     DNS_BLOCK,
                                                     &ResolvedIpInfo);
                if ((i4RetVal == DNS_NOT_RESOLVED) ||
                    (ResolvedIpInfo.Resolv4Addr.u1AddrLen == 0))
                {
                    /*cannot resolve the host name*/
                    return SNMP_FAILURE;
                }
                else if (i4RetVal == DNS_IN_PROGRESS)
                {
                    /*host name resolution in progress*/
                    return SNMP_FAILURE;
                }
                else if (i4RetVal == DNS_CACHE_FULL)
                {
                    /*cache full, cannot resolve at the moment*/
                    return SNMP_FAILURE;
                }

                MEMCPY (&pPing->u4Dest, ResolvedIpInfo.Resolv4Addr.au1Addr,
                        ResolvedIpInfo.Resolv4Addr.u1AddrLen);

                pPing->u4Dest = OSIX_HTONL (pPing->u4Dest);

            }
            if (PingSend (pPing) == OSIX_FAILURE)
            {
                /* If no valid Ping Instancee exists, Close the 
                   Socket Opened for sending Ping Request and 
                   receving Ping replies */
                if (PingGetValidInstanceCount () == 0)
                {
                    PingCloseSocket ();
                }
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:

            if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pPing->i1Oper == PING_OPER_IN_PROGRESS)
            {
                return SNMP_FAILURE;
            }

            /* Reset the Statistics */
            PingClearStatistics (pPing);
            pPing->i4RowStatus = i4SetValFsPingEntryStatus;
            return SNMP_SUCCESS;

        case DESTROY:

            if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
            {
                if (pPing->i1Oper == PING_OPER_IN_PROGRESS)
                {
                    return SNMP_FAILURE;
                }
                PingDelete (pPing);
            }
            return SNMP_SUCCESS;

        default:
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsPingHostName
 Input       :  The Indices
                FsPingIndex

                The Object
                setValFsPingHostName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsPingHostName(INT4 i4FsPingIndex , 
                     tSNMP_OCTET_STRING_TYPE *pSetValFsPingHostName)
{

    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
    {
        /* Ping Entry can not be updated when Ping is in progress */
        if (pPing->i1Oper != (INT1) PING_OPER_IN_PROGRESS)
        {
            STRNCPY (pPing->au1HostName, pSetValFsPingHostName->pu1_OctetList, 
                     pSetValFsPingHostName->i4_Length);

            pPing->au1HostName[pSetValFsPingHostName->i4_Length] = '\0';

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}


/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPingDest
 Input       :  The Indices
                FsPingIndex

                The Object 
                testValFsPingDest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPingDest (UINT4 *pu4ErrorCode, INT4 i4FsPingIndex,
                     UINT4 u4TestValFsPingDest)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Ping Entry can not be updated when Ping is in progress */
    if (pPing->i1Oper == (INT1) PING_OPER_IN_PROGRESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((IP_IS_ADDR_CLASS_D (u4TestValFsPingDest)) ||
        (IP_IS_ADDR_CLASS_E (u4TestValFsPingDest)) ||
        ((u4TestValFsPingDest == IP_ANY_ADDR)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPingTimeout
 Input       :  The Indices
                FsPingIndex

                The Object 
                testValFsPingTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPingTimeout (UINT4 *pu4ErrorCode, INT4 i4FsPingIndex,
                        INT4 i4TestValFsPingTimeout)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Ping Entry can not be updated when Ping is in progress */
    if (pPing->i1Oper == (INT1) PING_OPER_IN_PROGRESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPingTimeout < PING_MIN_TIMEOUT) ||
        (i4TestValFsPingTimeout > PING_MAX_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsPingTries
 Input       :  The Indices
                FsPingIndex

                The Object 
                testValFsPingTries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPingTries (UINT4 *pu4ErrorCode, INT4 i4FsPingIndex,
                      INT4 i4TestValFsPingTries)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Ping Entry can not be updated when Ping is in progress */
    if (pPing->i1Oper == (INT1) PING_OPER_IN_PROGRESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPingTries < PING_MIN_TIMEOUT) ||
        (i4TestValFsPingTries > PING_MAX_TRIES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPingDataSize
 Input       :  The Indices
                FsPingIndex

                The Object 
                testValFsPingDataSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPingDataSize (UINT4 *pu4ErrorCode, INT4 i4FsPingIndex,
                         INT4 i4TestValFsPingDataSize)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Ping Entry can not be updated when Ping is in progress */
    if (pPing->i1Oper == (INT1) PING_OPER_IN_PROGRESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPingDataSize < PING_MIN_DATA_SIZE) ||
        (i4TestValFsPingDataSize > (PING_MAX_DATA_SIZE + PING_CUST_MAX_DATA_SIZE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPingEntryStatus
 Input       :  The Indices
                FsPingIndex

                The Object 
                testValFsPingEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPingEntryStatus (UINT4 *pu4ErrorCode, INT4 i4FsPingIndex,
                            INT4 i4TestValFsPingEntryStatus)
{
    t_PING             *pPing = NULL;

    switch (i4TestValFsPingEntryStatus)
    {
        case CREATE_AND_WAIT:
            /* Entry already Exists */
            if ((PingGetInstance (i4FsPingIndex)) != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (i4FsPingIndex < 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:

            if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (pPing->i1Oper == PING_OPER_IN_PROGRESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:

            if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            /* Destination IP should be set for a Active Ping Instance */
            if (pPing->u4Dest == 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case DESTROY:

            /* When Ping in Progress,Entry can't be deleted */
            if ((pPing = PingGetInstance (i4FsPingIndex)) != NULL)
            {
                if (pPing->i1Oper == PING_OPER_IN_PROGRESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FsPingHostName
 Input       :  The Indices
                FsPingIndex

                The Object
                testValFsPingHostName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsPingHostName(UINT4 *pu4ErrorCode , 
                        INT4 i4FsPingIndex , 
                        tSNMP_OCTET_STRING_TYPE *pTestValFsPingHostName)
{
    t_PING             *pPing = NULL;

    if ((pPing = PingGetInstance (i4FsPingIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Ping Entry can not be updated when Ping is in progress */
    if (pPing->i1Oper == (INT1) PING_OPER_IN_PROGRESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsPingHostName->i4_Length > DNS_MAX_QUERY_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPingTable
 Input       :  The Indices
                FsPingIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPingTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : PingClearStatistics                                        */
/*                                                                           */
/* Description  :  Clear the Statistics in the Ping Instance                 */
/*                                                                           */
/* Input        : pPing - Ping Entry                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : PingEntry corresponding to PingIndex / NULL                */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PingClearStatistics (t_PING * pPing)
{
    pPing->i2SentCount = 0;
    pPing->u2AvgTime = 0;
    pPing->u2MaxTime = 0;
    pPing->u2MinTime = 0;
    pPing->i2Successes = 0;
    pPing->i2Seq = 0;
    pPing->i1Oper = PING_OPER_NOT_INITIATED;

}
