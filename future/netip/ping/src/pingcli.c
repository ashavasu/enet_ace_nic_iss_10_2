/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pingcli.c,v 1.22 2017/06/15 13:36:31 siva Exp $
 *
 * Description: Ping cli functions
 *
 *******************************************************************/

#ifndef __PINGCLI_C__
#define __PINGCLI_C__

#include "pinginc.h"
#include "pingcli.h"

/*********************************************************************
*  Function Name : cli_process_ping_cmd
*  Description   : Processes the Ping Commands using the Arguments 
*                  provided
*                  
*  Input(s)      : CliHandle - CLI context ID
*                  & Input Variables 
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/ CLI_FAILURE 
*********************************************************************/
INT4
cli_process_ping_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[PING_CLI_MAX_ARGS];
    UINT1              *pu1IpCxtName = NULL;
    INT1                i1argno = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4VcId = 0;
    tPingEntry          PingEntry;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT2               u2Port = 0;

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&PingEntry, 0, sizeof (tPingEntry));
    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    va_arg (ap, INT4);

    /* Fourth argument is ContextName */
    pu1IpCxtName = va_arg (ap, UINT1 *);

    /*Get context Id from context Name */
    if (pu1IpCxtName != NULL)
    {
        if (VcmIsVrfExist (pu1IpCxtName, &u4VcId) == VCM_FALSE)
        {
            CliPrintf (CliHandle, "\r%% Invalid VRF Id\r\n");
            va_end (ap);
            return CLI_FAILURE;
        }
    }
    else
    {
        u4VcId = VCM_DEFAULT_CONTEXT;
    }

    MEMSET (&PingEntry, 0, sizeof (tPingEntry));
    /* Walk through the rest of the arguments and store in args array. 
     * Store PING_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == PING_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_IP_PING:
            PingEntry.u4IpPingDest = *args[0];

            if (args[1] != NULL)
            {
                PingEntry.i4IpPingMTU = (INT4) *args[1];
            }
            else
            {
                PingEntry.i4IpPingMTU = (INT4) CLI_PING_DEFAULT_SIZE;
            }

            if (args[2] != NULL)
            {
                PingEntry.i4IpPingTries = (INT4) *args[2];
            }
            else
            {
                PingEntry.i4IpPingTries = (INT4) CLI_PING_DEFAULT_RETRY_COUNT;
            }

            if (args[3] != NULL)
            {
                PingEntry.i4IpPingTimeout = (INT4) *args[3];
            }
            else
            {
                PingEntry.i4IpPingTimeout = (INT4) CLI_PING_DEFAULT_TIMEOUT;
            }
            PingEntry.u4ContextId = u4VcId;
            PingEntry.uIpPingDfBit = *(UINT1 *) (args[4]);
            i4RetStatus = CliIpPing (CliHandle, &PingEntry, TRUE);
            break;

        case CLI_HOST_PING:

            STRNCPY (PingEntry.au1HostName, args[0], STRLEN (args[0]));

            PingEntry.au1HostName[STRLEN (args[0])] = '\0';
            *PingEntry.au1HostName =
                UtilStrToLower ((UINT1 *) PingEntry.au1HostName);

            if (args[1] != NULL)
            {
                PingEntry.i4IpPingMTU = (INT4) *args[1];
            }
            else
            {
                PingEntry.i4IpPingMTU = (INT4) CLI_PING_DEFAULT_SIZE;
            }

            if (args[2] != NULL)
            {
                PingEntry.i4IpPingTries = (INT4) *args[2];
            }
            else
            {
                PingEntry.i4IpPingTries = (INT4) CLI_PING_DEFAULT_RETRY_COUNT;
            }

            if (args[3] != NULL)
            {
                PingEntry.i4IpPingTimeout = (INT4) *args[3];
            }
            else
            {
                PingEntry.i4IpPingTimeout = (INT4) CLI_PING_DEFAULT_TIMEOUT;
            }
            PingEntry.u4ContextId = u4VcId;
            PingEntry.uIpPingDfBit = *(UINT1 *) (args[4]);
            i4RetStatus = CliIpPing (CliHandle, &PingEntry, FALSE);

            break;

        case CLI_SOURCE_IP_PING:
            PingEntry.u4IpPingDest = *(UINT4 *) (args[0]);

            if ((UINT4 *) (args[1]) != NULL)
            {
                PingEntry.i4IpPingMTU = *(UINT4 *) (args[1]);
            }
            else
            {
                PingEntry.i4IpPingMTU = (INT4) CLI_PING_DEFAULT_SIZE;
            }

            if ((UINT4 *) (args[2]) != NULL)
            {
                PingEntry.i4IpPingTries = *(UINT4 *) (args[2]);
            }
            else
            {
                PingEntry.i4IpPingTries = (INT4) CLI_PING_DEFAULT_RETRY_COUNT;
            }

            if ((UINT4 *) (args[3]) != NULL)
            {
                PingEntry.i4IpPingTimeout = *(UINT4 *) (args[3]);
            }
            else
            {
                PingEntry.i4IpPingTimeout = (INT4) CLI_PING_DEFAULT_TIMEOUT;
            }
            PingEntry.u4IpPingSource = *(UINT4 *) (args[4]);
            PingEntry.uIpPingDfBit = *(UINT1 *) (args[5]);

            i4RetStatus = CliIpPing (CliHandle, &PingEntry, TRUE);
            break;

        case CLI_SOURCE_VLAN_IP_PING:
            PingEntry.u4IpPingDest = *(UINT4 *) (args[0]);

            if ((UINT4 *) (args[1]) != NULL)
            {
                PingEntry.i4IpPingMTU = *(UINT4 *) (args[1]);
            }
            else
            {
                PingEntry.i4IpPingMTU = (INT4) CLI_PING_DEFAULT_SIZE;
            }

            if ((UINT4 *) (args[2]) != NULL)
            {
                PingEntry.i4IpPingTries = *(UINT4 *) (args[2]);
            }
            else
            {
                PingEntry.i4IpPingTries = (INT4) CLI_PING_DEFAULT_RETRY_COUNT;
            }

            if ((UINT4 *) (args[3]) != NULL)
            {
                PingEntry.i4IpPingTimeout = *(UINT4 *) (args[3]);
            }
            else
            {
                PingEntry.i4IpPingTimeout = (INT4) CLI_PING_DEFAULT_TIMEOUT;
            }
#ifdef IP_WANTED
            if (IpGetPortFromIfIndex ((UINT4) CLI_PTR_TO_U4 (args[4]), &u2Port)
                == IP_SUCCESS)
            {
                if (NetIpv4GetIfInfo (u2Port, &NetIpIfInfo) != NETIPV4_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r% Invalid Interface Index\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r% Invalid Interface Index\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
#else
            UNUSED_PARAM (u2Port);
#endif
            PingEntry.u4IpPingSource = NetIpIfInfo.u4Addr;
            PingEntry.uIpPingDfBit = *(UINT1 *) (args[5]);

            i4RetStatus = CliIpPing (CliHandle, &PingEntry, TRUE);
            break;

        default:
            CliPrintf (CliHandle, "\r% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;
    }

    if (i4RetStatus == CLI_FAILURE)
    {
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    return i4RetStatus;
}

/*********************************************************************
*  Function Name : CliIpPing
*  Description   : Calls the Low Level Set routine for ping table with
*                  ping destination,ping tries,ping data size.Only Destination
*                  is mandatory ,others fields if not given are filled
*                  up with the default values
*                  
*  Input(s)      : 1. CliHandle - CLI context ID
*                  2. pPingEntry - pointer to the structure PingEntry
*                  3. bIpflg    - flag to indicate received Ping entry is
*                  ip address or host name
*  Output(s)     : None.
*  Return Values : None. 
*********************************************************************/
INT4
CliIpPing (tCliHandle CliHandle, tPingEntry * pPingEntry, BOOL1 bIpflg)
{
    CHR1               *pu1Tmp = NULL;
    INT4                i4Retries = 0;    /*Total Number of Pings to send */
    INT4                i4PercentageLoss = 0;
    UINT4               u4Addr = 0;
    t_PING             *pPing = NULL;
    t_ping              PingInst;
    INT2                i2LastSucCnt = 0;
    UINT1               au1String[PING_CLI_MAX_LEN];
    INT4                i4Len = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = 0;

    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    MEMSET (&PingInst, 0, sizeof (t_ping));

    if (bIpflg == TRUE)
    {
        u4Addr = (pPingEntry->u4IpPingDest);

        if (IP_IS_ADDR_CLASS_E (u4Addr) || ((u4Addr & 0xFF000000) == 0)
            ||
            (!((IP_IS_ADDR_CLASS_A (u4Addr)) || (IP_IS_ADDR_CLASS_B (u4Addr))
               || (IP_IS_ADDR_CLASS_C (u4Addr))
               || (IP_IS_ADDR_CLASS_D (u4Addr)))))
        {
            CliPrintf (CliHandle, "\r%% Invalid IP Address \r\n");
            return (CLI_FAILURE);

        }
    }

    if (PingVcmIsVRExist (pPingEntry->u4ContextId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Context Id \r\n");
        return (CLI_FAILURE);
    }

    /* Ping Task semaphore is taken here in CLI Context.Instead of using
       CliRegisterLock as in normal cases,it is done because
       Synchronization Lock for PingInstance in CLI Context 
       will prevent from  releasing the Protocol Lock.Here,
       PingProtcol Lock is taken whenever PingDataStructure is
       accessed */

    PingLock ();
    pPing = PingInitRequest ((tPingEntry *) pPingEntry);
    if (pPing == NULL)
    {
        CliPrintf (CliHandle, "\r%% Internal Error, NO-INST \r\n");
        PingUnLock ();
        return (CLI_FAILURE);
    }

    PingInst.SemId = pPing->SemId;
    PingInst.u4Dest = pPing->u4Dest;
    PingInst.u4TimeTaken = pPing->u4TimeTaken;
    PingInst.i2Successes = pPing->i2Successes;
    PingInst.i2Timeout = pPing->i2Timeout;
    PingInst.i2SentCount = pPing->i2SentCount;
    PingInst.i2MaxTries = pPing->i2MaxTries;
    STRNCPY (PingInst.au1HostName, pPingEntry->au1HostName,
             STRLEN (pPingEntry->au1HostName));
    PingInst.au1HostName[STRLEN (pPingEntry->au1HostName)] = '\0';

    PingUnLock ();

    if (bIpflg == FALSE)
    {
        /*here 0 denotes(2nd arguement) its a blocking call */
        i4RetVal = FsUtlIPvXResolveHostName (PingInst.au1HostName, DNS_BLOCK,
                                             &ResolvedIpInfo);
        if (i4RetVal == DNS_IN_PROGRESS)
        {
            PingLock ();
            PingDelete (pPing);
            PingUnLock ();
            CliPrintf (CliHandle, "\r%% Host name resolution in Progress\r\n");
            return (CLI_FAILURE);
        }
        else if (i4RetVal == DNS_CACHE_FULL)
        {
            PingLock ();
            PingDelete (pPing);
            PingUnLock ();
            CliPrintf (CliHandle,
                       "\r%% DNS cache full, cannot resolve at the moment\r\n");
            return (CLI_FAILURE);
        }
        else if ((i4RetVal == DNS_NOT_RESOLVED) ||
                 (ResolvedIpInfo.Resolv4Addr.u1AddrLen == 0))
        {
            PingLock ();
            PingDelete (pPing);
            PingUnLock ();
            CliPrintf (CliHandle, "\r%% Cannot resolve the host name\r\n");
            return (CLI_FAILURE);
        }

        MEMCPY (&pPing->u4Dest, ResolvedIpInfo.Resolv4Addr.au1Addr,
                ResolvedIpInfo.Resolv4Addr.u1AddrLen);
        pPing->u4Dest = OSIX_HTONL (pPing->u4Dest);
    }

    PingInst.u4Dest = pPing->u4Dest;

    CLI_CONVERT_IPADDR_TO_STR (pu1Tmp, PingInst.u4Dest);

    i4Len = (INT4) (STRLEN (pu1Tmp) + 1);
    MEMCPY (au1String, pu1Tmp,
            ((i4Len <= PING_CLI_MAX_LEN) ? (i4Len) : (PING_CLI_MAX_LEN)));

    i2LastSucCnt = 0;
    for (i4Retries = 0; i4Retries < PingInst.i2MaxTries; i4Retries++)
    {
        PingLock ();
        if (PingSend (pPing) == OSIX_FAILURE)
        {
            /* If no valid Ping Instancee exists, Close the 
               Socket Opened for sending Ping Request and 
               receving Ping replies */
            if (PingGetValidInstanceCount () == 0)
            {
                PingCloseSocket ();
            }

            if (pPing->i1Oper == PING_OPER_COMPLETED)
            {
                CliPrintf (CliHandle,
                           "\r%% Ping Failed - (sendto/Packet Allocation)\r\n");
            }
            PingUnLock ();
            break;
        }
        PingUnLock ();

        if (OsixSemTake (PingInst.SemId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Internal Error, SEM-TAKE\r\n");
            PingLock ();
            PingDelete (pPing);
            PingUnLock ();
            return (CLI_FAILURE);
        }

        /* Get the Updated PingInstance Information */
        PingLock ();

        PingInst.SemId = pPing->SemId;
        PingInst.u4Dest = pPing->u4Dest;
        PingInst.u4TimeTaken = pPing->u4TimeTaken;
        PingInst.i2Successes = pPing->i2Successes;
        PingInst.i2Timeout = pPing->i2Timeout;
        PingInst.i2SentCount = pPing->i2SentCount;
        PingInst.i2MaxTries = pPing->i2MaxTries;

        PingUnLock ();

        if (PingInst.i2Successes > i2LastSucCnt)
        {
            i2LastSucCnt++;
            if (PingInst.u4TimeTaken > 0)
            {
                mmi_printf
                    ("\rReply Received From :%s, TimeTaken : %d msecs\r\n",
                     au1String, PingInst.u4TimeTaken);
            }
            else
            {
                mmi_printf
                    ("\rReply Received From :%s, TimeTaken : <1 msecs\r\n",
                     au1String);
            }
        }
        else
        {
            mmi_printf ("\rReply Not Received From : %s, ", au1String);

            mmi_printf ("Timeout : %d secs\r\n", PingInst.i2Timeout);
        }
    }

    /* Show the Ping Statistics and Delete the Ping Entry if the Tries Count */

    mmi_printf ("\r\n--- %s Ping Statistics --- \r\n", au1String);

    mmi_printf ("%d Packets Transmitted, ", PingInst.i2SentCount);

    mmi_printf ("%d Packets Received, ", PingInst.i2Successes);

    if (PingInst.i2SentCount == 0)
    {
        /* No packet sent out */
        i4PercentageLoss = 0;
    }
    else
    {
        i4PercentageLoss = (((PingInst.i2SentCount -
                              PingInst.i2Successes) * IP_HUNDRED) /
                            PingInst.i2SentCount);
    }
    mmi_printf ("%d%% Packets Loss \r\n", i4PercentageLoss);

    PingLock ();
    PingDelete (pPing);
    PingUnLock ();

    return (CLI_SUCCESS);

}
#endif
