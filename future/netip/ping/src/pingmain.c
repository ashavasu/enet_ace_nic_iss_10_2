/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pingmain.c,v 1.33 2016/02/27 10:04:12 siva Exp $
 *
 * Description: Ping task functions
 *
 *******************************************************************/

#include "pinginc.h"
#include "pingcli.h"
#include "fspingwr.h"
#include "fsmppiwr.h"
#ifndef PINGMAIN_C
#define PINGMAIN_C
extern tOsixSemId gLnxEvtSemId;
/********************************************************************
 * Description:
 *          The Ping software is built over ICMP ECHO REQUEST and            
 *          ECHO RESPONSE messages. The network administrator can 'Ping '    
 *          a remote host by invoking ping routines through network          
 *          management protocols.  Also the software allows for arbitory     
 *          message lengths and the time intervals.                          
 *           
 *          Implementation here is specific to fsping.mib. 
 *          In this implementation,Multiple pings (even to same destination)
 *          can be triggered simultaneously.This is achevied by having Ping 
 *          Instance Id as a unique Identifier.
 *                                                                           
 *          The ping is triggered setting the RowStatus status active
 *          with appropriate destination IP Address in Ping Instance record.
*           For each timeout the ping timeout handler sends the     
 *          echo request.  The current statistics about ping can be          
 *          obtained using Network Management variables.                     
 ********************************************************************/

t_PING              gaPingTable[PING_MAX_INSTANCES + PING_CUST_MAX_INSTANCES];    /* PING Data structure */
PRIVATE INT1        ga1RecvBuff[PING_BUFFER_SIZE];
tTimerListId        gPingTimerListId;
INT4                gi4PingSocket = -1;
tOsixSemId          gPingSemId;
tOsixTaskId         gPingTaskId;
UINT4               gu4PingDbg = 0;
/*****************************************************************************/
/* Function     : PingTaskMain                                               */
/*                                                                           */
/* Description  : This is main task Ping Module.It continually waits for     */
/*                the events.When event arrives, it calls different          */
/*                functions to handle the events                             */
/* Input        : Task Name                                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PingTaskMain (INT1 *pi1TaskParam)
{
    UINT4               u4EventMask = 0;

    UNUSED_PARAM (pi1TaskParam);

    if (OsixTskIdSelf (&gPingTaskId) == OSIX_FAILURE)
    {
        PING_TRC_ARG (ALL_FAILURE_TRC, "Ping Task initialization failed\n");
        PING_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Initializes Ping DataStructure */
    if (PingInit () == OSIX_FAILURE)
    {
        PING_TRC_ARG (ALL_FAILURE_TRC, "Ping Init failed\n");
        PING_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate Task Initialization Success */
    PING_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        OsixEvtRecv (gPingTaskId,
                     PING_PKT_ARRIVAL_EVENT | PING_TMR_EXPIRY_EVENT,
                     OSIX_WAIT | OSIX_EV_ANY, &u4EventMask);

        /* Process the Packet recevied on ICMP Socket */
        if (u4EventMask & PING_PKT_ARRIVAL_EVENT)
        {
            PingLock ();
            PingReplyHandler ();
            PingUnLock ();
        }

        /* Handle Retry Timer Expiry */
        if (u4EventMask & PING_TMR_EXPIRY_EVENT)
        {
            PingLock ();
            PingHandleTimerExpiry ();
            PingUnLock ();
        }
    }
}

/*****************************************************************************/
/* Function     : PingInit                                                   */
/*                                                                           */
/* Description  : Initializes the Data Structures by Ping Module             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
PingInit (VOID)
{
    t_PING             *pPing = NULL;
    INT2                i2Id = 0;

    /* Create a TimerList */
    if (TmrCreateTimerList (PING_TASK_NAME,
                            PING_TMR_EXPIRY_EVENT,
                            NULL, &gPingTimerListId) != TMR_SUCCESS)
    {
        PING_TRC_ARG (ALL_FAILURE_TRC,
                      "PingInit: Timer List creation failed\n");
        return OSIX_FAILURE;
    }

    /* Create the Semaphore for PingTask */
    if ((OsixCreateSem (PING_TASK_SEM_NAME, 1, 0, &gPingSemId)) != OSIX_SUCCESS)
    {
        PING_TRC_ARG (ALL_FAILURE_TRC, "PingInit: Semaphore creation failed\n");
        return OSIX_FAILURE;
    }

#if  (defined (SNMP_2_WANTED)) || (defined (SNMPV3_WANTED))
    RegisterFSPING ();
    RegisterFSMPPI ();
#endif
    /* Initialize Ping Instances */
    PING_SCAN_INSTANCE_TABLE (pPing, i2Id)
    {
        PingInitRec (pPing);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PingInitRec                                                */
/*                                                                           */
/* Description  : Initializes a Ping Entry                                   */
/*                                                                           */
/* Input        : pPing - Pointer to a Ping Entry                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PingInitRec (t_PING * pPing)
{
    /* Initialize all the variables to default state */
    TmrStopTimer (gPingTimerListId, &(pPing->Timer.TimerNode));
    MEMSET (&(pPing->Timer), 0, sizeof (t_PING_TIMER));
    pPing->u4Dest = 0;
    pPing->u4ContextId = VCM_DEFAULT_CONTEXT;
    pPing->i2Timeout = PING_DEF_TIMEOUT;
    pPing->i2MaxTries = PING_DEF_TRIES;
    pPing->i2DataSize = PING_DEF_DATA_SIZE;
    pPing->i4RowStatus = PING_INVALID_ID;
    pPing->i1Oper = PING_OPER_NOT_INITIATED;
    pPing->SemId = (tOsixSemId) PING_INVALID_ID;
    pPing->i2SentCount = 0;
    pPing->u2AvgTime = 0;
    pPing->u2MaxTime = 0;
    pPing->u2MinTime = 0;
    pPing->i2Successes = 0;
    pPing->i4PingIndex = PING_INVALID_ID;
    pPing->i2Id = PING_INVALID_ID;
    pPing->i2Seq = 0;
    pPing->i2AppId = 0;
}

/*****************************************************************************/
/* Function     : PingHandleTimerExpiry                                      */
/*                                                                           */
/* Description  : Routine that Handle Ping Timer Expiry.If the trigger is    */
/*                through SNMP,Call Sending Ping.Or Release sem and let      */
/*                application send further EchoRequests                      */
/*                                                                           */
/* Input        : pPing - Pointer to a Ping Entry                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PingHandleTimerExpiry (VOID)
{
    tTMO_APP_TIMER     *pTimer = NULL;
    t_PING             *pPing = NULL;

    TmrGetExpiredTimers (gPingTimerListId, &pTimer);

    PING_TRC_ARG (MGMT_TRC, "PingHandleTimerExpiry: Ping Timer expired\n");
    while (pTimer != NULL)
    {
        pPing = (t_PING *) pTimer;
        if (pPing->Timer.u1Id == PING_REPLY_TIMER_ID)
        {
            if (pPing->i2AppId == SNMP_PING_APP_ID)
            {
                /* Ping is init from SNMP, continue with Retry */
                PingSend (pPing);
            }
            else
            {
                /* Indicate appn regarding the ping timeout, by giving the Sem 
                   The application triggers, next request */
                OsixSemGive (pPing->SemId);
                if (pPing->i2SentCount == pPing->i2MaxTries)
                {
                    pPing->i1Oper = PING_OPER_COMPLETED;
                }
            }
        }
        TmrGetExpiredTimers (gPingTimerListId, &pTimer);
    }

    /* If no valid Ping Instancee exists, Close the Socket Opened
       for sending Ping Request and receving Ping replies */
    if (PingGetValidInstanceCount () == 0)
    {
        PingCloseSocket ();
    }
}

/*****************************************************************************/
/* Function     : PingOpenSocket                                             */
/*                                                                           */
/* Description  : Open and Initializes the Socket used by Ping Module        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PingOpenSocket (t_PING * pPing)
{
    struct sockaddr_in  Sin;
    UINT1               u1PmtuFlag = IPPMTUDISC_DO;
#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED) 
    tLnxVrfEventInfo  LnxVrfEventInfo;
    MEMSET ( &LnxVrfEventInfo, 0, sizeof ( LnxVrfEventInfo));
#endif


    PING_TRC_ARG (MGMT_TRC, "PingOpenSocket: Opening a new socket for "
                  "ping session\n");
    /* Open the RawSocket for sending EchoRequests and 
       Receving EchoReplies */

#if defined (LNXIP4_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    
    LnxVrfEventInfo.u4ContextId =pPing->u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_INET;
    LnxVrfEventInfo.i4SockType=SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = IPPROTO_ICMP;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
    LnxVrfSockLock();
    LnxVrfEventHandling(&LnxVrfEventInfo,&gi4PingSocket);
    LnxVrfSockUnLock();
#else
    gi4PingSocket = socket (AF_INET, SOCK_RAW, IPPROTO_ICMP);
#endif
    if (gi4PingSocket < 0)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&Sin, 0, sizeof (Sin));
    Sin.sin_family = AF_INET;
    Sin.sin_port = PING_ICMP_IDENTIFIER;
    Sin.sin_addr.s_addr = OSIX_HTONL(pPing->u4Source);
    if (bind (gi4PingSocket, (struct sockaddr *) &Sin, sizeof (Sin)) < 0)
    {
        PING_TRC_ARG (ALL_FAILURE_TRC, "PingOpenSocket: Socket bind failed\n");
        return OSIX_FAILURE;
    }
    if(pPing->uIpPingDfBit == OSIX_TRUE)
    {
        if (setsockopt
            (gi4PingSocket, IPPROTO_IP, IP_MTU_DISCOVER, &u1PmtuFlag,
             sizeof (UINT1)) != OSIX_SUCCESS)
        {
            return CFA_FAILURE;
        }
    }
    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (gi4PingSocket, PingProcessPktInIcmpSocket) != OSIX_SUCCESS)
    {
        PING_TRC_ARG (ALL_FAILURE_TRC, "PingOpenSocket: SelAddFd failed\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PingCloseSocket                                            */
/*                                                                           */
/* Description  : Closes the Raw Socket used by Ping Module.When no ping     */
/*                instance exists,the socket is closed                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PingCloseSocket (VOID)
{
    INT4                i4RetVal = 0;

    /* Remove the Socket Descriptor added to Select utility for 
       Packet Reception */
    if (SelRemoveFd (gi4PingSocket) != OSIX_SUCCESS)
    {
        close (gi4PingSocket);
        gi4PingSocket = -1;
        PING_TRC_ARG (ALL_FAILURE_TRC, "PingCloseSocket: SelRemoveFd failed\n");
        return OSIX_FAILURE;
    }

    /* Close the RAW Socket Opened */
    if (gi4PingSocket != -1)
    {
        i4RetVal = close (gi4PingSocket);

        if (i4RetVal < 0)
        {
            PING_TRC_ARG (ALL_FAILURE_TRC,
                          "PingCloseSocket: Socket close failed\n");
            return (OSIX_FAILURE);
        }
    }
    gi4PingSocket = -1;
    PING_TRC_ARG (MGMT_TRC, "PingCloseSocket: Closed the ping socket\n");
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PingProcessPktInIcmpSocket                                 */
/*                                                                           */
/* Description  : Posts a Event to Ping Task upon Packet reception in the    */
/*                Socket.It will be called by Select Utility                 */
/*                                                                           */
/* Input        : i4SockFd   - Socket Descriptor                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PingProcessPktInIcmpSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    /* Notify PingTask about Packet Reception */
    OsixEvtSend (gPingTaskId, PING_PKT_ARRIVAL_EVENT);
}

/*****************************************************************************/
/* Function     : PingCreate                                                 */
/*                                                                           */
/* Description  : Creates a Ping Data Structure                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Returns Ping Entry,Else return NULL                        */
/*                                                                           */
/*****************************************************************************/
t_PING             *
PingCreate (VOID)
{
    UINT1               au1SemName[PING_MAX_SEM_NAME_LEN];
    t_PING             *pPing = NULL;
    INT2                i2Id = 0;

    PING_TRC_ARG (MGMT_TRC, "PingCreate: Creating a new ping session\n");
    PING_SCAN_INSTANCE_TABLE_FREE_ENTRY (pPing, i2Id)
    {
        SPRINTF ((CHR1 *) au1SemName, "PN%d", i2Id);
        if (OsixSemCrt (au1SemName, &(pPing->SemId)) != OSIX_SUCCESS)
        {
            return NULL;
        }
        pPing->i2Id = i2Id;
        pPing->Timer.u1Id = PING_REPLY_TIMER_ID;
        return (pPing);
    }

    PING_TRC_ARG (ALL_FAILURE_TRC,
                  "PingCreate: Creating ping session failed\n");
    return NULL;
}

/*****************************************************************************/
/* Function     : PingDelete                                                 */
/*                                                                           */
/* Description  : Deletes the Ping Entry                                     */
/*                                                                           */
/* Input        : Ping Entry                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PingDelete (t_PING * pPing)
{
    PING_TRC_ARG (MGMT_TRC, "PingDelete: Deleting a ping session\n");
    TmrStopTimer (gPingTimerListId, &(pPing->Timer.TimerNode));
    OsixSemDel (pPing->SemId);
    PingInitRec (pPing);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PingSend                                                   */
/*                                                                           */
/* Description  : Sends Ping data based on Ping Entry Information            */
/*                                                                           */
/* Input        : Ping Entry                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PingSend (t_PING * pPing)
{
    struct sockaddr_in  ToDest;
    INT1               *pi1SendBuff = NULL;
    tPingIcmpHdr        PingIcmpHdr;
    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4Status = 0;
    INT4                i4RetVal = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1PingData = 0;
    tUtlSysPreciseTime  StartPreciseTime;

    MEMSET (&StartPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    pi1SendBuff = gaPingTable[pPing->i4PingIndex].ai1SendBuff;

    PING_TRC_ARG (MGMT_TRC, "PingSend: Sending a ping packet\n");
    /* For Self Ping, when the interface is down, return failure.
       This is applicable for both FSIP and LNXIP */
    if ((NetIpv4IfIsOurAddressInCxt (pPing->u4ContextId, pPing->u4Dest)
         == NETIPV4_SUCCESS))
    {
        if (NetIpv4GetIfIndexFromAddrInCxt (pPing->u4ContextId, pPing->u4Dest,
                                            &u4IfIndex) != NETIPV4_SUCCESS)
        {
            pPing->i1Oper = PING_OPER_NOT_INITIATED;
            PING_TRC_ARG (ALL_FAILURE_TRC, "PingSend: Cannot obtain the "
                          "interface index\n");
            return OSIX_FAILURE;
        }
        if (NetIpv4GetIfInfo (u4IfIndex, &NetIpIfInfo) != NETIPV4_SUCCESS)
        {
            pPing->i1Oper = PING_OPER_NOT_INITIATED;
            PING_TRC_ARG (ALL_FAILURE_TRC, "PingSend: Cannot obtain the "
                          "interface info\n");
            return OSIX_FAILURE;
        }
        if ((NetIpIfInfo.u4Admin == IPIF_ADMIN_DISABLE) ||
            (NetIpIfInfo.u4Oper == IPIF_OPER_DISABLE))
        {
            pPing->i1Oper = PING_OPER_NOT_INITIATED;
            PING_TRC_ARG (ALL_FAILURE_TRC, "PingSend: Interface status is"
                          "disabled\n");
            return OSIX_FAILURE;
        }
    }
    /* If the Ping Instance created is the first one, Open the Socket
       for sending Ping Request and receving Ping replies */
    if (PingGetValidInstanceCount () == 0)
    {
        if (PingOpenSocket (pPing) == OSIX_FAILURE)
        {
            pPing->i1Oper = PING_OPER_NOT_INITIATED;
            return OSIX_FAILURE;
        }
    }

    /* Update the Status of the Ping - inprogress/completed */
    if (pPing->i2SentCount < pPing->i2MaxTries)
    {
        if (pPing->i2SentCount == 0)
        {
            pPing->i1Oper = PING_OPER_IN_PROGRESS;
        }

        /* Get the Precisetime and store in pPing object, later we will use it to calculate ping response  */
        UtlGetPreciseSysTime (&StartPreciseTime);
        pPing->u4Seconds = StartPreciseTime.u4Sec;
        pPing->u4NanoSeconds = StartPreciseTime.u4NanoSec;

        PING_RESTART_TIMER (gPingTimerListId, &(pPing->Timer.TimerNode),
                            pPing->i2Timeout);
    }
    else
    {
        TmrStopTimer (gPingTimerListId, &(pPing->Timer.TimerNode));
        pPing->i1Oper = PING_OPER_COMPLETED;
        return OSIX_SUCCESS;
    }

    /* Fill Destination Information needed for Socket to send Packet */
    MEMSET (&ToDest, 0, sizeof (ToDest));
    ToDest.sin_family = AF_INET;
    ToDest.sin_port = 0;
    ToDest.sin_addr.s_addr = OSIX_HTONL (pPing->u4Dest);

    /*Fill the ICMP Header Information */
    MEMSET (&PingIcmpHdr, 0, sizeof (tPingIcmpHdr));
    PingIcmpHdr.i1Type = PING_ICMP_ECHO;
    PingIcmpHdr.i1Code = 0;
    pPing->i2Seq++;
    PingIcmpHdr.args.Identification.i2Id = pPing->i2Id;
    PingIcmpHdr.args.Identification.i2Seq = pPing->i2Seq;

    /* Fill the ICMP Header */
    MEMCPY (pi1SendBuff, (INT1 *) &PingIcmpHdr, sizeof (tPingIcmpHdr));

    /* Fill the Ping Data in the Buffer */
    u1PingData = PING_DATA;
    MEMSET (pi1SendBuff + sizeof (tPingIcmpHdr), u1PingData, pPing->i2DataSize);

    /* Calculate and fill the CheckSum */
    PingIcmpHdr.u2Cksum = OSIX_NTOHS (UtlIpCSumLinBuf (pi1SendBuff,
                                                       sizeof (tPingIcmpHdr) +
                                                       (UINT4) pPing->
                                                       i2DataSize));

    /* Copy the checkSum in the Appropriate Offset */
    MEMCPY (pi1SendBuff + IP_TWO, (INT1 *) &PingIcmpHdr.u2Cksum,
            sizeof (UINT2));
#ifdef IP_WANTED
    i4RetVal = setsockopt (gi4PingSocket, IPPROTO_IP, IP_PKT_TX_CXTID,
                           &pPing->u4ContextId, sizeof (UINT4));
#endif
    i4Status =
        sendto (gi4PingSocket, pi1SendBuff,
                (INT4) (sizeof (tPingIcmpHdr) + (UINT2) pPing->i2DataSize), 0,
                (struct sockaddr *) &ToDest, (INT4) (sizeof (ToDest)));

    if (i4Status < 0)
    {
        TmrStopTimer (gPingTimerListId, &(pPing->Timer.TimerNode));

        pPing->i1Oper = PING_OPER_COMPLETED;
        PING_TRC_ARG (ALL_FAILURE_TRC, "PingSend: Sendto failed\n");

        return OSIX_FAILURE;
    }
    pPing->i2SentCount++;
    PING_TRC_ARG (MGMT_TRC, "PingSend: Sent the ping packet\n");
    UNUSED_PARAM (i4RetVal);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PingReplyHandler                                           */
/*                                                                           */
/* Description  : Reads the Packet from Socket and then                      */
/*               it searches the ping table for the match. If we have an     */
/*               entry for the host and if we are waiting for the response   */
/*               then the statistics is updated                              */
/*                                                                           */
/* Input        : Ping Entry                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PingReplyHandler (VOID)
{
    t_PING             *pPing = NULL;
    tPingIcmpHdr       *pICMP = NULL;
    t_IP_HEADER        *pIpHdr = NULL;
    struct sockaddr_in  PeerAddr;
    INT1               *pi1RecvBuff = NULL;
    UINT2               u2IpHdrLen = 0;
    INT4                i4AddrLen = sizeof (PeerAddr);
    INT4                i4DataLen = 0;
    UINT4               u4SrcIp = 0;
    tUtlSysPreciseTime  CurrentPreciseTime;
    UINT4               u4RemainingTimeInSec = 0;
    UINT4               u4RemainingTimeInNSec = 0;

    MEMSET (&CurrentPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    MEMSET (&PeerAddr, 0, sizeof (PeerAddr));
    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_port = PING_ICMP_IDENTIFIER;
    pi1RecvBuff = ga1RecvBuff;
    MEMSET (pi1RecvBuff, 0, PING_BUFFER_SIZE);

    /* Call recvfrom to receive the packet */
    if ((i4DataLen = recvfrom (gi4PingSocket, pi1RecvBuff,
                               (INT4) PING_BUFFER_SIZE, 0,
                               (struct sockaddr *) &PeerAddr, &i4AddrLen)) <= 0)
    {
        PING_TRC_ARG (ALL_FAILURE_TRC, "PingReplyHandler: recvfrom failed\n");
        return OSIX_FAILURE;
    }

    PING_TRC_ARG (MGMT_TRC, "PingReplyHandler: Received a ping packet \n");

    pIpHdr = (t_IP_HEADER *) (VOID *) pi1RecvBuff;

    u2IpHdrLen = (UINT2) ((pIpHdr->u1Ver_hdrlen & 0x0f) * sizeof (UINT4));

    u4SrcIp = OSIX_HTONL (pIpHdr->u4Src);

    pICMP = (tPingIcmpHdr *) (VOID *) (pi1RecvBuff + u2IpHdrLen);

    /* Only Echo Replies should be condidered */
    if (pICMP->i1Type == PING_ICMP_ECHO_REPLY)
    {
        /* Do a LookUp whether the reply is for the Ping sent by Us */
        if ((pICMP->args.Identification.i2Id >= 0) &&
            (pICMP->args.Identification.i2Id < (PING_MAX_INSTANCES + PING_CUST_MAX_INSTANCES)))
        {
            pPing = &gaPingTable[pICMP->args.Identification.i2Id];
        }

        /* If Valid Sequnce no,then update the Statistics */
        if ((pPing != NULL) &&
            (pPing->u4Dest == u4SrcIp) &&
            (pICMP->args.Identification.i2Seq == pPing->i2Seq))
        {

            /* Calculating u4Timetaken in msec using UtlGetPreciseSysTime  */
            UtlGetPreciseSysTime (&CurrentPreciseTime);
            u4RemainingTimeInSec = CurrentPreciseTime.u4Sec;
            u4RemainingTimeInNSec = CurrentPreciseTime.u4NanoSec;

            if (u4RemainingTimeInSec >= pPing->u4Seconds)
            {
                if (u4RemainingTimeInNSec < pPing->u4NanoSeconds)
                {
                    u4RemainingTimeInSec = CurrentPreciseTime.u4Sec;
                    u4RemainingTimeInNSec = CurrentPreciseTime.u4NanoSec;
                    u4RemainingTimeInSec--;
                    u4RemainingTimeInNSec += (IP_THOUSAND * 1000000);
                }

                u4RemainingTimeInSec = u4RemainingTimeInSec - pPing->u4Seconds;
                u4RemainingTimeInNSec =
                    u4RemainingTimeInNSec - pPing->u4NanoSeconds;
            }

            pPing->u4TimeTaken =
                ((u4RemainingTimeInSec * IP_THOUSAND) +
                 (u4RemainingTimeInNSec / 1000000));

            if (pPing->i2Successes == 0)
            {
                pPing->u2MaxTime = (UINT2) pPing->u4TimeTaken;
                pPing->u2MinTime = (UINT2) pPing->u4TimeTaken;
            }
            else
            {
                if (pPing->u4TimeTaken > pPing->u2MaxTime)
                {
                    pPing->u2MaxTime = (UINT2) pPing->u4TimeTaken;
                }
                if (pPing->u4TimeTaken < pPing->u2MinTime)
                {
                    pPing->u2MinTime = (UINT2) pPing->u4TimeTaken;
                }
            }

            pPing->i2Successes++;
            /*
             *                (N1 * M1) + (N2 * M2)
             *   Mean Time =  ----------------------   Where N2 = 1
             *                       N1 + N2           N1 = pPing->i2Success - 1
             */

            pPing->u2AvgTime =
                (UINT2) ((pPing->u2MaxTime + pPing->u2MinTime) / IP_TWO);

            if (pPing->i2AppId == SNMP_PING_APP_ID)
            {
                /* Ping is init from SNMP, continue with next req */
                PingSend (pPing);
            }
            else
            {
                /* Indicate appn regarding the ping response, by giving the 
                   Sem . The application triggers, next request */
                TmrStopTimer (gPingTimerListId, &(pPing->Timer.TimerNode));
                OsixSemGive (pPing->SemId);
                if (pPing->i2SentCount == pPing->i2MaxTries)
                {
                    pPing->i1Oper = PING_OPER_COMPLETED;
                }
            }
        }
    }

    PING_TRC_ARG (MGMT_TRC,
                  "PingReplyHandler: Adding the ping socket to the select list \n");
    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (gi4PingSocket, PingProcessPktInIcmpSocket) != OSIX_SUCCESS)
    {
        PING_TRC_ARG (ALL_FAILURE_TRC, "PingReplyHandler: SelAddFd failed \n");
        return OSIX_FAILURE;
    }
    /* If no valid Ping Instancee exists, Close the Socket Opened
       for sending Ping Request and receving Ping replies */
    if (PingGetValidInstanceCount () == 0)
    {
        PING_TRC_ARG (MGMT_TRC, "PingReplyHandler: Closing the ping socket \n");
        PingCloseSocket ();
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PingGetValidInstanceCount                                  */
/*                                                                           */
/* Description  : Provides the count of active Ping Instances.(i.e OnGoing   */
/*                Ping)                                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Returns PingInstance Count                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PingGetValidInstanceCount (VOID)
{
    t_PING             *pPing = NULL;
    INT4                i4PingInstCount = 0;
    INT2                i2Id = 0;

    PING_SCAN_INSTANCE_TABLE (pPing, i2Id)
    {
        if (pPing->i1Oper == PING_OPER_IN_PROGRESS)
        {
            i4PingInstCount++;
        }
    }
    return i4PingInstCount;
}

/*****************************************************************************/
/* Function     : PingLock                                                   */
/*                                                                           */
/* Description  : Takes the Ping Task Semaphore                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PingLock (VOID)
{
    if (OsixSemTake (gPingSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : PingUnLock                                                 */
/*                                                                           */
/* Description  : Releases the PingTask Semaphore taken                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS/ SNMP_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PingUnLock (VOID)
{
    OsixSemGive (gPingSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : PingVcmIsVRExist                                           */
/*                                                                           */
/* Description  : This function determines if the given context is a valid   */
/*                 context created in the router.                            */
/*                                                                           */
/* Input        : u4ContextId - The context Id                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/ OSIX_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
PingVcmIsVRExist (UINT4 u4ContextId)
{
    if (VcmIsL3VcExist (u4ContextId) == VCM_FALSE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function     : PingEnableDebug     
*                                            
* Description  : This function configures the Ping trace flag 
*
* Input        : u4TrcValue  - The configured trace value.
*
* Output       : None                         
*
* Returns      : None                      
*
***************************************************************************/
PUBLIC VOID
PingEnableDebug (UINT4 u4TrcValue)
{
    PingLock ();
    gu4PingDbg = u4TrcValue;
    PingUnLock ();
    return;
}

/*****************************************************************************/
/* Function     : PingGetInstance                                            */
/*                                                                           */
/* Description  : Get the Ping Entry for the  PingIndex provided through SNMP*/
/*                                                                           */
/* Input        : i4PingIndex - Index of PingTable                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : PingEntry corresponding to PingIndex / NULL                */
/*                                                                           */
/*****************************************************************************/
t_PING             *
PingGetInstance (INT4 i4PingIndex)
{
    t_PING             *pPing = NULL;
    INT2                i2Id = 0;

    PING_SCAN_INSTANCE_TABLE (pPing, i2Id)
    {
        if (pPing->i4PingIndex == i4PingIndex)
        {
            return pPing;
        }
    }
    return NULL;
}

/*********************************************************************
*  Function Name : IpResolveNeighbor
*  Description   : Calls the Low Level Set routine for ping table with
*                  ping destination,ping tries,ping data size.Only Destination
*                  is mandatory ,others fields if not given are filled
*                  up with the default values
*
*  Input(s)      : PingEntry - Variable of structure PingEntry
*  Output(s)     : None.
*  Return Values : None.
*********************************************************************/
INT4
IpResolveNeighbor (tPingEntry PingEntry)
{
    CHR1               *pu1Tmp = NULL;
    INT4                i4Retries = 0;    /*Total Number of Pings to send */
    UINT4               u4Addr = PingEntry.u4IpPingDest;
    t_PING             *pPing = NULL;
    t_ping              PingInst;
    UINT1               au1String[PING_CLI_MAX_LEN];
    INT4                i4Len = 0;

    if (IP_IS_ADDR_CLASS_E (u4Addr) || ((u4Addr & 0xFF000000) == 0)
        ||
        (!((IP_IS_ADDR_CLASS_A (u4Addr)) || (IP_IS_ADDR_CLASS_B (u4Addr))
           || (IP_IS_ADDR_CLASS_C (u4Addr)) || (IP_IS_ADDR_CLASS_D (u4Addr)))))
    {
        return (CLI_FAILURE);

    }

    if (PingVcmIsVRExist (PingEntry.u4ContextId) != OSIX_SUCCESS)
    {
        return (CLI_FAILURE);
    }

    /* Ping Task semaphore is taken here in CLI Context.Instead of using
       CliRegisterLock as in normal cases,it is done because
       Synchronization Lock for PingInstance in CLI Context
       will prevent from  releasing the Protocol Lock.Here,
       PingProtcol Lock is taken whenever PingDataStructure is
       accessed */

    PingLock ();
    pPing = PingInitRequest ((tPingEntry *) & PingEntry);
    if (pPing == NULL)
    {
        PingUnLock ();
        return (CLI_FAILURE);
    }

    PingInst.SemId = pPing->SemId;
    PingInst.u4Dest = pPing->u4Dest;
    PingInst.u4TimeTaken = pPing->u4TimeTaken;
    PingInst.i2Successes = pPing->i2Successes;
    PingInst.i2Timeout = pPing->i2Timeout;
    PingInst.i2SentCount = pPing->i2SentCount;
    PingInst.i2MaxTries = pPing->i2MaxTries;

    PingUnLock ();

    CLI_CONVERT_IPADDR_TO_STR (pu1Tmp, PingInst.u4Dest);

    i4Len = (INT4) (STRLEN (pu1Tmp) + 1);
    MEMCPY (au1String, pu1Tmp,
            ((i4Len <= PING_CLI_MAX_LEN) ? (i4Len) : (PING_CLI_MAX_LEN)));

    for (i4Retries = 0; i4Retries < PingInst.i2MaxTries; i4Retries++)
    {
        PingLock ();
        if (PingSend (pPing) == OSIX_FAILURE)
        {
            /* If no valid Ping Instancee exists, Close the
               Socket Opened for sending Ping Request and
               receving Ping replies */
            if (PingGetValidInstanceCount () == 0)
            {
                PingCloseSocket ();
            }

            PingUnLock ();
            break;
        }
        PingUnLock ();

        if (OsixSemTake (PingInst.SemId) != OSIX_SUCCESS)
        {
            PingLock ();
            PingDelete (pPing);
            PingUnLock ();
            return (CLI_FAILURE);
        }

        /* Get the Updated PingInstance Information */
        PingLock ();

        PingInst.SemId = pPing->SemId;
        PingInst.u4Dest = pPing->u4Dest;
        PingInst.u4TimeTaken = pPing->u4TimeTaken;
        PingInst.i2Successes = pPing->i2Successes;
        PingInst.i2Timeout = pPing->i2Timeout;
        PingInst.i2SentCount = pPing->i2SentCount;
        PingInst.i2MaxTries = pPing->i2MaxTries;

        PingUnLock ();
    }

    PingLock ();
    PingDelete (pPing);
    PingUnLock ();

    return (CLI_SUCCESS);
}

/*********************************************************************
 * *  Function Name : PingInitRequest
 * *  Description   : Get the free Ping Instance for triggering Ping and
 * *                  fill them with Parameters provided through CLI.
 * *
 * *  Input(s)      : PingEntry - Variable of structure PingEntry
 * *  Output(s)     : None.
 * *  Return Values : PingInstance filled with appropriate parameters
 * *********************************************************************/

t_PING     *
PingInitRequest (tPingEntry * pPingEntry)
{
    t_PING             *pPing = NULL;

    pPing = PingCreate ();

    if (pPing != NULL)
    {
        pPing->u4Dest = pPingEntry->u4IpPingDest;
        pPing->u4ContextId = pPingEntry->u4ContextId;
        pPing->i2Timeout = (INT2) pPingEntry->i4IpPingTimeout;
        pPing->i2MaxTries = (INT2) pPingEntry->i4IpPingTries;
        pPing->i2DataSize = (INT2) pPingEntry->i4IpPingMTU;
        pPing->i4RowStatus = ACTIVE;
        pPing->i2AppId = CLI_PING_APP_ID;
        pPing->i4PingIndex = (INT4) pPing->i2Id;
        STRNCPY (pPing->au1HostName, pPingEntry->au1HostName,
                 STRLEN (pPingEntry->au1HostName));
        pPing->au1HostName[STRLEN (pPingEntry->au1HostName)] = '\0';
    }
    return pPing;
}


#endif
