/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pingdef.h,v 1.11 2015/05/20 13:23:10 siva Exp $
 *
 * Description: Ping utility header file
 *
 *******************************************************************/
#ifndef   __PING_DEF_H__
#define   __PING_DEF_H__

#define PING_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define PING_RESTART_TIMER(TimerListId, pTmrRef, u4Duration)   \
   {\
      TmrStopTimer((TimerListId), (pTmrRef));   \
      TmrStartTimer((TimerListId), (pTmrRef), \
          ((UINT4) (u4Duration) * SYS_NUM_OF_TIME_UNITS_IN_A_SEC));   \
   }

#define   PING_MAX_INSTANCES                10 



#define PING_SCAN_INSTANCE_TABLE_FREE_ENTRY(pPtr, id) \
                   PING_SCAN_INSTANCE_TABLE(pPtr, id) \
                   if(pPtr->i2Id == PING_INVALID_ID)

#define   PING_PKT_ARRIVAL_EVENT      0x00000002
#define   PING_TASK_NAME              ((UINT1 *)"PNG")
#define   PING_TASK_SEM_NAME          ((UINT1 *)"PING")

#define   PING_TMR_EXPIRY_EVENT       0x00000001 
#define   PING_REPLY_TIMER_ID         1

#define   PING_ICMP_IDENTIFIER            23456
#define   PING_ICMP_ECHO                  8
#define   PING_ICMP_ECHO_REPLY            0

#define   PING_DEF_TIMEOUT                  1
#define   PING_MIN_TIMEOUT                  1
#define   PING_MAX_TIMEOUT                100

#define   PING_MIN_DATA_SIZE                0
#define   PING_MAX_DATA_SIZE                2080
#define   PING_DEF_DATA_SIZE                64

#define   PING_ICMP_HDR_SIZE                8
#define   PING_BUFFER_SIZE            (IP_HDR_LEN + IP_MAX_OPT_LEN + \
                                      PING_ICMP_HDR_SIZE + PING_MAX_DATA_SIZE + PING_CUST_MAX_DATA_SIZE)

#define   PING_DEF_TRIES                    3
#define   PING_MIN_TRIES                    1
#define   PING_MAX_TRIES                 1000

#define   PING_OPER_NOT_INITIATED           1
#define   PING_OPER_IN_PROGRESS             2
#define   PING_OPER_COMPLETED               3


#define   PING_INVALID_ID                   (-1)

#define   SNMP_PING_APP_ID                  1
#define   CLI_PING_APP_ID                   2

#define   PING_MAX_SEM_NAME_LEN            4

/* Data sent in Ping */
#define   PING_DATA                        0xA5

#ifdef TRACE_WANTED

# define PING_MOD_TRC     0x00020000
# define PING_NAME        "PING"
# define PING_DBG_FLAG    gu4PingDbg

#define PING_TRC_ARG(Val, Fmt)  if((PING_DBG_FLAG & PING_MOD_TRC) == PING_MOD_TRC) {\
    UtlTrcLog (PING_DBG_FLAG, Val, PING_NAME, Fmt); }

#else

#define PING_TRC_ARG(Val, Fmt) 

#endif


#endif            /* __PING_DEF_H__*/
