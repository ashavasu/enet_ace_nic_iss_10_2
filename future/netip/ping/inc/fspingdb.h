/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspingdb.h,v 1.5 2014/11/03 12:29:39 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPINGDB_H
#define _FSPINGDB_H

UINT1 FsPingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsping [] ={1,3,6,1,4,1,2076,106};
tSNMP_OID_TYPE fspingOID = {8, fsping};


UINT4 FsPingIndex [ ] ={1,3,6,1,4,1,2076,106,1,1,1,1};
UINT4 FsPingDest [ ] ={1,3,6,1,4,1,2076,106,1,1,1,2};
UINT4 FsPingTimeout [ ] ={1,3,6,1,4,1,2076,106,1,1,1,3};
UINT4 FsPingTries [ ] ={1,3,6,1,4,1,2076,106,1,1,1,4};
UINT4 FsPingDataSize [ ] ={1,3,6,1,4,1,2076,106,1,1,1,5};
UINT4 FsPingStatus [ ] ={1,3,6,1,4,1,2076,106,1,1,1,6};
UINT4 FsPingSendCount [ ] ={1,3,6,1,4,1,2076,106,1,1,1,7};
UINT4 FsPingAverageTime [ ] ={1,3,6,1,4,1,2076,106,1,1,1,8};
UINT4 FsPingMaxTime [ ] ={1,3,6,1,4,1,2076,106,1,1,1,9};
UINT4 FsPingMinTime [ ] ={1,3,6,1,4,1,2076,106,1,1,1,10};
UINT4 FsPingSuccesses [ ] ={1,3,6,1,4,1,2076,106,1,1,1,11};
UINT4 FsPingEntryStatus [ ] ={1,3,6,1,4,1,2076,106,1,1,1,12};
UINT4 FsPingHostName [ ] ={1,3,6,1,4,1,2076,106,1,1,1,13};


tMbDbEntry fspingMibEntry[]= {

{{12,FsPingIndex}, GetNextIndexFsPingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPingTableINDEX, 1, 0, 0, NULL},

{{12,FsPingDest}, GetNextIndexFsPingTable, FsPingDestGet, FsPingDestSet, FsPingDestTest, FsPingTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsPingTableINDEX, 1, 0, 0, NULL},

{{12,FsPingTimeout}, GetNextIndexFsPingTable, FsPingTimeoutGet, FsPingTimeoutSet, FsPingTimeoutTest, FsPingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPingTableINDEX, 1, 0, 0, "5"},

{{12,FsPingTries}, GetNextIndexFsPingTable, FsPingTriesGet, FsPingTriesSet, FsPingTriesTest, FsPingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPingTableINDEX, 1, 0, 0, "3"},

{{12,FsPingDataSize}, GetNextIndexFsPingTable, FsPingDataSizeGet, FsPingDataSizeSet, FsPingDataSizeTest, FsPingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPingTableINDEX, 1, 0, 0, "64"},

{{12,FsPingStatus}, GetNextIndexFsPingTable, FsPingStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPingTableINDEX, 1, 0, 0, NULL},

{{12,FsPingSendCount}, GetNextIndexFsPingTable, FsPingSendCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPingTableINDEX, 1, 0, 0, NULL},

{{12,FsPingAverageTime}, GetNextIndexFsPingTable, FsPingAverageTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPingTableINDEX, 1, 0, 0, NULL},

{{12,FsPingMaxTime}, GetNextIndexFsPingTable, FsPingMaxTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPingTableINDEX, 1, 0, 0, NULL},

{{12,FsPingMinTime}, GetNextIndexFsPingTable, FsPingMinTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPingTableINDEX, 1, 0, 0, NULL},

{{12,FsPingSuccesses}, GetNextIndexFsPingTable, FsPingSuccessesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPingTableINDEX, 1, 0, 0, NULL},

{{12,FsPingEntryStatus}, GetNextIndexFsPingTable, FsPingEntryStatusGet, FsPingEntryStatusSet, FsPingEntryStatusTest, FsPingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPingTableINDEX, 1, 0, 1, NULL},

{{12,FsPingHostName}, GetNextIndexFsPingTable, FsPingHostNameGet, FsPingHostNameSet, FsPingHostNameTest, FsPingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPingTableINDEX, 1, 0, 0, NULL},
};
tMibData fspingEntry = { 13, fspingMibEntry };

#endif /* _FSPINGDB_H */

