/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pingprot.h,v 1.7 2015/05/06 12:28:16 siva Exp $
 *
 * Description: Contains common includes used by PING submodule.
 *
 *******************************************************************/
#ifndef _PING_PROTO_H
#define _PING_PROTO_H

INT4 PingInit PROTO ((VOID));
VOID PingInitRec PROTO ((t_PING *pPing));

VOID  PingHandleTimerExpiry PROTO ((VOID));
INT4  PingOpenSocket PROTO ((t_PING *pPing));
INT4  PingCloseSocket PROTO ((VOID));
VOID  PingProcessPktInIcmpSocket PROTO ((INT4 i4SockFd));
INT4  PingGetValidInstanceCount PROTO ((VOID));

INT4      PingDelete PROTO ((t_PING * pPing));
t_PING  * PingCreate PROTO ((VOID));

INT4 PingSend PROTO ((t_PING * pPing));
INT4  PingReplyHandler PROTO ((VOID));
INT4  PingVcmIsVRExist PROTO ((UINT4));

PUBLIC INT4   PingLock PROTO ((VOID));
PUBLIC INT4   PingUnLock PROTO ((VOID));

PUBLIC VOID PingEnableDebug PROTO ((UINT4));

t_PING     *PingGetInstance (INT4 i4PingIndex);

#endif /* _PING_PROTO_H */
