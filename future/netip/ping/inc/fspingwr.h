/* $Id: fspingwr.h,v 1.4 2014/11/03 12:29:39 siva Exp $ */
#ifndef _FSPINGWR_H
#define _FSPINGWR_H
INT4 GetNextIndexFsPingTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSPING(VOID);

VOID UnRegisterFSPING(VOID);
INT4 FsPingDestGet(tSnmpIndex *, tRetVal *);
INT4 FsPingTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsPingTriesGet(tSnmpIndex *, tRetVal *);
INT4 FsPingDataSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsPingStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPingSendCountGet(tSnmpIndex *, tRetVal *);
INT4 FsPingAverageTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPingMaxTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPingMinTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPingSuccessesGet(tSnmpIndex *, tRetVal *);
INT4 FsPingEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPingHostNameGet(tSnmpIndex *, tRetVal *);
INT4 FsPingDestSet(tSnmpIndex *, tRetVal *);
INT4 FsPingTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsPingTriesSet(tSnmpIndex *, tRetVal *);
INT4 FsPingDataSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsPingEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPingHostNameSet(tSnmpIndex *, tRetVal *);
INT4 FsPingDestTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPingTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPingTriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPingDataSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPingEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPingHostNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





#endif /* _FSPINGWR_H */
