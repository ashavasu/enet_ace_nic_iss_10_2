/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspinglw.h,v 1.5 2014/11/03 12:29:39 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsPingTable. */
INT1
nmhValidateIndexInstanceFsPingTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPingTable  */

INT1
nmhGetFirstIndexFsPingTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPingTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPingDest ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPingTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingTries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingDataSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingSendCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingAverageTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingMaxTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingMinTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingSuccesses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPingEntryStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPingHostName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPingDest ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsPingTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPingTries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPingDataSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPingEntryStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPingHostName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPingDest ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsPingTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPingTries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPingDataSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPingEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPingHostName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
