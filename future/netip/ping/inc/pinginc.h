/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pinginc.h,v 1.8 2015/10/26 13:37:27 siva Exp $
 *
 * Description: Contains common includes used by PING submodule.
 *
 *******************************************************************/
#ifndef __PING_INC_H__
#define __PING_INC_H__
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "fssocket.h"
#include "pingdef.h"
#include "pingtdfs.h"
#include "pingprot.h"
#include "pingcli.h"
#include "vcm.h"
#include "dns.h"
#include   "fsutil.h"
#ifdef LNXIP4_WANTED
#include "lnxip.h"
#endif
t_PING     *PingInitRequest (tPingEntry * pPingEntry);
#endif /*__PING_INC_H__ */
