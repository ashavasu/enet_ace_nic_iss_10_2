/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmppilw.h,v 1.3 2014/11/03 12:29:39 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIPingTable. */
INT1
nmhValidateIndexInstanceFsMIPingTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIPingTable  */

INT1
nmhGetFirstIndexFsMIPingTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIPingTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIPingDest ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPingContextId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingTimeout ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingTries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingDataSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingSendCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingAverageTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingMaxTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingMinTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingSuccesses ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIPingEntryStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIPingHostName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIPingDest ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIPingContextId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPingTimeout ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPingTries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPingDataSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPingEntryStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIPingHostName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIPingDest ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIPingContextId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPingTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPingTries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPingDataSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPingEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIPingHostName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIPingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
