/* $Id: fsmppiwr.h,v 1.3 2014/11/03 12:29:39 siva Exp $ */
#ifndef _FSMPPIWR_H
#define _FSMPPIWR_H
INT4 GetNextIndexFsMIPingTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMPPI(VOID);

VOID UnRegisterFSMPPI(VOID);
INT4 FsMIPingDestGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingTriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingDataSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingSendCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingAverageTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingMaxTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingMinTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingSuccessesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingEntryStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingHostNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingDestSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingContextIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingTriesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingDataSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingEntryStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingHostNameSet(tSnmpIndex *, tRetVal *);
INT4 FsMIPingDestTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPingContextIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPingTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPingTriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPingDataSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPingEntryStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPingHostNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIPingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMPPIWR_H */
