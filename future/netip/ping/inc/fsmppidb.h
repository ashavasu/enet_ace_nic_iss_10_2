/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmppidb.h,v 1.3 2014/11/03 12:29:39 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPPIDB_H
#define _FSMPPIDB_H

UINT1 FsMIPingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsmppi [] ={1,3,6,1,4,1,29601,2,36};
tSNMP_OID_TYPE fsmppiOID = {9, fsmppi};


UINT4 FsMIPingIndex [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,1};
UINT4 FsMIPingDest [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,2};
UINT4 FsMIPingContextId [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,3};
UINT4 FsMIPingTimeout [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,4};
UINT4 FsMIPingTries [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,5};
UINT4 FsMIPingDataSize [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,6};
UINT4 FsMIPingStatus [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,7};
UINT4 FsMIPingSendCount [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,8};
UINT4 FsMIPingAverageTime [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,9};
UINT4 FsMIPingMaxTime [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,10};
UINT4 FsMIPingMinTime [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,11};
UINT4 FsMIPingSuccesses [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,12};
UINT4 FsMIPingEntryStatus [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,13};
UINT4 FsMIPingHostName [ ] ={1,3,6,1,4,1,29601,2,36,1,1,1,14};


tMbDbEntry fsmppiMibEntry[]= {

{{13,FsMIPingIndex}, GetNextIndexFsMIPingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIPingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIPingDest}, GetNextIndexFsMIPingTable, FsMIPingDestGet, FsMIPingDestSet, FsMIPingDestTest, FsMIPingTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIPingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIPingContextId}, GetNextIndexFsMIPingTable, FsMIPingContextIdGet, FsMIPingContextIdSet, FsMIPingContextIdTest, FsMIPingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPingTableINDEX, 1, 0, 0, "0"},

{{13,FsMIPingTimeout}, GetNextIndexFsMIPingTable, FsMIPingTimeoutGet, FsMIPingTimeoutSet, FsMIPingTimeoutTest, FsMIPingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPingTableINDEX, 1, 0, 0, "1"},

{{13,FsMIPingTries}, GetNextIndexFsMIPingTable, FsMIPingTriesGet, FsMIPingTriesSet, FsMIPingTriesTest, FsMIPingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPingTableINDEX, 1, 0, 0, "3"},

{{13,FsMIPingDataSize}, GetNextIndexFsMIPingTable, FsMIPingDataSizeGet, FsMIPingDataSizeSet, FsMIPingDataSizeTest, FsMIPingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIPingTableINDEX, 1, 0, 0, "64"},

{{13,FsMIPingStatus}, GetNextIndexFsMIPingTable, FsMIPingStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIPingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIPingSendCount}, GetNextIndexFsMIPingTable, FsMIPingSendCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIPingAverageTime}, GetNextIndexFsMIPingTable, FsMIPingAverageTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIPingMaxTime}, GetNextIndexFsMIPingTable, FsMIPingMaxTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIPingMinTime}, GetNextIndexFsMIPingTable, FsMIPingMinTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIPingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIPingSuccesses}, GetNextIndexFsMIPingTable, FsMIPingSuccessesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIPingTableINDEX, 1, 0, 0, NULL},

{{13,FsMIPingEntryStatus}, GetNextIndexFsMIPingTable, FsMIPingEntryStatusGet, FsMIPingEntryStatusSet, FsMIPingEntryStatusTest, FsMIPingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIPingTableINDEX, 1, 0, 1, NULL},

{{13,FsMIPingHostName}, GetNextIndexFsMIPingTable, FsMIPingHostNameGet, FsMIPingHostNameSet, FsMIPingHostNameTest, FsMIPingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIPingTableINDEX, 1, 0, 0, NULL},
};
tMibData fsmppiEntry = { 14, fsmppiMibEntry };

#endif /* _FSMPPIDB_H */

