/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pingtdfs.h,v 1.11 2015/05/20 13:23:11 siva Exp $
 *
 * Description: Ping utility header file
 *
 *******************************************************************/
#ifndef   __PING_TDF_H__
#define   __PING_TDF_H__
#include "dns.h"
/* Timer Structure used for Ping */
typedef struct
{
    tTmrAppTimer  TimerNode;
    UINT1          u1Id;             
    UINT1          u1Align;
    UINT2          u2Align;
} t_PING_TIMER;

typedef struct
{
    t_PING_TIMER Timer;
    tOsixSemId   SemId;       /* Semaphore Id of the PingEntry */
    UINT4        u4Dest;      /* Destination address to which ping is sent */
    UINT4        u4ContextId; /* The virtual router in which ping is 
                                 initiated */
    UINT4        u4TimeTaken; /* Time taken for Last Reply */
    INT4         i4PingIndex; /* Unique Identifier for PingEntry */
    INT4         i4RowStatus; /* Row Status of the Ping Entry */
    UINT2        u2AvgTime;   /* Average time taken for successful replies */
    UINT2        u2MaxTime;   /* Max delayed pkt so far */
    UINT2        u2MinTime;   /* Time taken by the fastest reply received */
    INT2         i2Timeout;   /* Timeout to be used for pinging */
    INT2         i2MaxTries;  /* Number of echo requests to send */
    INT2         i2DataSize;  /* Size of the data to be sent with ping */
    INT2         i2Id;        /* Used internally to match respons to request */
    INT2         i2Seq;
    INT2         i2SentCount;  /* No of requests sent already */
    INT2         i2Successes;  /* Total number of replies received */
    INT2         i2AppId;      /* if the ping is init by SNMP */
    UINT1        u1Align;
    INT1         i1Oper;       /* Indicates if the ping is in progress */
    INT1         ai1SendBuff[PING_BUFFER_SIZE];
    UINT4        u4Seconds;
    UINT4        u4NanoSeconds;
    UINT4        u4Source;
    UINT1        au1HostName[DNS_MAX_QUERY_LEN];
    UINT1        uIpPingDfBit;
} t_PING;
typedef struct
{
 tOsixSemId   SemId;
    UINT4        u4Dest;
    UINT4        u4TimeTaken;
    INT2         i2MaxTries;
    INT2         i2Successes;
    INT2         i2Timeout;
    INT2         i2SentCount;
    UINT1        au1HostName[DNS_MAX_QUERY_LEN];
    UINT1        au1Pad[1];
} t_ping;
    
typedef struct _PingIcmpHeader
{
    INT1       i1Type;
    INT1       i1Code;
    UINT2      u2Cksum;
    tIcmpArgs  args;
} tPingIcmpHdr;


#ifdef PINGMAIN_C
#define EXTERN
#else
#define EXTERN extern
#endif

EXTERN  t_PING  gaPingTable[PING_MAX_INSTANCES + PING_CUST_MAX_INSTANCES]; /* PING Data structure */
extern UINT1 gau1ZeroPingHostName[DNS_MAX_QUERY_LEN];
#define PING_SCAN_INSTANCE_TABLE(pPtr, id) \
       for(pPtr=&gaPingTable[0],id=0;id <(PING_MAX_INSTANCES + PING_CUST_MAX_INSTANCES);pPtr=&gaPingTable[++id])\

#endif            /*__IP_PING_H__*/
