#!/bin/csh
#* $Id: make.h,v 1.9 2016/03/04 11:04:30 siva Exp $
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :                                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 19th October 1999                             |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Cleanall option                            |
# |                            3. (Sample) Packaging (eg. make review)       |
# |                            4. Project/Module Backup (eg. daily, weekly)  |
# |                            5. Project FTP options (internal releases,    |
# |                               external releases if possible)             |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

# This switch enables the needed protocols to get compiled with IP
PROTOCOL_OPNS = -DIPOA_WANTED 

# This is used to specify the compiler option
COMPILER_TYPE = -DANSI -DUNIX

ifeq (${FSIP4}, YES) 
PROT_TASK_OPNS = -DIP_WANTED
endif

ifeq (${CLI_LNXIP}, YES)
PING_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                               -DIS_SLI_WRAPPER_MODULE
endif

ifeq (${DHCP_LNXIP}, YES)
FSIP_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                               -DIS_SLI_WRAPPER_MODULE
endif

GLOBAL_OPNS = ${TRACE_OPNS} ${DEBUG_OPNS} ${PROTOCOL_OPNS} \
              ${CONFIG_OPNS} ${DUMP_OPNS} ${COMPILER_TYPE} \
              ${PROT_TASK_OPNS} \
              ${GENERAL_COMPILATION_SWITCHES} ${SYSTEM_COMPILATION_SWITCHES}

############################################################################
#                         Directories                                      #
############################################################################


IP_BASE_DIR  = ${BASE_DIR}/netip
IP_OBJD      = ${BASE_DIR}/netip/obj

FSIP_BASE_DIR  = ${IP_BASE_DIR}/fsip
IP_INCD        = ${FSIP_BASE_DIR}/inc
FSIP_OBJD      = ${FSIP_BASE_DIR}/obj

IP_UDPD        = ${FSIP_BASE_DIR}/udp
IP_UDPINCD     = ${IP_UDPD}/inc
IP_UDPSRCD     = ${IP_UDPD}/src
IP_UDPOBJD     = ${IP_UDPD}/obj

IP_IPD         = ${FSIP_BASE_DIR}/ip
IP_IPINCD      = ${IP_IPD}/inc
IP_IPSRCD      = ${IP_IPD}/src
IP_IPOBJD      = ${IP_IPD}/obj

IP_IPIFD       = ${FSIP_BASE_DIR}/ipif
IP_IPIFINCD    = ${IP_IPIFD}/inc
IP_IPIFSRCD    = ${IP_IPIFD}/src
IP_IPIFOBJD    = ${IP_IPIFD}/obj

IP_ICMPD       = ${FSIP_BASE_DIR}/icmp
IP_ICMPSRCD    = ${IP_ICMPD}/src
IP_ICMPINCD    = ${IP_ICMPD}/inc
IP_ICMPOBJD    = ${IP_ICMPD}/obj

IP_V4OV6D      = ${FSIP_BASE_DIR}/v4ov6
IP_V4OV6SRCD   = ${IP_V4OV6D}
IP_V4OV6INCD   = ${IP_V4OV6D}
IP_V4OV6OBJD   = ${IP_V4OV6D}

IP_TRCRTD      = ${FSIP_BASE_DIR}/trcrt
IP_TRCRTINCD   = ${IP_TRCRTD}/inc
IP_TRCRTSRCD   = ${IP_TRCRTD}/src
IP_TRCRTOBJD   = ${IP_TRCRTD}/obj

IP_IRDPD       = ${FSIP_BASE_DIR}/irdp
IP_IRDPINCD    = ${IP_IRDPD}/inc
IP_IRDPSRCD    = ${IP_IRDPD}/src
IP_IRDPOBJD    = ${IP_IRDPD}/obj

IP_RARPD       = ${FSIP_BASE_DIR}/rarp
IP_RARPINCD    = ${IP_RARPD}/inc
IP_RARPSRCD    = ${IP_RARPD}/src
IP_RARPOBJD    = ${IP_RARPD}/obj

BOOTP_BASE_DIR = ${FSIP_BASE_DIR}/bootp
BOOTP_INCD     = ${BOOTP_BASE_DIR}/inc
BOOTP_SRCD     = ${BOOTP_BASE_DIR}/src
BOOTP_OBJD     = ${BOOTP_BASE_DIR}/obj

IP_RTMD      = ${IP_BASE_DIR}/rtm
IP_RTMINCD   = ${IP_RTMD}/inc
IP_RTMSRCD   = ${IP_RTMD}/src
IP_RTMOBJD   = ${IP_RTMD}/obj

ifeq (DIP_RTMTEST_WANTED, $(findstring DIP_RTMTEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
IP_RTMTSTD = $(IP_RTMD)/test
endif

IP_MGMTD      = ${IP_BASE_DIR}/ipmgmt
IP_MGMTINCD   = ${IP_MGMTD}/inc
IP_MGMTOBJD   = ${IP_MGMTD}/obj

IP_PINGD      = ${IP_BASE_DIR}/ping
IP_PINGINCD   = ${IP_PINGD}/inc
IP_PINGSRCD   = ${IP_PINGD}/src
IP_PINGOBJD   = ${IP_PINGD}/obj

TRIEINCD  = ${BASE_DIR}/util/trie
TRIELIBD  = ${BASE_DIR}/util/trie/lib
IP6LIBD  =  ${BASE_DIR}/util/ip6
############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

IP_GLOBAL_INCLUDES  = -I${IP_UDPINCD} \
                      -I${IP_INCD} -I${IP_IPINCD} -I${IP_RTMINCD}\
                      -I${IP_IPIFINCD} -I${IP_ICMPINCD} \
                      -I${IP_IRDPINCD} -I${TRIEINCD} -I${IP_PINGINCD}\
                      -I${IP_V4OV6INCD} -I${IP6LIBD} \
                      -I${IP_RARPINCD} \
                      -I${IP_MGMTINCD}

GLOBAL_INCLUDES = $(IP_GLOBAL_INCLUDES) \
                    $(COMMON_INCLUDE_DIRS)

ifeq (DIP_RTMTEST_WANTED, $(findstring DIP_RTMTEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
RTM_INCLUDES += -I$(RTM_TSTD)
endif

#############################################################################

