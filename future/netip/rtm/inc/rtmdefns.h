/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmdefns.h,v 1.42 2016/07/29 09:53:38 siva Exp $
 *
 * Description: This file contains #definitions used in the 
 *              RTM module. 
 *
 *******************************************************************/
 #ifndef _RTM_DEFNS_H
 #define _RTM_DEFNS_H

/* Contains definitions used in RTM */
#define   RTM_MIN(a,b)   (a < b) ? a : b
#define   RTM_ROUTE_PERMIT                    1
#define   RTM_ROUTE_DENY                      2

#define   RTM_PERMIT_TABLE         RTM_ROUTE_PERMIT
#define   RTM_DENY_TABLE           RTM_ROUTE_DENY

#define   RTM_DEF_CIDR_METRIC1              -1

#define   RTM_ADMIN_STATUS_ENABLED                1
#define   RTM_ADMIN_STATUS_DISABLED               2

#define   RTM_IP_ROUTE_MAX_DISTANCE               255
#define   RTM_IP_ROUTE_MIN_DISTANCE               1

#define   RTM_FILTER_ENABLED                      1
#define   RTM_FILTER_DISABLED                     2

#define   RTM_REDISTRIBUTION_ENABLED              1
#define   RTM_REDISTRIBUTION_DISABLED             2

#define   RTM_ECMP_ACROSS_PROTO_ENABLED           1
#define   RTM_ECMP_ACROSS_PROTO_DISABLED          2

#define   RTM_VRF_ROUTE_LEAK_ENABLED              1
#define   RTM_VRF_ROUTE_LEAK_DISABLED             2

#define   RTM_AUTOMATIC_BIT_SET          0x80000000

#define   RTM_AUDIT_SHOW_CMD    "snmpwalk mib name fsMIStdInetCidrRouteTable > "
#define   RTM_CLI_MAX_GROUPS_LINE_LEN    200
#define   RTM_CLI_EOF                  2
#define   RTM_CLI_NO_EOF                 1
#define   RTM_CLI_RDONLY               OSIX_FILE_RO
#define   RTM_CLI_WRONLY               OSIX_FILE_WO


#define   RTM_OSPF_INTERNAL_ROUTES                1
#define   RTM_OSPF_EXTERNAL_ROUTES                2

/* Possible cmd values from SNMP to RTM */
#define RTM_CHG_IN_RRD_CONTROL_TABLE     1
#define RTM_OSPF_INT_EXT_CHG             2
#define RTM_CHG_IN_RRD_DEF_CTRL_MODE     3

/* RTM Task Processing Event */
#define  RTM_MESSAGE_ARRIVAL_EVENT       0x00000001
#define  RTM_FILTER_ADDED_EVENT          0x00000002
#define  RTM_TMR_EVENT                   0x00000004
#define  RTM_RM_PKT_EVENT                0x00000008
#define  RTM_HA_PEND_BLKUPD_EVENT        0x00000010
#define  RTM_RED_TIMER_EVENT             0x00000020
#define  RTM_ROUTE_MSG_EVENT             0x00000040
#define  RTM_ROUTE_PROCESS_EVENT         0x00000080
#define  RTM_HA_FRT_PEND_BLKUPD_EVENT    0x00000400

#define   RTM_INVALID_REGN_ID               0xff
#define   NULL_STR                          "\0\0\0\0"
#define   RTM_INVALID_ROUTING_PROTOCOL_ID   0xff

#define   RTM_MAX_MESSAGE_SIZE               2800

#define   RTM_ROUTE_REDISTRIBUTED               1
#define   RTM_ROUTE_TO_BE_REDISTRIBUTED         2
#define   RTM_ROUTE_TO_BE_DELETED               3

#define   RTM_DONT_REDISTRIBUTE                 0

#define   RTM_ROUTE_DONT_DELETE                 1
#define   RTM_ROUTE_DELETE                      2

#define   RTM_SEND_ROUTE_UPDATE                 1
#define   RTM_SEND_ROUTE_CHG_NOTIFY             2

#define   RTM_MAX_TRIE_NUM_ENTRIES     50

#define   RTM_ANY_PROTOCOL              0
#define   RTM_MAX_KEY_SIZE              8

#define   RTM_NO_CHG_IN_EXPORT_LIST     1
#define   RTM_CHG_IN_EXPORT_LIST        2

#define   RTM_SEND_RT_CHG_MSG           1
#define   RTM_DONT_SEND_RT_CHG_MSG      2
#define   RTM_DELETE_INSTALLED_RT       3

/* RTM Timer Related Constants */
#define   RTM_ONESEC_TIMER              1

#define   RTM_GR_TIMER                  8

  /*Timer to set  DLF setting */
#define   RTM_DLF_TIMER                 2
#define   RTM_DLF_TIMER_INTERVAL       30
/* Timer to initiate arp resolution for ECMP route */
#define   RTM_ECMPPRT_TIMER             4
#define   RTM_ECMPPRT_TIMER_INTERVAL   60
#define   RTM_ECMP_TIMER_STOP           0
#define   RTM_ECMP_TIMER_RUNNING        1

/* Timer to initiate retry for failed routes */
#define   RTM_FRT_TIMER             9
#define   RTM_FRT_TIMER_INTERVAL   60

#define   RTM_MAX_TMR                   4


#define DEFAULT_ROUTE            0x00000000

#define RTM_LEFT_MOST_NIBBLE_TAG_MASK    0xf0000000
#define RTM_BGP_DENY_TAG1        0xa0000000
#define RTM_BGP_DENY_TAG2        0xe0000000
#define RTM_ROUTESNUM_TO_SCAN 20 /* No Logical reason for this no */

/* Protocol Preference  moved to code/future/inc/ip.h */

#define RTM_MAX_PATHS                MAX_PATHS

#define   RTM_RB_GREATER 1
#define   RTM_RB_EQUAL   0
#define   RTM_RB_LESS   -1
#define   RTM_SNMP_TRUE   1
#define   RTM_SNMP_FALSE  2
#define   RTM_SET_RT_REACHABLE         1
#define   RTM_DONT_SET_RT_REACHABLE    2

#define  RTM_MORE                      0x01
#define  RTM_COMPLETE                  0x02
                                          
#define RTM_NO_RRT_WITH_THIS_NH         0xff /* This macro indicates that 
                     there exist no entry in Reachable  next hop table(RNHT) */


/* OSPF route types */

#define IP_OSPF_INTRA_AREA    1
#define IP_OSPF_INTER_AREA    2
#define IP_OSPF_TYPE_1_EXT    3
#define IP_OSPF_TYPE_2_EXT    4
#define IP_OSPF_NSSA_1_EXT    5
#define IP_OSPF_NSSA_2_EXT    6


#define RTM_GREATER      1
#define RTM_LESSER      -1
#define RTM_EQUAL        0

#define RTM_ONE          1

#define RTM_INDEX_COMPARE(pRtmInfo, u4IpAddr) \
     ((pRtmInfo->u4Ip_addr > u4IpAddr) ? RTM_GREATER : \
         ((pRtmInfo->u4Ip_addr < u4IpAddr) ? RTM_LESSER : RTM_EQUAL))

#define GLOBAL_HW_AUDIT_LEVEL HW_AUDIT_NORMAL
#define HW_AUDIT_OPTIMISED    1
#define HW_AUDIT_NORMAL       2

#define   NP_PRESENT            1
#define   NP_NOT_PRESENT        2

#ifdef L2RED_WANTED
#define RTM_GET_NODE_STATUS()  gRtmRedGlobalInfo.u1NodeStatus
#else
#define RTM_GET_NODE_STATUS()  RM_ACTIVE
#endif

#define RTM_AUDIT_FILE_ACTIVE "/tmp/rtm_output_file_active"
#define RTM_AUDIT_FILE_STDBY "/tmp/rtm_output_file_stdby"

#ifdef L2RED_WANTED
#define RTM_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define RTM_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

#ifdef RM_WANTED
#define   RTM_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? RTM_SUCCESS : RTM_FAILURE)
#else
#define   RTM_IS_NP_PROGRAMMING_ALLOWED() RTM_SUCCESS
#endif

#endif
