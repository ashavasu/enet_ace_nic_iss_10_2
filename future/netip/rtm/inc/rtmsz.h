/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmsz.h,v 1.8 2016/10/07 13:19:26 siva Exp $
 *
 * Description:This file contains common includes in
 *             the RTM module.
 *
 *******************************************************************/

#ifndef _RTMSZ_H
enum {
    MAX_RTM_APP_SPEC_INFO_SIZING_ID,
    MAX_RTM_CONTEXTS_SIZING_ID,
    MAX_RTM_PEND_NEXTHOP_SIZING_ID,
    MAX_RTM_PEND_RT_ENTRIES_SIZING_ID,
    MAX_RTM_REDIST_NODES_SIZING_ID,
    MAX_RTM_RESLV_NEXTHOP_SIZING_ID,
    MAX_RTM_ROUTE_TABLE_ENTRIES_SIZING_ID,
    MAX_RTM_RRD_CTRL_INFO_SIZING_ID,
    MAX_RTM_RM_QUE_SIZE_SIZING_ID,
    MAX_RTM_DYN_MSG_SIZE_SIZING_ID,
    MAX_RTM_ROUTE_MSG_SIZING_ID,
    MAX_RTM_FRT_SIZING_ID,
    MAX_RTM_COMM_SIZING_ID,
    RTM_MAX_SIZING_ID
};


#ifdef  _RTMSZ_C
tMemPoolId RTMMemPoolIds[ RTM_MAX_SIZING_ID];
INT4  RtmSizingMemCreateMemPools(VOID);
VOID  RtmSizingMemDeleteMemPools(VOID);
INT4  RtmSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RTMSZ_C  */
extern tMemPoolId RTMMemPoolIds[ ];
extern INT4  RtmSizingMemCreateMemPools(VOID);
extern VOID  RtmSizingMemDeleteMemPools(VOID);
extern INT4  RtmSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _RTMSZ_C  */


#ifdef  _RTMSZ_C
tFsModSizingParams FsRTMSizingParams [] = {
{ "tAppSpecInfo", "MAX_RTM_APP_SPEC_INFO", sizeof(tAppSpecInfo),MAX_RTM_APP_SPEC_INFO, MAX_RTM_APP_SPEC_INFO,0 },
{ "tRtmCxt", "MAX_RTM_CONTEXTS", sizeof(tRtmCxt),MAX_RTM_CONTEXTS, MAX_RTM_CONTEXTS,0 },
{ "tPNhNode", "MAX_RTM_PEND_NEXTHOP", sizeof(tPNhNode),MAX_RTM_PEND_NEXTHOP, MAX_RTM_PEND_NEXTHOP,0 },
{ "tPRtEntry", "MAX_RTM_PEND_RT_ENTRIES", sizeof(tPRtEntry),MAX_RTM_PEND_RT_ENTRIES, MAX_RTM_PEND_RT_ENTRIES,0 },
{ "tRtmRedisNode", "MAX_RTM_REDIST_NODES", sizeof(tRtmRedisNode),MAX_RTM_REDIST_NODES, MAX_RTM_REDIST_NODES,0 },
{ "tRNhNode", "MAX_RTM_RESLV_NEXTHOP", sizeof(tRNhNode),MAX_RTM_RESLV_NEXTHOP, MAX_RTM_RESLV_NEXTHOP,0 },
{ "tRtInfo", "MAX_RTM_ROUTE_TABLE_ENTRIES", sizeof(tRtInfo),MAX_RTM_ROUTE_TABLE_ENTRIES, MAX_RTM_ROUTE_TABLE_ENTRIES,0 },
{ "tRrdControlInfo", "MAX_RTM_RRD_CTRL_INFO", sizeof(tRrdControlInfo),MAX_RTM_RRD_CTRL_INFO, MAX_RTM_RRD_CTRL_INFO,0 },
{ "tRtmRmMsg", "MAX_RTM_RM_QUE_SIZE", sizeof(tRtmRmMsg),MAX_RTM_RM_QUE_SIZE, MAX_RTM_RM_QUE_SIZE,0 },
{ "tRtmRedTable", "MAX_RTM_DYN_MSG_SIZE", sizeof(tRtmRedTable),MAX_RTM_DYN_MSG_SIZE, MAX_RTM_DYN_MSG_SIZE,0 },
{ "tRtmRtMsg", "MAX_RTM_ROUTE_MSG", sizeof(tRtmRtMsg),MAX_RTM_ROUTE_MSG, MAX_RTM_ROUTE_MSG,0 },
{"tRtmFrtInfo","MAX_RTM_FAILED_ROUTE",sizeof(tRtmFrtInfo), MAX_RTM_FAILED_ROUTE,MAX_RTM_FAILED_ROUTE,0},
{ "tRtRmapComm", "MAX_RTM_COMM",sizeof(tRtRmapComm),MAX_RTM_COMM,MAX_RTM_COMM,0},
{"\0","\0",0,0,0,0}
};
#else  /*  _RTMSZ_C  */
extern tFsModSizingParams FsRTMSizingParams [];
#endif /*  _RTMSZ_C  */

#endif
