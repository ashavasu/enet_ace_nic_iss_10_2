/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmextn.h,v 1.14 2015/06/03 02:54:26 siva Exp $
 *
 * Description:This file contains extern definitions used in
 *             the RTM module.                              
 *                               
 *******************************************************************/
#ifndef _RTM_EXTN_H
#define _RTM_EXTN_H

extern tRtmSystemSize       gRtmSystemSize;

extern tRtmGlobalInfo       gRtmGlobalInfo;

extern tOsixQId            gRtmMsgQId;
extern tOsixQId            gRtmSnmpMsgQId;
extern tOsixQId            gRtmRtMsgQId;
extern tOsixTaskId         gRtmTaskId;
extern UINT1          gu1FIBFlag; /* Indicates wheteher FIB is full or not*/
extern UINT1          gu1RtmRouteCxtFlag; /* Flag to indicate Route Handling in Data Plane in RTM thread context */
extern INT4 ArpSendRequest PROTO ((UINT2 u2Port,
                                   UINT4 u4Dest,
                                   INT2 i2Hardware, UINT1 u1EncapType));

extern tRtmRedGlobalInfo   gRtmRedGlobalInfo;
extern  UINT4 IssSzGetSizingParamsForModule (CHR1 * pu1ModName, CHR1 * pu1MacroName);
extern  UINT1               gu1ECMPPRTTimerRunning;

#endif /* _RTM_EXTN_H */
