/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmfrt.h,v 1.1 2015/04/24 12:04:51 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for RTM Failed route module.
 *              
 *******************************************************************/
#ifndef __RTM_FRT_H
#define __RTM_FRT_H

INT4 RtmRBTreeFrtEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn);
PUBLIC INT4 RtmFrtInitGlobalInfo (VOID);
INT4 RtmFrtAddInfo (tRtInfo * pRtInfo, UINT4 u4CxtId, UINT1 u1RtCount);
tRtmFrtInfo *RtmFrtGetInfo (tRtInfo * pRtInfo, UINT4 u4CxtId);
INT4 RtmFrtDeleteEntry (tRtmFrtInfo *pRtmFrtNode);
VOID
RtmGetFrtCtxId(tRtmFrtInfo *pRtmFrtNode,UINT4 *pCtxId);
tRtmFrtInfo *
RtmGetNextFrtEntry(tRtmFrtInfo *pRtmFrtNode);
tRtmFrtInfo *
RtmGetFirstFrtEntry(VOID);
INT4
RtmFrtDeInitGlobalInfo (VOID);
VOID
RtmGetFrtTableCount(UINT4 *pFrtCount);
#define MAX_RTM_RETRY_ROUTE_COUNT 50
#endif /* __RTM_RED_H */
