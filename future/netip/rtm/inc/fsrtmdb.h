/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrtmdb.h,v 1.15 2016/02/08 10:40:06 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRTMDB_H
#define _FSRTMDB_H

UINT1 FsRrdControlTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsRrdRoutingProtoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsRtmCommonRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsrtm [] ={1,3,6,1,4,1,2076,107};
tSNMP_OID_TYPE fsrtmOID = {8, fsrtm};


UINT4 FsRrdRouterId [ ] ={1,3,6,1,4,1,2076,107,1,1};
UINT4 FsRrdFilterByOspfTag [ ] ={1,3,6,1,4,1,2076,107,1,2};
UINT4 FsRrdFilterOspfTag [ ] ={1,3,6,1,4,1,2076,107,1,3};
UINT4 FsRrdFilterOspfTagMask [ ] ={1,3,6,1,4,1,2076,107,1,4};
UINT4 FsRrdRouterASNumber [ ] ={1,3,6,1,4,1,2076,107,1,5};
UINT4 FsRrdAdminStatus [ ] ={1,3,6,1,4,1,2076,107,1,6};
UINT4 FsRtmThrottleLimit [ ] ={1,3,6,1,4,1,2076,107,1,7};
UINT4 FsRtmMaximumBgpRoutes [ ] ={1,3,6,1,4,1,2076,107,1,8};
UINT4 FsRtmMaximumOspfRoutes [ ] ={1,3,6,1,4,1,2076,107,1,9};
UINT4 FsRtmMaximumRipRoutes [ ] ={1,3,6,1,4,1,2076,107,1,10};
UINT4 FsRtmMaximumStaticRoutes [ ] ={1,3,6,1,4,1,2076,107,1,11};
UINT4 FsRtmMaximumISISRoutes [ ] ={1,3,6,1,4,1,2076,107,1,12};
UINT4 FsRtmIpStaticRouteDistance [ ] ={1,3,6,1,4,1,2076,107,1,13};
UINT4 FsEcmpAcrossProtocolAdminStatus [ ] ={1,3,6,1,4,1,2076,107,1,14};
UINT4 FsRtmRouteLeakStatus [ ] ={1,3,6,1,4,1,2076,107,1,15};
UINT4 FsRrdControlDestIpAddress [ ] ={1,3,6,1,4,1,2076,107,2,1,1};
UINT4 FsRrdControlNetMask [ ] ={1,3,6,1,4,1,2076,107,2,1,2};
UINT4 FsRrdControlSourceProto [ ] ={1,3,6,1,4,1,2076,107,2,1,3};
UINT4 FsRrdControlDestProto [ ] ={1,3,6,1,4,1,2076,107,2,1,4};
UINT4 FsRrdControlRouteExportFlag [ ] ={1,3,6,1,4,1,2076,107,2,1,5};
UINT4 FsRrdControlRowStatus [ ] ={1,3,6,1,4,1,2076,107,2,1,6};
UINT4 FsRrdRoutingProtoId [ ] ={1,3,6,1,4,1,2076,107,3,1,1};
UINT4 FsRrdRoutingRegnId [ ] ={1,3,6,1,4,1,2076,107,3,1,2};
UINT4 FsRrdRoutingProtoTaskIdent [ ] ={1,3,6,1,4,1,2076,107,3,1,3};
UINT4 FsRrdRoutingProtoQueueIdent [ ] ={1,3,6,1,4,1,2076,107,3,1,4};
UINT4 FsRrdAllowOspfAreaRoutes [ ] ={1,3,6,1,4,1,2076,107,3,1,5};
UINT4 FsRrdAllowOspfExtRoutes [ ] ={1,3,6,1,4,1,2076,107,3,1,6};
UINT4 FsRtmCommonRouteDest [ ] ={1,3,6,1,4,1,2076,107,4,1,1};
UINT4 FsRtmCommonRouteMask [ ] ={1,3,6,1,4,1,2076,107,4,1,2};
UINT4 FsRtmCommonRouteTos [ ] ={1,3,6,1,4,1,2076,107,4,1,3};
UINT4 FsRtmCommonRouteNextHop [ ] ={1,3,6,1,4,1,2076,107,4,1,4};
UINT4 FsRtmCommonRouteIfIndex [ ] ={1,3,6,1,4,1,2076,107,4,1,5};
UINT4 FsRtmCommonRouteType [ ] ={1,3,6,1,4,1,2076,107,4,1,6};
UINT4 FsRtmCommonRouteProto [ ] ={1,3,6,1,4,1,2076,107,4,1,7};
UINT4 FsRtmCommonRouteAge [ ] ={1,3,6,1,4,1,2076,107,4,1,8};
UINT4 FsRtmCommonRouteInfo [ ] ={1,3,6,1,4,1,2076,107,4,1,9};
UINT4 FsRtmCommonRouteNextHopAS [ ] ={1,3,6,1,4,1,2076,107,4,1,10};
UINT4 FsRtmCommonRouteMetric1 [ ] ={1,3,6,1,4,1,2076,107,4,1,11};
UINT4 FsRtmCommonRoutePrivateStatus [ ] ={1,3,6,1,4,1,2076,107,4,1,12};
UINT4 FsRtmCommonRouteStatus [ ] ={1,3,6,1,4,1,2076,107,4,1,13};
UINT4 FsRtmCommonRoutePreference [ ] ={1,3,6,1,4,1,2076,107,4,1,14};
UINT4 FsRtmRedEntryTime [ ] ={1,3,6,1,4,1,2076,107,5,1};
UINT4 FsRtmRedExitTime [ ] ={1,3,6,1,4,1,2076,107,5,2};

tMbDbEntry fsrtmMibEntry[]= {

{{10,FsRrdRouterId}, NULL, FsRrdRouterIdGet, FsRrdRouterIdSet, FsRrdRouterIdTest, FsRrdRouterIdDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrdFilterByOspfTag}, NULL, FsRrdFilterByOspfTagGet, FsRrdFilterByOspfTagSet, FsRrdFilterByOspfTagTest, FsRrdFilterByOspfTagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRrdFilterOspfTag}, NULL, FsRrdFilterOspfTagGet, FsRrdFilterOspfTagSet, FsRrdFilterOspfTagTest, FsRrdFilterOspfTagDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRrdFilterOspfTagMask}, NULL, FsRrdFilterOspfTagMaskGet, FsRrdFilterOspfTagMaskSet, FsRrdFilterOspfTagMaskTest, FsRrdFilterOspfTagMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "-1"},

{{10,FsRrdRouterASNumber}, NULL, FsRrdRouterASNumberGet, FsRrdRouterASNumberSet, FsRrdRouterASNumberTest, FsRrdRouterASNumberDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRrdAdminStatus}, NULL, FsRrdAdminStatusGet, FsRrdAdminStatusSet, FsRrdAdminStatusTest, FsRrdAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRtmThrottleLimit}, NULL, FsRtmThrottleLimitGet, FsRtmThrottleLimitSet, FsRtmThrottleLimitTest, FsRtmThrottleLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},

{{10,FsRtmMaximumBgpRoutes}, NULL, FsRtmMaximumBgpRoutesGet, FsRtmMaximumBgpRoutesSet, FsRtmMaximumBgpRoutesTest, FsRtmMaximumBgpRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRtmMaximumOspfRoutes}, NULL, FsRtmMaximumOspfRoutesGet, FsRtmMaximumOspfRoutesSet, FsRtmMaximumOspfRoutesTest, FsRtmMaximumOspfRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRtmMaximumRipRoutes}, NULL, FsRtmMaximumRipRoutesGet, FsRtmMaximumRipRoutesSet, FsRtmMaximumRipRoutesTest, FsRtmMaximumRipRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRtmMaximumStaticRoutes}, NULL, FsRtmMaximumStaticRoutesGet, FsRtmMaximumStaticRoutesSet, FsRtmMaximumStaticRoutesTest, FsRtmMaximumStaticRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRtmMaximumISISRoutes}, NULL, FsRtmMaximumISISRoutesGet, FsRtmMaximumISISRoutesSet, FsRtmMaximumISISRoutesTest, FsRtmMaximumISISRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRtmIpStaticRouteDistance}, NULL, FsRtmIpStaticRouteDistanceGet, FsRtmIpStaticRouteDistanceSet, FsRtmIpStaticRouteDistanceTest, FsRtmIpStaticRouteDistanceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsEcmpAcrossProtocolAdminStatus}, NULL, FsEcmpAcrossProtocolAdminStatusGet, FsEcmpAcrossProtocolAdminStatusSet, FsEcmpAcrossProtocolAdminStatusTest, FsEcmpAcrossProtocolAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsRtmRouteLeakStatus}, NULL, FsRtmRouteLeakStatusGet, FsRtmRouteLeakStatusSet, FsRtmRouteLeakStatusTest, FsRtmRouteLeakStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},


{{11,FsRrdControlDestIpAddress}, GetNextIndexFsRrdControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRrdControlTableINDEX, 2, 0, 0, NULL},

{{11,FsRrdControlNetMask}, GetNextIndexFsRrdControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRrdControlTableINDEX, 2, 0, 0, NULL},

{{11,FsRrdControlSourceProto}, GetNextIndexFsRrdControlTable, FsRrdControlSourceProtoGet, FsRrdControlSourceProtoSet, FsRrdControlSourceProtoTest, FsRrdControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrdControlTableINDEX, 2, 0, 0, "1"},

{{11,FsRrdControlDestProto}, GetNextIndexFsRrdControlTable, FsRrdControlDestProtoGet, FsRrdControlDestProtoSet, FsRrdControlDestProtoTest, FsRrdControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRrdControlTableINDEX, 2, 0, 0, "0"},

{{11,FsRrdControlRouteExportFlag}, GetNextIndexFsRrdControlTable, FsRrdControlRouteExportFlagGet, FsRrdControlRouteExportFlagSet, FsRrdControlRouteExportFlagTest, FsRrdControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrdControlTableINDEX, 2, 0, 0, "1"},

{{11,FsRrdControlRowStatus}, GetNextIndexFsRrdControlTable, FsRrdControlRowStatusGet, FsRrdControlRowStatusSet, FsRrdControlRowStatusTest, FsRrdControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrdControlTableINDEX, 2, 0, 1, NULL},

{{11,FsRrdRoutingProtoId}, GetNextIndexFsRrdRoutingProtoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsRrdRoutingProtoTableINDEX, 1, 0, 0, NULL},

{{11,FsRrdRoutingRegnId}, GetNextIndexFsRrdRoutingProtoTable, FsRrdRoutingRegnIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRrdRoutingProtoTableINDEX, 1, 0, 0, NULL},

{{11,FsRrdRoutingProtoTaskIdent}, GetNextIndexFsRrdRoutingProtoTable, FsRrdRoutingProtoTaskIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRrdRoutingProtoTableINDEX, 1, 0, 0, NULL},

{{11,FsRrdRoutingProtoQueueIdent}, GetNextIndexFsRrdRoutingProtoTable, FsRrdRoutingProtoQueueIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsRrdRoutingProtoTableINDEX, 1, 0, 0, NULL},

{{11,FsRrdAllowOspfAreaRoutes}, GetNextIndexFsRrdRoutingProtoTable, FsRrdAllowOspfAreaRoutesGet, FsRrdAllowOspfAreaRoutesSet, FsRrdAllowOspfAreaRoutesTest, FsRrdRoutingProtoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrdRoutingProtoTableINDEX, 1, 0, 0, "1"},

{{11,FsRrdAllowOspfExtRoutes}, GetNextIndexFsRrdRoutingProtoTable, FsRrdAllowOspfExtRoutesGet, FsRrdAllowOspfExtRoutesSet, FsRrdAllowOspfExtRoutesTest, FsRrdRoutingProtoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRrdRoutingProtoTableINDEX, 1, 0, 0, "1"},

{{11,FsRtmCommonRouteDest}, GetNextIndexFsRtmCommonRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsRtmCommonRouteMask}, GetNextIndexFsRtmCommonRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsRtmCommonRouteTos}, GetNextIndexFsRtmCommonRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsRtmCommonRouteNextHop}, GetNextIndexFsRtmCommonRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsRtmCommonRouteIfIndex}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRouteIfIndexGet, FsRtmCommonRouteIfIndexSet, FsRtmCommonRouteIfIndexTest, FsRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRtmCommonRouteTableINDEX, 4, 0, 0, "0"},

{{11,FsRtmCommonRouteType}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRouteTypeGet, FsRtmCommonRouteTypeSet, FsRtmCommonRouteTypeTest, FsRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsRtmCommonRouteProto}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRouteProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsRtmCommonRouteAge}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsRtmCommonRouteTableINDEX, 4, 0, 0, "0"},

{{11,FsRtmCommonRouteInfo}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRouteInfoGet, FsRtmCommonRouteInfoSet, FsRtmCommonRouteInfoTest, FsRtmCommonRouteTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsRtmCommonRouteNextHopAS}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRouteNextHopASGet, FsRtmCommonRouteNextHopASSet, FsRtmCommonRouteNextHopASTest, FsRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRtmCommonRouteTableINDEX, 4, 0, 0, "0"},

{{11,FsRtmCommonRouteMetric1}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRouteMetric1Get, FsRtmCommonRouteMetric1Set, FsRtmCommonRouteMetric1Test, FsRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRtmCommonRouteTableINDEX, 4, 0, 0, "-1"},

{{11,FsRtmCommonRoutePrivateStatus}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRoutePrivateStatusGet, FsRtmCommonRoutePrivateStatusSet, FsRtmCommonRoutePrivateStatusTest, FsRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{11,FsRtmCommonRouteStatus}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRouteStatusGet, FsRtmCommonRouteStatusSet, FsRtmCommonRouteStatusTest, FsRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRtmCommonRouteTableINDEX, 4, 0, 1, NULL},

{{11,FsRtmCommonRoutePreference}, GetNextIndexFsRtmCommonRouteTable, FsRtmCommonRoutePreferenceGet, FsRtmCommonRoutePreferenceSet, FsRtmCommonRoutePreferenceTest, FsRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRtmCommonRouteTableINDEX, 4, 0, 0, NULL},

{{10,FsRtmRedEntryTime}, NULL, FsRtmRedEntryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsRtmRedExitTime}, NULL, FsRtmRedExitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fsrtmEntry = { sizeof(fsrtmMibEntry)/sizeof(fsrtmMibEntry[0]), fsrtmMibEntry };
#endif /* _FSRTMDB_H */

