/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmport.h,v 1.15 2015/07/15 09:57:48 siva Exp $
 *
 * Description:This file contains porting related           
 *             defintions used in RTM module.               
 *
 *******************************************************************/
#ifndef _RTM_PORT_H
#define _RTM_PORT_H
#endif /* _RTM_PORT_H */

#define   RTM_MAIN_TASK_NAME    "RTM"
#define   RTM_TASK_MODE         OSIX_DEFAULT_TASK_MODE
#define   RTM_TASK_PRIORITY     30
#define   RTM_TASK_STACK_SIZE   0x8000

/* Includes LOCAL, STATIC, RIP, OSPF and BGP routes */

#define RTM_INIT_COMPLETE(u4Status)       lrInitComplete(u4Status)
#define   RTM_MESSAGE_ARRIVAL_Q_DEPTH      IP_DEV_MAX_IP_INTF * 2 /* Queue Depth = Total IP interfaces available in h/w *2 */ 

#define   RTM_SNMP_IF_Q_DEPTH              10
#define   RTM_RT_MSG_Q_DEPTH               1000

#define   RTM_MESSAGE_ARRIVAL_Q_NAME      "RTMQ"
#define   RTM_MESSAGE_ARRIVAL_Q_MODE      OSIX_GLOBAL

#define   RTM_SNMP_IF_Q_NAME              "RTSQ"
#define   RTM_SNMP_IF_Q_MODE              OSIX_GLOBAL
#define   RTM_RT_MSG_Q_NAME              "RTRQ" 
#define   RTM_RT_MSG_Q_MODE              OSIX_GLOBAL 


#define   RTM_EVENT_WAIT_FLAGS            (OSIX_WAIT | OSIX_EV_ANY)

#define   RTM_EVENT_WAIT_TIMEOUT                0

#define   RTM_RM_PKT_ARRIVAL_Q_DEPTH     10
#define   RTM_RM_PKT_ARRIVAL_Q_NAME      "RTMRMQ"

#define RTM_PROTOCOL_SEMAPHORE       ((UINT1 *) "RTMP")
#define ROUTE_TABLE_SEMA4            ((UINT1 *) "RTBL")   

#define  RTM_FIB_FULL                 1
#define  RTM_FIB_NOT_FULL             2
