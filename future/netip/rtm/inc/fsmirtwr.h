/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmirtwr.h,v 1.8 2016/02/08 10:40:06 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _FSMIRTWR_H
#define _FSMIRTWR_H

VOID RegisterFSMIRT(VOID);

VOID UnRegisterFSMIRT(VOID);
INT4 FsMIRtmThrottleLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsMIEcmpAcrossProtocolAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmRouteLeakStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmThrottleLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsMIEcmpAcrossProtocolAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmRouteLeakStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmThrottleLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIEcmpAcrossProtocolAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmRouteLeakStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmThrottleLimitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIEcmpAcrossProtocolAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIRtmRouteLeakStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMIRtmTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIRrdRouterIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterByOspfTagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterOspfTagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterOspfTagMaskGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdRouterASNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdForceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRTMIpStaticRouteDistanceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdRouterIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterByOspfTagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterOspfTagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterOspfTagMaskSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdRouterASNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdForceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRTMIpStaticRouteDistanceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdRouterIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterByOspfTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterOspfTagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdFilterOspfTagMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdRouterASNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdForceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRTMIpStaticRouteDistanceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIRrdControlTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIRrdControlSourceProtoGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlDestProtoGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlRouteExportFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlSourceProtoSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlDestProtoSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlRouteExportFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlSourceProtoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlDestProtoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlRouteExportFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdControlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIRrdRoutingProtoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIRrdRoutingRegnIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdRoutingProtoTaskIdentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdRoutingProtoQueueIdentGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAllowOspfAreaRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAllowOspfExtRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAllowOspfAreaRoutesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAllowOspfExtRoutesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAllowOspfAreaRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdAllowOspfExtRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRrdRoutingProtoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIRtmCommonRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIRtmCommonRouteIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteProtoGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteAgeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteInfoGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteNextHopASGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteMetric1Get(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRoutePrivateStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRoutePreferenceGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteInfoSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteNextHopASSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteMetric1Set(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRoutePrivateStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRoutePreferenceSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteInfoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteNextHopASTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteMetric1Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRoutePrivateStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRoutePreferenceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmCommonRouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMIRtmRedEntryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmRedExitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumBgpRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumOspfRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumRipRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumStaticRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumISISRoutesGet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumBgpRoutesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumOspfRoutesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumRipRoutesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumStaticRoutesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumISISRoutesSet(tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumBgpRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumOspfRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumRipRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumStaticRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumISISRoutesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIRtmMaximumBgpRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIRtmMaximumOspfRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIRtmMaximumRipRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIRtmMaximumStaticRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMIRtmMaximumISISRoutesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSMIRTWR_H */
