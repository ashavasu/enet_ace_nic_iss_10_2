/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrtmlw.h,v 1.12 2016/02/08 10:40:06 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrdRouterId ARG_LIST((UINT4 *));

INT1
nmhGetFsRrdFilterByOspfTag ARG_LIST((INT4 *));

INT1
nmhGetFsRrdFilterOspfTag ARG_LIST((INT4 *));

INT1
nmhGetFsRrdFilterOspfTagMask ARG_LIST((INT4 *));

INT1
nmhGetFsRrdRouterASNumber ARG_LIST((INT4 *));

INT1
nmhGetFsRrdAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRtmThrottleLimit ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsEcmpAcrossProtocolAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsRtmMaximumBgpRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRtmMaximumOspfRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRtmMaximumRipRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRtmMaximumStaticRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRtmMaximumISISRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsRtmIpStaticRouteDistance ARG_LIST((INT4 *));

INT1
nmhGetFsRtmRouteLeakStatus ARG_LIST((INT4 *));
  
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrdRouterId ARG_LIST((UINT4 ));

INT1
nmhSetFsRrdFilterByOspfTag ARG_LIST((INT4 ));

INT1
nmhSetFsRrdFilterOspfTag ARG_LIST((INT4 ));

INT1
nmhSetFsRrdFilterOspfTagMask ARG_LIST((INT4 ));

INT1
nmhSetFsRrdRouterASNumber ARG_LIST((INT4 ));

INT1
nmhSetFsRrdAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsRtmThrottleLimit ARG_LIST((UINT4 ));

INT1
nmhSetFsEcmpAcrossProtocolAdminStatus ARG_LIST((INT4 ));
INT1
nmhSetFsRtmMaximumBgpRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRtmMaximumOspfRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRtmMaximumRipRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRtmMaximumStaticRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRtmMaximumISISRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsRtmIpStaticRouteDistance ARG_LIST((INT4 ));

INT1
nmhSetFsRtmRouteLeakStatus ARG_LIST((INT4 ));
  
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrdRouterId ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRrdFilterByOspfTag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrdFilterOspfTag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrdFilterOspfTagMask ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrdRouterASNumber ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRrdAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRtmThrottleLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsEcmpAcrossProtocolAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRtmMaximumBgpRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRtmMaximumOspfRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRtmMaximumRipRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRtmMaximumStaticRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRtmMaximumISISRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsRtmIpStaticRouteDistance ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRtmRouteLeakStatus ARG_LIST((UINT4 *  ,INT4 ));
  
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrdRouterId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrdFilterByOspfTag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrdFilterOspfTag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrdFilterOspfTagMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrdRouterASNumber ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRrdAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtmThrottleLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsEcmpAcrossProtocolAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtmMaximumBgpRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtmMaximumOspfRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtmMaximumRipRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtmMaximumStaticRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtmMaximumISISRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtmIpStaticRouteDistance ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRtmRouteLeakStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
  
/* Proto Validate Index Instance for FsRrdControlTable. */
INT1
nmhValidateIndexInstanceFsRrdControlTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrdControlTable  */

INT1
nmhGetFirstIndexFsRrdControlTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrdControlTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrdControlSourceProto ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRrdControlDestProto ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRrdControlRouteExportFlag ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRrdControlRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrdControlSourceProto ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRrdControlDestProto ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRrdControlRouteExportFlag ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRrdControlRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrdControlSourceProto ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRrdControlDestProto ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRrdControlRouteExportFlag ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRrdControlRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrdControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRrdRoutingProtoTable. */
INT1
nmhValidateIndexInstanceFsRrdRoutingProtoTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRrdRoutingProtoTable  */

INT1
nmhGetFirstIndexFsRrdRoutingProtoTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRrdRoutingProtoTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRrdRoutingRegnId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrdRoutingProtoTaskIdent ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRrdRoutingProtoQueueIdent ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsRrdAllowOspfAreaRoutes ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsRrdAllowOspfExtRoutes ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRrdAllowOspfAreaRoutes ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsRrdAllowOspfExtRoutes ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRrdAllowOspfAreaRoutes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsRrdAllowOspfExtRoutes ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRrdRoutingProtoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRtmCommonRouteTable. */
INT1
nmhValidateIndexInstanceFsRtmCommonRouteTable ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRtmCommonRouteTable  */

INT1
nmhGetFirstIndexFsRtmCommonRouteTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRtmCommonRouteTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRtmCommonRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRtmCommonRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRtmCommonRouteProto ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRtmCommonRouteAge ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRtmCommonRouteInfo ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsRtmCommonRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRtmCommonRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRtmCommonRoutePrivateStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRtmCommonRoutePreference ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsRtmCommonRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRtmCommonRouteIfIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRtmCommonRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRtmCommonRouteInfo ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsRtmCommonRouteNextHopAS ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRtmCommonRouteMetric1 ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRtmCommonRoutePrivateStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRtmCommonRoutePreference ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsRtmCommonRouteStatus ARG_LIST((UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRtmCommonRouteIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRtmCommonRouteType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRtmCommonRouteInfo ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsRtmCommonRouteNextHopAS ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRtmCommonRouteMetric1 ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRtmCommonRoutePrivateStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRtmCommonRoutePreference ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsRtmCommonRouteStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRtmCommonRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRtmRedEntryTime ARG_LIST((INT4 *));

INT1
nmhGetFsRtmRedExitTime ARG_LIST((INT4 *));

UINT4 RtmGetTotalMemoryBlocksAllocated(VOID);
