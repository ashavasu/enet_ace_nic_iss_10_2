/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmtdfs.h,v 1.47 2017/11/14 07:31:13 siva Exp $
 *
 * Description:This file contains type definitions used     
 *             in the RTM module. This file also contains   
 *             message structures used for the interaction 
 *             with routing protocols.
 *
 *******************************************************************/
#ifndef _RTMTDFS_H
#define _RTMTDFS_H

   /* RTM Control Node structure */
typedef struct RrdControlInfo
{
    tTMO_SLL_NODE       pNext;          /* Pointer to the next node in list */
    struct  RtmCxt    *pRtmCxt;       /* pointer to the vrf/context to which the RRD Control
                                         * information is associated */
    UINT4               u4DestNet;      /* Destination net */
    UINT4               u4SubnetRange;  /* Range of subnets */
    UINT4               u4RouteTag;     /* Route tag to be used for this route
                                         * in case of manual configuration.
                                         */
    UINT2               u2DestProtocolMask;
                                        /* Destination protocols to which this
                                         * route can be exported or not.
                                         */
    UINT1               u1SrcProtocolId;/* The Routing protocol ID which learned
                                         * this route.
                                         */
    UINT1               u1RtExportFlag; /* For host specific routes - to allow
                                         * single route in a large set of deny
                                         * entries.
                                         */
    UINT1               u1RowStatus;    /* Status of this entry. */
    UINT1               au1AlignmentByte[3];
}
tRrdControlInfo;

typedef struct RtmOspfTypeChg
{
    struct RtmCxt   *pRtmCxt; /* pointer to the RTM vrf/context */
    UINT2             u2ProtoId;
    UINT1             u1OspfRtType;
    UINT1             u1OspfStatus;
}
tRtmOspfTypeChg;

/* GR Timer */
typedef struct RtmGRTimer
{
    tTmrBlk              RtmGRTmr;  /* Timer used to indicate that the 
                                       maximum graceful restart interval 
                                       of a particular routing protocol 
                                       has elapsed */
    UINT4                u4GRTimer; /* Maximum restart interval 
                                       of the routing protocol */
}
tRtmGRTimer;

/* RTM Registration Table */
typedef struct RtmRegistrationInfo
{
    struct RtmCxt     *pRtmCxt;       /* pointer to the vrf/context */
    tRtmGRTimer         GRTmr;                /* GR Timer Node for a routing protocol */
    INT4                (*DeliverToApplication)(tRtmRespInfo *pRespInfo,
                                                tRtmMsgHdr *RtmHeader);
    UINT1               au1TaskName[RTM_MAX_TASK_NAME_LEN]; /* Task name of the
                                                             * registered
                                                             * routing protocol
                                                             */
    UINT1               au1QName[RTM_MAX_Q_NAME_LEN];  /* Queue name to which
                                                        * messages to be posted
                                                        */
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Event;                /* Event to be given to RP */
    UINT2               u2RoutingProtocolId;    /* Routing Protocol ID */
    UINT2               u2NextRegId;            /* Next Reg ID in the RTM RT */
    UINT2               u2ExportProtoMask;      /* Protocols from which
                                                 * routes are exported to
                                                 * this protocols
                                                 */
    UINT1               u1AllowOspfInternals;   /* Allow or Deny all OSPF
                                                 * inter-area and
                                                 * intra-area address-mask
                                                 * pairs
                                                 */
    UINT1               u1AllowOspfExternals;   /* Allow or Deny all 
                                                 * OSPF external type 1
                                                 * and type 2 routes 
                                                 */
    UINT1               u1BitMask;              /* Bit mask to indicate 
                                                 * that the RP requires 
                                                 * route to be redistributed
                                                 * or not. If set as 0x80,
                                                 * then the routes will be
                                                 * redistributed and a
                                                 * registration acknowledge
                                                 * message will be sent. Else
                                                 * route will not be
                                                 * redistributed to this
                                                 * protocol.
                                                 */

    UINT1               u1State;                /* Processing State of this
                                                 * entry. Says whether any 
                                                 * event is in progress or
                                                 * configuration is stable.
                                                 */
#define         RTM_MASK_STABLE                0x01
#define         RTM_MASK_ENABLE_INPROGRESS     0x02
#define         RTM_MASK_DISABLE_INPROGRESS    0x04
   UINT1               u1RestartState;
#define         RTM_IGP_CONVERGED            1
#define         RTM_IGP_RESTARTING            2
    UINT1               au1AlignmentByte[1];    /* Byte for alignment */
}
tRtmRegistrationInfo;

   /* RTM Configuration Table */
typedef struct RtmConfigInfo
{
    UINT4               u4OspfTagValue;
    UINT4               u4OspfTagMask;
    UINT2               u2AlignmentWord;
    UINT1               u1RrdFilterByOspfTag;
    UINT1               u1AlignmentByte;
}
tRtmConfigInfo;

   /* This structure is exchanged in Module data between
      SNMP and RTM. This structure is passed from low level routine to RTM if
      a new filter is added or deleted in RRD control table 
      or if there is a change in the allow or deny of OSPF internal 
      (intra-area or inter-area) and external (type 1 or type 2) routes. */

typedef struct RtmSnmpIfMsg
{
    tRrdControlInfo    *pRtmFilter;
    tRtmRegnId          RegnId; /* RegId and VRF Id of the context in
                                 * which RTM configuration is done */
    UINT1               u1Cmd;
    UINT1               u1OspfRtType;
    UINT1               u1PermitOrDeny;
    UINT1               u1AlignmentByte;
}
tRtmSnmpIfMsg;

typedef struct RtmInitRoute
{
    struct RtmCxt     *pRtmCxt; /* pointer to the RTM vrf/context */
    UINT2               u2DestProto;
    UINT2               u2Data;
    UINT2               u2Event;
/* Values for this Event */
#define  RTM_ENABLE_MASK        1
#define  RTM_DISABLE_MASK       2
    UINT2               u2Align;
}
tRtmInitRoute;
typedef struct RtmRtMsg 
{ 
    union{
    tNetIpv4RtInfo RtInfo; 
    tRtmRegnId  RtmRegnId;
    tRtmRegnMsg RtmRegnMsg;
    }unRtmMsg;
    UINT1 u1MsgType; 
    UINT1 u1PadBits[3]; 
} tRtmRtMsg; 


   /* RTM Redistribution Process Node */
typedef struct RtmRedisNode
{
    tTMO_SLL_NODE      NextNode;
    tRtmRegnId         RegId;
    UINT4              u4EnaInitDest;
    UINT4              u4DisInitDest;
    UINT4              u4EnableProtoMask;
    UINT4              u4EnablePendProtoMask;
    UINT4              u4DisableProtoMask;
    UINT4              u4EnaInitMask;
    UINT4              u4DisInitMask;
    UINT1              u1Event;
#define             RTM_ENABLE_PROTO_MASK          0x01
#define             RTM_DISABLE_PROTO_MASK         0x02
    UINT1               u1Pad[3];
}
tRtmRedisNode;

/* RTM Context Specific Information */
typedef struct RtmCxt
{
    tRtmRegistrationInfo    aRtmRegnTable[MAX_ROUTING_PROTOCOLS];
                                      /* RTM registration table */
    tRtmConfigInfo          RtmConfigInfo;
                                      /* RTM configuration Parameters */
    /* Dont Alter the position of the Ctrl List. It should always remain in
     * the last position. */
    tTMO_SLL                RtmCtrlList;
                                      /* Singly linked list to hold the
                                       * RRD Control Table. */
    VOID                   *pIpRtTblRoot;
                                      /* Pointer to the Root node of
                                       * IP Routing Table. */



    tTmrBlk                RtmDLFTmr;     /* Timer to enable DLF
                                               setting */

    tTmrBlk                RtmECMPPRTTmr; /* Timer to check periodically, the
                                               reachability of ECMP routes*/

    tTMO_SLL                RtmInvdRtList; /* Link list containing static
                                              Routes having no valid IfIndex
                                              to reach destination
                                            1.Directly connected route
                                             having interface down
                                            2.Route having nexthops that dont
                                              have local interface to reach */


    UINT4                u4ContextId;  /* VRF Id of the RTM context */
    UINT4                u4IpFwdTblRouteNum;
                                      /* Number of rotues in the IP
                                       * Routing Table. */
    UINT4                u4RouterId; /* Router Id for this RTM context */
    UINT4                u4TempRouterId; /* Router Id for this RTM context */
    UINT4                u4StaticRoutes;
                                      /* Total Number of statis Routes in 
                                       * this RTM Context. */
    UINT4                u4ConnectedRoutes;
                                      /* Total Number of connected Routes in 
                                       * this RTM Context. */
    UINT4                u4RipRoutes;
                                      /* Total Number of rip Routes in this
                                       * RTM Context. */
    UINT4                u4BgpRoutes;
                                      /* Total Number of bgp Routes in this
                                       * RTM Context. */
    UINT4                u4OspfRoutes;
                                      /* Total Number of ospf Routes in this
                                       * RTM Context. */
    UINT4                u4IsisRoutes;
                                      /* Total Number of isis Routes in this
                                       * RTM Context. */
    UINT4                u4EcmpRoutes;
                                      /* Total Number of ecmp Routes in this
                                       * RTM Context. */
    UINT4                u4FailedRoutes;
                                      /* Total Number of Failed Routes in this
                                       * RTM Context. */
    UINT2                u2AsNumber; /* AS Number */
    UINT2                u2TempAsNumber; /* AS Number */
    UINT2                u2RtmRtStartIndex;
                                      /* RTM Registration Table start index */
    UINT2                u2RtmRtLastIndex;
                                      /* RTM Registration Table last index */
    UINT2                u2NoStaticRts;
    UINT1                u1RrdAdminStatus; /* RRD Admin Status */
    UINT1                u1RrdForceStatus; /* RRD Force Option */
    UINT1                u1RtmIBgpRedistribute ; /* IBgp redisrtibution status */
    UINT1           u1RtrIdType;
    UINT1                u1IpDefaultDistance;  /*Static IPv4 Default Adiministrative distance */
    UINT1                au1Reserved[1];
    UINT1                au1Preference[MAX_ROUTING_PROTOCOLS];
                                      /* Protocol Preference for same Route */
}
tRtmCxt;

typedef struct _RtmFrtTable {
    tRBNodeEmbd  RbNode;  /* RbNode for the entry */
    UINT4        u4CxtId;   /* Context Id */
    UINT4        u4DestNet; /* Destination Network */
    UINT4        u4DestMask;   /* Destination Mask */
    UINT4        u4NextHop; /* Next Hop Ip address */
    UINT4        u4Flag;     /* Indicates reachablity of route,Best route & Hardwarestatus */
                       /* if last bit is 1, the route is reachable route
 *                         *   0 -indicates not a best reachable route */
    UINT4        u4RtIfIndx;
    UINT4        u4HwIntfId[2];/*Used for storing hardware Interface Id of Host*/
    INT4         i4MetricType;
    UINT2        u2RtProto;      /* The protocol id through which the route
                                    is learnt */
    UINT1        u1BitMask;
    UINT1        u1DelFlag; /* delete flag - to delete the entry or not */
    UINT1        u1RtCount;
    UINT1        au1pad[3];
}tRtmFrtInfo;
/* Global Variable typedef Declaration */
typedef struct RtmGlobalInfo
{
    tRtmCxt                *apRtmCxt[RTM_MAX_CONTEXT_LMT]; /* Pointers to RTM Context */
    tRtmCxt                *pRtmCxt;       /* Pointer to Current Rtm Context */
    tTMO_SLL                RtmRedisInitList;
                                      /* Singly linked list to hold the
                                       * Route Redistribution
                                       * Initialisation nodes. */
    tMemPoolId              RtmCxtPoolId;  /* MemPool for RTM Context */
    tMemPoolId              RtmRrdCrtlPoolId;  /* MemPool for RRD Control Table*/
    tMemPoolId              RtmRtMsgPoolId;  /* MemPool for Route update messages. */
    tMemPoolId              RtmRtPoolId;   /* MemPool for route entries used in RTM */
    tMemPoolId              RtmRedisNodePoolId; /* MemPool for redist node */
    tMemPoolId              RtmRmapCommunityPoolId;
    tTimerListId            RtmTmrListId;  /* RTM Timer List */
    tMemPoolId              RtmAppSpecInfoPoolId;
    tRBTree                 pPRTRBRoot; /* Root of the RBTree Maintined for
                                           Pending Routes(Best Routes having
                                           unresolved NextHop) */

    tRBTree                 pECMPRBRoot; /* Root of the RBTree maintained for
                                           ECMP routes */

    tRBTree                 pRNHTRBRoot; /* Root of the RBTree Maintined for
                                            Resolved Next Hop Table(RNHT) */
    tRBTree                 pRouteNHRBRoot; /* Root of the RBTree Maintined for
                                               Next Hop entries being used by routes */

    tMemPoolId              RtmPNextHopPoolId; /* MemPool for NextHop Entries
                                               in PRT and ECMPPRT*/

    tMemPoolId              RtmURRtPoolId;  /* MemPool for Nodes that hold the
                                            route with unresolved Next Hop */

    tMemPoolId              RtmRNextHopPoolId; /* MemPool for NextHop Entries in
                                               Resolved Next Hop Table */
    tTmrBlk                 RtmOneSecTmr;
    tOsixQId                RtmRmPktQId;        /* Queue for receiving the 
                                                  events sent from RM module */
    tMemPoolId              RtmRedDynMsgPoolId; /* MemPool for the dynamic 
                                                  messages received from RM */
    tMemPoolId              RtmRedRMPoolId;    /* Mempool for the RM events
                                                received from RM. These events
                                                are posted to RTM module 
                                                using RmPktQ */
    tMemPoolId              RtmFrtPoolId;   /* Mempool for the adding
                                                 npapi failed route in RB tree*/
    tRBTree                 RtmRedTable;    /* RBTree for holding informations
                                             learnt through RM messages. This
                                             RBTree is also used by active node
                                             to send dynamic update from a 
                                             single point */
    tRBTree                 RtmFrtTable;   /* RBTree for holding NPAPI route
                                              programming Failed Route Entry(FRT).*/
    tTmrBlk                RtmFRTTmr; /* Timer to retry periodically, the
                                             failed routes in hw*/
    tRtmFrtInfo             RtmFrtInfo;  /* RTM Timer List for failed routes */
     tRBTree                 RtmRedPRTNode;    /* RBTree for holding informations
                                                  about the drop routes added in
                                                   hardware */


    UINT4                   u4ThrotLimit; /* Throttle limit for RTM */
    UINT4                   u4IpFwdTblRts;
                                      /* Total Number of Routes in all
                                       * the RTM Context. */

   /* Total Number of static and dynamic Routes installed in all the RTM Context. */

    UINT4                   u4StaticRts;
    UINT4                   u4BgpRts;
    UINT4                   u4RipRts;
    UINT4                   u4OspfRts;
    UINT4                   u4IsisRts;
 /*Max number of protocol routes to be programmed in RTM*/
    UINT4                   u4MaxRTMBgpRoute;
    UINT4                   u4MaxRTMRipRoute;
    UINT4                   u4MaxRTMOspfRoute;
    UINT4                   u4MaxRTMStaticRoute;
    UINT4                   u4MaxRTMIsisRoute;
    UINT4                   u4MaxReties;

    UINT1                   u1OneSecTimerStart;
    UINT1                   u1OneSecTmrStartReq;
    UINT1                   u1EcmpAcrossProtocol;
    UINT1                   u1VrfRouteLeakStatus;
}
tRtmGlobalInfo;

typedef struct _RtmRedPRTNode {
    tRBNodeEmbd  RbNode;  /* RbNode for the entry */
    UINT4        u4CxtId;   /* Context Id */
    UINT4        u4DestNet; /* Destination Network */
    UINT4        u4DestMask;   /* Destination Mask */
    UINT4        u4NextHop; /* Next Hop Ip address */
    UINT4        u4Flag;     /* Indicates reachablity of route,Best route & Hardwarestatus */
                             /* if last bit is 1, the route is reachable route
                              *  0 -indicates not a best reachable route */
    UINT2        u2RtProto;      /* The protocol id through which the route
                                    is learnt */
    UINT1        u1DelFlag; /* delete flag - to delete the entry or not */
    UINT1        au1Pad[1];
}tRtmRedPRTNode;




/* Next Hop Entry in the pending Routing table (PRT) and ECMPPRT */
typedef struct PNextHopNode {
    tTMO_SLL            routeEntryList;  /* List of best routes having 
                                            unresolved next hop(u4NextHop) */
    tRtmCxt             *pRtmCxt;   /* RTM Context Pointer */
    UINT4               u4NextHop;  /* UnResolved next hop */ 
    UINT4        u4MaxReties; /* max ARP request retries */
}tPNhNode;

/* Pending route entry in PRT and ECMPPRT*/
typedef struct PRtEntry
{
  tTMO_SLL_NODE   nextRtEntry; 
  tRtInfo            *pPendRt; /* Poniter to the Route Entry Added to Trie */
  tRtmCxt            *pRtmCxt;   /* RTM Context Pointer */
}tPRtEntry;


/* Next Hop Entry in the Resolved next hop table */
typedef struct RNextHopNode {
    tRtmCxt             *pRtmCxt;   /* RTM Context Pointer */
    UINT4               u4NextHop;  /* A Resolved next hop */
    UINT4               u4RouteCount; /* No of best routes having u4NextHop 
                                         as next hop */
}tRNhNode;

typedef struct _RtmRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tRtmRmCtrlMsg;

typedef struct _RtmRedTable {
    tRBNodeEmbd  RbNode;  /* RbNode for the entry */
    UINT4        u4CxtId;   /* Context Id */
    UINT4        u4DestNet; /* Destination Network */
    UINT4        u4DestMask;   /* Destination Mask */
    UINT4        u4NextHop; /* Next Hop Ip address */
    UINT4        u4Flag;     /* Indicates reachablity of route,Best route & Hardwarestatus */
                       /* if last bit is 1, the route is reachable route
                        *   0 -indicates not a best reachable route */
    UINT4        u4HwIntfId[2];/*Used for storing hardware Interface Id of Host*/
    UINT2        u2RtProto;      /* The protocol id through which the route
                                    is learnt */
    UINT1        u1DelFlag; /* delete flag - to delete the entry or not */
    UINT1        au1Pad[1];
}tRtmRedTable;

typedef struct _RtmRmMsg
{
     tRtmRmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tRtmRmMsg;

typedef struct _RtmHARedMarker
{
    UINT4    u4CxtId;
    UINT4    u4DestNet;
    UINT4    u4DestMask;
    UINT4    u4NextHop;
    UINT4    u4HwIntfId[2];/*Used for storing hardware Interface Id of Host*/
    UINT2    u2RtProto;
    UINT1    u1Pad[2];
}tRtmHARedMarker;

typedef struct _RtmHARedFrtMarker
{
    UINT4    u4CxtId;
    UINT4    u4DestNet;
    UINT4    u4DestMask;
    UINT4    u4NextHop;
    INT4     i4MetricType;
    UINT2    u2RtProto;
    UINT1    u1BitMask;
    UINT1    u1RtCount;
}tRtmHARedFrtMarker;

typedef struct _sRtmRedGlobalInfo{
    tRtmHARedMarker   RtmHARedMarker;/* Holds the keys for the RtmTable
                                           * entry which is marked for batch
                                           * processing module.*/
    tRtmHARedFrtMarker   RtmHARedFrtMarker;/* Holds the keys for the FrtTable
                                           * entry which is marked for batch
                                           * processing module.*/ 
    INT4                i4RtmRedEntryTime;  /* Entry time of standby to active
                                                function */
    INT4                i4RtmRedExitTime;  /* Exit time of standby to active
                                                function */
    UINT1               u1FrtBulkUpdStatus;
    UINT1               u1BulkUpdStatus;/* bulk update status
                                         * RTM_HA_UPD_NOT_STARTED 0
                                         * RTM_HA_UPD_COMPLETED 1
                                         * RTM_HA_UPD_IN_PROGRESS 2,
                                         * RTM_HA_UPD_ABORTED 3 */
    UINT1   u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1   u1NumPeersUp;     /* Indicates number of standby nodes
                                 that are up. */
    UINT1   bBulkReqRcvd;     /* To check whether bulk request recieved
                                 from standby before RM_STANDBY_UP event. */
    UINT1   u1RedStatus;      /* Redundancy status - whether enabled or not */
    UINT1   au1Padding[2];    /* Reserved */
}tRtmRedGlobalInfo;

#define RTM_ROUTE_CXT_FLAG gu1RtmRouteCxtFlag
#define RTM_RM_MSG_POOL_ID gRtmGlobalInfo.RtmRedRMPoolId
#define RTM_DYN_MSG_POOL_ID gRtmGlobalInfo.RtmRedDynMsgPoolId
#define RTM_FRT_MSG_POOL_ID gRtmGlobalInfo.RtmFrtPoolId


#define RTM_CIDR_RT_TYPE_IN_CXT(pRtmCxt)\
                   (CIDR_ROUTE_TABLE | (((UINT4)pRtmCxt->u4ContextId) << 16))
#define RTM_CONTROL_INFO_ALLOC(pu1Block)\
            (pu1Block = (tRrdControlInfo *) (MemAllocMemBlk ((tMemPoolId)gRtmGlobalInfo.RtmRrdCrtlPoolId)))
#define RTM_CONTROL_INFO_FREE(pu1Block)\
             MemReleaseMemBlock(gRtmGlobalInfo.RtmRrdCrtlPoolId, (UINT1 *)(pu1Block))

#define RTM_REDIST_NODE_ALLOC(pu1Block)\
            (pu1Block = (tRtmRedisNode *) (MemAllocMemBlk ((tMemPoolId)gRtmGlobalInfo.RtmRedisNodePoolId)))
#define RTM_REDIST_NODE_FREE(pu1Block)\
             MemReleaseMemBlock(gRtmGlobalInfo.RtmRedisNodePoolId, (UINT1 *)(pu1Block))
#define RTM_CONTEXT_ALLOC(pu1Block)\
            (pu1Block = (tRtmCxt *) (MemAllocMemBlk ((tMemPoolId)gRtmGlobalInfo.RtmCxtPoolId)))
#define RTM_CONTEXT_FREE(pu1Block)\
             MemReleaseMemBlock(gRtmGlobalInfo.RtmCxtPoolId, (UINT1 *)(pu1Block))

#define MALLOC_RTM_COMM_ENTRY(pCommunity) \
            (pCommunity = (tRtRmapComm*) MemAllocMemBlk(gRtmGlobalInfo.RtmRmapCommunityPoolId))

#define  IP_RTM_COMM_FREE(pCommunity) \
             MemReleaseMemBlock(gRtmGlobalInfo.RtmRmapCommunityPoolId, (UINT1 *)(pCommunity))


#define MALLOC_IP_ROUTE_ENTRY(pRt) \
            (pRt = (tRtInfo *)MemAllocMemBlk(gRtmGlobalInfo.RtmRtPoolId))

#define  IP_RT_FREE(pRt) \
             MemReleaseMemBlock(gRtmGlobalInfo.RtmRtPoolId, (UINT1 *)(pRt))

#define RTM_ROUTE_MSG_ENTRY_ALLOC(pRt) \
             (pRt = (tRtmRtMsg *)MemAllocMemBlk(gRtmGlobalInfo.RtmRtMsgPoolId)) 

#define  RTM_ROUTE_MSG_FREE(pRt) \
              MemReleaseMemBlock(gRtmGlobalInfo.RtmRtMsgPoolId, (UINT1 *)(pRt)) 
  


#define  RTM_OFFSET(x,y)   FSAP_OFFSETOF(x,y)

#define RTM_APP_SPEC_INFO(pAppSpecInfo) \
            (pAppSpecInfo = (tAppSpecInfo *)MemAllocMemBlk(gRtmGlobalInfo.RtmAppSpecInfoPoolId))

#define  APP_INFO_FREE(pAppSpecInfo) \
             MemReleaseMemBlock(gRtmGlobalInfo.RtmAppSpecInfoPoolId, (UINT1 *)(pAppSpecInfo))


/* RTM Macro used to delete the node while scanning. */
#define RTM_SLL_DYN_Scan(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First ((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))))

typedef struct RTInfo {
tRtInfo             apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
}tAppSpecInfo;
#endif
