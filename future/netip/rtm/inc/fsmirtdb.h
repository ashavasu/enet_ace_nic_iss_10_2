/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmirtdb.h,v 1.10 2016/02/08 10:40:06 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMIRTDB_H
#define _FSMIRTDB_H

UINT1 FsMIRtmTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIRrdControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIRrdRoutingProtoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIRtmCommonRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsmirt [] ={1,3,6,1,4,1,29601,2,31};
tSNMP_OID_TYPE fsmirtOID = {9, fsmirt};


UINT4 FsMIRtmThrottleLimit [ ] ={1,3,6,1,4,1,29601,2,31,1,1};
UINT4 FsMIEcmpAcrossProtocolAdminStatus [ ] ={1,3,6,1,4,1,29601,2,31,1,2};
UINT4 FsMIRtmRouteLeakStatus [ ] ={1,3,6,1,4,1,29601,2,31,1,3};
UINT4 FsMIRtmContextId [ ] ={1,3,6,1,4,1,29601,2,31,2,1,1};
UINT4 FsMIRrdRouterId [ ] ={1,3,6,1,4,1,29601,2,31,2,1,2};
UINT4 FsMIRrdFilterByOspfTag [ ] ={1,3,6,1,4,1,29601,2,31,2,1,3};
UINT4 FsMIRrdFilterOspfTag [ ] ={1,3,6,1,4,1,29601,2,31,2,1,4};
UINT4 FsMIRrdFilterOspfTagMask [ ] ={1,3,6,1,4,1,29601,2,31,2,1,5};
UINT4 FsMIRrdRouterASNumber [ ] ={1,3,6,1,4,1,29601,2,31,2,1,6};
UINT4 FsMIRrdAdminStatus [ ] ={1,3,6,1,4,1,29601,2,31,2,1,7};
UINT4 FsMIRrdForce [ ] ={1,3,6,1,4,1,29601,2,31,2,1,8};
UINT4 FsMIRTMIpStaticRouteDistance [ ] ={1,3,6,1,4,1,29601,2,31,2,1,9};
UINT4 FsMIRrdControlDestIpAddress [ ] ={1,3,6,1,4,1,29601,2,31,3,1,1};
UINT4 FsMIRrdControlNetMask [ ] ={1,3,6,1,4,1,29601,2,31,3,1,2};
UINT4 FsMIRrdControlSourceProto [ ] ={1,3,6,1,4,1,29601,2,31,3,1,3};
UINT4 FsMIRrdControlDestProto [ ] ={1,3,6,1,4,1,29601,2,31,3,1,4};
UINT4 FsMIRrdControlRouteExportFlag [ ] ={1,3,6,1,4,1,29601,2,31,3,1,5};
UINT4 FsMIRrdControlRowStatus [ ] ={1,3,6,1,4,1,29601,2,31,3,1,6};
UINT4 FsMIRrdRoutingProtoId [ ] ={1,3,6,1,4,1,29601,2,31,4,1,1};
UINT4 FsMIRrdRoutingRegnId [ ] ={1,3,6,1,4,1,29601,2,31,4,1,2};
UINT4 FsMIRrdRoutingProtoTaskIdent [ ] ={1,3,6,1,4,1,29601,2,31,4,1,3};
UINT4 FsMIRrdRoutingProtoQueueIdent [ ] ={1,3,6,1,4,1,29601,2,31,4,1,4};
UINT4 FsMIRrdAllowOspfAreaRoutes [ ] ={1,3,6,1,4,1,29601,2,31,4,1,5};
UINT4 FsMIRrdAllowOspfExtRoutes [ ] ={1,3,6,1,4,1,29601,2,31,4,1,6};
UINT4 FsMIRtmCommonRouteDest [ ] ={1,3,6,1,4,1,29601,2,31,5,1,1};
UINT4 FsMIRtmCommonRouteMask [ ] ={1,3,6,1,4,1,29601,2,31,5,1,2};
UINT4 FsMIRtmCommonRouteTos [ ] ={1,3,6,1,4,1,29601,2,31,5,1,3};
UINT4 FsMIRtmCommonRouteNextHop [ ] ={1,3,6,1,4,1,29601,2,31,5,1,4};
UINT4 FsMIRtmCommonRouteIfIndex [ ] ={1,3,6,1,4,1,29601,2,31,5,1,5};
UINT4 FsMIRtmCommonRouteType [ ] ={1,3,6,1,4,1,29601,2,31,5,1,6};
UINT4 FsMIRtmCommonRouteProto [ ] ={1,3,6,1,4,1,29601,2,31,5,1,7};
UINT4 FsMIRtmCommonRouteAge [ ] ={1,3,6,1,4,1,29601,2,31,5,1,8};
UINT4 FsMIRtmCommonRouteInfo [ ] ={1,3,6,1,4,1,29601,2,31,5,1,9};
UINT4 FsMIRtmCommonRouteNextHopAS [ ] ={1,3,6,1,4,1,29601,2,31,5,1,10};
UINT4 FsMIRtmCommonRouteMetric1 [ ] ={1,3,6,1,4,1,29601,2,31,5,1,11};
UINT4 FsMIRtmCommonRoutePrivateStatus [ ] ={1,3,6,1,4,1,29601,2,31,5,1,12};
UINT4 FsMIRtmCommonRouteStatus [ ] ={1,3,6,1,4,1,29601,2,31,5,1,13};
UINT4 FsMIRtmCommonRoutePreference [ ] ={1,3,6,1,4,1,29601,2,31,5,1,14};
UINT4 FsMIRtmRedEntryTime [ ] ={1,3,6,1,4,1,29601,2,31,6,1};
UINT4 FsMIRtmRedExitTime [ ] ={1,3,6,1,4,1,29601,2,31,6,2};
UINT4 FsMIRtmMaximumBgpRoutes [ ] ={1,3,6,1,4,1,29601,2,31,7};
UINT4 FsMIRtmMaximumOspfRoutes [ ] ={1,3,6,1,4,1,29601,2,31,8};
UINT4 FsMIRtmMaximumRipRoutes [ ] ={1,3,6,1,4,1,29601,2,31,9};
UINT4 FsMIRtmMaximumStaticRoutes [ ] ={1,3,6,1,4,1,29601,2,31,10};
UINT4 FsMIRtmMaximumISISRoutes [ ] ={1,3,6,1,4,1,29601,2,31,11};




tMbDbEntry fsmirtMibEntry[]= {

{{11,FsMIRtmThrottleLimit}, NULL, FsMIRtmThrottleLimitGet, FsMIRtmThrottleLimitSet, FsMIRtmThrottleLimitTest, FsMIRtmThrottleLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},

{{11,FsMIEcmpAcrossProtocolAdminStatus}, NULL, FsMIEcmpAcrossProtocolAdminStatusGet, FsMIEcmpAcrossProtocolAdminStatusSet, FsMIEcmpAcrossProtocolAdminStatusTest, FsMIEcmpAcrossProtocolAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMIRtmRouteLeakStatus}, NULL, FsMIRtmRouteLeakStatusGet, FsMIRtmRouteLeakStatusSet, FsMIRtmRouteLeakStatusTest, FsMIRtmRouteLeakStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
  
{{12,FsMIRtmContextId}, GetNextIndexFsMIRtmTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRtmTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRrdRouterId}, GetNextIndexFsMIRtmTable, FsMIRrdRouterIdGet, FsMIRrdRouterIdSet, FsMIRrdRouterIdTest, FsMIRtmTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIRtmTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRrdFilterByOspfTag}, GetNextIndexFsMIRtmTable, FsMIRrdFilterByOspfTagGet, FsMIRrdFilterByOspfTagSet, FsMIRrdFilterByOspfTagTest, FsMIRtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRtmTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRrdFilterOspfTag}, GetNextIndexFsMIRtmTable, FsMIRrdFilterOspfTagGet, FsMIRrdFilterOspfTagSet, FsMIRrdFilterOspfTagTest, FsMIRtmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtmTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRrdFilterOspfTagMask}, GetNextIndexFsMIRtmTable, FsMIRrdFilterOspfTagMaskGet, FsMIRrdFilterOspfTagMaskSet, FsMIRrdFilterOspfTagMaskTest, FsMIRtmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtmTableINDEX, 1, 0, 0, "-1"},

{{12,FsMIRrdRouterASNumber}, GetNextIndexFsMIRtmTable, FsMIRrdRouterASNumberGet, FsMIRrdRouterASNumberSet, FsMIRrdRouterASNumberTest, FsMIRtmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtmTableINDEX, 1, 0, 0, "0"},

{{12,FsMIRrdAdminStatus}, GetNextIndexFsMIRtmTable, FsMIRrdAdminStatusGet, FsMIRrdAdminStatusSet, FsMIRrdAdminStatusTest, FsMIRtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRtmTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRrdForce}, GetNextIndexFsMIRtmTable, FsMIRrdForceGet, FsMIRrdForceSet, FsMIRrdForceTest, FsMIRtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRtmTableINDEX, 1, 0, 0, "2"},

{{12,FsMIRTMIpStaticRouteDistance}, GetNextIndexFsMIRtmTable, FsMIRTMIpStaticRouteDistanceGet, FsMIRTMIpStaticRouteDistanceSet, FsMIRTMIpStaticRouteDistanceTest, FsMIRtmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtmTableINDEX, 1, 0, 0, NULL},

{{12,FsMIRrdControlDestIpAddress}, GetNextIndexFsMIRrdControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRrdControlTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRrdControlNetMask}, GetNextIndexFsMIRrdControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRrdControlTableINDEX, 3, 0, 0, NULL},

{{12,FsMIRrdControlSourceProto}, GetNextIndexFsMIRrdControlTable, FsMIRrdControlSourceProtoGet, FsMIRrdControlSourceProtoSet, FsMIRrdControlSourceProtoTest, FsMIRrdControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrdControlTableINDEX, 3, 0, 0, "1"},

{{12,FsMIRrdControlDestProto}, GetNextIndexFsMIRrdControlTable, FsMIRrdControlDestProtoGet, FsMIRrdControlDestProtoSet, FsMIRrdControlDestProtoTest, FsMIRrdControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRrdControlTableINDEX, 3, 0, 0, "0"},

{{12,FsMIRrdControlRouteExportFlag}, GetNextIndexFsMIRrdControlTable, FsMIRrdControlRouteExportFlagGet, FsMIRrdControlRouteExportFlagSet, FsMIRrdControlRouteExportFlagTest, FsMIRrdControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrdControlTableINDEX, 3, 0, 0, "1"},

{{12,FsMIRrdControlRowStatus}, GetNextIndexFsMIRrdControlTable, FsMIRrdControlRowStatusGet, FsMIRrdControlRowStatusSet, FsMIRrdControlRowStatusTest, FsMIRrdControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrdControlTableINDEX, 3, 0, 1, NULL},

{{12,FsMIRrdRoutingProtoId}, GetNextIndexFsMIRrdRoutingProtoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIRrdRoutingProtoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRrdRoutingRegnId}, GetNextIndexFsMIRrdRoutingProtoTable, FsMIRrdRoutingRegnIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRrdRoutingProtoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRrdRoutingProtoTaskIdent}, GetNextIndexFsMIRrdRoutingProtoTable, FsMIRrdRoutingProtoTaskIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIRrdRoutingProtoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRrdRoutingProtoQueueIdent}, GetNextIndexFsMIRrdRoutingProtoTable, FsMIRrdRoutingProtoQueueIdentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIRrdRoutingProtoTableINDEX, 2, 0, 0, NULL},

{{12,FsMIRrdAllowOspfAreaRoutes}, GetNextIndexFsMIRrdRoutingProtoTable, FsMIRrdAllowOspfAreaRoutesGet, FsMIRrdAllowOspfAreaRoutesSet, FsMIRrdAllowOspfAreaRoutesTest, FsMIRrdRoutingProtoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrdRoutingProtoTableINDEX, 2, 0, 0, "1"},

{{12,FsMIRrdAllowOspfExtRoutes}, GetNextIndexFsMIRrdRoutingProtoTable, FsMIRrdAllowOspfExtRoutesGet, FsMIRrdAllowOspfExtRoutesSet, FsMIRrdAllowOspfExtRoutesTest, FsMIRrdRoutingProtoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRrdRoutingProtoTableINDEX, 2, 0, 0, "1"},

{{12,FsMIRtmCommonRouteDest}, GetNextIndexFsMIRtmCommonRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRtmCommonRouteMask}, GetNextIndexFsMIRtmCommonRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRtmCommonRouteTos}, GetNextIndexFsMIRtmCommonRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRtmCommonRouteNextHop}, GetNextIndexFsMIRtmCommonRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRtmCommonRouteIfIndex}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRouteIfIndexGet, FsMIRtmCommonRouteIfIndexSet, FsMIRtmCommonRouteIfIndexTest, FsMIRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, "0"},

{{12,FsMIRtmCommonRouteType}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRouteTypeGet, FsMIRtmCommonRouteTypeSet, FsMIRtmCommonRouteTypeTest, FsMIRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRtmCommonRouteProto}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRouteProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRtmCommonRouteAge}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, "0"},

{{12,FsMIRtmCommonRouteInfo}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRouteInfoGet, FsMIRtmCommonRouteInfoSet, FsMIRtmCommonRouteInfoTest, FsMIRtmCommonRouteTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRtmCommonRouteNextHopAS}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRouteNextHopASGet, FsMIRtmCommonRouteNextHopASSet, FsMIRtmCommonRouteNextHopASTest, FsMIRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, "0"},

{{12,FsMIRtmCommonRouteMetric1}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRouteMetric1Get, FsMIRtmCommonRouteMetric1Set, FsMIRtmCommonRouteMetric1Test, FsMIRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, "-1"},

{{12,FsMIRtmCommonRoutePrivateStatus}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRoutePrivateStatusGet, FsMIRtmCommonRoutePrivateStatusSet, FsMIRtmCommonRoutePrivateStatusTest, FsMIRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIRtmCommonRouteStatus}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRouteStatusGet, FsMIRtmCommonRouteStatusSet, FsMIRtmCommonRouteStatusTest, FsMIRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIRtmCommonRouteTableINDEX, 5, 0, 1, NULL},

{{12,FsMIRtmCommonRoutePreference}, GetNextIndexFsMIRtmCommonRouteTable, FsMIRtmCommonRoutePreferenceGet, FsMIRtmCommonRoutePreferenceSet, FsMIRtmCommonRoutePreferenceTest, FsMIRtmCommonRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIRtmCommonRouteTableINDEX, 5, 0, 0, NULL},

{{11,FsMIRtmRedEntryTime}, NULL, FsMIRtmRedEntryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMIRtmRedExitTime}, NULL, FsMIRtmRedExitTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsMIRtmMaximumBgpRoutes}, NULL, FsMIRtmMaximumBgpRoutesGet, FsMIRtmMaximumBgpRoutesSet, FsMIRtmMaximumBgpRoutesTest, FsMIRtmMaximumBgpRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIRtmMaximumOspfRoutes}, NULL, FsMIRtmMaximumOspfRoutesGet, FsMIRtmMaximumOspfRoutesSet, FsMIRtmMaximumOspfRoutesTest, FsMIRtmMaximumOspfRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIRtmMaximumRipRoutes}, NULL, FsMIRtmMaximumRipRoutesGet, FsMIRtmMaximumRipRoutesSet, FsMIRtmMaximumRipRoutesTest, FsMIRtmMaximumRipRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIRtmMaximumStaticRoutes}, NULL, FsMIRtmMaximumStaticRoutesGet, FsMIRtmMaximumStaticRoutesSet, FsMIRtmMaximumStaticRoutesTest, FsMIRtmMaximumStaticRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsMIRtmMaximumISISRoutes}, NULL, FsMIRtmMaximumISISRoutesGet, FsMIRtmMaximumISISRoutesSet, FsMIRtmMaximumISISRoutesTest, FsMIRtmMaximumISISRoutesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsmirtEntry = { sizeof(fsmirtMibEntry)/sizeof(fsmirtMibEntry[0]), fsmirtMibEntry };

#endif /* _FSMIRTDB_H */

