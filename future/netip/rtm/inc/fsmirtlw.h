/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmirtlw.h,v 1.8 2016/02/08 10:40:06 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRtmThrottleLimit ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIEcmpAcrossProtocolAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIRtmRouteLeakStatus ARG_LIST((INT4 *));
 
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRtmThrottleLimit ARG_LIST((UINT4 ));

INT1
nmhSetFsMIEcmpAcrossProtocolAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMIRtmRouteLeakStatus ARG_LIST((INT4 ));
  
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRtmThrottleLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIEcmpAcrossProtocolAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIRtmRouteLeakStatus ARG_LIST((UINT4 *  ,INT4 ));
  
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRtmThrottleLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIEcmpAcrossProtocolAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtmRouteLeakStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

  
/* Proto Validate Index Instance for FsMIRtmTable. */
INT1
nmhValidateIndexInstanceFsMIRtmTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRtmTable  */

INT1
nmhGetFirstIndexFsMIRtmTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRtmTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRrdRouterId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIRrdFilterByOspfTag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrdFilterOspfTag ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrdFilterOspfTagMask ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrdRouterASNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrdAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRrdForce ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIRTMIpStaticRouteDistance ARG_LIST((INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRrdRouterId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIRrdFilterByOspfTag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrdFilterOspfTag ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrdFilterOspfTagMask ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrdRouterASNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrdAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRrdForce ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIRTMIpStaticRouteDistance ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRrdRouterId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIRrdFilterByOspfTag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdFilterOspfTag ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdFilterOspfTagMask ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdRouterASNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdForce ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIRTMIpStaticRouteDistance ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRtmTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRrdControlTable. */
INT1
nmhValidateIndexInstanceFsMIRrdControlTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRrdControlTable  */

INT1
nmhGetFirstIndexFsMIRrdControlTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRrdControlTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRrdControlSourceProto ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRrdControlDestProto ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRrdControlRouteExportFlag ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRrdControlRowStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRrdControlSourceProto ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRrdControlDestProto ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRrdControlRouteExportFlag ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRrdControlRowStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRrdControlSourceProto ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdControlDestProto ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdControlRouteExportFlag ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdControlRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRrdControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRrdRoutingProtoTable. */
INT1
nmhValidateIndexInstanceFsMIRrdRoutingProtoTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRrdRoutingProtoTable  */

INT1
nmhGetFirstIndexFsMIRrdRoutingProtoTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRrdRoutingProtoTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRrdRoutingRegnId ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIRrdRoutingProtoTaskIdent ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRrdRoutingProtoQueueIdent ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIRrdAllowOspfAreaRoutes ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIRrdAllowOspfExtRoutes ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRrdAllowOspfAreaRoutes ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIRrdAllowOspfExtRoutes ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRrdAllowOspfAreaRoutes ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIRrdAllowOspfExtRoutes ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRrdRoutingProtoTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIRtmCommonRouteTable. */
INT1
nmhValidateIndexInstanceFsMIRtmCommonRouteTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIRtmCommonRouteTable  */

INT1
nmhGetFirstIndexFsMIRtmCommonRouteTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIRtmCommonRouteTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRtmCommonRouteIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRtmCommonRouteType ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRtmCommonRouteProto ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRtmCommonRouteAge ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRtmCommonRouteInfo ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMIRtmCommonRouteNextHopAS ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRtmCommonRouteMetric1 ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRtmCommonRoutePrivateStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRtmCommonRoutePreference ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIRtmCommonRouteStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRtmCommonRouteIfIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRtmCommonRouteType ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRtmCommonRouteInfo ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsMIRtmCommonRouteNextHopAS ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRtmCommonRouteMetric1 ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRtmCommonRoutePrivateStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRtmCommonRoutePreference ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIRtmCommonRouteStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRtmCommonRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRtmCommonRouteType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRtmCommonRouteInfo ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsMIRtmCommonRouteNextHopAS ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRtmCommonRouteMetric1 ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRtmCommonRoutePrivateStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRtmCommonRoutePreference ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIRtmCommonRouteStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRtmCommonRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRtmRedEntryTime ARG_LIST((INT4 *));

INT1
nmhGetFsMIRtmRedExitTime ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIRtmMaximumBgpRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtmMaximumOspfRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtmMaximumRipRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtmMaximumStaticRoutes ARG_LIST((UINT4 *));

INT1
nmhGetFsMIRtmMaximumISISRoutes ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIRtmMaximumBgpRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtmMaximumOspfRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtmMaximumRipRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtmMaximumStaticRoutes ARG_LIST((UINT4 ));

INT1
nmhSetFsMIRtmMaximumISISRoutes ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIRtmMaximumBgpRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtmMaximumOspfRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtmMaximumRipRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtmMaximumStaticRoutes ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMIRtmMaximumISISRoutes ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIRtmMaximumBgpRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtmMaximumOspfRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtmMaximumRipRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtmMaximumStaticRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIRtmMaximumISISRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
