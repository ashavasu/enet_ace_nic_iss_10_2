/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmprots.h,v 1.45 2017/11/14 07:31:13 siva Exp $
 *
 * Description:This file contains prototype declarations in 
 *             RTM module.                                  
 *                               
 *******************************************************************/
#ifndef _RTM_PROTOS_H
#define _RTM_PROTOS_H

/* Contains prototypes of functions defined in RTM module */
/* rtmmain.c */
VOID                RtmGetSizingParams (VOID);
INT1                RtmEnqueuePktToRpsInCxt (tRtmCxt *pRtmCxt,
                                        tRtmRespInfo *pRespInfo,
                                        tRtmMsgHdr *pMsgHdr,
                                        tRtmRegnId *pRegnId);
UINT2               RtmGetRegnIndexFromRegnIdInCxt (tRtmCxt *pRtmCxt,
                                               tRtmRegnId * pRegnId);
INT4                RtmMemInit PROTO ((VOID));
INT4                RtmCreateCxt PROTO ((UINT4 u4contextId));
VOID                RtmDeleteCxt PROTO ((UINT4 u4contextId));
VOID                RtmConfigInitInCxt PROTO ((tRtmCxt *pRtmCxt));
INT4                RtmStartGRTmr (tRtmRegnId *pRegnId, tIpBuf *pBuf);
INT4                RtmStopGRTmrInCxt (tRtmCxt *pRtmCxt, tRtmRegnId * pRegnId);
VOID                RtmProcessTick (VOID);
INT4                RtmAddRedisEventInCxt (tRtmCxt *pRtmCxt,
                                      tRtmRegnId *pRegnId, 
                                      UINT4 u4ProtoMask,
                                       UINT4 u4Event);
INT4                RtmInitControlTableToDenyModeInCxt PROTO ((tRtmCxt *pRtmCxt));
VOID                RtmReInitRtInCxt (tRtmCxt * pRtmCxt, tRtmRegnId *pRegnId);
UINT2               RtmGetFreeIndexInRt (tRtmRegnId *pRegnId);
INT1                RtmInitTrieForCurrentAppInCxt (tRtmCxt *pRtmCxt,
                                                   tRtmRegnId *pRegnId);
INT4                RtmProcessRegistrationInCxt (tRtmCxt *pRtmCxt,
                                                 tRtmRegnMsg RegnMsg);
VOID                RtmHandleRegnMsg (tIpBuf * pBuf);
INT4                RtmUtilRegisterInCxt (tRtmCxt *pRtmCxt,
                          tRtmRegnId * pRegnId, UINT1 u1BitMask,
                          INT4 (*SendToApplication) (tRtmRespInfo * pRespInfo,
                                                     tRtmMsgHdr * RtmHeader));
INT4                RtmUtilDeregister (tRtmCxt *pRtmCxt, tRtmRegnId * pRegnId);
INT1                RtmUtilGetNextBestRtInCxt (tRtmCxt *pRtmCxt, tRtInfo InRtInfo,
                              tRtInfo * pOutRtInfo);
VOID                RtmHandleRrdEnableMsg (tRtmRrdMsg *pRtmRrdMsg);
VOID                RtmHandleRrdDisableMsg (tRtmRrdMsg *pRtmRrdMsg);
INT1                RtmSendAckToRpInCxt (tRtmCxt *pRtmCxt, tRtmRegnId *pRegnId);
INT1               RtmSendRouteAckToRpInCxt (tRtmCxt *pRtmCxt, tRtmRegnId *pRegnId,
                                              tNetIpv4RtInfo * pNetRtInfo, INT4 i4RetVal);
INT4                RtmProcessProtoMaskEventInCxt (tRtmCxt *pRtmCxt,
                                              tRtmRedisNode * pRtmRedisNode);
VOID                RtmRouteRedistributionInCxt (tRtmCxt *pRtmCxt,
                                            tRtInfo *pRtUpdate,
                                            UINT1 u1IsRtBest, UINT2 u2ChgBit,
                                            UINT2 u2DestProtoMask);
VOID                RtmDuplicateAndSendToRpsInCxt (tRtmCxt *pRtmCxt,
                                              tRtmMsgHdr *pMsgHdr,
                                              tNetIpv4RtInfo *pNetRtInfo,
                                              tRtmRegnId *pSrcRegnId,
                                              UINT2 *pu2DestProtoMask);
VOID                RtmHandleRouteUpdatesInCxt PROTO   ((tRtmCxt *pRtmCxt,
                                          tRtInfo *pRtUpdate,
                                          UINT1 u1IsRtBest,
                                          UINT2 u2ChgBit));
VOID                RtmTrieDeleteHandler (tDeleteOutParams *pDeleteParams);
VOID                RtmClearProtoRouteFromTrieInCxt (tRtmCxt *pRtmCxt,
                                                     tRtmRegnId *pRegnId);

VOID                RtmVcmCallBackFn (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId,
                                      UINT1 u1BitMap);
VOID                RtmVcmMsgHandler (tIpBuf * pBuf);
/* rtmfilt.c */
VOID                RtmCheckRouteForRedistributionInCxt (tRtmCxt *pRtmCxt, 
                                                    tRtmRegnId *pRegnId,
                                                    tRtInfo * pRtInfo,
                                                    UINT2 *pu2DestProtoMask);
VOID                RtmUpdateDestMaskInCxt (tRtmCxt *pRtmCxt, 
                                       tRtInfo * pRtInfo,
                                       tRtmRegnId *pSrcRegnId,
                                       UINT2 *pu2DestProtoMask);
VOID                RtmCheckForOspfIntOrExtInCxt (tRtmCxt *pRtmCxt, 
                                             tRtmRegnId *pRegnId, UINT4 u4Tag,
                                             UINT2 *pu2DestProtoMask);
VOID                RtmFilterCheckInCxt (tRtmCxt *pRtmCxt, 
                                    tRtmRegnId *pSrcRegnId, tRtInfo * pRtInfo,
                                    UINT2 *pu2DestProtoMask);
VOID                RtmHandleChgInDefMode (tRrdControlInfo * pNewFilter,
                                           UINT1 u1Status);
VOID                RtmHandleChgInFilterTable (tRrdControlInfo * pNewFilter,
                                               UINT1 u1Status);
INT4                RtmHandleOspfTypeRouteRedistribution (tRtInfo *pRtInfo,
                                                          VOID *pAppSpecData);
VOID                RtmHandleChgInOspfType (tRtmRegnId *pRegnId,
                                            UINT1 u1OspfType,
                                            UINT1 u1PermitOrDeny);
VOID                RtmDeleteAnEntryFromControlTable(tRrdControlInfo *pControlTableEntry);
VOID                RtmHandleNewEntryToControlTable(tRrdControlInfo *pNewFilter);
VOID                RtmGetRoutesFromCRTToRPs(tRrdControlInfo *pNewFilter);
INT4                RtmHandleFilteredRouteRedistribution (tRtInfo *pRtInfo,
                                                          VOID *pAppSpecData);
VOID                RtmFilterRedistributedRoutes (tRrdControlInfo * pNewFilter);

INT4                RtmHandleExportMaskEvent (tRtInfo *pRtInfo,
                                              VOID *pAppSpecData);
INT4                RtmCheckForMatch(UINT2 u2RedisProtoMask, INT4 i4MetricType);
INT4                RtmCheckForLevel(UINT2 u2RedisProtoMask, INT4 i4MetricType);

/* rtmtrie.c */
INT4    IpTblRowStatusAction (tNetIpv4RtInfo * pRt, INT4 *pi4SetStatus,
                              UINT1 *pu1UpdateNeeded);
VOID    RtmUtilProcessGRRouteCleanUp PROTO ((tRtmCxt *pRtmCxt, 
                                             tRtmRegnId *pRegnId, INT1 i1GRCompFlag));

INT4    RtmUtilIpv4LeakRoute (UINT1 u1CmdType, tNetIpv4RtInfo * pNetRtInfo);
INT4    IpForwardingTableAddRouteInCxt PROTO ((tRtmCxt * pRtmCxt,
                                               tRtInfo * pIpRoute));
INT4    IpForwardingTableModifyRouteInCxt PROTO ((tRtmCxt *pRtmCxt,
                                             tRtInfo * pIpRoute));
INT4    IpForwardingTableDeleteRouteInCxt PROTO ((tRtmCxt *pRtmCxt,
                                                  tRtInfo * pIpRoute));
INT1    IpGetBestRouteEntryInCxt     PROTO ((tRtmCxt *pRtmCxt, 
                                        tRtInfo InRtInfo,
                                        tRtInfo ** ppOutRtInfo));
INT1    IpGetNextBestRouteEntryInCxt PROTO ((tRtmCxt *pRtmCxt, 
                                        tRtInfo InRtInfo,
                                        tRtInfo ** ppOutRtInfo));

INT1    IpGetNextRouteEntryInCxt  PROTO ((tRtmCxt *pRtmCxt,
                                        tRtInfo InRtInfo,
                                            INT1 i1AppId,
                              tOutputParams * pOutParams));

VOID    IpScanRouteTableForBestRouteInCxt PROTO ((tRtmCxt *pRtmCxt, 
                                                  tInputParams *pInParams,
                                             INT4 (*pAppSpecFunc)
                                                          (tRtInfo *pRtInfo,
                                                           VOID *pAppSpecData),
                                             UINT4 u4MaxRouteCnt,
                                             tOutputParams   *pOutParams,
                                             VOID *pAppSpecData));
INT4
IpFwdTblGetBestRouteInCxt PROTO ((tRtmCxt *pRtmCxt, tOutputParams pOutParams,
                                  tRtInfo ** pRt, UINT1   u1QueryFlag));
VOID RtmInitPreferenceInCxt PROTO ((tRtmCxt *pRtmCxt));

INT4 RtmLocalIfInitInCxt PROTO ((tRtmCxt *pRtmCxt));
INT4 RtmStaticRtInitInCxt PROTO ((tRtmCxt *pRtmCxt));

        
VOID RTM_Process_Message_Arrival_Event (VOID);
VOID RTM_Process_Filter_Added_Event (VOID);

VOID RtmCopyRouteInfoInCxt PROTO ((UINT4 u4ContextId, tRtInfo * pRt,
                                   tNetIpv4RtInfo * pNetIpRtInfo));

INT4  RtmHandleRtWithInvalidIfIndexInCxt PROTO ((tRtmCxt *pRtmCxt, 
                                            UINT4 u4Dest, UINT4 u4Mask, 
                                            UINT4 u4Tos, UINT4 u4NextHop, 
                                            INT4 i4Val, UINT1 u1ObjType));
INT4 RtmUtilRetryNpPrgForFailedRoute PROTO ((tRtmFrtInfo * pRtmFrtInfo));
UINT1 RtmUtilCheckRtExistsWithInvalidIfIndex PROTO ((tRtmCxt *pRtmCxt,
                                                  UINT4 u4Dest, UINT4  u4Mask,
                                                  UINT4 u4Tos,UINT4  u4NextHop, 
                                                  tRtInfo *pRtFound));

VOID RtmHandleOperUpIndicationInCxt PROTO ((tRtmCxt *pRtmCxt, UINT2 u2Index));

VOID RtmHandleECMPBestRoutesInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo  *pBestRt,
                                          INT4 i4Metric, UINT1 u1Flag));

VOID RtmGetInstalledRtCnt PROTO ((tRtInfo *pRt, UINT1 *u1RtCount));

tRtInfo * RtmGetNotifiedRt PROTO ((tRtInfo *pRt));

VOID RtmHandleECMPRtInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo * pRt,
                 tRtInfo * pBestRt, UINT1 u1Operation, UINT1 u1Command));

VOID RtmHandleECMPRtAdditionInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo * pRt,
                                   tRtInfo * pNotifiedRt, UINT1 u1RtCount));

VOID RtmHandleECMPRtDeletionInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo * pRt,
                                          UINT1 u1RtCount, UINT1 u1IncRtCnt));

VOID RtmNotifyNewECMPRouteInCxt PROTO ((tRtmCxt *pRtmCxt,
                                        tRtInfo *pRt, INT4 i4Metric));
VOID RtmGetBestRouteCount PROTO ((tRtInfo * pRt, INT4 i4Metric ,UINT1 *u1RtCount));
VOID RtmHandleBestRouteInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo * pRt,
                                     UINT1 u1Operation));
VOID RtmNotifyRtToAppsInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo * pRt,
                                    tRtInfo * pRt1, UINT1 u1Operation));

INT4 RtmDeleteRouteFromFwdTblInCxt PROTO ((tRtmCxt *pRtmCxt, 
                                           tRtInfo* pRt));
INT4 RtmAddRouteInFwdTblInCxt PROTO ((tRtmCxt *pRtmCxt,
                                      tRtInfo * pRt));
INT4 RtmIsRouteExistInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo * pIpRoute,
                                  tRtInfo ** ppTmpRt));
VOID RtmGetStaticRouteCount PROTO ((UINT4 *pu4StaticRtCount));

PUBLIC UINT1 RtmIsReachableNextHopInCxt  PROTO ((tRtmCxt *pRtmCxt, UINT4  u4NextHopAddr));
VOID             RtmProcessECMPPRTTimerExpiryInCxt PROTO ((tRtmCxt *pRtmCxt));

VOID RtmDelRtFromPRTInCxt(tRtmCxt * pRtmCxt, UINT4 u4DestNet,UINT4 u4DestMask,UINT4 u4NextHop,UINT4 i44Metric1,UINT2 u2RtProto);
VOID RtmDelRtFromECMPPRTInCxt(tRtmCxt * pRtmCxt, UINT4 u4DestNet,UINT4 u4DestMask,UINT4 u4NextHop,UINT4 i4Metric1,UINT2 u2RtProto);


 /* API provided for ARP to Program Routes */
VOID RtmUpdateRouteCount PROTO ((tRtmCxt * pRtmCxt, tRtInfo * pRt, UINT4 u4Action));
PUBLIC INT4  RtmHandlePRTRoutesForArpUpdationInCxt PROTO ((tRtmCxt *pRtmCxt, 
                                                      UINT4  u4NextHopIpAddr,
                                                      UINT1  u1Operation,
                                                      INT1 i1Flag));
INT4       RtmHandleECMPPRTRtsForArpUpdationInCxt PROTO ((tRtmCxt *pRtmCxt, UINT4 u4NextHopAddr));
INT4       RtmAddRouteToDataPlaneInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo * pRt, UINT1 u1RtCount));
INT4       RtmDeleteRouteFromDataPlaneInCxt PROTO ((tRtmCxt * pRtmCxt, tRtInfo * pRt, UINT1 u1RtCount));
PUBLIC INT4 RtmDeleteRtFromFIBInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRt, UINT4 u4CfaIfIndex, 
                                                     UINT1 u1RtCount));
PUBLIC INT4 RtmAddRtToFIBInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRt, UINT4 u4CfaIfIndex, 
                                                UINT1 u1RtCount));
INT4      RtmHandleArpEntryDeletionInCxt PROTO  ((tRtmCxt *pRtmCxt, UINT4 u4NextHopIpAddr));
INT4      RtmRBCompareRNh    PROTO ((tRBElem *e1, tRBElem *e2));
INT4      RtmUpdateResolvedNHEntryInCxt PROTO ((tRtmCxt *pRtmCxt, UINT4 u4NextHop, UINT1 u1Operation));
INT4      RtmRBComparePNh   PROTO ((tRBElem *e1, tRBElem *e2));
INT4      RtmAddRtToPRTInCxt      PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pPRoute));
INT4      RtmDeleteRtFromPRTInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pPRoute));
INT4      RtmAddRtToECMPPRTInCxt  PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRoute));
INT4      RtmResolveNextHop  PROTO ((tPNhNode *pPNhNode, UINT4 u4IfIndex));
INT4      RtmDeleteRtFromECMPPRTInCxt   PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRoute));
VOID      RtmAddAllEcmpRtToEcmpPrtInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pEcmpRt));
VOID      RtmDelAllEcmpRtFromEcmpPrtInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pEcmpRt));

INT4    RtmUpdateLocalRouteInCxt PROTO ((UINT1 u1Status, UINT4 u4ContextId,
                                         UINT4 u4NetWork, UINT4 u4NetMask, 
                                         UINT4 u4IfIndex));

INT4   RtmChangeRtStatusInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo * pRtInfo,
                                      UINT2 u2IfIndex, INT1 i1Status));


INT4
RtmConfBestRoutesInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRt,  UINT1 u1RtInstall, UINT1 u1Cmd));

INT4
RtmNotifyRtToHigherLayerAppsInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pNewRt, tRtInfo *pOldRt, UINT1 u1RtInstall, UINT1 u1Cmd));

INT4
RtmAddRouteToDataPlaneWithoutNotifyInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRt, UINT1 u1RtCount));

INT4
RtmDeleteRouteFromDataPlaneWithoutNotifyInCxt PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRt, UINT1 u1RtCount));

INT4
RtmResolveRoute PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRt, UINT1 *pu1IsRtReachable));

UINT4
RtmGetECMPSupportGroups PROTO ((VOID));
VOID
RtmFailedRouteAction PROTO ((tRtmCxt *pRtmCxt, tRtInfo *pRt));

/* Fn PROTO TYPE Definition for rtmcxtutl.c */
PUBLIC INT4      UtilRtmVcmIsVcExist PROTO ((UINT4 u4RtmCxtId));
PUBLIC tRtmCxt * UtilRtmGetCxt PROTO ((UINT4 u4RtmCxtId));
PUBLIC INT4      UtilRtmGetFirstCxtId PROTO ((UINT4 *pu4RtmCxtId));
PUBLIC INT4      UtilRtmGetNextCxtId PROTO ((UINT4 u4RtmCxtId,
                                             UINT4 *pu4NextRtmCxtId));
PUBLIC INT4      UtilRtmGetVcmSystemMode PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4      UtilRtmGetVcmSystemModeExt PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4      UtilRtmIsValidCxtId PROTO ((UINT4 u4RtmCxtId));
PUBLIC INT4      UtilRtmGetCxtIdFromCxtName PROTO ((UINT1 *pu1Alias,
                                                    UINT4 *pu4VcNum));
PUBLIC INT4      UtilRtmGetVcmAliasName PROTO ((UINT4, UINT1*));
PUBLIC VOID      UtilRtmIncMsrForRtmTable PROTO ((UINT4 u4RtmCxtId,
                                  INT4 i4SetVal, UINT4 *pu4ObjectId,
                                  UINT4 u4OidLength, CHR1 c1Type));

PUBLIC VOID      UtilRtmIncMsrForRrdControlTable PROTO ((UINT4 u4RtmCxtId,
                                  UINT4 u4DestIp, UINT4 u4DestMask,
                                  INT4 i4SetVal, UINT4 *pu4ObjectId,
                                  UINT4 u4OidLength, UINT1 u1RowStatus));

PUBLIC VOID      UtilRtmIncMsrForRrdProtoTable PROTO ((UINT4 u4RtmCxtId,
                                  INT4 i4ProtoId,  INT4 i4SetVal,
                                  UINT4 *pu4ObjectId, UINT4 u4OidLength));

PUBLIC VOID     UtilRtmIncMsrForRtPrivateStatus PROTO ((UINT4 u4RtmCxtId,
                                UINT4 u4Dest, UINT4 u4RouteMask, INT4 i4Tos,
                                UINT4 u4NextHop, INT4 i4SetVal, UINT4 *pu4ObjectId,
                                UINT4 u4OidLength));

PUBLIC VOID     UtilRtmIncMsrForRtmScalar PROTO ((UINT4 u4SetVal, UINT4 *pu4ObjectId,
                                                  UINT4 u4OidLength, CHR1 c1Type));

PUBLIC INT4     RtmClearContextEntries PROTO ((UINT4 u4ContextId));

INT4
RtmRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
RtmGetShowCmdOutputAndCalcChkSum (UINT2 *);
INT4
RtmCliGetShowCmdOutputToFile (UINT1 *);

INT4
RtmCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
RtmCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);


    
/* Fn PROTO TYPE Definition for rtmtmrif.c */
INT4             RtmTmrSetTimer PROTO ((tTmrBlk * pTimer, UINT1 u1TimerId,
                                        INT4 i4NrTicks));
VOID             RtmProcessTimerExpiry PROTO ((VOID));
VOID             RtmProcessOneSecTimerExpiry PROTO ((VOID));
VOID             RtmProcessFRTTimerExpiry PROTO((VOID));
VOID             RtmUpdateRoutesForArpInitiationInCxt PROTO ((tRtmCxt * pRtmCxt,
                                      tRtInfo ** ppRtArray, UINT1 u1PathCnt,
                                      UINT1 *pau1NewFlag));
VOID             RtmProcessGRTimerExpiryInCxt PROTO ((tRtmCxt *pRtmCxt, 
                                                      tRtmRegnId RegnId));
    
UINT1 RtmCheckRtExistsWithInvalidIfIndexInCxt PROTO ((UINT4 u4contextId,
                                                  UINT4 u4Dest, UINT4  u4Mask,
                                                  UINT4 u4Tos,UINT4  u4NextHop, 
                                                  tRtInfo *pRtFound));

INT4 RtmGetIgpConvergenceStateInCxt (UINT4);
INT4 RTMGetForwardingStateInCxt (UINT4);

INT1 RtmUtilGetNextAllRtInCxt (tRtmCxt * pRtmCxt, tRtInfo InRtInfo, 
                               tRtInfo * pOutRtInfo);

INT4  RtmRBTreeRedEntryCmp PROTO ((tRBElem *, tRBElem *));
INT4  RtmRBTreeRedPRTEntryCmp PROTO ((tRBElem *, tRBElem *));
INT4  RtmRedInitGlobalInfo PROTO ((VOID));
INT4  RtmRedRmRegisterProtocols PROTO ((tRmRegParams *pRmReg));
INT4  RtmRedRmDeRegisterProtocols PROTO ((VOID));
INT4  RtmRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
VOID  RtmRedHandleRmEvents PROTO ((VOID));
VOID  RtmRedHandleGoActive PROTO ((VOID));
VOID  RtmRedHandleGoStandby PROTO ((tRtmRmMsg * pMsg));
VOID  RtmRedHandleStandbyToActive PROTO ((VOID));
VOID  RtmRedHandleActiveToStandby PROTO ((VOID));
VOID  RtmRedHandleIdleToActive PROTO ((VOID));
VOID  RtmRedHandleIdleToStandby PROTO ((VOID));
VOID  RtmRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
VOID  RtmRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
UINT4 RtmRedRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));
INT4  RtmRedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));
INT4  RtmRedSendMsgToRm PROTO ((tRmMsg * pMsg , UINT2 u2Length ));
VOID  RtmRedSendBulkReqMsg PROTO ((VOID));
INT4  RtmRedDeInitGlobalInfo PROTO ((VOID));
VOID  RtmRedSendBulkUpdTailMsg PROTO ((VOID));
VOID  RtmRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));
VOID  RtmRedAddDynamicInfo PROTO ((tRtInfo *pRtmInfo, UINT4 u4CxtId));
VOID  RtmRedSendDynamicInfo PROTO ((INT1 i1RouteType));
VOID  RtmRedPRTAddDynamicInfo PROTO ((tRtInfo *pRtmInfo, UINT4 u4CxtId));
VOID  RtmRedSendBulkUpdMsg PROTO ((VOID));
VOID  RtmRedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID  RtmRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID  RtmRedHwAudit PROTO ((VOID));
INT4  RtmRedSendBulkInfo (UINT1 *pu1BulkUpdPendFlg);
VOID  RtmRedistributeIBgpInternalEnable(UINT4 u4ContextId);
VOID  RtmRedistributeIBgpInternalDisable(UINT4 u4ContextId);
INT4  RtmRedUpdateRtParams (tRtInfo *pRtInfo, UINT4 u4Context);
VOID  RtmRedSyncFrtInfo (tRtInfo *pRtInfo, UINT4 u4CxtId, UINT1 u1RtCount,UINT1 u1Flag);
VOID  RtmRedProcessFrtBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID  RtmRedProcessSyncFrtInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);
INT4  RtmRedSendFrtBulkInfo (UINT1 *pu1BulkUpdPendFlg);
VOID  RtmRedSendFrtBulkUpdMsg PROTO ((VOID));


INT4
RtmAddNextHopForRoute (tRtmCxt * pRtmCxt, tRtInfo *pRtInfo);
INT4
RtmDeleteNextHopforRoute (tRtmCxt * pRtmCxt, tRtInfo *pRtInfo);
tPNhNode *
RtmGetReachableNextHopInCxt ( tRtmCxt * pRtmCxt, UINT4 u4NextHopIpAddr);
INT4
RtmRBCompareRNhForRoute (tRBElem * e1, tRBElem * e2);
#ifdef DHCP_LNXIP_WANTED
INT4 RtmNetIpv4LeakRoute (UINT1 u1NetIpv4RtCmd , tRtInfo *pRtInfo);
#endif

#endif /* _RTM_PROTOS_H */
