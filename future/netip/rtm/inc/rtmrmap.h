/*******************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmrmap.h,v 1.4 2009/11/23 07:41:55 prabuc Exp $
 *
 * Description:This file contains prototype declarations in 
 *             Route Map functionality.                                  
 *                               
 *******************************************************************/
#ifndef _RTM_RMAP_H
#define _RTM_RMAP_H
/* Macros */
#define RTM_NOT_REGISTERED_WITH_RMAP     1
#define RTM_REGISTERED_WITH_RMAP         2

/* Proto types */
INT4 RtmCheckIsRouteMapConfiguredInCxt PROTO((tRtmCxt *pRtmCxt,
                                              UINT2 u2Proto));
VOID RtmApplyRouteMapRuleAndRedistributeInCxt PROTO((
                    tRtmCxt *pRtmCxt, UINT2 u2SrcProtoId, 
                    tRtInfo * pRtInfo, UINT1 u1IsRtBest, UINT2 u2ChgBit,
                    UINT2 u2DstProtoMask));
INT4 RtmHandleRouteMapChgsInRts PROTO((tRtInfo * pRtInfo, VOID *pAppSpecData));
INT4 RtmProcessRouteMapChanges PROTO((UINT1 *pu1RMapName));
VOID RtmResetRouteMapBitMaskInCxt PROTO((tRtmCxt *pRtmCxt, 
                                    UINT2 *pu2DestProtoMask));
VOID RtmSetRouteMapBitMaskInCxt PROTO((tRtmCxt * pRtmCxt,
                                  UINT2 *pu2DestProtoMask));

INT4 RtmSendRouteMapUpdateMsg (UINT1 *pu1RMapName ,
                                UINT4 u4Status);

#endif /* _RTM_RMAP_H */

