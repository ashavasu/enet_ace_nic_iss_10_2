/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtminc.h,v 1.22 2015/10/17 11:49:37 siva Exp $
 *
 * Description:This file contains common includes in        
 *             the RTM module.                              
 *                               
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "iss.h"
#include "cli.h"
#include "trieapif.h"
#include "triecidr.h"
#include "ipvx.h"
#ifdef IP_WANTED
#include "cruport.h"
#include "tmoport.h"
#include "other.h"
#include "ipport.h"
#include "ippdudfs.h"
#endif
#ifdef LNXIP4_WANTED
#include "lnxipmod.h"
#include "lnxipreg.h"
#endif
#ifdef ICCH_WANTED
#include "icch.h"
#endif
#include "snmctdfs.h"
#include "rtmport.h"
#include "rtm.h"
#include "rtmx.h"
#include "rmap.h"
#include "rtmdefns.h"
#include "rtmtdfs.h"
#include "rtmprots.h"
#include "rtmrmap.h"
#include "rtmextn.h"
#include "snmccons.h"
#include "bgp.h"
#include "ospf.h"
#include "cfa.h"
#include "mpls.h"
#include "isis.h"

#include "arp.h"
#include "vcm.h"
#include "redblack.h"
#include "rtmsz.h"

#include "fssyslog.h"
#ifdef NPAPI_WANTED
#include "bridge.h"
#include "npapi.h"
#include "cfanp.h"
#include "ipnp.h"
#include "ipnpwr.h"
#include "cfanpwr.h"
#endif/* NPAPI_WANTED */
