/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmstb.c,v 1.4 2017/11/14 07:31:13 siva Exp $
 *
 * Description: This file contains RTM Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __RTMRED_C
#define __RTMRED_C

#include "rtminc.h"
#include "rtmred.h"

/************************************************************************/
/*  Function Name   : RtmRedInitGlobalInfo                            */
/*                                                                      */
/*  Description     : This function is invoked by the RTM module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
RtmRedInitGlobalInfo (VOID)
{
    gRtmRedGlobalInfo.u1RedStatus = IP_FALSE;
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : RtmRedRmRegisterProtocols                       */
/*                                                                      */
/*  Description     : This function is invoked by the RTM module to    */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
RtmRedRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedDeInitGlobalInfo                        */
/*                                                                      */
/* Description        : This function is invoked by the RTM module     */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register RTM with RM.            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
RtmRedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : RtmRmDeRegisterProtocols                        */
/*                                                                      */
/*  Description     : This function is invoked by the RTM module to    */
/*                    de-register itself with the RM module.            */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
RtmRedRmDeRegisterProtocols (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedRmCallBack                              */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to RTM        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RtmRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedHandleRmEvents                          */
/*                                                                      */
/* Description        : This function is invoked by the RTM module to  */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the RTM Q Msg        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleRmEvents ()
{
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleGoActive                          */
/*                                                                      */
/* Description        : This function is invoked by the RTM upon*/
/*                      receiving the GO_ACTIVE indication from RM      */
/*                      module. And this function responds to RM with an*/
/*                      acknowledgement.                                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleGoActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleGoStandby                         */
/*                                                                      */
/* Description        : This function is invoked by the RTM upon*/
/*                      receiving the GO_STANBY indication from RM      */
/*                      module. And this function responds to RM module */
/*                      with an acknowledgement.                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleGoStandby (tRtmRmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleIdleToActive                      */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleIdleToStandby                     */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleStandbyToActive                   */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleActiveToStandby                   */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessPeerMsgAtActive                  */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessPeerMsgAtStandby                 */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedRmReleaseMemoryForMsg                   */
/*                                                                      */
/* Description        : This function is invoked by the RTM module to  */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RtmRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedRmReleaseMemoryForMsg                   */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RtmRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedSendBulkReqMsg                          */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedSendBulkReqMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RtmRedSendBulkUpdMsg                      */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedSendBulkUpdMsg (VOID)
{
    return;
}

/***********************************************************************
 * Function Name      : RtmRedSendBulkInfo                         
 *                                                                      
 * Description        : This function sends the Bulk update messages to 
 *                      the peer standby RTM.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : RTM_SUCCESS/RTM_FAILURE                         
 ***********************************************************************/

PUBLIC INT4
RtmRedSendBulkInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UNUSED_PARAM (pu1BulkUpdPendFlg);
    return RTM_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the RTM offers an IP address */
/*                      to the RTM client and dynamically update the   */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pRtmBindingInfo - binding entry to be synced up*/
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
RtmRedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessBulkTailMsg                      */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************
 * Function Name      : RtmRedSendDynamicInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the RTM offers an IP address 
 *                      to the RTM client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : i1RouteType - Add/Delete Route
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
RtmRedSendDynamicInfo (INT1 i1RouteType)
{
    UNUSED_PARAM (i1RouteType);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessBulkInfo               */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RtmRedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessDynamicInfo               */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RtmRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedAddDynamicInfo                            */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pRtInfo - Rtm Route  message                    */
/*                      u1Operation - add or delete entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
RtmRedAddDynamicInfo (tRtInfo * pRtInfo, UINT4 u4CxtId)
{
    UNUSED_PARAM (pRtInfo);
    UNUSED_PARAM (u4CxtId);
    return;
}

/*-------------------------------------------------------------------+
 * Function           : RtmRBTreeRedEntryCmp
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : -1  if pRBElem <  pRBElemIn
 *                      1   if pRBElem >  pRBElemIn
 *                      0   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy entries in lexicographic
 * order of the indices of the Redundancy entry.
+-------------------------------------------------------------------*/

INT4
RtmRBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    UNUSED_PARAM (pRBElem);
    UNUSED_PARAM (pRBElemIn);
    return 0;
}

/*-------------------------------------------------------------------+
 * Function           : RtmRedUpdateRtParams
 *
 * Input(s)           : pRtInfo, u4Context
 *
 * Output(s)          : Updated pRtInfo
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE       
 *
 * Action :
 * This procedure searches for the route node in the temporary RTM RED  
 * table and if a node is found, the route's flag information is updated
 * and the node is removed from RED table.
+-------------------------------------------------------------------*/
INT4
RtmRedUpdateRtParams (tRtInfo * pRtInfo, UINT4 u4Context)
{
    UNUSED_PARAM (pRtInfo);
    UNUSED_PARAM (u4Context);
    return RTM_FAILURE;
}

/************************************************************************/
/* Function Name      : RtmRedSendFrtBulkUpdMsg                         */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
RtmRedSendFrtBulkUpdMsg (VOID)
{
    return;
}

/***********************************************************************
 * Function Name      : RtmRedSendFrtBulkInfo                         
 *                                                                      
 * Description        : This function sends the Bulk update messages to 
 *                      the peer standby RTM.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : RTM_SUCCESS/RTM_FAILURE                         
 ***********************************************************************/

INT4
RtmRedSendFrtBulkInfo (UINT1 *pu1BulkUpdPendFlg)
{
    UNUSED_PARAM (pu1BulkUpdPendFlg);
    return RTM_SUCCESS;
}

/************************************************************************
 * Function Name      : RtmRedSyncFrtInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the RTM offers an IP address 
 *                      to the RTM client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/
VOID
RtmRedSyncFrtInfo (tRtInfo * pRtInfo, UINT4 u4CxtId, UINT1 u1RtCount,
                   UINT1 u1Flag)
{
    UNUSED_PARAM (pRtInfo);
    UNUSED_PARAM (u4CxtId);
    UNUSED_PARAM (u1RtCount);
    UNUSED_PARAM (u1Flag);
    return;
}

/************************************************************************/
/* Function Name      : RtmFrtProcessBulkInfo                           */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RtmRedProcessFrtBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessSyncFrtInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RtmRedProcessSyncFrtInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    return;
}

#endif
