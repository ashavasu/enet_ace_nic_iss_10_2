/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* $Id: rtmtmrif.c,v 1.15 2017/11/14 07:31:13 siva Exp $
*******************************************************************/
#include "rtminc.h"
#include "rtmfrt.h"

/************************************************************************/
/* Function           : RtmTmrSetTimer                                  */
/*                                                                      */
/* Input(s)           : pTimer - Pointer to Timer block                 */
/*                    : u1TimerId - Timer Id                            */
/*                    : i4Duration - Time in seconds for which the      */
/*                    :              timer is started                   */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : TMR_SUCCESS/TMR_FAILURE                         */
/*                                                                      */
/************************************************************************/
PUBLIC INT4
RtmTmrSetTimer (tTmrBlk * pTimer, UINT1 u1TimerId, INT4 i4Duration)
{
    if (TmrStart (gRtmGlobalInfo.RtmTmrListId, pTimer, u1TimerId,
                  (UINT4) i4Duration, 0) != TMR_SUCCESS)
    {
        return (INT4) TMR_FAILURE;
    }
    return TMR_SUCCESS;
}

/************************************************************************/
/* Function           : RtmProcessECMPPRTTimerExpiryInCxt               */
/*                                                                      */
/* Input(s)           : pRtmCxt - RTM Context Pointer                   */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : This function processes the expiry of  ECMPPRT  */
/*                      Timer.This timer verifies the reachability of   */
/*                      ECMP routes available in ECMPPRT periodically.  */
/************************************************************************/
VOID
RtmProcessECMPPRTTimerExpiryInCxt (tRtmCxt * pRtmCxt)
{
    tPNhNode           *pPNHNode = NULL;
    tPNhNode            ExstNhNode;
    /* Tree to resolve for all the next hops in PRT */
    MEMSET (&ExstNhNode, 0, sizeof (tPNhNode));
    pPNHNode = (tPNhNode *) RBTreeGetFirst (gRtmGlobalInfo.pECMPRBRoot);
    while (pPNHNode != NULL)
    {

        ExstNhNode.u4NextHop = pPNHNode->u4NextHop;
        ExstNhNode.pRtmCxt = pPNHNode->pRtmCxt;
        RtmResolveNextHop (pPNHNode, IPIF_INVALID_INDEX);
        pPNHNode =
            RBTreeGetNext (gRtmGlobalInfo.pECMPRBRoot, &ExstNhNode, NULL);
    }
    RtmTmrSetTimer (&(pRtmCxt->RtmECMPPRTTmr), RTM_ECMPPRT_TIMER,
                    RTM_ECMPPRT_TIMER_INTERVAL);
    gu1ECMPPRTTimerRunning = RTM_ECMP_TIMER_RUNNING;

}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmProcessTimerExpiry
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function process the expired timers in RTM.
 *         
+-------------------------------------------------------------------*/
VOID
RtmProcessTimerExpiry (VOID)
{
    tTmrAppTimer       *pTmr = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tRtmRegnId          RegnId;
    UINT1               u1TimerId = 0;
    INT4                i4Offset = 0;
    tRtmRegistrationInfo *pRtmRegistrationInfo = NULL;
    tRtmGRTimer        *pRtmGRTmr = NULL;

    while ((pTmr =
            TmrGetNextExpiredTimer (gRtmGlobalInfo.RtmTmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pTmr)->u1TimerId;
        switch (u1TimerId)
        {
            case RTM_ONESEC_TIMER:
                RtmProcessOneSecTimerExpiry ();
                break;
            case RTM_GR_TIMER:
                i4Offset = RTM_OFFSET (tRtmGRTimer, RtmGRTmr);
                pRtmGRTmr =
                    (tRtmGRTimer *) (VOID *) ((UINT1 *) pTmr - i4Offset);
                i4Offset = 0;
                i4Offset = RTM_OFFSET (tRtmRegistrationInfo, GRTmr);
                pRtmRegistrationInfo =
                    (tRtmRegistrationInfo *) (VOID *)
                    ((UINT1 *) pRtmGRTmr - i4Offset);
                MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
                RegnId.u2ProtoId = pRtmRegistrationInfo->u2RoutingProtocolId;
                RegnId.u4ContextId = pRtmRegistrationInfo->pRtmCxt->u4ContextId;
                RtmProcessGRTimerExpiryInCxt (pRtmRegistrationInfo->pRtmCxt,
                                              RegnId);
                break;
#ifdef NPAPI_WANTED
            case RTM_DLF_TIMER:
                i4Offset = RTM_OFFSET (tRtmCxt, RtmDLFTmr);
                pRtmCxt = (tRtmCxt *) (VOID *) ((UINT1 *) pTmr - i4Offset);
                IpFsNpCfaVrfSetDlfStatus (pRtmCxt->u4ContextId, TRUE);
                break;
#endif
            case RTM_ECMPPRT_TIMER:
                i4Offset = RTM_OFFSET (tRtmCxt, RtmECMPPRTTmr);
                pRtmCxt = (tRtmCxt *) (VOID *) ((UINT1 *) pTmr - i4Offset);
                RtmProcessECMPPRTTimerExpiryInCxt (pRtmCxt);
                break;

            case RTM_FRT_TIMER:
                RtmProcessFRTTimerExpiry ();
                break;
            default:
                break;
        }
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmProcessOneSecTimerExpiry
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function processes the One Second Timer 
 *                      Expiry
 *         
+-------------------------------------------------------------------*/
VOID
RtmProcessOneSecTimerExpiry (VOID)
{
    gRtmGlobalInfo.u1OneSecTimerStart = FALSE;

    /* Process One Second Timer. Check if there is any work 
     * is pending or not. If any work is pending then process it. */
    RtmProcessTick ();

    /* Check whether the 1SEC Timer needed to be restarted or not. */
    if (gRtmGlobalInfo.u1OneSecTmrStartReq == TRUE)
    {
        /* Timer needs to be started. Check whether the timer is running
         * or not. */
        if (gRtmGlobalInfo.u1OneSecTimerStart == FALSE)
        {
            /* Start the one second Timer. */
            RtmTmrSetTimer (&(gRtmGlobalInfo.RtmOneSecTmr),
                            RTM_ONESEC_TIMER, 1);
            gRtmGlobalInfo.u1OneSecTimerStart = TRUE;
        }
        gRtmGlobalInfo.u1OneSecTmrStartReq = FALSE;
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmProcessFRTTimerExpiry
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function processes the One Second Timer 
 *                      Expiry
 *         
+-------------------------------------------------------------------*/
VOID
RtmProcessFRTTimerExpiry (VOID)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtmFrtInfo         RtmFrtInfo;
    tRtmFrtInfo        *pNextRtmFrtInfo = NULL;
    tRtInfo             RtInfo;
    UINT1               u1RetryRtCount = 0;

    MEMSET (&RtmFrtInfo, 0, sizeof (tRtmFrtInfo));
    MEMSET (&RtInfo, 0, sizeof (tRtInfo));
    RtmFrtInfo.u4CxtId = gRtmGlobalInfo.RtmFrtInfo.u4CxtId;
    RtmFrtInfo.u4DestNet = gRtmGlobalInfo.RtmFrtInfo.u4DestNet;
    RtmFrtInfo.u4DestMask = gRtmGlobalInfo.RtmFrtInfo.u4DestMask;
    RtmFrtInfo.u4NextHop = gRtmGlobalInfo.RtmFrtInfo.u4NextHop;
    pNextRtmFrtInfo = RtmGetNextFrtEntry (&RtmFrtInfo);
    while (pNextRtmFrtInfo != NULL)
    {
        MEMSET (&RtmFrtInfo, 0, sizeof (tRtmFrtInfo));
        /* Np reprogramming must be tried only in active. As Standby progrmaming
         * will be taken care via IPC call*/
        if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
        {
            if (RtmUtilRetryNpPrgForFailedRoute (pNextRtmFrtInfo) ==
                RTM_SUCCESS)
            {
                if (RtmFrtDeleteEntry (pNextRtmFrtInfo) == RTM_SUCCESS)
                {
                    pRtmCxt = UtilRtmGetCxt (pNextRtmFrtInfo->u4CxtId);
                    if (pRtmCxt != NULL)
                    {
                        pRtmCxt->u4FailedRoutes--;
                        RtInfo.u4DestNet = RtmFrtInfo.u4DestNet;
                        RtInfo.u4DestMask = RtmFrtInfo.u4DestMask;
                        RtInfo.u4NextHop = RtmFrtInfo.u4NextHop;
                        RtmRedSyncFrtInfo (&RtInfo, pRtmCxt->u4ContextId, 0,
                                           RTM_DELETE_ROUTE);
                    }
                }
            }
        }
        u1RetryRtCount++;
        if (u1RetryRtCount > MAX_RTM_RETRY_ROUTE_COUNT)
        {
            break;                /* Store the node info in global var and Restart the timer */
        }
        RtmFrtInfo.u4CxtId = pNextRtmFrtInfo->u4CxtId;
        RtmFrtInfo.u4DestNet = pNextRtmFrtInfo->u4DestNet;
        RtmFrtInfo.u4DestMask = pNextRtmFrtInfo->u4DestMask;
        RtmFrtInfo.u4NextHop = pNextRtmFrtInfo->u4NextHop;
        pNextRtmFrtInfo = RtmGetNextFrtEntry (&RtmFrtInfo);
    }
    if (pNextRtmFrtInfo != NULL)
    {
        gRtmGlobalInfo.RtmFrtInfo.u4CxtId = pNextRtmFrtInfo->u4CxtId;
        gRtmGlobalInfo.RtmFrtInfo.u4DestNet = pNextRtmFrtInfo->u4DestNet;
        gRtmGlobalInfo.RtmFrtInfo.u4DestMask = pNextRtmFrtInfo->u4DestMask;
        gRtmGlobalInfo.RtmFrtInfo.u4NextHop = pNextRtmFrtInfo->u4NextHop;
    }
    else
    {
        MEMSET (&gRtmGlobalInfo.RtmFrtInfo, 0, sizeof (tRtmFrtInfo));
    }
    RtmTmrSetTimer (&(gRtmGlobalInfo.RtmFRTTmr), RTM_FRT_TIMER,
                    RTM_FRT_TIMER_INTERVAL);
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmProcessGRTimerExpiryInCxt
 *
 * Input(s)           : pRtmCxt - RTM Context pointer
 *                    : RegnId - RTM Registraion ID             
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function processes the GR Timer 
 *                      Expiry
 *         
+-------------------------------------------------------------------*/
VOID
RtmProcessGRTimerExpiryInCxt (tRtmCxt * pRtmCxt, tRtmRegnId RegnId)
{
    /* Process GR Timer expiry. Perform route cleanup for the specified 
       protocol and deregister the routing protocol from the 
       registration list as the protocol doesn`t come up
       within the specified graceful time period. */
    RtmUtilProcessGRRouteCleanUp (pRtmCxt, &RegnId, IP_FAILURE);
    RtmUtilDeregister (pRtmCxt, &RegnId);
}
