/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmred.c,v 1.25 2017/11/14 07:31:13 siva Exp $
 *
 * Description: This file contains RTM Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __RTMRED_C
#define __RTMRED_C

#include "rtminc.h"
#include "rtmred.h"
#include "rtmfrt.h"
#ifndef LNXIP4_WANTED
#include "ipinc.h"
#endif
/************************************************************************/
/*  Function Name   : RtmRedInitGlobalInfo                              */
/*                                                                      */
/*  Description     : This function is invoked by the RTM module while  */
/*                    task initialisation. It initialises the red       */
/*                    global variables.                                 */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
RtmRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    gRtmRedGlobalInfo.u1RedStatus = IP_TRUE;
    gRtmGlobalInfo.RtmRedTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tRtmRedTable, RbNode), RtmRBTreeRedEntryCmp);

    if (gRtmGlobalInfo.RtmRedTable == NULL)
    {
        return RTM_FAILURE;
    }

    gRtmGlobalInfo.RtmRedPRTNode = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tRtmRedPRTNode, RbNode), RtmRBTreeRedPRTEntryCmp);

    if (gRtmGlobalInfo.RtmRedPRTNode == NULL)
    {
        RBTreeDelete (gRtmGlobalInfo.RtmRedTable);
        return RTM_FAILURE;
    }

    /* Create the RM Packet arrival Q */
    if (OsixQueCrt ((UINT1 *) RTM_RM_PKT_ARRIVAL_Q_NAME,
                    OSIX_MAX_Q_MSG_LEN,
                    FsRTMSizingParams[MAX_RTM_RM_QUE_SIZE_SIZING_ID].
                    u4PreAllocatedUnits,
                    &gRtmGlobalInfo.RtmRmPktQId) != OSIX_SUCCESS)
    {
        return RTM_FAILURE;
    }

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_RTM_APP_ID;
    RmRegParams.pFnRcvPkt = RtmRedRmCallBack;

    /* Registering RTM with RM */
    if (RtmRedRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    RTM_GET_NODE_STATUS () = RM_INIT;
    RTM_NUM_STANDBY_NODES () = 0;
    gRtmRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : RtmRedRmRegisterProtocols                         */
/*                                                                      */
/*  Description     : This function is invoked by the RTM module to     */
/*                    register itself with the RM module. This          */
/*                    registration is required to send/receive peer     */
/*                    synchronization messages and control events       */
/*                                                                      */
/*  Input(s)        : pRmReg -- Pointer to structure tRmRegParams       */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
RtmRedRmRegisterProtocols (tRmRegParams * pRmReg)
{

    /* Registering RTM protocol with RM */
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedDeInitGlobalInfo                          */
/*                                                                      */
/* Description        : This function is invoked by the RTM module      */
/*                      during module shutdown and this function        */
/*                      deinitializes the redundancy global variables   */
/*                      and de-register RTM with RM.                    */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

INT4
RtmRedDeInitGlobalInfo (VOID)
{
    if (gRtmGlobalInfo.RtmRmPktQId != IP_ZERO)
    {
        OsixQueDel (gRtmGlobalInfo.RtmRmPktQId);
        gRtmGlobalInfo.RtmRmPktQId = IP_ZERO;
    }
    if (gRtmGlobalInfo.RtmRedTable != NULL)
    {
        RBTreeDelete (gRtmGlobalInfo.RtmRedTable);
        gRtmGlobalInfo.RtmRedTable = NULL;
    }

    if (gRtmGlobalInfo.RtmRedPRTNode != NULL)
    {
        RBTreeDelete (gRtmGlobalInfo.RtmRedPRTNode);
        gRtmGlobalInfo.RtmRedPRTNode = NULL;
    }

    if (RtmRedRmDeRegisterProtocols () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/*  Function Name   : RtmRmDeRegisterProtocols                          */
/*                                                                      */
/*  Description     : This function is invoked by the RTM module to     */
/*                    de-register itself with the RM module.            */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/

PUBLIC INT4
RtmRedRmDeRegisterProtocols (VOID)
{
    /* Deregiserting RTM from RM */
    if (RmDeRegisterProtocols (RM_RTM_APP_ID) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to RTM        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RtmRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tRtmRmMsg          *pMsg = NULL;

    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT))
    {
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        return OSIX_FAILURE;
    }

    if ((pMsg =
         (tRtmRmMsg *) MemAllocMemBlk ((tMemPoolId) RTM_RM_MSG_POOL_ID)) ==
        NULL)
    {
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RtmRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tRtmRmMsg));

    pMsg->RmCtrlMsg.pData = pData;
    pMsg->RmCtrlMsg.u1Event = u1Event;
    pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

    /* Send the message associated with the event to RTM module in a queue */
    if (OsixQueSend (gRtmGlobalInfo.RtmRmPktQId,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock ((tMemPoolId) RTM_RM_MSG_POOL_ID, (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return OSIX_FAILURE;
    }

    /* Post a event to RTM to process RM events */
    OsixEvtSend (gRtmTaskId, RTM_RM_PKT_EVENT);

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedHandleRmEvents                            */
/*                                                                      */
/* Description        : This function is invoked by the RTM module to   */
/*                      process all the events and messages posted by   */
/*                      the RM module                                   */
/*                                                                      */
/* Input(s)           : pMsg -- Pointer to the RTM Q Msg                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleRmEvents ()
{
    tRtmRmMsg          *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    while (OsixQueRecv (gRtmGlobalInfo.RtmRmPktQId,
                        (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:

                RtmRedHandleGoActive ();
                break;
            case GO_STANDBY:
                RtmRedHandleGoStandby (pMsg);
                /* pMsg is passed as a argument to GoStandby function and
                 * it is released there. If the transformation is from active 
                 * to standby then there is no need for releasing the mempool
                 * as we are calling DeInit function. This function clears
                 * all the mempools, so it is returned here instead of break.*/
                return;
            case RM_STANDBY_UP:
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                RTM_NUM_STANDBY_NODES () = pData->u1NumStandby;
                RtmRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (RTM_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    RTM_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_NOT_STARTED;
                    gRtmRedGlobalInfo.u1FrtBulkUpdStatus =
                        RTM_HA_UPD_NOT_STARTED;
                    RtmRedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                RTM_NUM_STANDBY_NODES () = pData->u1NumStandby;
                RtmRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                ProtoAck.u4AppId = RM_RTM_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gRtmRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
                {
                    /* Process the message at active */
                    RtmRedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                  pMsg->RmCtrlMsg.u2DataLen);
                }
                else if (gRtmRedGlobalInfo.u1NodeStatus == RM_STANDBY)
                {
                    /* Process the message at standby */
                    RtmRedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                   pMsg->RmCtrlMsg.u2DataLen);
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                /* Config restore complete event is sent by the RM on completing
                 * the static configurations. If the node status is init, 
                 * and the rm state is standby, then the RTM status is changed 
                 * from idle to standby */
                if (gRtmRedGlobalInfo.u1NodeStatus == RM_INIT)
                {
                    if (RTM_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        RtmRedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                        RmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;
            case L2_INITIATE_BULK_UPDATES:
                /* L2 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                RtmRedSendBulkReqMsg ();
                break;
            case RM_DYNAMIC_SYNCH_AUDIT:
                RtmRedHandleDynSyncAudit ();
                break;
            default:
                break;

        }
        MemReleaseMemBlock (RTM_RM_MSG_POOL_ID, (UINT1 *) pMsg);
    }
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleGoActive                            */
/*                                                                      */
/* Description        : This function is invoked by the RTM upon        */
/*                      receiving the GO_ACTIVE indication from RM      */
/*                      module. And this function responds to RM with an*/
/*                      acknowledgement.                                */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_NOT_STARTED;
    gRtmRedGlobalInfo.u1FrtBulkUpdStatus = RTM_HA_UPD_NOT_STARTED;

    if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        return;
    }
    if (RTM_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        RtmRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (RTM_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the rtm entries 
         * and change the state to active */
        RtmRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (RTM_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        RTM_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_NOT_STARTED;
        gRtmRedGlobalInfo.u1FrtBulkUpdStatus = RTM_HA_UPD_NOT_STARTED;
        RtmRedSendBulkUpdMsg ();
    }
    RmApiHandleProtocolEvent (&ProtoEvt);

    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleGoStandby                           */
/*                                                                      */
/* Description        : This function is invoked by the RTM upon        */
/*                      receiving the GO_STANBY indication from RM      */
/*                      module. And this function responds to RM module */
/*                      with an acknowledgement.                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleGoStandby (tRtmRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
    MemReleaseMemBlock (RTM_RM_MSG_POOL_ID, (UINT1 *) pMsg);
    gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_NOT_STARTED;
    gRtmRedGlobalInfo.u1FrtBulkUpdStatus = RTM_HA_UPD_NOT_STARTED;

    if (RTM_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        return;
    }
    if (RTM_GET_NODE_STATUS () == RM_INIT)
    {
        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        RtmRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleIdleToActive                        */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleIdleToActive (VOID)
{
    /* Node status is set to active and the number of standby nodes 
     * are updated */
    RTM_GET_NODE_STATUS () = RM_ACTIVE;
    RTM_RM_GET_NUM_STANDBY_NODES_UP ();

    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleIdleToStandby                       */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleIdleToStandby (VOID)
{
    /* the node status is set to standby and no of standby nodes is set to 0 */
    RTM_GET_NODE_STATUS () = RM_STANDBY;
    RTM_NUM_STANDBY_NODES () = 0;

    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleStandbyToActive (VOID)
{
    UINT4               u4CurrentTime;

    /* Hardware audit is done and the hardware and software entries are 
     * checked for synchronization */
    OsixGetSysTime ((tOsixSysTime *) & u4CurrentTime);
    gRtmRedGlobalInfo.i4RtmRedEntryTime = (INT4) u4CurrentTime;

    /* Hardware audit is done and the hardware and software entries are
     * checked for synchronization */
    RtmRedHwAudit ();

    RTM_GET_NODE_STATUS () = RM_ACTIVE;
    RTM_RM_GET_NUM_STANDBY_NODES_UP ();

    OsixGetSysTime ((tOsixSysTime *) & u4CurrentTime);
    gRtmRedGlobalInfo.i4RtmRedExitTime = (INT4) u4CurrentTime;

    return;
}

/************************************************************************/
/* Function Name      : RtmRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedHandleActiveToStandby (VOID)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo            *pRtInfo = NULL;
    tRtInfo             InRtInfo;
    UINT4               u4CxtId = 0;
    UINT4               u4NxtCxtId = 0;

    INT4                i4RetVal = 0;

    /*update the statistics */
    RTM_GET_NODE_STATUS () = RM_STANDBY;
    RTM_RM_GET_NUM_STANDBY_NODES_UP ();

    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
    if (UtilRtmGetFirstCxtId (&u4CxtId) == RTM_FAILURE)
    {
        return;
    }
    pRtmCxt = UtilRtmGetCxt (u4CxtId);
    while (pRtmCxt != NULL)
    {
        i4RetVal = RtmUtilGetNextAllRtInCxt (pRtmCxt, InRtInfo, pRtInfo);
        while (i4RetVal == IP_SUCCESS)
        {
            if ((pRtInfo->u2RtProto == OSPF_ID) ||
                (pRtInfo->u2RtProto == STATIC_ID) ||
                (pRtInfo->u2RtProto == BGP_ID))
            {
                pRtInfo->u4Flag = 0;
            }
            MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
            InRtInfo.u4DestNet = pRtInfo->u4DestNet;
            InRtInfo.u4DestMask = pRtInfo->u4DestMask;
            InRtInfo.u4NextHop = pRtInfo->u4NextHop;
            InRtInfo.u4HwIntfId[0] = pRtInfo->u4HwIntfId[0];
            InRtInfo.u4HwIntfId[1] = pRtInfo->u4HwIntfId[1];
            i4RetVal = RtmUtilGetNextAllRtInCxt (pRtmCxt, InRtInfo, pRtInfo);
        }
        if (UtilRtmGetNextCxtId (u4CxtId, &u4NxtCxtId) == RTM_FAILURE)
        {
            break;
        }
        pRtmCxt = UtilRtmGetCxt (u4NxtCxtId);
        u4CxtId = u4NxtCxtId;
    }

    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessPeerMsgAtActive                    */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;

    RTM_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    RTM_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u4OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != RTM_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == RTM_RED_BULK_REQ_MESSAGE)
    {
        gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_NOT_STARTED;
        gRtmRedGlobalInfo.u1FrtBulkUpdStatus = RTM_HA_UPD_NOT_STARTED;
        if (!RTM_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gRtmRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;

            return;
        }
        RtmRedSendBulkUpdMsg ();
    }

    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2RemMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    u2MinLen = RTM_RED_TYPE_FIELD_SIZE + RTM_RED_LEN_FIELD_SIZE;

    RTM_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);

    RTM_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u2Length < u2MinLen)
    {
        /* If length is less that the minimum length, then ignore the message
         * and send error to RM */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);

        return;
    }
    u2OffSet = (UINT2) u4OffSet;
    u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

    if ((u2OffSet + u2RemMsgLen) != u2DataLen)
    {
        /* If there is a length mismatch between the message and the given 
         * length, ignore the message and send error to RM */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }
    switch (u1MsgType)
    {
        case RTM_RED_BULK_UPD_TAIL_MESSAGE:
            if (u2Length != RTM_RED_BULK_UPD_TAIL_MSG_SIZE)
            {
                ProtoEvt.u4Error = RM_PROCESS_FAIL;

                RmApiHandleProtocolEvent (&ProtoEvt);
                return;
            }
            RtmRedProcessBulkTailMsg (pMsg, &u2OffSet);
            break;
        case RTM_RED_BULK_INFO:
            RtmRedProcessBulkInfo (pMsg, &u2OffSet);
            break;
        case RTM_RED_FRT_BULK_INFO:
            RtmRedProcessFrtBulkInfo (pMsg, &u2OffSet);
            break;
        case RTM_RED_DYN_INFO:
            RtmRedProcessDynamicInfo (pMsg, &u2OffSet);
            break;
        case RTM_RED_SYNC_FRT_INFO:
            RtmRedProcessSyncFrtInfo (pMsg, &u2OffSet);
            break;
        default:
            break;
    }

    return;
}

/************************************************************************/
/* Function Name      : RtmRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the RTM module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RtmRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
RtmRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;

    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_RTM_APP_ID, RM_RTM_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedSendBulkReqMsg                            */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;
    UINT4               u4OffSet = 0;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    RTM Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RTM_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, RTM_RED_BULK_REQ_MESSAGE);
    RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, RTM_RED_BULK_REQ_MSG_SIZE);
    u2OffSet = (UINT2) u4OffSet;

    if (RtmRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    return;
}

/************************************************************************/
/* Function Name      : RtmRedSendFrtBulkUpdMsg                         */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedSendFrtBulkUpdMsg (VOID)
{
    UINT1               u1BulkUpdPendFlg = OSIX_FALSE;

    if (RTM_IS_STANDBY_UP () == OSIX_FALSE)
    {
        return;
    }

    if (gRtmRedGlobalInfo.u1FrtBulkUpdStatus == RTM_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        gRtmRedGlobalInfo.u1FrtBulkUpdStatus = RTM_HA_UPD_IN_PROGRESS;
        MEMSET (&gRtmRedGlobalInfo.RtmHARedFrtMarker, 0,
                sizeof (tRtmHARedFrtMarker));
    }
    if (gRtmRedGlobalInfo.u1FrtBulkUpdStatus == RTM_HA_UPD_IN_PROGRESS)
    {
        /* If bulk update flag is in progress, sent the bulk Info */
        if (RtmRedSendFrtBulkInfo (&u1BulkUpdPendFlg) != RTM_SUCCESS)
        {
            /* If there is any error in sending bulk update, set the 
             * bulk update flag to aborted */
            gRtmRedGlobalInfo.u1FrtBulkUpdStatus = RTM_HA_UPD_ABORTED;
        }
        else
        {
            if (u1BulkUpdPendFlg == OSIX_TRUE)
            {
                /* Send an event to the RTM main task indicating that there
                 * are pending entries in the RTM database to be synced */

                OsixEvtSend (gRtmTaskId, RTM_HA_FRT_PEND_BLKUPD_EVENT);
            }
            else
            {
                /* Send the tail msg to indicate the completion of Bulk
                 * update process.
                 */
                gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_COMPLETED;
                MEMSET (&gRtmRedGlobalInfo.RtmHARedFrtMarker, 0,
                        sizeof (tRtmHARedFrtMarker));
                RmSetBulkUpdatesStatus (RM_RTM_APP_ID);

                RtmRedSendBulkUpdTailMsg ();
            }
        }
    }

    return;

}

/************************************************************************/
/* Function Name      : RtmRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedSendBulkUpdMsg (VOID)
{
    UINT1               u1BulkUpdPendFlg = OSIX_FALSE;

    if (RTM_IS_STANDBY_UP () == OSIX_FALSE)
    {
        return;
    }

    if (gRtmRedGlobalInfo.u1BulkUpdStatus == RTM_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_IN_PROGRESS;
        MEMSET (&gRtmRedGlobalInfo.RtmHARedMarker, 0, sizeof (tRtmHARedMarker));
    }
    if (gRtmRedGlobalInfo.u1BulkUpdStatus == RTM_HA_UPD_IN_PROGRESS)
    {
        /* If bulk update flag is in progress, sent the bulk Info */
        if (RtmRedSendBulkInfo (&u1BulkUpdPendFlg) != RTM_SUCCESS)
        {
            /* If there is any error in sending bulk update, set the 
             * bulk update flag to aborted */
            gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_ABORTED;
        }
        else
        {
            RmSetBulkUpdatesStatus (RM_RTM_APP_ID);
            if (u1BulkUpdPendFlg == OSIX_TRUE)
            {
                /* Send an event to the RTM main task indicating that there
                 * are pending entries in the RTM database to be synced */

                OsixEvtSend (gRtmTaskId, RTM_HA_PEND_BLKUPD_EVENT);
            }
            else
            {
                /* Send the tail msg to indicate the completion of Bulk
                 * update process.
                 */
                gRtmRedGlobalInfo.u1BulkUpdStatus = RTM_HA_UPD_COMPLETED;
                MEMSET (&gRtmRedGlobalInfo.RtmHARedMarker, 0,
                        sizeof (tRtmHARedMarker));
                RtmRedSendFrtBulkUpdMsg ();
            }
        }
    }

    return;

}

/***********************************************************************
 * Function Name      : RtmRedSendBulkInfo                         
 *                                                                      
 * Description        : This function sends the Bulk update messages to 
 *                      the peer standby RTM.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : RTM_SUCCESS/RTM_FAILURE                         
 ***********************************************************************/

PUBLIC INT4
RtmRedSendBulkInfo (UINT1 *pu1BulkUpdPendFlg)
{
    tRmMsg             *pMsg = NULL;
    tRtmHARedMarker    *pRtmRedMarker = NULL;
    tRtInfo            *pPrevRtInfo = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo             RtInfo;
    tRtInfo             NextRtInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4CxtId = 0;
    UINT4               u4NxtCxtId = 0;
    INT4                i4RetVal = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = 0;
    UINT4               u4NoOffset = 0;
    INT1                i1RouteType = NETIPV4_ADD_ROUTE;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    RTM Bulk Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|    
     *    ---------------------------------------------------------------    
     *    | Msg. Type | Length | No of   | Dest    | Mask    | NextHop |    
     *    |           |        | Entries | Network | Length  | Address |    
     *    ---------------------------------------------------------------    
     *
     *    |<-2 B->|<-1 B->|<- 1 B ->|
     *    ---------------------------
     *    | Route | Route | Hw      |
     *    | Proto | Flag  | Status  |
     *    ---------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RTM_RED_MAX_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return RTM_FAILURE;
    }

    pRtmRedMarker = &(gRtmRedGlobalInfo.RtmHARedMarker);

    RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, RTM_RED_BULK_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u4LenOffSet = u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    u4NoOffset = u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgPacked);

    u4CxtId = pRtmRedMarker->u4CxtId;
    pRtmCxt = UtilRtmGetCxt (u4CxtId);
    while (pRtmCxt != NULL)
    {
        MEMSET (&RtInfo, 0, sizeof (tRtInfo));
        MEMSET (&NextRtInfo, 0, sizeof (tRtInfo));
        RtInfo.u4DestNet = pRtmRedMarker->u4DestNet;
        RtInfo.u4DestMask = pRtmRedMarker->u4DestMask;
        RtInfo.u4NextHop = pRtmRedMarker->u4NextHop;
        i4RetVal = RtmUtilGetNextAllRtInCxt (pRtmCxt, RtInfo, &NextRtInfo);
        while (i4RetVal == IP_SUCCESS)
        {
            if ((u4OffSet + RTM_RED_DYN_INFO_SIZE) > RTM_RED_MAX_MSG_SIZE)
            {
                /* If message size is greater than MAX_SIZE, set the pending
                 * flag as true and break */
                *pu1BulkUpdPendFlg = OSIX_TRUE;
                break;
            }

            if ((NextRtInfo.u2RtProto == OSPF_ID) ||
                (NextRtInfo.u2RtProto == STATIC_ID) ||
                (NextRtInfo.u2RtProto == BGP_ID))
            {
                RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4CxtId);
                RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, NextRtInfo.u4DestNet);
                RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, NextRtInfo.u4DestMask);
                RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, NextRtInfo.u4NextHop);
                RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, NextRtInfo.u4Flag);
                RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, NextRtInfo.u4HwIntfId[0]);
                RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, NextRtInfo.u4HwIntfId[1]);
                RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, NextRtInfo.u2RtProto);
                RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, i1RouteType);

                pPrevRtInfo = &NextRtInfo;
                u2MsgPacked = (UINT2) (u2MsgPacked + 1);
            }

            MEMSET (&RtInfo, 0, sizeof (tRtInfo));
            RtInfo.u4DestNet = NextRtInfo.u4DestNet;
            RtInfo.u4DestMask = NextRtInfo.u4DestMask;
            RtInfo.u4NextHop = NextRtInfo.u4NextHop;
            i4RetVal = RtmUtilGetNextAllRtInCxt (pRtmCxt, RtInfo, &NextRtInfo);
        }
        if (UtilRtmGetNextCxtId (u4CxtId, &u4NxtCxtId) == RTM_FAILURE)
        {
            break;
        }
        u4CxtId = u4NxtCxtId;
        pRtmCxt = UtilRtmGetCxt (u4CxtId);
    }
    u2OffSet = (UINT2) u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4LenOffSet, u2OffSet);
    RTM_RM_PUT_2_BYTE (pMsg, &u4NoOffset, u2MsgPacked);

    if (u2MsgPacked == 0)
    {
        /* If message packet is 0, then do not send the message */
        RM_FREE (pMsg);
        return RTM_SUCCESS;
    }

    if (RtmRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        *pu1BulkUpdPendFlg = OSIX_FALSE;
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return RTM_FAILURE;
    }

    pRtmRedMarker->u4CxtId = u4CxtId;
    pRtmRedMarker->u4DestNet = pPrevRtInfo->u4DestNet;
    pRtmRedMarker->u4DestMask = pPrevRtInfo->u4DestMask;
    pRtmRedMarker->u4NextHop = pPrevRtInfo->u4NextHop;
    pRtmRedMarker->u2RtProto = pPrevRtInfo->u2RtProto;
    pRtmRedMarker->u4HwIntfId[0] = pPrevRtInfo->u4HwIntfId[0];
    pRtmRedMarker->u4HwIntfId[1] = pPrevRtInfo->u4HwIntfId[1];
    /* RtmRedMarker is set to the last updated entry */
    return RTM_SUCCESS;
}

/***********************************************************************
 * Function Name      : RtmRedSendFrtBulkInfo                         
 *                                                                      
 * Description        : This function sends the Bulk update messages to 
 *                      the peer standby RTM.                           
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : pu1BulkUpdPendFlg                               
 *                                                                      
 * Returns            : RTM_SUCCESS/RTM_FAILURE                         
 ***********************************************************************/

PUBLIC INT4
RtmRedSendFrtBulkInfo (UINT1 *pu1BulkUpdPendFlg)
{

    tRmMsg             *pMsg = NULL;
    tRtmHARedFrtMarker *pRtmRedFrtMarker = NULL;
    tRtmFrtInfo        *pPrevRtmFrtInfo = NULL;
    tRtmFrtInfo         RtmFrtInfo;
    tRtmFrtInfo        *pNextRtmFrtInfo = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgPacked = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = 0;
    UINT4               u4NoOffset = 0;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    RTM Bulk Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|    
     *    ------------------------------------------------------------------------    
     *    | Msg. Type | Length | No of   | Context |  Dest    | Mask    | NextHop |    
     *    |           |        | Entries |   Id    |  Network | Length  | Address |    
     *    -------------------------------------------------------------------------    
     *
     *    |<-2 B->|<-4 B->|<- 4 B ->|<- 1 B ->|<- 1 B ->|
     *    ----------------------------------------------
     *    | Route | Route | Metric  | Bit     | Route  |
     *    | Proto | Flag  | Type    | Mask    | Count  |
     *    ---------------------------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RTM_RED_MAX_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return RTM_FAILURE;
    }

    pRtmRedFrtMarker = &(gRtmRedGlobalInfo.RtmHARedFrtMarker);

    RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, RTM_RED_FRT_BULK_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u4LenOffSet = u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    u4NoOffset = u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgPacked);
    MEMSET (&RtmFrtInfo, 0, sizeof (tRtmFrtInfo));
    RtmFrtInfo.u4CxtId = pRtmRedFrtMarker->u4CxtId;
    RtmFrtInfo.u4DestNet = pRtmRedFrtMarker->u4DestNet;
    RtmFrtInfo.u4DestMask = pRtmRedFrtMarker->u4DestMask;
    RtmFrtInfo.u4NextHop = pRtmRedFrtMarker->u4NextHop;
    RtmFrtInfo.u2RtProto = pRtmRedFrtMarker->u2RtProto;
    pNextRtmFrtInfo = RtmGetNextFrtEntry (&RtmFrtInfo);
    while (pNextRtmFrtInfo != NULL)
    {
        if ((u4OffSet + RTM_RED_FRT_INFO_SIZE) > RTM_RED_MAX_MSG_SIZE)
        {
            /* If message size is greater than MAX_SIZE, set the pending
             * flag as true and break */
            *pu1BulkUpdPendFlg = OSIX_TRUE;
            break;
        }

        if ((pNextRtmFrtInfo->u2RtProto == OSPF_ID) ||
            (pNextRtmFrtInfo->u2RtProto == STATIC_ID) ||
            (pNextRtmFrtInfo->u2RtProto == BGP_ID))
        {
            RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->u4CxtId);
            RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->u4DestNet);
            RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->u4DestMask);
            RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->u4NextHop);
            RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->u4Flag);
            RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->i4MetricType);
            RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->u1BitMask);
            RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->u2RtProto);
            RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, pNextRtmFrtInfo->u1RtCount);
            pPrevRtmFrtInfo = pNextRtmFrtInfo;
            u2MsgPacked = (UINT2) (u2MsgPacked + 1);
        }

        MEMSET (&RtmFrtInfo, 0, sizeof (tRtmFrtInfo));
        RtmFrtInfo.u4CxtId = pNextRtmFrtInfo->u4CxtId;
        RtmFrtInfo.u4DestNet = pNextRtmFrtInfo->u4DestNet;
        RtmFrtInfo.u4DestMask = pNextRtmFrtInfo->u4DestMask;
        RtmFrtInfo.u4NextHop = pNextRtmFrtInfo->u4NextHop;
        RtmFrtInfo.u2RtProto = pNextRtmFrtInfo->u2RtProto;
        pNextRtmFrtInfo = RtmGetNextFrtEntry (&RtmFrtInfo);
    }
    u2OffSet = (UINT2) u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4LenOffSet, u2OffSet);
    RTM_RM_PUT_2_BYTE (pMsg, &u4NoOffset, u2MsgPacked);

    if (u2MsgPacked == 0)
    {
        /* If message packet is 0, then do not send the message */
        RM_FREE (pMsg);
        return RTM_SUCCESS;
    }

    if (RtmRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        *pu1BulkUpdPendFlg = OSIX_FALSE;
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return RTM_FAILURE;
    }

    pRtmRedFrtMarker->u4CxtId = pPrevRtmFrtInfo->u4CxtId;
    pRtmRedFrtMarker->u4DestNet = pPrevRtmFrtInfo->u4DestNet;
    pRtmRedFrtMarker->u4DestMask = pPrevRtmFrtInfo->u4DestMask;
    pRtmRedFrtMarker->u4NextHop = pPrevRtmFrtInfo->u4NextHop;
    pRtmRedFrtMarker->u2RtProto = pPrevRtmFrtInfo->u2RtProto;
    pRtmRedFrtMarker->u1BitMask = pPrevRtmFrtInfo->u1BitMask;
    pRtmRedFrtMarker->i4MetricType = pPrevRtmFrtInfo->i4MetricType;
    pRtmRedFrtMarker->u1RtCount = pPrevRtmFrtInfo->u1RtCount;
    /* RtmRedMarker is set to the last updated entry */
    return RTM_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the RTM offers an IP address         */
/*                      to the RTM client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pRtmBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
RtmRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT4               u4OffSet = 0;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr *  RTM_RED_BULK_UPD_TAIL_MSG  * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RTM_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, RTM_RED_BULK_UPD_TAIL_MESSAGE);
    RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, RTM_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    u2OffSet = (UINT2) u4OffSet;
    if (RtmRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
RtmRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);

    return;
}

/************************************************************************
 * Function Name      : RtmRedSendDynamicInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the RTM offers an IP address 
 *                      to the RTM client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : i1RouteType - Add/Delete route
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
RtmRedSendDynamicInfo (INT1 i1RouteType)
{
    tRmMsg             *pMsg = NULL;
    tRtmRedTable       *pRtmRedInfo;
    tRtmRedTable        RtmRedInfo;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4MsgAllocSize = 0;
    UINT4               u4Count = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2OffSet = 0;
    UINT4               u4OffSet = 0;
    UINT4               u4LenOffSet = 0;
    UINT2               u2MsgPacked = 0;
    UINT4               u4NoOffset = 0;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    RBTreeCount (gRtmGlobalInfo.RtmRedTable, &u4Count);
    u4MsgAllocSize = RTM_RED_MIM_MSG_SIZE;
    u4MsgAllocSize += (u4Count * (RTM_RED_DYN_INFO_SIZE));
    u4MsgAllocSize = (u4MsgAllocSize < RTM_RED_MAX_MSG_SIZE) ?
        u4MsgAllocSize : RTM_RED_MAX_MSG_SIZE;

    /* Message is allcoated upto required size. The number of entries in the 
     * RBTree is got and then it is multiplied with the size of a single 
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    RTM Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|    
     *    ---------------------------------------------------------------    
     *    | Msg. Type | Length | No of   | Dest    | Mask    | NextHop |    
     *    |           |        | Entries | Network | Length  | Address |    
     *    ---------------------------------------------------------------    
     *
     *    |<-2 B->|<-1 B->|<- 1 B ->|
     *    ---------------------------
     *    | Route | Route | Hw      |
     *    | Proto | Flag  | Status  |
     *    ---------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (u4MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        return;
    }

    RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, RTM_RED_DYN_INFO);

    /* INITIALLY SET THE LENGTH TO MAXIMUM LENGTH */
    u4LenOffSet = u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    u4NoOffset = u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgPacked);

    pRtmRedInfo = RBTreeGetFirst (gRtmGlobalInfo.RtmRedTable);
    while (pRtmRedInfo != NULL)
    {
        if ((UINT4) (u4OffSet + RTM_RED_DYN_INFO_SIZE) > u4MsgAllocSize)
        {
            /* If there message size is greater than the allocated size, then
             * the remaining entries are sent in another update. */
            break;
        }

        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtmRedInfo->u4CxtId);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtmRedInfo->u4DestNet);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtmRedInfo->u4DestMask);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtmRedInfo->u4NextHop);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtmRedInfo->u4Flag);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtmRedInfo->u4HwIntfId[0]);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtmRedInfo->u4HwIntfId[1]);
        RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, pRtmRedInfo->u2RtProto);
        RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, i1RouteType);
        u2MsgPacked = (UINT2) (u2MsgPacked + 1);

        MEMSET (&RtmRedInfo, 0, sizeof (tRtmRedTable));
        RtmRedInfo.u2RtProto = pRtmRedInfo->u2RtProto;
        RtmRedInfo.u4DestNet = pRtmRedInfo->u4DestNet;
        RtmRedInfo.u4DestMask = pRtmRedInfo->u4DestMask;
        RtmRedInfo.u4NextHop = pRtmRedInfo->u4NextHop;
        RtmRedInfo.u4HwIntfId[0] = pRtmRedInfo->u4HwIntfId[0];
        RtmRedInfo.u4HwIntfId[1] = pRtmRedInfo->u4HwIntfId[1];
        pRtmRedInfo = RBTreeGetNext (gRtmGlobalInfo.RtmRedTable,
                                     (tRBElem *) & RtmRedInfo, NULL);
    }
    u2OffSet = (UINT2) u4OffSet;
    RTM_RM_PUT_2_BYTE (pMsg, &u4LenOffSet, u2OffSet);
    RTM_RM_PUT_2_BYTE (pMsg, &u4NoOffset, u2MsgPacked);

    if (u2MsgPacked == 0)
    {
        /* If there are no new items to sent, then it the message is not sent */
        RM_FREE (pMsg);
        return;
    }

    if (RtmRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }
    pRtmRedInfo =
        RBTreeGet (gRtmGlobalInfo.RtmRedTable, (tRBElem *) & RtmRedInfo);
    if (pRtmRedInfo != NULL)
    {
        pRtmRedInfo->u1DelFlag = RTM_RED_DEL;
    }
    /* The last updated message's delete flag is set */
    pRtmRedInfo = RBTreeGetFirst (gRtmGlobalInfo.RtmRedTable);
    while (pRtmRedInfo != NULL)
    {
        RBTreeRem (gRtmGlobalInfo.RtmRedTable, pRtmRedInfo);
        if (pRtmRedInfo->u1DelFlag == RTM_RED_DEL)
        {
            MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID, (UINT1 *) pRtmRedInfo);
            break;
        }
        MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID, (UINT1 *) pRtmRedInfo);

        MEMSET (&RtmRedInfo, 0, sizeof (tRtmRedTable));
        RtmRedInfo.u2RtProto = pRtmRedInfo->u2RtProto;
        RtmRedInfo.u4DestNet = pRtmRedInfo->u4DestNet;
        RtmRedInfo.u4DestMask = pRtmRedInfo->u4DestMask;
        RtmRedInfo.u4NextHop = pRtmRedInfo->u4NextHop;
        RtmRedInfo.u4HwIntfId[0] = pRtmRedInfo->u4HwIntfId[0];
        RtmRedInfo.u4HwIntfId[1] = pRtmRedInfo->u4HwIntfId[1];
        pRtmRedInfo = RBTreeGetNext (gRtmGlobalInfo.RtmRedTable,
                                     (tRBElem *) & RtmRedInfo, NULL);
    }

    return;
}

/************************************************************************
 * Function Name      : RtmRedSyncFrtInfo 
 *                                                
 * Description        : This function sends the binding table dynamic  
 *                      sync up message to Standby node from the Active 
 *                      node, when the RTM offers an IP address 
 *                      to the RTM client and dynamically update the   
 *                      binding table entries.                        
 *                                                                   
 * Input(s)           : None
 *                                                                    
 * Output(s)          : None                                         
 *                                                                  
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                
 ************************************************************************/

PUBLIC VOID
RtmRedSyncFrtInfo (tRtInfo * pRtInfo, UINT4 u4CxtId, UINT1 u1RtCount,
                   UINT1 u1Flag)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT4               u4MsgAllocSize = 0;
    UINT4               u4LenOffSet = 0;
    UINT4               u4NoOffset = 0;
    UINT2               u2MsgPacked = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2MsgLen = 0;

    ProtoEvt.u4AppId = RM_RTM_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (RTM_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return;
    }
    u4MsgAllocSize = RTM_RED_FRT_INFO_SIZE;

    /* Message is allcoated upto required size. The number of entries in the
     * RBTree is got and then it is multiplied with the size of a single
     * dynamic update to get the total count. If that length is greater than
     * MAX size, then the message is allocated upto max size */

    /*
     *    RTM Dynamic Update message
     *
     *    <- 1 byte ->|<- 2 B->|<- 2 B ->|<- 4 B ->|<- 4 B ->|<- 4 B ->|
     *    ---------------------------------------------------------------
     *    | Msg. Type | Length | No of   | Dest    | Mask    | NextHop |
     *    |           |        | Entries | Network | Length  | Address |
     *    ---------------------------------------------------------------
     *
     *    |<-2 B->|<-1 B->|<-4 B ->|<- 1 B ->|<- 1 B ->
     *    --------------------------------------------
     *    | Route | Route | Metric | Bit     | Route  |
     *    | Proto | Flag  | Type   | Mask    | Count  |
     *    --------------------------------------------
     */
    u4MsgAllocSize = u4MsgAllocSize + 9 + 1;
    if ((pMsg = RM_ALLOC_TX_BUF (u4MsgAllocSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);

        return;
    }
    else
    {
        u2OffSet = 0;

        RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, RTM_RED_SYNC_FRT_INFO);
        u4LenOffSet = u4OffSet;
        RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
        u4NoOffset = u4OffSet;
        RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgPacked);

        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4CxtId);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtInfo->u4DestNet);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtInfo->u4DestMask);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtInfo->u4NextHop);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtInfo->u4Flag);
        RTM_RM_PUT_4_BYTE (pMsg, &u4OffSet, pRtInfo->i4MetricType);
        RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, pRtInfo->u1BitMask);
        RTM_RM_PUT_2_BYTE (pMsg, &u4OffSet, pRtInfo->u2RtProto);
        RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, u1RtCount);
        RTM_RM_PUT_1_BYTE (pMsg, &u4OffSet, u1Flag);
        u2MsgPacked = (UINT2) (u2MsgPacked + 1);
        u2OffSet = (UINT2) u4OffSet;
        RTM_RM_PUT_2_BYTE (pMsg, &u4LenOffSet, u2OffSet);
        RTM_RM_PUT_2_BYTE (pMsg, &u4NoOffset, u2MsgPacked);

        if (u2MsgPacked == 0)
        {
            /* If there are no new items to sent, then it the message is not sent */
            RM_FREE (pMsg);
            return;
        }

        if (RtmRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
            return;
        }

    }
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessBulkInfo                           */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RtmRedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRtInfo            *pRtInfo = NULL;
    tRtInfo             RtInfo;
    tRtmCxt            *pRtmCxt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtmRedTable        RtmRedNode;
    tRtmRedTable       *pRtmRedNode = NULL;
    UINT4               u4NextHop = 0;
    UINT4               au4Indx[IP_TWO];
    UINT4               u4CxtId;
    UINT4               u4Flag;
    UINT4               u4DestNet;
    UINT4               u4DestMask;
    UINT4               u4HwIntfId[2];
    INT4                i4OutCome = IP_SUCCESS;
    UINT2               u2RtProto;
    UINT2               u2NoOfEntries = 0;
    UINT4               u4TempOffSet = (UINT4) *pu2OffSet;
    INT1                i1RouteType = 0;

    RTM_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2NoOfEntries);
    while (u2NoOfEntries > 0)
    {
        /* The number of entries is got from the message and it is processed */
        u2NoOfEntries--;
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4CxtId);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4DestNet);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4DestMask);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4NextHop);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4Flag);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4HwIntfId[0]);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4HwIntfId[1]);
        RTM_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2RtProto);
        RTM_RM_GET_1_BYTE (pMsg, &u4TempOffSet, i1RouteType);

        pRtmCxt = UtilRtmGetCxt (u4CxtId);
        if (pRtmCxt == NULL)
        {
            RM_FREE (pMsg);
            return;
        }
        au4Indx[0] = u4DestNet;
        au4Indx[1] = u4DestMask;
        au4Indx[0] = IP_HTONL ((au4Indx[0] & au4Indx[1]));
        au4Indx[1] = IP_HTONL (au4Indx[1]);
        InParams.Key.pKey = (UINT1 *) au4Indx;
        InParams.i1AppId = (INT1) (u2RtProto - 1);
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        OutParams.Key.pKey = NULL;

        i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

        if (i4OutCome == TRIE_SUCCESS)
        {
            pRtInfo = (tRtInfo *) OutParams.pAppSpecInfo;

            while (pRtInfo != NULL)
            {
                if ((pRtInfo)->u4NextHop == u4NextHop)
                {
                    pRtInfo->u4Flag = u4Flag;
                    break;
                }
                pRtInfo = pRtInfo->pNextAlternatepath;
            }
        }
        else
        {
            /* When the route is not found in trie and the HWSTATUS
             * flag is set, then the RPS has added the route 
             * to active but the sync to Standby and standby RPS
             * informing RTM is yet to occur. Hence, preserve the 
             * RTM route syncup till RPS informs RTM. The REDTable 
             * will be cleared when RPS adds the route to RTM */
            if (u4Flag & RTM_RT_HWSTATUS)
            {
                MEMSET (&RtInfo, 0, sizeof (tRtInfo));
                RtInfo.u4DestNet = u4DestNet;
                RtInfo.u4DestMask = u4DestMask;
                RtInfo.u4NextHop = u4NextHop;
                RtInfo.u4HwIntfId[0] = u4HwIntfId[1];
                RtInfo.u4HwIntfId[1] = u4HwIntfId[0];
                RtInfo.u2RtProto = u2RtProto;
                RtInfo.u4Flag = u4Flag;
                RtmRedAddDynamicInfo (&RtInfo, u4CxtId);
            }
        }
        if (pRtInfo != NULL)
        {
            RtmRedNode.u4CxtId = u4CxtId;
            RtmRedNode.u4DestNet = pRtInfo->u4DestNet;
            RtmRedNode.u4DestMask = pRtInfo->u4DestMask;
            RtmRedNode.u4NextHop = pRtInfo->u4NextHop;
            RtmRedNode.u4HwIntfId[0] = pRtInfo->u4HwIntfId[1];
            RtmRedNode.u4HwIntfId[1] = pRtInfo->u4HwIntfId[0];
            RtmRedNode.u2RtProto = pRtInfo->u2RtProto;
            if ((u4Flag & RTM_RT_HWSTATUS))
            {
                pRtmRedNode = RBTreeGet (gRtmGlobalInfo.RtmRedTable,
                                         (tRBElem *) & RtmRedNode);
                /* If the hardware status is NP_PRESENT, then the corresponding
                 * entry is deleted from the temporary RBTree and added to the
                 * software. If the hardware status is NP_NOT_PRESENT, then the
                 * corresponding entry is added to the temporary data 
                 * structure */

                if (pRtmRedNode != NULL)
                {
                    RBTreeRem (gRtmGlobalInfo.RtmRedTable, pRtmRedNode);
                    MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID,
                                        (UINT1 *) pRtmRedNode);
                }
            }
            else if (!(u4Flag & RTM_RT_HWSTATUS))
            {
                RtmRedAddDynamicInfo (pRtInfo, u4CxtId);
            }
        }
        if (pRtInfo != NULL)
        {
            if ((i1RouteType == NETIPV4_ADD_ROUTE)
                && (u4Flag & RTM_RT_DROP_ROUTE))
            {
                RtmRedPRTAddDynamicInfo (pRtInfo, u4CxtId);
            }
        }

    }
    *pu2OffSet = (UINT2) u4TempOffSet;
    return;
}

/************************************************************************/
/* Function Name      : RtmFrtProcessBulkInfo                           */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RtmRedProcessFrtBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRtInfo            *pRtInfo = NULL;
    tRtInfo             RtInfo;
    tRtmCxt            *pRtmCxt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtmFrtInfo         RtmFrtNode;
    tRtmFrtInfo        *pRtmFrtNode = NULL;
    UINT4               u4NextHop = 0;
    UINT4               au4Indx[IP_TWO];
    UINT4               u4CxtId = 0;
    UINT4               u4Flag = 0;
    UINT4               u4DestNet = 0;
    UINT4               u4DestMask = 0;
    UINT1               u1BitMask = 0;
    UINT1               u1RtCount = 0;
    UINT4               u4MetricType = 0;
    INT4                i4OutCome = IP_FAILURE;
    UINT2               u2RtProto = 0;
    UINT2               u2NoOfEntries = 0;
    UINT4               u4TempOffSet = (UINT4) *pu2OffSet;

    RTM_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2NoOfEntries);
    while (u2NoOfEntries > 0)
    {
        /* The number of entries is got from the message and it is processed */
        u2NoOfEntries--;
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4CxtId);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4DestNet);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4DestMask);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4NextHop);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4Flag);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4MetricType);
        RTM_RM_GET_1_BYTE (pMsg, &u4TempOffSet, u1BitMask);
        RTM_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2RtProto);
        RTM_RM_GET_1_BYTE (pMsg, &u4TempOffSet, u1RtCount);
        pRtmCxt = UtilRtmGetCxt (u4CxtId);
        if (pRtmCxt == NULL)
        {
            RM_FREE (pMsg);
            return;
        }
        au4Indx[0] = u4DestNet;
        au4Indx[1] = u4DestMask;
        au4Indx[0] = IP_HTONL ((au4Indx[0] & au4Indx[1]));
        au4Indx[1] = IP_HTONL (au4Indx[1]);
        InParams.Key.pKey = (UINT1 *) au4Indx;
        InParams.i1AppId = (INT1) (u2RtProto - 1);
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        OutParams.Key.pKey = NULL;

        i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

        if (i4OutCome == TRIE_SUCCESS)
        {
            pRtInfo = (tRtInfo *) OutParams.pAppSpecInfo;

            while (pRtInfo != NULL)
            {
                if ((pRtInfo)->u4NextHop == u4NextHop)
                {
                    break;
                }
                pRtInfo = pRtInfo->pNextAlternatepath;
            }
        }
        if (pRtInfo != NULL)
        {
            if (pRtInfo->u4Flag & RTM_RT_HWSTATUS)
            {
                MEMSET (&RtmFrtNode, 0, sizeof (tRtmFrtInfo));
                RtmFrtNode.u4CxtId = u4CxtId;
                RtmFrtNode.u4DestNet = pRtInfo->u4DestNet;
                RtmFrtNode.u4DestMask = pRtInfo->u4DestMask;
                RtmFrtNode.u4NextHop = pRtInfo->u4NextHop;
                RtmFrtNode.i4MetricType = pRtInfo->i4MetricType;
                RtmFrtNode.u1BitMask = pRtInfo->u1BitMask;
                RtmFrtNode.u2RtProto = pRtInfo->u2RtProto;
                pRtmFrtNode = RBTreeGet (gRtmGlobalInfo.RtmFrtTable,
                                         (tRBElem *) & RtmFrtNode);
                /* If the hardware status is NP_PRESENT, then the corresponding
                 * entry is deleted from the temporary RBTree and added to the
                 * software. If the hardware status is NP_NOT_PRESENT, then the
                 * corresponding entry is added to the temporary data 
                 * structure */

                if (pRtmFrtNode != NULL)
                {
                    RBTreeRem (gRtmGlobalInfo.RtmFrtTable, pRtmFrtNode);
                    MemReleaseMemBlock (RTM_FRT_MSG_POOL_ID,
                                        (UINT1 *) pRtmFrtNode);
                }
            }
            else
            {
                /* When the route is not found in trie and the HWSTATUS
                 * flag is set, then the RPS has added the route 
                 * to active but the sync to Standby and standby RPS
                 * informing RTM is yet to occur. Hence, preserve the 
                 * RTM route syncup till RPS informs RTM. The REDTable 
                 * will be cleared when RPS adds the route to RTM */
                MEMSET (&RtInfo, 0, sizeof (tRtInfo));
                RtInfo.u4DestNet = u4DestNet;
                RtInfo.u4DestNet = u4DestNet;
                RtInfo.u4DestMask = u4DestMask;
                RtInfo.u4NextHop = u4NextHop;
                RtInfo.u2RtProto = u2RtProto;
                RtInfo.u1BitMask = u1BitMask;
                RtInfo.i4MetricType = (INT4) u4MetricType;
                RtInfo.u4Flag = u4Flag;
                if (RtmFrtAddInfo (&RtInfo, u4CxtId, u1RtCount) == RTM_SUCCESS)
                {
                    pRtmCxt = UtilRtmGetCxt (u4CxtId);
                    pRtmCxt->u4FailedRoutes++;
                }
                else
                {
                    UtlTrcLog (1, 1, "",
                               "ERROR[RTM]: IPV4 Failed Route Add Failed"
                               " route %x mask %x Nexthop %x metric %d\n",
                               RtInfo.u4DestNet, RtInfo.u4DestMask,
                               RtInfo.u4NextHop, RtInfo.i4Metric1);
                }

            }
        }
    }
    *pu2OffSet = (UINT2) u4TempOffSet;
    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RtmRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRtInfo            *pRtInfo = NULL;
    tRtInfo             RtInfo;
    tRtmCxt            *pRtmCxt = NULL;
    tRtmRedTable        RtmRedNode;
    tRtmRedTable       *pRtmRedNode = NULL;
    tRtmRedPRTNode      RtmRedPRTNode;
    tRtmRedPRTNode     *pRtmRedPRTNode = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               u4DestNet;
    UINT4               u4DestMask;
    UINT4               u4HwIntfId[2];
    UINT4               u4NextHop = 0;
    UINT4               au4Indx[IP_TWO];
    UINT4               u4CxtId;
    UINT4               u4Flag;
    INT4                i4OutCome = IP_FAILURE;
    UINT2               u2RtProto;
    UINT2               u2NoOfEntries = 0;
    UINT4               u4TempOffSet = (UINT4) *pu2OffSet;
    INT1                i1RouteType = 0;

    MEMSET (&RtmRedNode, 0, sizeof (tRtmRedTable));
    MEMSET (&RtmRedPRTNode, 0, sizeof (tRtmRedPRTNode));

    RTM_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2NoOfEntries);

    while (u2NoOfEntries > 0)
    {
        /* The number of entries is got from the message and it is processed */
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4CxtId);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4DestNet);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4DestMask);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4NextHop);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4Flag);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4HwIntfId[0]);
        RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4HwIntfId[1]);
        RTM_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2RtProto);
        RTM_RM_GET_1_BYTE (pMsg, &u4TempOffSet, i1RouteType);
        pRtmCxt = UtilRtmGetCxt (u4CxtId);
        if (pRtmCxt == NULL)
        {
            RM_FREE (pMsg);
            return;
        }
        au4Indx[0] = u4DestNet;
        au4Indx[1] = u4DestMask;
        au4Indx[0] = IP_HTONL ((au4Indx[0] & au4Indx[1]));
        au4Indx[1] = IP_HTONL (au4Indx[1]);
        InParams.Key.pKey = (UINT1 *) au4Indx;
        InParams.i1AppId = (INT1) (u2RtProto - 1);
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        OutParams.Key.pKey = NULL;

        i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

        if (i4OutCome == TRIE_SUCCESS)
        {
            pRtInfo = (tRtInfo *) OutParams.pAppSpecInfo;

            while (pRtInfo != NULL)
            {
                if ((pRtInfo)->u4NextHop == u4NextHop)
                {
                    pRtInfo->u4Flag = u4Flag;
                    break;
                }
                pRtInfo = pRtInfo->pNextAlternatepath;
            }
        }
        else
        {
            /* When the route is not found in trie and the HWSTATUS
             * flag is set, then the RPS has added the route 
             * to active but the sync to Standby and standby RPS
             * informing RTM is yet to occur. Hence, preserve the 
             * RTM route syncup till RPS informs RTM. The REDTable 
             * will be cleared when RPS adds the route to RTM */
            if (u4Flag & RTM_RT_HWSTATUS)
            {
                MEMSET (&RtInfo, 0, sizeof (tRtInfo));
                RtInfo.u4DestNet = u4DestNet;
                RtInfo.u4DestMask = u4DestMask;
                RtInfo.u4NextHop = u4NextHop;
                RtInfo.u2RtProto = u2RtProto;
                RtInfo.u4HwIntfId[1] = u4HwIntfId[0];
                RtInfo.u4HwIntfId[0] = u4HwIntfId[1];
                RtInfo.u4Flag = u4Flag;
                RtmRedAddDynamicInfo (&RtInfo, u4CxtId);
            }
        }

        if (pRtInfo != NULL)
        {
            RtmRedNode.u4CxtId = u4CxtId;
            RtmRedNode.u4DestNet = pRtInfo->u4DestNet;
            RtmRedNode.u4DestMask = pRtInfo->u4DestMask;
            RtmRedNode.u4NextHop = pRtInfo->u4NextHop;
            RtmRedNode.u4HwIntfId[0] = pRtInfo->u4HwIntfId[1];
            RtmRedNode.u4HwIntfId[1] = pRtInfo->u4HwIntfId[0];
            RtmRedNode.u2RtProto = pRtInfo->u2RtProto;
            if ((u4Flag & RTM_RT_HWSTATUS))
            {
                pRtmRedNode = RBTreeGet (gRtmGlobalInfo.RtmRedTable,
                                         (tRBElem *) & RtmRedNode);
                /* If the hardware status is NP_PRESENT, then the corresponding
                 * entry is deleted from the temporary RBTree and added to the
                 * software. If the hardware status is NP_NOT_PRESENT, then the
                 * corresponding entry is added to the temporary data 
                 * structure */

                if (pRtmRedNode != NULL)
                {
                    RBTreeRem (gRtmGlobalInfo.RtmRedTable, pRtmRedNode);
                    MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID,
                                        (UINT1 *) pRtmRedNode);
                }
            }
            else if (!(u4Flag & RTM_RT_HWSTATUS))
            {
                RtmRedAddDynamicInfo (pRtInfo, u4CxtId);
            }
        }
        if (pRtInfo != NULL)
        {
            RtmRedPRTNode.u4CxtId = u4CxtId;
            RtmRedPRTNode.u4DestNet = pRtInfo->u4DestNet;
            RtmRedPRTNode.u4DestMask = pRtInfo->u4DestMask;
            RtmRedPRTNode.u4NextHop = pRtInfo->u4NextHop;
            RtmRedPRTNode.u2RtProto = pRtInfo->u2RtProto;

            /* Drop route deletion */
            if ((i1RouteType == NETIPV4_DELETE_ROUTE)
                && (u4Flag & RTM_RT_DROP_ROUTE))
            {
                pRtmRedPRTNode = RBTreeGet (gRtmGlobalInfo.RtmRedPRTNode,
                                            (tRBElem *) & RtmRedPRTNode);

                if (pRtmRedPRTNode != NULL)
                {
                    RBTreeRem (gRtmGlobalInfo.RtmRedPRTNode, pRtmRedPRTNode);
                    MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID,
                                        (UINT1 *) pRtmRedPRTNode);
                }
            }
            else if ((i1RouteType == NETIPV4_ADD_ROUTE)
                     && (u4Flag & RTM_RT_DROP_ROUTE))
            {
                RtmRedPRTAddDynamicInfo (pRtInfo, u4CxtId);
            }
        }

        u2NoOfEntries--;
    }
    *pu2OffSet = (UINT2) u4TempOffSet;

    return;
}

/************************************************************************/
/* Function Name      : RtmRedProcessSyncFrtInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
RtmRedProcessSyncFrtInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRtmFrtInfo        *pRtmFrtNode = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo             RtInfo;
    tRtInfo            *pRtInfo = NULL;
    UINT4               u4DestNet = 0;
    UINT4               u4DestMask = 0;
    UINT4               u4TempOffSet = (UINT4) *pu2OffSet;
    UINT4               u4NextHop = 0;
    UINT4               u4CxtId = 0;
    UINT4               u4Flag = 0;
    UINT4               u4MetricType = 0;
    UINT2               u2RtProto = 0;
    UINT2               u2NoOfEntries = 0;
    UINT1               u1BitMask = 0;
    UINT1               u1Flag = 0;
    UINT1               u1RtCount = 0;
    UINT4               au4Indx[IP_TWO];
    INT4                i4OutCome = IP_FAILURE;

    MEMSET (&RtInfo, 0, sizeof (tRtInfo));
    MEMSET (&InParams, 0, sizeof (tInputParams));
    MEMSET (&OutParams, 0, sizeof (tOutputParams));

    RTM_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2NoOfEntries);

    RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4CxtId);
    RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4DestNet);
    RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4DestMask);
    RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4NextHop);
    RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4Flag);
    RTM_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4MetricType);
    RTM_RM_GET_1_BYTE (pMsg, &u4TempOffSet, u1BitMask);
    RTM_RM_GET_2_BYTE (pMsg, &u4TempOffSet, u2RtProto);
    RTM_RM_GET_1_BYTE (pMsg, &u4TempOffSet, u1RtCount);
    RTM_RM_GET_1_BYTE (pMsg, &u4TempOffSet, u1Flag);
    pRtmCxt = UtilRtmGetCxt (u4CxtId);

    if (pRtmCxt == NULL)
    {
        RM_FREE (pMsg);
        return;
    }

    au4Indx[0] = u4DestNet;
    au4Indx[1] = u4DestMask;
    au4Indx[0] = IP_HTONL ((au4Indx[0] & au4Indx[1]));
    au4Indx[1] = IP_HTONL (au4Indx[1]);
    InParams.Key.pKey = (UINT1 *) au4Indx;
    InParams.i1AppId = (INT1) (u2RtProto - 1);
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    OutParams.Key.pKey = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        pRtInfo = (tRtInfo *) OutParams.pAppSpecInfo;

        while (pRtInfo != NULL)
        {
            if ((pRtInfo)->u4NextHop == u4NextHop)
            {
                break;
            }
            pRtInfo = pRtInfo->pNextAlternatepath;
        }
    }
    RtInfo.u4DestNet = u4DestNet;
    RtInfo.u4DestMask = u4DestMask;
    RtInfo.u4NextHop = u4NextHop;
    RtInfo.i4MetricType = (INT4) u4MetricType;
    RtInfo.u1BitMask = u1BitMask;
    RtInfo.u2RtProto = u2RtProto;
    RtInfo.u4Flag = u4Flag;

    if (u1Flag == RTM_ADD_ROUTE)
    {
        if (pRtInfo != NULL)
        {
            /*  Adding a new node */
            if (RtmFrtAddInfo (&RtInfo, u4CxtId, u1RtCount) == RTM_SUCCESS)
            {
                pRtmCxt = UtilRtmGetCxt (u4CxtId);
                pRtmCxt->u4FailedRoutes++;
            }
            else
            {
                UtlTrcLog (1, 1, "",
                           "ERROR[RTM]: IPV4 Failed Route Add Failed"
                           " route %x mask %x Nexthop %x metric %d\n",
                           RtInfo.u4DestNet, RtInfo.u4DestMask,
                           RtInfo.u4NextHop, RtInfo.i4Metric1);
            }
        }
    }
    else
    {
        /* Deleting a Node based on the flag status */
        pRtmFrtNode = RtmFrtGetInfo (&RtInfo, u4CxtId);
        if (pRtmFrtNode != NULL)
        {
            if (RtmFrtDeleteEntry (pRtmFrtNode) == RTM_SUCCESS)
            {
                pRtmCxt = UtilRtmGetCxt (u4CxtId);
                pRtmCxt->u4FailedRoutes--;
            }
        }
    }

    return;
}

/************************************************************************/
/* Function Name      : RtmRedAddDynamicInfo                            */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pRtInfo - Rtm Route  message                    */
/*                      u4CxtId - Context Id of the entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
RtmRedAddDynamicInfo (tRtInfo * pRtInfo, UINT4 u4CxtId)
{
    tRtmRedTable       *pRtmRedNode = NULL;
    tRtmRedTable       *pTmpRtmRedNode = NULL;
    tRtmRedTable        RtmRedNode;
    UINT4               u4Result = 0;

    if (pRtInfo == NULL)
    {
        return;
    }
    if ((pRtInfo->u2RtProto != STATIC_ID) && (pRtInfo->u2RtProto != OSPF_ID)
        && (pRtInfo->u2RtProto != BGP_ID) && (pRtInfo->u2RtProto != RIP_ID)
        && (pRtInfo->u2RtProto != ISIS_ID))
    {
        return;
    }
    if ((RTM_GET_NODE_STATUS () == RM_ACTIVE)
        && ((gRtmRedGlobalInfo.u1BulkUpdStatus == RTM_HA_UPD_NOT_STARTED)
            || (RTM_IS_STANDBY_UP () == OSIX_FALSE)))
    {
        /* If bulk update is not started, then do not add the entry in the
         * RBtree as this will be taken care through bulk update */
        return;
    }

    /* Check whether the entry is already present in the RBtree, if present
     * just update the fields. If not present, allocate memory and add the 
     * entry to the RBtree */
    RtmRedNode.u4CxtId = u4CxtId;
    RtmRedNode.u4DestNet = pRtInfo->u4DestNet;
    RtmRedNode.u4DestMask = pRtInfo->u4DestMask;
    RtmRedNode.u4NextHop = pRtInfo->u4NextHop;
    RtmRedNode.u4HwIntfId[0] = pRtInfo->u4HwIntfId[0];
    RtmRedNode.u4HwIntfId[1] = pRtInfo->u4HwIntfId[1];
    RtmRedNode.u2RtProto = pRtInfo->u2RtProto;

    pTmpRtmRedNode = RBTreeGet (gRtmGlobalInfo.RtmRedTable,
                                (tRBElem *) & RtmRedNode);
    if (pTmpRtmRedNode == NULL)
    {
        pRtmRedNode =
            (tRtmRedTable *) MemAllocMemBlk ((tMemPoolId) RTM_DYN_MSG_POOL_ID);
        if (pRtmRedNode == NULL)
        {
            return;
        }
        MEMSET (pRtmRedNode, 0, sizeof (tRtmRedTable));
        pRtmRedNode->u4CxtId = u4CxtId;
        pRtmRedNode->u4DestNet = pRtInfo->u4DestNet;
        pRtmRedNode->u4DestMask = pRtInfo->u4DestMask;
        pRtmRedNode->u4NextHop = pRtInfo->u4NextHop;
        pRtmRedNode->u4HwIntfId[0] = pRtInfo->u4HwIntfId[0];
        pRtmRedNode->u4HwIntfId[1] = pRtInfo->u4HwIntfId[1];
        pRtmRedNode->u2RtProto = pRtInfo->u2RtProto;
        pRtmRedNode->u4Flag = pRtInfo->u4Flag;
        u4Result = RBTreeAdd (gRtmGlobalInfo.RtmRedTable, pRtmRedNode);
    }
    else
    {
        pRtmRedNode = pTmpRtmRedNode;
    }
    pRtmRedNode->u4Flag = pRtInfo->u4Flag;

    UNUSED_PARAM (u4Result);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedPRTAddDynamicInfo                         */
/*                                                                      */
/* Description        : This function adds the dynamic entries to the   */
/*                      global tree and this tree is scanned everytime  */
/*                      for sending the dynamic entry.                  */
/*                                                                      */
/* Input(s)           : pRtInfo - Rtm Route  message                    */
/*                      u4CxtId - Context Id of the entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
RtmRedPRTAddDynamicInfo (tRtInfo * pRtInfo, UINT4 u4CxtId)
{
    tRtmRedPRTNode     *pRtmRedPRTNode = NULL;
    tRtmRedPRTNode     *pTmpRtmRedPRTNode = NULL;
    tRtmRedPRTNode      RtmRedPRTNode;
    UINT4               u4Result = 0;

    /* Check whether the entry is already present in the RBtree, if present
     * just update the fields. If not present, allocate memory and add the
     * entry to the RBtree */
    RtmRedPRTNode.u4CxtId = u4CxtId;
    RtmRedPRTNode.u4DestNet = pRtInfo->u4DestNet;
    RtmRedPRTNode.u4DestMask = pRtInfo->u4DestMask;
    RtmRedPRTNode.u4NextHop = pRtInfo->u4NextHop;
    RtmRedPRTNode.u2RtProto = pRtInfo->u2RtProto;

    pTmpRtmRedPRTNode = RBTreeGet (gRtmGlobalInfo.RtmRedPRTNode,
                                   (tRBElem *) & RtmRedPRTNode);
    if (pTmpRtmRedPRTNode == NULL)
    {
        pRtmRedPRTNode =
            (tRtmRedPRTNode *) MemAllocMemBlk ((tMemPoolId)
                                               RTM_DYN_MSG_POOL_ID);
        if (pRtmRedPRTNode == NULL)
        {
            return;
        }
        pRtmRedPRTNode->u4CxtId = u4CxtId;
        pRtmRedPRTNode->u4DestNet = pRtInfo->u4DestNet;
        pRtmRedPRTNode->u4DestMask = pRtInfo->u4DestMask;
        pRtmRedPRTNode->u4NextHop = pRtInfo->u4NextHop;
        pRtmRedPRTNode->u2RtProto = pRtInfo->u2RtProto;
        pRtmRedPRTNode->u4Flag = pRtInfo->u4Flag;
        u4Result = RBTreeAdd (gRtmGlobalInfo.RtmRedPRTNode, pRtmRedPRTNode);
        if (u4Result == RB_FAILURE)
        {
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Adding routes to PRT Node failed"
                       "u4DestNet : %x , u4DestMask : %x \n",
                       pRtmRedPRTNode->u4DestNet, pRtmRedPRTNode->u4DestMask);
        }

    }
    UNUSED_PARAM (u4Result);
    return;
}

/************************************************************************/
/* Function Name      : RtmRedHwAudit                                   */
/*                                                                      */
/* Description        : This function does the hardware audit in two    */
/*                      approches.                                      */
/*                                                                      */
/*                      First:                                          */
/*                      When there is a transaction between standby and */
/*                      active node, the RtmRedTable is walked, if      */
/*                      there  are any entries in the table, they are   */
/*                      verified with the hardware, if the entry is     */
/*                      present in the hardware, then the entry is      */
/*                      added to the sofware. If not, the entry is      */
/*                      deleted.                                        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
RtmRedHwAudit ()
{
#ifdef NPAPI_WANTED
    tRtmRedTable       *pRtmRedInfo = NULL;
    tRtmRedPRTNode     *pRtmRedPRTInfo = NULL;
    tFsNpNextHopInfo    nextHopInfo;
    tRtInfo            *pRtInfo = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tNpRtmInput         RtmNpInParam;
    tNpRtmOutput        RtmNpOutParam;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];
    UINT4               u4CfaIfIndex = 0;
    INT4                i4OutCome = IP_FAILURE;
    INT4                i4FreeDefIpB4Del = 0;
    UINT1               u1RtCount = 0;
    UINT1               bu1TblFull = FNP_FALSE;

    MEMSET (&RtmNpInParam, 0, sizeof (tNpRtmInput));
    MEMSET (&RtmNpOutParam, 0, sizeof (tNpRtmOutput));
    MEMSET (&InParams, 0, sizeof (tInputParams));
    MEMSET (&OutParams, 0, sizeof (tOutputParams));

    /* If the audit level is optimized(applicable only in async NP calls) the
     * temporary RBtree is scanned and the entries in the RBtree are checked 
     * in the hardware. If present, then it is added in the software. If not
     * then the entry is ignored. In sync NP calls, there wont be any need to 
     * do the hardware audit in this manner as the entries are added to the
     * standby database only after hardware call is success */

    pRtmRedInfo = RBTreeGetFirst (gRtmGlobalInfo.RtmRedTable);

    while (pRtmRedInfo != NULL)
    {
        RtmNpInParam.u4CxtId = pRtmRedInfo->u4CxtId;
        RtmNpInParam.u4DestNet = pRtmRedInfo->u4DestNet;
        RtmNpInParam.u4DestMask = pRtmRedInfo->u4DestMask;
        RtmNpInParam.u4NextHop = pRtmRedInfo->u4NextHop;
        RtmNpInParam.u4HwIntfId[0] = pRtmRedInfo->u4HwIntfId[0];
        RtmNpInParam.u4HwIntfId[1] = pRtmRedInfo->u4HwIntfId[1];
        pRtmCxt = UtilRtmGetCxt (pRtmRedInfo->u4CxtId);
        if (pRtmCxt == NULL)
        {
            RBTreeRem (gRtmGlobalInfo.RtmRedTable, pRtmRedInfo);
            MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID, (UINT1 *) pRtmRedInfo);
            pRtmRedInfo = RBTreeGetFirst (gRtmGlobalInfo.RtmRedTable);
            continue;
        }
        au4Indx[0] = IP_HTONL ((pRtmRedInfo->u4DestNet)
                               & (pRtmRedInfo->u4DestMask));
        au4Indx[1] = IP_HTONL (pRtmRedInfo->u4DestMask);
        InParams.Key.pKey = (UINT1 *) au4Indx;
        InParams.i1AppId = (INT1) (pRtmRedInfo->u2RtProto - 1);
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        OutParams.Key.pKey = NULL;

        i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));
        MEMSET (&nextHopInfo, 0, sizeof (nextHopInfo));
        nextHopInfo.u4NextHopGt = pRtmRedInfo->u4NextHop;
        nextHopInfo.u1RtCount = u1RtCount;
        nextHopInfo.u4IfIndex = u4CfaIfIndex;
        nextHopInfo.u4HwIntfId[0] = pRtmRedInfo->u4HwIntfId[0];
        nextHopInfo.u4HwIntfId[1] = pRtmRedInfo->u4HwIntfId[1];

        if (IpFsNpIpv4UcGetRoute (RtmNpInParam, &RtmNpOutParam) == FNP_SUCCESS)
        {
            if (i4OutCome == TRIE_SUCCESS)
            {
                pRtInfo = (tRtInfo *) OutParams.pAppSpecInfo;

                while (pRtInfo != NULL)
                {
                    if ((pRtInfo)->u4NextHop == RtmNpOutParam.u4NextHop)
                    {
                        pRtInfo->u4Flag |= RTM_RT_HWSTATUS;
                        break;
                    }
                    pRtInfo = pRtInfo->pNextAlternatepath;
                }
            }
            else
            {
                if (IpFsNpIpv4UcDelRoute (pRtmRedInfo->u4CxtId,
                                          pRtmRedInfo->u4DestNet,
                                          pRtmRedInfo->u4DestMask,
                                          nextHopInfo,
                                          &i4FreeDefIpB4Del) == FNP_FAILURE)
                {
                    UtlTrcLog (1, 1, "",
                               "ERROR[RTM]: Route Entry Deletion failed"
                               "u4DestNet : %x , u4DestMask : %x \n",
                               pRtmRedInfo->u4DestNet, pRtmRedInfo->u4DestMask);
                }
            }

        }
        else
        {
            /* Add the entry in hardware if it is present in s/w and
             * not present in h/w */
            if (i4OutCome == TRIE_SUCCESS)
            {
                if (IpFsNpIpv4UcAddRoute
                    (pRtmRedInfo->u4CxtId, pRtmRedInfo->u4DestNet,
                     pRtmRedInfo->u4DestMask,
#ifndef NPSIM_WANTED
                     &nextHopInfo,
#else
                     nextHopInfo,
#endif
                     &bu1TblFull) == FNP_FAILURE)
                {
                    UtlTrcLog (1, 1, "",
                               "ERROR[RTM]: Route Entry Addition failed"
                               "u4DestNet : %x , u4DestMask : %x \n",
                               pRtmRedInfo->u4DestNet, pRtmRedInfo->u4DestMask);
                    pRtInfo = (tRtInfo *) OutParams.pAppSpecInfo;
                    if (pRtInfo != NULL)
                    {
                        if ((RtmFrtAddInfo
                             (pRtInfo, pRtmRedInfo->u4CxtId,
                              u1RtCount)) == RTM_SUCCESS)
                        {
                            pRtmCxt->u4FailedRoutes++;
                            RtmRedSyncFrtInfo (pRtInfo, pRtmRedInfo->u4CxtId,
                                               u1RtCount, RTM_ADD_ROUTE);
                        }
                        else
                        {
                            UtlTrcLog (1, 1, "",
                                       "ERROR[RTM]: IPV4 Failed Route Add Failed"
                                       " route %x mask %x Nexthop %x metric %d\n",
                                       pRtInfo->u4DestNet, pRtInfo->u4DestMask,
                                       pRtInfo->u4NextHop, pRtInfo->i4Metric1);
                        }
                    }
                }

                pRtmRedInfo->u4HwIntfId[0] = nextHopInfo.u4HwIntfId[0];

            }
        }
        RBTreeRem (gRtmGlobalInfo.RtmRedTable, pRtmRedInfo);
        MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID, (UINT1 *) pRtmRedInfo);

        pRtmRedInfo = RBTreeGetFirst (gRtmGlobalInfo.RtmRedTable);
    }

    pRtmRedPRTInfo = RBTreeGetFirst (gRtmGlobalInfo.RtmRedPRTNode);

    while (pRtmRedPRTInfo != NULL)
    {
        RtmNpInParam.u4CxtId = pRtmRedPRTInfo->u4CxtId;
        RtmNpInParam.u4DestNet = pRtmRedPRTInfo->u4DestNet;
        RtmNpInParam.u4NextHop = pRtmRedPRTInfo->u4NextHop;

        MEMSET (&nextHopInfo, 0, sizeof (nextHopInfo));
        nextHopInfo.u4NextHopGt = pRtmRedPRTInfo->u4NextHop;
        nextHopInfo.u1RtCount = u1RtCount;
        nextHopInfo.u4IfIndex = u4CfaIfIndex;
        if (IpFsNpIpv4UcGetRoute (RtmNpInParam, &RtmNpOutParam) == FNP_SUCCESS)
        {
            if (IpFsNpIpv4UcDelRoute (pRtmRedPRTInfo->u4CxtId,
                                      pRtmRedPRTInfo->u4DestNet,
                                      pRtmRedPRTInfo->u4DestMask,
                                      nextHopInfo,
                                      &i4FreeDefIpB4Del) == FNP_FAILURE)
            {
                UtlTrcLog (1, 1, "",
                           "ERROR[RTM]: Route Entry Deletion failed"
                           "u4DestNet : %x , u4DestMask : %x \n",
                           pRtmRedPRTInfo->u4DestNet,
                           pRtmRedPRTInfo->u4DestMask);
            }
            nextHopInfo.u1RtType = FNP_RT_LOCAL;
            if (IpFsNpIpv4UcAddRoute
                (pRtmRedPRTInfo->u4CxtId, pRtmRedPRTInfo->u4DestNet,
                 pRtmRedPRTInfo->u4DestMask,
#ifndef NPSIM_WANTED
                 &nextHopInfo,
#else
                 nextHopInfo,
#endif
                 &bu1TblFull) == FNP_FAILURE)
            {
                UtlTrcLog (1, 1, "",
                           "ERROR[RTM]: Route Entry Addition failed"
                           "u4DestNet : %x , u4DestMask : %x \n",
                           pRtmRedPRTInfo->u4DestNet,
                           pRtmRedPRTInfo->u4DestMask);
            }
        }
        RBTreeRem (gRtmGlobalInfo.RtmRedPRTNode, pRtmRedPRTInfo);
        MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID, (UINT1 *) pRtmRedPRTInfo);

        pRtmRedPRTInfo = RBTreeGetFirst (gRtmGlobalInfo.RtmRedPRTNode);
    }

#endif

    return;
}

/*-------------------------------------------------------------------+
 * Function           : RtmRBTreeRedEntryCmp
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : -1  if pRBElem <  pRBElemIn
 *                      1   if pRBElem >  pRBElemIn
 *                      0   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy entries in lexicographic
 * order of the indices of the Redundancy entry.
+-------------------------------------------------------------------*/

INT4
RtmRBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tRtmRedTable       *pRtmRed = pRBElem;
    tRtmRedTable       *pRtmRedIn = pRBElemIn;

    if (pRtmRed->u4CxtId < pRtmRedIn->u4CxtId)
    {
        return -1;
    }
    else if (pRtmRed->u4CxtId > pRtmRedIn->u4CxtId)
    {
        return 1;
    }
    if (pRtmRed->u4DestNet < pRtmRedIn->u4DestNet)
    {
        return -1;
    }
    else if (pRtmRed->u4DestNet > pRtmRedIn->u4DestNet)
    {
        return 1;
    }

    if (pRtmRed->u4DestMask < pRtmRedIn->u4DestMask)
    {
        return -1;
    }
    else if (pRtmRed->u4DestMask > pRtmRedIn->u4DestMask)
    {
        return 1;
    }

    if (pRtmRed->u4NextHop < pRtmRedIn->u4NextHop)
    {
        return -1;
    }
    else if (pRtmRed->u4NextHop > pRtmRedIn->u4NextHop)
    {
        return 1;
    }

    if (pRtmRed->u2RtProto < pRtmRedIn->u2RtProto)
    {
        return -1;
    }
    else if (pRtmRed->u2RtProto > pRtmRedIn->u2RtProto)
    {
        return 1;
    }

    return 0;
}

 /*-------------------------------------------------------------------+
 * Function           : RtmRBTreeRedPRTEntryCmp
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : -1  if pRBElem <  pRBElemIn
 *                      1   if pRBElem >  pRBElemIn
 *                      0   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy entries in lexicographic
 * order of the indices of the Redundancy entry.
+-------------------------------------------------------------------*/

INT4
RtmRBTreeRedPRTEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tRtmRedPRTNode     *pRtmRed = pRBElem;
    tRtmRedPRTNode     *pRtmRedIn = pRBElemIn;

    if (pRtmRed->u4CxtId < pRtmRedIn->u4CxtId)
    {
        return -1;
    }
    else if (pRtmRed->u4CxtId > pRtmRedIn->u4CxtId)
    {
        return 1;
    }
    if (pRtmRed->u4DestNet < pRtmRedIn->u4DestNet)
    {
        return -1;
    }
    else if (pRtmRed->u4DestNet > pRtmRedIn->u4DestNet)
    {
        return 1;
    }

    if (pRtmRed->u4DestMask < pRtmRedIn->u4DestMask)
    {
        return -1;
    }
    else if (pRtmRed->u4DestMask > pRtmRedIn->u4DestMask)
    {
        return 1;
    }

    if (pRtmRed->u4NextHop < pRtmRedIn->u4NextHop)
    {
        return -1;
    }
    else if (pRtmRed->u4NextHop > pRtmRedIn->u4NextHop)
    {
        return 1;
    }

    if (pRtmRed->u2RtProto < pRtmRedIn->u2RtProto)
    {
        return -1;
    }
    else if (pRtmRed->u2RtProto > pRtmRedIn->u2RtProto)
    {
        return 1;
    }

    return 0;
}

/*-------------------------------------------------------------------+
 * Function           : RtmRedUpdateRtParams
 *
 * Input(s)           : pRtInfo, u4Context
 *
 * Output(s)          : Updated pRtInfo
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE       
 *
 * Action :
 * This procedure searches for the route node in the temporary RTM RED  
 * table and if a node is found, the route's flag information is updated
 * and the node is removed from RED table.
+-------------------------------------------------------------------*/
INT4
RtmRedUpdateRtParams (tRtInfo * pRtInfo, UINT4 u4Context)
{
    tRtmRedTable        RtmRedNode;
    tRtmRedTable       *pRtmRedNode = NULL;

    RtmRedNode.u4CxtId = u4Context;
    RtmRedNode.u4DestNet = pRtInfo->u4DestNet;
    RtmRedNode.u4DestMask = pRtInfo->u4DestMask;
    RtmRedNode.u4NextHop = pRtInfo->u4NextHop;
    RtmRedNode.u4HwIntfId[0] = pRtInfo->u4HwIntfId[0];
    RtmRedNode.u4HwIntfId[1] = pRtInfo->u4HwIntfId[1];
    RtmRedNode.u2RtProto = pRtInfo->u2RtProto;

    pRtmRedNode = RBTreeGet (gRtmGlobalInfo.RtmRedTable,
                             (tRBElem *) & RtmRedNode);
    if (pRtmRedNode != NULL)
    {
        RBTreeRem (gRtmGlobalInfo.RtmRedTable, pRtmRedNode);
        pRtInfo->u4Flag = pRtmRedNode->u4Flag;
        MemReleaseMemBlock (RTM_DYN_MSG_POOL_ID, (UINT1 *) pRtmRedNode);
        return RTM_SUCCESS;
    }
    return RTM_FAILURE;
}

/*****************************************************************************/
/* Function Name      : RtmRedHandleDynSyncAudit                             */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RtmRedHandleDynSyncAudit (VOID)
{
/*On receiving this event, RTM should execute show cmd and calculate checksum*/
    RtmExecuteCmdAndCalculateChkSum ();
}

/*****************************************************************************/
/* Function Name      : RtmExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
RtmExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_RTM_APP_ID;
    UINT2               u2ChkSum = 0;

    RTM_PROT_UNLOCK ();
    if (RtmGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == RTM_FAILURE)
    {
        RTM_PROT_LOCK ();
        return;
    }

#ifdef L2RED_WANTED
    if (RtmRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == RTM_FAILURE)
    {
        RTM_PROT_LOCK ();
        return;
    }
#else
    UNUSED_PARAM (u2AppId);
#endif
    RTM_PROT_LOCK ();
    return;
}
#endif
