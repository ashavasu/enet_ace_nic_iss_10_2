/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmintf.c,v 1.51 2017/11/14 07:31:13 siva Exp $
 *
 * Description: This File contains the Functios that update the Routing
 *              Table w.r.t to the Modification in the 
 *          Params of Local Interface.
 *
 *******************************************************************/

#include "rtminc.h"
#include "fsmirtlw.h"

PRIVATE VOID
       RtmUtilInsertInRtmInvdRtList (tRtmCxt * pRtmCxt, tRtInfo * pRtInfo);
/*-------------------------------------------------------------------+
 *
 * Function           : RtmLocalIfInitInCxt
 *
 * Input(s)           : pRtmCxt - RTM Context Pointer
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS or IP_FAILURE
 *
 * Action :
 * Initializes the routing table for local interface.
 *
+-------------------------------------------------------------------*/
INT4
RtmLocalIfInitInCxt (tRtmCxt * pRtmCxt)
{
    tRtmRegnId          RegnId;
    INT4                i4RetVal = RTM_FAILURE;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
    RegnId.u2ProtoId = CIDR_LOCAL_ID;
    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    i4RetVal = RtmUtilRegisterInCxt (pRtmCxt, &RegnId, 0, NULL);
    if (i4RetVal == RTM_SUCCESS)
    {
        return IP_SUCCESS;
    }
    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmStaticRtInitInCxt
 *
 * Input(s)           : pRtmCxt - RTM  Context Pointer
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS or IP_FAILURE
 *
 * Action :
 * Initializes the Routing Table for static route.
 *
+-------------------------------------------------------------------*/
INT4
RtmStaticRtInitInCxt (tRtmCxt * pRtmCxt)
{
    tRtmRegnId          RegnId;
    INT4                i4RetVal = RTM_FAILURE;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));

    RegnId.u2ProtoId = CIDR_STATIC_ID;
    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    i4RetVal = RtmUtilRegisterInCxt (pRtmCxt, &RegnId, 0, NULL);
    if (i4RetVal == RTM_SUCCESS)
    {
        return IP_SUCCESS;
    }
    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
  *
  * Function           : RtmChangeRtStatusInCxt
  * *
  * Input(s)           : pRtInfo - Pointer to RouteInfo
  *                      u2IfIndex - Interface Index
  *                      i1Status  - Interface Status
  *                      pRtmCxt - RTM Context Pointer
  *
  * Output(s)          : None
  *
  * Returns            : IP_SUCCESS.
  *
  * Action :
  *  Changes status of all the routes whose gateway is on this interface
 +-------------------------------------------------------------------*/
INT4
RtmChangeRtStatusInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRtInfo,
                        UINT2 u2IfIndex, INT1 i1Status)
{
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tNetIpv4RtInfo      NetRtInfo;
    tRtInfo            *apRtLst[MAX_PATHS];
    UINT2               u2RtCount = 0;

    MEMSET ((UINT1 *) &NetRtInfo, 0, sizeof (tNetIpv4RtInfo));

    /* Alternate route cant be retrieved after deletion.So scan and
     * get all the alternate paths */
    for (pTmpRt = pRtInfo; pTmpRt != NULL; pTmpRt = pTmpRt->pNextAlternatepath)
    {
        if (u2RtCount < MAX_PATHS)
        {
            apRtLst[u2RtCount] = pTmpRt;
            u2RtCount++;
        }
    }

    while (u2RtCount--)
    {
        if (u2RtCount < MAX_PATHS)
        {
            pTmpRt = apRtLst[u2RtCount];
        }
        if ((pTmpRt != NULL) && ((UINT2) (pTmpRt->u4RtIfIndx) == u2IfIndex))
        {
            switch (i1Status)
            {
                case IPIF_OPER_DISABLE:
                    /* Delete the route Entry from Trie and add it 
                     * to temporary route list */
                    RtmCopyRouteInfoInCxt (pRtmCxt->u4ContextId,
                                           pTmpRt, &NetRtInfo);

                    if (MALLOC_IP_ROUTE_ENTRY (pRt) != NULL)
                    {
                        MEMCPY (pRt, pTmpRt, sizeof (tRtInfo));
                        TMO_SLL_Init_Node ((&(pRt->NextRtInfo)));

                        if (pRt->u4NextHop != 0)
                        {
                            pRt->u4RtIfIndx = IPIF_INVALID_INDEX;
                        }
                        RtmUtilInsertInRtmInvdRtList (pRtmCxt, pRt);

                    }
                    RtmUtilIpv4LeakRoute (NETIPV4_DELETE_ROUTE, &NetRtInfo);
                    if (NULL != pRt)
                    {
                        pRt->u4Flag &= (UINT4) (~RTM_RT_HWSTATUS);
                    }
                    KW_FALSEPOSITIVE_FIX (pRt);

                    break;

                case IPIF_OPER_INVALID:
                    /* Delete the directly connected routes and 
                     * add routes having next hop to Route List */
                    RtmCopyRouteInfoInCxt (pRtmCxt->u4ContextId,
                                           pTmpRt, &NetRtInfo);
                    if (pTmpRt->u4NextHop != 0)
                    {
                        if (MALLOC_IP_ROUTE_ENTRY (pRt) != NULL)
                        {
                            MEMCPY (pRt, pTmpRt, sizeof (tRtInfo));
                            TMO_SLL_Init_Node ((&(pRt->NextRtInfo)));
                            pRt->u4RtIfIndx = IPIF_INVALID_INDEX;
                            RtmUtilInsertInRtmInvdRtList (pRtmCxt, pRt);
                        }
                    }
                    KW_FALSEPOSITIVE_FIX (pRt);
                    RtmUtilIpv4LeakRoute (NETIPV4_DELETE_ROUTE, &NetRtInfo);
                    break;

                default:
                    return IP_FAILURE;
            }
        }
    }
    return (IP_SUCCESS);
}

/***************************************************************************/
/*   Function Name   : RtmHandleRtWithInvalidIfIndexInCxt                  */
/*   Description     :This function processes the requests for static route*/
/*                     management.It handle route creation messages.If the */
/*                     route for creation is having no valid interface index*/
/*                     to reach the next hop,those routes are maintained in*/
/*                     seperate Link List.For other casees,update the route*/
/*                     exists in ths list,Else return IP_FAILURE           */
/*                    itializes the Protocol Preference                    */
/*   Input(s)        : pRtmCxt - RTM Context pointer                       */
/*                   : u4Dest - Destination network                        */
/*                     u4Mask - Network Mask to be used for dest network   */
/*                     u4Tos  - Type of service assicated with the route   */
/*                     u4NextHop - NextHop Address of the route            */
/*                     i4Val - Value to be set for the object passed       */
/*                     u1ObjType - Object for which value to bse set       */
/*   Output(s)       : None                                                */
/*   Return Value    : RTM_SUCCESS/RTM_FAILURE                             */
/***************************************************************************/
INT4
RtmHandleRtWithInvalidIfIndexInCxt (tRtmCxt * pRtmCxt, UINT4 u4Dest,
                                    UINT4 u4Mask, UINT4 u4Tos,
                                    UINT4 u4NextHop, INT4 i4Val,
                                    UINT1 u1ObjType)
{
    tRtInfo            *pRt = NULL;
    UINT4               u4Port = 0;
    /* Indicates Trie updation is needed */
    UINT1               u1TrieUpdate = FALSE;
    UINT4               u4IfCxtId = 0;
    INT4                i4LeakFlag = 0;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4RetVal = RTM_SUCCESS;
    tCfaIfInfo          CfaIfInfo;
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

    /* Scan the List and get the route if exists */
    TMO_SLL_Scan ((&pRtmCxt->RtmInvdRtList), pRt, tRtInfo *)
    {
        if ((pRt->u4DestNet == u4Dest) &&
            (pRt->u4DestMask == u4Mask) &&
            (pRt->u4Tos == u4Tos) && (pRt->u4NextHop == u4NextHop))
        {
            break;
        }
    }

    if (u1ObjType == IPFWD_ROUTE_ROW_STATUS)
    {
        if ((pRt == NULL) &&
            (i4Val != IPFWD_CREATE_AND_GO) && (i4Val != IPFWD_CREATE_AND_WAIT))
        {
            return RTM_FAILURE;
        }
        switch (i4Val)
        {
            case IPFWD_CREATE_AND_GO:
                if (MALLOC_IP_ROUTE_ENTRY (pRt) == NULL)
                {
                    return (RTM_NO_ROOM);
                }
                MEMSET (pRt, 0, sizeof (tRtInfo));
                TMO_SLL_Init_Node ((&(pRt->NextRtInfo)));
                pRt->u4DestNet = u4Dest;
                pRt->u4DestMask = u4Mask;
                pRt->u4Tos = u4Tos;
                pRt->u4NextHop = u4NextHop;
                pRt->u4RtIfIndx = IPIF_INVALID_INDEX;
                pRt->u2RtProto = CIDR_STATIC_ID;
                pRt->u1Preference = STATIC_DEFAULT_PREFERENCE;
                if (u4NextHop != 0)
                {
                    pRt->u2RtType = IPROUTE_REMOTE_TYPE;

                    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
                    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

                    RtQuery.u4DestinationIpAddress = u4NextHop;
                    RtQuery.u4DestinationSubnetMask = 0xffffffff;
                    RtQuery.u2AppIds = (UINT2) ALL_ROUTING_PROTOCOL;
                    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
                    RtQuery.u4ContextId = pRtmCxt->u4ContextId;

                    if (RtmUtilIpv4GetRoute (&RtQuery, &NetIpRtInfo)
                        == NETIPV4_SUCCESS)
                    {
                        /* instead of checking local route, check can be made to allow all other
                         * routes also
                         */
                        if (NetIpRtInfo.u2RtProto == CIDR_LOCAL_ID)
                        {
                            u1TrieUpdate = TRUE;
                            pRt->u4RtIfIndx = NetIpRtInfo.u4RtIfIndx;
                        }
                    }
                }
                else
                {
                    pRt->u2RtType = IPROUTE_LOCAL_TYPE;
                }
                pRt->u4RtAge = OsixGetSysUpTime ();
                pRt->u4RtNxtHopAS = 0;
                pRt->i4Metric1 = STATIC_DEFAULT_PREFERENCE;
                pRt->u4RouteTag = 0;
                pRt->u4RowStatus = IPFWD_ACTIVE;
                if (u1TrieUpdate == FALSE)
                {
                    RtmUtilInsertInRtmInvdRtList (pRtmCxt, pRt);
                }
                break;

            case IPFWD_CREATE_AND_WAIT:
                if (MALLOC_IP_ROUTE_ENTRY (pRt) == NULL)
                {
                    return (RTM_NO_ROOM);
                }
                MEMSET (pRt, 0, sizeof (tRtInfo));
                TMO_SLL_Init_Node ((&(pRt->NextRtInfo)));
                pRt->u4DestNet = u4Dest;
                pRt->u4DestMask = u4Mask;
                pRt->u4Tos = u4Tos;
                pRt->u4NextHop = u4NextHop;
                pRt->u4RtIfIndx = IPIF_INVALID_INDEX;
                pRt->u2RtProto = CIDR_STATIC_ID;
                pRt->u1Preference = STATIC_DEFAULT_PREFERENCE;
                if (u4NextHop != 0)
                {
                    pRt->u2RtType = IPROUTE_REMOTE_TYPE;
                }
                else
                {
                    pRt->u2RtType = IPROUTE_LOCAL_TYPE;
                }
                pRt->u4RtAge = OsixGetSysUpTime ();
                pRt->u4RtNxtHopAS = 0;
                pRt->i4Metric1 = STATIC_DEFAULT_PREFERENCE;
                pRt->u4RouteTag = 0;
                pRt->u4RowStatus = IPFWD_NOT_READY;
                RtmUtilInsertInRtmInvdRtList (pRtmCxt, pRt);
                break;

            case IPFWD_ACTIVE:
                if (i4Val == (INT4) pRt->u4RowStatus)
                {
                    return RTM_SUCCESS;
                }
                switch (pRt->u4RowStatus)
                {
                    case IPFWD_NOT_IN_SERVICE:
                    case IPFWD_NOT_READY:
                        pRt->u4RowStatus = IPFWD_ACTIVE;
                        if ((NetIpv4GetCxtId (pRt->u4RtIfIndx,
                                              &u4IfCxtId) == NETIPV4_SUCCESS) &&
                            (u4IfCxtId != pRtmCxt->u4ContextId))
                        {
                            /* If the next-hop interface`s context-id is  
                             * different from the current RTM context-id
                             * then the incoming route is a VRF leak route*/
                            pRt->u1LeakRoute = RTM_VRF_LEAKED_ROUTE;

                            nmhGetFsMIRtmRouteLeakStatus (&i4LeakFlag);
                            if (i4LeakFlag != RTM_VRF_ROUTE_LEAK_ENABLED)
                            {
                                /*Leaked route can be allowed to Program in RTM
                                 *only if vrf-route-leaking is enabled*/
                                return NETIPV4_FAILURE;
                            }

                        }
                        if (u4NextHop == 0)
                        {
                            if (NetIpv4GetIfInfo (pRt->u4RtIfIndx, &NetIpIfInfo)
                                == NETIPV4_SUCCESS)
                            {
                                if ((NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
                                    || (pRt->u1LeakRoute ==
                                        RTM_VRF_LEAKED_ROUTE))
                                {
                                    u1TrieUpdate = TRUE;
                                }

                            }
                        }
                        else
                        {
                            MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
                            MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

                            RtQuery.u4DestinationIpAddress = u4NextHop;
                            RtQuery.u4DestinationSubnetMask = 0xffffffff;
                            RtQuery.u2AppIds = (UINT2) ALL_ROUTING_PROTOCOL;
                            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
                            RtQuery.u4ContextId = pRtmCxt->u4ContextId;

                            if (RtmUtilIpv4GetRoute (&RtQuery, &NetIpRtInfo)
                                == NETIPV4_SUCCESS)
                            {

                                if (NetIpv4GetCfaIfIndexFromPort
                                    (NetIpRtInfo.u4RtIfIndx,
                                     &u4Port) == NETIPV4_FAILURE)
                                {
                                    return NETIPV4_FAILURE;
                                }

                                if (CfaGetIfInfo (u4Port, &CfaIfInfo) ==
                                    CFA_FAILURE)
                                {
                                    return NETIPV4_FAILURE;
                                }

                                /* For unnumbered interface, eventhough route proto is
                                 * not local, ideally it should not be ignored by route
                                 * table manager. So allow unnumbered interface for Trie updation
                                 */

                                if ((NetIpRtInfo.u2RtProto == CIDR_LOCAL_ID)
                                    || (CfaIfInfo.u4UnnumAssocIPIf != 0)
                                    || (pRt->u1LeakRoute ==
                                        RTM_VRF_LEAKED_ROUTE))
                                {
                                    u1TrieUpdate = TRUE;
                                    pRt->u4RtIfIndx = NetIpRtInfo.u4RtIfIndx;
                                }
                            }
                        }
                        if (u1TrieUpdate == TRUE)
                        {
                            /* Delete the route from the list */
                            TMO_SLL_Delete (&(pRtmCxt->RtmInvdRtList),
                                            &(pRt->NextRtInfo));
                            if (pRt->u2RtProto == CIDR_STATIC_ID)
                            {
                                gRtmGlobalInfo.u4StaticRts--;
                            }

                        }
                        break;

                    default:
                        return RTM_FAILURE;
                }
                break;

            case IPFWD_NOT_IN_SERVICE:
                switch (pRt->u4RowStatus)
                {
                    case IPFWD_ACTIVE:
                        pRt->u4RowStatus = IPFWD_NOT_IN_SERVICE;
                        break;

                    case IPFWD_NOT_IN_SERVICE:
                        break;

                    default:
                        return RTM_FAILURE;
                }
                break;

            case IPFWD_DESTROY:
                /* Delete the route from the list */
                TMO_SLL_Delete (&(pRtmCxt->RtmInvdRtList), &(pRt->NextRtInfo));
                if (pRt->u2RtProto == CIDR_STATIC_ID)
                {
                    gRtmGlobalInfo.u4StaticRts--;
                }

                IP_RT_FREE (pRt);
                break;
            default:
                break;
        }
        i4RetVal = RTM_SUCCESS;
    }
    else
    {
        if (pRt == NULL)
        {
            return RTM_FAILURE;
        }
        switch (u1ObjType)
        {
            case IPFWD_ROUTE_IFINDEX:

                /* Get the Port no from IfIndex */
                if (NetIpv4GetPortFromIfIndex ((UINT4) i4Val, &u4Port)
                    != NETIPV4_SUCCESS)
                {
                    return RTM_FAILURE;
                }
                pRt->u4RtIfIndx = u4Port;
                break;

            case IPFWD_ROUTE_TYPE:
                pRt->u2RtType = (UINT2) i4Val;
                break;

            case IPFWD_ROUTE_NEXTHOPAS:
                pRt->u4RtNxtHopAS = (UINT4) i4Val;
                break;

            case IPFWD_REDIS_CTRL_FLAG:
                /* Setting redistribution control flag */
                if (i4Val == IP_DONT_REDIS_ROUTE)
                {
                    pRt->u1BitMask |= RTM_PRIV_RT_MASK;
                }
                else
                {
                    pRt->u1BitMask =
                        (UINT1) (pRt->u1BitMask & (~RTM_PRIV_RT_MASK));
                }
                break;

            case IPFWD_ROUTE_METRIC1:
                pRt->i4Metric1 = i4Val;
                break;

            case IPFWD_ROUTE_PREFERENCE:
                pRt->u1Preference = (UINT1) i4Val;
                break;

            default:
                return RTM_FAILURE;
        }
        i4RetVal = RTM_SUCCESS;
    }

    if (u1TrieUpdate == TRUE)
    {

        RtmCopyRouteInfoInCxt (pRtmCxt->u4ContextId, pRt, &NetIpRtInfo);

        if (RtmUtilIpv4LeakRoute (NETIPV4_ADD_ROUTE,
                                  &NetIpRtInfo) == NETIPV4_FAILURE)
        {
            i4RetVal = RTM_FAILURE;
        }
        IP_RT_FREE (pRt);
        pRt = NULL;
    }
    return i4RetVal;
}

/**************************************************************************/
/*   Function Name   : RtmUtilCheckRtExistsWithInvalidIfIndex             */
/*   Description     : This function fills  the route exists in temporary */
/*                     List.Else return FALSE                             */
/*   Input(s)        : pRtmCxt - RTM context pointer                      */
/*                   : u4Dest - Destination network                       */
/*                     u4Mask - Network Mask to be used for dest network  */
/*                     u4Tos  - Type of service assicated with the route  */
/*                     u4NextHop - NextHop Address of the route           */
/*   Output(s)       : pRtFound - Route found in temporary route list     */
/*   Return Value    : TRUE/FALSE                                         */
/**************************************************************************/
UINT1
RtmUtilCheckRtExistsWithInvalidIfIndex (tRtmCxt * pRtmCxt, UINT4 u4Dest,
                                        UINT4 u4Mask, UINT4 u4Tos,
                                        UINT4 u4NextHop, tRtInfo * pRtFound)
{
    tRtInfo            *pRt = NULL;
    UINT1               u1RtFound = FALSE;

    /* Scan the temporary route list.If entry exists, Copy the entry founf
     * Else return FALSE. */
    TMO_SLL_Scan (&(pRtmCxt->RtmInvdRtList), pRt, tRtInfo *)
    {
        if ((pRt->u4DestNet == u4Dest) &&
            (pRt->u4DestMask == u4Mask) &&
            (pRt->u4Tos == u4Tos) && (pRt->u4NextHop == u4NextHop))
        {
            u1RtFound = TRUE;
            MEMCPY (pRtFound, pRt, sizeof (tRtInfo));
            break;
        }
    }
    return u1RtFound;
}

/**************************************************************************/
/*   Function Name   : RtmHandleOperUpIndicationInCxt                     */
/*   Description     : This function act on statuc routes having no local */
/*                    for reaching nexthop for Oper up indication         */
/*   Input(s)        : pRtmCxt  - Rtm Context Pointer                     */
/*                   : u2Index  - IP Interface Index                      */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
RtmHandleOperUpIndicationInCxt (tRtmCxt * pRtmCxt, UINT2 u2Index)
{
    UINT4               u4IpAddr = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4Port = 0;
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTempRt = NULL;
    tNetIpv4RtInfo      NetIpRtInfo;
    tNetIpv4IfInfo      IpIntfInfo;
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&IpIntfInfo, 0, sizeof (tNetIpv4IfInfo));

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (NetIpv4GetIfInfo (u2Index, &IpIntfInfo) == NETIPV4_SUCCESS)
    {
        if (NetIpv4GetCfaIfIndexFromPort
            (IpIntfInfo.u4IfIndex, &u4Port) == NETIPV4_SUCCESS)
        {
            if (CfaGetIfInfo (u4Port, &CfaIfInfo) == CFA_SUCCESS)
            {
                /* If interface IP ADDRESS zero then no need to
                 * check valid nexthop route list */
                /* Also unnumbered interface will have IP ADDRESS zero,
                 * Hence, those interfaces needs to be taken care */
                if ((IpIntfInfo.u4Addr != IP_ZERO)
                    || (CfaIfInfo.u4UnnumAssocIPIf != 0))
                {

                    /* Scan the temporary route list */
                    RTM_SLL_DYN_Scan (&(pRtmCxt->RtmInvdRtList), pRt, pTempRt,
                                      tRtInfo *)
                    {
                        if (pRt->u4NextHop == 0)
                        {
                            if (pRt->u4RtIfIndx == u2Index)
                            {
                                /* Delete the route from the list and add it to RTM Trie */
                                TMO_SLL_Delete (&(pRtmCxt->RtmInvdRtList),
                                                &(pRt->NextRtInfo));
                                if (pRt->u2RtProto == CIDR_STATIC_ID)
                                {
                                    gRtmGlobalInfo.u4StaticRts--;
                                }

                                RtmCopyRouteInfoInCxt (pRtmCxt->u4ContextId,
                                                       pRt, &NetIpRtInfo);
                                RtmUtilIpv4LeakRoute (NETIPV4_ADD_ROUTE,
                                                      &NetIpRtInfo);
                                IP_RT_FREE (pRt);
                            }
                        }
                        else
                        {
                            MEMSET (&IpIntfInfo, 0, sizeof (tNetIpv4IfInfo));
                            if ((NetIpv4GetIfInfo (u2Index, &IpIntfInfo) ==
                                 NETIPV4_SUCCESS)
                                && (IpIntfInfo.u4Addr != IP_ZERO))
                            {
                                u4IpAddr = IpIntfInfo.u4Addr;
                                u4NetMask = IpIntfInfo.u4NetMask;
                                do
                                {
                                    /* Add the routes having network same as that of interface 
                                     * to RTM Trie */
                                    if ((u4IpAddr & u4NetMask) ==
                                        (pRt->u4NextHop & u4NetMask))
                                    {
                                        /* If the route is having gateway same as that of interface
                                         * IP Addres,don't add to RTM Trie */
                                        if (u4IpAddr != pRt->u4NextHop)
                                        {
                                            pRt->u4RtIfIndx = (UINT4) u2Index;
                                            TMO_SLL_Delete (&
                                                            (pRtmCxt->
                                                             RtmInvdRtList),
                                                            &(pRt->NextRtInfo));
                                            if (pRt->u2RtProto ==
                                                CIDR_STATIC_ID)
                                            {
                                                gRtmGlobalInfo.u4StaticRts--;
                                            }

                                            RtmCopyRouteInfoInCxt (pRtmCxt->
                                                                   u4ContextId,
                                                                   pRt,
                                                                   &NetIpRtInfo);
                                            RtmUtilIpv4LeakRoute
                                                (NETIPV4_ADD_ROUTE,
                                                 &NetIpRtInfo);
                                            IP_RT_FREE (pRt);
                                            break;
                                        }
                                    }
                                }
                                while (NetIpv4GetNextSecondaryAddress
                                       (u2Index, u4IpAddr, &u4IpAddr,
                                        &u4NetMask) == NETIPV4_SUCCESS);
                            }
                        }        /* END OF IF ELSE */
                    }            /* END OF DYN SCAN */
                }                /* END OF IF (UNNUM INTF) */
            }                    /* END OF IF (CfaGetIfInfo == CFA_SUCCESS) */
        }                        /* END OF IF (NetIpv4GetCfaIfIndexFromPort == NETIPV4_SUCCESS) */
    }                            /* END OF IF (NetIpv4GetIfInfo == NETIPV4_SUCCESS) */
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmIntfNotifyRoute 
 *
 * Input(s)           : Post Local route message addition to RTM
 *
 * Output(s)          : None.
 *
 * Returns            : -1 on failure else 0
 *
 * Action             : This routine is called whenever a new IP address is
 *                      available in CFA
+-------------------------------------------------------------------*/
INT4
RtmIntfNotifyRoute (UINT1 u1State, tRtmRouteUpdateInfo * pRtmRouteUpdateInfo)
{
    tOsixMsg           *pRtmMsg = NULL;
    tRtmMsgHdr         *pRtmMsgHdr = NULL;

    if ((pRtmMsg =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tRtmRouteUpdateInfo),
                                       0)) == NULL)
    {
        return IP_FAILURE;
    }

    pRtmMsgHdr = (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = u1State;
    pRtmMsgHdr->RegnId.u4ContextId = pRtmRouteUpdateInfo->u4ContextId;

    if (IP_COPY_TO_BUF (pRtmMsg, pRtmRouteUpdateInfo,
                        0, sizeof (tRtmRouteUpdateInfo)) == IP_BUF_FAILURE)
    {

        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return IP_FAILURE;
    }

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) == RTM_FAILURE)
    {
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmUpdateLocalRouteInCxt
 *
 * Input(s)           : u1Status - RTM_ADD_ROUTE/RTM_DELETE_ROUTE
 *                    : u4ContextId - RTM context ID             
 *                    : u4NetWork - Route dest Network
 *                    : u4NetMask - Route dest NetMask
 *                    : u4IfIndex - IfIndex
 *
 * Output(s)          : None.
 *
 * Returns            : -1 on failure else 0
 *
 * Action             : RTM calls this routine for addition/deletion 
 *                      of local route to its IP Routing Table.
 *         
+-------------------------------------------------------------------*/
INT4
RtmUpdateLocalRouteInCxt (UINT1 u1Status, UINT4 u4ContextId, UINT4 u4NetWork,
                          UINT4 u4NetMask, UINT4 u4IfIndex)
{
    tNetIpv4RtInfo      NetRtInfo;
    INT4                i4OutCome = 0;
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET ((UINT1 *) &NetRtInfo, 0, sizeof (tNetIpv4RtInfo));

    NetRtInfo.u4ContextId = u4ContextId;
    NetRtInfo.u4DestNet = u4NetWork;

    NetRtInfo.u4DestMask = u4NetMask;
    /* To avoid addition of local route with dest 0 */
    if (NetRtInfo.u4DestNet == 0)
    {
        return NETIPV4_FAILURE;
    }
    NetRtInfo.u2RtType = IPROUTE_LOCAL_TYPE;
    NetRtInfo.u4RtNxtHopAs = 0;
    NetRtInfo.u2RtProto = CIDR_LOCAL_ID;
    NetRtInfo.u4RouteTag = 0;
    NetRtInfo.i4Metric1 = (INT4) IP_LOCAL_RT_METRIC;
    NetRtInfo.u4NextHop = 0;
    NetRtInfo.u4RtIfIndx = u4IfIndex;
    if (u1Status == RTM_ADD_ROUTE)
    {
        /*Since the local route should not be added in active state
         *when oper state is disable so the current oper state is 
         *is verified and rowstatus is set according to that.*/
        if ((NetIpv4GetIfInfo (u4IfIndex, &NetIpIfInfo) == NETIPV4_SUCCESS) &&
            (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE))
        {
            NetRtInfo.u4RowStatus = IPFWD_ACTIVE;
        }
        else
        {
            NetRtInfo.u4RowStatus = IPFWD_NOT_READY;
        }
    }
    else if (u1Status == RTM_DELETE_ROUTE)
    {
        NetRtInfo.u4RowStatus = IPFWD_DESTROY;
    }
    NetRtInfo.u2ChgBit = IP_BIT_ALL;

    i4OutCome = RtmUtilIpv4LeakRoute (u1Status, &NetRtInfo);
    if (i4OutCome == IP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/************************************************************************
 *  Function Name   : RtmUtilInsertInRtmInvdRtList
 *  Description     : An util to insert the given route 
 *                    in RtmInvdRtList in correct position so that 
                      the list is in sorted order.
 *  Input           : pRtInfo -  route to be inserted
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
RtmUtilInsertInRtmInvdRtList (tRtmCxt * pRtmCxt, tRtInfo * pRtInfo)
{
    tRtInfo            *pTmpRtInfo = NULL;
    tRtInfo            *pPrevRtInfo = NULL;
    tNetIpv4RtInfo      RtInfo;
    tNetIpv4RtInfo      TmpRtInfo;

    MEMSET (&RtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&TmpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    RtInfo.u4DestNet = pRtInfo->u4DestNet;
    RtInfo.u4DestMask = pRtInfo->u4DestMask;
    RtInfo.u4Tos = pRtInfo->u4Tos;
    RtInfo.u4NextHop = pRtInfo->u4NextHop;

    TMO_SLL_Scan (&(pRtmCxt->RtmInvdRtList), pTmpRtInfo, tRtInfo *)
    {
        if (pTmpRtInfo != NULL)
        {

            TmpRtInfo.u4DestNet = pTmpRtInfo->u4DestNet;
            TmpRtInfo.u4DestMask = pTmpRtInfo->u4DestMask;
            TmpRtInfo.u4Tos = pTmpRtInfo->u4Tos;
            TmpRtInfo.u4NextHop = pTmpRtInfo->u4NextHop;

            if (RtmUtilCheckIsRouteGreater (&TmpRtInfo, &RtInfo) == IP_SUCCESS)
            {
                break;
            }
            else
            {
                pPrevRtInfo = pTmpRtInfo;
            }
        }
    }
    if (pPrevRtInfo == NULL)
    {
        /* Node to be inserted in first position */
        TMO_SLL_Insert (&(pRtmCxt->RtmInvdRtList), NULL,
                        (tTMO_SLL_NODE *) & (pRtInfo->NextRtInfo));
    }
    else if (pTmpRtInfo == NULL)
    {
        /* Node to be inserted in last position */
        TMO_SLL_Insert (&(pRtmCxt->RtmInvdRtList),
                        (tTMO_SLL_NODE *) & (pPrevRtInfo->
                                             NextRtInfo),
                        (tTMO_SLL_NODE *) & (pRtInfo->NextRtInfo));
    }
    else
    {
        /* Node to be inserted in middle */
        TMO_SLL_Insert_In_Middle (&(pRtmCxt->RtmInvdRtList),
                                  (tTMO_SLL_NODE *) &
                                  (pPrevRtInfo->NextRtInfo),
                                  (tTMO_SLL_NODE *) &
                                  (pRtInfo->NextRtInfo),
                                  (tTMO_SLL_NODE *) & (pTmpRtInfo->NextRtInfo));
    }
    if (pRtInfo->u2RtProto == CIDR_STATIC_ID)
    {
        gRtmGlobalInfo.u4StaticRts++;
    }
}

/************************************************************************
 *  Function Name   : RtmUtilRetryNpPrgForFailedRoute
 *  Description     : An util to retry rogramming the given route 
                      in hardware.
 *  Input           : pRtInfo -  route to be inserted
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
INT4
RtmUtilRetryNpPrgForFailedRoute (tRtmFrtInfo * pRtmFrtInfo)
{
    INT4                i4OutCome = RTM_FAILURE;
#ifdef NPAPI_WANTED
    tRtmCxt            *pRtmCxt = NULL;
    UINT1               bu1TblFull = FNP_FALSE;
    UINT4               au4Indx[IP_TWO];
    UINT4               u4CfaIfIndex = 0;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tCfaIfInfo          IfInfo;
    tFsNpNextHopInfo    nextHopInfo;
    tRtInfo            *pTmpRt = NULL;

    pRtmCxt = UtilRtmGetCxt (pRtmFrtInfo->u4CxtId);
    if (pRtmCxt == NULL)
    {
        i4OutCome = RTM_FAILURE;
        return i4OutCome;
    }
    MEMSET (&OutParams, 0, sizeof (tOutputParams));
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    au4Indx[0] =
        IP_HTONL ((pRtmFrtInfo->u4DestNet) & (pRtmFrtInfo->u4DestMask));
    au4Indx[1] = IP_HTONL (pRtmFrtInfo->u4DestMask);

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = (INT1) (pRtmFrtInfo->u2RtProto - 1);
    InParams.Key.pKey = (UINT1 *) au4Indx;

    OutParams.Key.pKey = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        /* Route entry is available, check for the matching route entry 
         * based on nexthop and tos, by traversing through all 
         * alternate paths. */

        pTmpRt = (tRtInfo *) OutParams.pAppSpecInfo;

        /* Next-hop identifies the exact route */

        while (pTmpRt != NULL)
        {
            if ((pTmpRt)->u4NextHop == pRtmFrtInfo->u4NextHop)
            {

                MEMSET (&nextHopInfo, 0, sizeof (tFsNpNextHopInfo));

                /* Get the CFA Interface Index from Ip Port No */
                if (NetIpv4GetCfaIfIndexFromPort ((pTmpRt)->u4RtIfIndx,
                                                  &u4CfaIfIndex) ==
                    NETIPV4_FAILURE)
                {
                    i4OutCome = RTM_FAILURE;
                    break;
                }
                nextHopInfo.u4NextHopGt = (pTmpRt)->u4NextHop;
                nextHopInfo.u4IfIndex = u4CfaIfIndex;

                if ((((pTmpRt)->u4Flag & RTM_RT_REACHABLE) == 0) &&
                    (((pTmpRt)->u4Flag & RTM_RT_IN_PRT) == 0))
                {
                    nextHopInfo.u1RtType = FNP_RT_LOCAL;
                }

                nextHopInfo.u1RtCount = pRtmFrtInfo->u1RtCount;

                if (CfaGetIfInfo (nextHopInfo.u4IfIndex, &IfInfo) ==
                    CFA_FAILURE)
                {
                    i4OutCome = RTM_FAILURE;
                    break;
                }

                if (CFA_PPP == IfInfo.u1IfType)
                {
                    nextHopInfo.u1IfType = CFA_PPP;
                    CfaGetLLIfIndex (u4CfaIfIndex, &nextHopInfo.u4LLIfIndex);
                }

                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId (nextHopInfo.u4IfIndex,
                                   &(nextHopInfo.u2VlanId)) == CFA_FAILURE))
                {
                    i4OutCome = RTM_FAILURE;
                    break;
                }

                if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    pTmpRt->u4Flag &= (UINT4) ~RTM_RT_HWSTATUS;
                    RtmRedAddDynamicInfo (pTmpRt, pRtmFrtInfo->u4CxtId);
                    RtmRedSendDynamicInfo (NETIPV4_ADD_ROUTE);
                }
                if (IpFsNpIpv4UcAddRoute
                    (pRtmFrtInfo->u4CxtId, (pTmpRt)->u4DestNet,
                     (pTmpRt)->u4DestMask,
#ifndef NPSIM_WANTED
                     &nextHopInfo,
#else
                     nextHopInfo,
#endif
                     &bu1TblFull) == FNP_FAILURE)
                {
                    i4OutCome = RTM_FAILURE;
                    break;
                }
                (pTmpRt)->u4HwIntfId[0] = nextHopInfo.u4HwIntfId[0];

                /* The HwStatus flag is used only when Async NP Call is used. 
                 * The HwStatus is set as NotPresent at first and when the NP_CALL is
                 * success, then the flag is updated to RTM_RT_HWSTATUS. 
                 * This state is synced with the standby node through Dynamic syncup */

                if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
                {
                    (pTmpRt)->u4Flag |= RTM_RT_HWSTATUS;
                    RtmRedAddDynamicInfo ((pTmpRt), pRtmFrtInfo->u4CxtId);
                    RtmRedSendDynamicInfo (NETIPV4_ADD_ROUTE);
                }

                if (bu1TblFull == FNP_TRUE)
                {
                    /* OverFlow in FIB , Update the global Flag */
                    gu1FIBFlag = RTM_FIB_FULL;
                }
                if (((pTmpRt)->u4Flag & RTM_RT_HWSTATUS))
                {
#ifdef LNXIP4_WANTED
                    RtmNetIpv4LeakRoute (NETIPV4_ADD_ROUTE, pTmpRt);
#endif
                    /* Function that add the route to NP and kernal */
                    RtmNotifyRtToAppsInCxt (pRtmCxt, pTmpRt, NULL,
                                            NETIPV4_ADD_ROUTE);
                }
                i4OutCome = RTM_SUCCESS;
                break;
            }
            pTmpRt = (pTmpRt)->pNextAlternatepath;
        }
    }
#else
    UNUSED_PARAM (pRtmFrtInfo);
    i4OutCome = RTM_SUCCESS;
#endif

    return i4OutCome;
}
