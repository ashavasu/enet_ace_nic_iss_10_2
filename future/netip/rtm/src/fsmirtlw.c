/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmirtlw.c,v 1.13 2016/10/15 09:18:03 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "rrdcli.h"
#include "fsmirtcli.h"
#include "rtminc.h"
#include  "fsmirtlw.h"
#include  "fsrtmlw.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRtmThrottleLimit
 Input       :  The Indices

                The Object 
                retValFsMIRtmThrottleLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmThrottleLimit (UINT4 *pu4RetValFsMIRtmThrottleLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsRtmThrottleLimit (pu4RetValFsMIRtmThrottleLimit);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIEcmpAcrossProtocolAdminStatus
 Input       :  The Indices

                The Object
                retValFsMIEcmpAcrossProtocolAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIEcmpAcrossProtocolAdminStatus(INT4 *pi4RetValFsMIEcmpAcrossProtocolAdminStatus)
{

    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsEcmpAcrossProtocolAdminStatus (pi4RetValFsMIEcmpAcrossProtocolAdminStatus);
    return i1Return;
}

/****************************************************************************
* Function    :  nmhGetFsMIRtmRouteLeakStatus
* Input       :  The Indices
*	 
*                The Object
*                retValFsMIRtmRouteLeakStatus
* Output      :  The Get Low Lev Routine Take the Indices &
*                store the Value requested in the Return val.
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1 nmhGetFsMIRtmRouteLeakStatus(INT4 *pi4RetValFsMIRtmRouteLeakStatus)
{
	 
    INT1                i1Return = SNMP_FAILURE;
 
    i1Return = nmhGetFsRtmRouteLeakStatus (pi4RetValFsMIRtmRouteLeakStatus);
    return i1Return;
 
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRtmThrottleLimit
 Input       :  The Indices

                The Object 
                setValFsMIRtmThrottleLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtmThrottleLimit (UINT4 u4SetValFsMIRtmThrottleLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhSetFsRtmThrottleLimit (u4SetValFsMIRtmThrottleLimit);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIEcmpAcrossProtocolAdminStatus
 Input       :  The Indices

                The Object
                setValFsMIEcmpAcrossProtocolAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIEcmpAcrossProtocolAdminStatus(INT4 i4SetValFsMIEcmpAcrossProtocolAdminStatus)
{
    INT1 i1Return = 0;

    i1Return  = nmhSetFsEcmpAcrossProtocolAdminStatus (i4SetValFsMIEcmpAcrossProtocolAdminStatus);
    return i1Return;
}

/****************************************************************************
*Function    :  nmhSetFsMIRtmRouteLeakStatus
*Input       :  The Indices
* 											
*               The Object
*               setValFsMIRtmRouteLeakStatus
*Output      :  The Set Low Lev Routine Take the Indices &
*               Sets the Value accordingly.
*Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1 nmhSetFsMIRtmRouteLeakStatus(INT4 i4SetValFsMIRtmRouteLeakStatus)
{
    INT1 i1Return = 0;
 
    i1Return  = nmhSetFsRtmRouteLeakStatus (i4SetValFsMIRtmRouteLeakStatus);
    return i1Return;
 
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmThrottleLimit
 Input       :  The Indices

                The Object 
                testValFsMIRtmThrottleLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtmThrottleLimit (UINT4 *pu4ErrorCode,
                               UINT4 u4TestValFsMIRtmThrottleLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsRtmThrottleLimit (pu4ErrorCode,
                                     u4TestValFsMIRtmThrottleLimit);
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIEcmpAcrossProtocolAdminStatus
 Input       :  The Indices

                The Object
                testValFsMIEcmpAcrossProtocolAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIEcmpAcrossProtocolAdminStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFsMIEcmpAcrossProtocolAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return =
        nmhTestv2FsEcmpAcrossProtocolAdminStatus (pu4ErrorCode,
                                     i4TestValFsMIEcmpAcrossProtocolAdminStatus);
    return i1Return;
}

/****************************************************************************
*Function    :  nmhTestv2FsMIRtmRouteLeakStatus
* Input       :  The Indices
*	 
*                The Object
*                testValFsMIRtmRouteLeakStatus
*Output      :  The Test Low Lev Routine Take the Indices &
*               Test whether that Value is Valid Input for Set.
*               Stores the value of error code in the Return val
*Error Codes :  The following error codes are to be returned
*               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
*               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
*               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
*               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
*               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
*Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtmRouteLeakStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFsMIRtmRouteLeakStatus)
{
    INT1                i1Return = SNMP_FAILURE;
 
    i1Return =
        nmhTestv2FsRtmRouteLeakStatus (pu4ErrorCode,
                                     i4TestValFsMIRtmRouteLeakStatus);
    return i1Return;
 
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRtmThrottleLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRtmThrottleLimit (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIEcmpAcrossProtocolAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/   
INT1 nmhDepv2FsMIEcmpAcrossProtocolAdminStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIRtmRouteLeakStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
******************************************************************************/
INT1 nmhDepv2FsMIRtmRouteLeakStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
       UNUSED_PARAM(pu4ErrorCode);
       UNUSED_PARAM(pSnmpIndexList);
       UNUSED_PARAM(pSnmpVarBind);
return SNMP_SUCCESS;
}
/* LOW LEVEL Routines for Table : FsMIRtmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRtmTable
 Input       :  The Indices
                FsMIRtmContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRtmTable (INT4 i4FsMIRtmContextId)
{
    INT4                i4MaxCxts = 0;

    i4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         (INT4) FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIRtmContextId < RTM_DEFAULT_CXT_ID) ||
        (i4FsMIRtmContextId >= i4MaxCxts))
    {
        return SNMP_FAILURE;
    }

    if (UtilRtmVcmIsVcExist ((UINT4) i4FsMIRtmContextId) == RTM_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (UtilRtmIsValidCxtId ((UINT4) i4FsMIRtmContextId) == RTM_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRtmTable
 Input       :  The Indices
                FsMIRtmContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRtmTable (INT4 *pi4FsMIRtmContextId)
{
    if (UtilRtmGetFirstCxtId ((UINT4 *) pi4FsMIRtmContextId) == RTM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRtmTable
 Input       :  The Indices
                FsMIRtmContextId
                nextFsMIRtmContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRtmTable (INT4 i4FsMIRtmContextId,
                             INT4 *pi4NextFsMIRtmContextId)
{
    if (UtilRtmGetNextCxtId ((UINT4) i4FsMIRtmContextId,
                             (UINT4 *) pi4NextFsMIRtmContextId) == RTM_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRrdRouterId
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                retValFsMIRrdRouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdRouterId (INT4 i4FsMIRtmContextId, UINT4 *pu4RetValFsMIRrdRouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrdRouterId (pu4RetValFsMIRrdRouterId);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdFilterByOspfTag
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                retValFsMIRrdFilterByOspfTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdFilterByOspfTag (INT4 i4FsMIRtmContextId,
                              INT4 *pi4RetValFsMIRrdFilterByOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrdFilterByOspfTag (pi4RetValFsMIRrdFilterByOspfTag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdFilterOspfTag
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                retValFsMIRrdFilterOspfTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdFilterOspfTag (INT4 i4FsMIRtmContextId,
                            INT4 *pi4RetValFsMIRrdFilterOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrdFilterOspfTag (pi4RetValFsMIRrdFilterOspfTag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdFilterOspfTagMask
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                retValFsMIRrdFilterOspfTagMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdFilterOspfTagMask (INT4 i4FsMIRtmContextId,
                                INT4 *pi4RetValFsMIRrdFilterOspfTagMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrdFilterOspfTagMask (pi4RetValFsMIRrdFilterOspfTagMask);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdRouterASNumber
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                retValFsMIRrdRouterASNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdRouterASNumber (INT4 i4FsMIRtmContextId,
                             INT4 *pi4RetValFsMIRrdRouterASNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrdRouterASNumber (pi4RetValFsMIRrdRouterASNumber);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdAdminStatus
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                retValFsMIRrdAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdAdminStatus (INT4 i4FsMIRtmContextId,
                          INT4 *pi4RetValFsMIRrdAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRrdAdminStatus (pi4RetValFsMIRrdAdminStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdForce
 Input       :  The Indices
                FsMIRtmContextId

                The Object
                retValFsMIRrdForce
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdForce (INT4 i4FsMIRtmContextId, INT4 *pi4RetValFsMIRrdForce)
{
    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt ((UINT4) i4FsMIRtmContextId);
    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMIRrdForce = (INT4) pRtmCxt->u1RrdForceStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIRTMIpStaticRouteDistance
 Input       :  The Indices
                FsMIRtmContextId

                The Object
                retValFsMIRTMIpStaticRouteDistance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIRTMIpStaticRouteDistance(INT4 i4FsMIRtmContextId , 
                                   INT4 *pi4RetValFsMIRTMIpStaticRouteDistance)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFsRtmIpStaticRouteDistance (pi4RetValFsMIRTMIpStaticRouteDistance);
    UtilRtmResetContext ();
    return i1Return;
}


/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRrdRouterId
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                setValFsMIRrdRouterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdRouterId (INT4 i4FsMIRtmContextId, UINT4 u4SetValFsMIRrdRouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrdRouterId (u4SetValFsMIRrdRouterId);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdFilterByOspfTag
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                setValFsMIRrdFilterByOspfTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdFilterByOspfTag (INT4 i4FsMIRtmContextId,
                              INT4 i4SetValFsMIRrdFilterByOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrdFilterByOspfTag (i4SetValFsMIRrdFilterByOspfTag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdFilterOspfTag
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                setValFsMIRrdFilterOspfTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdFilterOspfTag (INT4 i4FsMIRtmContextId,
                            INT4 i4SetValFsMIRrdFilterOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrdFilterOspfTag (i4SetValFsMIRrdFilterOspfTag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdFilterOspfTagMask
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                setValFsMIRrdFilterOspfTagMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdFilterOspfTagMask (INT4 i4FsMIRtmContextId,
                                INT4 i4SetValFsMIRrdFilterOspfTagMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrdFilterOspfTagMask (i4SetValFsMIRrdFilterOspfTagMask);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdRouterASNumber
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                setValFsMIRrdRouterASNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdRouterASNumber (INT4 i4FsMIRtmContextId,
                             INT4 i4SetValFsMIRrdRouterASNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrdRouterASNumber (i4SetValFsMIRrdRouterASNumber);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdAdminStatus
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                setValFsMIRrdAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdAdminStatus (INT4 i4FsMIRtmContextId,
                          INT4 i4SetValFsMIRrdAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRrdAdminStatus (i4SetValFsMIRrdAdminStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdForce
 Input       :  The Indices
                FsMIRtmContextId

                The Object
                setValFsMIRrdForce
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdForce (INT4 i4FsMIRtmContextId, INT4 i4SetValFsMIRrdForce)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
    pRtmCxt = UtilRtmGetCxt ((UINT4) i4FsMIRtmContextId);
    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRtmCxt->u1RrdAdminStatus != RTM_ADMIN_STATUS_ENABLED)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsMIRrdForce == RTM_FORCE_DISABLE)
    {
        return SNMP_SUCCESS;
    }

    pRtmCxt->u1RrdForceStatus = (UINT1) i4SetValFsMIRrdForce;

    if ((pRtmCxt->u4RouterId != pRtmCxt->u4TempRouterId) &&
        (pRtmCxt->u4TempRouterId != 0))
    {
        pRtmCxt->u4RouterId = pRtmCxt->u4TempRouterId;
    }

    if ((pRtmCxt->u2AsNumber != pRtmCxt->u2TempAsNumber) &&
        (pRtmCxt->u2TempAsNumber != 0))
    {
        pRtmCxt->u2AsNumber = pRtmCxt->u2TempAsNumber;
    }
    /* check whether any RP's had registered with RTM */
    /* If so start sending the Registration ACK messages to them. */

    for (u2Index = pRtmCxt->u2RtmRtStartIndex;
         u2Index < MAX_ROUTING_PROTOCOLS;
         u2Index = pRtmCxt->aRtmRegnTable[u2Index].u2NextRegId)
    {
        RegnId.u2ProtoId = pRtmCxt->aRtmRegnTable[u2Index].u2RoutingProtocolId;
        RegnId.u4ContextId = pRtmCxt->u4ContextId;
        RtmSendAckToRpInCxt (pRtmCxt, &RegnId);

    }
    pRtmCxt->u1RrdForceStatus = (UINT1) RTM_FORCE_DISABLE;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMIRTMIpStaticRouteDistance
 Input       :  The Indices
                FsMIRtmContextId

                The Object
                setValFsMIRTMIpStaticRouteDistance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIRTMIpStaticRouteDistance(INT4 i4FsMIRtmContextId , 
                                   INT4 i4SetValFsMIRTMIpStaticRouteDistance)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFsRtmIpStaticRouteDistance (i4SetValFsMIRTMIpStaticRouteDistance);
    UtilRtmResetContext ();
    return i1Return;
}


/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdRouterId
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                testValFsMIRrdRouterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdRouterId (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                          UINT4 u4TestValFsMIRrdRouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FsRrdRouterId (pu4ErrorCode, u4TestValFsMIRrdRouterId);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdFilterByOspfTag
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                testValFsMIRrdFilterByOspfTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdFilterByOspfTag (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                                 INT4 i4TestValFsMIRrdFilterByOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdFilterByOspfTag (pu4ErrorCode,
                                       i4TestValFsMIRrdFilterByOspfTag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdFilterOspfTag
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                testValFsMIRrdFilterOspfTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdFilterOspfTag (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                               INT4 i4TestValFsMIRrdFilterOspfTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdFilterOspfTag (pu4ErrorCode,
                                     i4TestValFsMIRrdFilterOspfTag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdFilterOspfTagMask
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                testValFsMIRrdFilterOspfTagMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdFilterOspfTagMask (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                                   INT4 i4TestValFsMIRrdFilterOspfTagMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdFilterOspfTagMask (pu4ErrorCode,
                                         i4TestValFsMIRrdFilterOspfTagMask);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdRouterASNumber
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                testValFsMIRrdRouterASNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdRouterASNumber (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                                INT4 i4TestValFsMIRrdRouterASNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdRouterASNumber (pu4ErrorCode,
                                      i4TestValFsMIRrdRouterASNumber);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdAdminStatus
 Input       :  The Indices
                FsMIRtmContextId

                The Object 
                testValFsMIRrdAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdAdminStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                             INT4 i4TestValFsMIRrdAdminStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdAdminStatus (pu4ErrorCode, i4TestValFsMIRrdAdminStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdForce
 Input       :  The Indices
                FsMIRtmContextId

                The Object
                testValFsMIRrdForce
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdForce (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                       INT4 i4TestValFsMIRrdForce)
{
    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt ((UINT4) i4FsMIRtmContextId);
    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Force option can be enabled to rewrite AS Number & RouterId if and only if RRD Admin Status is
       Enabled. */
    if (pRtmCxt->u1RrdAdminStatus != RTM_ADMIN_STATUS_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RRD_DISABLED);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMIRrdForce != RTM_FORCE_ENABLE) &&
        (i4TestValFsMIRrdForce != RTM_FORCE_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD_INVALID_INPUT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhTestv2FsMIRTMIpStaticRouteDistance
 Input       :  The Indices
                FsMIRtmContextId

                The Object
                testValFsMIRTMIpStaticRouteDistance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIRTMIpStaticRouteDistance(UINT4 *pu4ErrorCode, 
                                      INT4 i4FsMIRtmContextId, 
                                      INT4 i4TestValFsMIRTMIpStaticRouteDistance)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRtmIpStaticRouteDistance (pu4ErrorCode, 
                                             i4TestValFsMIRTMIpStaticRouteDistance);
    UtilRtmResetContext ();
    return i1Return;
}


/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRtmTable
 Input       :  The Indices
                FsMIRtmContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRtmTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRrdControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRrdControlTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRrdControlTable (INT4 i4FsMIRtmContextId,
                                             UINT4
                                             u4FsMIRrdControlDestIpAddress,
                                             UINT4 u4FsMIRrdControlNetMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxts = 0;

    i4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         (INT4) FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIRtmContextId < RTM_DEFAULT_CXT_ID) ||
        (i4FsMIRtmContextId >= i4MaxCxts))
    {
        return i1RetVal;
    }

    if (UtilRtmVcmIsVcExist ((UINT4) i4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }

    if (UtilRtmIsValidCxtId ((UINT4) i4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }
    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    i1RetVal =
        nmhValidateIndexInstanceFsRrdControlTable
        (u4FsMIRrdControlDestIpAddress, u4FsMIRrdControlNetMask);
    UtilRtmResetContext ();
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRrdControlTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRrdControlTable (INT4 *pi4FsMIRtmContextId,
                                     UINT4 *pu4FsMIRrdControlDestIpAddress,
                                     UINT4 *pu4FsMIRrdControlNetMask)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRtmContextId = 0;

    if (UtilRtmGetFirstCxtId ((UINT4 *) pi4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIRtmContextId = *pi4FsMIRtmContextId;
        if (UtilRtmSetContext ((UINT4) *pi4FsMIRtmContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFsRrdControlTable (pu4FsMIRrdControlDestIpAddress,
                                               pu4FsMIRrdControlNetMask);
        UtilRtmResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
    }
    while (UtilRtmGetNextCxtId ((UINT4) i4FsMIRtmContextId,
                                (UINT4 *) pi4FsMIRtmContextId) != RTM_FAILURE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRrdControlTable
 Input       :  The Indices
                FsMIRtmContextId
                nextFsMIRtmContextId
                FsMIRrdControlDestIpAddress
                nextFsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask
                nextFsMIRrdControlNetMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRrdControlTable (INT4 i4FsMIRtmContextId,
                                    INT4 *pi4NextFsMIRtmContextId,
                                    UINT4 u4FsMIRrdControlDestIpAddress,
                                    UINT4 *pu4NextFsMIRrdControlDestIpAddress,
                                    UINT4 u4FsMIRrdControlNetMask,
                                    UINT4 *pu4NextFsMIRrdControlNetMask)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtmIsValidCxtId ((UINT4) i4FsMIRtmContextId) == RTM_SUCCESS)
    {
        if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFsRrdControlTable (u4FsMIRrdControlDestIpAddress,
                                              pu4NextFsMIRrdControlDestIpAddress,
                                              u4FsMIRrdControlNetMask,
                                              pu4NextFsMIRrdControlNetMask);
        UtilRtmResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilRtmGetNextCxtId ((UINT4) i4FsMIRtmContextId,
                                     (UINT4 *) pi4NextFsMIRtmContextId))
               == RTM_SUCCESS)
        {
            if (UtilRtmSetContext ((UINT4) *pi4NextFsMIRtmContextId) ==
                SNMP_FAILURE)
            {
                return i1RetVal;
            }
            i1RetVal =
                nmhGetFirstIndexFsRrdControlTable
                (pu4NextFsMIRrdControlDestIpAddress,
                 pu4NextFsMIRrdControlNetMask);
            UtilRtmResetContext ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIRtmContextId = *pi4NextFsMIRtmContextId;
        }
    }
    else
    {
        *pi4NextFsMIRtmContextId = i4FsMIRtmContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRrdControlSourceProto
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                retValFsMIRrdControlSourceProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdControlSourceProto (INT4 i4FsMIRtmContextId,
                                 UINT4 u4FsMIRrdControlDestIpAddress,
                                 UINT4 u4FsMIRrdControlNetMask,
                                 INT4 *pi4RetValFsMIRrdControlSourceProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdControlSourceProto (u4FsMIRrdControlDestIpAddress,
                                       u4FsMIRrdControlNetMask,
                                       pi4RetValFsMIRrdControlSourceProto);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdControlDestProto
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                retValFsMIRrdControlDestProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdControlDestProto (INT4 i4FsMIRtmContextId,
                               UINT4 u4FsMIRrdControlDestIpAddress,
                               UINT4 u4FsMIRrdControlNetMask,
                               INT4 *pi4RetValFsMIRrdControlDestProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdControlDestProto (u4FsMIRrdControlDestIpAddress,
                                     u4FsMIRrdControlNetMask,
                                     pi4RetValFsMIRrdControlDestProto);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdControlRouteExportFlag
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                retValFsMIRrdControlRouteExportFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdControlRouteExportFlag (INT4 i4FsMIRtmContextId,
                                     UINT4 u4FsMIRrdControlDestIpAddress,
                                     UINT4 u4FsMIRrdControlNetMask,
                                     INT4
                                     *pi4RetValFsMIRrdControlRouteExportFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdControlRouteExportFlag (u4FsMIRrdControlDestIpAddress,
                                           u4FsMIRrdControlNetMask,
                                           pi4RetValFsMIRrdControlRouteExportFlag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdControlRowStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                retValFsMIRrdControlRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdControlRowStatus (INT4 i4FsMIRtmContextId,
                               UINT4 u4FsMIRrdControlDestIpAddress,
                               UINT4 u4FsMIRrdControlNetMask,
                               INT4 *pi4RetValFsMIRrdControlRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdControlRowStatus (u4FsMIRrdControlDestIpAddress,
                                     u4FsMIRrdControlNetMask,
                                     pi4RetValFsMIRrdControlRowStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRrdControlSourceProto
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                setValFsMIRrdControlSourceProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdControlSourceProto (INT4 i4FsMIRtmContextId,
                                 UINT4 u4FsMIRrdControlDestIpAddress,
                                 UINT4 u4FsMIRrdControlNetMask,
                                 INT4 i4SetValFsMIRrdControlSourceProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrdControlSourceProto (u4FsMIRrdControlDestIpAddress,
                                       u4FsMIRrdControlNetMask,
                                       i4SetValFsMIRrdControlSourceProto);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdControlDestProto
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                setValFsMIRrdControlDestProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdControlDestProto (INT4 i4FsMIRtmContextId,
                               UINT4 u4FsMIRrdControlDestIpAddress,
                               UINT4 u4FsMIRrdControlNetMask,
                               INT4 i4SetValFsMIRrdControlDestProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrdControlDestProto (u4FsMIRrdControlDestIpAddress,
                                     u4FsMIRrdControlNetMask,
                                     i4SetValFsMIRrdControlDestProto);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdControlRouteExportFlag
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                setValFsMIRrdControlRouteExportFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdControlRouteExportFlag (INT4 i4FsMIRtmContextId,
                                     UINT4 u4FsMIRrdControlDestIpAddress,
                                     UINT4 u4FsMIRrdControlNetMask,
                                     INT4 i4SetValFsMIRrdControlRouteExportFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrdControlRouteExportFlag (u4FsMIRrdControlDestIpAddress,
                                           u4FsMIRrdControlNetMask,
                                           i4SetValFsMIRrdControlRouteExportFlag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdControlRowStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                setValFsMIRrdControlRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdControlRowStatus (INT4 i4FsMIRtmContextId,
                               UINT4 u4FsMIRrdControlDestIpAddress,
                               UINT4 u4FsMIRrdControlNetMask,
                               INT4 i4SetValFsMIRrdControlRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrdControlRowStatus (u4FsMIRrdControlDestIpAddress,
                                     u4FsMIRrdControlNetMask,
                                     i4SetValFsMIRrdControlRowStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdControlSourceProto
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                testValFsMIRrdControlSourceProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdControlSourceProto (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRtmContextId,
                                    UINT4 u4FsMIRrdControlDestIpAddress,
                                    UINT4 u4FsMIRrdControlNetMask,
                                    INT4 i4TestValFsMIRrdControlSourceProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdControlSourceProto (pu4ErrorCode,
                                          u4FsMIRrdControlDestIpAddress,
                                          u4FsMIRrdControlNetMask,
                                          i4TestValFsMIRrdControlSourceProto);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdControlDestProto
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                testValFsMIRrdControlDestProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdControlDestProto (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                                  UINT4 u4FsMIRrdControlDestIpAddress,
                                  UINT4 u4FsMIRrdControlNetMask,
                                  INT4 i4TestValFsMIRrdControlDestProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdControlDestProto (pu4ErrorCode,
                                        u4FsMIRrdControlDestIpAddress,
                                        u4FsMIRrdControlNetMask,
                                        i4TestValFsMIRrdControlDestProto);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdControlRouteExportFlag
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                testValFsMIRrdControlRouteExportFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdControlRouteExportFlag (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIRtmContextId,
                                        UINT4 u4FsMIRrdControlDestIpAddress,
                                        UINT4 u4FsMIRrdControlNetMask,
                                        INT4
                                        i4TestValFsMIRrdControlRouteExportFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdControlRouteExportFlag (pu4ErrorCode,
                                              u4FsMIRrdControlDestIpAddress,
                                              u4FsMIRrdControlNetMask,
                                              i4TestValFsMIRrdControlRouteExportFlag);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdControlRowStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask

                The Object 
                testValFsMIRrdControlRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdControlRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                                  UINT4 u4FsMIRrdControlDestIpAddress,
                                  UINT4 u4FsMIRrdControlNetMask,
                                  INT4 i4TestValFsMIRrdControlRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdControlRowStatus (pu4ErrorCode,
                                        u4FsMIRrdControlDestIpAddress,
                                        u4FsMIRrdControlNetMask,
                                        i4TestValFsMIRrdControlRowStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRrdControlTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdControlDestIpAddress
                FsMIRrdControlNetMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRrdControlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRrdRoutingProtoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRrdRoutingProtoTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRrdRoutingProtoTable (INT4 i4FsMIRtmContextId,
                                                  INT4 i4FsMIRrdRoutingProtoId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxts = 0;

    i4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         (INT4) FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIRtmContextId < RTM_DEFAULT_CXT_ID) ||
        (i4FsMIRtmContextId >= i4MaxCxts))
    {
        return i1RetVal;
    }

    if (UtilRtmVcmIsVcExist ((UINT4) i4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }

    if (UtilRtmIsValidCxtId ((UINT4) i4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }
    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    i1RetVal =
        nmhValidateIndexInstanceFsRrdRoutingProtoTable
        (i4FsMIRrdRoutingProtoId);
    UtilRtmResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRrdRoutingProtoTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRrdRoutingProtoTable (INT4 *pi4FsMIRtmContextId,
                                          INT4 *pi4FsMIRrdRoutingProtoId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRtmContextId = 0;

    if (UtilRtmGetFirstCxtId ((UINT4 *) pi4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIRtmContextId = *pi4FsMIRtmContextId;
        if (UtilRtmSetContext ((UINT4) *pi4FsMIRtmContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetFirstIndexFsRrdRoutingProtoTable (pi4FsMIRrdRoutingProtoId);
        UtilRtmResetContext ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
    }
    while (UtilRtmGetNextCxtId ((UINT4) i4FsMIRtmContextId,
                                (UINT4 *) pi4FsMIRtmContextId) != RTM_FAILURE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRrdRoutingProtoTable
 Input       :  The Indices
                FsMIRtmContextId
                nextFsMIRtmContextId
                FsMIRrdRoutingProtoId
                nextFsMIRrdRoutingProtoId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRrdRoutingProtoTable (INT4 i4FsMIRtmContextId,
                                         INT4 *pi4NextFsMIRtmContextId,
                                         INT4 i4FsMIRrdRoutingProtoId,
                                         INT4 *pi4NextFsMIRrdRoutingProtoId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtmIsValidCxtId ((UINT4) i4FsMIRtmContextId) == RTM_SUCCESS)
    {
        if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal =
            nmhGetNextIndexFsRrdRoutingProtoTable (i4FsMIRrdRoutingProtoId,
                                                   pi4NextFsMIRrdRoutingProtoId);
        UtilRtmResetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilRtmGetNextCxtId ((UINT4) i4FsMIRtmContextId,
                                     (UINT4 *) pi4NextFsMIRtmContextId))
               == RTM_SUCCESS)
        {
            if (UtilRtmSetContext ((UINT4) *pi4NextFsMIRtmContextId) ==
                SNMP_FAILURE)
            {
                return i1RetVal;
            }
            i1RetVal =
                nmhGetFirstIndexFsRrdRoutingProtoTable
                (pi4NextFsMIRrdRoutingProtoId);
            UtilRtmResetContext ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIRtmContextId = *pi4NextFsMIRtmContextId;
        }
    }
    else
    {
        *pi4NextFsMIRtmContextId = i4FsMIRtmContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRrdRoutingRegnId
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                retValFsMIRrdRoutingRegnId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdRoutingRegnId (INT4 i4FsMIRtmContextId,
                            INT4 i4FsMIRrdRoutingProtoId,
                            INT4 *pi4RetValFsMIRrdRoutingRegnId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdRoutingRegnId (i4FsMIRrdRoutingProtoId,
                                  pi4RetValFsMIRrdRoutingRegnId);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdRoutingProtoTaskIdent
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                retValFsMIRrdRoutingProtoTaskIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdRoutingProtoTaskIdent (INT4 i4FsMIRtmContextId,
                                    INT4 i4FsMIRrdRoutingProtoId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsMIRrdRoutingProtoTaskIdent)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdRoutingProtoTaskIdent (i4FsMIRrdRoutingProtoId,
                                          pRetValFsMIRrdRoutingProtoTaskIdent);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdRoutingProtoQueueIdent
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                retValFsMIRrdRoutingProtoQueueIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdRoutingProtoQueueIdent (INT4 i4FsMIRtmContextId,
                                     INT4 i4FsMIRrdRoutingProtoId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMIRrdRoutingProtoQueueIdent)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdRoutingProtoQueueIdent (i4FsMIRrdRoutingProtoId,
                                           pRetValFsMIRrdRoutingProtoQueueIdent);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdAllowOspfAreaRoutes
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                retValFsMIRrdAllowOspfAreaRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdAllowOspfAreaRoutes (INT4 i4FsMIRtmContextId,
                                  INT4 i4FsMIRrdRoutingProtoId,
                                  INT4 *pi4RetValFsMIRrdAllowOspfAreaRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdAllowOspfAreaRoutes (i4FsMIRrdRoutingProtoId,
                                        pi4RetValFsMIRrdAllowOspfAreaRoutes);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRrdAllowOspfExtRoutes
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                retValFsMIRrdAllowOspfExtRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRrdAllowOspfExtRoutes (INT4 i4FsMIRtmContextId,
                                 INT4 i4FsMIRrdRoutingProtoId,
                                 INT4 *pi4RetValFsMIRrdAllowOspfExtRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRrdAllowOspfExtRoutes (i4FsMIRrdRoutingProtoId,
                                       pi4RetValFsMIRrdAllowOspfExtRoutes);
    UtilRtmResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRrdAllowOspfAreaRoutes
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                setValFsMIRrdAllowOspfAreaRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdAllowOspfAreaRoutes (INT4 i4FsMIRtmContextId,
                                  INT4 i4FsMIRrdRoutingProtoId,
                                  INT4 i4SetValFsMIRrdAllowOspfAreaRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrdAllowOspfAreaRoutes (i4FsMIRrdRoutingProtoId,
                                        i4SetValFsMIRrdAllowOspfAreaRoutes);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRrdAllowOspfExtRoutes
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                setValFsMIRrdAllowOspfExtRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRrdAllowOspfExtRoutes (INT4 i4FsMIRtmContextId,
                                 INT4 i4FsMIRrdRoutingProtoId,
                                 INT4 i4SetValFsMIRrdAllowOspfExtRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRrdAllowOspfExtRoutes (i4FsMIRrdRoutingProtoId,
                                       i4SetValFsMIRrdAllowOspfExtRoutes);
    UtilRtmResetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdAllowOspfAreaRoutes
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                testValFsMIRrdAllowOspfAreaRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdAllowOspfAreaRoutes (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIRtmContextId,
                                     INT4 i4FsMIRrdRoutingProtoId,
                                     INT4 i4TestValFsMIRrdAllowOspfAreaRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdAllowOspfAreaRoutes (pu4ErrorCode,
                                           i4FsMIRrdRoutingProtoId,
                                           i4TestValFsMIRrdAllowOspfAreaRoutes);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRrdAllowOspfExtRoutes
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId

                The Object 
                testValFsMIRrdAllowOspfExtRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRrdAllowOspfExtRoutes (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRtmContextId,
                                    INT4 i4FsMIRrdRoutingProtoId,
                                    INT4 i4TestValFsMIRrdAllowOspfExtRoutes)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRrdAllowOspfExtRoutes (pu4ErrorCode, i4FsMIRrdRoutingProtoId,
                                          i4TestValFsMIRrdAllowOspfExtRoutes);
    UtilRtmResetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRrdRoutingProtoTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRrdRoutingProtoId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRrdRoutingProtoTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIRtmCommonRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIRtmCommonRouteTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIRtmCommonRouteTable (INT4 i4FsMIRtmContextId,
                                                 UINT4 u4FsMIRtmCommonRouteDest,
                                                 UINT4 u4FsMIRtmCommonRouteMask,
                                                 INT4 i4FsMIRtmCommonRouteTos,
                                                 UINT4
                                                 u4FsMIRtmCommonRouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxts = 0;

    i4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         (INT4) FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4FsMIRtmContextId < RTM_DEFAULT_CXT_ID) ||
        (i4FsMIRtmContextId >= i4MaxCxts))
    {
        return i1RetVal;
    }

    if (UtilRtmVcmIsVcExist ((UINT4) i4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }

    if (UtilRtmIsValidCxtId ((UINT4) i4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }
    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    i1RetVal =
        nmhValidateIndexInstanceFsRtmCommonRouteTable (u4FsMIRtmCommonRouteDest,
                                                       u4FsMIRtmCommonRouteMask,
                                                       i4FsMIRtmCommonRouteTos,
                                                       u4FsMIRtmCommonRouteNextHop);
    UtilRtmResetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIRtmCommonRouteTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIRtmCommonRouteTable (INT4 *pi4FsMIRtmContextId,
                                         UINT4 *pu4FsMIRtmCommonRouteDest,
                                         UINT4 *pu4FsMIRtmCommonRouteMask,
                                         INT4 *pi4FsMIRtmCommonRouteTos,
                                         UINT4 *pu4FsMIRtmCommonRouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIRtmContextId = 0;

    if (UtilRtmGetFirstCxtId ((UINT4 *) pi4FsMIRtmContextId) == RTM_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIRtmContextId = *pi4FsMIRtmContextId;
        if (UtilRtmSetContext ((UINT4) *pi4FsMIRtmContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        if (UtilIpvxSetCxt ((UINT4) *pi4FsMIRtmContextId) == SNMP_FAILURE)
        {
			UtilRtmResetContext ();
            return i1RetVal;
        }

        i1RetVal =
            nmhGetFirstIndexFsRtmCommonRouteTable (pu4FsMIRtmCommonRouteDest,
                                                   pu4FsMIRtmCommonRouteMask,
                                                   pi4FsMIRtmCommonRouteTos,
                                                   pu4FsMIRtmCommonRouteNextHop);
        UtilRtmResetContext ();
        UtilIpvxReleaseCxt ();
        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }
    }
    while (UtilRtmGetNextCxtId ((UINT4) i4FsMIRtmContextId,
                                (UINT4 *) pi4FsMIRtmContextId) != RTM_FAILURE);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIRtmCommonRouteTable
 Input       :  The Indices
                FsMIRtmContextId
                nextFsMIRtmContextId
                FsMIRtmCommonRouteDest
                nextFsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                nextFsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                nextFsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop
                nextFsMIRtmCommonRouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIRtmCommonRouteTable (INT4 i4FsMIRtmContextId,
                                        INT4 *pi4NextFsMIRtmContextId,
                                        UINT4 u4FsMIRtmCommonRouteDest,
                                        UINT4 *pu4NextFsMIRtmCommonRouteDest,
                                        UINT4 u4FsMIRtmCommonRouteMask,
                                        UINT4 *pu4NextFsMIRtmCommonRouteMask,
                                        INT4 i4FsMIRtmCommonRouteTos,
                                        INT4 *pi4NextFsMIRtmCommonRouteTos,
                                        UINT4 u4FsMIRtmCommonRouteNextHop,
                                        UINT4 *pu4NextFsMIRtmCommonRouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (UtilRtmIsValidCxtId ((UINT4) i4FsMIRtmContextId) == RTM_SUCCESS)
    {
        if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        if (UtilIpvxSetCxt ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
        {	
			UtilRtmResetContext ();
            return i1RetVal;
        }


        i1RetVal =
            nmhGetNextIndexFsRtmCommonRouteTable (u4FsMIRtmCommonRouteDest,
                                                  pu4NextFsMIRtmCommonRouteDest,
                                                  u4FsMIRtmCommonRouteMask,
                                                  pu4NextFsMIRtmCommonRouteMask,
                                                  i4FsMIRtmCommonRouteTos,
                                                  pi4NextFsMIRtmCommonRouteTos,
                                                  u4FsMIRtmCommonRouteNextHop,
                                                  pu4NextFsMIRtmCommonRouteNextHop);
        UtilRtmResetContext ();
        UtilIpvxReleaseCxt ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((UtilRtmGetNextCxtId ((UINT4) i4FsMIRtmContextId,
                                     (UINT4 *) pi4NextFsMIRtmContextId))
               == RTM_SUCCESS)
        {
            if (UtilRtmSetContext ((UINT4) *pi4NextFsMIRtmContextId) ==
                SNMP_FAILURE)
            {
                return i1RetVal;
            }
            i1RetVal =
                nmhGetFirstIndexFsRtmCommonRouteTable
                (pu4NextFsMIRtmCommonRouteDest, pu4NextFsMIRtmCommonRouteMask,
                 pi4NextFsMIRtmCommonRouteTos,
                 pu4NextFsMIRtmCommonRouteNextHop);
            UtilRtmResetContext ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIRtmContextId = *pi4NextFsMIRtmContextId;
        }
    }
    else
    {
        *pi4NextFsMIRtmContextId = i4FsMIRtmContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRouteIfIndex
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRouteIfIndex (INT4 i4FsMIRtmContextId,
                                 UINT4 u4FsMIRtmCommonRouteDest,
                                 UINT4 u4FsMIRtmCommonRouteMask,
                                 INT4 i4FsMIRtmCommonRouteTos,
                                 UINT4 u4FsMIRtmCommonRouteNextHop,
                                 INT4 *pi4RetValFsMIRtmCommonRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRouteIfIndex (u4FsMIRtmCommonRouteDest,
                                       u4FsMIRtmCommonRouteMask,
                                       i4FsMIRtmCommonRouteTos,
                                       u4FsMIRtmCommonRouteNextHop,
                                       pi4RetValFsMIRtmCommonRouteIfIndex);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRouteType
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRouteType (INT4 i4FsMIRtmContextId,
                              UINT4 u4FsMIRtmCommonRouteDest,
                              UINT4 u4FsMIRtmCommonRouteMask,
                              INT4 i4FsMIRtmCommonRouteTos,
                              UINT4 u4FsMIRtmCommonRouteNextHop,
                              INT4 *pi4RetValFsMIRtmCommonRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRouteType (u4FsMIRtmCommonRouteDest,
                                    u4FsMIRtmCommonRouteMask,
                                    i4FsMIRtmCommonRouteTos,
                                    u4FsMIRtmCommonRouteNextHop,
                                    pi4RetValFsMIRtmCommonRouteType);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRouteProto
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRouteProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRouteProto (INT4 i4FsMIRtmContextId,
                               UINT4 u4FsMIRtmCommonRouteDest,
                               UINT4 u4FsMIRtmCommonRouteMask,
                               INT4 i4FsMIRtmCommonRouteTos,
                               UINT4 u4FsMIRtmCommonRouteNextHop,
                               INT4 *pi4RetValFsMIRtmCommonRouteProto)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRouteProto (u4FsMIRtmCommonRouteDest,
                                     u4FsMIRtmCommonRouteMask,
                                     i4FsMIRtmCommonRouteTos,
                                     u4FsMIRtmCommonRouteNextHop,
                                     pi4RetValFsMIRtmCommonRouteProto);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRouteAge
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRouteAge (INT4 i4FsMIRtmContextId,
                             UINT4 u4FsMIRtmCommonRouteDest,
                             UINT4 u4FsMIRtmCommonRouteMask,
                             INT4 i4FsMIRtmCommonRouteTos,
                             UINT4 u4FsMIRtmCommonRouteNextHop,
                             INT4 *pi4RetValFsMIRtmCommonRouteAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRouteAge (u4FsMIRtmCommonRouteDest,
                                   u4FsMIRtmCommonRouteMask,
                                   i4FsMIRtmCommonRouteTos,
                                   u4FsMIRtmCommonRouteNextHop,
                                   pi4RetValFsMIRtmCommonRouteAge);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRouteInfo
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRouteInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRouteInfo (INT4 i4FsMIRtmContextId,
                              UINT4 u4FsMIRtmCommonRouteDest,
                              UINT4 u4FsMIRtmCommonRouteMask,
                              INT4 i4FsMIRtmCommonRouteTos,
                              UINT4 u4FsMIRtmCommonRouteNextHop,
                              tSNMP_OID_TYPE * pRetValFsMIRtmCommonRouteInfo)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRouteInfo (u4FsMIRtmCommonRouteDest,
                                    u4FsMIRtmCommonRouteMask,
                                    i4FsMIRtmCommonRouteTos,
                                    u4FsMIRtmCommonRouteNextHop,
                                    pRetValFsMIRtmCommonRouteInfo);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRouteNextHopAS
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRouteNextHopAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRouteNextHopAS (INT4 i4FsMIRtmContextId,
                                   UINT4 u4FsMIRtmCommonRouteDest,
                                   UINT4 u4FsMIRtmCommonRouteMask,
                                   INT4 i4FsMIRtmCommonRouteTos,
                                   UINT4 u4FsMIRtmCommonRouteNextHop,
                                   INT4 *pi4RetValFsMIRtmCommonRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRouteNextHopAS (u4FsMIRtmCommonRouteDest,
                                         u4FsMIRtmCommonRouteMask,
                                         i4FsMIRtmCommonRouteTos,
                                         u4FsMIRtmCommonRouteNextHop,
                                         pi4RetValFsMIRtmCommonRouteNextHopAS);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRouteMetric1
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRouteMetric1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRouteMetric1 (INT4 i4FsMIRtmContextId,
                                 UINT4 u4FsMIRtmCommonRouteDest,
                                 UINT4 u4FsMIRtmCommonRouteMask,
                                 INT4 i4FsMIRtmCommonRouteTos,
                                 UINT4 u4FsMIRtmCommonRouteNextHop,
                                 INT4 *pi4RetValFsMIRtmCommonRouteMetric1)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRouteMetric1 (u4FsMIRtmCommonRouteDest,
                                       u4FsMIRtmCommonRouteMask,
                                       i4FsMIRtmCommonRouteTos,
                                       u4FsMIRtmCommonRouteNextHop,
                                       pi4RetValFsMIRtmCommonRouteMetric1);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRoutePrivateStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRoutePrivateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRoutePrivateStatus (INT4 i4FsMIRtmContextId,
                                       UINT4 u4FsMIRtmCommonRouteDest,
                                       UINT4 u4FsMIRtmCommonRouteMask,
                                       INT4 i4FsMIRtmCommonRouteTos,
                                       UINT4 u4FsMIRtmCommonRouteNextHop,
                                       INT4
                                       *pi4RetValFsMIRtmCommonRoutePrivateStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRoutePrivateStatus (u4FsMIRtmCommonRouteDest,
                                             u4FsMIRtmCommonRouteMask,
                                             i4FsMIRtmCommonRouteTos,
                                             u4FsMIRtmCommonRouteNextHop,
                                             pi4RetValFsMIRtmCommonRoutePrivateStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRoutePreference
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object
                retValFsMIRtmCommonRoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsMIRtmCommonRoutePreference(INT4 i4FsMIRtmContextId , 
                                   UINT4 u4FsMIRtmCommonRouteDest, 
                                   UINT4 u4FsMIRtmCommonRouteMask, 
                                   INT4 i4FsMIRtmCommonRouteTos, 
                                   UINT4 u4FsMIRtmCommonRouteNextHop, 
                                   INT4 *pi4RetValFsMIRtmCommonRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRoutePreference (u4FsMIRtmCommonRouteDest,
                                          u4FsMIRtmCommonRouteMask,
                                          i4FsMIRtmCommonRouteTos,
                                          u4FsMIRtmCommonRouteNextHop,
                                          pi4RetValFsMIRtmCommonRoutePreference);
    UtilRtmResetContext ();
    return i1Return;
}


/****************************************************************************
 Function    :  nmhGetFsMIRtmCommonRouteStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                retValFsMIRtmCommonRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmCommonRouteStatus (INT4 i4FsMIRtmContextId,
                                UINT4 u4FsMIRtmCommonRouteDest,
                                UINT4 u4FsMIRtmCommonRouteMask,
                                INT4 i4FsMIRtmCommonRouteTos,
                                UINT4 u4FsMIRtmCommonRouteNextHop,
                                INT4 *pi4RetValFsMIRtmCommonRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFsRtmCommonRouteStatus (u4FsMIRtmCommonRouteDest,
                                      u4FsMIRtmCommonRouteMask,
                                      i4FsMIRtmCommonRouteTos,
                                      u4FsMIRtmCommonRouteNextHop,
                                      pi4RetValFsMIRtmCommonRouteStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRtmCommonRouteIfIndex
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                setValFsMIRtmCommonRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtmCommonRouteIfIndex (INT4 i4FsMIRtmContextId,
                                 UINT4 u4FsMIRtmCommonRouteDest,
                                 UINT4 u4FsMIRtmCommonRouteMask,
                                 INT4 i4FsMIRtmCommonRouteTos,
                                 UINT4 u4FsMIRtmCommonRouteNextHop,
                                 INT4 i4SetValFsMIRtmCommonRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRtmCommonRouteIfIndex (u4FsMIRtmCommonRouteDest,
                                       u4FsMIRtmCommonRouteMask,
                                       i4FsMIRtmCommonRouteTos,
                                       u4FsMIRtmCommonRouteNextHop,
                                       i4SetValFsMIRtmCommonRouteIfIndex);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtmCommonRouteType
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                setValFsMIRtmCommonRouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtmCommonRouteType (INT4 i4FsMIRtmContextId,
                              UINT4 u4FsMIRtmCommonRouteDest,
                              UINT4 u4FsMIRtmCommonRouteMask,
                              INT4 i4FsMIRtmCommonRouteTos,
                              UINT4 u4FsMIRtmCommonRouteNextHop,
                              INT4 i4SetValFsMIRtmCommonRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRtmCommonRouteType (u4FsMIRtmCommonRouteDest,
                                    u4FsMIRtmCommonRouteMask,
                                    i4FsMIRtmCommonRouteTos,
                                    u4FsMIRtmCommonRouteNextHop,
                                    i4SetValFsMIRtmCommonRouteType);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtmCommonRouteInfo
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                setValFsMIRtmCommonRouteInfo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtmCommonRouteInfo (INT4 i4FsMIRtmContextId,
                              UINT4 u4FsMIRtmCommonRouteDest,
                              UINT4 u4FsMIRtmCommonRouteMask,
                              INT4 i4FsMIRtmCommonRouteTos,
                              UINT4 u4FsMIRtmCommonRouteNextHop,
                              tSNMP_OID_TYPE * pSetValFsMIRtmCommonRouteInfo)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRtmCommonRouteInfo (u4FsMIRtmCommonRouteDest,
                                    u4FsMIRtmCommonRouteMask,
                                    i4FsMIRtmCommonRouteTos,
                                    u4FsMIRtmCommonRouteNextHop,
                                    pSetValFsMIRtmCommonRouteInfo);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtmCommonRouteNextHopAS
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                setValFsMIRtmCommonRouteNextHopAS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtmCommonRouteNextHopAS (INT4 i4FsMIRtmContextId,
                                   UINT4 u4FsMIRtmCommonRouteDest,
                                   UINT4 u4FsMIRtmCommonRouteMask,
                                   INT4 i4FsMIRtmCommonRouteTos,
                                   UINT4 u4FsMIRtmCommonRouteNextHop,
                                   INT4 i4SetValFsMIRtmCommonRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRtmCommonRouteNextHopAS (u4FsMIRtmCommonRouteDest,
                                         u4FsMIRtmCommonRouteMask,
                                         i4FsMIRtmCommonRouteTos,
                                         u4FsMIRtmCommonRouteNextHop,
                                         i4SetValFsMIRtmCommonRouteNextHopAS);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtmCommonRouteMetric1
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                setValFsMIRtmCommonRouteMetric1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtmCommonRouteMetric1 (INT4 i4FsMIRtmContextId,
                                 UINT4 u4FsMIRtmCommonRouteDest,
                                 UINT4 u4FsMIRtmCommonRouteMask,
                                 INT4 i4FsMIRtmCommonRouteTos,
                                 UINT4 u4FsMIRtmCommonRouteNextHop,
                                 INT4 i4SetValFsMIRtmCommonRouteMetric1)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRtmCommonRouteMetric1 (u4FsMIRtmCommonRouteDest,
                                       u4FsMIRtmCommonRouteMask,
                                       i4FsMIRtmCommonRouteTos,
                                       u4FsMIRtmCommonRouteNextHop,
                                       i4SetValFsMIRtmCommonRouteMetric1);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtmCommonRoutePrivateStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                setValFsMIRtmCommonRoutePrivateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtmCommonRoutePrivateStatus (INT4 i4FsMIRtmContextId,
                                       UINT4 u4FsMIRtmCommonRouteDest,
                                       UINT4 u4FsMIRtmCommonRouteMask,
                                       INT4 i4FsMIRtmCommonRouteTos,
                                       UINT4 u4FsMIRtmCommonRouteNextHop,
                                       INT4
                                       i4SetValFsMIRtmCommonRoutePrivateStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRtmCommonRoutePrivateStatus (u4FsMIRtmCommonRouteDest,
                                             u4FsMIRtmCommonRouteMask,
                                             i4FsMIRtmCommonRouteTos,
                                             u4FsMIRtmCommonRouteNextHop,
                                             i4SetValFsMIRtmCommonRoutePrivateStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtmCommonRoutePreference
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object
                setValFsMIRtmCommonRoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsMIRtmCommonRoutePreference(INT4 i4FsMIRtmContextId, 
                                   UINT4 u4FsMIRtmCommonRouteDest, 
                                   UINT4 u4FsMIRtmCommonRouteMask, 
                                   INT4 i4FsMIRtmCommonRouteTos, 
                                   UINT4 u4FsMIRtmCommonRouteNextHop, 
                                   INT4 i4SetValFsMIRtmCommonRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFsRtmCommonRoutePreference (u4FsMIRtmCommonRouteDest,
                                          u4FsMIRtmCommonRouteMask,
                                          i4FsMIRtmCommonRouteTos,
                                          u4FsMIRtmCommonRouteNextHop,
                                          i4SetValFsMIRtmCommonRoutePreference);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIRtmCommonRouteStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                setValFsMIRtmCommonRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIRtmCommonRouteStatus (INT4 i4FsMIRtmContextId,
                                UINT4 u4FsMIRtmCommonRouteDest,
                                UINT4 u4FsMIRtmCommonRouteMask,
                                INT4 i4FsMIRtmCommonRouteTos,
                                UINT4 u4FsMIRtmCommonRouteNextHop,
                                INT4 i4SetValFsMIRtmCommonRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    if (UtilIpvxSetCxt ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
		UtilRtmResetContext ();
        return i1Return;
    }

    i1Return =
        nmhSetFsRtmCommonRouteStatus (u4FsMIRtmCommonRouteDest,
                                      u4FsMIRtmCommonRouteMask,
                                      i4FsMIRtmCommonRouteTos,
                                      u4FsMIRtmCommonRouteNextHop,
                                      i4SetValFsMIRtmCommonRouteStatus);
    UtilRtmResetContext ();
    UtilIpvxReleaseCxt ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmCommonRouteIfIndex
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                testValFsMIRtmCommonRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtmCommonRouteIfIndex (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRtmContextId,
                                    UINT4 u4FsMIRtmCommonRouteDest,
                                    UINT4 u4FsMIRtmCommonRouteMask,
                                    INT4 i4FsMIRtmCommonRouteTos,
                                    UINT4 u4FsMIRtmCommonRouteNextHop,
                                    INT4 i4TestValFsMIRtmCommonRouteIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRtmCommonRouteIfIndex (pu4ErrorCode,
                                          u4FsMIRtmCommonRouteDest,
                                          u4FsMIRtmCommonRouteMask,
                                          i4FsMIRtmCommonRouteTos,
                                          u4FsMIRtmCommonRouteNextHop,
                                          i4TestValFsMIRtmCommonRouteIfIndex);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmCommonRouteType
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                testValFsMIRtmCommonRouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtmCommonRouteType (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                                 UINT4 u4FsMIRtmCommonRouteDest,
                                 UINT4 u4FsMIRtmCommonRouteMask,
                                 INT4 i4FsMIRtmCommonRouteTos,
                                 UINT4 u4FsMIRtmCommonRouteNextHop,
                                 INT4 i4TestValFsMIRtmCommonRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRtmCommonRouteType (pu4ErrorCode, u4FsMIRtmCommonRouteDest,
                                       u4FsMIRtmCommonRouteMask,
                                       i4FsMIRtmCommonRouteTos,
                                       u4FsMIRtmCommonRouteNextHop,
                                       i4TestValFsMIRtmCommonRouteType);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmCommonRouteInfo
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                testValFsMIRtmCommonRouteInfo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtmCommonRouteInfo (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                                 UINT4 u4FsMIRtmCommonRouteDest,
                                 UINT4 u4FsMIRtmCommonRouteMask,
                                 INT4 i4FsMIRtmCommonRouteTos,
                                 UINT4 u4FsMIRtmCommonRouteNextHop,
                                 tSNMP_OID_TYPE *
                                 pTestValFsMIRtmCommonRouteInfo)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRtmCommonRouteInfo (pu4ErrorCode, u4FsMIRtmCommonRouteDest,
                                       u4FsMIRtmCommonRouteMask,
                                       i4FsMIRtmCommonRouteTos,
                                       u4FsMIRtmCommonRouteNextHop,
                                       pTestValFsMIRtmCommonRouteInfo);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmCommonRouteNextHopAS
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                testValFsMIRtmCommonRouteNextHopAS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtmCommonRouteNextHopAS (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIRtmContextId,
                                      UINT4 u4FsMIRtmCommonRouteDest,
                                      UINT4 u4FsMIRtmCommonRouteMask,
                                      INT4 i4FsMIRtmCommonRouteTos,
                                      UINT4 u4FsMIRtmCommonRouteNextHop,
                                      INT4 i4TestValFsMIRtmCommonRouteNextHopAS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRtmCommonRouteNextHopAS (pu4ErrorCode,
                                            u4FsMIRtmCommonRouteDest,
                                            u4FsMIRtmCommonRouteMask,
                                            i4FsMIRtmCommonRouteTos,
                                            u4FsMIRtmCommonRouteNextHop,
                                            i4TestValFsMIRtmCommonRouteNextHopAS);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmCommonRouteMetric1
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                testValFsMIRtmCommonRouteMetric1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtmCommonRouteMetric1 (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIRtmContextId,
                                    UINT4 u4FsMIRtmCommonRouteDest,
                                    UINT4 u4FsMIRtmCommonRouteMask,
                                    INT4 i4FsMIRtmCommonRouteTos,
                                    UINT4 u4FsMIRtmCommonRouteNextHop,
                                    INT4 i4TestValFsMIRtmCommonRouteMetric1)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRtmCommonRouteMetric1 (pu4ErrorCode,
                                          u4FsMIRtmCommonRouteDest,
                                          u4FsMIRtmCommonRouteMask,
                                          i4FsMIRtmCommonRouteTos,
                                          u4FsMIRtmCommonRouteNextHop,
                                          i4TestValFsMIRtmCommonRouteMetric1);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmCommonRoutePrivateStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                testValFsMIRtmCommonRoutePrivateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtmCommonRoutePrivateStatus (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIRtmContextId,
                                          UINT4 u4FsMIRtmCommonRouteDest,
                                          UINT4 u4FsMIRtmCommonRouteMask,
                                          INT4 i4FsMIRtmCommonRouteTos,
                                          UINT4 u4FsMIRtmCommonRouteNextHop,
                                          INT4
                                          i4TestValFsMIRtmCommonRoutePrivateStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRtmCommonRoutePrivateStatus (pu4ErrorCode,
                                                u4FsMIRtmCommonRouteDest,
                                                u4FsMIRtmCommonRouteMask,
                                                i4FsMIRtmCommonRouteTos,
                                                u4FsMIRtmCommonRouteNextHop,
                                                i4TestValFsMIRtmCommonRoutePrivateStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmCommonRoutePreference
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object
                testValFsMIRtmCommonRoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsMIRtmCommonRoutePreference(UINT4 *pu4ErrorCode, 
                                      INT4 i4FsMIRtmContextId, 
                                      UINT4 u4FsMIRtmCommonRouteDest,
                                      UINT4 u4FsMIRtmCommonRouteMask, 
                                      INT4 i4FsMIRtmCommonRouteTos, 
                                      UINT4 u4FsMIRtmCommonRouteNextHop, 
                                      INT4 i4TestValFsMIRtmCommonRoutePreference)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
       nmhTestv2FsRtmCommonRoutePreference (pu4ErrorCode,
                                            u4FsMIRtmCommonRouteDest,
                                            u4FsMIRtmCommonRouteMask,
                                            i4FsMIRtmCommonRouteTos,
                                            u4FsMIRtmCommonRouteNextHop,
                                            i4TestValFsMIRtmCommonRoutePreference);
    UtilRtmResetContext ();
    return i1Return;
}


/****************************************************************************
 Function    :  nmhTestv2FsMIRtmCommonRouteStatus
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop

                The Object 
                testValFsMIRtmCommonRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIRtmCommonRouteStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIRtmContextId,
                                   UINT4 u4FsMIRtmCommonRouteDest,
                                   UINT4 u4FsMIRtmCommonRouteMask,
                                   INT4 i4FsMIRtmCommonRouteTos,
                                   UINT4 u4FsMIRtmCommonRouteNextHop,
                                   INT4 i4TestValFsMIRtmCommonRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (UtilRtmSetContext ((UINT4) i4FsMIRtmContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FsRtmCommonRouteStatus (pu4ErrorCode, u4FsMIRtmCommonRouteDest,
                                         u4FsMIRtmCommonRouteMask,
                                         i4FsMIRtmCommonRouteTos,
                                         u4FsMIRtmCommonRouteNextHop,
                                         i4TestValFsMIRtmCommonRouteStatus);
    UtilRtmResetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRtmCommonRouteTable
 Input       :  The Indices
                FsMIRtmContextId
                FsMIRtmCommonRouteDest
                FsMIRtmCommonRouteMask
                FsMIRtmCommonRouteTos
                FsMIRtmCommonRouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIRtmCommonRouteTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIRtmRedEntryTime
 Input       :  The Indices

                The Object 
                retValFsMIRtmRedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmRedEntryTime (INT4 *pi4RetValFsMIRtmRedEntryTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsRtmRedEntryTime (pi4RetValFsMIRtmRedEntryTime);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmRedExitTime
 Input       :  The Indices

                The Object 
                retValFsMIRtmRedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIRtmRedExitTime (INT4 *pi4RetValFsMIRtmRedExitTime)
{
    INT1                i1Return = SNMP_FAILURE;

    i1Return = nmhGetFsRtmRedExitTime (pi4RetValFsMIRtmRedExitTime);

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIRtmMaximumBgpRoutes
 Input       :  The Indices

                The Object
                retValFsMIRtmMaximumBgpRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtmMaximumBgpRoutes(UINT4 *pu4RetValFsMIRtmMaximumBgpRoutes)
{

 return nmhGetFsRtmMaximumBgpRoutes(pu4RetValFsMIRtmMaximumBgpRoutes);

}
/****************************************************************************
 Function    :  nmhGetFsMIRtmMaximumOspfRoutes
 Input       :  The Indices

                The Object
                retValFsMIRtmMaximumOspfRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtmMaximumOspfRoutes(UINT4 *pu4RetValFsMIRtmMaximumOspfRoutes)
{

  return nmhGetFsRtmMaximumOspfRoutes(pu4RetValFsMIRtmMaximumOspfRoutes);   

}
/****************************************************************************
 Function    :  nmhGetFsMIRtmMaximumRipRoutes
 Input       :  The Indices

                The Object
                retValFsMIRtmMaximumRipRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtmMaximumRipRoutes(UINT4 *pu4RetValFsMIRtmMaximumRipRoutes)
{

  return nmhGetFsRtmMaximumRipRoutes(pu4RetValFsMIRtmMaximumRipRoutes);

}

/****************************************************************************
 Function    :  nmhGetFsMIRtmMaximumStaticRoutes
 Input       :  The Indices

                The Object
                retValFsMIRtmMaximumStaticRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtmMaximumStaticRoutes(UINT4 *pu4RetValFsMIRtmMaximumStaticRoutes)
{

  return nmhGetFsRtmMaximumStaticRoutes(pu4RetValFsMIRtmMaximumStaticRoutes);

}
/****************************************************************************
 Function    :  nmhGetFsMIRtmMaximumISISRoutes
 Input       :  The Indices

                The Object
                retValFsMIRtmMaximumISISRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMIRtmMaximumISISRoutes(UINT4 *pu4RetValFsMIRtmMaximumISISRoutes)
{

   return nmhGetFsRtmMaximumISISRoutes(pu4RetValFsMIRtmMaximumISISRoutes);

}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIRtmMaximumBgpRoutes
 Input       :  The Indices

                The Object
                setValFsMIRtmMaximumBgpRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtmMaximumBgpRoutes(UINT4 u4SetValFsMIRtmMaximumBgpRoutes)
{

  return nmhSetFsRtmMaximumBgpRoutes(u4SetValFsMIRtmMaximumBgpRoutes);

}
/****************************************************************************
 Function    :  nmhSetFsMIRtmMaximumOspfRoutes
 Input       :  The Indices

                The Object
                setValFsMIRtmMaximumOspfRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhSetFsMIRtmMaximumOspfRoutes(UINT4 u4SetValFsMIRtmMaximumOspfRoutes)
{

   return nmhSetFsRtmMaximumOspfRoutes(u4SetValFsMIRtmMaximumOspfRoutes);

}
/****************************************************************************
 Function    :  nmhSetFsMIRtmMaximumRipRoutes
 Input       :  The Indices

                The Object
                setValFsMIRtmMaximumRipRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtmMaximumRipRoutes(UINT4 u4SetValFsMIRtmMaximumRipRoutes)
{

   return nmhSetFsRtmMaximumRipRoutes(u4SetValFsMIRtmMaximumRipRoutes);

}
/****************************************************************************
 Function    :  nmhSetFsMIRtmMaximumStaticRoutes
 Input       :  The Indices

                The Object
                setValFsMIRtmMaximumStaticRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtmMaximumStaticRoutes(UINT4 u4SetValFsMIRtmMaximumStaticRoutes)
{

  return nmhSetFsRtmMaximumStaticRoutes(u4SetValFsMIRtmMaximumStaticRoutes);

}
/****************************************************************************
 Function    :  nmhSetFsMIRtmMaximumISISRoutes
 Input       :  The Indices

                The Object
                setValFsMIRtmMaximumISISRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMIRtmMaximumISISRoutes(UINT4 u4SetValFsMIRtmMaximumISISRoutes)
{

  return nmhSetFsRtmMaximumISISRoutes(u4SetValFsMIRtmMaximumISISRoutes);

}
/* Low Level TEST Routines for All Objects  */
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmMaximumBgpRoutes
 Input       :  The Indices

                The Object
                testValFsMIRtmMaximumBgpRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtmMaximumBgpRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtmMaximumBgpRoutes)
{

   return nmhTestv2FsRtmMaximumBgpRoutes(pu4ErrorCode,u4TestValFsMIRtmMaximumBgpRoutes);

}
/****************************************************************************
 Function    :  nmhTestv2FsMIRtmMaximumOspfRoutes
 Input       :  The Indices

                The Object
                testValFsMIRtmMaximumOspfRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtmMaximumOspfRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtmMaximumOspfRoutes)
{

   return nmhTestv2FsRtmMaximumOspfRoutes(pu4ErrorCode,u4TestValFsMIRtmMaximumOspfRoutes);

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmMaximumRipRoutes
 Input       :  The Indices

                The Object
                testValFsMIRtmMaximumRipRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtmMaximumRipRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtmMaximumRipRoutes)
{

   return nmhTestv2FsRtmMaximumRipRoutes(pu4ErrorCode,u4TestValFsMIRtmMaximumRipRoutes);

}
/****************************************************************************
 Function    :  nmhTestv2FsMIRtmMaximumStaticRoutes
 Input       :  The Indices

                The Object
                testValFsMIRtmMaximumStaticRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtmMaximumStaticRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtmMaximumStaticRoutes)
{

   return nmhTestv2FsRtmMaximumStaticRoutes(pu4ErrorCode,u4TestValFsMIRtmMaximumStaticRoutes);

}

/****************************************************************************
 Function    :  nmhTestv2FsMIRtmMaximumISISRoutes
 Input       :  The Indices

                The Object
                testValFsMIRtmMaximumISISRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMIRtmMaximumISISRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsMIRtmMaximumISISRoutes)
{

   return nmhTestv2FsRtmMaximumISISRoutes(pu4ErrorCode,u4TestValFsMIRtmMaximumISISRoutes);

}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIRtmMaximumBgpRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtmMaximumBgpRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIRtmMaximumOspfRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtmMaximumOspfRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsMIRtmMaximumRipRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtmMaximumRipRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIRtmMaximumStaticRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtmMaximumStaticRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsMIRtmMaximumISISRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMIRtmMaximumISISRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}

