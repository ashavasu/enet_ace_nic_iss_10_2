
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmfilt.c,v 1.16 2015/01/05 12:24:11 siva Exp $
 *
 * Description:This file contains functions which process   
 *             the change in the rrd filter table and       
 *             functions which checks whether a route can be
 *             can be redistributed or not.                 
 *******************************************************************/

#include "rtminc.h"

/*-------------------------------------------------------------------+
 *
 * Function           : RtmCheckRouteForRedistributionInCxt 
 *
 * Input(s)           : pRegnId - Registration ID for protocol from which
 *                                this route is learnt.
 *                      pRtInfo - Pointer to the route in the routing table.
 *                      pRtmCxt - Pointer to the RTM Context                
 *
 * Output(s)          : pu2DestProtoMask - Pointer to the destination protocol
 *                      mask to which this route can be redistributed. 
 *
 * Returns            : None. 
 *
 * Action             : This function handles the addition or deletion of
 *                      in the RRD Control table.  
 *         
+-------------------------------------------------------------------*/
VOID
RtmCheckRouteForRedistributionInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId,
                                     tRtInfo * pRtInfo, UINT2 *pu2DestProtoMask)
{
    tRtmRegnId          DestRegnId;
    UINT2               u2DestProtoMask = 0;
    UINT2               u2RegnId = 0;
    UINT4               u4DefDestNet = 0;

    *pu2DestProtoMask = 0;

    /* Check whether any allow inter/intra area routes is enabled
     * if the route is from OSPF.
     */
    if (pRegnId->u2ProtoId == OSPF_ID)
    {
        /* This route is to be redistributed only to these mib enabled
         * routing protocols.
         */
        for (u2RegnId = pRtmCxt->u2RtmRtStartIndex;
             u2RegnId < MAX_ROUTING_PROTOCOLS;
             u2RegnId = pRtmCxt->aRtmRegnTable[u2RegnId].u2NextRegId)
        {
            if (pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId == OSPF_ID)
            {
                continue;
            }

            DestRegnId.u2ProtoId =
                pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId;
            DestRegnId.u4ContextId = pRtmCxt->u4ContextId;

            /* Set the DestProtoMask for this Routing Protocol.
             * If route is not to be redistributed to this protocol
             * then the mask will be resetted. */
            RTM_SET_BIT (*pu2DestProtoMask,
                         pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId);
            if (RtmCheckForMatch
                (pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask,
                 pRtInfo->i4MetricType) == RTM_FAILURE)
            {
                RTM_CLEAR_BIT (*pu2DestProtoMask,
                               pRtmCxt->aRtmRegnTable[u2RegnId].
                               u2RoutingProtocolId);
            }
            RtmCheckForOspfIntOrExtInCxt (pRtmCxt, &DestRegnId,
                                          pRtInfo->u4RouteTag,
                                          pu2DestProtoMask);
        }

        if (*pu2DestProtoMask == 0)
        {
            /* Route redistribution is not permitted. */
            return;
        }
    }

    if (pRegnId->u2ProtoId == ISIS_ID)
    {
        /* This route is to be redistributed only to these mib enabled
         * routing protocols.
         */
        /* Return if the incoming route is default */
        if (pRtInfo->u4DestNet == u4DefDestNet)
        {
            return;
        }
        for (u2RegnId = pRtmCxt->u2RtmRtStartIndex;
             u2RegnId < MAX_ROUTING_PROTOCOLS;
             u2RegnId = pRtmCxt->aRtmRegnTable[u2RegnId].u2NextRegId)
        {
            if (pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId == ISIS_ID)
            {
                continue;
            }

            if (RtmCheckForLevel
                (pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask,
                 pRtInfo->i4MetricType) == RTM_SUCCESS)
            {
                RTM_SET_BIT (*pu2DestProtoMask,
                         pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId);
            }
        }
		if (*pu2DestProtoMask == 0)
		{ 
			return;
		}
    }

	
    RtmFilterCheckInCxt (pRtmCxt, pRegnId, pRtInfo, &u2DestProtoMask);

    if (u2DestProtoMask != 0)
    {
        RtmUpdateDestMaskInCxt (pRtmCxt, pRtInfo, pRegnId, &u2DestProtoMask);
    }

    if (pRegnId->u2ProtoId == OSPF_ID)
    {
        *pu2DestProtoMask &= u2DestProtoMask;
    }
    else
    {
        *pu2DestProtoMask |= u2DestProtoMask;
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmUpdateDestMaskInCxt
 *
 * Input(s)           : pRtInfo - Pointer to the route in the routing table.
 *                      pSrcRegnId - Info about Source of this route.
 *                      pRtmCxt - Rtm Context pointer              
 *
 * Output(s)          : pu2DestProtoMask - Pointer to the destination protocol
 *                      mask to which this route can be redistributed. 
 *
 * Returns            : None. 
 *
 * Action             : This function handles the filtering based on the
 *                      tag and AS number.
 *         
+-------------------------------------------------------------------*/
VOID
RtmUpdateDestMaskInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRtInfo,
                        tRtmRegnId * pSrcRegnId, UINT2 *pu2DestProtoMask)
{
    tRtmRegnId          DestRegnId;
    UINT2               u2RegnIdOfRp = 0;

    switch (pSrcRegnId->u2ProtoId)
    {
        case CIDR_STATIC_ID:
            /* no need to check for any more thing */
            break;

        case RIP_ID:
            /* Check for TAG values */
            if (pRtmCxt->RtmConfigInfo.u1RrdFilterByOspfTag ==
                RTM_FILTER_ENABLED)
            {
                /* Check the subfields of the TAG */

                if ((pRtInfo->u4RouteTag &
                     pRtmCxt->RtmConfigInfo.u4OspfTagMask) ==
                    (pRtmCxt->RtmConfigInfo.u4OspfTagValue &
                     pRtmCxt->RtmConfigInfo.u4OspfTagMask))
                {
                    /* This route should not be exported to any one else */
                    *pu2DestProtoMask = 0;
                }

            }
            break;

        case OSPF_ID:
            /* Check for filtering based on OSPF TAG */
            if (pRtmCxt->RtmConfigInfo.u1RrdFilterByOspfTag ==
                RTM_FILTER_ENABLED)
            {
                /* Check the subfields of the TAG */

                if ((pRtInfo->u4RouteTag &
                     pRtmCxt->RtmConfigInfo.u4OspfTagMask) ==
                    (pRtmCxt->RtmConfigInfo.u4OspfTagValue &
                     pRtmCxt->RtmConfigInfo.u4OspfTagMask))
                {
                    /* This route should not be exported to any one else */
                    *pu2DestProtoMask = 0;
                }
                break;
            }

            /* check for the filtering based on OSPF internal or external 
               routes */
            /* OSPF routes can be redistributed to RIP and BGP only */
            if (*pu2DestProtoMask & RTM_RIP_MASK)
            {
                DestRegnId.u2ProtoId = RIP_ID;
                DestRegnId.u4ContextId = pRtmCxt->u4ContextId;
                u2RegnIdOfRp = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt,
                                                               &DestRegnId);
                if (u2RegnIdOfRp == RTM_INVALID_REGN_ID)
                {
                    /* RIP not registered. No need to redistribute. */
                    *pu2DestProtoMask =
                        (UINT2) (*pu2DestProtoMask & ~RTM_RIP_MASK);
                }
            }                    /* RIP_MASK */

            if (*pu2DestProtoMask & RTM_BGP_MASK)
            {
                if ((((pRtInfo->u4RouteTag) & (RTM_LEFT_MOST_NIBBLE_TAG_MASK))
                     == RTM_BGP_DENY_TAG1) ||
                    (((pRtInfo->u4RouteTag) & (RTM_LEFT_MOST_NIBBLE_TAG_MASK))
                     == RTM_BGP_DENY_TAG2))
                {
                    *pu2DestProtoMask =
                        (UINT2) (*pu2DestProtoMask & ~RTM_BGP_MASK);
                    break;
                }
                DestRegnId.u2ProtoId = BGP_ID;
                DestRegnId.u4ContextId = pRtmCxt->u4ContextId;
                u2RegnIdOfRp = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt,
                                                               &DestRegnId);
                if (u2RegnIdOfRp == RTM_INVALID_REGN_ID)
                {
                    /* BGP not registered. No need to redistribute. */
                    *pu2DestProtoMask = (UINT2)
                        (*pu2DestProtoMask & ~RTM_BGP_MASK);
                }
            }                    /* BGP_MASK */

            break;

        case BGP_ID:
            /* Check for the BIT MASK received from BGP */
            /* If a route with bitmask = 0x80 is received from BGP, then this
               route should not be redistributed to other RP's. This route is
               filtered based on AS number */

            if (pRtmCxt->u1RtmIBgpRedistribute == RTM_REDISTRIBUTION_DISABLED)
            {
                if (pRtInfo->u1BitMask & RTM_FILTER_BASED_ON_AS_MASK)
                {
                    *pu2DestProtoMask = 0;
                }
            }
            break;

        default:
            break;

    }                            /* End of SWITCH */
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmCheckForOspfIntOrExtInCxt
 *
 * Input(s)           : pRegnId - Registration Info of the routing protocol
 *                      to which this route can be exported or not.
 *                      u4Tag        - Value of the route tag.
 *                      pRtmCxt - Pointer to the RTM Context                
 *
 * Output(s)          : *pu2DestProtoMask - Clears the bit if this route should
 *                      not be redistributed. 
 *
 * Returns            : None. 
 *
 * Action             : This function updates the mask and filters destinations
 *                      based on OSPF internal or external.
 *         
+-------------------------------------------------------------------*/
VOID
RtmCheckForOspfIntOrExtInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId,
                              UINT4 u4Tag, UINT2 *pu2DestProtoMask)
{
    UINT2               u2RegnIdOfRp = 0;
    UINT2               u2Mask = 0;

    u2RegnIdOfRp = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);

    if (u2RegnIdOfRp != RTM_INVALID_REGN_ID)
    {
        RTM_SET_BIT (u2Mask,
                     pRtmCxt->aRtmRegnTable[u2RegnIdOfRp].u2RoutingProtocolId);

        if (u4Tag == 0)
        {
            /* This is a OSPF internal route */
            /* Check whether internal routes are allowed to this RP or Not */
            if (pRtmCxt->aRtmRegnTable[u2RegnIdOfRp].u1AllowOspfInternals ==
                RTM_REDISTRIBUTION_DISABLED)
            {
                /* This RP dont want OSPF inter area or intra area routes */
                *pu2DestProtoMask = (UINT2) (*pu2DestProtoMask & ~u2Mask);
            }
        }
        else
        {
            /* This is OSPF external route */
            if (pRtmCxt->aRtmRegnTable[u2RegnIdOfRp].u1AllowOspfExternals ==
                RTM_REDISTRIBUTION_DISABLED)
            {
                /* This RP dont want OSPF external type 1 or type 2 routes */
                *pu2DestProtoMask = (UINT2) (*pu2DestProtoMask & ~u2Mask);
            }
        }
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmFilterCheckInCxt
 *
 * Input(s)           : pSrcRegnId - Registration Info about the Routing Protocol
 *                                   from which this route is learnt.
 *                      pRtInfo - Pointer to the route info structure.
 *                      pRtmCxt - Pointer to the RTM Context                
 *
 * Output(s)          : *pu2DestProtoMask - Bit mask of the routing protocols
 *                      to which this route can be redistributed. 
 *
 * Returns            : None. 
 *
 * Action             : This function checks the filters configured by the     
 *                      administrator in the RTM Control table and allows
 *                      or denies this route to the routing protocols based on
 *                      the configuration.
 *         
+-------------------------------------------------------------------*/
VOID
RtmFilterCheckInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pSrcRegnId,
                     tRtInfo * pRtInfo, UINT2 *pu2DestProtoMask)
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    UINT4               u4Range = 0;
    UINT4               u4Mask = 0;

    *pu2DestProtoMask = 0;

    /* All entries in the Control Table are sorted based on DestNet, in
     * decending order. So if the given route matches with any entry, just
     * return because this will be the longest possible prefix match and no
     * need to process any other entry.
     */
    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        if (pRtmCtrlInfo->u1RowStatus == RTM_ACTIVE)
        {
            /* See whether the destination net occurs in the specified range
               or not */
            if (pRtmCtrlInfo->u4DestNet == IP_ANY_ADDR)
            {
                u4Range = IP_GEN_BCAST_ADDR;
                u4Mask = IP_ANY_ADDR;
            }
            else
            {
                u4Range = ~(pRtmCtrlInfo->u4SubnetRange);
                u4Mask = pRtmCtrlInfo->u4SubnetRange;
            }

            if ((pRtInfo->u4DestNet >= pRtmCtrlInfo->u4DestNet) &&
                ((pRtInfo->u4DestNet - pRtmCtrlInfo->u4DestNet) <=
                 pRtmCtrlInfo->u4SubnetRange))
            {
                /* Route occurs in the specified range */
                /* Check whether source protocol ID in the filter is any 
                   protocol (0) or the specified protocol */
                if ((pSrcRegnId->u2ProtoId == pRtmCtrlInfo->u1SrcProtocolId) ||
                    (pRtmCtrlInfo->u1SrcProtocolId == RTM_ANY_PROTOCOL))
                {
                    /* Set the Destination Protocol Mask appropriately */
                    if (pRtmCtrlInfo->u1RtExportFlag == RTM_ROUTE_PERMIT)
                    {
                        if (pRtmCtrlInfo->u2DestProtocolMask ==
                            RTM_ANY_PROTOCOL)
                        {
                            *pu2DestProtoMask |= RTM_ALL_RPS_MASK;
                            RTM_CLEAR_BIT (*pu2DestProtoMask,
                                           pSrcRegnId->u2ProtoId);
                        }
                        else
                        {
                            *pu2DestProtoMask |=
                                pRtmCtrlInfo->u2DestProtocolMask;
                            RTM_CLEAR_BIT (*pu2DestProtoMask,
                                           pSrcRegnId->u2ProtoId);
                        }
                    }
                    else
                    {
                        /* Set Destination Protocol Mask appropriately */
                        if (pRtmCtrlInfo->u2DestProtocolMask ==
                            RTM_ANY_PROTOCOL)
                        {
                            *pu2DestProtoMask = 0;
                        }
                        else
                        {
                            *pu2DestProtoMask = RTM_ALL_RPS_MASK;
                            *pu2DestProtoMask =
                                (UINT2) (*pu2DestProtoMask &
                                         (~pRtmCtrlInfo->u2DestProtocolMask));
                            RTM_CLEAR_BIT (*pu2DestProtoMask,
                                           pSrcRegnId->u2ProtoId);
                        }
                    }
                    /* Longest Prefix Match is found. */
                    return;
                }                /* Source Protocol ID match */
            }
        }                        /* ACTIVE/DELETE IN-PROGRESS ENTRY */
    }                            /* Scan thru the filter list */
    UNUSED_PARAM (u4Mask);
    UNUSED_PARAM (u4Range);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleChgInDefMode
 *
 * Input(s)           : pNewFilter - Pointer to the changed filter.
 *                      u1Status   - New Status of this entry.
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function handles the change in the mode of the
 *                      RRD Control table.  
 *         
+-------------------------------------------------------------------*/
VOID
RtmHandleChgInDefMode (tRrdControlInfo * pNewFilter, UINT1 u1Status)
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;

    TMO_SLL_Scan (&(pNewFilter->pRtmCxt->RtmCtrlList), pRtmCtrlInfo,
                  tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == pNewFilter->u4DestNet)
            && (pRtmCtrlInfo->u4SubnetRange == pNewFilter->u4SubnetRange))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* Entry is not present */
        return;
    }

    pNewFilter->u1RtExportFlag = u1Status;

    RtmHandleNewEntryToControlTable (pNewFilter);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleChgInFilterTable
 *
 * Input(s)           : pNewFilter - Pointer to the changed filter.
 *                      u1Status   - New Status of this entry.
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function handles the addition or deletion of
 *                      in the RRD Control table.  
 *         
+-------------------------------------------------------------------*/
VOID
RtmHandleChgInFilterTable (tRrdControlInfo * pNewFilter, UINT1 u1Status)
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;

    UNUSED_PARAM (u1Status);
    TMO_SLL_Scan (&(pNewFilter->pRtmCxt->RtmCtrlList), pRtmCtrlInfo,
                  tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == pNewFilter->u4DestNet)
            && (pRtmCtrlInfo->u4SubnetRange == pNewFilter->u4SubnetRange))
        {
            break;
            /* We found the correct node in the list */
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* Entry is not present */
        return;
    }

    if (pNewFilter->u1RowStatus == RTM_ACTIVE)
    {
        RtmHandleNewEntryToControlTable (pNewFilter);
    }
    else if (pNewFilter->u1RowStatus == RTM_DESTROY)
    {
        RtmDeleteAnEntryFromControlTable (pNewFilter);
        /* Dont process the Filter Table entry beyond this point, because
         * it is already freed. */
    }

    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleOspfTypeRouteRedistribution
 *
 * Input(s)           : pRtInfo - Info about the best route to be 
 *                                redistributed.
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : This function is the call back function to 
 *                      handle the route redistribution of the given
 *                      best route when ever there is change in the
 *                      Ospf Route Advt Policy Change.
 *
+-------------------------------------------------------------------*/
INT4
RtmHandleOspfTypeRouteRedistribution (tRtInfo * pRtInfo, VOID *pAppSpecData)
{
    tRtmOspfTypeChg    *pOspfTypeChg = NULL;

    if (pRtInfo->u2RtProto != OSPF_ID)
    {
        return RTM_SUCCESS;
    }

    pOspfTypeChg = (tRtmOspfTypeChg *) pAppSpecData;

    if (pOspfTypeChg->u1OspfRtType == RTM_OSPF_INTERNAL_ROUTES)
    {
        /* Change in OSPF Internal Route policy */
        if (pRtInfo->u4RouteTag != 0)
        {
            /* AS External Route */
            return RTM_SUCCESS;
        }
    }
    else
    {
        /* Change in OSPF External Route policy */
        if (pRtInfo->u4RouteTag == 0)
        {
            /* AS Internal Route */
            return RTM_SUCCESS;
        }
    }

    if (pOspfTypeChg->u1OspfStatus == RTM_REDISTRIBUTION_ENABLED)
    {
        /* New Policy is to advertise */
        RtmHandleRouteUpdatesInCxt (pOspfTypeChg->pRtmCxt, pRtInfo,
                                    TRUE, IP_BIT_ALL);
    }
    else
    {
        /* New Policy is to withdraw. */
        RtmHandleRouteUpdatesInCxt (pOspfTypeChg->pRtmCxt, pRtInfo,
                                    FALSE, IP_BIT_STATUS);
    }

    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleChgInOspfType
 *
 * Input(s)           : pRegnId - Info about Routing protocol in which there is
 *                      a change in the allow of OSPF internal or external
 *                      routes.
 *                      u1OspfType - Indicates whether OSPF internal or
 *                      external.
 *                      u1PermitOrDeny - Indicates redistribution enabled or
 *                      disabled.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : This function will be called whenever there is a change
 *                      in the allow/deny of OSPF routes to this routing 
 *                      protocol. This function calls appropriate routines to
 *                      send appropriate messages. 
 *         
+-------------------------------------------------------------------*/
VOID
RtmHandleChgInOspfType (tRtmRegnId * pRegnId,
                        UINT1 u1OspfType, UINT1 u1PermitOrDeny)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtmOspfTypeChg     OspfTypeChg;
    tRtmCxt            *pRtmCxt = NULL;
    UINT4               au4Indx[IP_TWO];
    UINT4               au4OutIndx[IP_TWO];
    UINT2               u2RegnId = 0;

    pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);

    if (pRtmCxt == NULL)
    {
        return;
    }
    /* The destination protocol ID can be RIP or BGP only */
    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);

    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        return;
    }

    if (u1PermitOrDeny == RTM_REDISTRIBUTION_ENABLED)
    {
        if (u1OspfType == RTM_OSPF_INTERNAL_ROUTES)
        {
            pRtmCxt->aRtmRegnTable[u2RegnId].u1AllowOspfInternals =
                u1PermitOrDeny;
        }
        else
        {
            pRtmCxt->aRtmRegnTable[u2RegnId].u1AllowOspfExternals =
                u1PermitOrDeny;
        }
    }

    OspfTypeChg.u2ProtoId = pRegnId->u2ProtoId;
    OspfTypeChg.u1OspfRtType = u1OspfType;
    OspfTypeChg.u1OspfStatus = u1PermitOrDeny;
    OspfTypeChg.pRtmCxt = pRtmCxt;

    /* This function scans thru the routing table for OSPF routes
       and redistributes the route as per the new policy */
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    au4OutIndx[0] = 0;
    au4OutIndx[1] = 0;

    ROUTE_TBL_LOCK ();
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = (UINT1 *) &(au4OutIndx[0]);

    /* This function will take care of collecting the route to be deleted and
       sending the delete message to RP's */
    IpScanRouteTableForBestRouteInCxt (pRtmCxt, &InParams,
                                       RtmHandleOspfTypeRouteRedistribution, 0,
                                       &OutParams, (VOID *) &OspfTypeChg);
    ROUTE_TBL_UNLOCK ();

    if (u1PermitOrDeny == RTM_REDISTRIBUTION_DISABLED)
    {
        if (u1OspfType == RTM_OSPF_INTERNAL_ROUTES)
        {
            pRtmCxt->aRtmRegnTable[u2RegnId].u1AllowOspfInternals =
                u1PermitOrDeny;
        }
        else
        {
            pRtmCxt->aRtmRegnTable[u2RegnId].u1AllowOspfExternals =
                u1PermitOrDeny;
        }
    }

    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmDeleteAnEntryFromControlTable
 *
 * Input(s)           : pControlTableEntry- Entry to be deleted from the control
 *                      table.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : This function deletes an entry from the control table.
 *                      When the control table is in Permit mode, delete the 
 *                      routes from Export list and inform the routing protocol
 *                      When the control table is in Deny mode, add the routes 
 *                      to export list, picking from CRT.
 *         
+-------------------------------------------------------------------*/
VOID
RtmDeleteAnEntryFromControlTable (tRrdControlInfo * pControlTableEntry)
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;

    /* Delete the entry from the control list before processing
     * the routes. */
    TMO_SLL_Scan (&(pControlTableEntry->pRtmCxt->RtmCtrlList),
                  pRtmCtrlInfo, tRrdControlInfo *)
    {
        if ((pRtmCtrlInfo->u4DestNet == pControlTableEntry->u4DestNet) &&
            (pRtmCtrlInfo->u4SubnetRange == pControlTableEntry->u4SubnetRange))
        {
            TMO_SLL_Delete (&(pControlTableEntry->pRtmCxt->RtmCtrlList),
                            &(pRtmCtrlInfo->pNext));
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* Entry not found. */
        return;
    }

    if (pControlTableEntry->u1RtExportFlag == RTM_ROUTE_PERMIT)
    {
        /* 
         *  Collect the redistributed routes, that falls in this range
         *  and send the delete message to Routing Protocols. 
         */
        RtmFilterRedistributedRoutes (pControlTableEntry);
    }
    else if (pControlTableEntry->u1RtExportFlag == RTM_ROUTE_DENY)
    {
        /* 
         *  Collect the routes, that falls in this range and
         *  if necessary redistribute the route to Routing Protocols. 
         */
        RtmGetRoutesFromCRTToRPs (pControlTableEntry);
    }

    /* Free the control table entry */
    RTM_CONTROL_INFO_FREE (pRtmCtrlInfo);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleNewEntryToControlTable
 *
 * Input(s)           : pNewFilter- Entry to be handled for the control
 *                      table.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : This function adds the routes from CRT to export list
 *                      if the filter is permit mode for Permit table; else 
 *                      deletes the routes fall on this filter, from the export 
 *                      list and informs the routing protocols.
+-------------------------------------------------------------------*/
VOID
RtmHandleNewEntryToControlTable (tRrdControlInfo * pNewFilter)
{
    if (pNewFilter->u1RtExportFlag == RTM_ROUTE_DENY)
    {
        /* Adding a new deny filter. Get the matching routes from RIB and if
         * already redistributed then withdrawn them. */
        RtmFilterRedistributedRoutes (pNewFilter);
    }
    else
    {
        /* Adding a new permit filter. Get the matching routes from RIB and if
         * already not redistributed, redistribute them. */
        RtmGetRoutesFromCRTToRPs (pNewFilter);
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleFilteredRouteRedistribution
 *
 * Input(s)           : pRtInfo - Info about the best route to be 
 *                                redistributed.
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : This function is the call back function to 
 *                      handle the route redistribution of the given
 *                      best route when ever there is change in the
 *                      filter policy.
 *
+-------------------------------------------------------------------*/
INT4
RtmHandleFilteredRouteRedistribution (tRtInfo * pRtInfo, VOID *pAppSpecData)
{
    tRrdControlInfo    *pNewFilter = NULL;
    tRtmRegnId          RegnId;
    UINT4               u4Range = 0;
    UINT4               u4Mask = 0;
    UINT2               u2SrcProtoMask = 0;
    UINT2               u2FilterSrcProtoMask = 0;
    UINT2               u2DestProtoMask = RTM_DONT_REDISTRIBUTE;
    UINT2               u2SendProtoMask = 0;
    UINT2               u2DelProtoMask = 0;

    pNewFilter = (tRrdControlInfo *) pAppSpecData;

    if (pNewFilter->u1SrcProtocolId == RTM_ANY_PROTOCOL)
    {
        u2FilterSrcProtoMask = (UINT2) ~RTM_ANY_PROTOCOL;
    }
    else
    {
        RTM_SET_BIT (u2FilterSrcProtoMask, pNewFilter->u1SrcProtocolId);
    }

    RTM_SET_BIT (u2SrcProtoMask, pRtInfo->u2RtProto);

    if (pNewFilter->u4DestNet == IP_ANY_ADDR)
    {
        u4Range = IP_GEN_BCAST_ADDR;
        u4Mask = IP_ANY_ADDR;
    }
    else
    {
        u4Range = ~(pNewFilter->u4SubnetRange);
        u4Mask = pNewFilter->u4SubnetRange;
    }

    if ((pNewFilter->u4DestNet <= pRtInfo->u4DestNet) &&
        ((pRtInfo->u4DestNet - pNewFilter->u4DestNet) <=
         pNewFilter->u4SubnetRange))
    {
        /* Route matches the configured control entry. */
        /* Need to redistribute this route */
        RegnId.u2ProtoId = pRtInfo->u2RtProto;
        RegnId.u4ContextId = pNewFilter->pRtmCxt->u4ContextId;

        /* Check whether the route can be redistributed. */
        RtmCheckRouteForRedistributionInCxt (pNewFilter->pRtmCxt, &RegnId,
                                             pRtInfo, &u2DestProtoMask);

        /* Reset the Bit Mask corresponding to RPs for whom Route Map is
         * configured, so that the filter updation won't affect the routes 
         routes which are distributed using Route Map Rule */

        RtmResetRouteMapBitMaskInCxt (pNewFilter->pRtmCxt, &u2DestProtoMask);

        if (u2DestProtoMask != RTM_DONT_REDISTRIBUTE)
        {
            /* Route can be redistributed to the protocols specified by
             * u2DestProtoMask. Check whether the route is already 
             * redistributed to these protocols or not. If not redistributed
             * then send it. If already redistributed and cannot be
             * redistributed now, then withdraw it. */
            u2SendProtoMask = (UINT2) (u2DestProtoMask &
                                       (~
                                        (u2DestProtoMask & pRtInfo->
                                         u2RedisMask)));
            u2DelProtoMask =
                (UINT2) (pRtInfo->
                         u2RedisMask &
                         (~(u2DestProtoMask & pRtInfo->u2RedisMask)));
            if (u2SendProtoMask != 0)
            {
                /* Redistribute the route to these protocols */
                RtmRouteRedistributionInCxt (pNewFilter->pRtmCxt, pRtInfo,
                                             TRUE, IP_BIT_ALL, u2SendProtoMask);
            }
            if (u2DelProtoMask != 0)
            {
                /* Withdraw the route from these protocols */
                RtmRouteRedistributionInCxt (pNewFilter->pRtmCxt, pRtInfo,
                                             FALSE, IP_BIT_ALL, u2DelProtoMask);
            }
        }
        else
        {
            /* Route need not be redistributed as per current Control Table.
             * Check whether the route is already redistributed to any other
             * protocol. If yes, the withdraw them. */
            if (pRtInfo->u2RedisMask != 0)
            {
                RtmRouteRedistributionInCxt (pNewFilter->pRtmCxt, pRtInfo,
                                             FALSE, IP_BIT_ALL,
                                             pRtInfo->u2RedisMask);
            }
        }
    }

    UNUSED_PARAM (u4Range);
    UNUSED_PARAM (u4Mask);
    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmGetRoutesFromCRTToRPs
 *
 * Input(s)           : pNewFilter- Control table Entry used to get 
 *                      the routes falls in this range, from CRT table.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : Gets the routes falling in the filter and adds to 
 *                      the export list.
+-------------------------------------------------------------------*/
VOID
RtmGetRoutesFromCRTToRPs (tRrdControlInfo * pNewFilter)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];
    UINT4               au4OutIndx[IP_TWO];

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    au4OutIndx[0] = 0;
    au4OutIndx[1] = 0;

    /* Take a RTM DS lock */
    ROUTE_TBL_LOCK ();
    InParams.pRoot = pNewFilter->pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = (UINT1 *) &(au4OutIndx[0]);

    /* This function will take care of collecting the route to be deleted and
       sending the delete message to RP's */
    IpScanRouteTableForBestRouteInCxt (pNewFilter->pRtmCxt, &InParams,
                                       RtmHandleFilteredRouteRedistribution, 0,
                                       &OutParams, (VOID *) pNewFilter);
    /* Release the RTM DS lock */
    ROUTE_TBL_UNLOCK ();
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmFilterRedistributedRoutes
 *
 * Input(s)           : pNewFilter- Control table Entry used to delete 
 *                      the routes falls in this range.
 *
 * Output(s)          : None.
 *
 * Returns            : None. 
 *
 * Action             : This function withdraws the redistributed routes
 *                      falls in the range from the respective routing
 *                      protocols.
+-------------------------------------------------------------------*/
VOID
RtmFilterRedistributedRoutes (tRrdControlInfo * pNewFilter)
{
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];
    UINT4               au4OutIndx[IP_TWO];

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    au4OutIndx[0] = 0;
    au4OutIndx[1] = 0;

    ROUTE_TBL_LOCK ();
    InParams.pRoot = pNewFilter->pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.pAppSpecInfo = NULL;
    OutParams.Key.pKey = (UINT1 *) &(au4OutIndx[0]);

    /* This function will take care of collecting the route to be redistributed
     * and sending the update message to RP's */
    IpScanRouteTableForBestRouteInCxt (pNewFilter->pRtmCxt, &InParams,
                                       RtmHandleFilteredRouteRedistribution, 0,
                                       &OutParams, (VOID *) pNewFilter);

    ROUTE_TBL_UNLOCK ();
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmFormLevelOrMatchMask 
 *
 * Input(s)           : u2RedisProtoMask -- Protocol to be Redistributed.
 *
 * Output(s)          : None
 *                     
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE 
 *
 * Action             : This function handles redistribution of best ISIS
 *                      routes for the corresponding Match type in ISIS.
 *                      The Match type can be ISIS Level-1 rotues or Level-2 rotues
+-------------------------------------------------------------------*/
INT4
RtmCheckForLevel (UINT2 u2RedisProtoMask, INT4 i4MetricType)
{
        switch (i4MetricType)
        {
            case IP_ISIS_LEVEL1:

                if ((u2RedisProtoMask & RTM_ISISL1_MASK) == RTM_ISISL1_MASK)
                {
                    return RTM_SUCCESS;
                }
                break;
            case IP_ISIS_LEVEL2:

                if ((u2RedisProtoMask & RTM_ISISL2_MASK) == RTM_ISISL2_MASK)
                {
                    return RTM_SUCCESS;
                }
                break;
            default:
                if (i4MetricType == 0)
                {
                    /* For NON ISIS types i1MetricType will be zero
                     * So Match need not be done */
				return RTM_FAILURE;
                }
                break;
        }
    return RTM_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmCheckForMatch 
 *
 * Input(s)           : u2RedisProtoMask -- Protocol to be Redistributed.
 *
 * Output(s)          : None
 *                     
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE 
 *
 * Action             : This function handles redistribution of best OSPF
 *                      routes for the corresponding Match type in OSPF.
 *                      The Match type can be internal ospf routes or 
 *                      external ospf routes or in the combination 
 *                      of this too.
 *         
+-------------------------------------------------------------------*/
INT4
RtmCheckForMatch (UINT2 u2RedisProtoMask, INT4 i4MetricType)
{
    if ((u2RedisProtoMask & RTM_MATCH_ALL_TYPE) == 0)
    {
        return RTM_SUCCESS;
    }
    else
    {
        switch (i4MetricType)
        {
            case IP_OSPF_INTRA_AREA:
            case IP_OSPF_INTER_AREA:

                if ((u2RedisProtoMask & RTM_MATCH_INT_TYPE) ==
                    RTM_MATCH_INT_TYPE)
                {
                    return RTM_SUCCESS;
                }
                break;

            case IP_OSPF_TYPE_1_EXT:
            case IP_OSPF_TYPE_2_EXT:

                if ((u2RedisProtoMask & RTM_MATCH_EXT_TYPE) ==
                    RTM_MATCH_EXT_TYPE)
                {
                    return RTM_SUCCESS;
                }
                break;

            case IP_OSPF_NSSA_1_EXT:
            case IP_OSPF_NSSA_2_EXT:

                if ((u2RedisProtoMask & RTM_MATCH_NSSA_TYPE) ==
                    RTM_MATCH_NSSA_TYPE)
                {
                    return RTM_SUCCESS;
                }
                break;
            default:
                if (i4MetricType == 0)
                {
                    /* For NON OSPF types i1MetricType will be zero
                     * So Match need not be done */
                    return RTM_SUCCESS;
                }
                break;
        }
    }
    return RTM_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmRedistributeIBgpInternalEnable
 *
 * Input(s)           : u4ContextId.
 *
 * Output(s)          : None
 *
 *
 * Returns            : None
 *
 * Action             : This function triggers the redistribution of 
 *                      the IBGP routes to IGP routing protocols for the  
 *                      routes which are already in the bgp routing table
                        when  bgp redistribute-internal command is given 
+------------------------------------------------------------------*/

VOID
RtmRedistributeIBgpInternalEnable (UINT4 u4ContextId)
{
    tRtmCxt            *pRtmCxt;
    tRrdControlInfo     NewFilter;

    MEMSET (&NewFilter, 0, sizeof (tRrdControlInfo));

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return;
    }
    NewFilter.pRtmCxt = pRtmCxt;
    NewFilter.u4DestNet = IP_ANY_ADDR;
    NewFilter.u4SubnetRange = IP_GEN_BCAST_ADDR;
    NewFilter.u1RtExportFlag = RTM_ROUTE_PERMIT;
    NewFilter.u1RowStatus = RTM_ACTIVE;
    NewFilter.u4RouteTag = 0;
    NewFilter.u1SrcProtocolId = BGP_ID;
    NewFilter.u2DestProtocolMask = RTM_ANY_PROTOCOL;

    RtmGetRoutesFromCRTToRPs (&NewFilter);

}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmRedistributeIBgpInternalDisable
 *
 * Input(s)           : None.
 *
 * Output(s)          : None
 *
 *
 * Returns            : None
 *
 * Action             : This function triggers the  with draw of 
 *                      redistributed IBGP routes from IGP routing protocols
                        when  no bgp redistribute-internal command is given
+------------------------------------------------------------------*/

VOID
RtmRedistributeIBgpInternalDisable (UINT4 u4ContextId)
{

    tRtmCxt            *pRtmCxt;
    tRrdControlInfo     NewFilter;

    MEMSET (&NewFilter, 0, sizeof (tRrdControlInfo));

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return;
    }

    NewFilter.pRtmCxt = pRtmCxt;
    NewFilter.u4DestNet = IP_ANY_ADDR;
    NewFilter.u4SubnetRange = IP_GEN_BCAST_ADDR;
    NewFilter.u1RtExportFlag = RTM_ROUTE_DENY;
    NewFilter.u1RowStatus = RTM_ACTIVE;
    NewFilter.u4RouteTag = 0;
    NewFilter.u1SrcProtocolId = BGP_ID;
    NewFilter.u2DestProtocolMask = RTM_ANY_PROTOCOL;
    RtmGetRoutesFromCRTToRPs (&NewFilter);

}
