/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmmain.c,v 1.66 2017/11/23 14:12:48 siva Exp $
 *
 * Description:This file contains functions which process   
 *             the messages from the routing protocols.     
 *
 *******************************************************************/

#include "rtminc.h"
#include "iss.h"
#include "fsrtmwr.h"
#include "fsmirtwr.h"
#include "rtmred.h"
#include "rtmfrt.h"
tRtmSystemSize      gRtmSystemSize;
                        /* RTM System Size Parameter */
tRtmGlobalInfo      gRtmGlobalInfo;
                        /* RTM Global Structure */

tOsixSemId          gRtmProtoSem = 0;
tOsixSemId          gRouteTblSemId = 0;

UINT1               gu1FIBFlag;    /* Indicates wheteher FIB is full or not */
UINT1               gu1RtmRouteCxtFlag = OSIX_FALSE;    /* Flag indicates Route handling in Data Plane in RTM thread Context */
UINT1               gu1ECMPPRTTimerRunning = RTM_ECMP_TIMER_STOP;    /*Flag that indicates whether the ECMPPRT is running or not */
tRtmRedGlobalInfo   gRtmRedGlobalInfo;
UINT1               gu1EcmpAcrossProtocol = RTM_ECMP_ACROSS_PROTO_ENABLED;

/*-------------------------------------------------------------------+
 *
 * Function           : RtmInit 
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function initialises the global data structures of 
 *                      of RTM.
 *         
+-------------------------------------------------------------------*/
INT4
RtmInit ()
{
    tRtmCxt            *pRtmCxt = NULL;
    tVcmRegInfo         VcmRegInfo;
    UINT4               u4MaxNH = 0;

    /* Create the  RtmProtocol Semaphore */
    if ((OsixCreateSem (RTM_PROTOCOL_SEMAPHORE, 1, 0,
                        &gRtmProtoSem)) != OSIX_SUCCESS)
    {
        return RTM_FAILURE;
    }

    /* Create a Semaphore for Common Routing Table in order to provide a 
     * data struture lock */
    if ((OsixCreateSem (ROUTE_TABLE_SEMA4, 1, 0,
                        &gRouteTblSemId)) != OSIX_SUCCESS)
    {
        return RTM_FAILURE;
    }

    /* Create Timer-Lists for RTM Timers */
    if (TmrCreateTimerList ((const UINT1 *) RTM_MAIN_TASK_NAME,
                            RTM_TMR_EVENT, NULL,
                            &gRtmGlobalInfo.RtmTmrListId) != TMR_SUCCESS)
    {
        return RTM_FAILURE;
    }
    MEMSET (&gRtmGlobalInfo.RtmFrtInfo, 0, sizeof (tRtmFrtInfo));
    RtmTmrSetTimer (&(gRtmGlobalInfo.RtmFRTTmr), RTM_FRT_TIMER,
                    RTM_FRT_TIMER_INTERVAL);
    if (RtmMemInit () == RTM_FAILURE)
    {
        return RTM_FAILURE;
    }
    /*Create the RBTree for PRT */
    u4MaxNH =
        FsRTMSizingParams[MAX_RTM_PEND_NEXTHOP_SIZING_ID].u4PreAllocatedUnits;
    gRtmGlobalInfo.pPRTRBRoot = RBTreeCreate (u4MaxNH, RtmRBComparePNh);

    if (gRtmGlobalInfo.pPRTRBRoot == NULL)
    {
        return RTM_FAILURE;
    }

    /*Create the RBTree for ECMPPRT */
    u4MaxNH =
        FsRTMSizingParams[MAX_RTM_PEND_NEXTHOP_SIZING_ID].u4PreAllocatedUnits;
    gRtmGlobalInfo.pECMPRBRoot = RBTreeCreate (u4MaxNH, RtmRBComparePNh);

    if (gRtmGlobalInfo.pECMPRBRoot == NULL)
    {
        return RTM_FAILURE;
    }

    /*Create the RBTree for Resolved Next Hop Table */
    u4MaxNH =
        FsRTMSizingParams[MAX_RTM_RESLV_NEXTHOP_SIZING_ID].u4PreAllocatedUnits;
    gRtmGlobalInfo.pRNHTRBRoot = RBTreeCreate (u4MaxNH, RtmRBCompareRNh);

    if (gRtmGlobalInfo.pRNHTRBRoot == NULL)
    {
        return RTM_FAILURE;
    }
    u4MaxNH = MAX_NEXTHOP_ROUTES;
    gRtmGlobalInfo.pRouteNHRBRoot =
        RBTreeCreate (u4MaxNH, RtmRBCompareRNhForRoute);
    if (gRtmGlobalInfo.pRouteNHRBRoot == NULL)
    {
        return RTM_FAILURE;
    }

    gRtmGlobalInfo.u4ThrotLimit = RTM_MAX_PROTOMASK_ROUTES;
    gRtmGlobalInfo.u1OneSecTimerStart = FALSE;
    gRtmGlobalInfo.u1OneSecTmrStartReq = FALSE;
    gRtmGlobalInfo.u1EcmpAcrossProtocol = RTM_ECMP_ACROSS_PROTO_ENABLED;
    gRtmGlobalInfo.u4MaxRTMBgpRoute = DEF_RTM_BGP_ROUTE_ENTRIES;
    gRtmGlobalInfo.u4MaxRTMOspfRoute = DEF_RTM_OSPF_ROUTE_ENTRIES;
    gRtmGlobalInfo.u4MaxRTMStaticRoute = DEF_RTM_STATIC_ROUTE_ENTRIES;
    gRtmGlobalInfo.u4MaxRTMRipRoute = DEF_RTM_RIP_ROUTE_ENTRIES;
    gRtmGlobalInfo.u4MaxRTMIsisRoute = DEF_RTM_ISIS_ROUTE_ENTRIES;
    gRtmGlobalInfo.u1VrfRouteLeakStatus = RTM_VRF_ROUTE_LEAK_DISABLED;
    gRtmGlobalInfo.u4BgpRts = IP_ZERO;
    gRtmGlobalInfo.u4RipRts = IP_ZERO;
    gRtmGlobalInfo.u4OspfRts = IP_ZERO;
    gRtmGlobalInfo.u4IsisRts = IP_ZERO;

    TMO_SLL_Init (&(gRtmGlobalInfo.RtmRedisInitList));

#if  defined (SNMP_2_WANTED) || defined (SNMPV3_WANTED)
    RegisterFSRTM ();
    RegisterFSMIRT ();
#endif

    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = RtmVcmCallBackFn;
    VcmRegInfo.u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
    VcmRegInfo.u1ProtoId = RTM_PROTOCOL_ID;
    VcmRegisterHLProtocol (&VcmRegInfo);

    if (RtmCreateCxt (RTM_DEFAULT_CXT_ID) == RTM_FAILURE)
    {
        return RTM_FAILURE;
    }
    pRtmCxt = UtilRtmGetCxt (RTM_DEFAULT_CXT_ID);
    if (pRtmCxt == NULL)
    {
        return RTM_FAILURE;
    }
    if (UtilRtmGetVcmSystemModeExt (RTM_PROTOCOL_ID) == VCM_SI_MODE)
    {
        gRtmGlobalInfo.pRtmCxt = pRtmCxt;
    }

    /* For initializing the redundancy related variables and creating the 
     * queues and timers for redundancy */
    if (RtmRedInitGlobalInfo () == RTM_FAILURE)
    {
        return RTM_FAILURE;
    }
    /* For initializing the NPAPI failed route entry related variables and 
     * table */
    if (RtmFrtInitGlobalInfo () == RTM_FAILURE)
    {
        return RTM_FAILURE;
    }

    return RTM_SUCCESS;
}

/*****************************************************************************
 *
 * Function           : RtmDeInit 
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function reinitialises the global data structures of 
 *                      of RTM.
 *         
*****************************************************************************/

VOID
RtmDeInit ()
{
    /* For deinitializing the redundancy related variables and deleting the 
     * queues and timers used for redundancy */
    RtmRedDeInitGlobalInfo ();
    RtmFrtDeInitGlobalInfo ();

    if (gRtmProtoSem != 0)
    {
        OsixSemDel (gRtmProtoSem);
    }

    if (gRouteTblSemId != 0)
    {
        OsixSemDel (gRouteTblSemId);
    }

    if (gRtmGlobalInfo.RtmTmrListId)
    {
        TmrDeleteTimerList (gRtmGlobalInfo.RtmTmrListId);
    }

    RtmSizingMemDeleteMemPools ();

    if (gRtmGlobalInfo.pRNHTRBRoot != NULL)
    {
        RBTreeDelete (gRtmGlobalInfo.pRNHTRBRoot);
    }

    if (gRtmGlobalInfo.pPRTRBRoot != NULL)
    {
        RBTreeDelete (gRtmGlobalInfo.pPRTRBRoot);
    }

    if (gRtmGlobalInfo.pECMPRBRoot != NULL)
    {
        RBTreeDelete (gRtmGlobalInfo.pECMPRBRoot);
    }
    return;
}

/*****************************************************************************
 *
 * Function           : RtmMemInit 
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : This function Creates the memory pool for RTM
 *         
*****************************************************************************/
INT4
RtmMemInit (VOID)
{
    INT4                i4Ret = OSIX_FAILURE;
    tPNhNode           *pPNhEntry = NULL;
    tRNhNode           *pRNhEntry = NULL;
    tPRtEntry          *pRtEntry = NULL;

    UNUSED_PARAM (pPNhEntry);
    UNUSED_PARAM (pRNhEntry);
    UNUSED_PARAM (pRtEntry);

    i4Ret = RtmSizingMemCreateMemPools ();

    /* Update the mempool identifiers */
    gRtmGlobalInfo.RtmAppSpecInfoPoolId =
        RTMMemPoolIds[MAX_RTM_APP_SPEC_INFO_SIZING_ID];
    gRtmGlobalInfo.RtmCxtPoolId = RTMMemPoolIds[MAX_RTM_CONTEXTS_SIZING_ID];
    gRtmGlobalInfo.RtmRrdCrtlPoolId =
        RTMMemPoolIds[MAX_RTM_RRD_CTRL_INFO_SIZING_ID];
    gRtmGlobalInfo.RtmRtMsgPoolId = RTMMemPoolIds[MAX_RTM_ROUTE_MSG_SIZING_ID];
    gRtmGlobalInfo.RtmRtPoolId =
        RTMMemPoolIds[MAX_RTM_ROUTE_TABLE_ENTRIES_SIZING_ID];
    gRtmGlobalInfo.RtmRedisNodePoolId =
        RTMMemPoolIds[MAX_RTM_REDIST_NODES_SIZING_ID];
    gRtmGlobalInfo.RtmPNextHopPoolId =
        RTMMemPoolIds[MAX_RTM_PEND_NEXTHOP_SIZING_ID];
    gRtmGlobalInfo.RtmRNextHopPoolId =
        RTMMemPoolIds[MAX_RTM_RESLV_NEXTHOP_SIZING_ID];
    gRtmGlobalInfo.RtmURRtPoolId =
        RTMMemPoolIds[MAX_RTM_PEND_RT_ENTRIES_SIZING_ID];
    gRtmGlobalInfo.RtmRedRMPoolId =
        RTMMemPoolIds[MAX_RTM_RM_QUE_SIZE_SIZING_ID];
    gRtmGlobalInfo.RtmRedDynMsgPoolId =
        RTMMemPoolIds[MAX_RTM_DYN_MSG_SIZE_SIZING_ID];
    gRtmGlobalInfo.RtmFrtPoolId = RTMMemPoolIds[MAX_RTM_FRT_SIZING_ID];
    gRtmGlobalInfo.RtmRmapCommunityPoolId =
        RTMMemPoolIds[MAX_RTM_COMM_SIZING_ID];
    if (i4Ret == OSIX_FAILURE)
        return RTM_FAILURE;
    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmInitControlTableToDenyModeInCxt
 *
 * Input(s)           : pRtmCxt - RtmContext pointer
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : This function initialises the Rtm Control table to 
 *                      Deny mode allowing all the routes for RRD except 
 *                      0.0.0.0 route.
 *         
+-------------------------------------------------------------------*/
INT4
RtmInitControlTableToDenyModeInCxt (tRtmCxt * pRtmCxt)
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;

    if (RTM_CONTROL_INFO_ALLOC (pRtmCtrlInfo) == NULL)
    {
        /* Malloc failed */
        return RTM_FAILURE;
    }
    /* Initialise all the variables in the RRD Control info */

    pRtmCtrlInfo->u4DestNet = IP_ANY_ADDR;
    pRtmCtrlInfo->u4SubnetRange = IP_GEN_BCAST_ADDR;
    pRtmCtrlInfo->u1SrcProtocolId = RTM_ANY_PROTOCOL;
    pRtmCtrlInfo->u2DestProtocolMask = RTM_ANY_PROTOCOL;
    pRtmCtrlInfo->u1RtExportFlag = RTM_ROUTE_PERMIT;
    pRtmCtrlInfo->u4RouteTag = 0;
    pRtmCtrlInfo->u1RowStatus = RTM_ACTIVE;
    pRtmCtrlInfo->pRtmCxt = pRtmCxt;

    TMO_SLL_Insert (&(pRtmCxt->RtmCtrlList),
                    (tTMO_SLL_NODE *) NULL, &(pRtmCtrlInfo->pNext));

    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmConfigInitInCxt 
 *
 * Input(s)           : pRtmCxt - RTM Context Pointer 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function initialises the configuration table 
 *                      of RTM Context.
 *         
+-------------------------------------------------------------------*/
VOID
RtmConfigInitInCxt (tRtmCxt * pRtmCxt)
{
    pRtmCxt->u4RouterId = 0;
    pRtmCxt->u2AsNumber = 0;
    pRtmCxt->u1RrdAdminStatus = RTM_ADMIN_STATUS_DISABLED;
}

/*-------------------------------------------------------------------+
 * Function           : RtmStartGRTmr
 *
 * Input(s)           : pRegnID  - Router protcol ID and instance.
 *                      pBuf - Pointer to the incoming Buffer.
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS or RTM_FAILURE
 *
 * Action             : This function initialises and starts the GR 
 *                      Timer data structure for the particular 
 *                      routing protocol instance.
+-------------------------------------------------------------------*/
INT4
RtmStartGRTmr (tRtmRegnId * pRegnId, tIpBuf * pBuf)
{
    tRtmCxt            *pRtmCxt = NULL;
    UINT4               u4RtmGRTimer = 0;
    UINT2               u2RegnId = RTM_INVALID_REGN_ID;

    pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);

    if (pRtmCxt == NULL)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return (RTM_FAILURE);
    }
    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return (RTM_FAILURE);
    }

    if (IP_COPY_FROM_BUF (pBuf, &u4RtmGRTimer, 0,
                          sizeof (u4RtmGRTimer)) != sizeof (u4RtmGRTimer))
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return (RTM_FAILURE);
    }

    IP_RELEASE_BUF (pBuf, FALSE);

    pRtmCxt->aRtmRegnTable[u2RegnId].GRTmr.u4GRTimer = u4RtmGRTimer;

    RtmTmrSetTimer (&(pRtmCxt->aRtmRegnTable[u2RegnId].GRTmr.RtmGRTmr),
                    RTM_GR_TIMER, (INT4) u4RtmGRTimer);

    /* Mark the restarting state of the protocol as restarting */
    pRtmCxt->aRtmRegnTable[u2RegnId].u1RestartState = RTM_IGP_RESTARTING;

    return (RTM_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : RtmStopGRTmrInCxt
 *
 * Input(s)           : pRegnID  - Deregistration Information.
 *                    : pRtmCxt  - RTM Context Pointer.
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS or RTM_FAILURE
 *
 * Action             : This function stops the running GR
 *                      Timer data structure of a particular 
 *                      routing protocol
+-------------------------------------------------------------------*/
INT4
RtmStopGRTmrInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId)
{
    UINT2               u2RegnId = RTM_INVALID_REGN_ID;

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        return (RTM_FAILURE);
    }

    if (TmrStop (gRtmGlobalInfo.RtmTmrListId,
                 &(pRtmCxt->aRtmRegnTable[u2RegnId].GRTmr.RtmGRTmr))
/*                      u1RtCount : Reachable installed route count     */
        != TMR_SUCCESS)
    {
        return (RTM_FAILURE);
    }

    return (RTM_SUCCESS);
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmProcessTick
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None.
 *
 * Action             : This function is RTM work horse function. RTM
 *                      has got many functionalities to be performed
 *                      which will hog the CPU for a long time. Such
 *                      functions should be broken up such that the
 *                      whole functionality should be performed in
 *                      small chuncks and thus avoiding CPU hog. These
 *                      function can be performed as a part of the
 *                      RTM Workhorse functions.
 *         
+-------------------------------------------------------------------*/
VOID
RtmProcessTick (VOID)
{
    tRtmRedisNode      *pNode = NULL;
    tRtmRedisNode      *pTmpNode = NULL;
    tRtmRegnId          RegnId;
    UINT2               u2RegnId = 0;
    tRtmCxt            *pRtmCxt = NULL;

    /* The various task that needs to be throttled are listed below:
     * 1) ProtoMask Enable or Disable */

    /* Process the ProtoMask Enable/Disable Event. */
    RTM_SLL_DYN_Scan (&gRtmGlobalInfo.RtmRedisInitList, pNode, pTmpNode,
                      tRtmRedisNode *)
    {
        pRtmCxt = UtilRtmGetCxt (pNode->RegId.u4ContextId);

        if (pRtmCxt == NULL)
        {
            TMO_SLL_Delete (&gRtmGlobalInfo.RtmRedisInitList,
                            &(pNode->NextNode));
            RTM_REDIST_NODE_FREE (pNode);
        }
        else
        {
            if (RtmProcessProtoMaskEventInCxt (pRtmCxt, pNode) == RTM_COMPLETE)
            {
                /* Completed processing the ProtoMask Event for this protocol.
                 * Remove the node from the list and reset the status of the
                 * protocol registration entry. */
                RegnId = pNode->RegId;
                TMO_SLL_Delete (&gRtmGlobalInfo.RtmRedisInitList,
                                &(pNode->NextNode));
                RTM_REDIST_NODE_FREE (pNode);

                if ((u2RegnId =
                     RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt,
                                                     &RegnId)) !=
                    RTM_INVALID_REGN_ID)
                {
                    pRtmCxt->aRtmRegnTable[u2RegnId].u1State = RTM_MASK_STABLE;
                }
            }
            else
            {
                /* Starting the one-second timer */
                gRtmGlobalInfo.u1OneSecTmrStartReq = TRUE;
            }
        }
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmAddRedisEvent
 *
 * Input(s)           : pRegnId - Registriation Id for protocol for which
 *                                redistribution to be performed.
 *                      u4ProtoMask - Protocol Mask to be enabled or
 *                                    disabled.
 *                      u4Event - Enable or Disable Event.
 *                      pRtmCxt - Rtm Context Pointer
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS or RTM_FAILURE
 *
 * Action             : This function will update the Redistribution list
 *                      for processing the given request.
 *         
+-------------------------------------------------------------------*/
INT4
RtmAddRedisEventInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId,
                       UINT4 u4ProtoMask, UINT4 u4Event)
{
    tRtmRedisNode      *pNode = NULL;
    UINT4               u4PendMask = 0;
    UINT2               u2RegnId = 0;

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);

    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        return RTM_FAILURE;
    }

    if (pRtmCxt->aRtmRegnTable[u2RegnId].u1State == RTM_MASK_STABLE)
    {
        /* Allocate the Redistribute node and add it to the Redistribution
         * Handling List. Also reset the RTM_MASK_STABLE flag */
        if (RTM_REDIST_NODE_ALLOC (pNode) == NULL)
        {
            return RTM_FAILURE;
        }
        TMO_SLL_Init_Node (&pNode->NextNode);
        pNode->RegId.u2ProtoId = pRegnId->u2ProtoId;
        pNode->RegId.u4ContextId = pRtmCxt->u4ContextId;
        TMO_SLL_Add (&gRtmGlobalInfo.RtmRedisInitList, &(pNode->NextNode));
        pRtmCxt->aRtmRegnTable[u2RegnId].u1State = 0;

        if (u4Event == RTM_ENABLE_PROTO_MASK)
        {
            pRtmCxt->aRtmRegnTable[u2RegnId].u1State =
                RTM_MASK_ENABLE_INPROGRESS;
            pNode->u4EnableProtoMask = u4ProtoMask;
            pNode->u1Event = RTM_ENABLE_PROTO_MASK;
        }
        else
        {
            pRtmCxt->aRtmRegnTable[u2RegnId].u1State =
                RTM_MASK_DISABLE_INPROGRESS;
            pNode->u4DisableProtoMask = u4ProtoMask;
            pNode->u1Event = RTM_DISABLE_PROTO_MASK;
        }
    }
    else
    {
        /* Already Redistribution is in progress for some event. Try and get
         * the node from the list. */
        TMO_SLL_Scan (&gRtmGlobalInfo.RtmRedisInitList, pNode, tRtmRedisNode *)
        {
            if ((pNode->RegId.u2ProtoId == pRegnId->u2ProtoId) &&
                (pNode->RegId.u4ContextId == pRegnId->u4ContextId))
            {
                break;
            }
        }

        if (pNode == NULL)
        {
            /* some error - Node should present in list but not present. */
            return RTM_FAILURE;
        }

        if (u4Event == RTM_ENABLE_PROTO_MASK)
        {
            if ((pNode->u4EnableProtoMask & u4ProtoMask) == u4ProtoMask)
            {
                /* Protocol Mask already enabled and operation in progress
                 * so no need for any operation. */
                return RTM_SUCCESS;
            }

            /* If any protocol is pending for enable, no need to process it. */
            u4ProtoMask &= ~pNode->u4EnablePendProtoMask;

            /* Check whether Disabling is happening for any of these protocols.
             * If so, keep the enabling event in pending and process once the
             * the disable event is completed.
             */
            u4PendMask = pNode->u4DisableProtoMask & u4ProtoMask;
            if (u4PendMask != 0)
            {
                /* Disable is going on for these protocol. So keep the enable
                 * event as pending.
                 */
                pNode->u4EnablePendProtoMask |= u4PendMask;
                u4ProtoMask &= ~u4PendMask;
            }

            if (u4ProtoMask == 0)
            {
                /* No protocol needs to be enabled. Probably protocol enable
                 * is pending. */
                return RTM_SUCCESS;
            }

            /* Add the new mask to the enable Mask */
            pNode->u4EnableProtoMask |= u4ProtoMask;

            /* Reset the Enable Init Prefix and Prefix len, so that the 
             * iteration starts all over again. */
            pNode->u4EnaInitDest = 0;
            pNode->u4EnaInitMask = 0;

            /* Set the Enable in progress flag in the registration structure */
            pRtmCxt->aRtmRegnTable[u2RegnId].u1State |=
                RTM_MASK_ENABLE_INPROGRESS;
        }
        else
        {
            if ((pNode->u4DisableProtoMask & u4ProtoMask) == u4ProtoMask)
            {
                /* Protocol Mask already disabled and operation in progress
                 * so no need for any operation. */
                return RTM_SUCCESS;
            }
            u4PendMask = pNode->u4EnablePendProtoMask & u4ProtoMask;
            if (u4PendMask != 0)
            {
                /* Some Pending protocols are disabled. */
                pNode->u4EnablePendProtoMask &= ~u4PendMask;
                u4ProtoMask &= ~u4PendMask;
                if (u4ProtoMask == 0)
                {
                    /* No need for any more operation. */
                    return RTM_SUCCESS;
                }
            }

            /* Check whether Enabling is happening for these protocols.
             * If so stop them. */
            pNode->u4EnableProtoMask &= ~u4ProtoMask;
            if (pNode->u4EnableProtoMask == 0)
            {
                /* No protocol going through Enable event. */
                pRtmCxt->aRtmRegnTable[u2RegnId].u1State =
                    (UINT1) (pRtmCxt->aRtmRegnTable[u2RegnId].
                             u1State & (~RTM_MASK_ENABLE_INPROGRESS));
            }

            /* Add the new mask to the disable Mask */
            pNode->u4DisableProtoMask |= u4ProtoMask;

            /* Reset the Disable Init Prefix and Prefix len, so that the 
             * iteration starts all over again. */
            pNode->u4DisInitDest = 0;
            pNode->u4DisInitMask = 0;

            /* Set the Disable in progress flag in the registration structure */
            pRtmCxt->aRtmRegnTable[u2RegnId].u1State |=
                RTM_MASK_DISABLE_INPROGRESS;
        }
    }

    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmReInitRtInCxt 
 *
 * Input(s)           : pRegnId - Registration Id
 *                    : pRtmCxt - RTM Context pointer
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function reinitialises the specified entry in the 
 *                      registration table of RTM.
 *         
+-------------------------------------------------------------------*/
VOID
RtmReInitRtInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId)
{
    UINT2               u2RegnId = 0;
    UINT2               u2Len = 0;

    u2RegnId = RtmGetFreeIndexInRt (pRegnId);

    if (u2RegnId == MAX_ROUTING_PROTOCOLS)
    {
        /* Registration table is FULL */
        return;
    }
    u2Len =
        (UINT2) (STRLEN (NULL_STR) <
                 RTM_MAX_TASK_NAME_LEN ? STRLEN (NULL_STR) :
                 RTM_MAX_TASK_NAME_LEN - 1);
    STRNCPY (pRtmCxt->aRtmRegnTable[u2RegnId].au1TaskName, NULL_STR, u2Len);
    pRtmCxt->aRtmRegnTable[u2RegnId].au1TaskName[u2Len] = '\0';
    u2Len =
        (UINT2) (STRLEN (NULL_STR) <
                 RTM_MAX_Q_NAME_LEN ? STRLEN (NULL_STR) : RTM_MAX_Q_NAME_LEN -
                 1);
    STRNCPY (pRtmCxt->aRtmRegnTable[u2RegnId].au1QName, NULL_STR, u2Len);
    pRtmCxt->aRtmRegnTable[u2RegnId].au1QName[u2Len] = '\0';
    pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask = 0;
    pRtmCxt->aRtmRegnTable[u2RegnId].u2NextRegId = MAX_ROUTING_PROTOCOLS;
    pRtmCxt->aRtmRegnTable[u2RegnId].u1State = RTM_MASK_STABLE;
    pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId =
        RTM_INVALID_ROUTING_PROTOCOL_ID;
    pRtmCxt->aRtmRegnTable[u2RegnId].u1AllowOspfInternals =
        RTM_REDISTRIBUTION_DISABLED;
    pRtmCxt->aRtmRegnTable[u2RegnId].u1AllowOspfExternals =
        RTM_REDISTRIBUTION_DISABLED;
    pRtmCxt->aRtmRegnTable[u2RegnId].pRtmCxt = pRtmCxt;
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmGetFreeIndexInRt
 *
 * Input(s)           : pRegnId - Registration Information.
 *
 * Output(s)          : None
 *
 * Returns            : Returns a free index / MAX_ROUTING_PROTOCOLS
 *
 * Action             : This function finds a free place in the RTM registration
 *                      table.
 *         
+-------------------------------------------------------------------*/
UINT2
RtmGetFreeIndexInRt (tRtmRegnId * pRegnId)
{
    if ((pRegnId->u2ProtoId > 0)
        && (pRegnId->u2ProtoId <= MAX_ROUTING_PROTOCOLS))
    {
        /* Protocol Id - 1 is used as the registration Id. This
         * will enable faster lookup. */
        return ((UINT2) (pRegnId->u2ProtoId - 1));
    }

    /* Protocol not supported. */
    return MAX_ROUTING_PROTOCOLS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmInitTrieForCurrentAppInCxt 
 *
 * Input(s)           : pRegnId  - Registration Info
 *                    : pRtmCxt  - Rtm Context Pointer
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : This function creates trie instance for the given  
 *                      routing protocol.
 *         
+-------------------------------------------------------------------*/
INT1
RtmInitTrieForCurrentAppInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId)
{
    tCreateParams       RpCreate;
    INT1                i1IpRtTblId = 0;

    RpCreate.u1KeySize = IP_TWO * sizeof (UINT4);    /* key is address + mask */
    RpCreate.u4Type = RTM_CIDR_RT_TYPE_IN_CXT (pRtmCxt);
    RpCreate.u1AppId = (UINT1) pRegnId->u2ProtoId;
    i1IpRtTblId = (INT1) (TrieCreate (&(RpCreate), &(pRtmCxt->pIpRtTblRoot)));

    /* Since TRIE returns APP_ID - 1 we are not storing the ID */
    return ((i1IpRtTblId >= 0) ? (RTM_SUCCESS) : (RTM_FAILURE));

}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmProcessRegistrationInCxt
 *
 * Input(s)           : RegnMsg - Registration message information.
 *                    : pRtmCxt  - Rtm Context Pointer
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS - If the registration is successful
 *                      else other failure values.
 *
 * Action             : This function process the registration message from
 *                      the routing protocols. Sends acknowledgement when the 
 *                      registration is successful.
 *         
+-------------------------------------------------------------------*/
INT4
RtmProcessRegistrationInCxt (tRtmCxt * pRtmCxt, tRtmRegnMsg RegnMsg)
{
    tRtmRegnId          RegnId;
    UINT2               u2RegnId = 0;
    INT1                i1RetVal = 0;
    tRtmRegistrationInfo *pRtmRegInfo = NULL;

    RegnId = RegnMsg.RegnId;

    u2RegnId = RtmGetFreeIndexInRt (&RegnId);
    if (u2RegnId == MAX_ROUTING_PROTOCOLS)
    {
        /* Registration table is FULL */
        return RTM_NO_RESOURCE;
    }
    pRtmRegInfo = &(pRtmCxt->aRtmRegnTable[u2RegnId]);

    if (pRtmRegInfo->u2RoutingProtocolId == RegnMsg.RegnId.u2ProtoId)
    {
        /* Protocol has already been registered */
        return RTM_SUCCESS;
    }

    i1RetVal = RtmInitTrieForCurrentAppInCxt (pRtmCxt, &RegnId);
    if (i1RetVal == RTM_FAILURE)
    {
        /* TRIE init failied for this APP ID */
        RtmReInitRtInCxt (pRtmCxt, &RegnId);
        return RTM_NO_RESOURCE;
    }

    /* Fill the Registration Information. */
    if (STRLEN (RegnMsg.au1TaskName) != 0)
    {
        MEMCPY (pRtmRegInfo->au1TaskName,
                RegnMsg.au1TaskName, RTM_MAX_TASK_NAME_LEN);
    }

    if (STRLEN (RegnMsg.au1QName) != 0)
    {
        MEMCPY (pRtmRegInfo->au1QName, RegnMsg.au1QName, RTM_MAX_Q_NAME_LEN);
    }

    pRtmRegInfo->u4Event = RegnMsg.u4Event;
    pRtmRegInfo->u1BitMask = RegnMsg.u1BitMask;
    pRtmRegInfo->u2RoutingProtocolId = RegnMsg.RegnId.u2ProtoId;
    pRtmRegInfo->DeliverToApplication = RegnMsg.DeliverToApplication;
    if (RegnMsg.u1BitMask == RTM_ACK_REQUIRED)
    {
        /* If route needs to be redistributed to the registering protocol
         * set the OSPF INTERNAL and EXTERNAL route as ENABLE. */
        pRtmRegInfo->u1AllowOspfInternals = RTM_REDISTRIBUTION_ENABLED;
        pRtmRegInfo->u1AllowOspfExternals = RTM_REDISTRIBUTION_ENABLED;
    }

    /* RTM Send REGISTRATION ACKNOWLEDGEMENT to the Routing protocol if the
       RRD Admin status is enabled. */

    if (pRtmCxt->u1RrdAdminStatus == RTM_ADMIN_STATUS_ENABLED)
    {
        if (RtmSendAckToRpInCxt (pRtmCxt, &RegnId) != RTM_SUCCESS)
        {
            /* Failure in sending the ACK message to the RP. So reinit the
               registration table. */
            RtmReInitRtInCxt (pRtmCxt, &RegnId);
            return RTM_ACK_SEND_FAIL;
        }
    }

    if (pRtmCxt->u2RtmRtStartIndex == MAX_ROUTING_PROTOCOLS)
    {
        pRtmCxt->u2RtmRtStartIndex = u2RegnId;
        pRtmCxt->u2RtmRtLastIndex = u2RegnId;
    }
    else if (pRtmCxt->u2RtmRtLastIndex < MAX_ROUTING_PROTOCOLS)
    {
        /* we should not update for the first regn id zero */
        pRtmCxt->aRtmRegnTable[pRtmCxt->u2RtmRtLastIndex].u2NextRegId =
            u2RegnId;
        pRtmCxt->u2RtmRtLastIndex = u2RegnId;
    }

    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleRegnMsg
 *
 * Input(s)           : pBuf - Pointer to the buffer received from the routing 
 *                      protocol.
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function handles the registration message from the  *                      routing protocols. Sends acknowledgement when the 
 *                      registration is successful. 
 *         
+-------------------------------------------------------------------*/
VOID
RtmHandleRegnMsg (tIpBuf * pBuf)
{
    tRtmRegnMsg         RegnMsg;
    tRtmCxt            *pRtmCxt = NULL;

    if (IP_COPY_FROM_BUF (pBuf, &RegnMsg, 0, sizeof (tRtmRegnMsg)) !=
        sizeof (tRtmRegnMsg))
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    IP_RELEASE_BUF (pBuf, FALSE);
    pRtmCxt = UtilRtmGetCxt (RegnMsg.RegnId.u4ContextId);

    if (pRtmCxt != NULL)
    {
        RtmProcessRegistrationInCxt (pRtmCxt, RegnMsg);
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmUtilRegisterInCxt
 *
 * Input(s)           : pRegnId - Registration Info about the protocol
 *                                registering with RTM
 *                      u1BitMask   - Value indicating whether route needs to
 *                                    be redistributed or not.
 *                      SendToApplication - Callback Function for handling
 *                                          message from RTM. 
 *                      pRtmCxt - RTM context Pointer                     
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS - If the registration is successful
 *                      else other failure values.
 *
 * Action             : This function handles the registration by the routing 
 *                      protocols. Sends acknowledgement when the 
 *                      registration is successful. 
 *         
+-------------------------------------------------------------------*/
INT4
RtmUtilRegisterInCxt (tRtmCxt * pRtmCxt,
                      tRtmRegnId * pRegnId,
                      UINT1 u1BitMask,
                      INT4 (*SendToApplication) (tRtmRespInfo * pRespInfo,
                                                 tRtmMsgHdr * RtmHeader))
{
    tRtmRegnMsg         RegnMsg;
    INT4                i4RetVal = 0;

    MEMSET (&RegnMsg, 0, sizeof (tRtmRegnMsg));

    RegnMsg.RegnId = *pRegnId;
    RegnMsg.u1BitMask = u1BitMask;
    RegnMsg.DeliverToApplication = SendToApplication;

    i4RetVal = RtmProcessRegistrationInCxt (pRtmCxt, RegnMsg);
    return i4RetVal;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmUtilDeregister
 *
 * Input(s)           : pRegnId - DeRegistraion Info of the routing protocol
 *                      which sent this DeRegistration message.
 *                      pRtmCxt - RTM context pointer 
 *
 * Output(s)          : Updation of Registration table 
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : This function handles the DeRegistration message  
 *                      from the routing protocols.
 * 
+-------------------------------------------------------------------*/
INT4
RtmUtilDeregister (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId)
{
    UINT2               u2RegnId = 0;
    UINT2               u2Index = 0;
    UINT2               u2PrevIndex = 0;

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        return RTM_FAILURE;
    }

    /* Reinitialise the registration table and update start and last index */
    if (pRtmCxt->u2RtmRtStartIndex == u2RegnId)
    {
        if (pRtmCxt->u2RtmRtLastIndex == u2RegnId)
        {
            pRtmCxt->u2RtmRtLastIndex =
                pRtmCxt->aRtmRegnTable[u2RegnId].u2NextRegId;
        }
        pRtmCxt->u2RtmRtStartIndex =
            pRtmCxt->aRtmRegnTable[u2RegnId].u2NextRegId;
    }
    else
    {
        /* this routing protocol sits in between other routing protocols or it
         * is  at the last */
        for (u2Index = pRtmCxt->u2RtmRtStartIndex,
             u2PrevIndex = pRtmCxt->u2RtmRtStartIndex;
             u2Index < MAX_ROUTING_PROTOCOLS;
             u2Index = pRtmCxt->aRtmRegnTable[u2Index].u2NextRegId)
        {
            if (u2Index == u2RegnId)
            {
                if (pRtmCxt->u2RtmRtLastIndex == u2RegnId)
                {
                    pRtmCxt->u2RtmRtLastIndex = u2PrevIndex;
                }
                pRtmCxt->aRtmRegnTable[u2PrevIndex].u2NextRegId =
                    pRtmCxt->aRtmRegnTable[u2Index].u2NextRegId;
                break;
            }
            u2PrevIndex = u2Index;
        }                        /* For loop */

    }

    /* Clear the Protocol route from TRIE */
    RtmClearProtoRouteFromTrieInCxt (pRtmCxt, pRegnId);

    RtmReInitRtInCxt (pRtmCxt, pRegnId);
    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmSendAckToRpInCxt
 *
 * Input(s)           : pRegnId - Registration ID to which the ACK message has
 *                      to be sent.
 *                      pRtmCxt -RTM Context Pointer
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS - If ACK sent successfully.
 *                      RTM_FAILURE - If ACK Not sent successfully. 
 *
 * Action             : This function sends the registration ack message to the  *                      given routing protocol.
 *         
+-------------------------------------------------------------------*/
INT1
RtmSendAckToRpInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId)
{
    tRtmRespInfo        RtmRespInfo;
    tRtmRegnAckMsg      RtmAckMsg;
    tRtmMsgHdr          MsgHdr;
    UINT2               u2RegnId = 0;

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        return RTM_FAILURE;
    }

    if (!(pRtmCxt->aRtmRegnTable[u2RegnId].u1BitMask & RTM_ACK_REQUIRED))
    {
        return RTM_SUCCESS;
    }

    MEMSET ((UINT1 *) &RtmAckMsg, 0, sizeof (tRtmRegnAckMsg));
    RtmRespInfo.pRegnAck = &RtmAckMsg;

    /* Fill the RTM Registration Ack message. */
    RtmRespInfo.pRegnAck->u2ASNumber = pRtmCxt->u2AsNumber;
    RtmRespInfo.pRegnAck->u2Reserved = 0;
    RtmRespInfo.pRegnAck->u4RouterId = pRtmCxt->u4RouterId;
    RtmRespInfo.pRegnAck->u4ContextId = pRtmCxt->u4ContextId;
    RtmRespInfo.pRegnAck->u4MaxMessageSize = RTM_MAX_MESSAGE_SIZE;

    MsgHdr.RegnId = *pRegnId;
    MsgHdr.u1MessageType = RTM_REGISTRATION_ACK_MESSAGE;
    MsgHdr.u2MsgLen = (UINT2) sizeof (tRtmRegnAckMsg);

    if (RtmEnqueuePktToRpsInCxt (pRtmCxt, &RtmRespInfo,
                                 &MsgHdr, pRegnId) != RTM_SUCCESS)
    {
        return RTM_FAILURE;
    }

    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+

 * Function           : RtmSendRouteAckToRpInCxt
 *
 * Input(s)           : pRtmCxt - RTM context pointer.
 *                      pRegnId - Registration ID to which the ACK message has
 *                      to be send.
 *                      i1Status - Status of updated route send to RTM.
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS - If ACK sent successfully.
 *                      RTM_FAILURE - If ACK Not sent successfully.
 *
 * Action             : This function send route updation Ack to given                                                     *                      routing protocol
 *
-------------------------------------------------------------------*/
INT1
RtmSendRouteAckToRpInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId,
                          tNetIpv4RtInfo * pNetRtInfo, INT4 i4Status)
{
    tRtmRespInfo        RtmRespInfo;
    tRtmRegnAckMsg      RtmAckMsg;
    tNetIpv4RtInfo      RtInfo;
    tRtmMsgHdr          MsgHdr;
    UINT2               u2RegnId = 0;

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        UtlTrcLog (1, 1, "", "ERROR[RTM]: RTM Invalid Registration ID");

        return RTM_FAILURE;
    }
    MEMSET ((UINT1 *) &RtmRespInfo, 0, sizeof (tRtmRespInfo));
    MEMSET ((UINT1 *) &MsgHdr, 0, sizeof (tRtmMsgHdr));

    MEMSET ((UINT1 *) &RtmAckMsg, 0, sizeof (tRtmRegnAckMsg));
    RtmRespInfo.pRegnAck = &RtmAckMsg;

    MEMSET ((UINT1 *) &RtInfo, 0, sizeof (tNetIpv4RtInfo));
    RtmRespInfo.pRtInfo = &RtInfo;

    /* Fill the RTM Registration Ack message. */
    RtmRespInfo.pRtInfo = pNetRtInfo;
    RtmRespInfo.pRegnAck->u2ASNumber = pRtmCxt->u2AsNumber;
    RtmRespInfo.pRegnAck->u2Reserved = 0;
    RtmRespInfo.pRegnAck->u4RouterId = pRtmCxt->u4RouterId;
    RtmRespInfo.pRegnAck->u4ContextId = pRtmCxt->u4ContextId;
    RtmRespInfo.pRegnAck->u4MaxMessageSize = RTM_MAX_MESSAGE_SIZE;
    RtmRespInfo.pRegnAck->i4Status = i4Status;

    MsgHdr.RegnId = *pRegnId;
    MsgHdr.u1MessageType = RTM_ROUTE_UPDATE_ACK_MESSAGE;
    MsgHdr.u2MsgLen = (UINT2) sizeof (tRtmRegnAckMsg);
    if (RtmEnqueuePktToRpsInCxt (pRtmCxt, &RtmRespInfo,
                                 &MsgHdr, pRegnId) != RTM_SUCCESS)
    {
        UtlTrcLog (1, 1, "",
                   "ERROR[RTM]: RTM Failed to send Route update Response to Protocol\n");

        return RTM_FAILURE;
    }

    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleRrdEnableMsg 
 *
 * Input(s)           : pRegnID  - Registration Information.
 *                      pBuf - Pointer to the buffer received from the routing 
 *                      protocol.
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function handles the Route Redistribution Enable
 *                      message from the routing protocols. This function scans
 *                      through the Routing Table, collects the request
 *                      protocol routes and sends the update message to RP's. 
 *         
+-------------------------------------------------------------------*/
VOID
RtmHandleRrdEnableMsg (tRtmRrdMsg * pRtmRrdMsg)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtmRegnId         *pRegnId = NULL;
    tRtmRegnId          RegnId;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + IP_FOUR];
    UINT2               u2RegnId = 0;
    UINT2               u2DestProtoMask = 0;    /* Destination Protocol Mask */
    UINT2               u2TempDestMask = 0;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
    MEMSET (au1RMapName, 0, sizeof (au1RMapName));
    pRegnId = &RegnId;

    MEMCPY (pRegnId, &(pRtmRrdMsg->RegnId), sizeof (tRtmRegnId));
    pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);

    if (pRtmCxt == NULL)
    {
        return;
    }

    u2DestProtoMask = pRtmRrdMsg->u2DestProtoMask;
    STRNCPY (au1RMapName, pRtmRrdMsg->au1RMapName, (sizeof (au1RMapName) - 1));

    /* Get the protocols, registration ID. */
    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        return;
    }

    if (pRtmCxt->aRtmRegnTable[u2RegnId].u1BitMask != RTM_ACK_REQUIRED)
    {
        /* No need for redistributing routes to this protocol. */
        return;
    }

    MEMSET (pRtmCxt->aRtmRegnTable[u2RegnId].au1RMapName, 0,
            (RMAP_MAX_NAME_LEN + IP_FOUR));
    if (STRLEN (au1RMapName) != 0)
    {
        /* Store the Route Map Name in the Registration Table */
        MEMCPY (pRtmCxt->aRtmRegnTable[u2RegnId].au1RMapName, au1RMapName,
                STRLEN (au1RMapName));
    }
    RTM_SET_BIT (u2TempDestMask, pRegnId->u2ProtoId);

    if (((u2TempDestMask & u2DestProtoMask) == u2TempDestMask) ||
        (((u2DestProtoMask &
           pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask) ==
          u2DestProtoMask)))
    {
        /* Invalid protomask or protomask already enabled. */
        return;
    }

    /* Find the difference between the already exported protocol mask and the
       new one becos we would have redistributed routes from some protocols 
       previously */

    u2DestProtoMask = (UINT2) (u2DestProtoMask &
                               ~(pRtmCxt->aRtmRegnTable[u2RegnId].
                                 u2ExportProtoMask));

    /* Update the Destination Protocol mask in the RTM Registration Table */
    if ((u2DestProtoMask & RTM_ISISL1L2_MASK))
    {
        pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask =
            (pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask) |
            (u2DestProtoMask | RTM_ISIS_MASK);
    }
    else
    {
        pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask =
            pRtmCxt->aRtmRegnTable[u2RegnId].
            u2ExportProtoMask | u2DestProtoMask;
    }

    /* Add the protocol Mask to the ProtoMask Redistribution Handle list. */
    RtmAddRedisEventInCxt (pRtmCxt, pRegnId,
                           u2DestProtoMask, RTM_ENABLE_PROTO_MASK);

    /* Check and if necessary start the one second Timer Task */
    if (gRtmGlobalInfo.u1OneSecTimerStart == FALSE)
    {
        /* Timer not running. Start the one second Timer. */
        RtmTmrSetTimer (&(gRtmGlobalInfo.RtmOneSecTmr), RTM_ONESEC_TIMER, 1);
        gRtmGlobalInfo.u1OneSecTimerStart = TRUE;
    }

    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleRrdDisableMsg 
 *
 * Input(s)           : pRegnID  - Deregistration Information.
 *                      pBuf - Pointer to the incoming Buffer.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action             : This function handles the redistribute disable    
 *                      message from the routing protocols.
 * 
+-------------------------------------------------------------------*/
VOID
RtmHandleRrdDisableMsg (tRtmRrdMsg * pRtmRrdMsg)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtmRegnId         *pRegnId = NULL;
    tRtmRegnId          RegnId;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + IP_FOUR];
    UINT2               u2RegnId = 0;
    UINT2               u2DestProtoMask = 0;    /* Destination Protocol Mask */
    UINT2               u2TempDestMask = 0;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
    MEMSET (au1RMapName, 0, sizeof (au1RMapName));
    pRegnId = &RegnId;

    MEMCPY (pRegnId, &(pRtmRrdMsg->RegnId), sizeof (tRtmRegnId));
    pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);

    if (pRtmCxt == NULL)
    {
        return;
    }

    u2DestProtoMask = pRtmRrdMsg->u2DestProtoMask;
    STRNCPY (au1RMapName, pRtmRrdMsg->au1RMapName, (sizeof (au1RMapName) - 1));

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        return;
    }

    RTM_SET_BIT (u2TempDestMask, pRegnId->u2ProtoId);

    if ((u2TempDestMask & u2DestProtoMask) == u2TempDestMask)
    {
        /* Invalid Proto Mask */
        return;
    }

    /* Find  the already enabled exported protocol mask and the
       disable only those protocols */
    u2DestProtoMask = u2DestProtoMask &
        (pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask);

    if (u2DestProtoMask == 0)
    {
        /* Protocols are not enabled earlier. */
        return;
    }

    /* Update the Destination Protocol mask in the RTM Registration Table */
    pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask = (UINT2)
        (pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask & ~u2DestProtoMask);

    /*ISIS level1 and Level2 both unmasked, so unmask ISIS_MASK */
    if (!
        (pRtmCxt->aRtmRegnTable[u2RegnId].
         u2ExportProtoMask & RTM_ISISL1L2_MASK))
    {
        pRtmCxt->aRtmRegnTable[u2RegnId].u2ExportProtoMask =
            (UINT2) (pRtmCxt->aRtmRegnTable[u2RegnId].
                     u2ExportProtoMask & ~(RTM_ISIS_MASK));
    }
    if (au1RMapName[0] == '\0')
    {
        MEMSET (pRtmCxt->aRtmRegnTable[u2RegnId].au1RMapName, 0,
                (RMAP_MAX_NAME_LEN + IP_FOUR));
    }
    /* Add the protocol Mask to the ProtoMask Redistribution Handle list. */
    RtmAddRedisEventInCxt (pRtmCxt, pRegnId,
                           u2DestProtoMask, RTM_DISABLE_PROTO_MASK);

    /* Check and if necessary start the one second Timer Task */
    if (gRtmGlobalInfo.u1OneSecTimerStart == FALSE)
    {
        /* Timer not running. Start the one second Timer. */
        RtmTmrSetTimer (&(gRtmGlobalInfo.RtmOneSecTmr), RTM_ONESEC_TIMER, 1);
        gRtmGlobalInfo.u1OneSecTimerStart = TRUE;
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmProcessProtoMaskEventInCxt
 *
 * Input(s)           : pRtmRedisNode - Info about the protomask event
 *                                       to be processed.
 *                      pRtmCxt - RTM Context Pointer
 *
 * Output(s)          : None
 *
 * Returns            : RTM_COMPLETE - If processing is complete
 *                      RTM_MORE - If more processing is to be done
 *
 * Action             : This function process the protomask enable/disable
 *                      event. Then constructs the update message by scanning
 *                      the routing table and sends it to the specified Routing
 *                      Protocol.
 *         
+-------------------------------------------------------------------*/
INT4
RtmProcessProtoMaskEventInCxt (tRtmCxt * pRtmCxt, tRtmRedisNode * pRtmRedisNode)
{
    /* Need to Process Route redistribution */
    tRtmInitRoute       RtmInitInfo;
    UINT2               u2RegnId = 0;
    UINT2               u2RouteCnt = 0;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];
    UINT4               au4OutIndx[IP_TWO];

    u2RouteCnt = (UINT2) (((pRtmRedisNode->u1Event & RTM_ENABLE_PROTO_MASK) &&
                           (pRtmRedisNode->u1Event & RTM_DISABLE_PROTO_MASK))
                          ? gRtmGlobalInfo.u4ThrotLimit / IP_TWO :
                          (gRtmGlobalInfo.u4ThrotLimit));
    if (pRtmRedisNode->u1Event & RTM_ENABLE_PROTO_MASK)
    {
        au4Indx[0] = pRtmRedisNode->u4EnaInitDest;
        au4Indx[1] = pRtmRedisNode->u4EnaInitMask;

        ROUTE_TBL_LOCK ();
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        InParams.i1AppId = ALL_ROUTING_PROTOCOL;
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        au4OutIndx[0] = 0;
        au4OutIndx[1] = 0;
        OutParams.pAppSpecInfo = NULL;
        OutParams.Key.pKey = (UINT1 *) &(au4OutIndx[0]);

        u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt,
                                                   &pRtmRedisNode->RegId);
        if (u2RegnId == RTM_INVALID_REGN_ID)
        {
            ROUTE_TBL_UNLOCK ();
            return RTM_FAILURE;
        }

        /* Scan through the routing table and redistribute the routes to the
         * specified protocol.
         */
        RtmInitInfo.u2Data = (UINT2) pRtmRedisNode->u4EnableProtoMask;
        RtmInitInfo.u2DestProto = pRtmRedisNode->RegId.u2ProtoId;
        RtmInitInfo.u2Event = RTM_ENABLE_PROTO_MASK;
        RtmInitInfo.pRtmCxt = pRtmCxt;

        IpScanRouteTableForBestRouteInCxt (pRtmCxt, &InParams,
                                           RtmHandleExportMaskEvent,
                                           u2RouteCnt,
                                           &OutParams, (VOID *) &(RtmInitInfo));

        ROUTE_TBL_UNLOCK ();
        if ((au4OutIndx[0] == 0) && (au4OutIndx[1] == 0))
        {
            /* Finished with protocol enable event. */
            pRtmRedisNode->u4EnaInitDest = 0;
            pRtmRedisNode->u4EnaInitMask = 0;
            pRtmRedisNode->u1Event =
                (UINT1) (pRtmRedisNode->u1Event & (~RTM_ENABLE_PROTO_MASK));
            pRtmRedisNode->u4EnableProtoMask = 0;
        }
        else
        {
            /* Update the Enable Init Prefix Info */
            pRtmRedisNode->u4EnaInitDest = au4OutIndx[0];
            pRtmRedisNode->u4EnaInitMask = au4OutIndx[1];
        }
    }

    if (pRtmRedisNode->u1Event & RTM_DISABLE_PROTO_MASK)
    {
        au4Indx[0] = pRtmRedisNode->u4DisInitDest;
        au4Indx[1] = pRtmRedisNode->u4DisInitMask;

        ROUTE_TBL_LOCK ();
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        InParams.i1AppId = ALL_ROUTING_PROTOCOL;
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        au4OutIndx[0] = 0;
        au4OutIndx[1] = 0;
        OutParams.pAppSpecInfo = NULL;
        OutParams.Key.pKey = (UINT1 *) &(au4OutIndx[0]);

        u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt,
                                                   &pRtmRedisNode->RegId);
        if (u2RegnId == RTM_INVALID_REGN_ID)
        {
            ROUTE_TBL_UNLOCK ();
            return RTM_FAILURE;
        }

        /* Scan through the routing table and redistribute the routes to the
         * specified protocol.
         */
        RtmInitInfo.u2Data = (UINT2) pRtmRedisNode->u4DisableProtoMask;
        RtmInitInfo.u2DestProto = pRtmRedisNode->RegId.u2ProtoId;
        RtmInitInfo.u2Event = RTM_DISABLE_PROTO_MASK;
        RtmInitInfo.pRtmCxt = pRtmCxt;

        IpScanRouteTableForBestRouteInCxt (pRtmCxt, &InParams,
                                           RtmHandleExportMaskEvent,
                                           u2RouteCnt,
                                           &OutParams, (VOID *) &(RtmInitInfo));
        ROUTE_TBL_UNLOCK ();

        if ((au4OutIndx[0] == 0) && (au4OutIndx[1] == 0))
        {
            /* Finished with protocol disable event. */
            pRtmRedisNode->u4DisInitDest = 0;
            pRtmRedisNode->u4DisInitMask = 0;
            pRtmRedisNode->u1Event =
                (UINT1) (pRtmRedisNode->u1Event & (~RTM_DISABLE_PROTO_MASK));
            pRtmRedisNode->u4DisableProtoMask = 0;
        }
        else
        {
            /* Update the Disable Init Prefix Info */
            pRtmRedisNode->u4DisInitDest = au4OutIndx[0];
            pRtmRedisNode->u4DisInitMask = au4OutIndx[1];
        }
    }

    if (pRtmRedisNode->u1Event == 0)
    {
        if (pRtmRedisNode->u4EnablePendProtoMask == 0)
        {
            /* Route redistribution complete */
            return RTM_COMPLETE;
        }
        else
        {
            /* Some protocols are pending for Enable Event. Handle it. */
            pRtmRedisNode->u1Event = RTM_ENABLE_PROTO_MASK;
            pRtmRedisNode->u4EnableProtoMask |=
                pRtmRedisNode->u4EnablePendProtoMask;
            pRtmRedisNode->u4EnablePendProtoMask = 0;
        }
    }

    return RTM_MORE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmRouteRedistributionInCxt
 *
 * Input(s)           : pRtUpdate - Info about best Route to be redistributed.
 *                      u1IsRtBest - Info whether the route is best or not.
 *                      u2ChgBit   - Info about the Route attributes whose
 *                                   value have been changed.
 *                      u2DestProtoMask - Protocols to which the route is
 *                                        either redistributed or withdrawn
 *                      pRtmCxt - Rtm Context Pointer 
 *
 * Output(s)          : Redistributes the route to the registered protocols.
 *
 * Returns            : None
 *
 * Action             : This function will be called whenever the RTM needs to
 *                      redistribute/withdraw routes to specific protocols.
 *                      
+-------------------------------------------------------------------*/
VOID
RtmRouteRedistributionInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRtUpdate,
                             UINT1 u1IsRtBest, UINT2 u2ChgBit,
                             UINT2 u2DestProtoMask)
{
    tNetIpv4RtInfo      RtUpdate;
    tRtmMsgHdr          MsgHdr;
    tRtmRegnId          RegnId;
    UINT1               u1MsgType = RTM_ROUTE_UPDATE_MESSAGE;

    MEMSET (&RtUpdate, 0, (sizeof (tNetIpv4RtInfo)));

    /*In ColdStandby,Stack Ip should not be distributed by 
     * routing protocols as it is meant only for the 
     * internal communication between switches in stacking system*/

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if (pRtUpdate->u4DestNet == RTM_LINK_LOCAL_NETWORK)
        {
            return;
        }
    }

#ifdef ICCH_WANTED
    /* 
     * ICCL IP shall not be redistributed to Protocols.
     * IcchApiIsIcclAddrRedistributable will return OSIX_SUCCESS for  
     * ICCH IP and the same shall not be redistributed. 
     */
    if (OSIX_SUCCESS == IcchApiIsIcclAddrRedistributable (pRtUpdate->u4DestNet))
    {
        return;
    }
#endif
    if (pRtUpdate->pCommunity != NULL)
    {
        /*allocate memnory for the community in netipv4rtinfo*
         *based on the community count of rmap info*/
        if (MALLOC_RTM_COMM_ENTRY (RtUpdate.pCommunity) == NULL)
        {
            /*memory allocation failed */
            return;
        }
        MEMSET (RtUpdate.pCommunity, 0, sizeof (tRtRmapComm));
    }
    if (u2DestProtoMask != RTM_DONT_REDISTRIBUTE)
    {
        RegnId.u2ProtoId = pRtUpdate->u2RtProto;
        RegnId.u4ContextId = pRtmCxt->u4ContextId;
        RTM_COPY_ROUTE_INFO_TO_UPDATE_MSG (pRtUpdate, RtUpdate);
        RtUpdate.u4ContextId = pRtmCxt->u4ContextId;
        RtUpdate.u2ChgBit = u2ChgBit;

        if ((u1IsRtBest == FALSE) || (RtUpdate.u4RowStatus != RTM_ACTIVE))
        {
            /* Route is either not Active or earlier best route but
             * not best route now. Need to withdraw this route from
             * the routing protocols. */
            RtUpdate.u4RowStatus = RTM_DESTROY;
            u1MsgType = RTM_ROUTE_CHANGE_NOTIFY_MESSAGE;
        }
        else
        {
            /* Route is best route and should be redistributed. But route is
             * either redistributed for first time or can be replacement
             * route. Set the message type accordingly. */
            RtUpdate.u4RowStatus = RTM_ACTIVE;
            if (u2ChgBit == IP_BIT_ALL)
            {
                /* All attributes are changed means route is getting
                 * redistributed for the first time */
                u1MsgType = RTM_ROUTE_UPDATE_MESSAGE;
            }
            else
            {
                /* Route is a replacment route. */
                u1MsgType = RTM_ROUTE_CHANGE_NOTIFY_MESSAGE;
            }
        }

        MsgHdr.u1MessageType = u1MsgType;
        MsgHdr.RegnId.u2ProtoId = pRtUpdate->u2RtProto;
        MsgHdr.RegnId.u4ContextId = pRtmCxt->u4ContextId;
        MsgHdr.u2MsgLen = sizeof (tNetIpv4RtInfo);

        RtmDuplicateAndSendToRpsInCxt (pRtmCxt, &MsgHdr, &RtUpdate, &RegnId,
                                       &u2DestProtoMask);

        /* Update the route's redistribution Flag */
        if (RtUpdate.u4RowStatus == RTM_DESTROY)
        {
            pRtUpdate->u2RedisMask =
                (UINT2) (pRtUpdate->u2RedisMask & (~u2DestProtoMask));
        }
        else
        {
            pRtUpdate->u2RedisMask |= u2DestProtoMask;
        }
    }
    /*release memory for the community in RtUpdate i
     *based on the community count of rmap info*/
    if (RtUpdate.pCommunity != NULL)
    {
        IP_RTM_COMM_FREE (RtUpdate.pCommunity);
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleRouteUpdatesInCxt
 *
 * Input(s)           : pRtUpdate - Info about best Route to be redistributed.
 *                      u1IsRtBest - Info whether the route is best or not.
 *                      u2ChgBit   - Info about the Route attributes whose
 *                                   value have been changed.
 *                      pRtmCxt - Rtm Context Pointer 
 *
 * Output(s)          : Redistributes the route to the registered protocols.
 *
 * Returns            : None
 *
 * Action             : This function will be called whenever the RTM receives
 *                      a route that is selected as best route in IP FWD needs
 *                      to be redistributed or the earlier selected best route
 *                      has been removed. This function verifies whether
 *                      the route can be redistributed or not. If yes, then
 *                      the route is redistributed to the registered routing
 *                      protocols.
 *                      
 *NOTE : This function is the basic input function for handling route
         redistribution. If RTM needs to be ported with any other IP stack
         then this function needs to be called for redistributing routes
         to the registered routing protocols.
+-------------------------------------------------------------------*/
VOID
RtmHandleRouteUpdatesInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRtUpdate,
                            UINT1 u1IsRtBest, UINT2 u2ChgBit)
{
    tRtmRegnId          RegnId;
    UINT2               u2DestProtoMask = RTM_DONT_REDISTRIBUTE;
    UINT2               u2RegnIndex = 0;
    UINT2               u2DestMask = 0;
    UINT2               u2SrcProtoMask = 0;

    RTM_SET_BIT (u2SrcProtoMask, pRtUpdate->u2RtProto);
    UNUSED_PARAM (u2SrcProtoMask);

    /* This function will not check whether redistribution is enabled in
       the destination protocol or not. It should be taken care while
       sending the updates. */
    RegnId.u2ProtoId = pRtUpdate->u2RtProto;
    RegnId.u4ContextId = pRtmCxt->u4ContextId;

    /* After deriving the u2DestProtoMask, reset the mask for the RPs where
     * Route Map is enabled. For a particulur RP, filter rule and Route
     * Map rule cannot be applied, Route Map rule superceds the fileter
     * rule */

    RtmCheckRouteForRedistributionInCxt (pRtmCxt, &RegnId,
                                         pRtUpdate, &u2DestProtoMask);

    for (u2RegnIndex = pRtmCxt->u2RtmRtStartIndex;
         u2RegnIndex < MAX_ROUTING_PROTOCOLS;
         u2RegnIndex = pRtmCxt->aRtmRegnTable[u2RegnIndex].u2NextRegId)
    {
        u2DestMask = 0;
        if (pRtmCxt->aRtmRegnTable[u2RegnIndex].u1BitMask != RTM_ACK_REQUIRED)
        {
            /* Protocol does not required any message. */
            continue;
        }

        RTM_SET_BIT (u2DestMask,
                     pRtmCxt->aRtmRegnTable[u2RegnIndex].u2RoutingProtocolId);

        if ((u2DestProtoMask & u2DestMask) != u2DestMask)
        {
            /* Route need not be redistributed to this protocol. */
            /* Hence, no need to check for Route-map configuration */
            continue;
        }

        if (RtmCheckIsRouteMapConfiguredInCxt
            (pRtmCxt, pRtmCxt->aRtmRegnTable[u2RegnIndex].u2RoutingProtocolId)
            != RTM_FAILURE)
        {
            /* Route map exists */
            if ((pRtUpdate->u1BitMask & RTM_PRIV_RT_MASK) != RTM_PRIV_RT_MASK)
            {
                RtmApplyRouteMapRuleAndRedistributeInCxt (pRtmCxt,
                                                          RegnId.u2ProtoId,
                                                          pRtUpdate, u1IsRtBest,
                                                          u2ChgBit,
                                                          u2DestProtoMask);
            }
        }
        else
        {
            if ((u2DestProtoMask != RTM_DONT_REDISTRIBUTE) &&
                ((pRtUpdate->u1BitMask & RTM_PRIV_RT_MASK) != RTM_PRIV_RT_MASK))
            {
                RtmRouteRedistributionInCxt (pRtmCxt, pRtUpdate, u1IsRtBest,
                                             u2ChgBit, u2DestMask);
            }
        }
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmDuplicateAndSendToRpsInCxt
 *
 * Input(s)           : pMsgHdr - Pointer to the Rtm Response message header
 *                      pNetRtInfo - Pointer to the route info to be
 *                      redistributed.
 *                      pSrcRegnId - Information about the Source of this route.
 *                      pu2DestProtoMask - Protocol mask to which the route
 *                                         needs to be sent.
 *                      pRtmCxt - Rtm Context Pointer 
 *
 * Output(s)          : pu2DestProtoMask - Protocol mask to which the route
 *                                         has been sent.
 *
 * Returns            : None.
 *
 * Action             : This function will be called whenever RTM wants to
 *                      send a packet to multiple RP's. This function will 
 *                      duplicate and send the packet to all RP's. This 
 *                      also updates the export list of the routing protocols.
 * 
+-------------------------------------------------------------------*/
VOID
RtmDuplicateAndSendToRpsInCxt (tRtmCxt * pRtmCxt, tRtmMsgHdr * pMsgHdr,
                               tNetIpv4RtInfo * pNetRtInfo,
                               tRtmRegnId * pSrcRegnId, UINT2 *pu2DestProtoMask)
{
    tRtmRegnId          DestRegnId;
    tRtmRespInfo        RtmRespInfo;
    UINT2               u2RegnIndex = 0;
    UINT2               u2SrcProtoMask = 0;
    UINT2               u2DestMask = 0;
    UINT2               u2RedisMask = 0;
    INT1                i1RetVal = 0;

    RTM_SET_BIT (u2SrcProtoMask, pSrcRegnId->u2ProtoId);

    for (u2RegnIndex = pRtmCxt->u2RtmRtStartIndex;
         u2RegnIndex < MAX_ROUTING_PROTOCOLS;
         u2RegnIndex = pRtmCxt->aRtmRegnTable[u2RegnIndex].u2NextRegId)
    {
        DestRegnId.u2ProtoId =
            pRtmCxt->aRtmRegnTable[u2RegnIndex].u2RoutingProtocolId;
        DestRegnId.u4ContextId = pRtmCxt->u4ContextId;
        u2DestMask = 0;

        if (pRtmCxt->aRtmRegnTable[u2RegnIndex].u1BitMask != RTM_ACK_REQUIRED)
        {
            /* Protocol does not required any message. */
            continue;
        }

        /* Route can be redistributed to this protocol. */
        RTM_SET_BIT (u2DestMask,
                     pRtmCxt->aRtmRegnTable[u2RegnIndex].u2RoutingProtocolId);

        if ((*pu2DestProtoMask & u2DestMask) != u2DestMask)
        {
            /* Route need not be redistributed to this protocol. */
            continue;
        }

        if (pRtmCxt->aRtmRegnTable[u2RegnIndex].u2ExportProtoMask &
            u2SrcProtoMask)
        {
            /* Redistribution is enabled for this protocol */
            RtmRespInfo.pRtInfo = pNetRtInfo;
            i1RetVal =
                RtmEnqueuePktToRpsInCxt (pRtmCxt, &RtmRespInfo,
                                         pMsgHdr, &DestRegnId);
            u2RedisMask |= u2DestMask;
        }
    }

    *pu2DestProtoMask = u2RedisMask;
    UNUSED_PARAM (i1RetVal);
    return;
}

/**************************************************************************/
/*   Function Name   : RtmTrieDeleteHandler                               */
/*   Description     : This function is the call back function registered */
/*                     to handle the Trie Delete operation.               */
/*   Input(s)        : pDeleteParams - Pointer to the routes needs to be  */
/*                                     freed.                             */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
RtmTrieDeleteHandler (tDeleteOutParams * pDeleteParams)
{
    tRtInfo           **pRt = NULL;
    UINT2               u2Index = 0;

    /* While calling TrieDelete the pointer to Array is stored as a pointer.
     * So while retrieving consider the Array as pointer to pointer */
    pRt = (tRtInfo **) pDeleteParams->pAppSpecInfo;

    for (u2Index = 0; u2Index < MAX_ROUTING_PROTOCOLS; u2Index++)
    {
        if (pRt[u2Index] != NULL)
        {
            /* Free the route */
            IP_RT_FREE (pRt[u2Index]);
            pRt[u2Index] = NULL;
        }
    }

    return;
}

/**************************************************************************/
/*   Function Name   : RtmClearProtoRouteFromTrie                         */
/*   Description     : This function will remove all the route learnt     */
/*                     from the given protocol from the TRIE.             */
/*   Input(s)        : pRegnID - Protocol Info whose route need to cleared*/
/*                   : pRtmCxt - Rtm Context Pointer                      */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
RtmClearProtoRouteFromTrieInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId)
{
    tInputParams        InParams;
    tDeleteOutParams    DeleteOutParams;
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    INT4                i4OutCome = 0;

    ROUTE_TBL_LOCK ();
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = (INT1) (pRegnId->u2ProtoId - 1);

    DeleteOutParams.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    DeleteOutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    DeleteOutParams.u4NumEntries = MAX_ROUTING_PROTOCOLS;

    i4OutCome = TrieDelete (&InParams, RtmTrieDeleteHandler, &DeleteOutParams);
    ROUTE_TBL_UNLOCK ();
    UNUSED_PARAM (i4OutCome);
    return;
}

/**************************************************************************/
/*   Function Name   : RtmGetSizingParams                  */
/*   Description     : This function get the Sizing parameters            */
/*   Input(s)        : None                                               */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

void
RtmGetSizingParams (void)
{
    /* Initialise the sizing parameters */
    gRtmSystemSize.u4RtmMaxRoutes = 0;
    GetRtmSizingParams (&gRtmSystemSize);
}

/* -------------------------------------------------------------------+
 * Function           : RtmProtocolLock 
 * 
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE 
 *
 * Action             : Takes a task Sempaphore  
+-------------------------------------------------------------------*/

INT4
RtmProtocolLock (VOID)
{
    if (OsixSemTake (gRtmProtoSem) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : RtmProtocolUnLock
 *
 * Input(s)           : None.  
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE 
 *
 * Action             : Takes a task Sempaphore  
+-------------------------------------------------------------------*/

INT4
RtmProtocolUnlock (VOID)
{
    OsixSemGive (gRtmProtoSem);
    return SNMP_SUCCESS;
}

INT4
RouteTableLock (VOID)
{
    if (OsixSemTake (gRouteTblSemId) != OSIX_SUCCESS)
    {
        return RTM_FAILURE;
    }
    return RTM_SUCCESS;
}

INT4
RouteTableUnlock (VOID)
{
    OsixSemGive (gRouteTblSemId);
    return RTM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtmCreateCxt                                               */
/*                                                                           */
/* Description  : Creates Rtm context                                        */
/*                                                                           */
/* Input        : u4ContextId,                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM_SUCCESS/RTM_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
INT4
RtmCreateCxt (UINT4 u4ContextId)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    UINT4               u4MaxCxts = 0;

    u4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if (u4ContextId >= u4MaxCxts)
    {
        return RTM_FAILURE;
    }

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt != NULL)
    {
        /* Context is already created so return */
        return RTM_SUCCESS;
    }
    if (RTM_CONTEXT_ALLOC (pRtmCxt) == NULL)
    {
        /* Memory Allocation for RTM context failed */
        return RTM_FAILURE;
    }

    MEMSET (pRtmCxt, 0, sizeof (tRtmCxt));
    /* Assign the Context ID */
    pRtmCxt->u4ContextId = u4ContextId;
    gRtmGlobalInfo.apRtmCxt[u4ContextId] = pRtmCxt;

    pRtmCxt->u2RtmRtStartIndex = MAX_ROUTING_PROTOCOLS;
    pRtmCxt->u2RtmRtLastIndex = MAX_ROUTING_PROTOCOLS;

    pRtmCxt->u1RtmIBgpRedistribute = RTM_REDISTRIBUTION_DISABLED;
    /* Initialise the RRD Control List */
    TMO_SLL_Init (&(pRtmCxt->RtmCtrlList));

    /* Initialize temporary route list */
    TMO_SLL_Init (&(pRtmCxt->RtmInvdRtList));

    /* Initialise the RTM Registration Table */
    for (u2Index = 0; u2Index < MAX_ROUTING_PROTOCOLS; u2Index++)
    {
        RegnId.u2ProtoId = (UINT2) (u2Index + 1);
        RegnId.u4ContextId = u4ContextId;
        RtmReInitRtInCxt (pRtmCxt, &RegnId);
    }

    pRtmCxt->pIpRtTblRoot = NULL;
    pRtmCxt->RtmConfigInfo.u1RrdFilterByOspfTag = RTM_FILTER_DISABLED;

    /* Initialize protocol Preference Table */
    RtmInitPreferenceInCxt (pRtmCxt);
    /* Initialze the RRD Addmin status, RtrId and As Num */
    RtmConfigInitInCxt (pRtmCxt);

    /* Register with RTM for Local and Static routes. */
    if (RtmLocalIfInitInCxt (pRtmCxt) == IP_FAILURE)
    {
        RTM_CONTEXT_FREE (pRtmCxt);
        return RTM_FAILURE;
    }

    if (RtmStaticRtInitInCxt (pRtmCxt) == IP_FAILURE)
    {
        RTM_CONTEXT_FREE (pRtmCxt);
        return RTM_FAILURE;
    }

    /*  Initialise RtmControl table to Deny mode by adding a PERMIT_ALL entry
       in the table */
    if (RtmInitControlTableToDenyModeInCxt (pRtmCxt) != RTM_SUCCESS)
    {
        RTM_CONTEXT_FREE (pRtmCxt);
        return RTM_FAILURE;
    }
    pRtmCxt->u1IpDefaultDistance = STATIC_DEFAULT_PREFERENCE;
    return RTM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtmDeleteCxt                                               */
/*                                                                           */
/* Description  : Deletes Rtm context                                        */
/*                                                                           */
/* Input        : u4ContextId,                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM_SUCCESS/RTM_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
VOID
RtmDeleteCxt (UINT4 u4ContextId)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtmRegnId          RegnId;
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRrdControlInfo    *pRtmNextCtrlInfo = NULL;
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTempRt = NULL;
    tRtmRedisNode      *pNode = NULL;
    tRtmRedisNode      *pTmpNode = NULL;
    tPRtEntry          *pPRt = NULL;
    tPRtEntry          *pPRtNextEntry = NULL;
    tPNhNode           *pPNhEntry = NULL;
    tPNhNode           *pNextPNhEntry = NULL;
    tRNhNode           *pRNhEntry = NULL;
    tRNhNode           *pNextRNhEntry = NULL;
    UINT4               u4MaxCxts = 0;

    u4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((u4ContextId < u4MaxCxts) &&
        (gRtmGlobalInfo.apRtmCxt[u4ContextId] != NULL))
    {
        pRtmCxt = gRtmGlobalInfo.apRtmCxt[u4ContextId];
    }
    else
    {
        return;
    }

    if (pRtmCxt == NULL)
    {
        return;
    }

    pPNhEntry = RBTreeGetFirst (gRtmGlobalInfo.pPRTRBRoot);

    while (pPNhEntry != NULL)
    {
        pNextPNhEntry =
            RBTreeGetNext (gRtmGlobalInfo.pPRTRBRoot, pPNhEntry, NULL);
        if (pPNhEntry->pRtmCxt->u4ContextId == pRtmCxt->u4ContextId)
        {
            TMO_DYN_SLL_Scan (&pPNhEntry->routeEntryList, pPRt,
                              pPRtNextEntry, tPRtEntry *)
            {
                /* Delete the pending route entry from the list */
                TMO_SLL_Delete (&pPNhEntry->routeEntryList,
                                (tTMO_SLL_NODE *) pPRt);
                /* Reset the flag that indicates whether 
                 * route exists in PRT or not */
                pPRt->pPendRt->u4Flag &= (UINT4) (~RTM_RT_IN_PRT);
                MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                                    (UINT1 *) pPRt);
            }
            /* No pending route exists in the list.Delete the next hop
             * entry from RB Tree */
            RBTreeRemove (gRtmGlobalInfo.pPRTRBRoot, pPNhEntry);
            MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                                (UINT1 *) pPNhEntry);

        }
        pPNhEntry = pNextPNhEntry;
    }
    pRNhEntry = RBTreeGetFirst (gRtmGlobalInfo.pRNHTRBRoot);

    while (pRNhEntry != NULL)
    {
        pNextRNhEntry =
            RBTreeGetNext (gRtmGlobalInfo.pRNHTRBRoot, pRNhEntry, NULL);
        if (pRNhEntry->pRtmCxt->u4ContextId == pRtmCxt->u4ContextId)
        {
            /* No pending route exists in the list.Delete the next hop
             * entry from RB Tree */
            RBTreeRemove (gRtmGlobalInfo.pRNHTRBRoot, pRNhEntry);
            MemReleaseMemBlock (gRtmGlobalInfo.RtmRNextHopPoolId,
                                (UINT1 *) pRNhEntry);

        }
        pRNhEntry = pNextRNhEntry;
    }
    pPNhEntry = RBTreeGetFirst (gRtmGlobalInfo.pECMPRBRoot);

    while (pPNhEntry != NULL)
    {
        pNextPNhEntry =
            RBTreeGetNext (gRtmGlobalInfo.pECMPRBRoot, pPNhEntry, NULL);
        if (pPNhEntry->pRtmCxt->u4ContextId == pRtmCxt->u4ContextId)
        {
            TMO_DYN_SLL_Scan (&pPNhEntry->routeEntryList, pPRt,
                              pPRtNextEntry, tPRtEntry *)
            {
                /* Delete the pending route entry from the list */
                TMO_SLL_Delete (&pPNhEntry->routeEntryList,
                                (tTMO_SLL_NODE *) pPRt);
                /* Reset the flag that indicates whether 
                 * route exists in PRT or not */
                pPRt->pPendRt->u4Flag &= (UINT4) (~RTM_RT_IN_PRT);
                MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                                    (UINT1 *) pPRt);
            }
            /* No pending route exists in the list.Delete the next hop
             * entry from RB Tree */
            RBTreeRemove (gRtmGlobalInfo.pECMPRBRoot, pPNhEntry);
            MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                                (UINT1 *) pPNhEntry);

        }
        pPNhEntry = pNextPNhEntry;
    }
    TmrStop (gRtmGlobalInfo.RtmTmrListId, &(pRtmCxt->RtmDLFTmr));
    TmrStop (gRtmGlobalInfo.RtmTmrListId, &(pRtmCxt->RtmECMPPRTTmr));

    while ((pRtmCxt->u2RtmRtStartIndex != MAX_ROUTING_PROTOCOLS) &&
           (pRtmCxt->u2RtmRtStartIndex < MAX_ROUTING_PROTOCOLS))
    {
        RegnId.u2ProtoId = (UINT2) (pRtmCxt->u2RtmRtStartIndex + 1);
        RegnId.u4ContextId = pRtmCxt->u4ContextId;
        TmrStopTimer (gRtmGlobalInfo.RtmTmrListId,
                      &(pRtmCxt->aRtmRegnTable[pRtmCxt->u2RtmRtStartIndex].
                        GRTmr.RtmGRTmr.TimerNode));

        RtmUtilDeregister (pRtmCxt, &RegnId);
    }

    TMO_DYN_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo,
                      pRtmNextCtrlInfo, tRrdControlInfo *)
    {
        TMO_SLL_Delete (&(pRtmCxt->RtmCtrlList), &(pRtmCtrlInfo->pNext));
        RTM_CONTROL_INFO_FREE (pRtmCtrlInfo);
    }
    TMO_DYN_SLL_Scan (&(pRtmCxt->RtmInvdRtList), pRt, pTempRt, tRtInfo *)
    {
        TMO_SLL_Delete (&(pRtmCxt->RtmInvdRtList), &(pRt->NextRtInfo));
        if (pRt->u2RtProto == CIDR_STATIC_ID)
        {
            gRtmGlobalInfo.u4StaticRts--;
        }

        IP_RT_FREE (pRt);
    }
    TMO_DYN_SLL_Scan (&(gRtmGlobalInfo.RtmRedisInitList), pNode, pTmpNode,
                      tRtmRedisNode *)
    {
        if (pRtmCxt->u4ContextId == pNode->RegId.u4ContextId)
        {
            TMO_SLL_Delete (&gRtmGlobalInfo.RtmRedisInitList,
                            &(pNode->NextNode));
            RTM_REDIST_NODE_FREE (pNode);
        }
    }
    gRtmGlobalInfo.apRtmCxt[pRtmCxt->u4ContextId] = NULL;
    RTM_CONTEXT_FREE (pRtmCxt);
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmVcmCallBackFn
 *
 * Description        : Function to Indicate Context mapping/deletion to 
 *                      RTM. This fucntion is registered with VCM module         
 *                      as a call back function  
 *
 * Input              : u4IfIndex   - Ip Interface Index                           
 *                      u4VcmCxtId  - Context Id                                   
 *                      u1BitMap    - Bit Map to identify the change               
 *
 * Output(s)          : None.
 *
 * Returns            : None
 *
 * Action             : This routine is called whenever a L3 Context is
 *                      added/deleted.
+-------------------------------------------------------------------*/
VOID
RtmVcmCallBackFn (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1BitMap)
{
    tOsixMsg           *pRtmMsg = NULL;
    tRtmMsgHdr         *pRtmMsgHdr = NULL;

    UNUSED_PARAM (u4IpIfIndex);

    /* only the Context Create/delete indication is needed */
    if ((u1BitMap & (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE)) == 0)
    {
        return;
    }
    if (u1BitMap & VCM_CONTEXT_CREATE)
    {
        RTM_PROT_LOCK ();
        RtmCreateCxt (u4VcmCxtId);
        RTM_PROT_UNLOCK ();
        return;
    }

    if ((pRtmMsg =
         CRU_BUF_Allocate_MsgBufChain ((2 * sizeof (UINT4)), 0)) == NULL)
    {
        return;
    }

    pRtmMsgHdr = (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = RTM_VCM_MSG_RCVD;
    pRtmMsgHdr->RegnId.u4ContextId = u4VcmCxtId;

    /* copy the  VCM change indication */
    if (IP_COPY_TO_BUF (pRtmMsg, &u1BitMap,
                        0, sizeof (UINT1)) == IP_BUF_FAILURE)
    {

        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return;
    }

    /* Delete the static configurations done this context */
    RTM_PROT_LOCK ();
    RtmClearContextEntries (u4VcmCxtId);
    RTM_PROT_UNLOCK ();

    /* copy the  VCM change indication */
    if (IP_COPY_TO_BUF (pRtmMsg, &u4VcmCxtId,
                        sizeof (UINT4), sizeof (UINT4)) == IP_BUF_FAILURE)
    {

        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return;
    }
    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) == RTM_FAILURE)
    {
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return;
    }

    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmVcmMsgHandler
 *
 * Input              : pBuf - Pointer to the buffer having VCM context
 *                      creation and delete indication
 *
 * Output(s)          : None.
 *
 * Returns            : None
 *
 * Action             : This routine is called whenever a L3 Context is
 *                      added/deleted.
+-------------------------------------------------------------------*/
VOID
RtmVcmMsgHandler (tIpBuf * pRtmMsg)
{
    UINT4               u4ContextId = 0;
    UINT1               u1BitMap = 0;

    /* copy the  VCM change indication */
    if (IP_COPY_FROM_BUF (pRtmMsg, &u1BitMap,
                          0, sizeof (UINT1)) != sizeof (UINT1))
    {
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return;
    }

    /* copy the  VCM change indication */
    if (IP_COPY_FROM_BUF (pRtmMsg, &u4ContextId,
                          sizeof (UINT4), sizeof (UINT4)) != sizeof (UINT4))
    {
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return;
    }
    if (u1BitMap == VCM_CONTEXT_DELETE)
    {
        RtmDeleteCxt (u4ContextId);
    }

    IP_RELEASE_BUF (pRtmMsg, FALSE);
    return;
}

/* -------------------------------------------------------------------+
 * Function           : RTMGetForwardingStateInCxt
 *
 * Input(s)           : u4ContextID
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : Returns the forwarding state
+-------------------------------------------------------------------*/

INT4
RTMGetForwardingStateInCxt (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return RTM_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : RtmGetIgpConvergenceStateInCxt
 *
 * Input(s)           : u4ContextId
 *
 * Output(s)          : None.
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : Returns the IGP converged state
+-------------------------------------------------------------------*/

INT4
RtmGetIgpConvergenceStateInCxt (UINT4 u4ContextId)
{
    UINT2               u2Index = 0;
    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);
    if (pRtmCxt == NULL)
    {
        return RTM_FAILURE;
    }

    /* Initialise the RTM6 Registration Table */
    for (u2Index = 0; u2Index < MAX_ROUTING_PROTOCOLS; u2Index++)
    {
        /* If any IGP supports Graceful restarts need to be added in this check */
        if ((u2Index == (ISIS_ID - 1)) || (u2Index == (OSPF_ID - 1)))
        {
            /* If the IGP is restarting then return failure */
            if (pRtmCxt->aRtmRegnTable[u2Index].u1RestartState ==
                RTM_IGP_RESTARTING)
            {
                return RTM_FAILURE;
            }
        }
    }                            /* End of For */
    return RTM_SUCCESS;
}
