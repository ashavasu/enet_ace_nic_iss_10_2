/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmmbsm.c,v 1.20 2015/06/17 04:46:29 siva Exp $
 *
 *******************************************************************/
#ifdef MBSM_WANTED
#include "rtminc.h"

#ifdef LNXIP4_WANTED
#include "dhcp.h"
#define PROTOCOL_ENABLED 1

VOID                IpMbsmInitProtocols (tMbsmSlotInfo * pSlotInfo);
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : IpRtmMbsmUpdateCardStatus                                  */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the Line card          */
/*                change status to the IP RTM Task                           */
/*                                                                           */
/*                                                                           */
/* Input        : pProtoMsg - Contains the Slot and Port Information         */
/*                i1Status  - Line card Up/Down status                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

INT4
IpRtmMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tRtmMsgHdr         *pParams = NULL;

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tMbsmProtoMsg), 0)) == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP, "CRU BUF Allocation Failed\n");
        return MBSM_FAILURE;
    }

    /* Copy the message to the CRU buffer */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pProtoMsg, 0,
                               sizeof (tMbsmProtoMsg));

    pParams = (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pBuf);
    pParams->u1MessageType = (UINT1) i4Event;

    if (OsixQueSend (gRtmMsgQId, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, TRUE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP, "Send to Q Failed\n");
        return MBSM_FAILURE;
    }

    OsixEvtSend (gRtmTaskId, RTM_MESSAGE_ARRIVAL_EVENT);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : RtmMbsmUpdateRouteTbl                                      */
/*                                                                           */
/* Description  : Updates the Routing table in the NP based on the           */
/*                Line card status received                                  */
/*                                                                           */
/*                                                                           */
/* Input        : pBuf - Buffer containing the protocol message information  */
/*                u1Status - Line card Status (UP/DOWN)                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
RtmMbsmUpdateRouteTbl (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo             RtInfo;
    tRtInfo             NextRtInfo;
    UINT1               bu1TblFull = FNP_FALSE;
    INT4                i4RetStatus = MBSM_SUCCESS;
    UINT2               u2VlanId = 0;
    tFsNpNextHopInfo    NxtHopInfo;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmProtoAckMsg    protoAckMsg;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    INT4                i4Flag = TRUE;
    UINT4               u4CfaIfIndex;
    UINT4               u4ContextId;
    UINT4               u4NextContextId;
    tCfaIfInfo          IfInfo;
    UINT1               u1RtCount = 0;

    if ((CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
                                    sizeof (tMbsmProtoMsg))) == CRU_FAILURE)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                  "CRU BUF Copy From Buf Chain Failed\n");
        return;
    }

    pSlotInfo = &(ProtoMsg.MbsmSlotInfo);
    pPortInfo = &(ProtoMsg.MbsmPortInfo);

    MEMSET (&RtInfo, 0, sizeof (tRtInfo));
    MEMSET (&NextRtInfo, 0, sizeof (tRtInfo));
    MEMSET (&NxtHopInfo, 0, sizeof (tFsNpNextHopInfo));
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    if (u1Cmd == MBSM_MSG_CARD_INSERT)
    {
        if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
        {
#ifdef LNXIP4_WANTED
            /* Update the card for protocol status in case of LnxIp. */

            IpMbsmInitProtocols (pSlotInfo);
#endif
            /* Get the first valid context ID and RTM context pointer */
            if (UtilRtmGetFirstCxtId (&u4NextContextId) != RTM_FAILURE)
            {
                pRtmCxt = UtilRtmGetCxt (u4NextContextId);
            }

            while (pRtmCxt != NULL)
            {
                while (RtmUtilGetNextBestRtInCxt (pRtmCxt,
                                                  RtInfo,
                                                  &NextRtInfo) != IP_FAILURE)
                {
                    if (NextRtInfo.u4RtIfIndx == IPIF_INVALID_INDEX)
                    {
                        MEMCPY (&RtInfo, &NextRtInfo, sizeof (tRtInfo));
                        continue;
                    }
                    RtmGetBestRouteCount (&NextRtInfo, NextRtInfo.i4Metric1,
                                          &u1RtCount);
                    NxtHopInfo.u1RtCount = u1RtCount;
                    NxtHopInfo.u4NextHopGt = NextRtInfo.u4NextHop;
                    if (NetIpv4GetCfaIfIndexFromPort
                        (NextRtInfo.u4RtIfIndx,
                         &u4CfaIfIndex) == NETIPV4_FAILURE)
                    {
                        i4RetStatus = MBSM_FAILURE;
                        break;
                    }
                    NxtHopInfo.u4IfIndex = u4CfaIfIndex;

                    if ((CfaIsMgmtPort (u4CfaIfIndex) == TRUE) &&
                        (CFA_OOB_MGMT_INTF_ROUTING == FALSE))
                    {
                        i4Flag = FALSE;
                    }

                    if (CfaGetIfInfo (u4CfaIfIndex, &IfInfo) == CFA_FAILURE)
                    {
                        i4RetStatus = MBSM_FAILURE;
                        break;
                    }

                    /*  when port is oob port and routing over 
                       oob port is not supported, then don't add the route */

                    if (i4Flag == TRUE)
                    {
                        if (((NextRtInfo.u4Flag & RTM_RT_REACHABLE) == 0) &&
                            ((NextRtInfo.u4Flag & RTM_RT_IN_PRT) == 0))
                        {
                            NxtHopInfo.u1RtType = FNP_RT_LOCAL;
                        }

                        if (IfInfo.u1IfType == CFA_L3IPVLAN)
                        {
                            if (CfaGetVlanId (u4CfaIfIndex, &u2VlanId) !=
                                CFA_SUCCESS)
                            {
                                i4RetStatus = MBSM_FAILURE;
                                break;
                            }
                            NxtHopInfo.u2VlanId = u2VlanId;
                        }
                        if (FNP_FAILURE ==
                            IpFsNpMbsmIpv4UcAddRoute (pRtmCxt->u4ContextId,
                                                      NextRtInfo.u4DestNet,
                                                      NextRtInfo.u4DestMask,
#ifndef NPSIM_WANTED
                                                      &NxtHopInfo,
#else
                                                      NxtHopInfo,
#endif
                                                      &bu1TblFull, pSlotInfo))
                        {
                            i4RetStatus = MBSM_FAILURE;
                        }

                        if (bu1TblFull == FNP_TRUE)
                        {
                            break;
                        }
                    }

                    MEMCPY (&RtInfo, &NextRtInfo, sizeof (tRtInfo));
                }                /* End of while loop for Routes in RTM Context */

                u4ContextId = u4NextContextId;
                pRtmCxt = NULL;
                /* Get the next valid RTM context pointer */
                if (UtilRtmGetNextCxtId (u4ContextId, &u4NextContextId) !=
                    RTM_FAILURE)
                {
                    pRtmCxt = UtilRtmGetCxt (u4NextContextId);
                }

            }                    /* End of while loop for RTM Context */

        }
    }

    /* Send an acknowledgment to the MBSM module to inform the completion
     * of updation of interfaces in the NP
     */

    protoAckMsg.i4RetStatus = i4RetStatus;
    protoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
    protoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
    MbsmSendAckFromProto (&protoAckMsg);

}

#ifdef LNXIP4_WANTED

/*****************************************************************************/
/*                                                                           */
/* Function     : IpNpMbsmInitProtocols                                      */
/*                                                                           */
/* Description  : Enables the protocols registered                           */
/*                                                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
IpMbsmInitProtocols (tMbsmSlotInfo * pSlotInfo)
{
    IpFsNpMbsmIpInit (pSlotInfo);
#ifdef DHCP_RLY_WANTED

    /*Since Dhcp relay task is Dynamic, we are initialising the NP programming 
       from here */

    if (DhcpIsRlyEnabledInAnyCxt() == PROTOCOL_ENABLED)
    {
        if (IpFsNpMbsmDhcpRlyInit (pSlotInfo) != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                      "Card Insertion: FsNpDhcpRlyInit Failed \n");
            /* Need to return */
            return;
        }
    }
#endif

#ifdef DHCP_SRV_WANTED

    /*Since Dhcp Server task is Dynamic, we are initialising the NP programming 
       from here */

    if (DhcpIsSrvEnabled () == PROTOCOL_ENABLED)
    {
        if (IpFsNpMbsmDhcpSrvInit (pSlotInfo) != FNP_SUCCESS)
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_IP,
                      "Card Insertion: FsNpDhcpSrvInit Failed \n");
            /* Need to return */
            return;
        }
    }
#endif

}
#endif /* LNXIP4_WANTED */

#endif
