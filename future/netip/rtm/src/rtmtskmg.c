/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmtskmg.c,v 1.45 2017/11/14 07:31:13 siva Exp $
 *
 * Description: Task related functions are handled here. 
 *
 *******************************************************************/

#include "rtminc.h"
#include "rtmred.h"

#define MAX_RTM_PROCESS_ROUTE_COUNT 20

tOsixQId            gRtmMsgQId;
tOsixQId            gRtmSnmpMsgQId;
tOsixQId            gRtmRtMsgQId;
tOsixTaskId         gRtmTaskId;
VOID                RTM_Process_Route_Msg_Event (VOID);
extern UINT1        gu1EcmpAcrossProtocol;

/*-------------------------------------------------------------------+
 *
 * Function           : RtmTaskInit 
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action             : This function will be called from the ROOT module (CFA).
 *                      This function spawns the RTM TASK. 
 *                      Also allocates memory for the route entries in the 
 *                      common routing table.  
 *         
+-------------------------------------------------------------------*/
INT1
RtmTaskInit ()
{

#ifdef LNXIP4_WANTED
    /* Initialize the Registration Table and Trie Structure */
    IpInitRegTable ();
#endif
    /* Initialises the RTM Global Info Structure */
    if (RtmInit () == RTM_FAILURE)
    {
        RtmDeInit ();
        return RTM_FAILURE;
    }

    if (OsixQueCrt ((UINT1 *) RTM_MESSAGE_ARRIVAL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    RTM_MESSAGE_ARRIVAL_Q_DEPTH, &gRtmMsgQId) != OSIX_SUCCESS)
    {
        OsixTskDel (gRtmTaskId);
        return RTM_FAILURE;
    }

    if (OsixQueCrt ((UINT1 *) RTM_SNMP_IF_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    RTM_SNMP_IF_Q_DEPTH, &gRtmSnmpMsgQId) != OSIX_SUCCESS)
    {
        OsixQueDel (gRtmMsgQId);
        OsixTskDel (gRtmTaskId);
        return RTM_FAILURE;
    }
    if (OsixQueCrt ((UINT1 *) RTM_RT_MSG_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                    RTM_RT_MSG_Q_DEPTH, &gRtmRtMsgQId) != OSIX_SUCCESS)
    {
        OsixQueDel (gRtmSnmpMsgQId);
        OsixQueDel (gRtmMsgQId);
        OsixTskDel (gRtmTaskId);
        return RTM_FAILURE;
    }
    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmTaskMain 
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function initializes the global data structures
 *                      of the RTM Module. This function also waits for the 
 *                      messages from the routing protocols.
 *         
+-------------------------------------------------------------------*/
VOID
RtmTaskMain (INT1 *pi1TaskParam)
{
    UINT4               u4EventMask = 0;
    UINT4               u4EntryCount = 0;

    UNUSED_PARAM (pi1TaskParam);

    if (OsixTskIdSelf (&gRtmTaskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        RTM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (RtmTaskInit () != RTM_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        RTM_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef LNXIP4_WANTED
    RtmSubTaskInit ();
#endif /* LNXIP4_WANTED */

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_RTM, RtmSendRouteMapUpdateMsg);
#endif /*ROUTEMAP_WANTED */

    /* Indicate the status of initialization to the main routine */
    RTM_INIT_COMPLETE (OSIX_SUCCESS);

    for (;;)
    {

        OsixEvtRecv (gRtmTaskId,
                     RTM_MESSAGE_ARRIVAL_EVENT | RTM_FILTER_ADDED_EVENT |
                     RTM_ROUTE_MSG_EVENT | MSR_RESTORE_COMPLETE |
                     RTM_TMR_EVENT | RTM_RM_PKT_EVENT | RTM_RED_TIMER_EVENT |
                     RTM_HA_PEND_BLKUPD_EVENT | RTM_HA_FRT_PEND_BLKUPD_EVENT,
                     RTM_EVENT_WAIT_FLAGS, &u4EventMask);

        RTM_PROT_LOCK ();

        if (u4EventMask & RTM_MESSAGE_ARRIVAL_EVENT)
        {
            RTM_Process_Message_Arrival_Event ();
        }

        if (u4EventMask & RTM_FILTER_ADDED_EVENT)
        {
            RTM_Process_Filter_Added_Event ();
        }
        if (u4EventMask & RTM_ROUTE_MSG_EVENT)
        {
            RTM_Process_Route_Msg_Event ();
        }
        if (u4EventMask & RTM_TMR_EVENT)
        {
            RtmProcessTimerExpiry ();
        }
        if (u4EventMask & RTM_RM_PKT_EVENT)
        {
            RtmRedHandleRmEvents ();
        }

        if (u4EventMask & RTM_HA_PEND_BLKUPD_EVENT)
        {
            RtmRedSendBulkUpdMsg ();
        }

        if (u4EventMask & RTM_HA_FRT_PEND_BLKUPD_EVENT)
        {
            RtmRedSendFrtBulkUpdMsg ();
        }

        if (u4EventMask & MSR_RESTORE_COMPLETE)
        {
            gu1EcmpAcrossProtocol = gRtmGlobalInfo.u1EcmpAcrossProtocol;
        }

        if (gRtmRedGlobalInfo.u1RedStatus == IP_TRUE)
        {
            if ((gRtmRedGlobalInfo.u1NodeStatus == RM_ACTIVE) &&
                (RTM_IS_STANDBY_UP () == OSIX_TRUE))
            {
                u4EntryCount = 0;
                RBTreeCount (gRtmGlobalInfo.RtmRedTable, &u4EntryCount);
                if (u4EntryCount > 0)
                {
                    RtmRedSendDynamicInfo (NETIPV4_MODIFY_ROUTE);
                }
            }
        }

        RTM_PROT_UNLOCK ();
    }                            /* FOREVER */
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmEnqueuePktToRps
 *
 * Input(s)           : pRespInfo - Pointer to the RTM Response info.
 *                      RtmHeader - Pointer to the RTM Response Header message
 *                      pRegnId - Register ID.
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action             : This function initializes the global data structures
                        of the RTM Module. This function also waits for the 
                        messages from the routing protocols.
 *         
+-------------------------------------------------------------------*/
INT1
RtmEnqueuePktToRpsInCxt (tRtmCxt * pRtmCxt, tRtmRespInfo * pRespInfo,
                         tRtmMsgHdr * pRtmHeader, tRtmRegnId * pRegnId)
{
    INT1                i1ReturnStatus = RTM_FAILURE;
    UINT2               u2RegnId = 0;

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId != RTM_INVALID_REGN_ID)
    {
        if (pRtmCxt->aRtmRegnTable[u2RegnId].DeliverToApplication != NULL)
        {
            i1ReturnStatus = (INT1)
                (pRtmCxt->aRtmRegnTable[u2RegnId].
                 DeliverToApplication (pRespInfo, pRtmHeader));
        }
    }

    return (i1ReturnStatus);
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmGetRegnIndexFromRegnIdInCxt
 *
 * Input(s)           : pRegnId    - Registration Information.
 *                    : pRtmCxt    - Rtm Context Pointer 
 *
 * Output(s)          : None
 *
 * Returns            : Registraion Table Index
 *
 * Action             : This function scans thru the RTM registration table 
 *                      and finds the registration ID for the matching      
 *                      protocol ID.
 *         
+-------------------------------------------------------------------*/
UINT2
RtmGetRegnIndexFromRegnIdInCxt (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId)
{
    UINT2               u2Index = 0;

    /* Registration Id is (Protocol Id - 1) */
    u2Index = (UINT2) (pRegnId->u2ProtoId - 1);
    if (u2Index < MAX_ROUTING_PROTOCOLS)
    {
        if (pRtmCxt->aRtmRegnTable[u2Index].u2RoutingProtocolId ==
            pRegnId->u2ProtoId)
        {
            return u2Index;
        }
    }

    /* Protocol is not registered. */
    return RTM_INVALID_REGN_ID;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RpsEnqueuePktToRtm
 *
 * Input(s)           : pBuf - Pointer to the buffer to be enqueued.
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action             : This function enqueues the packet to the RTM task
                        and then sends event to it.
 *
+-------------------------------------------------------------------*/
INT1
RpsEnqueuePktToRtm (tIpBuf * pBuf)
{
    if (OsixQueSend (gRtmMsgQId, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        return (RTM_FAILURE);
    }

    OsixEvtSend (gRtmTaskId, RTM_MESSAGE_ARRIVAL_EVENT);
    return (RTM_SUCCESS);
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleExportMaskEvent
 *
 * Input(s)           : pRtInfo - Info about the best route to be 
 *                                redistributed.
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : This function is the call back function to 
 *                      handle the route redistribution when RP request
 *                      for redistribution of any specific protocol routes.
 *
+-------------------------------------------------------------------*/
INT4
RtmHandleExportMaskEvent (tRtInfo * pRtInfo, VOID *pAppSpecData)
{
    tRtmRegnId          RegnId;
    tRtmInitRoute      *pRtmInitInfo = (tRtmInitRoute *) pAppSpecData;
    UINT2               u2SrcProtoMask = 0;
    UINT2               u2DestProtoMask = RTM_DONT_REDISTRIBUTE;
    UINT2               u2RedisProtoMask = 0;
    INT4                i4RetVal = RTM_SUCCESS;

    /* Check whether this best route needs to be redistributed */

    RTM_SET_BIT (u2RedisProtoMask, pRtmInitInfo->u2DestProto);
    RTM_SET_BIT (u2SrcProtoMask, pRtInfo->u2RtProto);

    if (pRtmInitInfo->u2Data & RTM_MATCH_ALL_TYPE)
    {
        pRtmInitInfo->u2Data |= RTM_OSPF_MASK;
    }
    if (pRtmInitInfo->u2Data & RTM_ISISL1L2_MASK)
    {
        pRtmInitInfo->u2Data |= RTM_ISIS_MASK;
    }
    if (pRtInfo->u2RtProto == OSPF_ID)
    {
        i4RetVal =
            RtmCheckForMatch (pRtmInitInfo->u2Data, pRtInfo->i4MetricType);
    }
    if (pRtInfo->u2RtProto == ISIS_ID)
    {
        i4RetVal =
            RtmCheckForLevel (pRtmInitInfo->u2Data, pRtInfo->i4MetricType);
    }

    if (((pRtmInitInfo->u2Data & u2SrcProtoMask) == u2SrcProtoMask) &&
        (i4RetVal == RTM_SUCCESS))
    {
        /* Route needs to be redistributed. */
        if (pRtmInitInfo->u2Event == RTM_ENABLE_MASK)
        {
            RegnId.u2ProtoId = pRtInfo->u2RtProto;
            RegnId.u4ContextId = pRtmInitInfo->pRtmCxt->u4ContextId;

#ifdef ICCH_WANTED
            /* ICCL private route is not be redistributed */
            if (IcchApiIsIcclAddrRedistributable (pRtInfo->u4DestNet) ==
                OSIX_SUCCESS)
            {
                return RTM_SUCCESS;
            }
#endif

            if (RtmCheckIsRouteMapConfiguredInCxt (pRtmInitInfo->pRtmCxt,
                                                   pRtmInitInfo->u2DestProto) ==
                RTM_FAILURE)
            {
                RtmCheckRouteForRedistributionInCxt (pRtmInitInfo->pRtmCxt,
                                                     &RegnId, pRtInfo,
                                                     &u2DestProtoMask);

                if ((u2DestProtoMask != RTM_DONT_REDISTRIBUTE)
                    && ((u2DestProtoMask & u2RedisProtoMask) ==
                        u2RedisProtoMask)
                    && ((pRtInfo->u1BitMask & RTM_PRIV_RT_MASK) !=
                        RTM_PRIV_RT_MASK))
                {
                    /* Route can be redistributed to the registering protocol. */
                    RtmRouteRedistributionInCxt (pRtmInitInfo->pRtmCxt, pRtInfo,
                                                 TRUE, IP_BIT_ALL,
                                                 u2RedisProtoMask);
                }

            }

            /* Route Map configured for this Dest RP, apply the Route Map rule 
             * and redistribute the route */
            else if ((pRtInfo->u1BitMask & RTM_PRIV_RT_MASK) !=
                     RTM_PRIV_RT_MASK)
            {
                RtmApplyRouteMapRuleAndRedistributeInCxt (pRtmInitInfo->pRtmCxt,
                                                          RegnId.u2ProtoId,
                                                          pRtInfo, TRUE,
                                                          IP_BIT_ALL,
                                                          u2RedisProtoMask);

            }
        }
        else
        {
            /* It is expected that routing protocol will take care of removing
             * the redistributed routes. So just reset the Redistribute Flag */
            pRtInfo->u2RedisMask =
                (UINT2) (pRtInfo->u2RedisMask & (~u2RedisProtoMask));
        }
    }

    return RTM_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RTM_Process_Message_Arrival_Event
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action             : This function dequeues the packet from  RTM Message
                        Queue.
 *
+-------------------------------------------------------------------*/
VOID
RTM_Process_Message_Arrival_Event (VOID)
{
    tIpBuf             *pBuf = NULL;
    tRtmMsgHdr         *pRtmMsgHdr = NULL;
    tRtmRrdMsg          RtmRrdMsg;
    tRtmRegnId          RegnId;
    tRtmRouteUpdateInfo RtmRouteUpdateInfo;
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo             Rt;
    tRtInfo            *pExstRt = NULL;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4OldNet = 0;
    UINT4               u4NewNet = 0;
    INT4                i4OutCome = IP_FAILURE;

    while (OsixQueRecv (gRtmMsgQId, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        /* The message header given by the routing protocols will be
           there in the module data of the message. */

        pRtmMsgHdr = (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pBuf);

        switch (pRtmMsgHdr->u1MessageType)
        {
            case RTM_REGISTRATION_MESSAGE:

                RtmHandleRegnMsg (pBuf);
                break;

            case RTM_REDISTRIBUTE_ENABLE_MESSAGE:
            case RTM_REDISTRIBUTE_DISABLE_MESSAGE:
                MEMSET (&RtmRrdMsg, 0, sizeof (tRtmRrdMsg));
                MEMCPY (&(RtmRrdMsg.RegnId), &(pRtmMsgHdr->RegnId),
                        sizeof (tRtmRegnId));

                if (IP_COPY_FROM_BUF
                    (pBuf, &(RtmRrdMsg.u2DestProtoMask), 0,
                     sizeof (UINT2)) != sizeof (UINT2))
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    return;
                }

                if (IP_COPY_FROM_BUF
                    (pBuf, RtmRrdMsg.au1RMapName, sizeof (UINT2),
                     (RMAP_MAX_NAME_LEN)) != RMAP_MAX_NAME_LEN)
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    return;
                }
                if (pRtmMsgHdr->u1MessageType ==
                    RTM_REDISTRIBUTE_ENABLE_MESSAGE)
                {
                    RtmHandleRrdEnableMsg (&RtmRrdMsg);
                }
                else
                {
                    RtmHandleRrdDisableMsg (&RtmRrdMsg);
                }
                IP_RELEASE_BUF (pBuf, FALSE);
                break;

            case RTM_DEREGISTRATION_MESSAGE:

                RegnId = pRtmMsgHdr->RegnId;
                if ((pRtmCxt = UtilRtmGetCxt (RegnId.u4ContextId)) != NULL)
                {
                    RtmUtilDeregister (pRtmCxt, &RegnId);
                }
                IP_RELEASE_BUF (pBuf, FALSE);
                break;
#ifdef LNXIP4_WANTED
            case RTM_CREATE_INT_MSG:

                RtmHandleIfCreationDeletion (IPIF_CREATE_INTERFACE, pBuf);
                break;

            case RTM_DELETE_INT_MSG:

                RtmHandleIfCreationDeletion (IPIF_DELETE_INTERFACE, pBuf);
                break;

            case RTM_OPER_UP_MSG:

                RtmHandleIfStateChng (IP_INTERFACE_UP, pBuf);
                break;

            case RTM_OPER_DOWN_MSG:

                RtmHandleIfStateChng (IP_INTERFACE_DOWN, pBuf);
                break;

            case RTM_MTU_UPDATE_MSG:

                RtmHandleIfStateChng (IP_PORT_MTU_CHANGE, pBuf);
                break;

            case RTM_SPEED_UPDATE_MSG:

                RtmHandleIfStateChng (IP_PORT_SPEED_CHANGE, pBuf);
                break;
#endif
            case RTM_INTF_ROUTE_ADD_MSG:
            case RTM_INTF_ROUTE_DEL_MSG:
                if (IP_COPY_FROM_BUF (pBuf, &RtmRouteUpdateInfo, 0,
                                      sizeof (tRtmRouteUpdateInfo))
                    != sizeof (tRtmRouteUpdateInfo))
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    break;
                }
                u4NewNet = RtmRouteUpdateInfo.u4NewIpAddr &
                    RtmRouteUpdateInfo.u4NewNetMask;
                if (pRtmMsgHdr->u1MessageType == RTM_INTF_ROUTE_ADD_MSG)
                {
                    RtmUpdateLocalRouteInCxt (RTM_ADD_ROUTE,
                                              RtmRouteUpdateInfo.u4ContextId,
                                              u4NewNet,
                                              RtmRouteUpdateInfo.u4NewNetMask,
                                              RtmRouteUpdateInfo.u4IfIndex);
                }
                else
                {
                    RtmUpdateLocalRouteInCxt (RTM_DELETE_ROUTE,
                                              RtmRouteUpdateInfo.u4ContextId,
                                              u4NewNet,
                                              RtmRouteUpdateInfo.u4NewNetMask,
                                              RtmRouteUpdateInfo.u4IfIndex);
                }
                IP_RELEASE_BUF (pBuf, FALSE);
                break;

            case RTM_INTF_ROUTE_MODIFY_MSG:

                if (IP_COPY_FROM_BUF (pBuf, &RtmRouteUpdateInfo, 0,
                                      sizeof (tRtmRouteUpdateInfo))
                    != sizeof (tRtmRouteUpdateInfo))
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    break;
                }
                u4OldNet = RtmRouteUpdateInfo.u4OldIpAddr &
                    RtmRouteUpdateInfo.u4OldNetMask;

                u4NewNet = RtmRouteUpdateInfo.u4NewIpAddr &
                    RtmRouteUpdateInfo.u4NewNetMask;

                RtmUpdateLocalRouteInCxt (RTM_DELETE_ROUTE,
                                          RtmRouteUpdateInfo.u4ContextId,
                                          u4OldNet,
                                          RtmRouteUpdateInfo.u4OldNetMask,
                                          RtmRouteUpdateInfo.u4IfIndex);

                RtmUpdateLocalRouteInCxt (RTM_ADD_ROUTE,
                                          RtmRouteUpdateInfo.u4ContextId,
                                          u4NewNet,
                                          RtmRouteUpdateInfo.u4NewNetMask,
                                          RtmRouteUpdateInfo.u4IfIndex);

                IP_RELEASE_BUF (pBuf, FALSE);
                break;

            case RTM_ROUTEMAP_UPDATE_MSG:
                MEMSET (au1RMapName, 0, sizeof (au1RMapName));
                /* copy map name from offset 4 */
                if (IP_COPY_FROM_BUF (pBuf, au1RMapName, IP_FOUR,
                                      RMAP_MAX_NAME_LEN) !=
                    (INT4) RMAP_MAX_NAME_LEN)
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    break;
                }

                RtmProcessRouteMapChanges (au1RMapName);

                IP_RELEASE_BUF (pBuf, FALSE);
                break;

            case RTM_GR_NOTIFY_MSG:
                RegnId.u2ProtoId = pRtmMsgHdr->RegnId.u2ProtoId;
                RegnId.u4ContextId = pRtmMsgHdr->RegnId.u4ContextId;

                if ((RegnId.u2ProtoId == BGP_ID) ||
                    (RegnId.u2ProtoId == RIP_ID))
                {
                    RtmGRStaleRtMark (&RegnId);
                }
                if (RegnId.u2ProtoId != RIP_ID)
                {
                    RtmStartGRTmr (&RegnId, pBuf);
                }
                else
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                }
                break;

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                RtmMbsmUpdateRouteTbl (pBuf, pRtmMsgHdr->u1MessageType);
                IP_RELEASE_BUF (pBuf, FALSE);
                break;
#endif
            case RTM_VCM_MSG_RCVD:
                RtmVcmMsgHandler (pBuf);
                break;
            case RTM_IBGP_ROUTE_REDISTRIBUTE_ENABLE:
                RegnId.u4ContextId = pRtmMsgHdr->RegnId.u4ContextId;
                if ((pRtmCxt = UtilRtmGetCxt (RegnId.u4ContextId)) != NULL)
                {
                    pRtmCxt->u1RtmIBgpRedistribute = RTM_REDISTRIBUTION_ENABLED;
                }
                RtmRedistributeIBgpInternalEnable (RegnId.u4ContextId);

                break;

            case RTM_IBGP_ROUTE_REDISTRIBUTE_DISABLE:
                RegnId.u4ContextId = pRtmMsgHdr->RegnId.u4ContextId;
                if ((pRtmCxt = UtilRtmGetCxt (RegnId.u4ContextId)) != NULL)
                {
                    pRtmCxt->u1RtmIBgpRedistribute =
                        RTM_REDISTRIBUTION_DISABLED;
                }

                RtmRedistributeIBgpInternalDisable (RegnId.u4ContextId);
                IP_RELEASE_BUF (pBuf, FALSE);
                break;

            case RTM_INTERFACE_UP_REDISTRIBUTE:
                RegnId.u4ContextId = pRtmMsgHdr->RegnId.u4ContextId;
                MEMSET (&Rt, 0, sizeof (tRtInfo));
                if (IP_COPY_FROM_BUF (pBuf, &Rt, 0,
                                      sizeof (tRtInfo)) != sizeof (tRtInfo))
                {
                    IP_RELEASE_BUF (pBuf, FALSE);
                    break;
                }
                if ((pRtmCxt = UtilRtmGetCxt (RegnId.u4ContextId)) != NULL)
                {
                    i4OutCome = RtmIsRouteExistInCxt (pRtmCxt, &Rt, &pExstRt);
                    if (i4OutCome == IP_ROUTE_FOUND)
                    {
                        RtmNotifyRtToAppsInCxt (pRtmCxt, pExstRt, NULL,
                                                NETIPV4_ADD_ROUTE);
                    }
                }
                IP_RELEASE_BUF (pBuf, FALSE);
                break;

            default:
                IP_RELEASE_BUF (pBuf, FALSE);
                break;
        }
    }                            /* While */
}

/*-------------------------------------------------------------------+
 *
 * Function           : RTM_Process_Filter_Added_Event
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Returns            : SUCCESS, FAILURE
 *
 * Action             : This function Proceeses the Message posted for Filters
*-------------------------------------------------------------------*/
VOID
RTM_Process_Filter_Added_Event (VOID)
{
    tIpBuf             *pBuf = NULL;
    tRtmSnmpIfMsg      *pSnmpIfMsg = NULL;

    while (OsixQueRecv (gRtmSnmpMsgQId, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        pSnmpIfMsg = (tRtmSnmpIfMsg *) IP_GET_MODULE_DATA_PTR (pBuf);

        switch (pSnmpIfMsg->u1Cmd)
        {
            case RTM_CHG_IN_RRD_CONTROL_TABLE:
                RtmHandleChgInFilterTable (pSnmpIfMsg->pRtmFilter,
                                           pSnmpIfMsg->u1PermitOrDeny);
                break;

            case RTM_CHG_IN_RRD_DEF_CTRL_MODE:
                RtmHandleChgInDefMode (pSnmpIfMsg->pRtmFilter,
                                       pSnmpIfMsg->u1PermitOrDeny);
                break;

            case RTM_OSPF_INT_EXT_CHG:
                RtmHandleChgInOspfType (&(pSnmpIfMsg->RegnId),
                                        pSnmpIfMsg->u1OspfRtType,
                                        pSnmpIfMsg->u1PermitOrDeny);
                break;
            default:
                break;
        }
        IP_RELEASE_BUF (pBuf, FALSE);
    }
}

/*-------------------------------------------------------------------+ 
* 
* Function           : RTM_Process_Route_Msg_Event 
* 
* Input(s)           : None 
* 
* Output(s)          : None 
* 
* Returns            : None 
* 
* Action             : This function Proceeses the Message posted for 
*                      route updates. 
*-------------------------------------------------------------------*/
VOID
RTM_Process_Route_Msg_Event (VOID)
{
    tRtmRtMsg          *pRtmRtMsg = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    UINT4               u4Count = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2Index = 0;
    INT4                i4RetVal = IP_FAILURE;

#ifdef RTM_ROUTE_CXT_FLAG
    tRtmRegnId          RegnId;
    UINT2               u2Protocol = 0;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));
#endif

    while (OsixQueRecv (gRtmRtMsgQId, (UINT1 *) &pRtmRtMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pRtmRtMsg->u1MsgType)
        {
            case RTM_REGN_MESSAGE:

                pRtmCxt =
                    UtilRtmGetCxt (pRtmRtMsg->unRtmMsg.RtmRegnId.u4ContextId);
                if (pRtmCxt == NULL)
                {
                    RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                    continue;
                }

                RtmUtilRegisterInCxt (pRtmCxt, &pRtmRtMsg->unRtmMsg.RtmRegnId,
                                      pRtmRtMsg->unRtmMsg.RtmRegnMsg.u1BitMask,
                                      pRtmRtMsg->unRtmMsg.RtmRegnMsg.
                                      DeliverToApplication);

                RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                break;

            case RTM_DEREGN_MESSAGE:

                pRtmCxt =
                    UtilRtmGetCxt (pRtmRtMsg->unRtmMsg.RtmRegnId.u4ContextId);
                if (pRtmCxt == NULL)
                {
                    RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                    continue;
                }
                RtmUtilDeregister (pRtmCxt, &pRtmRtMsg->unRtmMsg.RtmRegnId);
                RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                break;

            case RTM_ADD_ROUTE:
            case RTM_DELETE_ROUTE:
            case RTM_MODIFY_ROUTE:
                i4RetVal = RtmUtilIpv4LeakRoute (pRtmRtMsg->u1MsgType,
                                                 &pRtmRtMsg->unRtmMsg.RtInfo);
#ifdef RTM_ROUTE_CXT_FLAG
                u2Protocol = pRtmRtMsg->unRtmMsg.RtInfo.u2RtProto;
                if ((u2Protocol == BGP_ID) || (u2Protocol == OSPF_ID))
                {
                    pRtmCxt =
                        UtilRtmGetCxt (pRtmRtMsg->unRtmMsg.RtInfo.u4ContextId);
                    if (pRtmCxt == NULL)
                    {
                        RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                        continue;
                    }

                    RegnId.u4ContextId = pRtmCxt->u4ContextId;
                    RegnId.u2ProtoId = pRtmRtMsg->unRtmMsg.RtInfo.u2RtProto;
                    RtmSendRouteAckToRpInCxt (pRtmCxt, &RegnId,
                                              &pRtmRtMsg->unRtmMsg.RtInfo,
                                              i4RetVal);
                }
#else
                UNUSED_PARAM (i4RetVal);

#endif
                RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                u4Count++;

                if (u4Count > MAX_RTM_PROCESS_ROUTE_COUNT)
                {
                    OsixEvtSend (gRtmTaskId, RTM_ROUTE_MSG_EVENT);
                    OsixEvtSend (gRtmTaskId, RTM_ROUTE_PROCESS_EVENT);
                    return;
                }
                break;

            case RTM_OPER_UP_MESSAGE:
                u2Index = (UINT2) pRtmRtMsg->unRtmMsg.RtInfo.u4RtIfIndx;
                if (NetIpv4GetCxtId (u2Index, &u4ContextId) == NETIPV4_FAILURE)
                {

                    RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                    return;
                }

                pRtmCxt = UtilRtmGetCxt (u4ContextId);

                if (pRtmCxt == NULL)
                {
                    RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                    return;
                }

                RtmHandleOperUpIndicationInCxt (pRtmCxt, u2Index);
                RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                break;

            default:
                RTM_ROUTE_MSG_FREE (pRtmRtMsg);
                break;
        }

    }
}
