/* * $Id: rtmsz.c,v 1.6 2013/12/18 12:48:31 siva Exp $ */
#ifndef _RTMSZ_C
#define _RTMSZ_C
#include "rtminc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

INT4
RtmSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RTM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsRTMSizingParams[i4SizingId].u4StructSize,
                                     FsRTMSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(RTMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            RtmSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
RtmSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRTMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RTMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
RtmSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RTM_MAX_SIZING_ID; i4SizingId++)
    {
        if (RTMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RTMMemPoolIds[i4SizingId]);
            RTMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
#endif
