/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmapi.c,v 1.70.2.1 2018/04/16 13:17:34 siva Exp $
 *
 * Description: Action routines of API for RTM
 *
 *******************************************************************/

#include "rtminc.h"
#include "ipcli.h"
#include "iss.h"
#include "fsmirtlw.h"

UINT4               gu4InvalidStRouteCnt = IP_ZERO;
extern UINT4        gu4ValidStRouteCnt;
static tRtInfo      gInRouteInfo;
/*****************************************************************************/
/* Function     : RtmApiTriggerArpResolutionInCxt                            */
/*                                                                           */
/* Description  :Initiates ARP Resolution after the route lookup             */
/*               This function is called only from the ARP module            */
/*               ARP Lock is already taken before calling this function,     */
/*               hence inside this fn, ARP Calls are done without ARP_LOCK   */
/*                                                                           */
/* Input        : u4Destination - Destination address                        */
/*              : u4ContextId - u4Context Id                                 */
/*              : pBuf - Pending buffer to be sent after arp resolution      */
/*                                                                           */
/* Output       : None                                                       */
/* Return Value : IP_SUCCESS/IP_FAILURE                                      */
/*****************************************************************************/
PUBLIC INT4
RtmApiTriggerArpResolutionInCxt (UINT4 u4ContextId, UINT4 u4Destination,
                                 tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               au4Indx[2];    /* The Key of Trie is made-up of address
                                       and mask */
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pRtInfos[MAX_ROUTING_PROTOCOLS];
    tRtInfo            *pDropRt = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo            *pRtInfo[RTM_MAX_PATHS];
    UINT4               u4IfIndex = 0;
    INT4                i4Status = IP_FAILURE;
    INT4                i4RetVal = IP_FAILURE;
    UINT2               u2RtType = 0;
    UINT1               au1NewFlag[RTM_MAX_PATHS];
    UINT1               u1UpdationRequired = FALSE;
    UINT1               u1Count = 0;
    UINT1               u1PathCnt = 0;
    INT1                i1State = 0;

    /* Take RTM Lock */
    RTM_PROT_LOCK ();
    /* Initialize the RtInfo array to NULL */
    pRtmCxt = UtilRtmGetCxt (u4ContextId);
    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return i4RetVal;
    }

    MEMSET ((INT1 *) pRtInfos, 0, (sizeof (tRtInfo *) * MAX_ROUTING_PROTOCOLS));

    au4Indx[0] = IP_HTONL (u4Destination);
    au4Indx[1] = ISS_IP_DEF_NET_MASK;

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    OutParams.pAppSpecInfo = pRtInfos;

    /* Identify the route entry to be used for reaching the
     * given destination */
    i4Status = TrieLookupEntry (&(InParams), &(OutParams));

    if (i4Status == TRIE_SUCCESS)
    {
        i4Status = IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRt,
                                              RTM_GET_BST_WITH_PARAMS);

        if (i4Status == IP_SUCCESS)
        {
            u2RtType = pRt->u2RtType;

            u4IfIndex = pRt->u4RtIfIndx;

            /* Store all nexthop informations in the array */
            pTmpRt = pRt;
            pDropRt = pRt;
            while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pRt->i4Metric1))
            {
                if (pTmpRt->u4RowStatus == IPFWD_ACTIVE)
                {
                    MALLOC_IP_ROUTE_ENTRY (pRtInfo[u1PathCnt]);
                    if (pRtInfo[u1PathCnt] == NULL)
                    {
                        for (u1Count = 0; u1Count < u1PathCnt; u1Count++)
                        {
                            IP_RT_FREE (pRtInfo[u1Count]);
                        }
                        RTM_PROT_UNLOCK ();
                        return IP_FAILURE;
                    }

                    MEMCPY (pRtInfo[u1PathCnt], pTmpRt, sizeof (tRtInfo));
                    if (pTmpRt->u2RtType == IPROUTE_LOCAL_TYPE)
                    {
                        pRtInfo[u1PathCnt]->u4NextHop = u4Destination;
                    }
                    u1PathCnt++;
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }
        }
    }
    else
    {
#ifdef NPAPI_WANTED
        if (IpFsNpCfaVrfSetDlfStatus (pRtmCxt->u4ContextId, FALSE) ==
            FNP_SUCCESS)
        {
            RtmTmrSetTimer (&(pRtmCxt->RtmDLFTmr), RTM_DLF_TIMER,
                            RTM_DLF_TIMER_INTERVAL);
        }
#endif
    }

    RTM_PROT_UNLOCK ();
    if (i4Status == IP_SUCCESS)
    {
        /* Packet is destined to the directly connected Host..
         * Trigger ARP using the destination of the packet. */
        if ((u2RtType == IPROUTE_LOCAL_TYPE) && (u1PathCnt == 1))
        {
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
            if (ArpInitiateAddressResolution
                (u4Destination, u4IfIndex, &i1State, pBuf) == ARP_FAILURE)
            {
                /* Remove the Entry created in the DLF Hash Table */
                IpFsCfaHwRemoveIpNetRcvdDlfInHash (pRtmCxt->u4ContextId,
                                                   u4Destination, 0xffffffff);
            }
#else
            ArpInitiateAddressResolution (u4Destination, u4IfIndex, &i1State,
                                          pBuf);
#endif
        }
        else
        {
            MEMSET (au1NewFlag, 0, sizeof (UINT1) * RTM_MAX_PATHS);

            /* Trigger ARP using nexthops of the identified route */
            u1Count = 0;
            while (u1Count < u1PathCnt)
            {
                /* Add drop route in hardware for unreachable next-hop entry */
                if ((pRtInfo[u1Count]->u4Flag & RTM_RT_REACHABLE) == 0)
                {
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
                    if (ArpInitiateAddressResolution
                        (pRtInfo[u1Count]->u4NextHop,
                         pRtInfo[u1Count]->u4RtIfIndx, &i1State,
                         pBuf) == ARP_FAILURE)
                    {
                        /* Remove the Entry created in the DLF Hash Table */
                        IpFsCfaHwRemoveIpNetRcvdDlfInHash (pRtmCxt->u4ContextId,
                                                           u4Destination,
                                                           0xffffffff);
                    }
#else
                    ArpInitiateAddressResolution (pRtInfo[u1Count]->u4NextHop,
                                                  pRtInfo[u1Count]->u4RtIfIndx,
                                                  &i1State, pBuf);
#endif
                    pBuf = NULL;

                    /* pBuf is set as NULL from the second route. In case of
                     * ECMP routes, the data should take only one path, so that
                     * the data is sent after the ARP is resolved for the next 
                     * hop. */
                    if ((u2RtType == IPROUTE_REMOTE_TYPE) && (u1PathCnt == 1))
                    {
                        /* Deleting the route installed as local route
                         * and adding the drop route */
                        if ((pDropRt->u4Flag & RTM_RT_DROP_ROUTE) == 0)
                        {

                            RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt,
                                                              pDropRt, 1);
                            pDropRt->u4Flag |= RTM_RT_DROP_ROUTE;
                            RtmAddRouteToDataPlaneInCxt (pRtmCxt, pDropRt, 1);
                        }

                    }

                    if ((i1State == ARP_DYNAMIC) || (i1State == ARP_STATIC))
                    {
                        /* If the route is already installed to NP, Dont install
                         * it again */
                        au1NewFlag[u1Count] = RTM_RT_REACHABLE;
                    }
                    else
                    {
                        au1NewFlag[u1Count] = RTM_RT_IN_PRT;
                    }
                    u1UpdationRequired = TRUE;
#ifdef LNXIP4_WANTED
                    /* When ECMP route is installed in H/W and the packet is 
                     * received to CPU, any of the unreachable ECMP route may 
                     * have been selected.So trigger arp for all unreachable
                     * next hops */
                    if (au1NewFlag[u1Count] == RTM_RT_IN_PRT)
                    {
                        ArpSendRequest ((UINT2) pRtInfo[u1Count]->u4RtIfIndx,
                                        pRtInfo[u1Count]->u4NextHop,
                                        ARP_ENET_TYPE, CFA_ENCAP_ENETV2);
                    }
#endif
                }
                u1Count++;
            }

            if (u1UpdationRequired == TRUE)
            {
                RTM_PROT_LOCK ();
                pRtmCxt = UtilRtmGetCxt (u4ContextId);
                if (pRtmCxt == NULL)
                {
                    RTM_PROT_UNLOCK ();
                    for (u1Count = 0; u1Count < u1PathCnt; u1Count++)
                    {
                        IP_RT_FREE (pRtInfo[u1Count]);
                    }
                    return IP_FAILURE;
                }

                RtmUpdateRoutesForArpInitiationInCxt (pRtmCxt,
                                                      pRtInfo, u1PathCnt,
                                                      au1NewFlag);

                RTM_PROT_UNLOCK ();
            }
        }
        i4RetVal = IP_SUCCESS;
    }

    for (u1Count = 0; u1Count < u1PathCnt; u1Count++)
    {
        IP_RT_FREE (pRtInfo[u1Count]);
    }

    /* Release the RTM protocol Lock */
    return i4RetVal;
}

/*****************************************************************************
 * Function     : RtmApiHandlePRTRoutesForArpUpdationInCxt                   *
 * Description  : Scans PRT for the Resolved NextHop and deletes them        *
 * Input        : u4NextHopAddr - NextHop Address                            *
 *                i1Flag - Indicates state of the ARP                        *
 *                u4ContextId - RTM Context ID                               *
 *                u1Operation - Inicates whether ARP addition/deletion       *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
PUBLIC INT4
RtmApiHandlePRTRoutesForArpUpdationInCxt (UINT4 u4ContextId,
                                          UINT4 u4NextHopAddr,
                                          UINT1 u1Operation, INT1 i1Flag)
{
    tPNhNode           *pPNhEntry = NULL, ExstNhNode;
    tPRtEntry          *pPRtEntry;
    tRtInfo            *pBestRt = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    UINT4               u4IfIndex = IP_ZERO;
    INT4                i4RetVal = IP_FAILURE;
    UINT1               u1RtCount = 1;
    UINT1               u1EncapType;
    UINT1               u1NHReachState = FALSE;
    INT1                ai1MacAddr[CFA_ENET_ADDR_LEN];

    /* Take RTM Lock */
    RTM_PROT_LOCK ();
    /* Initialize the RtInfo array to NULL */
    pRtmCxt = UtilRtmGetCxt (u4ContextId);
    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return i4RetVal;
    }

    if (u1Operation == ARP_DELETION)
    {
        if (i1Flag != ARP_PENDING)
        {
            /* Handle the deletion of valid ARP entry */
            i4RetVal = RtmHandleArpEntryDeletionInCxt (pRtmCxt, u4NextHopAddr);
            RTM_PROT_UNLOCK ();
            return i4RetVal;
        }
    }
#if defined (NPAPI_WANTED) && defined (CFA_WANTED)
    else if (i1Flag != ARP_PENDING)
    {
        /* Remove the Entry created in the DLF Hash Table */
        IpFsCfaHwRemoveIpNetRcvdDlfInHash (pRtmCxt->u4ContextId, u4NextHopAddr,
                                           0xffffffff);
    }
#endif

    ExstNhNode.u4NextHop = u4NextHopAddr;
    ExstNhNode.pRtmCxt = pRtmCxt;

    pPNhEntry = RBTreeGet (gRtmGlobalInfo.pPRTRBRoot, &ExstNhNode);

    /* No route exists with this next hop in PRT */
    if (pPNhEntry == NULL)
    {
        if (u1Operation == ARP_ADDITION)
        {
            i4RetVal = IP_SUCCESS;
            if (RtmHandleECMPPRTRtsForArpUpdationInCxt (pRtmCxt, u4NextHopAddr)
                == IP_FAILURE)
            {
                i4RetVal = IP_FAILURE;
            }
            RTM_PROT_UNLOCK ();
            return i4RetVal;
        }

        i4RetVal = IP_SUCCESS;
        RTM_PROT_UNLOCK ();
        return i4RetVal;
    }

    if (u1Operation == ARP_ADDITION)
    {
        /* ARP is resolved ..Update the reachable nexthop table */
        RtmUpdateResolvedNHEntryInCxt (pRtmCxt, u4NextHopAddr, RTM_ADD_ROUTE);
    }

    /* Process all the routes having this next hop in PRT */
    while ((pPRtEntry = (tPRtEntry *)
            TMO_SLL_First (&pPNhEntry->routeEntryList)) != NULL)
    {
        u1NHReachState = FALSE;
        TMO_SLL_Delete (&pPNhEntry->routeEntryList,
                        (tTMO_SLL_NODE *) pPRtEntry);

        /* Reset the flag that indicates whether route exists in 
         * PRT or not */
        pPRtEntry->pPendRt->u4Flag =
            (pPRtEntry->pPendRt->u4Flag & (UINT4) (~RTM_RT_IN_PRT));

        IpGetBestRouteEntryInCxt (pRtmCxt, *(pPRtEntry->pPendRt), &pBestRt);

        if (pPRtEntry->pPendRt->u4Flag & RTM_ECMP_RT)
        {
            /* ECMP route */
            /* Delete the route entry installed in Hardware with
             * old ARP entry information */
            RtmHandleECMPRtInCxt (pRtmCxt, pPRtEntry->pPendRt,
                                  pBestRt, NETIPV4_DELETE_ROUTE,
                                  RTM_DONT_SET_RT_REACHABLE);
            pPRtEntry->pPendRt->u4Flag |= RTM_ECMP_RT;
            /* Add the route to data plane with new ARP Entry Information */
            if (u1Operation == ARP_ADDITION)
            {
                RtmHandleECMPRtInCxt (pRtmCxt, pPRtEntry->pPendRt,
                                      pBestRt, NETIPV4_ADD_ROUTE,
                                      RTM_SET_RT_REACHABLE);
            }
            else
            {
                RtmHandleECMPRtInCxt (pRtmCxt, pPRtEntry->pPendRt,
                                      pBestRt, NETIPV4_ADD_ROUTE,
                                      RTM_DONT_SET_RT_REACHABLE);
            }
            RtmNotifyNewECMPRouteInCxt (pRtmCxt, pBestRt,
                                        pPRtEntry->pPendRt->i4Metric1);
        }
        else
        {
            /* Delete the route entry installed in Hardware with
             * old ARP entry information */

            ARP_PROT_UNLOCK ();
            /* ARP protocol lock is taken within the API ArpResolveWithIndex */
            i4RetVal =
                CfaIpIfGetIfIndexForNetInCxt (u4ContextId,
                                              pPRtEntry->pPendRt->u4NextHop,
                                              &u4IfIndex);
            if (i4RetVal == CFA_FAILURE)
            {
                i4RetVal = IP_FAILURE;
                RTM_PROT_UNLOCK ();
                return i4RetVal;

            }
            i4RetVal =
                ArpResolveWithIndex (u4IfIndex, pPRtEntry->pPendRt->u4NextHop,
                                     ai1MacAddr, &u1EncapType);
            ARP_PROT_LOCK ();
            if (i4RetVal == ARP_SUCCESS)
            {
                u1NHReachState = TRUE;    /* set the nexthop is reachable */
                pPRtEntry->pPendRt->u4Flag |= RTM_RT_REACHABLE;
            }

            if (((pPRtEntry->pPendRt->u4Flag) & (RTM_RT_DROP_ROUTE))
                && (!((pPRtEntry->pPendRt->u4Flag) & RTM_RT_REACHABLE)))
            {
                i4RetVal = IP_SUCCESS;
                RTM_PROT_UNLOCK ();
                return i4RetVal;
            }

            else
            {
                if ((pPRtEntry->pPendRt->u4Flag & RTM_RT_REACHABLE))
                {
                    if (pPRtEntry->pPendRt->u4Flag & RTM_RT_DROP_ROUTE)
                    {
                        pPRtEntry->pPendRt->u4Flag &= (~RTM_RT_DROP_ROUTE);
                    }

                    RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt,
                                                      pPRtEntry->pPendRt,
                                                      u1RtCount);
                    if (u1Operation == ARP_ADDITION)
                    {
                        pPRtEntry->pPendRt->u4Flag |= RTM_RT_REACHABLE;
                        RtmUpdateResolvedNHEntryInCxt (pRtmCxt,
                                                       pPRtEntry->pPendRt->
                                                       u4NextHop,
                                                       RTM_ADD_ROUTE);
                    }
                    /* Add the route to data plane with new ARP Entry Information */
                    RtmAddRouteToDataPlaneInCxt (pRtmCxt,
                                                 pPRtEntry->pPendRt, u1RtCount);

                    /* Delete the oute from PRT if present */
                    if (pPRtEntry->pPendRt->u4Flag & RTM_RT_IN_PRT)
                    {
                        RtmDeleteRtFromPRTInCxt (pRtmCxt, pPRtEntry->pPendRt);
                    }

                    if (pPRtEntry->pPendRt->u4Flag & RTM_RT_IN_ECMPPRT)
                    {
                        RtmDeleteRtFromECMPPRTInCxt (pRtmCxt,
                                                     pPRtEntry->pPendRt);
                    }
                }
            }

        }

        MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId, (UINT1 *) pPRtEntry);
    }
    if (u1NHReachState == TRUE)
    {
        if (RBTreeRemove (gRtmGlobalInfo.pPRTRBRoot, pPNhEntry) == RB_FAILURE)
        {
            RTM_PROT_UNLOCK ();
            return i4RetVal;
        }
        MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                            (UINT1 *) pPNhEntry);
    }

    i4RetVal = IP_SUCCESS;
    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

/************************************************************************/
/* Function           : RtmApiIsReachableNextHopInCxt                      */
/*                                                                      */
/* Input(s)           : u4NextHop - IP Address                          */
/*                      pRtmCxt -RTM Context Pointer                    */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : Returns TRUE if IP Address exists in Resolved   */
/*                      Next Hop Table Else FALSE                       */
/************************************************************************/
UINT1
RtmApiIsReachableNextHopInCxt (UINT4 u4ContextId, UINT4 u4NextHopAddr)
{
    tRtmCxt            *pRtmCxt = NULL;

    RTM_PROT_LOCK ();

    pRtmCxt = UtilRtmGetCxt (u4ContextId);
    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return FALSE;
    }

    if (RtmIsReachableNextHopInCxt (pRtmCxt, u4NextHopAddr) == FALSE)
    {
        RTM_PROT_UNLOCK ();
        return FALSE;
    }
    RTM_PROT_UNLOCK ();
    return TRUE;

}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmIpv4LeakRoute
 *
 * Input(s)           : u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetRtInfo  - Route Information.
 *                      This function is protected by RTMv4 Lock 
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS or IP_FAILURE
 *
 * Action             : This function either add/modify/delete the route
 *                      from IP Trie and redistributes
 *                      the route to the registered routing protocols
 *         
+-------------------------------------------------------------------*/
INT4
RtmIpv4LeakRoute (UINT1 u1CmdType, tNetIpv4RtInfo * pNetRtInfo)
{
    INT4                i4RetVal = IP_FAILURE;
    UINT4               u4EventMask = 0;
    tRtmRtMsg          *pRtmMsg = NULL;

    /* The below changes are done for programming route in hardware in 
       RTM context instead of protocol context. If route is programmed 
       using protocol context, the SDK makes priority inversion of the task 
       resulting in the protocol task to run in lower proiority. This results 
       in failures during scalability testing as sufficient time is not given 
       to the protocol task. This is achieved by using a queue to post 
       the route details and handling the same. This will be followed 
       when gu1RtmRouteCxtFlag is set to OSIX_TRUE */

    if (gu1RtmRouteCxtFlag == OSIX_TRUE)
    {
        if (RTM_ROUTE_MSG_ENTRY_ALLOC (pRtmMsg) == NULL)
        {
            UtlTrcLog (1, 1, "",
                       "INFO[RTM]: Route Entry Message Allocation Failure - Processing the RTM QUEUE Entries\n");
            while (1)
            {
                OsixEvtRecv (gRtmTaskId, RTM_ROUTE_PROCESS_EVENT,
                             RTM_EVENT_WAIT_FLAGS, &u4EventMask);
                if (MemGetFreeUnits (gRtmGlobalInfo.RtmRtMsgPoolId) > 0)
                {
                    break;
                }
            }

            if (RTM_ROUTE_MSG_ENTRY_ALLOC (pRtmMsg) == NULL)
            {
                UtlTrcLog (1, 1, "",
                           "ERROR[RTM]: Route Entry Message Allocation Failure\n");
                return IP_FAILURE;
            }
        }

        pRtmMsg->u1MsgType = u1CmdType;

        if (MEMCPY (&(pRtmMsg->unRtmMsg.RtInfo), pNetRtInfo,
                    sizeof (tNetIpv4RtInfo)) == NULL)
        {
            RTM_ROUTE_MSG_FREE (pRtmMsg);
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Route Entry Message Creation Failure \n");
            return IP_FAILURE;
        }

        if (OsixQueSend (gRtmRtMsgQId, (UINT1 *) &pRtmMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            RTM_ROUTE_MSG_FREE (pRtmMsg);
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Route Entry Message Send to Queue Failure\n");
            return (RTM_FAILURE);
        }

        OsixEvtSend (gRtmTaskId, RTM_ROUTE_MSG_EVENT);

        return IP_SUCCESS;
    }
    else
    {
        RTM_PROT_LOCK ();
        i4RetVal = RtmUtilIpv4LeakRoute (u1CmdType, pNetRtInfo);
        RTM_PROT_UNLOCK ();
    }
    return i4RetVal;
}

/******************************************************************************
    Function            :   IpGetFwdTableRouteNumInCxt

    Description         :   This function returns the number of route present
                            in the IP Fwd Table

    Input parameters    :   u4RtmCxt     - Rtm context

    Output parameters   :   pu4IpFwdTblRouteNum - number of routes in IP Fwd
                                                  table.

    Global variables
    Affected            :   None. 

    Return value        :   None.

******************************************************************************/
VOID
IpGetFwdTableRouteNumInCxt (UINT4 u4ContextId, UINT4 *pu4IpFwdTblRouteNum)
{
    tRtmCxt            *pRtmCxt = NULL;
    pRtmCxt = UtilRtmGetCxt (u4ContextId);
    if (pRtmCxt == NULL)
    {
        return;
    }
    *pu4IpFwdTblRouteNum = pRtmCxt->u4IpFwdTblRouteNum;
    return;
}

 /******************************************************************************
 * Function Name    :   RtmNetIpv4GetRoute 
 *
 * Description      :   This function provides the Route (either Exact Route or
 *                      Best route) for a given destination and Mask based on
 *                      the incoming request.
 *
 * Inputs           :   pRtQuery - Infomation about the route to be
 *                                 retrieved.
 *
 * Outputs          :   pNetIpRtInfo - Information about the requested route.
 *
 * Return Value     :   IP_SUCCESS - if the route is present.
 *                      IP_FAILURE - if the route is not present.
 *
 ****************************************************************************** */
INT4
RtmNetIpv4GetRoute (tRtInfoQueryMsg * pRtQuery, tNetIpv4RtInfo * pNetIpRtInfo)
{
    INT4                i4RetVal = IP_FAILURE;

    RTM_PROT_LOCK ();

    i4RetVal = RtmUtilIpv4GetRoute (pRtQuery, pNetIpRtInfo);

    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
 *                                                                           *
 * Function           : RtmNetIpv4GetNextBestRtEntryInCxt                    *
 *                                                                           *
 * Input(s)           : IP address  & Mask in tRtInfo                        * 
 *                    : u4ContextId -RTM Context ID                          * 
 *                                                                           *
 * Output(s)          :  Next Route Entry Information (pOutRtInfo)           *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           * 
 * Action :                                                                  *
 *   Fetches the Next Best Route Entry For the given IpAddress & NetMask.    *
 *   If The Input IpAddress & NetMask is 0, then First Entry from            * 
 *   the IP Forward Table is given as Output. The Values are to be set       *
 *   in InRtInfo, by the Calling Routing.                                    *
 *****************************************************************************/

INT1
RtmNetIpv4GetNextBestRtEntryInCxt (UINT4 u4ContextId, tRtInfo InRtInfo,
                                   tRtInfo * pOutRtInfo)
{
    tRtmCxt            *pRtmCxt = NULL;

    RTM_PROT_LOCK ();
    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return IP_FAILURE;
    }
    if (RtmUtilGetNextBestRtInCxt (pRtmCxt, InRtInfo, pOutRtInfo) == IP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return IP_FAILURE;
    }
    RTM_PROT_UNLOCK ();
    return IP_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  IpTableGetObjectInCxt                            */
/*                                                                       */
/* Description       :  This fuction does a GET Object in  the  IP Cidr  */
/*                      forwarding table based on the Object type        */
/*                      specified.                                       */
/*                      RTM protocol should be taken before calling this */
/*                      Function                                         */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  u4Dest - The Destination address.                */
/*                      u4Mask - The Desination Mask                     */
/*                      i4Tos  - The Type of service                     */
/*                      u4NextHop - The Next hop gateway address         */
/*                      i4Proto -   The routing protocol                 */
/*                      u1ObjType - The Object type being accessed       */
/*                      u4ContextId - RTM Context ID                     */
/*                                                                       */
/* Output(s)         :  *pi4Val - The Value of the object for which the  */
/*                      GET has been performed                           */
/*                                                                       */
/* Returns           :  IP_SUCCESS | IP_FAILURE                          */
/*************************************************************************/
INT4
IpTableGetObjectInCxt (UINT4 u4ContextId, UINT4 u4Dest, UINT4 u4Mask,
                       INT4 i4Tos, UINT4 u4NextHop, INT4 i4Proto, INT4 *pi4Val,
                       UINT1 u1ObjType)
{
    UINT1               u1FoundFlag = FALSE;
    UINT1               u1MinPreference = IP_PREFERENCE_INFINITY;
    UINT4               u4App = 0;
    INT4                i4OutCome = 0;
    UINT4               au4Indx[IP_TWO];
    tOutputParams       OutParams;
    tInputParams        InParams;
    tRtInfo            *pRt = NULL;
    tRtInfo             ExstRt;
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo            *pStRt = NULL;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return IP_FAILURE;
    }

    if (RtmUtilCheckRtExistsWithInvalidIfIndex (pRtmCxt, u4Dest, u4Mask,
                                                (UINT4) i4Tos, u4NextHop,
                                                &ExstRt) == TRUE)
    {
        u1FoundFlag = TRUE;
    }
    else
    {
        /* Check whether we have the route for the given index */
        au4Indx[0] = IP_HTONL (u4Dest);
        au4Indx[1] = IP_HTONL (u4Mask);

        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
        InParams.i1AppId = ALL_ROUTING_PROTOCOL;

        OutParams.Key.pKey = NULL;
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
        OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

        /* scan the route table */
        i4OutCome = TrieSearchEntry (&(InParams), &OutParams);

        if (i4OutCome == TRIE_FAILURE)
        {
            return IP_FAILURE;
        }

        if (i4Proto != ALL_ROUTING_PROTOCOL)
        {
            pRt = ((tRtInfo **) OutParams.pAppSpecInfo)[(UINT4) i4Proto - 1];
            if (pRt != NULL)
            {
                while ((pRt != NULL) && (u1FoundFlag == FALSE))
                {
                    /* Makefile Changes - comparison btwn signed and unsigned */
                    if ((pRt->u4NextHop == u4NextHop)
                        && (pRt->u4Tos == (UINT4) i4Tos))
                    {
                        MEMCPY (&ExstRt, pRt, sizeof (tRtInfo));
                        u1FoundFlag = TRUE;
                    }
                    else
                    {
                        pRt = pRt->pNextAlternatepath;
                    }
                }
            }
        }
        else
        {
            for (u4App = 0; u4App < MAX_ROUTING_PROTOCOLS; u4App++)
            {
                pRt = ((tRtInfo **) OutParams.pAppSpecInfo)[u4App];
                if (u4App == STATIC_RT_INDEX)
                {
                    if ((pRt != NULL) &&
                        (pRt->u4NextHop == u4NextHop) &&
                        (pRt->u4Tos == (UINT4) i4Tos))
                    {
                        pStRt = pRt;
                        u1FoundFlag = TRUE;
                        continue;
                    }
                }
                if (pRt != NULL)
                {
                    while (pRt != NULL)
                    {
                        /* Makefile Changes - comparison btwn signed and unsigned */
                        if ((pRt->u4NextHop == u4NextHop)
                            && (pRt->u4Tos == (UINT4) i4Tos)
                            && (pRt->u1Preference <= u1MinPreference))
                        {
                            u1FoundFlag = TRUE;
                            u1MinPreference = pRt->u1Preference;
                            MEMCPY (&ExstRt, pRt, sizeof (tRtInfo));
                        }
                        pRt = pRt->pNextAlternatepath;
                    }
                }
            }
            if ((pStRt != NULL) && (pStRt->u4RowStatus == IPFWD_ACTIVE)
                && (pStRt->i4Metric1 <= u1MinPreference))
            {
                MEMCPY (&ExstRt, pStRt, sizeof (tRtInfo));
            }
        }
    }
    if (u1FoundFlag == FALSE)
    {
        return IP_FAILURE;
    }

    switch (u1ObjType)
    {

        case IPFWD_ROUTE_IFINDEX:
            *pi4Val = (INT4) ExstRt.u4RtIfIndx;
            break;

        case IPFWD_ROUTE_TYPE:
            *pi4Val = ExstRt.u2RtType;
            break;

        case IPFWD_ROUTE_PROTO:
            *pi4Val = ExstRt.u2RtProto;
            break;

        case IPFWD_ROUTE_AGE:
            *pi4Val = (INT4) OsixGetSysUpTime ();
            *pi4Val -= (INT4) ExstRt.u4RtAge;
            break;

        case IPFWD_ROUTE_INFO:
            *pi4Val = IPFWD_ROUTE_INFO_DEFAULT;
            break;

        case IPFWD_ROUTE_NEXTHOPAS:
            *pi4Val = (INT4) ExstRt.u4RtNxtHopAS;
            break;

        case IPFWD_ROUTE_METRIC1:
            *pi4Val = ExstRt.i4Metric1;
            break;

        case IPFWD_ROUTE_STATUS:
            *pi4Val = (INT4) ExstRt.u4Flag;
            break;

        case IPFWD_ROUTE_PREFERENCE:
            /* Preference of Static route is stored in Metric field of route
             * Structure. This facilitates having a single function used for 
             * sorting the metric or Preference.
             */
            if (ExstRt.u1Preference != 0)
            {
                *pi4Val = ExstRt.u1Preference;
            }
            else
            {
                if (((ExstRt.u2RtProto - 1) >= 0) &&
                    ((ExstRt.u2RtProto - 1) < MAX_ROUTING_PROTOCOLS))
                {
                    *pi4Val = pRtmCxt->au1Preference[ExstRt.u2RtProto - 1];
                }
            }
            break;

        case IPFWD_ROUTE_METRIC2:
        case IPFWD_ROUTE_METRIC3:
        case IPFWD_ROUTE_METRIC4:
        case IPFWD_ROUTE_METRIC5:
            *pi4Val = IPFWD_ROUTE_METRIC_DEFAULT;
            break;

        case IPFWD_REDIS_CTRL_FLAG:
            if ((ExstRt.u1BitMask & RTM_PRIV_RT_MASK) == RTM_PRIV_RT_MASK)
            {
                *pi4Val = IP_DONT_REDIS_ROUTE;
            }
            else
            {
                *pi4Val = IP_REDIS_ROUTE;
            }
            break;

        case IPFWD_ROUTE_ROW_STATUS:
            if ((ExstRt.u4RowStatus == IPFWD_ACTIVE) ||
                (ExstRt.u4RowStatus == IPFWD_NOT_IN_SERVICE))
            {
                *pi4Val = (INT4) ExstRt.u4RowStatus;
            }
            else
            {
                *pi4Val = IPFWD_NOT_READY;
            }
            break;

        default:
            return IP_FAILURE;
    }

    return (IP_SUCCESS);
}

/**************************************************************************/
/*   Function Name   : RtmCheckRtExistsWithInvalidIfIndex                */
/*   Description     : This function fills  the route exists in temporary */
/*                     List.Else return FALSE                             */
/*   Input(s)        : u4Dest - Destination network                       */
/*                     u4Mask - Network Mask to be used for dest network  */
/*                     u4Tos  - Type of service assicated with the route  */
/*                     u4NextHop - NextHop Address of the route           */
/*                     u4ContextId - RTM Context ID                     */
/*   Output(s)       : pRtFound - Route found in temporary route list     */
/*   Return Value    : TRUE/FALSE                                         */
/**************************************************************************/
UINT1
RtmCheckRtExistsWithInvalidIfIndexInCxt (UINT4 u4ContextId, UINT4 u4Dest,
                                         UINT4 u4Mask, UINT4 u4Tos,
                                         UINT4 u4NextHop, tRtInfo * pRtFound)
{
    tRtmCxt            *pRtmCxt = NULL;
    UINT1               u1RtFound = FALSE;

    RTM_PROT_LOCK ();
    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return u1RtFound;
    }

    u1RtFound = RtmUtilCheckRtExistsWithInvalidIfIndex (pRtmCxt, u4Dest, u4Mask,
                                                        u4Tos, u4NextHop,
                                                        pRtFound);
    RTM_PROT_UNLOCK ();
    return u1RtFound;
}

/**************************************************************************/
/*   Function Name   : RtmProcessGRRouteCleanUp                           */
/*   Description     : This function will cleanup all the old routes      */
/*                     updated before graceful restart, and resets the    */
/*                     IP_GR_BIT updated during GR process.               */
/*   Input(s)        : pRegnID - Protocol Info whose routes needs to be   */
/*                     cleared                                            */
/*                     i1GRCompFlag - GR Process competion Flag           */
/*                     (IP_SUCCESS/IP_FAILURE)                            */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
RtmProcessGRRouteCleanUp (tRtmRegnId * pRegnId, INT1 i1GRCompFlag)
{
    tRtmCxt            *pRtmCxt = NULL;

    RTM_PROT_LOCK ();
    pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);

    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return;
    }
    RtmUtilProcessGRRouteCleanUp (pRtmCxt, pRegnId, i1GRCompFlag);
    RTM_PROT_UNLOCK ();
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmDeregister
 *
 * Input(s)           : pRegnId - DeRegistraion Info of the routing protocol
 *                      which sent this DeRegistration message.
 *
 * Output(s)          : Updation of Registration table 
 *
 * Returns            : RTM_SUCCESS/RTM_FAILURE
 *
 * Action             : This function handles the DeRegistration message  
 *                      from the routing protocols.
 * 
+-------------------------------------------------------------------*/
INT4
RtmDeregister (tRtmRegnId * pRegnId)
{

    tRtmCxt            *pRtmCxt = NULL;
    tRtmRtMsg          *pRtmMsg = NULL;
    INT4                i4RetVal = RTM_FAILURE;

    if (gu1RtmRouteCxtFlag == OSIX_TRUE)
    {
        if (RTM_ROUTE_MSG_ENTRY_ALLOC (pRtmMsg) == NULL)
        {
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Registration Message Allocation Failure\n");
            return RTM_FAILURE;
        }

        MEMSET (pRtmMsg, 0, sizeof (tRtmRtMsg));

        pRtmMsg->u1MsgType = RTM_DEREGN_MESSAGE;

        if (MEMCPY (&(pRtmMsg->unRtmMsg.RtmRegnId), pRegnId,
                    sizeof (tRtmRegnId)) == NULL)
        {
            RTM_ROUTE_MSG_FREE (pRtmMsg);
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Registration Message Creation Failure \n");
            return RTM_FAILURE;
        }

        if (OsixQueSend (gRtmRtMsgQId, (UINT1 *) &pRtmMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            RTM_ROUTE_MSG_FREE (pRtmMsg);
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Registration Message Send to Queue Failure\n");
            return (RTM_FAILURE);
        }

        OsixEvtSend (gRtmTaskId, RTM_ROUTE_MSG_EVENT);

        return RTM_SUCCESS;
    }
    else
    {
        RTM_PROT_LOCK ();
        pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);

        if (pRtmCxt == NULL)
        {
            RTM_PROT_UNLOCK ();
            return i4RetVal;
        }
        i4RetVal = RtmUtilDeregister (pRtmCxt, pRegnId);
        RTM_PROT_UNLOCK ();
    }
    return i4RetVal;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmRegister
 *
 * Input(s)           : pRegnId - Registration Info about the protocol
 *                                registering with RTM
 *                      u1BitMask   - Value indicating whether route needs to
 *                                    be redistributed or not.
 *                      SendToApplication - Callback Function for handling
 *                                          message from RTM. 
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS - If the registration is successful
 *                      else other failure values.
 *
 * Action             : This function handles the registration by the routing 
 *                      protocols. Sends acknowledgement when the 
 *                      registration is successful. 
 *         
+-------------------------------------------------------------------*/
INT4
RtmRegister (tRtmRegnId * pRegnId,
             UINT1 u1BitMask,
             INT4 (*SendToApplication) (tRtmRespInfo * pRespInfo,
                                        tRtmMsgHdr * RtmHeader))
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtmRtMsg          *pRtmMsg = NULL;
    INT4                i4RetVal = RTM_FAILURE;

    if (gu1RtmRouteCxtFlag == OSIX_TRUE)
    {
        if (RTM_ROUTE_MSG_ENTRY_ALLOC (pRtmMsg) == NULL)
        {
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Registration Message Allocation Failure\n");
            return RTM_FAILURE;
        }

        MEMSET (pRtmMsg, 0, sizeof (tRtmRtMsg));

        pRtmMsg->u1MsgType = RTM_REGN_MESSAGE;
        pRtmMsg->unRtmMsg.RtmRegnMsg.u1BitMask = u1BitMask;
        pRtmMsg->unRtmMsg.RtmRegnMsg.DeliverToApplication = SendToApplication;

        if (MEMCPY (&(pRtmMsg->unRtmMsg.RtmRegnId), pRegnId,
                    sizeof (tRtmRegnId)) == NULL)
        {
            RTM_ROUTE_MSG_FREE (pRtmMsg);
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Registration Message Creation Failure \n");
            return RTM_FAILURE;
        }

        if (OsixQueSend (gRtmRtMsgQId, (UINT1 *) &pRtmMsg,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            RTM_ROUTE_MSG_FREE (pRtmMsg);
            UtlTrcLog (1, 1, "",
                       "ERROR[RTM]: Registration Message Send to Queue Failure\n");
            return (RTM_FAILURE);
        }

        OsixEvtSend (gRtmTaskId, RTM_ROUTE_MSG_EVENT);

        return RTM_SUCCESS;
    }
    else
    {
        RTM_PROT_LOCK ();
        pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);
        if (pRtmCxt == NULL)
        {
            RTM_PROT_UNLOCK ();
            return i4RetVal;
        }
        i4RetVal =
            RtmUtilRegisterInCxt (pRtmCxt, pRegnId, u1BitMask,
                                  SendToApplication);
        RTM_PROT_UNLOCK ();
    }
    return i4RetVal;
}

/*-------------------------------------------------------------------+
  *
  * Function           : RtmProcessRouteForIfStatChange
  *
  * Input(s)           : u2Index -  Interface Index
  *                      i1Status - State of the Interface
  *
  * Output(s)          : None
  *
  * Returns            : None
  *
  * Action :
  * Reflects the status of interfaces on the routes whose gateway can be
  * reached on those interface. (This is applicable to the local iface
  * route for that interface and the static routes configured via that 
  * interface.)
 +-------------------------------------------------------------------*/
VOID
RtmProcessRouteForIfStatChange (UINT2 u2Index, INT1 i1Status)
{

    RTM_PROT_LOCK ();
    RtmUtilProcessRtForIfStatChange (u2Index, i1Status);
    RTM_PROT_UNLOCK ();
    return;
}

/*-------------------------------------------------------------------+
  *
  * Function           : RtmUtilProcessRtForIfStatChange
  *
  * Input(s)           : u2Index -  Interface Index
  *                      i1Status - State of the Interface
  *
  * Output(s)          : None
  *
  * Returns            : None
  *
  * Action :
  * Reflects the status of interfaces on the routes whose gateway can be
  * reached on those interface. (This is applicable to the local iface
  * route for that interface and the static routes configured via that 
  * interface.)
 +-------------------------------------------------------------------*/
VOID
RtmUtilProcessRtForIfStatChange (UINT2 u2Index, INT1 i1Status)
{
    UINT4               u4IpAddr = 0;
    UINT4               u4NetMask = 0;
    tNetIpv4IfInfo      IpIntfInfo;
    tRtInfo             OutRtInfo;
    tNetIpv4RtInfo      NetRtInfo;
    tRtInfo            *pRt = &OutRtInfo;
    tRtmCxt            *pRtmCxt = NULL;
    UINT4               u4ContextId = 0;
    INT1                i1RetVal = IP_FAILURE;
    tOutputParams       OutParams;
    tRtmRouteUpdateInfo RtmRouteUpdateInfo;
    tOsixMsg           *pRtmMsg = NULL;
    tRtmMsgHdr         *pRtmMsgHdr = NULL;
#ifdef IP_WANTED
    if ((gRtmGlobalInfo.u1VrfRouteLeakStatus == RTM_VRF_ROUTE_LEAK_ENABLED)
        && (i1Status == IPIF_OPER_DISABLE))
    {
        IPvxDeleteIpv4LeakedStaticRouteInCxt ((INT1) CIDR_STATIC_ID,
                                              VCM_INVALID_VC, u2Index);
    }
#endif
    if (NetIpv4GetCxtId (u2Index, &u4ContextId) == NETIPV4_FAILURE)
    {
        return;
    }

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return;
    }
    MEMSET (&gInRouteInfo, 0, sizeof (tRtInfo));
    MEMSET (&OutRtInfo, 0, sizeof (tRtInfo));

    MEMSET (&OutParams, 0, sizeof (tOutputParams));

    if (i1Status == IPIF_OPER_ENABLE)
    {
        /* Routes on which we need to act for OPER UP is
         * maintained in the temporary route list */
        RtmHandleOperUpIndicationInCxt (pRtmCxt, u2Index);
    }
    else
    {
        if (IpForwardingTableGetRouteInfoInCxt (pRtmCxt->u4ContextId,
                                                gInRouteInfo.u4DestNet,
                                                gInRouteInfo.u4DestMask,
                                                CIDR_STATIC_ID,
                                                &pRt) == IP_SUCCESS)
        {
            RtmChangeRtStatusInCxt (pRtmCxt, pRt, u2Index, i1Status);
        }
        /* Handle static route updates for interface state change */

        i1RetVal = IpGetNextRouteEntryInCxt (pRtmCxt, gInRouteInfo,
                                             CIDR_STATIC_ID, &OutParams);

        while (i1RetVal == IP_SUCCESS)
        {
            pRt = (tRtInfo *) OutParams.pAppSpecInfo;
            if (pRt == NULL)
            {
                break;
            }
            gInRouteInfo.u4DestNet = pRt->u4DestNet;
            gInRouteInfo.u4DestMask = pRt->u4DestMask;
            MEMSET (&OutParams, 0, sizeof (tOutputParams));

            i1RetVal = IpGetNextRouteEntryInCxt (pRtmCxt, gInRouteInfo,
                                                 CIDR_STATIC_ID, &OutParams);

            RtmChangeRtStatusInCxt (pRtmCxt, pRt, u2Index, i1Status);
        }
    }

    /* Act on Local routes for interface state change */
    if (NetIpv4GetIfInfo (u2Index, &IpIntfInfo) == NETIPV4_FAILURE)
    {
        return;
    }
    u4IpAddr = IpIntfInfo.u4Addr;
    u4NetMask = IpIntfInfo.u4NetMask;

    do
    {
        gInRouteInfo.u4DestNet = u4IpAddr & u4NetMask;
        gInRouteInfo.u4DestMask = u4NetMask;

        pRt = &OutRtInfo;
        if (IpForwardingTableGetRouteInfoInCxt (pRtmCxt->u4ContextId,
                                                gInRouteInfo.u4DestNet,
                                                gInRouteInfo.u4DestMask,
                                                CIDR_LOCAL_ID,
                                                &pRt) == IP_SUCCESS)
        {
            MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
            NetRtInfo.u4DestNet = pRt->u4DestNet;
            NetRtInfo.u4DestMask = pRt->u4DestMask;
            NetRtInfo.u4Tos = pRt->u4Tos;
            NetRtInfo.u4NextHop = pRt->u4NextHop;
            NetRtInfo.u4RtIfIndx = (UINT4) u2Index;
            NetRtInfo.u4RtNxtHopAs = pRt->u4RtNxtHopAS;
            NetRtInfo.u4RtAge = pRt->u4RtAge;
            NetRtInfo.u4RouteTag = pRt->u4RouteTag;
            NetRtInfo.i4Metric1 = pRt->i4Metric1;
            NetRtInfo.u2RtProto = pRt->u2RtProto;
            NetRtInfo.u2RtType = pRt->u2RtType;
            NetRtInfo.u4ContextId = pRtmCxt->u4ContextId;

            if (i1Status == IPIF_OPER_ENABLE)
            {
                /* Interface is coming UP now */
                /* make those routes which are in NOT_READY state to ACTIVE */
                if (pRt->u4RowStatus == IPFWD_NOT_READY)
                {
                    NetRtInfo.u4RowStatus = (UINT4) IPFWD_ACTIVE;
                    SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit,
                                               IP_BIT_STATUS);

                    pRt->u4RowStatus = NetRtInfo.u4RowStatus;

                    if ((pRtmMsg =
                         CRU_BUF_Allocate_MsgBufChain (sizeof (tRtInfo),
                                                       0)) == NULL)
                    {
                        return;
                    }

                    pRtmMsgHdr =
                        (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pRtmMsg);

                    pRtmMsgHdr->u1MessageType = RTM_INTERFACE_UP_REDISTRIBUTE;
                    pRtmMsgHdr->RegnId.u4ContextId = NetRtInfo.u4ContextId;

                    if (IP_COPY_TO_BUF (pRtmMsg, pRt,
                                        0, sizeof (tRtInfo)) == IP_BUF_FAILURE)
                    {

                        IP_RELEASE_BUF (pRtmMsg, FALSE);
                        return;
                    }

                    /* Send the buffer to RTM */
                    if (RpsEnqueuePktToRtm (pRtmMsg) == RTM_FAILURE)
                    {
                        IP_RELEASE_BUF (pRtmMsg, FALSE);
                        return;
                    }
                }
                else
                {
                    NetRtInfo.u4RowStatus = pRt->u4RowStatus;
                }
            }
            else
            {
                /* Interface is going down now */
                /* Make the ACTIVE routes to NOT_READY state */
                if (pRt->u4RowStatus == IPFWD_ACTIVE)
                {
                    ROUTE_TBL_UNLOCK ();
                    NetRtInfo.u4RowStatus = (UINT4) IPFWD_NOT_READY;
                    SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit,
                                               IP_BIT_STATUS);
                }
                else
                {
                    NetRtInfo.u4RowStatus = pRt->u4RowStatus;
                }
            }

            if (NetRtInfo.u2ChgBit != 0)
            {
                /* Some change in the route parameter. */
                RtmUtilIpv4LeakRoute (NETIPV4_MODIFY_ROUTE, &NetRtInfo);
            }
        }
        /* When interface creation message is not yet processed by RTM, 
         * RTM leak route to make the route as best will fail. Hence,
         * post a message to RTM to change the route as best route after
         * processing the route addition message. 
         * In case of oper down indication, when route is not present in 
         * RTM Trie do nothing */
        else if (i1Status == IPIF_OPER_ENABLE)
        {
            MEMSET (&RtmRouteUpdateInfo, 0, sizeof (tRtmRouteUpdateInfo));
            RtmRouteUpdateInfo.u4ContextId = u4ContextId;
            RtmRouteUpdateInfo.u4NewIpAddr = gInRouteInfo.u4DestNet;
            RtmRouteUpdateInfo.u4NewNetMask = gInRouteInfo.u4DestMask;
            RtmRouteUpdateInfo.u4IfIndex = (UINT4) u2Index;
            RtmIntfNotifyRoute (RTM_INTF_ROUTE_ADD_MSG, &RtmRouteUpdateInfo);
        }
    }
    while (NetIpv4GetNextSecondaryAddress (u2Index, u4IpAddr, &u4IpAddr,
                                           &u4NetMask) == NETIPV4_SUCCESS);
    return;
}

/************************************************************************/
/* Function           : RtmUtilActOnStaticRtForIfDelete                 */
/* Input(s)           : u4IfIndex                                       */
/*                    : u4ContextId - RTM Context Id;                   */
/* Output(s)          : None                                            */
/* Returns            : IP_SUCCESS, IP_FAILURE                          */
/* Action             : Scans the Routing Table, and Deletes all the    */
/*                      Routes reachable through u4IfIndex.(port)       */
/************************************************************************/
INT4
RtmUtilActOnStaticRtForIfDelete (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tRtInfo             InRtInfo;
    tOutputParams       OutParams;
    tRtInfo             OutRtInfo;
    tRtInfo            *pRt = &OutRtInfo;
    INT4                i4RetVal = IP_FAILURE;
    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return IP_FAILURE;
    }
    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
    MEMSET (&OutRtInfo, 0, sizeof (tRtInfo));
    MEMSET (&OutParams, 0, sizeof (tOutputParams));

    /* Act on Default route for interface deletion */
    if (IpForwardingTableGetRouteInfoInCxt (pRtmCxt->u4ContextId,
                                            InRtInfo.u4DestNet,
                                            InRtInfo.u4DestMask,
                                            CIDR_STATIC_ID, &pRt) == IP_SUCCESS)
    {
        RtmChangeRtStatusInCxt (pRtmCxt, pRt, (UINT2) u4IfIndex,
                                IPIF_OPER_INVALID);
    }

    i4RetVal = IpGetNextRouteEntryInCxt (pRtmCxt, InRtInfo,
                                         CIDR_STATIC_ID, &OutParams);
    while (i4RetVal == IP_SUCCESS)
    {
        pRt = (tRtInfo *) OutParams.pAppSpecInfo;
        if (pRt == NULL)
        {
            break;
        }
        InRtInfo.u4DestNet = pRt->u4DestNet;
        InRtInfo.u4DestMask = pRt->u4DestMask;
        MEMSET (&OutParams, 0, sizeof (tOutputParams));
        i4RetVal = IpGetNextRouteEntryInCxt (pRtmCxt, InRtInfo,
                                             CIDR_STATIC_ID, &OutParams);
        RtmChangeRtStatusInCxt (pRtmCxt, pRt, (UINT2) u4IfIndex,
                                IPIF_OPER_INVALID);
    }

    return IP_SUCCESS;
}

/************************************************************************/
/* Function           : RtmActOnStaticRoutesForIfDeleteInCxt            */
/* Input(s)           : u4IfIndex                                       */
/*                    : u4ContextId - RTM Context Id;                   */
/* Output(s)          : None                                            */
/* Returns            : IP_SUCCESS, IP_FAILURE                          */
/* Action             : Scans the Routing Table, and Deletes all the    */
/*                      Routes reachable through u4IfIndex.(port)       */
/************************************************************************/
INT4
RtmActOnStaticRoutesForIfDeleteInCxt (UINT4 u4ContextId, UINT4 u4IfIndex)
{

    RTM_PROT_LOCK ();
    RtmUtilActOnStaticRtForIfDelete (u4ContextId, u4IfIndex);
    RTM_PROT_UNLOCK ();
    return IP_SUCCESS;

}

/*************************************************************************/
/* Function Name     :  IpCidrTblSetObjectInCxt                          */
/*                                                                       */
/* Description       :  This fuction does a SET Object in  the  IP Cidr  */
/*                      forwarding table based on the Object type        */
/*                      specified.                                       */
/*                      RTM protocol should be taken before calling this */
/*                      Function                                         */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  u4ContextId - RTM context ID                     */
/*                   :  u4Dest - The Destination address.                */
/*                      u4Mask - The Desination Mask                     */
/*                      i4Tos  - The Type of service                     */
/*                      u4NextHop - The Next hop gatewa address          */
/*                      i4Proto -   The routing protocol                 */
/*                      u1ObjType - The Object type being accessed       */
/*                                                                       */
/* Output(s)         :  *pi4Val - The Value of the object for which the  */
/*                                SET is performed                       */
/*                                                                       */
/* Returns           :  RTM_SUCCESS | RTM_FAILURE                        */
/*************************************************************************/
INT4
IpCidrTblSetObjectInCxt (UINT4 u4ContextId, UINT4 u4Dest, UINT4 u4Mask,
                         INT4 i4Tos, UINT4 u4NextHop, INT4 i4Proto,
                         INT4 *pi4Val, UINT1 u1ObjType)
{
    tNetIpv4RtInfo      NetRtInfo;
    tRtInfo            *paRtSpecInfo[MAX_ROUTING_PROTOCOLS + 1];
    tRtInfo            *pRt = NULL;
    UINT4               u4Port = 0;
    UINT1               u1FoundFlag = FALSE;
    UINT1               u1CmdType = 0;
    UINT1               u1UpdateNeeded = 0;
    INT4                i4RetVal = 0;
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo             RtSpecInfo;

    UNUSED_PARAM (i4Proto);

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return RTM_FAILURE;
    }

    i4RetVal = RtmHandleRtWithInvalidIfIndexInCxt (pRtmCxt, u4Dest, u4Mask,
                                                   (UINT4) i4Tos, u4NextHop,
                                                   *pi4Val, u1ObjType);

    if (i4RetVal == RTM_NO_ROOM)
    {
        return RTM_FAILURE;
    }

    if (i4RetVal == RTM_SUCCESS)
    {
        return RTM_SUCCESS;
    }
    paRtSpecInfo[STATIC_RT_INDEX] = &(RtSpecInfo);
    MEMSET (paRtSpecInfo[STATIC_RT_INDEX], 0, sizeof (RtSpecInfo));

    if (IpForwardingTableGetRouteInfoInCxt
        (pRtmCxt->u4ContextId, u4Dest, u4Mask, CIDR_STATIC_ID,
         &(paRtSpecInfo[STATIC_RT_INDEX])) == IP_SUCCESS)
    {
        pRt = paRtSpecInfo[STATIC_RT_INDEX];
        if (pRt != NULL)
        {
            while ((pRt != NULL) && (u1FoundFlag == FALSE))
            {
                /* Makefile Changes - comparison btwn signed and unsigned */
                if ((pRt->u4NextHop == u4NextHop) &&
                    (pRt->u4Tos == (UINT4) i4Tos))
                {
                    u1FoundFlag = TRUE;
                }
                else
                {
                    pRt = pRt->pNextAlternatepath;
                }
            }
        }
    }
    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
    NetRtInfo.u4DestNet = u4Dest;
    NetRtInfo.u4DestMask = u4Mask;
    NetRtInfo.u4ContextId = u4ContextId;
    NetRtInfo.u4Tos = (UINT4) i4Tos;
    NetRtInfo.u4NextHop = u4NextHop;
    NetRtInfo.u2RtProto = CIDR_STATIC_ID;

    if ((pRt != NULL) && (u1FoundFlag == TRUE))
    {
        /* Route is already present. */
        NetRtInfo.u4RtIfIndx = pRt->u4RtIfIndx;
        NetRtInfo.u2RtType = pRt->u2RtType;
        NetRtInfo.u4RtAge = pRt->u4RtAge;
        NetRtInfo.u4RtNxtHopAs = pRt->u4RtNxtHopAS;
        NetRtInfo.i4Metric1 = pRt->i4Metric1;
        NetRtInfo.u1Preference = pRt->u1Preference;
        NetRtInfo.u4RowStatus = pRt->u4RowStatus;
        NetRtInfo.u4RouteTag = pRt->u4RouteTag;
        NetRtInfo.u1BitMask = pRt->u1BitMask;
    }
    else
    {
        NetRtInfo.u4RowStatus = IPFWD_ROW_DOES_NOT_EXIST;
        /* Row doesnt exist .So parameters cant be set now */
        if (u1ObjType != IPFWD_ROUTE_ROW_STATUS)
        {
            return RTM_FAILURE;
        }
    }

    /* Update the Route with new value */
    switch (u1ObjType)
    {
        case IPFWD_ROUTE_IFINDEX:

            /* Get the Port no from IfIndex */
            if (NetIpv4GetPortFromIfIndex ((UINT4) *pi4Val, &u4Port)
                != NETIPV4_SUCCESS)
            {
                return RTM_SUCCESS;
            }
            NetRtInfo.u4RtIfIndx = u4Port;
            SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_INTRFAC);
            break;

        case IPFWD_ROUTE_TYPE:
            if (NetRtInfo.u2RtType == (UINT2) *pi4Val)
            {
                return RTM_SUCCESS;
            }
            NetRtInfo.u2RtType = (UINT2) *pi4Val;
            SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_RT_TYPE);
            break;

        case IPFWD_ROUTE_NEXTHOPAS:
            if (NetRtInfo.u4RtNxtHopAs == (UINT2) *pi4Val)
            {
                return RTM_SUCCESS;
            }
            NetRtInfo.u4RtNxtHopAs = (UINT4) *pi4Val;
            SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_NH_AS);
            break;

        case IPFWD_ROUTE_PREFERENCE:
            /*  IPFWD_ROUTE_PREFERENCE---> Common Routing Table
             */
            if (NetRtInfo.u1Preference == (UINT2) *pi4Val)
            {
                return RTM_SUCCESS;
            }
            NetRtInfo.u1Preference = (UINT1) *pi4Val;
            break;

        case IPFWD_ROUTE_METRIC1:
            /*  IPFWD_ROUTE_METRIC1 ---> Standard CIDR Table
             */
            if (NetRtInfo.i4Metric1 == (UINT2) *pi4Val)
            {
                return RTM_SUCCESS;
            }
            NetRtInfo.i4Metric1 = *pi4Val;
            SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_METRIC);
            break;

        case IPFWD_ROUTE_ROW_STATUS:
            /* Entry doent exist.Return Success */
            if (*pi4Val == IPFWD_DESTROY)
            {
                if (NetRtInfo.u4RowStatus == IPFWD_ROW_DOES_NOT_EXIST)
                {
                    return RTM_FAILURE;
                }
            }

            if (NetRtInfo.u4RowStatus == (UINT2) *pi4Val)
            {
                return RTM_SUCCESS;
            }

            /* Update the RowStatus Value. */
            if (IpTblRowStatusAction (&NetRtInfo, pi4Val,
                                      &u1UpdateNeeded) == SNMP_FAILURE)
            {
                return RTM_FAILURE;
            }
            SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_STATUS);
            break;

        case IPFWD_REDIS_CTRL_FLAG:
            /* Setting redistribution control flag */
            if (*pi4Val == IP_DONT_REDIS_ROUTE)
            {
                if ((NetRtInfo.u1BitMask &
                     RTM_PRIV_RT_MASK) == RTM_PRIV_RT_MASK)
                {
                    return RTM_SUCCESS;
                }
                NetRtInfo.u1BitMask |= RTM_PRIV_RT_MASK;
            }
            else
            {
                if ((NetRtInfo.u1BitMask & RTM_PRIV_RT_MASK) == 0)
                {
                    return RTM_SUCCESS;
                }
                NetRtInfo.u1BitMask =
                    (UINT1) (NetRtInfo.u1BitMask & (~RTM_PRIV_RT_MASK));
            }
            SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_REDISCTRL);
            break;

        default:
            return RTM_FAILURE;
    }

    /* Update the IP Fwd Table */

    /* If the administrative distance is 255, the router does not believe
       the source of that route and does not install the route in the
       routing table.So,for UNKNOWN AD(255) send delete action to RTM */

    if ((NetRtInfo.u4RowStatus == IPFWD_DESTROY) ||
        (NetRtInfo.i4Metric1 == IP_UNKNOWN_AD))
    {
        u1CmdType = NETIPV4_DELETE_ROUTE;
    }
    else if (u1FoundFlag == TRUE)
    {
        u1CmdType = NETIPV4_MODIFY_ROUTE;
    }
    else
    {
        u1CmdType = NETIPV4_ADD_ROUTE;
    }

    if (RtmUtilIpv4LeakRoute (u1CmdType, &NetRtInfo) == NETIPV4_FAILURE)
    {
        return RTM_FAILURE;
    }

    return RTM_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  IpCidrTblTestObjectInCxt                         */
/*                                                                       */
/* Description       :  This fuction does a TEST Object in  the  IP Cidr */
/*                      forwarding table based on the Object type        */
/*                      specified.                                       */
/*                      RTM protocol should be taken before calling this */
/*                      Function                                         */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  u4ContextId - RTM context ID                     */
/*                   :  u4Dest - The Destination address.                */
/*                      u4Mask - The Desination Mask                     */
/*                      i4Tos  - The Type of service                     */
/*                      u4NextHop - The Next hop gatewa address          */
/*                      i4Proto -   The routing protocol                 */
/*                      u1ObjType - The Object type being accessed       */
/*                                                                       */
/* Output(s)         :  *pi4Val - The Value of the object for which the  */
/*                                TEST is performed                      */
/*                                                                       */
/* Returns           :  RTM_SUCCESS | RTM_FAILURE                        */
/*************************************************************************/
INT4
IpCidrTblTestObjectInCxt (UINT4 u4ContextId, UINT4 u4Dest, UINT4 u4Mask,
                          INT4 i4Tos, UINT4 u4NextHop, INT4 i4Proto,
                          INT4 *pi4Val, UINT1 u1ObjType, UINT4 *pu4ErrorCode)
{
    UINT1               u1FoundFlag = FALSE;
    tRtInfo            *pRt = NULL;
    tRtInfo            *paRtSpecInfo[MAX_ROUTING_PROTOCOLS + 1];
    INT4                i4RetVal = RTM_FAILURE;
    INT4                i4StaticRouteCount = 0;
    UINT4               u4Port = 0;
    UINT4               u4IfCxtId = 0;
    UINT4               u4StaticRtCount = 0;
    tRtInfo             ExstRt;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1StackIfName[CFA_MAX_PORT_NAME_LENGTH];
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo             RtSpecInfo;
    INT4                i4Value = 0;
    INT4                i4RouteLeakStatus = 0;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return RTM_FAILURE;
    }
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1StackIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if ((i4Proto != INVALID_PROTO) && (i4Proto != CIDR_STATIC_ID))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return RTM_FAILURE;
    }

    /* Invalid Mask and network combination */
    if (((u4Dest & u4Mask) != u4Dest))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP_INVALID_IP_MASK_ERR);
        return RTM_FAILURE;
    }

    /* NextHop Ip addr configured for a static route 
     * should not be our local interface ip addr */
    if ((u4NextHop != 0) &&
        (NetIpv4IfIsOurAddressInCxt (u4ContextId,
                                     u4NextHop) == NETIPV4_SUCCESS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_IP_INVALID_NEXTHOP_ERR);
        return RTM_FAILURE;
    }

    /* Dest Ip network configured for a static route 
     * should not be same as connected routes */
    i4RetVal = IpTableGetObjectInCxt (u4ContextId, u4Dest, u4Mask,
                                      i4Tos, u4NextHop, i4Proto,
                                      &i4Value, IPFWD_ROUTE_ROW_STATUS);
    if (IP_SUCCESS == i4RetVal)
    {
        if (IP_SUCCESS == IpTableGetObjectInCxt (u4ContextId, u4Dest,
                                                 u4Mask, i4Tos,
                                                 u4NextHop, i4Proto,
                                                 &i4Value, IPFWD_ROUTE_PROTO))
        {
            if (CIDR_LOCAL_ID == i4Value)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_IP_CONNECTED_ROUTE_EXISTS);
                return RTM_FAILURE;
            }
        }

    }

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        SPRINTF ((CHR1 *) au1StackIfName, "%s%u",
                 CFA_STACK_VLAN_INTERFACE, CFA_DEFAULT_STACK_VLAN_ID);
        CfaCliGetIfName ((UINT4) *pi4Val, (INT1 *) au1IfName);

        if (STRCMP (au1IfName, au1StackIfName) == 0)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
            return RTM_FAILURE;
        }
        if (IS_LINK_LOCAL_ADDR (u4NextHop))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_IP_INVALID_NEXTHOP_ERR);
            return RTM_FAILURE;
        }

    }
    if (i4Tos != 0)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return RTM_FAILURE;
    }

    switch (u1ObjType)
    {

        case IPFWD_ROUTE_IFINDEX:

            /* Check for the presence of the Interface */
            if (NetIpv4GetPortFromIfIndex ((UINT4) *pi4Val, &u4Port)
                == NETIPV4_SUCCESS)
            {
                if (nmhGetFsMIRtmRouteLeakStatus (&i4RouteLeakStatus) ==
                    SNMP_FAILURE)
                {
                    return RTM_FAILURE;
                }
                /* If VRF route leaking is Disabled ,Validate the context id */
                if (i4RouteLeakStatus != RTM_VRF_ROUTE_LEAK_ENABLED)
                {
                    if ((NetIpv4GetCxtId (u4Port, &u4IfCxtId) ==
                         NETIPV4_SUCCESS) && (u4IfCxtId == u4ContextId))
                    {
                        i4RetVal = RTM_SUCCESS;
                    }
                    else
                    {
                        /* specifiec interface is not present in the given context */
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
                        return RTM_FAILURE;
                    }
                }
                /*VRF Route Leaking is Enabled */

                /*Static route addtion  in Non-Default VRF with next hop
                 *as another non-Default VRF interface is blocked */
                else if ((NetIpv4GetCxtId (u4Port, &u4IfCxtId) ==
                          NETIPV4_SUCCESS) && ((u4IfCxtId > 0)
                                               && (u4ContextId > 0)))
                {
                    /* specific interface is not present in the given context */
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
                    return RTM_FAILURE;
                }
                /* VRF Route Leaking is enabled .Donot validate the context id */
                /* Static route additon in Default VRF with next hop as non-Default
                 * VRF and vice-versa is allowed */
                else
                {
                    i4RetVal = RTM_SUCCESS;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_IP_INVALID_INTF_ERR);
                return RTM_FAILURE;
            }
            break;

        case IPFWD_ROUTE_TYPE:
            if (*pi4Val > 0 && *pi4Val < 5)
            {
                i4RetVal = RTM_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return RTM_FAILURE;
            }
            break;

        case IPFWD_ROUTE_PROTO:
            /* Only Static routes can be set through these low level routines */
            *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
            i4RetVal = RTM_FAILURE;
            break;

        case IPFWD_ROUTE_METRIC1:
            if (((*pi4Val >= 1) && (*pi4Val <= IP_PREFERENCE_INFINITY))
                || (*pi4Val == IPFWD_ROUTE_METRIC_DEFAULT))
            {
                i4RetVal = RTM_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return RTM_FAILURE;
            }
            break;

        case IPFWD_ROUTE_METRIC2:
        case IPFWD_ROUTE_METRIC3:
        case IPFWD_ROUTE_METRIC4:
        case IPFWD_ROUTE_METRIC5:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return RTM_FAILURE;

        case IPFWD_ROUTE_PREFERENCE:
            if ((*pi4Val >= 1) && (*pi4Val <= IP_PREFERENCE_INFINITY))
            {
                i4RetVal = RTM_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return RTM_FAILURE;
            }
            break;

        case IPFWD_REDIS_CTRL_FLAG:
            if ((*pi4Val == IP_REDIS_ROUTE) || (*pi4Val == IP_DONT_REDIS_ROUTE))
            {
                i4RetVal = RTM_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return RTM_FAILURE;
            }
            break;

        case IPFWD_ROUTE_ROW_STATUS:
            if ((*pi4Val != IPFWD_NOT_READY) &&
                ((*pi4Val >= IPFWD_ACTIVE) && (*pi4Val <= IPFWD_DESTROY)))
            {
                i4RetVal = RTM_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return RTM_FAILURE;
            }
            break;

        case IPFWD_ROUTE_NEXTHOPAS:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (RTM_FAILURE);

        case IPFWD_ROUTE_INFO:
            /* Test for OID done separately in calling function */
            return (RTM_SUCCESS);

        default:
            *pu4ErrorCode = SNMP_ERR_GEN_ERR;
            return RTM_FAILURE;
    }

    paRtSpecInfo[STATIC_RT_INDEX] = &(RtSpecInfo);

    MEMSET (paRtSpecInfo[STATIC_RT_INDEX], 0, sizeof (RtSpecInfo));

    /* Check whether route exists in Temporary route list.If exists,get the
     * route entry .Else search in the RTM Trie*/
    if (RtmUtilCheckRtExistsWithInvalidIfIndex (pRtmCxt, u4Dest, u4Mask,
                                                (UINT4) i4Tos, u4NextHop,
                                                &ExstRt) == TRUE)
    {
        u1FoundFlag = TRUE;
    }
    else
    {
        if (IpForwardingTableGetRouteInfoInCxt (pRtmCxt->u4ContextId, u4Dest,
                                                u4Mask, CIDR_STATIC_ID,
                                                &(paRtSpecInfo
                                                  [STATIC_RT_INDEX])) ==
            IP_SUCCESS)
        {
            pRt = paRtSpecInfo[STATIC_RT_INDEX];
            if (pRt != NULL)
            {
                while ((pRt != NULL) && (u1FoundFlag == FALSE))
                {
                    if ((pRt->u4NextHop == u4NextHop) &&
                        /* Makefile Changes - comparison btwn signed and unsigned */
                        (pRt->u4Tos == (UINT4) i4Tos))
                    {
                        u1FoundFlag = TRUE;
                    }
                    else
                    {
                        pRt = pRt->pNextAlternatepath;
                    }
                }
            }
        }
        if (u1FoundFlag == TRUE)
        {
            MEMCPY (&ExstRt, pRt, sizeof (tRtInfo));
        }
    }

    if (u1ObjType != IPFWD_ROUTE_ROW_STATUS)
    {

        if (u1FoundFlag == TRUE)
        {
            switch (ExstRt.u4RowStatus)
            {
                case IPFWD_ROW_DOES_NOT_EXIST:
                case IPFWD_NOT_IN_SERVICE:
                case IPFWD_NOT_READY:
                case IPFWD_CREATE_AND_WAIT:
                    i4RetVal = RTM_SUCCESS;
                    break;

                    /* For a route, which is ACTIVE, We are allowing to change the
                     * preference/Metric to allow static route promotion on the fly. 
                     * Other things should not be changed.
                     */

                case IPFWD_ACTIVE:
                    if ((u1ObjType == IPFWD_ROUTE_METRIC1) ||
                        (u1ObjType == IPFWD_ROUTE_PREFERENCE) ||
                        (u1ObjType == IPFWD_REDIS_CTRL_FLAG))
                    {
                        i4RetVal = RTM_SUCCESS;
                    }
                    else
                    {
                        *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
                        i4RetVal = RTM_FAILURE;
                    }
                    break;

                case IPFWD_DESTROY:
                    *pu4ErrorCode = SNMP_ERR_NO_ACCESS;
                    i4RetVal = RTM_FAILURE;
                    break;

                default:
                    *pu4ErrorCode = SNMP_ERR_GEN_ERR;
                    i4RetVal = RTM_FAILURE;
                    break;
            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            i4RetVal = RTM_FAILURE;
        }
    }
    else
    {
        /* the object is row status */
        if (u1FoundFlag == FALSE)
        {
            /* Route does not exist. */
            if ((*pi4Val == IPFWD_ACTIVE) ||
                (*pi4Val == IPFWD_NOT_IN_SERVICE) ||
                (*pi4Val == IPFWD_NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4RetVal = RTM_FAILURE;
            }
            else
            {
                RtmGetStaticRouteCount (&u4StaticRtCount);
                /* Static route count will be calculated at run time (system.size if 
                 * present) and if route calculated value is a negative value then 
                 * static route are not allowed to create.*/
                i4StaticRouteCount = (INT4) gRtmGlobalInfo.u4MaxRTMStaticRoute;
                i4StaticRouteCount =
                    (i4StaticRouteCount > 0) ? i4StaticRouteCount : 0;
                if (u4StaticRtCount == (UINT4) i4StaticRouteCount)
                {
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    CLI_SET_ERR (CLI_IP_MAX_STATIC_RT_ERR);
                    i4RetVal = RTM_FAILURE;
                }
            }
        }
        else
        {
            /* Route exist. */
            if ((*pi4Val == IPFWD_CREATE_AND_GO) ||
                (*pi4Val == IPFWD_CREATE_AND_WAIT))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i4RetVal = RTM_FAILURE;
            }
            else if (*pi4Val == IPFWD_ACTIVE)
            {
                if ((ExstRt.u4NextHop == 0)
                    && (ExstRt.u4RtIfIndx == IPIF_INVALID_INDEX))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (CLI_IP_INVALID_NEXTHOP_ERR);
                    i4RetVal = RTM_FAILURE;
                }
            }
        }
    }
    return i4RetVal;
}

/*************************************************************************
 * Function Name  : RtmAddAllRtEntriesInCxt                               *
 * Description    : Add all Route Entries to Forwarding Plane             *
 * Input          : u4ContextId -RTM context ID                           *
 * Output         : None                                                  *
 * Returns        : None                                                  *
 **************************************************************************/
VOID
RtmAddAllRtEntriesInCxt (UINT4 u4ContextId)
{
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];    /* Address and Mask form the Key for
                                               Trie */
    UINT2               u2NoPgmRt = 0;
    tRtmCxt            *pRtmCxt = NULL;
    UINT1               au1Key[2 * sizeof (UINT4)];

    RTM_PROT_LOCK ();

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return;
    }

    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = au1Key;
    MEMSET (OutParams.Key.pKey, 0, 2 * sizeof (UINT4));

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;

    /* scan the route table and add all Routes to NP */
    while (TrieGetNextEntry (&(InParams), &(OutParams)) == TRIE_SUCCESS)
    {
        if (IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRt,
                                       RTM_GET_BST_WITH_PARAMS) == IP_SUCCESS)
        {
            if (pRt->u2RtProto != CIDR_LOCAL_ID)
            {
                pTmpRt = pRt;
                while ((pTmpRt != NULL) &&
                       (pTmpRt->i4Metric1 == pRt->i4Metric1))
                {
                    if ((pTmpRt->u4Flag & RTM_NOTIFIED_RT)
                        || (pTmpRt->u4Flag & RTM_RT_REACHABLE))
                    {
                        RtmAddRouteToDataPlaneInCxt (pRtmCxt,
                                                     pTmpRt,
                                                     (UINT1) (++u2NoPgmRt));
                    }
                    pTmpRt = pTmpRt->pNextAlternatepath;
                }
            }
        }
        u2NoPgmRt = 0;

        InParams.i1AppId = ALL_ROUTING_PROTOCOL;
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;

        /* Update the Key to Get the Next Entry */
        InParams.Key.pKey = OutParams.Key.pKey;
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
        OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    }
    RTM_PROT_UNLOCK ();
    return;
}

/*************************************************************************
 * Function Name  : RtmDelAllRtEntriesInCxt                               *
 * Description    : Delete all route entries from forwarding plane.       *
 * Input          : u4ContextId -RTM context ID                           *
 * Output         : None                                                  *
 * Returns        : None                                                  *
 **************************************************************************/
VOID
RtmDelAllRtEntriesInCxt (UINT4 u4ContextId)
{
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pRt = NULL;
    UINT4               u4CfaIfIndex = 0;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];    /* Address and Mask form the Key for
                                               Trie */
    UINT1               u1RtCount = 0;
    tRtmCxt            *pRtmCxt = NULL;
    UINT1               au1Key[2 * sizeof (UINT4)];

    RTM_PROT_LOCK ();

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return;
    }

    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));

    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = au1Key;
    MEMSET (OutParams.Key.pKey, 0, 2 * sizeof (UINT4));

    if (OutParams.Key.pKey == NULL)
    {
        RTM_PROT_UNLOCK ();
        return;
    }

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;

    /* scan the route table and delete all Routes from NP */
    while (TrieGetNextEntry (&(InParams), &(OutParams)) == TRIE_SUCCESS)
    {
        if (IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRt,
                                       RTM_GET_BST_WITH_PARAMS) == IP_SUCCESS)
        {
            /* u4CfaIfIndex is a unused variable in the
             * function RtmDeleteRtFromFIB.So we are not filling
             * with any proper  value */
            if (pRt->u2RtProto != CIDR_LOCAL_ID)
            {
                pTmpRt = pRt;
                RtmGetInstalledRtCnt (pRt, &u1RtCount);
                while ((pTmpRt != NULL) &&
                       (pTmpRt->i4Metric1 == pRt->i4Metric1))
                {
                    if ((pTmpRt->u4Flag & RTM_NOTIFIED_RT) &&
                        (pTmpRt->u4Flag & (UINT4) (~RTM_RT_REACHABLE)))
                    {
                        /* Only route installed as local route */
                        RtmDeleteRtFromFIBInCxt (pRtmCxt, pTmpRt,
                                                 u4CfaIfIndex, ++u1RtCount);
                    }
                    else
                    {
                        if (pTmpRt->u4Flag & RTM_RT_REACHABLE)
                        {
                            /* Route installed in dataplane. So delete 
                             * the route from dataplane*/
                            RtmDeleteRtFromFIBInCxt (pRtmCxt, pTmpRt,
                                                     u4CfaIfIndex, u1RtCount--);
                        }
                    }
                    pTmpRt = pTmpRt->pNextAlternatepath;
                }
            }
        }

        InParams.i1AppId = ALL_ROUTING_PROTOCOL;
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;

        /* Update the Key to Get the Next Entry */
        InParams.Key.pKey = OutParams.Key.pKey;
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
        OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    }

    RTM_PROT_UNLOCK ();
    return;
}

/************************************************************************
 *  Function Name   : RtmGetAndProgrammeBestRoute
 *  Description     : Function to Get the best route and programme in H/W
 *  Input           : pRtQuery 
 *  Output          : None
 *  Returns         : IP_SUCCESS or IP_FAILURE
 ************************************************************************/
INT4
RtmGetAndProgrammeBestRoute (tRtInfoQueryMsg * pRtQuery)
{
    tRtInfo             Rt;
    tRtInfo            *pBestRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    UINT1               u1BestRtCount = 0;
    INT4                i4RetVal = IP_SUCCESS;

    tRtmCxt            *pRtmCxt = NULL;

    RTM_PROT_LOCK ();

    pRtmCxt = UtilRtmGetCxt (pRtQuery->u4ContextId);

    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        i4RetVal = IP_FAILURE;
        return i4RetVal;
    }

    MEMSET (&Rt, 0, (sizeof (tRtInfo)));

    Rt.u4DestNet = pRtQuery->u4DestinationIpAddress;
    Rt.u4DestMask = pRtQuery->u4DestinationSubnetMask;

    if (IpGetBestRouteEntryInCxt (pRtmCxt, Rt, &pBestRt) == IP_SUCCESS)
    {
        pTmpRt = pBestRt;
        while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pBestRt->i4Metric1))
        {
            if ((pTmpRt->u4Flag & RTM_NOTIFIED_RT)
                || (pTmpRt->u4Flag & RTM_RT_REACHABLE))
            {
                RtmAddRouteToDataPlaneInCxt (pRtmCxt, pTmpRt, ++u1BestRtCount);
            }
            pTmpRt = pTmpRt->pNextAlternatepath;
        }
    }
    else
    {
        i4RetVal = IP_FAILURE;
    }
    RTM_PROT_UNLOCK ();
    return i4RetVal;
}

/************************************************************************
 *  Function Name   : RtmApiCheckInvalidRtExists
 *  Description     : Functions scans through the invalid route list to 
 *                    find the given route.
 *                    This function is called from ipmpsnif.c where the
 *                    RTM Lock is taken from the wrapper routine of the
 *                    respective nmh function.
 *
 *  Input           : u4ContextId - RTM context Id
 *                    u4DestNet - Destination network for the route
 *                    u4DestMask - Destination Mask for the route
 *                    i4Tos      - TOS 
 *                    u4NextHop  - Next hop for the route
 *  Output          : None
 *  Returns         : If route is found IP_SUCCESS is returned 
 *                    else IP_FAILURE is returned
 ************************************************************************/
INT4
RtmApiCheckInvalidRtExists (UINT4 u4ContextId, UINT4 u4DestNet,
                            UINT4 u4DestMask, INT4 i4Tos, UINT4 u4NextHop)
{
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo            *pRt = NULL;

    if ((pRtmCxt = UtilRtmGetCxt (u4ContextId)) == NULL)
    {
        return RTM_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmInvdRtList), pRt, tRtInfo *)
    {
        if ((pRt->u4DestNet == u4DestNet) &&
            (pRt->u4DestMask == u4DestMask) &&
            (pRt->u4Tos == (UINT4) i4Tos) && (pRt->u4NextHop == u4NextHop))
        {
            break;
        }
    }

    if (pRt == NULL)
    {
        return RTM_FAILURE;
    }
    return RTM_SUCCESS;

}

/*
 ******************************************************************************
 * Function Name    :   RtmUtilGetNextInactiveRtEntry
 *
 * Description      :   This function returns the Next Route Entry in 
 *                      Rtm Invalid Route List
 *                      
 *
 * Inputs           :   pNetRtInfo - Route Entry for which the Next Entry is
 *                      required.
 *
 * Outputs          :   pNextNetRtInfo - Next Route Entry in the Rtm Invalid
 *                      Route list
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
RtmUtilGetNextInactiveRtEntry (tNetIpv4RtInfo * pNetRtInfo,
                               tNetIpv4RtInfo * pNextNetRtInfo)
{

    tRtInfo            *pNextRtInfo = NULL;
    tRtInfo            *pTmpRtInfo = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tNetIpv4RtInfo      OutRtInfo;
    UINT1               u1Found = (UINT1) IP_FAILURE;

    MEMSET (&OutRtInfo, 0, sizeof (tNetIpv4RtInfo));
    if ((pNetRtInfo == NULL) || (pNextNetRtInfo == NULL))
    {
        return (NETIPV4_FAILURE);
    }

    pRtmCxt = UtilRtmGetCxt (pNetRtInfo->u4ContextId);

    if (pRtmCxt == NULL)
    {
        return NETIPV4_FAILURE;
    }
    TMO_SLL_Scan (&(pRtmCxt->RtmInvdRtList), pTmpRtInfo, tRtInfo *)
    {
        if (pTmpRtInfo != NULL)
        {
            OutRtInfo.u4DestNet = pTmpRtInfo->u4DestNet;
            OutRtInfo.u4DestMask = pTmpRtInfo->u4DestMask;
            OutRtInfo.u4Tos = pTmpRtInfo->u4Tos;
            OutRtInfo.u4NextHop = pTmpRtInfo->u4NextHop;

            if (RtmUtilCheckIsRouteGreater (&OutRtInfo, pNetRtInfo) ==
                IP_SUCCESS)
            {
                pNextRtInfo = pTmpRtInfo;
                u1Found = IP_SUCCESS;
                break;
            }
        }
    }

    if (u1Found == IP_SUCCESS)
    {
        pNextNetRtInfo->u4DestNet = pNextRtInfo->u4DestNet;
        pNextNetRtInfo->u4DestMask = pNextRtInfo->u4DestMask;
        pNextNetRtInfo->u4Tos = pNextRtInfo->u4Tos;
        pNextNetRtInfo->u4NextHop = pNextRtInfo->u4NextHop;
        pNextNetRtInfo->u4RtIfIndx = pNextRtInfo->u4RtIfIndx;
        pNextNetRtInfo->u4RtAge = pNextRtInfo->u4RtAge;
        pNextNetRtInfo->u4RtNxtHopAs = pNextRtInfo->u4RtNxtHopAS;
        pNextNetRtInfo->i4Metric1 = pNextRtInfo->i4Metric1;
        pNextNetRtInfo->u4RowStatus = pNextRtInfo->u4RowStatus;
        pNextNetRtInfo->u2RtType = pNextRtInfo->u2RtType;
        pNextNetRtInfo->u2RtProto = pNextRtInfo->u2RtProto;
        pNextNetRtInfo->u4RouteTag = pNextRtInfo->u4RouteTag;

        return (NETIPV4_SUCCESS);
    }
    return (NETIPV4_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   RtmUtilGetNextFwdTableRtEntry
 *
 * Description      :   This function returns the Next Route Entry in the Ip
 *                      Forwarding Table for a given Route Entry.
 *                      This function is called from lowlevel routines.
 *                      Rtm Lock protection is not given for this API. 
 *                      Rtm Lock should have been taken before calling this function.
 *                      
 *                      
 *
 * Inputs           :   pNetRtInfo - Route Entry for which the Next Entry is
 *                      required.
 *
 * Outputs          :   pNextNetRtInfo - Next Route Entry in the Ip Forwarding
 *                      Table.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
RtmUtilGetNextFwdTableRtEntry (tNetIpv4RtInfo * pNetRtInfo,
                               tNetIpv4RtInfo * pNextNetRtInfo)
{

    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo             RtInfo;
    tRtInfo             NextRtInfo;
    INT4                i4RetVal = NETIPV4_FAILURE;

    if ((pNetRtInfo == NULL) || (pNextNetRtInfo == NULL))
    {
        return (NETIPV4_FAILURE);
    }

    pRtmCxt = UtilRtmGetCxt (pNetRtInfo->u4ContextId);
    MEMSET (&RtInfo, 0, sizeof (tRtInfo));
    MEMSET (&NextRtInfo, 0, sizeof (tRtInfo));

    RtInfo.u4DestNet = pNetRtInfo->u4DestNet;
    RtInfo.u4DestMask = pNetRtInfo->u4DestMask;
    RtInfo.u4Tos = pNetRtInfo->u4Tos;
    RtInfo.u4NextHop = pNetRtInfo->u4NextHop;
    RtInfo.u4RtIfIndx = pNetRtInfo->u4RtIfIndx;
    RtInfo.u4RtAge = pNetRtInfo->u4RtAge;
    RtInfo.u4RtNxtHopAS = pNetRtInfo->u4RtNxtHopAs;
    RtInfo.i4Metric1 = pNetRtInfo->i4Metric1;
    RtInfo.u4RowStatus = pNetRtInfo->u4RowStatus;
    RtInfo.u2RtType = pNetRtInfo->u2RtType;
    RtInfo.u2RtProto = pNetRtInfo->u2RtProto;
    RtInfo.u4RouteTag = pNetRtInfo->u4RouteTag;

    if (pRtmCxt == NULL)
    {
        return NETIPV4_FAILURE;
    }
    i4RetVal = RtmUtilGetNextAllRtInCxt (pRtmCxt, RtInfo, &NextRtInfo);
    if (i4RetVal == IP_SUCCESS)
    {
        pNextNetRtInfo->u4DestNet = NextRtInfo.u4DestNet;
        pNextNetRtInfo->u4DestMask = NextRtInfo.u4DestMask;
        pNextNetRtInfo->u4Tos = NextRtInfo.u4Tos;
        pNextNetRtInfo->u4NextHop = NextRtInfo.u4NextHop;
        pNextNetRtInfo->u4RtIfIndx = NextRtInfo.u4RtIfIndx;
        pNextNetRtInfo->u4RtAge = NextRtInfo.u4RtAge;
        pNextNetRtInfo->u4RtNxtHopAs = NextRtInfo.u4RtNxtHopAS;
        pNextNetRtInfo->i4Metric1 = NextRtInfo.i4Metric1;
        pNextNetRtInfo->u4RowStatus = NextRtInfo.u4RowStatus;
        pNextNetRtInfo->u2RtType = NextRtInfo.u2RtType;
        pNextNetRtInfo->u2RtProto = NextRtInfo.u2RtProto;
        pNextNetRtInfo->u4RouteTag = NextRtInfo.u4RouteTag;

        return (NETIPV4_SUCCESS);
    }
    return (NETIPV4_FAILURE);
}

/*****************************************************************************
 *                                                                           *
 * Function           : RtmUtilGetNextBestRtInCxt                            *
 *                                                                           *
 * Inputs           :   pNetRtInfo - Route Entry for which the Next Entry is
 *                      required.
 *                      pRtmCxt -RtmContext Pointer                                   
 *
 * Outputs          :   pNextNetRtInfo - Next Route Entry in the Ip Forwarding
 *                      Table.
 *                    : u4ContextId -RTM Context ID                          * 
 *                                                                           *
 * Output(s)          :  Next Route Entry Information (pOutRtInfo)           *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           * 
 * Action :                                                                  *
 *   Fetches the Next Best Route Entry For the given IpAddress & NetMask.    *
 *   If The Input IpAddress & NetMask is 0, then First Entry from            * 
 *   the IP Forward Table is given as Output. The Values are to be set       *
 *   in InRtInfo, by the Calling Routing.                                    *
 *****************************************************************************/

INT1
RtmUtilGetNextBestRtInCxt (tRtmCxt * pRtmCxt, tRtInfo InRtInfo,
                           tRtInfo * pOutRtInfo)
{
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    UINT1               u1FoundFlag = FALSE;

    if (pOutRtInfo == NULL)
    {
        return IP_FAILURE;
    }

    if (IpGetBestRouteEntryInCxt (pRtmCxt, InRtInfo, &pRt) == IP_SUCCESS)
    {
        pTmpRt = pRt;
        while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pRt->i4Metric1))
        {
            if (u1FoundFlag == FALSE)
            {
                if (pTmpRt->u4NextHop > InRtInfo.u4NextHop)
                {
                    MEMCPY (pOutRtInfo, pTmpRt, sizeof (tRtInfo));
                    u1FoundFlag = TRUE;
                }
            }
            else
            {
                if ((pTmpRt->u4NextHop > InRtInfo.u4NextHop) &&
                    (pTmpRt->u4NextHop < (pOutRtInfo)->u4NextHop))
                {
                    MEMCPY (pOutRtInfo, pTmpRt, sizeof (tRtInfo));
                }
            }
            pTmpRt = pTmpRt->pNextAlternatepath;
        }
        if (u1FoundFlag == TRUE)
        {
            return IP_SUCCESS;
        }
    }

    if (IpGetNextBestRouteEntryInCxt (pRtmCxt, InRtInfo, &pRt) == IP_SUCCESS)
    {
        pTmpRt = pRt;
        while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pRt->i4Metric1))
        {
            if (u1FoundFlag == FALSE)
            {
                MEMCPY (pOutRtInfo, pTmpRt, sizeof (tRtInfo));
                u1FoundFlag = TRUE;
            }
            else
            {
                if (pTmpRt->u4NextHop < (pOutRtInfo)->u4NextHop)
                {
                    MEMCPY (pOutRtInfo, pTmpRt, sizeof (tRtInfo));
                }
            }
            pTmpRt = pTmpRt->pNextAlternatepath;
        }
        return IP_SUCCESS;
    }
    return IP_FAILURE;
}

/************************************************************************
 *  Function Name   : RtmApiGetBestRouteEntryInCxt
 *  Description     : An Api to get Best Route route entry. 
 *                    RTM_LOCK will be taken in CLI
 *                    
 *  Input           : u4RtmCxtId - VRF Id
 *                    InRtInfo - Input ROute Info
 *  Output          : ppOutRtInfo - Output Route Info
 *  
 *  Returns         : IP_SUCCESS or IP_FAILURE
 ************************************************************************/

INT4
RtmApiGetBestRouteEntryInCxt (UINT4 u4RtmCxtId,
                              tRtInfo InRtInfo, tRtInfo ** ppOutRtInfo)
{
    INT4                i4RetVal = IP_FAILURE;
    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt (u4RtmCxtId);
    if (pRtmCxt == NULL)
    {
        return i4RetVal;
    }
    i4RetVal = IpGetBestRouteEntryInCxt (pRtmCxt, InRtInfo, ppOutRtInfo);
    return i4RetVal;
}

/************************************************************************
 *  Function Name   : RtmApiGetNextBestRouteEntryInCxt
 *  Description     : An Api to get next Best Route route entry. 
 *                    RTM_LOCK will be taken in CLI
 *                    
 *  Input           : u4RtmCxtId - VRF Id
 *                    InRtInfo - Input ROute Info
 *  Output          : ppOutRtInfo - Output Route Info
 *  
 *  Returns         : IP_SUCCESS or IP_FAILURE
 ************************************************************************/

INT4
RtmApiGetNextBestRouteEntryInCxt (UINT4 u4RtmCxtId,
                                  tRtInfo InRtInfo, tRtInfo ** ppOutRtInfo)
{
    INT4                i4RetVal = IP_FAILURE;
    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt (u4RtmCxtId);
    if (pRtmCxt == NULL)
    {
        return i4RetVal;
    }

    i4RetVal = IpGetNextBestRouteEntryInCxt (pRtmCxt, InRtInfo, ppOutRtInfo);
    return i4RetVal;
}

/************************************************************************
 * Function Name   : RtmApiGetRouterId
 * Description     : An Api to handle protocol deregistration.
 * Input           : i4RtmContextId - VRF Id
 *                   pu4RouterId.
 * Output          : None
 * Returns         : IP_SUCCESS or IP_FAILURE
 *************************************************************************/
INT4
RtmApiGetRouterId (INT4 i4RtmContextId, UINT4 *pu4RouterId)
{

    tRtmCxt            *pRtmCxt = NULL;

    RTM_PROT_LOCK ();

    if (UtilRtmSetContext ((UINT4) i4RtmContextId) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return IP_FAILURE;
    }

    pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return IP_FAILURE;
    }

    *pu4RouterId = pRtmCxt->u4RouterId;
    UtilRtmResetContext ();

    RTM_PROT_UNLOCK ();

    return IP_SUCCESS;

}

/************************************************************************
 * Function Name   : RtmGetRtrIdConfigType
 * Description     : An Api to get protocol router-id configuration type.
 * Input           : i4RtmContextId - VRF Id
 *                   pu4Type.
 * Output          : None
 * Returns         : IP_SUCCESS or IP_FAILURE
 *************************************************************************/
INT4
RtmGetRtrIdConfigType (INT4 i4RtmContextId, UINT1 *pu1Type)
{
    tRtmCxt            *pRtmCxt = NULL;

    if (UtilRtmSetContext ((UINT4) i4RtmContextId) == SNMP_FAILURE)
    {
        return IP_FAILURE;
    }
    pRtmCxt = gRtmGlobalInfo.pRtmCxt;
    if (pRtmCxt == NULL)
    {
        return IP_FAILURE;
    }

    *pu1Type = pRtmCxt->u1RtrIdType;
    UtilRtmResetContext ();

    return IP_SUCCESS;
}

/************************************************************************
 *  Function Name   : RtmNetIpv4HandleProtoDeRegInCxt
 *  Description     : An Api to handle protocol deregistration. 
 *                    
 *  Input           : u4RtmCxtId - VRF Id
 *                    u1ProtocolId - protocol identifier which is being
 *                    deregistered.
 *  Output          : None
 *  
 *  Returns         : IP_SUCCESS or IP_FAILURE
 ************************************************************************/
VOID
RtmNetIpv4HandleProtoDeRegInCxt (UINT4 u4RtmCxtId, UINT1 u1ProtocolId)
{
    tRtInfo             InRtInfo;
    tRtInfo             OutRtInfo;
    tRtmCxt            *pRtmCxt = NULL;

    RTM_PROT_LOCK ();
    pRtmCxt = UtilRtmGetCxt (u4RtmCxtId);
    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return;
    }

    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
    while (RtmUtilGetNextBestRtInCxt (pRtmCxt, InRtInfo, &OutRtInfo)
           == IP_SUCCESS)
    {
        if (!((OutRtInfo.u2RtProto == u1ProtocolId) &&
              (IpForwardingTableDeleteRouteInCxt (pRtmCxt, &OutRtInfo) ==
               IP_SUCCESS)))
            MEMCPY (&InRtInfo, &OutRtInfo, sizeof (tRtInfo));
    }
    RTM_PROT_UNLOCK ();
}

INT4
IpForwardingTableGetRouteInfo (UINT4 u4Dest, UINT4 u4Mask,
                               INT1 i1ProtoId, tRtInfo ** pIpRoutes)
{
    return (IpForwardingTableGetRouteInfoInCxt
            (0, u4Dest, u4Mask, i1ProtoId, pIpRoutes));
}

INT1
IpGetNextBestRouteEntry (tRtInfo InRtInfo, tRtInfo ** ppOutRtInfo)
{
    return (IpGetNextBestRouteEntryInCxt
            (gRtmGlobalInfo.apRtmCxt[0], InRtInfo, ppOutRtInfo));
}

/************************************************************************
 *  Function Name   : RtmUtilCheckIsRouteGreater
 *  Description     : An util to check whether the first route is
 *                    greater than the second route.
 *  Input           : RtInfo1 - First route
 *                  : RtInfo2 - Second route
 *  Output          : returns IP_SUCCESS if the RtInfo1 > RtInfo2.
 *                    returns IP_FAILURE otherwise.
 *  
 *  Returns         : IP_SUCCESS or IP_FAILURE
 ************************************************************************/
UINT1
RtmUtilCheckIsRouteGreater (tNetIpv4RtInfo * pRtInfo1,
                            tNetIpv4RtInfo * pRtInfo2)
{

    /*first index */
    if (pRtInfo2->u4DestNet > pRtInfo1->u4DestNet)
    {
        return ((UINT1) IP_FAILURE);
    }
    if (pRtInfo2->u4DestNet < pRtInfo1->u4DestNet)
    {
        return IP_SUCCESS;
    }

    /*second index */
    if (pRtInfo2->u4DestMask > pRtInfo1->u4DestMask)
    {
        return ((UINT1) IP_FAILURE);
    }

    if (pRtInfo2->u4DestMask < pRtInfo1->u4DestMask)
    {
        return IP_SUCCESS;
    }

    /*third index */
    if (pRtInfo2->u4Tos > pRtInfo1->u4Tos)
    {
        return ((UINT1) IP_FAILURE);
    }

    if (pRtInfo2->u4Tos < pRtInfo1->u4Tos)
    {
        return IP_SUCCESS;
    }

    /*Fourth index */
    if (pRtInfo2->u4NextHop > pRtInfo1->u4NextHop)
    {
        return ((UINT1) IP_FAILURE);
    }

    if (pRtInfo2->u4NextHop < pRtInfo1->u4NextHop)
    {
        return IP_SUCCESS;
    }

    return ((UINT1) IP_FAILURE);

}

/*****************************************************************************
 *                                                                           *
 * Function           : RtmUtilGetNextAllRtInCxt                             *
 *                                                                           *
 * Inputs           :   pNetRtInfo - Route Entry for which the Next Entry is
 *                      required.
 *                      pRtmCxt -RtmContext Pointer                                   
 *
 * Outputs          :   pNextNetRtInfo - Next Route Entry in the Ip Forwarding
 *                      Table.
 *                                                                           *
 * Output(s)          :  Next Route Entry Information (pOutRtInfo)           *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *****************************************************************************/

INT1
RtmUtilGetNextAllRtInCxt (tRtmCxt * pRtmCxt, tRtInfo InRtInfo,
                          tRtInfo * pOutRtInfo)
{
    tOutputParams       OutParams;
    tRtInfo            *paAppSpecInfo[MAX_ROUTING_PROTOCOLS + 1];
    PRIVATE tRtInfo     aAppSpecInfo[MAX_ROUTING_PROTOCOLS + 1];
    tRtInfo            *pTmpRt = NULL;
    INT4                i4Cnt = 0;
    UINT1               u1FoundFlag = FALSE;
    INT1                i1RetVal;

    if (pOutRtInfo == NULL)
    {
        return IP_FAILURE;
    }

    MEMSET (aAppSpecInfo, 0, ((MAX_ROUTING_PROTOCOLS + 1) *
                              (sizeof (tRtInfo))));
    for (i4Cnt = 0; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
    {
        paAppSpecInfo[i4Cnt] = (tRtInfo *) & aAppSpecInfo[i4Cnt];
    }

    i1RetVal =
        (INT1) IpForwardingTableGetRouteInfoInCxt (pRtmCxt->u4ContextId,
                                                   InRtInfo.u4DestNet,
                                                   InRtInfo.u4DestMask,
                                                   ALL_ROUTING_PROTOCOL,
                                                   (tRtInfo **) &
                                                   paAppSpecInfo[0]);

    if (i1RetVal == IP_SUCCESS)
    {
        for (i4Cnt = 1; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
        {
            pTmpRt = paAppSpecInfo[i4Cnt];
            while (pTmpRt != NULL)
            {
                if (u1FoundFlag == FALSE)
                {
                    if (pTmpRt->u4NextHop > InRtInfo.u4NextHop)
                    {
                        MEMCPY (pOutRtInfo, pTmpRt, sizeof (tRtInfo));
                        u1FoundFlag = TRUE;
                    }
                }
                else
                {
                    if ((pTmpRt->u4NextHop > InRtInfo.u4NextHop) &&
                        (pTmpRt->u4NextHop < (pOutRtInfo)->u4NextHop))
                    {
                        MEMCPY (pOutRtInfo, pTmpRt, sizeof (tRtInfo));
                    }
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }
        }
        if (u1FoundFlag == TRUE)
        {
            return IP_SUCCESS;
        }
    }

    MEMSET (&OutParams, 0, sizeof (tOutputParams));
    MEMSET (paAppSpecInfo, 0, ((MAX_ROUTING_PROTOCOLS + 1) *
                               (sizeof (tRtInfo *))));

    OutParams.pAppSpecInfo = (VOID *) paAppSpecInfo;

    i1RetVal =
        IpGetNextRouteEntryInCxt (pRtmCxt, InRtInfo, ALL_ROUTING_PROTOCOL,
                                  &OutParams);

    if (i1RetVal == IP_SUCCESS)
    {
        for (i4Cnt = 0; i4Cnt <= MAX_ROUTING_PROTOCOLS; i4Cnt++)
        {
            pTmpRt = paAppSpecInfo[i4Cnt];
            while (pTmpRt != NULL)
            {
                if (u1FoundFlag == FALSE)
                {
                    MEMCPY (pOutRtInfo, pTmpRt, sizeof (tRtInfo));
                    u1FoundFlag = TRUE;
                }
                else
                {
                    if (pTmpRt->u4NextHop < (pOutRtInfo)->u4NextHop)
                    {
                        MEMCPY (pOutRtInfo, pTmpRt, sizeof (tRtInfo));
                    }
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }
        }
        if (u1FoundFlag == TRUE)
        {
            return IP_SUCCESS;
        }
    }
    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmApiConfigRRDAdminStatus
 *
 * Input(s)           : u4RtmCxtId
 *
 * Output(s)          : None
 *
 * Returns            : Sucess/Failure
 *
 * Action             : This function initializes the RTM during
 *                      startup.
 *
-------------------------------------------------------------------*/
INT1
RtmApiConfigRRDAdminStatus (UINT4 u4RtmCxtId)
{
    INT1                i1Return = SNMP_FAILURE;

    RTM_PROT_LOCK ();
    i1Return =
        nmhSetFsMIRrdAdminStatus ((INT4) u4RtmCxtId, RTM_ADMIN_STATUS_ENABLED);
    if (i1Return != SNMP_SUCCESS)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }
    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    :  ClearInetCidrRoute
 *
 * DESCRIPTION      : This function clear the CIDR Route counter.
 *
 * INPUT            : u4ModuleId
 *
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 ******************************************************************************/
VOID
ClearInetCidrRoute (UINT4 u4ContextId)
{

    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);
    if (pRtmCxt == NULL)
    {
        return;
    }
    RTM_PROT_LOCK ();
    pRtmCxt->u4IpFwdTblRouteNum = 0;
    RTM_PROT_UNLOCK ();
    return;

}

/******************************************************************************
 * FUNCTION NAME    :  RtmTstPopulateInetCidrRoute
 *
 * DESCRIPTION      : This function clear the CIDR Route counter.
 *
 * INPUT            : u4ModuleId
 *
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 ******************************************************************************/
VOID
RtmTstPopulateInetCidrRoute (UINT4 u4ContextId)
{

    tRtmCxt            *pRtmCxt = NULL;
    UINT4               u4Value = 5;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);
    if (pRtmCxt == NULL)
    {
        return;
    }
    RTM_PROT_LOCK ();
    pRtmCxt->u4IpFwdTblRouteNum = u4Value;
    RTM_PROT_UNLOCK ();
    return;

}

/*****************************************************************************/
/* Function Name      : RtmGetShowCmdOutputAndCalcChkSum                     */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
INT4
RtmGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
    {
        if (RtmCliGetShowCmdOutputToFile ((UINT1 *) RTM_AUDIT_FILE_ACTIVE) !=
            RTM_SUCCESS)
        {
            return RTM_FAILURE;
        }
        if (RtmCliCalcSwAudCheckSum
            ((UINT1 *) RTM_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != RTM_SUCCESS)
        {
            return RTM_FAILURE;
        }
    }
    else if (RTM_GET_NODE_STATUS () == RM_STANDBY)
    {
        if (RtmCliGetShowCmdOutputToFile ((UINT1 *) RTM_AUDIT_FILE_STDBY) !=
            RTM_SUCCESS)
        {
            return RTM_FAILURE;
        }
        if (RtmCliCalcSwAudCheckSum
            ((UINT1 *) RTM_AUDIT_FILE_STDBY, pu2SwAudChkSum) != RTM_SUCCESS)
        {
            return RTM_FAILURE;
        }
    }
    else
    {
        return RTM_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return RTM_SUCCESS;
}

/****************************************************************************
 *  Function    :  RtmConfigRrdRouterId
 *  Description :  This function is called for route-redistribution
 *                 in BGP
 *  Input       :  none
 *  Output      :  none
 *  Returns     :  none
 * ****************************************************************************/
INT1
RtmConfigRrdRouterId (UINT4 u4Context, UINT4 u4SetValFsbgp4Identifier)
{

    RTM_PROT_LOCK ();
    if (nmhSetFsMIRrdRouterId ((INT4) u4Context, u4SetValFsbgp4Identifier) ==
        SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return SNMP_FAILURE;
    }

    RTM_PROT_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  RtmResetRouterId
 *  Description :  This function is called to reset the router-id
 *                 in BGP
 *  Input       :  none
 *  Output      :  none
 *  Returns     :  none
 * ****************************************************************************/
INT1
RtmResetRouterId (UINT4 u4Context, UINT4 u4SetValFsbgp4Identifier)
{
    tRtmCxt            *pRtmCxt = NULL;

    RTM_PROT_LOCK ();

    if (UtilRtmSetContext ((UINT4) u4Context) == SNMP_FAILURE)
    {
        RTM_PROT_UNLOCK ();
        return IP_FAILURE;
    }

    pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        RTM_PROT_UNLOCK ();
        return IP_FAILURE;
    }

    pRtmCxt->u4RouterId = u4SetValFsbgp4Identifier;
    UtilRtmResetContext ();

    RTM_PROT_UNLOCK ();

    return IP_SUCCESS;

}

INT4
RtmAddNextHopForRoute (tRtmCxt * pRtmCxt, tRtInfo * pRtInfo)
{

    tPNhNode           *pNhEntry = NULL, ExstNhNode;
    tPRtEntry          *pRtEntry = NULL;
    tPRtEntry          *pRtCurr = NULL;
    MEMSET (&ExstNhNode, 0, sizeof (tPNhNode));
    /* If the route is directly connected(Having next hop as zero,
     *      * Dont add to pRouteNHRBRoot */
    if (pRtInfo->u4NextHop == 0)
    {
        return IP_SUCCESS;
    }

    /* Allocate for  Route node */
    if ((pRtEntry = (tPRtEntry *)
         MemAllocMemBlk (gRtmGlobalInfo.RtmURRtPoolId)) == NULL)
    {
        return IP_FAILURE;
    }

    /* Check whether Pending Next hop entry with this next hop exists */
    ExstNhNode.u4NextHop = pRtInfo->u4NextHop;
    ExstNhNode.pRtmCxt = pRtmCxt;

    pNhEntry = RBTreeGet (gRtmGlobalInfo.pRouteNHRBRoot, &ExstNhNode);

    if (pNhEntry == NULL)
    {
        /* Allocate the Next hope entry */
        if ((pNhEntry = (tPNhNode *) MemAllocMemBlk
             (gRtmGlobalInfo.RtmPNextHopPoolId)) == NULL)
        {
            MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                                (UINT1 *) pRtEntry);
            return IP_FAILURE;
        }
        pNhEntry->u4NextHop = pRtInfo->u4NextHop;
        pNhEntry->pRtmCxt = pRtmCxt;
        /* Add the next hop Entry to RBTree */
        if (RBTreeAdd (gRtmGlobalInfo.pRouteNHRBRoot, pNhEntry) == RB_FAILURE)
        {
            MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                                (UINT1 *) pRtEntry);
            MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                                (UINT1 *) pNhEntry);
            return IP_FAILURE;
        }
        TMO_SLL_Init (&(pNhEntry->routeEntryList));
    }

    /* Avoid duplicate node addition inlist */
    TMO_SLL_Scan (&pNhEntry->routeEntryList, pRtCurr, tPRtEntry *)
    {
        if ((pRtCurr->pPendRt == pRtInfo) &&
            (pRtCurr->pRtmCxt->u4ContextId == pRtmCxt->u4ContextId))

        {
            MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                                (UINT1 *) pRtEntry);

            return IP_SUCCESS;
        }
    }

    TMO_SLL_Init_Node (((tTMO_SLL_NODE *) pRtEntry));

    pRtEntry->pPendRt = pRtInfo;
    pRtEntry->pRtmCxt = pRtmCxt;

    TMO_SLL_Insert (&(pNhEntry->routeEntryList), NULL,
                    ((tTMO_SLL_NODE *) pRtEntry));

    return IP_SUCCESS;

}

INT4
RtmDeleteNextHopforRoute (tRtmCxt * pRtmCxt, tRtInfo * pRtInfo)
{
    tPNhNode           *pPNhEntry = NULL, ExstPNhNode;
    tPRtEntry          *pPRtCurr;

    /* If the route is directly connected(Having next hop as zero,
     *      * it should not be added to pRouteNHRBRoot */
    if (pRtInfo->u4NextHop == 0)
    {
        return IP_SUCCESS;
    }

    /* Get the Pending next hop node from pRouteNHRBRoot */
    MEMSET (&ExstPNhNode, 0, sizeof (tPNhNode));
    ExstPNhNode.u4NextHop = pRtInfo->u4NextHop;
    ExstPNhNode.pRtmCxt = pRtmCxt;

    pPNhEntry = RBTreeGet (gRtmGlobalInfo.pRouteNHRBRoot, &ExstPNhNode);

    /* No entry exist in PRT */
    if (pPNhEntry == NULL)
    {
        return IP_FAILURE;
    }

    /* Get the Pending route entry having Trie Pointer */
    TMO_SLL_Scan (&pPNhEntry->routeEntryList, pPRtCurr, tPRtEntry *)
    {
        if ((pPRtCurr->pPendRt == pRtInfo) &&
            (pPRtCurr->pRtmCxt->u4ContextId == pRtmCxt->u4ContextId))

        {
            break;
        }
    }

    /* Route doesnt exist in PRT */
    if (pPRtCurr == NULL)
    {
        return IP_FAILURE;
    }

    /* Delete the pending route entry from the list */
    TMO_SLL_Delete (&pPNhEntry->routeEntryList, (tTMO_SLL_NODE *) pPRtCurr);

    MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId, (UINT1 *) pPRtCurr);

    if (TMO_SLL_Count (&(pPNhEntry->routeEntryList)) == 0)
    {
        /* No pending route exists in the list.Delete the next hop
         *          * entry from RB Tree */
        if (RBTreeRemove (gRtmGlobalInfo.pRouteNHRBRoot, pPNhEntry) ==
            RB_FAILURE)
        {
            return IP_FAILURE;
        }
        MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                            (UINT1 *) pPNhEntry);
    }

    return IP_SUCCESS;
}

tPNhNode           *
RtmGetReachableNextHopInCxt (tRtmCxt * pRtmCxt, UINT4 u4NextHopIpAddr)
{

    tPNhNode           *pPNhEntry = NULL, ExstPNhNode;
    MEMSET (&ExstPNhNode, 0, sizeof (tPNhNode));
    if (u4NextHopIpAddr == 0)
    {
        return NULL;

    }

    /* Get the Pending next hop node from pRouteNHRBRoot */
    ExstPNhNode.u4NextHop = u4NextHopIpAddr;
    ExstPNhNode.pRtmCxt = pRtmCxt;
    pPNhEntry = RBTreeGet (gRtmGlobalInfo.pRouteNHRBRoot, &ExstPNhNode);

    return pPNhEntry;

}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmApiHandleOperUpIndication
 *
 * Input(s)           : u2Port - Interface index for which oper up 
 *                               indication is to be processed
 *
 * Output(s)          : None
 *
 * Returns            : RTM_SUCCESS - If the RTM handling for oper-up 
 *                      is successful.
 *                      Else RTM_FAILURE.
 *
 * Action             : This function handles the Oper-up indication  
 *                      for the specific port.
 *         
+-------------------------------------------------------------------*/
INT4
RtmApiHandleOperUpIndication (UINT2 u2Port)
{
    tRtmRtMsg          *pRtmMsg = NULL;

    RTM_PROT_LOCK ();

    if (RTM_ROUTE_MSG_ENTRY_ALLOC (pRtmMsg) == NULL)
    {
        UtlTrcLog (1, 1, "",
                   "ERROR[RTM]: RTM Oper up Message Allocation Failure\n");
        RTM_PROT_UNLOCK ();
        return RTM_FAILURE;
    }

    MEMSET (pRtmMsg, 0, sizeof (tRtmRtMsg));

    pRtmMsg->u1MsgType = RTM_OPER_UP_MESSAGE;
    pRtmMsg->unRtmMsg.RtInfo.u4RtIfIndx = (UINT4) u2Port;

    if (OsixQueSend (gRtmRtMsgQId, (UINT1 *) &pRtmMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        RTM_ROUTE_MSG_FREE (pRtmMsg);
        UtlTrcLog (1, 1, "",
                   "ERROR[RTM]: Oper Up Message Send to Queue Failure\n");
        RTM_PROT_UNLOCK ();
        return (RTM_FAILURE);
    }

    OsixEvtSend (gRtmTaskId, RTM_ROUTE_MSG_EVENT);

    RTM_PROT_UNLOCK ();

    return RTM_SUCCESS;
}
