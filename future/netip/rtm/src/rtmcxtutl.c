/* $Id: rtmcxtutl.c,v 1.16 2016/07/29 09:53:38 siva Exp $*/
#include "rtminc.h"
#include "fsmirtlw.h"
#include "rmgr.h"
#include "fsmsipcli.h"
#include "ipvx.h"

PRIVATE VOID        RtmClearCommonRouteTable (UINT4 u4ContextId);
PRIVATE VOID        RtmClearRrdRoutingProtoTable (UINT4 u4ContextId);
PRIVATE VOID        RtmClearRrdControlTable (UINT4 u4ContextId);
PRIVATE VOID        RtmClearRoutingTable (UINT4 u4ContextId);

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilRtmGetVcmSystemMode                            */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE/VCM_SI_MODE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtmGetVcmSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilRtmGetVcmSystemModeExt                         */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_MI_MODE/VCM_SI_MODE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtmGetVcmSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmVcmIsVcExist                                        */
/*                                                                           */
/* Description  : This function finds and checks whether the given           */
/*               context ID is valid and available in VCM                    */
/*                                                                           */
/* Input        :  u4RtmCxtId  : Rtm Context Id                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM_SUCCESS/RTM_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtmVcmIsVcExist (UINT4 u4RtmCxtId)
{
    if (UtilRtmGetVcmSystemMode (RTM_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmIsL3VcExist (u4RtmCxtId) == VCM_FALSE)
        {
            return RTM_FAILURE;
        }
    }
    else
    {
        /* Single VRF mode */
        if (u4RtmCxtId != RTM_DEFAULT_CXT_ID)
        {
            return RTM_FAILURE;
        }
    }
    return RTM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmGetCxt                                              */
/*                                                                           */
/* Description  : This function finds and returns the pointer to the         */
/*                RTM context structure for the given RTM context ID         */
/*                                                                           */
/* Input        :  u4RtmCxtId  : Rtm Context Id                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to RTM  context structure, if context is found     */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC tRtmCxt     *
UtilRtmGetCxt (UINT4 u4RtmCxtId)
{
    if ((UtilRtmVcmIsVcExist (u4RtmCxtId) == RTM_FAILURE) ||
        (gRtmGlobalInfo.apRtmCxt[u4RtmCxtId] == NULL))
    {
        return NULL;
    }
    return (gRtmGlobalInfo.apRtmCxt[u4RtmCxtId]);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmGetNextCxtId                                       */
/*                                                                           */
/* Description  : This function finds the first valid ospf context ID        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pu4RtmCxtId                                               */
/*                                                                           */
/* Returns      : RTM_FAILURE/RTM_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtmGetFirstCxtId (UINT4 *pu4RtmCxtId)
{
    UINT4               u4RtmCxtId = 0;
    UINT4               u4MaxCxts = 0;

    u4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    for (u4RtmCxtId = 0; u4RtmCxtId < u4MaxCxts; u4RtmCxtId++)
    {
        if ((gRtmGlobalInfo.apRtmCxt[u4RtmCxtId]) != NULL)
        {
            *pu4RtmCxtId = u4RtmCxtId;
            return RTM_SUCCESS;

        }
    }
    return RTM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmGetNextCxtId                                       */
/*                                                                           */
/* Description  : This function finds the next valid ospf context ID         */
/*                                                                           */
/* Input        : u4RtmCxtId  : Rtm Context Id                             */
/*                                                                           */
/* Output       : pu4NextRtmCxtId                                           */
/*                                                                           */
/* Returns      : RTM_FAILURE/RTM_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
UtilRtmGetNextCxtId (UINT4 u4RtmCxtId, UINT4 *pu4NextRtmCxtId)
{
    UINT4               u4CurRtmCxtId = 0;
    UINT4               u4MaxCxts = 0;

    u4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);

    u4RtmCxtId++;
    for (u4CurRtmCxtId = u4RtmCxtId;
         ((u4CurRtmCxtId < u4MaxCxts) &&
          (gRtmGlobalInfo.apRtmCxt[u4CurRtmCxtId] == NULL)); u4CurRtmCxtId++);
    if (u4CurRtmCxtId >= u4MaxCxts)
    {
        return RTM_FAILURE;
    }
    else
    {
        *pu4NextRtmCxtId = u4CurRtmCxtId;
        return RTM_SUCCESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmSetContext                                          */
/*                                                                           */
/* Description  : This function sets the current RTM                         */
/*                context pointer (gRtmGlobalInfo.pRtmCxt)                   */
/*                 for the given RTM Context ID                              */
/*                                                                           */
/* Input        :  u4RtmCxtId  : RTM Context Id                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_FAILURE/SNMP_SUCCESS                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtmSetContext (UINT4 u4RtmCxtId)
{
    if (UtilRtmGetVcmSystemModeExt (RTM_PROTOCOL_ID) == VCM_SI_MODE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        /* Set the Context pointer for VRF Wanted case */
        if ((UtilRtmVcmIsVcExist (u4RtmCxtId) == RTM_FAILURE))
        {
            return SNMP_FAILURE;
        }

        if ((gRtmGlobalInfo.pRtmCxt =
             gRtmGlobalInfo.apRtmCxt[u4RtmCxtId]) == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmResetContext                                       */
/*                                                                           */
/* Description  : This function resets the current Rtm                      */
/*                context pointer (gRtmGlobalInfo.pRtmCxt)                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
UtilRtmResetContext ()
{
    if (UtilRtmGetVcmSystemModeExt (RTM_PROTOCOL_ID) == VCM_MI_MODE)
    {
        gRtmGlobalInfo.pRtmCxt = NULL;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmIsValidCxtId                                       */
/*                                                                           */
/* Description  : This function checks whether the given context ID          */
/*                is valid RTM CONTEXT ID  or INVALID CONTEXT ID            */
/*                                                                           */
/* Input        : u4RtmCxtId  : Rtm Context Id                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM_FAILURE/RTM_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtmIsValidCxtId (UINT4 u4RtmCxtId)
{
    UINT4               u4MaxCxts = 0;

    u4MaxCxts = RTM_MIN (RTM_MAX_CONTEXT_LMT,
                         FsRTMSizingParams[MAX_RTM_CONTEXTS_SIZING_ID].
                         u4PreAllocatedUnits);
    if (u4RtmCxtId < u4MaxCxts)
    {
        if ((gRtmGlobalInfo.apRtmCxt[u4RtmCxtId]) != NULL)
        {
            return RTM_SUCCESS;
        }
    }
    return RTM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmIncMsrForRtmScalar                                  */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                MI Scalar                                                  */
/*                                                                           */
/* Input        : u4SetVal    : Set Value                                    */
/*                pu4ObjectId : Object Id                                   */
/*                u4OidLength : Oid Length                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilRtmIncMsrForRtmScalar (UINT4 u4SetVal, UINT4 *pu4ObjectId,
                           UINT4 u4OidLength, CHR1 c1Type)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RtmProtocolLock;
    SnmpNotifyInfo.pUnLockPointer = RtmProtocolUnlock;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    switch (c1Type)
    {
        case 'p':
            {
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%p", u4SetVal));
                break;
            }
        case 'i':
            {
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", u4SetVal));
                break;
            }
        default:
            break;
    }

#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
    UNUSED_PARAM (c1Type);
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmIncMsrForRtmTable                                   */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                MI RTM Table                                               */
/*                                                                           */
/* Input        : u4RtmCxtId  : Rtm Context Id                               */
/*                i4SetVal    : Set Value                                    */
/*                pu4ObjectId : Object Id                                   */
/*                u4OidLength : Oid Length                                  */
/*                c1Type      : set value type                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilRtmIncMsrForRtmTable (UINT4 u4RtmCxtId, INT4 i4SetVal, UINT4 *pu4ObjectId,
                          UINT4 u4OidLength, CHR1 c1Type)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RtmProtocolLock;
    SnmpNotifyInfo.pUnLockPointer = RtmProtocolUnlock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    switch (c1Type)
    {
        case 'p':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p", u4RtmCxtId, i4SetVal));
            break;
        }
        case 'i':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", u4RtmCxtId, i4SetVal));
            break;
        }
        default:
            break;
    }
#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4RtmCxtId);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
    UNUSED_PARAM (c1Type);
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmIncMsrForRrdControlTable                            */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                MI RRD Control Table                                       */
/*                                                                           */
/* Input        : u4RtmCxtId  : Rtm Context Id                               */
/*                u4DestIp : RRD Destination Address                         */
/*                u4DestMask - RRD dest mask                                 */
/*                i4SetVal    : Set Value                                    */
/*                pu4ObjectId : Object Id                                    */
/*                u4OidLength : Oid Length                                   */
/*                u1RowStatus : u1RowStatus                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilRtmIncMsrForRrdControlTable (UINT4 u4RtmCxtId, UINT4 u4DestIp,
                                 UINT4 u4DestMask, INT4 i4SetVal,
                                 UINT4 *pu4ObjectId, UINT4 u4OidLength,
                                 UINT1 u1RowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = u1RowStatus;
    SnmpNotifyInfo.pLockPointer = RtmProtocolLock;
    SnmpNotifyInfo.pUnLockPointer = RtmProtocolUnlock;
    SnmpNotifyInfo.u4Indices = IP_THREE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", u4RtmCxtId,
                      u4DestIp, u4DestMask, i4SetVal));
#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4RtmCxtId);
    UNUSED_PARAM (u4DestIp);
    UNUSED_PARAM (u4DestMask);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
    UNUSED_PARAM (u1RowStatus);
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmIncMsrForRrdProtoTable                              */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                MI RRD Proto Table                                         */
/*                                                                           */
/* Input        : u4RtmCxtId  : Rtm Context Id                               */
/*                i4ProtoId   : Routing protocols Id                         */
/*                i4SetVal    : Set Value                                    */
/*                pu4ObjectId : Object Id                                    */
/*                u4OidLength : Oid Length                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilRtmIncMsrForRrdProtoTable (UINT4 u4RtmCxtId, INT4 i4ProtoId,
                               INT4 i4SetVal, UINT4 *pu4ObjectId,
                               UINT4 u4OidLength)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RtmProtocolLock;
    SnmpNotifyInfo.pUnLockPointer = RtmProtocolUnlock;
    SnmpNotifyInfo.u4Indices = IP_TWO;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", u4RtmCxtId,
                      i4ProtoId, i4SetVal));
#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4RtmCxtId);
    UNUSED_PARAM (i4ProtoId);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilRtmIncMsrForRtPrivateStatus                            */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                MI RRD Proto Table                                         */
/*                                                                           */
/* Input        : u4RtmCxtId  : Rtm Context Id                               */
/*                u4Dest      : RT Dest Network                              */
/*                u4RouteMask : RT Dest mask                                 */
/*                i4Tos       : TOS                                          */
/*                u4NextHop   : RT Next Hop                                  */
/*                i4SetVal    : Set Value                                    */
/*                pu4ObjectId : Object Id                                    */
/*                u4OidLength : Oid Length                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
UtilRtmIncMsrForRtPrivateStatus (UINT4 u4RtmCxtId, UINT4 u4Dest,
                                 UINT4 u4RouteMask, INT4 i4Tos,
                                 UINT4 u4NextHop,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId,
                                 UINT4 u4OidLength)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum;

    RTM_PROT_UNLOCK ();
    IpvxUnLock ();

    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLength;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = RtmProtocolLock;
    SnmpNotifyInfo.pUnLockPointer = RtmProtocolUnlock;
    SnmpNotifyInfo.u4Indices = IP_FIVE;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p %i", u4RtmCxtId,
                      u4Dest, u4RouteMask, i4Tos, u4NextHop, i4SetVal));

    RTM_PROT_LOCK ();
    IpvxLock ();
#if !defined (ISS_WANTED) && !defined (RM_WANTED)
    UNUSED_PARAM (u4RtmCxtId);
    UNUSED_PARAM (u4Dest);
    UNUSED_PARAM (u4RouteMask);
    UNUSED_PARAM (i4Tos);
    UNUSED_PARAM (u4NextHop);
    UNUSED_PARAM (i4SetVal);
    UNUSED_PARAM (pu4ObjectId);
    UNUSED_PARAM (u4OidLength);
#endif
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilRtmGetCxtIdFromCxtName                         */
/*                                                                           */
/*     DESCRIPTION      : This function check whether the entry is present   */
/*                        for the correponding Vrf-name in VCM.              */
/*                        if yes it will                                     */
/*                        return the Context-Id for the respective vrf-name  */
/*                                                                           */
/*     INPUT            : pu1Alias   - Name of the Switch.                   */
/*                                                                           */
/*     OUTPUT           : pu4VcNum   - Context-Id of the switch              */
/*                                                                           */
/*     RETURNS          : RTM_SUCCESS/RTM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtmGetCxtIdFromCxtName (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    if (VcmIsVrfExist (pu1Alias, pu4VcNum) == VCM_FALSE)
    {
        return RTM_FAILURE;
    }
    return RTM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilRtmGetVcmAliasName                            */
/*                                                                           */
/*     DESCRIPTION      : This function will return the alias name of the    */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Vrf Name.                         */
/*                                                                           */
/*     RETURNS          : RTM_SUCCESS / RTM_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilRtmGetVcmAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    if (VcmGetAliasName (u4ContextId, pu1Alias) == VCM_FAILURE)
    {
        return RTM_FAILURE;
    }
    return RTM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RtmClearContextEntries                            */
/*                                                                           */
/*     DESCRIPTION      : This function clears the context based             */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : RTM_SUCCESS / RTM_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
RtmClearContextEntries (UINT4 u4ContextId)
{
    if (nmhValidateIndexInstanceFsMIRtmTable ((INT4) u4ContextId)
        == SNMP_FAILURE)
    {
        return RTM_FAILURE;
    }
    nmhSetFsMIRrdFilterByOspfTag ((INT4) u4ContextId, RTM_FILTER_DISABLED);
    nmhSetFsMIRrdRouterASNumber ((INT4) u4ContextId, 0);
    nmhSetFsMIRrdAdminStatus ((INT4) u4ContextId, RTM_ADMIN_STATUS_DISABLED);

    RtmClearRoutingTable (u4ContextId);
    RtmClearRrdControlTable (u4ContextId);
    RtmClearRrdRoutingProtoTable (u4ContextId);
    RtmClearCommonRouteTable (u4ContextId);
    return RTM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RtmClearRrdControlTable                           */
/*                                                                           */
/*     DESCRIPTION      : This function clears the RRD control table         */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
RtmClearRrdControlTable (UINT4 u4ContextId)
{
    INT4                i4NxtContextId = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4NxtIpAddress = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4NxtNetMask = 0;

    while (nmhGetNextIndexFsMIRrdControlTable ((INT4) u4ContextId,
                                               &i4NxtContextId, u4IpAddress,
                                               &u4NxtIpAddress, u4NetMask,
                                               &u4NxtNetMask) == SNMP_SUCCESS)
    {
        if (i4NxtContextId > (INT4) u4ContextId)
        {
            break;
        }
        u4IpAddress = u4NxtIpAddress;
        u4NetMask = u4NxtNetMask;
        nmhSetFsMIRrdControlRowStatus (i4NxtContextId, u4NxtIpAddress,
                                       u4NxtNetMask, RTM_DESTROY);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RtmClearRrdRoutingProtoTable                      */
/*                                                                           */
/*     DESCRIPTION      : This function clears the RRD Routing Proto Table   */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
RtmClearRrdRoutingProtoTable (UINT4 u4ContextId)
{
    INT4                i4NxtContextId = 0;
    INT4                i4Proto = 0;
    INT4                i4NxtProto = 0;

    while (nmhGetNextIndexFsMIRrdRoutingProtoTable ((INT4) u4ContextId,
                                                    &i4NxtContextId,
                                                    i4Proto, &i4NxtProto)
           == SNMP_SUCCESS)
    {
        if (i4NxtContextId > (INT4) u4ContextId)
        {
            break;
        }
        i4Proto = i4NxtProto;
        nmhSetFsMIRrdAllowOspfAreaRoutes (i4NxtContextId, i4NxtProto,
                                          RTM_REDISTRIBUTION_ENABLED);
        nmhSetFsMIRrdAllowOspfExtRoutes (i4NxtContextId, i4NxtProto,
                                         RTM_REDISTRIBUTION_ENABLED);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RtmClearCommonRouteTable                          */
/*                                                                           */
/*     DESCRIPTION      : This function clears the common routing table      */
/*                        configurations done for the given context.         */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
RtmClearCommonRouteTable (UINT4 u4ContextId)
{
    INT4                i4NxtContextId = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4NxtIpAddress = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4NxtNetMask = 0;
    INT4                i4RouteTos = 0;
    INT4                i4NxtRouteTos = 0;
    UINT4               u4RouteNextHop = 0;
    UINT4               u4NxtRouteNextHop = 0;

    while (nmhGetNextIndexFsMIRtmCommonRouteTable ((INT4) u4ContextId,
                                                   &i4NxtContextId, u4IpAddress,
                                                   &u4NxtIpAddress, u4NetMask,
                                                   &u4NxtNetMask, i4RouteTos,
                                                   &i4NxtRouteTos,
                                                   u4RouteNextHop,
                                                   &u4NxtRouteNextHop)
           == SNMP_SUCCESS)
    {
        if (i4NxtContextId > (INT4) u4ContextId)
        {
            break;
        }
        u4IpAddress = u4NxtIpAddress;
        u4NetMask = u4NxtNetMask;
        i4RouteTos = i4NxtRouteTos;
        u4RouteNextHop = u4NxtRouteNextHop;
        nmhSetFsMIRtmCommonRouteStatus (i4NxtContextId, u4NxtIpAddress,
                                        u4NxtNetMask, i4NxtRouteTos,
                                        u4NxtRouteNextHop, IPFWD_DESTROY);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : RtmClearRoutingTable                               */
/*                                                                           */
/*     DESCRIPTION      : This function clears the static IP routes          */
/*                        configurated for the given context.                */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

/* As the RTM context information is deleted before IP, the destroy
 * notification for the static routes configured via fsmsipvx.mib are
 * sent from here */
VOID
RtmClearRoutingTable (UINT4 u4ContextId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo            *pRt = NULL;
    tSNMP_OCTET_STRING_TYPE Dest;
    tSNMP_OCTET_STRING_TYPE RouteNextHop;
    tSNMP_OID_TYPE      RoutePolicy;
    UINT4               u4PrefixLen = 0;
    UINT4               u4RtPolicy[IP_TWO];
    UINT1               au1Dest[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1RouteHop[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (au1Dest, IP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1RouteHop, IP_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (u4RtPolicy, IP_ZERO, (IP_TWO * sizeof (UINT4)));
    Dest.i4_Length = IP_FOUR;
    RouteNextHop.i4_Length = IP_FOUR;
    Dest.pu1_OctetList = au1Dest;
    RouteNextHop.pu1_OctetList = au1RouteHop;
    RoutePolicy.u4_Length = IP_TWO;
    RoutePolicy.pu4_OidList = u4RtPolicy;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmInvdRtList), pRt, tRtInfo *)
    {
        pRt->u4DestNet = OSIX_HTONL (pRt->u4DestNet);
        MEMSET (Dest.pu1_OctetList, 0, IP_ADDR);
        MEMCPY (Dest.pu1_OctetList, &(pRt->u4DestNet),
                (sizeof (pRt->u4DestNet)));
        Dest.i4_Length = sizeof (pRt->u4DestNet);

        pRt->u4NextHop = OSIX_HTONL (pRt->u4NextHop);
        MEMSET (RouteNextHop.pu1_OctetList, 0, IP_ADDR);
        MEMCPY (RouteNextHop.pu1_OctetList, &(pRt->u4NextHop),
                (sizeof (pRt->u4NextHop)));
        RouteNextHop.i4_Length = sizeof (pRt->u4NextHop);

        /*      MEMCPY (Dest.pu1_OctetList, &pRt->u4DestNet, sizeof (UINT4));
           MEMCPY (RouteNextHop.pu1_OctetList, &pRt->u4NextHop, sizeof (UINT4));
         */
        u4PrefixLen = CfaGetCidrSubnetMaskIndex (pRt->u4DestMask);
        if (pRt->u4NextHop == 0)
        {
            RouteNextHop.i4_Length = 0;
        }

        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsMIStdInetCidrRouteStatus, 0,
                              TRUE, NULL, NULL, 7, SNMP_SUCCESS);

        SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %u %o %i %s %i",
                              (INT4) u4ContextId, INET_ADDR_TYPE_IPV4,
                              &Dest, u4PrefixLen, &RoutePolicy,
                              INET_ADDR_TYPE_IPV4, &RouteNextHop, DESTROY));
    }
    return;
}
