
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmprt.c,v 1.44 2018/02/12 11:49:26 siva Exp $
 *
 * Description:This file contains functions needed for Route Driven 
               implementation.Routes that are having Invalid NetHop
               (i.e UnResolved NextHop)  are maintained in Pending 
                Route Table.Routines that access this PRT are 
                maintained in this File.               
 *******************************************************************/
#include "rtminc.h"

extern UINT4        u4NotifyHigherLayer;

/*****************************************************************************/
/* Function     : RtmRBComparePNh                                            */
/*                                                                           */
/* Description  : Compare the Next hop entries in PRT                        */
/*                                                                           */
/* Input        : e1        Pointer to Next hop Node node1                   */
/*                e2        Pointer to Next hop Node node2                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
RtmRBComparePNh (tRBElem * e1, tRBElem * e2)
{
    tPNhNode           *pPNhEntry1 = e1;
    tPNhNode           *pPNhEntry2 = e2;

    if (pPNhEntry1->pRtmCxt->u4ContextId > pPNhEntry2->pRtmCxt->u4ContextId)
    {
        return RTM_RB_GREATER;
    }
    else if (pPNhEntry1->pRtmCxt->u4ContextId <
             pPNhEntry2->pRtmCxt->u4ContextId)
    {
        return RTM_RB_LESS;
    }

    /* Context ID is equal, so validate the nexthop */

    if (pPNhEntry1->u4NextHop > pPNhEntry2->u4NextHop)
    {
        return RTM_RB_GREATER;
    }
    else if (pPNhEntry1->u4NextHop < pPNhEntry2->u4NextHop)
    {
        return RTM_RB_LESS;
    }
    return RTM_RB_EQUAL;
}

/*****************************************************************************/
/* Function     : RtmUpdateRoutesForArpInitiationInCxt                       */
/*                                                                           */
/* Description  : Updates route entry after ARP initialization               */
/*                                                                           */
/* Input        : pRtmCxt - RtmContext pointer                               */
/*              : pRtArray - Route Information                               */
/*              : u1PathCnt - Path Count                                     */
/*              : u1PathCnt - Path Count                                     */
/*              : pau1NewFlag - Status of the Route                          */
/*                                                                           */
/* Output       : None                                                       */
/* Retun Value  : None                                                       */
/*****************************************************************************/
VOID
RtmUpdateRoutesForArpInitiationInCxt (tRtmCxt * pRtmCxt,
                                      tRtInfo ** pRtArray, UINT1 u1PathCnt,
                                      UINT1 *pau1NewFlag)
{
    UINT4               au4Indx[2];    /* The Key of Trie is made-up of address
                                       and mask */
    INT4                i4Status = TRIE_FAILURE;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *pRoute = NULL;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pBestRt = NULL;
    tRtInfo            *pPrevRt = NULL;
    tRtInfo            *pRtInfos[MAX_ROUTING_PROTOCOLS];
    UINT1               u1Count = 0;
    UINT1               u1RtCount = 1;
    UINT1               u1NewFlag = 0;

    pRoute = (tRtInfo *) (pRtArray + u1Count);

    au4Indx[0] = IP_HTONL (pRoute->u4DestNet);
    au4Indx[1] = IP_HTONL (pRoute->u4DestMask);

    /* Initialize the RtInfo array to NULL */
    MEMSET ((INT1 *) pRtInfos, 0, (sizeof (tRtInfo *) * MAX_ROUTING_PROTOCOLS));

    ROUTE_TBL_LOCK ();

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    OutParams.Key.pKey = NULL;
    OutParams.pAppSpecInfo = pRtInfos;

    i4Status = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4Status == TRIE_SUCCESS)
    {
        /* Get the route exists in the Trie */
        i4Status = IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pBestRt,
                                              RTM_GET_BST_WITH_PARAMS);
    }

    if (i4Status == IP_SUCCESS)
    {
        while (u1Count < u1PathCnt)
        {
            u1NewFlag = *(pau1NewFlag + u1Count);
            pPrevRt = (tRtInfo *) (pRtArray + u1Count);

            if (u1NewFlag != 0)
            {
                pTmpRt = pBestRt;

                while ((pTmpRt != NULL) &&
                       (pTmpRt->i4Metric1 == pPrevRt->i4Metric1))
                {
                    if (pPrevRt->u4NextHop == pTmpRt->u4NextHop)
                    {
                        if (u1NewFlag == RTM_RT_REACHABLE)
                        {
                            if (pTmpRt->u4Flag & RTM_ECMP_RT)
                            {
                                /* ECMP route */
                                RtmHandleECMPRtInCxt (pRtmCxt, pTmpRt, pBestRt,
                                                      NETIPV4_ADD_ROUTE,
                                                      RTM_SET_RT_REACHABLE);
                                RtmNotifyNewECMPRouteInCxt (pRtmCxt, pBestRt,
                                                            pTmpRt->i4Metric1);
                            }
                            else
                            {
                                /* Deleting the route installed as local route 
                                 * and adding the updated route */
                                RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt,
                                                                  pTmpRt,
                                                                  u1RtCount);
                                pTmpRt->u4Flag |= RTM_RT_REACHABLE;
                                RtmAddRouteToDataPlaneInCxt (pRtmCxt, pTmpRt,
                                                             u1RtCount);
                                RtmUpdateResolvedNHEntryInCxt (pRtmCxt,
                                                               pTmpRt->
                                                               u4NextHop,
                                                               RTM_ADD_ROUTE);
                            }
                        }
                        else
                        {
                            RtmAddRtToPRTInCxt (pRtmCxt, pTmpRt);
                        }
                        break;
                    }
                    pTmpRt = pTmpRt->pNextAlternatepath;
                }
            }
            u1Count++;
        }
    }
    ROUTE_TBL_UNLOCK ();
}

/*****************************************************************************
 * Function     : RtmHandlePRTRoutesForArpUpdation                           *
 * Description  : Scans PRT for the Resolved NextHop and deletes them        *
 * Input        : u4NextHopAddr - NextHop Address                            *
 *                i1Flag - Indicates state of the ARP                        *
 *                pRtmCxt - RTM Context pointer                              *
 *                u1Operation - Inicates whether ARP addition/deletion       *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
PUBLIC INT4
RtmHandlePRTRoutesForArpUpdationInCxt (tRtmCxt * pRtmCxt, UINT4 u4NextHopAddr,
                                       UINT1 u1Operation, INT1 i1Flag)
{
    tPNhNode           *pPNhEntry = NULL, ExstNhNode;
    tPRtEntry          *pPRtEntry;
    tRtInfo            *pBestRt = NULL;
    UINT1               u1RtCount = 1;

    if (u1Operation == ARP_DELETION)
    {
        if (i1Flag != ARP_PENDING)
        {
            /* Handle the deletion of valid ARP entry */
            return (RtmHandleArpEntryDeletionInCxt (pRtmCxt, u4NextHopAddr));
        }
    }

    ROUTE_TBL_LOCK ();
    ExstNhNode.u4NextHop = u4NextHopAddr;
    ExstNhNode.pRtmCxt = pRtmCxt;

    pPNhEntry = RBTreeGet (gRtmGlobalInfo.pPRTRBRoot, &ExstNhNode);

    /* No route exists with this next hop in PRT */
    if (pPNhEntry == NULL)
    {
        ROUTE_TBL_UNLOCK ();
        if (u1Operation == ARP_ADDITION)
        {
            if (RtmHandleECMPPRTRtsForArpUpdationInCxt (pRtmCxt, u4NextHopAddr)
                == IP_FAILURE)
                return IP_FAILURE;
        }

        return IP_SUCCESS;
    }

    if (u1Operation == ARP_ADDITION)
    {
        /* ARP is resolved ..Update the reachable nexthop table */
        RtmUpdateResolvedNHEntryInCxt (pRtmCxt, u4NextHopAddr, RTM_ADD_ROUTE);
    }

    /* Process all the routes having this next hop in PRT */
    while ((pPRtEntry = (tPRtEntry *)
            TMO_SLL_First (&pPNhEntry->routeEntryList)) != NULL)
    {
        TMO_SLL_Delete (&pPNhEntry->routeEntryList,
                        (tTMO_SLL_NODE *) pPRtEntry);

        /* Reset the flag that indicates whether route exists in 
         * PRT or not */
        pPRtEntry->pPendRt->u4Flag &= (UINT4) (~RTM_RT_IN_PRT);

        IpGetBestRouteEntryInCxt (pRtmCxt, *(pPRtEntry->pPendRt), &pBestRt);

        if (pPRtEntry->pPendRt->u4Flag & RTM_ECMP_RT)
        {
            /* ECMP route */
            /* Delete the route entry installed in Hardware with
             * old ARP entry information */
            RtmHandleECMPRtInCxt (pRtmCxt, pPRtEntry->pPendRt, pBestRt,
                                  NETIPV4_DELETE_ROUTE,
                                  RTM_DONT_SET_RT_REACHABLE);
            pPRtEntry->pPendRt->u4Flag |= RTM_ECMP_RT;
            /* Add the route to data plane with new ARP Entry Information */
            if (u1Operation == ARP_ADDITION)
            {
                RtmHandleECMPRtInCxt (pRtmCxt, pPRtEntry->pPendRt, pBestRt,
                                      NETIPV4_ADD_ROUTE, RTM_SET_RT_REACHABLE);
            }
            else
            {
                RtmHandleECMPRtInCxt (pRtmCxt, pPRtEntry->pPendRt, pBestRt,
                                      NETIPV4_ADD_ROUTE,
                                      RTM_DONT_SET_RT_REACHABLE);
            }
            RtmNotifyNewECMPRouteInCxt (pRtmCxt, pBestRt,
                                        pPRtEntry->pPendRt->i4Metric1);
        }
        else
        {
            /* Delete the route entry installed in Hardware with
             * old ARP entry information */
            RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt, pPRtEntry->pPendRt,
                                              u1RtCount);
            if (u1Operation == ARP_ADDITION)
            {
                pPRtEntry->pPendRt->u4Flag |= RTM_RT_REACHABLE;
                /* ARP is resolved .. reachable nexthop table is already Updated
                 * when ARP Addition is Called*/
            }
            /* Add the route to data plane with new ARP Entry Information */
            RtmAddRouteToDataPlaneInCxt (pRtmCxt, pPRtEntry->pPendRt,
                                         u1RtCount);
        }

        MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId, (UINT1 *) pPRtEntry);
    }

    if (RBTreeRemove (gRtmGlobalInfo.pPRTRBRoot, pPNhEntry) == RB_FAILURE)
    {
        ROUTE_TBL_UNLOCK ();
        return IP_FAILURE;
    }

    MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId, (UINT1 *) pPNhEntry);

    ROUTE_TBL_UNLOCK ();

    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmHandleECMPPRTRtsForArpUpdationInCxt                     *
 * Description  : Scans ECMPPRT for the Resolved NextHop and update them     *
 * Input        : u4NextHopAddr - NextHop Address                            *
 *                pRtmCxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmHandleECMPPRTRtsForArpUpdationInCxt (tRtmCxt * pRtmCxt, UINT4 u4NextHopAddr)
{
    tPNhNode           *pPNhEntry = NULL, ExstNhNode;
    tPRtEntry          *pEcmpRtEntry;
    tRtInfo            *pEcmpRt = NULL;
    tRtInfo            *pBestRt = NULL;
    UINT4               u4URNextHopCnt = 0;

    ROUTE_TBL_LOCK ();
    ExstNhNode.u4NextHop = u4NextHopAddr;
    ExstNhNode.pRtmCxt = pRtmCxt;

    pPNhEntry = RBTreeGet (gRtmGlobalInfo.pECMPRBRoot, &ExstNhNode);

    /* No route exists with this next hop in PRT */
    if (pPNhEntry == NULL)
    {
        ROUTE_TBL_UNLOCK ();
        return IP_SUCCESS;
    }

    /* ARP is resolved ..Update the reachable nexthop table */
    RtmUpdateResolvedNHEntryInCxt (pRtmCxt, u4NextHopAddr, RTM_ADD_ROUTE);

    /* Process all the routes having this next hop in ECMPPRT */
    while ((pEcmpRtEntry = (tPRtEntry *)
            TMO_SLL_First (&pPNhEntry->routeEntryList)) != NULL)
    {
        TMO_SLL_Delete (&pPNhEntry->routeEntryList,
                        (tTMO_SLL_NODE *) pEcmpRtEntry);

        /* Reset the flag that indicates whether route exists in 
         * ECMPPRT or not */
        pEcmpRt = pEcmpRtEntry->pPendRt;
        pEcmpRt->u4Flag &= (UINT4) (~RTM_RT_IN_ECMPPRT);

        /* Get the best route for the destination */
        IpGetBestRouteEntryInCxt (pRtmCxt, *pEcmpRt, &pBestRt);
        RtmHandleECMPRtInCxt (pRtmCxt, pEcmpRt, pBestRt, NETIPV4_ADD_ROUTE,
                              RTM_SET_RT_REACHABLE);
        RtmNotifyNewECMPRouteInCxt (pRtmCxt, pBestRt, pEcmpRt->i4Metric1);
        MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                            (UINT1 *) pEcmpRtEntry);
    }

    if (RBTreeRemove (gRtmGlobalInfo.pECMPRBRoot, pPNhEntry) == RB_FAILURE)
    {
        ROUTE_TBL_UNLOCK ();
        return IP_FAILURE;
    }
    MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId, (UINT1 *) pPNhEntry);

    RBTreeCount (gRtmGlobalInfo.pECMPRBRoot, &u4URNextHopCnt);
    if (u4URNextHopCnt == 0)
    {
        /* No more ECMP route available in ECMPPRT for next hop resolution. 
         * Stop the timer */
        TmrStop (gRtmGlobalInfo.RtmTmrListId, &(pRtmCxt->RtmECMPPRTTmr));
        gu1ECMPPRTTimerRunning = RTM_ECMP_TIMER_STOP;
    }

    ROUTE_TBL_UNLOCK ();
    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmAddRtToPRTInCxt                                         *
 * Description  : Add the Route pointer in Trie to the RBTree maintained     *
 *                for PRT.                                                   *
 * Input        : pPRoute - Pointer to the Route Added to trie               *
 *                pRtmCxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmAddRtToPRTInCxt (tRtmCxt * pRtmCxt, tRtInfo * pPRoute)
{
    tPNhNode           *pPNhEntry = NULL, ExstNhNode;
    tPRtEntry          *pPRt;
    tRtInfo            *pBestRt = NULL;
    UINT1               u1RtCount = 0;

    /* If the route is directly connected(Having next hop as zero,
     * Dont add to PRT */
    if (pPRoute->u4NextHop == 0)
    {
        return IP_SUCCESS;
    }

    /* Allocate for Pnding Route node */
    if ((pPRt = (tPRtEntry *) MemAllocMemBlk (gRtmGlobalInfo.RtmURRtPoolId))
        == NULL)
    {
        return IP_FAILURE;
    }

    if (pPRoute->u4Flag & RTM_RT_IN_ECMPPRT)
    {
        /* Delete the entry from ECMPPRT and add to PRT */
        RtmDeleteRtFromECMPPRTInCxt (pRtmCxt, pPRoute);
    }
    /* Check whether Pending Next hop entry with this next hop exists */
    ExstNhNode.u4NextHop = pPRoute->u4NextHop;
    ExstNhNode.pRtmCxt = pRtmCxt;

    pPNhEntry = RBTreeGet (gRtmGlobalInfo.pPRTRBRoot, &ExstNhNode);

    if (pPNhEntry == NULL)
    {
        /* Allocate the Pending Next hop node */
        if ((pPNhEntry = (tPNhNode *)
             MemAllocMemBlk (gRtmGlobalInfo.RtmPNextHopPoolId)) == NULL)
        {
            MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId, (UINT1 *) pPRt);
            return IP_FAILURE;
        }
        pPNhEntry->u4NextHop = pPRoute->u4NextHop;
        pPNhEntry->pRtmCxt = pRtmCxt;

        /* Add the Pending next hop Entry to RBTree */
        if (RBTreeAdd (gRtmGlobalInfo.pPRTRBRoot, pPNhEntry) == RB_FAILURE)
        {
            MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId, (UINT1 *) pPRt);
            MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                                (UINT1 *) pPNhEntry);
            return IP_FAILURE;
        }
        TMO_SLL_Init (&(pPNhEntry->routeEntryList));
    }

    TMO_SLL_Init_Node (((tTMO_SLL_NODE *) pPRt));

    pPRt->pPendRt = pPRoute;
    pPRt->pRtmCxt = pRtmCxt;

    TMO_SLL_Insert (&(pPNhEntry->routeEntryList), NULL,
                    ((tTMO_SLL_NODE *) pPRt));

    IpGetBestRouteEntryInCxt (pRtmCxt, *pPRoute, &pBestRt);
    RtmGetInstalledRtCnt (pBestRt, &u1RtCount);

    pPRt->pPendRt->u4Flag |= RTM_RT_IN_PRT;

    /* Delete the route installed in H/W for getting the packets
     * to CPU and install route to drop the packets */
    if ((pPRoute->u4Flag & RTM_NOTIFIED_RT) && u1RtCount == 0)
    {
        u4NotifyHigherLayer = 1;
        /* Delete the route installed in hardware to get packets to CPU 
         * and install route to drop packets.*/
        RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt, pPRt->pPendRt, ++u1RtCount);
        RtmAddRouteToDataPlaneInCxt (pRtmCxt, pPRt->pPendRt, u1RtCount);
        u4NotifyHigherLayer = 0;
    }

    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmAddRtToECMPPRTInCxt                                     *
 * Description  : Add the Route pointer in Trie to the RBTree maintained     *
 *                for ECMPPRT.                                               *
 * Input        : pEcmpRt - Pointer to the Route Added to ECMPPRT            *
 *                pRtmCxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmAddRtToECMPPRTInCxt (tRtmCxt * pRtmCxt, tRtInfo * pEcmpRt)
{
    tPNhNode           *pNhEntry = NULL, ExstNhNode;
    tPRtEntry          *pEcmpRtEntry = NULL;
    tPRtEntry          *pRtCurr = NULL;
    MEMSET (&ExstNhNode, 0, sizeof (tPNhNode));
    /* If the route is directly connected(Having next hop as zero,
     * Dont add to ECMPPRT */
    if (pEcmpRt->u4NextHop == 0)
    {
        return IP_SUCCESS;
    }

    if ((pEcmpRt->u4Flag & RTM_RT_IN_PRT) &&
        (pEcmpRt->u4Flag & RTM_RT_IN_ECMPPRT))
    {
        /* If the route is in PRT, don't add to ECMPPRT */
        return IP_SUCCESS;
    }

    /* Allocate for ECMP Route node */
    if ((pEcmpRtEntry = (tPRtEntry *)
         MemAllocMemBlk (gRtmGlobalInfo.RtmURRtPoolId)) == NULL)
    {
        return IP_FAILURE;
    }

    /* Check whether Pending Next hop entry with this next hop exists */
    ExstNhNode.u4NextHop = pEcmpRt->u4NextHop;
    ExstNhNode.pRtmCxt = pRtmCxt;

    pNhEntry = RBTreeGet (gRtmGlobalInfo.pECMPRBRoot, &ExstNhNode);

    if (pNhEntry == NULL)
    {
        /* Allocate the EcmpRt Next hope node */
        if ((pNhEntry = (tPNhNode *) MemAllocMemBlk
             (gRtmGlobalInfo.RtmPNextHopPoolId)) == NULL)
        {
            MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                                (UINT1 *) pEcmpRtEntry);
            return IP_FAILURE;
        }
        pNhEntry->u4NextHop = pEcmpRt->u4NextHop;
        pNhEntry->pRtmCxt = pRtmCxt;
        pNhEntry->u4MaxReties = 0;

        /* Add the ECMP next hop Entry to RBTree */
        if (RBTreeAdd (gRtmGlobalInfo.pECMPRBRoot, pNhEntry) == RB_FAILURE)
        {
            MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                                (UINT1 *) pEcmpRtEntry);
            MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                                (UINT1 *) pNhEntry);
            return IP_FAILURE;
        }
        TMO_SLL_Init (&(pNhEntry->routeEntryList));
    }

    /* Avoid duplicate node addition in ECMP PRT list */
    TMO_SLL_Scan (&pNhEntry->routeEntryList, pRtCurr, tPRtEntry *)
    {
        if ((pRtCurr->pPendRt == pEcmpRt) &&
            (pRtCurr->pRtmCxt->u4ContextId == pRtmCxt->u4ContextId))

        {
            MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId,
                                (UINT1 *) pEcmpRtEntry);

            return IP_SUCCESS;
        }
    }

    TMO_SLL_Init_Node (((tTMO_SLL_NODE *) pEcmpRtEntry));

    pEcmpRtEntry->pPendRt = pEcmpRt;
    pEcmpRtEntry->pRtmCxt = pRtmCxt;

    TMO_SLL_Insert (&(pNhEntry->routeEntryList), NULL,
                    ((tTMO_SLL_NODE *) pEcmpRtEntry));

    /* Update the flag to indicate that route exists in
     * ECMPPRT*/
    pEcmpRtEntry->pPendRt->u4Flag |= RTM_RT_IN_ECMPPRT;

    if (((pEcmpRt->u4Flag & RTM_RT_REACHABLE) == 0)
        && (gu1ECMPPRTTimerRunning == RTM_ECMP_TIMER_STOP))
    {
        /* Adding the first entry to ECMPPRT.Start the timer for next hop
         * resolution */

        RtmTmrSetTimer (&(pRtmCxt->RtmECMPPRTTmr), RTM_ECMPPRT_TIMER,
                        RTM_ECMPPRT_TIMER_INTERVAL);
        gu1ECMPPRTTimerRunning = RTM_ECMP_TIMER_RUNNING;
    }
    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmAddAllEcmpRtToEcmpPrtInCxt                              *
 * Description  : Add all the ECMP rts for the inputted destination from     *
 *                ECMPPRT                                                    *
 * Input        : pEcmpRt - Rt pointer which contain the destnt and subnet   *
 *                          information                                      *
 *                pRtmCxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
VOID
RtmAddAllEcmpRtToEcmpPrtInCxt (tRtmCxt * pRtmCxt, tRtInfo * pEcmpRt)
{
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pBestRt = NULL;

    IpGetBestRouteEntryInCxt (pRtmCxt, *pEcmpRt, &pBestRt);
    if (pBestRt == NULL)
    {
        return;
    }

    pTmpRt = pBestRt;
    if ((pBestRt->u4Flag & RTM_ECMP_RT) == 0)
    {
        return;
    }
    while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pBestRt->i4Metric1))
    {
        /* Add the route to ECMPPRT if the route is not resolved. */
        if ((pTmpRt->u4Flag & RTM_RT_REACHABLE) == 0)
        {
            RtmAddRtToECMPPRTInCxt (pRtmCxt, pTmpRt);
        }
        pTmpRt = pTmpRt->pNextAlternatepath;
    }
    return;
}

/*****************************************************************************
 * Function     : RtmDeleteRtFromPRT                                         *
 * Description  : Delete the Node with Route Pointer added to the PRT        *
 * Input        : pPRoute - Pointer to the Route Added to trie               *
 *                pRtmCxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/

INT4
RtmDeleteRtFromPRTInCxt (tRtmCxt * pRtmCxt, tRtInfo * pPRoute)
{
    tPNhNode           *pPNhEntry = NULL, ExstPNhNode;
    tPRtEntry          *pPRtCurr;
    UINT4               u4URNextHopCnt;

    /* If the route is directly connected(Having next hop as zero,
     * it should not have been added to PRT */
    if (pPRoute->u4NextHop == 0)
    {
        return IP_SUCCESS;
    }

    /* Get the Pending next hop node from PRT */
    ExstPNhNode.u4NextHop = pPRoute->u4NextHop;
    ExstPNhNode.pRtmCxt = pRtmCxt;

    pPNhEntry = RBTreeGet (gRtmGlobalInfo.pPRTRBRoot, &ExstPNhNode);

    /* No entry exist in PRT */
    if (pPNhEntry == NULL)
    {
        return IP_FAILURE;
    }

    /* Get the Pending route entry having Trie Pointer */
    TMO_SLL_Scan (&pPNhEntry->routeEntryList, pPRtCurr, tPRtEntry *)
    {
        if ((pPRtCurr->pPendRt == pPRoute) &&
            (pPRtCurr->pRtmCxt->u4ContextId == pRtmCxt->u4ContextId))

        {
            break;
        }
    }

    /* Route doesnt exist in PRT */
    if (pPRtCurr == NULL)
    {
        return IP_FAILURE;
    }

    /* Delete the pending route entry from the list */
    TMO_SLL_Delete (&pPNhEntry->routeEntryList, (tTMO_SLL_NODE *) pPRtCurr);

    /* Reset the flag that indicates whether route exists in 
     * PRT or not */
    pPRtCurr->pPendRt->u4Flag &= (UINT4) (~RTM_RT_IN_PRT);

    MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId, (UINT1 *) pPRtCurr);

    if (TMO_SLL_Count (&(pPNhEntry->routeEntryList)) == 0)
    {
        /* No pending route exists in the list.Delete the next hop
         * entry from RB Tree */
        if (RBTreeRemove (gRtmGlobalInfo.pPRTRBRoot, pPNhEntry) == RB_FAILURE)
        {
            return IP_FAILURE;
        }
        MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                            (UINT1 *) pPNhEntry);
    }

    RBTreeCount (gRtmGlobalInfo.pPRTRBRoot, &u4URNextHopCnt);

    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmDeleteRtFromECMPPRTInCxt                                *
 * Description  : Delete the Node with Route Pointer from  ECMPPRT           *
 * Input        : pEcmpRt - Pointer to the Route Added                       *
 * Output       : None                                                       *
 *                pRtmCxt -RTM Context Pointer                               *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/

INT4
RtmDeleteRtFromECMPPRTInCxt (tRtmCxt * pRtmCxt, tRtInfo * pEcmpRt)
{
    tPNhNode           *pNhEntry = NULL, ExstPNhNode;
    tPRtEntry          *pRtCurr;
    UINT4               u4URNextHopCnt = 0;

    /* If the route is directly connected(Having next hop as zero,
     * it should not have been added to ECMPPRT */
    if (pEcmpRt->u4NextHop == 0)
    {
        return IP_SUCCESS;
    }

    /* Get the ECMP next hop node from ECMPPRT */
    ExstPNhNode.u4NextHop = pEcmpRt->u4NextHop;
    ExstPNhNode.pRtmCxt = pRtmCxt;

    pNhEntry = RBTreeGet (gRtmGlobalInfo.pECMPRBRoot, &ExstPNhNode);

    /* No entry exist in ECMPPRT */
    if (pNhEntry == NULL)
    {
        return IP_FAILURE;
    }

    /* Get the Pending route entry having Trie Pointer */
    TMO_SLL_Scan (&pNhEntry->routeEntryList, pRtCurr, tPRtEntry *)
    {
        if ((pRtCurr->pPendRt == pEcmpRt) &&
            (pRtCurr->pRtmCxt->u4ContextId == pRtmCxt->u4ContextId))

        {
            break;
        }
    }

    /* Route doesnt exist in ECMPPRT */
    if (pRtCurr == NULL)
    {
        return IP_SUCCESS;
    }

    /* Delete the pending route entry from the list */
    TMO_SLL_Delete (&pNhEntry->routeEntryList, (tTMO_SLL_NODE *) pRtCurr);

    /* Reset the flag that indicates whether route exists in 
     * PRT or not */
    pRtCurr->pPendRt->u4Flag &= (UINT4) (~RTM_RT_IN_ECMPPRT);

    MemReleaseMemBlock (gRtmGlobalInfo.RtmURRtPoolId, (UINT1 *) pRtCurr);

    if (TMO_SLL_Count (&(pNhEntry->routeEntryList)) == 0)
    {
        /* No pending route exists in the list.Delete the next hop
         * entry from RB Tree */
        if (RBTreeRemove (gRtmGlobalInfo.pECMPRBRoot, pNhEntry) == RB_FAILURE)
        {
            return IP_FAILURE;
        }

        MemReleaseMemBlock (gRtmGlobalInfo.RtmPNextHopPoolId,
                            (UINT1 *) pNhEntry);
    }

    RBTreeCount (gRtmGlobalInfo.pECMPRBRoot, &u4URNextHopCnt);
    if (u4URNextHopCnt == 0)
    {
        /* No more ECMP route available in ECMPPRT for next hop resolution. 
         * Stop the timer */
        TmrStop (gRtmGlobalInfo.RtmTmrListId, &(pRtmCxt->RtmECMPPRTTmr));
        gu1ECMPPRTTimerRunning = RTM_ECMP_TIMER_STOP;
    }

    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmDelAllEcmpRtFromEcmpPrtInCxt                            *
 * Description  : Delete all the ECMP rts for the inputted destination from  *
 *                ECMPPRT                                                    *
 * Input        : pEcmpRt - Rt pointer which contain the destnt and subnet   *
 *                          information                                      *
 *                pRtmCxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
VOID
RtmDelAllEcmpRtFromEcmpPrtInCxt (tRtmCxt * pRtmCxt, tRtInfo * pEcmpRt)
{
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pBestRt = NULL;

    IpGetBestRouteEntryInCxt (pRtmCxt, *pEcmpRt, &pBestRt);

    pTmpRt = pBestRt;

    if (pBestRt == NULL)
    {
        return;
    }

    if ((pBestRt->u4Flag & RTM_ECMP_RT) == 0)
    {
        return;
    }
    while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pBestRt->i4Metric1))
    {
        /* Add the route to ECMPPRT if the route is not resolved. */
        if (pTmpRt->u4Flag & RTM_RT_IN_ECMPPRT)
        {
            RtmDeleteRtFromECMPPRTInCxt (pRtmCxt, pTmpRt);
        }
        pTmpRt = pTmpRt->pNextAlternatepath;
    }
    return;
}

/*****************************************************************************
 * Function     : RtmHandleArpEntryDeletionInCxt                             *
 * Description  : Scans Ip Routing Table for the Deleted ARP Entry and Add   *
 *                them to PRT                                                *
 * Input        : u4NextHopAddr - NextHop Address                            *
 *                pRtmCxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmHandleArpEntryDeletionInCxt (tRtmCxt * pRtmCxt, UINT4 u4NextHopIpAddr)
{
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pNotifiedRt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[2];    /* Address and Mask form the Key for
                                       Trie */
    tPNhNode           *pNhEntry = NULL;
    tPRtEntry          *pRtCurr = NULL;
    INT4                i4RetVal = IP_SUCCESS;
    UINT1               u1RtCount = 0;
    UINT1               u1NonEcmpRtCnt = 1;
    UINT1               au1Key[2 * sizeof (UINT4)];

    /*  Deleted ARP is not a next hop of any best route.
     *  Dont do anything */
    if (RtmIsReachableNextHopInCxt (pRtmCxt, u4NextHopIpAddr) == FALSE)
    {
        return IP_SUCCESS;
    }

    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    OutParams.Key.pKey = au1Key;
    MEMSET (OutParams.Key.pKey, 0, 2 * sizeof (UINT4));

    ROUTE_TBL_LOCK ();

    /* If default route is there with this next hop ,update the default route
     * installed in Hardware */
    if (TrieSearchEntry (&(InParams), &(OutParams)) == TRIE_SUCCESS)
    {
        if (IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRt,
                                       RTM_GET_BST_WITH_PARAMS) == IP_SUCCESS)
        {
            pTmpRt = pRt;
            while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pRt->i4Metric1))
            {
                if ((pTmpRt->u4Flag & RTM_RT_REACHABLE) &&
                    (pTmpRt->u4NextHop != 0))
                {
                    if (pTmpRt->u4NextHop == u4NextHopIpAddr)
                    {
                        if (pTmpRt->u4Flag & RTM_ECMP_RT)
                        {
                            /* ECMP Default Route */
                            /* Deleting  the route */
                            RtmHandleECMPRtInCxt (pRtmCxt, pTmpRt, pRt,
                                                  NETIPV4_DELETE_ROUTE,
                                                  RTM_DONT_SET_RT_REACHABLE);
                            pTmpRt->u4Flag |= RTM_ECMP_RT;
                            pTmpRt->u4Flag &= (UINT4) (~RTM_RT_REACHABLE);
                            RtmGetInstalledRtCnt (pRt, &u1RtCount);
                            pNotifiedRt = RtmGetNotifiedRt (pRt);
                            RtmAddRtToECMPPRTInCxt (pRtmCxt, pTmpRt);
                            if (u1RtCount == 0 && pNotifiedRt == NULL)
                            {
                                /* We have deleted the only resolved route 
                                 * installed in hardware.So add local route to 
                                 * receive packets to CPU */
                                u4NotifyHigherLayer = 1;
                                RtmHandleECMPRtInCxt (pRtmCxt, pRt, pRt,
                                                      NETIPV4_ADD_ROUTE,
                                                      RTM_DONT_SET_RT_REACHABLE);
                                u4NotifyHigherLayer = 0;
                            }
                            RtmNotifyNewECMPRouteInCxt (pRtmCxt, pRt,
                                                        pRt->i4Metric1);
                        }
                        else
                        {

                            /* Delete the route entry installed in Hardware with
                             * old ARP entry information */
                            pRt->u4Flag &= (UINT4) (~RTM_RT_REACHABLE);
                            u4NotifyHigherLayer = 1;
                            RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt, pRt,
                                                              u1NonEcmpRtCnt);
                            u1NonEcmpRtCnt = 0;
                            /* Add the route to data plane to receive further 
                             * packets to CPU */
                            RtmAddRouteToDataPlaneInCxt (pRtmCxt, pRt,
                                                         u1NonEcmpRtCnt);

                            RtmAddRtToECMPPRTInCxt (pRtmCxt, pRt);

                            u4NotifyHigherLayer = 0;
                        }
                        /* Update the resolved nexthop table */
                        i4RetVal =
                            RtmUpdateResolvedNHEntryInCxt (pRtmCxt,
                                                           pRt->u4NextHop,
                                                           RTM_DELETE_ROUTE);
                    }
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }
            /* No more best route entries are available with 
             * the next hop of deleted ARP entry.Stop scanning 
             * IP routing Table */
            if (i4RetVal == RTM_NO_RRT_WITH_THIS_NH)
            {
                ROUTE_TBL_UNLOCK ();
                return IP_SUCCESS;
            }
        }
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
    }

    if ((pNhEntry =
         RtmGetReachableNextHopInCxt (pRtmCxt, u4NextHopIpAddr)) == NULL)
    {
        ROUTE_TBL_UNLOCK ();
        return IP_SUCCESS;
    }

    TMO_SLL_Scan (&pNhEntry->routeEntryList, pRtCurr, tPRtEntry *)
    {
        if (pRtCurr != NULL && pRtCurr->pPendRt != NULL)
        {
            au4Indx[0] = IP_HTONL (pRtCurr->pPendRt->u4DestNet);
            au4Indx[1] = IP_HTONL (pRtCurr->pPendRt->u4DestMask);
            InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
            InParams.i1AppId = ALL_ROUTING_PROTOCOL;
            InParams.pRoot = pRtmCxt->pIpRtTblRoot;
            MEMSET (apAppSpecInfo, 0,
                    MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
            OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

            OutParams.Key.pKey = au1Key;
            MEMSET (OutParams.Key.pKey, 0, 2 * sizeof (UINT4));

            /* scan the route table with the NextHop and add */
            if (TrieSearchEntry (&(InParams), &(OutParams)) == TRIE_SUCCESS)
            {
                if (IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRt,
                                               RTM_GET_BST_WITH_PARAMS) ==
                    IP_SUCCESS)
                {
                    /* Delete the best route haiving given next hop */
                    pTmpRt = pRt;
                    while ((pTmpRt != NULL)
                           && (pTmpRt->i4Metric1 == pRt->i4Metric1))
                    {
                        if ((pTmpRt->u4Flag & RTM_RT_REACHABLE) &&
                            (pTmpRt->u4NextHop != 0))
                        {
                            if (pTmpRt->u4NextHop == u4NextHopIpAddr)
                            {
                                if (pTmpRt->u4Flag & RTM_ECMP_RT)
                                {
                                    /* ECMP Route */
                                    /* Deleting  the route */
                                    RtmHandleECMPRtInCxt (pRtmCxt,
                                                          pTmpRt, pRt,
                                                          NETIPV4_DELETE_ROUTE,
                                                          RTM_DONT_SET_RT_REACHABLE);
                                    pTmpRt->u4Flag |= RTM_ECMP_RT;
                                    pTmpRt->u4Flag &=
                                        (UINT4) (~RTM_RT_REACHABLE);
                                    RtmGetInstalledRtCnt (pRt, &u1RtCount);
                                    pNotifiedRt = RtmGetNotifiedRt (pRt);
                                    /* Atleast one reachable route exist in 
                                     * hardware. So add the route to ECMPPRT */
                                    RtmAddRtToECMPPRTInCxt (pRtmCxt, pTmpRt);
                                    if (u1RtCount == 0 && pNotifiedRt == NULL)
                                    {
                                        /* We have deleted the only resolved route 
                                         * installed in hardware.So add local route to 
                                         * receive packets to CPU */
                                        u4NotifyHigherLayer = 1;
                                        RtmHandleECMPRtInCxt (pRtmCxt, pRt, pRt,
                                                              NETIPV4_ADD_ROUTE,
                                                              RTM_DONT_SET_RT_REACHABLE);
                                        u4NotifyHigherLayer = 0;
                                    }

                                    RtmNotifyNewECMPRouteInCxt (pRtmCxt, pRt,
                                                                pRt->i4Metric1);
                                }
                                else
                                {
                                    u4NotifyHigherLayer = 1;
                                    /* Delete the route entry installed in Hardware with
                                     * old ARP entry information */
                                    pTmpRt->u4Flag &=
                                        (UINT4) (~RTM_RT_REACHABLE);
                                    RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt,
                                                                      pTmpRt,
                                                                      u1NonEcmpRtCnt);

                                    u1NonEcmpRtCnt = 0;
                                    /* Add the route to data plane to receive further 
                                     * packets to CPU */
                                    RtmAddRouteToDataPlaneInCxt (pRtmCxt,
                                                                 pTmpRt,
                                                                 u1NonEcmpRtCnt);
                                    u4NotifyHigherLayer = 0;

                                    RtmAddRtToECMPPRTInCxt (pRtmCxt, pTmpRt);

                                }

                                /* Update the resolved nexthop table */
                                i4RetVal =
                                    RtmUpdateResolvedNHEntryInCxt (pRtmCxt,
                                                                   pTmpRt->
                                                                   u4NextHop,
                                                                   RTM_DELETE_ROUTE);
                            }
                        }
                        pTmpRt = pTmpRt->pNextAlternatepath;
                    }

                    /* No more best route entries are available with the
                     * next hop of deleted ARP entry.Stop scanning IP routing 
                     * Table */
                    if (i4RetVal == RTM_NO_RRT_WITH_THIS_NH)
                    {
                        break;
                    }
                }
            }
        }
    }

    ROUTE_TBL_UNLOCK ();
    return IP_SUCCESS;
}

/*****************************************************************************/
/* Function     : RtmRBCompareRNh                                            */
/*                                                                           */
/* Description  : Compare the Next hop entries in Resolved next hop table    */
/*                                                                           */
/* Input        : e1        Pointer to Next hop Node node1                   */
/*                e2        Pointer to Next hop Node node2                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. RB_EQUAL if keys of both the elements are same.         */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. RB_GREATER if key of first element is greater than      */
/*                second element key                                         */
/*****************************************************************************/
INT4
RtmRBCompareRNh (tRBElem * e1, tRBElem * e2)
{
    tRNhNode           *pNhEntry1 = e1;
    tRNhNode           *pNhEntry2 = e2;

    if (pNhEntry1->pRtmCxt->u4ContextId > pNhEntry2->pRtmCxt->u4ContextId)
    {
        return RTM_RB_GREATER;
    }
    else if (pNhEntry1->pRtmCxt->u4ContextId < pNhEntry2->pRtmCxt->u4ContextId)
    {
        return RTM_RB_LESS;
    }
    /* Context ID is same, so validate the nexthop */
    if (pNhEntry1->u4NextHop > pNhEntry2->u4NextHop)
    {
        return RTM_RB_GREATER;
    }
    else if (pNhEntry1->u4NextHop < pNhEntry2->u4NextHop)
    {
        return RTM_RB_LESS;
    }
    return RTM_RB_EQUAL;
}

INT4
RtmRBCompareRNhForRoute (tRBElem * e1, tRBElem * e2)
{
    tPNhNode           *pNhEntry1 = e1;
    tPNhNode           *pNhEntry2 = e2;

    if (pNhEntry1->pRtmCxt->u4ContextId > pNhEntry2->pRtmCxt->u4ContextId)
    {
        return RTM_RB_GREATER;
    }
    else if (pNhEntry1->pRtmCxt->u4ContextId < pNhEntry2->pRtmCxt->u4ContextId)
    {
        return RTM_RB_LESS;
    }
    /* Context ID is same, so validate the nexthop */
    if (pNhEntry1->u4NextHop > pNhEntry2->u4NextHop)
    {
        return RTM_RB_GREATER;
    }
    else if (pNhEntry1->u4NextHop < pNhEntry2->u4NextHop)
    {
        return RTM_RB_LESS;
    }
    return RTM_RB_EQUAL;
}

/*****************************************************************************
 * Function     : RtmUpdateResolvedNHEntryInCxt                              *
 * Description  : Updates Resolved Next Hop Entry for the route count.       *
 *                This updation is done whenever a best route with a         *
 *                particular next hop becomes reachable                      *
 *                                                                           *
 * Input        : u4NextHop  - Next hop of the route being added/deleted     *
 *                to/from Linux IP/Kernel                                    *
 *                u1Operation - Indicated route is added/deleted             *
 *                pRtmCxt -RTM Context Pointer                               *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmUpdateResolvedNHEntryInCxt (tRtmCxt * pRtmCxt, UINT4 u4NextHop,
                               UINT1 u1Operation)
{
    tRNhNode           *pRNhEntry = NULL, ExstRNhNode;

    /* If the route is directly connected(Having next hop as zero,
     * Dont update RNHT  */
    if (u4NextHop == 0)
    {
        return IP_SUCCESS;
    }

    ExstRNhNode.u4NextHop = u4NextHop;
    ExstRNhNode.pRtmCxt = pRtmCxt;

    pRNhEntry = RBTreeGet (gRtmGlobalInfo.pRNHTRBRoot, &ExstRNhNode);

    if (u1Operation == RTM_ADD_ROUTE)
    {
        if (pRNhEntry == NULL)
        {
            if ((pRNhEntry = (tRNhNode *) MemAllocMemBlk
                 (gRtmGlobalInfo.RtmRNextHopPoolId)) == NULL)
            {
                return IP_FAILURE;
            }
            pRNhEntry->u4NextHop = u4NextHop;
            pRNhEntry->pRtmCxt = pRtmCxt;
            pRNhEntry->u4RouteCount = 1;

            /* Add the resolved next hop Entry to RNHT */
            if (RBTreeAdd (gRtmGlobalInfo.pRNHTRBRoot, pRNhEntry) == RB_FAILURE)
            {
                MemReleaseMemBlock (gRtmGlobalInfo.RtmRNextHopPoolId,
                                    (UINT1 *) pRNhEntry);
                return IP_FAILURE;
            }
        }
        else
        {
            (pRNhEntry->u4RouteCount)++;
        }
    }

    if (u1Operation == RTM_DELETE_ROUTE)
    {
        if (pRNhEntry == NULL)
        {
            return IP_FAILURE;
        }
        (pRNhEntry->u4RouteCount)--;

        if (pRNhEntry->u4RouteCount == 0)
        {
            /* Delete the resolved next hop Entry to RNHT */
            if (RBTreeRemove (gRtmGlobalInfo.pRNHTRBRoot,
                              pRNhEntry) == RB_FAILURE)
            {
                return IP_FAILURE;
            }
            MemReleaseMemBlock (gRtmGlobalInfo.RtmRNextHopPoolId,
                                (UINT1 *) pRNhEntry);
            return RTM_NO_RRT_WITH_THIS_NH;
        }
    }
    return IP_SUCCESS;
}

/************************************************************************/
/* Function           : RtmIsReachableNextHopInCxt                      */
/*                                                                      */
/* Input(s)           : u4NextHop - IP Address                          */
/*                      pRtmCxt -RTM Context Pointer                    */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : Returns TRUE if IP Address exists in Resolved   */
/*                      Next Hop Table Else FALSE                       */
/************************************************************************/
UINT1
RtmIsReachableNextHopInCxt (tRtmCxt * pRtmCxt, UINT4 u4NextHopAddr)
{
    tRNhNode           *pRNhEntry = NULL, ExstRNhNode;

    ExstRNhNode.u4NextHop = u4NextHopAddr;
    ExstRNhNode.pRtmCxt = pRtmCxt;

    pRNhEntry = RBTreeGet (gRtmGlobalInfo.pRNHTRBRoot, &ExstRNhNode);

    if (pRNhEntry == NULL)
    {
        return FALSE;
    }
    return TRUE;
}

/*****************************************************************************/
/* Function Name      : RtmRmEnqChkSumMsgToRm                                */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RTM_SUCCESS/RTM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
RtmRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef RM_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return RTM_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return RTM_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmResolveNextHop                                          *
 * Description  : Triggers ARP for the inputted next hop                     *
 * Input        : u4NextHop  - Next Hop Address to be resolved               *
 *                u4IfIndex  - Interface Index on which ARP should be sent   *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
PUBLIC INT4
RtmResolveNextHop (tPNhNode * pPNhNode, UINT4 u4IfIndex)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tPRtEntry          *pCurrEntry = NULL;
    tRtInfo            *pEcmpRt = NULL;
    tRtInfo            *pBestRt = NULL;
    tRtInfo            *pDropRt = NULL;
    tPRtEntry          *pTmpEntry = NULL;
    UINT4               u4NextHop = 0;
    INT4                i4RetVal = ARP_FAILURE;
    UINT2               u2Port;
    INT1                ai1MacAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1EncapType;

    u4NextHop = pPNhNode->u4NextHop;
    /* Check whether arp entry exist for the nexthop */
    i4RetVal = ArpResolveInCxt (u4NextHop, ai1MacAddr, &u1EncapType,
                                pPNhNode->pRtmCxt->u4ContextId);

    if (i4RetVal == ARP_FAILURE)
    {
        /* Arp entry is not available. Initiate arp request */
        if (u4IfIndex == IPIF_INVALID_INDEX)
        {
            /* Get the IfIndex to use for triggering ARP */
            MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
            MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

            RtQuery.u4DestinationIpAddress = u4NextHop;
            RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
            RtQuery.u2AppIds = (UINT2) ALL_ROUTING_PROTOCOL;
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
            RtQuery.u4ContextId = pPNhNode->pRtmCxt->u4ContextId;

            if (RtmUtilIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
            {

                return IP_FAILURE;
            }

            u2Port = (UINT2) NetIpRtInfo.u4RtIfIndx;
        }
        else
        {
            u2Port = (UINT2) u4IfIndex;
        }

        /* Arp Resolve failed, Send Arp request for maximum retries of count 5 
         * Delete drop route in hardware, if Arp request exceeded the count */
        /* Scan the route list , add all routes with this next hop to dataplane */
        UTL_SLL_OFFSET_SCAN (&(pPNhNode->routeEntryList),
                             pCurrEntry, pTmpEntry, tPRtEntry *)
        {
            pDropRt = pCurrEntry->pPendRt;
            if (pDropRt == NULL)
            {
                continue;
            }
            if (pDropRt->u4NextHop == u4NextHop)
            {
                if ((pDropRt->u4Flag) & (RTM_RT_DROP_ROUTE))
                {
                    pPNhNode->u4MaxReties++;
                    if (pPNhNode->u4MaxReties == ARP_MAX_RETRIES)
                    {
                        RtmDeleteRouteFromDataPlaneInCxt (pPNhNode->pRtmCxt,
                                                          pDropRt, 1);
                        pDropRt->u4Flag &= (UINT4) (~RTM_RT_DROP_ROUTE);
                        RtmAddRouteToDataPlaneInCxt (pPNhNode->pRtmCxt,
                                                     pDropRt, 1);
                        pPNhNode->u4MaxReties = 0;
                        return IP_SUCCESS;
                    }
                }
            }
        }
        /* Arp Resolve failed, Send Arp request for maximum retries of count 5
         * Delete drop route in hardware, if Arp request exceeded the count */

        /* Sending ARP request */
        ArpSendRequest (u2Port, u4NextHop, ARP_ENET_TYPE, CFA_ENCAP_ENETV2);
        return IP_SUCCESS;

    }
    /* Arp Entry exist. Add the routes from ECMPPRT with this nextop to 
     * dataplane */

    /* Scan the route list , add all routes with this next hop to dataplane */
    UTL_SLL_OFFSET_SCAN (&(pPNhNode->routeEntryList),
                         pCurrEntry, pTmpEntry, tPRtEntry *)
    {
        pEcmpRt = pCurrEntry->pPendRt;
        if (pEcmpRt == NULL)
        {
            continue;
        }

        RtmDeleteRtFromECMPPRTInCxt (pPNhNode->pRtmCxt, pEcmpRt);
        IpGetBestRouteEntryInCxt (pPNhNode->pRtmCxt, *pEcmpRt, &pBestRt);
        if (pBestRt == NULL)
        {
            continue;
        }
        u4NotifyHigherLayer = 1;
        RtmHandleECMPRtInCxt (pPNhNode->pRtmCxt, pEcmpRt, pBestRt,
                              NETIPV4_ADD_ROUTE, RTM_SET_RT_REACHABLE);
        RtmNotifyNewECMPRouteInCxt (pPNhNode->pRtmCxt, pBestRt,
                                    pEcmpRt->i4Metric1);
        u4NotifyHigherLayer = 0;
    }
    return IP_SUCCESS;
}
