/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved    

 * $Id: rtmtrie.c,v 1.133 2017/11/14 07:31:13 siva Exp $

 * Description:This file contains IP interface functions for updating/
 *             Querying the Trie.
 *******************************************************************/

#include "rtminc.h"
#include "rtmfrt.h"
#ifndef LNXIP4_WANTED
#include "ipinc.h"
#endif
#include "pingcli.h"

extern UINT1        gu1EcmpAcrossProtocol;
UINT4               u4NotifyHigherLayer = 0;
/**************************************************************************/
/*   Function Name   : RtmInitPreferenceInCxt                             */
/*   Description     : This function initializes the Protocol Preference  */
/*   Input(s)        : pRtmCxt -RtmContext pointer                        */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
RtmInitPreferenceInCxt (tRtmCxt * pRtmCxt)
{
    /* Initialize Protocol Preference */
    pRtmCxt->au1Preference[STATIC_RT_INDEX] = STATIC_DEFAULT_PREFERENCE;
    pRtmCxt->au1Preference[RIP_RT_INDEX] = RIP_DEFAULT_PREFERENCE;
    pRtmCxt->au1Preference[ISIS_RT_INDEX] = ISIS_DEFAULT_PREFERENCE;
    pRtmCxt->au1Preference[OSPF_RT_INDEX] = OSPF_DEFAULT_PREFERENCE;
    pRtmCxt->au1Preference[BGP_RT_INDEX] = BGP_DEFAULT_PREFERENCE;
}

/*-------------------------------------------------------------------+
 *    
 * Function           : RtmUtilIpv4LeakRoute
 *
 * Input(s)           : u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetRtInfo  - Route Information.
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS - Route added to trie, reachable, added to hw
 *                      IP_FAILURE - Route not added to trie
 *                        IP_SUCCESS_NOT_ADD_HW - Route not selected as best route/
 *                                                best route  not reachable but old
 *                                                best route reachable
 *                        IP_NO_ROOM.
 *
 * Action             : This function either add/modify/delete the route
 *                      from IP Trie and redistributes
 *                      the route to the registered routing protocols
 *                      This function is not protected by RTMv4 Lock.
 *                      Hence if a external module needs to update the routes 
 *                      in RTM, then use RtmIpv4LeakRoute function
 *         
+-------------------------------------------------------------------*/
INT4
RtmUtilIpv4LeakRoute (UINT1 u1CmdType, tNetIpv4RtInfo * pNetRtInfo)
{
    tRtInfo             RtInfo;
    tRtmFrtInfo        *pRtmFrtNode = NULL;
    INT4                i4RetVal = IP_FAILURE;
    INT4                i4ReturnVal = IP_FAILURE;
    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt (pNetRtInfo->u4ContextId);

    if (pRtmCxt == NULL)
    {
        return i4RetVal;
    }

    MEMSET (&RtInfo, 0, sizeof (tRtInfo));

    RtInfo.u4DestNet = pNetRtInfo->u4DestNet;

    RtInfo.u4DestMask = pNetRtInfo->u4DestMask;
    RtInfo.u4Tos = pNetRtInfo->u4Tos;
    RtInfo.u4NextHop = pNetRtInfo->u4NextHop;
    RtInfo.u4RtIfIndx = pNetRtInfo->u4RtIfIndx;
    RtInfo.u2RtType = pNetRtInfo->u2RtType;
    RtInfo.u2RtProto = pNetRtInfo->u2RtProto;
    RtInfo.u4RtAge = pNetRtInfo->u4RtAge;
    RtInfo.u4RtNxtHopAS = pNetRtInfo->u4RtNxtHopAs;
    RtInfo.i4Metric1 = pNetRtInfo->i4Metric1;
    RtInfo.u4RowStatus = pNetRtInfo->u4RowStatus;
    RtInfo.u4RouteTag = pNetRtInfo->u4RouteTag;
    RtInfo.u1BitMask = pNetRtInfo->u1BitMask;
    RtInfo.u1Preference = pNetRtInfo->u1Preference;
    RtInfo.i4MetricType = pNetRtInfo->u1MetricType;
    RtInfo.u1LeakRoute = pNetRtInfo->u1LeakRoute;

    if ((pNetRtInfo->u2ChgBit & IP_BIT_GR))
    {
        RtInfo.u4Flag |= RTM_GR_RT;
    }
    if ((RtInfo.u4DestNet & RtInfo.u4DestMask) != RtInfo.u4DestNet)
    {
        RtInfo.u4DestNet = (RtInfo.u4DestNet & RtInfo.u4DestMask);

    }

    switch (u1CmdType)
    {
        case RTM_ADD_ROUTE:
        {
            i4RetVal = IpForwardingTableAddRouteInCxt (pRtmCxt, &RtInfo);
            break;
        }
        case RTM_DELETE_ROUTE:
        {
            i4RetVal = IpForwardingTableDeleteRouteInCxt (pRtmCxt, &RtInfo);
            /* Delete the route entry if entry present
             * in NPAPI failed route entry table*/
            if (i4RetVal == RTM_SUCCESS)
            {
                pRtmFrtNode = RtmFrtGetInfo (&RtInfo, pNetRtInfo->u4ContextId);
                if (pRtmFrtNode != NULL)
                {
                    i4ReturnVal = RtmFrtDeleteEntry (pRtmFrtNode);
                    if (i4ReturnVal == RTM_SUCCESS)
                    {
                        pRtmCxt->u4FailedRoutes--;
                        RtmRedSyncFrtInfo (&RtInfo, pRtmCxt->u4ContextId, 0,
                                           RTM_DELETE_ROUTE);
                    }
                }
            }

            break;
        }
        case RTM_MODIFY_ROUTE:
        {
            i4RetVal = IpForwardingTableModifyRouteInCxt (pRtmCxt, &RtInfo);
            break;
        }
        default:
            return (IP_FAILURE);
    }

    if (i4RetVal == IP_SUCCESS)
    {
        return (IP_SUCCESS);
    }
    return i4RetVal;
}

/*******************************************************************************
    Function            :   IpForwardingTableAddRouteInCxt

    Description         :   This function is interface to add route to the
                            forwarding table. This function actually checks for
                            the route entry based on destination address, mask,
                            nexthop and TOS. If it is already present updates
                            the rowstatus and metric. If it is not there adds
                            the route entry in the forwarding table.                       

    Input parameters    :   pIpRoute : Route entry to be added.
                        :   pRtmCxt : Rtm Context Pointer               

    Output parameters   :   None.

    Global variables
    Affected            :   gIpRtTable.

    Returns            : IP_SUCCESS - Route added to trie, reachable, added to hw
                         IP_FAILURE - Route not added to trie
                          IP_SUCCESS_NOT_ADD_HW - Route not selected as best route/
                                                 best route  not reachable but old
                                                 best route reachable
                          IP_NO_ROOM.
*******************************************************************************/
INT4
IpForwardingTableAddRouteInCxt (tRtmCxt * pRtmCxt, tRtInfo * pIpRoute)
{
    tRtInfo            *pTmpRt = NULL;
    INT4                i4OutCome = IP_FAILURE;
    INT4                i4RetVal = IP_FAILURE;
    UINT1               u1Flag = IP_SUCCESS;

    /* For routes other than rejected and aggregated routes 
       the interface index should not be invalid */
#ifdef IP_WANTED
    if ((pIpRoute->u4RowStatus == IPFWD_ACTIVE) &&
        (pIpRoute->u2RtType != REJECT) &&
        (pIpRoute->u2RtProto != OTHERS_ID) &&
        (pIpRoute->u4RtIfIndx == IPIF_INVALID_INDEX))
    {
        return IP_FAILURE;
    }
#endif

    ROUTE_TBL_LOCK ();

    i4OutCome = RtmIsRouteExistInCxt (pRtmCxt, pIpRoute, &pTmpRt);

    if ((i4OutCome != IP_ROUTE_FOUND) || (pTmpRt == NULL))
    {

        /*Memory availablity to store routes are tested with the values of
         * gRtmGlobalInfo.u4MaxRTM<Proto>Route and gRtmGlobalInfo.u4<PROTO>Rts
         * gRtmGlobalInfo.u4MaxRTM<Proto>Route stores maximum number if routes allowed
         * gRtmGlobalInfo.u4<PROTO>Rts stores total number of routes installed */

        switch (pIpRoute->u2RtProto)
        {
            case BGP_ID:
                if (gRtmGlobalInfo.u4MaxRTMBgpRoute <
                    (gRtmGlobalInfo.u4BgpRts + 1))
                {
                    u1Flag = IP_NO_ROOM;
                }
                break;
            case OSPF_ID:
                if (gRtmGlobalInfo.u4MaxRTMOspfRoute <
                    (gRtmGlobalInfo.u4OspfRts + 1))
                {
                    u1Flag = IP_NO_ROOM;
                }
                break;
            case RIP_ID:
                if (gRtmGlobalInfo.u4MaxRTMRipRoute <
                    (gRtmGlobalInfo.u4RipRts + 1))
                {
                    u1Flag = IP_NO_ROOM;
                }
                break;
            case ISIS_ID:
                if (gRtmGlobalInfo.u4MaxRTMIsisRoute <
                    (gRtmGlobalInfo.u4IsisRts + 1))
                {
                    u1Flag = IP_NO_ROOM;
                }
                break;
            default:
                /*Static route addition is handled in Test routine IpCidrTblTestObjectInCxt */
                break;

        }

        if (u1Flag != IP_NO_ROOM)
        {
            i4RetVal = RtmAddRouteInFwdTblInCxt (pRtmCxt, pIpRoute);
        }
        ROUTE_TBL_UNLOCK ();
    }
    else
    {
        /* IpForwardingTableModifyRoute will take Lock.So Release it */
        ROUTE_TBL_UNLOCK ();
        /* Route entry is available, check for the matching route entry 
         * by traversing through all alternate paths. 
         */
        /* The allocated route entry will be useful only in the 
         * case route is not already existing. If the route is 
         * there already , we are no more going to use this new 
         * route entry . So we need to release it here.
         */

        if (((pTmpRt->u2RtProto == ISIS_ID) && (pIpRoute->u2RtProto == ISIS_ID))
            && ((pTmpRt->i4MetricType == IP_ISIS_LEVEL2) &&
                (pIpRoute->i4MetricType == IP_ISIS_LEVEL1)))
        {
            i4RetVal = IpForwardingTableDeleteRouteInCxt (pRtmCxt, pTmpRt);
            if (i4RetVal == IP_SUCCESS)
            {
                i4RetVal = IpForwardingTableAddRouteInCxt (pRtmCxt, pIpRoute);
            }
            return i4RetVal;
        }
        i4RetVal = IpForwardingTableModifyRouteInCxt (pRtmCxt, pIpRoute);

    }

    return i4RetVal;
}

/*****************************************************************************
    Function            :   RtmIsRouteExist

    Description         :   This function actually checks
                            for the route entry based on destination address,
                            mask, nexthop and TOS. If it is already there
                            returns IP_ROUTE_FOUND. If it is not
                            there returns IP_FAILURE.
    Input parameters    :   pIpRoute : Route entry to be modified.
                        :   pRtmCxt  : RTM context Pointer

    Output parameters   :   ppTmpRt - Pointer to the route entry pointer
                            which is already exists.

    Global variables
    Affected            :   Routing Table.

    Return value        :   IP_FAILURE/IP_ROUTE_FOUND

*******************************************************************************/
INT4
RtmIsRouteExistInCxt (tRtmCxt * pRtmCxt, tRtInfo * pIpRoute, tRtInfo ** ppTmpRt)
{
    INT4                i4OutCome = IP_FAILURE;
    UINT4               au4Indx[IP_TWO];
    tInputParams        InParams;
    tOutputParams       OutParams;

    MEMSET (&OutParams, 0, sizeof (tOutputParams));

    au4Indx[0] = IP_HTONL ((pIpRoute->u4DestNet) & (pIpRoute->u4DestMask));
    au4Indx[1] = IP_HTONL (pIpRoute->u4DestMask);

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = (INT1) (pIpRoute->u2RtProto - 1);
    InParams.Key.pKey = (UINT1 *) au4Indx;

    OutParams.Key.pKey = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        /* Route entry is available, check for the matching route entry 
         * based on nexthop and tos, by traversing through all 
         * alternate paths. */

        *ppTmpRt = (tRtInfo *) OutParams.pAppSpecInfo;

        /* Next-hop identifies the exact route */

        while (*ppTmpRt != NULL)
        {
            if ((*ppTmpRt)->u4NextHop == pIpRoute->u4NextHop)
            {
                i4OutCome = IP_ROUTE_FOUND;
                break;
            }
            *ppTmpRt = (*ppTmpRt)->pNextAlternatepath;
        }
    }

    return i4OutCome;
}

/*****************************************************************************
    Function            :   IpForwardingTableModifyRouteInCxt

    Description         :   This function is interface to modify the route in
                            the forwarding table. This function actually checks
                            for the route entry based on destination address,
                            mask, nexthop and TOS. If it is already there
                            updates the rowstatus and metric. If it is not
                            there returns IP_ROUTE_NOT_FOUND.
    Input parameters    :   pIpRoute : Route entry to be modified.
                        :   pRtmCxt  : RTM context Pointer

    Output parameters   :   None.

    Global variables
    Affected            :   Routing Table.

     Returns            : IP_SUCCESS - Route added to trie, reachable, added to hw
                          IP_FAILURE - Route not added to trie
                           IP_SUCCESS_NOT_ADD_HW - Route not selected as best route/
                                                 best route  not reachable but old
                                                 best route reachable
                           IP_NO_ROOM.

*******************************************************************************/
INT4
IpForwardingTableModifyRouteInCxt (tRtmCxt * pRtmCxt, tRtInfo * pIpRoute)
{
    tRtInfo            *pRt = NULL;
    tRtInfo            *pExstRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pOldBestRt = NULL;
    tRtInfo            *pNewBestRt = NULL;
    tRtInfo            *pNotifiedRt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    INT4                i4OutCome = IP_FAILURE;
    INT4                i4RetVal = IP_FAILURE;
    INT4                i4TmpRtStatus = 0;
    UINT4               au4Indx[IP_TWO];
    UINT4               u2ChgBit = 0;
    UINT2               u2BestRtCount = 0;
    UINT1               u1UpdateNeeded = 0;
    UINT1               u1Command = 0;
    UINT1               u1RtCount = 0;
    UINT1               u1OldBstRtCnt = 0;
    UINT1               u1Flag = IP_SUCCESS;

    /* For routes other than rejected and aggregated routes 
       the interface index should not be invalid */
#ifdef IP_WANTED
    if ((pIpRoute->u4RowStatus == IPFWD_ACTIVE) &&
        (pIpRoute->u2RtType != REJECT) &&
        (pIpRoute->u2RtProto != OTHERS_ID) &&
        (pIpRoute->u4RtIfIndx == IPIF_INVALID_INDEX))
    {
        return IP_FAILURE;
    }
#endif

    ROUTE_TBL_LOCK ();

    i4OutCome = RtmIsRouteExistInCxt (pRtmCxt, pIpRoute, &pExstRt);

    if ((i4OutCome != IP_ROUTE_FOUND) || (pExstRt == NULL))
    {

        /*Memory availablity to store routes are tested with the values of
         * gRtmGlobalInfo.u4MaxRTM<Proto>Route and gRtmGlobalInfo.u4<PROTO>Rts
         * gRtmGlobalInfo.u4MaxRTM<Proto>Route stores maximum number if routes allowed
         * gRtmGlobalInfo.u4<PROTO>Rts stores total number of routes installed */

        switch (pIpRoute->u2RtProto)
        {
            case BGP_ID:
                if (gRtmGlobalInfo.u4MaxRTMBgpRoute <
                    (gRtmGlobalInfo.u4BgpRts + 1))
                {
                    u1Flag = IP_NO_ROOM;
                }
                break;
            case OSPF_ID:
                if (gRtmGlobalInfo.u4MaxRTMOspfRoute <
                    (gRtmGlobalInfo.u4OspfRts + 1))
                {
                    u1Flag = IP_NO_ROOM;
                }
                break;
            case RIP_ID:
                if (gRtmGlobalInfo.u4MaxRTMRipRoute <
                    (gRtmGlobalInfo.u4RipRts + 1))
                {
                    u1Flag = IP_NO_ROOM;
                }
                break;
            case ISIS_ID:
                if (gRtmGlobalInfo.u4MaxRTMIsisRoute <
                    (gRtmGlobalInfo.u4IsisRts + 1))
                {
                    u1Flag = IP_NO_ROOM;
                }
                break;
            default:
                /*Static route addition is handled in Test routine IpCidrTblTestObjectInCxt */
                break;

        }

        if (u1Flag != IP_NO_ROOM)
        {
            i4RetVal = RtmAddRouteInFwdTblInCxt (pRtmCxt, pIpRoute);
        }
    }
    else
    {
        if (MALLOC_IP_ROUTE_ENTRY (pRt) == NULL)
        {
            ROUTE_TBL_UNLOCK ();
            return (IP_NO_ROOM);
        }

        MEMSET (pRt, 0, sizeof (tRtInfo));
        MEMCPY (pRt, pIpRoute, sizeof (tRtInfo));

        au4Indx[0] = IP_HTONL ((pIpRoute->u4DestNet) & (pIpRoute->u4DestMask));
        au4Indx[1] = IP_HTONL (pIpRoute->u4DestMask);

        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        InParams.i1AppId = (INT1) (pIpRoute->u2RtProto - 1);
        InParams.Key.pKey = (UINT1 *) au4Indx;

        OutParams.Key.pKey = NULL;

        /* Get the current Best route. */
        IpGetBestRouteEntryInCxt (pRtmCxt, *pRt, &pOldBestRt);
        if (pOldBestRt != NULL)
        {
            RtmGetBestRouteCount (pOldBestRt, pOldBestRt->i4Metric1,
                                  &u1OldBstRtCnt);
        }

        /* We are modifying the existing route */
        if (pRt->u4RtIfIndx != pExstRt->u4RtIfIndx)
        {
            /* interface is different */
            SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP_BIT_INTRFAC);
            pExstRt->u4RtIfIndx = pRt->u4RtIfIndx;

        }

        if (pRt->u1BitMask != pExstRt->u1BitMask)
        {
            (pRt->u1BitMask == 0) ? (pExstRt->u1BitMask &=
                                     (UINT1) (~RTM_PRIV_RT_MASK)) : (pExstRt->
                                                                     u1BitMask
                                                                     |=
                                                                     RTM_PRIV_RT_MASK);
            SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP_BIT_REDISCTRL);
        }

        if (pRt->u4Tos != pExstRt->u4Tos)
        {
            /* Tos is not matching */
            SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP_BIT_TOS);
            pExstRt->u4Tos = pRt->u4Tos;
        }

        if (pRt->u2RtType != pExstRt->u2RtType)
        {
            /* Route Type is not matching */
            SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP_BIT_RT_TYPE);
            pExstRt->u2RtType = pRt->u2RtType;
        }

        if (pRt->u4RtNxtHopAS != pExstRt->u4RtNxtHopAS)
        {
            /* Next-Hop AS is not matching */
            SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP_BIT_NH_AS);
            pExstRt->u4RtNxtHopAS = pRt->u4RtNxtHopAS;
        }

        if ((pExstRt->u4Flag & RTM_GR_RT) && (pExstRt->u2RtProto) == BGP_ID)
        {
            pExstRt->u4Flag &= (UINT4) (~RTM_GR_RT);
            TrieUpdate (&(InParams), pExstRt,
                        (UINT4) pExstRt->i4Metric1, &(OutParams));
        }
        if ((pExstRt->u4Flag & RTM_GR_RT) && (pExstRt->u2RtProto) == RIP_ID)
        {
            pExstRt->u4Flag &= (UINT4) (~RTM_GR_RT);
            TrieUpdate (&(InParams), pExstRt,
                        (UINT4) pExstRt->i4Metric1, &(OutParams));
        }

        if ((pExstRt->u4Flag & RTM_GR_RT) && (pExstRt->u2RtProto) == ISIS_ID)
        {
            pExstRt->u4Flag &= (UINT4) (~RTM_GR_RT);
            TrieUpdate (&(InParams), pExstRt,
                        (UINT4) pExstRt->i4Metric1, &(OutParams));
        }

        /* Set GR flag for the routes updated by routing protocol */
        if (pRt->u4Flag & RTM_GR_RT)
        {
            pExstRt->u4Flag |= RTM_GR_RT;
            TrieUpdate (&(InParams), pExstRt,
                        (UINT4) pExstRt->i4Metric1, &(OutParams));
        }

        if (pRt->i4Metric1 != pExstRt->i4Metric1)
        {
            /* metric is not matching */
            SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP_BIT_METRIC);
            pExstRt->i4Metric1 = pRt->i4Metric1;
            TrieUpdate (&(InParams), pExstRt, (UINT4) pRt->i4Metric1,
                        &(OutParams));

        }

        if (pRt->u4RowStatus != pExstRt->u4RowStatus)
        {
            pExstRt->u4RowStatus = pRt->u4RowStatus;
            SET_BIT_FOR_CHANGED_PARAM (u2ChgBit, IP_BIT_STATUS);
            if (pRt->u4RowStatus == IPFWD_ACTIVE)
            {
                u1UpdateNeeded = IP_NOT_READY_TO_ACTIVE;
                /* Current State is ACTIVE and previous Status is not-Active */
                TrieUpdate (&(InParams), pExstRt, (UINT4) pExstRt->i4Metric1,
                            &(OutParams));
            }
            else
            {
                /* Current State is Not ACTIVE but previous Status is Active */
                u1UpdateNeeded = IP_ACTIVE_TO_NOT_IN_SERVICE;
                TrieUpdate (&(InParams), pExstRt,
                            (UINT4) IP_PREFERENCE_INFINITY, &(OutParams));
            }
        }

        if ((u2ChgBit != 0)
            || ((pExstRt != NULL) &&
                (pExstRt->u1Preference != pRt->u1Preference)))
        {
            /* This Bitmask will be set only in the case any change 
             * is there for the route. So avoids the unnecessary 
             * notifications to the registered protocols */

            pExstRt->u1Preference = pRt->u1Preference;
            IpGetBestRouteEntryInCxt (pRtmCxt, *pRt, &pNewBestRt);
            pTmpRt = pNewBestRt;

            /* If a new route becomes the best route, check if the route is an
             * ECMP route and then set the ECMP route flag accordingly*/
            while ((pTmpRt != NULL)
                   && (pTmpRt->i4Metric1 == pNewBestRt->i4Metric1)
                   && (pTmpRt->u4RowStatus == IPFWD_ACTIVE))
            {
                if (((pTmpRt->u2RtProto == ISIS_ID)
                     && (pNewBestRt->u2RtProto == ISIS_ID)
                     && (pTmpRt->i4MetricType == pNewBestRt->i4MetricType))
                    || ((pTmpRt->u2RtProto != ISIS_ID)
                        && (pNewBestRt->u2RtProto != ISIS_ID)))
                {
                    u2BestRtCount++;
                }
                if (u2BestRtCount > 1)
                {
                    if ((pTmpRt->u4Flag & RTM_ECMP_RT) == 0)
                    {
                        pTmpRt->u4Flag |= RTM_ECMP_RT;
                        pRtmCxt->u4EcmpRoutes++;
                    }
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }
            if (u2BestRtCount > 1)
            {
                if ((pNewBestRt->u4Flag & RTM_ECMP_RT) == 0)
                {
                    pNewBestRt->u4Flag |= RTM_ECMP_RT;
                    pRtmCxt->u4EcmpRoutes++;
                }
            }

            if ((u2ChgBit & IP_BIT_REDISCTRL) == IP_BIT_REDISCTRL)
            {
                if (pExstRt->u4Flag & RTM_ECMP_RT)
                {
                    /* Handle ECMP routes for redistribution based on
                     * Private flag status */
                }
                else
                {
                    if (pExstRt->u1BitMask & RTM_PRIV_RT_MASK)
                    {
                        i4TmpRtStatus = (INT4) pExstRt->u4RowStatus;

                        pExstRt->u4RowStatus = IPFWD_DESTROY;

                        RtmHandleRouteUpdatesInCxt (pRtmCxt, pExstRt,
                                                    FALSE, IP_BIT_STATUS);

                        pExstRt->u4RowStatus = (UINT4) i4TmpRtStatus;
                    }
                    else
                    {
                        /* Update the redistributed route */
                        RtmHandleRouteUpdatesInCxt (pRtmCxt, pExstRt,
                                                    TRUE, IP_BIT_ALL);
                    }

                }
            }

            /* If modified route was not a best route, or if after modification
             * it didnt become a best route, exit*/
            if (u1UpdateNeeded == IP_ACTIVE_TO_NOT_IN_SERVICE)
            {
                /* If route was the old best route, delete the route and add 
                 * new best route */
                if (pOldBestRt != NULL)
                {
                    if (pOldBestRt->i4Metric1 == pExstRt->i4Metric1)
                    {
                        /* Route was old best route. So delete the same */
                        if (pExstRt->u4Flag & RTM_ECMP_RT)
                        {
                            if ((pExstRt->u4Flag & RTM_RT_REACHABLE) ||
                                (pExstRt->u4Flag & RTM_NOTIFIED_RT))
                            {
                                u1Command = RTM_DELETE_INSTALLED_RT;
                            }
                            else
                            {
                                u1Command = RTM_DONT_SET_RT_REACHABLE;
                            }

                            RtmHandleECMPRtInCxt (pRtmCxt, pExstRt, pNewBestRt,
                                                  NETIPV4_DELETE_ROUTE,
                                                  u1Command);

                            if ((pNewBestRt != NULL) && (u1OldBstRtCnt == 2))
                            {
                                /* Reset the ECMP flag if we have deleted 
                                 * one of the two available ECMP routes*/
                                pNewBestRt->u4Flag &= (UINT4) (~RTM_ECMP_RT);
                            }
                        }
                        else
                        {
                            RtmHandleBestRouteInCxt (pRtmCxt, pExstRt,
                                                     NETIPV4_DELETE_ROUTE);
                        }
                    }
                }
                if (pNewBestRt != NULL)
                {
                    RtmGetInstalledRtCnt (pNewBestRt, &u1RtCount);
                    if (u1RtCount > 0)
                    {
                        /* The new best route exist in h/w.So this route needs to 
                         * be notified.So it is handled seperately here */
                        RtmNotifyNewECMPRouteInCxt (pRtmCxt, pNewBestRt,
                                                    pNewBestRt->i4Metric1);
                    }

                    pNotifiedRt = RtmGetNotifiedRt (pNewBestRt);

                    if (pNewBestRt->u4Flag & RTM_ECMP_RT)
                    {
                        if (pNotifiedRt == NULL)
                        {
                            /* No Notified route is available.This may  
                             * happen when the new best route will have a 
                             * different metric or when a notified unreachable 
                             * route is getting deleted.So update the hardware
                             * for new Best route and notify one among them to
                             * applications */
                            RtmHandleECMPBestRoutesInCxt (pRtmCxt, pNewBestRt,
                                                          pNewBestRt->i4Metric1,
                                                          NETIPV4_ADD_ROUTE);
                        }
                    }
                    else
                    {
                        /* One of the two ECMP route is made not-in serive.So this
                         * new best route have been an ECMP route previously, but
                         * now it is a single route or a new best route is 
                         * identified */
                        if ((pNewBestRt->u4Flag & RTM_NOTIFIED_RT) == 0)
                        {
                            RtmHandleBestRouteInCxt (pRtmCxt, pNewBestRt,
                                                     NETIPV4_ADD_ROUTE);
                        }
                    }
                }
            }
            else if (u1UpdateNeeded == IP_NOT_READY_TO_ACTIVE)
            {
                if (pNewBestRt != NULL)
                {
                    /* Route is new best route */
                    if (pRt->i4Metric1 == pNewBestRt->i4Metric1)
                    {
                        if (pOldBestRt != NULL)
                        {
                            if (pOldBestRt->i4Metric1 != pNewBestRt->i4Metric1)
                            {
                                /* Route was not the best route.But now it 
                                 * is the best route. So delete the old 
                                 * best routes */
                                if (pOldBestRt->u4Flag & RTM_ECMP_RT)
                                {
                                    /* Old best route is an ECMP route.So 
                                     * delete all  ECMP routes installed 
                                     * in hardware.*/
                                    RtmHandleECMPBestRoutesInCxt (pRtmCxt,
                                                                  pOldBestRt,
                                                                  pOldBestRt->
                                                                  i4Metric1,
                                                                  NETIPV4_DELETE_ROUTE);
                                }
                                else
                                {
                                    RtmHandleBestRouteInCxt (pRtmCxt,
                                                             pOldBestRt,
                                                             NETIPV4_DELETE_ROUTE);
                                }
                            }
                        }

                        /* Add the new best route to dataplane */
                        if (pExstRt->u4Flag & RTM_ECMP_RT)
                        {
                            RtmHandleECMPRtInCxt (pRtmCxt, pExstRt, pNewBestRt,
                                                  NETIPV4_ADD_ROUTE,
                                                  RTM_DONT_SET_RT_REACHABLE);
                            RtmNotifyNewECMPRouteInCxt (pRtmCxt, pNewBestRt,
                                                        pNewBestRt->i4Metric1);
                        }
                        else
                        {
                            RtmHandleBestRouteInCxt (pRtmCxt, pExstRt,
                                                     NETIPV4_ADD_ROUTE);
                        }
                    }
                }
            }
            else if (((u2ChgBit & IP_BIT_METRIC) == IP_BIT_METRIC) &&
                     (pRt->u4RowStatus == IPFWD_ACTIVE))
            {
                if ((pNewBestRt != NULL) && (pOldBestRt != NULL))
                {
                    if (pOldBestRt != pNewBestRt)
                    {
                        /* Route was not the best route.But now it
                         * is the best route. So delete the old
                         * best routes */
                        if (pOldBestRt->u4Flag & RTM_ECMP_RT)
                        {
                            /* Old best route is an ECMP route.So
                             * delete all  ECMP routes installed
                             * in hardware.*/
                            RtmHandleECMPBestRoutesInCxt (pRtmCxt,
                                                          pOldBestRt,
                                                          pOldBestRt->
                                                          i4Metric1,
                                                          NETIPV4_DELETE_ROUTE);
                        }
                        else
                        {
                            RtmHandleBestRouteInCxt (pRtmCxt, pOldBestRt,
                                                     NETIPV4_DELETE_ROUTE);
                        }

                        /* Add the new best route to dataplane */
                        if ((pRt->i4Metric1 == pNewBestRt->i4Metric1) &&
                            (pExstRt->u4Flag & RTM_ECMP_RT))
                        {
                            RtmHandleECMPRtInCxt (pRtmCxt, pExstRt, pNewBestRt,
                                                  NETIPV4_ADD_ROUTE,
                                                  RTM_DONT_SET_RT_REACHABLE);
                            RtmNotifyNewECMPRouteInCxt (pRtmCxt, pNewBestRt,
                                                        pNewBestRt->i4Metric1);
                        }
                        else
                        {
                            RtmHandleBestRouteInCxt (pRtmCxt,
                                                     pNewBestRt,
                                                     NETIPV4_ADD_ROUTE);
                        }
                    }
                }
            }
        }

        /* This case exists when the route entry already present
         * and we made modifications to the existing route entry,
         * this allocated entry is used for only temperary allocation,
         * So need to be released.
         */
        if (!(pRt->u4Flag & RTM_RT_HWSTATUS))
        {
            i4RetVal = IP_SUCCESS_NOT_ADD_TO_HW;
        }
        else
        {
            i4RetVal = IP_SUCCESS;
        }
        IP_RT_FREE (pRt);
    }

    ROUTE_TBL_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************
   Function            :   RtmAddRouteInFwdTblInCxt

    Description         :   This function is interface to modify the route in
                            the forwarding table. This function actually checks
                            for the route entry based on destination address,
                            mask, nexthop and TOS. If it is already there
                            updates the rowstatus and metric. If it is not
                            there returns IP_ROUTE_NOT_FOUND.
    Input parameters    :   pIpRoute : Route entry to be modified.
                        :   pRtmCxt  : Rtm Context Pointer.

    Output parameters   :   None.

    Global variables
    Affected            :   Routing Table.

    Returns            : IP_SUCCESS - Route added to trie, reachable, added to hw
                         IP_FAILURE - Route not added to trie
                         IP_SUCCESS_NOT_ADD_HW - Route not selected as best route/
                                                best route not reachable but old
                                                best route reachable
                         IP_NO_ROOM.

*******************************************************************************/
INT4
RtmAddRouteInFwdTblInCxt (tRtmCxt * pRtmCxt, tRtInfo * pIpRoute)
{
    tRtInfo            *pRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pOldBestRt = NULL;
    tRtInfo            *pNewBestRt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];
    INT4                i4OutCome = IP_FAILURE;
    INT4                i4RetVal = IP_FAILURE;
    UINT2               u2BestRtCount = 0;
    UINT1               u1RtCanBest = FALSE;
    INT1                ai1MacAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1EncapType;

    if (MALLOC_IP_ROUTE_ENTRY (pRt) == NULL)
    {
        return (IP_NO_ROOM);
    }

    MEMSET (pRt, 0, sizeof (tRtInfo));
    MEMCPY (pRt, pIpRoute, sizeof (tRtInfo));

    au4Indx[0] = IP_HTONL ((pIpRoute->u4DestNet) & (pIpRoute->u4DestMask));
    au4Indx[1] = IP_HTONL (pIpRoute->u4DestMask);

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = (INT1) (pIpRoute->u2RtProto - 1);
    InParams.Key.pKey = (UINT1 *) au4Indx;
    OutParams.Key.pKey = NULL;

    IpGetBestRouteEntryInCxt (pRtmCxt, *pRt, &pOldBestRt);

    /* Route entry is not there, so add the new route */
    if (RTM_GET_NODE_STATUS () == RM_STANDBY)
    {
        RtmRedUpdateRtParams (pRt, pRtmCxt->u4ContextId);
    }

    i4OutCome = TrieAddEntry (&InParams, pRt, &OutParams);

    if (i4OutCome == TRIE_SUCCESS)
    {

        switch (pRt->u2RtProto)
        {
            case BGP_ID:
                gRtmGlobalInfo.u4BgpRts += 1;
                break;
            case OSPF_ID:
                gRtmGlobalInfo.u4OspfRts += 1;
                break;
            case RIP_ID:
                gRtmGlobalInfo.u4RipRts += 1;
                break;
            case ISIS_ID:
                gRtmGlobalInfo.u4IsisRts += 1;
                break;
            case CIDR_STATIC_ID:
                gRtmGlobalInfo.u4StaticRts += 1;
                break;
            default:
                break;
        }

        if (pRt->u2RtProto != CIDR_LOCAL_ID)
        {
            RtmAddNextHopForRoute (pRtmCxt, pRt);
        }

        pRtmCxt->u4IpFwdTblRouteNum++;
        gRtmGlobalInfo.u4IpFwdTblRts++;
        if ((pRt->u4RowStatus == IPFWD_ACTIVE) && (pRt->u2RtType != REJECT))
        {
            IpGetBestRouteEntryInCxt (pRtmCxt, *pRt, &pNewBestRt);

            /* Check if the added route is the new best route */
            if (pRt == pNewBestRt)
            {
                /* Get existing the Old best Route */
                if (pOldBestRt != NULL)
                {
                    if (pOldBestRt->u4Flag & RTM_ECMP_RT)
                    {
                        /* Delete all the Old best ECMP routes */
                        RtmHandleECMPBestRoutesInCxt (pRtmCxt, pOldBestRt,
                                                      pOldBestRt->i4Metric1,
                                                      NETIPV4_DELETE_ROUTE);
                    }
                    else
                    {
                        /* Delete the old best route */
                        RtmHandleBestRouteInCxt (pRtmCxt, pOldBestRt,
                                                 NETIPV4_DELETE_ROUTE);
                    }

                }
                u1RtCanBest = TRUE;
            }

            pTmpRt = pNewBestRt;
            /* Is added route is alternate having equal metric */
            while ((pTmpRt != NULL) &&
                   (pTmpRt->i4Metric1 == pNewBestRt->i4Metric1) &&
                   (pTmpRt->u4RowStatus == IPFWD_ACTIVE))
            {
                if (((pTmpRt->u2RtProto == ISIS_ID)
                     && (pNewBestRt->u2RtProto == ISIS_ID)
                     && (pTmpRt->i4MetricType == pNewBestRt->i4MetricType))
                    || ((pTmpRt->u2RtProto != ISIS_ID)
                        && (pNewBestRt->u2RtProto != ISIS_ID)))
                {
                    /* Route is alternate best */
                    if (pTmpRt == pRt)
                    {
                        u1RtCanBest = TRUE;
                        if (u2BestRtCount > 0)
                        {

                            if ((pTmpRt->u4Flag & RTM_ECMP_RT) == 0)
                            {
                                pTmpRt->u4Flag |= RTM_ECMP_RT;
                                pRtmCxt->u4EcmpRoutes++;
                            }
                        }
                    }
                    u2BestRtCount++;
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }

            if (u2BestRtCount > 1)
            {
                if ((pNewBestRt->u4Flag & RTM_ECMP_RT) == 0)
                {
                    pNewBestRt->u4Flag |= RTM_ECMP_RT;
                    pRtmCxt->u4EcmpRoutes++;
                }
                /* Now Best route is ECMP Route.if best route 
                 * not reachable then delete best 
                 * route from PRT List and add ECMP PRT List */
                if ((pNewBestRt->u4Flag & RTM_RT_REACHABLE) == 0)
                {
                    RtmDeleteRtFromPRTInCxt (pRtmCxt, pNewBestRt);
                    RtmAddRtToECMPPRTInCxt (pRtmCxt, pNewBestRt);
                }
            }

            /* Route coming for addition is the best route. */
            if (u1RtCanBest == TRUE)
            {
                if ((pRt->u4Flag & RTM_ECMP_RT) == 0)
                {
                    /* Route coming for addition is not an ECMP route.
                     * Route will be installed as local route to data plane
                     * irrespective of whether arp is resolved or not.*/

                    /* Unlock and Lock before calling ARP function, since a lock is taken 
                     * inside ArpResolve.
                     * As Counting semaphore is used here, unlock and lock should not 
                     * cause any problems. 
                     */
                    ARP_PROT_UNLOCK ();
                    i4RetVal =
                        ArpResolveInCxt (pNewBestRt->u4NextHop, ai1MacAddr,
                                         &u1EncapType, pRtmCxt->u4ContextId);
                    ARP_PROT_LOCK ();
                    if (i4RetVal == ARP_SUCCESS)
                    {
                        /* ARP is resolved already.
                         * so Setting the route as reachable */
                        pNewBestRt->u4Flag |= RTM_RT_REACHABLE;
                    }
                    else
                    {
                        /* ARP is not resolved then add route to PRT list */
                        RtmAddRtToPRTInCxt (pRtmCxt, pRt);
                    }
                    RtmHandleBestRouteInCxt (pRtmCxt, pRt, NETIPV4_ADD_ROUTE);
                }
                else
                {
                    /* Route coming for addition is an ECMP route */
                    RtmHandleECMPRtInCxt (pRtmCxt, pRt, pNewBestRt,
                                          NETIPV4_ADD_ROUTE,
                                          RTM_DONT_SET_RT_REACHABLE);
                    RtmNotifyNewECMPRouteInCxt (pRtmCxt, pNewBestRt,
                                                pRt->i4Metric1);
                }
            }
        }
        if (!(pRt->u4Flag & RTM_RT_HWSTATUS))
        {
            i4RetVal = IP_SUCCESS_NOT_ADD_TO_HW;
        }
        else
        {
            i4RetVal = IP_SUCCESS;
        }
    }
    else
    {
        /* Couldn't add the entry in the forwarding data base */
        i4RetVal = IP_FAILURE;
        IP_RT_FREE (pRt);
    }

    return i4RetVal;
}

/*******************************************************************************
    Function            :   RtmDelRtFromECMPPRTInCxt
    Description         :   This function is used to Delete the ECMP route from 
                the ECMPPRT List when this route is not available 
                in Trie Datastructure
    Input parameters    :   u4DestNet : Destination network.
                        :   u4DestMask : Destination Mask.
                        :   u4NextHop : Next Hop .
                        :   i4Metric1 : Metric value.
                        :   u2RtProto : Id through which the route is learnt
                        :   pRtmCxt  : Rtm Context Pointer.

    Output parameters   :   None.
    Global variables
    Affected            :   None.
    Return value        :   None.

*******************************************************************************/

VOID
RtmDelRtFromECMPPRTInCxt (tRtmCxt * pRtmCxt, UINT4 u4DestNet, UINT4 u4DestMask,
                          UINT4 u4NextHop, UINT4 i4Metric1, UINT2 u2RtProto)
{
    tPNhNode           *pNhEntry = NULL, ExstNhNode;
    tPRtEntry          *pRtCurr = NULL;
    tPRtEntry          *pTmpEntry = NULL;
    UNUSED_PARAM (i4Metric1);

    ExstNhNode.u4NextHop = u4NextHop;
    ExstNhNode.pRtmCxt = pRtmCxt;
    pNhEntry = RBTreeGet (gRtmGlobalInfo.pECMPRBRoot, &ExstNhNode);

    if (pNhEntry == NULL)
    {
        return;
    }

    UTL_SLL_OFFSET_SCAN (&(pNhEntry->routeEntryList), pRtCurr, pTmpEntry,
                         tPRtEntry *)
    {
        if ((pRtCurr->pPendRt != NULL) &&
            (pRtCurr->pPendRt->u4DestNet == u4DestNet) &&
            (pRtCurr->pPendRt->u4DestMask == u4DestMask) &&
            (pRtCurr->pPendRt->u4NextHop == u4NextHop) &&
            (pRtCurr->pPendRt->u2RtProto == u2RtProto))
        {
            RtmDeleteRtFromECMPPRTInCxt (pRtmCxt, pRtCurr->pPendRt);
        }
    }
    return;
}

/*******************************************************************************
    Function            :   RtmDelRtFromPRTInCxt
    Description         :   This function is used to Delete the best route from 
                the PRT List when this route is not available 
                in Trie Datastructure
    Input parameters    :   u4DestNet : Destination network.
                        :   u4DestMask : Destination Mask.
                        :   u4NextHop : Next Hop .
                        :   i4Metric1 : Metric value.
                        :   u2RtProto : Id through which the route is learnt
                        :   pRtmCxt  : Rtm Context Pointer.

    Output parameters   :   None.
    Global variables
    Affected            :   None.
    Return value        :   None.

*******************************************************************************/

VOID
RtmDelRtFromPRTInCxt (tRtmCxt * pRtmCxt, UINT4 u4DestNet, UINT4 u4DestMask,
                      UINT4 u4NextHop, UINT4 i4Metric1, UINT2 u2RtProto)
{
    tPNhNode           *pNhEntry = NULL, ExstNhNode;
    tPRtEntry          *pRtCurr = NULL;
    tPRtEntry          *pTmpEntry = NULL;
    UNUSED_PARAM (i4Metric1);

    ExstNhNode.u4NextHop = u4NextHop;
    ExstNhNode.pRtmCxt = pRtmCxt;
    pNhEntry = RBTreeGet (gRtmGlobalInfo.pPRTRBRoot, &ExstNhNode);

    if (pNhEntry == NULL)
    {
        return;
    }
    UTL_SLL_OFFSET_SCAN (&(pNhEntry->routeEntryList), pRtCurr, pTmpEntry,
                         tPRtEntry *)
    {
        if ((pRtCurr->pPendRt->u4DestNet == u4DestNet) &&
            (pRtCurr->pPendRt->u4DestMask == u4DestMask) &&
            (pRtCurr->pPendRt->u4NextHop == u4NextHop) &&
            (pRtCurr->pPendRt->u2RtProto == u2RtProto))
        {
            RtmDeleteRtFromPRTInCxt (pRtmCxt, pRtCurr->pPendRt);
        }
    }
    return;
}

/*******************************************************************************
    Function            :   IpForwardingTableDeleteRouteInCxt

    Description         :   This function is interface for the routing protocols
                            to delete the route in the forwarding table. This 
                            function actually checks for the route entry based 
                            on destination address, mask, nexthop and TOS. If it
                            is there deletes the route entry. 
                            If it is not there returns IP_ROUTE_NOT_FOUND.                       
    Input parameters    :   pIpRoute : Route entry to be modified.
                        :   pRtmCxt  : Rtm Context Pointer.

    Output parameters   :   None.

    Global variables
    Affected            :   gIpRtTable.

    Return value        :   IP_SUCCESS/IP_FAILURE/IP_ROUTE_NOT_FOUND.

*******************************************************************************/
INT4
IpForwardingTableDeleteRouteInCxt (tRtmCxt * pRtmCxt, tRtInfo * pIpRoute)
{
    INT4                i4OutCome = IP_SUCCESS;
    tRtInfo            *pRt = NULL;

    ROUTE_TBL_LOCK ();
    i4OutCome = RtmIsRouteExistInCxt (pRtmCxt, pIpRoute, &pRt);
    if ((i4OutCome == IP_ROUTE_FOUND)
        && (pRt->u4Tos == pIpRoute->u4Tos)
        && (pRt->u2RtProto == pIpRoute->u2RtProto))
    {
        /* checking the level for ISIS before deleting */
        if (((pIpRoute->u2RtProto == ISIS_ID) && (pRt->u2RtProto == ISIS_ID))
            && (pIpRoute->i4MetricType != pRt->i4MetricType))
        {
            ROUTE_TBL_UNLOCK ();
            return i4OutCome;
        }
        i4OutCome = RtmDeleteRouteFromFwdTblInCxt (pRtmCxt, pRt);
    }

    ROUTE_TBL_UNLOCK ();
    return i4OutCome;
}

/*******************************************************************************
    Function            :   RtmDeleteRouteFromFwdTblInCxt

    Description         :   This function is interface for the routing protocols
                            to delete the route in the forwarding table. This 
                            function actually checks for the route entry based 
                            on destination address, mask, nexthop and TOS. If it
                            is there deletes the route entry. 
                            If it is not there returns IP_ROUTE_NOT_FOUND.                       
    Input parameters    :   pIpRoute : Route entry to be modified.
                        :   pRtmCxt  : Rtm Context Pointer.

    Output parameters   :   None.

    Global variables
    Affected            :   gIpRtTable.

    Return value        :   IP_SUCCESS/IP_FAILURE/IP_ROUTE_NOT_FOUND.

*******************************************************************************/
INT4
RtmDeleteRouteFromFwdTblInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt)
{
    INT4                i4OutCome = IP_SUCCESS;
    UINT4               u4PrevRowStatus;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pBestRt = NULL;
    tRtInfo            *pNewBestRt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];
    UINT1               u1BestRtCount = 0;
    UINT1               u1EcmpRt = FALSE;
    UINT1               u1NewBestRtCnt = 0;
    UINT1               u1Flag = 0;
    UINT4               u4DestNet = pRt->u4DestNet;
    UINT4               u4DestMask = pRt->u4DestMask;
    UINT4               u4NextHop = pRt->u4NextHop;
    UINT4               u4Metric1 = (UINT4) pRt->i4Metric1;
    UINT2               u2RtProto = pRt->u2RtProto;
    UINT1               u1RtCount = 0;

    au4Indx[0] = IP_HTONL ((pRt->u4DestNet) & (pRt->u4DestMask));
    au4Indx[1] = IP_HTONL (pRt->u4DestMask);

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = (INT1) (pRt->u2RtProto - 1);
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;

    u4PrevRowStatus = pRt->u4RowStatus;
    /* Get the current best route. */
    IpGetBestRouteEntryInCxt (pRtmCxt, *pRt, &pBestRt);

    pTmpRt = pBestRt;
    RtmGetBestRouteCount (pBestRt, pRt->i4Metric1, &u1BestRtCount);

    /* Delete the entry from RED table if present */
    if (RTM_GET_NODE_STATUS () == RM_STANDBY)
    {
        RtmRedUpdateRtParams (pRt, pRtmCxt->u4ContextId);
    }

    if (u1BestRtCount > 1)
    {
        while ((pTmpRt != NULL) &&
               (pTmpRt->i4Metric1 == pBestRt->i4Metric1)
               && (pTmpRt->u4RowStatus == IPFWD_ACTIVE))
        {
            /* Route is alternate best */
            if (pTmpRt == pRt)
            {
                u1EcmpRt = TRUE;
                break;
            }
            pTmpRt = pTmpRt->pNextAlternatepath;
        }
    }

    i4OutCome = TrieDeleteEntry (&InParams, &OutParams, pRt->u4NextHop);

    if (i4OutCome == TRIE_SUCCESS)
    {

        switch (pRt->u2RtProto)
        {
            case BGP_ID:
                gRtmGlobalInfo.u4BgpRts -= 1;
                break;
            case OSPF_ID:
                gRtmGlobalInfo.u4OspfRts -= 1;
                break;
            case RIP_ID:
                gRtmGlobalInfo.u4RipRts -= 1;
                break;
            case ISIS_ID:
                gRtmGlobalInfo.u4IsisRts -= 1;
                break;
            case CIDR_STATIC_ID:
                gRtmGlobalInfo.u4StaticRts -= 1;
                break;
            default:
                break;
        }
        if (pRt->u2RtProto != CIDR_LOCAL_ID)
        {
            RtmDeleteNextHopforRoute (pRtmCxt, pRt);
        }

        /* Get the New best route if present. */
        IpGetBestRouteEntryInCxt (pRtmCxt, *pRt, &pNewBestRt);

        pRtmCxt->u4IpFwdTblRouteNum--;
        gRtmGlobalInfo.u4IpFwdTblRts--;
        /* Set the row-status to destroy and set the
         * bitmask to status change to notify
         */
        pRt->u4RowStatus = IPFWD_NOT_IN_SERVICE;
        /* Only if the deleted route's  pevious Row
         * status was active, there is a possibility for
         * change in best route */
        if (u4PrevRowStatus == IPFWD_ACTIVE)
        {
            /* Check whether the deleted route
             * was the best route */
            if ((pRt == pBestRt) || (u1EcmpRt == TRUE))
            {
                if (pRt->u4Flag & RTM_ECMP_RT)
                {
                    if ((pRt->u4Flag & RTM_RT_REACHABLE) ||
                        (pRt->u4Flag & RTM_NOTIFIED_RT))
                    {
                        u1Flag = RTM_DELETE_INSTALLED_RT;
                    }
                    else
                    {
                        u1Flag = RTM_DONT_SET_RT_REACHABLE;
                    }

                    /* One of the ECMP route is getting deleted */
                    RtmHandleECMPRtInCxt (pRtmCxt, pRt, pNewBestRt,
                                          NETIPV4_DELETE_ROUTE, u1Flag);
                    pRtmCxt->u4EcmpRoutes--;
                }
                else
                {
                    /* Delete the route from data plane */
                    RtmHandleBestRouteInCxt (pRtmCxt, pRt,
                                             NETIPV4_DELETE_ROUTE);
                    if (pRt->u4Flag & RTM_RT_IN_PRT)
                    {
                        RtmDeleteRtFromPRTInCxt (pRtmCxt, pRt);
                    }

                }

                if (pNewBestRt != NULL)
                {
                    /* Check if the new best route is ECMP with 
                     * different metric. If so set the ECMP route  
                     * flag for all the ECMP  routes*/
                    if (pNewBestRt->i4Metric1 != pRt->i4Metric1)
                    {
                        RtmGetBestRouteCount (pNewBestRt,
                                              pNewBestRt->i4Metric1,
                                              &u1NewBestRtCnt);
                        if (u1NewBestRtCnt > 1)
                        {
                            /* New Best route is an ECMP route */
                            pTmpRt = pNewBestRt;
                            while ((pTmpRt != NULL) &&
                                   (pTmpRt->i4Metric1 == pNewBestRt->i4Metric1))
                            {
                                if (((pTmpRt->u2RtProto == ISIS_ID)
                                     && (pNewBestRt->u2RtProto == ISIS_ID)
                                     && (pTmpRt->i4MetricType ==
                                         pNewBestRt->i4MetricType))
                                    || ((pTmpRt->u2RtProto != ISIS_ID)
                                        && (pNewBestRt->u2RtProto != ISIS_ID)))
                                {
                                    pTmpRt->u4Flag |= RTM_ECMP_RT;
                                }
                                pTmpRt = pTmpRt->pNextAlternatepath;
                            }
                            RtmHandleECMPBestRoutesInCxt (pRtmCxt, pNewBestRt,
                                                          pNewBestRt->i4Metric1,
                                                          NETIPV4_ADD_ROUTE);
                        }
                        else
                        {
                            /* New best route is not an ECMP route */
                            RtmHandleBestRouteInCxt (pRtmCxt, pNewBestRt,
                                                     NETIPV4_ADD_ROUTE);
                            if (i4OutCome != IP_ROUTE_NOT_FOUND)
                            {
                                RtmDelRtFromECMPPRTInCxt (pRtmCxt, u4DestNet,
                                                          u4DestMask, u4NextHop,
                                                          u4Metric1, u2RtProto);
                                RtmDelRtFromPRTInCxt (pRtmCxt, u4DestNet,
                                                      u4DestMask, u4NextHop,
                                                      u4Metric1, u2RtProto);
                            }
                            IP_RT_FREE (pRt);
                            return i4OutCome;
                        }
                    }
                    else
                    {
                        if (u1BestRtCount == IP_TWO)
                        {
                            /* We have deleted one of the two ECMP 
                             * routes.So reset the ECMP flag for 
                             * the best route */
                            pNewBestRt->u4Flag &= (UINT4) (~RTM_ECMP_RT);
                            if (((pNewBestRt->
                                  u4Flag & RTM_RT_REACHABLE) == 0)
                                &&
                                ((pNewBestRt->u4Flag & RTM_NOTIFIED_RT) == 0))
                            {
                                RtmHandleBestRouteInCxt (pRtmCxt, pNewBestRt,
                                                         NETIPV4_ADD_ROUTE);
                                if (i4OutCome != IP_ROUTE_NOT_FOUND)
                                {
                                    RtmDelRtFromECMPPRTInCxt (pRtmCxt,
                                                              u4DestNet,
                                                              u4DestMask,
                                                              u4NextHop,
                                                              u4Metric1,
                                                              u2RtProto);
                                    RtmDelRtFromPRTInCxt (pRtmCxt, u4DestNet,
                                                          u4DestMask, u4NextHop,
                                                          u4Metric1, u2RtProto);
                                }
                                IP_RT_FREE (pRt);
                                return i4OutCome;
                            }

                        }
                    }

                    /* New Best route is an ECMP route. 
                     * If the new route is already installed,
                     * dont add it again to dataplane*/
                    if (((pNewBestRt->u4Flag & RTM_RT_REACHABLE) == 0)
                        && ((pNewBestRt->u4Flag & RTM_NOTIFIED_RT) == 0))
                    {

                        RtmGetInstalledRtCnt (pRt, &u1RtCount);
                        if (u1RtCount == 0)
                        {

                            RtmHandleECMPRtInCxt (pRtmCxt, pNewBestRt,
                                                  pNewBestRt, NETIPV4_ADD_ROUTE,
                                                  RTM_DONT_SET_RT_REACHABLE);
                        }
                    }

                    RtmNotifyNewECMPRouteInCxt (pRtmCxt, pNewBestRt,
                                                pNewBestRt->i4Metric1);
                }
            }

            IP_RT_FREE (pRt);
            return i4OutCome;
        }
    }
    if (i4OutCome != IP_ROUTE_NOT_FOUND)
    {
        RtmDelRtFromECMPPRTInCxt (pRtmCxt, u4DestNet, u4DestMask, u4NextHop,
                                  u4Metric1, u2RtProto);
        RtmDelRtFromPRTInCxt (pRtmCxt, u4DestNet, u4DestMask, u4NextHop,
                              u4Metric1, u2RtProto);
    }
    IP_RT_FREE (pRt);
    return i4OutCome;
}

/*****************************************************************************
 * Function Name    :   RtmNotifyNewECMPRouteInCxt 
 *
 * Description      :   This function checks whether the  best route for the destnation
 *                      is already notified or not
 *
 * Inputs           :   pRt - Start pointer of ECMP Lists
 *                      i4Metric - Metric to be refered for ECMP List
 *                      pRtmCxt - RTM Context Pointer
 *
 * Return Value     :   TRUE - if the notified route is present
 *                      FALSE - if the notified route is not present in ECMP List
    *                                                                              
 *****************************************************************************/
VOID
RtmNotifyNewECMPRouteInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt, INT4 i4Metric)
{
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pNotifiedRt = NULL;
    tRtInfo            *pReachRt = NULL;

    UNUSED_PARAM (pTmpRt);
    UNUSED_PARAM (pNotifiedRt);
    UNUSED_PARAM (pReachRt);
    UNUSED_PARAM (pRtmCxt);
    UNUSED_PARAM (pRt);
    UNUSED_PARAM (i4Metric);
    /* Check whether any of the ECMP best route is already 
     * notified to applications */
    return;
    /*
       pTmpRt = pRt;
       while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == i4Metric))
       {
       if (pTmpRt->u4Flag & RTM_NOTIFIED_RT)
       {
       pNotifiedRt = pTmpRt;
       }
       if (pTmpRt->u4Flag & RTM_RT_REACHABLE)
       {
       if (pReachRt == NULL)
       {
       pReachRt = pTmpRt;
       }
       }
       pTmpRt = pTmpRt->pNextAlternatepath;
       }

       / There exists only one route.Notify that route 
       * to Higher layers/ 
       if (pNotifiedRt == NULL)
       {
       if (pReachRt != NULL)
       {
       pNotifiedRt = pReachRt;
       }
       else
       {
       pNotifiedRt = pRt;
       }

       if (pNotifiedRt != NULL)
       {
       RtmNotifyRtToAppsInCxt (pRtmCxt, pNotifiedRt, NETIPV4_ADD_ROUTE);
       }
       return;
       }

       / A Notified route already exists..But the new route for
       * addition is reachable.So notify the old route entry for
       * deletion and notify new reachable route /
       if ((pNotifiedRt != NULL) &&
       ((pNotifiedRt->u4Flag & RTM_RT_REACHABLE) == 0) && (pReachRt != NULL))
       {
       /Delete the notified unreachable route and
       * notify new reachable route identified /
       RtmNotifyRtToAppsInCxt (pRtmCxt, pNotifiedRt, NETIPV4_DELETE_ROUTE);
       RtmNotifyRtToAppsInCxt (pRtmCxt, pReachRt, NETIPV4_ADD_ROUTE);
       } */
}

 /*****************************************************************************
 * Function Name    :   RtmGetBestRouteCount 
 *
 * Description      :   Provides the number of best routes with the given metric
 *                      in the provided list
 *
 * Inputs           :   pRt - Start pointer of ECMP Lists
 *                      i4Metric - Metric to be refered for ECMP List
 *
 * Return Value     :  No of best routes in the ECMP List 
 *****************************************************************************/
VOID
RtmGetBestRouteCount (tRtInfo * pRt, INT4 i4Metric, UINT1 *u1RtCount)
{
    UINT1               u1ECMPRtCount = 0;
    tRtInfo            *pTmpRt = NULL;

    pTmpRt = pRt;
    while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == i4Metric))
    {
        u1ECMPRtCount++;
        pTmpRt = pTmpRt->pNextAlternatepath;
    }
    *u1RtCount = u1ECMPRtCount;
}

/*****************************************************************************
 * Function Name    :   RtmGetInstalledRtCnt 
 *
 * Description      :   Provides the number of resolved best routes installed in 
 *                      the hardware with the given metric for the given 
 *                      destination.
 *
 * Inputs           :   pRt - Route Pointer which contain destination and 
 *                            netmask information.
 *
 * Return Value     :  No of resolved best routes installed in the hardware 
 *****************************************************************************/
VOID
RtmGetInstalledRtCnt (tRtInfo * pRt, UINT1 *pu1RtCount)
{
    tRtInfo            *pTmpRt = NULL;
    UINT1               u1ECMPRtCount = 0;

    pTmpRt = pRt;
    while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pRt->i4Metric1))
    {
        if (pTmpRt->u4Flag & RTM_RT_REACHABLE)
        {
            u1ECMPRtCount++;
        }
        pTmpRt = pTmpRt->pNextAlternatepath;
    }
    *pu1RtCount = u1ECMPRtCount;
}

/*****************************************************************************
 * Function Name    :   RtmGetNotifiedRt 
 *
 * Description      :   Provides the notified route 
 *
 * Inputs           :   pRt - Route Pointer which contain destination and netmask
 *                            information.
 *
 * Return Value     :  Pointer to notified route entry 
 *
 *****************************************************************************/
tRtInfo            *
RtmGetNotifiedRt (tRtInfo * pBestRt)
{
    tRtInfo            *pTmpRt = NULL;

    pTmpRt = pBestRt;
    while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pBestRt->i4Metric1))
    {
        if (pTmpRt->u4Flag & RTM_NOTIFIED_RT)
        {
            return pTmpRt;
        }
        pTmpRt = pTmpRt->pNextAlternatepath;
    }

    return NULL;
}

/************************************************************************/
/* Function           : RtmHandleECMPRtInCxt                            */
/*                                                                      */
/* Input(s)           : pRt : ECMP route entryNone                      */
/*                      pBestRt : Best route for the destination        */
/*                      u1Operation : Specifies if it is route addition */
/*                      or route deletion                               */
/*                      u1RtReachable : Specify if the route for        */
/*                      addition is reachable or not.If deletion, it    */
/*                      specify if the route for deletion was reachable */
/*                      or notified.                                    */
/*                      pRtmCxt : RtmContext Pointer                    */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : This function processes the resolved ECMP best  */
/*                      rts.It notifies protocols registered about rt   */
/*                      change,install the route to NP/Kernel and update*/
/*                      Reachable next hop table                        */
/************************************************************************/
VOID
RtmHandleECMPRtInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt, tRtInfo * pBestRt,
                      UINT1 u1Operation, UINT1 u1Command)
{
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pNotifiedRt = NULL;
    UINT1               u1RtCount = 0;
    UINT1               u1EncapType;
    INT4                i4RetVal = ARP_FAILURE;
    INT1                ai1MacAddr[CFA_ENET_ADDR_LEN];

    /* Get the installed reachable route count and Notified route */
    pTmpRt = pBestRt;
    while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pBestRt->i4Metric1)
           && (pTmpRt->u4RowStatus == IPFWD_ACTIVE))
    {
        if (pTmpRt->u4Flag & RTM_RT_REACHABLE)
        {
            u1RtCount++;
        }
        if (pTmpRt->u4Flag & RTM_NOTIFIED_RT)
        {
            pNotifiedRt = pTmpRt;
        }
        pTmpRt = pTmpRt->pNextAlternatepath;
    }
    if (u1Command == RTM_SET_RT_REACHABLE)
    {
        if ((pRt->u4Flag & RTM_RT_REACHABLE) == 0)
        {
            /* Setting the route as reachable */
            pRt->u4Flag |= RTM_RT_REACHABLE;
            if (pRt->u4Flag & RTM_RT_DROP_ROUTE)
            {
                pRt->u4Flag &= (~RTM_RT_DROP_ROUTE);
            }
            u1RtCount++;
        }
    }
    else
    {
        /* Unlock and Lock before calling ARP function, since a lock is taken 
         * inside ArpResolve.
         * As Counting semaphore is used here, unlock and lock should not 
         * cause any problems. 
         */
        ARP_PROT_UNLOCK ();
        i4RetVal = ArpResolveInCxt (pRt->u4NextHop, ai1MacAddr, &u1EncapType,
                                    pRtmCxt->u4ContextId);

        ARP_PROT_LOCK ();
        if (i4RetVal == ARP_SUCCESS)
        {
            /* ARP is resolved already.
             * so Setting the route as reachable
             * and incrementing reachable route count.*/
            pRt->u4Flag |= RTM_RT_REACHABLE;
            if (pRt->u4Flag & RTM_RT_DROP_ROUTE)
            {
                pRt->u4Flag &= (~RTM_RT_DROP_ROUTE);
            }
            if ((u1Operation == NETIPV4_ADD_ROUTE)
                || (pRt->u2RtProto == STATIC_ID))
            {
                /*  Route count is added only when we call for ROUTE ADD .
                 *  During ROUTE DELETE , arp will be not there in STATIC Route ,
                 *  so we will increment the count accordingly
                 *  In Dynamic entry , we will be having PROPER route count
                 *  so we can avoid incrementing it again here */
                u1RtCount++;
            }
        }
    }
    if (u1Operation == NETIPV4_ADD_ROUTE)
    {
        /* Function that add the route to NP and kernal */

        RtmHandleECMPRtAdditionInCxt (pRtmCxt, pRt, pNotifiedRt, u1RtCount);
    }
    else
    {
        /* Function that delete the route from NP and kernal */
        RtmHandleECMPRtDeletionInCxt (pRtmCxt, pRt, u1RtCount, u1Command);
    }
}

/************************************************************************/
/* Function           : RtmHandleECMPRtAdditionInCxt                    */
/*                                                                      */
/* Input(s)           : pRt : Route to be added                         */
/*                      u1RtCount : Reachable installed route count     */
/*                      pRtmCxt : Reachable installed route count       */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : This function install the route to NP/Kernel    */
/*                      and update reachable next hop table             */
/************************************************************************/
VOID
RtmHandleECMPRtAdditionInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt,
                              tRtInfo * pNotifiedRt, UINT1 u1RtCount)
{
    if (pRt->u4Flag & RTM_RT_REACHABLE)
    {
        if (pRt->u4Flag & RTM_RT_IN_ECMPPRT)
        {
            /* Delete the route fromECMPPRT */
            RtmDeleteRtFromECMPPRTInCxt (pRtmCxt, pRt);
        }

        if (pNotifiedRt == NULL)
        {
            /* Adding the first resolved route to dataplane.So add all
             * ECMP routes to ECMPPRT for further resolution*/
            RtmAddAllEcmpRtToEcmpPrtInCxt (pRtmCxt, pRt);
        }
        else
        {
            if (u1RtCount == 1)
            {
                /* Route is installed as local route in dataplane.Delete the 
                 * local route from NP and Kernal and install the resolved 
                 * route to dataplane. Add the deleted route to ECMPPRT */
                RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt,
                                                  pNotifiedRt, (u1RtCount));
            }
        }
        RtmAddRouteToDataPlaneInCxt (pRtmCxt, pRt, (u1RtCount));

        /* Update Reachable next hop table */
        RtmUpdateResolvedNHEntryInCxt (pRtmCxt, pRt->u4NextHop, RTM_ADD_ROUTE);
    }
    else
    {
        if (pNotifiedRt == NULL)
        {
            /* No route is notified and installed in dataplane.Add route as
             * local route to get packets to CPU*/
            u4NotifyHigherLayer = 1;
            RtmAddRouteToDataPlaneInCxt (pRtmCxt, pRt, (UINT1) (u1RtCount + 1));
            u4NotifyHigherLayer = 0;
        }
        /* Add the route to ECMPPRT for nexthop resolution */
        RtmAddRtToECMPPRTInCxt (pRtmCxt, pRt);

    }
}

/************************************************************************/
/* Function           : RtmHandleECMPRtDeletionInCxt                    */
/*                                                                      */
/* Input(s)           : pRt : Route to be deleted                       */
/*                      u1RtCount : Reachable installed route count     */
/*                      pRtmCxt : RTM Context Pointer                   */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : This function delete the route from  NP/Kernel  */
/*                      and update reachable next hop table             */
/************************************************************************/
VOID
RtmHandleECMPRtDeletionInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt,
                              UINT1 u1RtCount, UINT1 u1Command)
{
    tRtmFrtInfo        *pRtmFrtNode = NULL;

    /* Forcefully give notification to h/w. No need to check flags for h/w intimation
     * flag checks are required only for clearning local database in control plane
     */
    /* Delete  the route from Hardware */
    if (pRt->u2RtProto != STATIC_ID)
    {
        if (u1Command == RTM_DELETE_INSTALLED_RT)
        {
            /* One of the Two ECMP routes is deleted in Trie.
             * Reachability will be gone for the Route Deleted.
             *So incrementing the RouteCount by one inorder to handle 
             *Route Deletion in hardware properly*/
            u1RtCount++;
        }
    }

    RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt, pRt, u1RtCount);
    /*Reverting back the value of RouteCount after NP call */
    if (pRt->u2RtProto != STATIC_ID)
    {
        u1RtCount--;
    }
    /* Delete if the entry is present in FRT RBTree */
    pRtmFrtNode = RtmFrtGetInfo (pRt, pRtmCxt->u4ContextId);
    if (pRtmFrtNode != NULL)
    {
        if (RtmFrtDeleteEntry (pRtmFrtNode) == RTM_SUCCESS)
        {
            pRtmCxt->u4FailedRoutes--;
            RtmRedSyncFrtInfo (pRt, pRtmCxt->u4ContextId, 0, RTM_DELETE_ROUTE);
        }
    }

    /* Delete the oute from PRT if present */
    if (pRt->u4Flag & RTM_RT_IN_PRT)
    {
        RtmDeleteRtFromPRTInCxt (pRtmCxt, pRt);
    }

    if (pRt->u4Flag & RTM_RT_IN_ECMPPRT)
    {
        RtmDeleteRtFromECMPPRTInCxt (pRtmCxt, pRt);
    }

    /* Reset the flags associated with the best route entry */
    pRt->u4Flag = 0;
}

/************************************************************************/
/* Function           : RtmNotifyRtToAppsInCxt                          */
/*                                                                      */
/* Input(s)           : pRt : Route to be Notified                      */
/*                      u1Operaton : Specify route deletion or addition */
/*                      pRtmCxt - RTM Context Pointer                   */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : This function notifies the apps for route       */
/*                      addition and deletion                           */
/************************************************************************/

VOID
RtmNotifyRtToAppsInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt, tRtInfo * pRt1,
                        UINT1 u1Operation)
{
    INT4                i4TmpRtStatus = 0;

    if (u1Operation == NETIPV4_ADD_ROUTE)
    {
        if (pRt == NULL)
        {
            return;
        }
        RtmHandleRouteUpdatesInCxt (pRtmCxt, pRt, TRUE, IP_BIT_ALL);
        pRt->u4Flag |= RTM_NOTIFIED_RT;

        /* Noify registered protocols about best route change */
#ifdef IP_WANTED
        IpRtChgNotifyInCxt (pRtmCxt->u4ContextId, pRt, IP_BIT_ALL);
#endif
        IpRtChngNotifyInCxt (pRtmCxt->u4ContextId, pRt, NULL,
                             NETIPV4_ADD_ROUTE);

    }
    else if (u1Operation == NETIPV4_DELETE_ROUTE)
    {
        if (pRt == NULL)
        {
            return;
        }

        if ((pRt->u4Flag & RTM_NOTIFIED_RT)
#ifdef RM_WANTED
            || ((RmGetNodePrevState () == RM_STANDBY)
                && (RmGetNodeState () == RM_ACTIVE))
#endif
            )
        {

            pRt->u4Flag &= (UINT4) (~RTM_NOTIFIED_RT);
            i4TmpRtStatus = (INT4) pRt->u4RowStatus;

            pRt->u4RowStatus = IPFWD_DESTROY;

            RtmHandleRouteUpdatesInCxt (pRtmCxt, pRt, FALSE, IP_BIT_STATUS);

#ifdef IP_WANTED
            IpRtChgNotifyInCxt (pRtmCxt->u4ContextId, pRt, IP_BIT_STATUS);
#endif
            IpRtChngNotifyInCxt (pRtmCxt->u4ContextId, pRt, NULL,
                                 NETIPV4_DELETE_ROUTE);

            pRt->u4RowStatus = (UINT4) i4TmpRtStatus;
        }
    }
    else
    {
        if ((pRt1 == NULL) || (pRt == NULL))
        {
            return;
        }

        if (pRt1->u4Flag & RTM_NOTIFIED_RT)
        {
            pRt1->u4Flag &= (UINT4) (~RTM_NOTIFIED_RT);
            i4TmpRtStatus = (INT4) pRt1->u4RowStatus;

            pRt1->u4RowStatus = IPFWD_DESTROY;

            RtmHandleRouteUpdatesInCxt (pRtmCxt, pRt1, FALSE, IP_BIT_STATUS);
            pRt1->u4RowStatus = (UINT4) i4TmpRtStatus;
        }

        RtmHandleRouteUpdatesInCxt (pRtmCxt, pRt, TRUE, IP_BIT_ALL);
        pRt->u4Flag |= RTM_NOTIFIED_RT;
#ifdef IP_WANTED
        IpRtChgNotifyInCxt (pRtmCxt->u4ContextId, pRt1, IP_BIT_STATUS);
        IpRtChgNotifyInCxt (pRtmCxt->u4ContextId, pRt, IP_BIT_ALL);
#endif
        IpRtChngNotifyInCxt (pRtmCxt->u4ContextId, pRt, pRt1,
                             NETIPV4_MODIFY_ROUTE);

    }
}

/*****************************************************************************
 * Function     : RtmHandleECMPBestRoutesInCxt                               *
 * Description  : Check for ECMP routes in the alternate path list.          *
 *                If ECMP is not supported,                                  *
 *                  If any of the reachable ECMP route exists,               *
 *                    Install that as best route                             *
 *                  Else                                                     *
 *                     Add all the ECMP Routes to PRT                        *
 *                 else                                                      *
 *                 For all the ECMP Routes                                   *
 *                     If route is reachable,                                *
 *                        Install the best route                             *
 *                     else                                                  *
 *                       Add/Delete the route to/from PRT                    * 
 *                                                                           * 
 * Input        : pRt  - Pointer to the altenate path list                   *
 *                i4Metric  - Metric of the Best route                       *
 *                u1Operation - Addition/Deletion to be performaed on ECMP   *
 *                routes                                                     *
 *                pRtmCxt - RTM Context Pointer                              *
 * Output       : None                                                       *
 * Returns      : None                                                       *
 *****************************************************************************/
VOID
RtmHandleECMPBestRoutesInCxt (tRtmCxt * pRtmCxt, tRtInfo * pBestRt,
                              INT4 i4Metric, UINT1 u1Operation)
{
    tRtInfo            *pTmpRt = NULL;

    pTmpRt = pBestRt;

    /* Verify any of the alternate best route with same metric 
     * is reachable */
    while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 <= i4Metric))
    {
        if (pTmpRt->i4Metric1 == i4Metric)
        {
            if (u1Operation == NETIPV4_ADD_ROUTE)
            {
                if (pTmpRt->u4RowStatus == IPFWD_ACTIVE)
                {
                    /* Install the best route identified */
                    RtmHandleECMPRtInCxt (pRtmCxt, pTmpRt, pBestRt,
                                          NETIPV4_ADD_ROUTE,
                                          RTM_DONT_SET_RT_REACHABLE);
                }
                RtmNotifyNewECMPRouteInCxt (pRtmCxt, pBestRt,
                                            pBestRt->i4Metric1);
            }
            else
            {
                RtmHandleECMPRtInCxt (pRtmCxt, pTmpRt, pBestRt,
                                      NETIPV4_DELETE_ROUTE,
                                      RTM_DONT_SET_RT_REACHABLE);
            }
        }
        pTmpRt = pTmpRt->pNextAlternatepath;
    }
}

/************************************************************************/
/* Function           : RtmHandleBestRouteInCxt                         */
/*                                                                      */
/* Input(s)           : pRtmCxt - Rtm Context Pointer                   */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/*                                                                      */
/* Action             : This function processes the best routes         */
/*                      It notifies protocols registed about route      */
/*                      change,install the route to NP/Kernel and update*/
/*                      Reachable next hop table                        */
/************************************************************************/
VOID
RtmHandleBestRouteInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt, UINT1 u1Operation)
{
    if (u1Operation == NETIPV4_ADD_ROUTE)
    {
        if (pRt->u4RtIfIndx == IPIF_INVALID_INDEX)
        {
            return;
        }

        /* Add the route to NP */
        if (RtmAddRouteToDataPlaneInCxt (pRtmCxt, pRt, 1) == IP_FAILURE)
        {

            return;
        }

        if (pRt->u4Flag & RTM_RT_REACHABLE)
        {
            /* Update Reachable next hop table */
            RtmUpdateResolvedNHEntryInCxt (pRtmCxt, pRt->u4NextHop,
                                           RTM_ADD_ROUTE);
        }
        else
        {
            /* pRt is not an ECMP route. the purpose of using this function
             * is to add in not reachable next hop table list for
             * nexthop resolution */
            RtmAddRtToECMPPRTInCxt (pRtmCxt, pRt);
        }

    }
    else
    {

        if (pRt->u4RtIfIndx == IPIF_INVALID_INDEX)
        {
            return;
        }

        /* Delete  the route from Hardware */
        if (RtmDeleteRouteFromDataPlaneInCxt (pRtmCxt, pRt, 1) == IP_FAILURE)
        {
            return;
        }

        if (pRt->u4Flag & RTM_RT_REACHABLE)
        {
            /* Update the resolved nexthop table */
            RtmUpdateResolvedNHEntryInCxt (pRtmCxt, pRt->u4NextHop,
                                           RTM_DELETE_ROUTE);
        }
        else
        {
            RtmDeleteRtFromPRTInCxt (pRtmCxt, pRt);
            RtmDeleteRtFromECMPPRTInCxt (pRtmCxt, pRt);
        }

        /* Reset the flags associated with the best route entry */
        pRt->u4Flag = 0;
    }
}

/*******************************************************************************
    Function            :   IpForwardingTableGetRouteInfoInCxt

    Description         :   This function is interface for the routing protocols
                            to get the routing information learnt from a 
                            particular protocol. This is done based on the 
                            protocol Id the calling function provides. If the 
                            protocol Id is -1, the function gives all the 
                            routes for that destination. 
    Input parameters    :   u4Dest   : Destination address for which the route 
                                       info need to be provided.
                            u4Mask   : Mask for the destination address.
                            i1ProtoId: Protocol Id.
                            u4ContextId: Rtm Context ID    

    Output parameters   :   pIpRoutes : Pointer to the array of route entries.
                                        The calling function should allocate 
                                        memory based on the protocol Id.

    Global variables
    Affected            :   None.

    Return value        :   IP_SUCCESS/IP_FAILURE/IP_ROUTE_NOT_FOUND.

*******************************************************************************/
INT4
IpForwardingTableGetRouteInfoInCxt (UINT4 u4ContextId, UINT4 u4Dest,
                                    UINT4 u4Mask, INT1 i1ProtoId,
                                    tRtInfo ** pIpRoutes)
{
    INT4                i4OutCome = IP_SUCCESS;
    UINT1               u1Index, u1Found = 0;
    UINT4               au4Indx[IP_TWO];
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *pRt = NULL;
    tRtInfo            *apAppSpecInfo[MAX_APPLICATIONS];
    tRtmCxt            *pRtmCxt = NULL;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if ((pRtmCxt == NULL) || (pIpRoutes == NULL))
    {
        return IP_FAILURE;
    }

    au4Indx[0] = IP_HTONL (u4Dest & u4Mask);
    au4Indx[1] = IP_HTONL (u4Mask);

    ROUTE_TBL_LOCK ();
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    if (i1ProtoId != -1)
    {
        InParams.i1AppId = (INT1) (i1ProtoId - 1);
    }
    else
    {
        InParams.i1AppId = i1ProtoId;
    }
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    OutParams.Key.pKey = NULL;
    MEMSET (apAppSpecInfo, 0, MAX_APPLICATIONS * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.pObject1 = NULL;
    OutParams.pObject2 = NULL;

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        /* route is present */
        if (i1ProtoId != -1)
        {
            pRt = ((tRtInfo *) OutParams.pAppSpecInfo);

            if (pRt != NULL)
            {
                MEMCPY ((*pIpRoutes), pRt, sizeof (tRtInfo));
                ROUTE_TBL_UNLOCK ();
                return IP_SUCCESS;
            }
            ROUTE_TBL_UNLOCK ();
            return IP_ROUTE_NOT_FOUND;
        }
        else
        {
            for (u1Index = 1; u1Index <= MAX_ROUTING_PROTOCOLS; u1Index++)
            {
                pRt = ((tRtInfo **) OutParams.pAppSpecInfo)[u1Index - 1];

                if (pRt != NULL)
                {
                    u1Found = 1;
                    MEMCPY (pIpRoutes[u1Index], pRt, sizeof (tRtInfo));
                }
            }
            if (u1Found == 1)
            {
                ROUTE_TBL_UNLOCK ();
                return IP_SUCCESS;
            }
            else
            {
                ROUTE_TBL_UNLOCK ();
                return (IP_ROUTE_NOT_FOUND);
            }
        }
    }
    ROUTE_TBL_UNLOCK ();
    return IP_ROUTE_NOT_FOUND;
}

/* After taking RouteTable Lock only,this function Should be called */
/*****************************************************************************
 *                                                                           *
 * Function           : IpGetBestRouteEntryInCxt                             *
 *                                                                           *
 * Input(s)           : IP address  & Mask in tRtInfo                        * 
 *                    : pRtmCxt - Rtm Context Pointer                        * 
 *                                                                           *
 * Output(s)          : Pointer to Next Route Entry (ppOutRtInfo)            *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           * 
 * Action :                                                                  *
 *   Fetches the best Route Entry For the given IpAddress & NetMask.         *
 *   The Values are to be set in InRtInfo, by the Calling Routing.           *
 *   If ECMP support is defined,a route with best params(first route in      *
 *   in alternate list) is returned.Else a best reachable route if exists is *
 *   is returned or return a route as done in ECMP case.                     *
 *****************************************************************************/

INT1
IpGetBestRouteEntryInCxt (tRtmCxt * pRtmCxt, tRtInfo InRtInfo,
                          tRtInfo ** ppOutRtInfo)
{
    tRtInfo            *pRt = NULL;
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tInputParams        InParams;
    tOutputParams       OutParams;
    INT1                i1RetVal = IP_FAILURE;
    INT4                i4OutCome = 0;
    UINT4               au4Indx[IP_TWO];    /* Address and Mask form the Key for
                                               Trie */

    *ppOutRtInfo = NULL;

    au4Indx[0] = IP_HTONL (InRtInfo.u4DestNet);
    au4Indx[1] = IP_HTONL (InRtInfo.u4DestMask);

    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;

    OutParams.Key.pKey = NULL;

    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));

    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    /* scan the route table */
    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_SUCCESS)
    {
        if (IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRt,
                                       RTM_GET_BST_WITH_PARAMS) == IP_SUCCESS)
        {
            pRt->u4Flag |= RTM_BEST_RT;
            *ppOutRtInfo = pRt;
            i1RetVal = IP_SUCCESS;
        }
    }
    return i1RetVal;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpGetNextRouteEntryInCxt                             *
 *                                                                           *
 * Input(s)           : InRtInfo - Information about the route whose next    *
 *                                 route needs to be returned.
 *                      i1AppId  - Id of Application whose route needs to be *
 *                                 returned.                                 *
 *                      pRtmCxt - pRtmCxt Context Pointer                    *
 *                                                                           *
 * Output(s)          : pOutRtInfo - Pointer to the matching route entry     *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           * 
 * Action :                                                                  *
 *   Fetches the next Route Entry For the given Input information.           *
 *****************************************************************************/

INT1
IpGetNextRouteEntryInCxt (tRtmCxt * pRtmCxt, tRtInfo InRtInfo,
                          INT1 i1AppId, tOutputParams * pOutParams)
{
    INT4                i4OutCome = 0;
    UINT4               au4Indx[IP_TWO];    /* Address and Mask form the Key for
                                               Trie */
    tInputParams        InParams;

    MEMSET (&InParams, 0, sizeof (tInputParams));
    au4Indx[0] = IP_HTONL (InRtInfo.u4DestNet);
    au4Indx[1] = IP_HTONL (InRtInfo.u4DestMask);

    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    if (i1AppId == -1)
        InParams.i1AppId = -1;
    else
        InParams.i1AppId = (INT1) (i1AppId - 1);

    ROUTE_TBL_LOCK ();
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;

    /* scan the route table */
    i4OutCome = TrieGetNextEntry (&(InParams), pOutParams);
    if (i4OutCome == TRIE_SUCCESS)
    {
        ROUTE_TBL_UNLOCK ();
        return IP_SUCCESS;
    }

    ROUTE_TBL_UNLOCK ();
    return IP_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 * Function           : IpGetNextBestRouteEntryInCxt                         *
 *                                                                           *
 * Input(s)           : IP address  & Mask in tRtInfo                        * 
 *                    : pRtmCxt -RTM Context Pointer                         * 
 *                                                                           *
 * Output(s)          : Pointer to Next Route Entry (ppOutRtInfo)            *
 *                                                                           *
 * Returns            : IP_SUCCESS / IP_FAILURE                              *
 *                                                                           * 
 * Action :                                                                  *
 *   Fetches the Next Best Route Entry For the given IpAddress & NetMask.    *
 *   The Values are to be set in InRtInfo, by the Calling Routing.           *
 *****************************************************************************/

INT1
IpGetNextBestRouteEntryInCxt (tRtmCxt * pRtmCxt, tRtInfo InRtInfo,
                              tRtInfo ** ppOutRtInfo)
{
    tRtInfo            *pRt = NULL;
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tInputParams        InParams;
    tOutputParams       OutParams;
    INT4                i4OutCome = 0;
    UINT4               au4Indx[IP_TWO];    /* Address and Mask form the Key for
                                               Trie */
    UINT4               au4OutIndx[IP_TWO];    /* Address and Mask form the Key for
                                               Trie */

    au4Indx[0] = IP_HTONL (InRtInfo.u4DestNet);
    au4Indx[1] = IP_HTONL (InRtInfo.u4DestMask);

    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;

    ROUTE_TBL_LOCK ();
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;

    au4OutIndx[0] = 0;
    au4OutIndx[1] = 0;
    OutParams.Key.pKey = (UINT1 *) &(au4OutIndx[0]);

    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));

    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;

    /* scan the route table */
    for (;;)
    {
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            break;
        }
        if (IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRt,
                                       RTM_GET_BST_WITH_PARAMS) == IP_SUCCESS)
        {
            *ppOutRtInfo = pRt;
            ROUTE_TBL_UNLOCK ();
            return IP_SUCCESS;
        }

        /* No Best Route in this entry. Get next entry. */
        au4Indx[0] = au4OutIndx[0];
        au4Indx[1] = au4OutIndx[1];

        au4OutIndx[0] = 0;
        au4OutIndx[1] = 0;
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * (sizeof (tRtInfo *)));
        OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
        pRt = NULL;
    }

    ROUTE_TBL_UNLOCK ();
    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpScanRouteTableForBestRouteInCxt
 *
 * Input(s)           : pInParams    - Information about the Input parameter
 *                                     used for scanning the Route Table
 *                      pAppSpecFunc - Call back function for processing
 *                                     Application specific information
 *                      u4MaxRouteCnt- Number of route entries to be scanned.
 *                                     If Set as 0, then need to scan the
 *                                     entire Routing Table.
 *                      pRtmCxt - RTM context Pointer                          
 *
 * Output(s)          : pOutParams   - Updated Information about the next
 *                                     route to be scanned.
 *
 * Returns            : None
 *
 * Action             : This function scans throught the Route Table for the
 *                      given number of route and the callback function is
 *                      called for each route entry. If the maximum entries
 *                      has been processed, the next route entry to be
 *                      processed is stored in the pOutParams structure.
 *
+-------------------------------------------------------------------*/
VOID
IpScanRouteTableForBestRouteInCxt (tRtmCxt * pRtmCxt,
                                   tInputParams * pInParams,
                                   INT4 (*pAppSpecFunc) (tRtInfo * pRtInfo,
                                                         VOID *pAppSpecData),
                                   UINT4 u4MaxRouteCnt,
                                   tOutputParams * pOutParams,
                                   VOID *pAppSpecData)
{
    tRtInfo            *apAppSpecInfo[MAX_ROUTING_PROTOCOLS];
    tRtInfo            *pRtInfo = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               u4RtCnt = 0;
    INT4                i4OutCome = 0;
    UINT1               u1KeySize = 0;
    UINT1               u1CheckRtCnt = TRUE;
    UINT1               au1Key[2 * sizeof (UINT4)];

    if (u4MaxRouteCnt == 0)
    {
        u1CheckRtCnt = FALSE;
    }
    else
    {
        u1CheckRtCnt = TRUE;
    }

    InParams.pRoot = pInParams->pRoot;
    InParams.i1AppId = pInParams->i1AppId;
    InParams.Key.pKey = pInParams->Key.pKey;
    u1KeySize = (IP_TWO * sizeof (UINT4));

    MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));
    OutParams.pAppSpecInfo = (VOID *) apAppSpecInfo;
    OutParams.Key.pKey = au1Key;
    MEMSET (OutParams.Key.pKey, 0, 2 * sizeof (UINT4));
    MEMCPY (OutParams.Key.pKey, InParams.Key.pKey, u1KeySize);

    i4OutCome = TrieSearchEntry (&(InParams), &(OutParams));

    if (i4OutCome == TRIE_FAILURE)
    {
        /* First entry is not present. Try and get the next entry if
         * present. */
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
    }
    while (i4OutCome == TRIE_SUCCESS)
    {
        if (IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRtInfo,
                                       RTM_GET_REACHABLE_BEST) == IP_SUCCESS)
        {
            /* If  reachable best route exists, notiy it to apps..
             * Else notify the existing best route */
            u4RtCnt++;
            /* Call back routine */
            pAppSpecFunc (pRtInfo, pAppSpecData);
        }

        /* update the index here */
        InParams.Key.pKey = OutParams.Key.pKey;
        MEMSET (apAppSpecInfo, 0, MAX_ROUTING_PROTOCOLS * sizeof (tRtInfo *));

        /* this operation should be stopped when get next fails as
         * get next failure represents normal termination.
         */
        i4OutCome = TrieGetNextEntry (&(InParams), &(OutParams));
        if (i4OutCome == TRIE_FAILURE)
        {
            /* No more route exist. */
            break;
        }

        /* If we have finished processing more than expected number of route
         * entries then return. */
        if ((u1CheckRtCnt == TRUE) && (u4RtCnt > u4MaxRouteCnt))
        {
            MEMCPY (pOutParams->Key.pKey, OutParams.Key.pKey, u1KeySize);
            return;
        }
    }

    /* No more route is present in the Route Table. Update the Input Param
     * to reflect this and return. */
    MEMSET (pOutParams->Key.pKey, 0, u1KeySize);
    return;
}

/*************************************************************************/
/* Function Name     :  IpFwdTblGetBestRouteInCxt                        */
/*                                                                       */
/* Description       :  This function Gets the best route that will be   */
/*                      used for forwarding. It uses the same routing    */
/*                      used by IpGetRoute function. So when you change  */
/*                      the routing algorithm, please change this        */
/*                      fuction also. Using IpGetRoute is not passible   */
/*                      here. With Load sharing enabled, we may view i   */
/*                      different route which was not chosen in the      */
/*                      forwarding path.                                 */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  pOutParams - Pointer to tOutputParams which has all the routes*/
/*                      available for a particular dest.                 */
/*                      u1QueryFlag - Flag indicates that reachable best */
/*                      is required or a best route is required          */
/*                pRtmCxt - RTM context Pointer                          */
/* Output(s)         :  Route Entry to be Displayed                      */
/*                                                                       */
/* Returns           :  IP_SUCCESS/IP_FAILURE                            */
/*************************************************************************/
INT4
IpFwdTblGetBestRouteInCxt (tRtmCxt * pRtmCxt, tOutputParams pOutParams,
                           tRtInfo ** pRt, UINT1 u1QueryFlag)
{
    INT1                i1DesiredRtIndex = RT_NOT_FOUND;
/*
    UINT1               u1OldPreference = 0;
    UINT1               u1PreferProto = 0;
    UINT1               u1NewPreference;
*/
    tRtInfo           **pRtPtclTbl = NULL;
    tRtInfo            *pStRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pBestRt = NULL;
    tRtInfo            *pRtOne = NULL;
    UINT1               u1Index;
    UINT1               u1MinPreference = IP_PREFERENCE_INFINITY;
    UINT1               u1Pref = IP_PREFERENCE_INFINITY;

    pRtPtclTbl = (tRtInfo **) pOutParams.pAppSpecInfo;

    /* Check if local interface */
    if ((pRtPtclTbl[LOCAL_RT_INDEX] != NULL) &&
        (pRtPtclTbl[LOCAL_RT_INDEX]->u4RowStatus == IPFWD_ACTIVE))
    {
        i1DesiredRtIndex = LOCAL_RT_INDEX;
    }

    /* If Local route is not available, we need to find the route either from
     * dynamic protocol or static entry. */

    if (i1DesiredRtIndex != LOCAL_RT_INDEX)
    {
        if (pRtPtclTbl[STATIC_RT_INDEX] != NULL)
        {
            pStRt = pRtPtclTbl[STATIC_RT_INDEX];

            /* If the first Route is not the Active Route, Scan 
             * the Alternate routes and identify any active route
             * if available */
            if (pStRt->u4RowStatus != IPFWD_ACTIVE)
            {
                while (pStRt->pNextAlternatepath != NULL)
                {
                    pStRt = pStRt->pNextAlternatepath;
                    if (pStRt->u4RowStatus == IPFWD_ACTIVE)
                    {
                        break;
                    }
                }
            }
        }

        /*******************************************************************/
        /* Best Route selection should be based on the following criteria. */
        /*                                                                 */
        /* 1. Select the Local route, if it exists.                        */
        /* 2. Select the Static route, if it is the only one route         */
        /* 3. If there is no Static route configured,                      */
        /*    select the protocol route with least protocol preference.    */
        /* 4. If there is a Static route along with Protocol routes        */
        /*    route selection is done as follows,                          */
        /*    (a). Select the Static route,                                */
        /*    if Static route metric value <= least Protocol preference    */
        /*                                    value in gaPreference array. */
        /*                                                                 */
        /*    (b). Select the protocol route with least preference,        */
        /*    if Static route metric value > the selected protocol         */
        /*                                   route preference.             */
        /*                                                                 */
        /*******************************************************************/

        for (u1Index = 0; u1Index < MAX_ROUTING_PROTOCOLS; u1Index++)
        {
            if (u1Index == STATIC_RT_INDEX)
            {                    /* skip static routes */
                continue;
            }

            pTmpRt = pRtPtclTbl[u1Index];
            while (pTmpRt != NULL)
            {
                if (pTmpRt->u4RowStatus == IPFWD_ACTIVE)
                {
                    if (pTmpRt->u1Preference != 0)
                    {
                        u1Pref = pTmpRt->u1Preference;
                    }
                    else
                    {
                        u1Pref = pRtmCxt->au1Preference[u1Index];
                    }

                    if (u1Pref < u1MinPreference)
                    {
                        u1MinPreference = u1Pref;
                        i1DesiredRtIndex = (INT1) u1Index;
                        pBestRt = pTmpRt;
                    }
                }                /*endif(pTmpRt->u4RowStatus == IPFWD_ACTIVE) */
                if ((pTmpRt != NULL) && (pTmpRt->pNextAlternatepath != NULL)
                    && (pTmpRt->u2RtProto == ISIS_ID))
                {
                    pRtOne = (tRtInfo *) pTmpRt->pNextAlternatepath;
                    if (pRtOne->u2RtProto == ISIS_ID)
                    {
                        if (pTmpRt->i4MetricType != pRtOne->i4MetricType)
                        {
                            if (((pTmpRt->i4MetricType == IP_ISIS_LEVEL1)
                                 && (pRtOne->i4MetricType ==
                                     IP_ISIS_INTER_AREA))
                                || ((pTmpRt->i4MetricType == IP_ISIS_LEVEL1)
                                    && (pRtOne->i4MetricType == IP_ISIS_LEVEL2))
                                || ((pTmpRt->i4MetricType == IP_ISIS_LEVEL2)
                                    && (pRtOne->i4MetricType ==
                                        IP_ISIS_INTER_AREA)))
                            {
                                pBestRt = pTmpRt;
                            }

                            else if (((pRtOne->i4MetricType == IP_ISIS_LEVEL1)
                                      && (pTmpRt->i4MetricType ==
                                          IP_ISIS_INTER_AREA))
                                     ||
                                     ((pRtOne->i4MetricType == IP_ISIS_LEVEL1)
                                      && (pTmpRt->i4MetricType ==
                                          IP_ISIS_LEVEL2))
                                     ||
                                     ((pRtOne->i4MetricType == IP_ISIS_LEVEL2)
                                      && (pTmpRt->i4MetricType ==
                                          IP_ISIS_INTER_AREA)))

                            {
                                pBestRt = pRtOne;
                            }
                        }
                    }
                }
                pTmpRt = pTmpRt->pNextAlternatepath;
            }                    /* endwhile (pTmpRt != NULL) */
        }                        /* endfor( u1Index =0; u1Index < MAX_ROUTING_PROTOCOLS; u1Index++) */

        if ((pStRt != NULL) && (pStRt->u4RowStatus == IPFWD_ACTIVE)
            && (pStRt->i4Metric1 <= u1MinPreference))
        {
            i1DesiredRtIndex = STATIC_RT_INDEX;
            pBestRt = pStRt;
        }

    }                            /* end of if(i1DesiredRtIndex != LOCAL_RT_INDEX) */

    if (i1DesiredRtIndex == RT_NOT_FOUND)
    {
        return IP_FAILURE;
    }
    else if (pBestRt != NULL)
    {
        *pRt = pBestRt;
    }
    else
    {
        *pRt = pRtPtclTbl[i1DesiredRtIndex];
    }

    /* If any route is having reachable next hop,then retun it. */
    if (u1QueryFlag == RTM_GET_REACHABLE_BEST)
    {
        pTmpRt = *pRt;
        while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == (*pRt)->i4Metric1))
        {
            if (pTmpRt->u4Flag & RTM_RT_REACHABLE)
            {
                *pRt = pTmpRt;
                break;
            }
            pTmpRt = pTmpRt->pNextAlternatepath;
        }
    }

    return IP_SUCCESS;
}

/************************************************************************
 * Function Name  : RtmCopyRouteInfoInCxt                               *
 * Description    :Copies the RouteInformation into NetIp Structure     *
 * Input        : pPt - Route From the Trie                             *
 *                pNetIpRtInfo - NetIpStructure to be filled            *
 *                u4ContextId -RtmContextID                             *
 * Output       : None                                                  *
 * Returns      : None                                                  *
 ************************************************************************/
VOID
RtmCopyRouteInfoInCxt (UINT4 u4ContextId, tRtInfo * pRt,
                       tNetIpv4RtInfo * pNetIpRtInfo)
{
    pNetIpRtInfo->u4ContextId = u4ContextId;
    pNetIpRtInfo->u4DestNet = pRt->u4DestNet;
    pNetIpRtInfo->u4DestMask = pRt->u4DestMask;
    pNetIpRtInfo->u4NextHop = pRt->u4NextHop;
    pNetIpRtInfo->u4RtIfIndx = pRt->u4RtIfIndx;
    pNetIpRtInfo->u4RouteTag = pRt->u4RouteTag;
    pNetIpRtInfo->i4Metric1 = pRt->i4Metric1;
    pNetIpRtInfo->u4RtAge = pRt->u4RtAge;
    pNetIpRtInfo->u4RtNxtHopAs = pRt->u4RtNxtHopAS;
    pNetIpRtInfo->u4RowStatus = pRt->u4RowStatus;
    pNetIpRtInfo->u4Tos = pRt->u4Tos;
    pNetIpRtInfo->u2RtType = pRt->u2RtType;
    pNetIpRtInfo->u2RtProto = pRt->u2RtProto;
    pNetIpRtInfo->u1BitMask = pRt->u1BitMask;
    pNetIpRtInfo->u1Preference = pRt->u1Preference;
    pNetIpRtInfo->u1LeakRoute = pRt->u1LeakRoute;
    /*gaPreference[(pRt->u2RtProto) - 1]; */
}

/*************************************************************************
 * Function Name  : RtmGetStaticRouteCount                                *
 * Description    : Returns the no of static route available              *
 * Input          : None                                                  *
 * Output         : None                                                  *
 * Returns        : None                                                  *
 **************************************************************************/
VOID
RtmGetStaticRouteCount (UINT4 *pu4StaticRtCount)
{
    *pu4StaticRtCount = gRtmGlobalInfo.u4StaticRts;
}

/**************************************************************************/
/*   Function Name   : RtmUtilProcessGRRouteCleanUp                       */
/*   Description     : This function will cleanup all the old routes      */
/*                     updated before graceful restart, and resets the    */
/*                     IP_GR_BIT updated during GR process.               */
/*   Input(s)        : pRegnID - Protocol Info whose routes needs to be   */
/*                     cleared                                            */
/*                     i1GRCompFlag - GR Process competion Flag           */
/*                     (IP_SUCCESS/IP_FAILURE)                            */
/*                     pRtmCxt - RTM Context Pointer                      */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
RtmUtilProcessGRRouteCleanUp (tRtmCxt * pRtmCxt, tRtmRegnId * pRegnId,
                              INT1 i1GRCompFlag)
{
    tAppSpecInfo       *pAppSpecInfo = NULL;
    tRtInfo            *pExstRt = NULL;
    tRtInfo            *pNextRt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[IP_TWO];
    UINT2               u2RegnId = RTM_INVALID_REGN_ID;
    tRtInfo            *pTmpRt = NULL;
    UINT4               u4RtCount = 0;
    UINT1               au1Key[2 * sizeof (UINT4)];

    if (RTM_APP_SPEC_INFO (pAppSpecInfo) == NULL)
    {
        return;
    }

    MEMSET (&InParams, 0, sizeof (tInputParams));
    MEMSET (&OutParams, 0, sizeof (tOutputParams));
    MEMSET (pAppSpecInfo, 0, sizeof (tAppSpecInfo));
    OutParams.pAppSpecInfo = (VOID *) pAppSpecInfo;
    OutParams.Key.pKey = au1Key;
    MEMSET (OutParams.Key.pKey, 0, 2 * sizeof (UINT4));

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        APP_INFO_FREE (pAppSpecInfo);
        return;
    }
    RtmStopGRTmrInCxt (pRtmCxt, pRegnId);

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = (INT1) u2RegnId;
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    while (TrieGetNextEntry (&InParams, &OutParams) == TRIE_SUCCESS)
    {

        pExstRt = (tRtInfo *) OutParams.pAppSpecInfo;
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        InParams.i1AppId = (INT1) u2RegnId;

        /* Update the key to get the next entry */
        InParams.Key.pKey = OutParams.Key.pKey;

        /* GR process completed. If the GR bit is set ,reset it on successful
           GR completion , else remove the 
           route entry as it is no longer valid. */

        /*Find the number of alternate paths */
        u4RtCount = 0;
        pTmpRt = pExstRt;
        while (pTmpRt != NULL)
        {
            u4RtCount++;
            pTmpRt = pTmpRt->pNextAlternatepath;
        }

        while ((pExstRt != NULL) && (u4RtCount > 0))
        {
            /* Existing route entry gets deleted(pExstRt),
             * if the GR bit is not set. Hence store the next 
             * alternate path route information pointer in pNextRt */
            pNextRt = pExstRt->pNextAlternatepath;
            if ((pExstRt->u4Flag & RTM_GR_RT) && (i1GRCompFlag == IP_SUCCESS))
            {
                if ((pExstRt->u2RtProto != BGP_ID) &&
                    (pExstRt->u2RtProto != RIP_ID))
                {
                    pExstRt->u4Flag &= (UINT4) (~RTM_GR_RT);
                    TrieUpdate (&(InParams), pExstRt,
                                (UINT4) pExstRt->i4Metric1, &(OutParams));
                }
                else
                {
                    /* Delete all the BGP stale routes on successul 
                     * exit 
                     */
                    RtmDeleteRouteFromFwdTblInCxt (pRtmCxt, pExstRt);
                }
                u4RtCount--;
            }
            else
            {
                /* Delete the stale routes only on failure exit */
                if (((pExstRt->u2RtProto != BGP_ID) &&
                     (pExstRt->u2RtProto != RIP_ID)) ||
                    ((i1GRCompFlag == IP_FAILURE) &&
                     (pExstRt->u4Flag & RTM_GR_RT)))
                {
                    RtmDeleteRouteFromFwdTblInCxt (pRtmCxt, pExstRt);
                }
                u4RtCount--;
            }
            pExstRt = pNextRt;
        }
        MEMSET (pAppSpecInfo, 0, sizeof (tAppSpecInfo));
        OutParams.pAppSpecInfo = (VOID *) pAppSpecInfo;
    }

    /* Mark the restarting state of the protocol as converged */
    pRtmCxt->aRtmRegnTable[u2RegnId].u1RestartState = RTM_IGP_CONVERGED;
    APP_INFO_FREE (pAppSpecInfo);
    return;
}

/*****************************************************************************
 * Function     : RtmAddRouteToDataPlaneInCxt                                *
 * Description  : Add route to Forwarding Plane                              *
 *                If there is a link between ARP table and IP table in the 
 *                hardware, then check for ARP entry is done.
 *                
 * Input        : pPt - Route to be Added to  Forward Path                   *
 *                pRtmCxt - RTM context Pointer                              *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmAddRouteToDataPlaneInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt, UINT1 u1RtCount)
{
    UINT4               u4CfaIfIndex = 0;
    UINT1               u1IpForward;

    if (NetIpv4GetIpForwardingInCxt (pRtmCxt->u4ContextId,
                                     &u1IpForward) == NETIPV4_FAILURE)
    {
        return IP_FAILURE;
    }
    else
    {
        /* Dont add the route to NP if IP forwarding is disabled */
        if (u1IpForward != IP_FORW_ENABLE)
        {
            return IP_SUCCESS;
        }
    }

    /* Local routes need not be added to the hardware.
     * It is taken care by the ARP table. */
    if (gu1FIBFlag != RTM_FIB_FULL)

    {
        /* Get the CFA Interface Index from Ip Port No */
        if (NetIpv4GetCfaIfIndexFromPort (pRt->u4RtIfIndx,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            return IP_FAILURE;
        }
        /* If the Port is OOB Port, and the routing over the *
         * mangement interface is wanted then program the    *
         * route in NP else  DontProgram in NP or Add to PRT */
        if ((CfaIsMgmtPort (u4CfaIfIndex) == TRUE) &&
            (CFA_OOB_MGMT_INTF_ROUTING == FALSE))
        {
            return IP_SUCCESS;
        }
        RtmUpdateRouteCount (pRtmCxt, pRt, RTM_RT_COUNT_INC);
        RtmAddRtToFIBInCxt (pRtmCxt, pRt, u4CfaIfIndex, u1RtCount);

#ifdef LNXIP4_WANTED
        RtmNetIpv4LeakRoute (NETIPV4_ADD_ROUTE, pRt);
#else
        /* To Create route in Linux kernel using Netlink socket, 
         * if DHCP is Linux IP and ISS is compiled for Fsip*/
#ifdef DHCP_LNXIP_WANTED
        if ((CfaIsMgmtPort (u4CfaIfIndex) == TRUE) &&
            (pRt->u2RtProto == OTHERS_ID))
        {
            RtmNetIpv4LeakRoute (NETIPV4_ADD_ROUTE, pRt);
        }
#endif
#endif
        /* Function that add the route to NP and kernal */
        if (u4NotifyHigherLayer == 0)
        {
            RtmNotifyRtToAppsInCxt (pRtmCxt, pRt, NULL, NETIPV4_ADD_ROUTE);
        }
    }

    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmDeleteRouteFromDataPlaneInCxt                           *
 * Description  : Checks for Valid ARP Entry for the NextHop Address         *
 *                If present Delete the Route from the FIB                     *
 *                Else Delete from PRT.                                      *
 * Input        : pRt - Pointer to the Route                                 *
 *                pRtmCxt - RTM context Pointer                              *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmDeleteRouteFromDataPlaneInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt,
                                  UINT1 u1RtCount)
{
    UINT4               u4CfaIfIndex = 0;
    /* Get the CFA Interface Index from Ip Port No */

    if (NetIpv4GetCfaIfIndexFromPort (pRt->u4RtIfIndx,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return IP_FAILURE;
    }

    RtmUpdateRouteCount (pRtmCxt, pRt, RTM_RT_COUNT_DEC);
    RtmDeleteRtFromFIBInCxt (pRtmCxt, pRt, u4CfaIfIndex, u1RtCount);

    /* Notify Apps regarding the route deletion */

#ifdef LNXIP4_WANTED
    RtmNetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, pRt);
#else
    /* To delete route created in Linux kernel using Netlink socket,
     * if DHCP is Linux IP and ISS is compiled for Fsip  */
#ifdef DHCP_LNXIP_WANTED
    if ((CfaIsMgmtPort (u4CfaIfIndex) == TRUE) && (pRt->u2RtProto == OTHERS_ID))
    {
        RtmNetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, pRt);
    }
#endif
#endif
    if (u4NotifyHigherLayer == 0)
    {
        RtmNotifyRtToAppsInCxt (pRtmCxt, pRt, NULL, NETIPV4_DELETE_ROUTE);
    }

    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmDeleteRtFromFIBInCxt                                    *
 * Description  : Delete route from Forwarding Plan                          *
 * Input        : pPt - Route to be deleted to  Forward Path                 *
 *                pRtmCxt - RTM context Pointer                              *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmDeleteRtFromFIBInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt,
                         UINT4 u4CfaIfIndex, UINT1 u1RtCount)
{

#ifdef NPAPI_WANTED
    INT4                i4FreeDefIpB4Del = 0;
    tFsNpNextHopInfo    nextHopInfo;
    tRtmFrtInfo        *pRtmFrtNode = NULL;

#ifdef MPLS_WANTED
    tGenU4Addr          DestNet;
    /* If Mpls route is already present for the route, no need to
     *  * * delete this route in h/w
     *   * * */

    MEMSET (&DestNet, 0, sizeof (tGenU4Addr));

    DestNet.Addr.u4Addr = pRt->u4DestNet;
    DestNet.u2AddrType = MPLS_IPV4_ADDR_TYPE;
    if (MplsIsFtnExist (&DestNet, pRt->u4DestMask) == TRUE)
    {
        pRt->u4Flag &= ~RTM_RT_HWSTATUS;
        return IP_SUCCESS;
    }

#endif

    pRtmFrtNode = RtmFrtGetInfo (pRt, pRtmCxt->u4ContextId);
    if (pRtmFrtNode != NULL)
    {
        if (RtmFrtDeleteEntry (pRtmFrtNode) == RTM_SUCCESS)
        {
            pRtmCxt->u4FailedRoutes--;
            RtmRedSyncFrtInfo (pRt, pRtmCxt->u4ContextId, 0, RTM_DELETE_ROUTE);
        }
    }

    if (RTM_IS_NP_PROGRAMMING_ALLOWED () != RTM_SUCCESS)
    {
        return IP_SUCCESS;
    }

    MEMSET (&nextHopInfo, 0, sizeof (nextHopInfo));

    nextHopInfo.u4NextHopGt = pRt->u4NextHop;
    nextHopInfo.u1RtCount = u1RtCount;
    nextHopInfo.u4IfIndex = u4CfaIfIndex;
    nextHopInfo.u4HwIntfId[0] = pRt->u4HwIntfId[0];
#ifdef CFA_WANTED
    /* Remove the Entry created in the DLF Hash Table */

    IpFsCfaHwRemoveIpNetRcvdDlfInHash (pRtmCxt->u4ContextId, pRt->u4DestNet,
                                       pRt->u4DestMask);
#endif
    /* u4Flag is set to RTM_RT_HWSTATUS before deleting the route 
     * from the hardware & it is synced with the standby node.
     * After the route is deleted from the hardware i.e, when 
     * FsNpIpv4UcDelRoute is SUCCESS, the u4Flag is reset 
     * and it is synced again. This is done for optimized hardware audit */

    if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
    {
        pRt->u4Flag |= RTM_RT_HWSTATUS;
        RtmRedAddDynamicInfo (pRt, pRtmCxt->u4ContextId);
        RtmRedSendDynamicInfo (NETIPV4_DELETE_ROUTE);
    }
    if (pRt->u4Flag & RTM_NOTIFIED_RT)
    {
        if (IpFsNpIpv4UcDelRoute (pRtmCxt->u4ContextId,
                                  pRt->u4DestNet, pRt->u4DestMask,
                                  nextHopInfo,
                                  &i4FreeDefIpB4Del) == FNP_FAILURE)
        {
            /* Delete route from hardware failed */
            return IP_FAILURE;
        }
    }
    if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
    {
        pRt->u4Flag &= (UINT4) ~RTM_RT_HWSTATUS;
        RtmRedAddDynamicInfo (pRt, pRtmCxt->u4ContextId);
        RtmRedSendDynamicInfo (NETIPV4_DELETE_ROUTE);
    }

    /* The HwStatus flag is used only when Async NP Call is used. 
     * The HwStatus is set as NotPresent at first and when the NP_CALL is
     * success, then the flag is updated to RTM_RT_HWSTATUS. 
     * This state is synced with the standby node through Dynamic syncup */

    if (i4FreeDefIpB4Del != 0)
    {
        /* Update the global Flag */
        gu1FIBFlag = RTM_FIB_NOT_FULL;
    }
#else
    pRt->u4Flag &= (UINT4) (~RTM_RT_HWSTATUS);
#endif
    UNUSED_PARAM (pRtmCxt);
    UNUSED_PARAM (pRt);
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u1RtCount);

    return IP_SUCCESS;
}

/*****************************************************************************
 * Function     : RtmAddRtToFIBInCxt                                         *
 * Description  : Add route to Forwarding Plane                              *
 * Input        : pPt - Route to be Added to  Forward Path                   *
 *                u4CfaIfIndex - CFA interface Index                         *
 *                u2RtCount    - No of routes for the destination            *
 *                pRtmCxt - RTM context Pointer                              *
 * Output       : None                                                       *
 * Returns      : IP_SUCCESS/IP_FAILURE                                      *
 *****************************************************************************/
INT4
RtmAddRtToFIBInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt, UINT4 u4CfaIfIndex,
                    UINT1 u1RtCount)
{

#ifdef NPAPI_WANTED
    tFsNpNextHopInfo    nextHopInfo;
    tCfaIfInfo          IfInfo;
#ifdef MPLS_WANTED
    tGenU4Addr          DestNet;
#endif
    UINT4               u4MaskBits = 0;
    UINT4               u4ContextId;
    UINT1               au1NetAddress[MAX_ADDR_LEN];
    UINT1               bu1TblFull = FNP_FALSE;
    CHR1               *pu1NetAddress = NULL;
    MEMSET (au1NetAddress, 0, MAX_ADDR_LEN);
    CLI_CONVERT_IPADDR_TO_STR (pu1NetAddress, pRt->u4DestNet);
    u4MaskBits = CliGetMaskBits (pRt->u4DestMask);
#ifdef MPLS_WANTED
    /* If Mpls route is already present for the route, no need to 
     * add this route in h/w
     * */

    MEMSET (&DestNet, 0, sizeof (tGenU4Addr));

    DestNet.Addr.u4Addr = pRt->u4DestNet;
    DestNet.u2AddrType = MPLS_IPV4_ADDR_TYPE;
    if (MplsIsFtnExist (&DestNet, pRt->u4DestMask) == TRUE)
    {
        pRt->u4Flag |= RTM_RT_HWSTATUS;
        return IP_SUCCESS;
    }
#endif
    if (RTM_IS_NP_PROGRAMMING_ALLOWED () != RTM_SUCCESS)
    {
        return IP_SUCCESS;
    }

    if (VcmGetContextIdFromCfaIfIndex (u4CfaIfIndex, &u4ContextId) ==
        VCM_FAILURE)
    {
        return IP_SUCCESS;
    }

    MEMSET (&nextHopInfo, 0, sizeof (tFsNpNextHopInfo));

    nextHopInfo.u4ContextId = u4ContextId;
    nextHopInfo.u4NextHopGt = pRt->u4NextHop;
    nextHopInfo.u4IfIndex = u4CfaIfIndex;

    if (((pRt->u4Flag & RTM_RT_REACHABLE) == 0) &&
        ((pRt->u4Flag & RTM_RT_IN_PRT) == 0))
    {
        nextHopInfo.u1RtType = FNP_RT_LOCAL;
    }
    /* Check for drop route */
    if ((pRt->u4Flag & RTM_RT_DROP_ROUTE) == RTM_RT_DROP_ROUTE)
    {
        nextHopInfo.u1RtType = FNP_IP_DROP;
    }

    nextHopInfo.u1RtCount = u1RtCount;

    if (CfaGetIfInfo (nextHopInfo.u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return IP_FAILURE;
    }

    if (CFA_PPP == IfInfo.u1IfType)
    {
        nextHopInfo.u1IfType = CFA_PPP;
        CfaGetLLIfIndex (u4CfaIfIndex, &nextHopInfo.u4LLIfIndex);
    }

    if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
        (CfaGetVlanId (nextHopInfo.u4IfIndex,
                       &(nextHopInfo.u2VlanId)) == CFA_FAILURE))
    {
        return IP_FAILURE;
    }

#ifdef CFA_WANTED
    /* Remove the Entry created in the DLF Hash Table */
    IpFsCfaHwRemoveIpNetRcvdDlfInHash (pRtmCxt->u4ContextId, pRt->u4DestNet,
                                       pRt->u4DestMask);
    IpFsCfaHwRemoveIpNetRcvdDlfInHash (pRtmCxt->u4ContextId, pRt->u4NextHop,
                                       0xffffffff);
#endif

    if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
    {
        pRt->u4Flag &= (UINT4) ~RTM_RT_HWSTATUS;
        RtmRedAddDynamicInfo (pRt, pRtmCxt->u4ContextId);
        RtmRedSendDynamicInfo (NETIPV4_ADD_ROUTE);
    }
    if (IpFsNpIpv4UcAddRoute
        (pRtmCxt->u4ContextId, pRt->u4DestNet, pRt->u4DestMask,
#ifndef NPSIM_WANTED
         &nextHopInfo,
#else
         nextHopInfo,
#endif
         &bu1TblFull) == FNP_FAILURE)
    {
        /* Add NPAPI failed route programming to Failed route table */
        if ((RtmFrtAddInfo (pRt, pRtmCxt->u4ContextId, u1RtCount)) ==
            RTM_SUCCESS)
        {
            pRtmCxt->u4FailedRoutes++;
            RtmRedSyncFrtInfo (pRt, pRtmCxt->u4ContextId, u1RtCount,
                               RTM_ADD_ROUTE);
        }
        else
        {
            UtlTrcLog (1, 1, "", "ERROR[RTM]: IPV4 Failed Route Add Failed"
                       " route %x mask %x Nexthop %x metric %d\n",
                       pRt->u4DestNet, pRt->u4DestMask, pRt->u4NextHop,
                       pRt->i4Metric1);
        }
        return IP_FAILURE;
    }
    pRt->u4HwIntfId[0] = nextHopInfo.u4HwIntfId[0];

    /* The HwStatus flag is used only when Async NP Call is used. 
     * The HwStatus is set as NotPresent at first and when the NP_CALL is
     * success, then the flag is updated to RTM_RT_HWSTATUS. 
     * This state is synced with the standby node through Dynamic syncup */

    if (RTM_GET_NODE_STATUS () == RM_ACTIVE)
    {
        pRt->u4Flag |= RTM_RT_HWSTATUS;
        RtmRedAddDynamicInfo (pRt, pRtmCxt->u4ContextId);
        RtmRedSendDynamicInfo (NETIPV4_ADD_ROUTE);
    }

    if (bu1TblFull == FNP_TRUE)
    {
        /* OverFlow in FIB , Update the global Flag */
        gu1FIBFlag = RTM_FIB_FULL;
    }
#else
    pRt->u4Flag |= RTM_RT_HWSTATUS;
#endif
    UNUSED_PARAM (pRtmCxt);
    UNUSED_PARAM (pRt);
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (u1RtCount);
    return IP_SUCCESS;
}

/*************************************************************************/
/* Function Name     :  IpTblRowStatusAction                             */
/*                                                                       */
/* Description       :  This function implements the State Machine       */
/*                      mentioned in the RFC1903 for Row Status          */
/*                      objects                                          */
/*                                                                       */
/* Global Variables  :  None                                             */
/*                                                                       */
/* Input(s)          :  pRt          - Pointer to Route Entr             */
/*                      pi4SetStatus - Pointer to Row Status value to    */
/*                                     be set                            */
/*                      pu1DpndObjSetFlag - Pointer to the Flag which    */
/*                                          signifies whether the        */
/*                                          dependent object have been   */
/*                                          SET or Not                   */
/*                                                                       */
/* Output(s)         :  None                                             */
/*                                                                       */
/* Returns           :  SNMP_SUCCESS | SNMP_FAILURE                      */
/*************************************************************************/
INT4
IpTblRowStatusAction (tNetIpv4RtInfo * pRt, INT4 *pi4SetStatus,
                      UINT1 *pu1UpdateNeeded)
{
    switch (*pi4SetStatus)
    {
        case IPFWD_ACTIVE:
            switch (pRt->u4RowStatus)
            {
                case IPFWD_NOT_IN_SERVICE:
                case IPFWD_NOT_READY:
                    pRt->u4RowStatus = IPFWD_ACTIVE;
                    /* This is added to Sort the Trie Routes, so that 
                     * Number of Active routes are updated in the trie 
                     * and check for row status active need not be done
                     * in the forwarding path.
                     */
                    *pu1UpdateNeeded = IP_NOT_READY_TO_ACTIVE;
                    break;

                default:
                    return SNMP_FAILURE;
            }
            break;

        case IPFWD_NOT_IN_SERVICE:
            switch (pRt->u4RowStatus)
            {
                case IPFWD_ACTIVE:
                    pRt->u4RowStatus = IPFWD_NOT_IN_SERVICE;
                    *pu1UpdateNeeded = IP_ACTIVE_TO_NOT_IN_SERVICE;
                    break;

                case IPFWD_NOT_IN_SERVICE:
                    break;

                default:
                    return SNMP_FAILURE;
            }

            break;

        case IPFWD_DESTROY:
            pRt->u4RowStatus = IPFWD_DESTROY;
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

 /******************************************************************************
 * Function Name    :   RtmUtilIpv4GetRoute 
 *
 * Description      :   This function provides the Route (either Exact Route or
 *                      Best route) for a given destination and Mask based on
 *                      the incoming request.
 *
 * Inputs           :   pRtQuery - Infomation about the route to be
 *                                 retrieved.
 *
 * Outputs          :   pNetIpRtInfo - Information about the requested route.
 *
 * Return Value     :   IP_SUCCESS - if the route is present.
 *                      IP_FAILURE - if the route is not present.
 *
 ******************************************************************************/
INT4
RtmUtilIpv4GetRoute (tRtInfoQueryMsg * pRtQuery, tNetIpv4RtInfo * pNetIpRtInfo)
{
    UINT4               au4Indx[IP_TWO];    /* The Key of Trie is made-up of 
                                               address and mask */
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtInfo            *pRt = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo            *apRtInfos[MAX_ROUTING_PROTOCOLS];
    INT4                i4Status = IP_FAILURE;
    UINT2               u2RouteCount = 0;
    pRtmCxt = UtilRtmGetCxt (pRtQuery->u4ContextId);

    if (pRtmCxt == NULL)
    {
        return IP_FAILURE;
    }

    /* Initialize the RtInfo array to NULL */
    MEMSET ((INT1 *) apRtInfos, 0,
            (sizeof (tRtInfo *) * MAX_ROUTING_PROTOCOLS));

    au4Indx[0] = IP_HTONL (pRtQuery->u4DestinationIpAddress);
    au4Indx[1] = IP_HTONL (pRtQuery->u4DestinationSubnetMask);

    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = ALL_ROUTING_PROTOCOL;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);
    OutParams.Key.pKey = NULL;
    OutParams.pAppSpecInfo = apRtInfos;

    if (pRtQuery->u1QueryFlag == RTM_QUERIED_FOR_NEXT_HOP)
    {
        /* Query is for Next Hop */
        i4Status = TrieLookupEntry (&(InParams), &(OutParams));
    }
    else
    {
        /* Query is for exact destination */
        i4Status = TrieSearchEntry (&(InParams), &(OutParams));
    }

    if (i4Status == TRIE_SUCCESS)
    {
        /* If the application Queried for a Route from one or more Specific 
           application,Return a valid Route from any one of the
           application */
        if (pRtQuery->u2AppIds != 0)
        {
            for (u2RouteCount = 0, pRt = NULL;
                 u2RouteCount < MAX_ROUTING_PROTOCOLS; u2RouteCount++)
            {
                if ((apRtInfos[u2RouteCount] != NULL) &&
                    (((pRtQuery->u2AppIds >> u2RouteCount) & 0x0001) == 1))
                {
                    pRt = apRtInfos[u2RouteCount];
                    if (pRt->u4RowStatus == IPFWD_ACTIVE)
                    {
                        RtmCopyRouteInfoInCxt (pRtmCxt->u4ContextId, pRt,
                                               pNetIpRtInfo);
                        return IP_SUCCESS;
                    }
                }
            }
        }
        else
        {
            /* Return the best reachable route exists */
            i4Status = IpFwdTblGetBestRouteInCxt (pRtmCxt, OutParams, &pRt,
                                                  RTM_GET_REACHABLE_BEST);
            if ((i4Status == IP_SUCCESS) && (pRt != NULL))
            {
                RtmCopyRouteInfoInCxt (pRtmCxt->u4ContextId, pRt, pNetIpRtInfo);
                return IP_SUCCESS;
            }
        }
    }

    /* Desired Route is not identified Hence return Failure */
    return IP_FAILURE;
}

/**************************************************************************/
/*   Function Name   : RtmGRStaleRtMark                                   */
/*   Description     : This function will mark the stale routes           */
/*                                  during GR process.               */
/*   Input(s)        : pRegnID - Protocol Info whose routes needs to be   */
/*                     marked as stale routes                             */
/*                     (IP_SUCCESS/IP_FAILURE)                            */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/

VOID
RtmGRStaleRtMark (tRtmRegnId * pRegnId)
{
    tAppSpecInfo       *pAppSpecInfo = NULL;
    tRtInfo            *pExstRt = NULL;
    tRtInfo            *pNextRt = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[2];
    UINT2               u2RegnId = RTM_INVALID_REGN_ID;
    tRtmCxt            *pRtmCxt = NULL;
    UINT1               au1Key[2 * sizeof (UINT4)];

    pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);
    if (RTM_APP_SPEC_INFO (pAppSpecInfo) == NULL)
    {
        return;
    }

    if (pRtmCxt == NULL)
    {
        APP_INFO_FREE (pAppSpecInfo);
        return;
    }

    MEMSET (&InParams, 0, sizeof (tInputParams));
    MEMSET (&OutParams, 0, sizeof (tOutputParams));
    MEMSET (pAppSpecInfo, 0, sizeof (tAppSpecInfo));
    OutParams.pAppSpecInfo = (VOID *) pAppSpecInfo;
    OutParams.Key.pKey = au1Key;
    MEMSET (OutParams.Key.pKey, 0, 2 * sizeof (UINT4));
    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        APP_INFO_FREE (pAppSpecInfo);
        return;
    }

    ROUTE_TBL_LOCK ();
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = (INT1) u2RegnId;
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    while (TrieGetNextEntry (&InParams, &OutParams) == TRIE_SUCCESS)
    {

        pExstRt = (tRtInfo *) OutParams.pAppSpecInfo;
        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        InParams.i1AppId = (INT1) u2RegnId;

        /* Update the key to get the next entry */
        InParams.Key.pKey = OutParams.Key.pKey;

        while (pExstRt != NULL)
        {
            pNextRt = pExstRt->pNextAlternatepath;
            pExstRt->u4Flag |= RTM_GR_RT;
            pExstRt = pNextRt;
        }

        MEMSET (pAppSpecInfo, 0, sizeof (tAppSpecInfo));
        OutParams.pAppSpecInfo = (VOID *) pAppSpecInfo;

    }

    ROUTE_TBL_UNLOCK ();
    APP_INFO_FREE (pAppSpecInfo);
    return;
}

/**************************************************************************/
/*   Function Name   : RtmIsForwPlanPreserved                            */
/*   Description     : This function will query the RTM and check whether */
/*                        mentioned Protocol routes are available or not  */
/*   Input(s)        : pRegnID - Protocol Info whose routes needs to be   */
/*                     checked                                            */
/*   Output(s)       : None                                               */
/*   Return Value    : ISIS_TRUE -Success                                 */
/*                     ISIS_FALSE - Failure                               */
/**************************************************************************/

INT4
RtmIsForwPlanPreserved (tRtmRegnId * pRegnId)
{
    tAppSpecInfo       *pAppSpecInfo = NULL;
    tInputParams        InParams;
    tOutputParams       OutParams;
    UINT4               au4Indx[2];
    UINT2               u2RegnId = RTM_INVALID_REGN_ID;
    tRtmCxt            *pRtmCxt = NULL;
    UINT1               au1Key[2 * sizeof (UINT4)];

    pRtmCxt = UtilRtmGetCxt (pRegnId->u4ContextId);

    if (RTM_APP_SPEC_INFO (pAppSpecInfo) == NULL)
    {
        return IP_FAILURE;
    }

    if (pRtmCxt == NULL)
    {
        APP_INFO_FREE (pAppSpecInfo);
        return IP_FAILURE;
    }

    MEMSET (&InParams, 0, sizeof (tInputParams));
    MEMSET (&OutParams, 0, sizeof (tOutputParams));
    MEMSET (pAppSpecInfo, 0, sizeof (tAppSpecInfo));
    OutParams.pAppSpecInfo = (VOID *) pAppSpecInfo;
    OutParams.Key.pKey = au1Key;
    MEMSET (OutParams.Key.pKey, 0, 2 * sizeof (UINT4));

    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRegnId);
    if (u2RegnId == RTM_INVALID_REGN_ID)
    {
        APP_INFO_FREE (pAppSpecInfo);
        return IP_SUCCESS;
    }
    ROUTE_TBL_LOCK ();
    InParams.pRoot = pRtmCxt->pIpRtTblRoot;
    InParams.i1AppId = (INT1) u2RegnId;
    au4Indx[0] = 0;
    au4Indx[1] = 0;
    InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

    if (TrieGetNextEntry (&InParams, &OutParams) == TRIE_SUCCESS)
    {
        ROUTE_TBL_UNLOCK ();
        APP_INFO_FREE (pAppSpecInfo);
        return IP_SUCCESS;
    }

    ROUTE_TBL_UNLOCK ();
    APP_INFO_FREE (pAppSpecInfo);
    return IP_FAILURE;
}

/*********************************************************************************************************
 *   Function    : RtmConfBestRoutesInCxt ()
 *
 *   Description : This function will provide add / delete indication for the route info provided based on
 *                 u1Cmd. u1RtInstall will indicate whether to install 
 *                 * single route, value = 0
 *                 * ECMP group, value = RTM_ECMP_RT
 *
 *   Called by   : IpForwardingTableAddRouteInCxt (), IpForwardingTableDeleteRouteInCxt (),
 *                 IpForwardingTableModifyRouteInCxt ().
 *
 *   Inputs      : pRtmCxt     - RTM Context Structure pointer
 *                 pNewRt      - Route Info for Route Add / modification
 *                 u1RtInstall - ZERO / RTM_ECMP_RT
 *                 u1Cmd       - NETIPV4_ADD_ROUTE / NETIPV4_DELETE_ROUTE
 *
 *   Output      : NONE
 *
 *   Returns     : IP_SUCCESS /IP_FAILURE
 * *******************************************************************************************************/
INT4
RtmConfBestRoutesInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt, UINT1 u1RtInstall,
                        UINT1 u1Cmd)
{
    tRtInfo            *pTempRt = NULL;
    tRtInfo            *pBestRt = NULL;
    INT4                i4RetVal = IP_SUCCESS;
    UINT1               u1IsRtReachable = IP_ZERO;
    UINT1               u1RtCount = IP_ZERO;
    UINT1               u1InstalledCount = IP_ZERO;

    if (pRtmCxt == NULL)
    {
        return IP_FAILURE;
    }

    if (u1Cmd == NETIPV4_ADD_ROUTE)
    {
        i4RetVal = RtmResolveRoute (pRtmCxt, pRt, &u1IsRtReachable);
        if (i4RetVal == IP_FAILURE)
        {
            return i4RetVal;
        }

        if (u1RtInstall == RTM_ECMP_RT)
        {
            pTempRt = pRt;
            while (pTempRt != NULL)
            {
                if ((pTempRt->u4Flag & RTM_ECMP_RT) == RTM_ECMP_RT)
                {
                    if (u1IsRtReachable > 0)
                    {
                        if ((pTempRt->u4Flag & RTM_RT_REACHABLE)
                            && (!(pTempRt->u4Flag & RTM_RT_HWSTATUS)))
                        {
                            u1RtCount++;
                            pTempRt->u1ECMPRt = TRUE;
                            RtmAddRouteToDataPlaneWithoutNotifyInCxt (pRtmCxt,
                                                                      pTempRt,
                                                                      u1RtCount);
                            RtmUpdateResolvedNHEntryInCxt (pRtmCxt,
                                                           pTempRt->u4NextHop,
                                                           RTM_ADD_ROUTE);
                        }
                    }
                    else
                    {
                        pTempRt->u1ECMPRt = FALSE;
                        RtmAddRouteToDataPlaneWithoutNotifyInCxt (pRtmCxt,
                                                                  pTempRt, 1);
                        break;
                    }
                }
                pTempRt = pTempRt->pNextAlternatepath;
            }
        }
        else
        {
            if ((pRt->u4Flag & RTM_ECMP_RT) == RTM_ECMP_RT)
            {
                if ((pRt->u4Flag & RTM_RT_REACHABLE) != RTM_RT_REACHABLE)
                {
                    return IP_SUCCESS;
                }

                IpGetBestRouteEntryInCxt (pRtmCxt, *pRt, &pBestRt);
                RtmGetInstalledRtCnt (pBestRt, &u1RtCount);
                pTempRt = pBestRt;

                pRt->u1ECMPRt = TRUE;
                u1RtCount++;    /* Number of route count needs to be incremented as new route is installed to NP */
                RtmAddRouteToDataPlaneWithoutNotifyInCxt (pRtmCxt, pRt,
                                                          u1RtCount);
                RtmUpdateResolvedNHEntryInCxt (pRtmCxt, pRt->u4NextHop,
                                               RTM_ADD_ROUTE);

                while (pTempRt != NULL)
                {
                    if ((pTempRt->u4Flag & RTM_RT_HWSTATUS)
                        && (!(pTempRt->u4Flag & RTM_RT_REACHABLE)))
                    {
                        pTempRt->u1ECMPRt = FALSE;
                        RtmDeleteRouteFromDataPlaneWithoutNotifyInCxt (pRtmCxt,
                                                                       pTempRt,
                                                                       u1RtCount);
                        u1RtCount--;
                    }
                    pTempRt = pTempRt->pNextAlternatepath;
                }
            }
            else
            {
                pRt->u1ECMPRt = FALSE;
                RtmAddRouteToDataPlaneWithoutNotifyInCxt (pRtmCxt, pRt, 1);
                if (u1IsRtReachable > 0)
                {
                    RtmUpdateResolvedNHEntryInCxt (pRtmCxt, pRt->u4NextHop,
                                                   RTM_ADD_ROUTE);
                }
            }
        }
    }

    if (u1Cmd == NETIPV4_DELETE_ROUTE)
    {
        IpGetBestRouteEntryInCxt (pRtmCxt, *pRt, &pBestRt);
        RtmGetInstalledRtCnt (pBestRt, &u1InstalledCount);

        if (u1RtInstall != RTM_ECMP_RT)
        {
            if (pRt->u4Flag & RTM_ECMP_RT)
            {
                if (pRt->u4Flag & RTM_RT_IN_ECMPPRT)
                {
                    RtmDeleteRtFromECMPPRTInCxt (pRtmCxt, pRt);
                }
                else
                {
                    RtmUpdateResolvedNHEntryInCxt (pRtmCxt, pRt->u4NextHop,
                                                   RTM_DELETE_ROUTE);
                }

                if (u1InstalledCount == 0)    /* Route to be deleted will not be in the NewBestRoute List */
                {
                    i4RetVal =
                        RtmResolveRoute (pRtmCxt, pBestRt, &u1IsRtReachable);
                    if (i4RetVal == IP_FAILURE)
                    {
                        return i4RetVal;
                    }

                    pTempRt = pBestRt;
                    while (pTempRt != NULL)
                    {
                        if ((pTempRt->u4Flag & RTM_ECMP_RT) == RTM_ECMP_RT)
                        {
                            if (u1IsRtReachable > 0)
                            {
                                if ((pTempRt->u4Flag & RTM_RT_REACHABLE)
                                    && (!(pTempRt->u4Flag & RTM_RT_HWSTATUS)))
                                {
                                    u1RtCount++;
                                    pTempRt->u1ECMPRt = TRUE;
                                    RtmAddRouteToDataPlaneWithoutNotifyInCxt
                                        (pRtmCxt, pTempRt, u1RtCount);
                                    RtmUpdateResolvedNHEntryInCxt (pRtmCxt,
                                                                   pTempRt->
                                                                   u4NextHop,
                                                                   RTM_ADD_ROUTE);
                                }
                            }
                            else
                            {
                                pTempRt->u1ECMPRt = FALSE;
                                RtmAddRouteToDataPlaneWithoutNotifyInCxt
                                    (pRtmCxt, pTempRt, 1);
                                break;
                            }
                        }
                        pTempRt = pTempRt->pNextAlternatepath;
                    }
                }

                if (pRt->u4Flag & RTM_RT_HWSTATUS)
                {
                    pRt->u1ECMPRt = FALSE;
                    RtmDeleteRouteFromDataPlaneWithoutNotifyInCxt (pRtmCxt, pRt,
                                                                   u1RtCount);
                }
            }
            else
            {
                if (pRt->u4Flag & RTM_RT_IN_PRT)
                {
                    RtmDeleteRtFromPRTInCxt (pRtmCxt, pRt);
                }
                else
                {
                    RtmUpdateResolvedNHEntryInCxt (pRtmCxt, pRt->u4NextHop,
                                                   RTM_DELETE_ROUTE);
                }

                if (pRt->u4Flag & RTM_RT_HWSTATUS)
                {
                    pRt->u1ECMPRt = FALSE;
                    RtmDeleteRouteFromDataPlaneWithoutNotifyInCxt (pRtmCxt, pRt,
                                                                   u1RtCount);
                }
            }
        }
        else
        {
            pTempRt = pRt;
#if 0
            /* uncomment - RTM_ENH */
            RtmGetBestRouteCount (pBestRt, &u1RtCount);
#endif

            while (pTempRt != NULL)
            {
                if (pTempRt->u4Flag & RTM_RT_IN_ECMPPRT)
                {
                    RtmDeleteRtFromECMPPRTInCxt (pRtmCxt, pTempRt);
                }
                else
                {
                    RtmUpdateResolvedNHEntryInCxt (pRtmCxt, pTempRt->u4NextHop,
                                                   RTM_DELETE_ROUTE);
                }

                if (pTempRt->u4Flag & RTM_RT_HWSTATUS)
                {
                    pTempRt->u1ECMPRt = FALSE;
                    RtmDeleteRouteFromDataPlaneWithoutNotifyInCxt (pRtmCxt,
                                                                   pTempRt,
                                                                   u1RtCount);
                    u1RtCount--;
                }
                pTempRt = pTempRt->pNextAlternatepath;
            }
        }
    }

    return i4RetVal;
}

INT4
RtmNotifyRtToHigherLayerAppsInCxt (tRtmCxt * pRtmCxt, tRtInfo * pNewRt,
                                   tRtInfo * pOldRt, UINT1 u1RtInstall,
                                   UINT1 u1Cmd)
{
    tRtInfo            *pBestRt = NULL;
    tRtInfo            *pTempRt = NULL;
    tRtInfo            *pTempRt1 = NULL;
    tRtInfo            *pTempRt2 = NULL;

    if (u1Cmd == NETIPV4_ADD_ROUTE)
    {
        if (u1RtInstall == RTM_ECMP_RT)
        {
            pTempRt = pNewRt;
            while (pTempRt != NULL)
            {
                if (pTempRt->u4Flag & RTM_RT_HWSTATUS)
                {
                    /* Function that add the route to NP and kernal */
                    RtmNotifyRtToAppsInCxt (pRtmCxt, pTempRt, NULL,
                                            NETIPV4_ADD_ROUTE);
                    break;
                }
                pTempRt = pTempRt->pNextAlternatepath;
            }
        }
        else
        {
            if (pNewRt->u4Flag & RTM_ECMP_RT)
            {
                /* Get Best route entry for this destination
                 * Check if there are any Notified route
                 * If exists, check if it is reachable, if reachable, this need not be notified
                 * else delete that notified route & notify this new route if this is reachable
                 */

                IpGetBestRouteEntryInCxt (pRtmCxt, *pNewRt, &pBestRt);
                pTempRt = pBestRt;
                while (pTempRt != NULL)
                {
                    if (pTempRt->u4Flag & RTM_NOTIFIED_RT)
                    {
                        if (pTempRt->u4Flag & RTM_RT_REACHABLE)
                        {
                            break;
                        }
                        else
                        {
                            if (pNewRt->u4Flag & RTM_RT_REACHABLE)
                            {
                                RtmNotifyRtToAppsInCxt (pRtmCxt, pNewRt,
                                                        pTempRt,
                                                        NETIPV4_MODIFY_ROUTE);
                            }
                            break;
                        }
                    }
                    pTempRt = pTempRt->pNextAlternatepath;
                }

            }
            else
            {
                /* Function that add the route to NP and kernal */
                RtmNotifyRtToAppsInCxt (pRtmCxt, pNewRt, NULL,
                                        NETIPV4_ADD_ROUTE);
            }
        }
    }

    if (u1Cmd == NETIPV4_DELETE_ROUTE)
    {
        if (u1RtInstall == RTM_ECMP_RT)
        {
            pTempRt = pOldRt;
            while (pTempRt != NULL)
            {
                if (pTempRt->u4Flag & RTM_NOTIFIED_RT)
                {
                    RtmNotifyRtToAppsInCxt (pRtmCxt, pTempRt, NULL,
                                            NETIPV4_DELETE_ROUTE);
                }
                pTempRt = pTempRt->pNextAlternatepath;
            }
        }
        else
        {
            /* Route Deleted needs to be notified to HL Apps, if it was already notified
             * Only one route from an ECMP group will be notified to HL Apps.
             * Hence, if the route deleted is a notified route, then we need to select another
             * route from group to notify the group
             */

            if (pOldRt->u4Flag & RTM_ECMP_RT)
            {
                if (pOldRt->u4Flag & RTM_NOTIFIED_RT)
                {
                    IpGetBestRouteEntryInCxt (pRtmCxt, *pOldRt, &pBestRt);
                    pTempRt = pBestRt;
                    while (pTempRt != NULL)
                    {
                        if ((pTempRt != pOldRt)
                            && (pTempRt->u4Flag & RTM_RT_REACHABLE))
                        {
                            break;
                        }
                        pTempRt = pTempRt->pNextAlternatepath;
                    }
                    if (pTempRt == NULL)
                    {
                        if (pBestRt != pOldRt)
                        {
                            pTempRt = pBestRt;
                        }
                        else if (pBestRt->pNextAlternatepath != NULL)
                        {
                            pTempRt = pBestRt->pNextAlternatepath;
                        }
                    }
                    RtmNotifyRtToAppsInCxt (pRtmCxt, pTempRt, pOldRt,
                                            NETIPV4_MODIFY_ROUTE);
                }
            }
            else
            {
                if (pOldRt->u4Flag & RTM_NOTIFIED_RT)
                {
                    RtmNotifyRtToAppsInCxt (pRtmCxt, pOldRt, NULL,
                                            NETIPV4_DELETE_ROUTE);
                }
            }
        }
    }

    if (u1Cmd == NETIPV4_MODIFY_ROUTE)
    {
        pTempRt = pNewRt;
        while (pTempRt != NULL)
        {
            if (pTempRt->u4Flag & RTM_RT_REACHABLE)
            {
                pTempRt1 = pTempRt;
                break;
            }

            if ((pTempRt2 == NULL) && (pTempRt->u4Flag & RTM_RT_HWSTATUS))
            {
                pTempRt2 = pTempRt;
            }

            pTempRt = pTempRt->pNextAlternatepath;
        }

        if (pTempRt1 == NULL)
        {
            pTempRt1 = pTempRt2;
        }

        pTempRt = NULL;
        pTempRt2 = NULL;

        pTempRt = pOldRt;
        while (pTempRt != NULL)
        {
            if (pTempRt->u4Flag & RTM_NOTIFIED_RT)
            {
                pTempRt2 = pTempRt;
                break;
            }
            pTempRt = pTempRt->pNextAlternatepath;
        }
        RtmNotifyRtToAppsInCxt (pRtmCxt, pTempRt1, pTempRt2,
                                NETIPV4_MODIFY_ROUTE);
    }
    return IP_SUCCESS;
}

INT4
RtmAddRouteToDataPlaneWithoutNotifyInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt,
                                          UINT1 u1RtCount)
{
    UINT4               u4CfaIfIndex = 0;
    UINT1               u1IpForward;

    if ((pRtmCxt == NULL) || (pRt == NULL))
    {
        return IP_FAILURE;
    }

    if (NetIpv4GetIpForwardingInCxt (pRtmCxt->u4ContextId,
                                     &u1IpForward) == NETIPV4_FAILURE)
    {
        return IP_FAILURE;
    }
    else
    {
        /* Dont add the route to NP if IP forwarding is disabled */
        if (u1IpForward != IP_FORW_ENABLE)
        {
            return IP_SUCCESS;
        }
    }

    /* Local routes need not be added to the hardware.
     *      * It is taken care by the ARP table. */
    if (gu1FIBFlag != RTM_FIB_FULL)

    {
        /* Get the CFA Interface Index from Ip Port No */
        if (NetIpv4GetCfaIfIndexFromPort (pRt->u4RtIfIndx,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            return IP_FAILURE;
        }
        /* If the Port is OOB Port, and the routing over the *
         *          * mangement interface is wanted then program the    *
         *                   * route in NP else  DontProgram in NP or Add to PRT */
        if ((CfaIsMgmtPort (u4CfaIfIndex) == TRUE) &&
            (CFA_OOB_MGMT_INTF_ROUTING == FALSE))
        {
            return IP_SUCCESS;
        }

        RtmAddRtToFIBInCxt (pRtmCxt, pRt, u4CfaIfIndex, u1RtCount);
#ifdef LNXIP4_WANTED
        RtmNetIpv4LeakRoute (NETIPV4_ADD_ROUTE, pRt);
#endif
    }
    return IP_SUCCESS;
}

INT4
RtmDeleteRouteFromDataPlaneWithoutNotifyInCxt (tRtmCxt * pRtmCxt, tRtInfo * pRt,
                                               UINT1 u1RtCount)
{
    UINT4               u4CfaIfIndex = IP_ZERO;

    if ((pRtmCxt == NULL) || (pRt == NULL))
    {
        return IP_FAILURE;
    }

    /* Get the CFA Interface Index from Ip Port No */
    if (NetIpv4GetCfaIfIndexFromPort (pRt->u4RtIfIndx,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return IP_FAILURE;
    }
    RtmDeleteRtFromFIBInCxt (pRtmCxt, pRt, u4CfaIfIndex, u1RtCount);
#ifdef LNXIP4_WANTED
    RtmNetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, pRt);
#endif
    return IP_SUCCESS;
}

INT4
RtmResolveRoute (tRtmCxt * pRtmCxt, tRtInfo * pRt, UINT1 *pu1IsRtReachable)
{
    tPingEntry          PingEntry;
    tRtInfo            *pTempRt = NULL;
    INT4                i4RetVal = ARP_SUCCESS;
    UINT1               u1EncapType;
    INT1                ai1MacAddr[CFA_ENET_ADDR_LEN];

    MEMSET (ai1MacAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (&PingEntry, 0, sizeof (tPingEntry));

    if (pRtmCxt == NULL)
    {
        return IP_FAILURE;
    }

    pTempRt = pRt;
    while (pTempRt != NULL)
    {
        /* Unlock and Lock before calling ARP function, since a lock is taken
         * inside ArpResolve.
         * As Counting semaphore is used here, unlock and lock should not
         * cause any problems.
         */

        if (pTempRt->u4RowStatus != IPFWD_ACTIVE)
        {
            pTempRt = pTempRt->pNextAlternatepath;
            continue;
        }

        ARP_PROT_UNLOCK ();
        i4RetVal =
            ArpResolveInCxt (pTempRt->u4NextHop, ai1MacAddr, &u1EncapType,
                             pRtmCxt->u4ContextId);
        ARP_PROT_LOCK ();
        if (i4RetVal == ARP_SUCCESS)
        {
            /* ARP is resolved already.
             * so Setting the route as reachable
             * and incrementing reachable route count.*/
            pTempRt->u4Flag |= RTM_RT_REACHABLE;
            (*pu1IsRtReachable)++;
        }
        else
        {
            PingEntry.u4IpPingDest = pTempRt->u4NextHop;
            PingEntry.i4IpPingMTU = (INT4) CLI_PING_DEFAULT_SIZE;
            PingEntry.i4IpPingTries = (INT4) IP_ONE;
            PingEntry.i4IpPingTimeout = (INT4) CLI_PING_DEFAULT_TIMEOUT;
            PingEntry.u4ContextId = pRtmCxt->u4ContextId;
            IpResolveNeighbor (PingEntry);
            if (pTempRt->u4Flag & RTM_ECMP_RT)
            {
                RtmAddRtToECMPPRTInCxt (pRtmCxt, pTempRt);
            }
            else
            {
                RtmAddRtToPRTInCxt (pRtmCxt, pTempRt);
            }
        }
        pTempRt = pTempRt->pNextAlternatepath;
    }
    return IP_SUCCESS;
}

UINT4
RtmGetECMPSupportGroups ()
{
    UINT4               u4EcmpGroups = 2;

    /* Add NP Call once NPSIM files are baselined */
    return u4EcmpGroups;
}

VOID
RtmFailedRouteAction (tRtmCxt * pRtmCxt, tRtInfo * pRt)
{
    UNUSED_PARAM (pRtmCxt);
    UtlTrcLog (1, 1, "", "ERROR[RTM]: IPV4 Failed Route Add Failed"
               " route %x mask %x Nexthop %x metric %d\n",
               pRt->u4DestNet, pRt->u4DestMask, pRt->u4NextHop, pRt->i4Metric1);
}

/**************************************************************************/
/*   Function Name   : RtmUpdateRouteCount                                */
/*   Description     : This function will increment the route count for   */
/*                     static and dynamic protocols                       */
/*   Input(s)        : pRtmCxt  - RTM Context                             */
/*                     pRt      - RouteInfo                               */
/*                     u4Action - Increment or Decrement Operation        */
/*   Output(s)       : None                                               */
/**************************************************************************/
VOID
RtmUpdateRouteCount (tRtmCxt * pRtmCxt, tRtInfo * pRt, UINT4 u4Action)
{
    if ((u4Action == RTM_RT_COUNT_INC) && (pRt->u1CountFlag == FALSE))
    {
        switch (pRt->u2RtProto)
        {
            case CIDR_LOCAL_ID:
                pRtmCxt->u4ConnectedRoutes++;
                break;

            case CIDR_STATIC_ID:
                pRtmCxt->u4StaticRoutes++;
                break;
            case RIP_ID:
                pRtmCxt->u4RipRoutes++;
                break;

            case BGP_ID:
                pRtmCxt->u4BgpRoutes++;
                break;

            case OSPF_ID:
                pRtmCxt->u4OspfRoutes++;
                break;

            case ISIS_ID:
                pRtmCxt->u4IsisRoutes++;
                break;

            default:
                break;
        }
        pRt->u1CountFlag = TRUE;
    }
    if ((u4Action == RTM_RT_COUNT_DEC) && (pRt->u1CountFlag == TRUE))
    {
        switch (pRt->u2RtProto)
        {
            case CIDR_LOCAL_ID:
                pRtmCxt->u4ConnectedRoutes--;
                break;

            case CIDR_STATIC_ID:
                pRtmCxt->u4StaticRoutes--;
                break;

            case RIP_ID:
                pRtmCxt->u4RipRoutes--;
                break;

            case BGP_ID:
                pRtmCxt->u4BgpRoutes--;
                break;

            case OSPF_ID:
                pRtmCxt->u4OspfRoutes--;
                break;

            case ISIS_ID:
                pRtmCxt->u4IsisRoutes--;
                break;

            default:
                break;
        }
        pRt->u1CountFlag = FALSE;

    }
}

/**************************************************************************/
/*   Function Name   : RtmCheckEcmpRouteCountForDest                      */
/*   Description     : This function will give the number of static routes*/
/*                     configured for a destination via different nexthos */
/*   Input(s)        : u4ContextId- Context ID                            */
/*                     pRt               - RouteInfo                      */
/*                     u4IpRouteDest     - Destination                    */
/*                     u4IpRouteMask     - Destination Mask               */
/*                     u4IpRouteNextHop  - Next Hop                       */
/*                     i4Metric          - Metric                         */
/*   Output(s)       : u4RouteCount - Route Count                         */
/**************************************************************************/
INT4
RtmCheckEcmpRouteCountForDest (UINT4 u4ContextId, UINT4 u4IpRouteDest,
                               UINT4 u4IpRouteMask, INT4 i4Metric)
{
    tRtInfo            *pTmpRtInfo = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo            *pRt = NULL;
    tRtInfo            *paRtSpecInfo[MAX_ROUTING_PROTOCOLS + 1];
    INT4                i4RtCount = ZERO;
    tRtInfo             RtSpecInfo;

    pRtmCxt = UtilRtmGetCxt (u4ContextId);

    if (pRtmCxt == NULL)
    {
        return RTM_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmInvdRtList), pTmpRtInfo, tRtInfo *)
    {
        if (pTmpRtInfo != NULL)
        {
            if ((pTmpRtInfo->u4DestNet == u4IpRouteDest) &&
                (pTmpRtInfo->u4DestMask == u4IpRouteMask) &&
                (pTmpRtInfo->i4Metric1 == i4Metric))
            {
                i4RtCount++;
            }
        }
    }

    paRtSpecInfo[STATIC_RT_INDEX] = &(RtSpecInfo);
    if (IpForwardingTableGetRouteInfoInCxt
        (pRtmCxt->u4ContextId, u4IpRouteDest, u4IpRouteMask, CIDR_STATIC_ID,
         &(paRtSpecInfo[STATIC_RT_INDEX])) == IP_SUCCESS)
    {
        pRt = paRtSpecInfo[STATIC_RT_INDEX];
        if (pRt != NULL)
        {
            pTmpRtInfo = pRt;
            while (pTmpRtInfo != NULL)
            {
                if (pTmpRtInfo->i4Metric1 == i4Metric)
                {
                    i4RtCount++;
                }
                pTmpRtInfo = pTmpRtInfo->pNextAlternatepath;
            }
        }
    }
    return i4RtCount;
}
