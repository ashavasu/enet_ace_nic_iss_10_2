
/********************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmsinp.c,v 1.3 2013/03/28 11:46:49 siva Exp $
 *
 *******************************************************************/

#include "rtminc.h"
/*****************************************************************************
 *    Function Name             : FsNpCfaVrfSetDlfStatus
 *    Description               : This function registers/de-registers to get
 *                                the L3 the DLF packets to CPU
 *    Input(s)                  : Virutal routerid and dlf status - True/False
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : None.
 *
 *****************************************************************************/
PUBLIC INT4
FsNpCfaVrfSetDlfStatus (UINT4 u4VrfId, UINT1 u1Status)
{
    UNUSED_PARAM (u4VrfId);
    return (FsNpCfaSetDlfStatus (u1Status));
}
