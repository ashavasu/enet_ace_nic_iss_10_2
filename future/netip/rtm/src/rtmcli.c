/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmcli.c,v 1.38 2016/03/29 10:48:21 siva Exp $
 *
 * Description: Action routines of IP module specific commands
 *
 *******************************************************************/
#ifndef __RRDCLI_C__
#define __RRDCLI_C__

#include "rtminc.h"
#include "rrdcli.h"
#include "rtm.h"
#include "vcm.h"
#include "rtmdefns.h"
#include "fsrtmwr.h"
#include "fsmirtlw.h"
#include "fsrtmlw.h"
#include "bgp4cli.h"
#include "iscli.h"

extern UINT1 gu1EcmpAcrossProtocol;

const CHR1         *paProtoTable[] = {
    "none",
    "other",
    "local",
    "static",
    "icmp",
    "egp",
    "ggp",
    "hello",
    "rip",
    "isis",
    "esis",
    "cicsoigrp",
    "bbnspfigp",
    "ospf",
    "bgp",
    "idpr",
    "ciscoeigrp"
};

/**************************************************************************/
/*  Function Name   : cli_process_rrd_cmd                                */
/*                                                                        */
/*  Description     : Protocol CLI message handler function               */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
cli_process_rrd_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[RRD_CLI_MAX_ARGS];
    UINT1               u1ExportFlag;
    INT1                i1argno = 0;
    INT4                i4SrcProto = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4MaxRoute = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT1              *pu1RtmCxtName = NULL;
    UINT4               u4RtmCxtId = VCM_INVALID_VC;
    INT1                i1IsShowCommand = FALSE;
    UINT1               u1ProtoType = 0;

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    va_arg (ap, INT4);

    /* Fourth argument is always ContextName */
    pu1RtmCxtName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in args array. 
     * Store RRD_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == RRD_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    CliRegisterLock (CliHandle, RtmProtocolLock, RtmProtocolUnlock);
    RTM_PROT_LOCK ();

    if (pu1RtmCxtName != NULL)
    {
        if (UtilRtmGetCxtIdFromCxtName (pu1RtmCxtName,
                                        &u4RtmCxtId) == RTM_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid RTM Context Id\r\n");
            CliUnRegisterLock (CliHandle);
            RTM_PROT_UNLOCK ();
            return CLI_FAILURE;
        }

    }
    if (RtmCliGetCommandType (u4Command, &i1IsShowCommand) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Command Identifier\r\n");
        CliUnRegisterLock (CliHandle);
        RTM_PROT_UNLOCK ();
        return CLI_FAILURE;
    }
    if (i1IsShowCommand == TRUE)
    {

        if (pu1RtmCxtName == NULL)
        {
            /* Show command should display for all the 
               Context */
            u4RtmCxtId = VCM_INVALID_VC;
        }
        switch (u4Command)
        {
            case CLI_IP_SHOW_PROTOCOLS:
                i4RetStatus =
                    ShowRoutingProtocolInfoInCxt (CliHandle, u4RtmCxtId);
                break;

            case CLI_RRD_SHOW_CONTROL_INFO:
                i4RetStatus = ShowRtmControlInfoInCxt (CliHandle, u4RtmCxtId);
                break;

            case CLI_RRD_SHOW_STATUS:
                i4RetStatus = ShowRrdStatusInCxt (CliHandle, u4RtmCxtId);
                break;

            case CLI_RRD_SHOW_MAX_ROUTE:
               i4RetStatus  = RtmShowRouteMemReservation(CliHandle);
               break;

            case CLI_RRD_SHOW_ECMP_ACROSS_PROTOCOL:
               RtmShowEcmpAcrossProtocols(CliHandle);
               break;

#ifdef RM_WANTED
            case CLI_RRD_RED_DYN_INFO:
                i4RetStatus = ShowRrdRedDynInfo (CliHandle);
                break;
#endif

            default:
                CliPrintf (CliHandle, "\r% Invalid Command\r\n");
                i4RetStatus = CLI_FAILURE;
                break;

        }
    }
    else
    {
        if (pu1RtmCxtName == NULL)
        {
            /* IF context name is not provided then configuration should be
             * done in default RTM context */
            u4RtmCxtId = RTM_DEFAULT_CXT_ID;
        }
        /* Set Context Id for RTM, which will be used by the following
           configuration commands */
        if (UtilRtmSetContext (u4RtmCxtId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Rtm Context Id\r\n");
            CliUnRegisterLock (CliHandle);
            RTM_PROT_UNLOCK ();
            return CLI_FAILURE;
        }
        switch (u4Command)
        {
            case CLI_RRD_AS_NUM:
                /* args[0] - Autonomous System Number */
                i4RetStatus = CliRrdAsNum (CliHandle, *(INT4 *) args[0]);
                break;

            case CLI_RRD_ROUTER_ID:
                /* args[0] - Router ID */
                i4RetStatus = CliRrdRouterId (CliHandle, *args[0]);
                break;

            case CLI_RRD_EXPORT_OSPF:
                /* args[0] - Flag Indicating Area Route/External Route */
                /* args[1] - Destination Protocol */
                if (CLI_PTR_TO_U4 (args[0]) == CLI_RRD_OSPF_AREA_RT)
                {
                    i4RetStatus = CliRrdOspfAreaStatus (CliHandle,
                                                        CLI_PTR_TO_I4 (args[1]),
                                                        CLI_ENABLE);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == CLI_RRD_OSPF_EXT_RT)
                {
                    i4RetStatus = CliRrdOspfExtStatus (CliHandle,
                                                       CLI_PTR_TO_I4 (args[1]),
                                                       CLI_ENABLE);
                }
                break;

            case CLI_RRD_NO_EXPORT_OSPF:
                /* args[0] - Flag Indicating Area Route/External Route */
                /* args[1] - Destination Protocol */
                if (CLI_PTR_TO_U4 (args[0]) == CLI_RRD_OSPF_AREA_RT)
                {
                    i4RetStatus = CliRrdOspfAreaStatus (CliHandle,
                                                        CLI_PTR_TO_I4 (args[1]),
                                                        CLI_DISABLE);
                }
                else if (CLI_PTR_TO_U4 (args[0]) == CLI_RRD_OSPF_EXT_RT)
                {
                    i4RetStatus = CliRrdOspfExtStatus (CliHandle,
                                                       CLI_PTR_TO_I4 (args[1]),
                                                       CLI_DISABLE);
                }
                break;

            case CLI_RRD_DEF_CONTROL_STATUS:
                /* args[0] - Flag Indicating Permit/Deny */
                i4RetStatus = CliRrdDefControlEntry (CliHandle,
                                                     CLI_PTR_TO_I4 (args[0]));
                break;

            case CLI_RRD_REDISTRIBUTE_POLICY:
                /* args[0] - Flag Indicating Permit/Deny */
                /* args[1] - Destination Network */
                /* args[2] -  Address Range */
                /* args[3] - Source Protocol ID */
                /* args[4] - Destination Protocol Mask */
                u1ExportFlag = (UINT1) (CLI_PTR_TO_U4 (args[0]));
                i4SrcProto = CLI_PTR_TO_I4 (args[3]);
                i4RetStatus = CliRrdAddControlTable (CliHandle, u1ExportFlag,
                                                     *args[1],
                                                     *args[2],
                                                     i4SrcProto,
                                                     CLI_PTR_TO_I4 (args[4]));
                break;

            case CLI_RRD_NO_REDISTRIBUTE_POLICY:
                /* args[0] - Destination Network */
                /* args[1] -  Address Range */
                i4RetStatus =
                    CliRrdDelControlTable (CliHandle, *args[0], *args[1]);
                break;

            case CLI_RRD_THROT_VALUE:
                i4RetStatus = CliRrdSetThrottleLimit (CliHandle, *args[0]);
                break;

           case CLI_RRD_MAX_ROUTE:
               /* args [0] --> Protocol type
                * args [1] --> Value to be configured
                * args [2] --> Flag to reset
                */
               u1ProtoType = (UINT1) CLI_PTR_TO_U4(args[0]);
               if ( CLI_PTR_TO_U4(args[2]) == DISABLED )
               {
                       switch ( u1ProtoType )
                       {
                               case STATIC_ID: u4MaxRoute = DEF_RTM_STATIC_ROUTE_ENTRIES;
                                               break;
                               case BGP_ID: u4MaxRoute = DEF_RTM_BGP_ROUTE_ENTRIES;
                                            break;
                               case OSPF_ID: u4MaxRoute = DEF_RTM_OSPF_ROUTE_ENTRIES;
                                             break;
                               case RIP_ID: u4MaxRoute = DEF_RTM_RIP_ROUTE_ENTRIES;
                                            break;
                               case ISIS_ID: u4MaxRoute = DEF_RTM_ISIS_ROUTE_ENTRIES;
                                            break;
                               default: 
                                       break;   
                       }
               }
               else
               {
                       u4MaxRoute = (UINT4) *(args[1]);
               }

               i4RetStatus = RtmSetMaxRoute (u1ProtoType, u4MaxRoute);
               break;


            case CLI_RRD_AS_ROUTERID_FORCE:
                /* args[1] -> Enable */
                i4RetStatus =
                    CliRrdForceAsNumRouterId (CliHandle, u4RtmCxtId,
                                              RTM_FORCE_ENABLE);
                break;

            case CLI_ECMP_ACROSS_PROTOCOL:
                i4RetStatus =
                    CliEcmpAcrossProtocols (CliHandle, CLI_PTR_TO_U4(args[0]));
                break;
            case CLI_VRF_ROUTE_LEAK:
		i4RetStatus =
		    CliVrfRouteLeaking (CliHandle, CLI_PTR_TO_U4(args[0]));
		break;
            
            default:
                CliPrintf (CliHandle, "\r% Invalid Command\r\n");
                i4RetStatus = CLI_FAILURE;
                break;

        }
        /*Reset RTM Context */
        UtilRtmResetContext ();
    }

    if ((i4RetStatus == (INT4) CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_RRD_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", RrdCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);
    RTM_PROT_UNLOCK ();

    return i4RetStatus;
}

#ifdef RM_WANTED
/*********************************************************************
*  Function Name : ShowRrdRedDynInfo
*  Description   : Reads the gloabal RTM table and gets the 
                   routing information updated dynamically

*   Input(s)      : CliHandle   - CdliContext ID
                    i4RrdAsNum  - VRF Id
*  Output(s)     : None
*  Return Values : CLI_SUCCESS/CLI_FAILURE.
**********************************************************************/

INT4
ShowRrdRedDynInfo (tCliHandle CliHandle)
{

    tRtmRedTable       *pRtmRedInfo = NULL;
    CHR1               *pu1IpAddr = NULL;

    pRtmRedInfo = RBTreeGetFirst (gRtmGlobalInfo.RtmRedTable);
    CliPrintf (CliHandle,
               "\r\n%-5s    %-5s    %-5s    %-5s    %-5s    %-5s    %-5s\r\n",
               "ContextId", "DestNet", "DestMask", "NextHop", "ReachableFlag",
               "Protocol", "DelFlag");
    CliPrintf (CliHandle,
               "---------    -------    --------    -------    -------------    --------    -------\r\n");

    while (pRtmRedInfo != NULL)
    {
        CliPrintf (CliHandle, "%5d", pRtmRedInfo->u4CxtId);
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, pRtmRedInfo->u4DestNet);
        CliPrintf (CliHandle, "%15s", pu1IpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, pRtmRedInfo->u4DestMask);
        CliPrintf (CliHandle, "%12s", pu1IpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, pRtmRedInfo->u4DestNet);
        CliPrintf (CliHandle, "%12s", pu1IpAddr);
        CliPrintf (CliHandle, "%12d", pRtmRedInfo->u4Flag);
        CliPrintf (CliHandle, "%12d", pRtmRedInfo->u2RtProto);
        CliPrintf (CliHandle, "%12d\r\n", pRtmRedInfo->u1DelFlag);
        pRtmRedInfo = RBTreeGetNext (gRtmGlobalInfo.RtmRedTable,
                                     (tRBElem *) pRtmRedInfo, NULL);

    }
    return CLI_SUCCESS;
}
#endif

/*********************************************************************
*  Function Name : CliRrdAsNum
*  Description   : Calls Low level Set Routine for setting AS Number 
*                  
*  Input(s)      : i4RrdAsNum - Autonomous System Number 
*  Output(s)     : None 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdAsNum (tCliHandle CliHandle, INT4 i4RrdAsNum)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRrdRouterASNumber (&u4ErrorCode, i4RrdAsNum) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetFsRrdRouterASNumber (i4RrdAsNum) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrdRouterId
*  Description   : Calls Low level Set Routine for setting RouterID 
*                  and enables RRD.                  
*  Input(s)      : u4RouterId - Router ID. 
*  Output(s)     : None. 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdRouterId (tCliHandle CliHandle, UINT4 u4RouterId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRrdRouterId (&u4ErrorCode, u4RouterId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdRouterId (u4RouterId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsRrdAdminStatus (&u4ErrorCode, CLI_ENABLE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdAdminStatus (CLI_ENABLE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrdDefControlEntry
*  Description   : Calls Low level Set Routines for Configuring RRD
*                  control table default entry.
*                  
*  Input(s)      : u1ExportFlag - Default Permit/Deny Entry
*  Output(s)     : None 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdDefControlEntry (tCliHandle CliHandle, INT4 i4ExportFlag)
{
    CliRrdModDefCtrlEntry (i4ExportFlag);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : CliRrdAddControlTable
*  Description   : Calls Low level Set Routines for adding entries into RRD
*                  control table.
*                  
*  Input(s)      : u1ExportFlag    - To Identify either Permit or Deny Entry
*                  u4DestNet   - Destination Network  
*                  u4DestRange - Address Range
*                  i4SrcProcol     - Source Protocol  
*                  i4DestProtoMask - Destination Protocol Mask
*  Output(s)     : None. 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdAddControlTable (tCliHandle CliHandle, UINT1 u1ExportFlag,
                       UINT4 u4DestNet, UINT4 u4DestRange, INT4 i4SrcProtocol,
                       INT4 i4DestProtoMask)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4CtDestProtoMask = 0;
    INT4                i4CtExportFlag = 0;

    if (nmhGetFsRrdControlRowStatus (u4DestNet, u4DestRange,
                                     &i4RowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsRrdControlRowStatus
            (&u4ErrorCode, u4DestNet, u4DestRange,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsRrdControlRowStatus (u4DestNet, u4DestRange,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (nmhGetFsRrdControlDestProto (u4DestNet, u4DestRange,
                                          (INT4 *) &u4CtDestProtoMask) ==
             SNMP_SUCCESS)
    {
        if (nmhGetFsRrdControlRouteExportFlag (u4DestNet, u4DestRange,
                                               &i4CtExportFlag) == SNMP_SUCCESS)
        {
            if ((UINT1) i4CtExportFlag == u1ExportFlag)
            {
                i4DestProtoMask |= (INT4) u4CtDestProtoMask;
            }
        }
        if (nmhSetFsRrdControlRouteExportFlag (u4DestNet, u4DestRange,
                                               RTM_ROUTE_PERMIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2FsRrdControlSourceProto (&u4ErrorCode, u4DestNet, u4DestRange,
                                          i4SrcProtocol) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdControlSourceProto (u4DestNet, u4DestRange,
                                       i4SrcProtocol) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsRrdControlDestProto (&u4ErrorCode, u4DestNet, u4DestRange,
                                        i4DestProtoMask) == SNMP_FAILURE)
    {

        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdControlDestProto (u4DestNet, u4DestRange, i4DestProtoMask)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsRrdControlRouteExportFlag (&u4ErrorCode, u4DestNet,
                                              u4DestRange,
                                              u1ExportFlag) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdControlRouteExportFlag (u4DestNet, u4DestRange, u1ExportFlag)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsRrdControlRowStatus (&u4ErrorCode, u4DestNet, u4DestRange,
                                        ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdControlRowStatus (u4DestNet, u4DestRange, ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrdDelControlTable
*  Description   : Calls Low level Set Routines for deleting Entries from RRD
*                  control table.
*                  
*  Input(s)      : u4DestNet   - Destination Network  
*                  u4DestRange - Address Range
*  Output(s)     : None. 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdDelControlTable (tCliHandle CliHandle, UINT4 u4DestNet, UINT4 u4DestRange)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRrdControlRowStatus (&u4ErrorCode, u4DestNet, u4DestRange,
                                        DESTROY) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdControlRowStatus (u4DestNet, u4DestRange,
                                     DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrdOspfAreaStatus
*  Description   : Calls Low level Set Routines to set the status of
                    Ospf Area routes redistribution for protocol.
*  Input(s)      : i4DestProtoId - Destination Protocol ID
*                  i4OspfRtStatus -  Enable/Disable Status
*  Output(s)     : None
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdOspfAreaStatus (tCliHandle CliHandle, INT4 i4DestProtoId,
                      INT4 i4OspfRtStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRrdAllowOspfAreaRoutes (&u4ErrorCode, i4DestProtoId,
                                           i4OspfRtStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdAllowOspfAreaRoutes (i4DestProtoId, i4OspfRtStatus)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrdOspfExtStatus
*  Description   : Calls Low level Set Routines to set the status of
                    Ospf External routes redistribution for protocol.
*                  
*  Input(s)      : i4DestProtoId - Destination Protocol ID
*                  i4OspfRtStatus -  Enable/Disable Status
*  Output(s)     : None 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdOspfExtStatus (tCliHandle CliHandle, INT4 i4DestProtoId,
                     INT4 i4OspfRtStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsRrdAllowOspfExtRoutes (&u4ErrorCode, i4DestProtoId,
                                          i4OspfRtStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsRrdAllowOspfExtRoutes (i4DestProtoId, i4OspfRtStatus)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrdSetThrottleLimit
*  Description   : Calls Low level Set Routines to set the
                    throttle limit
*                  
*  Input(s)      : u4ThrottleLimit - Throttle Limit value
*
*  Output(s)     : None 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdSetThrottleLimit (tCliHandle CliHandle, UINT4 u4ThrottleLimit)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsRtmThrottleLimit (&u4ErrorCode, u4ThrottleLimit) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    nmhSetFsRtmThrottleLimit (u4ThrottleLimit);
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrdForceAsNumRouterId
*  Description   : Calls Low level Set Routines to enable the
*                  force option
*                  
*  Input(s)      : u4RtmCxtId - ContextId
*                  u4ForceFlag - ForceFlagStatus
*
*  Output(s)     : None 
*  Return Values : None. 
**********************************************************************/
INT4
CliRrdForceAsNumRouterId (tCliHandle CliHandle, UINT4 u4RtmCxtId,
                          UINT4 u4ForceFlag)
{
    INT1                i1RetVal;
    UINT4               u4ErrorCode = 0;

    i1RetVal =
        nmhTestv2FsMIRrdForce (&u4ErrorCode, (INT4) u4RtmCxtId,
                               (INT4) u4ForceFlag);
    if (i1RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    i1RetVal = nmhSetFsMIRrdForce ((INT4) u4RtmCxtId, (INT4) u4ForceFlag);
    if (i1RetVal == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : CliEcmpAcrossProtocols
*  Description   : Calls Low level Set Routines to enable/disable the
*                  ECMP across protocols feature
*                  
*  Input(s)      : u4RtmCxtId - ContextId
*                  u1AdminStatus - admin status of ECMP across protocol
*                                  feature 
*
*  Output(s)     : None 
*  Return Values : None. 
**********************************************************************/
INT4
CliEcmpAcrossProtocols (tCliHandle CliHandle, UINT4 u4AdminStatus)
{
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal;
    UNUSED_PARAM (CliHandle);

    i1RetVal = nmhTestv2FsMIEcmpAcrossProtocolAdminStatus (&u4ErrorCode,
                                                           (INT4)u4AdminStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    i1RetVal = nmhSetFsMIEcmpAcrossProtocolAdminStatus ((INT4)u4AdminStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    CliPrintf(CliHandle,"Ecmp-across-protocol configuration will be "
                        "taking effect after MSR only\r\n\n");

    return (CLI_SUCCESS);
}

/*********************************************************************
 Function Name : CliVrfRouteLeaking
 Description   : Calls Low level Set Routines to enable/disable the
                 VRF route leaking between Default and non-defaultVRF

 Input(s)      : u4RtmCxtId - ContextId
                 u1AdminStatus - admin status of VRF route leaking
                 feature

 Output(s)     : None

 Return Values : None.
**********************************************************************/
INT4
CliVrfRouteLeaking (tCliHandle CliHandle, UINT4 u4AdminStatus)
{
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal;
    UNUSED_PARAM (CliHandle);
	 
    i1RetVal = nmhTestv2FsMIRtmRouteLeakStatus (&u4ErrorCode,
                (INT4)u4AdminStatus);
	 
    if (i1RetVal == SNMP_FAILURE)
    {
          return (CLI_FAILURE);
    }

    i1RetVal = nmhSetFsMIRtmRouteLeakStatus ((INT4)u4AdminStatus);
    if (i1RetVal == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
	
    return (CLI_SUCCESS);
}
/*********************************************************************
*  Function Name : ShowRtmControlInfoInCxt
*  Description   : Calls Low level GetFirst/Next Routines and
                    displays fsRrdControlTable.
*                  
*  Input(s)      : None.
*  Output(s)     : None 
*  Return Values : None. 
**********************************************************************/
INT4
ShowRtmControlInfoInCxt (tCliHandle CliHandle, UINT4 u4RtmCxtId)
{
    UINT4               u4IpAddr = 0, u4Range = 0;
    UINT4               u4CurrentIpAddr = 0, u4CurrentRange = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4SrcProto = 0, i4DestProto = 0, i4Flag = 0;
    INT4                i4RowStatus = 0, i4RetVal = 0;
    CHR1               *pu1IpAddr = NULL;
    UINT1               u1MoreProto = TRUE;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4CurrentRtmCxtId = VCM_INVALID_VC;
    UINT4               u4ContextId;
    UINT1               au1RtmCxtName[VCM_ALIAS_MAX_LEN];

    if (u4RtmCxtId == VCM_INVALID_VC)
    {
        u4ShowAllCxt = TRUE;
        i4RetVal = nmhGetFirstIndexFsMIRrdControlTable
            ((INT4 *) &u4RtmCxtId, &u4IpAddr, &u4Range);
    }
    else
    {
        u4ContextId = u4RtmCxtId;
        i4RetVal = nmhGetNextIndexFsMIRrdControlTable
            ((INT4) u4ContextId, (INT4 *) &u4RtmCxtId,
             u4CurrentIpAddr, &u4IpAddr, u4CurrentRange, &u4Range);

        if (u4ContextId != u4RtmCxtId)
        {
            /*No entries for this context exists */
            return CLI_SUCCESS;
        }
    }

    while (i4RetVal == SNMP_SUCCESS)
    {
        if (u4CurrentRtmCxtId != u4RtmCxtId)
        {
            UtilRtmGetVcmAliasName (u4RtmCxtId, au1RtmCxtName);
            CliPrintf (CliHandle, "\r\n\nVRF  Name:  %s\r\n", au1RtmCxtName);

            /*display the header */
            CliPrintf (CliHandle,
                       "\rDestination      Range               SrcProto    DestProto          Flag  \r\n");

            CliPrintf (CliHandle,
                       "-----------      -----               --------    -----------        ----  \r\n");

        }

        nmhGetFsMIRrdControlSourceProto ((INT4) u4RtmCxtId, u4IpAddr, u4Range,
                                         &i4SrcProto);

        nmhGetFsMIRrdControlDestProto ((INT4) u4RtmCxtId, u4IpAddr, u4Range,
                                       &i4DestProto);

        nmhGetFsMIRrdControlRouteExportFlag ((INT4) u4RtmCxtId, u4IpAddr,
                                             u4Range, &i4Flag);

        nmhGetFsMIRrdControlRowStatus ((INT4) u4RtmCxtId, u4IpAddr, u4Range,
                                       &i4RowStatus);

        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4IpAddr);

        CliPrintf (CliHandle, "%-16s", pu1IpAddr);

        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4Range);

        CliPrintf (CliHandle, " %-16s", pu1IpAddr);

        CliPrintf (CliHandle, "    %-12s", paProtoTable[i4SrcProto]);

        u1MoreProto = TRUE;

        if (i4DestProto == 0)
        {
            CliPrintf (CliHandle, "%-14s", "others");
        }

        if (i4DestProto == RTM_ALL_RPS_MASK)
        {
            CliPrintf (CliHandle, "%-14s", "all");
        }
        else
        {
            if (i4DestProto & RTM_RIP_MASK)
            {
                CliPrintf (CliHandle, "%-7s", "rip");
                u1MoreProto = FALSE;
            }

            if (i4DestProto & RTM_BGP_MASK)
            {
                CliPrintf (CliHandle, "%-7s", "bgp");
                if (u1MoreProto == FALSE)
                {
                    u1MoreProto = TRUE;
                }
                else
                {
                    u1MoreProto = FALSE;
                }
            }

            if (i4DestProto & RTM_OSPF_MASK)
            {
                CliPrintf (CliHandle, "%-7s", "ospf");
                if (u1MoreProto == FALSE)
                {
                    u1MoreProto = TRUE;
                }
                else
                {
                    u1MoreProto = FALSE;
                }
            }
            if (u1MoreProto == FALSE)
            {
                CliPrintf (CliHandle, "%-7s", "");
            }
        }

        u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                            "     %-6s\r\n",
                                            ((i4Flag == 1) ? "Allow" : "Deny"));

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt so break */
            break;
        }

        u4CurrentRtmCxtId = u4RtmCxtId;
        u4CurrentIpAddr = u4IpAddr;
        u4CurrentRange = u4Range;

        i4RetVal = nmhGetNextIndexFsMIRrdControlTable
            ((INT4) u4CurrentRtmCxtId, (INT4 *) &u4RtmCxtId,
             u4CurrentIpAddr, &u4IpAddr, u4CurrentRange, &u4Range);

        if ((u4CurrentRtmCxtId != u4RtmCxtId) && (u4ShowAllCxt == FALSE))
        {
            /*Finished displaying entries belonging to one VRF */
            break;
        }

    }                            /*end of while */

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : ShowRrdStatusInCxt
*  Description   : Calls Low level GetFirst/Next Routines and
                    displays fsRrdRegnTable.
*                  
*  Input(s)      : None.
*  Output(s)     : ppRespMsg - filled up table entries
*  Return Values : None. 
**********************************************************************/
INT4
ShowRrdStatusInCxt (tCliHandle CliHandle, UINT4 u4RtmCxtId)
{
    INT4                i4RtProtoId = 0;
    INT4                i4CurrentRtProtoId = 0;
    CHR1               *pu1IpAddr = NULL;
    INT4                i4AreaAllow, i4ExtAllow;
    tSNMP_OCTET_STRING_TYPE OctectStrProtoName;
    INT4                i4RTMStatus = 0;
    INT4                i4RtrASNo = 0;
    INT4                i4RetVal = 0;
    UINT4               u4RtrId = 0;
    UINT4               u4ShowAllCxt = FALSE;
    UINT4               u4CurrentRtmCxtId = 0;
    UINT4               u4RrdCxtId = 0;
    UINT4               u4CurrentRrdCxtId = RTM_INVALID_CXT_ID;
    UINT4               u4PagingStatus = CLI_FAILURE;
    UINT4               u4ThrottleLimit = 0;
    UINT1               au1RtmCxtName[VCM_ALIAS_MAX_LEN];
    UINT1               au1OctetList[RRD_MAX_ADDR_BUFFER];

    OctectStrProtoName.pu1_OctetList = au1OctetList;

    /*If valid VRF Id is not passed, then display all contexts' information */
    if (u4RtmCxtId == RTM_INVALID_CXT_ID)
    {
        u4ShowAllCxt = TRUE;
    }

    if (u4ShowAllCxt == TRUE)
    {
        if (nmhGetFirstIndexFsMIRtmTable ((INT4 *) &u4RtmCxtId) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);

        }
    }
    if (u4ShowAllCxt == TRUE)
    {
        i4RetVal = nmhGetFirstIndexFsMIRrdRoutingProtoTable
            ((INT4 *) &u4RrdCxtId, &i4RtProtoId);
    }
    else
    {
        u4CurrentRrdCxtId = u4RtmCxtId;
        i4RetVal = nmhGetNextIndexFsMIRrdRoutingProtoTable
            ((INT4) u4CurrentRrdCxtId, (INT4 *) &u4RrdCxtId,
             i4CurrentRtProtoId, &i4RtProtoId);
        u4CurrentRrdCxtId = RTM_INVALID_CXT_ID;
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    nmhGetFsMIRtmThrottleLimit (&u4ThrottleLimit);
    CliPrintf (CliHandle, "\r\nCurrent Throttle Limit:  %u", u4ThrottleLimit);
    do
    {
        u4CurrentRtmCxtId = u4RtmCxtId;

        nmhGetFsMIRrdRouterId ((INT4) u4RtmCxtId, &u4RtrId);

        nmhGetFsMIRrdRouterASNumber ((INT4) u4RtmCxtId, &i4RtrASNo);

        nmhGetFsMIRrdAdminStatus ((INT4) u4RtmCxtId, &i4RTMStatus);

        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RtrId);

        UtilRtmGetVcmAliasName (u4RtmCxtId, au1RtmCxtName);
        CliPrintf (CliHandle, "\r\n\nVRF  Name:  %s\r\n", au1RtmCxtName);

        CliPrintf (CliHandle, "\rRouter ID is %-16s\r\n", pu1IpAddr);

        CliPrintf (CliHandle, "AS Number is %-d \r\n", i4RtrASNo);

        CliPrintf (CliHandle, "Current State is");

        CliPrintf (CliHandle, " %-14s\r\n",
                   ((i4RTMStatus == 1) ? "enabled" : "disabled"));

        CliPrintf (CliHandle, "Force option");

        while (i4RetVal == SNMP_SUCCESS)
        {

            if (u4CurrentRrdCxtId != u4RrdCxtId)
            {
                UtilRtmGetVcmAliasName (u4RrdCxtId, au1RtmCxtName);
                /*display the header */
                CliPrintf (CliHandle,
                           "\r\nProtoName    OspfAreaRoutes    OspfExtRoutes\r\n");
                CliPrintf (CliHandle,
                           "---------    --------------    -------------\r\n");
            }

            nmhGetFsMIRrdRoutingProtoTaskIdent
                ((INT4) u4RtmCxtId, i4RtProtoId, &(OctectStrProtoName));

            nmhGetFsMIRrdAllowOspfAreaRoutes
                ((INT4) u4RtmCxtId, i4RtProtoId, &i4AreaAllow);

            nmhGetFsMIRrdAllowOspfExtRoutes
                ((INT4) u4RtmCxtId, i4RtProtoId, &i4ExtAllow);

            CliPrintf (CliHandle, "%-13s", paProtoTable[i4RtProtoId]);

            CliPrintf (CliHandle, "%-18s",
                       ((i4AreaAllow == 1) ? "Enable" : "Disable"));

            u4PagingStatus = (UINT4) CliPrintf (CliHandle, "%-18s\r\n",
                                                ((i4ExtAllow ==
                                                  1) ? "Enable" : "Disable"));

            if (u4PagingStatus == CLI_FAILURE)
            {
                /* 
                 * User pressed 'q' at more prompt, no more print required, exit 
                 * 
                 * Setting the Count to -1 will result in not displaying the
                 * count of the entries.
                 */
                u4ShowAllCxt = FALSE;
            }

            u4CurrentRrdCxtId = u4RrdCxtId;
            i4CurrentRtProtoId = i4RtProtoId;

            i4RetVal = nmhGetNextIndexFsMIRrdRoutingProtoTable
                ((INT4) u4CurrentRrdCxtId, (INT4 *) &u4RrdCxtId,
                 i4CurrentRtProtoId, &i4RtProtoId);

            if (u4CurrentRrdCxtId != u4RrdCxtId)
            {
                /*Finished displaying entries belonging to one VRF */
                break;
            }

        }                        /*end of while */
        if (u4ShowAllCxt == FALSE)
        {
            /*If VRF ID is specified */
            break;
        }
    }
    while (nmhGetNextIndexFsMIRtmTable ((INT4) u4CurrentRtmCxtId,
                                        (INT4 *) &u4RtmCxtId) != SNMP_FAILURE);

    CliPrintf (CliHandle, "\r\n");
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : ShowRoutingProtocolInfoInCxt
*  Description   : Display information about current running Routing Protocols.
*                  
*  Input(s)      : ContextId
*  Output(s)     : None
*  Return Values : None. 
**********************************************************************/
INT4
ShowRoutingProtocolInfoInCxt (tCliHandle CliHandle, UINT4 u4RtmCxtId)
{
#ifdef RIP_WANTED
    if (u4RtmCxtId == VCM_INVALID_VC)
    {
        ShowRipProtocolInfo (CliHandle);
    }
    else
    {
        ShowRipProtocolInfoInCxt (CliHandle, (INT4) u4RtmCxtId);
    }
#endif

#ifdef OSPF_WANTED
    if (u4RtmCxtId == VCM_INVALID_VC)
    {
        ShowOspfProtocolInfo (CliHandle);
    }
    else
    {
        ShowOspfProtocolInfoInCxt (CliHandle, u4RtmCxtId);
    }
#endif

#ifdef BGP_WANTED
    if (u4RtmCxtId == VCM_INVALID_VC)
    {
        ShowBgpProtocolInfo (CliHandle);
    }
    else
    {
        ShowBgpProtocolInfoInCxt (CliHandle, u4RtmCxtId);
    }
#endif

#ifdef ISIS_WANTED
    if (u4RtmCxtId == VCM_INVALID_VC)
    {
        ShowIsisProtocolsInfo (CliHandle);
    }
    else
    {
        ShowIsisProtocolsInfoInCxt (CliHandle, u4RtmCxtId);
    }
#endif

#if (!((RIP_WANTED) ||  (OSPF_WANTED)) || (BGP_WANTED))
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4RtmCxtId);
#endif
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : CliRrdModDefControlEntry
*  Description   : Calls Low level Set Routines for modifying RRD
*                  control table default entry.
*                  
*  Input(s)      : i4ControlFlag -Status to be set for Default Entry 
*  Output(s)     : ppRespMsg - filled up with error Message, if any. 
*  Return Values : None. 
**********************************************************************/
VOID
CliRrdModDefCtrlEntry (INT4 i4ControlFlag)
{
    INT4                i4Flag = 0;

    if (nmhGetFsRrdControlRouteExportFlag (RRD_CLI_DEF_DEST,
                                           RRD_CLI_DEF_RANGE,
                                           &i4Flag) == SNMP_FAILURE)
    {
        return;
    }
    if (i4ControlFlag != i4Flag)
    {
        nmhSetFsRrdControlRouteExportFlag (RRD_CLI_DEF_DEST,
                                           RRD_CLI_DEF_RANGE, i4ControlFlag);
        nmhSetFsRrdControlRowStatus (RRD_CLI_DEF_DEST,
                                     RRD_CLI_DEF_RANGE, ACTIVE);
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RrdShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays current rrd configuration   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4ContextId - The VRF Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
RrdShowRunningConfig (tCliHandle CliHandle, INT4 i4ContextId,UINT4 u4Module)
{
    CliRegisterLock (CliHandle, RtmProtocolLock, RtmProtocolUnlock);
    RTM_PROT_LOCK ();

    RrdShowRunningConfigScalars (CliHandle, i4ContextId, u4Module);
    RrdShowRunningConfigTable (CliHandle, i4ContextId);

    RTM_PROT_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RrdShowRunningConfigScalars                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays current rrd configuration   */
/*                        for scalar objects                                 */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4ContextId - The VRF Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
RrdShowRunningConfigScalars (tCliHandle CliHandle, INT4 i4ContextId, UINT4 u4Module)
{
    UINT4               u4RetVal = 0;
    UINT4               u4ThrottleLimit = 0;
    INT4                i4EcmpAcrossProtocol = 0;
    INT4                i4RtmRouteLeakStatus = 0;
    UINT1               u1RetVal = 1;
    CHR1               *pu1IpAddr = NULL;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               u1BangStatus = FALSE;
 
    if ((UINT4) i4ContextId != RTM_DEFAULT_CXT_ID)
    {
        VcmGetAliasName ((UINT4) i4ContextId, au1ContextName);
    }
    nmhGetFsMIRrdRouterASNumber (i4ContextId, (INT4 *) &u4RetVal);
    if (u4RetVal)
    {
        u1BangStatus = TRUE;

        if ((u4Module == ISS_BGP_SHOW_RUNNING_CONFIG ) || (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
        {
            CliPrintf (CliHandle, "\r\nas-num %d", u4RetVal);
            if (i4ContextId != RTM_DEFAULT_CXT_ID)
            {
                CliPrintf (CliHandle, " vrf %s", au1ContextName);
            }
        }
        CliPrintf (CliHandle, "\r\n");
    }

    nmhGetFsMIRrdRouterId (i4ContextId, &u4RetVal);
    if (u4RetVal)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RetVal);
        RtmGetRtrIdConfigType (i4ContextId, &u1RetVal);
        if (u1RetVal == RTM_RTRID_CONFIG_TYPE_USER)
        {
            u1BangStatus = TRUE;

            if ((u4Module == ISS_BGP_SHOW_RUNNING_CONFIG ) || (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
            {
                CliPrintf (CliHandle, "router-id %s", pu1IpAddr);
                if (i4ContextId != RTM_DEFAULT_CXT_ID)
                {
                    CliPrintf (CliHandle, " vrf %s", au1ContextName);
                }
            }
        }
        CliPrintf (CliHandle, "\r\n");
    }
    nmhGetFsMIRtmThrottleLimit (&u4ThrottleLimit);
    if (u4ThrottleLimit != RTM_MAX_PROTOMASK_ROUTES)
    {  
        u1BangStatus = TRUE;

        CliPrintf (CliHandle, "\r\nipv4 route throttle limit %d",
                   u4ThrottleLimit);
        CliPrintf (CliHandle, "\r\n");

    }

    nmhGetFsMIEcmpAcrossProtocolAdminStatus (&i4EcmpAcrossProtocol);
    if (i4EcmpAcrossProtocol != RTM_ECMP_ACROSS_PROTO_ENABLED)
    {  
        u1BangStatus = TRUE;

        CliPrintf (CliHandle, "\r\nip ecmp-across-protocol disable\r\n");
    }
 
    if ((UINT4)i4ContextId == RTM_DEFAULT_CXT_ID)
    { 
        nmhGetFsMIRtmRouteLeakStatus (&i4RtmRouteLeakStatus);
        if (i4RtmRouteLeakStatus != RTM_VRF_ROUTE_LEAK_DISABLED)
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "\r\nset vrf-route-leaking enable\r\n");
        }
    }

    if ( (UINT4)i4ContextId == RTM_DEFAULT_CXT_ID)
    {
           u4RetVal = 0;
           nmhGetFsRtmMaximumStaticRoutes(&u4RetVal);
           if (u4RetVal != DEF_RTM_STATIC_ROUTE_ENTRIES)
           { 
                   u1BangStatus = TRUE;

                   CliPrintf (CliHandle, "\r\nip maximum-routes static %d",
                                   u4RetVal);
                   CliPrintf (CliHandle, "\r\n");
           }

           u4RetVal = 0;
           nmhGetFsRtmMaximumBgpRoutes(&u4RetVal);
           if (u4RetVal != DEF_RTM_BGP_ROUTE_ENTRIES)
           {      
                   u1BangStatus = TRUE;

                   CliPrintf (CliHandle, "\r\nip maximum-routes bgp %d",
                                   u4RetVal);
                   CliPrintf (CliHandle, "\r\n");
           }

           u4RetVal = 0;
           nmhGetFsRtmMaximumRipRoutes(&u4RetVal);
           if (u4RetVal != DEF_RTM_RIP_ROUTE_ENTRIES)
           {
                   u1BangStatus = TRUE;

                   CliPrintf (CliHandle, "\r\nip maximum-routes rip %d",
                                   u4RetVal);
                   CliPrintf (CliHandle, "\r\n");
           }

           u4RetVal = 0;
           nmhGetFsRtmMaximumOspfRoutes(&u4RetVal);
           if (u4RetVal != DEF_RTM_OSPF_ROUTE_ENTRIES)
           {
                   u1BangStatus = TRUE;

                   CliPrintf (CliHandle, "\r\nip maximum-routes ospf %d",
                                   u4RetVal);
                   CliPrintf (CliHandle, "\r\n");
           }

           u4RetVal = 0;
           nmhGetFsRtmMaximumISISRoutes(&u4RetVal);
           if (u4RetVal != DEF_RTM_ISIS_ROUTE_ENTRIES)
           {
                   u1BangStatus = TRUE;
                   CliPrintf (CliHandle, "\r\nip maximum-routes isis %d",
                                   u4RetVal);
                   CliPrintf (CliHandle, "\r\n");
           }
    }
    if(u1BangStatus == TRUE)
    {
        CliSetBangStatus(CliHandle, TRUE);
    } 
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RrdShowRunningConfigTable                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays current rrd configuration   */
/*                        for table objects                                  */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4ContextId - The VRF Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
RrdShowRunningConfigTable (tCliHandle CliHandle, INT4 i4ContextId)
{
    UINT4               u4RetVal = 0;
    UINT4               u4RetVal2 = 0;
    UINT4               u4RetVal3 = 0;
    CHR1               *pu1IpAddr = NULL;
    UINT1               u1MoreProto = TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4NextRetVal = 0;
    UINT4               u4NextRetVal2 = 0;
    INT4                i4NextCxt = 0;
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];
    UINT1               u1BangStatus = FALSE;
    if ((UINT4) i4ContextId != RTM_DEFAULT_CXT_ID)
    {
        VcmGetAliasName ((UINT4) i4ContextId, au1ContextName);
    }

    while (nmhGetNextIndexFsMIRrdRoutingProtoTable (i4ContextId, &i4NextCxt,
                                                    (INT4) u4RetVal,
                                                    (INT4 *) &u4NextRetVal)
           == SNMP_SUCCESS)
    {
        if (i4ContextId != i4NextCxt)
        {
            break;
        }
        u4RetVal = u4NextRetVal;

        nmhGetFsMIRrdAllowOspfAreaRoutes (i4ContextId, (INT4) u4RetVal,
                                          (INT4 *) &u4RetVal2);
        if ((u4RetVal2 == 2) &&
            ((STRCMP (paProtoTable[u4RetVal], "bgp") == 0) ||
             (STRCMP (paProtoTable[u4RetVal], "rip") == 0)))
        {
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "no export ospf ");

            if (i4ContextId != RTM_DEFAULT_CXT_ID)
            {
                CliPrintf (CliHandle, "vrf %s ", au1ContextName);
            }
            CliPrintf (CliHandle, "area-route %s\r\n", paProtoTable[u4RetVal]);
        }

        nmhGetFsMIRrdAllowOspfExtRoutes (i4ContextId, (INT4) u4RetVal,
                                         (INT4 *) &u4RetVal2);
        if ((u4RetVal2 == 2) &&
            ((STRCMP (paProtoTable[u4RetVal], "bgp") == 0) ||
             (STRCMP (paProtoTable[u4RetVal], "rip") == 0)))
        {   
            u1BangStatus = TRUE;
            CliPrintf (CliHandle, "no export ospf ");

            if (i4ContextId != RTM_DEFAULT_CXT_ID)
            {
                CliPrintf (CliHandle, "vrf %s ", au1ContextName);
            }
            CliPrintf (CliHandle, "external-route %s", paProtoTable[u4RetVal]);
        }
        u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r\n");
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
    }

    u4RetVal = 0;
    u4RetVal2 = 0;
    while (nmhGetNextIndexFsMIRrdControlTable (i4ContextId, &i4NextCxt,
                                               u4RetVal, &u4NextRetVal,
                                               u4RetVal2, &u4NextRetVal2)
           == SNMP_SUCCESS)
    {
        if (i4ContextId != i4NextCxt)
        {
            break;
        }
        u4RetVal = u4NextRetVal;
        u4RetVal2 = u4NextRetVal2;

        nmhGetFsMIRrdControlRowStatus (i4ContextId, u4RetVal, u4RetVal2,
                                       (INT4 *) &u4RetVal3);
        if (u4RetVal3 == ACTIVE)
        {
            if ((u4RetVal == RRD_CLI_DEF_DEST) &&
                (u4RetVal2 == RRD_CLI_DEF_RANGE))
            {
                nmhGetFsMIRrdControlRouteExportFlag (i4ContextId, u4RetVal,
                                                     u4RetVal2,
                                                     (INT4 *) &u4RetVal3);
                if (u4RetVal3 != 1)
                {
                    u1BangStatus = TRUE;
                    CliPrintf (CliHandle, "default redistribute-policy");
                    if (i4ContextId != RTM_DEFAULT_CXT_ID)
                    {
                        CliPrintf (CliHandle, " vrf %s", au1ContextName);
                    }
                    if (u4RetVal3 != 1)
                    {
                        CliPrintf (CliHandle, " deny\r\n");
                    }
                }

            }
            else
            {
                CliPrintf (CliHandle, "redistribute-policy");
                if (i4ContextId != RTM_DEFAULT_CXT_ID)
                {
                    CliPrintf (CliHandle, " vrf %s", au1ContextName);
                }
                nmhGetFsMIRrdControlRouteExportFlag (i4ContextId, u4RetVal,
                                                     u4RetVal2,
                                                     (INT4 *) &u4RetVal3);

                if (u4RetVal3 == 1)
                {
                    CliPrintf (CliHandle, " permit");
                }
                else
                {
                    CliPrintf (CliHandle, " deny");
                }

                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RetVal);
                CliPrintf (CliHandle, " %s", pu1IpAddr);

                CLI_CONVERT_IPADDR_TO_STR (pu1IpAddr, u4RetVal2);
                CliPrintf (CliHandle, " %s", pu1IpAddr);

                nmhGetFsMIRrdControlSourceProto (i4ContextId, u4RetVal,
                                                 u4RetVal2,
                                                 (INT4 *) &u4RetVal3);
                if (u4RetVal3 == 2)
                {
                    CliPrintf (CliHandle, " connected");
                }
                else
                {
                    CliPrintf (CliHandle, " %s", paProtoTable[u4RetVal3]);
                }

                nmhGetFsMIRrdControlDestProto (i4ContextId, u4RetVal, u4RetVal2,
                                               (INT4 *) &u4RetVal3);

                u1MoreProto = TRUE;

                if (u4RetVal3 == RTM_ALL_RPS_MASK)
                {
                    CliPrintf (CliHandle, " %s\r\n", "all");
                }
                else
                {
                    if (u4RetVal3 & RTM_RIP_MASK)
                    {
                        CliPrintf (CliHandle, " %s\r\n", "rip");
                        u1MoreProto = FALSE;
                    }

                    if (u4RetVal3 & RTM_BGP_MASK)
                    {
                        CliPrintf (CliHandle, " %s\r\n", "bgp");
                        if (u1MoreProto == FALSE)
                        {
                            u1MoreProto = TRUE;
                        }
                        else
                        {
                            u1MoreProto = FALSE;
                        }
                    }

                    if (u4RetVal3 & RTM_OSPF_MASK)
                    {
                        u4PagingStatus =
                            (UINT4) CliPrintf (CliHandle, " %s\r\n", "ospf");
                        if (u1MoreProto == FALSE)
                        {
                            u1MoreProto = TRUE;
                        }
                        else
                        {
                            u1MoreProto = FALSE;
                        }
                    }
                }
            }
            u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r\n");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }
        }
    }

    if(u1BangStatus == TRUE)
    {
        CliSetBangStatus(CliHandle, TRUE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : RtmCliGetCommandType                               */
/*                                                                           */
/*     DESCRIPTION      : This function returns the command type as          */
/*                        configuration command or show command              */
/*                                                                           */
/*     INPUT            : uCommand - command identifer                       */
/*                                                                           */
/*     OUTPUT           : pi1IsShowCommand                                   */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
RtmCliGetCommandType (UINT4 u4Command, INT1 *pi1IsShowCommand)
{
    INT4                i4RetStatus = 0;
    switch (u4Command)
    {
        case CLI_IP_SHOW_PROTOCOLS:
        case CLI_RRD_SHOW_CONTROL_INFO:
        case CLI_RRD_SHOW_MAX_ROUTE:
        case CLI_RRD_SHOW_ECMP_ACROSS_PROTOCOL:
        case CLI_RRD_SHOW_STATUS:
#ifdef RM_WANTED
        case CLI_RRD_RED_DYN_INFO:
#endif
            /* Set show command as true */
            *pi1IsShowCommand = TRUE;
            i4RetStatus = CLI_SUCCESS;
            break;
        case CLI_RRD_AS_NUM:
        case CLI_RRD_ROUTER_ID:
        case CLI_RRD_EXPORT_OSPF:
        case CLI_RRD_NO_EXPORT_OSPF:
        case CLI_RRD_DEF_CONTROL_STATUS:
        case CLI_RRD_REDISTRIBUTE_POLICY:
        case CLI_RRD_NO_REDISTRIBUTE_POLICY:
        case CLI_RRD_THROT_VALUE:
        case CLI_RRD_MAX_ROUTE:
        case CLI_RRD_AS_ROUTERID_FORCE:
        case CLI_ECMP_ACROSS_PROTOCOL:
        case CLI_VRF_ROUTE_LEAK:
            /* Set show command as False */
            *pi1IsShowCommand = FALSE;
            i4RetStatus = CLI_SUCCESS;
            break;
        default:
            /* Invalid Command identifier */
            i4RetStatus = CLI_FAILURE;
            break;
    }
    return i4RetStatus;
}

/*****************************************************************************/
/* Function Name      : RtmCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RTM_SUCCESS/RTM_FAILURE                              */
/*****************************************************************************/
INT4
RtmCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return RTM_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) RTM_AUDIT_SHOW_CMD)
        == CLI_FAILURE)
    {
        return RTM_FAILURE;
    }
    return RTM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RtmCliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RTM_SUCCESS/RTM_FAILURE                              */
/*****************************************************************************/
INT4
RtmCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT4                i4Fd;
    UINT2               u2CkSum = 0;
    INT2                i2ReadLen;
    INT1                ai1Buf[RTM_CLI_MAX_GROUPS_LINE_LEN + 1];

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, RTM_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return RTM_FAILURE;
    }
    MEMSET (ai1Buf, 0, RTM_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (RtmCliReadLineFromFile (i4Fd, ai1Buf, RTM_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != RTM_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);

            MEMSET (ai1Buf, '\0', RTM_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return RTM_FAILURE;
    }
    return RTM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : RtmCliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RTM_SUCCESS/RTM_FAILURE                              */
/*****************************************************************************/
INT1
RtmCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;
    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (RTM_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (RTM_CLI_EOF);
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RtmSetMaxRoute                                         */
/*                                                                           */
/* Description      : This function configures the maximum number of routes  */
/*                    to be programmed in RTM on per protocol basis          */
/* Input Parameters :                                                        */
/*                    1. u1ProtoType - Protocol type                         */
/*                    2. u4RouteDest - Maximum number of routes              */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4
RtmSetMaxRoute(UINT1 u1ProtoType, UINT4 u4MaxRTMRoute)
{
       UINT4 u4ErrCode = 0;

       switch (u1ProtoType)
       {
               case BGP_ID:
                           if (nmhTestv2FsRtmMaximumBgpRoutes(&u4ErrCode , u4MaxRTMRoute) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if(nmhSetFsRtmMaximumBgpRoutes(u4MaxRTMRoute) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                       break;

               case OSPF_ID:

                           if (nmhTestv2FsRtmMaximumOspfRoutes(&u4ErrCode , u4MaxRTMRoute) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if(nmhSetFsRtmMaximumOspfRoutes(u4MaxRTMRoute)== SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                       break;
               case RIP_ID:

                           if (nmhTestv2FsRtmMaximumRipRoutes(&u4ErrCode , u4MaxRTMRoute) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if (nmhSetFsRtmMaximumRipRoutes(u4MaxRTMRoute)== SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }

                       break;

               case STATIC_ID:

                           if (nmhTestv2FsRtmMaximumStaticRoutes(&u4ErrCode , u4MaxRTMRoute) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if (nmhSetFsRtmMaximumStaticRoutes(u4MaxRTMRoute) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }

                       break;
              case ISIS_ID: 
                           if (nmhTestv2FsRtmMaximumISISRoutes(&u4ErrCode , u4MaxRTMRoute) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }
                           if (nmhSetFsRtmMaximumISISRoutes(u4MaxRTMRoute) == SNMP_FAILURE)
                             {
                                    return CLI_FAILURE;
                             }

                       break;
              default:
                      break;

       }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RtmShowRouteMemReservation                             */
/*                                                                           */
/* Description      : This function configures the maximum number of routes  */
/*                    to be programmed in RTM on per protocol basis          */
/* Input Parameters :                                                        */
/*                    1. CliHandle - CliContext ID                           */
/*                                                                           */
/* Output Parameters : None                                                  */
/*                                                                           */
/* Return Value     :  CLI_SUCCESS/CLI_FAILURE                               */
/*****************************************************************************/
INT4 RtmShowRouteMemReservation ( tCliHandle CliHandle)
{

  UINT4 u4TotalBlks = 0;
  UINT4 u4BlksReserved = 0;
  UINT4 u4Temp = 0;
  INT1  i1Status = SNMP_FAILURE;

  u4TotalBlks = RtmGetTotalMemoryBlocksAllocated();

  CliPrintf (CliHandle, "Protocol Type     Blocks reserved\r\n");
  CliPrintf (CliHandle, "--------------------------------------\r\n");
  CliPrintf (CliHandle, "Total             %-32d\r\n",u4TotalBlks);
  CliPrintf (CliHandle, "connected         %-32d\r\n",SYS_DEF_MAX_INTERFACES);
  i1Status = nmhGetFsRtmMaximumStaticRoutes (&u4Temp);
  CliPrintf (CliHandle, "static            %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  i1Status = nmhGetFsRtmMaximumRipRoutes (&u4Temp);
  CliPrintf (CliHandle, "rip               %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  i1Status = nmhGetFsRtmMaximumBgpRoutes (&u4Temp );
  CliPrintf (CliHandle, "bgp               %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  i1Status = nmhGetFsRtmMaximumOspfRoutes (&u4Temp);
  CliPrintf (CliHandle, "ospf              %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;
  i1Status = nmhGetFsRtmMaximumISISRoutes (&u4Temp);
  CliPrintf (CliHandle, "isis              %-32d\r\n",u4Temp);
  u4BlksReserved += u4Temp;
  u4Temp = 0;

  u4BlksReserved += SYS_DEF_MAX_INTERFACES;
  u4Temp = u4TotalBlks - u4BlksReserved;
  CliPrintf (CliHandle, "Free Blocks       %-32d\r\n\r\n",u4Temp);

  UNUSED_PARAM(i1Status);
  return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function Name    : RtmShowEcmpAcrossProtocols                             */
/*                                                                           */
/* Description      : This function configures ecmp-across-protocols status  */
/*                                                                           */
/* Input Parameters : CliHandle - Cli Context ID                             */
/*                                                                           */
/* Output Parameters: None                                                   */
/*                                                                           */
/* Return Value     : CLI_SUCCESS/CLI_FAILURE                                */
/*****************************************************************************/

VOID RtmShowEcmpAcrossProtocols(tCliHandle CliHandle)
{

    /* Global variable responsible for decission making*/
    if (gu1EcmpAcrossProtocol == RTM_ECMP_ACROSS_PROTO_DISABLED)
    {
        CliPrintf (CliHandle, "ECMP across protocol Operational Status - DISABLED\r\n\n");
    }
    else
    {
        CliPrintf (CliHandle, "ECMP across protocol Operational Status - ENABLED\r\n\n");
    }

    /* Stored MIB object which will be taking affect after MSR*/
    if (gRtmGlobalInfo.u1EcmpAcrossProtocol == RTM_ECMP_ACROSS_PROTO_DISABLED)
    {
        CliPrintf (CliHandle, "ECMP across protocol Admin Status       - DISABLED\r\n\n");
    }
    else
    {
        CliPrintf (CliHandle, "ECMP across protocol Admin Status       - ENABLED\r\n\n");
    }
}

#endif
