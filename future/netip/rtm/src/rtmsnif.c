/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmsnif.c,v 1.35 2016/12/27 12:36:00 siva Exp $
 *
 * Description:This file contains low level routines of the 
 *             RTM module.                                  
 *
 *******************************************************************/

#include "rtminc.h"
#include "rrdcli.h"
#include "stdipwr.h"
#include "stdiplow.h"
#include "fsrtmwr.h"
#include "fsrtmlw.h"
#include "fsmirtcli.h"

/****************************************************************************
 Function    :  nmhGetFsRrdRouterId
 Input       :  The Indices

                The Object 
                retValFsRrdRouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdRouterId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdRouterId (UINT4 *pu4RetValFsRrdRouterId)
#else
INT1
nmhGetFsRrdRouterId (pu4RetValFsRrdRouterId)
     UINT4              *pu4RetValFsRrdRouterId;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsRrdRouterId = pRtmCxt->u4RouterId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrdFilterByOspfTag
 Input       :  The Indices

                The Object 
                retValFsRrdFilterByOspfTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdFilterByOspfTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdFilterByOspfTag (INT4 *pi4RetValFsRrdFilterByOspfTag)
#else
INT1
nmhGetFsRrdFilterByOspfTag (pi4RetValFsRrdFilterByOspfTag)
     INT4               *pi4RetValFsRrdFilterByOspfTag;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRrdFilterByOspfTag =
        (INT4) pRtmCxt->RtmConfigInfo.u1RrdFilterByOspfTag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrdFilterOspfTag
 Input       :  The Indices

                The Object 
                retValFsRrdFilterOspfTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdFilterOspfTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdFilterOspfTag (INT4 *pi4RetValFsRrdFilterOspfTag)
#else
INT1
nmhGetFsRrdFilterOspfTag (pi4RetValFsRrdFilterOspfTag)
     INT4               *pi4RetValFsRrdFilterOspfTag;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRrdFilterOspfTag = (INT4) pRtmCxt->RtmConfigInfo.u4OspfTagValue;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrdFilterOspfTagMask
 Input       :  The Indices

                The Object 
                retValFsRrdFilterOspfTagMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdFilterOspfTagMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdFilterOspfTagMask (INT4 *pi4RetValFsRrdFilterOspfTagMask)
#else
INT1
nmhGetFsRrdFilterOspfTagMask (pi4RetValFsRrdFilterOspfTagMask)
     INT4               *pi4RetValFsRrdFilterOspfTagMask;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRrdFilterOspfTagMask =
        (INT4) pRtmCxt->RtmConfigInfo.u4OspfTagMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrdRouterASNumber
 Input       :  The Indices

                The Object 
                retValFsRrdRouterASNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdRouterASNumber ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdRouterASNumber (INT4 *pi4RetValFsRrdRouterASNumber)
#else
INT1
nmhGetFsRrdRouterASNumber (pi4RetValFsRrdRouterASNumber)
     INT4               *pi4RetValFsRrdRouterASNumber;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRrdRouterASNumber = (INT4) pRtmCxt->u2AsNumber;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrdAdminStatus
 Input       :  The Indices

                The Object 
                retValFsRrdAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdAdminStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdAdminStatus (INT4 *pi4RetValFsRrdAdminStatus)
#else
INT1
nmhGetFsRrdAdminStatus (pi4RetValFsRrdAdminStatus)
     INT4               *pi4RetValFsRrdAdminStatus;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRrdAdminStatus = (INT4) pRtmCxt->u1RrdAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRtmThrottleLimit
 Input       :  The Indices

                The Object 
                retValFsRtmThrottleLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRtmThrottleLimit (UINT4 *pu4RetValFsRtmThrottleLimit)
{
    *pu4RetValFsRtmThrottleLimit = gRtmGlobalInfo.u4ThrotLimit;
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhGetFsRtmMaximumBgpRoutes
 Input       :  The Indices

                The Object
                retValFsRtmMaximumBgpRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRtmMaximumBgpRoutes(UINT4 *pu4RetValFsRtmMaximumBgpRoutes)
{

    *pu4RetValFsRtmMaximumBgpRoutes  = (UINT4) gRtmGlobalInfo.u4MaxRTMBgpRoute;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsRtmMaximumOspfRoutes
 Input       :  The Indices

                The Object
                retValFsRtmMaximumOspfRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRtmMaximumOspfRoutes(UINT4 *pu4RetValFsRtmMaximumOspfRoutes)
{
  
   *pu4RetValFsRtmMaximumOspfRoutes = (UINT4) gRtmGlobalInfo.u4MaxRTMOspfRoute;
   return SNMP_SUCCESS;
    
}
/****************************************************************************
 Function    :  nmhGetFsRtmMaximumRipRoutes
 Input       :  The Indices

                The Object
                retValFsRtmMaximumRipRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRtmMaximumRipRoutes(UINT4 *pu4RetValFsRtmMaximumRipRoutes)
{

    *pu4RetValFsRtmMaximumRipRoutes = (UINT4) gRtmGlobalInfo.u4MaxRTMRipRoute;
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhGetFsRtmMaximumStaticRoutes
 Input       :  The Indices

                The Object
                retValFsRtmMaximumStaticRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRtmMaximumStaticRoutes(UINT4 *pu4RetValFsRtmMaximumStaticRoutes)
{

    *pu4RetValFsRtmMaximumStaticRoutes = (UINT4) gRtmGlobalInfo.u4MaxRTMStaticRoute;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRtmMaximumISISRoutes
 Input       :  The Indices

                The Object
                retValFsRtmMaximumISISRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsRtmMaximumISISRoutes(UINT4 *pu4RetValFsRtmMaximumISISRoutes)
{

    *pu4RetValFsRtmMaximumISISRoutes = (UINT4) gRtmGlobalInfo.u4MaxRTMIsisRoute;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRtmIpStaticRouteDistance
 Input       :  The Indices

                The Object
                retValFsRtmIpStaticRouteDistance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsRtmIpStaticRouteDistance(INT4 *pi4RetValFsRtmIpStaticRouteDistance)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRtmIpStaticRouteDistance = (INT4) pRtmCxt->u1IpDefaultDistance;
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhSetFsRtmMaximumBgpRoutes
 Input       :  The Indices

                The Object
                setValFsRtmMaximumBgpRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRtmMaximumBgpRoutes(UINT4 u4SetValFsRtmMaximumBgpRoutes)
{

    gRtmGlobalInfo.u4MaxRTMBgpRoute = u4SetValFsRtmMaximumBgpRoutes;
    UtilRtmIncMsrForRtmScalar (u4SetValFsRtmMaximumBgpRoutes,
                               FsMIRtmMaximumBgpRoutes,
                               (sizeof (FsMIRtmMaximumBgpRoutes) /
                                sizeof (UINT4)), 'p' );
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhSetFsRtmMaximumOspfRoutes
 Input       :  The Indices

                The Object
                setValFsRtmMaximumOspfRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRtmMaximumOspfRoutes(UINT4 u4SetValFsRtmMaximumOspfRoutes)
{
    gRtmGlobalInfo.u4MaxRTMOspfRoute = u4SetValFsRtmMaximumOspfRoutes;
    UtilRtmIncMsrForRtmScalar (u4SetValFsRtmMaximumOspfRoutes,
                               FsMIRtmMaximumOspfRoutes,
                               (sizeof (FsMIRtmMaximumOspfRoutes) /
                                sizeof (UINT4)), 'p' );
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRtmMaximumRipRoutes
 Input       :  The Indices

                The Object
                setValFsRtmMaximumRipRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRtmMaximumRipRoutes(UINT4 u4SetValFsRtmMaximumRipRoutes)
{
    gRtmGlobalInfo.u4MaxRTMRipRoute = u4SetValFsRtmMaximumRipRoutes;
    UtilRtmIncMsrForRtmScalar (u4SetValFsRtmMaximumRipRoutes,
                               FsMIRtmMaximumRipRoutes,
                               (sizeof (FsMIRtmMaximumRipRoutes) /
                                sizeof (UINT4)), 'p');
    return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhSetFsRtmMaximumStaticRoutes
 Input       :  The Indices

                The Object
                setValFsRtmMaximumStaticRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRtmMaximumStaticRoutes(UINT4 u4SetValFsRtmMaximumStaticRoutes)
{
    gRtmGlobalInfo.u4MaxRTMStaticRoute = u4SetValFsRtmMaximumStaticRoutes;
    UtilRtmIncMsrForRtmScalar (u4SetValFsRtmMaximumStaticRoutes,
                               FsMIRtmMaximumStaticRoutes,
                               (sizeof (FsMIRtmMaximumStaticRoutes) /
                                sizeof (UINT4)), 'p');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRtmMaximumISISRoutes
 Input       :  The Indices

                The Object
                setValFsRtmMaximumISISRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRtmMaximumISISRoutes(UINT4 u4SetValFsRtmMaximumISISRoutes)
{
    gRtmGlobalInfo.u4MaxRTMIsisRoute = u4SetValFsRtmMaximumISISRoutes;
    UtilRtmIncMsrForRtmScalar (u4SetValFsRtmMaximumISISRoutes,
                               FsMIRtmMaximumISISRoutes,
                               (sizeof (FsMIRtmMaximumISISRoutes) /
                                sizeof (UINT4)), 'p');
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsRtmMaximumBgpRoutes
 Input       :  The Indices

                The Object
                testValFsRtmMaximumBgpRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRtmMaximumBgpRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRtmMaximumBgpRoutes)
{

        UINT4 u4TotalRTMMemRes = 0;

        u4TotalRTMMemRes = MAX_RTM_ROUTE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtmGlobalInfo.u4MaxRTMRipRoute+
                           gRtmGlobalInfo.u4MaxRTMOspfRoute + gRtmGlobalInfo.u4MaxRTMStaticRoute + 
                           gRtmGlobalInfo.u4MaxRTMIsisRoute) ;


        if ( gRtmGlobalInfo.u4MaxRTMBgpRoute >= u4TestValFsRtmMaximumBgpRoutes )
        {
             /* If already installed route count it more than new value then return failure*/
            if (gRtmGlobalInfo.u4BgpRts > u4TestValFsRtmMaximumBgpRoutes)
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                     CLI_SET_ERR(CLI_RRD_ROUTE_EXIST);
                    return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        else
        {   /* memory reservation is increased for BGP routes*/
                if (u4TestValFsRtmMaximumBgpRoutes <= u4TotalRTMMemRes)
                {
                        return SNMP_SUCCESS;
                }
                else
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_RRD_MEMORY_OVERRUN);
                        return SNMP_FAILURE;
                }
        }

}

/****************************************************************************
 Function    :  nmhTestv2FsRtmMaximumOspfRoutes
 Input       :  The Indices

                The Object
                testValFsRtmMaximumOspfRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRtmMaximumOspfRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRtmMaximumOspfRoutes)
{

        UINT4 u4TotalRTMMemRes = 0;

        u4TotalRTMMemRes = MAX_RTM_ROUTE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtmGlobalInfo.u4MaxRTMRipRoute+
                           gRtmGlobalInfo.u4MaxRTMBgpRoute + gRtmGlobalInfo.u4MaxRTMStaticRoute + 
                           gRtmGlobalInfo.u4MaxRTMIsisRoute) ;


        if ( gRtmGlobalInfo.u4MaxRTMOspfRoute >= u4TestValFsRtmMaximumOspfRoutes )
        {
             /* If already installed route count it more than new value then return failure*/
            if (gRtmGlobalInfo.u4OspfRts > u4TestValFsRtmMaximumOspfRoutes)
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                    CLI_SET_ERR(CLI_RRD_ROUTE_EXIST);
                    return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        else
        {   /* memory reservation is increased for BGP routes*/
                if (u4TestValFsRtmMaximumOspfRoutes <= u4TotalRTMMemRes)
                {
                        return SNMP_SUCCESS;
                }
                else
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                        CLI_SET_ERR( CLI_RRD_MEMORY_OVERRUN );
                        return SNMP_FAILURE;
                }
        }

}

/****************************************************************************
 Function    :  nmhTestv2FsRtmMaximumRipRoutes
 Input       :  The Indices

                The Object
                testValFsRtmMaximumRipRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRtmMaximumRipRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRtmMaximumRipRoutes)
{

        UINT4 u4TotalRTMMemRes = 0;

        u4TotalRTMMemRes = MAX_RTM_ROUTE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtmGlobalInfo.u4MaxRTMBgpRoute+
                           gRtmGlobalInfo.u4MaxRTMOspfRoute + gRtmGlobalInfo.u4MaxRTMStaticRoute +
                           gRtmGlobalInfo.u4MaxRTMIsisRoute) ;


        if ( gRtmGlobalInfo.u4MaxRTMRipRoute >= u4TestValFsRtmMaximumRipRoutes )
        {
             /* If already installed route count it more than new value then return failure*/
            if (gRtmGlobalInfo.u4RipRts > u4TestValFsRtmMaximumRipRoutes)
            {
                    *pu4ErrorCode =  SNMP_ERR_INCONSISTENT_VALUE ;
                    CLI_SET_ERR(CLI_RRD_ROUTE_EXIST);
                    return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        else
        {   /* memory reservation is increased for BGP routes*/
                if (u4TestValFsRtmMaximumRipRoutes <= u4TotalRTMMemRes)
                {
                        return SNMP_SUCCESS;
                }
                else
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE ;
                          CLI_SET_ERR(CLI_RRD_MEMORY_OVERRUN);
                        return SNMP_FAILURE;
                }
        }

}

/****************************************************************************
 Function    :  nmhTestv2FsRtmMaximumStaticRoutes
 Input       :  The Indices

                The Object
                testValFsRtmMaximumStaticRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRtmMaximumStaticRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRtmMaximumStaticRoutes)
{

        UINT4 u4TotalRTMMemRes = 0;

        u4TotalRTMMemRes = MAX_RTM_ROUTE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtmGlobalInfo.u4MaxRTMRipRoute+
                           gRtmGlobalInfo.u4MaxRTMOspfRoute + gRtmGlobalInfo.u4MaxRTMBgpRoute + 
                           gRtmGlobalInfo.u4MaxRTMIsisRoute) ;


        if ( gRtmGlobalInfo.u4MaxRTMStaticRoute >= u4TestValFsRtmMaximumStaticRoutes )
        {
             /* If already installed route count it more than new value then return failure*/
            if (gRtmGlobalInfo.u4StaticRts > u4TestValFsRtmMaximumStaticRoutes)
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                     CLI_SET_ERR(CLI_RRD_ROUTE_EXIST);
                    return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        else
        {   /* memory reservation is increased for BGP routes*/
                if (u4TestValFsRtmMaximumStaticRoutes <= u4TotalRTMMemRes)
                {
                        return SNMP_SUCCESS;
                }
                else
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_RRD_MEMORY_OVERRUN);
                        return SNMP_FAILURE;
                }
        }

}

/****************************************************************************
 Function    :  nmhTestv2FsRtmMaximumISISRoutes
 Input       :  The Indices

                The Object
                testValFsRtmMaximumISISRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsRtmMaximumISISRoutes(UINT4 *pu4ErrorCode , UINT4 u4TestValFsRtmMaximumISISRoutes)
{

        UINT4 u4TotalRTMMemRes = 0;

        u4TotalRTMMemRes = MAX_RTM_ROUTE_ENTRIES- (SYS_DEF_MAX_INTERFACES + gRtmGlobalInfo.u4MaxRTMRipRoute+
                           gRtmGlobalInfo.u4MaxRTMOspfRoute + gRtmGlobalInfo.u4MaxRTMBgpRoute + 
                           gRtmGlobalInfo.u4MaxRTMStaticRoute) ;


        if ( gRtmGlobalInfo.u4MaxRTMIsisRoute >= u4TestValFsRtmMaximumISISRoutes )
        {
             /* If already installed route count it more than new value then return failure*/
            if (gRtmGlobalInfo.u4IsisRts > u4TestValFsRtmMaximumISISRoutes)
            {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                     CLI_SET_ERR(CLI_RRD_ROUTE_EXIST);
                    return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        }
        else
        {   /* memory reservation is increased for BGP routes*/
                if (u4TestValFsRtmMaximumISISRoutes <= u4TotalRTMMemRes)
                {
                        return SNMP_SUCCESS;
                }
                else
                {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (CLI_RRD_MEMORY_OVERRUN);
                        return SNMP_FAILURE;
                }
        }

}

/****************************************************************************
 Function    :  nmhTestv2FsRtmIpStaticRouteDistance
 Input       :  The Indices

                The Object
                testValFsRtmIpStaticRouteDistance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsRtmIpStaticRouteDistance(UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsRtmIpStaticRouteDistance)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRtmIpStaticRouteDistance > RTM_IP_ROUTE_MAX_DISTANCE) ||
        (i4TestValFsRtmIpStaticRouteDistance < RTM_IP_ROUTE_MIN_DISTANCE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRtmMaximumBgpRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRtmMaximumBgpRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
       UNUSED_PARAM(pu4ErrorCode);
       UNUSED_PARAM(pSnmpIndexList);
       UNUSED_PARAM(pSnmpVarBind);
       return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsRtmMaximumOspfRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRtmMaximumOspfRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
       UNUSED_PARAM(pu4ErrorCode);
       UNUSED_PARAM(pSnmpIndexList);
       UNUSED_PARAM(pSnmpVarBind);
       return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRtmMaximumRipRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRtmMaximumRipRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
       UNUSED_PARAM(pu4ErrorCode);
       UNUSED_PARAM(pSnmpIndexList);
       UNUSED_PARAM(pSnmpVarBind);
       return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhDepv2FsRtmMaximumStaticRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRtmMaximumStaticRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
       UNUSED_PARAM(pu4ErrorCode);
       UNUSED_PARAM(pSnmpIndexList);
       UNUSED_PARAM(pSnmpVarBind);
       return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRtmMaximumISISRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsRtmMaximumISISRoutes(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
       UNUSED_PARAM(pu4ErrorCode);
       UNUSED_PARAM(pSnmpIndexList);
       UNUSED_PARAM(pSnmpVarBind);
       return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RtmGetTotalMemoryBlocksAllocated
 Descriptin  :  Returns the total memory block allocated to store routes of all protocol
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
UINT4 RtmGetTotalMemoryBlocksAllocated()
{
  return FsRTMSizingParams[MAX_RTM_ROUTE_TABLE_ENTRIES_SIZING_ID].u4PreAllocatedUnits;
}


/****************************************************************************
 Function    :  nmhGetFsRtmRedEntryTime
 Input       :  The Indices

                The Object
                retValFsRtmRedEntryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1
nmhGetFsRtmRedEntryTime (INT4 *pi4RetValFsRtmRedEntryTime)
{
    *pi4RetValFsRtmRedEntryTime = gRtmRedGlobalInfo.i4RtmRedEntryTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRtmRedExitTime
 Input       :  The Indices

                The Object
                retValFsRtmRedExitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
******************************************************************************/
INT1
nmhGetFsRtmRedExitTime (INT4 *pi4RetValFsRtmRedExitTime)
{
    *pi4RetValFsRtmRedExitTime = gRtmRedGlobalInfo.i4RtmRedExitTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsEcmpAcrossProtocolAdminStatus
 Input       :  The Indices

                The Object
                retValFsEcmpAcrossProtocolAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsEcmpAcrossProtocolAdminStatus(INT4 *pi4RetValFsEcmpAcrossProtocolAdminStatus)
{
    if (pi4RetValFsEcmpAcrossProtocolAdminStatus == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsEcmpAcrossProtocolAdminStatus = gRtmGlobalInfo.u1EcmpAcrossProtocol;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRtmRouteLeakStatus
 Input       :  The Indices
                The Object
                retValFsRtmRouteLeakStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1 nmhGetFsRtmRouteLeakStatus(INT4 *pi4RetValFsRtmRouteLeakStatus)                                    
{
    if (pi4RetValFsRtmRouteLeakStatus == NULL)
    {
        return SNMP_FAILURE;
    }
	 
    *pi4RetValFsRtmRouteLeakStatus = gRtmGlobalInfo.u1VrfRouteLeakStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRrdRouterId
 Input       :  The Indices

                The Object 
                setValFsRrdRouterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdRouterId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdRouterId (UINT4 u4SetValFsRrdRouterId)
#else
INT1
nmhSetFsRrdRouterId (u4SetValFsRrdRouterId)
     UINT4               u4SetValFsRrdRouterId;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRtmCxt->u4RouterId == 0)
    {
        pRtmCxt->u4RouterId = (UINT4) u4SetValFsRrdRouterId;
    }
    pRtmCxt->u4TempRouterId = (UINT4) u4SetValFsRrdRouterId;
    pRtmCxt->u1RtrIdType = RTM_RTRID_CONFIG_TYPE_USER;    /* router-id configured by user */
    UtilRtmIncMsrForRtmTable (pRtmCxt->u4ContextId,
                              (INT4) u4SetValFsRrdRouterId,
                              FsMIRrdRouterId,
                              (sizeof (FsMIRrdRouterId) / sizeof (UINT4)), 'p');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrdFilterByOspfTag
 Input       :  The Indices

                The Object 
                setValFsRrdFilterByOspfTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdFilterByOspfTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdFilterByOspfTag (INT4 i4SetValFsRrdFilterByOspfTag)
#else
INT1
nmhSetFsRrdFilterByOspfTag (i4SetValFsRrdFilterByOspfTag)
     INT4                i4SetValFsRrdFilterByOspfTag;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pRtmCxt->RtmConfigInfo.u1RrdFilterByOspfTag =
        (UINT1) i4SetValFsRrdFilterByOspfTag;
    UtilRtmIncMsrForRtmTable (pRtmCxt->u4ContextId,
                              i4SetValFsRrdFilterByOspfTag,
                              FsMIRrdFilterByOspfTag,
                              (sizeof (FsMIRrdFilterByOspfTag) /
                               sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrdFilterOspfTag
 Input       :  The Indices

                The Object 
                setValFsRrdFilterOspfTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdFilterOspfTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdFilterOspfTag (INT4 i4SetValFsRrdFilterOspfTag)
#else
INT1
nmhSetFsRrdFilterOspfTag (i4SetValFsRrdFilterOspfTag)
     INT4                i4SetValFsRrdFilterOspfTag;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pRtmCxt->RtmConfigInfo.u4OspfTagValue = (UINT4) i4SetValFsRrdFilterOspfTag;

    UtilRtmIncMsrForRtmTable (pRtmCxt->u4ContextId,
                              i4SetValFsRrdFilterOspfTag,
                              FsMIRrdFilterOspfTag,
                              (sizeof (FsMIRrdFilterOspfTag) /
                               sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrdFilterOspfTagMask
 Input       :  The Indices

                The Object 
                setValFsRrdFilterOspfTagMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdFilterOspfTagMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdFilterOspfTagMask (INT4 i4SetValFsRrdFilterOspfTagMask)
#else
INT1
nmhSetFsRrdFilterOspfTagMask (i4SetValFsRrdFilterOspfTagMask)
     INT4                i4SetValFsRrdFilterOspfTagMask;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pRtmCxt->RtmConfigInfo.u4OspfTagMask =
        (UINT4) i4SetValFsRrdFilterOspfTagMask;
    UtilRtmIncMsrForRtmTable (pRtmCxt->u4ContextId,
                              i4SetValFsRrdFilterOspfTagMask,
                              FsMIRrdFilterOspfTagMask,
                              (sizeof (FsMIRrdFilterOspfTagMask) /
                               sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrdRouterASNumber
 Input       :  The Indices

                The Object 
                setValFsRrdRouterASNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdRouterASNumber ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdRouterASNumber (INT4 i4SetValFsRrdRouterASNumber)
#else
INT1
nmhSetFsRrdRouterASNumber (i4SetValFsRrdRouterASNumber)
     INT4                i4SetValFsRrdRouterASNumber;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRtmCxt->u2AsNumber == 0)
    {
        pRtmCxt->u2AsNumber = (UINT2) i4SetValFsRrdRouterASNumber;
    }
    pRtmCxt->u2TempAsNumber = (UINT2) i4SetValFsRrdRouterASNumber;
    UtilRtmIncMsrForRtmTable (pRtmCxt->u4ContextId,
                              i4SetValFsRrdRouterASNumber,
                              FsMIRrdRouterASNumber,
                              (sizeof (FsMIRrdRouterASNumber) /
                               sizeof (UINT4)), 'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrdAdminStatus
 Input       :  The Indices

                The Object 
                setValFsRrdAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdAdminStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdAdminStatus (INT4 i4SetValFsRrdAdminStatus)
#else
INT1
nmhSetFsRrdAdminStatus (i4SetValFsRrdAdminStatus)
     INT4                i4SetValFsRrdAdminStatus;
#endif
{
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    MEMSET (&RegnId, 0, sizeof (tRtmRegnId));

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pRtmCxt->u1RrdAdminStatus == i4SetValFsRrdAdminStatus)
    {
        return SNMP_SUCCESS;
    }
    else if ((pRtmCxt->u1RrdAdminStatus == RTM_ADMIN_STATUS_ENABLED) &&
             (i4SetValFsRrdAdminStatus == RTM_ADMIN_STATUS_DISABLED))
    {
        /* Rtm already Enabled */
        return SNMP_FAILURE;
    }
    if (pRtmCxt->u4RouterId == 0)
    {
        /* Set the highest interface address as the default router id */
#if (CUST_BGP_ROUTER_ID_SELECT == OSIX_TRUE)
	UtilSelectRouterId (pRtmCxt->u4ContextId, &(pRtmCxt->u4RouterId));
#else
        NetIpv4GetHighestIpAddrInCxt (pRtmCxt->u4ContextId,
                                      &(pRtmCxt->u4RouterId));
#endif
        pRtmCxt->u1RtrIdType = RTM_RTRID_CONFIG_TYPE_DYNAMIC;    /* router-id dynamically elected */
    }

    if (pRtmCxt->u4RouterId == 0)
    {
#ifdef BGP_WANTED
        if (gRtmRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
        {
#endif
        /* Router-id not configrued. */
        return SNMP_FAILURE;
#ifdef BGP_WANTED
        }
#endif
    }

    pRtmCxt->u1RrdAdminStatus = (UINT1) i4SetValFsRrdAdminStatus;
    /* check whether any RP's had registered with RTM */
    /* If so start sending the Registration ACK messages to them. */

    for (u2Index = pRtmCxt->u2RtmRtStartIndex;
         u2Index < MAX_ROUTING_PROTOCOLS;
         u2Index = pRtmCxt->aRtmRegnTable[u2Index].u2NextRegId)
    {
        RegnId.u2ProtoId = pRtmCxt->aRtmRegnTable[u2Index].u2RoutingProtocolId;
        RegnId.u4ContextId = pRtmCxt->u4ContextId;
        RtmSendAckToRpInCxt (pRtmCxt, &RegnId);

    }
    UtilRtmIncMsrForRtmTable (pRtmCxt->u4ContextId,
                              i4SetValFsRrdAdminStatus,
                              FsMIRrdAdminStatus,
                              (sizeof (FsMIRrdAdminStatus) /
                               sizeof (UINT4)), 'i');

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRtmThrottleLimit
 Input       :  The Indices

                The Object 
                setValFsRtmThrottleLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRtmThrottleLimit (UINT4 u4SetValFsRtmThrottleLimit)
{
    gRtmGlobalInfo.u4ThrotLimit = u4SetValFsRtmThrottleLimit;
    UtilRtmIncMsrForRtmScalar (u4SetValFsRtmThrottleLimit,
                               FsMIRtmThrottleLimit,
                               (sizeof (FsMIRtmThrottleLimit) /
                                sizeof (UINT4)), 'p');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsEcmpAcrossProtocolAdminStatus
 Input       :  The Indices

                The Object
                setValFsEcmpAcrossProtocolAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsEcmpAcrossProtocolAdminStatus(INT4 i4SetValFsEcmpAcrossProtocolAdminStatus)
{

    gRtmGlobalInfo.u1EcmpAcrossProtocol = (UINT1)i4SetValFsEcmpAcrossProtocolAdminStatus;

    UtilRtmIncMsrForRtmScalar ((UINT4)i4SetValFsEcmpAcrossProtocolAdminStatus,
                               FsMIEcmpAcrossProtocolAdminStatus,
                               (sizeof (FsMIEcmpAcrossProtocolAdminStatus) /
				                sizeof(UINT4)),	'i');
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRtmRouteLeakStatus
 Input       :  The Indices
 
                The Object
                setValFsRtmRouteLeakStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsRtmRouteLeakStatus(INT4 i4SetValFsRtmRouteLeakStatus)                                      
{
    gRtmGlobalInfo.u1VrfRouteLeakStatus  = (UINT1)i4SetValFsRtmRouteLeakStatus;

#ifdef IP_WANTED
    if (i4SetValFsRtmRouteLeakStatus == RTM_VRF_ROUTE_LEAK_DISABLED)
    {
        /* When Route leaking is disabled ,delete all the leaked static routes*/
        if (IPvxDeleteIpv4LeakedStaticRouteInCxt ((INT1) CIDR_STATIC_ID, VCM_INVALID_VC, CFA_INVALID_INDEX)
            != IPVX_SUCCESS)
        {
             return SNMP_FAILURE;
        }
    }
#endif
	 
    UtilRtmIncMsrForRtmScalar ((UINT4)i4SetValFsRtmRouteLeakStatus,
                                FsMIRtmRouteLeakStatus,
                                (sizeof (FsMIRtmRouteLeakStatus) /
                                 sizeof(UINT4)), 'i');
	 
   return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRtmIpStaticRouteDistance
 Input       :  The Indices

                The Object
                setValFsRtmIpStaticRouteDistance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsRtmIpStaticRouteDistance(INT4 i4SetValFsRtmIpStaticRouteDistance)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;
    pRtmCxt->u1IpDefaultDistance = (UINT1) i4SetValFsRtmIpStaticRouteDistance;
    UtilRtmIncMsrForRtmTable (pRtmCxt->u4ContextId,
                              i4SetValFsRtmIpStaticRouteDistance,
                              FsMIRTMIpStaticRouteDistance,
                              (sizeof (FsMIRTMIpStaticRouteDistance) /
                               sizeof (UINT4)), 'i');

    return SNMP_SUCCESS; 
}


/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRrdRouterId
 Input       :  The Indices

                The Object 
                testValFsRrdRouterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdRouterId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdRouterId (UINT4 *pu4ErrorCode, UINT4 u4TestValFsRrdRouterId)
#else
INT1
nmhTestv2FsRrdRouterId (*pu4ErrorCode, u4TestValFsRrdRouterId)
     UINT4              *pu4ErrorCode;
     UINT4               u4TestValFsRrdRouterId;
#endif
{
    INT4                i4RetVal = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* the router ID should be one of the interface address of the router. */

    i4RetVal = NetIpv4IfIsOurAddressInCxt (pRtmCxt->u4ContextId,
                                           u4TestValFsRrdRouterId);
    if (i4RetVal != NETIPV4_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD_INVALID_RID);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdFilterByOspfTag
 Input       :  The Indices

                The Object 
                testValFsRrdFilterByOspfTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdFilterByOspfTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdFilterByOspfTag (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsRrdFilterByOspfTag)
#else
INT1
nmhTestv2FsRrdFilterByOspfTag (*pu4ErrorCode, i4TestValFsRrdFilterByOspfTag)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsRrdFilterByOspfTag;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsRrdFilterByOspfTag != RTM_FILTER_DISABLED) &&
        (i4TestValFsRrdFilterByOspfTag != RTM_FILTER_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD_INVALID_INPUT);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdFilterOspfTag
 Input       :  The Indices

                The Object 
                testValFsRrdFilterOspfTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdFilterOspfTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdFilterOspfTag (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsRrdFilterOspfTag)
#else
INT1
nmhTestv2FsRrdFilterOspfTag (*pu4ErrorCode, i4TestValFsRrdFilterOspfTag)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsRrdFilterOspfTag;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Filtering on Manual TAG should not be allowed */
    if (i4TestValFsRrdFilterOspfTag & (INT4) RTM_AUTOMATIC_BIT_SET)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_RRD_INVALID_INPUT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdFilterOspfTagMask
 Input       :  The Indices

                The Object 
                testValFsRrdFilterOspfTagMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdFilterOspfTagMask ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdFilterOspfTagMask (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsRrdFilterOspfTagMask)
#else
INT1
nmhTestv2FsRrdFilterOspfTagMask (*pu4ErrorCode, i4TestValFsRrdFilterOspfTagMask)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsRrdFilterOspfTagMask;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* Filtering on Manual TAG should not be allowed */
    if (i4TestValFsRrdFilterOspfTagMask & (INT4) RTM_AUTOMATIC_BIT_SET)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_RRD_INVALID_INPUT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdRouterASNumber
 Input       :  The Indices

                The Object 
                testValFsRrdRouterASNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdRouterASNumber ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdRouterASNumber (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsRrdRouterASNumber)
#else
INT1
nmhTestv2FsRrdRouterASNumber (*pu4ErrorCode, i4TestValFsRrdRouterASNumber)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsRrdRouterASNumber;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrdRouterASNumber < MIN_AS_NUM) ||
        (i4TestValFsRrdRouterASNumber > MAX_AS_NUM))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RRD_INVALID_INPUT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdAdminStatus
 Input       :  The Indices

                The Object 
                testValFsRrdAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdAdminStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdAdminStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsRrdAdminStatus)
#else
INT1
nmhTestv2FsRrdAdminStatus (*pu4ErrorCode, i4TestValFsRrdAdminStatus)
     UINT4              *pu4ErrorCode;
     INT4                i4TestValFsRrdAdminStatus;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrdAdminStatus != RTM_ADMIN_STATUS_ENABLED) &&
        (i4TestValFsRrdAdminStatus != RTM_ADMIN_STATUS_DISABLED))
    {
        CLI_SET_ERR (CLI_RRD_ONLY_ENABLE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* RTM Admin status can be set from DISBALED TO ENABLED only once 
       Before setting the admin status to enabled the administrator 
       should have configured values for AS Number and Router ID */

    if ((pRtmCxt->u1RrdAdminStatus == RTM_ADMIN_STATUS_ENABLED) &&
        (i4TestValFsRrdAdminStatus == RTM_ADMIN_STATUS_DISABLED))
    {
        CLI_SET_ERR (CLI_RRD_ONLY_ENABLE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsRrdAdminStatus == RTM_ADMIN_STATUS_ENABLED)
    {
        if (pRtmCxt->u2AsNumber == 0)
        {
            CLI_SET_ERR (CLI_RRD_RID_AS_NOTSET);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRtmThrottleLimit
 Input       :  The Indices

                The Object 
                testValFsRtmThrottleLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRtmThrottleLimit (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsRtmThrottleLimit)
{
    if (u4TestValFsRtmThrottleLimit < RTM_MIN_THRESHOLD_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RTM_WRONG_THROTTLE_LIMIT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsEcmpAcrossProtocolAdminStatus
 Input       :  The Indices

                The Object
                testValFsEcmpAcrossProtocolAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsEcmpAcrossProtocolAdminStatus(UINT4 *pu4ErrorCode ,
                INT4 i4TestValFsEcmpAcrossProtocolAdminStatus)
{

    INT1                i1Return = SNMP_SUCCESS;

    /* Wrong value error handler for SNMP, CLI terminal can not pass wrong value resticted */
    if ((i4TestValFsEcmpAcrossProtocolAdminStatus != RTM_ECMP_ACROSS_PROTO_ENABLED) &&
        (i4TestValFsEcmpAcrossProtocolAdminStatus != RTM_ECMP_ACROSS_PROTO_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return i1Return;
}
/****************************************************************************
 Function    :  nmhTestv2FsRtmRouteLeakStatus
 Input       :  The Indices

                The Object
                testValFsRtmRouteLeakStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/                           
INT1 nmhTestv2FsRtmRouteLeakStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFsRtmRouteLeakStatus)
{
    INT1                i1Return = SNMP_SUCCESS;

    /* Wrong value error handler for SNMP, CLI terminal can not pass wrong value resticted */
    if ((i4TestValFsRtmRouteLeakStatus != RTM_VRF_ROUTE_LEAK_ENABLED) &&
        (i4TestValFsRtmRouteLeakStatus != RTM_VRF_ROUTE_LEAK_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
	 
    return i1Return;
	 
}

/****************************************************************************
 Function    :  nmhDepv2FsRrdRouterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrdRouterId (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrdFilterByOspfTag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrdFilterByOspfTag (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrdFilterOspfTag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrdFilterOspfTag (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrdFilterOspfTagMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrdFilterOspfTagMask (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrdRouterASNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrdRouterASNumber (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrdAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrdAdminStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRtmThrottleLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRtmThrottleLimit (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRtmIpStaticRouteDistance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhDepv2FsRtmIpStaticRouteDistance(UINT4 *pu4ErrorCode, 
                                   tSnmpIndexList *pSnmpIndexList, 
                                   tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsEcmpAcrossProtocolAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsEcmpAcrossProtocolAdminStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRtmRouteLeakStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &    
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 	        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)        

 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1 nmhDepv2FsRtmRouteLeakStatus(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
    UNUSED_PARAM(pu4ErrorCode);                                                                             
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRrdControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRrdControlTable
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsRrdControlTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsRrdControlTable (UINT4 u4FsRrdControlDestIpAddress,
                                           UINT4 u4FsRrdControlNetMask)
#else
INT1
nmhValidateIndexInstanceFsRrdControlTable (u4FsRrdControlDestIpAddress,
                                           u4FsRrdControlNetMask)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((u4FsRrdControlDestIpAddress == IP_ANY_ADDR) &&
        (u4FsRrdControlNetMask != IP_GEN_BCAST_ADDR))
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress) &&
            (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We have this entry in the RRD control table */

            return SNMP_SUCCESS;
        }
    }

    /* No match found */
    u4FsRrdControlNetMask = 0;/*** assigned 0 to remove waning ***/
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRrdControlTable
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsRrdControlTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsRrdControlTable (UINT4 *pu4FsRrdControlDestIpAddress,
                                   UINT4 *pu4FsRrdControlNetMask)
#else
INT1
nmhGetFirstIndexFsRrdControlTable (pu4FsRrdControlDestIpAddress,
                                   pu4FsRrdControlNetMask)
     UINT4              *pu4FsRrdControlDestIpAddress;
     UINT4              *pu4FsRrdControlNetMask;
#endif
{
    tRrdControlInfo    *pleast = NULL, *pcurr = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /*
     * We can have an permit (deny) filter to control redistribution of
     * default routes as 0.0.0.0/0.0.0.0 , hence not advisible to call
     * get next of (0,0)
     */
    pcurr = (tRrdControlInfo *) TMO_SLL_First (&(pRtmCxt->RtmCtrlList));
    if (pcurr == NULL)
    {
        /* No entry is there in the RRD Control table */
        return (SNMP_FAILURE);
    }

    pleast = pcurr;
    do
    {
        if ((pcurr->u4DestNet < pleast->u4DestNet)
            || ((pcurr->u4DestNet == pleast->u4DestNet)
                && (pcurr->u4SubnetRange < pleast->u4SubnetRange)))
        {
            pleast = pcurr;
        }
        pcurr = (tRrdControlInfo *) TMO_SLL_Next (&(pRtmCxt->RtmCtrlList),
                                                  &(pcurr->pNext));
    }
    while (pcurr != NULL);

    *pu4FsRrdControlDestIpAddress = pleast->u4DestNet;
    *pu4FsRrdControlNetMask = pleast->u4SubnetRange;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRrdControlTable
 Input       :  The Indices
                FsRrdControlDestIpAddress
                nextFsRrdControlDestIpAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsRrdControlTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsRrdControlTable (UINT4 u4FsRrdControlDestIpAddress,
                                  UINT4 *pu4NextFsRrdControlDestIpAddress,
                                  UINT4 u4FsRrdControlNetMask,
                                  UINT4 *pu4NextFsRrdControlNetMask)
#else
INT1
nmhGetNextIndexFsRrdControlTable (u4FsRrdControlDestIpAddress,
                                  pu4NextFsRrdControlDestIpAddress,
                                  u4FsRrdControlNetMask,
                                  pu4NextFsRrdControlNetMask)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4              *pu4NextFsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     UINT4              *pu4NextFsRrdControlNetMask;
#endif
{
    tRrdControlInfo    *pnext = NULL, *pcurr = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pcurr = (tRrdControlInfo *) TMO_SLL_First (&(pRtmCxt->RtmCtrlList));
    if (pcurr == NULL)
    {
        /* No entry is there in the RRD Control table */
        return (SNMP_FAILURE);
    }

    do
    {
        if ((pcurr->u4DestNet > u4FsRrdControlDestIpAddress)
            || ((pcurr->u4DestNet == u4FsRrdControlDestIpAddress)
                && (pcurr->u4SubnetRange > u4FsRrdControlNetMask)))
        {
            /* if a value lesser than current least-larger is found update 
             * out current least-larger value.
             * IF no current least-larger value, update 
             *   current least-larger value
             */
            if ((pnext != NULL) && ((pnext->u4DestNet < pcurr->u4DestNet) ||
                                    ((pnext->u4DestNet == pcurr->u4DestNet)
                                     && (pnext->u4SubnetRange <
                                         pcurr->u4SubnetRange))))
            {
                /* no oper */
                pcurr = pcurr;
            }
            else
            {
                pnext = pcurr;
            }
        }
        pcurr = (tRrdControlInfo *) TMO_SLL_Next (&(pRtmCxt->RtmCtrlList),
                                                  &(pcurr->pNext));
    }
    while (pcurr != NULL);

    if (pnext != NULL)
    {
        *pu4NextFsRrdControlDestIpAddress = pnext->u4DestNet;
        *pu4NextFsRrdControlNetMask = pnext->u4SubnetRange;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRrdControlSourceProto
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                retValFsRrdControlSourceProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdControlSourceProto ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdControlSourceProto (UINT4 u4FsRrdControlDestIpAddress,
                               UINT4 u4FsRrdControlNetMask,
                               INT4 *pi4RetValFsRrdControlSourceProto)
#else
INT1
nmhGetFsRrdControlSourceProto (u4FsRrdControlDestIpAddress,
                               u4FsRrdControlNetMask,
                               pi4RetValFsRrdControlSourceProto)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4               *pi4RetValFsRrdControlSourceProto;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrdControlSourceProto = (INT4) pRtmCtrlInfo->u1SrcProtocolId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrdControlDestProto
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                retValFsRrdControlDestProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdControlDestProto ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdControlDestProto (UINT4 u4FsRrdControlDestIpAddress,
                             UINT4 u4FsRrdControlNetMask,
                             INT4 *pi4RetValFsRrdControlDestProto)
#else
INT1
nmhGetFsRrdControlDestProto (u4FsRrdControlDestIpAddress, u4FsRrdControlNetMask,
                             pi4RetValFsRrdControlDestProto)
     UINT4               u4FsRrdControlDestIpAddress;
     UIN4                u4FsRrdControlNetMask;
     INT4               *pi4RetValFsRrdControlDestProto;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrdControlDestProto = (INT4) pRtmCtrlInfo->u2DestProtocolMask;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRrdControlRouteExportFlag
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                retValFsRrdControlRouteExportFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdControlRouteExportFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdControlRouteExportFlag (UINT4 u4FsRrdControlDestIpAddress,
                                   UINT4 u4FsRrdControlNetMask,
                                   INT4 *pi4RetValFsRrdControlRouteExportFlag)
#else
INT1
nmhGetFsRrdControlRouteExportFlag (u4FsRrdControlDestIpAddress,
                                   u4FsRrdControlNetMask,
                                   pi4RetValFsRrdControlRouteExportFlag)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4               *pi4RetValFsRrdControlRouteExportFlag;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrdControlRouteExportFlag = (INT4) pRtmCtrlInfo->u1RtExportFlag;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRrdControlRowStatus
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                retValFsRrdControlRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdControlRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdControlRowStatus (UINT4 u4FsRrdControlDestIpAddress,
                             UINT4 u4FsRrdControlNetMask,
                             INT4 *pi4RetValFsRrdControlRowStatus)
#else
INT1
nmhGetFsRrdControlRowStatus (u4FsRrdControlDestIpAddress, u4FsRrdControlNetMask,
                             pi4RetValFsRrdControlRowStatus)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4               *pi4RetValFsRrdControlRowStatus;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        return SNMP_FAILURE;
    }
    *pi4RetValFsRrdControlRowStatus = (INT4) pRtmCtrlInfo->u1RowStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRrdControlSourceProto
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                setValFsRrdControlSourceProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdControlSourceProto ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdControlSourceProto (UINT4 u4FsRrdControlDestIpAddress,
                               UINT4 u4FsRrdControlNetMask,
                               INT4 i4SetValFsRrdControlSourceProto)
#else
INT1
nmhSetFsRrdControlSourceProto (u4FsRrdControlDestIpAddress,
                               u4FsRrdControlNetMask,
                               i4SetValFsRrdControlSourceProto)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4                i4SetValFsRrdControlSourceProto;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        return SNMP_FAILURE;
    }

    if (pRtmCtrlInfo->u1SrcProtocolId ==
        (UINT1) i4SetValFsRrdControlSourceProto)
    {
        return SNMP_SUCCESS;
    }

    pRtmCtrlInfo->u1SrcProtocolId = (UINT1) i4SetValFsRrdControlSourceProto;
    UtilRtmIncMsrForRrdControlTable (pRtmCxt->u4ContextId,
                                     u4FsRrdControlDestIpAddress,
                                     u4FsRrdControlNetMask,
                                     i4SetValFsRrdControlSourceProto,
                                     FsMIRrdControlSourceProto,
                                     (sizeof (FsMIRrdControlSourceProto) /
                                      sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRrdControlDestProto
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                setValFsRrdControlDestProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdControlDestProto ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdControlDestProto (UINT4 u4FsRrdControlDestIpAddress,
                             UINT4 u4FsRrdControlNetMask,
                             INT4 i4SetValFsRrdControlDestProto)
#else
INT1
nmhSetFsRrdControlDestProto (u4FsRrdControlDestIpAddress,
                             u4FsRrdControlNetMask,
                             i4SetValFsRrdControlDestProto)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4                i4SetValFsRrdControlDestProto;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        return SNMP_FAILURE;
    }

    if (pRtmCtrlInfo->u2DestProtocolMask ==
        (UINT2) i4SetValFsRrdControlDestProto)
    {
        return SNMP_SUCCESS;
    }

    pRtmCtrlInfo->u2DestProtocolMask = (UINT2) i4SetValFsRrdControlDestProto;
    UtilRtmIncMsrForRrdControlTable (pRtmCxt->u4ContextId,
                                     u4FsRrdControlDestIpAddress,
                                     u4FsRrdControlNetMask,
                                     i4SetValFsRrdControlDestProto,
                                     FsMIRrdControlDestProto,
                                     (sizeof (FsMIRrdControlDestProto) /
                                      sizeof (UINT4)), FALSE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRrdControlRouteExportFlag
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                setValFsRrdControlRouteExportFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdControlRouteExportFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdControlRouteExportFlag (UINT4 u4FsRrdControlDestIpAddress,
                                   UINT4 u4FsRrdControlNetMask,
                                   INT4 i4SetValFsRrdControlRouteExportFlag)
#else
INT1
nmhSetFsRrdControlRouteExportFlag (u4FsRrdControlDestIpAddress,
                                   u4FsRrdControlNetMask,
                                   i4SetValFsRrdControlRouteExportFlag)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4                i4SetValFsRrdControlRouteExportFlag;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tIpBuf             *pBuf = NULL;
    tRtmSnmpIfMsg      *pRtmParms = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        return SNMP_FAILURE;
    }

    if ((u4FsRrdControlDestIpAddress == IP_ANY_ADDR) &&
        (u4FsRrdControlNetMask == IP_GEN_BCAST_ADDR) &&
        (pRtmCtrlInfo->u1RowStatus == RTM_ACTIVE))
    {
        /* Change in the Control Table mode for ALL ROUTE Entry. Post a
         * message to RTM Task to handle this event. */
        if ((pBuf = IP_ALLOCATE_BUF (sizeof (UINT4), 0)) == NULL)
        {
            return SNMP_FAILURE;
        }

        pRtmParms = (tRtmSnmpIfMsg *) IP_GET_MODULE_DATA_PTR (pBuf);
        pRtmParms->u1Cmd = RTM_CHG_IN_RRD_DEF_CTRL_MODE;
        pRtmParms->pRtmFilter = pRtmCtrlInfo;
        pRtmParms->RegnId.u4ContextId = pRtmCxt->u4ContextId;

        switch (i4SetValFsRrdControlRouteExportFlag)
        {
            case RTM_ROUTE_PERMIT:
                pRtmParms->u1PermitOrDeny = RTM_ROUTE_PERMIT;
                break;

            case RTM_ROUTE_DENY:
                /* The row will be actually destroyed in RTM after
                   route redistribution to the routing protocols. */

                pRtmParms->u1PermitOrDeny = RTM_ROUTE_DENY;
                break;

            default:
                IP_RELEASE_BUF (pBuf, FALSE);
                return SNMP_FAILURE;
        }
        if (OsixQueSend (gRtmSnmpMsgQId, (UINT1 *) &pBuf,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            IP_RELEASE_BUF (pBuf, FALSE);
            return (SNMP_FAILURE);
        }

        OsixEvtSend (gRtmTaskId, RTM_FILTER_ADDED_EVENT);
    }
    else
    {
        pRtmCtrlInfo->u1RtExportFlag =
            (UINT1) i4SetValFsRrdControlRouteExportFlag;
    }
    UtilRtmIncMsrForRrdControlTable (pRtmCxt->u4ContextId,
                                     u4FsRrdControlDestIpAddress,
                                     u4FsRrdControlNetMask,
                                     i4SetValFsRrdControlRouteExportFlag,
                                     FsMIRrdControlRouteExportFlag,
                                     (sizeof (FsMIRrdControlRouteExportFlag) /
                                      sizeof (UINT4)), FALSE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsRrdControlRowStatus
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                setValFsRrdControlRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 p
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdControlRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdControlRowStatus (UINT4 u4FsRrdControlDestIpAddress,
                             UINT4 u4FsRrdControlNetMask,
                             INT4 i4SetValFsRrdControlRowStatus)
#else
INT1
nmhSetFsRrdControlRowStatus (u4FsRrdControlDestIpAddress,
                             u4FsRrdControlNetMask,
                             i4SetValFsRrdControlRowStatus)
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4                i4SetValFsRrdControlRowStatus;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    tRrdControlInfo    *pPrevRtmInfo = NULL;
    tRrdControlInfo    *pCurrRtmInfo = NULL;
    tIpBuf             *pBuf = NULL;
    tRtmSnmpIfMsg      *pRtmParms = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the current node in the list */
            break;
        }
    }

    if ((pRtmCtrlInfo == NULL) &&
        (i4SetValFsRrdControlRowStatus == RTM_CREATE_AND_WAIT))
    {
        /* The Row status could be CREATE AND WAIT */
        /* we need to create the node and add it to the list */
        if (RTM_CONTROL_INFO_ALLOC (pRtmCtrlInfo) == NULL)
        {
            /* Malloc failed */
            return SNMP_FAILURE;
        }
        /* Initialise all the variables in the RRD Control info */

        pRtmCtrlInfo->u4DestNet = u4FsRrdControlDestIpAddress;
        pRtmCtrlInfo->u4SubnetRange = u4FsRrdControlNetMask;
        pRtmCtrlInfo->u1SrcProtocolId = RTM_ANY_PROTOCOL;
        pRtmCtrlInfo->u2DestProtocolMask = RTM_ANY_PROTOCOL;
        pRtmCtrlInfo->u1RtExportFlag = RTM_ROUTE_PERMIT;
        pRtmCtrlInfo->u4RouteTag = 0;
        pRtmCtrlInfo->u1RowStatus = RTM_NOT_IN_SERVICE;
        pRtmCtrlInfo->pRtmCxt = pRtmCxt;

        /* Maintain the linked list in the sorted order - starting from
         * Highest Dest value to Lowest Dest Value. This will ensure that
         * if a match is found while scanning through the control table,
         * longest prefix match will be hit first. */
        TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pCurrRtmInfo, tRrdControlInfo *)
        {
            if (pCurrRtmInfo->u4DestNet < pRtmCtrlInfo->u4DestNet)
            {
                break;
            }
            pPrevRtmInfo = pCurrRtmInfo;
        }

        TMO_SLL_Insert (&(pRtmCxt->RtmCtrlList),
                        &(pPrevRtmInfo->pNext), &(pRtmCtrlInfo->pNext));
        UtilRtmIncMsrForRrdControlTable (pRtmCxt->u4ContextId,
                                         u4FsRrdControlDestIpAddress,
                                         u4FsRrdControlNetMask,
                                         i4SetValFsRrdControlRowStatus,
                                         FsMIRrdControlRowStatus,
                                         (sizeof (FsMIRrdControlRowStatus) /
                                          sizeof (UINT4)), TRUE);

        return SNMP_SUCCESS;
    }
    else if (pRtmCtrlInfo != NULL)
    {
        /* The entry already exists. */
        /* Either the administrator wants to delete the row or to make
         * the row active. */

        /* Send an event to the RTM task indicating the 
           addition of new filter. This is done to update the 
           export list of the protocols. Export list updation is
           a costly operation and it involves scanning through
           the routing table and export list. If it is updated 
           here then the SNMP manager may timeout before getting
           the response for the set operation. */

        if ((pBuf = IP_ALLOCATE_BUF (sizeof (UINT4), 0)) == NULL)
        {
            return SNMP_FAILURE;
        }

        pRtmParms = (tRtmSnmpIfMsg *) IP_GET_MODULE_DATA_PTR (pBuf);
        pRtmParms->u1Cmd = RTM_CHG_IN_RRD_CONTROL_TABLE;
        pRtmParms->pRtmFilter = pRtmCtrlInfo;
        pRtmParms->RegnId.u4ContextId = pRtmCxt->u4ContextId;

        switch (i4SetValFsRrdControlRowStatus)
        {
            case RTM_ACTIVE:
                pRtmCtrlInfo->u1RowStatus = RTM_ACTIVE;
                break;

            case RTM_DESTROY:
                /* The row will be actually destroyed in RTM after
                   updating the export list of the routing protocols. */

                pRtmCtrlInfo->u1RowStatus = RTM_DESTROY;
                break;

            default:
                IP_RELEASE_BUF (pBuf, FALSE);
                return SNMP_FAILURE;
        }
        if (OsixQueSend (gRtmSnmpMsgQId, (UINT1 *) &pBuf,
                         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            IP_RELEASE_BUF (pBuf, FALSE);
            return (SNMP_FAILURE);
        }

        OsixEvtSend (gRtmTaskId, RTM_FILTER_ADDED_EVENT);
        UtilRtmIncMsrForRrdControlTable (pRtmCxt->u4ContextId,
                                         u4FsRrdControlDestIpAddress,
                                         u4FsRrdControlNetMask,
                                         i4SetValFsRrdControlRowStatus,
                                         FsMIRrdControlRowStatus,
                                         (sizeof (FsMIRrdControlRowStatus) /
                                          sizeof (UINT4)), TRUE);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdControlSourceProto
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                testValFsRrdControlSourceProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdControlSourceProto ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdControlSourceProto (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsRrdControlDestIpAddress,
                                  UINT4 u4FsRrdControlNetMask,
                                  INT4 i4TestValFsRrdControlSourceProto)
#else
INT1
nmhTestv2FsRrdControlSourceProto (*pu4ErrorCode, u4FsRrdControlDestIpAddress,
                                  u4FsRrdControlNetMask,
                                  i4TestValFsRrdControlSourceProto)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4                i4TestValFsRrdControlSourceProto;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    /* Source protocol should be local, static, RIP, OSPF or BGP */
    switch (i4TestValFsRrdControlSourceProto)
    {
        case RTM_ANY_PROTOCOL:
        case CIDR_LOCAL_ID:
        case CIDR_STATIC_ID:
        case RIP_ID:
        case OSPF_ID:
        case BGP_ID:
            i1RetVal = SNMP_SUCCESS;
            break;

        default:
            CLI_SET_ERR (CLI_RRD_INVALID_SRCPROTO);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    u4FsRrdControlNetMask = 0;/*** Assigned some value to remove warnings **/
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdControlDestProto
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                testValFsRrdControlDestProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdControlDestProto ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdControlDestProto (UINT4 *pu4ErrorCode,
                                UINT4 u4FsRrdControlDestIpAddress,
                                UINT4 u4FsRrdControlNetMask,
                                INT4 i4TestValFsRrdControlDestProto)
#else
INT1
nmhTestv2FsRrdControlDestProto (*pu4ErrorCode, u4FsRrdControlDestIpAddress,
                                u4FsRrdControlNetMask,
                                i4TestValFsRrdControlDestProto)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4                i4TestValFsRrdControlDestProto;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    /* Destination Protocol can be RIP or OSPF or BGP or ANY */
    if ((i4TestValFsRrdControlDestProto == RTM_ANY_PROTOCOL) ||
        (i4TestValFsRrdControlDestProto & RTM_ALL_RPS_MASK) ||
        (i4TestValFsRrdControlDestProto & RTM_OSPF_AND_RIP_MASK) ||
        (i4TestValFsRrdControlDestProto & RTM_BGP_AND_OSPF_MASK) ||
        (i4TestValFsRrdControlDestProto & RTM_BGP_AND_RIP_MASK))
    {
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD_INVALID_DESTPROTO);
    }

    u4FsRrdControlNetMask = 0;/*** Assigned some value to remove warnings **/
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdControlRouteExportFlag
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                testValFsRrdControlRouteExportFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdControlRouteExportFlag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdControlRouteExportFlag (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsRrdControlDestIpAddress,
                                      UINT4 u4FsRrdControlNetMask,
                                      INT4 i4TestValFsRrdControlRouteExportFlag)
#else
INT1
nmhTestv2FsRrdControlRouteExportFlag (*pu4ErrorCode,
                                      u4FsRrdControlDestIpAddress,
                                      u4FsRrdControlNetMask,
                                      i4TestValFsRrdControlRouteExportFlag)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4                i4TestValFsRrdControlRouteExportFlag;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* No such index */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RRD_NOENTRY_CTRL_TABLE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrdControlRouteExportFlag == RTM_ROUTE_PERMIT) ||
        (i4TestValFsRrdControlRouteExportFlag == RTM_ROUTE_DENY))
    {
        /* the values are ok */
        /* check whether the Route Export flag is set or not. For all entries
         * other than the ALL ROUTE entry, setting Export Flag is possible
         * only once.
         */
        if ((u4FsRrdControlDestIpAddress != IP_ANY_ADDR) &&
            (pRtmCtrlInfo->u1RtExportFlag != RTM_ROUTE_PERMIT))
        {
            /* Entry is already configured. */
            if (pRtmCtrlInfo->u1RtExportFlag ==
                i4TestValFsRrdControlRouteExportFlag)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            i1RetVal = SNMP_SUCCESS;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    u4FsRrdControlNetMask = 0;/*** Assigned some value to remove warnings **/
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdControlRowStatus
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask

                The Object 
                testValFsRrdControlRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdControlRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdControlRowStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4FsRrdControlDestIpAddress,
                                UINT4 u4FsRrdControlNetMask,
                                INT4 i4TestValFsRrdControlRowStatus)
#else
INT1
nmhTestv2FsRrdControlRowStatus (*pu4ErrorCode, u4FsRrdControlDestIpAddress,
                                u4FsRrdControlNetMask,
                                i4TestValFsRrdControlRowStatus)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRrdControlDestIpAddress;
     UINT4               u4FsRrdControlNetMask;
     INT4                i4TestValFsRrdControlRowStatus;
#endif
{
    tRrdControlInfo    *pRtmCtrlInfo = NULL;
    UINT4               u4EntryCnt = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pRtmCxt->RtmCtrlList), pRtmCtrlInfo, tRrdControlInfo *)
    {
        u4EntryCnt++;
        /* Check whether the IP address and mask match */
        if ((pRtmCtrlInfo->u4DestNet == u4FsRrdControlDestIpAddress)
            && (pRtmCtrlInfo->u4SubnetRange == u4FsRrdControlNetMask))
        {
            /* We found the correct node in the list */
            break;
        }
    }

    if (pRtmCtrlInfo == NULL)
    {
        /* check whether the Row status is create and wait */
        if (i4TestValFsRrdControlRowStatus == RTM_CREATE_AND_WAIT)
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RRD_NOENTRY_CTRL_TABLE);
        }
    }
    else
    {
        /* The entry already exists */
        /* either the status should be active or destroy */
        if ((i4TestValFsRrdControlRowStatus == RTM_ACTIVE) ||
            (i4TestValFsRrdControlRowStatus == RTM_DESTROY))
        {
            i1RetVal = SNMP_SUCCESS;
        }
        else if (i4TestValFsRrdControlRowStatus == RTM_CREATE_AND_WAIT)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            i1RetVal = SNMP_FAILURE;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    u4FsRrdControlNetMask = 0;/*** Assigned some value to remove warnings **/
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhDepv2FsRrdControlTable
 Input       :  The Indices
                FsRrdControlDestIpAddress
                FsRrdControlNetMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrdControlTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRrdRoutingProtoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRrdRoutingProtoTable
 Input       :  The Indices
                FsRrdRoutingProtoId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsRrdRoutingProtoTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsRrdRoutingProtoTable (INT4 i4FsRrdRoutingProtoId)
#else
INT1
nmhValidateIndexInstanceFsRrdRoutingProtoTable (i4FsRrdRoutingProtoId)
     INT4                i4FsRrdRoutingProtoId;
#endif
{
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Check whether this routing protocol has registered with RTM or not */
    RegnId.u2ProtoId = (UINT2) i4FsRrdRoutingProtoId;
    RegnId.u4ContextId = pRtmCxt->u4ContextId;

    u2Index = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, &RegnId);

    if (u2Index == RTM_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRrdRoutingProtoTable
 Input       :  The Indices
                FsRrdRoutingProtoId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstFsRrdRoutingProtoTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFirstIndexFsRrdRoutingProtoTable (INT4 *pi4FsRrdRoutingProtoId)
#else
INT1
nmhGetFirstIndexFsRrdRoutingProtoTable (pi4FsRrdRoutingProtoId)
     INT4               *pi4FsRrdRoutingProtoId;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Check whether this routing protocol has registered with RTM or not */
    /* RTM assumes the registration of LOCAL or DIRECT ROUTES First and 
       no one will deregister the local route. Local route will be registered
       first and hence there will not be any updation in the start index */
    if (pRtmCxt->u2RtmRtStartIndex < MAX_ROUTING_PROTOCOLS)
    {
        if (pRtmCxt->aRtmRegnTable[pRtmCxt->u2RtmRtStartIndex].
            u2RoutingProtocolId != RTM_INVALID_ROUTING_PROTOCOL_ID)
        {
            *pi4FsRrdRoutingProtoId =
                pRtmCxt->aRtmRegnTable[pRtmCxt->u2RtmRtStartIndex].
                u2RoutingProtocolId;
            return SNMP_SUCCESS;
        }
    }

    /* No routing protocol has registered with RTM till now */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRrdRoutingProtoTable
 Input       :  The Indices
                FsRrdRoutingProtoId
                nextFsRrdRoutingProtoId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextFsRrdRoutingProtoTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetNextIndexFsRrdRoutingProtoTable (INT4 i4FsRrdRoutingProtoId,
                                       INT4 *pi4NextFsRrdRoutingProtoId)
#else
INT1
nmhGetNextIndexFsRrdRoutingProtoTable (i4FsRrdRoutingProtoId,
                                       pi4NextFsRrdRoutingProtoId)
     INT4                i4FsRrdRoutingProtoId;
     INT4               *pi4NextFsRrdRoutingProtoId;
#endif
{
    UINT2               u2Index = 0;
    UINT2               u2NextRpId = MAX_ROUTING_PROTOCOLS;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FsRrdRoutingProtoId < 0)
    {
        return SNMP_FAILURE;
    }

    /* Check whether this routing protocol has registered with RTM or not */

    for (u2Index = pRtmCxt->u2RtmRtStartIndex;
         u2Index < MAX_ROUTING_PROTOCOLS;
         u2Index = pRtmCxt->aRtmRegnTable[u2Index].u2NextRegId)
    {
        if (pRtmCxt->aRtmRegnTable[u2Index].u2RoutingProtocolId
            > i4FsRrdRoutingProtoId)
        {
            /* this routing protocol has registered with RTM */
            if (pRtmCxt->aRtmRegnTable[u2Index].u2RoutingProtocolId <
                u2NextRpId)
            {
                u2NextRpId =
                    pRtmCxt->aRtmRegnTable[u2Index].u2RoutingProtocolId;
            }
        }
    }

    if (u2NextRpId != MAX_ROUTING_PROTOCOLS)
    {
        *pi4NextFsRrdRoutingProtoId = (INT4) u2NextRpId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRrdRoutingRegnId
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                retValFsRrdRoutingRegnId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdRoutingRegnId ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdRoutingRegnId (INT4 i4FsRrdRoutingProtoId,
                          INT4 *pi4RetValFsRrdRoutingRegnId)
#else
INT1
nmhGetFsRrdRoutingRegnId (i4FsRrdRoutingProtoId, pi4RetValFsRrdRoutingRegnId)
     INT4                i4FsRrdRoutingProtoId;
     INT4               *pi4RetValFsRrdRoutingRegnId;
#endif
{
    /* Check whether this routing protocol has registered with RTM or not */
    tRtmRegnId          RegnId;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    RegnId.u2ProtoId = (UINT2) i4FsRrdRoutingProtoId;

    *pi4RetValFsRrdRoutingRegnId =
        (INT4) RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, &RegnId);

    if (*pi4RetValFsRrdRoutingRegnId == RTM_INVALID_REGN_ID)
    {
        /* this protocol not registered with RTM */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRrdRoutingProtoTaskIdent
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                retValFsRrdRoutingProtoTaskIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdRoutingProtoTaskIdent ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdRoutingProtoTaskIdent (INT4 i4FsRrdRoutingProtoId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsRrdRoutingProtoTaskIdent)
#else
INT1
nmhGetFsRrdRoutingProtoTaskIdent (i4FsRrdRoutingProtoId,
                                  pRetValFsRrdRoutingProtoTaskIdent)
     INT4                i4FsRrdRoutingProtoId;
     tSNMP_OCTET_STRING_TYPE *pRetValFsRrdRoutingProtoTaskIdent;
#endif
{
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    RegnId.u2ProtoId = (UINT2) i4FsRrdRoutingProtoId;

    u2Index = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, &RegnId);
    if (u2Index == RTM_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (pRetValFsRrdRoutingProtoTaskIdent->pu1_OctetList,
             pRtmCxt->aRtmRegnTable[u2Index].au1TaskName,
             RTM_MAX_TASK_NAME_LEN);

    pRetValFsRrdRoutingProtoTaskIdent->i4_Length = RTM_MAX_TASK_NAME_LEN;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrdRoutingProtoQueueIdent
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                retValFsRrdRoutingProtoQueueIdent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdRoutingProtoQueueIdent ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdRoutingProtoQueueIdent (INT4 i4FsRrdRoutingProtoId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsRrdRoutingProtoQueueIdent)
#else
INT1
nmhGetFsRrdRoutingProtoQueueIdent (i4FsRrdRoutingProtoId,
                                   pRetValFsRrdRoutingProtoQueueIdent)
     INT4                i4FsRrdRoutingProtoId;
     tSNMP_OCTET_STRING_TYPE *pRetValFsRrdRoutingProtoQueueIdent;
#endif
{
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    RegnId.u2ProtoId = (UINT2) i4FsRrdRoutingProtoId;

    u2Index = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, &RegnId);
    if (u2Index == RTM_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }

    STRNCPY (pRetValFsRrdRoutingProtoQueueIdent->pu1_OctetList,
             pRtmCxt->aRtmRegnTable[u2Index].au1QName, RTM_MAX_Q_NAME_LEN);

    pRetValFsRrdRoutingProtoQueueIdent->i4_Length = RTM_MAX_Q_NAME_LEN;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRrdAllowOspfAreaRoutes
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                retValFsRrdAllowOspfAreaRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdAllowOspfAreaRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdAllowOspfAreaRoutes (INT4 i4FsRrdRoutingProtoId,
                                INT4 *pi4RetValFsRrdAllowOspfAreaRoutes)
#else
INT1
nmhGetFsRrdAllowOspfAreaRoutes (i4FsRrdRoutingProtoId,
                                pi4RetValFsRrdAllowOspfAreaRoutes)
     INT4                i4FsRrdRoutingProtoId;
     INT4               *pi4RetValFsRrdAllowOspfAreaRoutes;
#endif
{
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    RegnId.u2ProtoId = (UINT2) i4FsRrdRoutingProtoId;

    u2Index = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, &RegnId);
    if (u2Index == RTM_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRrdAllowOspfAreaRoutes =
        (INT4) pRtmCxt->aRtmRegnTable[u2Index].u1AllowOspfInternals;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRrdAllowOspfExtRoutes
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                retValFsRrdAllowOspfExtRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsRrdAllowOspfExtRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhGetFsRrdAllowOspfExtRoutes (INT4 i4FsRrdRoutingProtoId,
                               INT4 *pi4RetValFsRrdAllowOspfExtRoutes)
#else
INT1
nmhGetFsRrdAllowOspfExtRoutes (i4FsRrdRoutingProtoId,
                               pi4RetValFsRrdAllowOspfExtRoutes)
     INT4                i4FsRrdRoutingProtoId;
     INT4               *pi4RetValFsRrdAllowOspfExtRoutes;
#endif
{
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    RegnId.u2ProtoId = (UINT2) i4FsRrdRoutingProtoId;

    u2Index = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, &RegnId);
    if (u2Index == RTM_INVALID_REGN_ID)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsRrdAllowOspfExtRoutes =
        (INT4) pRtmCxt->aRtmRegnTable[u2Index].u1AllowOspfExternals;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRrdAllowOspfAreaRoutes
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                setValFsRrdAllowOspfAreaRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdAllowOspfAreaRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdAllowOspfAreaRoutes (INT4 i4FsRrdRoutingProtoId,
                                INT4 i4SetValFsRrdAllowOspfAreaRoutes)
#else
INT1
nmhSetFsRrdAllowOspfAreaRoutes (i4FsRrdRoutingProtoId,
                                i4SetValFsRrdAllowOspfAreaRoutes)
     INT4                i4FsRrdRoutingProtoId;
     INT4                i4SetValFsRrdAllowOspfAreaRoutes;
#endif
{
    tIpBuf             *pBuf = NULL;
    tRtmSnmpIfMsg      *pRtmParms = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* the set routine will be called whenever there is change in the allow
       of internal routes - enabled to disabled or disabled to enabled */

    if ((pBuf = IP_ALLOCATE_BUF (sizeof (UINT4), 0)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pRtmParms = (tRtmSnmpIfMsg *) IP_GET_MODULE_DATA_PTR (pBuf);
    pRtmParms->u1Cmd = RTM_OSPF_INT_EXT_CHG;
    pRtmParms->RegnId.u2ProtoId = (UINT1) i4FsRrdRoutingProtoId;
    pRtmParms->RegnId.u4ContextId = pRtmCxt->u4ContextId;
    pRtmParms->u1OspfRtType = RTM_OSPF_INTERNAL_ROUTES;    /* can be inter area
                                                           or inter area */
    pRtmParms->u1PermitOrDeny = (UINT1) i4SetValFsRrdAllowOspfAreaRoutes;
    if (OsixQueSend (gRtmSnmpMsgQId, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return (SNMP_FAILURE);
    }

    OsixEvtSend (gRtmTaskId, RTM_FILTER_ADDED_EVENT);
    UtilRtmIncMsrForRrdProtoTable (pRtmCxt->u4ContextId,
                                   i4FsRrdRoutingProtoId,
                                   i4SetValFsRrdAllowOspfAreaRoutes,
                                   FsMIRrdAllowOspfAreaRoutes,
                                   (sizeof (FsMIRrdAllowOspfAreaRoutes) /
                                    sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRrdAllowOspfExtRoutes
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                setValFsRrdAllowOspfExtRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsRrdAllowOspfExtRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhSetFsRrdAllowOspfExtRoutes (INT4 i4FsRrdRoutingProtoId,
                               INT4 i4SetValFsRrdAllowOspfExtRoutes)
#else
INT1
nmhSetFsRrdAllowOspfExtRoutes (i4FsRrdRoutingProtoId,
                               i4SetValFsRrdAllowOspfExtRoutes)
     INT4                i4FsRrdRoutingProtoId;
     INT4                i4SetValFsRrdAllowOspfExtRoutes;
#endif
{
    tIpBuf             *pBuf = NULL;
    tRtmSnmpIfMsg      *pRtmParms = NULL;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    /* the set routine will be called whenever there is change in the allow
       of internal routes - enabled to disabled or disabled to enabled */

    if ((pBuf = IP_ALLOCATE_BUF (sizeof (UINT4), 0)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pRtmParms = (tRtmSnmpIfMsg *) IP_GET_MODULE_DATA_PTR (pBuf);
    pRtmParms->u1Cmd = RTM_OSPF_INT_EXT_CHG;
    pRtmParms->RegnId.u2ProtoId = (UINT1) i4FsRrdRoutingProtoId;
    pRtmParms->RegnId.u4ContextId = pRtmCxt->u4ContextId;
    pRtmParms->u1OspfRtType = RTM_OSPF_EXTERNAL_ROUTES;    /* can be inter area
                                                           or inter area */
    pRtmParms->u1PermitOrDeny = (UINT1) i4SetValFsRrdAllowOspfExtRoutes;

    if (OsixQueSend (gRtmSnmpMsgQId, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return (SNMP_FAILURE);
    }

    OsixEvtSend (gRtmTaskId, RTM_FILTER_ADDED_EVENT);
    UtilRtmIncMsrForRrdProtoTable (pRtmCxt->u4ContextId,
                                   i4FsRrdRoutingProtoId,
                                   i4SetValFsRrdAllowOspfExtRoutes,
                                   FsMIRrdAllowOspfExtRoutes,
                                   (sizeof (FsMIRrdAllowOspfExtRoutes) /
                                    sizeof (UINT4)));
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRrdAllowOspfAreaRoutes
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                testValFsRrdAllowOspfAreaRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdAllowOspfAreaRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdAllowOspfAreaRoutes (UINT4 *pu4ErrorCode,
                                   INT4 i4FsRrdRoutingProtoId,
                                   INT4 i4TestValFsRrdAllowOspfAreaRoutes)
#else
INT1
nmhTestv2FsRrdAllowOspfAreaRoutes (*pu4ErrorCode, i4FsRrdRoutingProtoId,
                                   i4TestValFsRrdAllowOspfAreaRoutes)
     UINT4              *pu4ErrorCode;
     INT4                i4FsRrdRoutingProtoId;
     INT4                i4TestValFsRrdAllowOspfAreaRoutes;
#endif
{
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrdAllowOspfAreaRoutes != RTM_REDISTRIBUTION_ENABLED) &&
        (i4TestValFsRrdAllowOspfAreaRoutes != RTM_REDISTRIBUTION_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RRD_NO_REDISTRIBUTE);
        return i1RetVal;
    }

    RegnId.u2ProtoId = (UINT2) i4FsRrdRoutingProtoId;
    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    u2Index = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, &RegnId);

    if (u2Index == RTM_INVALID_REGN_ID)
    {
        /* this protocol not registered with RTM */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RRD_PROTO_NOREG);
        return SNMP_FAILURE;
    }

    switch (i4FsRrdRoutingProtoId)
    {
        case RIP_ID:
        case BGP_ID:
            i1RetVal = SNMP_SUCCESS;
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_RRD_INVALID_DESTPROTO);
            break;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsRrdAllowOspfExtRoutes
 Input       :  The Indices
                FsRrdRoutingProtoId

                The Object 
                testValFsRrdAllowOspfExtRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsRrdAllowOspfExtRoutes ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
#ifdef __STDC__
INT1
nmhTestv2FsRrdAllowOspfExtRoutes (UINT4 *pu4ErrorCode,
                                  INT4 i4FsRrdRoutingProtoId,
                                  INT4 i4TestValFsRrdAllowOspfExtRoutes)
#else
INT1
nmhTestv2FsRrdAllowOspfExtRoutes (*pu4ErrorCode, i4FsRrdRoutingProtoId,
                                  i4TestValFsRrdAllowOspfExtRoutes)
     UINT4              *pu4ErrorCode;
     INT4                i4FsRrdRoutingProtoId;
     INT4                i4TestValFsRrdAllowOspfExtRoutes;
#endif
{
    tRtmRegnId          RegnId;
    UINT2               u2Index = 0;
    INT1                i1RetVal = SNMP_FAILURE;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRrdAllowOspfExtRoutes != RTM_REDISTRIBUTION_ENABLED) &&
        (i4TestValFsRrdAllowOspfExtRoutes != RTM_REDISTRIBUTION_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return i1RetVal;
    }

    RegnId.u2ProtoId = (UINT2) i4FsRrdRoutingProtoId;
    RegnId.u4ContextId = pRtmCxt->u4ContextId;
    u2Index = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, &RegnId);

    if (u2Index == RTM_INVALID_REGN_ID)
    {
        /* this protocol not registered with RTM */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RRD_PROTO_NOREG);
        return SNMP_FAILURE;
    }

    switch (i4FsRrdRoutingProtoId)
    {
        case RIP_ID:
        case BGP_ID:
            i1RetVal = SNMP_SUCCESS;
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_RRD_INVALID_DESTPROTO);
            break;
    }
    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRrdRoutingProtoTable
 Input       :  The Indices
                FsRrdRoutingProtoId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRrdRoutingProtoTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRtmCommonRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRtmCommonRouteTable
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

#ifdef __STDC__
INT1
nmhValidateIndexInstanceFsRtmCommonRouteTable (UINT4 u4FsRtmCommonRouteDest,
                                               UINT4 u4FsRtmCommonRouteMask,
                                               INT4 i4FsRtmCommonRouteTos,
                                               UINT4 u4FsRtmCommonRouteNextHop)
#else
INT1
nmhValidateIndexInstanceFsRtmCommonRouteTable (u4FsRtmCommonRouteDest,
                                               u4FsRtmCommonRouteMask,
                                               i4FsRtmCommonRouteTos,
                                               u4FsRtmCommonRouteNextHop)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
#endif
{
    return (nmhValidateIndexInstanceIpCidrRouteTable (u4FsRtmCommonRouteDest,
                                                      u4FsRtmCommonRouteMask,
                                                      i4FsRtmCommonRouteTos,
                                                      u4FsRtmCommonRouteNextHop));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRtmCommonRouteTable
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

#ifdef __STDC__
INT1
nmhGetFirstIndexFsRtmCommonRouteTable (UINT4 *pu4FsRtmCommonRouteDest,
                                       UINT4 *pu4FsRtmCommonRouteMask,
                                       INT4 *pi4FsRtmCommonRouteTos,
                                       UINT4 *pu4FsRtmCommonRouteNextHop)
#else
INT1
nmhGetFirstIndexFsRtmCommonRouteTable (*pu4FsRtmCommonRouteDest,
                                       *pu4FsRtmCommonRouteMask,
                                       *pi4FsRtmCommonRouteTos,
                                       *pu4FsRtmCommonRouteNextHop)
     UINT4              *pu4FsRtmCommonRouteDest;
     UINT4              *pu4FsRtmCommonRouteMask;
     INT4               *pi4FsRtmCommonRouteTos;
     UINT4              *pu4FsRtmCommonRouteNextHop;
#endif
{
     return (nmhGetFirstIndexIpCidrRouteTable (pu4FsRtmCommonRouteDest,
                                               pu4FsRtmCommonRouteMask,
                                               pi4FsRtmCommonRouteTos,
                                               pu4FsRtmCommonRouteNextHop));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRtmCommonRouteTable
 Input       :  The Indices
                FsRtmCommonRouteDest
                nextFsRtmCommonRouteDest
                FsRtmCommonRouteMask
                nextFsRtmCommonRouteMask
                FsRtmCommonRouteTos
                nextFsRtmCommonRouteTos
                FsRtmCommonRouteNextHop
                nextFsRtmCommonRouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
#ifdef __STDC__
INT1
nmhGetNextIndexFsRtmCommonRouteTable (UINT4 u4FsRtmCommonRouteDest,
                                      UINT4 *pu4NextFsRtmCommonRouteDest,
                                      UINT4 u4FsRtmCommonRouteMask,
                                      UINT4 *pu4NextFsRtmCommonRouteMask,
                                      INT4 i4FsRtmCommonRouteTos,
                                      INT4 *pi4NextFsRtmCommonRouteTos,
                                      UINT4 u4FsRtmCommonRouteNextHop,
                                      UINT4 *pu4NextFsRtmCommonRouteNextHop)
#else
INT1
nmhGetNextIndexFsRtmCommonRouteTable (u4FsRtmCommonRouteDest,
                                      *pu4NextFsRtmCommonRouteDest,
                                      u4FsRtmCommonRouteMask,
                                      *pu4NextFsRtmCommonRouteMask,
                                      i4FsRtmCommonRouteTos,
                                      *pi4NextFsRtmCommonRouteTos,
                                      u4FsRtmCommonRouteNextHop,
                                      *pu4NextFsRtmCommonRouteNextHop)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4              *pu4NextFsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     UINT4              *pu4NextFsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     INT4               *pi4NextFsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     UINT4              *pu4NextFsRtmCommonRouteNextHop;
#endif
{
     return (nmhGetNextIndexIpCidrRouteTable (u4FsRtmCommonRouteDest,
                                              pu4NextFsRtmCommonRouteDest,
                                              u4FsRtmCommonRouteMask,
                                              pu4NextFsRtmCommonRouteMask,
                                              i4FsRtmCommonRouteTos,
                                              pi4NextFsRtmCommonRouteTos,
                                              u4FsRtmCommonRouteNextHop, 
                                              pu4NextFsRtmCommonRouteNextHop));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRouteIfIndex
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRouteIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsRtmCommonRouteIfIndex (UINT4 u4FsRtmCommonRouteDest,
                               UINT4 u4FsRtmCommonRouteMask,
                               INT4 i4FsRtmCommonRouteTos,
                               UINT4 u4FsRtmCommonRouteNextHop,
                               INT4 *pi4RetValFsRtmCommonRouteIfIndex)
#else
INT1
nmhGetFsRtmCommonRouteIfIndex (u4FsRtmCommonRouteDest,
                               u4FsRtmCommonRouteMask,
                               i4FsRtmCommonRouteTos,
                               u4FsRtmCommonRouteNextHop,
                               *pi4RetValFsRtmCommonRouteIfIndex)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4               *pi4RetValFsRtmCommonRouteIfIndex;
#endif
{
    UINT4               u4CfaIfIndex = 0;
    UINT4               u4ContextId = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4ContextId = pRtmCxt->u4ContextId;

    if (IpFwdTblGetObjectInCxt (u4ContextId, u4FsRtmCommonRouteDest,
                                u4FsRtmCommonRouteMask, i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRouteIfIndex,
                                IPFWD_ROUTE_IFINDEX) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }


    if (*pi4RetValFsRtmCommonRouteIfIndex != IPIF_INVALID_INDEX)
    {
        /* Get the IfIndex from the Port */
        if (NetIpv4GetCfaIfIndexFromPort
            ((UINT4) *pi4RetValFsRtmCommonRouteIfIndex,
             &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        *pi4RetValFsRtmCommonRouteIfIndex = (INT4) u4CfaIfIndex;
    }
    else 
    {
        *pi4RetValFsRtmCommonRouteIfIndex = IP_IF_TYPE_INVALID;
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRouteType
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsRtmCommonRouteType (UINT4 u4FsRtmCommonRouteDest,
                            UINT4 u4FsRtmCommonRouteMask,
                            INT4 i4FsRtmCommonRouteTos,
                            UINT4 u4FsRtmCommonRouteNextHop,
                            INT4 *pi4RetValFsRtmCommonRouteType)
#else
INT1
nmhGetFsRtmCommonRouteType (u4FsRtmCommonRouteDest,
                            u4FsRtmCommonRouteMask,
                            i4FsRtmCommonRouteTos,
                            u4FsRtmCommonRouteNextHop,
                            *pi4RetValFsRtmCommonRouteType)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4               *pi4RetValFsRtmCommonRouteType;
#endif
{
    UINT4               u4ContextId = 0;

    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;
    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4ContextId = pRtmCxt->u4ContextId;

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRouteType,
                                IPFWD_ROUTE_TYPE) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRouteProto
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRouteProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsRtmCommonRouteProto (UINT4 u4FsRtmCommonRouteDest,
                             UINT4 u4FsRtmCommonRouteMask,
                             INT4 i4FsRtmCommonRouteTos,
                             UINT4 u4FsRtmCommonRouteNextHop,
                             INT4 *pi4RetValFsRtmCommonRouteProto)
#else
INT1
nmhGetFsRtmCommonRouteProto (u4FsRtmCommonRouteDest,
                             u4FsRtmCommonRouteMask,
                             i4FsRtmCommonRouteTos,
                             u4FsRtmCommonRouteNextHop,
                             *pi4RetValFsRtmCommonRouteProto)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4               *pi4RetValFsRtmCommonRouteProto;
#endif
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;
    UINT4               u4ContextId = 0;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4ContextId = pRtmCxt->u4ContextId;

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRouteProto,
                                IPFWD_ROUTE_PROTO) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRouteAge
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsRtmCommonRouteAge (UINT4 u4FsRtmCommonRouteDest,
                           UINT4 u4FsRtmCommonRouteMask,
                           INT4 i4FsRtmCommonRouteTos,
                           UINT4 u4FsRtmCommonRouteNextHop,
                           INT4 *pi4RetValFsRtmCommonRouteAge)
#else
INT1
nmhGetFsRtmCommonRouteAge (u4FsRtmCommonRouteDest,
                           u4FsRtmCommonRouteMask,
                           i4FsRtmCommonRouteTos,
                           u4FsRtmCommonRouteNextHop,
                           *pi4RetValFsRtmCommonRouteAge)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4               *pi4RetValFsRtmCommonRouteAge;
#endif
{

    UINT4               u4ContextId = 0;

    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4ContextId = pRtmCxt->u4ContextId;

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRouteAge,
                                IPFWD_ROUTE_AGE) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRouteInfo
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRouteInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsRtmCommonRouteInfo (UINT4 u4FsRtmCommonRouteDest,
                            UINT4 u4FsRtmCommonRouteMask,
                            INT4 i4FsRtmCommonRouteTos,
                            UINT4 u4FsRtmCommonRouteNextHop,
                            tSNMP_OID_TYPE * pRetValFsRtmCommonRouteInfo)
#else
INT1
nmhGetFsRtmCommonRouteInfo (u4FsRtmCommonRouteDest,
                            u4FsRtmCommonRouteMask,
                            i4FsRtmCommonRouteTos,
                            u4FsRtmCommonRouteNextHop,
                            *pRetValFsRtmCommonRouteInfo)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     tSNMP_OID_TYPE     *pRetValFsRtmCommonRouteInfo;
#endif
{
    UINT4               u4Unused = 0;
    INT4                i4Unused = 0;
    UINT4               u4ContextId = 0;

    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;
    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4ContextId = pRtmCxt->u4ContextId;

    /* Makefile Changes - unused parameter */
    u4Unused = u4FsRtmCommonRouteDest;
    u4Unused = u4FsRtmCommonRouteMask;
    i4Unused = i4FsRtmCommonRouteTos;
    u4Unused = u4FsRtmCommonRouteNextHop;
    u4Unused = u4ContextId;

    /* Return NULL VarBind and the memory Allocated by the Agent */
    (*pRetValFsRtmCommonRouteInfo).u4_Length = 2;
    (*pRetValFsRtmCommonRouteInfo).pu4_OidList[0] = 0;
    (*pRetValFsRtmCommonRouteInfo).pu4_OidList[1] = 0;

    UNUSED_PARAM (u4Unused);
    UNUSED_PARAM (i4Unused);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRouteNextHopAS
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRouteNextHopAS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsRtmCommonRouteNextHopAS (UINT4 u4FsRtmCommonRouteDest,
                                 UINT4 u4FsRtmCommonRouteMask,
                                 INT4 i4FsRtmCommonRouteTos,
                                 UINT4 u4FsRtmCommonRouteNextHop,
                                 INT4 *pi4RetValFsRtmCommonRouteNextHopAS)
#else
INT1
nmhGetFsRtmCommonRouteNextHopAS (u4FsRtmCommonRouteDest,
                                 u4FsRtmCommonRouteMask,
                                 i4FsRtmCommonRouteTos,
                                 u4FsRtmCommonRouteNextHop,
                                 *pi4RetValFsRtmCommonRouteNextHopAS)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4               *pi4RetValFsRtmCommonRouteNextHopAS;
#endif
{
    UINT4               u4ContextId = 0;
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4ContextId = pRtmCxt->u4ContextId;

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRouteNextHopAS,
                                IPFWD_ROUTE_NEXTHOPAS) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRouteMetric1
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRouteMetric1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsRtmCommonRouteMetric1 (UINT4 u4FsRtmCommonRouteDest,
                               UINT4 u4FsRtmCommonRouteMask,
                               INT4 i4FsRtmCommonRouteTos,
                               UINT4 u4FsRtmCommonRouteNextHop,
                               INT4 *pi4RetValFsRtmCommonRouteMetric1)
#else
INT1
nmhGetFsRtmCommonRouteMetric1 (u4FsRtmCommonRouteDest,
                               u4FsRtmCommonRouteMask,
                               i4FsRtmCommonRouteTos,
                               u4FsRtmCommonRouteNextHop,
                               *pi4RetValFsRtmCommonRouteMetric1)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4               *pi4RetValFsRtmCommonRouteMetric1;
#endif
{
    UINT4               u4ContextId = 0;

    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u4ContextId = pRtmCxt->u4ContextId;

    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRouteMetric1,
                                IPFWD_ROUTE_METRIC1) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRoutePrivateStatus
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRoutePrivateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRtmCommonRoutePrivateStatus (UINT4 u4FsRtmCommonRouteDest,
                                     UINT4 u4FsRtmCommonRouteMask,
                                     INT4 i4FsRtmCommonRouteTos,
                                     UINT4 u4FsRtmCommonRouteNextHop,
                                     INT4
                                     *pi4RetValFsRtmCommonRoutePrivateStatus)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (IpFwdTblGetObjectInCxt (pRtmCxt->u4ContextId,
                                u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRoutePrivateStatus,
                                IPFWD_REDIS_CTRL_FLAG) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRoutePreference
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object
                retValFsRtmCommonRoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsRtmCommonRoutePreference(UINT4 u4FsRtmCommonRouteDest, 
                                 UINT4 u4FsRtmCommonRouteMask, 
                                 INT4 i4FsRtmCommonRouteTos, 
                                 UINT4 u4FsRtmCommonRouteNextHop, 
                                 INT4 *pi4RetValFsRtmCommonRoutePreference)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (IpFwdTblGetObjectInCxt (pRtmCxt->u4ContextId,
                                u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRoutePreference,
                                IPFWD_ROUTE_PREFERENCE) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsRtmCommonRouteStatus
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                retValFsRtmCommonRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhGetFsRtmCommonRouteStatus (UINT4 u4FsRtmCommonRouteDest,
                              UINT4 u4FsRtmCommonRouteMask,
                              INT4 i4FsRtmCommonRouteTos,
                              UINT4 u4FsRtmCommonRouteNextHop,
                              INT4 *pi4RetValFsRtmCommonRouteStatus)
#else
INT1
nmhGetFsRtmCommonRouteStatus (u4FsRtmCommonRouteDest,
                              u4FsRtmCommonRouteMask,
                              i4FsRtmCommonRouteTos,
                              u4FsRtmCommonRouteNextHop,
                              *pi4RetValFsRtmCommonRouteStatus)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4               *pi4RetValFsRtmCommonRouteStatus;
#endif
{
    UINT4               u4ContextId = 0;

    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;
    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    u4ContextId = pRtmCxt->u4ContextId;
    if (IpFwdTblGetObjectInCxt (u4ContextId,
                                u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                pi4RetValFsRtmCommonRouteStatus,
                                IPFWD_ROUTE_ROW_STATUS) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRtmCommonRouteIfIndex
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                setValFsRtmCommonRouteIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsRtmCommonRouteIfIndex (UINT4 u4FsRtmCommonRouteDest,
                               UINT4 u4FsRtmCommonRouteMask,
                               INT4 i4FsRtmCommonRouteTos,
                               UINT4 u4FsRtmCommonRouteNextHop,
                               INT4 i4SetValFsRtmCommonRouteIfIndex)
#else
INT1
nmhSetFsRtmCommonRouteIfIndex (u4FsRtmCommonRouteDest,
                               u4FsRtmCommonRouteMask,
                               i4FsRtmCommonRouteTos,
                               u4FsRtmCommonRouteNextHop,
                               i4SetValFsRtmCommonRouteIfIndex)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4SetValFsRtmCommonRouteIfIndex;
#endif
{
    return (nmhSetIpCidrRouteIfIndex (u4FsRtmCommonRouteDest,
                                      u4FsRtmCommonRouteMask,
                                      i4FsRtmCommonRouteTos,
                                      u4FsRtmCommonRouteNextHop,
                                      i4SetValFsRtmCommonRouteIfIndex));
}

/****************************************************************************
 Function    :  nmhSetFsRtmCommonRouteType
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                setValFsRtmCommonRouteType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsRtmCommonRouteType (UINT4 u4FsRtmCommonRouteDest,
                            UINT4 u4FsRtmCommonRouteMask,
                            INT4 i4FsRtmCommonRouteTos,
                            UINT4 u4FsRtmCommonRouteNextHop,
                            INT4 i4SetValFsRtmCommonRouteType)
#else
INT1
nmhSetFsRtmCommonRouteType (u4FsRtmCommonRouteDest,
                            u4FsRtmCommonRouteMask,
                            i4FsRtmCommonRouteTos,
                            u4FsRtmCommonRouteNextHop,
                            i4SetValFsRtmCommonRouteType)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4SetValFsRtmCommonRouteType;
#endif
{
    return (nmhSetIpCidrRouteType (u4FsRtmCommonRouteDest,
                                   u4FsRtmCommonRouteMask,
                                   i4FsRtmCommonRouteTos,
                                   u4FsRtmCommonRouteNextHop,
                                   i4SetValFsRtmCommonRouteType));
}

/****************************************************************************
 Function    :  nmhSetFsRtmCommonRouteInfo
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                setValFsRtmCommonRouteInfo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsRtmCommonRouteInfo (UINT4 u4FsRtmCommonRouteDest,
                            UINT4 u4FsRtmCommonRouteMask,
                            INT4 i4FsRtmCommonRouteTos,
                            UINT4 u4FsRtmCommonRouteNextHop,
                            tSNMP_OID_TYPE * pSetValFsRtmCommonRouteInfo)
#else
INT1
nmhSetFsRtmCommonRouteInfo (u4FsRtmCommonRouteDest,
                            u4FsRtmCommonRouteMask,
                            i4FsRtmCommonRouteTos,
                            u4FsRtmCommonRouteNextHop,
                            *pSetValFsRtmCommonRouteInfo)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     tSNMP_OID_TYPE     *pSetValFsRtmCommonRouteInfo;
#endif
{
    return (nmhSetIpCidrRouteInfo (u4FsRtmCommonRouteDest,
                                   u4FsRtmCommonRouteMask,
                                   i4FsRtmCommonRouteTos,
                                   u4FsRtmCommonRouteNextHop,
                                   pSetValFsRtmCommonRouteInfo));
}

/****************************************************************************
 Function    :  nmhSetFsRtmCommonRouteNextHopAS
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                setValFsRtmCommonRouteNextHopAS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsRtmCommonRouteNextHopAS (UINT4 u4FsRtmCommonRouteDest,
                                 UINT4 u4FsRtmCommonRouteMask,
                                 INT4 i4FsRtmCommonRouteTos,
                                 UINT4 u4FsRtmCommonRouteNextHop,
                                 INT4 i4SetValFsRtmCommonRouteNextHopAS)
#else
INT1
nmhSetFsRtmCommonRouteNextHopAS (u4FsRtmCommonRouteDest,
                                 u4FsRtmCommonRouteMask,
                                 i4FsRtmCommonRouteTos,
                                 u4FsRtmCommonRouteNextHop,
                                 i4SetValFsRtmCommonRouteNextHopAS)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4SetValFsRtmCommonRouteNextHopAS;
#endif
{
    return (nmhSetIpCidrRouteNextHopAS (u4FsRtmCommonRouteDest,
                                        u4FsRtmCommonRouteMask,
                                        i4FsRtmCommonRouteTos,
                                        u4FsRtmCommonRouteNextHop,
                                        i4SetValFsRtmCommonRouteNextHopAS));
}

/****************************************************************************
 Function    :  nmhSetFsRtmCommonRouteMetric1
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                setValFsRtmCommonRouteMetric1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsRtmCommonRouteMetric1 (UINT4 u4FsRtmCommonRouteDest,
                               UINT4 u4FsRtmCommonRouteMask,
                               INT4 i4FsRtmCommonRouteTos,
                               UINT4 u4FsRtmCommonRouteNextHop,
                               INT4 i4SetValFsRtmCommonRouteMetric1)
#else
INT1
nmhSetFsRtmCommonRouteMetric1 (u4FsRtmCommonRouteDest,
                               u4FsRtmCommonRouteMask,
                               i4FsRtmCommonRouteTos,
                               u4FsRtmCommonRouteNextHop,
                               i4SetValFsRtmCommonRouteMetric1)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4SetValFsRtmCommonRouteMetric1;
#endif
{
    return (nmhSetIpCidrRouteMetric1 (u4FsRtmCommonRouteDest,
                                      u4FsRtmCommonRouteMask,
                                      i4FsRtmCommonRouteTos,
                                      u4FsRtmCommonRouteNextHop,
                                      i4SetValFsRtmCommonRouteMetric1));
}

/****************************************************************************
 Function    :  nmhSetFsRtmCommonRoutePrivateStatus
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                setValFsRtmCommonRoutePrivateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRtmCommonRoutePrivateStatus (UINT4 u4FsRtmCommonRouteDest,
                                     UINT4 u4FsRtmCommonRouteMask,
                                     INT4 i4FsRtmCommonRouteTos,
                                     UINT4 u4FsRtmCommonRouteNextHop,
                                     INT4 i4SetValFsRtmCommonRoutePrivateStatus)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (IpFwdTblSetObjectInCxt (pRtmCxt->u4ContextId,
                                u4FsRtmCommonRouteDest,
                                u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop,
                                INVALID_PROTO,
                                &i4SetValFsRtmCommonRoutePrivateStatus,
                                IPFWD_REDIS_CTRL_FLAG) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    UtilRtmIncMsrForRtPrivateStatus (pRtmCxt->u4ContextId,
                                     u4FsRtmCommonRouteDest,
                                     u4FsRtmCommonRouteMask,
                                     i4FsRtmCommonRouteTos,
                                     u4FsRtmCommonRouteNextHop,
                                     i4SetValFsRtmCommonRoutePrivateStatus,
                                     FsMIRtmCommonRoutePrivateStatus,
                                     (sizeof (FsMIRtmCommonRoutePrivateStatus) /
                                      sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRtmCommonRoutePreference
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object
                setValFsRtmCommonRoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsRtmCommonRoutePreference(UINT4 u4FsRtmCommonRouteDest, 
                                 UINT4 u4FsRtmCommonRouteMask, 
                                 INT4 i4FsRtmCommonRouteTos, 
                                 UINT4 u4FsRtmCommonRouteNextHop, 
                                 INT4 i4SetValFsRtmCommonRoutePreference)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (IpFwdTblSetObjectInCxt (pRtmCxt->u4ContextId,
                                u4FsRtmCommonRouteDest,
                                u4FsRtmCommonRouteMask,
                                i4FsRtmCommonRouteTos,
                                u4FsRtmCommonRouteNextHop,
                                INVALID_PROTO,
                                &i4SetValFsRtmCommonRoutePreference,
                                IPFWD_ROUTE_PREFERENCE) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    UtilRtmIncMsrForRtPrivateStatus (pRtmCxt->u4ContextId,
                                     u4FsRtmCommonRouteDest,
                                     u4FsRtmCommonRouteMask,
                                     i4FsRtmCommonRouteTos,
                                     u4FsRtmCommonRouteNextHop,
                                     i4SetValFsRtmCommonRoutePreference,
                                     FsMIRtmCommonRoutePreference,
                                     (sizeof (FsMIRtmCommonRoutePreference) /
                                      sizeof (UINT4)));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRtmCommonRouteStatus
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                setValFsRtmCommonRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhSetFsRtmCommonRouteStatus (UINT4 u4FsRtmCommonRouteDest,
                              UINT4 u4FsRtmCommonRouteMask,
                              INT4 i4FsRtmCommonRouteTos,
                              UINT4 u4FsRtmCommonRouteNextHop,
                              INT4 i4SetValFsRtmCommonRouteStatus)
#else
INT1
nmhSetFsRtmCommonRouteStatus (u4FsRtmCommonRouteDest,
                              u4FsRtmCommonRouteMask,
                              i4FsRtmCommonRouteTos,
                              u4FsRtmCommonRouteNextHop,
                              i4SetValFsRtmCommonRouteStatus)
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4SetValFsRtmCommonRouteStatus;
#endif
{
    return (nmhSetIpCidrRouteStatus (u4FsRtmCommonRouteDest,
                                     u4FsRtmCommonRouteMask,
                                     i4FsRtmCommonRouteTos,
                                     u4FsRtmCommonRouteNextHop,
                                     i4SetValFsRtmCommonRouteStatus));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRtmCommonRouteIfIndex
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                testValFsRtmCommonRouteIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsRtmCommonRouteIfIndex (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsRtmCommonRouteDest,
                                  UINT4 u4FsRtmCommonRouteMask,
                                  INT4 i4FsRtmCommonRouteTos,
                                  UINT4 u4FsRtmCommonRouteNextHop,
                                  INT4 i4TestValFsRtmCommonRouteIfIndex)
#else
INT1
nmhTestv2FsRtmCommonRouteIfIndex (*pu4ErrorCode,
                                  u4FsRtmCommonRouteDest,
                                  u4FsRtmCommonRouteMask,
                                  i4FsRtmCommonRouteTos,
                                  u4FsRtmCommonRouteNextHop,
                                  i4TestValFsRtmCommonRouteIfIndex)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4TestValFsRtmCommonRouteIfIndex;
#endif
{
    return (nmhTestv2IpCidrRouteIfIndex (pu4ErrorCode,
                                         u4FsRtmCommonRouteDest,
                                         u4FsRtmCommonRouteMask,
                                         i4FsRtmCommonRouteTos,
                                         u4FsRtmCommonRouteNextHop,
                                         i4TestValFsRtmCommonRouteIfIndex));
}

/****************************************************************************
 Function    :  nmhTestv2FsRtmCommonRouteType
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                testValFsRtmCommonRouteType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsRtmCommonRouteType (UINT4 *pu4ErrorCode,
                               UINT4 u4FsRtmCommonRouteDest,
                               UINT4 u4FsRtmCommonRouteMask,
                               INT4 i4FsRtmCommonRouteTos,
                               UINT4 u4FsRtmCommonRouteNextHop,
                               INT4 i4TestValFsRtmCommonRouteType)
#else
INT1
nmhTestv2FsRtmCommonRouteType (*pu4ErrorCode,
                               u4FsRtmCommonRouteDest,
                               u4FsRtmCommonRouteMask,
                               i4FsRtmCommonRouteTos,
                               u4FsRtmCommonRouteNextHop,
                               i4TestValFsRtmCommonRouteType)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4TestValFsRtmCommonRouteType;
#endif
{
    return (nmhTestv2IpCidrRouteType (pu4ErrorCode,
                                      u4FsRtmCommonRouteDest,
                                      u4FsRtmCommonRouteMask,
                                      i4FsRtmCommonRouteTos,
                                      u4FsRtmCommonRouteNextHop,
                                      i4TestValFsRtmCommonRouteType));
}

/****************************************************************************
 Function    :  nmhTestv2FsRtmCommonRouteInfo
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                testValFsRtmCommonRouteInfo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsRtmCommonRouteInfo (UINT4 *pu4ErrorCode,
                               UINT4 u4FsRtmCommonRouteDest,
                               UINT4 u4FsRtmCommonRouteMask,
                               INT4 i4FsRtmCommonRouteTos,
                               UINT4 u4FsRtmCommonRouteNextHop,
                               tSNMP_OID_TYPE * pTestValFsRtmCommonRouteInfo)
#else
INT1
nmhTestv2FsRtmCommonRouteInfo (UINT4 *pu4ErrorCode,
                               UINT4 u4FsRtmCommonRouteDest,
                               UINT4 u4FsRtmCommonRouteMask,
                               INT4 i4FsRtmCommonRouteTos,
                               UINT4 u4FsRtmCommonRouteNextHop,
                               tSNMP_OID_TYPE * pTestValFsRtmCommonRouteInfo)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     tSNMP_OID_TYPE     *pTestValFsRtmCommonRouteInfo;
#endif
{
    return (nmhTestv2IpCidrRouteInfo (pu4ErrorCode,
                                      u4FsRtmCommonRouteDest,
                                      u4FsRtmCommonRouteMask,
                                      i4FsRtmCommonRouteTos,
                                      u4FsRtmCommonRouteNextHop,
                                      pTestValFsRtmCommonRouteInfo));
}

/****************************************************************************
 Function    :  nmhTestv2FsRtmCommonRouteNextHopAS
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                testValFsRtmCommonRouteNextHopAS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsRtmCommonRouteNextHopAS (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsRtmCommonRouteDest,
                                    UINT4 u4FsRtmCommonRouteMask,
                                    INT4 i4FsRtmCommonRouteTos,
                                    UINT4 u4FsRtmCommonRouteNextHop,
                                    INT4 i4TestValFsRtmCommonRouteNextHopAS)
#else
INT1
nmhTestv2FsRtmCommonRouteNextHopAS (*pu4ErrorCode,
                                    u4FsRtmCommonRouteDest,
                                    u4FsRtmCommonRouteMask,
                                    i4FsRtmCommonRouteTos,
                                    u4FsRtmCommonRouteNextHop,
                                    i4TestValFsRtmCommonRouteNextHopAS)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4TestValFsRtmCommonRouteNextHopAS;
#endif
{
    return (nmhTestv2IpCidrRouteNextHopAS (pu4ErrorCode,
                                           u4FsRtmCommonRouteDest,
                                           u4FsRtmCommonRouteMask,
                                           i4FsRtmCommonRouteTos,
                                           u4FsRtmCommonRouteNextHop,
                                           i4TestValFsRtmCommonRouteNextHopAS));
}

/****************************************************************************
 Function    :  nmhTestv2FsRtmCommonRouteMetric1
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                testValFsRtmCommonRouteMetric1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsRtmCommonRouteMetric1 (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsRtmCommonRouteDest,
                                  UINT4 u4FsRtmCommonRouteMask,
                                  INT4 i4FsRtmCommonRouteTos,
                                  UINT4 u4FsRtmCommonRouteNextHop,
                                  INT4 i4TestValFsRtmCommonRouteMetric1)
#else
INT1
nmhTestv2FsRtmCommonRouteMetric1 (*pu4ErrorCode,
                                  u4FsRtmCommonRouteDest,
                                  u4FsRtmCommonRouteMask,
                                  i4FsRtmCommonRouteTos,
                                  u4FsRtmCommonRouteNextHop,
                                  i4TestValFsRtmCommonRouteMetric1)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4TestValFsRtmCommonRouteMetric1;
#endif
{
    return (nmhTestv2IpCidrRouteMetric1 (pu4ErrorCode,
                                         u4FsRtmCommonRouteDest,
                                         u4FsRtmCommonRouteMask,
                                         i4FsRtmCommonRouteTos,
                                         u4FsRtmCommonRouteNextHop,
                                         i4TestValFsRtmCommonRouteMetric1));
}

/****************************************************************************
 Function    :  nmhTestv2FsRtmCommonRoutePrivateStatus
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                testValFsRtmCommonRoutePrivateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRtmCommonRoutePrivateStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsRtmCommonRouteDest,
                                        UINT4 u4FsRtmCommonRouteMask,
                                        INT4 i4FsRtmCommonRouteTos,
                                        UINT4 u4FsRtmCommonRouteNextHop,
                                        INT4
                                        i4TestValFsRtmCommonRoutePrivateStatus)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRtmCommonRoutePrivateStatus < RTM_SNMP_TRUE) ||
        (i4TestValFsRtmCommonRoutePrivateStatus > RTM_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (IpFwdTblTestObjectInCxt (pRtmCxt->u4ContextId,
                                 u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                 i4FsRtmCommonRouteTos,
                                 u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                 &i4TestValFsRtmCommonRoutePrivateStatus,
                                 IPFWD_REDIS_CTRL_FLAG,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRtmCommonRoutePreference
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object
                testValFsRtmCommonRoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsRtmCommonRoutePreference(UINT4 *pu4ErrorCode, 
                                    UINT4 u4FsRtmCommonRouteDest, 
                                    UINT4 u4FsRtmCommonRouteMask, 
                                    INT4 i4FsRtmCommonRouteTos, 
                                    UINT4 u4FsRtmCommonRouteNextHop, 
                                    INT4 i4TestValFsRtmCommonRoutePreference)
{
    tRtmCxt            *pRtmCxt = gRtmGlobalInfo.pRtmCxt;

    if (pRtmCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsRtmCommonRoutePreference > RTM_IP_ROUTE_MAX_DISTANCE) ||
        (i4TestValFsRtmCommonRoutePreference < RTM_IP_ROUTE_MIN_DISTANCE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (IpFwdTblTestObjectInCxt (pRtmCxt->u4ContextId,
                                 u4FsRtmCommonRouteDest, u4FsRtmCommonRouteMask,
                                 i4FsRtmCommonRouteTos,
                                 u4FsRtmCommonRouteNextHop, INVALID_PROTO,
                                 &i4TestValFsRtmCommonRoutePreference,
                                 IPFWD_ROUTE_PREFERENCE,
                                 pu4ErrorCode) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRtmCommonRouteStatus
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop

                The Object 
                testValFsRtmCommonRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
#ifdef __STDC__
INT1
nmhTestv2FsRtmCommonRouteStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsRtmCommonRouteDest,
                                 UINT4 u4FsRtmCommonRouteMask,
                                 INT4 i4FsRtmCommonRouteTos,
                                 UINT4 u4FsRtmCommonRouteNextHop,
                                 INT4 i4TestValFsRtmCommonRouteStatus)
#else
INT1
nmhTestv2FsRtmCommonRouteStatus (*pu4ErrorCode,
                                 u4FsRtmCommonRouteDest,
                                 u4FsRtmCommonRouteMask,
                                 i4FsRtmCommonRouteTos,
                                 u4FsRtmCommonRouteNextHop,
                                 i4TestValFsRtmCommonRouteStatus)
     UINT4              *pu4ErrorCode;
     UINT4               u4FsRtmCommonRouteDest;
     UINT4               u4FsRtmCommonRouteMask;
     INT4                i4FsRtmCommonRouteTos;
     UINT4               u4FsRtmCommonRouteNextHop;
     INT4                i4TestValFsRtmCommonRouteStatus;
#endif
{
    return (nmhTestv2IpCidrRouteStatus (pu4ErrorCode,
                                        u4FsRtmCommonRouteDest,
                                        u4FsRtmCommonRouteMask,
                                        i4FsRtmCommonRouteTos,
                                        u4FsRtmCommonRouteNextHop,
                                        i4TestValFsRtmCommonRouteStatus));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRtmCommonRouteTable
 Input       :  The Indices
                FsRtmCommonRouteDest
                FsRtmCommonRouteMask
                FsRtmCommonRouteTos
                FsRtmCommonRouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRtmCommonRouteTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
