
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmfrt.c,v 1.1 2015/04/24 12:04:26 siva Exp $
 *
 * Description:This file contains functions needed for failed route 
               implementation.Routes that are failed are stored in  
               Failed Route Table(FRT).Routines that access this 
               Failed route are maintained in this File.               
 *******************************************************************/
#ifndef LNXIP4_WANTED
#include "ipinc.h"
#include "iptrace.h"
#endif
#include "rtminc.h"
#include "rtmfrt.h"
/************************************************************************/
/*  Function Name   : RtmFrtInitGlobalInfo                              */
/*                                                                      */
/*  Description     : This function is invoked by the RTM module while  */
/*                    task initialisation. It initialises the failed    */
/*                    route global variables.                           */
/*                                                                      */
/*  Input(s)        : None.                                             */
/*                                                                      */
/*  Output(s)       : None.                                             */
/*                                                                      */
/*  Returns         : OSIX_SUCCESS / OSIX_FAILURE                       */
/************************************************************************/
PUBLIC INT4
RtmFrtInitGlobalInfo (VOID)
{

    gRtmGlobalInfo.RtmFrtTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tRtmFrtInfo, RbNode), RtmRBTreeFrtEntryCmp);

    if (gRtmGlobalInfo.RtmFrtTable == NULL)
    {
        return RTM_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : RtmRBTreeFrtEntryCmp
 * 
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *     
 * Returns            : -1  if pRBElem <  pRBElemIn
 *                       1   if pRBElem >  pRBElemIn
 *                       0   if pRBElem == pRBElemIn
 * Action             : This procedure compares the two FRT entries 
 *                      in lexicographic order of the indices of the 
 *                      FRT entry.
 *-------------------------------------------------------------------*/
INT4
RtmRBTreeFrtEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tRtmFrtInfo       *pRtmFrt = pRBElem;
    tRtmFrtInfo       *pRtmFrtIn = pRBElemIn;

    if (pRtmFrt->u4CxtId < pRtmFrtIn->u4CxtId)
    {
        return -1;
    }
    else if (pRtmFrt->u4CxtId > pRtmFrtIn->u4CxtId)
    {
        return 1;
    }
    if (pRtmFrt->u4DestNet < pRtmFrtIn->u4DestNet)
    {
        return -1;
    }
    else if (pRtmFrt->u4DestNet > pRtmFrtIn->u4DestNet)
    {
        return 1;
    }

    if (pRtmFrt->u4DestMask < pRtmFrtIn->u4DestMask)
    {
        return -1;
    }
    else if (pRtmFrt->u4DestMask > pRtmFrtIn->u4DestMask)
    {
        return 1;
    }

    if (pRtmFrt->u4NextHop < pRtmFrtIn->u4NextHop)
    {
        return -1;
    }
    else if (pRtmFrt->u4NextHop > pRtmFrtIn->u4NextHop)
    {
        return 1;
    }

    if (pRtmFrt->u2RtProto < pRtmFrtIn->u2RtProto)
    {
        return -1;
    }
    else if (pRtmFrt->u2RtProto > pRtmFrtIn->u2RtProto)
    {
        return 1;
    }

    return 0;
}

/************************************************************************/
/* Function Name      : RtmFrtAddInfo                                   */
/*                                                                      */
/* Description        : This function adds the NPAPI Failed route       */
/*                      entries to the global tree.                     */
/*                                                                      */
/* Input(s)           : pRtInfo - Rtm Route  message                    */
/*                      u4CxtId - Context Id of the entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
RtmFrtAddInfo (tRtInfo * pRtInfo, UINT4 u4CxtId, UINT1 u1RtCount)
{
    tRtmFrtInfo       *pRtmFrtNode = NULL;
    tRtmFrtInfo       *pTmpRtmFrtNode = NULL;
    tRtmFrtInfo        RtmFrtNode;
    if (pRtInfo == NULL)
    {
        return OSIX_FAILURE;
    }
   /* Check whether the entry is already present in the RBtree, if present
    * just update the fields. If not present, allocate memory and add the
    * entry to the RBtree */
    RtmFrtNode.u4CxtId = u4CxtId;
    RtmFrtNode.u4DestNet = pRtInfo->u4DestNet;
    RtmFrtNode.u4DestMask = pRtInfo->u4DestMask;
    RtmFrtNode.u4NextHop = pRtInfo->u4NextHop;
    RtmFrtNode.u1BitMask = pRtInfo->u1BitMask;
    RtmFrtNode.i4MetricType = pRtInfo->i4MetricType;
    RtmFrtNode.u2RtProto = pRtInfo->u2RtProto;

    pTmpRtmFrtNode = RBTreeGet (gRtmGlobalInfo.RtmFrtTable,
                                (tRBElem *) & RtmFrtNode);
    if (pTmpRtmFrtNode == NULL)
    {
        pRtmFrtNode =
            (tRtmFrtInfo *) MemAllocMemBlk ((tMemPoolId) RTM_FRT_MSG_POOL_ID);
        if (pRtmFrtNode == NULL)
        {
            UtlTrcLog (1, 1, "",
                           "ERROR[RTM]: Failed Route Entry Message Allocation Failure\n");
            return OSIX_FAILURE;
        }
        MEMSET (pRtmFrtNode, 0, sizeof (tRtmFrtInfo));
        pRtmFrtNode->u4CxtId = u4CxtId;
        pRtmFrtNode->u4DestNet = pRtInfo->u4DestNet;
        pRtmFrtNode->u4DestMask = pRtInfo->u4DestMask;
        pRtmFrtNode->u4NextHop = pRtInfo->u4NextHop;
        RtmFrtNode.u1BitMask = pRtInfo->u1BitMask;
        RtmFrtNode.i4MetricType = pRtInfo->i4MetricType;
        pRtmFrtNode->u2RtProto = pRtInfo->u2RtProto;
        pRtmFrtNode->u4Flag = pRtInfo->u4Flag;
        pRtmFrtNode->u4RtIfIndx = pRtInfo->u4RtIfIndx;
        pRtmFrtNode->u1RtCount = u1RtCount;
        if ((RBTreeAdd (gRtmGlobalInfo.RtmFrtTable, pRtmFrtNode)) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    } 
    else
    {
        pRtmFrtNode = pTmpRtmFrtNode;
    }
    pRtmFrtNode->u4Flag = pRtInfo->u4Flag;
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmFrtGetInfo                                   */
/*                                                                      */
/* Description        : This function gets the NPAPI Failed route       */
/*                      entries to the global tree.                     */
/*                                                                      */
/* Input(s)           : pRtInfo - Rtm Route  message                    */
/*                      u4CxtId - Context Id of the entry               */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

tRtmFrtInfo *
RtmFrtGetInfo (tRtInfo * pRtInfo, UINT4 u4CxtId)
{
    tRtmFrtInfo        RtmFrtNode;
    tRtmFrtInfo       *pRtmFrtNode = NULL;
    /* Check whether the entry is already present in the RBtree, if present
     * just update the fields.*/
    RtmFrtNode.u4CxtId = u4CxtId;
    RtmFrtNode.u4DestNet = pRtInfo->u4DestNet;
    RtmFrtNode.u4DestMask = pRtInfo->u4DestMask;
    RtmFrtNode.u4NextHop = pRtInfo->u4NextHop;
    RtmFrtNode.u2RtProto = pRtInfo->u2RtProto;
    pRtmFrtNode = RBTreeGet (gRtmGlobalInfo.RtmFrtTable,(tRBElem *) & RtmFrtNode); 
    if(pRtmFrtNode == NULL)
    {
        return NULL;
    }
    return pRtmFrtNode;
}

/************************************************************************/
/* Function Name      : RtmFrtDeleteEntry                               */
/*                                                                      */
/* Description        : This function deletes the NPAPI Failed route    */
/*                      entries to the global tree.                     */
/*                                                                      */
/* Input(s)           : pRtmFrtNode - Rtm failed route  node            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
INT4
RtmFrtDeleteEntry (tRtmFrtInfo *pRtmFrtNode)
{
    /* Delete the route entry already present in the RBtree*/
    if (RBTreeRem (gRtmGlobalInfo.RtmFrtTable, (tRBElem *) pRtmFrtNode ) == NULL)
    {
         return RTM_FAILURE;
    }

    if (MEM_FAILURE == MemReleaseMemBlock (RTM_FRT_MSG_POOL_ID,
                                               (VOID *) pRtmFrtNode))
    {
            return RTM_FAILURE;
    }
    return RTM_SUCCESS;
}

/************************************************************************/
/* Function Name      : RtmGetFirstFrtEntry                             */
/*                                                                      */
/* Description        : This function get the first NPAPI Failed route  */
/*                      entries from the FRT table                      */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

tRtmFrtInfo *
RtmGetFirstFrtEntry()
{
   tRtmFrtInfo *pRtmFrtNode;
   pRtmFrtNode = RBTreeGetFirst (gRtmGlobalInfo.RtmFrtTable);
   if(pRtmFrtNode == NULL)
   {
      return NULL;
   }
   return pRtmFrtNode;
}


/************************************************************************/
/* Function Name      : RtmGetNextFrtEntry                              */
/*                                                                      */
/* Description        : This function get the next NPAPI Failed route  */
/*                      entries from the FRT table                      */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

tRtmFrtInfo *
RtmGetNextFrtEntry(tRtmFrtInfo *pRtmFrtNode)
{
    tRtmFrtInfo *pRtmNextFrtNode;
    pRtmNextFrtNode = RBTreeGetNext (gRtmGlobalInfo.RtmFrtTable,
                                     (tRBElem *)pRtmFrtNode, NULL);
    if(pRtmNextFrtNode == NULL)
    {
        return NULL;
    }
    return pRtmNextFrtNode;
}

/************************************************************************/
/* Function Name      : RtmGetFrtTableCount                             */
/*                                                                      */
/* Description        : This function gets the total number of entries  */
/*                      in FRT table                                    */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/
VOID
RtmGetFrtTableCount(UINT4 *pFrtCount)
{
   UINT4 u4Count=0;
   tRtmFrtInfo *pRtmFrtNode;
   pRtmFrtNode = RtmGetFirstFrtEntry();
   while(pRtmFrtNode != NULL)
   {
       u4Count++;
       pRtmFrtNode = RtmGetNextFrtEntry(pRtmFrtNode); 
   }   
   *pFrtCount = u4Count;
}

/************************************************************************/
/* Function Name      : RtmFrtDeInitGlobalInfo                          */
/*                                                                      */
/* Description        : This function is invoked by the RTM module      */
/*                      during module shutdown and this function        */
/*                      deinitializes the FRT tree                      */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/
INT4
RtmFrtDeInitGlobalInfo (VOID)
{
    if (gRtmGlobalInfo.RtmFrtTable != NULL)
    {
        RBTreeDelete (gRtmGlobalInfo.RtmFrtTable);
        gRtmGlobalInfo.RtmFrtTable = NULL;
    }
    return OSIX_SUCCESS;
}

