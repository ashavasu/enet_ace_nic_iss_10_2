/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rtmrmap.c,v 1.14 2016/10/07 13:19:28 siva Exp $
 *
 * Description:This file contains functions which process   
 *             the messages from the routing protocols.     
 *
 *******************************************************************/

#include "rtminc.h"

/**************************************************************************/
/*   Function Name   : RtmCheckIsRouteMapConfiguredInCxt                  */
/*   Description     : This function checks whether RouteMap is configured*/
/*                     for the given destionation RP                      */
/*   Input(s)        : u2Proto - Destination RP                           */
/*                   : pRtmCxt - RTM context Pointer                      */
/*   Output(s)       : None                                               */
/*   Return Value    : RTM_SUCCESS if Route Map configured                */
/*                     RTM_FAILURE otherwise                              */
/**************************************************************************/
INT4
RtmCheckIsRouteMapConfiguredInCxt (tRtmCxt * pRtmCxt, UINT2 u2Proto)
{
    UINT2               u2RegnId = 0;

    for (u2RegnId = pRtmCxt->u2RtmRtStartIndex;
         u2RegnId < MAX_ROUTING_PROTOCOLS;
         u2RegnId = pRtmCxt->aRtmRegnTable[u2RegnId].u2NextRegId)
    {
        if ((pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId == u2Proto) &&
            (STRLEN (pRtmCxt->aRtmRegnTable[u2RegnId].au1RMapName) != 0))
        {
            /* Route Map configured */
            return RTM_SUCCESS;
        }
    }
    /* Route Map not configured */
    return RTM_FAILURE;
}

/**************************************************************************/
/*   Function Name   : RtmResetRouteMapBitMaskInCxt                       */
/*   Description     : This function checks whether RouteMap is configured*/
/*                     for the given destionation RP, if its configured   */
/*                     resets the Bit Mask, so that filter rules cannot be*/
/*                     applied on the Destination RP where RouteMap is    */
/*                     enabled                                            */
/*   Input(s)        : pu2DestProtoMask - Destination ProtoMask for the   */
/*                     RPs for Filter rule is configured                  */
/*                     pRtmCxt - RTM Context pointer                      */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
RtmResetRouteMapBitMaskInCxt (tRtmCxt * pRtmCxt, UINT2 *pu2DestProtoMask)
{
    UINT2               u2Cnt = 0;
    UINT2               u2BitMask = RTM_PRIV_RT_MASK;

    for (u2Cnt = 0; u2Cnt < MAX_ROUTING_PROTOCOLS; u2Cnt++)
    {
        if ((u2BitMask & *pu2DestProtoMask) &&
            (STRLEN (pRtmCxt->aRtmRegnTable[u2Cnt].au1RMapName) != 0))
        {
            RTM_CLEAR_BIT (*pu2DestProtoMask, (u2Cnt + 1));
        }
        u2BitMask = (UINT2) (u2BitMask << 1);
    }
}

/**************************************************************************/
/*   Function Name   : RtmSetRouteMapBitMaskInCxt                         */
/*   Description     : This function checks whether RouteMap is configured*/
/*                     for the given destionation RP, if its configured   */
/*                     sets the Bit Mask                                  */
/*   Input(s)        : pu2DestProtoMask - Destination ProtoMask for the   */
/*                     RPs for Filter rule is configured                  */
/*                     pRtmCxt - RTM Context pointer                      */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
RtmSetRouteMapBitMaskInCxt (tRtmCxt * pRtmCxt, UINT2 *pu2DestProtoMask)
{
    UINT2               u2Cnt = 0;
    UINT2               u2BitMask = RTM_PRIV_RT_MASK;
    *pu2DestProtoMask = 0;

    for (u2Cnt = 0; u2Cnt < MAX_ROUTING_PROTOCOLS; u2Cnt++)
    {
        if (STRLEN (pRtmCxt->aRtmRegnTable[u2Cnt].au1RMapName) != 0)
        {
            RTM_SET_BIT (*pu2DestProtoMask, (u2Cnt + 1));
        }
        u2BitMask = (UINT2) (u2BitMask << 1);
    }
}

/**************************************************************************/
/*   Function Name   : RtmApplyRouteMapRuleAndRedistributeInCxt           */
/*   Description     : This function checks whether RouteMap is configured */
/*                     for the given destionation RP, If configured calls */
/*                     the routeMap module to apply the rule              */
/*   Input(s)        : pRegnId  - Destination Routing Protocols           */
/*                     registration ID                                    */
/*                     pRtInfo - Route in which Route Map Rule should be  */
/*                     applied and redistributed                          */
/*                     u1IsRtBest - Info whether the route is best or not */
/*                     u2ChgBit   - Info about the Route attributes whose */
/*                                   value have been changed.             */
/*                     u2DestProtoMask - Protocols to which the route is  */
/*                                      either redistributed or withdrawn */
/*                     pRtmCxt - RTM Context pointer                      */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
VOID
RtmApplyRouteMapRuleAndRedistributeInCxt (tRtmCxt * pRtmCxt, UINT2 u2SrcProtoId,
                                          tRtInfo * pRtInfo, UINT1 u1IsRtBest,
                                          UINT2 u2ChgBit, UINT2 u2DstProtoMask)
{
#ifndef ROUTEMAP_WANTED
    UNUSED_PARAM (pRtmCxt);
    UNUSED_PARAM (u2SrcProtoId);
    UNUSED_PARAM (pRtInfo);
    UNUSED_PARAM (u1IsRtBest);
    UNUSED_PARAM (u2ChgBit);
    UNUSED_PARAM (u2DstProtoMask);
#else
    tRtInfo             OutRtInfo;
    UINT2               u2DestMask = 0;
    UINT2               u2RegnId = 0;
    UINT1               u1Status = 0;
    tRtMapInfo          InMapInfo, OutMapInfo;

    MEMSET (&OutRtInfo, 0, sizeof(tRtInfo));
    /* This route is to be redistributed only to these mib enabled
     *  * routing protocols.  */
    for (u2RegnId = pRtmCxt->u2RtmRtStartIndex;
         u2RegnId < MAX_ROUTING_PROTOCOLS;
         u2RegnId = pRtmCxt->aRtmRegnTable[u2RegnId].u2NextRegId)
    {
        if (STRLEN (pRtmCxt->aRtmRegnTable[u2RegnId].au1RMapName) != 0)
        {

            RTM_SET_BIT (u2DestMask,
                         pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId);

            if ((u2DstProtoMask & u2DestMask) != u2DestMask)
            {
                RTM_CLEAR_BIT (u2DestMask,
                               pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId);
                continue;
            }

            /* convert structure */
            RMapTRtInfo2TRtMapInfo (&InMapInfo, pRtInfo, 0);

            /* Release RTM_PROT_LOCK to avoid deadlock 
             * during RMap trap generation */
            ROUTE_TBL_UNLOCK ();
            RTM_PROT_UNLOCK ();

            /* modify structure by applying MATCH/SET, get result PERMIT/DENY */
            u1Status = RMapApplyRule (&InMapInfo, &OutMapInfo,
                                      pRtmCxt->aRtmRegnTable[u2RegnId].
                                      au1RMapName);

            /* Take RTM_PROT_LOCK again */
            RTM_PROT_LOCK ();
            ROUTE_TBL_LOCK ();

            /* these two structure should be the same */
            MEMCPY (&OutRtInfo, pRtInfo, sizeof (tRtInfo));

            /*allocate memnory for the community in rtinfo*
             *based on the community count of rmap info*/
            if (MALLOC_RTM_COMM_ENTRY(OutRtInfo.pCommunity) == NULL)
            {
                /*memory allocation failed*/
                return;
            }
            MEMSET (OutRtInfo.pCommunity, 0,sizeof(tRtRmapComm));

            /* modify some route attributes, preserve others */
            RMapTRtMapInfo2TRtInfo (&OutRtInfo, &OutMapInfo);

            if (u1Status == RMAP_ROUTE_PERMIT)
            {
                RTM_CLEAR_BIT (u2DestMask, u2SrcProtoId);
            }
            else
            {
                u2DestMask = RTM_DONT_REDISTRIBUTE;
                RTM_CLEAR_BIT (u2DestMask, u2SrcProtoId);
            }

            if (u2DestMask != RTM_DONT_REDISTRIBUTE)
            {
                /* Route can be redistributed to the registering protocol. */
                RtmRouteRedistributionInCxt (pRtmCxt, &OutRtInfo,
                                             u1IsRtBest, u2ChgBit, u2DestMask);
                pRtInfo->u2RedisMask = OutRtInfo.u2RedisMask;

            }

        }
    }
    /*release memory for the community in rtinfo*
     *based on the community count of rmap info*/
    if (OutRtInfo.pCommunity != NULL)
    {
        IP_RTM_COMM_FREE(OutRtInfo.pCommunity);
    }
#endif /*ROUTEMAP_WANTED */
}

/**************************************************************************/
/*   Function Name   : RtmProcessRouteMapChanges                          */
/*   Description     : This function handles Route Map update msg from    */
/*                     Route Map module, with the given route map name,   */
/*                     it gets the Rotuing protocol which are registered  */
/*                     for this Route Map, and applies this Route Map rule*/
/*                     on all the routes in RTM and decides wether this   */
/*                     should redistributed or withdrqwn                  */
/*   Input(s)        : pu1RMapName - Route Map Name                       */
/*   Output(s)       : None                                               */
/*   Return Value    : None                                               */
/**************************************************************************/
INT4
RtmProcessRouteMapChanges (UINT1 *pu1RMapName)
{
#ifndef ROUTEMAP_WANTED
    UNUSED_PARAM (pu1RMapName);
    return RTM_SUCCESS;
#else
    UINT2               u2RegnId = 0;
    tInputParams        InParams;
    tOutputParams       OutParams;
    tRtmRegnId          RtmRegId;
    UINT4               au4Indx[IP_TWO];
    UINT4               au4OutIndx[IP_TWO];
    tRtmCxt            *pRtmCxt = NULL;
    UINT4               u4ContextId;
    UINT4               u4NextContextId;

    if (UtilRtmGetFirstCxtId (&u4ContextId) != RTM_FAILURE)
    {
        pRtmCxt = UtilRtmGetCxt (u4ContextId);
    }

    while (pRtmCxt != NULL)
    {
        /* clear input ip-adr, net-mask */
        au4Indx[0] = 0;
        au4Indx[1] = 0;

        /* clear output ip-adr, net-mask for any case */
        au4OutIndx[0] = 0;
        au4OutIndx[1] = 0;

        InParams.pRoot = pRtmCxt->pIpRtTblRoot;
        InParams.i1AppId = ALL_ROUTING_PROTOCOL;
        InParams.Key.pKey = (UINT1 *) &(au4Indx[0]);

        OutParams.pAppSpecInfo = NULL;
        OutParams.Key.pKey = (UINT1 *) &(au4OutIndx[0]);

        /* scan route table */
        for (u2RegnId = pRtmCxt->u2RtmRtStartIndex;
             u2RegnId < MAX_ROUTING_PROTOCOLS;
             u2RegnId = pRtmCxt->aRtmRegnTable[u2RegnId].u2NextRegId)
        {
            /* if redistribution with rmap is configured - apply rmap */
            if ((STRLEN (pRtmCxt->aRtmRegnTable[u2RegnId].au1RMapName) != 0) &&
                (STRCMP (pu1RMapName,
                         pRtmCxt->aRtmRegnTable[u2RegnId].au1RMapName) == 0))
            {
                ROUTE_TBL_LOCK ();

                /* This function will take care of collecting the route 
                 * to be redistributed and sending the update message to RP's */
                MEMSET (&RtmRegId, 0, sizeof (tRtmRegnId));
                RtmRegId.u2ProtoId =
                    pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId;
                RtmRegId.u4ContextId = pRtmCxt->u4ContextId;
                IpScanRouteTableForBestRouteInCxt (pRtmCxt, &InParams,
                                                   RtmHandleRouteMapChgsInRts,
                                                   0, &OutParams,
                                                   (VOID *) &RtmRegId);

                ROUTE_TBL_UNLOCK ();

            }
        }

        pRtmCxt = NULL;
        if (UtilRtmGetNextCxtId (u4ContextId, &u4NextContextId) != RTM_FAILURE)
        {
            pRtmCxt = UtilRtmGetCxt (u4NextContextId);
            u4ContextId = u4NextContextId;
        }

    }                            /* Scan for all the RTM context */
    return RTM_SUCCESS;
#endif /*ROUTEMAP_WANTED */
}

/**************************************************************************/
/*   Function Name   : RtmHandleRouteMapChgsInRts                         */
/*   Description     : This function handles Route Map update msg from    */
/*                     Route Map module, with the given route map name,   */
/*                     it gets the Rotuing protocol which are registered  */
/*                     for this Route Map, and applies this Route Map rule*/
/*                     on all the routes in RTM and decides wether this   */
/*                     should redistributed or withdrqwn                  */
/*                                                                        */
/*   Input(s)        : pRtInfo - Route in which changed route map rule    */
/*                               should be applied                        */
/*                     pAppSpecData - Destination Routing Protocol's Regn */
/*                     Id                                                 */
/*   Output(s)       : None                                               */
/*   Return Value    : RTM_SUCCESS/RTM_FAILURE                            */
/**************************************************************************/
INT4
RtmHandleRouteMapChgsInRts (tRtInfo * pRtInfo, VOID *pAppSpecData)
{
#ifndef ROUTEMAP_WANTED
    UNUSED_PARAM (pRtInfo);
    UNUSED_PARAM (pAppSpecData);
    return RTM_SUCCESS;
#else
    tRtmRegnId         *pRtmRegId = (tRtmRegnId *) pAppSpecData;
    tRtmCxt            *pRtmCxt = NULL;
    UINT2               u2RegnId;
    UINT2               u2DestProtoMask = 0;
    UINT2               u2SendProtoMask = 0;
    UINT1               u1Status;
    tRtMapInfo          InMapInfo, OutMapInfo;
    tRtInfo             OutRtInfo; /* Variable used
                                     * to hold the RMAP applied
                                     * changes not editing the 
                                     * actual protocol RtInfo.*/
    MEMSET (&OutRtInfo, 0, sizeof(tRtInfo));
    pRtmCxt = UtilRtmGetCxt (pRtmRegId->u4ContextId);

    if (pRtmCxt == NULL)
    {
        return RTM_FAILURE;
    }
    u2RegnId = RtmGetRegnIndexFromRegnIdInCxt (pRtmCxt, pRtmRegId);
    if ((u2RegnId == RTM_INVALID_REGN_ID) ||
        (u2RegnId >= MAX_ROUTING_PROTOCOLS))
    {
        return RTM_FAILURE;
    }

    RMapTRtInfo2TRtMapInfo (&InMapInfo, pRtInfo, 0);

    /* Release RTM_PROT_LOCK to avoid deadlock during RMap trap generation */
    ROUTE_TBL_UNLOCK ();
    RTM_PROT_UNLOCK ();

    u1Status = RMapApplyRule
        (&InMapInfo, &OutMapInfo, pRtmCxt->aRtmRegnTable[u2RegnId].au1RMapName);

    /* Take RTM_PROT_LOCK again */
    RTM_PROT_LOCK ();
    ROUTE_TBL_LOCK ();

    MEMCPY(&OutRtInfo,pRtInfo,sizeof(tRtInfo));
    /*allocate memnory for the community in rtinfo*
     *based on the community count of rmap info*/
    if (MALLOC_RTM_COMM_ENTRY(OutRtInfo.pCommunity) == NULL)
    {
        /*memory allocation failed*/
        return RTM_FAILURE;
    }
    MEMSET (OutRtInfo.pCommunity, 0, sizeof(tRtRmapComm));

    /* modify some route attributes, preserve others */
    RMapTRtMapInfo2TRtInfo (&OutRtInfo, &OutMapInfo);

    if (u1Status == RMAP_ROUTE_PERMIT)
    {
        RTM_SET_BIT (u2DestProtoMask,
                     pRtmCxt->aRtmRegnTable[u2RegnId].u2RoutingProtocolId);
    }
    else if (u1Status == RMAP_ROUTE_DENY)
    {
        u2DestProtoMask = RTM_DONT_REDISTRIBUTE;
    }

    if (u2DestProtoMask != RTM_DONT_REDISTRIBUTE)
    {
        /* Route can be redistributed to the protocols specified by
         * u2DestProtoMask. Check whether the route is already
         * redistributed to these protocols or not. If not redistributed
         * then send it. 
         */
        u2SendProtoMask = (UINT2) (u2DestProtoMask &
                                   (~(u2DestProtoMask & pRtInfo->u2RedisMask)));
        if (u2SendProtoMask != 0)
        {
            /* Redistribute the route to these protocols */
            RtmRouteRedistributionInCxt (pRtmCxt, &OutRtInfo, TRUE,
                                         IP_BIT_ALL, u2SendProtoMask);
        }
    }
    else
    {
        /* Route need not be redistributed as per current Route Map rule.
         * Check whether the route is already redistributed to any other
         * protocol. If yes, the withdraw them. */
        if (pRtInfo->u2RedisMask != 0)
        {
            RtmRouteRedistributionInCxt (pRtmCxt, &OutRtInfo, FALSE,
                                         IP_BIT_ALL, pRtInfo->u2RedisMask);
        }
    }
    /*release memory for the community in rtinfo*
         *based on the community count of rmap info*/
        if (OutRtInfo.pCommunity != NULL)
        {
          IP_RTM_COMM_FREE(OutRtInfo.pCommunity);
        }
    return RTM_SUCCESS;
#endif /*ROUTEMAP_WANTED */
}

/*****************************************************************************/
/* Function     : RtmSendRouteMapUpdateMsg                                   */
/*                                                                           */
/* Description  : This function posts an event to RTM from route map module  */
/*                It is called when route map is changed                     */
/*                                                                           */
/* Input        : pu1RMapName - RouteMap Name                                */
/*                u4Status - status of route map                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RTM_SUCCESS if Route Map update Msg Sent to RTM            */
/*                successfully                                               */
/*                RTM_FAILURE otherwise                                      */
/*****************************************************************************/
INT4
RtmSendRouteMapUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
#ifndef ROUTEMAP_WANTED
    UNUSED_PARAM (pu1RMapName);
    UNUSED_PARAM (u4Status);
    return RTM_SUCCESS;
#else
    tIpBuf             *pBuf;
    tRtmMsgHdr         *pHdr;
    tRtmMsgHdr          RtmHdr;
    INT4                i4OutCome;
    UINT4               u4Size;
    UINT1               au1NameBuf[RMAP_MAX_NAME_LEN + IP_FOUR];

    pHdr = &RtmHdr;
    u4Size = RMAP_MAX_NAME_LEN + sizeof (u4Status);    /* message size is hdr+ size of
                                                     * RouteMap Name+Status */

    /* enshure there is no garbage behind map name */
    MEMSET (au1NameBuf, 0, sizeof (au1NameBuf));
    STRNCPY (au1NameBuf, pu1RMapName, (RMAP_MAX_NAME_LEN + IP_FOUR));

    pBuf = IP_ALLOCATE_BUF (u4Size, 0);
    if (pBuf == NULL)
    {
        return RTM_FAILURE;
    }

   /*** copy message to be sent to buffer ***/
    pHdr = (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pBuf);

    pHdr->u1MessageType = RTM_ROUTEMAP_UPDATE_MSG;
    pHdr->u2MsgLen = (UINT2) u4Size;

    /*** copy status to offset 0 ***/
    if ((i4OutCome = IP_COPY_TO_BUF (pBuf, &u4Status, 0,
                                     sizeof (u4Status))) != CRU_SUCCESS)
    {

        IP_RELEASE_BUF (pBuf, FALSE);
        return RTM_FAILURE;
    }

    /*** copy map name to offset 4 ***/
    if ((i4OutCome = IP_COPY_TO_BUF (pBuf, au1NameBuf, sizeof (u4Status),
                                     RMAP_MAX_NAME_LEN)) != CRU_SUCCESS)
    {

        IP_RELEASE_BUF (pBuf, FALSE);
        return RTM_FAILURE;
    }

    /*** post to RTM queue ***/
    i4OutCome = RpsEnqueuePktToRtm (pBuf);

    return i4OutCome;
#endif /*ROUTEMAP_WANTED */
}
