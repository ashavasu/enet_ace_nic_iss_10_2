/* $Id: fsrtmwr.c,v 1.12 2016/02/08 10:40:10 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsrtmlw.h"
# include  "fsrtmwr.h"
# include  "fsrtmdb.h"
# include  "rtminc.h"

VOID
RegisterFSRTM ()
{
    SNMPRegisterMibWithContextIdAndLock (&fsrtmOID, &fsrtmEntry,
                                         RtmProtocolLock, RtmProtocolUnlock,
                                         UtilRtmSetContext,
                                         UtilRtmResetContext,
                                         SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsrtmOID, (const UINT1 *) "fsrtm");
}

VOID
UnRegisterFSRTM ()
{
    SNMPUnRegisterMib (&fsrtmOID, &fsrtmEntry);
    SNMPDelSysorEntry (&fsrtmOID, (const UINT1 *) "fsrtm");
}

INT4
FsRrdRouterIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrdRouterId (&(pMultiData->u4_ULongValue)));
}

INT4
FsRrdFilterByOspfTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrdFilterByOspfTag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrdFilterOspfTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrdFilterOspfTag (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrdFilterOspfTagMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrdFilterOspfTagMask (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrdRouterASNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrdRouterASNumber (&(pMultiData->i4_SLongValue)));
}

INT4
FsRrdAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRrdAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsRtmThrottleLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsRtmThrottleLimit (&(pMultiData->u4_ULongValue)));
}

INT4 FsEcmpAcrossProtocolAdminStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsEcmpAcrossProtocolAdminStatus(&(pMultiData->i4_SLongValue)));
}

INT4 FsRtmRouteLeakStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsRtmRouteLeakStatus(&(pMultiData->i4_SLongValue)));
}

INT4 FsRtmMaximumBgpRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsRtmMaximumBgpRoutes(&(pMultiData->u4_ULongValue)));
}
INT4 FsRtmMaximumOspfRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsRtmMaximumOspfRoutes(&(pMultiData->u4_ULongValue)));
}
INT4 FsRtmMaximumRipRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsRtmMaximumRipRoutes(&(pMultiData->u4_ULongValue)));
}
INT4 FsRtmMaximumStaticRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsRtmMaximumStaticRoutes(&(pMultiData->u4_ULongValue)));
}
INT4 FsRtmMaximumISISRoutesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsRtmMaximumISISRoutes(&(pMultiData->u4_ULongValue)));
}

INT4 
FsRtmIpStaticRouteDistanceGet(tSnmpIndex * pMultiIndex, 
                              tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhGetFsRtmIpStaticRouteDistance(&(pMultiData->i4_SLongValue)));
}


INT4
FsRrdRouterIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrdRouterId (pMultiData->u4_ULongValue));
}

INT4
FsRrdFilterByOspfTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrdFilterByOspfTag (pMultiData->i4_SLongValue));
}

INT4
FsRrdFilterOspfTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrdFilterOspfTag (pMultiData->i4_SLongValue));
}

INT4
FsRrdFilterOspfTagMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrdFilterOspfTagMask (pMultiData->i4_SLongValue));
}

INT4
FsRrdRouterASNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrdRouterASNumber (pMultiData->i4_SLongValue));
}

INT4
FsRrdAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRrdAdminStatus (pMultiData->i4_SLongValue));
}

INT4
FsRtmThrottleLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsRtmThrottleLimit (pMultiData->u4_ULongValue));
}

INT4 FsEcmpAcrossProtocolAdminStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsEcmpAcrossProtocolAdminStatus(pMultiData->i4_SLongValue));
}

INT4 FsRtmRouteLeakStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsRtmRouteLeakStatus(pMultiData->i4_SLongValue));
}

INT4 FsRtmMaximumBgpRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsRtmMaximumBgpRoutes(pMultiData->u4_ULongValue));
}


INT4 FsRtmMaximumOspfRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsRtmMaximumOspfRoutes(pMultiData->u4_ULongValue));
}


INT4 FsRtmMaximumRipRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsRtmMaximumRipRoutes(pMultiData->u4_ULongValue));
}


INT4 FsRtmMaximumStaticRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsRtmMaximumStaticRoutes(pMultiData->u4_ULongValue));
}


INT4 FsRtmMaximumISISRoutesSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsRtmMaximumISISRoutes(pMultiData->u4_ULongValue));
}

INT4 
FsRtmIpStaticRouteDistanceSet(tSnmpIndex * pMultiIndex, 
                              tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhSetFsRtmIpStaticRouteDistance(pMultiData->i4_SLongValue));
}

INT4
FsRrdRouterIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrdRouterId (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsRrdFilterByOspfTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrdFilterByOspfTag
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrdFilterOspfTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrdFilterOspfTag (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrdFilterOspfTagMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrdFilterOspfTagMask
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrdRouterASNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrdRouterASNumber (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrdAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRrdAdminStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRtmThrottleLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsRtmThrottleLimit (pu4Error, pMultiData->u4_ULongValue));
}

INT4 FsEcmpAcrossProtocolAdminStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsEcmpAcrossProtocolAdminStatus(pu4Error, pMultiData->i4_SLongValue));
}

INT4 FsRtmRouteLeakStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2FsRtmRouteLeakStatus(pu4Error, pMultiData->i4_SLongValue));
}

INT4 FsRtmMaximumBgpRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2FsRtmMaximumBgpRoutes(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsRtmMaximumOspfRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2FsRtmMaximumOspfRoutes(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsRtmMaximumRipRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2FsRtmMaximumRipRoutes(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsRtmMaximumStaticRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2FsRtmMaximumStaticRoutes(pu4Error, pMultiData->u4_ULongValue));
}


INT4 FsRtmMaximumISISRoutesTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2FsRtmMaximumISISRoutes(pu4Error, pMultiData->u4_ULongValue));
}

INT4 
FsRtmIpStaticRouteDistanceTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, 
                               tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
        return(nmhTestv2FsRtmIpStaticRouteDistance(pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsRrdRouterIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrdRouterId (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrdFilterByOspfTagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrdFilterByOspfTag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrdFilterOspfTagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrdFilterOspfTag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrdFilterOspfTagMaskDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrdFilterOspfTagMask
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrdRouterASNumberDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrdRouterASNumber
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRrdAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrdAdminStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRtmThrottleLimitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRtmThrottleLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsEcmpAcrossProtocolAdminStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsEcmpAcrossProtocolAdminStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRtmRouteLeakStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsRtmRouteLeakStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRtmMaximumBgpRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsRtmMaximumBgpRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRtmMaximumOspfRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsRtmMaximumOspfRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRtmMaximumRipRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsRtmMaximumRipRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRtmMaximumStaticRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsRtmMaximumStaticRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRtmMaximumISISRoutesDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsRtmMaximumISISRoutes(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsRtmIpStaticRouteDistanceDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
        return(nmhDepv2FsRtmIpStaticRouteDistance(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrdControlTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrdControlTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrdControlTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrdControlSourceProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdControlTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdControlSourceProto (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsRrdControlDestProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdControlTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdControlDestProto (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRrdControlRouteExportFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdControlTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdControlRouteExportFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrdControlRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdControlTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdControlRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRrdControlSourceProtoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrdControlSourceProto (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRrdControlDestProtoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrdControlDestProto (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrdControlRouteExportFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrdControlRouteExportFlag
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrdControlRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrdControlRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsRrdControlSourceProtoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRrdControlSourceProto (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsRrdControlDestProtoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRrdControlDestProto (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRrdControlRouteExportFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsRrdControlRouteExportFlag (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsRrdControlRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsRrdControlRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsRrdControlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrdControlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRrdRoutingProtoTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRrdRoutingProtoTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRrdRoutingProtoTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRrdRoutingRegnIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdRoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdRoutingRegnId (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsRrdRoutingProtoTaskIdentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdRoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdRoutingProtoTaskIdent
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRrdRoutingProtoQueueIdentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdRoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdRoutingProtoQueueIdent
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsRrdAllowOspfAreaRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdRoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdAllowOspfAreaRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRrdAllowOspfExtRoutesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRrdRoutingProtoTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRrdAllowOspfExtRoutes (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsRrdAllowOspfAreaRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrdAllowOspfAreaRoutes
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsRrdAllowOspfExtRoutesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRrdAllowOspfExtRoutes (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRrdAllowOspfAreaRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsRrdAllowOspfAreaRoutes (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsRrdAllowOspfExtRoutesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRrdAllowOspfExtRoutes (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsRrdRoutingProtoTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRrdRoutingProtoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsRtmCommonRouteTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsRtmCommonRouteTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsRtmCommonRouteTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsRtmCommonRouteIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsRtmCommonRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsRtmCommonRouteProtoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRouteProto (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsRtmCommonRouteAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRouteAge (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsRtmCommonRouteInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRouteInfo (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->pOidValue));

}

INT4
FsRtmCommonRouteNextHopASGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRouteNextHopAS
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsRtmCommonRouteMetric1Get (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRouteMetric1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsRtmCommonRoutePrivateStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRoutePrivateStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4 FsRtmCommonRoutePreferenceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        if (nmhValidateIndexInstanceFsRtmCommonRouteTable(
                pMultiIndex->pIndex[0].u4_ULongValue,
                pMultiIndex->pIndex[1].u4_ULongValue,
                pMultiIndex->pIndex[2].i4_SLongValue,
                pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
        {
                return SNMP_FAILURE;
        }
        return(nmhGetFsRtmCommonRoutePreference(
                pMultiIndex->pIndex[0].u4_ULongValue,
                pMultiIndex->pIndex[1].u4_ULongValue,
                pMultiIndex->pIndex[2].i4_SLongValue,
                pMultiIndex->pIndex[3].u4_ULongValue,
                &(pMultiData->i4_SLongValue)));

}

INT4
FsRtmCommonRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsRtmCommonRouteTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsRtmCommonRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsRtmCommonRouteIfIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRtmCommonRouteIfIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRtmCommonRouteType (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteInfoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRtmCommonRouteInfo (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiIndex->pIndex[3].u4_ULongValue,
                                        pMultiData->pOidValue));

}

INT4
FsRtmCommonRouteNextHopASSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRtmCommonRouteNextHopAS
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteMetric1Set (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRtmCommonRouteMetric1 (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRoutePrivateStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsRtmCommonRoutePrivateStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4 FsRtmCommonRoutePreferenceSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhSetFsRtmCommonRoutePreference(
                pMultiIndex->pIndex[0].u4_ULongValue,
                pMultiIndex->pIndex[1].u4_ULongValue,
                pMultiIndex->pIndex[2].i4_SLongValue,
                pMultiIndex->pIndex[3].u4_ULongValue,
                pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsRtmCommonRouteStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiIndex->pIndex[3].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteIfIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRtmCommonRouteIfIndex (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[3].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRtmCommonRouteType (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteInfoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsRtmCommonRouteInfo (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->pOidValue));

}

INT4
FsRtmCommonRouteNextHopASTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsRtmCommonRouteNextHopAS (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[3].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteMetric1Test (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsRtmCommonRouteMetric1 (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[3].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRoutePrivateStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsRtmCommonRoutePrivateStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4 FsRtmCommonRoutePreferenceTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        return (nmhTestv2FsRtmCommonRoutePreference(pu4Error,
                pMultiIndex->pIndex[0].u4_ULongValue,
                pMultiIndex->pIndex[1].u4_ULongValue,
                pMultiIndex->pIndex[2].i4_SLongValue,
                pMultiIndex->pIndex[3].u4_ULongValue,
                pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsRtmCommonRouteStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[3].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsRtmCommonRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsRtmCommonRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsRtmRedEntryTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsRtmRedEntryTime(&(pMultiData->i4_SLongValue)));
}
INT4
FsRtmRedExitTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    return(nmhGetFsRtmRedExitTime(&(pMultiData->i4_SLongValue)));
}

