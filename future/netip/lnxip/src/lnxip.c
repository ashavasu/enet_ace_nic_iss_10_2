/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxip.c,v 1.56 2016/02/27 10:14:51 siva Exp $
 *
 * Description: Functions handling the Linux IP routing table
 *              updation.
 *
 *******************************************************************/
#ifndef _LNXIP_C
#define _LNXIP_C

#include "rtminc.h"
#include "lnxip.h"
#include "tcp.h"
#include "lnxipint.h"
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
#include "lnxvrf.h"
#endif
#ifdef MRI_WANTED
#include "mri.h"
#endif

#ifdef NPAPI_WANTED
#include "ipnpwr.h"
#endif

#include "lnxiptap.h"
#include "arp.h"
   

/* Variable Declaration. */
fd_set              gReadFds;
fd_set              gReadCpyFds;
tNetLinkSock        RtmNLWrite = { -1, {0, 0, 0, 0}
};
tNetLinkSock        gLnxVrfNLWrite[SYS_DEF_MAX_NUM_CONTEXTS];
extern INT4         gai4SocketId[SYS_DEF_MAX_NUM_CONTEXTS];
extern INT4         gLnxVrfNetLinkSock[SYS_DEF_MAX_NUM_CONTEXTS];



UINT4               gu4RtmTrc = 0;
extern INT1         LnxIpSetForwarding (UINT1 u1Status);
extern UINT1        NetipIndexMgrInitWithSem (VOID);
extern VOID RegisterSTDTCP PROTO ((VOID));
/*****************************************************************************/
/* Function     : LnxTapLock                                                 */
/* Description  : Lock the LnxTap Main Task                                  */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*****************************************************************************/
PUBLIC INT4
LnxTapLock (VOID)
{
    /* Taking the semaphore */
    if (OsixTakeSem (SELF, LNXIP_TAP_SEM_NAME, OSIX_WAIT, ZERO) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : LnxTapUnLock                                               */
/* Description  : UnLock the LnxTap Main Task                                */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*****************************************************************************/
PUBLIC INT4
LnxTapUnLock (VOID)
{
    if (OsixGiveSem (SELF, LNXIP_TAP_SEM_NAME) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : LnxIpPostPktToTap                                          */
/* Description  : This function constructs Pkt Msg and post it to TAP Module */
/* Input        : u4IfIndex, pBuf, pEthHdr, u1Flag                           */
/* Output       : None                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/
VOID
LnxIpPostPktToTap (UINT4 u4IfIdx, tCRU_BUF_CHAIN_HEADER * pBuf,
                   tEnetV2Header * pEthHdr, UINT1 u1Flag)
{
    UINT1               au1Buffer[LNXIP_TAP_MAX_FRAME_SIZE];
    UINT4               u4PktSize = 0;
    INT4                i4PktOffset = 0;
    UINT2               u2EthType = 0;
    tCRU_BUF_CHAIN_HEADER *pPktBuf = NULL;
    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

    pBuf->ModuleData.InterfaceId.u4IfIndex = u4IfIdx;
    if (u1Flag == IP_ID)
    {
        /* Set Ether type to IP(0x800) */
        u2EthType = OSIX_HTONS (0x800);
    }
    else
    {
        /* Set Ether type to IP(0x800) */
        u2EthType = OSIX_HTONS (0x86DD);
    }

    if ((pPktBuf =
         CRU_BUF_Allocate_MsgBufChain (LNXIP_TAP_MAX_FRAME_SIZE, 0)) == NULL)
    {
        return;
    }
    if (pEthHdr != NULL)
    {
        i4PktOffset = 0;
        MEMCPY (&au1Buffer[i4PktOffset], pEthHdr->au1DstAddr,
                CFA_ENET_ADDR_LEN);
        i4PktOffset = i4PktOffset + CFA_ENET_ADDR_LEN;
        MEMCPY (&au1Buffer[i4PktOffset], pEthHdr->au1SrcAddr,
                CFA_ENET_ADDR_LEN);
        i4PktOffset = i4PktOffset + CFA_ENET_ADDR_LEN;
        MEMCPY (&au1Buffer[i4PktOffset], &u2EthType, sizeof (u2EthType));
        i4PktOffset = i4PktOffset + sizeof (u2EthType);
    }

    if (CRU_BUF_Copy_FromBufChain (pBuf, &(au1Buffer[i4PktOffset]), 0,
                                   (u4PktSize)) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
        return;
    }
    i4PktOffset = i4PktOffset + u4PktSize;
    if (CRU_BUF_Copy_OverBufChain (pPktBuf, &(au1Buffer[0]), 0,
                                   i4PktOffset) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
        return;
    }
    pPktBuf->ModuleData.InterfaceId.u4IfIndex = u4IfIdx;
    if (OsixQueSend (gLnxToTapIpQId, (UINT1 *) &pPktBuf, OSIX_DEF_MSG_LEN)
        == OSIX_SUCCESS)
    {
        if (OsixEvtSend (LNXIP_TASK_ID, LNXIP_TAP_PKT_TO_TAP_EVENT) !=
            OSIX_SUCCESS)
        {
            /* EVENT SEND FAILED */
            return;
        }
    }
    else
    {
        /* Queue Send Failed */
        CRU_BUF_Release_MsgBufChain (pPktBuf, FALSE);
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpMain                                        */
/*                                                                          */
/*    Description        : This function initializes the character device   */
/*                         used for communicating with the linux kernel     */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                        */
/****************************************************************************/
INT4
LnxIpMain (INT1 *pi1Dummy)
{
    tLnxIpTapMsg       *pLnxIpTapMsg = NULL;
    UINT4               u4EventMask = ZERO;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    if (LnxIpInit (pi1Dummy) == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return (OSIX_FAILURE);
    }
    if (OsixTskIdSelf (&LNXIP_TASK_ID) != OSIX_SUCCESS)
    {
        lrInitComplete (OSIX_FAILURE);
        return (OSIX_FAILURE);
    }
    lrInitComplete (OSIX_SUCCESS);
    if (gu1LnxTapEnabled == TRUE)
    {
        while (1)
        {
            OsixEvtRecv (LNXIP_TASK_ID,
                         (LNXIP_TAP_PKT_TO_TAP_EVENT |
                          LNXIP_TAP_PKT_FROM_TAP_EVENT),
                         (OSIX_WAIT | OSIX_EV_ANY), &u4EventMask);
            LnxTapLock ();
            if (u4EventMask & LNXIP_TAP_PKT_TO_TAP_EVENT)
            {
                while (OsixQueRecv (gLnxToTapIpQId, (UINT1 *) &pBuf,
                                    OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
                {
                    LnxIpWritePktToTap (pBuf);
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    pBuf = NULL;
                }
            }
            if (u4EventMask & LNXIP_TAP_PKT_FROM_TAP_EVENT)
            {
                while (OsixQueRecv
                       (gLnxFromTapIpQId, (UINT1 *) &pLnxIpTapMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
                {
                    INT4                i4ReleaseFd = pLnxIpTapMsg->i4TapFd;

                    if (pLnxIpTapMsg->u1MsgType == LNXIP_TAP_PKT_FROM_TUN)
                    {
                        LnxIpTapHandlePktFromTapEvent (pLnxIpTapMsg->i4TapFd);
                    }
                    MemReleaseMemBlock (gLnxTapIpEvntMemPoolId,
                                        (UINT1 *) pLnxIpTapMsg);

                    /* SelAddFd moved up as callback will be registered as soon as packet is read */

                    if (SelAddFd (i4ReleaseFd, LnxIpTapFdCbkFunc) ==
                        OSIX_FAILURE)

                    {
                        /* Need To Add Trace Message "Failed to add Fd to Select" */

                    }
                }
            }
            LnxTapUnLock ();
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpInit                                        */
/*                                                                          */
/*    Description        : This function initializes the character device   */
/*                         used for communicating with the linux kernel     */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                        */
/****************************************************************************/
INT4
LnxIpInit (INT1 *pi1Dummy)
{
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    tVcmRegInfo         VcmRegInfo;
#endif
    UNUSED_PARAM (pi1Dummy);

    /* Enable Forwarding */
    if (LnxIpSetForwarding (IP_FORW_ENABLE) == IP_FAILURE)
    {
        return (OSIX_FAILURE);
    }
	/* Initialise the index manager */
	if (NetipIndexMgrInitWithSem () == LNXIP_FAILURE)
	{
		return (OSIX_FAILURE);
	}
	

    /* Enable Gratuitous ARP */
    LnxIpSetGratuitousArp (LNX_GRATUITOUS_ARP_ENABLE);
    
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    MEMSET (&VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.u1ProtoId = IPFWD_PROTOCOL_ID;
    VcmRegInfo.pIfMapChngAndCxtChng = LnxVrfCallbackFn;
    VcmRegInfo.u1InfoMask = VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE;
    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        return (OSIX_FAILURE);
    }

#endif

    if (LnxIpIfInit () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

#ifndef MBSM_WANTED
#ifdef NPAPI_WANTED
    IpFsNpIpInit ();
#endif
#endif

#ifdef LINUX_310_WANTED
#ifdef VRRP_WANTED
    LnxIpVrrpKernelNetLinkInit ();
#endif
#endif

    CfaNotifyIpUp ();

#ifdef SNMP_2_WANTED
    /*
     * Remove this #ifdef when deleting the deprecated objects at
     * standard ip mib to support backward comp.
     * This RegisterSTDIP is replaced by RegisterSTDIPvx in futrure/netipvx
     */
#ifdef REMOVE_ON_STDIP_DEPR_OBJ_DELETE
    RegisterSTDIP ();
#endif /* REMOVE_ON_STDIP_DEPR_OBJ_DELETE */
    RegisterSTDTCP ();
#endif
#ifdef MRI_WANTED
    /* MRI module does not have a task. It executes in CLI/SNMP
       context only. So Initialize the MRI module
     */
    MriInit ();
#endif

    if (gu1LnxTapEnabled == TRUE)
    {
        if ((MemCreateMemPool (sizeof (tLnxIpTapMsg),
                        LNXIP_TAP_EVENT_BLOCKS,
                        MEM_DEFAULT_MEMORY_TYPE,
                        &gLnxTapIpEvntMemPoolId)) == MEM_FAILURE)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
        if ((MemCreateMemPool (sizeof (tLnxVrfMsg),
                        MAX_LNX_VRF_MSG_MEMBLK_SIZE,
                        MEM_DEFAULT_MEMORY_TYPE,
                        &gLnxVrfMsgMemPoolId)) == MEM_FAILURE)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
        if ((MemCreateMemPool (sizeof (tLnxVrfInfo),
                        MAX_LNX_VRF_VCINFO_SIZE,
                        MEM_DEFAULT_MEMORY_TYPE,
                        &gLnxVrfVcInfoMemPoolId)) == MEM_FAILURE)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
        if ((MemCreateMemPool (sizeof (tLnxVrfIfInfo),
                        MAX_LNX_VRF_VCINFO_SIZE,
                        MEM_DEFAULT_MEMORY_TYPE,
                        &gLnxVrfIfInfoMemPoolId)) == MEM_FAILURE)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
        if (OsixCreateSem (LNX_VRF_EVNT_SEM, 1, ZERO,
                    &gLnxEvtSemId) != OSIX_SUCCESS)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
        if (OsixCreateSem (LNX_VRF_GLOB_SEM, 1, ZERO,
                    &gLnxVrfGlobSemId) != OSIX_SUCCESS)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }

#endif
        /* Create semaphore for LnxTap task lock/unlock */
        if (OsixCreateSem (LNXIP_TAP_SEM_NAME, 1, ZERO,
                           &gLnxTapSemId) != OSIX_SUCCESS)
        {
            LnxDeInit();
            return (OSIX_FAILURE);
        }
        if (OsixQueCrt
                ((UINT1 *) LNXIP_TO_TAP_INTF_PACKET_QUEUE, OSIX_MAX_Q_MSG_LEN,
                 LNXIP_TO_TAP_INTF_PACKET_QUEUE_DEPTH,
                 &gLnxToTapIpQId) != OSIX_SUCCESS)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
        if (OsixQueCrt
                ((UINT1 *) LNXIP_FROM_TAP_INTF_PACKET_QUEUE, OSIX_MAX_Q_MSG_LEN,
                 LNXIP_FROM_TAP_INTF_PACKET_QUEUE_DEPTH,
                 &gLnxFromTapIpQId) != OSIX_SUCCESS)
        {
            LnxDeInit();
            /* Indicate the status of initialization to the main routine */
            return OSIX_FAILURE;
        }
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
        if (OsixQueCrt
                ((UINT1 *)LNX_VRF_EVNT_ARRIVAL_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                 LNXIP_FROM_TAP_INTF_PACKET_QUEUE_DEPTH,
                 &gLnxPostBkIpQId) != OSIX_SUCCESS)
        {
            LnxDeInit();
            /* Indicate the status of initialization to the main routine */
            return OSIX_FAILURE;
        }
        LNX_VRF_INFO_TABLE = LNX_VRF_RB_TREE_CREATE_EMBED(FSAP_OFFSETOF(tLnxVrfInfo
                    ,LnxVrfRbNode),LnxVrfContextEntryCompare);
        if(NULL == LNX_VRF_INFO_TABLE)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
        LNX_VRF_IFINFO_TABLE = LNX_VRF_RB_TREE_CREATE_EMBED(FSAP_OFFSETOF(tLnxVrfIfInfo
                    ,LnxVrfIfInfoNode),LnxVrfIfEntryCompare);
        if(NULL == LNX_VRF_IFINFO_TABLE)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
        LnxVrfNLSockInit();

#endif
        if ((MemCreateMemPool (sizeof (tLnxProxyArpEntry),
                        IP_DEV_MAX_IP_INTF,
                        MEM_HEAP_MEMORY_TYPE,
                        &gProxyArpStatusPoolId)) == MEM_FAILURE)
        {
            LnxDeInit();
            return OSIX_FAILURE;
        }
        TMO_SLL_Init (&(gProxyArpAdminStatusList));

    }
    return OSIX_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmSubTaskInit
 *
 * Input(s)           : None 
 *
 * Output(s)          : None
 *
 * Returns            : None
 *
 * Action             : This function initializes the data structures
 *                      used for interaction between RTM Main task and
 *                      the RTM subtask. This function should be
 *                      executed before RTM can handle any message.
 *         
+-------------------------------------------------------------------*/
VOID
RtmSubTaskInit (VOID)
{

    INT4                i4RetVal = 0;
    UINT4               u4Groups;

    if (RtmNLWrite.i4Sock < 0)
    {
        /* Socket for Writing to Linux Kernel */
        u4Groups = 0;
        i4RetVal = RtmNetlinkSockInit (&RtmNLWrite, u4Groups);
        if ((i4RetVal == NETIPV4_FAILURE) && (gu4RtmTrc))
        {
            PRINTF ("\r\n FAILED to initialize Write socket");
        }
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmNetIpv4LeakRoute
 *
 * Input(s)           : u1NetIpv4RtCmd - Specify whether the route needs
 *                                       to be added/modified/removed.
 *                      pRtInfo        - Info about the route to be
 *                                       updated in the IP Fwd Table.
 *
 * Output(s)          : Updated IP Fwd Table.
 *
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : This routine updates the Linux IP Fwd with the
 *                      given route info.
 *         
+-------------------------------------------------------------------*/
INT4
RtmNetIpv4LeakRoute (UINT1 u1NetIpv4RtCmd, tRtInfo * pRtInfo)
{
    tNetIpv4RtInfo      NxtHopInfo;
    tRtInfo             NxtHopRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4IfInfo      IpIfInfo;
    UINT4               u4DupRtFlag = OSIX_FALSE;
    INT4                i4RetVal = NETIPV4_FAILURE;
    tRtInfo            *pTmpRt = NULL;
    tRtmCxt            *pRtmCxt = NULL;
    tRtInfo            *pNewBestRt = NULL;
    UINT1               u1BestRtCnt = 0;

    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&NxtHopInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&NxtHopRtInfo, 0, sizeof (tRtInfo));
    MEMSET (&IpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    /* Since the local routes are added in the kernel when interface is created
     * So addition of local route to the kernel is not required*/
    if ((pRtInfo == NULL) || (pRtInfo->u2RtProto == CIDR_LOCAL_ID))
    {
        return (i4RetVal);
    }

    /* Check whether interface is unnumbered. If yes,set the nexthop as 0 */
    if (NetIpv4GetIfInfo (pRtInfo->u4RtIfIndx, &IpIfInfo) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    if ((IpIfInfo.u4Addr == 0) && (pRtInfo->u2RtProto == OSPF_ID)
        && (pRtInfo->u4NextHop != 0))
    {
        RtQuery.u4DestinationIpAddress = pRtInfo->u4NextHop;
        RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
        RtQuery.u2AppIds = ALL_ROUTING_PROTOCOL;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_EXACT_DEST;

        i4RetVal = RtmUtilIpv4GetRoute (&RtQuery, &NxtHopInfo);

        /* Check whether route is there for nexthop. If present, add the 
         * original route. If not present, add one dummy route for the nexthop
         * since Linux IP wont accept the routes for which route to nexthop is
         * not available in the routing table. After adding the original route,
         * delete the dummy route. */

        if (i4RetVal != IP_SUCCESS)
        {
            NxtHopRtInfo.u4DestNet = pRtInfo->u4NextHop;
            NxtHopRtInfo.u4DestMask = 0xFFFFFFFF;
            NxtHopRtInfo.u4NextHop = pRtInfo->u4NextHop;
            NxtHopRtInfo.u4RtIfIndx = pRtInfo->u4RtIfIndx;
            NxtHopRtInfo.u2RtType = pRtInfo->u2RtType;
            NxtHopRtInfo.u2RtProto = pRtInfo->u2RtProto;
            NxtHopRtInfo.u4RtAge = IPFWD_ROUTE_AGE_DEFAULT;
            NxtHopRtInfo.i4Metric1 = IPFWD_ROUTE_METRIC_DEFAULT;
            NxtHopRtInfo.u4RtNxtHopAS = IPFWD_ROUTE_NEXTHOPAS_DEFAULT;
            NxtHopRtInfo.u4RowStatus = IPFWD_ACTIVE;
            NxtHopRtInfo.u4RouteTag = LNXIP_DEF_ROUTE_TAG;
            NxtHopRtInfo.u1Preference = OSPF_DEFAULT_PREFERENCE;
            NxtHopRtInfo.i4MetricType = LNXIP_DEF_METRIC_TYPE;

            i4RetVal = NetLinkRouteModify (RTM_NEWROUTE, &NxtHopRtInfo);
            u4DupRtFlag = OSIX_TRUE;
        }

    }
    if (gu4RtmTrc)
    {
        PRINTF ("LeakRoute - DesNet:%d Mask:%d Gw:%d Metric:%d if:%d\n",
                pRtInfo->u4DestNet, pRtInfo->u4DestMask,
                pRtInfo->u4NextHop, pRtInfo->i4Metric1, pRtInfo->u4RtIfIndx);
    }

    switch (u1NetIpv4RtCmd)
    {
        case NETIPV4_ADD_ROUTE:
        {
            i4RetVal = NetLinkRouteModify (RTM_NEWROUTE, pRtInfo);
            break;
        }
        case NETIPV4_DELETE_ROUTE:
        {

            i4RetVal = NetLinkRouteModify (RTM_DELROUTE, pRtInfo);

            pRtmCxt = UtilRtmGetCxt (RTM_DEFAULT_CXT_ID);
            if (pRtmCxt == NULL)
            {
                return NETIPV4_FAILURE;
            }

            IpGetBestRouteEntryInCxt (pRtmCxt, *pRtInfo, &pNewBestRt);

            if (pNewBestRt == NULL)
            {
                /*Not an ECMP route. Deletion is done already from kernel hence return */
                return i4RetVal;
            }
            /* Since there is a kernel bug , we are now deleting all
             * the routes including the input route and adding the
             * all the routes back to RTM except the input route.
             *
             * If there are X+n routes in RTM and we want to delete
             * X+1 route, we delete all routes including X, X+1, X+2..
             * .. X+n, and then adding the routes X, X+2, X+n,
             * i.e except the input route (X+1).
             *
             */
            for (pTmpRt = pNewBestRt; pTmpRt != NULL;
                 pTmpRt = pTmpRt->pNextAlternatepath)
            {
                if (pTmpRt->i4Metric1 == pRtInfo->i4Metric1)
                {
                    i4RetVal = NetLinkRouteModify (RTM_DELROUTE, pTmpRt);
                }
            }

            for (pTmpRt = pNewBestRt; pTmpRt != NULL;
                 pTmpRt = pTmpRt->pNextAlternatepath)
            {
                if ((pRtInfo->u4DestNet == pTmpRt->u4DestNet) &&
                    (pRtInfo->u4DestMask == pTmpRt->u4DestMask) &&
                    (pRtInfo->u4NextHop == pTmpRt->u4NextHop) &&
                    (pRtInfo->u4RtIfIndx == pTmpRt->u4RtIfIndx))
                {
                    continue;
                }
                if (pTmpRt->i4Metric1 == pRtInfo->i4Metric1)
                {
                    /* If list contains unreachable routes followed by a reachable route,
                     * add reachable route in kernel */
                    if ((pTmpRt->u4Flag & RTM_RT_REACHABLE)
                        && (pTmpRt->u4Flag & RTM_RT_HWSTATUS))
                    {
                        i4RetVal = NetLinkRouteModify (RTM_NEWROUTE, pTmpRt);
                        u1BestRtCnt++;
                        break;
                    }
                }
            }
            /* In case no reachable route present in the list, 
             * install the first configured route in kernel*/
            if ((u1BestRtCnt == 0) && (pNewBestRt != pRtInfo))
            {
                i4RetVal = NetLinkRouteModify (RTM_NEWROUTE, pNewBestRt);
            }
            break;
        }
        case NETIPV4_MODIFY_ROUTE:
        {
            i4RetVal = NetLinkRouteModify (RTM_NEWROUTE, pRtInfo);
            break;
        }
        default:
            break;
    }

    if ((u4DupRtFlag == OSIX_TRUE) && (pRtInfo->u2RtProto == OSPF_ID))
    {
        i4RetVal = NetLinkRouteModify (RTM_DELROUTE, &NxtHopRtInfo);
    }
    return i4RetVal;
}

/*-------------------------------------------------------------------+
 *
 * Function           : NetLinkRouteModify
 *
 * Input(s)           : i4Cmd - Specify whether the route needs to be
 *                              added/modified/removed.
 *                      pRtInfo - Info about the route to be
 *                                   updated in the IP Fwd Table.
 
 *
 * Output(s)          : None
 *
 * Returns            : -1 on failure else 0
 *
 * Action             : This function will update the Linux Kernal IP
 *                      Fwd Table with the given route entry. 
 *         
+-------------------------------------------------------------------*/
INT4
NetLinkRouteModify (INT4 i4Cmd, tRtInfo * pRtInfo)
{
    INT4                i4ByteLen;
    tRtInfo            *pNewBestRt = NULL;
    tRtInfo            *pTmpRt = NULL;
    tRtInfo            *pTmpRtInfo = NULL;
    struct rtattr      *pRouteAttr = NULL;
    struct rtnexthop   *pRouteNxtHop = NULL;
    tRequest            Request;
    tNetIpv4IfInfo      IpIfInfo;
    tRtmCxt            *pRtmCxt = NULL;
    UINT4               u4DestNet = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4UnnumFlag = 0;
    UINT1               u1BestRtCnt = 0;
#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4 		u4ContextId = 0;
    UINT4    		u4CfaIfIndex = 0;
    INT4               	i4SockFd = -1;
    tLnxVrfEventInfo  LnxVrfEventInfo;
#endif

    MEMSET (&Request, 0, sizeof Request);
    MEMSET (&IpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    i4ByteLen = 4;

    Request.NLMsgHdr.nlmsg_len = NLMSG_LENGTH (sizeof (struct rtmsg));
    Request.NLMsgHdr.nlmsg_type = i4Cmd;
    if (i4Cmd == RTM_NEWROUTE)
    {
        Request.NLMsgHdr.nlmsg_flags = NLM_F_CREATE | NLM_F_REQUEST;
    }
    else
    {
        Request.NLMsgHdr.nlmsg_flags =
            NLM_F_CREATE | NLM_F_REQUEST | NLM_F_REPLACE;
    }
    Request.RtmMsg.rtm_family = AF_INET;
    Request.RtmMsg.rtm_table = RT_TABLE_MAIN;
    Request.RtmMsg.rtm_dst_len = RtmIpMaskToPrefixLen (pRtInfo->u4DestMask);

    Request.RtmMsg.rtm_protocol = RT_FS_PROTO;
    Request.RtmMsg.rtm_scope = RT_SCOPE_UNIVERSE;

    if (pRtInfo->u2RtProto == CIDR_LOCAL_ID)
    {
        Request.RtmMsg.rtm_type = RTN_LOCAL;
    }
    else
    {
        Request.RtmMsg.rtm_type = RTN_UNICAST;
    }

    /* Check whether interface is unnumbered. If yes,set the nexthop as 0 */

    if (NetIpv4GetIfInfo (pRtInfo->u4RtIfIndx, &IpIfInfo) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    if ((IpIfInfo.u4Addr == 0)
        && ((pRtInfo->u4DestNet == pRtInfo->u4NextHop)
            || (pRtInfo->u4NextHop == 0)))
    {
        u4UnnumFlag = 1;
        Request.RtmMsg.rtm_scope = RT_SCOPE_LINK;
    }

    u4DestNet = OSIX_HTONL (pRtInfo->u4DestNet);
    RtmAddAttribute (&Request.NLMsgHdr, sizeof Request, RTA_DST,
                     &u4DestNet, i4ByteLen);

    RtmAddAttribute (&Request.NLMsgHdr, sizeof Request,
                     RTA_PRIORITY, &pRtInfo->i4Metric1, sizeof (UINT4));

    pRtmCxt = UtilRtmGetCxt (IpIfInfo.u4ContextId);

    if (pRtmCxt == NULL)
    {
        return NETIPV4_FAILURE;
    }

    IpGetBestRouteEntryInCxt (pRtmCxt, *pRtInfo, &pNewBestRt);

    if ((pNewBestRt != NULL) && (pNewBestRt->u4Flag & RTM_ECMP_RT))
    {
        pTmpRt = pNewBestRt;
        while ((pTmpRt != NULL) && (pTmpRt->i4Metric1 == pNewBestRt->i4Metric1))
        {
            if ((pTmpRt->u4Flag & RTM_RT_REACHABLE)
                && (pTmpRt->u4Flag & RTM_RT_HWSTATUS))
            {
                u1BestRtCnt++;
                /*If the list contains unreachable routes followed by reachable routes
                 *save the first reachable one */
                if (u1BestRtCnt == 1)
                {
                    pTmpRtInfo = pTmpRt;
                }
            }
            pTmpRt = pTmpRt->pNextAlternatepath;
        }
    }
    pTmpRt = pTmpRtInfo;
    /* If Route addition is done, check whether ECMP routes have be updated
     * (add/delete) in the Kernel Routing Table. Deletion of a ECMP route simply
     * replaces the existing entry */
    if ((u1BestRtCnt > 1) && (!((i4Cmd == RTM_DELROUTE))))
    {
        Request.RtmMsg.rtm_flags |= RTM_F_EQUALIZE;
        Request.NLMsgHdr.nlmsg_flags |= NLM_F_REPLACE;
        Request.NLMsgHdr.nlmsg_type = RTM_NEWROUTE;

        /* When considering sizeof(struct rtattr) for ecmp routes,
         * sizeof data added for each ecmp route needs to be considered.
         * This is taken care for MAX_PATHS using the below calculation.
         * Also size of nlmsghdr and rtmmsg also needs to be added. */
 
        pRouteAttr =
            (struct rtattr *) (VOID *) &Request.u1Buf +
            ((MAX_PATHS * (sizeof(struct rtattr) + sizeof(UINT4)))
             + sizeof(struct nlmsghdr) + sizeof(struct rtmsg))/(sizeof(struct rtattr));        
        pRouteAttr->rta_type = RTA_MULTIPATH;
        pRouteAttr->rta_len = RTA_LENGTH (0);
        pRouteNxtHop = RTA_DATA (pRouteAttr);

        while (pNewBestRt != NULL)
        {
            if ((pNewBestRt->u4Flag & RTM_ECMP_RT)
                && (pNewBestRt->u4RowStatus == IPFWD_ACTIVE)
                && (pTmpRt->i4Metric1 == pNewBestRt->i4Metric1)
                && (pNewBestRt->u4Flag & RTM_RT_REACHABLE)
                && (pNewBestRt->u4Flag & RTM_RT_HWSTATUS))
            {

                RtmAddAttribute (&Request.NLMsgHdr, sizeof Request, RTA_OIF,
                                 &(pTmpRt->u4RtIfIndx), sizeof (UINT4));

                memset (pRouteNxtHop, 0, sizeof (struct rtnexthop));
                pRouteNxtHop->rtnh_len = sizeof (struct rtnexthop);
                pRouteAttr->rta_len += pRouteNxtHop->rtnh_len;

                u4NextHop = OSIX_HTONL (pNewBestRt->u4NextHop);
                RtmNetlinkAddRtAttribute (pRouteAttr, sizeof Request,
                                          RTA_GATEWAY, &u4NextHop, i4ByteLen);
                pRouteNxtHop->rtnh_ifindex = pNewBestRt->u4RtIfIndx;

                pRouteNxtHop->rtnh_len += sizeof (struct rtattr) + 4;

                pRouteNxtHop =
                    ((struct rtnexthop *) (VOID *) (((char *) (pRouteNxtHop)) +
                                                    RTNH_ALIGN ((pRouteNxtHop)->
                                                                rtnh_len)));

            }
            pNewBestRt = pNewBestRt->pNextAlternatepath;
        }
        /* If ECMP routes are configured, use RTA_MULTIPATH option to set
         * the alternative routes in Kernel route table */
        RtmAddAttribute (&Request.NLMsgHdr, sizeof Request, RTA_MULTIPATH,
                         RTA_DATA (pRouteAttr), RTA_PAYLOAD (pRouteAttr));
    }
    else
    {
        /* Non ECMP routes - Update the Next Hop and If Index details before
         * adding/deleting the same from the kernel route table */
        if (u4UnnumFlag == 0)
        {
            u4NextHop = OSIX_HTONL (pRtInfo->u4NextHop);
            RtmAddAttribute (&Request.NLMsgHdr, sizeof Request, RTA_GATEWAY,
                             &u4NextHop, i4ByteLen);
        }

        if ((pRtInfo->u4RtIfIndx != IPIF_INVALID_INDEX)
            && (pRtInfo->u4RtIfIndx > 0))
        {
            RtmAddAttribute (&Request.NLMsgHdr, sizeof Request, RTA_OIF,
                             &(pRtInfo->u4RtIfIndx), sizeof (UINT4));
        }
    }

#if defined(VRF_WANTED) && defined (LINUX_310_WANTED)
    u4ContextId = pRtmCxt->u4ContextId;
#ifdef LNXIP6_WANTED
    NetIpv6GetCfaIfIndexFromPort(pRtInfo->u4RtIfIndx,&u4CfaIfIndex);
#endif
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_NETLINK;
    LnxVrfEventInfo.i4SockType=SOCK_RAW;
    LnxVrfEventInfo.i4SockProto = NETLINK_ROUTE;
    LnxVrfEventInfo.u4IfIndex = u4CfaIfIndex;
    LnxVrfEventInfo.u1MsgType =LNX_VRF_OPEN_SOCKET ;

    if (gLnxVrfNLWrite[u4ContextId].i4Sock < 0)
    {
	    LnxVrfSockLock();
	    LnxVrfEventHandling(&LnxVrfEventInfo,&i4SockFd);
	    LnxVrfSockUnLock();
	    gLnxVrfNLWrite[u4ContextId].i4Sock = i4SockFd;     
	    RtmNetlinkSockInit (&gLnxVrfNLWrite[u4ContextId], 0);
    }
    RtmNLWrite= gLnxVrfNLWrite[u4ContextId];
#else
    if (RtmNLWrite.i4Sock < 0)
    {
	    RtmNetlinkSockInit (&RtmNLWrite, 0);
    }
#endif
    return RtmSendToNetLink (&Request.NLMsgHdr, &RtmNLWrite,
                             LNXIP_INET_AFI_IPV4);
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmNetlinkSockInit
 *
 * Input(s)           : pNLSocket - Pointer to the socket structure
 *                                  to be initialized.
 *
 * Output(s)          : pNLSocket - ID for the initialized socket.
 *
 * Returns            : NETIPV4_SUCCESS/NETIPV4_FAILURE. 
 *
 * Action             : This function will initialize the socket to the
 *                      specified address family.
 *         
+-------------------------------------------------------------------*/
INT4
RtmNetlinkSockInit (tNetLinkSock * pNLSocket, UINT4 u4Groups)
{
    INT4                i4Socket;
    INT4                i4RetVal;
    INT4                i4NameLen;
    struct sockaddr_nl  NLSockAddr;

    if(pNLSocket->i4Sock < 0)
    {
        i4Socket = socket (AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
        if (i4Socket < 0)
        {
            return NETIPV4_FAILURE;
        }
    }
    else
    {
        i4Socket = pNLSocket->i4Sock;
    }

    i4RetVal = fcntl (i4Socket, F_SETFL, O_NONBLOCK);
    if (i4RetVal < 0)
    {
        close (i4Socket);
        return NETIPV4_FAILURE;
    }

    MEMSET (&NLSockAddr, 0, sizeof NLSockAddr);
    NLSockAddr.nl_family = AF_NETLINK;
    NLSockAddr.nl_groups = u4Groups;

    i4RetVal = bind (i4Socket, (struct sockaddr *) &NLSockAddr,
                     sizeof NLSockAddr);
    if (i4RetVal < 0)
    {
        close (i4Socket);
        return NETIPV4_FAILURE;
    }

    i4NameLen = sizeof NLSockAddr;
    i4RetVal = getsockname (i4Socket, (struct sockaddr *) &NLSockAddr,
                            (socklen_t *) & i4NameLen);
    if (i4RetVal < 0 || i4NameLen != sizeof NLSockAddr)
    {
        close (i4Socket);
        return NETIPV4_FAILURE;
    }

    pNLSocket->snl = NLSockAddr;
    pNLSocket->i4Sock = i4Socket;
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmNetlinkGetRoute
 *
 * Input(s)           : pNLSocket - Pointer to the netlink socket.
 *
 * Output(s)          : None.
 *
 * Returns            :NETIPV4_SUCCESS/NETIPV4_FAILURE.
 *
 * Action             : Send a route request message to the Linux
 *                      Kernal.
 *         
+-------------------------------------------------------------------*/
INT4
RtmNetlinkGetRoute (tNetLinkSock * pNLSocket)
{
    INT4                i4RetVal;
    struct sockaddr_nl  NLSockAddr;
    UINT4               u4Prefix = 0;
    tRequest            Request;

    if (pNLSocket->i4Sock < 0)
    {
        if (gu4RtmTrc)
        {
            PRINTF ("Invalid socket identifier.");
        }
        return NETIPV4_FAILURE;
    }

    MEMSET (&NLSockAddr, 0, sizeof NLSockAddr);
    MEMSET (&Request, 0, sizeof Request);
    NLSockAddr.nl_family = AF_NETLINK;

    Request.NLMsgHdr.nlmsg_len = NLMSG_LENGTH (sizeof (struct rtmsg));
    Request.NLMsgHdr.nlmsg_flags = NLM_F_ROOT | NLM_F_REQUEST | NLM_F_MATCH;
    Request.NLMsgHdr.nlmsg_type = RTM_GETROUTE;
    Request.NLMsgHdr.nlmsg_pid = 0;
    Request.NLMsgHdr.nlmsg_seq = 0;
    Request.RtmMsg.rtm_family = AF_INET;
    Request.RtmMsg.rtm_scope = 0;
    Request.RtmMsg.rtm_table = 0;
    Request.RtmMsg.rtm_protocol = 0;
    Request.RtmMsg.rtm_type = RTN_UNICAST;
    Request.RtmMsg.rtm_dst_len = 0;

    RtmAddAttribute (&Request.NLMsgHdr, sizeof Request, RTA_DST,
                     &u4Prefix, sizeof (UINT4));

    /* Destination netlink address. */
    MEMSET (&NLSockAddr, 0, sizeof NLSockAddr);
    NLSockAddr.nl_family = AF_NETLINK;

    i4RetVal = sendto (pNLSocket->i4Sock, (void *) &Request,
                       sizeof Request, 0, (struct sockaddr *) &NLSockAddr,
                       sizeof NLSockAddr);
    if (i4RetVal < 0)
    {
        if (gu4RtmTrc)
        {
            PRINTF ("Sending message to kernel error is: %s", strerror (errno));
        }
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmNetlinkParseInfo
 *
 * Input(s)           : u1InfoFlag - Indicate whether ACK to be verified or
 *                      Route to be parsed.
 *                      pNLSocket  - Pointer to the netlink socket.
 *                      u2Afi - Address family of this socket
 *
 * Output(s)          : None. 
 *
 * Returns            : NETIPV4_FAILURE on error, else NETIPV4_SUCCESS
 *
 * Action             : Parse the response message received from Linux IP
 *                      for the route request message. 
 *         
+-------------------------------------------------------------------*/
INT4
RtmNetlinkParseInfo (UINT1 u1InfoFlag, tNetLinkSock * pNLSocket, UINT2 u2Afi)
{
    INT4                i4Status;
    INT4                i4RetVal = NETIPV4_SUCCESS;
    struct nlmsgerr    *NLMsgError;

    UNUSED_PARAM (u1InfoFlag);
    UNUSED_PARAM (u2Afi);

    while (1)
    {
        UINT1               u1Buf[4096];
        struct iovec        iov = { NULL, 0 };
        struct sockaddr_nl  NLSockAddr;
        struct msghdr       MsgHdr =
            { (void *) NULL, sizeof NLSockAddr, (void *) NULL, 1, NULL,
            0, 0
        };
        struct nlmsghdr    *NLMsgHdr;

        iov.iov_base = (void *) u1Buf;
        iov.iov_len = sizeof (u1Buf);
        MsgHdr.msg_name = (void *) &NLSockAddr;
        MsgHdr.msg_iov = (void *) &iov;

        i4Status = recvmsg (pNLSocket->i4Sock, &MsgHdr, 0);

        if (i4Status < 0)
        {
            if (errno == EINTR)
                continue;
            if (errno == EWOULDBLOCK)
                break;
            continue;
        }

        if (i4Status == 0)
        {
            if (gu4RtmTrc)
            {
                PRINTF ("\r\nNo message to read");
            }
            i4RetVal = NETIPV4_FAILURE;
            break;
        }

        if (MsgHdr.msg_namelen != sizeof NLSockAddr)
        {
            if (gu4RtmTrc)
            {
                PRINTF ("Incorrect length in sender address: length %d",
                        MsgHdr.msg_namelen);
            }
            i4RetVal = NETIPV4_FAILURE;
            break;
        }

        for (NLMsgHdr = (struct nlmsghdr *) (VOID *) u1Buf;
             NLMSG_OK (NLMsgHdr, (UINT4) i4Status);
             NLMsgHdr = ((i4Status) -= NLMSG_ALIGN ((NLMsgHdr)->nlmsg_len),
                         (struct nlmsghdr *) (VOID *) (((char *) (NLMsgHdr)) +
                                                       NLMSG_ALIGN ((NLMsgHdr)->
                                                                    nlmsg_len))))

        {
            if (NLMsgHdr->nlmsg_type == NLMSG_DONE)
            {
                break;
            }

            if (NLMsgHdr->nlmsg_type == NLMSG_ERROR)
            {
                NLMsgError = (struct nlmsgerr *) NLMSG_DATA (NLMsgHdr);

                /* Check for the Acknowledgment */
                if (NLMsgError->error == 0)
                {
                    /* return if not a multipart message, otherwise continue
                     *                      *                      */
                    if (!(NLMsgHdr->nlmsg_flags & NLM_F_MULTI))
                    {
                        break;
                    }
                    continue;
                }

                if (NLMsgHdr->nlmsg_len <
                    NLMSG_LENGTH (sizeof (struct nlmsgerr)))
                {
                    if (gu4RtmTrc)
                    {
                        PRINTF ("Truncated message\n");
                    }

                }
                i4RetVal = NETIPV4_FAILURE;
                break;
            }
        }

        /* Still Some more routes to be read */
        if (MsgHdr.msg_flags & MSG_TRUNC)
        {
            if (gu4RtmTrc)
            {
                PRINTF ("Truncated  message");
            }
            continue;
        }
        if (i4Status)
        {
            i4RetVal = NETIPV4_FAILURE;
        }
        return i4RetVal;
    }
    return i4RetVal;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmSendToNetLink
 *
 * Input(s)           : pNLMsgHdr - Pointer to the netlink message header
 *                      pNLSock - Pointer to the netlink socket
 *                      u2Afi - Address family for this socket.
 *
 * Output(s)          : None.
 *
 * Returns            : -1 on failure else 0 on success
 *
 * Action             : This function writes the given route information
 *                      in the netlink socket for updating the Linux IP
 *                      Fwd Table.
 *         
+-------------------------------------------------------------------*/
INT4
RtmSendToNetLink (struct nlmsghdr *pNLMsgHdr, tNetLinkSock * pNLSock,
                  UINT2 u2Afi)
{
    INT4                i4Status;
    struct sockaddr_nl  NLSockAddr;
    struct iovec        iov = { (void *) NULL, 0 };
    struct msghdr       MsgHdr =
        { (void *) NULL, sizeof NLSockAddr, (void *) NULL, 1, NULL, 0, 0 };
    INT4                i4Flags = 0;

    iov.iov_base = (void *) pNLMsgHdr;
    iov.iov_len = pNLMsgHdr->nlmsg_len;
    MsgHdr.msg_name = (void *) &NLSockAddr;
    MsgHdr.msg_iov = (void *) &iov;
    MsgHdr.msg_flags |= MSG_DONTWAIT;

    MEMSET (&NLSockAddr, 0, sizeof NLSockAddr);
    NLSockAddr.nl_family = AF_NETLINK;
    pNLMsgHdr->nlmsg_seq = 0;
    pNLMsgHdr->nlmsg_pid = pNLSock->snl.nl_pid;
    /* Request an acknowledgement by setting NLM_F_ACK */
    pNLMsgHdr->nlmsg_flags |= NLM_F_ACK;

    /* Send message to netlink interface. */
    i4Status = sendmsg (pNLSock->i4Sock, &MsgHdr, 0);
    if (i4Status < 0)
    {
        if (gu4RtmTrc)
        {
            PRINTF (" sendmsg() failed");
        }
        return NETIPV4_FAILURE;
    }

    /* 
     * Read the acknowledgment to finish the updation
     */

    if ((i4Flags = fcntl (pNLSock->i4Sock, F_GETFL, 0)) < 0)
    {
        if (gu4RtmTrc)
        {
            PRINTF ("FNCTL FAILURE - Getting the current flags\n");
        }
    }
    i4Flags &= ~O_NONBLOCK;
    if (fcntl (pNLSock->i4Sock, F_SETFL, i4Flags) < 0)
    {
        if (gu4RtmTrc)
        {
            PRINTF ("FNCTL FAILURE - Setting to Blocking\n");
        }
    }
    /* 
     *    * Get reply from netlink socket. 
     *       * The reply should either be an acknowlegement or an error.
     *          */
    i4Status = RtmNetlinkParseInfo (RTM_CHECK_ACK, pNLSock, u2Afi);
    /* Restore socket flags for nonblocking I/O */
    i4Flags |= O_NONBLOCK;
    if (fcntl (pNLSock->i4Sock, F_SETFL, i4Flags) < 0)
    {
        if (gu4RtmTrc)
        {
            PRINTF ("FNCTL FAILURE - Setting to NON Blocking\n");
        }
    }
    return i4Status;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleIfCreationDeletion. 
 *
 * Input(s)           : pBuf  -  buffer recvd from the cfa.
 *
 * Output(s)          : None.
 *
 * Returns            : -1 on failure else 0
 *
 * Action             : This routine is called when the interface 
 *                      creation/deletion notification received from 
 *                      cfa.
 *         
+-------------------------------------------------------------------*/
VOID
RtmHandleIfCreationDeletion (UINT1 u1Status, tIpBuf * pBuf)
{
    tNetIpv4IfInfo      NetIpIfInfo;

    if (IP_COPY_FROM_BUF (pBuf, &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo)) !=
        sizeof (tNetIpv4IfInfo))
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    IP_RELEASE_BUF (pBuf, FALSE);

    if (u1Status == IPIF_CREATE_INTERFACE)
    {
        /* Updation of Local route route is done from
         * CFA itself.So no other action is take on 
         * interface creation */
    }

    else if (u1Status == IPIF_DELETE_INTERFACE)
    {
        RtmUtilActOnStaticRtForIfDelete (VCM_DEFAULT_CONTEXT,
                                         NetIpIfInfo.u4IfIndex);

        IpIfStateChngNotify (&NetIpIfInfo, IFACE_DELETED);

    }

}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmHandleIfStateChng. 
 *
 * Input(s)           : pBuf  -  buffer recvd from the cfa.
 *
 * Output(s)          : None.
 *
 * Returns            : -1 on failure else 0
 *
 * Action             : This routine is called when the interface 
 *                      Interface State change notification received from 
 *                      cfa.
 *         
+-------------------------------------------------------------------*/
VOID
RtmHandleIfStateChng (UINT1 u1Status, tIpBuf * pBuf)
{
    tNetIpv4IfInfo      NetIpIfInfo;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfInfo *pLnxVrfInfo = NULL;
#endif

    if (IP_COPY_FROM_BUF (pBuf, &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo)) !=
        sizeof (tNetIpv4IfInfo))
    {
        IP_RELEASE_BUF (pBuf, FALSE);
        return;
    }

    IP_RELEASE_BUF (pBuf, FALSE);


#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(NetIpIfInfo.u4ContextId)
    {
        pLnxVrfInfo = LnxVrfInfoGet(NetIpIfInfo.u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return;
        }
        LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS, (CHR1 *)pLnxVrfInfo->au1NameSpace);
    }
#endif


    if (u1Status == IP_PORT_SPEED_CHANGE)
    {
        IpIfStateChngNotify (&NetIpIfInfo, IF_SPEED_BIT_MASK);
    }
    else if (u1Status == IP_PORT_MTU_CHANGE)
    {
        IpIfStateChngNotify (&NetIpIfInfo, IF_MTU_BIT_MASK);
    }
    else if (u1Status == IP_INTERFACE_UP)
    {
        NetIpIfInfo.u4Oper = IPIF_OPER_ENABLE;
        RtmUtilProcessRtForIfStatChange (NetIpIfInfo.u4IfIndex,
                                         NetIpIfInfo.u4Oper);
        IpIfStateChngNotify (&NetIpIfInfo, OPER_STATE);
    }
    else if (u1Status == IP_INTERFACE_DOWN)
    {
        NetIpIfInfo.u4Oper = IPIF_OPER_DISABLE;
        RtmUtilProcessRtForIfStatChange (NetIpIfInfo.u4IfIndex,
                                         NetIpIfInfo.u4Oper);
        IpIfStateChngNotify (&NetIpIfInfo, OPER_STATE );
    }

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(NetIpIfInfo.u4ContextId)
    {
        LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID);
    }
#endif
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmIpMaskToPrefixLen
 *
 * Input(s)           : u4Mask - Mask value. 
 *
 * Output(s)          : None.
 *
 * Returns            : Prefix length of the given mask.
 *
 * Action             : Returns the PrefixLen for the given mask.
 *         
+-------------------------------------------------------------------*/
INT4
RtmIpMaskToPrefixLen (UINT4 u4Mask)
{
    UINT1               u1Tmp;
    UINT1              *pu1Byte;
    UINT1               u1TestByte = 0;
    INT4                i4Len = 0;
    INT4                i4Byte, i4Bit;

    u4Mask = OSIX_NTOHL (u4Mask);
    pu1Byte = (UINT1 *) &u4Mask;
    for (i4Byte = 0; i4Byte < 4; i4Byte++)
    {
        u1TestByte = *pu1Byte;
        u1Tmp = 0x80;
        for (i4Bit = 0; i4Bit < 8; i4Bit++)
        {
            if (!(u1TestByte & u1Tmp))
            {
                break;
            }
            i4Len++;
            u1TestByte = u1TestByte << 1;
        }
        if (i4Bit != 8)
            break;
        pu1Byte++;
    }
    return (i4Len);
}

INT4
RtmAddAttribute (struct nlmsghdr *pNLMsgHdr, INT4 i4MaxLen, INT4 i4Type,
                 VOID *pData, INT4 i4Len)
{
    INT4                i4AlignLen;
    struct rtattr      *pRouteAttr;

    i4AlignLen = RTA_LENGTH (i4Len);

    if (NLMSG_ALIGN (pNLMsgHdr->nlmsg_len) + i4AlignLen > (UINT4) i4MaxLen)
        return NETIPV4_FAILURE;

    pRouteAttr = (struct rtattr *) (VOID *)
        (((char *) pNLMsgHdr) + NLMSG_ALIGN (pNLMsgHdr->nlmsg_len));
    pRouteAttr->rta_type = i4Type;
    pRouteAttr->rta_len = i4AlignLen;
    MEMMOVE (RTA_DATA (pRouteAttr), pData, i4Len);
    pNLMsgHdr->nlmsg_len = NLMSG_ALIGN (pNLMsgHdr->nlmsg_len) + i4AlignLen;

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : RtmNetlinkAddRtAttribute
 *
 * Input(s)           : pRtAttr - Pointer to Route Attribute
 *                      i4MaxLen - Max Length of the data
 *                      i4Type   - Type of the data
 *                      pData    - Data pointer
 *                      i4Len    - Total length of the incoming data
 *
 * Output(s)          : None.
 *
 * Returns            : NETIPV4_SUCCESS if the attributes are updated
 *                      correctly else NETIPV4_FAILURE
 *
 * Action             : This function updated the Route Attributes with 
 *                      the incoming data. This is used for building up 
 *                      ECMP routes
 *         
+-------------------------------------------------------------------*/
INT4
RtmNetlinkAddRtAttribute (struct rtattr *pRtAttr, INT4 i4MaxLen, INT4 i4Type,
                          VOID *pData, INT4 i4Len)
{
    INT4                i4AlignLen;
    struct rtattr      *pRouteAttr;

    i4AlignLen = RTA_LENGTH (i4Len);

    if (RTA_ALIGN (pRtAttr->rta_len) + i4AlignLen > i4MaxLen)
    {
        return NETIPV4_FAILURE;
    }

    pRouteAttr = (struct rtattr *) (VOID *)
        (((char *) pRtAttr) + RTA_ALIGN (pRtAttr->rta_len));
    pRouteAttr->rta_type = i4Type;
    pRouteAttr->rta_len = i4AlignLen;
    MEMCPY (RTA_DATA (pRouteAttr), pData, i4Len);
    pRtAttr->rta_len = NLMSG_ALIGN (pRtAttr->rta_len) + i4AlignLen;

    return NETIPV4_SUCCESS;
}

VOID
RtmNLParseRouteAttr (struct rtattr **ppTable, INT4 i4Max,
                     struct rtattr *pRtAttr, INT4 i4Len)
{
    while (RTA_OK (pRtAttr, i4Len))
    {
        if (pRtAttr->rta_type <= i4Max)
            ppTable[pRtAttr->rta_type] = pRtAttr;

        pRtAttr = ((i4Len) -= RTA_ALIGN ((pRtAttr)->rta_len),
                   (struct rtattr *) (VOID *) (((char *) (pRtAttr)) +
                                               RTA_ALIGN ((pRtAttr)->rta_len)));
    }
}


/*-------------------------------------------------------------------+
 *
 * Function           : LnxDeInit
 *
 * Description        : This function will delete the Semaphore and Queue.created.
 *
 * Input(s)           : VRFID
 *
 * Output(s)          : NONE
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 * *********************************************************************/
VOID
LnxDeInit()
{
    if(gLnxTapIpEvntMemPoolId != ZERO)
    {
        MemDeleteMemPool (gLnxTapIpEvntMemPoolId);
    }
    if(gLnxTapSemId != NULL)
    {
        OsixSemDel(gLnxTapSemId);
    }
    if(gLnxFromTapIpQId != NULL)
    {
        OsixQueDel (gLnxFromTapIpQId);
    }
    if(gLnxToTapIpQId != NULL)
    {
        OsixQueDel (gLnxToTapIpQId);
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if(gLnxVrfMsgMemPoolId != ZERO)
    {
        MemDeleteMemPool (gLnxVrfMsgMemPoolId);
    }
    if(gLnxVrfVcInfoMemPoolId != ZERO)
    {
        MemDeleteMemPool (gLnxVrfVcInfoMemPoolId);
    }
    if(gLnxVrfIfInfoMemPoolId != ZERO)
    {
        MemDeleteMemPool(gLnxVrfIfInfoMemPoolId);
    }
    if(gLnxEvtSemId != NULL)
    {
        OsixSemDel (gLnxEvtSemId);
    }
    if(gLnxVrfGlobSemId != NULL)
    {
	OsixSemDel (gLnxVrfGlobSemId);
    }
    if(gLnxVrfInfoRBRoot != NULL)
    {
        RBTreeDelete(gLnxVrfInfoRBRoot);
    }
    if(gLnxVrfIfInfoRBRoot != NULL)
    {
        RBTreeDelete(gLnxVrfIfInfoRBRoot);
    }
#endif
    if(gProxyArpStatusPoolId != ZERO)
    {
        MemDeleteMemPool(gProxyArpStatusPoolId);
    }

    return;
}

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfNLSockInit
 *
 * Description        : This function will initialize the netlink socket
 *                      structure of all vrf's
 *
 * Input(s)           : VRFID
 *
 * Output(s)          : NONE
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 * *********************************************************************/
VOID
LnxVrfNLSockInit()
{
    INT4    i4Index = 1;
    for(i4Index = 0;i4Index < SYS_DEF_MAX_NUM_CONTEXTS; i4Index++)
    {
        gLnxVrfNLWrite[i4Index].i4Sock = -1;
    }
}
#endif

/****************************************************************************
 Function    : LnxIpSetGratuitousArp

 Description : This function is used to enable/disable Gratuitous ARP
               message processing. 

 Input       : i4SetVal - Status to be set

 OutPut      : None

 Returns     : None
****************************************************************************/
VOID
LnxIpSetGratuitousArp (INT4 i4SetVal)
{
    CHR1                ac1Command[LIP4_LINE_LEN];
    INT4                i4Status = LNX_GRATUITOUS_ARP_ENABLE;

    if (i4SetVal == LNX_GRATUITOUS_ARP_ENABLE)
    {
        i4Status = LNX_GRATUITOUS_ARP_ENABLE;
    }
    else if (i4SetVal == LNX_GRATUITOUS_ARP_DISABLE)
    {
        i4Status = LNX_GRATUITOUS_ARP_DISABLE;
    }

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "echo %d > /proc/sys/net/ipv4/conf/all/arp_accept",
              (INT2) i4Status);
    system (ac1Command);
}

/****************************************************************************
 Function    : LnxIpSetReversePathFilter

 Description : Enable/Disable Reverse Path Filtering

 Input       : u1Status - Status to be set

 OutPut      : None

 Returns     : IP_SUCCESS/IP_FAILURE
****************************************************************************/
INT4
LnxIpSetReversePathFilter (UINT1 *pu1DevName, INT4 i4SetVal)
{
    CHR1  ac1Command[LIP4_LINE_LEN];

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command), "echo %d > /proc/sys/net/ipv4/conf/%s/rp_filter",
              (INT2) i4SetVal, pu1DevName);
    system (ac1Command);

    return IP_SUCCESS;
}


#endif /* _LNXIP_C */
