/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: netdev.c,v 1.4 2013/12/07 11:06:02 siva Exp $
 *
 * Description: This file contains routines for..
 *
 *              a) ISS NetDevice Module Initialization and Deinitialization
 *              b) Managing the Character Device that exists b/w ISS & ISS 
 *                 Netdevice Module
 *              c) Managing the Logical L3 Network Devices [that includes
 *                 VLAN or Router Port]
 *
 ***************************************************************************/
#ifndef _NET_DEV_C_
#define _NET_DEV_C_

#include "kerninc.h"
#include "oskern.h"
#include "chrdev.h"
#include "mux.h"
#include "netdev.h"
#include "iss.h"
#include "netglob.h"

/************************** PRIVATE DECLARATIONS ******************************/

PRIVATE INT4 NetDevCharRegister PROTO ((VOID));
PRIVATE VOID NetDevCharDeRegister PROTO ((VOID));
PRIVATE INT4 NetDeviceCharOpen PROTO ((struct inode * pINode,
                                       struct file * pFile));
PRIVATE INT4 NetDeviceCharRelease PROTO ((struct inode * pINode,
                                          struct file * pFile));
PRIVATE INT4 NetDeviceCharIoctl PROTO ((struct inode * pINode,
                                        struct file * pFile,
                                        UINT4 u4IoctlNum,
                                        FS_ULONG u4IoctlParam));
/* Character device definition for ISS <-> ISS Network Device Module */

static struct file_operations FileOps = {
  owner:THIS_MODULE,
  open:NetDeviceCharOpen,
  release:NetDeviceCharRelease,
  ioctl:NetDeviceCharIoctl,
};

/*********************** END OF PRIVATE DECLARATIONS **************************/

/* ------------ ISS <-> ISS NetDevice Character Device Interfaces ----------- */

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceCharOpen  
 *                                                                          
 *    DESCRIPTION      : Function to Open the Char device. 
 *
 *    INPUT            : pINode - Pointer to the INode Structure 
 *                       pFile  - Pointer to the File Structure 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for Success and -1 for failure 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
NetDeviceCharOpen (struct inode *pINode, struct file *pFile)
{
    UNUSED_PARAM (pINode);
    UNUSED_PARAM (pFile);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    KERN_INC_MOD_USAGE_COUNT ();

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceCharRelease
 *                                                                          
 *    DESCRIPTION      : Function to Release (close) the Char device. 
 *
 *    INPUT            : pINode - Pointer to the INode Structure 
 *                       pFile  - Pointer to the File Structure 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for Success and -1 for failure 
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
NetDeviceCharRelease (struct inode *pINode, struct file *pFile)
{
    UNUSED_PARAM (pINode);
    UNUSED_PARAM (pFile);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    KERN_DEC_MOD_USAGE_COUNT ();

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceCharIoctl 
 *                                                                          
 *    DESCRIPTION      : I/O Control Function for the ISS Netdevice <-> ISS 
 *                       character device that hndles ioctl () calls from ISS
 *                       to create/delete network devices (Logical L3
 *                       Interfaces).
 *
 *    INPUT            : pINode       - Pointer to the INode Structure 
 *                       pFile        - Pointer to the File Structure 
 *                       u4IoctlNum   - Ioctl Number              
 *                       u4IoctlParam - Arguments for the IOCTL    
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 in case of Success else error codes like -EFAULT
 *                       (copy_from_user () or copy_to_user () fails) , 
 *                       -ENOMEM (memory allocation failure during Network
 *                       Device creation) and -ENODEV if no such device exists
 *                       while deleting an interface
 *                                                                          
 ****************************************************************************/

PRIVATE INT4
NetDeviceCharIoctl (struct inode *pINode, struct file *pFile, UINT4 u4IoctlNum,
                    FS_ULONG u4IoctlParam)
{
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pINode);
    UNUSED_PARAM (pFile);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    switch (u4IoctlNum)
    {
        case NET_DEV_INTF_IOCTL:

            i4RetVal = NetDeviceIoctl (u4IoctlParam);
            break;

        default:
            /* Operation Not Supported */
            i4RetVal = -EOPNOTSUPP;
            NETDEV_TRC (ALL_FAILURE_TRC,
                        "NetDeviceCharIoctl : Invalid Ioctl Request \n");
            break;

    }

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return i4RetVal;
}

/* ----------------- Network Device Management Interfaces ------------------- */

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceOpen
 *                                                                          
 *    DESCRIPTION      : Enables the specified Net device
 *
 *    INPUT            : pDev         - Pointer to the net device     
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : 0 for success by default. -1 has to be returned for 
 *                       failure cases (in case if some updation is done in 
 *                       future)
 *
 ****************************************************************************/
INT4
NetDeviceOpen (tNetDevice * pDev)
{
    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Enables the specified Net device so that Device can be made ready to 
     * Transmit packets */
    netif_start_queue (pDev);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceRelease
 *                                                                          
 *    DESCRIPTION      : Disables the specified Net device
 *
 *    INPUT            : pDev         - Pointer to the net device     
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : 0 for success by default. -1 has to be returned for 
 *                       failure cases (in case if some updation is done in 
 *                       future)
 *                                                                          
 ****************************************************************************/
INT4
NetDeviceRelease (tNetDevice * pDev)
{
    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Disables the Transmission capability of the specified Device */
    netif_stop_queue (pDev);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceIoctl
 *                                                                          
 *    DESCRIPTION      : API to handle ioctl () request from ISS to
 *                       Create/Delete Logical L3 Interfaces
 *
 *    INPUT            : u4IoctlParam - ioctl Parameters  
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : 0 in case of Success else error codes like -EFAULT
 *                       (copy_from_user () or copy_to_user () fails) , -ENOMEM 
 *                       (memory allocation failure during Net Device creation)
 *                       and -ENODEV if no such device exists while deleting an
 *                       interface
 *                                                                          
 ****************************************************************************/

INT4
NetDeviceIoctl (FS_ULONG u4IoctlParam)
{
    tLnxIpIntfParams    LnxIpIntfParams;
    INT4                i4RetVal = 0;

    MEMSET (&LnxIpIntfParams, 0, sizeof (tLnxIpIntfParams));

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    if (KERN_COPY_FROM_USER (&LnxIpIntfParams,
                             (tLnxIpIntfParams *) u4IoctlParam,
                             sizeof (tLnxIpIntfParams)))
    {
        NETDEV_TRC (ALL_FAILURE_TRC,
                    "NetDeviceIoctl : Failed to get Interface Params \n");
        return -EFAULT;
    }

    switch (LnxIpIntfParams.u4Action)
    {
        case GDD_INTERFACE_CREATE:

            i4RetVal = NetDeviceCreate (&LnxIpIntfParams);
            break;

        case GDD_INTERFACE_DELETE:

            i4RetVal = NetDeviceDelete (LnxIpIntfParams.pu1IntfName);
            break;
        default:
            /* Operation Not Supported */
            i4RetVal = -EOPNOTSUPP;
            NETDEV_TRC (ALL_FAILURE_TRC,
                        "NetDeviceIoctl : Invalid Ioctl Request \n");
            break;

    }

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDevInterfaceTxmit
 *                                                                          
 *    DESCRIPTION      : Callback function to handle packets transmitted over   
 *                       this device. This function will be registered with the 
 *                       netdevice when it is initialized. 
 *
 *    INPUT            : pSkb         - SKB that is to Transmitted out
 *                       pDev         - Pointer to the net device
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : 0 for success and -1 for failure cases
 *                                                                          
 ****************************************************************************/
INT4
NetDevInterfaceTxmit (tSkb * pSkb, tNetDevice * pDev)
{
    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Transmit the packet to the underlying switch driver by calling up the
     * MUX Module's exported TX routine */
    MuxTxDevPacket (pSkb, pDev);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDevInterfaceInit
 *                                                                          
 *    DESCRIPTION      : Routine to Initialize the specified Net Device with their
 *                       respective Callback routines
 *
 *    INPUT            : pDev         - Pointer to the net device
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : 0 for success by default. -1 has to be returned for 
 *                       failure cases (in case if some updation is done in 
 *                       future)
 *                                                                          
 ****************************************************************************/
INT4
NetDevInterfaceInit (tNetDevice * pDev)
{
    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pDev->open = NetDeviceOpen;
    pDev->stop = NetDeviceRelease;
    pDev->hard_start_xmit = NetDevInterfaceTxmit;
    pDev->get_stats = NetDeviceGetStats;

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceCreate
 *                                                                          
 *    DESCRIPTION      : Creates a Net Device for the Specified L3 Interface
 *
 *    INPUT            : pLnxIpIntfParams  - Pointer to Interface Parameters
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : 0 for success and appropriate error code in case of
 *                       failures like net device Interface's memory allocation
 *                       failure (-ENOMEM) or net device registration failure
 *                                                                          
 ****************************************************************************/
INT4
NetDeviceCreate (tLnxIpIntfParams * pLnxIpIntfParams)
{
    tNetDevice         *pNetDevice = NULL;
    INT4                i4RetVal = 0;
    tNetDevPrivData    *pDevPrvData = NULL;

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Allocate Net Device Structure along with the Private Data
     * that is to be maintained for the Devices that we create from ISS */
    if ((pNetDevice = KERN_ALLOC_NETDEV (sizeof (tNetDevPrivData),
                                         (CONST CHR1 *)
                                         pLnxIpIntfParams->pu1IntfName,
                                         KERN_NETDEV_SETUP_FPTR)) == NULL)
    {

        NETDEV_TRC (OS_RESOURCE_TRC,
                    " NetDeviceCreate : Error while Allocating"
                    "Net Device Interface");
        return -ENOMEM;
    }

    MEMCPY (pNetDevice->dev_addr, pLnxIpIntfParams->InterfaceSet.au1MacAddress,
            MAC_ADDR_LEN);

    pNetDevice->init = NetDevInterfaceInit;

    /* Register the Allocated Net Device with Linux */
    if ((i4RetVal = KERN_REG_NETDEV (pNetDevice)) != 0)
    {
        NETDEV_TRC (ALL_FAILURE_TRC,
                    " NetDeviceCreate : Error while Registering"
                    "Net Device Interface or Already Present");
        KERN_FREE_NETDEV (pNetDevice);
        return i4RetVal;
    }

    pDevPrvData = KERN_NETDEV_PRIV_DATA (pNetDevice);
    MEMSET (pDevPrvData, 0, sizeof (tNetDevPrivData));

#ifdef VRRP_WANTED
    MuxVrrpInitIntfTable ((UINT2) pNetDevice->ifindex);
#endif /* VRRP_WANTED */

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceDelete
 *                                                                          
 *    DESCRIPTION      : Deletes the Net Device with the Specified Device Name
 *
 *    INPUT            : pu1DevName  - Pointer to the Net Device Name
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : 0 for success and appropriate error code like -ENODEV
 *                       if the device to be deleted does not exist in the
 *                       system
 *                                                                          
 ****************************************************************************/
INT4
NetDeviceDelete (UINT1 *pu1DevName)
{
    tNetDevice         *pNetDevice = NULL;

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    pNetDevice = KERN_DEV_GET_BY_NAME (pu1DevName);

    if (pNetDevice == NULL)
    {
        NETDEV_TRC (ALL_FAILURE_TRC,
                    " NetDeviceDelete : Invalid Interface Name");
        return -ENODEV;
    }

    KERN_DEV_GIVE_SEM (pNetDevice);

    KERN_DEREG_NETDEV (pNetDevice);

    KERN_FREE_NETDEV (pNetDevice);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDeviceGetStats
 *                                                                          
 *    DESCRIPTION      : Function to get the Statistics of a Specified Device
 *
 *    INPUT            : pDev  - Pointer to the Net Device
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : Pointer to tNetDevStats Structure
 *                                                                          
 ****************************************************************************/
tNetDevStats       *
NetDeviceGetStats (tNetDevice * pDev)
{
    tNetDevPrivData    *pDevPriv = KERN_NETDEV_PRIV_DATA (pDev);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);
    return &pDevPriv->NetDevStats;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDevCharRegister
 *                                                                          
 *    DESCRIPTION      : Function to Register ISS Netdevice <-> ISS Char Device
 *                       with Linux and the initialize the associated Queue to
 *                       read/write
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_FAILURE if character device registration fails
 *                       else OSIX_SUCCESS
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
NetDevCharRegister (VOID)
{
    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* Register the ISS Netdevice <->ISS Character Device and the initialize the 
     * associated Queue to read/write */

    if (KERN_REG_CHRDEV (NET_DEVICE_MAJOR_NUMBER, NET_DEVICE_FILE_NAME,
                         &FileOps) < 0)
    {
        NETDEV_TRC (ALL_FAILURE_TRC,
                    " NetDevCharRegister : Registration Failed");
        return OSIX_FAILURE;
    }

    NETDEV_TRC (NETDEV_INFO_TRC, " NetDevCharRegister : ISS <-> ISS Netdevice "
                "Character Device Registration : Success");

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDevCharDeRegister
 *                                                                          
 *    DESCRIPTION      : Function to DeRegister ISS Netdevice <->ISS Char Device 
 *                       from Linux
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
NetDevCharDeRegister (VOID)
{
    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* DeRegister the ISS Netdevice <-> ISS Char Device and release the memory
     * allocated for it */
    KERN_DEREG_CHRDEV (NET_DEVICE_MAJOR_NUMBER, NET_DEVICE_FILE_NAME);

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDevLoadModule
 *                                                                          
 *    DESCRIPTION      : Function to load ISS Netdevice Module into Kernel
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 0 for success -1 for failure
 *                                                                          
 ****************************************************************************/
INT4
NetDevLoadModule (VOID)
{
    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    gu4NetDevTrace = 0xffffffff;

    /* Register the Interfaces with User Space and does the Default 
     * Initializaitions required for the Netdevice Module */

    if (NetDevCharRegister () == OSIX_FAILURE)
    {
        NETDEV_TRC (ALL_FAILURE_TRC, "\r\n Registering ISS Netdevice "
                    "<-> ISS Character Device : Failed");
        return -1;
    }

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return 0;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : NetDevUnloadModule
 *                                                                          
 *    DESCRIPTION      : Function to unload ISS Netdevice Module from Kernel
 *                       This cleans the Character device created while loading
 *                       the module and deletes the Logical interface created
 *                       by ISS
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
NetDevUnloadModule (VOID)
{
    tNetDevice         *pNetDev = NULL;
    tNetDevice         *pNextNetDev = NULL;
    UINT1              *pu1DevName = NULL;

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Entering %s \n", __FUNCTION__);

    /* De-Register the Interfaces with User Space and revert the Default 
     * Initializaitions done for the ISS Netdevice Module */
    NetDevCharDeRegister ();

    /* Scan through the Interfaces available in the system and check 
     * whether they are created by ISS [L3 (VLAN/RPORT)].
     * If so, deregister them from the system & clean the memory associated 
     * with it */

    DEV_LIST_SCAN (pNetDev, pNextNetDev)
    {
        pu1DevName = (UINT1 *) pNetDev->name;
        if ((STRNCMP (pu1DevName, CFA_STACK_VLAN_INTERFACE,
                      STRLEN (CFA_STACK_VLAN_INTERFACE)) == 0) ||
            (STRNCMP (pu1DevName, ISS_RPORT_ALIAS_PREFIX,
                      STRLEN (ISS_RPORT_ALIAS_PREFIX)) == 0))
        {
            /* Free up the Interfaces created by us */
            NetDeviceDelete (pu1DevName);
        }
    }

    NETDEV_TRC_ARG1 (NETDEV_DEBUG_TRC, " Exiting %s \n", __FUNCTION__);

    return;
}

/* Define the Init/Deinit Fn's to be called when this module gets loaded to 
 * the Kernel */
module_init (NetDevLoadModule);

module_exit (NetDevUnloadModule);

MODULE_LICENSE ("Aricent");
#endif /* _NET_DEV_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  netdev.c                       */
/*-----------------------------------------------------------------------*/
