/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: netipmc.c,v 1.17 2015/03/08 11:04:29 siva Exp $
 *
 * Description:This file holds the APIs for IPv4 multicast routing 
 *             support with LinuxIp.
 *
 *******************************************************************/

#include "netipmc.h"

#if (__GLIBC__ >= 2 && __GLIBC_MINOR__ >= 9)
#include "fssocket.h"
/* Reset the option added from pack.h (#pragma pack(1)) to avoid
 * strcture misalignment issues */
#pragma pack()
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <linux/ip.h>
#include <linux/in.h>
/* Reset the option added from pack.h (#pragma pack(1)) to avoid
 *  * strcture misalignment issues */
#pragma pack()
#include <linux/mroute.h>
#endif

PRIVATE UINT1       gau1VifId[MAXVIFS];
PRIVATE UINT4       gau4IpPort[MAXVIFS];

PRIVATE INT4        LnxIpMcastInit (VOID);
PRIVATE VOID        LnxIpMcastDeInit (VOID);
PRIVATE INT4        NetIpv4EnableMcastOnIface (tNetIpMcastInfo *
                                               pNetIpMcastInfo);
PRIVATE INT4        NetIpv4DisableMcastOnIface (tNetIpMcastInfo *
                                                pNetIpMcastInfo);
PRIVATE INT4        NetIpv4FindFreeVifId (UINT2 *pu2VifId);
PRIVATE INT4        NetIpv4McastAddRoute (tNetIpMcRouteInfo * pIpMcastRoute);
PRIVATE INT4        NetIpv4McastDelRoute (tNetIpMcRouteInfo * pIpMcastRoute);
PRIVATE VOID        NetIpMcHandlePacket (VOID);
PRIVATE VOID        NetIpMcPostPkt (tCRU_BUF_CHAIN_HEADER * pBuf);
PRIVATE INT4        NetIpv4AddCpuPort (tNetIpMcRouteInfo * pIpMcastRoute);

/****************************************************************************
 *
 *    FUNCTION NAME    : LnxIpMcastTaskMain
 *
 *    DESCRIPTION      : receive unresolved multicast data packets at the socket
 *                       and hand over to multicast protocol.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LnxIpMcastTaskMain (VOID)
{
    if (LnxIpMcastInit () == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return (OSIX_FAILURE);
    }

    NetIpv4McastUpdateCpuPortStatus (ENABLED);

    lrInitComplete (OSIX_SUCCESS);

    /* Receive multicast packets from the IGMP socket */
    while (1)
    {
        NetIpMcHandlePacket ();
    }

    LnxIpMcastDeInit ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *    FUNCTION NAME    : NetIpv4RegisterMcPacket 
 *
 *    DESCRIPTION      : API to register for receiving Multicast control
 *                       or/and data packet from higher layer modules
 *
 *    INPUT            : pNetIpMcRegInfo  - Pointer having Multicast
 *                           registration info
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 ****************************************************************************/
INT4
NetIpv4RegisterMcPacket (tNetIpMcRegInfo * pNetIpMcRegInfo)
{
    UINT4               u4LoopIndex = 0;

    if ((pNetIpMcRegInfo->u4ProtocolId == 0) ||
        (pNetIpMcRegInfo->pControlPktRcv == NULL &&
         pNetIpMcRegInfo->pDataPktRcv == NULL))
    {
        /* Invalid registration param */
        return NETIPV4_FAILURE;
    }

    NETIPMC_MUTEX_LOCK ();
    for (u4LoopIndex = 0; u4LoopIndex < NETIPMC_MAX_PROTOCOLS; u4LoopIndex++)
    {
        if (gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].u4ProtocolId == 0)
        {
            gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].pControlPktRcv =
                pNetIpMcRegInfo->pControlPktRcv;
            gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].pDataPktRcv =
                pNetIpMcRegInfo->pDataPktRcv;
            gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].u4ProtocolId =
                pNetIpMcRegInfo->u4ProtocolId;
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_SUCCESS;
        }

        if (gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].u4ProtocolId ==
            pNetIpMcRegInfo->u4ProtocolId)
        {
            /* Already registered */
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
    }

    /* No free entries */
    NETIPMC_MUTEX_UNLOCK ();
    return NETIPV4_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4DeRegisterMcPacket 
 *
 *    DESCRIPTION      : Routine to deregister for receiving Multicast control
 *                       or/and data packet from higher layer modules
 *
 *    INPUT            : u4ProtocolId - Protocol id to register
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4DeRegisterMcPacket (UINT4 u4ProtocolId)
{
    UINT4               u4LoopIndex = 0;

    NETIPMC_MUTEX_LOCK ();
    for (u4LoopIndex = 0; u4LoopIndex < NETIPMC_MAX_PROTOCOLS; u4LoopIndex++)
    {
        if (gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].u4ProtocolId ==
            u4ProtocolId)
        {
            gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].pControlPktRcv =
                NULL;
            gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].pDataPktRcv = NULL;
            gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].u4ProtocolId = 0;
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_SUCCESS;
        }
    }

    /* No registration found */
    NETIPMC_MUTEX_UNLOCK ();
    return NETIPV4_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4SetMcStatusOnPort 
 *
 *    DESCRIPTION      : Enables/Disables multicast routing on an IpPort
 *                       according to u4McastStatus. 
 *
 *    INPUT            : pNetIpMcastInfo: contains the ipport and the 
 *                       corresponding multicast routing information 
 *                       u4McastStatus : ENABLED/DISABLED
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4SetMcStatusOnPort (tNetIpMcastInfo * pNetIpMcastInfo,
                          UINT4 u4McastStatus)
{
    if (pNetIpMcastInfo == NULL)
    {
        return NETIPV4_FAILURE;
    }
    if (u4McastStatus == ENABLED)
    {
        if (NetIpv4EnableMcastOnIface (pNetIpMcastInfo) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }
    else if (u4McastStatus == DISABLED)
    {
        if (NetIpv4DisableMcastOnIface (pNetIpMcastInfo) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }
    else
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :NetIpv4McastRouteUpdate 
 *
 *    DESCRIPTION      :Adds/deletes multicast route as per u4RouteFlag.  
 *
 *    INPUT            : pIpMcastRoute: Route information
 *                      u4RouteFlag : NETIPV4_ADD_ROUTE/NETIPV4_DELETE_ROUTE
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McastRouteUpdate (tNetIpMcRouteInfo * pIpMcastRoute, UINT4 u4RouteFlag)
{
    if (pIpMcastRoute == NULL)
    {
        return NETIPV4_FAILURE;
    }
    if (u4RouteFlag == NETIPV4_ADD_ROUTE)
    {
        if (NetIpv4McastAddRoute (pIpMcastRoute) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }
    else if (u4RouteFlag == NETIPV4_DELETE_ROUTE)
    {
        if (NetIpv4McastDelRoute (pIpMcastRoute) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }
    else
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    :NetIpv4McastUpdateCpuPortStatus
 *
 *    DESCRIPTION      : Creates/Deletes CPU Port into/from LinuxIp when PIM 
 *                       is enabled/disabled, as per the value of 
 *                       u4CpuPortStatus.
 *
 *    INPUT            : u4CpuPortStatus: ENABLED - Create  Cpu Port in LnxIp,
 *                                  i.e.,Create  a VIF in LnxIp  for CpuPort.
 *                                        DISABLED -Delete CpuPort from LnxIp,
 *                                  i.e.,Delete the VIF for CPUPort from LnxIp.  
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McastUpdateCpuPortStatus (UINT4 u4CpuPortStatus)
{
    struct vifctl       VifEntry;
    vifi_t              vifi = NETIPMC_CPUPORT_VIFID;
    INT4                i4RetStatus = NETIPV4_SUCCESS;

    MEMSET (&VifEntry, 0, sizeof (struct vifctl));

    if ((u4CpuPortStatus != ENABLED) && (u4CpuPortStatus != DISABLED))
    {
        /* invalid value of u4CpuPortStatus */
        return NETIPV4_FAILURE;
    }

    NETIPMC_MUTEX_LOCK ();

    VifEntry.vifc_vifi = vifi;

    if (u4CpuPortStatus == ENABLED)
    {
        VifEntry.vifc_flags = VIFF_REGISTER;
        VifEntry.vifc_threshold = NETIPMC_MAX_THRESHOLD;
        VifEntry.vifc_rate_limit = 0;

        /* Add the VIF for CPUPort  to LinuxIp */
        if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                        MRT_ADD_VIF, &VifEntry, sizeof (struct vifctl)) != 0)
        {
            perror ("NetIpv4EnableMcastOnIface-MRT_ADD_VIF Failed");
            i4RetStatus = NETIPV4_FAILURE;
        }
    }
    else                        /* u4CpuPortStatus == DISABLED */
    {
        /* Delete the VIF for CPUPort from LinuxIp. */
        if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                        MRT_DEL_VIF, &VifEntry, sizeof (struct vifctl)) != 0)
        {
            perror ("NetIpv4DisableMcastOnIface-MRT_DEL_VIF Failed:");
            i4RetStatus = NETIPV4_FAILURE;
        }
    }

    NETIPMC_MUTEX_UNLOCK ();
    return i4RetStatus;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4McastUpdateRouteCpuPort  
 *
 *    DESCRIPTION      : Adds/Deletes CPU Port into/from LinuxIp for the 
 *                       i/p routeentry as per the value of u4CpuPortStatus. 
 *
 *    INPUT            : pIpMcastRoute : Multicast route info as follows:
 *                                        (u4SrcAddr: Source Address
 *                                         u4GrpAddr: Multicast Group Address
 *                                         u4Iif    : Incoming Interface
 *                                         u4OifCnt : No.of outgoing i/fs
 *                                         *pOIf    : OifList )
 *                       u4CpuPortStatus: ENABLED - Add Cpu Port to 
 *                                                  multicast route in LnxIp,
 *                                        DISABLED -Delete CpuPort from 
 *                                                  multicast route in LnxIp,
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McastUpdateRouteCpuPort (tNetIpMcRouteInfo * pIpMcastRoute,
                                UINT4 u4CpuPortStatus)
{
    if (pIpMcastRoute == NULL)
    {
        return NETIPV4_FAILURE;
    }
    if (u4CpuPortStatus == ENABLED)
    {
        if (NetIpv4AddCpuPort (pIpMcastRoute) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }
    else if (u4CpuPortStatus == DISABLED)
    {
        /* In LinuxIp, any change in route needs addition of whole
         * route information to ip_mr_cache. Therefore, deleting the CPUPort
         * means adding the route again to LinuxIp, which automatically will
         * delete the cpuport.
         */
        if (NetIpv4McastAddRoute (pIpMcastRoute) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }
    else
    {
        /* invalid value of u4CpuPortStatus */
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LnxIpMcastInit 
 *
 *    DESCRIPTION      : Initialises multicast routing in linuxip.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
LnxIpMcastInit (VOID)
{
    int                 u1OptVal = 1;
    /* create semaphore */
    if (OsixSemCrt (NETIPMC_MUTEX_SEMNAME,
                    &(gNetIpMcGlobalInfo.NetIpSemId)) == OSIX_FAILURE)
    {
        perror ("LnxIpMcastInit-Semaphore creation failed.");
        OsixSemDel (gNetIpMcGlobalInfo.NetIpSemId);
        return OSIX_FAILURE;
    }
    OsixSemGive (gNetIpMcGlobalInfo.NetIpSemId);

    /* create socket */
    if ((gNetIpMcGlobalInfo.i4NetIpSockId =
         socket (AF_INET, SOCK_RAW, IPPROTO_IGMP)) < 0)
    {
        perror ("LnxIpMcastInit-socket creation failed.");
        OsixSemDel (gNetIpMcGlobalInfo.NetIpSemId);
        return OSIX_FAILURE;
    }

    /* To include packet info */
    if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP, IP_PKTINFO,
                    (char *) &u1OptVal, sizeof (u1OptVal)) != 0)
    {
        perror ("LnxIpMcastInit-PKT_INFO failed");
        OsixSemDel (gNetIpMcGlobalInfo.NetIpSemId);
        return OSIX_FAILURE;
    }

    /* set socket option as MRT_INIT, initalize mrouting */
    if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP, MRT_INIT,
                    (char *) &u1OptVal, sizeof (u1OptVal)) != 0)
    {
        perror ("LnxIpMcastInit-MRT_INIT failed");
        OsixSemDel (gNetIpMcGlobalInfo.NetIpSemId);
        return OSIX_FAILURE;
    }

    /* set socket option as MRT_ASSERT */
    if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP, MRT_ASSERT,
                    (char *) &u1OptVal, sizeof (u1OptVal)) != 0)
    {
        perror ("LnxIpMcastInit-MRT_ASSERT failed");
        OsixSemDel (gNetIpMcGlobalInfo.NetIpSemId);
        return OSIX_FAILURE;
    }

    /* set socket option as MRT_PIM */
    if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP, MRT_PIM,
                    (char *) &u1OptVal, sizeof (u1OptVal)) != 0)
    {
        perror ("LnxIpMcastInit-MRT_PIM failed");
        OsixSemDel (gNetIpMcGlobalInfo.NetIpSemId);
        return OSIX_FAILURE;
    }

    /* initialize  gau1VifId */
    MEMSET (gau1VifId, 0, sizeof (gau1VifId));
    MEMSET (gau4IpPort, 0, sizeof (gau4IpPort));
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LnxIpMcastDeInit
 *
 *    DESCRIPTION      : Deinitialises multicast routing in linuxip.
 *                       and deletes the semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
LnxIpMcastDeInit (VOID)
{
    NETIPMC_MUTEX_LOCK ();
    close (gNetIpMcGlobalInfo.i4NetIpSockId);
    gNetIpMcGlobalInfo.i4NetIpSockId = -1;
    NETIPMC_MUTEX_UNLOCK ();

    OsixSemDel (gNetIpMcGlobalInfo.NetIpSemId);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4EnableMcastOnIface
 *
 *    DESCRIPTION      : Enables multicasting on a Vif and adds to ip_mr_vif
 *
 *    INPUT            : pNetIpMcastInfo: contains the port number on which
 *                                multicasting is enabled.
 *                                and the multicast protocol(:PIM/DVMRP)
 *                                to be enabled on the i/f
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
NetIpv4EnableMcastOnIface (tNetIpMcastInfo * pNetIpMcastInfo)
{
    struct vifctl       VifEntry;
    struct ip_mreq      mreq;
    tNetIpv4IfInfo      NetIpIfInfo;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    UINT4               u4IpPort = 0;
    UINT2               u2FreeVifId = 0;
    UINT1               u1McastProtocol = 0;

    MEMSET (&VifEntry, 0, sizeof (struct vifctl));
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&mreq, 0, sizeof (mreq));

    if (pNetIpMcastInfo == NULL)
    {
        return NETIPV4_FAILURE;
    }
    u4IpPort = pNetIpMcastInfo->u4IpPort;
    u1McastProtocol = pNetIpMcastInfo->u1McastProtocol;
    /* get the PortMapNode */
    pPortMapNode = LnxIpGetPortMapNode (u4IpPort);
    if (pPortMapNode == NULL)
    {
        return (NETIPV4_FAILURE);
    }

    if ((u1McastProtocol != PIM_ID) &&
        (u1McastProtocol != DVMRP_ID) && (u1McastProtocol != IPPROTO_IGMP))
    {
        return NETIPV4_FAILURE;
    }

    /* Find the IpAddress corresponding to u4IpPort. */
    if (NetIpv4GetIfInfo (u4IpPort, &NetIpIfInfo) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    /* If the protocol is igmp join in the all IGMP routers group.
     * Other wise we will receive IGMPv3 packets */
    NETIPMC_MUTEX_LOCK ();
    if (u1McastProtocol == IPPROTO_IGMP)
    {
        mreq.imr_multiaddr.s_addr = OSIX_HTONL (IGMP_ALL_V3_ROUTERS_GROUP);
        mreq.imr_interface.s_addr = OSIX_HTONL (NetIpIfInfo.u4Addr);

        if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                        IP_ADD_MEMBERSHIP, (VOID *) &mreq, sizeof (mreq)) < 0)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
        mreq.imr_multiaddr.s_addr = OSIX_HTONL (IGMP_ALL_V2_ROUTERS_GROUP);
        mreq.imr_interface.s_addr = OSIX_HTONL (NetIpIfInfo.u4Addr);

        if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                        IP_ADD_MEMBERSHIP, (VOID *) &mreq, sizeof (mreq)) < 0)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
    }

    if (pPortMapNode->u1McastProtocol == 0)
    {
        /* find a free vifid.scan thru gau1VifId */
        if (NetIpv4FindFreeVifId (&u2FreeVifId) == NETIPV4_SUCCESS)
        {
            /* VifId MAXVIFS - 1 is reserved for PIM Cpu Port */
            if (u2FreeVifId >= (MAXVIFS - 1))
            {
                NETIPMC_MUTEX_UNLOCK ();
                return NETIPV4_FAILURE;
            }

            pPortMapNode->u2VifId = u2FreeVifId;

            gau1VifId[u2FreeVifId] = 1;
            gau4IpPort[u2FreeVifId] = u4IpPort;
            /* Add the vif to ip_mr_vif */
            MEMSET (&VifEntry, 0, sizeof (struct vifctl));
            VifEntry.vifc_vifi = pPortMapNode->u2VifId;
            VifEntry.vifc_flags = 0;
            VifEntry.vifc_threshold = NETIPMC_MAX_THRESHOLD;
            VifEntry.vifc_rate_limit = 0;
            VifEntry.vifc_lcl_addr.s_addr = OSIX_HTONL (NetIpIfInfo.u4Addr);
            VifEntry.vifc_rmt_addr.s_addr = 0;

            if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                            MRT_ADD_VIF, &VifEntry,
                            sizeof (struct vifctl)) != 0)
            {
                perror ("NetIpv4EnableMcastOnIface-MRT_ADD_VIF Failed");
                NETIPMC_MUTEX_UNLOCK ();
                return NETIPV4_FAILURE;
            }
        }
        else
        {
            perror ("NetIpv4EnableMcastOnIface-No Free Vif present");
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }

    }
    NETIPMC_MUTEX_UNLOCK ();

    /* Update pPortMapNode with  u1McastProtocol */
    if (u1McastProtocol == PIM_ID)
    {
        pPortMapNode->u1McastProtocol |= NETIPMC_PIM_ID;
    }
    else if (u1McastProtocol == DVMRP_ID)
    {
        pPortMapNode->u1McastProtocol |= NETIPMC_DVMRP_ID;
    }
    else
    {
        pPortMapNode->u1McastProtocol |= NETIPMC_IGMP_ID;
    }

    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4DisableMcastOnIface
 *
 *    DESCRIPTION      : Disables multicasting from a Vif and removes 
 *                       from ip_mr_vif
 *
 *    INPUT            : pNetIpMcastInfo: contains the port number on which
 *                                multicasting is disabled,
 *                                and the multicast protocol(:PIM/DVMRP)
 *                                to be disabled on the i/f
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE 
 *
 ****************************************************************************/
PRIVATE INT4
NetIpv4DisableMcastOnIface (tNetIpMcastInfo * pNetIpMcastInfo)
{
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;
    struct vifctl       VifEntry;
    struct ip_mreq      mreq;
    UINT4               u4IpPort = 0;
    UINT1               u1McastProtocol = 0;
    UINT1               u1RcvdMcastProtocol = 0;

    MEMSET (&VifEntry, 0, sizeof (struct vifctl));
    MEMSET (&mreq, 0, sizeof (mreq));

    if (pNetIpMcastInfo == NULL)
    {
        return NETIPV4_FAILURE;
    }
    u4IpPort = pNetIpMcastInfo->u4IpPort;
    u1McastProtocol = pNetIpMcastInfo->u1McastProtocol;

    /* get the PortMapNode */
    pPortMapNode = LnxIpGetPortMapNode (u4IpPort);
    if (pPortMapNode == NULL)
    {
        return (NETIPV4_FAILURE);
    }

    if (u1McastProtocol == PIM_ID)
    {
        u1RcvdMcastProtocol = NETIPMC_PIM_ID;
    }
    else if (u1McastProtocol == DVMRP_ID)
    {
        u1RcvdMcastProtocol = NETIPMC_DVMRP_ID;
    }
    else if (u1McastProtocol == IPPROTO_IGMP)
    {
        u1RcvdMcastProtocol = NETIPMC_IGMP_ID;
    }
    else
    {
        /* Invalid McastProtocol */
        return NETIPV4_FAILURE;
    }
    if (!(pPortMapNode->u1McastProtocol & u1RcvdMcastProtocol))
    {
        return NETIPV4_SUCCESS;
    }
    pPortMapNode->u1McastProtocol &= ~u1RcvdMcastProtocol;

    NETIPMC_MUTEX_LOCK ();

    if (u1McastProtocol == IPPROTO_IGMP)
    {
        /* Leave the all IGMP reouters group */
        /* Find the IpAddress corresponding to u4IpPort. */
        MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        if (NetIpv4GetIfInfo (u4IpPort, &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }

        mreq.imr_multiaddr.s_addr = OSIX_HTONL (IGMP_ALL_V3_ROUTERS_GROUP);
        mreq.imr_interface.s_addr = OSIX_HTONL (NetIpIfInfo.u4Addr);

        if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                        IP_DROP_MEMBERSHIP, (VOID *) &mreq, sizeof (mreq)) < 0)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }

        mreq.imr_multiaddr.s_addr = OSIX_HTONL (IGMP_ALL_V2_ROUTERS_GROUP);
        mreq.imr_interface.s_addr = OSIX_HTONL (NetIpIfInfo.u4Addr);

        if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                        IP_DROP_MEMBERSHIP, (VOID *) &mreq, sizeof (mreq)) < 0)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
    }

    /* if u1McastProtocol = 0, delete the vif */
    if (pPortMapNode->u1McastProtocol == 0)
    {

        MEMSET (&VifEntry, 0, sizeof (struct vifctl));
        VifEntry.vifc_vifi = pPortMapNode->u2VifId;

        /* VifId MAXVIFS - 1 is reserved for PIM Cpu Port */
        if (pPortMapNode->u2VifId >= (MAXVIFS - 1))
        {
            /* invalid Vif */
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
        gau1VifId[pPortMapNode->u2VifId] = 0;
        gau4IpPort[pPortMapNode->u2VifId] = 0;
        if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                        MRT_DEL_VIF, &VifEntry, sizeof (struct vifctl)) != 0)
        {
            perror ("NetIpv4DisableMcastOnIface-MRT_DEL_VIF Failed:");
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
    }

    NETIPMC_MUTEX_UNLOCK ();
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4McastAddRoute
 *
 *    DESCRIPTION      : Adds multicast route entry into linuxip.
 *
 *    INPUT            : pIpMcastRoute : Multicast route info as follows: 
 *                                  ( u4SrcAddr: Source Address
 *                                   u4GrpAddr: Multicast Group Address
 *                                   u4Iif    : Incoming Interface
 *                                   u4OifCnt : No. of outgoing interfaces 
 *                                   *pOIf    : OifList )
 *
 *    OUTPUT           : None 
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
NetIpv4McastAddRoute (tNetIpMcRouteInfo * pIpMcastRoute)
{
    struct mfcctl       McEntry;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    tLnxIpPortMapNode  *pPortMapOifNode = NULL;
    tNetIpOif          *pDsIf = NULL;
    tNetIpOif          *pOIf = NULL;
    UINT4               u4OifPort = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4Iif = 0;
    UINT4               u4OifCnt = 0;
    UINT2               u2Index = 0;

    MEMSET (&McEntry, 0, sizeof (struct mfcctl));

    if (pIpMcastRoute == NULL)
    {
        return NETIPV4_FAILURE;
    }
    u4SrcAddr = pIpMcastRoute->u4SrcAddr;
    u4GrpAddr = pIpMcastRoute->u4GrpAddr;
    u4Iif = pIpMcastRoute->u4Iif;
    u4OifCnt = pIpMcastRoute->u4OifCnt;
    pOIf = pIpMcastRoute->pOIf;

    if (u4GrpAddr == 0)
    {
        return NETIPV4_FAILURE;
    }
    NETIPMC_MUTEX_LOCK ();

    McEntry.mfcc_origin.s_addr = OSIX_HTONL (u4SrcAddr);
    McEntry.mfcc_mcastgrp.s_addr = OSIX_HTONL (u4GrpAddr);

    /* get the PortMapNode for the Iif */
    pPortMapNode = LnxIpGetPortMapNode (u4Iif);
    if (pPortMapNode != NULL)
    {
        McEntry.mfcc_parent = pPortMapNode->u2VifId;
    }

    /* update the OifList */
    pDsIf = pOIf;
    for (u2Index = 0; u2Index < (UINT2) u4OifCnt; u2Index++)
    {
        pPortMapOifNode = NULL;
        u4OifPort = 0;
        /*Get IpPort from IfIndex */

        if (NetIpv4GetPortFromIfIndex (pDsIf[u2Index].u4IfIndex, &u4OifPort)
            == NETIPV4_FAILURE)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
        /* Get the VifId */
        pPortMapOifNode = LnxIpGetPortMapNode (u4OifPort);
        if (pPortMapOifNode == NULL)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
        if (pPortMapOifNode->u2VifId >= (MAXVIFS - 1))    /*Cpuport is reserved
                                                         *for VifId MAXVIFS -1*/
        {
            /*invalid Vif */
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
        /* update the Ttl as 1 always */
        McEntry.mfcc_ttls[pPortMapOifNode->u2VifId] = 1;
    }

    /* Add the multicast entry into linuxip. */
    if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP, MRT_ADD_MFC,
                    (VOID *) &McEntry, sizeof (struct mfcctl)) < 0)
    {
        perror ("NetIpv4McastAddRoute- MRT_ADD_MFC Failed.\r\n");
        NETIPMC_MUTEX_UNLOCK ();

        return NETIPV4_FAILURE;
    }
    NETIPMC_MUTEX_UNLOCK ();
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4McastDelRoute
 *
 *    DESCRIPTION      : Deletes multicast route from linuxip. 
 *
 *    INPUT            : pIpMcastRoute : Multicast route info as follows:
 *                                  ( u4SrcAddr: Source Address
 *                                   u4GrpAddr: Multicast Group Address
 *                                   u4Iif    : Incoming Interface )
 *
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
NetIpv4McastDelRoute (tNetIpMcRouteInfo * pIpMcastRoute)
{
    struct mfcctl       McEntry;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4Iif = 0;

    MEMSET (&McEntry, 0, sizeof (struct mfcctl));

    if (pIpMcastRoute == NULL)
    {
        return NETIPV4_FAILURE;
    }

    u4SrcAddr = pIpMcastRoute->u4SrcAddr;
    u4GrpAddr = pIpMcastRoute->u4GrpAddr;
    u4Iif = pIpMcastRoute->u4Iif;
    if (u4GrpAddr == 0)
    {
        return NETIPV4_FAILURE;
    }

    McEntry.mfcc_origin.s_addr = OSIX_HTONL (u4SrcAddr);
    McEntry.mfcc_mcastgrp.s_addr = OSIX_HTONL (u4GrpAddr);

    /* get the PortMapNode for the Iif */
    pPortMapNode = LnxIpGetPortMapNode (u4Iif);
    if (pPortMapNode != NULL)
    {
        McEntry.mfcc_parent = pPortMapNode->u2VifId;
    }

    NETIPMC_MUTEX_LOCK ();
    if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP,
                    MRT_DEL_MFC, (VOID *) &McEntry, sizeof (struct mfcctl)) < 0)
    {
        perror ("NetIpv4McastDelRoute- MRT_DEL_MFC Failed.\r\n");
        NETIPMC_MUTEX_UNLOCK ();
        return NETIPV4_FAILURE;
    }
    NETIPMC_MUTEX_UNLOCK ();

    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4FindFreeVifId
 *
 *    DESCRIPTION      : returns a free VifId
 *
 *    INPUT            : None
 *
 *    OUTPUT           : *pu2VifId
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
NetIpv4FindFreeVifId (UINT2 *pu2VifId)
{
    UINT2               u2Index = 0;
    *pu2VifId = -1;

    for (u2Index = 0; u2Index < (MAXVIFS - 1); u2Index++)
    {
        if (gau1VifId[u2Index] == 0)
        {
            *pu2VifId = u2Index;
            return NETIPV4_SUCCESS;
        }
    }

    return NETIPV4_FAILURE;
}

/****************************************************************************
 *    FUNCTION NAME    : NetIpMcHandlePacket
 *
 *    DESCRIPTION      : This function receives IGMP packet from socket and 
 *                       posts to registered modules
 *
 *    INPUT            : pu1RecvPkt - Pointer to packet buffer
 *                       i4RecvBytes - Received packet length
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 ****************************************************************************/
PRIVATE VOID
NetIpMcHandlePacket (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tIpParms           *pIpParms = NULL;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    struct sockaddr_in  Recv_Node;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct iovec        Iov;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct msghdr       PktInfo;
    struct cmsghdr     *pCmsgHdr = NULL;
    UINT1               au1RecvPkt[CFA_ENET_MTU];
    UINT1               au1Cmsg[NETIP_IGMP_ANCILLARY_LEN];
    UINT1              *pu1RecvPkt = NULL;
    UINT4               u4IpPort;
    INT4                i4RecvBytes;

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&Recv_Node, 0, sizeof (struct sockaddr_in));

    pu1RecvPkt = au1RecvPkt;

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &Recv_Node;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pu1RecvPkt;
    Iov.iov_len = CFA_ENET_MTU;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = IPPROTO_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    i4RecvBytes = 0;

    while ((i4RecvBytes = recvmsg (gNetIpMcGlobalInfo.i4NetIpSockId,
                                   &PktInfo, 0)) > 0)
    {
        pCmsgHdr = NULL;
        pCmsgHdr = CMSG_FIRSTHDR (&PktInfo);
        if (pCmsgHdr == NULL)
        {
            continue;
        }
        pIpPktInfo = (struct in_pktinfo *) (VOID *) CMSG_DATA (pCmsgHdr);
        u4IpPort = (UINT4) pIpPktInfo->ipi_ifindex;

        pPortMapNode = LnxIpGetPortMapNode (u4IpPort);

        if (pPortMapNode == NULL)
        {
            return;
        }

        /* Copy the recvd linear buf to cru buf */
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
        {
            return;
        }

        pIpParms = (tIpParms *) (&pBuf->ModuleData);
        pIpParms->u2Port = (UINT2) u4IpPort;

        CRU_BUF_Copy_OverBufChain (pBuf, pu1RecvPkt, 0, i4RecvBytes);

        /* Post packet to registered modules */
        NetIpMcPostPkt (pBuf);
    }

    return;
}

/****************************************************************************
 *    FUNCTION NAME    : NetIpMcPostPkt
 *
 *    DESCRIPTION      : This function posts the received multicast packets
 *                       to registered functions
 *
 *    INPUT            : pBuf - Pointer to packet buffer
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 ****************************************************************************/
PRIVATE VOID
NetIpMcPostPkt (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    struct igmpmsg     *pIgmpMsg = NULL;
    struct igmpmsg      TmpIgmpMsg;
    UINT4               u4LoopIndex = 0;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;

    pIgmpMsg =
        (struct igmpmsg *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                                  sizeof (struct
                                                                          igmpmsg));
    if (pIgmpMsg == NULL)
    {
        /* The header is not contiguous in the buffer */
        pIgmpMsg = &TmpIgmpMsg;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIgmpMsg, 0,
                                   sizeof (struct igmpmsg));
    }

    /* Post packet to registered modules */
    NETIPMC_MUTEX_LOCK ();

    if (pIgmpMsg->im_mbz == 0)
    {
        if (pIgmpMsg->im_msgtype == IGMPMSG_WHOLEPKT)
        {
            CRU_BUF_Move_ValidOffset (pBuf, sizeof (struct igmpmsg));
        }

        /* Multicast data packet */
        for (u4LoopIndex = 0; u4LoopIndex < NETIPMC_MAX_PROTOCOLS;
             u4LoopIndex++)
        {
            if (gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].pDataPktRcv !=
                NULL)
            {
                /* Dulpicate the buffer and give it to all the registered
                 * protocols */
                pDupBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                if (pDupBuf != NULL)
                {
                    (gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].
                     pDataPktRcv) (pDupBuf);
                }
            }
        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    else
    {
        /* IGMP control packet */
        for (u4LoopIndex = 0; u4LoopIndex < NETIPMC_MAX_PROTOCOLS;
             u4LoopIndex++)
        {
            if (gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].
                pControlPktRcv != NULL)
            {
                pDupBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                if (pDupBuf != NULL)
                {
                    (gNetIpMcGlobalInfo.aMcProtoRegInfo[u4LoopIndex].
                     pControlPktRcv) (pDupBuf);
                }
            }
        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    NETIPMC_MUTEX_UNLOCK ();
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4AddCpuPort 
 *
 *    DESCRIPTION      : Adds CPUPort into LinuxIp for the input route entry 
 *
 *    INPUT            : pIpMcastRoute : Multicast route info as follows:
 *                                        (u4SrcAddr: Source Address
 *                                         u4GrpAddr: Multicast Group Address
 *                                         u4Iif    : Incoming Interface
 *                                         u4OifCnt : No.of outgoing i/fs
 *                                         *pOIf    : OifList )
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
PRIVATE INT4
NetIpv4AddCpuPort (tNetIpMcRouteInfo * pIpMcastRoute)
{
    struct mfcctl       McEntry;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    tLnxIpPortMapNode  *pPortMapOifNode = NULL;
    tNetIpOif          *pDsIf = NULL;
    tNetIpOif          *pOIf = NULL;
    UINT4               u4OifPort = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4Iif = 0;
    UINT4               u4OifCnt = 0;
    UINT2               u2Index = 0;

    MEMSET (&McEntry, 0, sizeof (struct mfcctl));

    if (pIpMcastRoute == NULL)
    {
        return NETIPV4_FAILURE;
    }
    u4SrcAddr = pIpMcastRoute->u4SrcAddr;
    u4GrpAddr = pIpMcastRoute->u4GrpAddr;
    u4Iif = pIpMcastRoute->u4Iif;
    u4OifCnt = pIpMcastRoute->u4OifCnt;
    pOIf = pIpMcastRoute->pOIf;

    if (u4GrpAddr == 0)
    {
        return NETIPV4_FAILURE;
    }
    NETIPMC_MUTEX_LOCK ();

    McEntry.mfcc_origin.s_addr = OSIX_HTONL (u4SrcAddr);
    McEntry.mfcc_mcastgrp.s_addr = OSIX_HTONL (u4GrpAddr);

    /* get the PortMapNode for the Iif */
    pPortMapNode = LnxIpGetPortMapNode (u4Iif);
    if (pPortMapNode != NULL)
    {
        McEntry.mfcc_parent = pPortMapNode->u2VifId;
    }

    /* update the OifList */
    pDsIf = pOIf;
    for (u2Index = 0; u2Index < (UINT2) u4OifCnt; u2Index++)
    {
        pPortMapOifNode = NULL;
        u4OifPort = 0;
        /*Get IpPort from IfIndex */

        if (NetIpv4GetPortFromIfIndex (pDsIf[u2Index].u4IfIndex, &u4OifPort)
            == NETIPV4_FAILURE)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
        /* Get the VifId */
        pPortMapOifNode = LnxIpGetPortMapNode (u4OifPort);
        if (pPortMapOifNode == NULL)
        {
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
        if (pPortMapOifNode->u2VifId >= (MAXVIFS - 1))    /*Cpuport is reserved
                                                         *for VifId MAXVIFS-1*/
        {
            /*invalid Vif */
            NETIPMC_MUTEX_UNLOCK ();
            return NETIPV4_FAILURE;
        }
        /* update the Ttl as 1 always */
        McEntry.mfcc_ttls[pPortMapOifNode->u2VifId] = 1;
    }

    /* Add the CPUPort to the Outgoing interfaces list */
    McEntry.mfcc_ttls[NETIPMC_CPUPORT_VIFID] = 1;

    /* Add the multicast entry into linuxip. */
    if (setsockopt (gNetIpMcGlobalInfo.i4NetIpSockId, IPPROTO_IP, MRT_ADD_MFC,
                    (VOID *) &McEntry, sizeof (struct mfcctl)) < 0)
    {
        perror ("NetIpv4AddCpuPort- MRT_ADD_MFC Failed.\r\n");
        NETIPMC_MUTEX_UNLOCK ();

        return NETIPV4_FAILURE;
    }

    NETIPMC_MUTEX_UNLOCK ();
    return NETIPV4_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4McGetRouteStats
 *
 *    DESCRIPTION      : Gets the Multicast route stats
 *
 *    INPUT            : u4GrpAddr: Multicast Group Address
 *                       u4SrcAddr: Source Address
 *                       u4StatType:Type of the stats
 *
 *    OUTPUT           : pu4StatVal : Stat value
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McGetRouteStats (UINT4 u4GrpAddr, UINT4 u4SrcAddr, UINT4 u4StatType,
                        UINT4 *pu4StatVal)
{
    struct sioc_sg_req  SgReq;
    INT4                i4RetVal = NETIPV4_SUCCESS;
    FS_ULONG            ulCount = 0;

    MEMSET (&SgReq, 0, sizeof (struct sioc_sg_req));

    SgReq.grp.s_addr = OSIX_HTONL (u4GrpAddr);
    SgReq.src.s_addr = OSIX_HTONL (u4SrcAddr);

    NETIPMC_MUTEX_LOCK ();
    if (ioctl (gNetIpMcGlobalInfo.i4NetIpSockId, SIOCGETSGCNT, &SgReq) != 0)
    {
        NETIPMC_MUTEX_UNLOCK ();
        return NETIPV4_FAILURE;
    }

    NETIPMC_MUTEX_UNLOCK ();

    switch (u4StatType)
    {
        case NETIPMC_ROUTE_PKTS:
            ulCount = SgReq.pktcnt;
            break;

        case NETIPMC_ROUTE_DIFF_IF_IN_PKTS:
            ulCount = SgReq.wrong_if;
            break;

        case NETIPMC_ROUTE_OCTETS:
            ulCount = SgReq.bytecnt;
            break;

        default:
            i4RetVal = NETIPV4_FAILURE;
    }

    *pu4StatVal = (UINT4) ulCount;
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : NetIpv4McGetIfaceStats
 *
 *    DESCRIPTION      : Gets the Multicast enabled interface  stats
 *
 *    INPUT            : i4IfIndex : Interface index
 *                       u4StatType:Type of the stats
 *
 *    OUTPUT           : pu4StatVal : Stat value
 *
 *    RETURNS          : NETIPV4_SUCCESS/NETIPV4_FAILURE
 *
 ****************************************************************************/
INT4
NetIpv4McGetIfaceStats (INT4 i4IfIndex, UINT4 u4StatType, UINT4 *pu4StatVal)
{
    struct sioc_vif_req VifReq;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    FS_ULONG            ulCount = 0;
    INT4                i4RetVal = NETIPV4_SUCCESS;
    UINT4               u4Port = 0;

    if (NetIpv4GetPortFromIfIndex (i4IfIndex, &u4Port) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    pPortMapNode = LnxIpGetPortMapNode (u4Port);
    if (pPortMapNode == NULL)
    {
        return NETIPV4_FAILURE;
    }

    MEMSET (&VifReq, 0, sizeof (struct sioc_vif_req));

    VifReq.vifi = pPortMapNode->u2VifId;

    NETIPMC_MUTEX_LOCK ();
    if (ioctl (gNetIpMcGlobalInfo.i4NetIpSockId, SIOCGETVIFCNT, &VifReq) != 0)
    {
        NETIPMC_MUTEX_UNLOCK ();
        return NETIPV4_FAILURE;
    }
    NETIPMC_MUTEX_UNLOCK ();

    switch (u4StatType)
    {
        case NETIPMC_IFACE_IN_OCTETS:
            ulCount = VifReq.icount;
            break;

        case NETIPMC_IFACE_OUT_OCTETS:
            ulCount = VifReq.ocount;
            break;

        default:
            i4RetVal = NETIPV4_FAILURE;
    }

    *pu4StatVal = (UINT4) ulCount;
    return i4RetVal;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  netipmc.c                      */
/*-----------------------------------------------------------------------*/
