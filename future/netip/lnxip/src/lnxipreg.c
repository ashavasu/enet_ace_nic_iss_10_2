/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxipreg.c,v 1.17 2015/10/26 13:43:34 siva Exp $
 *
 * Description:This file contains the functions related to  
 *             registration/deregistration of applications  
 *             with IP.                                    
 *
 *******************************************************************/
/**** $$TRACE_MODULE_NAME     = IP_FWD     ****/
/**** $$TRACE_SUB_MODULE_NAME = MAIN       ****/

#include "rtminc.h"
#ifdef DHCPC_WANTED
#include "dhcp.h"
#endif
#include "arp.h"

/* Variable declarations */
INT1                gi1RegTableUp = REGDOWN;
tHLProtoRegnTbl     gHLRegnTbl[IP_MAX_PROTOCOLS];

extern UINT2        gu2VlanId;
extern UINT1        gu1IfType;
/*
 * ****************************************************************************
 * Function Name     :  IpInitRegTable
 *
 * Description       :  This function intialises the Registration Table at
 *                      startup.
 *
 * Global Variables  :  gHLRegnTbl
 *    Affected
 *
 * Input(s)          :  None
 *
 * Output(s)         :  None
 *
 * Returns           :  None
 * *****************************************************************************
 */
VOID
IpInitRegTable (VOID)
{
    UINT1               u1Index;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT1               u1CxtIndex;
#endif

    for (u1Index = 0; u1Index < IP_MAX_PROTOCOLS; u1Index++)
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        for(u1CxtIndex = 0; u1CxtIndex < SYS_DEF_MAX_NUM_CONTEXTS; u1CxtIndex++)
        {
            gHLRegnTbl[u1Index].au1CtxReg[u1CxtIndex] = DEREGISTER;
        }
#else
        gHLRegnTbl[u1Index].u1RegFlag = DEREGISTER;
#endif
        gHLRegnTbl[u1Index].pIfStChng = NULL;
        gHLRegnTbl[u1Index].pRtChng = NULL;
        gHLRegnTbl[u1Index].pProtoPktRecv = NULL;
    }
    gi1RegTableUp = REGUP;
    return;
}

/*
 *******************************************************************************
 * Function Name     :  IpActOnProtoRegDereg
 *
 * Description       :  This function Intimates the OperStatus of all the IP
 *                      level interfaces that are up when the protocol registers
 *                      with IP for Interface State Change Notification.
 *                      
 * Global Variables  :  aRegTable
 *    Affected
 *
 * Input(s)          :  u1ProtocolId : Protocol Identifier to be deregistered.
 *
 * Output(s)         :  None.
 *
 * Returns           :  VOID
 * *****************************************************************************
 */
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
VOID 
IpActOnProtoRegDereg (UINT1 u1ProtocolId, UINT4 u4ContextId)
#else
VOID
IpActOnProtoRegDereg (UINT1 u1ProtocolId)
#endif
{
    tRtInfo             InRtInfo;
    tRtInfo             OutRtInfo;
    tRtInfo            *pOutRtInfo = NULL;
    tNetIpv4RtInfo      NetRtInfo;
    tNetIpv4IfInfo      NetIpIfInfo;

    pOutRtInfo = &OutRtInfo;

    MEMSET ((UINT1 *) &NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (gHLRegnTbl[u1ProtocolId].au1CtxReg[u4ContextId] == REGISTER)
#else    
    if (gHLRegnTbl[u1ProtocolId].u1RegFlag == REGISTER)
#endif
    {
        if (gHLRegnTbl[u1ProtocolId].pIfStChng != NULL)
        {
            if (NetIpv4GetFirstIfInfo (&NetIpIfInfo) == NETIPV4_FAILURE)
            {
                return;
            }

            do
            {
                if (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
                {
                    /* Anounce the information about this interface. */
                    gHLRegnTbl[u1ProtocolId].pIfStChng (&NetIpIfInfo,
                                                        OPER_STATE);
                }
            }
            while (NetIpv4GetNextIfInfo (NetIpIfInfo.u4IfIndex, &NetIpIfInfo) ==
                   NETIPV4_SUCCESS);
        }

        if (gHLRegnTbl[u1ProtocolId].pRtChng != NULL)
        {
            MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
            MEMSET (pOutRtInfo, 0, sizeof (tRtInfo));

            while ((RtmNetIpv4GetNextBestRtEntryInCxt
                    (VCM_DEFAULT_CONTEXT, InRtInfo, pOutRtInfo) == IP_SUCCESS))
            {
                MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
                NetRtInfo.u4DestNet = pOutRtInfo->u4DestNet;
                NetRtInfo.u4DestMask = pOutRtInfo->u4DestMask;
                NetRtInfo.u4Tos = pOutRtInfo->u4Tos;
                NetRtInfo.u4NextHop = pOutRtInfo->u4NextHop;
                NetRtInfo.u4RtIfIndx = pOutRtInfo->u4RtIfIndx;
                NetRtInfo.u4RtNxtHopAs = pOutRtInfo->u4RtNxtHopAS;
                NetRtInfo.u4RtAge = pOutRtInfo->u4RtAge;
                NetRtInfo.u4RouteTag = pOutRtInfo->u4RouteTag;
                NetRtInfo.i4Metric1 = pOutRtInfo->i4Metric1;
                NetRtInfo.u2RtProto = pOutRtInfo->u2RtProto;
                NetRtInfo.u2RtType = pOutRtInfo->u2RtType;
                NetRtInfo.u4RowStatus = IPFWD_ACTIVE;
                SET_BIT_FOR_CHANGED_PARAM (NetRtInfo.u2ChgBit, IP_BIT_ALL);

                gHLRegnTbl[u1ProtocolId].pRtChng (&NetRtInfo, (tNetIpv4RtInfo *)NULL, 
                                                  NETIPV4_ADD_ROUTE);
                MEMCPY (&InRtInfo, pOutRtInfo, sizeof (tRtInfo));
                MEMSET (pOutRtInfo, 0, sizeof (tRtInfo));
            }
        }
    }

    return;
}

/*
 * ****************************************************************************
 * Function Name     :  IpIfStateChngNotify
 *
 * Description       :  This function notifies an interface state change to all
 *                      the applications that have registered for the same.
 *                      This function is invoked by IPIF submodule.
 *
 * Global Variables  :  None.
 *    Affected
 *
 * Input(s)          :  pIfInfo    : Pointer to the interface info whose status
 *                                   has changed.
 *                      u4BitMap   : Bit map indicating the fields that have
 *                                   changed.
 *
 * Output(s)         :  None.
 *
 * Returns           :  None.
 * *****************************************************************************
 */
VOID
IpIfStateChngNotify (tNetIpv4IfInfo * pIfInfo, UINT4 u4BitMap)
{
    UINT1               u1Protocol;
    tCfaIfInfo          CfaIfInfo;
    MEMSET (&CfaIfInfo, 0, sizeof(tCfaIfInfo));
    if (CfaGetIfInfo (pIfInfo->u4CfaIfIndex, &CfaIfInfo) == CFA_SUCCESS)
    {
        pIfInfo->u2VlanId = CfaIfInfo.u2VlanId;
    }

    if (u4BitMap == 0)
    {
        return;
    }
    pIfInfo->u1CfaIfType = gu1IfType;

    for (u1Protocol = 0; u1Protocol < IP_MAX_PROTOCOLS; u1Protocol++)
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if ((gHLRegnTbl[u1Protocol].au1CtxReg[pIfInfo->u4ContextId])
                    == REGISTER &&
            (gHLRegnTbl[u1Protocol].pIfStChng != NULL))
#else
        if ((gHLRegnTbl[u1Protocol].u1RegFlag == REGISTER) &&
            (gHLRegnTbl[u1Protocol].pIfStChng != NULL))
#endif
        {
            gHLRegnTbl[u1Protocol].pIfStChng (pIfInfo, u4BitMap);
        }
    }

#ifdef DHCPC_WANTED
    if (pIfInfo->u4DhcpReleaseStatusFlag != DHCPC_RELEASE_STATUS_SET)
    {
        DhcpCHandleIfStateChg (pIfInfo->u4IfIndex, u4BitMap);
    }
#endif

#ifdef ARP_WANTED
    ArpNotifyIfStChg (pIfInfo->u4IfIndex, u4BitMap, pIfInfo->u4Oper);
#endif /* ARP_WANTED */
    return;
}

/*
 * ****************************************************************************
 * Function Name     :  IpRtChngNotifyInCxt
 *
 * Description       :  This function notifies a route change to all the
 *                      applications that have registered for the same. This
 *                      function is invoked.
 *
 * Global Variables  :  None.
 *    Affected
 *
 * Input(s)          :  pRtEntry  : Pointer to the route which has changed.
 *                      u1CmdType : ADD | DELETE | MODIFY. 
 *
 * Output(s)         :  None.
 *
 * Returns           :  None.
 * *****************************************************************************
 */

VOID
IpRtChngNotifyInCxt (UINT4 u4ContextId, tRtInfo * pRtEntry, tRtInfo * pRtEntry1, UINT1 u1CmdType)
{
    UINT1               u1Protocol;
    tNetIpv4RtInfo      NetRtInfo;
    tNetIpv4RtInfo      NetRtInfo1;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&NetRtInfo1, 0, sizeof (tNetIpv4RtInfo));

    NetRtInfo.u4DestNet = pRtEntry->u4DestNet;
    NetRtInfo.u4DestMask = pRtEntry->u4DestMask;
    NetRtInfo.u4Tos = pRtEntry->u4Tos;
    NetRtInfo.u4NextHop = pRtEntry->u4NextHop;
    NetRtInfo.u4RtIfIndx = pRtEntry->u4RtIfIndx;
    NetRtInfo.u4RtAge = pRtEntry->u4RtAge;
    NetRtInfo.u4RtNxtHopAs = pRtEntry->u4RtNxtHopAS;
    NetRtInfo.i4Metric1 = pRtEntry->i4Metric1;
    NetRtInfo.u4RowStatus = pRtEntry->u4RowStatus;
    NetRtInfo.u2RtType = pRtEntry->u2RtType;
    NetRtInfo.u2RtProto = pRtEntry->u2RtProto;
    NetRtInfo.u4RouteTag = pRtEntry->u4RouteTag;
    NetRtInfo.u4ContextId = u4ContextId;

    if (u1CmdType == NETIPV4_MODIFY_ROUTE)
    {
        NetRtInfo1.u4DestNet = pRtEntry1->u4DestNet;
        NetRtInfo1.u4DestMask = pRtEntry1->u4DestMask;
        NetRtInfo1.u4Tos = pRtEntry1->u4Tos;
        NetRtInfo1.u4NextHop = pRtEntry1->u4NextHop;
        NetRtInfo1.u4RtIfIndx = pRtEntry1->u4RtIfIndx;
        NetRtInfo1.u4RtAge = pRtEntry1->u4RtAge;
        NetRtInfo1.u4RtNxtHopAs = pRtEntry1->u4RtNxtHopAS;
        NetRtInfo1.i4Metric1 = pRtEntry1->i4Metric1;
        NetRtInfo1.u4RowStatus = pRtEntry1->u4RowStatus;
        NetRtInfo1.u2RtType = pRtEntry1->u2RtType;
        NetRtInfo1.u2RtProto = pRtEntry1->u2RtProto;
        NetRtInfo1.u4RouteTag = pRtEntry1->u4RouteTag;
        NetRtInfo1.u4ContextId = u4ContextId;
    }

    for (u1Protocol = 0; u1Protocol < IP_MAX_PROTOCOLS; u1Protocol++)
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if ((gHLRegnTbl[u1Protocol].au1CtxReg[u4ContextId] 
              == REGISTER) && (gHLRegnTbl[u1Protocol].pRtChng) != NULL)
#else
        if ((gHLRegnTbl[u1Protocol].u1RegFlag == REGISTER) &&
            (gHLRegnTbl[u1Protocol].pRtChng) != NULL)
#endif
        {
            if (pRtEntry->u2RtProto != u1Protocol)
                gHLRegnTbl[u1Protocol].pRtChng (&NetRtInfo, &NetRtInfo1, u1CmdType);
        }
    }
    return;
}
