/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxipint.c,v 1.89 2017/12/15 09:51:16 siva Exp $
 *
 * Description:This file contains Linux IP related routines.
 *
 *******************************************************************/
#ifdef LNXIP4_WANTED
#include "lr.h"
#include "cfa.h"
#include "iss.h"
#include "l2iwf.h"
#include "ip.h"
#include "rtm.h"
#include "lnxip.h"
#include "lnxipint.h"
#include "lnxipmod.h"
#include "ipvx.h"
#include "fssocket.h"
#include "lnxiptap.h"
#include "chrdev.h"
#include "netdev.h"
#if !defined (VRF_WANTED)
#include "vcm.h"
#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#include "lnxvrf.h"
#endif
#include "cfanp.h"
#ifdef NPAPI_WANTED
#include "ipnpwr.h"
#endif
#include "ipcli.h"

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
INT4                gai4SocketId[SYS_DEF_MAX_NUM_CONTEXTS + 1];
#endif
PRIVATE INT4        gi4socketid;

/* Global tbl maintained for LnxIpPort ->  CfaIfIndex mapping.
   This table is indexed by LnxIpPort no. */
tTMO_HASH_TABLE    *gpPortIfIdxMapTbl = NULL;
tMemPoolId          gPortMapMemPoolId = 0;
UINT4               gu4PortIfIdxMapTblSize;

UINT2               gu2VlanId = 0;
UINT1               gu1IfType = 0;

#if !defined (LINUX_310_WANTED)
extern INT4         VcmGetCxtIpIfaceList
PROTO ((UINT4 u4ContextId, UINT4 *pu4IfIndexArray));
#endif

#ifdef LINUX_310_WANTED
struct nlhandle     Nlkernel;    /* Kernel reflection channel */
struct nlhandle     Nlcmd;        /* Command channel */
CHR1               *ll_kind = "macvlan";

#endif

extern INT4         CfaGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName);

#ifdef LNXIP6_WANTED
extern INT4         Lip6RAConfig (UINT4 u4ContextId);
#endif

extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpTapFdCbkFunc                                */
/*                                                                          */
/*    Description        : This is the call back function from SelAdd Fd    */
/*                                                                          */
/*    Input(s)           : File Descriptor from SellAddFd.                  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : NONE                                             */
/****************************************************************************/

VOID
LnxIpTapFdCbkFunc (INT4 i4SockDesc)
{
    tLnxIpTapMsg       *pLnxIpTapMsg = NULL;
    UINT4               u4CreateStatus = ZERO;

    u4CreateStatus =
        MemAllocateMemBlock (gLnxTapIpEvntMemPoolId,
                             (UINT1 **) (VOID *) &pLnxIpTapMsg);
    if (u4CreateStatus == MEM_FAILURE)
    {
        return;
    }

    MEMSET (pLnxIpTapMsg, 0, sizeof (tLnxIpTapMsg));

    pLnxIpTapMsg->u1MsgType = LNXIP_TAP_PKT_FROM_TUN;
    pLnxIpTapMsg->i4TapFd = i4SockDesc;
    if (OsixQueSend
        (gLnxFromTapIpQId, (UINT1 *) &pLnxIpTapMsg,
         OSIX_DEF_MSG_LEN) == OSIX_SUCCESS)
    {
        if (OsixEvtSend (LNXIP_TASK_ID, LNXIP_TAP_PKT_FROM_TAP_EVENT) !=
            OSIX_SUCCESS)
        {
            /* EVENT SEND FAILED */
            return;
        }
    }
    else
    {
        MemReleaseMemBlock (gLnxTapIpEvntMemPoolId, (UINT1 *) pLnxIpTapMsg);
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpTapHandlePktFromTapEvent                    */
/*                                                                          */
/*    Description        : This is used to hadle Packet from Tap Interface. */
/*                         Reads Frame from Tap Iff and invokes             */
/*                         CfaHandleOutBoundPktFromLnxIpTap to Transmit     */
/*                         Frame to corresponding Ethernet Iff.             */
/*                                                                          */
/*    Input(s)           : File Descriptor of Respective Tap Iff.           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : NONE                                             */
/****************************************************************************/

VOID
LnxIpTapHandlePktFromTapEvent (INT4 i4SockDesc)
{
    tLnxIpTapIfMap     *pLnxIpTapIfMapInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1               au1Buffer[LNXIP_TAP_MAX_FRAME_SIZE];
    INT4                i4RetVal = ZERO;
    INT4                i4PktProcessedCnt = 0;

    if ((LnxIpTapGetIfMapFrmFileDesc (i4SockDesc, &pLnxIpTapIfMapInfo)) ==
        IP_FAILURE)
    {
        return;
    }
    while (1)
    {
        i4RetVal =
            FileRead (i4SockDesc, (CHR1 *) au1Buffer, LNXIP_TAP_MAX_FRAME_SIZE);

        if (i4RetVal == -1 && errno == EAGAIN)
        {
            break;
        }
        if (i4RetVal <= 0)
        {
            if (LnxIpTapDeleteIf (pLnxIpTapIfMapInfo->au1IfName) ==
                OSIX_FAILURE)
            {
                return;
            }
            return;
        }
        if ((pBuf =
             CRU_BUF_Allocate_MsgBufChain (LNXIP_TAP_MAX_FRAME_SIZE,
                                           0)) == NULL)
        {
            return;
        }
        CRU_BUF_Copy_OverBufChain (pBuf, au1Buffer, ZERO, (UINT4) i4RetVal);
        CfaHandleOutBoundPktFromLnxIpTap (pBuf,
                                          pLnxIpTapIfMapInfo->i4CfaIfIdx,
                                          i4RetVal);
        i4PktProcessedCnt++;
        if (i4PktProcessedCnt == LNXIP_MAX_PKTS_PROCESSED_FROM_LNX)
        {
            return;
        }

    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpTapGetIfMapFrmCfaIdx                        */
/*                                                                          */
/*    Description        : This is used to get Interface Mapping Information*/
/*                         between CFA and TAP Iff Index using Cfa Iff Idx. */
/*                         It is assumed that Lock must be taken before     */
/*                         Invoking this function.                          */
/*                                                                          */
/*    Input(s)           : Cfa Iff Index, Dbl Ptr to tLnxIpTapIfMap.        */
/*                                                                          */
/*    Output(s)          : Pointer to tLnxIpTapIfMap structure.             */
/*                                                                          */
/*    Returns            : NONE                                             */
/****************************************************************************/

INT4
LnxIpTapGetIfMapFrmCfaIdx (INT4 i4CfaIdx, tLnxIpTapIfMap ** pLnxIpTapIfMapInfo)
{
    INT4                i4Index = ZERO;
    for (i4Index = 0; i4Index < LNXIP_MAX_TAP_INTERFACES; i4Index++)
    {
        if (LnxIpTapIfMapInfo[i4Index].i4CfaIfIdx == i4CfaIdx)
        {
            *pLnxIpTapIfMapInfo = &LnxIpTapIfMapInfo[i4Index];
            break;
        }
    }
    if (i4Index == LNXIP_MAX_TAP_INTERFACES)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpTapGetIfMapFrmFileDesc                      */
/*                                                                          */
/*    Description        : This is used to get Interface Mapping Information*/
/*                         between CFA and TAP Iff Index using Tap Iff Idx. */
/*                         It is assumed that Lock must be taken before     */
/*                         Invoking this function.                          */
/*                                                                          */
/*    Input(s)           : Tap Iff Index, Dbl Ptr to tLnxIpTapIfMap.        */
/*                                                                          */
/*    Output(s)          : Pointer to tLnxIpTapIfMap structure.             */
/*                                                                          */
/*    Returns            : NONE                                             */
/****************************************************************************/

INT4
LnxIpTapGetIfMapFrmFileDesc (INT4 i4FileDesc,
                             tLnxIpTapIfMap ** pLnxIpTapIfMapInfo)
{
    INT4                i4Index = ZERO;
    for (i4Index = 0; i4Index < LNXIP_MAX_TAP_INTERFACES; i4Index++)
    {
        if (LnxIpTapIfMapInfo[i4Index].i4TapFd == i4FileDesc)
        {
            *pLnxIpTapIfMapInfo = &LnxIpTapIfMapInfo[i4Index];
            break;
        }
    }
    if (i4Index == LNXIP_MAX_TAP_INTERFACES)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpTapGetIfMapFrmIfName                        */
/*                                                                          */
/*    Description        : This is used to get Interface Mapping Information*/
/*                         between CFA and TAP Iff Index using Tap Iff Name.*/
/*                         It is assumed that Lock must be taken before     */
/*                         Invoking this function.                          */
/*                                                                          */
/*    Input(s)           : Ptr to Tap Iff Name, Dbl Ptr to tLnxIpTapIfMap.  */
/*                                                                          */
/*    Output(s)          : Pointer to tLnxIpTapIfMap structure.             */
/*                                                                          */
/*    Returns            : NONE                                             */
/****************************************************************************/

INT4
LnxIpTapGetIfMapFrmIfName (UINT1 *pu1IfName,
                           tLnxIpTapIfMap ** pLnxIpTapIfMapInfo)
{
    INT4                i4Index = ZERO;
    for (i4Index = 0; i4Index < LNXIP_MAX_TAP_INTERFACES; i4Index++)
    {
        if (STRCMP (LnxIpTapIfMapInfo[i4Index].au1IfName, pu1IfName) == 0)
        {
            *pLnxIpTapIfMapInfo = &LnxIpTapIfMapInfo[i4Index];
            break;
        }
    }
    if (i4Index == LNXIP_MAX_TAP_INTERFACES)
    {
        return IP_FAILURE;
    }
    return IP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpWritePktToTap                               */
/*                                                                          */
/*    Description        : This is used to Write Received Frame Buffer from */
/*                         CFA to TAP Interface.                            */
/*                                                                          */
/*    Input(s)           : Cfa Iff Idx, Ptr to Frame Buffer, Len of Frame.  */
/*                                                                          */
/*    Output(s)          : NONE.                                            */
/*                                                                          */
/*    Returns            : NONE                                             */
/****************************************************************************/

VOID
LnxIpWritePktToTap (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               au1Buffer[LNXIP_TAP_MAX_FRAME_SIZE];
    tLnxIpTapIfMap     *pLnxIpTapIfMapInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    INT4                i4RetVal = ZERO;
    INT4                i4CfaIfIdx = 0;
    UINT4               u4Dest = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4PktSize = 0;
    UINT2               u2Port = 0;
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    BOOL1               bOutBoundFlag = CFA_TRUE;
    tPktHandleInfo      PktHandleInfo;
    tNetIpv4IfInfo      IpIntfInfo;
#ifdef LNXIP6_WANTED
    tLip6If            *pLip6If = NULL;
#endif
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];

    MEMSET (&PktHandleInfo, 0, sizeof (tPktHandleInfo));
    MEMSET (au1IfHwAddr, 0, CFA_ENET_ADDR_LEN);
    MEMSET (&(au1Buffer[0]), 0, sizeof (au1Buffer));

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    if (u4PktSize > LNXIP_TAP_MAX_FRAME_SIZE)
    {
        return;
    }
    i4CfaIfIdx = (INT4) pBuf->ModuleData.InterfaceId.u4IfIndex;
    /* Lock Would Have been Already Taken */
    if ((LnxIpTapGetIfMapFrmCfaIdx (i4CfaIfIdx, &pLnxIpTapIfMapInfo)) ==
        IP_FAILURE)
    {
        return;
    }
    if (CRU_BUF_Copy_FromBufChain (pBuf, &(au1Buffer[0]), 0,
                                   u4PktSize) == CRU_FAILURE)
    {
        return;
    }

    i4RetVal =
        FileWrite (pLnxIpTapIfMapInfo->i4TapFd, (CHR1 *) & (au1Buffer[0]),
                   u4PktSize);
    if (i4RetVal == -1 && errno == EAGAIN)
    {
        /* Drop the Frame When Write Q is Full */
        return;
    }

    /* The below change is done for ARP request packet broadcast functionality.
       IP unicast packets destined for the interface are consumed above whereas
       other packets are sent to CFA to broadcast if member ports are present */

    /* Get Protocol and Frame type from ethernet header */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &(PktHandleInfo.EnetHdr), 0,
                               CFA_ENET_V2_HEADER_SIZE);

    PktHandleInfo.u2EtherProto = OSIX_NTOHS (PktHandleInfo.EnetHdr.u2LenOrType);

    if (CFA_IS_ENET_MAC_BCAST (&(PktHandleInfo.EnetHdr)))
    {
        PktHandleInfo.u1LinkFrameType = CFA_LINK_BCAST;
    }
    else if (CFA_IS_ENET_MAC_MCAST (&(PktHandleInfo.EnetHdr)))
    {
        PktHandleInfo.u1LinkFrameType = CFA_LINK_MCAST;
    }
    else
    {
        PktHandleInfo.u1LinkFrameType = CFA_LINK_UCAST;
    }
    /*Restricing packets to avoid looping */
    CfaGetIfHwAddr ((UINT4) i4CfaIfIdx, au1IfHwAddr);
    if (MEMCMP
        (&au1IfHwAddr, &PktHandleInfo.EnetHdr.au1SrcAddr,
         sizeof (au1IfHwAddr)) == 0)
    {
        return;
    }

    /* Make a decision to send to CFA or not */
    if ((PktHandleInfo.u2EtherProto == L2_ENET_IP)
        && (PktHandleInfo.u1LinkFrameType == CFA_LINK_UCAST))
    {
        u2Port = (UINT2) (CfaGetIfIpPort ((UINT4) i4CfaIfIdx));

        MEMSET (&IpIntfInfo, 0, sizeof (tNetIpv4IfInfo));

        pIpHdr =
            (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf,
                                                                   CFA_ENET_V2_HEADER_SIZE,
                                                                   CFA_IP_HDR_LEN);
        if (pIpHdr == NULL)
        {

            /* The header is not contiguous in the buffer */
            pIpHdr = &TmpIpHdr;

            /* Copy the header */
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr,
                                       CFA_ENET_V2_HEADER_SIZE, CFA_IP_HDR_LEN);
        }
        u4Dest = OSIX_NTOHL (pIpHdr->u4Dest);

        if (NetIpv4GetIfInfo (u2Port, &IpIntfInfo) == NETIPV4_SUCCESS)
        {
            u4IpAddr = IpIntfInfo.u4Addr;
            u4NetMask = IpIntfInfo.u4NetMask;

            do
            {
                if (MEMCMP (&u4IpAddr, &u4Dest, sizeof (u4IpAddr)) == 0)
                {
                    bOutBoundFlag = CFA_FALSE;
                    break;
                }
            }
            while (NetIpv4GetNextSecondaryAddress
                   (u2Port, u4IpAddr, &u4IpAddr,
                    &u4NetMask) == NETIPV4_SUCCESS);
        }
    }
    else if (PktHandleInfo.u2EtherProto == CFA_ENET_IPV6)
    {
#ifdef LNXIP6_WANTED
        pLip6If = Lip6UtlGetIfEntry ((UINT4) i4CfaIfIdx);
        if ((pLip6If != NULL) && (pLip6If->u1AdminStatus == NETIPV6_ADMIN_UP))
        {
            bOutBoundFlag = CFA_FALSE;
        }
#endif
    }

    if ((PktHandleInfo.u2EtherProto == L2_ENET_IP) &&
        (PktHandleInfo.u1LinkFrameType == CFA_LINK_UCAST)
        && (bOutBoundFlag == CFA_TRUE))
    {
        /* Send to CFA to transmit over member ports */
        pDupBuf = CRU_BUF_Duplicate_BufChain (pBuf);
        if (pDupBuf != NULL)
        {
            CfaHandleOutBoundPktFromLnxIpTap (pDupBuf, i4CfaIfIdx, u4PktSize);
        }
    }
    if (i4RetVal <= 0)
    {
        if (LnxIpTapDeleteIf (pLnxIpTapIfMapInfo->au1IfName) == OSIX_FAILURE)
        {
            return;
        }
        return;
    }

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpTapCreateIf                                 */
/*                                                                          */
/*    Description        : This is used to Create Tap Interface in Linux    */
/*                         and Update Interface Mapping DB with newly       */
/*                         Created TAP Iff Idx and respective Cfa Idx.      */
/*                                                                          */
/*    Input(s)           : Ptr to Tap Iff Name, Cfa Iff Idx.                */
/*                                                                          */
/*    Output(s)          : NONE.                                            */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                      */
/****************************************************************************/

INT4
LnxIpTapCreateIf (UINT1 *pu1IfName, INT4 i4CfaIfIndex, UINT1 u1IfType)
{
    INT4                i4TapFd = -1;
    INT4                i4Index = ZERO;
    INT4                i4Ret = ZERO;
    INT4                i4FdFlags = ZERO;
    struct ifreq        IfReq;
    i4TapFd = FileOpen ((UINT1 *) TAP_CHR_DEV, OSIX_FILE_RW);
    if (i4TapFd == IP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMSET (&IfReq, ZERO, sizeof (IfReq));
    IfReq.ifr_flags = IFF_NO_PI;
    IfReq.ifr_flags |= IFF_TAP;
    STRNCPY (IfReq.ifr_name, (CHR1 *) pu1IfName, (IFNAMSIZ - 1));
    if ((ioctl (i4TapFd, TUNSETIFF, (VOID *) &IfReq)) < 0)
    {
        FileClose (i4TapFd);
        return OSIX_FAILURE;
    }
    i4FdFlags = fcntl (i4TapFd, F_GETFL, ZERO);
    if (i4FdFlags == -1)
    {
        /* Need To Add Trace Message "Unable to get TAP Fd Flags" */
        FileClose (i4TapFd);
        return OSIX_FAILURE;
    }
    i4Ret = fcntl (i4TapFd, F_SETFL, i4FdFlags | O_NONBLOCK);
    if (i4Ret < 0)
    {
        /* Need To Add Trace Message
           "Unable to set Iff Non Block for TAP Fd" */
        FileClose (i4TapFd);
        return OSIX_FAILURE;
    }
    LnxTapLock ();
    for (i4Index = 0; i4Index < LNXIP_MAX_TAP_INTERFACES; i4Index++)
    {
        if (LnxIpTapIfMapInfo[i4Index].i4TapFd == 0)
        {
            LnxIpTapIfMapInfo[i4Index].i4TapFd = i4TapFd;
            LnxIpTapIfMapInfo[i4Index].i4CfaIfIdx = i4CfaIfIndex;
            LnxIpTapIfMapInfo[i4Index].u1IfType = u1IfType;
            STRCPY (LnxIpTapIfMapInfo[i4Index].au1IfName, pu1IfName);
            break;
        }
    }
    LnxTapUnLock ();
    if (i4Index == LNXIP_MAX_TAP_INTERFACES)
    {
        FileClose (i4TapFd);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpTapDeleteIf                                 */
/*                                                                          */
/*    Description        : This is used to Delete Tap Interface in Linux    */
/*                         and Update Interface Mapping DB.                 */
/*                                                                          */
/*    Input(s)           : Ptr to Tap Iff Name, Cfa Iff Idx.                */
/*                                                                          */
/*    Output(s)          : NONE.                                            */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                      */
/****************************************************************************/

INT4
LnxIpTapDeleteIf (UINT1 *pu1IfName)
{
    tLnxIpTapIfMap     *pLnxIpTapIfMapInfo = NULL;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfIfInfo      *pLnxVrfIfInfo = NULL;
#endif
    /* Not Taking Lock bcoz, we would have taken lock b4 invoking
     * LnxIpTapDeleteIf Function */
    if ((LnxIpTapGetIfMapFrmIfName (pu1IfName, &pLnxIpTapIfMapInfo)) ==
        IP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (SelRemoveFd (pLnxIpTapIfMapInfo->i4TapFd) == OSIX_FAILURE)
    {
        /* Need To Add Trace Message "Failed to Rem Fd from Select" */
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    /*Remove LnxVrfIfInfo node */
    pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1IfName);
    if (pLnxVrfIfInfo != NULL)
    {
        if (RBTreeRem (gLnxVrfIfInfoRBRoot, pLnxVrfIfInfo) == NULL)
        {
            return NETIPV4_FAILURE;
        }
        LNX_VRF_IFINFO_FREE (pLnxVrfIfInfo);
    }
#endif
    FileClose (pLnxIpTapIfMapInfo->i4TapFd);
    pLnxIpTapIfMapInfo->i4TapFd = ZERO;
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpIfInit                                      */
/*                                                                          */
/*    Description        : This function initialises LnxIp interface        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                        */
/****************************************************************************/
INT4
LnxIpIfInit (VOID)
{

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    gai4SocketId[LNX_VRF_DEFAULT_CXT_ID] =
        socket (PF_PACKET, SOCK_RAW, OSIX_HTONS (ETH_P_ALL));

    if (gai4SocketId[LNX_VRF_DEFAULT_CXT_ID] < 0)
    {
        perror ("socket fail");
        return OSIX_FAILURE;
    }

#else
    gi4socketid = socket (PF_PACKET, SOCK_RAW, OSIX_HTONS (ETH_P_ALL));

    if (gi4socketid < 0)
    {
        perror ("socket fail");
        return OSIX_FAILURE;
    }
#endif

    if (LnxIpInitPortIfIdxMapTbl () == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpInitPortIfIdxMapTbl                         */
/*                                                                          */
/*    Description        : This function initialises LnxIpPort To CfaIfIndex*/
/*                         Map table maintained for linux IP                */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_SUCCESS/OSIX_FAILURE                        */
/****************************************************************************/
INT4
LnxIpInitPortIfIdxMapTbl (VOID)
{
    struct if_nameindex *pIfNameIdx = NULL;
    UINT4               u4MaxSysIfs = 0;

    /* Get the total number of system interfaces available.
       if_nameindex() returns the list of all interfaces and their indices */
    pIfNameIdx = if_nameindex ();

    while (pIfNameIdx[u4MaxSysIfs].if_index != 0)
    {
        u4MaxSysIfs++;
    }

    /* Free the data returned from if_nameindex() */
    if_freenameindex (pIfNameIdx);

    /* Assumption: Interfaces are only created thru' ISS code and not allowed
       to be created thru' ifconfig command */
    gu4PortIfIdxMapTblSize = IPIF_MAX_LOGICAL_IFACES + u4MaxSysIfs;

    gpPortIfIdxMapTbl =
        TMO_HASH_Create_Table (gu4PortIfIdxMapTblSize, NULL, TRUE);

    if (NULL == gpPortIfIdxMapTbl)
    {
        printf (" Hash Table Creation failed for Port-InIndex Map Table \r\n");
        return OSIX_FAILURE;

    }
    if (MemCreateMemPool (sizeof (tLnxIpPortMapNode),
                          gu4PortIfIdxMapTblSize,
                          MEM_DEFAULT_MEMORY_TYPE, &gPortMapMemPoolId)
        != MEM_SUCCESS)
    {

        printf (" Mem Pool Creation failed for Port-InIndex Map Nodes \r\n");
        return OSIX_FAILURE;
    }

    return (OSIX_SUCCESS);
}

#ifdef KERNEL_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LinuxIpCreateNetworkInterface                    */
/*                                                                           */
/*    Description         : Creates an IP interface in Linux kernel.         */
/*                                                                           */
/*    Input(s)            : pu1DevName -  Device name                        */
/*                          pu1MacAddress  -  Device MAC address.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LinuxIpCreateNetworkInterface (UINT1 *pu1DevName,
                               tLnxIpPortParams LnxIpPortParams)
{
    tKernIntfParams     KernIntfParams;
    UINT1              *pInputParams = NULL;
    INT4                i4Err = -1;    /* Failure case for ioctl */

    MEMSET (&KernIntfParams, 0, sizeof (tKernIntfParams));

    KernIntfParams.pu1IntfName = MEM_CALLOC (1, STRLEN (pu1DevName), UINT1);

    MEMCPY (KernIntfParams.pu1IntfName, pu1DevName, STRLEN (pu1DevName));

    KernIntfParams.u4Action = GDD_INTERFACE_CREATE;

    MEMCPY (KernIntfParams.InterfaceSet.au1MacAddress,
            LnxIpPortParams.au1MacAddress, MAC_ADDR_LEN);

    pInputParams = (UINT1 *) &KernIntfParams;

    i4Err = ioctl (GDD_CHAR_DEV_FD, GDD_INTERFACE, pInputParams);

    MEM_FREE (KernIntfParams.pu1IntfName);

    if (i4Err < 0)
    {
        return OSIX_FAILURE;
    }

    if ((LnxIpPortParams.u2UpdateMask) &&
        (LinuxIpUpdateInterfaceParams (pu1DevName,
                                       LnxIpPortParams) == OSIX_FAILURE))
    {
        PRINTF ("LinuxIpUpdateInterfaceParams returns failure for Device %s\n",
                pu1DevName);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LinuxIpDeleteNetworkInterface                    */
/*                                                                           */
/*    Description         : Deletes an IP interface in Linux kernel.         */
/*                                                                           */
/*    Input(s)            : pu1DevName -  Device name                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LinuxIpDeleteNetworkInterface (UINT1 *pu1DevName)
{
    tKernIntfParams     KernIntfParams;
    UINT1              *pInputParams = NULL;
    INT4                i4Err = -1;

    MEMSET (&KernIntfParams, 0, sizeof (tKernIntfParams));

    KernIntfParams.pu1IntfName = MEM_CALLOC (1, STRLEN (pu1DevName), UINT1);

    MEMCPY (KernIntfParams.pu1IntfName, pu1DevName, STRLEN (pu1DevName));

    KernIntfParams.u4Action = GDD_INTERFACE_DELETE;

    pInputParams = (UINT1 *) &KernIntfParams;

    i4Err = ioctl (GDD_CHAR_DEV_FD, GDD_INTERFACE, pInputParams);

    MEM_FREE (KernIntfParams.pu1IntfName);

    if (i4Err < 0)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LinuxIpCreateLnxVlanInterface                    */
/*                                                                           */
/*    Description         : Creates an Linux Vlan interface in Linux kernel. */
/*                                                                           */
/*    Input(s)            : pu1DevName -  Device name                        */
/*                          LnxIpPortParams - A structure containing         */
/*                          Ip address, Subnet mask, Broadcast addr, Mtu     */
/*                          and update bitmask flag.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LinuxIpCreateLnxVlanInterface (UINT1 *pu1DevName,
                               tLnxIpPortParams LnxIpPortParams)
{
    UINT4               nm_type = VLAN_NAME_TYPE_RAW_PLUS_VID;
    INT4                i4Fd = -1;
    INT4                i4SockId = -1;
    INT1               *pi1VlanId;
    struct vlan_ioctl_args if_request;
    struct ifreq        if_req;
    CONST CHR1         *pc1ConfFileName = "/proc/net/vlan/config";

    MEMSET (&if_request, 0, sizeof (struct vlan_ioctl_args));
    MEMSET (&if_req, 0, sizeof (struct ifreq));

    if_request.u.name_type = nm_type;

    pi1VlanId = (INT1 *) STRCHR (pu1DevName, '.');
    MEMCPY (if_request.device1, pu1DevName,
            (STRLEN (pu1DevName) - STRLEN (pi1VlanId)));
    pi1VlanId++;

    if_request.u.VID = ATOI (pi1VlanId);
    if_request.cmd = ADD_VLAN_CMD;

    /* Open up the /proc/vlan/config */
    if ((i4Fd = open (pc1ConfFileName, O_RDONLY)) < 0)
    {
        /* err << "ERROR:  Could not open /proc/vlan/config.\n"; */
        printf ("WARNING:  Could not open /proc/net/vlan/config.\n");
        printf ("8021q module needs to be loaded in Linux\n");
        printf ("Linux Vlan Interface Not Created\n");
        return OSIX_FAILURE;

    }
    close (i4Fd);

    if ((i4SockId = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf ("FATAL:  Couldn't open a socket..go figure!\n");
        printf ("Linux Vlan Interface Not Created\n");
        return OSIX_FAILURE;
    }

    if (ioctl (i4SockId, SIOCSIFVLAN, &if_request) < 0)
    {
        printf ("ERROR: trying to add VLAN # to IF -:");
        printf ("Linux Vlan Interface Not Created\n");
        close (i4SockId);
        return OSIX_FAILURE;
    }

    if ((LnxIpPortParams.u2UpdateMask) &&
        (LinuxIpUpdateInterfaceParams (pu1DevName,
                                       LnxIpPortParams) == OSIX_FAILURE))
    {
        PRINTF ("LinuxIpUpdateInterfaceParams returns failure for Device %s\n",
                pu1DevName);
        close (i4SockId);
        return OSIX_FAILURE;
    }

    STRCPY (if_req.ifr_name, pu1DevName);
    if_req.ifr_flags |= (IFF_RUNNING);

    if (ioctl (i4SockId, SIOCSIFFLAGS, &if_req) < 0)
    {
        printf ("ERROR:  Couldn't make the linux vlan interface UP!\n");
        perror ("SIOCSIFFLAGS");
        close (i4SockId);
        return -1;
    }

    close (i4SockId);
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LinuxIpDeleteLnxVlanInterface                    */
/*                                                                           */
/*    Description         : Deletes an Linux Vlan interface in Linux kernel. */
/*                                                                           */
/*    Input(s)            : pu1DevName -  Device name                        */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LinuxIpDeleteLnxVlanInterface (UINT1 *pu1DevName)
{
    INT4                i4Fd = -1;
    INT4                i4SockId = -1;
    struct vlan_ioctl_args if_request;
    CONST CHR1         *pc1ConfFileName = "/proc/net/vlan/config";

    MEMSET (&if_request, 0, sizeof (struct vlan_ioctl_args));

    MEMCPY (if_request.device1, pu1DevName, (STRLEN (pu1DevName)));
    if_request.cmd = DEL_VLAN_CMD;

    /* Open up the /proc/vlan/config */
    if ((i4Fd = open (pc1ConfFileName, O_RDONLY)) < 0)
    {
        printf ("WARNING:  Could not open /proc/net/vlan/config.\n");
        printf ("8021q module needs to be loaded in Linux\n");
        printf ("Linux Vlan Interface Not Created\n");
        return OSIX_FAILURE;

    }
    close (i4Fd);

    if ((i4SockId = socket (AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf ("FATAL:  Couldn't open a socket..go figure!\n");
        printf ("Linux Vlan Interface Not Created\n");
        return OSIX_FAILURE;
    }

    if (ioctl (i4SockId, SIOCSIFVLAN, &if_request) < 0)
    {
        printf ("ERROR: trying to Delete VLAN # to IF -:");
        printf ("Linux Vlan Interface Not Deleted\n");
        close (i4SockId);
        return OSIX_FAILURE;
    }
    close (i4SockId);

    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LinuxIpUpdateInterfaceStatus                     */
/*                                                                           */
/*    Description         : Changes the IP interface UP/DOWN status in       */
/*                          Linux kernel.                                    */
/*                                                                           */
/*    Input(s)            : pu1DevName -  Device name                        */
/*                          u4Action   - Interface UP/ Interface Down        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LinuxIpUpdateInterfaceStatus (UINT1 *pu1DevName, UINT1 u1IfType, UINT1 u1Action)
{

    INT4                i4Err = -1;
    struct ifreq        if_req;
    tLnxIpTapIfMap     *pLnxIpTapIfMapInfo;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = LNX_VRF_DEFAULT_CXT_ID;
    tLnxVrfIfInfo      *pLnxVrfIfInfo = NULL;
#endif

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_name, pu1DevName);

    if_req.ifr_addr.sa_family = AF_INET;

    /* When Knet is supported, the interface is made up or 
     * down based on the action and Tap interface related 
     * action is not required when Knet is supported  */
    if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
    {
        UNUSED_PARAM (pLnxIpTapIfMapInfo);
        if (u1Action == CFA_IF_UP)
        {
            if (!STRNCMP (pu1DevName, "loopback", STRLEN ("loopback")))
            {
                if_req.ifr_flags = IFF_UP | IFF_RUNNING;

            }
            else
            {
                if_req.ifr_flags = IFF_UP | IFF_BROADCAST | IFF_MULTICAST |
                    IFF_RUNNING | IFF_PROMISC;
            }
        }
        else
        {
            if (!STRNCMP (pu1DevName, "loopback", STRLEN ("loopback")))
            {
                if_req.ifr_flags = IFF_RUNNING;
            }
            else
            {
                if_req.ifr_flags = IFF_BROADCAST | IFF_MULTICAST | IFF_RUNNING;
            }
        }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

        pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1DevName);
        if (pLnxVrfIfInfo != NULL)
        {
            u4ContextId = pLnxVrfIfInfo->u4ContextId;
        }
        i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFFLAGS, &if_req);
#else
        i4Err = ioctl (gi4socketid, SIOCSIFFLAGS, &if_req);
#endif

        if (i4Err < 0)
        {
            perror ("IF FLAGS Set");
            return OSIX_FAILURE;
        }

        return OSIX_SUCCESS;
    }

    if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_ENET) || 
	(u1IfType == CFA_PPP) || (u1IfType == CFA_LAGG))
    {
        LnxTapLock ();
        if ((LnxIpTapGetIfMapFrmIfName (pu1DevName, &pLnxIpTapIfMapInfo)) ==
            IP_FAILURE)
        {
            LnxTapUnLock ();
            return OSIX_FAILURE;
        }
    }

    if (u1Action == CFA_IF_UP)
    {
        if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_ENET) || 
	    (u1IfType == CFA_PPP) || (u1IfType == CFA_LAGG))
        {
            if (SelAddFd (pLnxIpTapIfMapInfo->i4TapFd, LnxIpTapFdCbkFunc) ==
                OSIX_FAILURE)
            {
                /* Need To Add Trace Message "Failed to add Fd to Select" */
            }
        }
        if (!STRNCMP (pu1DevName, "loopback", STRLEN ("loopback")))
        {
            if_req.ifr_flags = IFF_UP | IFF_RUNNING;

        }
        else
        {
            if_req.ifr_flags = IFF_UP | IFF_BROADCAST | IFF_MULTICAST |
                IFF_RUNNING | IFF_PROMISC;
        }
    }
    else
    {
        if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_ENET) || 
		(u1IfType == CFA_PPP) || (u1IfType == CFA_LAGG))
        {
            if (SelRemoveFd (pLnxIpTapIfMapInfo->i4TapFd) == OSIX_FAILURE)
            {
                /* Need To Add Trace Message "Failed to Rem Fd from Select" */
            }
        }
        if (!STRNCMP (pu1DevName, "loopback", STRLEN ("loopback")))
        {
            if_req.ifr_flags = IFF_RUNNING;
        }
        else
        {
            if_req.ifr_flags = IFF_BROADCAST | IFF_MULTICAST | IFF_RUNNING;
        }
    }
    if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_ENET) || 
	(u1IfType == CFA_PPP) || (u1IfType == CFA_LAGG))
    {
        LnxTapUnLock ();
    }

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)

    pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1DevName);
    if (pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFFLAGS, &if_req);
#else
    i4Err = ioctl (gi4socketid, SIOCSIFFLAGS, &if_req);
#endif

    if (i4Err < 0)
    {
        perror ("IF FLAGS Set");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LinuxIpUpdateInterfaceParams                     */
/*                                                                           */
/*    Description         : Updates an IP interface parameters in            */
/*                          Linux kernel.                                    */
/*                                                                           */
/*    Input(s)            : pu1DevName   -  Device name                      */
/*                          LnxIpPortParams     - A structure containing     */
/*                          Ip address, Subnet mask, Broadcast addr, Mtu     */
/*                          and update bitmask flag.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LinuxIpUpdateInterfaceParams (UINT1 *pu1DevName,
                              tLnxIpPortParams LnxIpPortParams)
{
    /* It is assumed that all the ioctl calls for setting the IP address,
     * Netmask, Broadcast address and MTU should succeed. The input parameters
     * to this function should have been validated at the calling place.
     * Hence cleanup on failure of a set has not been done .*/

    INT4                i4Err = -1;
    UINT1               u1RetVal = 0;
    struct ifreq        if_req;
    struct sockaddr_in *sinptr;
    INT4                i4SocketId = 0;
    INT4                pi4IfIndex = 0;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = LNX_VRF_DEFAULT_CXT_ID;
    tLnxVrfIfInfo      *pLnxVrfIfInfo = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#endif

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1DevName);
    if (pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo != NULL)
        {
            if (LnxVrfChangeCxt
                (LNX_VRF_NON_DEFAULT_NS,
                 (CHR1 *) pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
            {

                return OSIX_FAILURE;
            }
        }
    }
#endif
    if (STRSTR (pu1DevName, ":") == NULL)
    {
        /* disable reverse path filtering */
        LnxIpSetReversePathFilter (pu1DevName, LNX_RPF_DISABLE);
    }

    /* Setting the IP address */

#ifdef LINUX_310_WANTED
    CHR1                au1Cmd[LNX_MAX_COMMAND_LEN];
#endif

    if (LnxIpPortParams.u2UpdateMask & LNX_IPADDR_UPDATE)
    {
        MEMSET (&if_req, 0, sizeof (if_req));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        i4SocketId = gai4SocketId[u4ContextId];
#else
        i4SocketId = gi4socketid;
#endif

#if defined (LINUX_310_WANTED)
        /* NETLINK changes introduced to support Class E functionality */

        /* Get the interface index */
        if (LinuxIpGetInterfaceIndex (pu1DevName, &pi4IfIndex) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
        STRCPY (if_req.ifr_name, pu1DevName);
        if_req.ifr_addr.sa_family = AF_INET;
        /* Get current IP address configured */
        i4Err = ioctl (i4SocketId, SIOCGIFADDR, &if_req);
        if (i4Err >= ZERO)
        {
            /* Delete the existing IP address */
            sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_addr;
            i4Err = LnxIpAddDelAddr (sinptr->sin_addr.s_addr,
                                     pi4IfIndex, LNX_IPADDR_DEL);

            if (i4Err < 0)
            {
                perror ("IF ADDR Del");
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
                if (LnxVrfChangeCxt
                    (LNX_VRF_DEFAULT_NS,
                     (CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
                {
                    return OSIX_FAILURE;
                }
#endif
                return OSIX_FAILURE;
            }
        }
        /* Add new IP address */
        i4Err = LnxIpAddDelAddr (OSIX_NTOHL (LnxIpPortParams.u4IpAddr),
                                 pi4IfIndex, LNX_IPADDR_ADD);
#else
        UNUSED_PARAM (pi4IfIndex);
        UNUSED_PARAM (i4SocketId);
        STRNCPY (if_req.ifr_name, (CHR1 *) pu1DevName, IFNAMSIZ - 1);

        if_req.ifr_ifru.ifru_addr.sa_family = AF_INET;

        sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_addr;
        sinptr->sin_family = AF_INET;
        sinptr->sin_addr.s_addr = OSIX_HTONL (LnxIpPortParams.u4IpAddr);
        sinptr->sin_port = 0;
        i4Err = ioctl (gi4socketid, SIOCSIFADDR, &if_req);
#endif

        if (i4Err < 0)
        {
            perror ("IF ADDR Set");
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
            if (LnxVrfChangeCxt
                (LNX_VRF_DEFAULT_NS,
                 (CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
            {
                return OSIX_FAILURE;
            }
#endif
            return OSIX_FAILURE;
        }
    }

    /* Only IP address allocation and interface bring up are 
     * done for Knet interface  */
    if (ISS_HW_SUPPORTED == IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
    {
        /* Setting the interface UP */

        MEMSET (&if_req, 0, sizeof (if_req));
        STRCPY (if_req.ifr_name, pu1DevName);

        if_req.ifr_addr.sa_family = AF_INET;

        if_req.ifr_flags = IFF_UP;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFFLAGS, &if_req);
#else
        i4Err = ioctl (gi4socketid, SIOCSIFFLAGS, &if_req);
#endif
        if (i4Err < 0)
        {
            perror ("Setting the interface up");
            return OSIX_FAILURE;
        }
    }

    /* Setting the netmask */

    if (LnxIpPortParams.u2UpdateMask & LNX_SUBNET_UPDATE)
    {
        MEMSET (&if_req, 0, sizeof (if_req));

        STRCPY (if_req.ifr_name, pu1DevName);

        sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_netmask;
        sinptr->sin_family = AF_INET;
        sinptr->sin_addr.s_addr = OSIX_HTONL (LnxIpPortParams.u4SubNetMask);
        sinptr->sin_port = 0;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFNETMASK, &if_req);
#else
        i4Err = ioctl (gi4socketid, SIOCSIFNETMASK, &if_req);
#endif

        if (i4Err < 0)
        {
            perror ("IF ADDRESS Net mask Set");
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
            if (LnxVrfChangeCxt
                (LNX_VRF_DEFAULT_NS,
                 (CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
            {
                return OSIX_FAILURE;
            }
#endif
            return OSIX_FAILURE;
        }
    }

    /* Setting the broadcast address */
    if (LnxIpPortParams.u2UpdateMask & LNX_BCAST_UPDATE)
    {
        MEMSET (&if_req, 0, sizeof (if_req));

        STRCPY (if_req.ifr_name, pu1DevName);

        sinptr = (struct sockaddr_in *) (VOID *) &if_req.ifr_broadaddr;
        sinptr->sin_family = AF_INET;
        sinptr->sin_addr.s_addr = OSIX_HTONL (LnxIpPortParams.u4BcastAddr);
        sinptr->sin_port = 0;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFBRDADDR, &if_req);
#else
        i4Err = ioctl (gi4socketid, SIOCSIFBRDADDR, &if_req);
#endif

        if (i4Err < 0)
        {
            perror ("IF ADDRESS Broadcast Set");
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
            if (LnxVrfChangeCxt
                (LNX_VRF_DEFAULT_NS,
                 (CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
            {
                return OSIX_FAILURE;
            }
#endif
            return OSIX_FAILURE;
        }
    }
    /* Setting the MTU */

    if (LnxIpPortParams.u2UpdateMask & LNX_MTU_UPDATE)
    {
        MEMSET (&if_req, 0, sizeof (if_req));

        STRCPY (if_req.ifr_name, pu1DevName);

        if_req.ifr_mtu = LnxIpPortParams.u4Mtu;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFMTU, &if_req);
#else
        i4Err = ioctl (gi4socketid, SIOCSIFMTU, &if_req);
#endif

        if (i4Err < 0)
        {
            perror ("IF ADDRESS MTU Set");
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
            if (LnxVrfChangeCxt
                (LNX_VRF_DEFAULT_NS,
                 (CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
            {
                return OSIX_FAILURE;
            }
#endif
            return OSIX_FAILURE;
        }
    }

    /* Loopback interfaces in 3.10 kernel does not support MAC address change */
    u1RetVal = IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT);
    if (ISS_HW_SUPPORTED != u1RetVal)
    {
#ifdef LINUX_310_WANTED
        if (strstr ((const char *) pu1DevName, "loopback") == 0)
        {
#endif

            /* Set MAC Addr */
            if (LnxIpPortParams.u2UpdateMask & LNX_MACADDR_UPDATE)
            {
                if ((LinuxIpUpdateInterfaceMacAddress (pu1DevName,
                                                       LnxIpPortParams.
                                                       au1MacAddress)) ==
                    OSIX_FAILURE)
                {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
                    if (LnxVrfChangeCxt
                        (LNX_VRF_DEFAULT_NS,
                         (CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
                    {
                        return OSIX_FAILURE;
                    }
#endif
                    return OSIX_FAILURE;
                }
            }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
            if (LnxVrfChangeCxt
                (LNX_VRF_DEFAULT_NS,
                 (CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
            {
                return OSIX_FAILURE;
            }
#endif

#ifdef LINUX_310_WANTED
        }
        else
        {
#if defined (VRF_WANTED)
/* Switching the context to user VRF context based on u4ContextId for ip configuration*/
            if (u4ContextId != VCM_DEFAULT_CONTEXT)
            {
                pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
                if (pLnxVrfInfo != NULL)
                {
                    LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS,
                                     (CHR1 *) pLnxVrfInfo->au1NameSpace);
                }
            }
#endif
            MEMSET (au1Cmd, 0, sizeof (au1Cmd));
            sprintf (au1Cmd, "ifconfig %s up", pu1DevName);
            system (au1Cmd);
#if defined (VRF_WANTED)
/* Switching the context back to default from user VRF context */
            if (u4ContextId != VCM_DEFAULT_CONTEXT && pLnxVrfInfo != NULL)
            {
                LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                                 (CHR1 *) LNX_VRF_DEFAULT_NS_PID);

            }
#endif
        }
#endif
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LnxIpUpdateSecondaryIpInfo                       */
/*                                                                           */
/*    Description         : Updates the secondary IP address for the         */
/*                          interface.                                       */
/*                                                                           */
/*    Input(s)            : pu1DevName -  Device name                        */
/*                          pIpInfo - IP information                         */
/*                          u4Flag - Indicates operation to be performed     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                       */
/*****************************************************************************/
INT4
LnxIpUpdateSecondaryIpInfo (UINT1 *pu1DevName,
                            tIpConfigInfo * pIpInfo, UINT4 u4Flag)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tLnxIpPortParams    LnxIpPortParams;
    struct ifreq        if_req;
    INT4                i4Err = -1;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = LNX_VRF_DEFAULT_CXT_ID;
    tLnxVrfIfInfo      *pLnxVrfIfInfo = NULL;
#endif

    if (u4Flag & CFA_IP_IF_SECONDARY_ADDR_DEL)
    {
        /* Secondary address configured can be deleted by making alias interface 
         * down */
        MEMSET (&if_req, 0, sizeof (if_req));
        STRCPY (if_req.ifr_name, pu1DevName);

        if_req.ifr_addr.sa_family = AF_INET;

        if (!STRNCMP (pu1DevName, "loopback", STRLEN ("loopback")))
        {
            if_req.ifr_flags = IFF_RUNNING;
        }
        else
        {
            if_req.ifr_flags = IFF_BROADCAST | IFF_MULTICAST | IFF_RUNNING;
        }

        LnxTapUnLock ();

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1DevName);
        if (pLnxVrfIfInfo != NULL)
        {
            u4ContextId = pLnxVrfIfInfo->u4ContextId;
        }
        i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFFLAGS, &if_req);
#else
        i4Err = ioctl (gi4socketid, SIOCSIFFLAGS, &if_req);
#endif

        if (i4Err < 0)
        {
            perror ("IF FLAGS Set");
            i4RetVal = OSIX_FAILURE;
        }

        i4RetVal = OSIX_SUCCESS;
    }
    else
    {
        MEMSET (&LnxIpPortParams, 0, sizeof (tLnxIpPortParams));
        LnxIpPortParams.u4IpAddr = pIpInfo->u4Addr;
        LnxIpPortParams.u4SubNetMask = pIpInfo->u4NetMask;
        LnxIpPortParams.u4BcastAddr = pIpInfo->u4BcastAddr;

        LnxIpPortParams.u2UpdateMask = (UINT2) (LNX_IPADDR_UPDATE |
                                                LNX_SUBNET_UPDATE |
                                                LNX_BCAST_UPDATE);
        i4RetVal = LinuxIpUpdateInterfaceParams (pu1DevName, LnxIpPortParams);
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LinuxIpGetInterfaceIndex                         */
/*                                                                           */
/*    Description         : This function gets the Interface index for an    */
/*                                                                           */
/*    Input(s)            : pu1DevName   -  Device name                      */
/*                                                                           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LinuxIpGetInterfaceIndex (UINT1 *pu1DevName, INT4 *pi4IfIndex)
{
    INT4                i4Err = -1;
    struct ifreq        if_req;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = LNX_VRF_DEFAULT_CXT_ID;
    tLnxVrfIfInfo      *pLnxVrfIfInfo = NULL;
#endif

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_ifrn.ifrn_name, pu1DevName);

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1DevName);
    if (pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }
    i4Err = ioctl (gai4SocketId[u4ContextId], SIOCGIFINDEX, &if_req);
#else
    i4Err = ioctl (gi4socketid, SIOCGIFINDEX, &if_req);
#endif

    if (i4Err < 0)
    {
        perror ("IF Index get");
        return OSIX_FAILURE;
    }

    *pi4IfIndex = if_req.ifr_ifindex;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LinuxIpUpdateInterfaceMacAddress                 */
/*                                                                           */
/*    Description         : This function updates the MAC address of an IVR  */
/*                                                                           */
/*    Input(s)            : pu1DevName    -  Device name                     */
/*                          pu1MacAddress -  MAC address                     */
/*                                                                           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LinuxIpUpdateInterfaceMacAddress (UINT1 *pu1DevName, UINT1 *pu1MacAddress)
{
    INT4                i4Err = -1;
    struct ifreq        if_req;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = LNX_VRF_DEFAULT_CXT_ID;
    tLnxVrfIfInfo      *pLnxVrfIfInfo = NULL;
#endif

    /* Setting the interface down */

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1DevName);
    if (pLnxVrfIfInfo != NULL)
    {
        u4ContextId = pLnxVrfIfInfo->u4ContextId;
    }

#endif

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_name, pu1DevName);

    if_req.ifr_addr.sa_family = AF_INET;

    if_req.ifr_flags = IFF_BROADCAST | IFF_MULTICAST | IFF_RUNNING;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFFLAGS, &if_req);
#else
    i4Err = ioctl (gi4socketid, SIOCSIFFLAGS, &if_req);
#endif

    if (i4Err < 0)
    {
        perror ("Setting interface down");
        return OSIX_FAILURE;
    }

    /* Setting the MAC address */

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_name, pu1DevName);

    if_req.ifr_hwaddr.sa_family = AF_LOCAL;

    MEMCPY (if_req.ifr_hwaddr.sa_data, pu1MacAddress, MAC_ADDR_LEN);

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFHWADDR, &if_req);
#else
    i4Err = ioctl (gi4socketid, SIOCSIFHWADDR, &if_req);
#endif

    if (i4Err < 0)
    {
        perror ("IF MAC Set");
        return OSIX_FAILURE;
    }

    /* Setting the interface UP */

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_name, pu1DevName);

    if_req.ifr_addr.sa_family = AF_INET;

    if_req.ifr_flags = IFF_UP | IFF_BROADCAST | IFF_MULTICAST | IFF_RUNNING;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFFLAGS, &if_req);
#else
    i4Err = ioctl (gi4socketid, SIOCSIFFLAGS, &if_req);
#endif

    if (i4Err < 0)
    {
        perror ("Setting the interface up");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IpHandleInterfaceMsgFromCfa                      */
/*                                                                           */
/*                                                                           */
/*    Description         : This procedure is called by CFA to create,       */
/*                          update or delete interface parameters in linux   */
/*                          kernel.                                          */
/*                                                                           */
/*    Input(s)            : pIfaceParams - Pointer to a structure with       */
/*                          information about the interface.                 */
/*                                                                           */
/*                          u1Command    - Specifies interface               */
/*                          creation, deletion or updation.                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : IP_SUCCESS / IP_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
IpHandleInterfaceMsgFromCfa (t_IFACE_PARAMS * pIfaceParams, UINT1 u1Command)
{
    INT4                i4IfIpPortNum;
    tCfaIfInfo          CfaIfInfo;
    tLnxIpPortParams    LnxIpPortParams;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT2               u2UpdateMask = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4Index = 0;
    UINT1               u1HwKnetIntf = ISS_HW_NOT_SUPPORTED;
    tCfaIfInfo          IfInfo;
#ifdef WLC_WANTED
#ifdef LNXIP4_WANTED
    UINT1               au1SubIfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    INT4                i4LPortNum = 0;
    INT4                i4PhyPort = 0;
    INT4                i4SlotNum = 0;
    UINT1              *pu1IfPortName = NULL;
    UINT1               au1Command[LNX_MAX_COMMAND_LEN] = { 0 };
#endif /* LNXIP4_WANTED */
#endif /* WLC_WANTED */
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
#ifdef WLC_WANTED
#ifdef LNXIP4_WANTED
    MEMSET (au1SubIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1Command, 0, LNX_MAX_COMMAND_LEN);
#endif /* LNXIP4_WANTED */
#endif /* WLC_WANTED */
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    if (VcmGetContextIdFromCfaIfIndex ((UINT4) pIfaceParams->u2IfIndex,
                                       &u4ContextId) == VCM_FAILURE)
    {
        /* Interface to context mapping is not present in VCM */
        return IP_FAILURE;
    }

#endif
    if (pIfaceParams->u1IfType == CFA_TUNNEL)
    {
        /* Do nothing for tunnel interface */
        return IP_SUCCESS;
    }
    u1HwKnetIntf = IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT);
    switch (u1Command)
    {
        case IPIF_CREATE_INTERFACE:

            /*Get index from index manager.If index is zero, then break */
            u4Index = NetIpv4GetIndexFromIndexManager (LNXIP_ONE);
            if (u4Index == 0)
            {
                i4RetVal = OSIX_FAILURE;
                break;
            }

            MEMSET (&LnxIpPortParams, 0, sizeof (tLnxIpPortParams));

            LnxIpPortParams.u4IpAddr = pIfaceParams->u4IpAddr;
            LnxIpPortParams.u4SubNetMask = pIfaceParams->u4SubnetMask;
            LnxIpPortParams.u4BcastAddr = pIfaceParams->u4BcastAddr;
            LnxIpPortParams.u4Mtu = pIfaceParams->u4Mtu;
            if (CfaIsMgmtPort ((UINT4) pIfaceParams->u2IfIndex) == TRUE)
            {
                /* Dont Update Mask when the IP Address configured is 
                 * 0.0.0.0 */
                if (pIfaceParams->u4IpAddr == IP_ANY_ADDR)
                {
                    LnxIpPortParams.u2UpdateMask =
                        (UINT2) (LNX_IPADDR_UPDATE | LNX_MTU_UPDATE);
                }
                else
                {
                    LnxIpPortParams.u2UpdateMask =
                        (UINT2) (LNX_IPADDR_UPDATE | LNX_SUBNET_UPDATE |
                                 LNX_BCAST_UPDATE | LNX_MTU_UPDATE);
                }

                /* Mgmt port would have been created during bootup itself.
                 * Do not create the interface. Update the interface 
                 * parameters alone. */
                i4RetVal = LinuxIpUpdateInterfaceParams
                    (pIfaceParams->au1IfName, LnxIpPortParams);
            }
            else if (CfaIsLinuxVlanIntf ((UINT4) pIfaceParams->u2IfIndex) ==
                     TRUE)
            {
                i4RetVal =
                    LinuxIpCreateLnxVlanInterface (pIfaceParams->au1IfName,
                                                   LnxIpPortParams);
            }
            else
            {
                if (pIfaceParams->u4IpAddr != IP_ANY_ADDR)
                {
                    if (pIfaceParams->u1IfType == CFA_LOOPBACK)
                    {
                        LnxIpPortParams.u2UpdateMask =
                            (UINT2) (LNX_IPADDR_UPDATE | LNX_SUBNET_UPDATE |
                                     LNX_BCAST_UPDATE);

                    }
                    else
                    {
                        LnxIpPortParams.u2UpdateMask =
                            (UINT2) (LNX_IPADDR_UPDATE | LNX_SUBNET_UPDATE |
                                     LNX_BCAST_UPDATE | LNX_MTU_UPDATE);
                    }
                }

#ifdef KERNEL_WANTED
                i4RetVal = LinuxIpCreateNetworkInterface
                    (pIfaceParams->au1IfName, LnxIpPortParams);
#else

                if (u1HwKnetIntf == ISS_HW_NOT_SUPPORTED)
                {
                    if (pIfaceParams->u1IfType == CFA_LOOPBACK)
                    {

                        LinuxIpCreateLoopbackInterface (pIfaceParams->au1IfName,
                                                        u4ContextId);
                    }
                }
                /* Vlan Tap Interface changes for LINUXIP */
                if ((pIfaceParams->u1IfType == CFA_L3IPVLAN) ||
                    (pIfaceParams->u1IfType == CFA_ENET) ||
                    (pIfaceParams->u1IfType == CFA_PPP) ||
                    (pIfaceParams->u1IfType == CFA_LAGG))
                {

                    /* Tap interface creation is not required when HW supports
                     *                    kernel interface */

                    if (u1HwKnetIntf == ISS_HW_NOT_SUPPORTED)
                    {
                        i4RetVal = LnxIpTapCreateIf (pIfaceParams->au1IfName,
                                                     (INT4) pIfaceParams->
                                                     u2IfIndex,
                                                     pIfaceParams->u1IfType);
                        if (i4RetVal == OSIX_FAILURE)
                        {
                            return IP_FAILURE;
                        }
                    }
                }
#ifdef WLC_WANTED
#ifdef LNXIP4_WANTED
                /* Create VCONFIG interface, if the interface type is L3SUBINTF */
                if (pIfaceParams->u1IfType == CFA_L3SUB_INTF)
                {
                    CfaGetPortName (pIfaceParams->u2IfIndex, au1SubIfName);
                    pu1IfPortName = (au1SubIfName + 4);

                    if ((CfaCliGetPhysicalAndLogicalPortNum
                         ((INT1 *) pu1IfPortName, &i4PhyPort, &i4LPortNum,
                          &i4SlotNum)) == CFA_SUCCESS)
                    {
                        SPRINTF ((CHR1 *) au1Command, "vconfig rem %s.%d",
                                 CfaGddGetLnxIntfnameForPort ((UINT2)
                                                              i4PhyPort),
                                 i4LPortNum);
                        system ((CHR1 *) au1Command);

                        SPRINTF ((CHR1 *) au1Command, "vconfig add %s %d",
                                 CfaGddGetLnxIntfnameForPort ((UINT2)
                                                              i4PhyPort),
                                 i4LPortNum);
                        system ((CHR1 *) au1Command);
                        /* Delay in updating the interface details, as vconfig takes
                         * some time to get executed. Else, this will result in failure
                         * while updating IF params as the IF is not created yet */
                        OsixTskDelay (1 * SYS_TIME_TICKS_IN_A_SEC);
                    }
                }
#endif
#endif
#endif
                if ((pIfaceParams->u1IfType == CFA_L3IPVLAN) ||
                    (pIfaceParams->u1IfType == CFA_LOOPBACK) ||
                    (pIfaceParams->u1IfType == CFA_LAGG) ||
                    (pIfaceParams->u1IfType == CFA_ENET) ||
                    (pIfaceParams->u1IfType == CFA_PPP) ||
                    (pIfaceParams->u1IfType == CFA_L3SUB_INTF))
                {

                    if (MEMCMP (LnxIpPortParams.au1MacAddress,
                                pIfaceParams->MacAddress, MAC_ADDR_LEN) != ZERO)
                    {
                        LnxIpPortParams.u2UpdateMask |=
                            (UINT2) (LNX_MACADDR_UPDATE);
                    }

                    MEMCPY (LnxIpPortParams.au1MacAddress,
                            pIfaceParams->MacAddress, MAC_ADDR_LEN);
                }
                else
                {
                    /*  MEMCPY (LnxIpPortParams.au1MacAddress,
                       pIfaceParams->MacAddress, MAC_ADDR_LEN); */
                }
                i4RetVal = LinuxIpUpdateInterfaceParams
                    (pIfaceParams->au1IfName, LnxIpPortParams);
            }

            if (i4RetVal == OSIX_FAILURE)
            {
                break;
            }

            i4RetVal = LinuxIpGetInterfaceIndex
                (pIfaceParams->au1IfName, &i4IfIpPortNum);
            if (i4RetVal == OSIX_SUCCESS)
            {
                LnxIpAddPortToMappingTable ((UINT4) pIfaceParams->u2IfIndex,
                                            (UINT4) i4IfIpPortNum,
                                            pIfaceParams->u1IfType);

                CfaIfInfo.i4IpPort = i4IfIpPortNum;
                CfaSetIfInfo (IF_IP_PORTNUM, (UINT4) pIfaceParams->u2IfIndex,
                              &CfaIfInfo);
                CfaIfInfo.u4IndexMgrPort = u4Index;
                CfaSetIfInfo (IF_IP_IDX_MGR_NUM,
                              (UINT4) pIfaceParams->u2IfIndex, &CfaIfInfo);
            }
            else
            {
                NetIpv4ReleaseIndexToIndexManger (LNXIP_ONE, u4Index);

                PRINTF ("LinuxIpGetInterfaceIndex returns fail for Dev%s\n",
                        pIfaceParams->au1IfName);
                fflush (stdout);
            }
            /* To notify the oper change to the upper layers as well as to 
             * add the local route.*/
            pIfaceParams->u2Port = (UINT2) i4IfIpPortNum;
            /* Add tge index manager port to the ifaceparams  structure */
            pIfaceParams->u4IdxMgrPort = u4Index;

            LnxIpNotifyIfInfo (RTM_CREATE_INT_MSG, pIfaceParams);
            break;

        case IPIF_UPDATE_INTERFACE:

            /* Update the Kernel */
            MEMSET (&LnxIpPortParams, 0, sizeof (tLnxIpPortParams));
            LnxIpPortParams.u4IpAddr = pIfaceParams->u4IpAddr;
            LnxIpPortParams.u4SubNetMask = pIfaceParams->u4SubnetMask;
            LnxIpPortParams.u4BcastAddr = pIfaceParams->u4BcastAddr;
            LnxIpPortParams.u4Mtu = pIfaceParams->u4Mtu;
            LnxIpPortParams.u4IfSpeed = pIfaceParams->u4IfSpeed;
            if (MEMCMP (LnxIpPortParams.au1MacAddress, pIfaceParams->MacAddress,
                        MAC_ADDR_LEN) != ZERO)
            {
                LnxIpPortParams.u2UpdateMask = (UINT2) (LNX_MACADDR_UPDATE);
                MEMCPY (LnxIpPortParams.au1MacAddress,
                        pIfaceParams->MacAddress, MAC_ADDR_LEN);
            }

            /* Dont Update Mask when the IP Address configured is 
             * 0.0.0.0 */
            if (pIfaceParams->u4IpAddr == IP_ANY_ADDR)
            {
                LnxIpPortParams.u2UpdateMask =
                    (UINT2) (LNX_IPADDR_UPDATE | LNX_MTU_UPDATE);
            }
            else
            {
                LnxIpPortParams.u2UpdateMask =
                    (UINT2) (LNX_IPADDR_UPDATE | LNX_SUBNET_UPDATE |
                             LNX_BCAST_UPDATE | LNX_MTU_UPDATE);
            }

            if ((pIfaceParams->u1IfType == CFA_LOOPBACK) ||
		(pIfaceParams->u1IfType == CFA_PPP))
            {
                /* For loop back interface mtu value need not be updated.So the
                 * the UpdateMask need to be reset */
                LnxIpPortParams.u2UpdateMask &= (~LNX_MTU_UPDATE);
            }

            /*  In case of Kernel Network Updating the Linux with the interface 
             *  details, if the port is router port.
             */ 
            if (((pIfaceParams->u1IfType == CFA_ENET) || (pIfaceParams->u1IfType == CFA_PPP) ||
		 (pIfaceParams->u1IfType == CFA_LAGG )) 
                && (IfInfo.u1BridgedIface != CFA_DISABLED))
            {
                i4RetVal = LinuxIpGetInterfaceIndex (pIfaceParams->au1IfName,
                                                     &i4IfIpPortNum);
                if (i4RetVal == OSIX_SUCCESS)
                {
                    pPortMapNode = LnxIpGetPortMapNode (i4IfIpPortNum);
                    /* while changing port-vid, the corresponding interface
                     * in the linux gets deleted and a new interface is 
                     * created. So updating Interface index details is needed 
                     * after changing port-vid 
                     */
                    if (pPortMapNode == NULL)
                    {
                        if (u1HwKnetIntf == ISS_HW_SUPPORTED)
                        {
                            LnxIpAddPortToMappingTable ((UINT4) pIfaceParams->
                                                        u2IfIndex,
                                                        (UINT4) i4IfIpPortNum,
                                                        pIfaceParams->u1IfType);
                            CfaIfInfo.i4IpPort = i4IfIpPortNum;
                            CfaSetIfInfo (IF_IP_PORTNUM,
                                          (UINT4) pIfaceParams->u2IfIndex,
                                          &CfaIfInfo);
                        }
                    }
                    else
                    {
                        if (pPortMapNode->u4IpPortSpeed !=
                            pIfaceParams->u4IfSpeed)
                        {
                            u2UpdateMask = (UINT2) (LNX_SPEED_UPDATE);
                        }
                        if (pPortMapNode->u4IpPortMtu != pIfaceParams->u4Mtu)
                        {
                            u2UpdateMask =
                                (UINT2) (u2UpdateMask | LNX_MTU_UPDATE);
                        }

                    }
                }
                else
                {
                    UtlTrcLog (1, 1, "",
                               "ERROR: LinuxIpGetInterfaceIndex returns fail\n");
                }
                pIfaceParams->u2Port = (UINT2) i4IfIpPortNum;
            }
            i4RetVal = IpUpdatePortParams (pIfaceParams, u2UpdateMask);
            if (i4RetVal == OSIX_FAILURE)
            {
                break;
            }
            i4RetVal = LinuxIpUpdateInterfaceParams (pIfaceParams->
                                                     au1IfName,
                                                     LnxIpPortParams);

            break;

        case IPIF_DELETE_INTERFACE:

            LnxIpNotifyIfInfo (RTM_DELETE_INT_MSG, pIfaceParams);
            if (CfaIsMgmtPort ((UINT4) pIfaceParams->u2IfIndex) == TRUE)
            {
                i4RetVal = LinuxIpUpdateInterfaceStatus
                    (pIfaceParams->au1IfName, pIfaceParams->u1IfType,
                     CFA_IF_DOWN);
            }
            else if (CfaIsLinuxVlanIntf ((UINT4) pIfaceParams->u2IfIndex) ==
                     TRUE)
            {
                i4RetVal =
                    LinuxIpDeleteLnxVlanInterface (pIfaceParams->au1IfName);
            }
            else
            {
#ifdef KERNEL_WANTED
                i4RetVal = LinuxIpDeleteNetworkInterface
                    (pIfaceParams->au1IfName);
#else
                if (pIfaceParams->u1IfType == CFA_LOOPBACK)
                {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
                    if (u4ContextId)
                    {
                        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
                        if (pLnxVrfInfo == NULL)
                        {
                            return IP_FAILURE;
                        }
                        LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS,
                                         (CHR1 *) pLnxVrfInfo->au1NameSpace);
                    }
#endif
                    if (u1HwKnetIntf == ISS_HW_NOT_SUPPORTED)
                    {

                        LinuxIpDeleteLoopbackInterface (pIfaceParams->au1IfName,
                                                        u4ContextId);
                    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
                    if (u4ContextId)
                    {
                        LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                                         (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
                    }
#endif

                }
                /* Tap interface creation is not required when Knet
                 *                  * interface deletion is done */
                if (u1HwKnetIntf == ISS_HW_NOT_SUPPORTED)
                {
                    /* Vlan Tap Interface changes for LINUXIP */
                    if ((pIfaceParams->u1IfType == CFA_L3IPVLAN)
                        || (pIfaceParams->u1IfType == CFA_LAGG))
                    {
#ifdef LNXIP6_WANTED
                        Lip6StopRadvd (u4ContextId);
#if (defined(VRF_WANTED)) && (defined(LINUX_310_WANTED))
                        gau1RadvdRunning[u4ContextId] = OSIX_FALSE;
#else
                        gu1RadvdRunning = OSIX_FALSE;
#endif
#endif
                        LnxTapLock ();
                        i4RetVal = LnxIpTapDeleteIf (pIfaceParams->au1IfName);
                        LnxTapUnLock ();
#ifdef LNXIP6_WANTED
                        Lip6RAConfig (u4ContextId);
#endif
                    }
#ifdef WLC_WANTED
#ifdef LNXIP4_WANTED
                    if (pIfaceParams->u1IfType == CFA_L3SUB_INTF)
                    {
                        CfaGetPortName (pIfaceParams->u2IfIndex, au1SubIfName);
                        pu1IfPortName = (au1SubIfName + 4);

                        if ((CfaCliGetPhysicalAndLogicalPortNum
                             ((INT1 *) pu1IfPortName, &i4PhyPort, &i4LPortNum,
                              &i4SlotNum)) == CFA_SUCCESS)
                        {
                            SPRINTF ((CHR1 *) au1Command, "vconfig rem %s.%d",
                                     CfaGddGetLnxIntfnameForPort ((UINT2)
                                                                  i4PhyPort),
                                     i4LPortNum);
                            system ((CHR1 *) au1Command);
                        }
                    }
#endif
#endif
                    else
                    {
                        /*No operation for interfaces except CFA_L3IPVLAN that is physical interfaces */
                        i4RetVal = OSIX_SUCCESS;
                    }
                }
                else
                {
                    /* No operation for Knet interface */
                    i4RetVal = OSIX_SUCCESS;
                }
#endif

            }
            if (i4RetVal == OSIX_FAILURE)
            {
                break;
            }

            CfaIfInfo.i4IpPort = CFA_INVALID_INDEX;
            CfaSetIfInfo (IF_IP_PORTNUM, (UINT4) pIfaceParams->u2IfIndex,
                          &CfaIfInfo);

            LnxIpDeletePortFromMappingTable (pIfaceParams->u2Port);

            i4RetVal = OSIX_SUCCESS;

            /*Release the index manager for reuse */
            if (NetIpv4ReleaseIndexToIndexManger (LNXIP_ONE,
                                                  pIfaceParams->u4IdxMgrPort) ==
                LNXIP_SUCCESS)
            {
                i4RetVal = OSIX_SUCCESS;
            }
            else
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;

        default:

            i4RetVal = OSIX_SUCCESS;
            break;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        LNXIP_SET_IF_TBL_CHANGE_TIME ();
        return IP_SUCCESS;
    }
    else
    {
        return IP_FAILURE;
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpUpdatePortParams
 *
 * Input(s)           : pIfaceParams
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action             : Invoked by linuxip to change port parameters. 
 *
 *
+-------------------------------------------------------------------*/
INT4
IpUpdatePortParams (t_IFACE_PARAMS * pIfaceParams, UINT2 u2UpdateMask)
{
    t_IFACE_PARAMS      IfaceParams;
    UINT1               u1MsgType = 0;

    IfaceParams.u2Port = pIfaceParams->u2Port;
    IfaceParams.u4Mtu = pIfaceParams->u4Mtu;
    IfaceParams.u4IfSpeed = pIfaceParams->u4IfSpeed;
    if (u2UpdateMask & LNX_MTU_UPDATE)
    {
        u1MsgType = RTM_MTU_UPDATE_MSG;
        if (LnxIpNotifyIfInfo (u1MsgType, &IfaceParams) == IP_FAILURE)
        {
            return (OSIX_FAILURE);
        }
    }
    if (u2UpdateMask & LNX_SPEED_UPDATE)
    {
        u1MsgType = RTM_SPEED_UPDATE_MSG;
        if (LnxIpNotifyIfInfo (u1MsgType, &IfaceParams) == IP_FAILURE)
        {
            return (OSIX_FAILURE);
        }
    }
    return OSIX_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpUpdateInterfaceStatus 
 *
 * Input(s)           : u2Port  - IP port number
 *                      pu1IfName - Interface name
 *                      u1OperStatus - Interface up or down
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action             : Invoked by CFA
 *                      Changes the UP/DOWN status of the interface in IP.
 *
 *
+-------------------------------------------------------------------*/
INT4
IpUpdateInterfaceStatus (UINT2 u2Port, UINT1 *pu1IfName, UINT1 u1OperStatus)
{
    tLnxIpPortMapNode  *pPortMapNode = NULL;
    t_IFACE_PARAMS      IfaceParams;
    UINT1               u1Ipv4EnableStatus = IPVX_IPV4_ENABLE_STATUS_DOWN;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1CurrOperStatus = 0;
    tArpQMsg            ArpQMsg;
    tNetIpv4IfInfo      IpIntfInfo;
    pPortMapNode = LnxIpGetPortMapNode (u2Port);

    if (pPortMapNode == NULL)
    {
        return (IP_FAILURE);
    }

    u1CurrOperStatus = pPortMapNode->u1OperState;
    u1Ipv4EnableStatus = pPortMapNode->u1Ipv4EnableStatus;

    if (u1Ipv4EnableStatus != IPVX_IPV4_ENABLE_STATUS_UP)
    {
        /* Update the DS */
        pPortMapNode->u1OperState = u1OperStatus;
        return (IP_SUCCESS);
    }

    if (u1OperStatus == u1CurrOperStatus)
    {
        return (IP_SUCCESS);
    }

    i4RetVal =
        LinuxIpUpdateInterfaceStatus (pu1IfName, pPortMapNode->u1IfType,
                                      u1OperStatus);

    if (i4RetVal != OSIX_SUCCESS)
    {
        return IP_FAILURE;
    }

    /* Update the DS */
    pPortMapNode->u1OperState = u1OperStatus;

    IfaceParams.u2Port = u2Port;

    if (u1OperStatus == CFA_IF_UP)
    {
        /* to notify the oper change to the higher layers */
        LnxIpNotifyIfInfo (RTM_OPER_UP_MSG, &IfaceParams);

        MEMSET (&IpIntfInfo, 0, sizeof (tNetIpv4IfInfo));

        if (NetIpv4GetIfInfo (u2Port, &IpIntfInfo) == NETIPV4_SUCCESS)
        {
            if (IpIntfInfo.u4Addr != ZERO)
            {
                MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
                ArpQMsg.u4MsgType = CFA_GRAT_ARP_REQ;
                ArpQMsg.u4EncapType = CFA_ENCAP_ENETV2;
                ArpQMsg.pBuf = NULL;
                ArpQMsg.u2Port = u2Port;
                ArpQMsg.u4IpAddr = IpIntfInfo.u4Addr;

                if (ArpEnqueuePkt (&ArpQMsg) != ARP_SUCCESS)
                {
                    PRINTF ("\nIpIf: Gratituous Arp Sending is Failed\n");
                    return IP_FAILURE;
                }
            }
        }

    }
    else if (u1OperStatus == CFA_IF_DOWN)
    {
        LnxIpNotifyIfInfo (RTM_OPER_DOWN_MSG, &IfaceParams);
    }

    return IP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpDeInit                                      */
/*                                                                          */
/*    Description        : This function de-initialises the socket desc     */
/*                         used for communicating with the linux kernel     */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                            */
/****************************************************************************/
VOID
LnxIpDeInit (VOID)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    close (gai4SocketId[LNX_VRF_DEFAULT_CXT_ID]);
    gai4SocketId[LNX_VRF_DEFAULT_CXT_ID] = -1;
#else
    close (gi4socketid);
    gi4socketid = -1;
#endif
    LnxIpDeInitPortIfIdxMapTbl ();

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : LnxIpDeInitPortIfIdxMapTbl                       */
/*                                                                          */
/*    Description        : This function de-initialises LnxIpPort To        */
/*                         CfaIfIndex Map table maintained for linux IP     */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
LnxIpDeInitPortIfIdxMapTbl (VOID)
{

    TMO_HASH_Delete_Table (gpPortIfIdxMapTbl, LnxIpHashNodeDeleteFn);

    MemDeleteMemPool (gPortMapMemPoolId);

    gPortMapMemPoolId = 0;

    gpPortIfIdxMapTbl = NULL;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LnxIpHashNodeDeleteFn                            */
/*                                                                           */
/*    Description         : This function releases the memory for the given  */
/*                          Hashnide.                                        */
/*                                                                           */
/*    Input(s)            : pNode       - Hash Node.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
LnxIpHashNodeDeleteFn (tTMO_HASH_NODE * pNode)
{
    LnxIpReleaseBuff (LNXIP_PORT_MAP_NODE_BUFF, (UINT1 *) pNode);

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LnxIpGetPortMapHashIndex                         */
/*                                                                           */
/*    Description         : This function returns Hashindex for the given    */
/*                          u4IpPortNum;                                     */
/*                                                                           */
/*    Input(s)            : u4IpPortNum - IP Port Number.                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpPortIfIdxMapTbl                          */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : u4HashIndex                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
UINT4
LnxIpGetPortMapHashIndex (UINT4 u4IpPortNum)
{
    return ((u4IpPortNum % gpPortIfIdxMapTbl->u4_HashSize));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LnxIpAddPortToMappingTable                       */
/*                                                                           */
/*    Description         : This function adds IpPort-CfaIfIndex Map Node to */
/*                          Hash Table.                                      */
/*                                                                           */
/*    Input(s)            : u4CfaIfIndex - Cfa Interface Index               */
/*                        : u4IpPortNum  - IP Port Number.                   */
/*                        : u4IpAddr - IpAddr of that interface.             */
/*                        : u4NetMask - NetMask of that interface.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpPortIfIdxMapTbl                          */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : u4HashIndex                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
LnxIpAddPortToMappingTable (UINT4 u4CfaIfIndex, UINT4 u4IpPortNum,
                            UINT1 u1IfType)
{
    UINT4               u4HashIndex;
    UINT4               u4IfMtu;
    UINT4               u4IfSpeed;
    tLnxIpPortMapNode  *pPortMapNode;

    pPortMapNode =
        (tLnxIpPortMapNode *) (VOID *) LnxIpGetBuff (LNXIP_PORT_MAP_NODE_BUFF,
                                                     sizeof
                                                     (tLnxIpPortMapNode));

    if (NULL == pPortMapNode)
    {
        return OSIX_FAILURE;

    }

    TMO_HASH_Init_Node (&(pPortMapNode->SllNode));
    CfaGetIfMtu (u4CfaIfIndex, &u4IfMtu);
    CfaGetIfSpeed (u4CfaIfIndex, &u4IfSpeed);

    pPortMapNode->u4CfaIfIndex = u4CfaIfIndex;
    pPortMapNode->u4IpPortNum = u4IpPortNum;
    pPortMapNode->u4IpPortSpeed = u4IfSpeed;
    pPortMapNode->u4IpPortMtu = u4IfMtu;

    /* Set the Ipv4EnableStatus  as Enabled */
    pPortMapNode->u1Ipv4EnableStatus = IPVX_IPV4_ENABLE_STATUS_UP;
    pPortMapNode->u1IfType = u1IfType;
    pPortMapNode->u1OperState = CFA_IF_DOWN;

    u4HashIndex = LnxIpGetPortMapHashIndex (u4IpPortNum);

    TMO_HASH_Add_Node (gpPortIfIdxMapTbl, &(pPortMapNode->SllNode),
                       u4HashIndex, NULL);

    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LnxIpDeletePortFromMappingTable                  */
/*                                                                           */
/*    Description         : This function deletes IpPort-CfaIfIndex Map Node */
/*                          to Hash Table.                                   */
/*                                                                           */
/*    Input(s)            : u4CfaIfIndex - Cfa Interface Index               */
/*                        : u4IpPortNum  - IP Port Number.                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpPortIfIdxMapTbl                          */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : u4HashIndex                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
LnxIpDeletePortFromMappingTable (UINT4 u4IpPortNum)
{
    UINT4               u4HashIndex;
    tLnxIpPortMapNode  *pPortMapNode;

    pPortMapNode = LnxIpGetPortMapNode (u4IpPortNum);

    if (NULL == pPortMapNode)
    {
        return OSIX_FAILURE;
    }

    u4HashIndex = LnxIpGetPortMapHashIndex (u4IpPortNum);

    TMO_HASH_Delete_Node (gpPortIfIdxMapTbl, &(pPortMapNode->SllNode),
                          u4HashIndex);

    LnxIpReleaseBuff (LNXIP_PORT_MAP_NODE_BUFF, (UINT1 *) pPortMapNode);

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name    : LnxIpGetPortMapNode                               */
/*                                                                      */
/* Description      : Retrieves the Port Map Node Entry matching        */
/*                    the Ip Port Number.                               */
/*                                                                      */
/* Input(s)            : u4IpPortNum - IP Port Number.                  */
/*                                                                      */
/* Output(s)           : None                                           */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : gpPortIfIdxMapTbl                                 */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Pointer to the Gip Entry if entry is found        */
/*                    NULL otherwise.                                   */
/************************************************************************/
tLnxIpPortMapNode  *
LnxIpGetPortMapNode (UINT4 u4IpPortNum)
{
    UINT4               u4HashIndex;
    tLnxIpPortMapNode  *pPortMapNode;

    u4HashIndex = LnxIpGetPortMapHashIndex (u4IpPortNum);

    TMO_HASH_Scan_Bucket (gpPortIfIdxMapTbl, u4HashIndex, pPortMapNode,
                          tLnxIpPortMapNode *)
    {
        if (pPortMapNode->u4IpPortNum == u4IpPortNum)
        {
            return pPortMapNode;
        }
    }

    return NULL;
}

/************************************************************************/
/* Function Name    : LnxIpGetBuff ()                                   */
/*                                                                      */
/* Description      : Allocates a buffer of specified type              */
/*                                                                      */
/* Input(s)         : u1BufType - Type of the buffer to be allocated    */
/*                    u4Size    - Size of the buffer. This is used      */
/*                                only when the size of the requested   */
/*                                buffer is a variable.                 */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : Pointer to the buffer of the requested type       */
/*                        if buffer is available                        */
/*                    NULL otherwise                                    */
/************************************************************************/
UINT1              *
LnxIpGetBuff (UINT1 u1BufType, UINT4 u4Size)
{
    UINT1              *pu1Buf = NULL;

    UNUSED_PARAM (u4Size);

    switch (u1BufType)
    {

        case LNXIP_PORT_MAP_NODE_BUFF:
            pu1Buf = MemAllocMemBlk (gPortMapMemPoolId);
            break;

        default:
            return pu1Buf;
    }

    return pu1Buf;
}

/************************************************************************/
/* Function Name    : LnxIpReleaseBuff ()                               */
/*                                                                      */
/* Description      : Releases the given buffer.                        */
/*                                                                      */
/* Input(s)         : u1BufType - Type of the buffer to be released     */
/*                    pu1Buf    - Pointer to the buffer to be released  */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : void                                              */
/************************************************************************/
VOID
LnxIpReleaseBuff (UINT1 u1BufType, UINT1 *pu1Buf)
{
    switch (u1BufType)
    {

        case LNXIP_PORT_MAP_NODE_BUFF:
            MemReleaseMemBlock (gPortMapMemPoolId, pu1Buf);
            break;
        default:
            printf (" ");
            break;
    }
}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxIpNotifyIfInfo. 
 *
 * Input(s)           : pIfaceParams - Interface parameters given from cfa.
 *                      u1State - Interface up or down and route updation.
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action             : Invoked by CFA
 *                      Changes the UP/DOWN status of the interface in IP.
 *
 *
+-------------------------------------------------------------------*/
INT4
LnxIpNotifyIfInfo (UINT1 u1State, t_IFACE_PARAMS * pIfaceParams)
{
    tOsixMsg           *pRtmMsg;
    tRtmMsgHdr         *pRtmMsgHdr;
    tNetIpv4IfInfo      NetIpIfInfo;
    tLnxIpPortMapNode  *pPortMapNode = NULL;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
#endif

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    pPortMapNode = LnxIpGetPortMapNode (pIfaceParams->u2Port);

    if (pPortMapNode == NULL)
    {
        return IP_FAILURE;
    }

    if ((pRtmMsg =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tNetIpv4IfInfo), 0)) == NULL)
    {
        return IP_FAILURE;
    }

    pRtmMsgHdr = (tRtmMsgHdr *) IP_GET_MODULE_DATA_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = u1State;

    if (u1State == RTM_DELETE_INT_MSG || u1State == RTM_CREATE_INT_MSG)
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
        if (NetIpv4GetCfaIfIndexFromPort ((UINT4) pIfaceParams->u2Port,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            return (IP_FAILURE);
        }

        if (VcmGetContextIdFromCfaIfIndex (u4CfaIfIndex, &u4ContextId) ==
            VCM_FAILURE)
        {
            return IP_FAILURE;
        }
        NetIpIfInfo.u4ContextId = u4ContextId;
#endif
        NetIpIfInfo.u4IfIndex = (UINT4) pIfaceParams->u2Port;
        NetIpIfInfo.u4Mtu = pIfaceParams->u4Mtu;
        NetIpIfInfo.u4IfSpeed = pIfaceParams->u4IfSpeed;
        NetIpIfInfo.u4Addr = pIfaceParams->u4IpAddr;
        NetIpIfInfo.u4NetMask = pIfaceParams->u4SubnetMask;
        NetIpIfInfo.u4BcastAddr = pIfaceParams->u4BcastAddr;
        NetIpIfInfo.u4IfType = (UINT4) pIfaceParams->u1IfType;
        NetIpIfInfo.u4CfaIfIndex = (UINT4) pIfaceParams->u2IfIndex;
        gu2VlanId = pIfaceParams->u2VlanId;
        gu1IfType = pIfaceParams->u1IfType;
    }
    else if (u1State == RTM_OPER_UP_MSG || u1State == RTM_OPER_DOWN_MSG ||
             u1State == RTM_MTU_UPDATE_MSG || u1State == RTM_SPEED_UPDATE_MSG)
    {
        if (NetIpv4GetIfInfo ((UINT4) pIfaceParams->u2Port, &NetIpIfInfo)
            == NETIPV4_FAILURE)
        {
            IP_RELEASE_BUF (pRtmMsg, FALSE);
            return IP_FAILURE;
        }
        NetIpIfInfo.u4Mtu = pIfaceParams->u4Mtu;
        NetIpIfInfo.u4IfSpeed = pIfaceParams->u4IfSpeed;
        NetIpIfInfo.u4DhcpReleaseStatusFlag = pIfaceParams->u4ReleaseStatusFlag;
    }
    if (IP_COPY_TO_BUF (pRtmMsg, &NetIpIfInfo,
                        0, sizeof (tNetIpv4IfInfo)) == IP_BUF_FAILURE)
    {
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return IP_FAILURE;
    }

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) == IP_FAILURE)
    {
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return IP_FAILURE;
    }

    return IP_SUCCESS;
}

INT4
LinuxIpCreateLoopbackInterface (UINT1 *pu1DevName, UINT4 u4ContextId)
{
    INT4                i4SockId = -1;
    INT4                i4Err = -1;
    struct ifreq        if_req;

    if ((i4SockId = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
    {
        perror ("Raw socket creation fail");
        return OSIX_FAILURE;
    }

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_name, pu1DevName);

    if_req.ifr_addr.sa_family = AF_INET;

    if_req.ifr_flags = IFF_RUNNING;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    ioctl (gai4SocketId[u4ContextId], SIOCSIFFLAGS, &if_req);
#else
    ioctl (gi4socketid, SIOCSIFFLAGS, &if_req);
#endif

    ioctl (i4SockId, SIOCBRDELBR, pu1DevName);

    if (ioctl (i4SockId, SIOCBRADDBR, pu1DevName) < 0)
    {
        perror ("SIOCBRADDBR Failed");
        close (i4SockId);
        return OSIX_FAILURE;
    }

    /* Setting the interface down */

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_name, pu1DevName);

    if_req.ifr_addr.sa_family = AF_INET;

    if_req.ifr_flags = IFF_LOOPBACK;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    i4Err = ioctl (gai4SocketId[u4ContextId], SIOCSIFFLAGS, &if_req);
#else
    i4Err = ioctl (gi4socketid, SIOCSIFFLAGS, &if_req);
    UNUSED_PARAM (u4ContextId);
#endif

    if (i4Err < 0)
    {
        perror ("Setting interface as LOOPBACK");
        close (i4SockId);
        return OSIX_FAILURE;
    }

    close (i4SockId);
    return OSIX_SUCCESS;
}

INT4
LinuxIpDeleteLoopbackInterface (UINT1 *pu1DevName, UINT4 u4ContextID)
{

    INT4                i4SockId = -1;
    struct ifreq        if_req;
    UINT1               u1Len = 0;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfIfInfo      *pLnxVrfIfInfo = NULL;
#endif

    MEMSET (&if_req, 0, sizeof (if_req));
    u1Len = (UINT1) ((IFNAMSIZ > (STRLEN (pu1DevName))) ?
                     (STRLEN (pu1DevName)) : IFNAMSIZ - 1);
    STRNCPY (if_req.ifr_name, pu1DevName, u1Len);
    if_req.ifr_name[u1Len] = '\0';

    if ((i4SockId = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
    {
        perror ("Raw socket creation fail");
        return OSIX_FAILURE;
    }
    if_req.ifr_addr.sa_family = AF_INET;

    if_req.ifr_flags = IFF_RUNNING;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UNUSED_PARAM (gi4socketid);
    if ((ioctl (gai4SocketId[u4ContextID], SIOCSIFFLAGS, &if_req)) < 0)
#else
    UNUSED_PARAM (u4ContextID);
    if ((ioctl (gi4socketid, SIOCSIFFLAGS, &if_req)) < 0)
#endif
    {
        perror ("SIOCSIFFLAGS Failed");
        close (i4SockId);
        return OSIX_FAILURE;
    }

    if (ioctl (i4SockId, SIOCBRDELBR, pu1DevName) < 0)
    {
        perror ("SIOCBRADDBR Failed");
        close (i4SockId);
        return OSIX_FAILURE;
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    /*Remove LnxVrfIfInfo node */
    pLnxVrfIfInfo = LnxVrfIfInfoGet (pu1DevName);
    if (pLnxVrfIfInfo != NULL)
    {
        if (RBTreeRem (gLnxVrfIfInfoRBRoot, pLnxVrfIfInfo) == NULL)
        {
            return OSIX_FAILURE;
        }
        LNX_VRF_IFINFO_FREE (pLnxVrfIfInfo);
    }
#endif

    close (i4SockId);
    return OSIX_SUCCESS;
}

#ifdef LINUX_310_WANTED

/*******************************************************************************
 * Function Name   : LnxIpCreateNetSocket
 * Description     : Function to create a socket to netlink interface
 * Global Varibles :
 * Inputs          : u4Groups   - Groups
 * Output          : pNlHdl     - Pointer to Net Link Handle
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpCreateNetSocket (UINT4 u4Groups, struct nlhandle *pNlHdl)
{
    socklen_t           addr_len;
    INT4                i4Retval = 0;

    MEMSET (pNlHdl, 0, sizeof (pNlHdl));

    pNlHdl->fd = socket (AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);

    if (pNlHdl->fd < 0)
    {
        perror ("Netlink: Cannot open netlink socket");

        return NETIPV4_FAILURE;
    }

    i4Retval = fcntl (pNlHdl->fd, F_SETFL, O_NONBLOCK);

    if (i4Retval < 0)
    {
        perror ("Netlink: Cannot set netlink socket flags");

        close (pNlHdl->fd);

        return NETIPV4_FAILURE;
    }

    MEMSET (&pNlHdl->snl, 0, sizeof (pNlHdl->snl));

    pNlHdl->snl.nl_family = AF_NETLINK;
    pNlHdl->snl.nl_groups = u4Groups;

    i4Retval = bind (pNlHdl->fd, (struct sockaddr *) &pNlHdl->snl,
                     sizeof (pNlHdl->snl));

    if (i4Retval < 0)
    {
        perror ("Netlink: Cannot bind netlink socket");

        close (pNlHdl->fd);

        return NETIPV4_FAILURE;
    }

    addr_len = sizeof (pNlHdl->snl);
    i4Retval = getsockname (pNlHdl->fd, (struct sockaddr *) &pNlHdl->snl,
                            &addr_len);

    if (i4Retval < 0 || addr_len != sizeof (pNlHdl->snl))
    {
        perror ("Netlink: Cannot getsockname");

        close (pNlHdl->fd);
        return NETIPV4_FAILURE;
    }

    if (pNlHdl->snl.nl_family != AF_NETLINK)
    {
        perror ("Netlink: Wrong address family");
        close (pNlHdl->fd);
        return NETIPV4_FAILURE;
    }

    pNlHdl->seq = time (NULL);

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetClose
 * Description     : Function to close a netlink socket
 * Global Varibles :
 * Inputs          : pNlHdl    - Pointer to Net Link Handle
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetClose (struct nlhandle *pNlHdl)
{
    close (pNlHdl->fd);

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetLinkAddAddAttr
 * Description     : Function to add address attribute to Netlink msg header
 * Global Varibles :
 * Inputs          : pNlHdr     - Pointer to Netlink Message Header
 *                   i4MaxLen   - Max Len
 *                   i4Type     - Type
 *                   pData      - Data
 *                   i4Len      - Length
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS/
 *                   NETIPV4_FAILURE
 ******************************************************************************/
INT4
LnxIpNetLinkAddAddAttr (struct nlmsghdr *pNlHdr, INT4 i4MaxLen, INT4 i4Type,
                        VOID *pData, INT4 i4Len)
{
    INT4                i4ByteLen = 0;
    struct rtattr      *pRtA = NULL;

    i4ByteLen = RTA_LENGTH (i4Len);

    if ((NLMSG_ALIGN (pNlHdr->nlmsg_len) + i4ByteLen) > (UINT4) i4MaxLen)
    {
        return NETIPV4_FAILURE;
    }

    pRtA = (struct rtattr *) (VOID *) (((INT1 *) pNlHdr) +
                                       NLMSG_ALIGN (pNlHdr->nlmsg_len));
    /* Construct TLV */
    /* Type of data in the route */
    pRtA->rta_type = i4Type;
    /* Data len */
    pRtA->rta_len = i4ByteLen;
    /* Value  */
    MEMCPY (RTA_DATA (pRtA), pData, i4Len);

    /* total packet length = prev length + added length */
    pNlHdr->nlmsg_len = NLMSG_ALIGN (pNlHdr->nlmsg_len) + i4ByteLen;

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetTalkToKernel
 * Description     : Function to send message to netlink kernel socket,
 *                   then receive response
 * Global Varibles :
 * Inputs          : pNlHdl     - Pointer to Netlink Handle
 *                   pNlHdr     - Pointer to Netlink Header
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS/
 *                   NETIPV4_FAILURE
 ******************************************************************************/
INT4
LnxIpNetTalkToKernel (struct nlhandle *pNlHdl, struct nlmsghdr *pNlHdr)
{
    struct sockaddr_nl  snl;
    struct iovec        iov = { (VOID *) pNlHdr, pNlHdr->nlmsg_len };
    struct msghdr       msg =
        { (VOID *) &snl, sizeof snl, &iov, 1, NULL, 0, 0 };
    INT4                i4Retval = 0;
    INT4                i4Flags = 0;

    MEMSET (&snl, 0, sizeof snl);
    snl.nl_family = AF_NETLINK;
    snl.nl_pid = 0;
    snl.nl_groups = 0;

    pNlHdr->nlmsg_seq = ++pNlHdl->seq;

    /* Request Netlink acknowledgement */
    pNlHdr->nlmsg_flags |= NLM_F_ACK;

    /* Send message to netlink interface. */
    i4Retval = sendmsg (pNlHdl->fd, &msg, 0);

    if (i4Retval < 0)
    {
        perror ("Netlink: Talking to Kernel failed");

        return NETIPV4_FAILURE;
    }

    /* Set blocking flag */
    i4Retval = LnxIpNetSetBlock (pNlHdl, &i4Flags);

    if (i4Retval < 0)
    {
        perror ("Netlink: Warning, Couldn't set blocking flag to netlink "
                "socket...");
    }

    i4Retval = LnxIpNetParseInfo (LnxIpNetTalkFilter, pNlHdl, pNlHdr);

    /* Restore previous flags */
    if (i4Retval == 0)
    {
        LnxIpNetSetNonBlock (pNlHdl, &i4Flags);
    }

    return i4Retval;
}

/*******************************************************************************
 * Function Name   : LnxIpAddAttr32
 * Description     : Function to iproute2 utility function
 * Global Varibles :
 * Inputs          : pNlHdr      - Pointer to Netlink Header
 *                   i4MaxLen    - Max Len
 *                   i4Type      - Type
 *                   u4Data      - Data
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpAddAttr32 (struct nlmsghdr *pNlHdr, INT4 i4Maxlen, INT4 i4Type,
                UINT4 u4Data)
{
    struct rtattr      *pRta = NULL;
    INT4                i4Len = RTA_LENGTH (4);

    if (((INT4) NLMSG_ALIGN (pNlHdr->nlmsg_len)) + i4Len > i4Maxlen)
    {
        return NETIPV4_FAILURE;
    }

    pRta = (struct rtattr *)
        (((CHR1 *) pNlHdr) + NLMSG_ALIGN (pNlHdr->nlmsg_len));

    pRta->rta_type = i4Type;
    pRta->rta_len = i4Len;

    MEMCPY (RTA_DATA (pRta), &u4Data, sizeof (UINT4));

    pNlHdr->nlmsg_len = NLMSG_ALIGN (pNlHdr->nlmsg_len) + i4Len;

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpParseRtattr
 * Description     : Function to parse Rta attributes
 * Global Varibles :
 * Inputs          : tb        - Pointer to Table B
 *                   i4MaxLen  - Max Len
 *                   rta       - Pointer to Rta Attribute
 *                   i4Len     - Length
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/

VOID
LnxIpParseRtattr (struct rtattr **tb, INT4 i4MaxLen, struct rtattr *rta,
                  INT4 i4Len)
{
    while (RTA_OK (rta, i4Len))
    {
        if (rta->rta_type <= i4MaxLen)
        {
            tb[rta->rta_type] = rta;
        }

        rta = RTA_NEXT (rta, i4Len);
    }

    return;
}

/*******************************************************************************
 * Function Name   : LnxIpNetTalkFilter
 * Description     : Function to talk filter
 * Global Varibles :
 * Inputs          : Snl       - Socket Netlink Layer Address
 *                   pNlHdr    - Pointer to Netlink Layer Header
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetTalkFilter (struct sockaddr_nl *Snl, struct nlmsghdr *pNlHdr)
{
    UNUSED_PARAM (Snl);
    UNUSED_PARAM (pNlHdr);

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetRequest
 * Description     : Function to Fetch a specific type information
 *                   from netlink kernel
 * Global Varibles :
 * Inputs          : pNlHdl    - Pointer to Netlink Handle
 *                   i4Family  - Family
 *                   i4Type    - Type
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetRequest (struct nlhandle *pNlHdl, INT4 i4Family, INT4 i4Type)
{
    struct sockaddr_nl  snl;
    struct
    {
        struct nlmsghdr     nlh;
        struct rtgenmsg     g;
    } NlReq;
    INT4                i4RetVal = 0;

    MEMSET (&snl, 0, sizeof (snl));
    snl.nl_family = AF_NETLINK;

    NlReq.nlh.nlmsg_len = sizeof (NlReq);
    NlReq.nlh.nlmsg_type = i4Type;
    NlReq.nlh.nlmsg_flags = NLM_F_ROOT | NLM_F_MATCH | NLM_F_REQUEST;
    NlReq.nlh.nlmsg_pid = 0;
    NlReq.nlh.nlmsg_seq = ++pNlHdl->seq;
    NlReq.g.rtgen_family = i4Family;

    i4RetVal = sendto (pNlHdl->fd, (VOID *) &NlReq, sizeof (NlReq), 0,
                       (struct sockaddr *) &snl, sizeof (snl));

    if (i4RetVal < 0)
    {
        perror ("Netlink: sendto() failed");

        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetIfAddFilter
 * Description     : Function to add Netlink interface address lookup filter
 *
 * Global Varibles :
 * Inputs          : Snl           - Socket Netlink Layer Address
 *                   pNlHdr        - Pointer to Netlink Header
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetIfAddFilter (struct sockaddr_nl *Snl, struct nlmsghdr *pNlHdr)
{
    struct ifaddrmsg   *ifa;
    struct rtattr      *tb[IFA_MAX + 1];
    INT4                i4Len;
    VOID               *addr;

    UNUSED_PARAM (Snl);
    ifa = NLMSG_DATA (pNlHdr);

    /* Only IPV4 are valid us */
    if (ifa->ifa_family != AF_INET && ifa->ifa_family != AF_INET6)
        return NETIPV4_SUCCESS;

    if (pNlHdr->nlmsg_type != RTM_NEWADDR && pNlHdr->nlmsg_type != RTM_DELADDR)
        return NETIPV4_SUCCESS;

    i4Len = pNlHdr->nlmsg_len - NLMSG_LENGTH (sizeof (struct ifaddrmsg));
    if (i4Len < 0)
        return NETIPV4_FAILURE;

    MEMSET (tb, 0, sizeof (tb));
    LnxIpParseRtattr (tb, IFA_MAX, IFA_RTA (ifa), i4Len);

    if (tb[IFA_LOCAL] == NULL)
        tb[IFA_LOCAL] = tb[IFA_ADDRESS];
    if (tb[IFA_ADDRESS] == NULL)
        tb[IFA_ADDRESS] = tb[IFA_LOCAL];

    /* local interface address */
    addr = (tb[IFA_LOCAL] ? RTA_DATA (tb[IFA_LOCAL]) : NULL);

    if (addr == NULL)
        return NETIPV4_FAILURE;

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetAddLookup
 * Description     : Function to Adresses lookup bootstrap function
 * Global Varibles :
 * Inputs          :
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetAddLookup (VOID)
{
    struct nlhandle     nlh;
    INT4                status = 0;
    INT4                i4Retval, i4Flags;

    if (LnxIpCreateNetSocket (0, &nlh) < 0)
    {
        return NETIPV4_FAILURE;
    }

    /* Set blocking flag */
    i4Retval = LnxIpNetSetBlock (&nlh, &i4Flags);

    if (i4Retval < 0)
    {
        perror ("Setting Netlink socket to Blocking mode failed\r\n");
    }

    /* IPv4 Address lookup */
    if (LnxIpNetRequest (&nlh, AF_INET, RTM_GETADDR) < 0)
    {
        LnxIpNetClose (&nlh);
        return NETIPV4_FAILURE;
    }

    status = LnxIpNetParseInfo (LnxIpNetIfAddFilter, &nlh, NULL);

    /* IPv6 Address lookup */
    if (LnxIpNetRequest (&nlh, AF_INET6, RTM_GETADDR) < 0)
    {
        LnxIpNetClose (&nlh);

        return NETIPV4_FAILURE;
    }

    status = LnxIpNetParseInfo (LnxIpNetIfAddFilter, &nlh, NULL);

    return status;
}

/*******************************************************************************
 * Function Name   : LnxIpKernelNetLinkInit
 * Description     : Function to initialize netlink socket for VRRP
 * Global Varibles :
 * Inputs          :
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
VOID
LnxIpVrrpKernelNetLinkInit (VOID)
{
    UINT4               u4Groups = 0;
    CHR1                ac1Command[LIP4_LINE_LEN];

    /* First remove the IP Tables already created by us. Do the following to
     * remove the ip table entries.
     *    1. Flush the IP Table Chain "ARICENT" to remove rules created.
     *    2. Delink the IP Table Chain "ARICENT" from standard "INPUT" chain.
     *    3. Delete the Chain
     */
    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP4_IPTBL_FLUSH_CHAIN);
    system (ac1Command);

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP4_IPTBL_DELINK_CHAIN);
    system (ac1Command);

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP4_IPTBL_DELETE_CHAIN);
    system (ac1Command);

    /* Now, Create the IP Table Chain for ARICENT. IP Tables to be added
     * only on this chain. Do the followin to create chain.
     *    1. Create Chain
     *    2. Link the chain with standard "INPUT" chain.
     */
    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP4_IPTBL_CREATE_CHAIN);
    system (ac1Command);

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s", LIP4_IPTBL_LINK_CHAIN);
    system (ac1Command);

    /* Start with a netlink address lookup */
    LnxIpNetAddLookup ();

    /*
     * Prepare netlink kernel broadcast channel
     * subscribtion. We subscribe to LINK and ADDR
     * netlink broadcast messages.
     */
    u4Groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR;

    LnxIpCreateNetSocket (u4Groups, &Nlkernel);

    if (Nlkernel.fd < 0)
    {
        perror ("Error while registering Kernel netlink reflector channel");
    }

    /* Prepare netlink command channel. */
    LnxIpCreateNetSocket (0, &Nlcmd);

    if (Nlcmd.fd < 0)
    {
        perror ("Error while registering Kernel netlink cmd channel");
    }

    return;
}

/*******************************************************************************
 * Function Name   : LnxIpKernelNetLinkClose
 * Description     : Function to close netlink socket for VRRP
 * Global Varibles :
 * Inputs          :
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
VOID
LnxIpKernelNetLinkClose (VOID)
{
    LnxIpNetClose (&Nlkernel);

    LnxIpNetClose (&Nlcmd);

    return;
}

/*******************************************************************************
 * Function Name   : LnxIpNetSetBlock
 * Description     : Function to Set netlink socket channel as blocking
 * Global Varibles :
 * Inputs          : pNlHdl     - Pointer to Netlink Handle
 *                   *pi4Flags  - Flags
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetSetBlock (struct nlhandle *pNlHdl, INT4 *pi4Flags)
{
    if ((*pi4Flags = fcntl (pNlHdl->fd, F_GETFL, 0)) < 0)
    {
        perror ("Netlink: Cannot F_GETFL socket");

        return NETIPV4_FAILURE;
    }

    *pi4Flags &= ~O_NONBLOCK;
    if (fcntl (pNlHdl->fd, F_SETFL, *pi4Flags) < 0)
    {
        perror ("Netlink: Cannot F_SETFL socket ");

        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetParseInfo
 * Description     : Function to parse information from Net Link Socket
 * Global Varibles :
 * Inputs          : (*filter) - Call Back Function Pointer
 *                   pNlHdl    - Pointer to Netlink Handle
 *                   pNlHdr    - Pointer to Netlink Header
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/

INT4
LnxIpNetParseInfo (INT4 (*filter) (struct sockaddr_nl *, struct nlmsghdr *),
                   struct nlhandle *pNlHdl, struct nlmsghdr *pNlHdr)
{
    INT4                i4Status = 0;
    INT4                i4Retval = 0;
    INT4                i4Error = 0;
    static UINT1        au1buf[4096];
    struct iovec        iov = { au1buf, sizeof (au1buf) };
    struct sockaddr_nl  NLSockAddr;
    struct msghdr       MsgHdr
        = { (VOID *) &NLSockAddr, sizeof (NLSockAddr), &iov, 1, NULL, 0, 0 };
    struct nlmsghdr    *pNLMsgHdr = NULL;

    while (1)
    {
        i4Status = recvmsg (pNlHdl->fd, &MsgHdr, 0);

        if (i4Status < 0)
        {
            if (errno == EINTR)
            {
                continue;
            }

            if (errno == EWOULDBLOCK || errno == EAGAIN)
            {
                break;
            }

            continue;
        }

        if (i4Status == 0)
        {
            return NETIPV4_FAILURE;
        }

        if (MsgHdr.msg_namelen != sizeof (NLSockAddr))
        {
            return NETIPV4_FAILURE;
        }

        for (pNLMsgHdr = (struct nlmsghdr *) au1buf;
             NLMSG_OK (pNLMsgHdr, (UINT4) i4Status);
             pNLMsgHdr = NLMSG_NEXT (pNLMsgHdr, i4Status))
        {
            /* Finish of reading. */
            if (pNLMsgHdr->nlmsg_type == NLMSG_DONE)
            {
                return i4Retval;
            }

            /* Error handling. */
            if (pNLMsgHdr->nlmsg_type == NLMSG_ERROR)
            {
                struct nlmsgerr    *pErr
                    = (struct nlmsgerr *) NLMSG_DATA (pNLMsgHdr);

                /*
                 * If error == 0 then this is a netlink ACK.
                 * return if not related to multipart message.
                 */
                if (pErr->error == 0)
                {
                    if (!(pNLMsgHdr->nlmsg_flags & NLM_F_MULTI))
                    {
                        return NETIPV4_SUCCESS;
                    }

                    continue;
                }

                if (pNLMsgHdr->nlmsg_len <
                    NLMSG_LENGTH (sizeof (struct nlmsgerr)))
                {
                    return NETIPV4_FAILURE;
                }

                if (pNlHdr && (pErr->error == -EEXIST) &&
                    ((pNlHdr->nlmsg_type == RTM_NEWROUTE) ||
                     (pNlHdr->nlmsg_type == RTM_NEWADDR)))
                {
                    return NETIPV4_SUCCESS;
                }

                return NETIPV4_FAILURE;
            }

            /* Skip unsolicited messages from cmd channel */
            if (pNlHdl != &Nlcmd && pNLMsgHdr->nlmsg_pid == Nlcmd.snl.nl_pid)
            {
                continue;
            }

            i4Error = (*filter) (&NLSockAddr, pNLMsgHdr);

            if (i4Error < 0)
            {
                i4Retval = i4Error;
            }
        }

        if (MsgHdr.msg_flags & MSG_TRUNC)
        {
            continue;
        }

        if (i4Status)
        {
            return NETIPV4_FAILURE;
        }
    }

    return i4Retval;
}

/*******************************************************************************
 * Function Name   : LnxIpNetSetNonBlock
 * Description     : Function to Set netlink socket channel as non-blocking
 * Global Varibles :
 * Inputs          : pNlHdl      - Pointer to Netlink Handle
 *                   pi4Flags    - Flags
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetSetNonBlock (struct nlhandle *pNlHdl, INT4 *pi4Flags)
{
    *pi4Flags |= O_NONBLOCK;

    if (fcntl (pNlHdl->fd, F_SETFL, *pi4Flags) < 0)
    {
        perror ("Netlink: Cannot F_SETFL socket");

        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetLinkSetVmacAddr
 * Description     : Function to set the VMAC address on VIF(Link layer handling)
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetLinkSetVmacAddr (tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;
    UINT1               au1VMac[MAC_ADDR_LEN] =
        { 0x00, 0x00, 0x5e, 0x00, 0x01, 0x00 };

    MEMSET (&NlReq, 0, sizeof (NlReq));

    au1VMac[MAC_ADDR_LEN - 1] = (UINT1) (pVrrpNwIntf->u4VrId);

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST;
    NlReq.n.nlmsg_type = RTM_NEWLINK;
    NlReq.ifi.ifi_family = AF_INET;

    NlReq.ifi.ifi_index = pVrrpNwIntf->u4LnxIpPortNum;

    LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_ADDRESS,
                            au1VMac, MAC_ADDR_LEN);

    if (LnxIpNetTalkToKernel (&Nlcmd, &NlReq.n) < 0)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetLinkSetMode
 * Description     : Function to Set the MAC mode
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetLinkSetMode (tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;
    struct rtattr      *pLinkInfo = NULL;
    struct rtattr      *pData = NULL;

    MEMSET (&NlReq, 0, sizeof (NlReq));

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST;
    NlReq.n.nlmsg_type = RTM_NEWLINK;
    NlReq.ifi.ifi_family = AF_INET;
    NlReq.ifi.ifi_index = pVrrpNwIntf->u4LnxIpPortNum;

    pLinkInfo = NLMSG_TAIL (&NlReq.n);

    LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_LINKINFO, NULL, 0);
    LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_INFO_KIND,
                            ((VOID *) ll_kind), STRLEN (ll_kind));

    pData = NLMSG_TAIL (&NlReq.n);
    LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_INFO_DATA, NULL, 0);

    LnxIpAddAttr32 (&NlReq.n, sizeof (NlReq), IFLA_MACVLAN_MODE,
                    MACVLAN_MODE_PRIVATE);

    pData->rta_len = (VOID *) NLMSG_TAIL (&NlReq.n) - (VOID *) pData;
    pLinkInfo->rta_len = (VOID *) NLMSG_TAIL (&NlReq.n) - (VOID *) pLinkInfo;

    if (LnxIpNetTalkToKernel (&Nlcmd, &NlReq.n) < 0)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetLinkUp
 * Description     : Function to make VIF up in kernel
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetLinkUp (tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;

    MEMSET (&NlReq, 0, sizeof (NlReq));

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST;
    NlReq.n.nlmsg_type = RTM_NEWLINK;
    NlReq.ifi.ifi_family = AF_UNSPEC;
    NlReq.ifi.ifi_index = pVrrpNwIntf->u4LnxIpPortNum;
    NlReq.ifi.ifi_change |= IFF_UP;
    NlReq.ifi.ifi_flags |= IFF_UP;

    if (LnxIpNetTalkToKernel (&Nlcmd, &NlReq.n) < 0)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : VrrpNetLinkCreateVif
 * Description     : Function to Create VIF in Linux
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info
 * Output          : pu1DevName     - Device Name created for VRRP Interface
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetLinkCreateVif (UINT1 *pu1DevName, tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;
    struct rtattr      *pLinkInfo = NULL;
    CHR1                ac1IfName[IP_PORT_NAME_LEN];
    UINT1               au1IfName[IP_PORT_NAME_LEN];

    MEMSET (&NlReq, 0, sizeof (NlReq));
    MEMSET (ac1IfName, 0, IP_PORT_NAME_LEN);
    MEMSET (au1IfName, 0, IP_PORT_NAME_LEN);

    CfaGetIfName (pVrrpNwIntf->u4IfIndex, au1IfName);

    SNPRINTF (ac1IfName, IP_PORT_NAME_LEN, "vrrp.%d.%d",
              pVrrpNwIntf->u4VrId, pVrrpNwIntf->u1AddrType);

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL;
    NlReq.n.nlmsg_type = RTM_NEWLINK;
    NlReq.ifi.ifi_family = AF_INET;

    pLinkInfo = NLMSG_TAIL (&NlReq.n);

    LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_LINKINFO, NULL, 0);

    LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_INFO_KIND,
                            ((VOID *) ll_kind), STRLEN (ll_kind));

    pLinkInfo->rta_len = (VOID *) NLMSG_TAIL (&NlReq.n) - (VOID *) pLinkInfo;

    LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_LINK,
                            &pVrrpNwIntf->u4Port, sizeof (UINT4));

    LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_IFNAME, ac1IfName,
                            STRLEN (ac1IfName));

    MEMCPY (pu1DevName, ac1IfName, sizeof (ac1IfName));

    if (LnxIpNetTalkToKernel (&Nlcmd, &NlReq.n) < 0)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpNetLinkDelVmac
 * Description     : Function to unregister the VIF with ethernet
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpNetLinkDelVmac (tVrrpNwIntf * pVrrpNwIntf)
{
    tNtPktHdr           NlReq;

    MEMSET (&NlReq, 0, sizeof (NlReq));

    NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
    NlReq.n.nlmsg_flags = NLM_F_REQUEST;
    NlReq.n.nlmsg_type = RTM_DELLINK;
    NlReq.ifi.ifi_family = AF_INET;
    NlReq.ifi.ifi_index = pVrrpNwIntf->u4LnxIpPortNum;

    if (LnxIpNetTalkToKernel (&Nlcmd, &NlReq.n) < 0)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

#endif
/*******************************************************************************
 * Function Name   : LnxIpVifConfigInLnx
 * Description     : Function to add VRRP Virtual interface and MAC to Kernel
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpVrrpVifConfigInLnx (tVrrpNwIntf * pVrrpNwIntf)
{
#ifdef LINUX_310_WANTED
    tLnxIpPortParams    LnxIpPortParams;
    tIpConfigInfo       IpInfo;
    CHR1                ac1Command[LIP4_LINE_LEN];
    UINT1               au1IfName[IP_PORT_NAME_LEN];
    UINT1               au1DevName[IP_PORT_NAME_LEN];
    INT4                i4VifPort;
    UINT1               u1ValZero = 0;
    UINT1               u1ValOne = 1;
    UINT1               u1ValEight = 8;

    MEMSET (&LnxIpPortParams, 0, sizeof (tLnxIpPortParams));
    MEMSET (&IpInfo, 0, sizeof (IpInfo));
    MEMSET (ac1Command, 0, LIP4_LINE_LEN);
    MEMSET (au1IfName, 0, IP_PORT_NAME_LEN);
    MEMSET (au1DevName, 0, IP_PORT_NAME_LEN);

    if (LnxIpNetLinkCreateVif (au1DevName, pVrrpNwIntf) != NETIPV4_SUCCESS)
    {
        return NETIPV4_FAILURE;
    }

    SNPRINTF ((CHR1 *) au1IfName, sizeof (au1IfName), "%s", au1DevName);

    if (LinuxIpGetInterfaceIndex (au1IfName, &i4VifPort) != OSIX_SUCCESS)
    {
        return NETIPV4_FAILURE;
    }

    pVrrpNwIntf->u4LnxIpPortNum = i4VifPort;

    LnxIpNetLinkSetVmacAddr (pVrrpNwIntf);

    LnxIpNetLinkUp (pVrrpNwIntf);

    MEMCPY (&LnxIpPortParams.u4IpAddr,
            pVrrpNwIntf->VirtualIp.au1Addr, sizeof (UINT4));
    LnxIpPortParams.u4IpAddr = OSIX_NTOHL (LnxIpPortParams.u4IpAddr);

    LnxIpPortParams.u2UpdateMask = (UINT2) (LNX_IPADDR_UPDATE);

    LinuxIpUpdateInterfaceParams (au1IfName, LnxIpPortParams);

    SNPRINTF (ac1Command, sizeof (ac1Command), "echo %d > %s/%s/%s",
              u1ValOne, LIP4_PROC_CONF, au1IfName, LIP4_PROC_ACCEPTLOCAL);
    system (ac1Command);

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "echo %d > %s/%s/%s",
              u1ValZero, LIP4_PROC_CONF, au1IfName, LIP4_PROC_RPFILTER);
    system (ac1Command);

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "echo %d > %s/%s/%s",
              u1ValOne, LIP4_PROC_CONF, au1IfName, LIP4_PROC_ARPIGNORE);
    system (ac1Command);

    CfaGetIfName (pVrrpNwIntf->u4IfIndex, au1IfName);

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "echo %d > %s/%s/%s",
              u1ValOne, LIP4_PROC_CONF, au1IfName, LIP4_PROC_ARPIGNORE);
    system (ac1Command);

    if ((pVrrpNwIntf->b1IsIpvXOwner == TRUE) ||
        (pVrrpNwIntf->b1IsOwnerPresent == TRUE))
    {
        MEMSET (ac1Command, 0, LIP4_LINE_LEN);

        SNPRINTF (ac1Command, sizeof (ac1Command), "echo %d > %s/%s/%s",
                  u1ValEight, LIP4_PROC_CONF, au1IfName, LIP4_PROC_ARPIGNORE);
        system (ac1Command);
    }

    if ((pVrrpNwIntf->b1IsIpvXOwner == FALSE) &&
        (pVrrpNwIntf->u1AcceptMode == FALSE))
    {
        LnxIpVrrpAddDropFilter (pVrrpNwIntf->VirtualIp);
    }

    LnxIpNetLinkSetMode (pVrrpNwIntf);

#else
    UNUSED_PARAM (pVrrpNwIntf);
#endif
    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpVrrpVifDeleteInLnx
 * Description     : Function to remove VRRP Virtual Interface from kernel
 * Global Varibles :
 * Inputs          : pVrrpNwIntf    - Pointer to Vrrp Network Interface Info
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS.
 *                   NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpVrrpVifDeleteInLnx (tVrrpNwIntf * pVrrpNwIntf)
{
#ifdef LINUX_310_WANTED
    CHR1                ac1Command[LIP4_LINE_LEN];
    UINT1               au1IfName[IP_PORT_NAME_LEN];
    UINT1               u1ValOne = 1;

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);
    MEMSET (au1IfName, 0, IP_PORT_NAME_LEN);

    if (LnxIpNetLinkDelVmac (pVrrpNwIntf) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    CfaGetIfName (pVrrpNwIntf->u4IfIndex, au1IfName);

    if ((pVrrpNwIntf->b1IsOwnerPresent == FALSE) &&
        (pVrrpNwIntf->b1IsIpvXOwner == TRUE))
    {
        SNPRINTF (ac1Command, sizeof (ac1Command), "echo %d > %s/%s/%s",
                  u1ValOne, LIP4_PROC_CONF, au1IfName, LIP4_PROC_ARPIGNORE);
        system (ac1Command);
    }

    if ((pVrrpNwIntf->b1IsIpvXOwner == FALSE) &&
        (pVrrpNwIntf->u1AcceptMode == FALSE))
    {
        LnxIpVrrpDelDropFilter (pVrrpNwIntf->VirtualIp);
    }
#else
    UNUSED_PARAM (pVrrpNwIntf);
#endif

    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpVrrpAddSecondaryIp
 * Description     : This function add a secondary IP address to VRRP interface
 * Global Varibles :
 * Inputs          : pVrrpNwIntf   - Pointer to Vrrp Nw Interface Information
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/
VOID
LnxIpVrrpAddSecondaryIp (tVrrpNwIntf * pVrrpNwIntf)
{
#ifdef LINUX_310_WANTED
    tIpConfigInfo       IpInfo;
    UINT1               au1IfName[IP_PORT_NAME_LEN];
    UINT4               u4Flag = 0;

    MEMSET (&IpInfo, 0, sizeof (IpInfo));
    MEMSET (au1IfName, 0, IP_PORT_NAME_LEN);

    u4Flag = CFA_IP_IF_SECONDARY_ADDR_ADD;

    SNPRINTF ((CHR1 *) au1IfName, sizeof (au1IfName), "vrrp.%d.%d:%d",
              pVrrpNwIntf->u4VrId, pVrrpNwIntf->u1AddrType,
              pVrrpNwIntf->u1SecIndex);

    MEMCPY (&IpInfo.u4Addr, pVrrpNwIntf->IpvXAddr.au1Addr, sizeof (UINT4));
    IpInfo.u4Addr = OSIX_NTOHL (IpInfo.u4Addr);
    IpInfo.u4NetMask = 0;

    LnxIpUpdateSecondaryIpInfo (au1IfName, &IpInfo, u4Flag);

    if ((pVrrpNwIntf->b1IsIpvXOwner == FALSE) &&
        (pVrrpNwIntf->u1AcceptMode == FALSE))
    {
        LnxIpVrrpAddDropFilter (pVrrpNwIntf->IpvXAddr);
    }
#else
    UNUSED_PARAM (pVrrpNwIntf);
#endif

    return;
}

/*******************************************************************************
 * Function Name   : LnxIpVrrpDelSecondaryIp
 * Description     : This function deletes secondary IP address from VRRP interface
 * Global Varibles :
 * Inputs          : pVrrpNwIntf   - Pointer to Vrrp Nw Interface Information
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/
VOID
LnxIpVrrpDelSecondaryIp (tVrrpNwIntf * pVrrpNwIntf)
{
#ifdef LINUX_310_WANTED
    tIpConfigInfo       IpInfo;
    UINT1               au1IfName[IP_PORT_NAME_LEN];
    UINT4               u4Flag = 0;

    MEMSET (&IpInfo, 0, sizeof (IpInfo));
    MEMSET (au1IfName, 0, IP_PORT_NAME_LEN);

    u4Flag = CFA_IP_IF_SECONDARY_ADDR_DEL;

    SNPRINTF ((CHR1 *) au1IfName, sizeof (au1IfName), "vrrp.%d.%d:%d",
              pVrrpNwIntf->u4VrId, pVrrpNwIntf->u1AddrType,
              pVrrpNwIntf->u1SecIndex);

    MEMCPY (&IpInfo.u4Addr, pVrrpNwIntf->IpvXAddr.au1Addr, sizeof (UINT4));
    IpInfo.u4Addr = OSIX_NTOHL (IpInfo.u4Addr);
    IpInfo.u4NetMask = 0;

    LnxIpUpdateSecondaryIpInfo (au1IfName, &IpInfo, u4Flag);

    if ((pVrrpNwIntf->b1IsIpvXOwner == FALSE) &&
        (pVrrpNwIntf->u1AcceptMode == FALSE))
    {
        LnxIpVrrpDelDropFilter (pVrrpNwIntf->IpvXAddr);
    }

#else
    UNUSED_PARAM (pVrrpNwIntf);
#endif

    return;
}

/*******************************************************************************
 * Function Name   : LnxIpVrrpAddDropFilter
 * Description     : This function adds a filter to drop IP packets for the 
 *                   IP mentioned.
 * Global Varibles :
 * Inputs          : IpAddr   - IP Address
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/
VOID
LnxIpVrrpAddDropFilter (tIPvXAddr IpAddr)
{
#ifdef LINUX_310_WANTED
    CHR1                ac1Command[LIP4_LINE_LEN];

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s %d.%d.%d.%d %s",
              LIP4_IPTBL_ADD,
              IpAddr.au1Addr[0], IpAddr.au1Addr[1],
              IpAddr.au1Addr[2], IpAddr.au1Addr[3], LIP4_IPTBL_DROP);
    system (ac1Command);

#else
    UNUSED_PARAM (IpAddr);
#endif

    return;
}

/*******************************************************************************
 * Function Name   : LnxIpVrrpDelDropFilter
 * Description     : This function deletes a filter to drop IP packets for the 
 *                   IP mentioned.
 * Global Varibles :
 * Inputs          : IpAddr   - IP Address
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/
VOID
LnxIpVrrpDelDropFilter (tIPvXAddr IpAddr)
{
#ifdef LINUX_310_WANTED
    CHR1                ac1Command[LIP4_LINE_LEN];

    MEMSET (ac1Command, 0, LIP4_LINE_LEN);

    SNPRINTF (ac1Command, sizeof (ac1Command), "%s %d.%d.%d.%d %s",
              LIP4_IPTBL_DEL,
              IpAddr.au1Addr[0], IpAddr.au1Addr[1],
              IpAddr.au1Addr[2], IpAddr.au1Addr[3], LIP4_IPTBL_DROP);
    system (ac1Command);

#else
    UNUSED_PARAM (IpAddr);
#endif

    return;
}

/*******************************************************************************
 * Function Name   : LnxIpDeleteIpAddrOnRestart
 * Description     : This function deletes ip address assigned on interface
 *                   when ISS is restarted.
 *
 * Global Varibles :
 * Inputs          : None
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/
VOID
LnxIpDeleteIpAddrOnRestart ()
{
    UINT4               u4IpAddr = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               au4IpIfaceList[IPIF_MAX_LOGICAL_IFACES + 1];
    UINT1               au1IfName[IP_PORT_NAME_LEN];
    UINT2               u2Var = 0;
    tLnxIpPortParams    LnxIpPortParams;

    VcmGetCxtIpIfaceList (u4ContextId, &au4IpIfaceList[0]);
    if (au4IpIfaceList[0] == 0)
    {
        /* There is no interface mapped to this context */
        return;
    }
    for (u2Var = 1; ((u2Var <= au4IpIfaceList[0]) &&
                     (u2Var < IPIF_MAX_LOGICAL_IFACES)); u2Var++)
    {
        u4IfIndex = au4IpIfaceList[u2Var];
        MEMSET (&LnxIpPortParams, 0, sizeof (tLnxIpPortParams));
        LnxIpPortParams.u4IpAddr = u4IpAddr;
        LnxIpPortParams.u2UpdateMask = LNX_IPADDR_UPDATE;
        CfaGetIfName (u4IfIndex, au1IfName);
        LinuxIpUpdateInterfaceParams (au1IfName, LnxIpPortParams);
    }

    return;
}

#ifdef LINUX_310_WANTED
 /*******************************************************************************
 * Function Name    : LnxIpAddDelAddr
 * Description      : Function to add or delete ip address using netlink api
 * Inputs      : u4IpAddr  : New ip address to be added 
 *               u4IfIndex : Interface Index
 *               u4Flag    : 0 - Delete
 *                       1 - Add
 * Output      : None.
 * Returns         : NETIPV4_SUCCESS.
 *               NETIPV4_FAILURE.
 ******************************************************************************/
INT4
LnxIpAddDelAddr (UINT4 u4IpAddr, UINT4 u4IfIndex, UINT4 u4Flag)
{
    struct nlhandle     NlHdl;
    tNlAddrHdr          NlReq;

    MEMSET (&NlHdl, ZERO, sizeof (NlHdl));
    MEMSET (&NlReq, ZERO, sizeof (tNlAddrHdr));

    if (LnxIpCreateNetSocket (ZERO, &NlHdl) < ZERO)
    {
        return NETIPV4_FAILURE;
    }

    NlReq.nlh.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifaddrmsg));
    if (u4Flag == LNX_IPADDR_ADD)
    {
        NlReq.nlh.nlmsg_type = RTM_NEWADDR;
        NlReq.nlh.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL;
    }
    else if (u4Flag == LNX_IPADDR_DEL)
    {
        NlReq.nlh.nlmsg_type = RTM_DELADDR;
        NlReq.nlh.nlmsg_flags = NLM_F_REQUEST;
    }
    NlReq.addrmsg.ifa_family = AF_INET;
    /* Update prefix len to 32 as it will be updated correctly through ioctl */
    NlReq.addrmsg.ifa_prefixlen = LNX_MAX_PREFIX_LEN;
    NlReq.addrmsg.ifa_index = u4IfIndex;
    NlReq.addrmsg.ifa_scope = ZERO;

    LnxIpNetLinkAddAddAttr (&NlReq.nlh, sizeof (NlReq), IFA_LOCAL,
                            (UINT1 *) &u4IpAddr, IP_FOUR);
    if (LnxIpNetTalkToKernel (&NlHdl, &NlReq.nlh) < ZERO)
    {
        LnxIpNetClose (&NlHdl);
        return NETIPV4_FAILURE;
    }
    LnxIpNetClose (&NlHdl);
    return NETIPV4_SUCCESS;
}

/*******************************************************************************
 * Function Name   : LnxIpCheckInterfaceStatus 
 * Description     : This function checks for the interface status. 
 * Global Varibles :
 * Inputs          : IpAddr   - IP Address
 * Output          : None.
 * Returns         : NETIPV4_SUCCESS/NETIPV4_FAILURE 
 ******************************************************************************/
INT4
LnxIpCheckInterfaceStatus (UINT4 u4IpAddr, UINT4 u4ContextId)
{
    struct ifreq        if_req;
    UINT4               u4IfIndex = IP_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4Err = NETIPV4_FAILURE;

    MEMSET (&if_req, 0, sizeof (if_req));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (u4IpAddr == IP_ZERO)
    {
        return NETIPV4_FAILURE;
    }

    CfaIpIfGetIfIndexForNetInCxt (u4ContextId, u4IpAddr, &u4IfIndex);

    CfaGetIfName (u4IfIndex, au1IfName);
    STRNCPY (if_req.ifr_name, (CHR1 *) au1IfName, IFNAMSIZ);
    while (1)
    {
#ifdef VRF_WANTED
        if ((i4Err =
             ioctl (gai4SocketId[u4ContextId], SIOCGIFFLAGS, &if_req)) < 0)
#else
        if ((i4Err = ioctl (gi4socketid, SIOCGIFFLAGS, &if_req)) < 0)
#endif
        {
            perror ("SIOCGIFFLAGS");
            return NETIPV4_FAILURE;
        }
        if ((if_req.ifr_flags & IFF_UP) && (if_req.ifr_flags & IFF_RUNNING))
        {
            break;
        }
    }
    return NETIPV4_SUCCESS;
}
#endif /* LINUX_310_WANTED */
/*******************************************************************************
 * Function Name   : LnxIpGetProxyArpAdminStatus
 * Description     : This function is used to get the ProxyArpStatus for the given IfIndex.
 * Global Varibles :
 * Inputs          : i4FsIpifIndex
 * Output          : *pi4RetValFsIpifLocalProxyArpAdminStatus
 * Returns         : NETIPV4_SUCCESS/NETIPV4_FAILURE
 ******************************************************************************/

INT4
LnxIpGetProxyArpAdminStatus (INT4 i4IpifIndex,
                             INT4 *pi4RetValIpifLocalProxyArpAdminStatus)
{
    tLnxProxyArpEntry  *pLnxProxyArpEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT1               au1IfName[IF_PREFIX_LEN];
    UINT1               au1FileName[200];

    MEMSET (au1FileName, 0, sizeof (au1FileName));
    MEMSET (au1IfName, 0, sizeof (au1IfName));
    /*checking the value in SLL */
    TMO_SLL_Scan (&gProxyArpAdminStatusList, pNode, tTMO_SLL_NODE *)
    {
        pLnxProxyArpEntry = (tLnxProxyArpEntry *) pNode;
        if (pLnxProxyArpEntry->i4IfIndex == i4IpifIndex)
        {
            *pi4RetValIpifLocalProxyArpAdminStatus =
                pLnxProxyArpEntry->i1ProxyArpAdminStatus;
            return NETIPV4_SUCCESS;

        }

    }

    return NETIPV4_FAILURE;

}

/*******************************************************************************
 * Function Name   : LnxIpSetProxyArpAdminStatus
 * Description     : This function is used to Set the ProxyArpStatus in Linux for the given IfIndex.
 * Global Varibles :
 * Inputs          : i4IpifIndex,i4SetValIpifProxyArpAdminStatus
 * Output          : None
 * Returns         : NETIPV4_SUCCESS/NETIPV4_FAILURE
 ******************************************************************************/

INT4
LnxIpSetProxyArpAdminStatus (INT4 i4IpifIndex,
                             INT4 i4SetValIpifProxyArpAdminStatus)
{

    CHR1                ac1Command[LIP4_LINE_LEN];
    UINT1               au1IfName[IF_PREFIX_LEN];
    INT4                i4ProxyArpAdminStatus = 0;
    tLnxProxyArpEntry  *pLnxProxyArpEntry = NULL;
    tLnxProxyArpEntry  *pTmpProxyArpEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;

    MEMSET (ac1Command, 0, sizeof (ac1Command));
    MEMSET (au1IfName, 0, sizeof (au1IfName));

    if (LnxIpGetProxyArpAdminStatus (i4IpifIndex, &i4ProxyArpAdminStatus)
        != NETIPV4_FAILURE)
    {
        if (i4ProxyArpAdminStatus == i4SetValIpifProxyArpAdminStatus)
        {
            return NETIPV4_SUCCESS;
        }
    }
    if (i4SetValIpifProxyArpAdminStatus == ARP_PROXY_ENABLE)
    {
        if (((pLnxProxyArpEntry =
              (tLnxProxyArpEntry *) MemAllocMemBlk (gProxyArpStatusPoolId))
             == NULL))
        {
            return NETIPV4_FAILURE;
        }

        pNode = TMO_SLL_First (&(gProxyArpAdminStatusList));
        if (pNode == NULL)
        {
            TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pLnxProxyArpEntry);
            pLnxProxyArpEntry->i4IfIndex = i4IpifIndex;
            pLnxProxyArpEntry->i1ProxyArpAdminStatus = ARP_PROXY_ENABLE;
            TMO_SLL_Insert (&gProxyArpAdminStatusList, NULL,
                            (tTMO_SLL_NODE *) (pLnxProxyArpEntry));
        }
        else
        {
            TMO_SLL_Scan (&gProxyArpAdminStatusList, pNode, tTMO_SLL_NODE *)
            {
                pTmpProxyArpEntry = (tLnxProxyArpEntry *) pNode;
                if (pTmpProxyArpEntry->i4IfIndex < i4IpifIndex)
                {
                    pPrevNode = pNode;
                }
                else
                {
                    break;
                }
            }
            TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pLnxProxyArpEntry);
            pLnxProxyArpEntry->i4IfIndex = i4IpifIndex;
            pLnxProxyArpEntry->i1ProxyArpAdminStatus = ARP_PROXY_ENABLE;
            TMO_SLL_Insert (&gProxyArpAdminStatusList,
                            (tTMO_SLL_NODE *) pPrevNode,
                            (tTMO_SLL_NODE *) pLnxProxyArpEntry);

        }

    }
    else
    {
        TMO_SLL_Scan (&gProxyArpAdminStatusList, pLnxProxyArpEntry,
                      tLnxProxyArpEntry *)
        {
            if (pLnxProxyArpEntry->i4IfIndex == i4IpifIndex)
            {
                TMO_SLL_Delete (&gProxyArpAdminStatusList,
                                (tTMO_SLL_NODE *) pLnxProxyArpEntry);
                MemReleaseMemBlock (gProxyArpStatusPoolId,
                                    (UINT1 *) pLnxProxyArpEntry);
                break;
            }
        }

    }
    if (CfaGetInterfaceNameFromIndex ((UINT4) i4IpifIndex,
                                      au1IfName) == OSIX_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    SNPRINTF ((CHR1 *) ac1Command, sizeof (ac1Command),
              "echo %d > %s/%s/proxy_arp", i4SetValIpifProxyArpAdminStatus,
              LNXIP_PROXYARP_FILE_NAME, au1IfName);
    system (ac1Command);
    return NETIPV4_SUCCESS;

}

/*******************************************************************************
 * Function Name   : LnxIpTestProxyArpAdminStatus
 * Description     : This function is used to Test the ProxyArpStatus value in 
                     Linux with the Current value.
 * Global Varibles :
 * Inputs          : i4IpifIndex,i4SetValFsIpifProxyArpAdminStatus
 * Output          : pu4ErrorCode
 * Returns         : NETIPV4_SUCCESS/NETIPV4_FAILURE
 ******************************************************************************/

INT4                LnxIpTestProxyArpAdminStatus
    (UINT4 *pu4ErrorCode, INT4 i4IpifIndex,
     INT4 i4TestValIpifProxyArpAdminStatus)
{

    if ((i4TestValIpifProxyArpAdminStatus != ARP_PROXY_DISABLE) &&
        (i4TestValIpifProxyArpAdminStatus != ARP_PROXY_ENABLE))
    {
        return NETIPV4_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IpifIndex);
    return NETIPV4_SUCCESS;
}

/*************************************************************************************************
 * Function Name   : LnxGetNextIndexFsMIStdIpifTable
 * Description     : This function is used to get the Next IfIndex that consists the ProxyArp Status
 * Global Varibles :
 * Inputs          : i4FsMIStdIpifIndex
 * Output          : *pi4NextFsMIStdIpifIndex
 * Returns         : NETIPV4_SUCCESS/NETIPV4_FAILURE
 ***************************************************************************************************/

INT4
LnxGetNextIndexFsMIStdIpifTable (INT4 i4FsMIStdIpifIndex,
                                 INT4 *pi4NextFsMIStdIpifIndex)
{
    tLnxProxyArpEntry  *pLnxProxyArpEntry = NULL;
    tTMO_SLL_NODE      *pNode = NULL;

    if (i4FsMIStdIpifIndex == 0)
    {
        pNode = TMO_SLL_First (&(gProxyArpAdminStatusList));
        if (pNode == NULL)
        {
            return NETIPV4_FAILURE;
        }
        else
        {
            pLnxProxyArpEntry = (tLnxProxyArpEntry *) pNode;
            *pi4NextFsMIStdIpifIndex = pLnxProxyArpEntry->i4IfIndex;
            return NETIPV4_SUCCESS;
        }
    }
    else
    {
        TMO_SLL_Scan (&gProxyArpAdminStatusList, pNode, tTMO_SLL_NODE *)
        {
            if (pNode != NULL)
            {

                pLnxProxyArpEntry = (tLnxProxyArpEntry *) pNode;
                if (pLnxProxyArpEntry->i4IfIndex == i4FsMIStdIpifIndex)
                {
                    pNode = TMO_SLL_Next (&(gProxyArpAdminStatusList), pNode);
                    if (pNode == NULL)
                    {
                        return NETIPV4_FAILURE;
                    }
                    else
                    {
                        pLnxProxyArpEntry = (tLnxProxyArpEntry *) pNode;
                        *pi4NextFsMIStdIpifIndex = pLnxProxyArpEntry->i4IfIndex;
                        return NETIPV4_SUCCESS;
                    }
                }
            }
            else
            {
                return NETIPV4_FAILURE;
            }
        }
    }
    return NETIPV4_FAILURE;
}
#endif
