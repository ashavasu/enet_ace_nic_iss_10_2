/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxvrf.c,v 1.6 2016/02/03 10:49:39 siva Exp $
 *
 * Description: Functions handling the Linux IP VRF 
 *
 *******************************************************************/
#ifndef _LNXVRF_C
#define _LNXVRF_C
#include "lr.h"
#include "lnxipint.h"
#include "lnxvrf.h"
#include <sys/ipc.h>
#include <sys/msg.h>
#include "lnxipmod.h"
#include "lnxiptap.h"
#include "chrdev.h"
#include "netdev.h"

extern INT4         gai4SocketId[SYS_DEF_MAX_NUM_CONTEXTS];
INT4 gLnxVrfNetLinkSock[SYS_DEF_MAX_NUM_CONTEXTS];

static tTskStatus   gCurTskStatus = TSK_BEFORE_INIT;
PRIVATE INT1
LnxVrfLoopbackUpdStatus(UINT1 *pu1DevName,UINT1 u1Action,UINT1 u4VrfId);

/* Following function is used as  VCM callback functions. */
/*****************************************************************************/
/*                                                                           */
/* Function     : LnxVrfCallbackFn                                          */
/*                                                                           */
/* Description  : Function to Indicate Context/Interface mapping deletion to */
/*                LinuxIP. This fucntion is registered with VCM module          */
/*                as a call back function                                    */
/*                                                                           */
/* Input        : u4IfIndex   - Ip Interface Index                           */
/*                u4VcmCxtId  - Context Id                                   */
/*                u1BitMap    - Bit Map to identify the change               */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LnxVrfCallbackFn (UINT4 u4IpIfIndex,UINT4 u4VcmCxtId, UINT1 u1BitMap)
{
    UINT4 	u4RetVal = NETIPV4_FAILURE;
    UINT1   	u1MsgType = 0; 
    UNUSED_PARAM (u4IpIfIndex);

    if (u1BitMap == VCM_CONTEXT_CREATE)
    {
        gai4SocketId[u4VcmCxtId] = -1;
#ifdef LNXIP6_WANTED
        /* Initializing IPV6  */
        LnxVrfIpv6Initializations(u4VcmCxtId);
#endif
        gLnxVrfNetLinkSock[u4VcmCxtId] = -1;
        u4RetVal = LnxVrfCreateContext (u4VcmCxtId);
        u1MsgType = LNX_VRF_CREATE_CONTEXT;
    }
    else if (u1BitMap == VCM_CONTEXT_DELETE)
    {
        u4RetVal = LnxVrfDeleteContext (u4VcmCxtId);
        u1MsgType = LNX_VRF_DELETE_CONTEXT;
    }


    if((u4RetVal == (UINT4)NETIPV4_FAILURE))
    {
        VcmHandleStatusForLnxVrf (u4VcmCxtId, u1MsgType,
                u4RetVal,LNXVRF_ZERO);
        if(u1MsgType == (UINT1)LNX_VRF_CREATE_CONTEXT)
        {
            LnxVrfDeInitForContext(u4VcmCxtId);
        }
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfCreateContext
 *
 * Description        : This function spawns a new task LNXVRFx, 
 *                       where x is the VRF ID and posts an event 
 *                       along with the message type as VRF_CREATE_CXT.
 *
 * Input(s)           : u4ContextId. 
 *
 * Output(s)          : None.
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 *
 * Action             : 
 *
 *         
 ********************************************************************/
UINT4
LnxVrfCreateContext (UINT4 u4ContextId)
{
    UINT1               au1TskName[VCM_TASK_MAX_LEN];
    UINT1               au1HexName[LNX_VRF_HEX_LEN];
    tLnxVrfMsg         *pLnxVrfMsg = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;

    MEMSET (au1TskName, LNXVRF_ZERO, sizeof (au1TskName));
    MEMSET (au1HexName,LNXVRF_ZERO,sizeof(au1HexName));
    UtilDectoHexStr(u4ContextId,au1HexName);
    STRCPY ((CHR1 *) au1TskName, "LV");
    STRNCAT(au1TskName,au1HexName,LNX_VRF_HEX_LEN);
    au1TskName[VCM_TASK_MAX_LEN]='\0';
    gCurTskStatus = TSK_BEFORE_INIT;
    OsixSemTake (gLnxVrfGlobSemId);
    /*Creation of Task for each vrf 
     * OSIX_MAX_TSKS in inc/fsap.h and
     * OSIX_MAX_QUES in fsap2/pthreads/osix.h needs 
     * to be tuned accordingly whenever SYS_DEF_MAX_NUM_CONTEXTS
     * tuned to above 128*/
    if (LNX_VRF_TASK_CREATE (au1TskName, LNX_VRF_TASK_PRIORITY | OSIX_SCHED_RR,
                             OSIX_DEFAULT_STACK_SIZE,
                             (OsixTskEntry) LnxVrfAuditTaskMain, LNXVRF_ZERO,
                             &(LNX_VRF_AUDIT_TASK_ID)) != OSIX_SUCCESS)
    {
        OsixSemGive (gLnxVrfGlobSemId);
        return NETIPV4_FAILURE;
    }

    /* After the module init has been invoked or protocol task has been
     * spawned, wait on the "SEQ" semaphore. When the semaphore is given
     * back by the protocol, check if the initialization was successful.
     */
    if ((OsixSemTake (gLnxVrfGlobSemId) != OSIX_SUCCESS) ||
		    (gCurTskStatus != TSK_INIT_SUCCESS))

    {
	    PRINTF ("Task Init Failed for VRF <%d> \n",
			    u4ContextId);
	    OsixSemGive (gLnxVrfGlobSemId);
	    return NETIPV4_FAILURE;
    }

    pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
    if (pLnxVrfInfo == NULL)
    {
        OsixSemGive (gLnxVrfGlobSemId);
        return NETIPV4_FAILURE;
    }
    /*Allocation of memory for pLnxVrfMsg */
    if (LNX_VRF_MSG_ENTRY_ALLOC (pLnxVrfMsg) == NULL)
    {
        OsixSemGive (gLnxVrfGlobSemId);
        return NETIPV4_FAILURE;
    }
    MEMSET (pLnxVrfMsg, LNXVRF_ZERO, sizeof (tLnxVrfMsg));
    pLnxVrfMsg->u1MsgType = LNX_VRF_CREATE_CONTEXT;
    pLnxVrfMsg->u4VrfId = u4ContextId;
    /*Post the message in Queue */
    if (OsixQueSend ((tOsixQId) pLnxVrfInfo->LnxVrfQueId, (UINT1 *) &pLnxVrfMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        OsixSemGive (gLnxVrfGlobSemId);
        return NETIPV4_FAILURE;
    }
    /*Send an event for create context */
    if (OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_EVENT) != OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        OsixSemGive (gLnxVrfGlobSemId);
        return NETIPV4_FAILURE;
    }
    OsixSemGive (gLnxVrfGlobSemId);
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfDeleteContext
 *
 * Description        : This function will send an event along with 
 *                      the message type as VRF_DELETE_CXT to the 
 *                      LNXVRFx task, where x is the VRF ID.
 *
 * Input(s)           : u4ContextId. 
 *
 * Output(s)          : None.
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 *
 * Action             : 
 *
 *         
 ********************************************************************/

UINT4
LnxVrfDeleteContext (UINT4 u4ContextId)
{
    tLnxVrfMsg         *pLnxVrfMsg = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;

    pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
    if (pLnxVrfInfo == NULL)
    {
        return NETIPV4_FAILURE;
    }
    /*Allocation of memory for pLnxVrfMsg */
    if (LNX_VRF_MSG_ENTRY_ALLOC (pLnxVrfMsg) == NULL)
    {
        return NETIPV4_FAILURE;
    }
    MEMSET (pLnxVrfMsg, LNXVRF_ZERO, sizeof (tLnxVrfMsg));
    pLnxVrfMsg->u1MsgType = LNX_VRF_DELETE_CONTEXT;
    pLnxVrfMsg->u4VrfId = u4ContextId;
    /*Post the message in Queue */
    if (OsixQueSend ((tOsixQId) pLnxVrfInfo->LnxVrfQueId, (UINT1 *) &pLnxVrfMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }
    /*Send an event for create context */
    if (OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_EVENT) != OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;

}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfAuditTaskMain
 *
 * Description        : This function waits for the LNX_VRF 
 *             events posted by different functions from VCM module.
 *                       
 *
 * Input(s)           : NONE
 *
 * Output(s)          : None.
 *
 * Returns            : NONE
 *         
 ********************************************************************/
VOID
LnxVrfAuditTaskMain (INT1 *pi1TaskParam)
{
    UINT4               u4EventMask = LNXVRF_ZERO;
    tOsixTaskId         u4TskId = LNXVRF_ZERO;
    UNUSED_PARAM (pi1TaskParam);
    if (OsixTskIdSelf (&u4TskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        LNX_VRF_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (LnxVrfInit (u4TskId) != OSIX_SUCCESS)
    {
        /*Indicate the status of initialization to the main routine */
        LNX_VRF_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    LNX_VRF_INIT_COMPLETE (OSIX_SUCCESS);
    while (1)
    {
        OsixEvtRecv (u4TskId, LNX_VRF_EVENT |
                     LNX_VCM_INTERFACE_EVENT,
                     LNX_VRF_EVENT_WAIT_FLAGS, &u4EventMask);
        LNX_VRF_PROT_LOCK ();
        LNX_VRF_Process_Message_Arrival_Event ();

        LNX_VRF_PROT_UNLOCK ();
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfInit
 *
 * Description        : This function creates semphores,queue and memory allocation
 *
 * Input(s)           : NONE
 *
 * Output(s)          : None.
 *
 * Returns            : NONE
 *         
 ***********************************************************************/
INT1
LnxVrfInit (UINT4 u4TskId)
{
    const UINT1        *pTaskName;
    UINT1               au1SemName[VCM_TASK_MAX_LEN];
    UINT1               au1QueueName[VCM_TASK_MAX_LEN];
    UINT1               au1HexLen[VCM_TASK_MAX_LEN];
    UINT1               au1NsName[VCM_NS_MAX_LEN];
    UINT4               u4VrfId = LNXVRF_ZERO;
    tLnxVrfInfo        *pLnxVrfInfo;

    pTaskName = OsixExGetTaskName (u4TskId);
    SPRINTF ((CHR1 *) au1HexLen, "0x%s", (pTaskName + LNX_VRF_TASKOFFSET));
    u4VrfId = (UINT4)UtilHexStrToDecimal(au1HexLen);
    if(u4VrfId == (UINT4)LNXVRF_SMALLER)
    {
	    return NETIPV4_FAILURE;
    }
    /*Queue Creatiobn */
    MEMSET (au1QueueName, LNXVRF_ZERO, sizeof (au1QueueName));
    STRCPY ((CHR1 *) au1QueueName, "LQ");
    STRNCAT(au1QueueName,(pTaskName + LNX_VRF_TASKOFFSET),LNX_VRF_HEX_LEN);
    au1QueueName[VCM_TASK_MAX_LEN]='\0';
    if (OsixQueCrt (au1QueueName, OSIX_MAX_Q_MSG_LEN,
                    LNX_VRF_MESSAGE_ARRIVAL_Q_DEPTH,
                    &gLnxVrfQId) != OSIX_SUCCESS)
    {
        return NETIPV4_FAILURE;
    }

    /* Create the vrf Semaphore */
    MEMSET (au1SemName, LNXVRF_ZERO, sizeof (au1SemName));
    STRCPY ((CHR1 *) au1SemName, "LS");
    STRNCAT(au1SemName,(pTaskName + LNX_VRF_TASKOFFSET),LNX_VRF_HEX_LEN);
    au1SemName[VCM_TASK_MAX_LEN]='\0';
    if ((OsixCreateSem (au1SemName, 1, LNXVRF_ZERO, &gLnxVrfSemId)) !=
        OSIX_SUCCESS)
    {
        OsixQueDel (gLnxVrfQId);
        return NETIPV4_FAILURE;
    }
    if (LNX_VRF_INFO_ENTRY_ALLOC (pLnxVrfInfo) == NULL)
    {
        return NETIPV4_FAILURE;
    }
    MEMSET (pLnxVrfInfo, LNXVRF_ZERO, sizeof (tLnxVrfInfo));
    pLnxVrfInfo->u4VrfId = u4VrfId;
    pLnxVrfInfo->u4TskId = u4TskId;
    pLnxVrfInfo->LnxVrfSemId = gLnxVrfSemId;
    pLnxVrfInfo->LnxVrfQueId = gLnxVrfQId;

    MEMSET (au1NsName, LNXVRF_ZERO, sizeof (au1NsName));
    SNPRINTF ((CHR1 *) au1NsName, VCM_NS_MAX_LEN, "ns%d", u4VrfId);

    MEMCPY (pLnxVrfInfo->au1NameSpace, au1NsName, STRLEN (au1NsName));

    if (RBTreeAdd (gLnxVrfInfoRBRoot, pLnxVrfInfo) == RB_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : LNX_VRF_Process_Message_Arrival_Event
 *
 * Description        : This function deques the pkt from lnx_vrf queue
 *
 * Input(s)           : NONE
 *
 * Output(s)          : None.
 *
 * Returns            : NONE
 ********************************************************************/
VOID
LNX_VRF_Process_Message_Arrival_Event (VOID)
{
    tLnxVrfMsg         *pLnxVrfMsg = NULL;
    tOsixQId           *pQueId;
    UINT4               u4VrfId = 0;

    pQueId = LnxVrfQueueIdGet ();
    if (pQueId == NULL)
    {
        return;
    }

    while (OsixQueRecv (pQueId, (UINT1 *) &pLnxVrfMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pLnxVrfMsg->u1MsgType)
        {
            case LNX_VRF_CREATE_CONTEXT:
            {
                pLnxVrfMsg->u4RetVal = (UINT4) LnxVrfCreateContextInLnx
                    (pLnxVrfMsg->u4VrfId);
                break;
            }

            case LNX_VRF_DELETE_CONTEXT:
            {
                u4VrfId = pLnxVrfMsg->u4VrfId;
                LNX_VRF_MSG_FREE (pLnxVrfMsg);
                LnxVrfDeleteContextInLnx(u4VrfId);
                break;
            }
            case LNX_VRF_IF_MAP_CONTEXT:
            {
                if ( pLnxVrfMsg->u1IfType == CFA_LOOPBACK)
                {
                    pLnxVrfMsg->u4RetVal =
                        (UINT4) IpHandleLoopbackIfaceMapping (pLnxVrfMsg->u4VrfId,
                                pLnxVrfMsg->u4IfIndex);
                }
                else
                {
                    pLnxVrfMsg->u4RetVal =
                        (UINT4) IpHandleIfaceMapping (pLnxVrfMsg->u4VrfId,
                                pLnxVrfMsg->u4IfIndex,
                                pLnxVrfMsg->i4Sockdomain,
                                pLnxVrfMsg->i4SockType,
                                pLnxVrfMsg->i4SockProto);
                }
                break;
            }
            case LNX_VRF_IF_UNMAP_CONTEXT:
            {
                if(pLnxVrfMsg->u1IfType == CFA_LOOPBACK)
                {
                    pLnxVrfMsg->u4RetVal =
                        (UINT4) IpHandleLoopbackIfaceUnMapping (pLnxVrfMsg->u4VrfId,
                                pLnxVrfMsg->u4IfIndex);
                }
                else
                {
                    pLnxVrfMsg->u4RetVal =
                        (UINT4) IpHandleIfaceUnMapping (pLnxVrfMsg->u4VrfId,
                                pLnxVrfMsg->u4IfIndex,
                                pLnxVrfMsg->i4Sockdomain,
                                pLnxVrfMsg->i4SockType,
                                pLnxVrfMsg->i4SockProto);
                }
                break;
            }
            case LNX_VRF_OPEN_SOCKET:
            {
                pLnxVrfMsg->u4RetVal =
                    (UINT4) LnxVrfOpenSockInVrfCxt (pLnxVrfMsg->u4VrfId,
                                                    pLnxVrfMsg->i4Sockdomain,
                                                    pLnxVrfMsg->i4SockType,
                                                    pLnxVrfMsg->i4SockProto);
                break;
            }

        }

        VcmHandleStatusForLnxVrf (pLnxVrfMsg->u4VrfId, pLnxVrfMsg->u1MsgType,
                                  pLnxVrfMsg->u4RetVal,pLnxVrfMsg->u4IfIndex);

	if((pLnxVrfMsg->u4RetVal == (UINT4)NETIPV4_FAILURE) && (pLnxVrfMsg->u1MsgType == (UINT1)LNX_VRF_CREATE_CONTEXT))
	{
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
		LnxVrfDeInitForContext(pLnxVrfMsg->u4VrfId);
	}
	LNX_VRF_MSG_FREE (pLnxVrfMsg);
    }
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfCreateContextInLnx
 *
 * Description        : This function will map the name space NSx with the new user configured VRF ID
 *
 * Input(s)           : VRFID
 *
 * Output(s)          : NONE
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 ********************************************************************/
INT1
LnxVrfCreateContextInLnx (UINT4 u4VrfId)
{
    tLnxVrfInfo        *pLnxVrfInfo;
    INT4                i4Netns = LNXVRF_ZERO;
    CHR1                net_path[MAX_NETNS_PATH];
    UINT1               au1IfName[IP_PORT_NAME_LEN];
#ifdef LNXIP6_WANTED
    INT4                i4Ipv6SockFd = -1;
#endif

    MEMSET (&net_path, 0, MAX_NETNS_PATH);

    pLnxVrfInfo = LnxVrfInfoGet (u4VrfId);
    if (pLnxVrfInfo == NULL)
    {
        return NETIPV4_FAILURE;
    }
    SNPRINTF (net_path, sizeof(net_path), "%s/%s", NETNS_RUN_DIR, pLnxVrfInfo->au1NameSpace);

    i4Netns = open (net_path, O_RDONLY);
    if (i4Netns < LNXVRF_ZERO)
    {
        perror ("Cannot open network namespace");
        return NETIPV4_FAILURE;
    }
    if (setns (i4Netns, CLONE_NEWNET) < LNXVRF_ZERO)
    {
        perror ("seting the network namespace failed");
        close(i4Netns);
        return NETIPV4_FAILURE;
    }
    gai4SocketId[u4VrfId] =
        LnxVrfOpenSocket (PF_PACKET, SOCK_RAW, OSIX_HTONS (ETH_P_ALL));
    if (gai4SocketId[u4VrfId] < 0)
    {
        perror ("socket creation failed !!!\r\n");
        close(i4Netns);
	return NETIPV4_FAILURE;
    }
#ifdef LNXIP6_WANTED
    /* Updating Global Socket for IPV6 for u4VrfId */
    i4Ipv6SockFd = LnxVrfOpenSocket (AF_INET6, SOCK_DGRAM, IPPROTO_IP);
    if (i4Ipv6SockFd < 0)
    {
        perror ("socket creation failed !!!\r\n");
        close(i4Netns);
        return NETIPV4_FAILURE;
    }
    LnxVrfIpv6UpdateGlobSock(u4VrfId, i4Ipv6SockFd);
    LnxVrfEnableIpv6Forwarding();
#endif
    /*To Create a socket for ARP module*/
    if(LnxVrfNetlinkSockInit(u4VrfId) == NETIPV4_FAILURE)
    {
        close(i4Netns);
	return NETIPV4_FAILURE;
    }
    STRCPY(au1IfName,"lo");
    if(LnxVrfLoopbackUpdStatus(au1IfName,CFA_IF_UP,u4VrfId) == NETIPV4_FAILURE)
    {
        close(i4Netns);
        return NETIPV4_FAILURE;
    }
    close(i4Netns);
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfDeleteContextInLnx
 *
 * Description        : This function will send an event along with 
 *                      the message type as VRF_DELETE_CXT to the 
 *                      LNXVRFx task, where x is the VRF ID.
 *
 * Input(s)           : u4ContextId. 
 *
 * Output(s)          : None.
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 *
 * Action             : 
 *
 *         
 ********************************************************************/

INT1
LnxVrfDeleteContextInLnx (UINT4 u4ContextId)
{
	tLnxVrfInfo        *pLnxVrfInfo = NULL;
	tOsixTaskId        u4TskId = LNXVRF_ZERO;
	UINT1              au1IfName[IP_PORT_NAME_LEN];

	pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);

	if (pLnxVrfInfo == NULL)
	{
		return NETIPV4_FAILURE;
	}
	if(RBTreeRem (gLnxVrfInfoRBRoot, pLnxVrfInfo) == NULL)
	{
		return NETIPV4_FAILURE;
	}
	STRCPY(au1IfName,"lo");
	if(LnxVrfLoopbackUpdStatus(au1IfName,CFA_IF_DOWN,u4ContextId) == NETIPV4_FAILURE)
	{
		return NETIPV4_FAILURE;
	}
	u4TskId = pLnxVrfInfo->u4TskId;
	if((tOsixSemId) pLnxVrfInfo->LnxVrfSemId != NULL)
	{
		OsixSemDel ((tOsixSemId) pLnxVrfInfo->LnxVrfSemId);
	}
	if ((tOsixQId)pLnxVrfInfo->LnxVrfQueId != NULL)
	{
		OsixQueDel ((tOsixQId) pLnxVrfInfo->LnxVrfQueId);
	}
	if(gai4SocketId[u4ContextId] != -1)
	{
		close (gai4SocketId[u4ContextId]);
		gai4SocketId[u4ContextId] = -1;
	}
	SelRemoveFd (gLnxVrfNetLinkSock[u4ContextId]);
#ifdef LNXIP6_WANTED
    /* De initializing Global socket for IPV6*/
    LnxVrfIpv6DeInitializations (u4ContextId);
#endif
	if(gLnxVrfNetLinkSock[u4ContextId] != -1)
	{
		close(gLnxVrfNetLinkSock[u4ContextId]);
		gLnxVrfNetLinkSock[u4ContextId] = -1;
	}


	LNX_VRF_INFO_FREE(pLnxVrfInfo);
	VcmHandleStatusForLnxVrf (u4ContextId, LNX_VRF_DELETE_CONTEXT,
			NETIPV4_SUCCESS,LNXVRF_ZERO);
	OsixTskDel (u4TskId);
	return NETIPV4_SUCCESS;

}

/*-------------------------------------------------------------------+
 *
 * Function           : IpHandleIfaceMapping
 *
 * Description        : This function will unmap the interface from default context or
 *          from non-default context and attach it to new context.
 *          On unmapping,interface will be mapped to default context.
 *
 *
 *
 * Input(s)           : u4VrfId,u4IfIndex
 *
 * Output(s)          : NONE
 *
 * Returns            : NONE
 ********************************************************************/

INT4
IpHandleIfaceMapping (UINT4 u4NewCxtId, UINT2 u4IfIndex, INT4 i4Sockdomain,
                      INT4 i4SockType, INT4 i4SockProto)
{
    tCfaIfInfo          CfaIfInfo;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    tLnxVrfMsg         *pLnxVrfMsg = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4SockId = -1;
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tOsixTaskId         u4TskId = LNXVRF_ZERO;


    pLnxVrfInfo = LnxVrfInfoGet (u4NewCxtId);
    if (pLnxVrfInfo == NULL)
    {
	    OsixTskIdSelf (&u4TskId);
        OsixEvtSend (u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    if(LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    if (LnxIpCreateNetSocket (0, &nlh) < 0)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    if (LnxVrfMapUnMapIntInLnxCxt (RTM_NEWLINK, 0, (CHR1 *)CfaIfInfo.au1IfName, (CHR1 *)pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE )
    {
	    LnxIpNetClose (&nlh);
	    OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
	    return NETIPV4_FAILURE;
    }

    LnxIpNetClose (&nlh);

    if(LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS, (CHR1 *) pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    /* Open a socket and send back, this socket will be used for the operations made on that interface */
    i4SockId = LnxVrfOpenSocket (i4Sockdomain, i4SockType, i4SockProto);
    if (i4SockId < 0)
    {
	    perror ("socket creation failed !!!\r\n");
	    OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
	    return NETIPV4_FAILURE;
    }
    if(LNX_VRF_IFINFO_ENTRY_ALLOC(pLnxVrfIfInfo) == NULL)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    MEMSET(pLnxVrfIfInfo,0,sizeof(tLnxVrfIfInfo));
    pLnxVrfIfInfo->u4IfIndex=u4IfIndex;
    pLnxVrfIfInfo->u4ContextId = u4NewCxtId;
    MEMCPY(pLnxVrfIfInfo->au1IfName,CfaIfInfo.au1IfName,STRLEN(CfaIfInfo.au1IfName));

    if(RBTreeAdd(gLnxVrfIfInfoRBRoot,pLnxVrfIfInfo) == RB_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    /*Allocation of memory for pLnxVrfMsg */
    if (LNX_VRF_MSG_ENTRY_ALLOC (pLnxVrfMsg) == NULL)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    MEMSET (pLnxVrfMsg, 0, sizeof (tLnxVrfMsg));
    pLnxVrfMsg->u4VrfId = u4NewCxtId;
    pLnxVrfMsg->u1MsgType = LNX_VRF_IF_MAP_CONTEXT;
    pLnxVrfMsg->i4SockId = i4SockId;
#if LNXIP6_WANTED
    LnxVrfUpdateCxtIdforIpv6Int(u4NewCxtId, u4IfIndex,0);
#endif
    /*Post the message in Queue */
    if (OsixQueSend (gLnxPostBkIpQId, (UINT1 *) &pLnxVrfMsg, OSIX_DEF_MSG_LEN)
            != OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    /*Send an event for create context */
    if (OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_UDP_SOCKET_STATUS_EVENT) !=
            OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpHandleIfaceUnMapping
 *
 * Description        : This function will unmap the interface from default context or
 *          from non-default context and attach it to new context.
 *          On unmapping,interface will be mapped to default context.
 *
 *
 *
 * Input(s)           : u4VrfId,u4IfIndex
 *
 * Output(s)          : NONE
 *
 * Returns            : NONE
 ********************************************************************/

INT4
IpHandleIfaceUnMapping (UINT4 u4NewCxtId, UINT2 u4IfIndex, INT4 i4Sockdomain,
                        INT4 i4SockType, INT4 i4SockProto)
{
    INT4                i4SockId = -1;
    tCfaIfInfo          CfaIfInfo;
    tLnxVrfMsg         *pLnxVrfMsg = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    tLnxVrfIfInfo       LnxVrfIfInfo;
    tLnxVrfIfInfo       *pLnxVrfIfInfo = NULL;
    tOsixTaskId        	u4TskId = LNXVRF_ZERO;

    pLnxVrfInfo = LnxVrfInfoGet (u4NewCxtId);
    if (pLnxVrfInfo == NULL)
    {
	    OsixTskIdSelf (&u4TskId);
        OsixEvtSend (u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    /* 1 for name space name
     * 2 for ns PID*/
    if (LnxIpCreateNetSocket (0, &nlh) < 0)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }


    if (LnxVrfMapUnMapIntInLnxCxt (RTM_NEWLINK, 0, (CHR1 *)CfaIfInfo.au1IfName, (CHR1 *) "1") == NETIPV4_FAILURE)
    {
	    LnxIpNetClose (&nlh);
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
	    return NETIPV4_FAILURE;
    }

    LnxIpNetClose (&nlh);

    /* 1 for name space name
     * 2 for ns PID*/
    if(LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS, (CHR1 *)LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    i4SockId = LnxVrfOpenSocket (i4Sockdomain, i4SockType, i4SockProto);
    if (i4SockId < 0)
    {
        perror ("socket creation failed !!!\r\n");
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    if(LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS, (CHR1 *)pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    if (LNX_VRF_MSG_ENTRY_ALLOC (pLnxVrfMsg) == NULL)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    MEMSET (pLnxVrfMsg, 0, sizeof (tLnxVrfMsg));
    pLnxVrfMsg->u4VrfId = u4NewCxtId;
    pLnxVrfMsg->u1MsgType = LNX_VRF_IF_UNMAP_CONTEXT;
    pLnxVrfMsg->i4SockId = i4SockId;
    STRCPY(LnxVrfIfInfo.au1IfName,CfaIfInfo.au1IfName);

    /*Remove LnxVrfIfInfo node*/
    pLnxVrfIfInfo = RBTreeGet(gLnxVrfIfInfoRBRoot,(tRBElem *)&LnxVrfIfInfo);
    if(pLnxVrfIfInfo != NULL)
    {
        if(RBTreeRem (gLnxVrfIfInfoRBRoot, pLnxVrfIfInfo) == NULL)
        {
		OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
            return NETIPV4_FAILURE;
        }
	LNX_VRF_IFINFO_FREE(pLnxVrfIfInfo);
    }
#if LNXIP6_WANTED
    LnxVrfUpdateCxtIdforIpv6Int(VCM_DEFAULT_CONTEXT, u4IfIndex,0);
#endif
    /*Post the message in Queue */
    if (OsixQueSend (gLnxPostBkIpQId, (UINT1 *) &pLnxVrfMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }
    /*Send an event for create context */
    if (OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_UDP_SOCKET_STATUS_EVENT) !=
        OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfContextEntryCompare
 *
 * Description        : This function will compare vrfid for addition to RB Tree
 *
 * Input(s)           : VRFID
 *
 * Output(s)          : NONE
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 ********************************************************************/
INT4
LnxVrfContextEntryCompare (tRBElem * e1, tRBElem * e2)
{
    UINT4               u4VrfId1 = ((tLnxVrfInfo *) e1)->u4VrfId;
    UINT4               u4VrfId2 = ((tLnxVrfInfo *) e2)->u4VrfId;

    if (u4VrfId1 < u4VrfId2)
    {
        return LNXVRF_SMALLER;
    }
    else if (u4VrfId1 > u4VrfId2)
    {
        return LNXVRF_GREATER;
    }
    return LNXVRF_EQUAL;

}

/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfContextEntryCompare
 *
 * Description        : This function will compare vrfid for addition to RB Tree
 *
 * Input(s)           : VRFID
 *
 * Output(s)          : NONE
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 ********************************************************************/

INT4
LnxVrfIfEntryCompare(tRBElem * e1, tRBElem * e2)
{
    UINT1    au1IfName1[24];
    UINT1    au1IfName2[24];
    STRCPY(au1IfName1,((tLnxVrfIfInfo *) e1)->au1IfName);
    STRCPY(au1IfName2,((tLnxVrfIfInfo *) e2)->au1IfName);

    if(STRCMP(au1IfName1,au1IfName2) < 0)
    {
        return LNXVRF_SMALLER;
    }
    else if(STRCMP(au1IfName1,au1IfName2) > 0)
    {
        return LNXVRF_GREATER;
    }
    return LNXVRF_EQUAL;
}


/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfContextEntryCompare
 *
 * Description        : This function will compare vrfid for addition to RB Tree
 *
 * Input(s)           : VRFID
 *
 * Output(s)          : NONE
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 ********************************************************************/
tLnxVrfInfo        *
LnxVrfInfoGet (UINT4 u4VrfId)
{

    tLnxVrfInfo         LnxVrfInfo;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;

    LnxVrfInfo.u4VrfId = u4VrfId;

    pLnxVrfInfo = RBTreeGet (gLnxVrfInfoRBRoot, (tRBElem *) & LnxVrfInfo);

    return pLnxVrfInfo;
}
/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfIfInfoGet
 *
 * Description        : This function will return LnxVrfIfInfo based on
 *                      device name
 *
 * Input(s)           : VRFID
 *
 * Output(s)          : NONE
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 ********************************************************************/
tLnxVrfIfInfo *
LnxVrfIfInfoGet(UINT1 *pu1IfName)
{

    tLnxVrfIfInfo LnxVrfIfInfo;
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;

    STRCPY(LnxVrfIfInfo.au1IfName,pu1IfName);

    pLnxVrfIfInfo = RBTreeGet(gLnxVrfIfInfoRBRoot,(tRBElem *)&LnxVrfIfInfo);

    return pLnxVrfIfInfo;

}
/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfQueueIdGet
 *
 * Description        : This function will retrieve TLnxVrfInfo node based on vrf-id
 *
 * Input(s)           : VRFID
 *
 * Output(s)          : NONE
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 * ******************************************************************/
tOsixQId           *
LnxVrfQueueIdGet ()
{

    const UINT1        *pTaskName;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    tOsixTaskId         u4TskId = LNXVRF_ZERO;
    UINT4               u4VrfId = LNXVRF_ZERO;
    UINT1               au1HexLen[VCM_TASK_MAX_LEN];
    tOsixQId           *pQueId = LNXVRF_ZERO;
    if (OsixTskIdSelf (&u4TskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        LNX_VRF_INIT_COMPLETE (OSIX_FAILURE);
        return NULL;
    }
    pTaskName = OsixExGetTaskName (u4TskId);
    SPRINTF ((CHR1 *) au1HexLen, "0x%s", (pTaskName + LNX_VRF_TASKOFFSET));
    u4VrfId = (UINT4)UtilHexStrToDecimal(au1HexLen);
    if(u4VrfId == (UINT4)LNXVRF_SMALLER)
    {
	    return NULL;
    }
    pLnxVrfInfo = LnxVrfInfoGet (u4VrfId);
    if (pLnxVrfInfo == NULL)
    {
        return NULL;
    }
    pQueId = (tOsixQId) pLnxVrfInfo->LnxVrfQueId;

    return pQueId;
}

/* -------------------------------------------------------------------+
 * Function           : LnxVrfLock
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : Takes a task Sempaphore
+-------------------------------------------------------------------*/
INT4
LnxVrfLock (VOID)
{
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    const UINT1        *pTaskName;
    tOsixTaskId         u4TskId = LNXVRF_ZERO;
    UINT4               u4VrfId = LNXVRF_ZERO;
    UINT1               au1HexLen[VCM_TASK_MAX_LEN];
    if (OsixTskIdSelf (&u4TskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        LNX_VRF_INIT_COMPLETE (OSIX_FAILURE);
        return NETIPV4_FAILURE;
    }

    pTaskName = OsixExGetTaskName (u4TskId);
    SPRINTF ((CHR1 *) au1HexLen, "0x%s", (pTaskName + LNX_VRF_TASKOFFSET));
    u4VrfId = (UINT4)UtilHexStrToDecimal(au1HexLen);
    if(u4VrfId == (UINT4)LNXVRF_SMALLER)
    {
            return NETIPV4_FAILURE;
    }

    pLnxVrfInfo = LnxVrfInfoGet (u4VrfId);
    if (OsixSemTake ((tOsixSemId) pLnxVrfInfo->LnxVrfSemId) != OSIX_SUCCESS)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : LnxVrfUnLock
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : Takes a task Sempaphore
+-------------------------------------------------------------------*/
INT4
LnxVrfUnLock (VOID)
{
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    const UINT1        *pTaskName;
    tOsixTaskId         u4TskId = LNXVRF_ZERO;
    UINT4               u4VrfId = LNXVRF_ZERO;
    UINT1               au1HexLen[VCM_TASK_MAX_LEN];
    if (OsixTskIdSelf (&u4TskId) == OSIX_FAILURE)
    {
        /* Indicate the status of initialization to the main routine */
        LNX_VRF_INIT_COMPLETE (OSIX_FAILURE);
        return NETIPV4_FAILURE;
    }

    pTaskName = OsixExGetTaskName (u4TskId);
    SPRINTF ((CHR1 *) au1HexLen, "0x%s", (pTaskName + LNX_VRF_TASKOFFSET));
    u4VrfId = (UINT4)UtilHexStrToDecimal(au1HexLen);
    if(u4VrfId == (UINT4)LNXVRF_SMALLER)
    {
            return NETIPV4_FAILURE;
    }

    pLnxVrfInfo = LnxVrfInfoGet (u4VrfId);
    if (OsixSemGive ((tOsixSemId) pLnxVrfInfo->LnxVrfSemId) != OSIX_SUCCESS)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/* -------------------------------------------------------------------+
 * Function           : LnxVrfSockLock
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : Takes a task Sempaphore
+---------------------------------i----------------------------------*/

INT4 
LnxVrfSockLock(VOID)
{
    if (OsixSemTake (gLnxEvtSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}
/* -------------------------------------------------------------------+
 * Function           : LnxVrfSockUnLock
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : Takes a task Sempaphore
+---------------------------------i----------------------------------*/
INT4
LnxVrfSockUnLock(VOID)
{
    if (OsixSemGive (gLnxEvtSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
/* -------------------------------------------------------------------+
 * Function           : LnxVrfChangeCxt
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : Changes the context to the specified namespace.
+---------------------------------i----------------------------------*/


INT4
LnxVrfChangeCxt (INT4 type, CHR1 * ns)
{
    INT4                i4Netns = LNXVRF_ZERO;
    char                net_path[99];

    if (type == LNX_VRF_NON_DEFAULT_NS)
    {
        SPRINTF (net_path, "/var/run/netns/%s", ns);
        i4Netns = open (net_path, O_RDONLY);
    }
    else
    {
        SPRINTF (net_path, "/proc/%s/ns/net", ns);
        i4Netns = open (net_path, O_RDONLY);
    }

    if (i4Netns < 0)
    {
        perror ("Cannot open network namespace");
        return NETIPV4_FAILURE;
    }
    if (setns (i4Netns, CLONE_NEWNET) < 0)
    {
        perror ("seting the network namespace failed");
        close(i4Netns);
        return NETIPV4_FAILURE;
    }
    close(i4Netns);
    return NETIPV4_SUCCESS;

}
/* -------------------------------------------------------------------+
 * Function           : LnxVrfOpenSocket
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : NETIPv4_SUCCESS/NETPV4_FAILURE
 *
 * Action             : Opens a socket with specified socket parametrs.
+---------------------------------i----------------------------------*/


INT4
LnxVrfOpenSocket (INT4 domain, INT4 type, INT4 protocol)
{
    return socket (domain, type, protocol);
}

/*****************************************************************************/
/* Function Name      : LnxVrfEventHandling                                      */
/*                                                                           */
/* Description        : This function His called from varipus modules to get   */
/*                       socket descriptior                                    */
/*                                                                           */
/*                                                                           */
/* Input(s)           : i4VRId  - virtual router id                          */
/*                                                                           */
/* Output(s)          : pu1MacAddr - MAC Address.                            */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE.                           */
/*****************************************************************************/
INT4
LnxVrfEventHandling (tLnxVrfEventInfo * pLnxVrfPostBack, INT4 *pi4SockFd)
{
	tLnxVrfInfo        *pLnxVrfInfo = NULL;
	tLnxVrfMsg         *pLnxVrfMsg = NULL;

	UINT4               u4EventMask = ZERO;
	UINT4               u4Event = ZERO;

	if(pLnxVrfPostBack->u4ContextId)
	{
		pLnxVrfInfo = LnxVrfInfoGet (pLnxVrfPostBack->u4ContextId);
		if (pLnxVrfInfo == NULL)
		{
			return NETIPV4_FAILURE;
		}

		/*Allocation of memory for pLnxVrfMsg */
		if (LNX_VRF_MSG_ENTRY_ALLOC (pLnxVrfMsg) == NULL)
		{
			return NETIPV4_FAILURE;
		}

		MEMSET (pLnxVrfMsg, 0, sizeof (tLnxVrfMsg));
		pLnxVrfMsg->u4VrfId = pLnxVrfPostBack->u4ContextId;
		pLnxVrfMsg->u1MsgType = pLnxVrfPostBack->u1MsgType;
		pLnxVrfMsg->i4Sockdomain = pLnxVrfPostBack->i4Sockdomain;
		pLnxVrfMsg->i4SockType = pLnxVrfPostBack->i4SockType;
		pLnxVrfMsg->i4SockProto = pLnxVrfPostBack->i4SockProto;
		pLnxVrfMsg->u4IfIndex = pLnxVrfPostBack->u4IfIndex;
        pLnxVrfMsg->u1IfType = pLnxVrfPostBack->u1IfType;
		/*Currently we are using only one event process funtion, but using many unnecessary events
		 * we can use single event itself and after message receive via queue, processing will be based on msg type */
		u4Event = LNX_VRF_EVENT;

		/*Post the message in Queue */
		if (OsixQueSend
				((tOsixQId) pLnxVrfInfo->LnxVrfQueId, (UINT1 *) &pLnxVrfMsg,
				 OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
		{
			LNX_VRF_MSG_FREE (pLnxVrfMsg);
			return NETIPV4_FAILURE;
		}
		/*Send an event for create context */
		if (OsixEvtSend (pLnxVrfInfo->u4TskId, u4Event) != OSIX_SUCCESS)
		{
			LNX_VRF_MSG_FREE (pLnxVrfMsg);
			return NETIPV4_FAILURE;
		}

		OsixEvtRecv (pLnxVrfInfo->u4TskId, LNX_VRF_UDP_SOCKET_STATUS_EVENT | 
				LNX_VRF_FAILURE_STATUS_EVENT ,
				OSIX_WAIT, &u4EventMask);

		if(u4EventMask & LNX_VRF_UDP_SOCKET_STATUS_EVENT)
		{
			if (OsixQueRecv (gLnxPostBkIpQId, (UINT1 *) &pLnxVrfMsg,
						OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
			{

				*pi4SockFd = pLnxVrfMsg->i4SockId;
				LNX_VRF_MSG_FREE (pLnxVrfMsg);
				return NETIPV4_SUCCESS;
			}
			LNX_VRF_MSG_FREE (pLnxVrfMsg);
			return NETIPV4_SUCCESS;
		}
		if(u4EventMask & LNX_VRF_FAILURE_STATUS_EVENT)
		{
			/*Freeing pLnxVrfMsg*/
			LNX_VRF_MSG_FREE (pLnxVrfMsg);
			return NETIPV4_FAILURE;
		}

	}
	else
	{
		*pi4SockFd = LnxVrfOpenSocket (pLnxVrfPostBack->i4Sockdomain, pLnxVrfPostBack->i4SockType, pLnxVrfPostBack->i4SockProto);
		if (*pi4SockFd < 0)
		{
			perror ("socket creation failed !!!\r\n");
			return NETIPV4_FAILURE;
		}
		return NETIPV4_SUCCESS;
	}
	return NETIPV4_FAILURE;
}

UINT4
LnxVrfOpenSockInVrfCxt (UINT4 u4ContextId, INT4 i4Sockdomain, INT4 i4SockType,
                        INT4 i4SockProto)
{
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    tLnxVrfMsg         *pLnxVrfMsg = NULL;
    INT4                i4SockId = -1;
    tOsixTaskId 	u4TskId = LNXVRF_ZERO; 

    i4SockId = LnxVrfOpenSocket (i4Sockdomain, i4SockType, i4SockProto);
    if (i4SockId < 0)
    {
        perror ("socket creation failed !!!\r\n");
    }

    pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
    if (pLnxVrfInfo == NULL)
    {
	    OsixTskIdSelf (&u4TskId);
        OsixEvtSend (u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    /*Allocation of memory for pLnxVrfMsg */
    if (LNX_VRF_MSG_ENTRY_ALLOC (pLnxVrfMsg) == NULL)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    MEMSET (pLnxVrfMsg, 0, sizeof (tLnxVrfMsg));
    pLnxVrfMsg->u4VrfId = u4ContextId;
    pLnxVrfMsg->u1MsgType = LNX_VRF_OPEN_SOCKET;
    pLnxVrfMsg->i4SockId = i4SockId;
    /*Post the message in Queue */
    if (OsixQueSend (gLnxPostBkIpQId, (UINT1 *) &pLnxVrfMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }
    /*Send an event for create context */
    if (OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_UDP_SOCKET_STATUS_EVENT) !=
        OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

INT4 LnxVrfNetlinkSockInit(UINT4 u4VrfId)
{

    UINT4               u4Groups;
    INT4                i4Ret;
    struct sockaddr_nl  snl;

    u4Groups =
        RTMGRP_NOTIFY | RTMGRP_NEIGH | RTMGRP_IPV6_IFADDR | RTMGRP_IPV6_PREFIX;

    gLnxVrfNetLinkSock[u4VrfId] = LnxVrfOpenSocket (AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (gLnxVrfNetLinkSock[u4VrfId] < 0)
    {
        perror ("NETLINK socket  for ARP creation failed.\n");
        return NETIPV4_FAILURE;
    }
    i4Ret = fcntl (gLnxVrfNetLinkSock[u4VrfId], F_SETFL, O_NONBLOCK);
    if (i4Ret < 0)
    {
        perror ("NETLINK socket for ARP fcntl failed.\n");
        close (gLnxVrfNetLinkSock[u4VrfId]);
        return NETIPV4_FAILURE;
    }

    MEMSET (&snl, 0, sizeof snl);
    snl.nl_family = AF_NETLINK;
    snl.nl_groups = u4Groups;

    /* Bind the socket to the netlink structure for anything. */
    i4Ret = bind (gLnxVrfNetLinkSock[u4VrfId], (struct sockaddr *) &snl, sizeof snl);
    if (i4Ret < 0)
    {
        perror (" NETLINK socket for ARP bind failed .\n");
        close (gLnxVrfNetLinkSock[u4VrfId]);
        return NETIPV4_FAILURE;
    }
    SelAddFd(gLnxVrfNetLinkSock[u4VrfId],ArpNotifyArpTask);

    return NETIPV4_SUCCESS;

}
/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfLoopbackUpdStatus
 *
 * Description        : This function will update the status of loopback
 *                      
 *
 * Input(s)           : u4ContextId. 
 *
 * Output(s)          : None.
 *
 *********************************************************************/
INT1 
LnxVrfLoopbackUpdStatus(UINT1 *pu1DevName,UINT1 u1Action,UINT1 u4VrfId)
{
    INT4                i4Err = -1;
    struct ifreq        if_req;

    MEMSET (&if_req, 0, sizeof (if_req));
    STRCPY (if_req.ifr_name, pu1DevName);
    if_req.ifr_addr.sa_family = AF_INET;
    if (u1Action == CFA_IF_UP)
    {
        if_req.ifr_flags = IFF_UP | IFF_BROADCAST | IFF_MULTICAST |
            IFF_RUNNING;

    }
    else
    {
        if_req.ifr_flags = IFF_BROADCAST | IFF_MULTICAST | IFF_RUNNING;
    }
    i4Err = ioctl (gai4SocketId[u4VrfId], SIOCSIFFLAGS, &if_req);
    if (i4Err < 0)
    {
        perror ("IF FLAGS Set");
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}


/*-------------------------------------------------------------------+
 *
 * Function           : LnxVrfDeInitForContext
 *
 * Description        : This function will clear the resources allocated 
 *                      for context in case of Context creation fails  
 *
 * Input(s)           : u4ContextId. 
 *
 * Output(s)          : None.
 *
 * Returns            : NETIPV4_SUCCESS / NETIPV4_FAILURE
 *
 * Action             : 
 *
 *         
 ********************************************************************/

INT1
LnxVrfDeInitForContext (UINT4 u4ContextId)
{
	tLnxVrfInfo        *pLnxVrfInfo = NULL;
        tOsixTaskId        u4TskId = LNXVRF_ZERO;

	pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);

	if (pLnxVrfInfo == NULL)
	{
		return NETIPV4_FAILURE;
	}
        u4TskId = pLnxVrfInfo->u4TskId;
	if((tOsixSemId) pLnxVrfInfo->LnxVrfSemId != NULL)
	{
		OsixSemDel ((tOsixSemId) pLnxVrfInfo->LnxVrfSemId);
	}
	if ((tOsixQId)pLnxVrfInfo->LnxVrfQueId != NULL)
	{
		OsixQueDel ((tOsixQId) pLnxVrfInfo->LnxVrfQueId);
	}
	if(gai4SocketId[u4ContextId] != -1)
	{
		close (gai4SocketId[u4ContextId]);
		gai4SocketId[u4ContextId] = -1;
	}
	if(gLnxVrfNetLinkSock[u4ContextId] != -1)
	{
		close(gLnxVrfNetLinkSock[u4ContextId]);
		gLnxVrfNetLinkSock[u4ContextId] = -1;
	}

	if(RBTreeRem (gLnxVrfInfoRBRoot, pLnxVrfInfo) == NULL)
	{
		return NETIPV4_FAILURE;
	}

	LNX_VRF_INFO_FREE(pLnxVrfInfo);
	OsixTskDel (u4TskId);
	return NETIPV4_SUCCESS;
}

INT4
LnxVrfGetNetnsInteger (INT4 *val, CHR1 *arg, INT4 base)
{
    FS_LONG            res;
    CHR1               *ptr;

    if (!arg || !*arg)
        return NETIPV4_FAILURE;
    res = strtol (arg, &ptr, base);
    if (!ptr || ptr == arg || *ptr || res > INT_MAX || res < INT_MIN)
    {
        return NETIPV4_FAILURE;
    }
    *val = res;
    return NETIPV4_SUCCESS;
}

INT4
LnxVrfGetNetnsFd (CHR1 *name)
{
    CHR1                pathbuf[MAX_NETNS_PATH];
    CHR1         *path, *ptr;

    path = name;
    ptr = strchr (name, '/');
    if (!ptr)
    {
        SNPRINTF (pathbuf, sizeof (pathbuf), "%s/%s", NETNS_RUN_DIR, name);
        path = pathbuf;
    }
    return open (path, O_RDONLY);
}

INT4
LnxVrfMapUnMapIntInLnxCxt (INT4 cmd, UINT4 flags, CHR1 *dev, CHR1 *argv)
{
	INT4                 len;
	CHR1               *name = NULL;
	tNtPktHdr           NlReq;
	INT4                netns = -1;

	memset (&NlReq, 0, sizeof (NlReq));

	NlReq.n.nlmsg_len = NLMSG_LENGTH (sizeof (struct ifinfomsg));
	NlReq.n.nlmsg_flags = NLM_F_REQUEST | flags;
	NlReq.n.nlmsg_type = cmd;
	NlReq.ifi.ifi_family = 0;

	if ((netns = LnxVrfGetNetnsFd (argv)) >= 0)
		LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_NET_NS_FD, &netns, 4);
	else if (LnxVrfGetNetnsInteger (&netns, argv, 0) == 0)
		LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_NET_NS_PID, &netns, 4);

	if (LnxIpNetRequest (&nlh, AF_UNSPEC, RTM_GETLINK) < 0)
	{
		return NETIPV4_FAILURE;
	}

	if( LnxIpNetParseInfo (LnxIpNetIfAddFilter, &nlh, NULL) == NETIPV4_FAILURE)
	{
		return NETIPV4_FAILURE;
	}



	if (!(flags & NLM_F_CREATE))
	{
		if (!dev)
		{
                        /*Need to add Critical Message*/
			/*fprintf (stderr, "Not enough information: \"dev\" "
					"argument is required.\n");*/
                        return NETIPV4_FAILURE;
		}

		NlReq.ifi.ifi_index = if_nametoindex (dev);
		if (NlReq.ifi.ifi_index == 0)
		{
			/*fprintf (stderr, "Cannot find device \"%s\"\n", dev);*/
                        /*Need to add Critical Message*/
			return NETIPV4_FAILURE;
		}
	}
	else
	{
		/* Allow "ip link add dev" and "ip link add name" */
		if (!name)
			name = dev;
	}
	if (name)
	{
		len = strlen (name) + 1;
		if ( (len == 1) || (len > IFNAMSIZ) )
		{
			/* Trace message need to be added*/
			return NETIPV4_FAILURE;
		}
		LnxIpNetLinkAddAddAttr (&NlReq.n, sizeof (NlReq), IFLA_IFNAME, name, len);
	}

	if (LnxIpNetTalkToKernel (&nlh, &NlReq.n) < 0)
	{
		return NETIPV4_FAILURE;
	}

	return NETIPV4_SUCCESS;
}
/*****************************************************************************/
/*                                                                           */
/* Function     : LnxVrfInitComplete                                         */
/*                                                                           */
/* Description  : Function to Indicate Task Init status                      */
/*                                                                           */
/* Input        : u4Status   - Task Status                                   */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID LnxVrfInitComplete (UINT4 u4Status)
{
    /* Depending on the status returned, set the Task Init Status
     * for the current task.
     */
    if (u4Status == OSIX_SUCCESS)
    {
	    gCurTskStatus = TSK_INIT_SUCCESS;
    }
    else
    {
	    gCurTskStatus = TSK_INIT_FAILURE;
    }

    /* Relinquish the "SEQ" semaphore and return */
    if (OsixSemGive (gLnxVrfGlobSemId) != OSIX_SUCCESS)
    {
        return;
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LnxVrfGetSocketFd                                          */
/*                                                                           */
/* Description  : Function to to get context based socketFd                  */
/*                                                                           */
/* Input        : u4ContextId - contextId                                    */
/*                i4Sockdomain - input for socket()                          */
/*                i4SockType - input for socket()                            */
/*                i4SockProto - input for socket()                           */
/*                u1MsgType - Messagetype of the task requested socketfd     */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : i4SockFd                                                   */
/*                                                                           */
/*****************************************************************************/

INT4 LnxVrfGetSocketFd (UINT4 u4ContextId, INT4 i4Sockdomain, INT4 i4SockType, 
                        INT4 i4SockProto, UINT1 u1MsgType)
{
    tLnxVrfEventInfo    LnxVrfEventInfo;
    INT4    i4SockFd = -1;

    MEMSET(&LnxVrfEventInfo,0,sizeof(tLnxVrfEventInfo));
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = i4Sockdomain;
    LnxVrfEventInfo.i4SockType = i4SockType;
    LnxVrfEventInfo.i4SockProto = i4SockProto;
    LnxVrfEventInfo.u1MsgType = u1MsgType;
    LnxVrfSockLock();
    LnxVrfEventHandling(&LnxVrfEventInfo, &i4SockFd);
    LnxVrfSockUnLock();
    return i4SockFd;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpHandleLoopbackIfaceMapping
 *
 * Description        : This function will unmap the interface from 
 *                      default context or from non-default context and 
 *                      attach it to new context. On unmapping,interface 
 *                      will be mapped to default context.
 *
 *
 *
 * Input(s)           : u4VrfId,u4IfIndex
 *
 * Output(s)          : NONE
 *
 * Returns            : NONE
 ********************************************************************/
INT4
IpHandleLoopbackIfaceMapping (UINT4 u4NewCxtId, UINT4 u4IfIndex)
{
    tCfaIfInfo          CfaIfInfo;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    tLnxVrfMsg         *pLnxVrfMsg = NULL;
    INT4                i4SockId = -1;
    INT4                i4RetVal = -1;
    INT4                i4IfIpPortNum = 0;
    tLnxVrfIfInfo *pLnxVrfIfInfo = NULL;
    tOsixTaskId         u4TskId = LNXVRF_ZERO;

    pLnxVrfInfo = LnxVrfInfoGet (u4NewCxtId);
    if (pLnxVrfInfo == NULL)
    {
        OsixTskIdSelf (&u4TskId);
        OsixEvtSend (u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    if(LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,(CHR1 *) LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    if (LinuxIpDeleteLoopbackInterface (CfaIfInfo.au1IfName, VCM_DEFAULT_CONTEXT)
        == OSIX_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    if(LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS, (CHR1 *) pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }


    if( LinuxIpCreateLoopbackInterface (CfaIfInfo.au1IfName, u4NewCxtId) == OSIX_FAILURE )
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    if(LNX_VRF_IFINFO_ENTRY_ALLOC(pLnxVrfIfInfo) == NULL)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    MEMSET(pLnxVrfIfInfo,0,sizeof(tLnxVrfIfInfo));
    pLnxVrfIfInfo->u4IfIndex=u4IfIndex;
    pLnxVrfIfInfo->u4ContextId = u4NewCxtId;
    MEMCPY(pLnxVrfIfInfo->au1IfName,CfaIfInfo.au1IfName,STRLEN(CfaIfInfo.au1IfName));

    if(RBTreeAdd(gLnxVrfIfInfoRBRoot,pLnxVrfIfInfo) == RB_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    i4RetVal = LinuxIpGetInterfaceIndex
        (CfaIfInfo.au1IfName, &i4IfIpPortNum);
    if (i4RetVal == OSIX_SUCCESS)
    {
        LnxIpAddPortToMappingTable ((UINT2)u4IfIndex,
                                    i4IfIpPortNum,
                                    CfaIfInfo.u1IfType);

        CfaIfInfo.i4IpPort = i4IfIpPortNum;
        CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex,
                      &CfaIfInfo);
    }
    else
    {
        PRINTF ("LinuxIpGetInterfaceIndex returns fail for Dev%s\n",
                CfaIfInfo.au1IfName);
        fflush (stdout);
    }

    /*Allocation of memory for pLnxVrfMsg */
    if (LNX_VRF_MSG_ENTRY_ALLOC (pLnxVrfMsg) == NULL)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    MEMSET (pLnxVrfMsg, 0, sizeof (tLnxVrfMsg));
    pLnxVrfMsg->u4VrfId = u4NewCxtId;
    pLnxVrfMsg->u1MsgType = LNX_VRF_IF_MAP_CONTEXT;
    pLnxVrfMsg->i4SockId = i4SockId;
#if LNXIP6_WANTED
    LnxVrfUpdateCxtIdforIpv6Int(u4NewCxtId, u4IfIndex,i4IfIpPortNum);
#endif
    /*Post the message in Queue */
    if (OsixQueSend (gLnxPostBkIpQId, (UINT1 *) &pLnxVrfMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    /*Send an event for create context */
    if (OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_UDP_SOCKET_STATUS_EVENT) !=
        OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           : IpHandleLoopbackIfaceUnMapping
 *
 * Description        : This function will unmap the interface from 
 *                      default context or from non-default context and 
 *                      attach it to new context. On unmapping,interface 
 *                      will be mapped to default context.
 *
 * Input(s)           : u4NewCxtId,u4IfIndex
 *
 * Output(s)          : NONE
 *
 * Returns            : NONE
 ********************************************************************/

INT4
IpHandleLoopbackIfaceUnMapping (UINT4 u4NewCxtId, UINT4 u4IfIndex)
{
    INT4                i4SockId = -1;
    INT4                i4RetVal = 0;
    INT4                i4IfIpPortNum = 0;
    tCfaIfInfo          CfaIfInfo;
    tLnxVrfMsg         *pLnxVrfMsg = NULL;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    tLnxVrfIfInfo       LnxVrfIfInfo;
    tLnxVrfIfInfo       *pLnxVrfIfInfo = NULL;
    tOsixTaskId        	u4TskId = LNXVRF_ZERO;

    pLnxVrfInfo = LnxVrfInfoGet (u4NewCxtId);
    if (pLnxVrfInfo == NULL)
    {
        OsixTskIdSelf (&u4TskId);
        OsixEvtSend (u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    /* 1 for name space name
     * 2 for ns PID*/
    if (LinuxIpDeleteLoopbackInterface (CfaIfInfo.au1IfName, u4NewCxtId) == OSIX_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    if(LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS, (CHR1 *)LNX_VRF_DEFAULT_NS_PID) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    if(LinuxIpCreateLoopbackInterface (CfaIfInfo.au1IfName, VCM_DEFAULT_CONTEXT)
       == OSIX_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }
    i4RetVal = LinuxIpGetInterfaceIndex
        (CfaIfInfo.au1IfName, &i4IfIpPortNum);
    if (i4RetVal == OSIX_SUCCESS)
    {
	    if(LnxIpAddPortToMappingTable ((UINT2)u4IfIndex,
				    i4IfIpPortNum,
				    CfaIfInfo.u1IfType) == OSIX_FAILURE)
	    {
		    OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
		    return NETIPV4_FAILURE;
	    }
	    CfaIfInfo.i4IpPort = i4IfIpPortNum;
	    CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex,
			    &CfaIfInfo);
    }
    else
    {
        PRINTF ("LinuxIpGetInterfaceIndex returns fail for Dev%s\n",
                CfaIfInfo.au1IfName);
        fflush (stdout);
    }

    if(LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS, (CHR1 *)pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    if (LNX_VRF_MSG_ENTRY_ALLOC (pLnxVrfMsg) == NULL)
    {
        OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
        return NETIPV4_FAILURE;
    }

    MEMSET (pLnxVrfMsg, 0, sizeof (tLnxVrfMsg));
    pLnxVrfMsg->u4VrfId = u4NewCxtId;
    pLnxVrfMsg->i4SockId = i4SockId;
    STRCPY(LnxVrfIfInfo.au1IfName,CfaIfInfo.au1IfName);

    /*Remove LnxVrfIfInfo node*/
    pLnxVrfIfInfo = RBTreeGet(gLnxVrfIfInfoRBRoot,(tRBElem *)&LnxVrfIfInfo);
    if(pLnxVrfIfInfo != NULL)
    {
        if(RBTreeRem (gLnxVrfIfInfoRBRoot, pLnxVrfIfInfo) == NULL)
        {
            OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_FAILURE_STATUS_EVENT);
            return NETIPV4_FAILURE;
        }
        LNX_VRF_IFINFO_FREE(pLnxVrfIfInfo);
    }

#if LNXIP6_WANTED
    LnxVrfUpdateCxtIdforIpv6Int(VCM_DEFAULT_CONTEXT, u4IfIndex,i4IfIpPortNum);
#endif

    /*Post the message in Queue */
    if (OsixQueSend (gLnxPostBkIpQId, (UINT1 *) &pLnxVrfMsg, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }
    /*Send an event for create context */
    if (OsixEvtSend (pLnxVrfInfo->u4TskId, LNX_VRF_UDP_SOCKET_STATUS_EVENT) !=
        OSIX_SUCCESS)
    {
        LNX_VRF_MSG_FREE (pLnxVrfMsg);
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

#endif /* _LNXVRF_C */
