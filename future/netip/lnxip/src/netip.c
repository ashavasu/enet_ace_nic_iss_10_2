/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: netip.c,v 1.41 2017/12/20 11:18:46 siva Exp $
 *
 * Description: Standardised Ip Interface with HL protocols.
 *
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "rtm.h"
#include "trieapif.h"
#include "triecidr.h"
#include "lnxip.h"
#include "lnxipmod.h"
#include "lnxipreg.h"
#include "vcm.h"
#ifdef ICCH_WANTED
#include "icch.h"
#endif
#ifdef VRRP_WANTED
#include "vrrp.h"
#endif
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ipnpwr.h"
#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#include "lnxvrf.h"
#endif

extern tHLProtoRegnTbl gHLRegnTbl[];
extern INT1         LnxIpGetForwarding (UINT4 *pu4IpForwarding);
extern UINT4        NetipIndexMgrGetAvailableIndex (UINT1 u1GrpID);
extern UINT1        NetipIndexMgrRelIndex (UINT1 u1GrpID, UINT4 u4Index);
extern UINT1        NetipIndexMgrCheckGrpFull (UINT1 u1GrpID);
UINT4               gu4ContextId = VCM_DEFAULT_CONTEXT;
/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfInfo
 *
 * Description      :   This function provides the Interface related
 *                      Information for a given IfIndex.
 *
 * Inputs           :   u4IfIndex
 *
 * Outputs          :   pNetIpIfInfo - If-Info for the given IfIndex.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIfInfo (UINT4 u4IfIndex, tNetIpv4IfInfo * pNetIpIfInfo)
{
    struct ifreq        ifreq;
    struct sockaddr_in *saddr;
    tCfaIfInfo          CfaIfInfo;
    INT4                i4SockFd;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
#endif

    MEMSET (pNetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetCfaIfIndexFromPort (u4IfIndex,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }
    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }
    else
    {
        pNetIpIfInfo->u4IfSpeed = CfaIfInfo.u4IfSpeed;
        pNetIpIfInfo->u4IfHighSpeed = CfaIfInfo.u4IfHighSpeed;
        pNetIpIfInfo->u4IfType = CfaIfInfo.u1IfType;
        pNetIpIfInfo->u2VlanId = CfaIfInfo.u2VlanId;
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (VcmGetContextIdFromCfaIfIndex (u4CfaIfIndex, &u4ContextId) ==
        VCM_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    i4SockFd = LnxVrfGetSocketFd (u4ContextId, AF_INET, SOCK_DGRAM,
                                  0, LNX_VRF_OPEN_SOCKET);

    pNetIpIfInfo->u4ContextId = u4ContextId;
#endif

#if !(defined(VRF_WANTED) && defined(LINUX_310_WANTED))
    /* Check if the Lnx port no. is valid */
    if (if_indextoname (u4IfIndex, ifreq.ifr_name) == NULL)
    {
        return (NETIPV4_FAILURE);
    }

    if ((i4SockFd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("NetIpv4GetIfInfo - socket creation failed\r\n");
        return (NETIPV4_FAILURE);
    }

#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    MEMCPY (ifreq.ifr_name, CfaIfInfo.au1IfName, IFNAMSIZ);
#endif
    MEMCPY (pNetIpIfInfo->au1IfName, ifreq.ifr_name, IFNAMSIZ);
    pNetIpIfInfo->u4IfIndex = u4IfIndex;    /* IP Port no */
    pNetIpIfInfo->u4CfaIfIndex = u4CfaIfIndex;    /* Cfa If Index */

    if (ioctl (i4SockFd, SIOCGIFMTU, &ifreq) == 0)
    {
        pNetIpIfInfo->u4Mtu = ifreq.ifr_mtu;
    }

    if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) == 0)
    {
        saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
        pNetIpIfInfo->u4Addr = OSIX_NTOHL (saddr->sin_addr.s_addr);
    }

    if (ioctl (i4SockFd, SIOCGIFNETMASK, &ifreq) == 0)
    {
        saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_netmask;
        pNetIpIfInfo->u4NetMask = OSIX_NTOHL (saddr->sin_addr.s_addr);
    }

    if (ioctl (i4SockFd, SIOCGIFBRDADDR, &ifreq) == 0)
    {
        saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_broadaddr;
        pNetIpIfInfo->u4BcastAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);
    }

    /* EncapType - to be obtained from CFA. */
    pNetIpIfInfo->u1EncapType = CFA_ENCAP_ENETV2;

    /* Set the default value */
    pNetIpIfInfo->u4Admin = IPIF_ADMIN_DISABLE;
    pNetIpIfInfo->u4Oper = IPIF_OPER_DISABLE;

    if (ioctl (i4SockFd, SIOCGIFFLAGS, &ifreq) == 0)
    {
        if (ifreq.ifr_flags & IFF_UP)
        {
            if (CfaIfInfo.u1IfOperStatus == CFA_IF_UP)
            {
                pNetIpIfInfo->u4Admin = IPIF_ADMIN_ENABLE;
                pNetIpIfInfo->u4Oper = IPIF_OPER_ENABLE;
            }
        }
    }

    close (i4SockFd);

    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetLinuxIfName
 *
 * Description      :   This function provides the Linux 
 *                      Interface name.
 *
 * Inputs           :   Linux If Index
 *
 * Outputs          :   pu1IfName - If-Name for the given IfIndex.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE
 *
 *******************************************************************************
 */
INT4
NetIpv4GetLinuxIfName (UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    struct ifreq        ifreq;
    if (if_indextoname (u4IfIndex, ifreq.ifr_name) == NULL)
    {
        perror
            ("NetIpv4GetLinuxIfName - getting Linux If name from port failed\r\n");
        return (NETIPV4_FAILURE);
    }
    MEMCPY (pu1IfName, ifreq.ifr_name, IFNAMSIZ);
    return (NETIPV4_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetSrcAddrToUseForDestInCxt
 * *                     Provides the Source address for a given
 *                       destination address in the specified context.
 *
 * Input(s)           :  u4ContextId - the context id
 *                       u4Dest - destination address
 * Output(s)          : *4pSrc_addr_to_use
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             :  Provides the Source address for a given
 *                       destination address in the specified context
------------------------------------------------------------------- */

INT4
NetIpv4GetSrcAddrToUseForDestInCxt (UINT4 u4ContextId, UINT4 u4Dest,
                                    UINT4 *pu4pSrc_addr_to_use)
{
    INT4                i4RetVal = NETIPV4_FAILURE;

    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        return NETIPV4_FAILURE;
    }

    i4RetVal = NetIpv4GetSrcAddrToUseForDest (u4Dest, pu4pSrc_addr_to_use);
    return i4RetVal;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetFirstIfInfoInCxt
 *
 * Description      :   This function provides the First Entry in the If Config
 *                      Record. 
 *
 * Inputs           :   u4ContextId
 *
 * Outputs          :   pNetIpIfInfo - First If Config Record.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 * 
 *******************************************************************************
 */
INT4
NetIpv4GetFirstIfInfoInCxt (UINT4 u4ContextId, tNetIpv4IfInfo * pNetIpIfInfo)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    return (NetIpv4GetFirstIfInfoUsingCxt (pNetIpIfInfo, u4ContextId));
#else

    UNUSED_PARAM (u4ContextId);
    return (NetIpv4GetFirstIfInfo (pNetIpIfInfo));
#endif
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetFirstIfInfo
 *
 * Description      :   This function provides the First Entry in the If Config
 *                      Record. 
 *
 * Inputs           :   None.
 *
 * Outputs          :   pNetIpIfInfo - First If Config Record.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 * 
 *******************************************************************************
 */
INT4
NetIpv4GetFirstIfInfo (tNetIpv4IfInfo * pNetIpIfInfo)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4FirstIfIndex = CFA_INVALID_INDEX;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
    UINT4               u4Index = 0;

    pIfnameIdx = if_nameindex ();
    if (pIfnameIdx == NULL)
    {
        perror ("if_nameindex call failed:");
        return (NETIPV4_FAILURE);
    }
    while (pIfnameIdx[u4Index].if_index != 0)
    {
        /* Skip Linux physical interfaces */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_SUCCESS)
        {
            u4FirstIfIndex = pIfnameIdx[u4Index].if_index;
            break;
        }
        u4Index++;
    }
    if_freenameindex (pIfnameIdx);

    if (u4FirstIfIndex == CFA_INVALID_INDEX)
    {
        return (NETIPV4_FAILURE);
    }

    return (NetIpv4GetIfInfo (u4FirstIfIndex, pNetIpIfInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetNextIfInfoInCxt
 *
 * Description      :   This function returns the Next If Config Record for a
 *                      given IfIndex.
 *
 * Inputs           :   u4IfIndex, u4ContextId
 *
 * Outputs          :   pNextNetIpIfInfo - If-Info of the Next Interface Index.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetNextIfInfoInCxt (UINT4 u4ContextId,
                           UINT4 u4IfIndex, tNetIpv4IfInfo * pNextNetIpIfInfo)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    return (NetIpv4GetNextIfInfoUsingCxt (u4IfIndex, pNextNetIpIfInfo,
                                          u4ContextId));
#else
    UNUSED_PARAM (u4ContextId);
    return (NetIpv4GetNextIfInfo (u4IfIndex, pNextNetIpIfInfo));
#endif
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetNextIfInfo
 *
 * Description      :   This function returns the Next If Config Record for a
 *                      given IfIndex.
 *
 * Inputs           :   u4IfIndex
 *
 * Outputs          :   pNextNetIpIfInfo - If-Info of the Next Interface Index.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetNextIfInfo (UINT4 u4IfIndex, tNetIpv4IfInfo * pNextNetIpIfInfo)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4NextIfIndex = CFA_INVALID_INDEX;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
    UINT4               u4Index = 0;

    pIfnameIdx = if_nameindex ();
    if (pIfnameIdx == NULL)
    {
        perror ("if_nameindex call failed:");
        return (NETIPV4_FAILURE);
    }
    while (pIfnameIdx[u4Index].if_index != 0)
    {
        if (pIfnameIdx[u4Index].if_index > u4IfIndex)
        {
            /* If the index is created through ISS,return that. */
            if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                              &u4CfaIfIndex) == NETIPV4_SUCCESS)
            {
                u4NextIfIndex = pIfnameIdx[u4Index].if_index;
                break;
            }
        }
        u4Index++;
    }

    if_freenameindex (pIfnameIdx);

    if (u4NextIfIndex == CFA_INVALID_INDEX)
    {
        return (NETIPV4_FAILURE);
    }

    return (NetIpv4GetIfInfo (u4NextIfIndex, pNextNetIpIfInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfIndexFromName
 *
 * Description      :   This function returns the IfIndex for a given IfName.
 *
 * Inputs           :   pu1IfName  - IfName.
 *
 * Outputs          :   pu4IfIndex - IfIndex for the given IfName.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */
INT4
NetIpv4GetIfIndexFromName (UINT1 *pu1IfName, UINT4 *pu4IfIndex)
{
    if ((*pu4IfIndex = if_nametoindex ((const CHR1 *) (VOID *) pu1IfName)) == 0)
    {
        return (NETIPV4_FAILURE);
    }
    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfIndexFromAddrInCxt
 *
 * Description      :   This function returns the IfIndex for a given Ip
 *                      Address.
 *
 * Inputs           :   u4IpAddr, u4ContextId
 *
 * Outputs          :   pu4IfIndex - IfIndex for the given Ip Address.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetIfIndexFromAddrInCxt (UINT4 u4ContextId,
                                UINT4 u4IpAddr, UINT4 *pu4IfIndex)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    return (NetIpv4GetIfIndexFromAddrUsingCxt (u4IpAddr, pu4IfIndex,
                                               u4ContextId));
#else
    UNUSED_PARAM (u4ContextId);
    return (NetIpv4GetIfIndexFromAddr (u4IpAddr, pu4IfIndex));
#endif
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfIndexFromAddr
 *
 * Description      :   This function returns the IfIndex for a given Ip
 *                      Address.
 *
 * Inputs           :   u4IpAddr
 *
 * Outputs          :   pu4IfIndex - IfIndex for the given Ip Address.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIfIndexFromAddr (UINT4 u4IpAddr, UINT4 *pu4IfIndex)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4Index = 0;
    struct ifreq        ifreq;
    INT4                i4SockFd;
    struct sockaddr_in *saddr;
    INT4                i4RetVal = NETIPV4_FAILURE;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfEventInfo    LnxVrfEventInfo;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
    UINT4               u4ContextId = VCM_DEFAULT_CONTEXT;
    MEMSET (&LnxVrfEventInfo, 0, sizeof (LnxVrfEventInfo));
#endif

    *pu4IfIndex = 0;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    u4ContextId = gu4ContextId;
    LnxVrfEventInfo.u4ContextId = u4ContextId;
    LnxVrfEventInfo.i4Sockdomain = AF_INET;
    LnxVrfEventInfo.i4SockType = SOCK_DGRAM;
    LnxVrfEventInfo.i4SockProto = 0;
    LnxVrfEventInfo.u1MsgType = LNX_VRF_OPEN_SOCKET;
    LnxVrfSockLock ();
    LnxVrfEventHandling (&LnxVrfEventInfo, &i4SockFd);
    LnxVrfSockUnLock ();
#else
    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
#endif
    if (i4SockFd < 0)
    {
        perror ("NetIpv4GetIfIndexFromAddr - socket creation failed\r\n");
        return (NETIPV4_FAILURE);
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS,
                             (CHR1 *) pLnxVrfInfo->au1NameSpace);
        }
    }
#endif
    pIfnameIdx = if_nameindex ();
    while ((pIfnameIdx != NULL) && (pIfnameIdx[u4Index].if_index != 0))
    {
        /* Skip Linux physical interfaces */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            u4Index++;
            continue;
        }

        MEMCPY (ifreq.ifr_name, pIfnameIdx[u4Index].if_name, IFNAMSIZ);
        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) == 0)
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            if (OSIX_NTOHL (saddr->sin_addr.s_addr) == u4IpAddr)
            {
                *pu4IfIndex = pIfnameIdx[u4Index].if_index;
                i4RetVal = NETIPV4_SUCCESS;
                break;
            }
        }

        u4Index++;
    }
    if (pIfnameIdx != NULL)
    {
        if_freenameindex (pIfnameIdx);
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                             (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
    }
#endif
    close (i4SockFd);

    return (i4RetVal);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfCount
 *
 * Description      :   This function returns the number of active interfaces
 *
 * Inputs           :   None.
 *
 * Outputs          :   pu4IfCount - Number of interfaces.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */
INT4
NetIpv4GetIfCount (UINT4 *pu4IfCount)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4IfCount = 0;
    UINT4               u4Index = 0;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;

    *pu4IfCount = 0;

    pIfnameIdx = if_nameindex ();
    if (pIfnameIdx == NULL)
    {
        perror ("if_nameindex call failed:");
        return (NETIPV4_FAILURE);
    }
    while (pIfnameIdx[u4Index].if_index != 0)
    {
        /* Skip the physical interfaces as we are interested in VLAN 
           interface count. This is done by validating the port no. in
           the map table.  */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_SUCCESS)
        {
            u4IfCount++;
        }

        u4Index++;
    }
    if_freenameindex (pIfnameIdx);

    *pu4IfCount = u4IfCount;
    return (NETIPV4_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IsLoopbackAddress
 *                      Checks the IP Address is a Loopback Address
 *
 * Input(s)           : u4IpAddress - Ip Address
 * Output(s)          : None.
 *
 * Returns            : NETIPV4_SUCCESS - If u4IpAddress is Loopback
 *                                        address
 *                      NETIPV4_FAILURE - If u4IpAddress is not
 *                                        Loopback address
 * Action             : Checks the IP Address is a Loopback Address
------------------------------------------------------------------- */
INT4
NetIpv4IsLoopbackAddress (UINT4 u4IpAddress)
{
    UINT4               u4Port = 0;
    UINT4               u4CfaIfIndex = 0;
    tCfaIfInfo          CfaIfInfo;

    MEMSET ((UINT1 *) &CfaIfInfo, 0, sizeof (CfaIfInfo));

    if (NetIpv4GetIfIndexFromAddr (u4IpAddress, &u4Port) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    if (NetIpv4GetCfaIfIndexFromPort (u4Port, &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetHighestIpAddrInCxt
 *
 * Description      :   This function returns the highest IP Address among the
 *                      active V4 Interface address.
 *
 * Inputs           :   u4ContextId
 *
 * Outputs          :   pu4IfCount - Number of interfaces.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */

INT4
NetIpv4GetHighestIpAddrInCxt (UINT4 u4ContextId, UINT4 *pu4IpAddress)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    return (NetIpv4GetHighestIpAddrUsingCxt (pu4IpAddress, u4ContextId));
#else
    UNUSED_PARAM (u4ContextId);
    return (NetIpv4GetHighestIpAddr (pu4IpAddress));
#endif
}

/*
 *************************************************************************
 * Function Name    :   NetIpv4GetHighestIpAddr
 *
 * Description      :   This function returns the highest IP Address among the
 *                      active V4 Interface address.
 *
 * Inputs           :   None.
 *
 * Outputs          :   pu4IfCount - Number of interfaces.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */
INT4
NetIpv4GetHighestIpAddr (UINT4 *pu4IpAddress)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4Index = 0;
    struct ifreq        ifreq;
    INT4                i4SockFd;
    struct sockaddr_in *saddr;
    UINT4               u4IfAddr = 0;
    UINT4               u4HighIfAddr = 0;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
#ifdef ICCH_WANTED
    UINT4               u4IcclIfIndex = CFA_INVALID_INDEX;
#endif /*ICCH_WANTED */
    if ((i4SockFd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("NetIpv4GetHighestIpAddr - socket creation failed\r\n");
        return (NETIPV4_FAILURE);
    }

    pIfnameIdx = if_nameindex ();
    if (pIfnameIdx == NULL)
    {
        perror ("if_nameindex call failed:");
        close (i4SockFd);
        return (NETIPV4_FAILURE);
    }

#ifdef ICCH_WANTED
    IcchGetIcclL3IfIndex (&u4IcclIfIndex);
#endif /* ICCH_WANTED */
    while (pIfnameIdx[u4Index].if_index != 0)
    {
        /* Skip Linux physical interfaces */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            u4Index++;
            continue;
        }

#ifdef ICCH_WANTED
        if (u4IcclIfIndex == u4CfaIfIndex)
        {
            u4Index++;
            continue;
        }
#endif /*ICCH_WANTED */
        if (u4CfaIfIndex == CFA_OOB_MGMT_IFINDEX)
        {
            u4Index++;
            continue;
        }
        MEMCPY (ifreq.ifr_name, pIfnameIdx[u4Index].if_name, IFNAMSIZ);
        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) == 0)
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            u4IfAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);
            u4HighIfAddr = (u4HighIfAddr > u4IfAddr) ? u4HighIfAddr : u4IfAddr;
        }

        u4Index++;
    }
    if_freenameindex (pIfnameIdx);
    close (i4SockFd);

    *pu4IpAddress = u4HighIfAddr;
    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4LeakRoute
 *
 * Description      :   This function, depending on the value of u1CmdType Adds
 *                      (or) Deletes (or) modifies the given Route Information
 *                      stored in IpFwdTable accordingly.
 *
 * Inputs           :   u1CmdType - Command to ADD | Delete | Modify the Route.
 *                      pNetRtInfo - Route to be Added | Deleted | Modified.
 *
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *******************************************************************************
 */

INT4
NetIpv4LeakRoute (UINT1 u1CmdType, tNetIpv4RtInfo * pNetRtInfo)
{

    INT4                i4RetVal = NETIPV4_FAILURE;

    i4RetVal = RtmIpv4LeakRoute (u1CmdType, pNetRtInfo);

    if (i4RetVal == IP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetRoute
 *
 * Description      :   This function provides the Route (either Exact Route or
 *                      Best route) for a given destination and Mask based on
 *                      the incoming request.
 *
 * Inputs           :   pRtQuery - Infomation about the route to be
 *                                 retrieved.
 *
 * Outputs          :   pNetIpRtInfo - Information about the requested route.
 *
 * Return Value     :   NETIPV4_SUCCESS - if the route is present.
 *                      NETIPV4_FAILURE - if the route is not present.
 *
 ******************************************************************************
 */

INT4
NetIpv4GetRoute (tRtInfoQueryMsg * pRtQuery, tNetIpv4RtInfo * pNetIpRtInfo)
{
    if (RtmNetIpv4GetRoute (pRtQuery, pNetIpRtInfo) == IP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetFirstFwdTableRouteEntry
 *
 * Description      :   This function provides the First Route Entry present in
 *                      the Ip Forwarding Table.
 *
 * Inputs           :   None.
 *
 * Outputs          :   pNetRtInfo - First Route Entry of the Ip Forwarding
 *                      Table.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetFirstFwdTableRouteEntry (tNetIpv4RtInfo * pNetRtInfo)
{
    tRtInfo             RtInfo;
    tRtInfo             NextRtInfo;
    INT4                i4RetVal = NETIPV4_FAILURE;

    if (pNetRtInfo == NULL)
    {
        return (NETIPV4_FAILURE);
    }

    MEMSET (&RtInfo, 0, sizeof (tRtInfo));
    MEMSET (&NextRtInfo, 0, sizeof (tRtInfo));

    /*  Calling IpGetNextRtEntry() with DestAddr and Mask as Zero
     *  returns the First Route Entry in the Ip Forwarding Table
     */

    i4RetVal = RtmNetIpv4GetNextBestRtEntryInCxt (IP_DEFAULT_CONTEXT,
                                                  RtInfo, &NextRtInfo);
    if (i4RetVal == IP_SUCCESS)
    {
        pNetRtInfo->u4DestNet = NextRtInfo.u4DestNet;
        pNetRtInfo->u4DestMask = NextRtInfo.u4DestMask;
        pNetRtInfo->u4Tos = NextRtInfo.u4Tos;
        pNetRtInfo->u4NextHop = NextRtInfo.u4NextHop;
        pNetRtInfo->u4RtIfIndx = NextRtInfo.u4RtIfIndx;
        pNetRtInfo->u4RtAge = NextRtInfo.u4RtAge;
        pNetRtInfo->u4RtNxtHopAs = NextRtInfo.u4RtNxtHopAS;
        pNetRtInfo->i4Metric1 = NextRtInfo.i4Metric1;
        pNetRtInfo->u4RowStatus = NextRtInfo.u4RowStatus;
        pNetRtInfo->u2RtType = NextRtInfo.u2RtType;
        pNetRtInfo->u2RtProto = NextRtInfo.u2RtProto;
        pNetRtInfo->u4RouteTag = NextRtInfo.u4RouteTag;

        return (NETIPV4_SUCCESS);
    }
    return (NETIPV4_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetNextFwdTableRouteEntry
 *
 * Description      :   This function returns the Next Route Entry in the Ip
 *                      Forwarding Table for a given Route Entry.
 *
 * Inputs           :   pNetRtInfo - Route Entry for which the Next Entry is
 *                      required.
 *
 * Outputs          :   pNextNetRtInfo - Next Route Entry in the Ip Forwarding
 *                      Table.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4GetNextFwdTableRouteEntry (tNetIpv4RtInfo * pNetRtInfo,
                                  tNetIpv4RtInfo * pNextNetRtInfo)
{

    tRtInfo             RtInfo;
    tRtInfo             NextRtInfo;
    INT4                i4RetVal = NETIPV4_FAILURE;

    if ((pNetRtInfo == NULL) || (pNextNetRtInfo == NULL))
    {
        return (NETIPV4_FAILURE);
    }

    MEMSET (&RtInfo, 0, sizeof (tRtInfo));
    MEMSET (&NextRtInfo, 0, sizeof (tRtInfo));

    RtInfo.u4DestNet = pNetRtInfo->u4DestNet;
    RtInfo.u4DestMask = pNetRtInfo->u4DestMask;
    RtInfo.u4Tos = pNetRtInfo->u4Tos;
    RtInfo.u4NextHop = pNetRtInfo->u4NextHop;
    RtInfo.u4RtIfIndx = pNetRtInfo->u4RtIfIndx;
    RtInfo.u4RtAge = pNetRtInfo->u4RtAge;
    RtInfo.u4RtNxtHopAS = pNetRtInfo->u4RtNxtHopAs;
    RtInfo.i4Metric1 = pNetRtInfo->i4Metric1;
    RtInfo.u4RowStatus = pNetRtInfo->u4RowStatus;
    RtInfo.u2RtType = pNetRtInfo->u2RtType;
    RtInfo.u2RtProto = pNetRtInfo->u2RtProto;
    RtInfo.u4RouteTag = pNetRtInfo->u4RouteTag;

    i4RetVal =
        RtmNetIpv4GetNextBestRtEntryInCxt (IP_DEFAULT_CONTEXT, RtInfo,
                                           &NextRtInfo);
    if (i4RetVal == IP_SUCCESS)
    {
        pNextNetRtInfo->u4DestNet = NextRtInfo.u4DestNet;
        pNextNetRtInfo->u4DestMask = NextRtInfo.u4DestMask;
        pNextNetRtInfo->u4Tos = NextRtInfo.u4Tos;
        pNextNetRtInfo->u4NextHop = NextRtInfo.u4NextHop;
        pNextNetRtInfo->u4RtIfIndx = NextRtInfo.u4RtIfIndx;
        pNextNetRtInfo->u4RtAge = NextRtInfo.u4RtAge;
        pNextNetRtInfo->u4RtNxtHopAs = NextRtInfo.u4RtNxtHopAS;
        pNextNetRtInfo->i4Metric1 = NextRtInfo.i4Metric1;
        pNextNetRtInfo->u4RowStatus = NextRtInfo.u4RowStatus;
        pNextNetRtInfo->u2RtType = NextRtInfo.u2RtType;
        pNextNetRtInfo->u2RtProto = NextRtInfo.u2RtProto;
        pNextNetRtInfo->u4RouteTag = NextRtInfo.u4RouteTag;

        return (NETIPV4_SUCCESS);
    }
    return (NETIPV4_FAILURE);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4RegisterHigherLayerProtocol
 *
 * Description      :   This function is used by Higher Layer Protocols to
 *                      register with Ip for Receiving their packets or for
 *                      getting notification on IF changes or Route Changes.
 *
 * Inputs           :   pRegInfo - Information about the registering protocol
 *                                 and the call back functions.
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4RegisterHigherLayerProtocol (tNetIpRegInfo * pRegInfo)
{
    if (gi1RegTableUp != REGUP)
    {
        return (NETIPV4_FAILURE);
    }
    if (pRegInfo->u1ProtoId >= IP_MAX_PROTOCOLS)
    {
        return (NETIPV4_FAILURE);
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if ((gHLRegnTbl[pRegInfo->u1ProtoId].au1CtxReg[pRegInfo->u4ContextId]
         == REGISTER) && (pRegInfo->u1ProtoId != SNOOP_ID))
    {
        return (NETIPV4_FAILURE);
    }
    gHLRegnTbl[pRegInfo->u1ProtoId].au1CtxReg[pRegInfo->u4ContextId] = REGISTER;
#else
    if ((gHLRegnTbl[pRegInfo->u1ProtoId].u1RegFlag == REGISTER) &&
        (pRegInfo->u1ProtoId != SNOOP_ID))
    {
        return (NETIPV4_FAILURE);
    }

    gHLRegnTbl[pRegInfo->u1ProtoId].u1RegFlag = REGISTER;
#endif
    if (pRegInfo->u2InfoMask & NETIPV4_IFCHG_REQ)
    {
        gHLRegnTbl[pRegInfo->u1ProtoId].pIfStChng =
            (VOID (*)(tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4BitMap))
            pRegInfo->pIfStChng;
    }
    else
    {
        gHLRegnTbl[pRegInfo->u1ProtoId].pIfStChng = NULL;
    }
    if (pRegInfo->u2InfoMask & NETIPV4_ROUTECHG_REQ)
    {
        gHLRegnTbl[pRegInfo->u1ProtoId].pRtChng =
            (VOID (*)
             (tNetIpv4RtInfo * pNetIpRtInfo, tNetIpv4RtInfo * pNetIpRtInfo1,
              UINT1 u1CmdType)) pRegInfo->pRtChng;
    }
    else
    {
        gHLRegnTbl[pRegInfo->u1ProtoId].pRtChng = NULL;
    }
    if (pRegInfo->u2InfoMask & NETIPV4_PROTO_PKT_REQ)
    {
        gHLRegnTbl[pRegInfo->u1ProtoId].pProtoPktRecv =
            (VOID (*)(tIP_BUF_CHAIN_HEADER *, UINT2, UINT4, tIP_INTERFACE,
                      UINT1)) pRegInfo->pProtoPktRecv;
    }
    else
    {
        gHLRegnTbl[pRegInfo->u1ProtoId].pProtoPktRecv = NULL;
    }
#if defined (VRRP_WANTED) && defined (NPAPI_WANTED)
    if (pRegInfo->u1ProtoId == VRRPMODULE)
    {
        FsNpIpv4VrrpInstallFilter ();
    }
#endif

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    IpActOnProtoRegDereg (pRegInfo->u1ProtoId, pRegInfo->u4ContextId);
#else
    IpActOnProtoRegDereg (pRegInfo->u1ProtoId);
#endif
    return (NETIPV4_SUCCESS);
}

 /******************************************************************************
 * Function Name    :   NetIpv4DeRegisterHigherLayerProtocolInCxt 
 *
 * Description      :   This function De-Registers the Protocol specified by
 *                      the Application.
 *
 * Inputs           :   u1ProtoId - Protocol Identifier.
 *                      u4ContextId - Context ID
 *
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4DeRegisterHigherLayerProtocolInCxt (UINT4 u4ContextId, UINT1 u1ProtoId)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    return (NetIpv4DeRegisterHigherLayerProtocolUsingCxt
            (u1ProtoId, u4ContextId));
#else
    UNUSED_PARAM (u4ContextId);
    return NetIpv4DeRegisterHigherLayerProtocol (u1ProtoId);
#endif
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4DeRegisterHigherLayerProtocol 
 *
 * Description      :   This function De-Registers the Protocol specified by
 *                      the Application.
 *
 * Inputs           :   u1ProtoId - Protocol Identifier.
 *
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4DeRegisterHigherLayerProtocol (UINT1 u1ProtoId)
{
    if (u1ProtoId >= IP_MAX_PROTOCOLS)
    {
        return (NETIPV4_FAILURE);
    }
    if (gHLRegnTbl[u1ProtoId].u1RegFlag == DEREGISTER)
    {
        return (NETIPV4_SUCCESS);
    }
    else
    {
        gHLRegnTbl[u1ProtoId].u1RegFlag = DEREGISTER;
        gHLRegnTbl[u1ProtoId].pIfStChng = NULL;
        gHLRegnTbl[u1ProtoId].pRtChng = NULL;
        gHLRegnTbl[u1ProtoId].pProtoPktRecv = NULL;
        return (NETIPV4_SUCCESS);
    }
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIpForwardingInCxt
 *
 * Description      :   This function returns the IP forwarding flag
 *
 * Inputs           :   None 
 *
 * Outputs          :   u4ContextId - context identifier
 *                      pu1IpForward -  Global Ip Forwarding flag
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIpForwardingInCxt (UINT4 u4ContextId, UINT1 *pu1IpForward)
{
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    return (NetIpv4GetIpForwardingUsingCxt (pu1IpForward, u4ContextId));
#else
    UNUSED_PARAM (u4ContextId);
    return (NetIpv4GetIpForwarding (pu1IpForward));
#endif
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIpForwarding
 *
 * Description      :   This function returns the IP forwarding flag
 *
 * Inputs           :   None 
 *
 * Outputs          :   pu1IpForward -  Global Ip Forwarding flag
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIpForwarding (UINT1 *pu1IpForward)
{
    UINT4               u4IpForward;

    if (LnxIpGetForwarding (&u4IpForward) == SNMP_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    *pu1IpForward = (UINT1) u4IpForward;
    return (NETIPV4_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IfIsOurAddress
 * *                     Checks whether IP belongs to Local Interface
 *
 * Input(s)           : u4IpAddress - Ip Address
 * Output(s)          : None.
                                                                                                                             
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4IfIsOurAddress (UINT4 u4IpAddress)
{
    return (NetIpv4IfIsOurAddressInCxt (VCM_DEFAULT_CONTEXT, u4IpAddress));
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IfIsOurAddressInCxt
 * *                     Checks whether IP belongs to Local Interface
 *
 * Input(s)           :  u4ContextId - context identifier
 *                       u4IpAddress - Ip Address
 * Output(s)          : None.
                                                                                                                             
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4IfIsOurAddressInCxt (UINT4 u4ContextId, UINT4 u4IpAddress)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4Index = 0;
    struct ifreq        ifreq;
    INT4                i4SockFd;
    struct sockaddr_in *saddr;
    UINT4               u4IfAddr = 0;
    INT4                i4RetVal = NETIPV4_FAILURE;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    tLnxVrfInfo        *pLnxVrfInfo = NULL;
#else
    UNUSED_PARAM (u4ContextId);
#endif

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    i4SockFd = LnxVrfGetSocketFd (u4ContextId, AF_INET, SOCK_DGRAM,
                                  0, LNX_VRF_OPEN_SOCKET);
#else
    i4SockFd = socket (AF_INET, SOCK_DGRAM, 0);
#endif
    if (i4SockFd < 0)
    {
        perror ("NetIpv4IfIsOurAddress - socket creation failed\r\n");
        return (NETIPV4_FAILURE);
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS,
                             (CHR1 *) pLnxVrfInfo->au1NameSpace);
        }
    }
#endif
    pIfnameIdx = if_nameindex ();
    while ((pIfnameIdx != NULL) && (pIfnameIdx[u4Index].if_index != 0))
    {
        /* Skip Linux physical interfaces */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            u4Index++;
            continue;
        }

        MEMSET (&ifreq, 0, sizeof (ifreq));
        if (STRLEN (pIfnameIdx[u4Index].if_name) < IFNAMSIZ)
        {
            MEMCPY (ifreq.ifr_name, pIfnameIdx[u4Index].if_name,
                    (STRLEN (pIfnameIdx[u4Index].if_name) + 1));
        }
        else
        {
            MEMCPY (ifreq.ifr_name, pIfnameIdx[u4Index].if_name, IFNAMSIZ);
        }

        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) == 0)
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            u4IfAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);
            if (u4IfAddr == u4IpAddress)
            {
                i4RetVal = NETIPV4_SUCCESS;
                break;
            }
        }

        u4Index++;
    }
    if (pIfnameIdx != NULL)
    {
        if_freenameindex (pIfnameIdx);
    }
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    if (u4ContextId != 0)
    {
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                             (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
    }
#endif
    close (i4SockFd);

    return (i4RetVal);
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetCfaIfIndexFromPort
 * *                   Provides the CFA IfIndex Corresponding to IP Port No
 *
 * Input(s)           : u4Port - IP Port Number
 * Output(s)          : CFA Interface Index
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Provides CFA IFIndex Corresponding to IP Port No
------------------------------------------------------------------- */
INT4
NetIpv4GetCfaIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    INT4                i4RetVal = NETIPV4_FAILURE;
    tLnxIpPortMapNode  *pPortMapNode;

    *pu4IfIndex = CFA_INVALID_INDEX;

    pPortMapNode = LnxIpGetPortMapNode (u4Port);

    if (NULL != pPortMapNode)
    {
        *pu4IfIndex = pPortMapNode->u4CfaIfIndex;
        i4RetVal = NETIPV4_SUCCESS;

    }

    return (i4RetVal);
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetPortFromIfIndex
 * *                     Provides the IP Port Number Corresponding to IfIndex
 *
 * Input(s)           : u4IfIndex - CFA IfIndex
 * Output(s)          : IP Port Number.
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Provides the IP Port Number Corresponding to IfIndex
------------------------------------------------------------------- */
INT4
NetIpv4GetPortFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4Port)
{
    *pu4Port = CFA_IF_IPPORT ((UINT2) u4IfIndex);

    if ((*pu4Port == 0) || (*pu4Port == CFA_INVALID_INDEX))
    {
        return NETIPV4_FAILURE;
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetSrcAddrToUseForDest
 * *                     Provides the Source address for a given
 *                       destination address
 *
 * Input(s)           : u4Dest - destination address
 * Output(s)          : *4pSrc_addr_to_use
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             :  Provides the Source address for a given
 *                       destination address
------------------------------------------------------------------- */
INT4
NetIpv4GetSrcAddrToUseForDest (UINT4 u4Dest, UINT4 *pu4SrcAddr)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4RetVal = NETIPV4_SUCCESS;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4Dest;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u2AppIds = ALL_ROUTING_PROTOCOL;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    i4RetVal = NetIpv4GetRoute (&RtQuery, &NetIpRtInfo);

    if (i4RetVal == NETIPV4_SUCCESS)
    {
        if (NetIpv4GetIfInfo (NetIpRtInfo.u4RtIfIndx, &NetIpIfInfo)
            == NETIPV4_FAILURE)
        {
            i4RetVal = NETIPV4_FAILURE;
        }
        else
        {
            *pu4SrcAddr = NetIpIfInfo.u4Addr;
        }
    }

    return (i4RetVal);
}

/*
*+---------------------------------------------------------------------+
*| Function Name : IpIsLocalAddrInCxt                                  |
*|                                                                     |
*| Description   : Checks if the given address belongs to any of the   |  
*|                 local net in the specified context                  |  
*|                                                                     |  
*| Input         : u4ContextId, u4Addr, pu2Port                        |
*|                                                                     |  
*| Output        : u2Port of the Interface to which the addr belongs   |  
*|                                                                     |  
*| Returns       : IP_SUCCESS / IP_FAILURE                             |  
*|                                                                     |  
*+---------------------------------------------------------------------+
*/
INT4
IpIsLocalAddrInCxt (UINT4 u4ContextId, UINT4 u4Addr, UINT2 *pu2Port)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationIpAddress = u4Addr;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4ContextId = u4ContextId;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if ((NetIpRtInfo.u2RtProto == CIDR_LOCAL_ID) &&
            (NetIpRtInfo.u4RtIfIndx <= IPIF_MAX_LOGICAL_IFACES))
        {
            *pu2Port = (UINT2) NetIpRtInfo.u4RtIfIndx;
            return IP_SUCCESS;
        }
    }
    return IP_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IpIsLocalNet
 * *                     Checks whether IP belongs to Local Network
 *
 * Input(s)           : u4IpAddress - Ip Address
 * Output(s)          : None.

 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4IpIsLocalNet (UINT4 u4IpAddress)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4Index = 0;
    struct ifreq        ifreq;
    INT4                i4SockFd;
    struct sockaddr_in *saddr;
    UINT4               u4IfAddr = 0;
    UINT4               u4IfMask = 0;
    INT4                i4RetVal = NETIPV4_FAILURE;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;

    if ((i4SockFd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("NetIpv4IpIsLocalNet - socket creation failed\r\n");
        return (NETIPV4_FAILURE);
    }

    pIfnameIdx = if_nameindex ();
    if (pIfnameIdx == NULL)
    {
        perror ("if_nameindex call failed:");
        close (i4SockFd);
        return (NETIPV4_FAILURE);
    }
    while (pIfnameIdx[u4Index].if_index != 0)
    {
        /* Skip Linux physical interfaces */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            u4Index++;
            continue;
        }

        MEMCPY (ifreq.ifr_name, pIfnameIdx[u4Index].if_name, IFNAMSIZ);
        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) == 0)
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            u4IfAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);

            if (ioctl (i4SockFd, SIOCGIFNETMASK, &ifreq) == 0)
            {
                saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_netmask;
                u4IfMask = OSIX_NTOHL (saddr->sin_addr.s_addr);

                if ((u4IfAddr & u4IfMask) == (u4IpAddress & u4IfMask))
                {
                    i4RetVal = NETIPV4_SUCCESS;
                    break;
                }
            }
        }

        u4Index++;
    }
    if_freenameindex (pIfnameIdx);
    close (i4SockFd);

    return (i4RetVal);
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IpIsLocalNetInCxt
 * *                    Checks whether IP belongs to Local Network
 *                      in a context
 *
 * Input(s)           : u4ContextId - Context Identifier
 *             u4IpAddress - IP Address
 * Output(s)          : None.

 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4IpIsLocalNetInCxt (UINT4 u4ContextId, UINT4 u4IpAddress)
{
    UNUSED_PARAM (u4ContextId);
    return (NetIpv4IpIsLocalNet (u4IpAddress));
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4IpIsLocalNetOnInterface 
 * *                    Checks whether IP belongs to Local Network 
 *             on the Interface. 
 *
 * Input(s)           : u4CfaIfIndex - CFA Interface Index 
 *             u4IpAddress - IP Address
 * Output(s)          : None.

 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4IpIsLocalNetOnInterface (UINT4 u4CfaIfIndex, UINT4 u4IpAddress)
{
    struct if_nameindex *pIfnameIdx = NULL;
    struct ifreq        ifreq;
    INT4                i4SockFd;
    struct sockaddr_in *saddr;
    UINT4               u4IfAddr = 0;
    UINT4               u4IfMask = 0;
    INT4                i4RetVal = NETIPV4_FAILURE;

    if ((i4SockFd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("NetIpv4IpIsLocalNet - socket creation failed\r\n");
        return (NETIPV4_FAILURE);
    }

    pIfnameIdx = if_nameindex ();
    if (pIfnameIdx == NULL)
    {
        perror ("if_nameindex call failed:");
        close (i4SockFd);
        return (NETIPV4_FAILURE);
    }
    if (pIfnameIdx[u4CfaIfIndex].if_index != 0)
    {
        MEMCPY (ifreq.ifr_name, pIfnameIdx[u4CfaIfIndex].if_name, IFNAMSIZ);
        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) == 0)
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            u4IfAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);

            if (ioctl (i4SockFd, SIOCGIFNETMASK, &ifreq) == 0)
            {
                saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_netmask;
                u4IfMask = OSIX_NTOHL (saddr->sin_addr.s_addr);

                if ((u4IfAddr & u4IfMask) == (u4IpAddress & u4IfMask))
                {
                    i4RetVal = NETIPV4_SUCCESS;
                }
            }
        }
    }

    if_freenameindex (pIfnameIdx);
    close (i4SockFd);

    return (i4RetVal);
}

/*-------------------------------------------------------------------+
 * Function           :  NetIpv4GetNextSecondaryAddress
 *                     
 *Description         : Get the next secondary ip address over the interface
                        if the given ip adddress is primary ip address,firsr
                        secondary ip address is returned from the function
 *
 * Input(s)           : u2Port      - Ip Interface Index 
 *                      u4IpAddress - Ip Address
 *
 * Output(s)          : *pu4NextIpAddr - Next Secondary address
 *                      *pu4NetMask    - Mask associated with the secondary
 *                                       address
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
NetIpv4GetNextSecondaryAddress (UINT2 u2Port, UINT4 u4IpAddr,
                                UINT4 *pu4NextIpAddr, UINT4 *pu4NetMask)
{
    UINT4               u4CfaIfIndex;

    if (NetIpv4GetCfaIfIndexFromPort (u2Port, &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return NETIPV4_FAILURE;
    }
    if (CfaIpIfGetNextSecondaryAddress (u4CfaIfIndex, u4IpAddr,
                                        pu4NextIpAddr,
                                        pu4NetMask) == IP_SUCCESS)
    {
        return NETIPV4_SUCCESS;
    }
    return NETIPV4_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           :  NetIpv4GetCxtId
 *                     
 *Description         : This functions gets the context id associated with the 
                        the given IP Interface Index
 *
 * Input(s)           : u4IfIndex     - IP Port Number 
 *
 * Output(s)          : *pu4CxtId - Context id of the interface
 *                                   having port number as u4IfIndex 
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : This fucntion calls VcmGetContextInfoFromIpIfIndex
 *                      to get the context id associated with u4IfIndex 
------------------------------------------------------------------- */
INT4
NetIpv4GetCxtId (UINT4 u4IfIndex, UINT4 *pu4CxtId)
{
#if defined (MI_WANTED) && defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT4               u4CfaIfIndex = 0;
    tCfaIfInfo          CfaIfInfo;
    if (NetIpv4GetCfaIfIndexFromPort (u4IfIndex,
                                      &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }
    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    if (VcmGetContextIdFromCfaIfIndex (u4CfaIfIndex, pu4CxtId) == VCM_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

#else
    *pu4CxtId = 0;
    UNUSED_PARAM (u4IfIndex);
#endif
    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 *
 * Function           :ip_src_addr_to_use_for_dest
 *
 * Input(s)           : u4Dest, u4pSrc_addr_to_use
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 *   Gets the IP address for the interface on which the given destination
 * could be reached.
 *
+-------------------------------------------------------------------*/
INT4
ip_src_addr_to_use_for_dest (UINT4 u4Dest, UINT4 *pu4pSrc_addr_to_use)
{
    return (ip_src_addr_to_use_for_dest_InCxt (IP_DEFAULT_CONTEXT, u4Dest,
                                               pu4pSrc_addr_to_use));
}

/*-------------------------------------------------------------------+
 *
 * Function           :ip_src_addr_to_use_for_dest_InCxt 
 *
 * Input(s)           : u4ContextId, u4Dest, u4pSrc_addr_to_use
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action :
 *   Gets the IP address for the interface on which the given destination
 * could be reached in the specified context
 *
+-------------------------------------------------------------------*/
INT4
ip_src_addr_to_use_for_dest_InCxt (UINT4 u4ContextId, UINT4 u4Dest,
                                   UINT4 *pu4pSrc_addr_to_use)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Dest);
    UNUSED_PARAM (pu4pSrc_addr_to_use);
    return IP_SUCCESS;

}

/*-------------------------------------------------------------------+
 * Function           :  IpHandlePathStatusChange
 *
 *Description         : This functions gets the IpNbrInfo from BFD
 *                      inorder to clear the route entry from the table
 *                      and clears the arp cache.
 *
 * Input(s)           : u4ContextId - Context id of the interface
 *                      pIpNbrInfo  - Next hop details to clear
 *                                    route and arp cache.
 * Output(s)          : NONE
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */
INT1
IpHandlePathStatusChange (UINT4 u4ContextId, tClientNbrIpPathInfo * pIpNbrInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pIpNbrInfo);
    return NETIPV4_SUCCESS;
}

#ifdef VRRP_WANTED
/*-------------------------------------------------------------------+
 * Function           : NetIpv4CreateVrrpInterface
 *                     
 * Description        : Enables VRRP capability for the IP interface or
 *                      adds address as virtual address to the IP
 *                      interface.
 *
 * Input(s)           : pVrrpNwIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : None
 *
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv4CreateVrrpInterface (tVrrpNwIntf * pVrrpNwIntf)
{
    if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_CREATE)
    {
        if (LnxIpVrrpVifConfigInLnx (pVrrpNwIntf) == NETIPV4_FAILURE)
        {
            return (NETIPV4_FAILURE);
        }

#ifdef NPAPI_WANTED
        if (IpFsNpVrrpHwProgram (VRRP_NP_CREATE_INTERFACE,
                                 pVrrpNwIntf) == FNP_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
#endif
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_SECONDARY_CREATE)
    {
        LnxIpVrrpAddSecondaryIp (pVrrpNwIntf);
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_ACCEPT_ADD)
    {
        LnxIpVrrpDelDropFilter (pVrrpNwIntf->IpvXAddr);
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4DeleteVrrpInterface
 *                     
 * Description        : Disables VRRP capability for the IP interface or
 *                      deletes virtual address from the IP interface.
 *
 * Input(s)           : pVrrpNwIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : None
 *
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv4DeleteVrrpInterface (tVrrpNwIntf * pVrrpNwIntf)
{
    if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_DELETE)
    {
        if (LnxIpVrrpVifDeleteInLnx (pVrrpNwIntf) == NETIPV4_FAILURE)
        {
            return (NETIPV4_FAILURE);
        }

#ifdef NPAPI_WANTED
        if (IpFsNpVrrpHwProgram (VRRP_NP_DELETE_INTERFACE,
                                 pVrrpNwIntf) == FNP_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
#endif
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_SECONDARY_DELETE)
    {
        LnxIpVrrpDelSecondaryIp (pVrrpNwIntf);
    }
    else if (pVrrpNwIntf->u1Action == VRRP_NW_INTF_ACCEPT_DEL)
    {
        LnxIpVrrpAddDropFilter (pVrrpNwIntf->IpvXAddr);
    }

    return NETIPV4_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetVrrpInterface
 *            
 * Description        : Retrieves VRRP information corresponding to
 *                      IP interface from NP.
 *
 *                      This NetIp API also handles the below actions
 *                            1. To Change Network action.
 *                            2. To Check Primary IP against Associated
 *                               IP.
 *                      The above actions are only applicable for
 *                      Linux IP.
 *
 * Input(s)           : pVrrpNwInIntf    - Pointer to VRRP Interface
 *                                       Information.
 *
 * Output(s)          : pVrrpNwOutIntf   - Pointer to VRRP Interface
 *                                       Information.
 *
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
------------------------------------------------------------------- */
INT4
NetIpv4GetVrrpInterface (tVrrpNwIntf * pVrrpNwInIntf,
                         tVrrpNwIntf * pVrrpNwOutIntf)
{
    if (pVrrpNwInIntf->b1IsChgNwActionReqd == TRUE)
    {
        if (pVrrpNwInIntf->u1Action == VRRP_NW_INTF_MCAST_CREATE)
        {
            if (pVrrpNwInIntf->b1IsAcceptConf == TRUE)
            {
                pVrrpNwInIntf->u1Action = VRRP_NW_INTF_ACCEPT_ADD;
            }
            else
            {
                pVrrpNwInIntf->u1Action = VRRP_NW_INTF_SECONDARY_CREATE;
            }
        }
        else if (pVrrpNwInIntf->u1Action == VRRP_NW_INTF_MCAST_DELETE)
        {
            if (pVrrpNwInIntf->b1IsAcceptConf == TRUE)
            {
                pVrrpNwInIntf->u1Action = VRRP_NW_INTF_ACCEPT_DEL;
            }
            else
            {
                pVrrpNwInIntf->u1Action = VRRP_NW_INTF_SECONDARY_DELETE;
            }
        }
        else if (pVrrpNwInIntf->u1Action == VRRP_NW_INTF_SECONDARY_CREATE)
        {
            if (pVrrpNwInIntf->b1IsAcceptConf == TRUE)
            {
                pVrrpNwInIntf->u1Action = VRRP_NW_INTF_ACCEPT_ADD;
            }
        }
        else if (pVrrpNwInIntf->u1Action == VRRP_NW_INTF_SECONDARY_DELETE)
        {
            if (pVrrpNwInIntf->b1IsAcceptConf == TRUE)
            {
                pVrrpNwInIntf->u1Action = VRRP_NW_INTF_ACCEPT_DEL;
            }
        }

        MEMCPY (pVrrpNwOutIntf, pVrrpNwInIntf, sizeof (tVrrpNwIntf));

        return NETIPV4_SUCCESS;
    }
    else if (pVrrpNwInIntf->b1IsCheckIpReqd == TRUE)
    {
        if (((pVrrpNwInIntf->u1Action == VRRP_NW_INTF_SECONDARY_CREATE) ||
             (pVrrpNwInIntf->u1Action == VRRP_NW_INTF_SECONDARY_DELETE)) &&
            (IPVX_ADDR_COMPARE (pVrrpNwInIntf->CheckIp,
                                pVrrpNwInIntf->IpvXAddr) == 0))
        {
            return NETIPV4_FAILURE;
        }

        MEMCPY (pVrrpNwOutIntf, pVrrpNwInIntf, sizeof (tVrrpNwIntf));

        return NETIPV4_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (IpFsNpVrrpHwProgram (VRRP_NP_GET_INTERFACE,
                             pVrrpNwInIntf) == FNP_FAILURE)
    {
        return NETIPV4_FAILURE;
    }

    MEMCPY (pVrrpNwOutIntf, pVrrpNwInIntf, sizeof (tVrrpNwIntf));
#endif

    return NETIPV4_SUCCESS;
}
#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfIndexFromAddrUsingCxt
 *
 * Description      :   This function returns the IfIndex for a given Ip
 *                      Address.
 *
 * Inputs           :   u4IpAddr
 *                      u4ContextId
 *
 * Outputs          :   pu4IfIndex - IfIndex for the given Ip Address.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIfIndexFromAddrUsingCxt (UINT4 u4IpAddr, UINT4 *pu4IfIndex,
                                   UINT4 u4ContextId)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4Index = 0;
    struct ifreq        ifreq;
    INT4                i4SockFd;
    struct sockaddr_in *saddr;
    INT4                i4RetVal = NETIPV4_FAILURE;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;

    *pu4IfIndex = 0;
    i4SockFd = LnxVrfGetSocketFd (u4ContextId, AF_INET, SOCK_DGRAM,
                                  0, LNX_VRF_OPEN_SOCKET);
    if (i4SockFd < 0)
    {
        perror ("NetIpv4GetIfIndexFromAddr - socket creation failed\r\n");
        return (NETIPV4_FAILURE);
    }

    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_NON_DEFAULT_NS,
                             (CHR1 *) pLnxVrfInfo->au1NameSpace);
        }

    }

    pIfnameIdx = if_nameindex ();
    while ((pIfnameIdx != NULL) && (pIfnameIdx[u4Index].if_index != 0))
    {
        /* Skip Linux physical interfaces */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            u4Index++;
            continue;
        }

        MEMCPY (ifreq.ifr_name, pIfnameIdx[u4Index].if_name, IFNAMSIZ);
        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) == 0)
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            if (OSIX_NTOHL (saddr->sin_addr.s_addr) == u4IpAddr)
            {
                *pu4IfIndex = pIfnameIdx[u4Index].if_index;
                i4RetVal = NETIPV4_SUCCESS;
                break;
            }
        }

        u4Index++;
    }
    if (pIfnameIdx != NULL)
    {
        if_freenameindex (pIfnameIdx);
    }
    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                             (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
    }
    close (i4SockFd);

    return (i4RetVal);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetFirstIfInfoUsingCxt
 *
 * Description      :   This function provides the First Entry in the If Config
 *                      Record. 
 *
 * Inputs           :   u4ContextId
 *
 * Outputs          :   pNetIpIfInfo - First If Config Record.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 * 
 *******************************************************************************
 */
INT4
NetIpv4GetFirstIfInfoUsingCxt (tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4ContextId)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4FirstIfIndex = CFA_INVALID_INDEX;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
    UINT4               u4Index = 0;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;

    if (u4ContextId > 0)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return NETIPV4_FAILURE;
        }
        if (LnxVrfChangeCxt
            (LNX_VRF_NON_DEFAULT_NS,
             (CHR1 *) pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }

    pIfnameIdx = if_nameindex ();
    if (pIfnameIdx == NULL)
    {
        perror ("NetIpv4GetFirstIfInfoUsingCxt - if_nameindex Failed \r\n");
        return (NETIPV4_FAILURE);
    }

    while (pIfnameIdx[u4Index].if_index != 0)
    {
        /* Skip Linux physical interfaces */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_SUCCESS)
        {
            u4FirstIfIndex = pIfnameIdx[u4Index].if_index;
            break;
        }
        u4Index++;
    }
    if_freenameindex (pIfnameIdx);

    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                             (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
    }
    if (u4FirstIfIndex == CFA_INVALID_INDEX)
    {
        return (NETIPV4_FAILURE);
    }
    return (NetIpv4GetIfInfo (u4FirstIfIndex, pNetIpIfInfo));
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetNextIfInfoUsingCxt
 *
 * Description      :   This function returns the Next If Config Record for a
 *                      given IfIndex.
 *
 * Inputs           :   u4IfIndex
 *                      u4ContextId
 *
 * Outputs          :   pNextNetIpIfInfo - If-Info of the Next Interface Index.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetNextIfInfoUsingCxt (UINT4 u4IfIndex,
                              tNetIpv4IfInfo * pNextNetIpIfInfo,
                              UINT4 u4ContextId)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4NextIfIndex = CFA_INVALID_INDEX;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
    UINT4               u4Index = 0;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;

    if (u4ContextId > 0)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return NETIPV4_FAILURE;
        }
        if (LnxVrfChangeCxt
            (LNX_VRF_NON_DEFAULT_NS,
             (CHR1 *) pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }

    pIfnameIdx = if_nameindex ();
    while (pIfnameIdx[u4Index].if_index != 0)
    {
        if (pIfnameIdx[u4Index].if_index > u4IfIndex)
        {
            /* If the index is created through ISS,return that. */
            if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                              &u4CfaIfIndex) == NETIPV4_SUCCESS)
            {
                u4NextIfIndex = pIfnameIdx[u4Index].if_index;
                break;
            }
        }
        u4Index++;
    }

    if_freenameindex (pIfnameIdx);

    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                             (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
    }
    if (u4NextIfIndex == CFA_INVALID_INDEX)
    {
        return (NETIPV4_FAILURE);
    }
    return (NetIpv4GetIfInfo (u4NextIfIndex, pNextNetIpIfInfo));
}

/*
 *************************************************************************
 * Function Name    :   NetIpv4GetHighestIpAddrUsingCxt
 *
 * Description      :   This function returns the highest IP Address among the
 *                      active V4 Interface address.
 *
 * Inputs           :   u4ContextId
 *
 * Outputs          :   pu4IfCount - Number of interfaces.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 ******************************************************************************* */
INT4
NetIpv4GetHighestIpAddrUsingCxt (UINT4 *pu4IpAddress, UINT4 u4ContextId)
{
    struct if_nameindex *pIfnameIdx = NULL;
    UINT4               u4Index = 0;
    struct ifreq        ifreq;
    INT4                i4SockFd;
    struct sockaddr_in *saddr;
    UINT4               u4IfAddr = 0;
    UINT4               u4HighIfAddr = 0;
    UINT4               u4CfaIfIndex = CFA_INVALID_INDEX;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;

    if (u4ContextId > 0)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return NETIPV4_FAILURE;
        }
        if (LnxVrfChangeCxt
            (LNX_VRF_NON_DEFAULT_NS,
             (CHR1 *) pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }

    if ((i4SockFd = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror ("NetIpv4GetHighestIpAddr - socket creation failed\r\n");
        return (NETIPV4_FAILURE);
    }

    pIfnameIdx = if_nameindex ();
    while (pIfnameIdx[u4Index].if_index != 0)
    {
        /* Skip Linux physical interfaces */
        if (NetIpv4GetCfaIfIndexFromPort (pIfnameIdx[u4Index].if_index,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            u4Index++;
            continue;
        }

        MEMCPY (ifreq.ifr_name, pIfnameIdx[u4Index].if_name, IFNAMSIZ);
        if (ioctl (i4SockFd, SIOCGIFADDR, &ifreq) == 0)
        {
            saddr = (struct sockaddr_in *) (VOID *) &ifreq.ifr_addr;
            u4IfAddr = OSIX_NTOHL (saddr->sin_addr.s_addr);
            u4HighIfAddr = (u4HighIfAddr > u4IfAddr) ? u4HighIfAddr : u4IfAddr;
        }

        u4Index++;
    }
    if_freenameindex (pIfnameIdx);
    close (i4SockFd);

    *pu4IpAddress = u4HighIfAddr;
    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                             (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
    }
    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4DeRegisterHigherLayerProtocolUsingCxt
 *
 * Description      :   This function De-Registers the Protocol specified by
 *                      the Application.
 *
 * Inputs           :   u1ProtoId - Protocol Identifier.
 *                      u4ContextId - ContextId
 *
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4DeRegisterHigherLayerProtocolUsingCxt (UINT1 u1ProtoId,
                                              UINT4 u4ContextId)
{
    INT4                i4Count = 0;
    UINT1               u1Registerflag = 0;

    if (u1ProtoId >= IP_MAX_PROTOCOLS)
    {
        return (NETIPV4_FAILURE);
    }
    if (gHLRegnTbl[u1ProtoId].au1CtxReg[u4ContextId] == DEREGISTER)
    {
        return (NETIPV4_SUCCESS);
    }
    else
    {
        gHLRegnTbl[u1ProtoId].au1CtxReg[u4ContextId] = DEREGISTER;
        for (i4Count = 0; i4Count < SYS_DEF_MAX_NUM_CONTEXTS; i4Count++)
        {
            if (gHLRegnTbl[u1ProtoId].au1CtxReg[i4Count] == REGISTER)
            {
                u1Registerflag = REGISTER;
                break;
            }
        }
        if (u1Registerflag == DEREGISTER)
        {
            gHLRegnTbl[u1ProtoId].pIfStChng = NULL;
            gHLRegnTbl[u1ProtoId].pRtChng = NULL;
            gHLRegnTbl[u1ProtoId].pProtoPktRecv = NULL;
        }
        return (NETIPV4_SUCCESS);
    }
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIpForwardingUsingCxt
 *
 * Description      :   This function returns the IP forwarding flag
 *
 * Inputs           :   u4ContextId
 *
 * Outputs          :   pu1IpForward -  Global Ip Forwarding flag
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIpForwardingUsingCxt (UINT1 *pu1IpForward, UINT4 u4ContextId)
{
    UINT4               u4IpForward;
    tLnxVrfInfo        *pLnxVrfInfo = NULL;

    if (u4ContextId > 0)
    {
        pLnxVrfInfo = LnxVrfInfoGet (u4ContextId);
        if (pLnxVrfInfo == NULL)
        {
            return NETIPV4_FAILURE;
        }
        if (LnxVrfChangeCxt
            (LNX_VRF_NON_DEFAULT_NS,
             (CHR1 *) pLnxVrfInfo->au1NameSpace) == NETIPV4_FAILURE)
        {
            return NETIPV4_FAILURE;
        }
    }

    if (LnxIpGetForwarding (&u4IpForward) == SNMP_FAILURE)
    {
        return (NETIPV4_FAILURE);
    }

    *pu1IpForward = (UINT1) u4IpForward;
    if (u4ContextId != VCM_DEFAULT_CONTEXT)
    {
        if (pLnxVrfInfo != NULL)
        {
            LnxVrfChangeCxt (LNX_VRF_DEFAULT_NS,
                             (CHR1 *) LNX_VRF_DEFAULT_NS_PID);
        }
    }
    return (NETIPV4_SUCCESS);
}
#endif /* VRF_WANTED && LINUX_310_WANTED */

UINT4
NetIpv4GetIndexFromIndexManager (UINT1 u1GroupId)
{
    UINT4               u4Index = 0;
    u4Index = NetipIndexMgrGetAvailableIndex (u1GroupId);
    return u4Index;
}

INT4
NetIpv4ReleaseIndexToIndexManger (UINT1 u1GroupId, UINT4 u4Index)
{
    INT4                i4RetVal = LNXIP_FAILURE;
    i4RetVal = NetipIndexMgrRelIndex (u1GroupId, u4Index);
    return i4RetVal;
}

UINT1
NetIpv4IsIndexAvailable (UINT1 u1GrpID)
{
    if (LNXIP_ONE == NetipIndexMgrCheckGrpFull (u1GrpID))
    {
        return NETIPV4_FAILURE;
    }
    return NETIPV4_SUCCESS;
}
