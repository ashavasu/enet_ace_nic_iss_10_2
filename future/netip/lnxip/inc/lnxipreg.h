/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxipreg.h,v 1.6 2015/10/26 13:43:33 siva Exp $
 *
 * Description: Application Registered parameters
 *
 *******************************************************************/
#ifndef __LNXIPREG_H__
#define __LNXIPREG_H__

#include "ip.h"

#define   DEREGISTER            0
#define   REGISTER              1
#define   REGDOWN               0
#define   REGUP                 1

#define   FUNCPTR    UINT4

typedef struct _HLProtoRegnTbl
{
    VOID (*pIfStChng)     (tNetIpv4IfInfo *pNetIpIfInfo, UINT4 u4BitMap);
    VOID (*pRtChng)       (tNetIpv4RtInfo *pNetIpRtInfo, tNetIpv4RtInfo *pNetIpRtInfo1, UINT1 u1CmdType);
    VOID (*pProtoPktRecv) (tIP_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len,
                           UINT4 u4IfIndex, tIP_INTERFACE InterfaceId,
                           UINT1 u1Flag);
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    UINT1 au1CtxReg[SYS_DEF_MAX_NUM_CONTEXTS];
#endif
    UINT1 u1RegFlag;
    UINT1 u1AlignmentByte;    /* 3 bytes to ensure a 4-byte boundary */
    UINT2 u2AlignmentByte;
} tHLProtoRegnTbl;


/* Function Declarations */
VOID IpInitRegTable PROTO ((VOID));
INT1 IpRegisterLowerLayerProtocol PROTO ((UINT1 u1InterfaceType,
                                          INT4 (*pWrite) (VOID),
                                          INT4 (*pGetHwAddr) (VOID),
                                          UINT4 u4NumInterfaces));
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
VOID IpActOnProtoRegDereg PROTO ((UINT1 u1ProtocolId, UINT4 u4ContextId));
#else
VOID IpActOnProtoRegDereg PROTO ((UINT1 u1ProtocolId));
#endif

VOID IpIfStateChngNotify (tNetIpv4IfInfo *pIfInfo, UINT4 u4BitMap);

#endif /* __LNXIPREG_H__ */
