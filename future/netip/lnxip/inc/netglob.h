/****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: netglob.h,v 1.2 2010/08/16 13:55:58 prabuc Exp $
 *
 * Description: This file contains the Constants, Macro's and Prototypes 
 *              related to ISS NetDevice Module
 *
 ***************************************************************************/
#ifndef _NET_GLOB_H_
#define _NET_GLOB_H_

/* ----------------------- Global Declarations --------------------- */

#ifdef _NET_DEV_C_
UINT4               gu4NetDevTrace;
#else
extern UINT4        gu4NetDevTrace;
#endif /* _NET_DEV_C_ */

/* ----------------- Constant & Macro Definitions ------------------ */

/* Debug Trace related Constants */

#define NETDEV_TRC_FLAG  gu4NetDevTrace

#define NETDEV_MOD_NAME  ((const char *)"NETDEV")

/* Basic Trace Types are inherited from inc/trace.h
 * Additional Types required for ISS Netdevice Module have been defined here */

#define	NETDEV_DEBUG_TRC	     0x00001000	/* Function Entry/Exit Traces */
#define	NETDEV_INFO_TRC 	     0x00002000	/* Information Traces */

#define NETDEV_TRC(TraceType, Str)    \
        MOD_TRC(NETDEV_TRC_FLAG, TraceType, NETDEV_MOD_NAME, (const char *)Str)
    
#define NETDEV_TRC_ARG1(TraceType, Str, Arg1)               \
MOD_TRC_ARG1(NETDEV_TRC_FLAG, TraceType, NETDEV_MOD_NAME, (const char *)Str, Arg1)

#define NETDEV_TRC_ARG2(TraceType, Str, Arg1, Arg2)               \
MOD_TRC_ARG2(NETDEV_TRC_FLAG, TraceType, NETDEV_MOD_NAME, (const char *)Str, Arg1, Arg2)
    
/* ----------------------- Function Prototypes --------------------- */

/* ISS Netdevice Module Management routines */

INT4 NetDevLoadModule   PROTO ((VOID));
VOID NetDevUnloadModule PROTO ((VOID));

/* Network Device Management routines */

INT4 NetDevInterfaceInit    PROTO ((tNetDevice *pDev));
INT4 NetDeviceCreate  PROTO ((tLnxIpIntfParams *pLnxIpIntfParams));
INT4 NetDeviceDelete  PROTO ((UINT1 *pu1DevName));
INT4 NetDeviceOpen    PROTO ((tNetDevice *pDev));
INT4 NetDeviceRelease PROTO ((tNetDevice *pDev));
INT4 NetDevInterfaceTxmit   PROTO ((tSkb *pSkb, tNetDevice *pDev));
INT4 NetDeviceIoctl   PROTO ((FS_ULONG u4IoctlParam));
tNetDevStats * NetDeviceGetStats PROTO ((tNetDevice *pDev));

#endif /* _NET_GLOB_H_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  netglob.h                      */
/*-----------------------------------------------------------------------*/
