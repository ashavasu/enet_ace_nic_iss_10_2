/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxipmod.h,v 1.15 2015/10/05 10:45:30 siva Exp $
 *
 * Description:This file contains porting related to linux
 *             defintions used in RTM module.               
 *
 *******************************************************************/
#ifndef _LNXIPMOD_H
#define _LNXIPMOD_H

#include "fssocket.h"

#define   RTM_SUB_TASK_NAME    "SRTM"
#define   RTM_SUB_TASK_PRIORITY 35

#define   IF_NAMESIZE  16

#define LNXIP_ONE 1
#define LNXIP_ZERO 0
#define LNXIP_FAILURE 0
#define LNXIP_SUCCESS 1

/* Definitions for quering the interface information */
#define IFI_ALIAS   1           /* ifi_addr is an alias */
#define IFI_NAME    16          /* same as IFNAMSIZ in <net/if.h> */
#define IFI_HADDR   8
#define LNXIP_INET_AFI_IPV4  1

#define RTM_CHECK_ACK 2

/* Macro declaration. */
#define   IP_MALLOC(u2Size, UINT1)   MEM_MALLOC(u2Size, UINT1)
#define   IP_FREE(pu1Base)           MEM_FREE(pu1Base)
#define   IP_DUPLICATE_BUF(pBuf)     CRU_BUF_Duplicate_BufChain((pBuf))
#define IP_CREATE_MEM_POOL(u4BlkSize, u4NumBlks, pPoolId) \
            MemCreateMemPool((u4BlkSize),              \
                             (u4NumBlks),              \
                             MEM_DEFAULT_MEMORY_TYPE,  \
                             (pPoolId))
                   
#define REQ_BUFFER_SIZE      1024
/* Socket interface to kernel */
typedef struct _netlinksock
{
    INT4                i4Sock;
    struct sockaddr_nl  snl;
}tNetLinkSock;

typedef struct _request
{
        struct nlmsghdr     NLMsgHdr;
        struct rtmsg        RtmMsg;
        UINT1               u1Buf[REQ_BUFFER_SIZE];
}tRequest;

/* Variable Declaration. */
#ifndef _RTMLNIP_C
extern tNetLinkSock  RtmNLListen;
extern tNetLinkSock  RtmNLWrite;
extern tNetLinkSock  RtmNLRead;
#endif
#ifndef _LNXIP_C
extern tNetLinkSock  gLnxVrfNLWrite[SYS_DEF_MAX_NUM_CONTEXTS];
#endif



/* Extern Function declartions. */
extern void RtmNLParseRouteAttr (struct rtattr **, INT4 , struct rtattr *, INT4);
extern INT4 RtmAddAttribute (struct nlmsghdr *n, INT4 maxlen, INT4 type,
                           void *data, INT4 alen);

/* Function Declarations. */
INT1    RtmLnxIpTskInit (VOID);
VOID    RtmSubTaskInit (VOID);
INT4    RtmNetlinkSockInit (tNetLinkSock *, UINT4 u4Groups);
INT4    NetlinkSocket (tNetLinkSock *nl, UINT4 groups, UINT2 u2Afi);
INT4    RtmNetlinkParseInfo (UINT1 u1InfoFlag , tNetLinkSock *nl, 
                             UINT2 u2Afi);
INT4    RtmNetIpv4LeakRoute (UINT1 u1NetIpv4RtCmd , tRtInfo *pRtInfo);
VOID    RtmSubTaskMain (INT1 *pi1TaskParam);
INT4    RtmNetlinkGetRoute (tNetLinkSock *nl);
INT4    NetLinkRouteModify (INT4 i4Cmd, tRtInfo *pRtInfo);
INT4    RtmSendToNetLink (struct nlmsghdr *n, tNetLinkSock *nl, UINT2 u2Afi);
INT4    RtmNetlinkAddRtAttribute (struct rtattr *pRtAttr, INT4 i4MaxLen,
                                  INT4 i4Type, VOID *pData, INT4 i4Len);
INT4    netlink_talk_filter (struct sockaddr_nl *snl, 
                    struct nlmsghdr *h, UINT2 u2Afi, VOID *pArg);
VOID    RtmDeRegisterAllAppIds(VOID);
INT4    RtmIpMaskToPrefixLen (UINT4 u4Mask);
VOID    RtmHandleIfStateChng (UINT1 u1Status, tIpBuf * pBuf);
VOID    RtmHandleIfCreationDeletion (UINT1 u1Status, tIpBuf * pBuf);
#endif /* __LNX_IP_H */
