/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: netipmc.h,v 1.5 2013/01/07 12:18:42 siva Exp $
 *
 * Description:This file holds the headerfiles and macros 
 *             for netipmc.c
 *
 *******************************************************************/
#ifndef _NETIPMC_H
#define _NETIPMC_H
/*-------- header files included----------*/
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "cust.h"
#include "cli.h"
#include "lnxip.h"
#include "lnxipint.h"
#include "pim.h"
#include "dvmrp.h"
#include "igmp.h"

/* --------------- constants------------------*/ 
#define NETIPMC_DVMRP_ID         0x00000001
#define NETIPMC_PIM_ID           0x00000002
#define NETIPMC_IGMP_ID          0x00000004
#define NETIPMC_MAX_THRESHOLD    255

#define NETIPMC_MAX_PROTOCOLS    3 /* IGMP/PIM/DVMRP */
#define NETIP_IGMP_ANCILLARY_LEN 24
#define NETIPMC_CPUPORT_VIFID    (MAXVIFS - 1)

#define NETIPMC_MUTEX_SEMNAME   ((UINT1 *) "LNXM")

/*----------------type definitions------------*/
typedef struct _NetIpMcGlobalInfo
{
    tNetIpMcRegInfo aMcProtoRegInfo[NETIPMC_MAX_PROTOCOLS];
    tOsixSemId      NetIpSemId;
    INT4            i4NetIpSockId;   
}tNetIpMcGlobalInfo; 
/*----------------global declaration-----------*/
tNetIpMcGlobalInfo gNetIpMcGlobalInfo;

/*---------------macros-----------------------*/
#define NETIPMC_MUTEX_LOCK() OsixSemTake(gNetIpMcGlobalInfo.NetIpSemId);
#define NETIPMC_MUTEX_UNLOCK() OsixSemGive(gNetIpMcGlobalInfo.NetIpSemId);
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  netipmc.h                      */
/*-----------------------------------------------------------------------*/
