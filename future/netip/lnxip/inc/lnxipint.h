/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxipint.h,v 1.25 2015/09/18 10:41:35 siva Exp $
 *
 * Description:This file contains definitions and function
 *             prototypes related to Linux IP interface.    
 *
 *******************************************************************/
#ifndef _LNXIPINT_H
#define _LNXIPINT_H

#include <features.h>    /* for the glibc version number */

#if __GLIBC__ >= 2 && __GLIBC_MINOR__ >= 9
#include "fssocket.h"
#endif


/* included to decl the struct if_nameIndex and 
   APIs: if_nameindex() and if_freenameindex() */

#include <cfa.h>
#include <chrdev.h>
#include "arp.h"
#define LNX_IPADDR_UPDATE      0x00000001
#define LNX_MACADDR_UPDATE     0x00000002
#define LNX_SUBNET_UPDATE      0x00000010
#define LNX_BCAST_UPDATE       0x00000100
#define LNX_MTU_UPDATE         0x00001000
#define LNX_SPEED_UPDATE       0x00002000
#define LNX_IPADDR_ADD         1
#define LNX_IPADDR_DEL         2
#define LNX_MAX_PREFIX_LEN     32
typedef struct _LnxIpPortParams{
    UINT4 u4IpAddr;
    UINT4 u4SubNetMask;
    UINT4 u4BcastAddr;
    UINT4 u4Mtu;
    UINT4 u4IfSpeed;
    UINT2 u2UpdateMask;
    UINT1 au1MacAddress[MAC_ADDR_LEN];
}tLnxIpPortParams;

#define LNXIP_PORT_MAP_NODE_BUFF  1

/* Constants defined for dummy route addition */
#define LNXIP_DEF_ROUTE_TAG 0
#define LNXIP_DEF_METRIC_TYPE 0

#define LNXIP_MAX_PKTS_PROCESSED_FROM_LNX 10
#define LNXIP_TASK_ID  gLnxIpTskId

#ifdef LINUX_310_WANTED
#ifndef MSG_TRUNC
#define MSG_TRUNC      0x20
#endif                          /* MSG_TRUNC */

/* local definitions */
#define ETHERNET_HW_LEN         6
#define IPPROTO_ADDR_LEN        4
#undef FREE

/* types definitions */
#define IF_NAMESIZ    20        /* Max interface lenght size */
#define IF_HWADDR_MAX 20        /* Max MAC address length size */

/* utility macro */
#define ELEMENT_NEXT(E)         ((E) = (E)->next)
#define ELEMENT_DATA(E)         ((E)->data)
#define LIST_HEAD(L)            ((L)->head)
#define LIST_TAIL_DATA(L)       ((L)->tail->data)
#define LIST_ISEMPTY(L)         ((L) == NULL || ((L)->head == NULL && (L)->tail == NULL))
#define LIST_SIZE(V)            ((V)->count)
/* Define types */
#define NETLINK_TIMER (30 * TIMER_HZ)
#define NLMSG_TAIL(nmsg) ((struct rtattr *) (((void *) (nmsg)) + NLMSG_ALIGN((nmsg)->nlmsg_len)))

struct nlhandle {
        INT4 fd;
        struct sockaddr_nl snl;
        UINT4 seq;
};

extern struct nlhandle Nlcmd;           /* Command channel */

/* prototypes */
typedef struct _NtPktHdr
{
    struct nlmsghdr n;
    struct ifinfomsg ifi;
    INT1 buf[256];
} tNtPktHdr;

typedef struct NlAddrHdr
{
    struct nlmsghdr  nlh;
    struct ifaddrmsg addrmsg;
    char   buffer[256];
} tNlAddrHdr;


INT4 LnxIpNetLinkSetVmacAddr (tVrrpNwIntf *pVrrpNwIntf);
INT4 LnxIpNetLinkSetMode (tVrrpNwIntf *pVrrpNwIntf);
INT4 LnxIpNetLinkUp (tVrrpNwIntf *pVrrpNwIntf);
INT4 LnxIpNetLinkCreateVif (UINT1 *pu1DevName,tVrrpNwIntf *pVrrpNwIntf);
INT4 LnxIpNetLinkDelVmac (tVrrpNwIntf *pVrrpNwIntf);

VOID LnxIpKernelNetLinkClose (VOID);
INT4 LnxIpCreateNetSocket (UINT4 u4Groups, struct nlhandle *nl);

INT4 LnxIpNetClose (struct nlhandle *nl);
INT4 LnxIpNetSetBlock (struct nlhandle *nl, INT4 *flags);
INT4 LnxIpNetSetNonBlock (struct nlhandle *nl, INT4 *flags);

INT4 LnxIpAddAttr32 (struct nlmsghdr *n, INT4 maxlen, INT4 type, UINT4 data);

INT4 LnxIpNetLinkAddAddAttr (struct nlmsghdr *, INT4 , INT4 , VOID *, INT4 );
INT4 LnxIpRtaAddattr (struct rtattr *rta, INT4 maxlen, INT4 type,
                      const VOID *data, INT4 alen);
VOID LnxIpParseRtattr (struct rtattr **tb, INT4 max, struct rtattr *rta, INT4 len);
INT4 LnxIpNetParseInfo (INT4 (*filter) (struct sockaddr_nl *, struct nlmsghdr *),
                        struct nlhandle *nl, struct nlmsghdr *n);
INT4 LnxIpNetTalkFilter (struct sockaddr_nl *snl, struct nlmsghdr *h);
INT4 LnxIpNetTalkToKernel (struct nlhandle *nl, struct nlmsghdr *n);
INT4 LnxIpNetRequest (struct nlhandle *nl, INT4 family, INT4 type);
INT4 LnxIpNetIfLinkFilter (struct sockaddr_nl *snl, struct nlmsghdr *h);
INT4 LnxIpNetIfAddFilter (struct sockaddr_nl *snl, struct nlmsghdr *h);
INT4 LnxIpNetIfLookup (VOID);
INT4 LnxIpNetAddLookup (VOID);
INT4 LnxIpAddDelAddr(UINT4 u4IpAddr, UINT4 u4IfIndex, UINT4 u4Flag);
#ifdef VRRP_WANTED
VOID LnxIpVrrpKernelNetLinkInit PROTO ((VOID));
#endif

#endif

#define LNX_GRATUITOUS_ARP_ENABLE 1
#define LNX_GRATUITOUS_ARP_DISABLE 0

PUBLIC INT4
LinuxIpCreateNetworkInterface PROTO ((UINT1* pu1DevName,
                                      tLnxIpPortParams LnxIpPortParams));

PUBLIC INT4 
LinuxIpDeleteNetworkInterface PROTO ((UINT1* pu1DevName));

PUBLIC INT4
LinuxIpCreateLnxVlanInterface PROTO ((UINT1 *pu1DevName, 
                tLnxIpPortParams LnxIpPortParams ));

PUBLIC INT4
LinuxIpDeleteLnxVlanInterface PROTO ((UINT1 *pu1DevName));




PUBLIC INT4 
LinuxIpUpdateInterfaceParams PROTO ((UINT1* pu1DevName, tLnxIpPortParams
                              LnxIpPortParams));
PUBLIC INT4
LinuxIpGetInterfaceIndex PROTO ((UINT1* pu1DevName,INT4*  pi4IfIndex ));

PUBLIC INT4
LinuxIpUpdateInterfaceMacAddress PROTO ((UINT1* pu1DevName,
                                         UINT1* pu1MacAddress));

PUBLIC INT4 LnxIpInitPortIfIdxMapTbl PROTO ((VOID));
PUBLIC VOID LnxIpDeInitPortIfIdxMapTbl PROTO ((VOID));
PUBLIC VOID LnxIpDeInit PROTO ((VOID));
PUBLIC INT4 LnxIpIfInit PROTO ((VOID));
VOID  
LnxIpHashNodeDeleteFn PROTO ((tTMO_HASH_NODE *pNode));
UINT4 
LnxIpGetPortMapHashIndex PROTO ((UINT4 u4IpPortNum));
INT4 LnxIpAddPortToMappingTable (UINT4 u4CfaIfIndex, UINT4 u4IpPortNum, UINT1 u1IfType);
INT4
LnxIpDeletePortFromMappingTable PROTO ((UINT4 u4IpPortNum));
UINT1              *
LnxIpGetBuff PROTO ((UINT1 u1BufType, UINT4 u4Size));
VOID LnxIpReleaseBuff PROTO ((UINT1 u1BufType, UINT1 *pu1Buf));
INT4 LinuxIpCreateLoopbackInterface (UINT1 *pu1DevName, UINT4 u4ContextId);
INT4 LinuxIpDeleteLoopbackInterface (UINT1 *pu1DevName, UINT4 u4ContextId);
VOID LnxIpSetGratuitousArp (INT4 i4SetVal);
#endif

