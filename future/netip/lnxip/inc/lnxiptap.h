/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxiptap.h,v 1.8 2016/02/27 10:14:51 siva Exp $
 *
 * Description:This file holds the macros and prototypes  
 *             for LNX IP TAP Implementation.
 *
 *******************************************************************/

#ifndef _LNXIP_TAP_H
#define _LNXIP_TAP_H

#include "fssocket.h"

#ifdef LNXIP4_WANTED

#define TAP_CHR_DEV "/dev/net/tun"
#define LNXIP_TAP_EVENT_BLOCKS 100
#define LNXIP_TAP_PKT_BLOCKS   100
#define LNXIP_MAX_TAP_INTERFACES IP_DEV_MAX_L3VLAN_INTF
#define LNXIP_TAP_MAX_FRAME_SIZE 2000

#define LNXIP_TAP_SEM_NAME           (const UINT1 *) "LNXTAP_SEM"
#define LNXIP_TAP_TASKNAME "LNXTAP"
#define LNXIP_TAP_PKT_TO_TAP_EVENT 0x01
#define LNXIP_TAP_PKT_FROM_TAP_EVENT 0x02

#define LNXIP_TAP_PKT_FROM_TUN 0x01
#define LNXIP_TAP_PKT_FROM_IP  0x02
#define LNXIP_TAP_PKT_FROM_IP6 0x03

/*constants for LNX TAP Based IP queue*/
#define LNXIP_TO_TAP_INTF_PACKET_QUEUE       "LTTQ"
#define LNXIP_FROM_TAP_INTF_PACKET_QUEUE       "LFTQ"
#define LNXIP_TO_TAP_INTF_PACKET_QUEUE_DEPTH 100
#define LNXIP_FROM_TAP_INTF_PACKET_QUEUE_DEPTH LNXIP_MAX_TAP_INTERFACES



typedef struct LnxIpTapIfMap{
    INT4 i4TapFd;
    UINT1 au1IfName[IP_PORT_NAME_LEN];
    INT4 i4CfaIfIdx;
    UINT1 u1IfType;
    UINT1 u1Pad[3];
}tLnxIpTapIfMap;

typedef struct LnxIpTapMsg{
    INT4          i4TapFd;
    INT4          i4CfaIfIdx;
    UINT1         au1IfName[IP_PORT_NAME_LEN];
    UINT1         *pu1PktBuf;
    UINT2         u2PktLen;
    UINT1         u1MsgType;
    UINT1         u1Padding;
} tLnxIpTapMsg;

typedef struct
{
    tTMO_SLL_NODE      NextNode;
    INT4     i4IfIndex;
    INT1     i1ProxyArpAdminStatus;
    UINT1    u1Pad[3];
}tLnxProxyArpEntry;

#ifdef _LNXIP_C
tMemPoolId gLnxTapIpEvntMemPoolId;
tOsixQId   gLnxToTapIpQId;
tOsixQId   gLnxFromTapIpQId;
tOsixTaskId gLnxIpTskId;
tOsixSemId gLnxTapSemId = ZERO;
tLnxIpTapIfMap LnxIpTapIfMapInfo[LNXIP_MAX_TAP_INTERFACES];
tMemPoolId  gProxyArpStatusPoolId;
tTMO_SLL            gProxyArpAdminStatusList;

#if defined(LNXIP4_WANTED) && \
               !defined(KERNEL_WANTED)
UINT1      gu1LnxTapEnabled = TRUE;
#else
UINT1      gu1LnxTapEnabled = FALSE;
#endif

#else
extern tMemPoolId gLnxTapIpEvntMemPoolId;
extern tOsixQId   gLnxToTapIpQId;
extern tOsixQId   gLnxFromTapIpQId;
extern tOsixTaskId gLnxIpTskId;
extern tOsixSemId gLnxTapSemId;
extern tLnxIpTapIfMap LnxIpTapIfMapInfo[LNXIP_MAX_TAP_INTERFACES];
extern tMemPoolId  gProxyArpStatusPoolId;
extern tTMO_SLL            gProxyArpAdminStatusList;
#endif

PUBLIC INT4 LnxTapLock (VOID);
PUBLIC INT4 LnxTapUnLock (VOID);
VOID LnxIpTapHandlePktFromTapEvent(INT4 i4SockDesc);
VOID LnxIpWritePktToTap(tCRU_BUF_CHAIN_HEADER *pBuf);
INT4 LnxIpTapGetIfMapFrmFileDesc (INT4 i4FileDesc, tLnxIpTapIfMap **pLnxIpTapIfMapInfo);
INT4 LnxIpTapDeleteIf (UINT1 *pu1IfName);
INT4 LnxIpTapGetIfMapFrmCfaIdx (INT4 i4CfaIdx, tLnxIpTapIfMap **pLnxIpTapIfMapInfo);
INT4 LnxIpTapGetIfMapFrmIfName (UINT1 *pu1IfName, tLnxIpTapIfMap **pLnxIpTapIfMapInfo);
INT4 LnxIpTapCreateIf (UINT1 *pu1IfName, INT4 i4CfaIfIndex, UINT1 u1IfType);
VOID LnxIpPostPktToTap(UINT4, tCRU_BUF_CHAIN_HEADER *, tEnetV2Header *, UINT1);
extern VOID CfaHandleOutBoundPktFromLnxIpTap(tCRU_BUF_CHAIN_HEADER *, INT4,
                                      UINT4);
#endif
#endif
