/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lnxvrf.h,v 1.3 2015/03/09 13:04:33 siva Exp $
 *
 * Description:This file contains definitions and function
 *             prototypes related to Linux IP interface.    
 *
 *******************************************************************/
#ifndef _LNXVRF_H
#define _LNXVRF_H

#include "vcm.h"
#ifdef _LNXVRF_C
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <fcntl.h>
/*#include <dlfcn.h>*/
#include <linux/if_link.h>
#include <linux/if_addr.h>
#include <errno.h>
#include <sys/socket.h>
#include <asm/types.h>
/*#include <linux/if.h>*/
#include <net/if.h>
/*#include <linux/if_packet.h>*/
#include <linux/if_ether.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <linux/sockios.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#include <sys/param.h>
#endif
#define LNXVRF_ZERO      0
#ifdef _LNXVRF_C
tMemPoolId gLnxVrfMsgMemPoolId;
tMemPoolId gLnxVrfVcInfoMemPoolId;
tMemPoolId gLnxVrfIfInfoMemPoolId;
tOsixTaskId gu4LnxVrfTskId = LNXVRF_ZERO;
tOsixSemId gLnxVrfSemId = LNXVRF_ZERO;
tOsixSemId gLnxEvtSemId = LNXVRF_ZERO;
tOsixSemId gLnxVrfGlobSemId = LNXVRF_ZERO;
tOsixQId   gLnxVrfQId;
tOsixQId    gLnxPostBkIpQId;
#else
extern tMemPoolId gLnxVrfMsgMemPoolId;
extern tMemPoolId gLnxVrfVcInfoMemPoolId;
extern tMemPoolId gLnxVrfIfInfoMemPoolId;
extern tOsixTaskId gu4LnxVrfTskId;
extern tOsixSemId gLnxVrfSemId;
extern tOsixSemId gLnxEvtSemId;
extern tOsixSemId gLnxVrfGlobSemId;
extern tOsixQId   gLnxVrfQId;
extern tOsixQId    gLnxPostBkIpQId;
#endif

#ifdef _LNXIP_C
tRBTree    gLnxVrfInfoRBRoot;
tRBTree    gLnxVrfIfInfoRBRoot;

#else
extern tRBTree    gLnxVrfInfoRBRoot;
extern tRBTree    gLnxVrfIfInfoRBRoot;
#endif
#define LNX_VRF_HEX_LEN  2
/*Mempool-id*/
/*Allocation and release of memory for pLnxVrfMsg*/
#define LNX_VRF_MSG_ENTRY_ALLOC(pLnxVrfMsg) \
             (pLnxVrfMsg = (tLnxVrfMsg *)MemAllocMemBlk(gLnxVrfMsgMemPoolId))
#define  LNX_VRF_MSG_FREE(pLnxVrfMsg) \
              MemReleaseMemBlock(gLnxVrfMsgMemPoolId, (UINT1 *)(pLnxVrfMsg))
/*Allocation and release of memory for pLnxVrfInfo*/
#define LNX_VRF_INFO_ENTRY_ALLOC(pLnxVrfInfo) \
             (pLnxVrfInfo = (tLnxVrfInfo *)MemAllocMemBlk(gLnxVrfVcInfoMemPoolId))
#define  LNX_VRF_INFO_FREE(pLnxVrfInfo) \
              MemReleaseMemBlock(gLnxVrfVcInfoMemPoolId, (UINT1 *)(pLnxVrfInfo))
#define LNX_VRF_IFINFO_ENTRY_ALLOC(pLnxVrfIfInfo) \
             (pLnxVrfIfInfo = (tLnxVrfIfInfo *)MemAllocMemBlk(gLnxVrfIfInfoMemPoolId))
#define  LNX_VRF_IFINFO_FREE(pLnxVrfIfInfo) \
              MemReleaseMemBlock(gLnxVrfIfInfoMemPoolId, (UINT1 *)(pLnxVrfIfInfo))
#define LNX_VCM_INFO_ENTRY_ALLOC(pLnxVcmInfo) \
             (pLnxVcmInfo = (tLnxVcmInfo *)MemAllocMemBlk(gLnxVrfVcInfoMemPoolId))
            

#define LNX_VRF_INIT_COMPLETE(u4Status)       LnxVrfInitComplete(u4Status)
#define LNX_VRF_TASK_PRIORITY   60
#define LNX_VRF_TASK_CREATE  OsixTskCrt
#define LNX_VRF_INFO_TABLE      gLnxVrfInfoRBRoot
#define LNX_VRF_IFINFO_TABLE      gLnxVrfIfInfoRBRoot
#define LNX_VRF_RB_TREE_CREATE_EMBED RBTreeCreateEmbedded
#define LNX_VRF_AUDIT_TASK_ID  gu4LnxVrfTskId

#define LNX_VRF_EVENT     0x00000001
#define LNX_VCM_INTERFACE_EVENT   0x00000002
#define LNX_VRF_EVENT_WAIT_FLAGS  (OSIX_WAIT | OSIX_EV_ANY)
#define LNX_VRF_SEMAPHORE         ((UINT1 *) "LNXVRF")
#define LNX_VRF_EVNT_SEM                ((UINT1 *) "EVNTLOCK")
#define LNX_VRF_GLOB_SEM                ((UINT1 *) "LNXVRFGLOB")
#define LNX_VRF_MESSAGE_ARRIVAL_Q_NAME   "LNVR"
#define LNX_VRF_EVNT_ARRIVAL_Q_NAME   "EVTQ"
#define LNX_VRF_MESSAGE_ARRIVAL_Q_DEPTH       SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_LNX_VRF_MSG_MEMBLK_SIZE  SYS_DEF_MAX_NUM_CONTEXTS
#define MAX_LNX_VRF_VCINFO_SIZE   SYS_DEF_MAX_NUM_CONTEXTS
#define LNX_VRF_PROT_LOCK              LnxVrfLock
#define LNX_VRF_PROT_UNLOCK           LnxVrfUnLock
#define NETIPV4_SUCCESS                 0
#define NETIPV4_FAILURE                 -1
#define LNXVRF_SMALLER  -1
#define LNXVRF_GREATER   1
#define LNXVRF_EQUAL     0
#define LNXVRF_TASK_DELAY 2
#define LNX_VRF_TASKOFFSET      2
#define LNX_VRF_DEFAULT_CXT_ID  VCM_DEFAULT_CONTEXT


typedef struct LnxVrfMsg
{
    UINT4 u4VrfId;
    UINT4 u4RetVal;
    UINT4 u4IfIndex;
    UINT4 u4TskId;
    INT4 i4SockId;
    INT4 i4Sockdomain;
    INT4 i4SockType;
    INT4 i4SockProto;
    UINT1 u1MsgType;
    UINT1 u1IfType;
    UINT1         au1Reserved[2];
}tLnxVrfMsg ;

typedef enum
{
    TSK_BEFORE_INIT = 0,
    TSK_INIT_SUCCESS,
    TSK_INIT_FAILURE
}
tTskStatus;


#ifdef _LNXVRF_C

#define NETNS_RUN_DIR "/var/run/netns"

struct nlhandle     nlh =  { .fd = -1 };

#endif
/*Prototypes*/
INT1    LnxVrfInit  PROTO ((UINT4 u4TskId));
VOID LNX_VRF_Process_Message_Arrival_Event (VOID);
INT1
LnxVrfCreateContextInLnx (UINT4 u4VrfId);
INT1
LnxVrfDeInitForContext (UINT4 u4ContextId);

INT1
LnxVrfDeleteContextInLnx (UINT4 u4VrfId);
INT4
LnxVrfContextEntryCompare PROTO ((tRBElem *e1, tRBElem *e2));

INT4
LnxVrfIfEntryCompare(tRBElem * e1, tRBElem * e2);


tOsixQId *LnxVrfQueueIdGet(VOID);

INT4
IpHandleIfaceMapping (UINT4 u4NewCxtId, UINT2 u4IfIndex, INT4 i4Sockdomain, INT4 i4SockType, INT4 i4SockProto);

INT4
IpHandleIfaceUnMapping (UINT4 u4NewCxtId, UINT2 u4IfIndex, INT4 i4Sockdomain, INT4 i4SockType, INT4 i4SockProto);

INT4
LnxVrfLock(VOID);
INT4
LnxVrfUnLock(VOID);
UINT4 LnxVrfOpenSockInVrfCxt(UINT4 u4ContextId,INT4 i4Sockdomain,INT4 i4SockType,INT4 i4SockProto);
INT4 LnxVrfNetlinkSockInit(UINT4 u4ContextId);

INT4 LnxVrfMapUnMapIntInLnxCxt (INT4 cmd, UINT4 flags, CHR1 *dev, CHR1 *argv);
INT4        LnxVrfOpenSocket (INT4 domain, INT4 type, INT4 protocol);

INT4 LnxVrfGetNetnsInteger (INT4 *val, CHR1 *arg, INT4 base);
INT4 LnxVrfGetNetnsFd (CHR1 *name);
VOID LnxVrfInitComplete(UINT4 u4Status);
INT4 IpHandleLoopbackIfaceMapping (UINT4, UINT4);
INT4 IpHandleLoopbackIfaceUnMapping (UINT4, UINT4);

#endif

