/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: isnpapi.c,v 1.2 2014/11/11 13:01:04 siva Exp $
 *
 *******************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : isnpapi.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Isis                                           */
/*    MODULE NAME           : Isis                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains functions that interface    */
/*                            with hardware                                  */
/*---------------------------------------------------------------------------*/

#include "isincl.h"

/*****************************************************************************/
/* Function Name      : IsisHwProgram                                         */
/*                                                                           */
/* Description        : This function enables ISIS functionality             */
/*                      at the hardware when desired                         */
/*                                                                           */
/* Input(s)           : u1Status                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISIS_SUCCESS / ISIS_FAILURE                          */
/*****************************************************************************/

INT1
IsisHwProgram (UINT1 u1Status)
{
    if (IpFsNpIsisProgram (u1Status) == FNP_FAILURE)
    {
        return ISIS_FAILURE;
    }
    
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IsisFsMbsmHwProgram                                  */
/*                                                                           */
/* Description        : This function enables ISIS functionality             */
/*                      at the hardware when desired                         */
/*                                                                           */
/* Input(s)           : pSlotInfo                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISIS_SUCCESS / ISIS_FAILURE                          */
/*****************************************************************************/

#ifdef MBSM_WANTED

UINT1
IsisFsMbsmHwProgram (tMbsmSlotInfo * pSlotInfo, UINT1 u1Status)
{
        if (IpFsNpMbsmIsisProgram (pSlotInfo,u1Status) ==  FNP_FAILURE)
        {
                return ISIS_FAILURE;
        }

        return ISIS_SUCCESS;
}

#endif
 
