/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: isissz.c,v 1.4 2013/11/29 11:04:13 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _ISISSZ_C
#include "isincl.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
IsisSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ISIS_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsISISSizingParams[i4SizingId].u4StructSize,
                              FsISISSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(ISISMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            IsisSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
IsisSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsISISSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, ISISMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
IsisSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ISIS_MAX_SIZING_ID; i4SizingId++)
    {
        if (ISISMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (ISISMemPoolIds[i4SizingId]);
            ISISMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
