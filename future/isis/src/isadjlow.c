/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: isadjlow.c,v 1.8 2017/09/11 13:44:07 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
#include "isincl.h"
#include "isextn.h"

/* LOW LEVEL Routines for Table : IsisISAdjTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisISAdjTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisISAdjTable (INT4 i4IsisSysInstance,
                                        INT4 i4IsisCircIndex,
                                        INT4 i4IsisISAdjIndex)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisISAdjTable ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE) ||
        (i4IsisISAdjIndex <= 0) || (i4IsisCircIndex <= 0))
    {

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid indices. \n"
                 "The indices are SysInstance = %d"
                 "CircIndex = %d, AdjIndex = %d\n",
                 i4IsisSysInstance, i4IsisCircIndex, i4IsisISAdjIndex));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting "
                 "nmhValidateIndexInstanceIsisISAdjTable ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            i4RetVal = IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                         &pAdjEntry);
            if (i4RetVal == ISIS_SUCCESS)
            {
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Invalid indices.The indices are "
                         "SysInstance = %d\n" " CircIndex = %d\n, "
                         "AdjIndex = %d\n", i4IsisSysInstance,
                         i4IsisCircIndex, i4IsisISAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Invalid indices.The indices are "
                     "SysInstance = %d\n" " CircIndex = %d\n, "
                     "AdjIndex = %d\n", i4IsisSysInstance,
                     i4IsisCircIndex, i4IsisISAdjIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid indices.The indices are "
                 "SysInstance = %d\n" " CircIndex = %d\n, "
                 "AdjIndex = %d\n", i4IsisSysInstance,
                 i4IsisCircIndex, i4IsisISAdjIndex));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisISAdjTable ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisISAdjTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisISAdjTable (INT4 *pi4IsisSysInstance,
                                INT4 *pi4IsisCircIndex, INT4 *pi4IsisISAdjIndex)
{
    return (nmhGetNextIndexIsisISAdjTable (0, pi4IsisSysInstance, 0,
                                           pi4IsisCircIndex, 0,
                                           pi4IsisISAdjIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisISAdjTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisCircIndex
                nextIsisCircIndex
                IsisISAdjIndex
                nextIsisISAdjIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisISAdjTable (INT4 i4IsisSysInstance,
                               INT4 *pi4NextIsisSysInstance,
                               INT4 i4IsisCircIndex, INT4 *pi4NextIsisCircIndex,
                               INT4 i4IsisISAdjIndex,
                               INT4 *pi4NextIsisISAdjIndex)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT1                i1RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetNextIndexIsisISAdjTable ()\n"));

    if (IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4IsisSysInstance,
                                            (UINT4 *) &i4IsisSysInstance)
            == ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : NO Further valid index available.\n"
                     "The Indices are SysInstance: %u CircIndex: %d "
                     "AdjIndex :%d\n", i4IsisSysInstance, i4IsisCircIndex,
                     i4IsisISAdjIndex));

            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting nmhGetNextIndexIsisISAdjTable ()\n"));

            return (SNMP_FAILURE);
        }
        else
        {
            i4IsisCircIndex = 0;
            i4IsisISAdjIndex = 0;
        }
    }

    if (pContext == NULL)
    {
        i4RetValue =
            IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    }
    while (pContext != NULL)
    {
        if (nmhUtlGetNextIndexIsisISAdjTable
            (i4IsisSysInstance, i4IsisCircIndex, i4IsisISAdjIndex,
             &i4IsisISAdjIndex) == ISIS_FAILURE)
        {
            if (nmhUtlGetNextIndexIsisCircTable ((UINT4) i4IsisSysInstance,
                                                 (UINT4) i4IsisCircIndex,
                                                 (UINT4 *) &i4IsisCircIndex)
                == ISIS_FAILURE)
            {
                if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4IsisSysInstance,
                                                    (UINT4 *)
                                                    &i4IsisSysInstance) ==
                    ISIS_FAILURE)
                {

                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : NO Further valid index available.\n"
                             "The Indices are sys instance: %d "
                             "CircIndex: %d AdjIndex :%d\n",
                             i4IsisSysInstance, i4IsisCircIndex,
                             i4IsisISAdjIndex));
                    NMP_EE ((ISIS_LGST,
                             "NMP <X> : Exiting "
                             "nmhGetNextIndexIsisISAdjTable ()\n"));

                    return (SNMP_FAILURE);
                }
                else
                {
                    i4IsisCircIndex = 0;
                    i4IsisISAdjIndex = 0;
                    continue;
                }
            }
            else
            {
                i4IsisISAdjIndex = 0;
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = i4IsisSysInstance;
            *pi4NextIsisCircIndex = i4IsisCircIndex;
            *pi4NextIsisISAdjIndex = i4IsisISAdjIndex;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> :The next Indices are sys instance:"
                     " %d CircIndex: %d AdjIndex :%d\n",
                     i4IsisSysInstance, i4IsisCircIndex, i4IsisISAdjIndex));

            i1RetVal = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetNextIndexIsisISAdjTable ()\n"));
    UNUSED_PARAM (i4RetValue);
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisISAdjState
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex

                The Object 
                retValIsisISAdjState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjState (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                      INT4 i4IsisISAdjIndex, INT4 *pi4RetValIsisISAdjState)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjState ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {

                *pi4RetValIsisISAdjState = pAdjEntry->u1AdjState;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d "
                         "AdjIndex :%d\n", i4IsisSysInstance,
                         i4IsisCircIndex, i4IsisISAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }

        else
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjState ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisISAdjNeighSNPAAddress
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex

                The Object 
                retValIsisISAdjNeighSNPAAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjNeighSNPAAddress (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                 INT4 i4IsisISAdjIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIsisISAdjNeighSNPAAddress)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetIsisISAdjNeighSNPAAddress ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal =
            IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex, &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            i4RetVal = IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                         &pAdjEntry);

            if (i4RetVal == ISIS_SUCCESS)
            {
                MEMCPY (pRetValIsisISAdjNeighSNPAAddress->pu1_OctetList,
                        pAdjEntry->au1AdjNbrSNPA, ISIS_SNPA_ADDR_LEN);

                pRetValIsisISAdjNeighSNPAAddress->i4_Length
                    = ISIS_SNPA_ADDR_LEN;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d ",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisISAdjNeighSNPAAddress ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisISAdjNeighSysType
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex

                The Object 
                retValIsisISAdjNeighSysType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjNeighSysType (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                             INT4 i4IsisISAdjIndex,
                             INT4 *pi4RetValIsisISAdjNeighSysType)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjSysType ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal =
            IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex, &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {

                *pi4RetValIsisISAdjNeighSysType
                    = (INT4) pAdjEntry->u1AdjNbrSysType;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIndex));

                i1RetCode = SNMP_SUCCESS;
            }
        }
        else
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjSysType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisISAdjNeighSysID
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex

                The Object 
                retValIsisISAdjNeighSysID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjNeighSysID (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                           INT4 i4IsisISAdjIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValIsisISAdjNeighSysID)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjSysId ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal =
            IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex, &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {
                MEMCPY (pRetValIsisISAdjNeighSysID->pu1_OctetList,
                        pAdjEntry->au1AdjNbrSysID, ISIS_SYS_ID_LEN);

                pRetValIsisISAdjNeighSysID->i4_Length = ISIS_SYS_ID_LEN;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjsysId ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisISAdjUsage
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex

                The Object 
                retValIsisISAdjUsage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjUsage (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                      INT4 i4IsisISAdjIndex, INT4 *pi4RetValIsisISAdjUsage)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjUsage ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {
                /* As the Adjacency Usage is maintained as
                 * ISIS_LEVEL1, ISIS_LEVEL2, or ISIS_LEVEL12
                 * with the values of 1, 2 and 3 respectively,
                 * the value of Usage is returned as AdjUsage + 1
                 * As Value 1 of AdjUsage is for undefined as per the MIB
                 */

                *pi4RetValIsisISAdjUsage = (INT4) (pAdjEntry->u1AdjUsage);
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjUsage ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisISAdjHoldTimer
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex

                The Object 
                retValIsisISAdjHoldTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjHoldTimer (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 i4IsisISAdjIndex,
                          INT4 *pi4RetValIsisISAdjHoldTimer)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjHoldTimer ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {

            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {
                *pi4RetValIsisISAdjHoldTimer = (INT4) pAdjEntry->u2HoldTime;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjHoldTimer ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisISAdjNeighPriority
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex

                The Object 
                retValIsisISAdjNeighPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjNeighPriority (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                              INT4 i4IsisISAdjIndex,
                              INT4 *pi4RetValIsisISAdjNeighPriority)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjNeighPriority ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {
            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {
                *pi4RetValIsisISAdjNeighPriority = pAdjEntry->u1AdjNbrPriority;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjNeighPriority ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisISAdjUpTime
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex

                The Object 
                retValIsisISAdjUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjUpTime (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                       INT4 i4IsisISAdjIndex, INT4 *pi4RetValIsisISAdjUpTime)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4CurrTick = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjUptime ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {
            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {
                u4CurrTick = ISIS_GET_CURR_TICK (pContext);
                *pi4RetValIsisISAdjUpTime = (INT4) (u4CurrTick -
                                                    pAdjEntry->u4AdjUpTime);
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjNeighSNPAAddress"));

    return (i1RetCode);

}

/* LOW LEVEL Routines for Table : IsisISAdjAreaAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisISAdjAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex
                IsisISAdjAreaAddrIndex
                IsisISAdjAreaAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisISAdjAreaAddrTable (INT4 i4IsisSysInstance,
                                                INT4 i4IsisCircIndex,
                                                INT4 i4IsisISAdjIndex,
                                                INT4 i4IsisISAdjAreaAddrIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pIsisISAdjAreaAddress)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               au1AreaAddr[ISIS_AREA_ADDR_LEN];
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisISAdjAreaAddrTable ()\n"));

    MEMSET (au1AreaAddr, 0, ISIS_AREA_ADDR_LEN);

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE) ||
        (i4IsisCircIndex <= 0) ||
        (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS) ||
        (i4IsisISAdjIndex <= 0) ||
        (i4IsisISAdjAreaAddrIndex <= 0) ||
        (pIsisISAdjAreaAddress->i4_Length == 0) ||
        (MEMCMP (pIsisISAdjAreaAddress->pu1_OctetList,
                 au1AreaAddr, pIsisISAdjAreaAddress->i4_Length) == 0))
    {

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid indices.The indices are "
                 "SysInstance = %d\n, CircIndex = %d\n, AdjIndex = "
                 "%d\n, AdjAreaAddrIndex = %d\n Address Len = %d, "
                 "Address = %p ",
                 i4IsisSysInstance, i4IsisCircIndex, i4IsisISAdjIndex,
                 i4IsisISAdjAreaAddrIndex, pIsisISAdjAreaAddress->i4_Length,
                 pIsisISAdjAreaAddress->pu1_OctetList));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting "
                 "nmhValidateIndexInstanceIsisISAdjAreaAddrTable ()\n"));

        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {
            i4RetVal = IsisAdjGetAdjRec (pCktEntry,
                                         (UINT4) i4IsisISAdjIndex, &pAdjEntry);

            if (i4RetVal == ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "SNMP_SUCCESS"));
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                i1RetCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIndex));
            }
        }
        else
        {
            i1RetCode = SNMP_FAILURE;
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisISAdjAreaAddrTable ()\n"));
    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisISAdjAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex
                IsisISAdjAreaAddrIndex
                IsisISAdjAreaAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisISAdjAreaAddrTable (INT4 *pi4IsisSysInstance,
                                        INT4 *pi4IsisCircIndex,
                                        INT4 *pi4IsisISAdjIndex,
                                        INT4 *pi4IsisISAdjAreaAddrIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pIsisISAdjAreaAddress)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    UINT1               au1OctetList[ISIS_AREA_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE FirstAdjAreaAddr;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered "
             "nmhGetFirstIndexIsisISAdjAreaAddrTable ()\n"));

    FirstAdjAreaAddr.pu1_OctetList = au1OctetList;
    FirstAdjAreaAddr.i4_Length = ISIS_AREA_ADDR_LEN;

    i1RetCode =
        nmhGetNextIndexIsisISAdjAreaAddrTable (0, pi4IsisSysInstance, 0,
                                               pi4IsisCircIndex, 0,
                                               pi4IsisISAdjIndex, 0,
                                               pi4IsisISAdjAreaAddrIndex,
                                               &FirstAdjAreaAddr,
                                               pIsisISAdjAreaAddress);
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhGetFirstIndexIsisISAdjAreaAddrTable ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisISAdjAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisCircIndex
                nextIsisCircIndex
                IsisISAdjIndex
                nextIsisISAdjIndex
                IsisISAdjAreaAddrIndex
                nextIsisISAdjAreaAddrIndex
                IsisISAdjAreaAddress
                nextIsisISAdjAreaAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisISAdjAreaAddrTable (INT4 i4IsisSysInstance,
                                       INT4 *pi4NextIsisSysInstance,
                                       INT4 i4IsisCircIndex,
                                       INT4 *pi4NextIsisCircIndex,
                                       INT4 i4IsisISAdjIndex,
                                       INT4 *pi4NextIsisISAdjIndex,
                                       INT4 i4IsisISAdjAreaAddrIndex,
                                       INT4 *pi4NextIsisISAdjAreaAddrIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pIsisISAdjAreaAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextIsisISAdjAreaAddress)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered "
             "nmhGetNextIndexIsisISAdjAreaAddrTable ()\n"));

    UNUSED_PARAM (pIsisISAdjAreaAddress);

    if ((i4IsisSysInstance < 0) || (i4IsisCircIndex < 0) ||
        (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS) ||
        (i4IsisISAdjIndex < 0) || (i4IsisISAdjAreaAddrIndex < 0))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <T> :Invalid indices : \n The indices are SysInstance :"
                 "%d, CircIndex : %d, AdjIndex : %d\n, AdjAreaIndex : %d",
                 i4IsisSysInstance, i4IsisCircIndex, i4IsisISAdjIndex,
                 i4IsisISAdjAreaAddrIndex));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhGetNextIndexIsisISAdjAreaAddrTable ()\n"));

        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_FAILURE)
    {
        i4RetVal = nmhUtlGetNextIndexIsisSysTable ((UINT4) i4IsisSysInstance,
                                                   (UINT4 *)
                                                   &i4IsisSysInstance);
        if (i4RetVal == ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> :No Next indices available "));

            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting "
                     "nmhGetNextIndexIsisISAdjAreaAddrTable ()\n"));

            return (SNMP_FAILURE);
        }
        else
        {
            i4IsisCircIndex = 0;
            i4IsisISAdjIndex = 0;
            i4IsisISAdjAreaAddrIndex = 0;
            i4RetValue =
                IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
        }
    }

    while (pContext != NULL)
    {
        if (nmhUtlGetNextIndexIsisISAdjAreaAddrTable
            (i4IsisSysInstance, i4IsisCircIndex, i4IsisISAdjIndex,
             i4IsisISAdjAreaAddrIndex, pi4NextIsisISAdjAreaAddrIndex,
             pNextIsisISAdjAreaAddress) == ISIS_FAILURE)
        {
            if (nmhUtlGetNextIndexIsisISAdjTable
                (i4IsisSysInstance, i4IsisCircIndex,
                 i4IsisISAdjIndex, &i4IsisISAdjIndex) == ISIS_FAILURE)
            {

                if (nmhUtlGetNextIndexIsisCircTable ((UINT4) i4IsisSysInstance,
                                                     (UINT4) i4IsisCircIndex,
                                                     (UINT4 *) &i4IsisCircIndex)
                    == ISIS_FAILURE)
                {

                    if (nmhUtlGetNextIndexIsisSysTable
                        ((UINT4) i4IsisSysInstance,
                         (UINT4 *) &i4IsisSysInstance) == ISIS_FAILURE)
                    {
                        NMP_PT ((ISIS_LGST,
                                 "NMP <E> :No Next indices available. "));

                        NMP_EE ((ISIS_LGST,
                                 "NMP <X> : Exiting "
                                 "nmhGetNextIndexIsisISAdjAreaAddrTable () "
                                 "\n"));

                        return (SNMP_FAILURE);
                    }
                    else
                    {
                        i4IsisCircIndex = 0;
                        i4IsisISAdjIndex = 0;
                        i4IsisISAdjAreaAddrIndex = 0;
                        continue;
                    }
                }
                else
                {
                    i4IsisISAdjIndex = 0;
                    i4IsisISAdjAreaAddrIndex = 0;
                    continue;
                }
            }
            else
            {
                i4IsisISAdjAreaAddrIndex = 0;
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = i4IsisSysInstance;
            *pi4NextIsisCircIndex = i4IsisCircIndex;
            *pi4NextIsisISAdjIndex = i4IsisISAdjIndex;

            NMP_PT ((ISIS_LGST,
                     "NMP <T> :Next indices available.\n"
                     "The indices are SysInstance :  %d, "
                     "CircIndex : %d, AdjIndex : %d, AdjAreaAddrIndex : %d,"
                     "Area Address :%x.%x \n",
                     i4IsisSysInstance, i4IsisCircIndex,
                     i4IsisISAdjIndex,
                     i4IsisISAdjAreaAddrIndex,
                     pNextIsisISAdjAreaAddress->pu1_OctetList[0],
                     pNextIsisISAdjAreaAddress->pu1_OctetList[1]));

            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexIsisISAdjAreaAddrTable ()\n"));
    UNUSED_PARAM (i4RetValue);
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : IsisISAdjIPAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisISAdjIPAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex
                IsisISAdjIPAddrIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisISAdjIPAddrTable (INT4 i4IsisSysInstance,
                                              INT4 i4IsisCircIndex,
                                              INT4 i4IsisISAdjIndex,
                                              INT4 i4IsisISAdjIPAddrIndex)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisISAdjIPAddrTable ()\n"));

    UNUSED_PARAM (i4IsisISAdjIndex);
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE) ||
        (i4IsisCircIndex <= 0) ||
        (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS) ||
        (i4IsisISAdjIPAddrIndex <= 0))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid indices.\n"
                 "The indices are SysInstance = %d "
                 "CircIndex = %d, AdjIndex = %d\n",
                 i4IsisSysInstance, i4IsisCircIndex, i4IsisISAdjIPAddrIndex));

        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            i4RetVal = IsisAdjGetAdjRec (pCktEntry,
                                         (UINT4) i4IsisISAdjIndex, &pAdjEntry);
            if (i4RetVal == ISIS_SUCCESS)
            {
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                i1RetCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIPAddrIndex));
            }
        }
        else
        {
            i1RetCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));
        }
    }
    else
    {
        i1RetCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d CircIndex: %d\n",
                 i4IsisSysInstance, i4IsisCircIndex));
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisISAdjAreaAddrTable ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisISAdjIPAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex
                IsisISAdjIPAddrIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisISAdjIPAddrTable (INT4 *pi4IsisSysInstance,
                                      INT4 *pi4IsisCircIndex,
                                      INT4 *pi4IsisISAdjIndex,
                                      INT4 *pi4IsisISAdjIPAddrIndex)
{
    return (nmhGetNextIndexIsisISAdjIPAddrTable (0, pi4IsisSysInstance, 0,
                                                 pi4IsisCircIndex, 0,
                                                 pi4IsisISAdjIndex, 0,
                                                 pi4IsisISAdjIPAddrIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisISAdjIPAddrTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisCircIndex
                nextIsisCircIndex
                IsisISAdjIndex
                nextIsisISAdjIndex
                IsisISAdjIPAddrIndex
                nextIsisISAdjIPAddrIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisISAdjIPAddrTable (INT4 i4IsisSysInstance,
                                     INT4 *pi4NextIsisSysInstance,
                                     INT4 i4IsisCircIndex,
                                     INT4 *pi4NextIsisCircIndex,
                                     INT4 i4IsisISAdjIndex,
                                     INT4 *pi4NextIsisISAdjIndex,
                                     INT4 i4IsisISAdjIPAddrIndex,
                                     INT4 *pi4NextIsisISAdjIPAddrIndex)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    UINT1               u1AdjIpAddrType = 0;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetNextIndexIsisISAdjIPAddrTable ()\n"));

    if ((i4IsisSysInstance < 0) || (i4IsisCircIndex < 0) ||
        (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS) ||
        (i4IsisISAdjIndex < 0) || (i4IsisISAdjIPAddrIndex < 0))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> :Invalid Indices : The indices are"
                 " SysInstance :  %d\n, CircIndex : %d\n, "
                 "AdjIndex : %d\n, IPAddrIndex : %d\n", i4IsisSysInstance,
                 i4IsisCircIndex, i4IsisISAdjIndex, i4IsisISAdjIPAddrIndex));

        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_FAILURE)
    {
        i4RetVal = nmhUtlGetNextIndexIsisSysTable ((UINT4) i4IsisSysInstance,
                                                   (UINT4 *)
                                                   &i4IsisSysInstance);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> :No further valid Indices "));

            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting nmhGetNextIndexIsisISAdjIPAddrTable"
                     "()\n"));

            return (SNMP_FAILURE);
        }
        else
        {
            i4IsisCircIndex = 0;
            i4IsisISAdjIndex = 0;
        }
    }
    if (pContext == NULL)
    {
        i4RetValue =
            IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    }

    i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex, &pCktEntry);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if ((IsisAdjGetAdjRec (pCktEntry, (UINT4) i4IsisISAdjIndex,
                               &pAdjEntry)) == ISIS_SUCCESS)
        {
            u1AdjIpAddrType = pAdjEntry->u1AdjIpAddrType;
        }
    }

    while (pContext != NULL)
    {

        if (u1AdjIpAddrType != 0)
        {
            if ((u1AdjIpAddrType == ISIS_ADDR_IPVX) &&
                (i4IsisISAdjIPAddrIndex == ISIS_IPV4_ADDR_INDEX))
            {
                *pi4NextIsisSysInstance = i4IsisSysInstance;
                *pi4NextIsisCircIndex = i4IsisCircIndex;
                *pi4NextIsisISAdjIndex = i4IsisISAdjIndex;
                *pi4NextIsisISAdjIPAddrIndex = ISIS_IPV6_ADDR_INDEX;
                break;
            }
        }

        if (nmhUtlGetNextIndexIsisISAdjIPAddrTable (i4IsisSysInstance,
                                                    i4IsisCircIndex,
                                                    i4IsisISAdjIndex,
                                                    &i4IsisISAdjIndex)
            == ISIS_FAILURE)
        {

            if (nmhUtlGetNextIndexIsisCircTable ((UINT4) i4IsisSysInstance,
                                                 (UINT4) i4IsisCircIndex,
                                                 (UINT4 *) &i4IsisCircIndex) ==
                ISIS_FAILURE)
            {

                if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4IsisSysInstance,
                                                    (UINT4 *)
                                                    &i4IsisSysInstance) ==
                    ISIS_FAILURE)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> :No Further valid indices available"))
                        NMP_EE ((ISIS_LGST, "NMP <X> : "
                                 "Exiting nmhGetNextIndexIsisISAdjIPAddrTable ()\n"));

                    return (SNMP_FAILURE);
                }
                else
                {
                    i4IsisCircIndex = 0;
                    i4IsisISAdjIndex = 0;
                    continue;
                }
            }
            else
            {
                i4IsisISAdjIndex = 0;
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = i4IsisSysInstance;
            *pi4NextIsisCircIndex = i4IsisCircIndex;
            *pi4NextIsisISAdjIndex = i4IsisISAdjIndex;
            *pi4NextIsisISAdjIPAddrIndex = 1;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> :Next indices available "
                     "are SysInstance :  %d\n, CircIndex : %d\n, "
                     "AdjIndex : %d\n, IPAddrIndex : %d\n", i4IsisSysInstance,
                     i4IsisCircIndex, i4IsisISAdjIndex,
                     i4IsisISAdjIPAddrIndex));

            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexIsisISAdjIPAddrTable ()\n"));
    return (SNMP_SUCCESS);
    UNUSED_PARAM (i4RetValue);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisISAdjIPAddressType
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex
                IsisISAdjIPAddrIndex

                The Object 
                retValIsisISAdjIPAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjIPAddressType (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                              INT4 i4IsisISAdjIndex,
                              INT4 i4IsisISAdjIPAddrIndex,
                              INT4 *pi4RetValIsisISAdjIPAddressType)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    UINT4               u4AdjIdx = (UINT4) i4IsisISAdjIndex;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjIPAddressType ()\n"));

    UNUSED_PARAM (i4IsisISAdjIndex);

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            if ((IsisAdjGetAdjRec (pCktEntry, u4AdjIdx,
                                   &pAdjEntry)) == ISIS_SUCCESS)
            {
                if (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPVX)
                {
                    if (i4IsisISAdjIPAddrIndex == ISIS_IPV4_ADDR_INDEX)
                    {
                        *pi4RetValIsisISAdjIPAddressType = ISIS_ADDR_IPV4;
                    }
                    if (i4IsisISAdjIPAddrIndex == ISIS_IPV6_ADDR_INDEX)
                    {
                        *pi4RetValIsisISAdjIPAddressType = ISIS_ADDR_IPV6;
                    }
                }
                else
                {
                    *pi4RetValIsisISAdjIPAddressType =
                        pAdjEntry->u1AdjIpAddrType;
                }
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjIPAddrIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjIPAddressType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisISAdjIPAddress
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjIndex
                IsisISAdjIPAddrIndex

                The Object 
                retValIsisISAdjIPAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisISAdjIPAddress (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 i4IsisISAdjIndex, INT4 i4IsisISAdjIPAddrIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValIsisISAdjIPAddress)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    UINT4               u4AdjIdx = (UINT4) i4IsisISAdjIndex;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisISAdjIPAddress ()\n"));

    UNUSED_PARAM (i4IsisISAdjIndex);

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            if ((IsisAdjGetAdjRec (pCktEntry, u4AdjIdx,
                                   &pAdjEntry)) == ISIS_SUCCESS)
            {
                if (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pRetValIsisISAdjIPAddress->pu1_OctetList,
                            pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                            ISIS_MAX_IPV4_ADDR_LEN);
                    pRetValIsisISAdjIPAddress->i4_Length =
                        ISIS_MAX_IPV4_ADDR_LEN;

                }
                else if (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPV6)
                {
                    MEMCPY (pRetValIsisISAdjIPAddress->pu1_OctetList,
                            pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                            ISIS_MAX_IPV6_ADDR_LEN);
                    pRetValIsisISAdjIPAddress->i4_Length =
                        ISIS_MAX_IPV6_ADDR_LEN;
                }
                else if (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPVX)
                {
                    if (i4IsisISAdjIPAddrIndex == ISIS_IPV4_ADDR_INDEX)
                    {
                        MEMCPY (pRetValIsisISAdjIPAddress->pu1_OctetList,
                                pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                                ISIS_MAX_IPV4_ADDR_LEN);
                        pRetValIsisISAdjIPAddress->i4_Length =
                            ISIS_MAX_IPV4_ADDR_LEN;

                    }
                    else if (i4IsisISAdjIPAddrIndex == ISIS_IPV6_ADDR_INDEX)
                    {
                        MEMCPY (pRetValIsisISAdjIPAddress->pu1_OctetList,
                                pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                                ISIS_MAX_IPV6_ADDR_LEN);
                        pRetValIsisISAdjIPAddress->i4_Length =
                            ISIS_MAX_IPV6_ADDR_LEN;
                    }

                }
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the "
                         "indices  sys instance: %d CircIndex: %d "
                         "AdjIndex :%d\n", i4IsisSysInstance,
                         i4IsisCircIndex, i4IsisISAdjIPAddrIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjIPAddress ()\n"));

    return (i1RetCode);

}

/* LOW LEVEL Routines for Table : IsisISAdjProtSuppTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisISAdjProtSuppTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjProtSuppIndex
                IsisISAdjProtSuppProtocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisISAdjProtSuppTable (INT4 i4IsisSysInstance,
                                                INT4 i4IsisCircIndex,
                                                INT4 i4IsisISAdjProtSuppIndex,
                                                INT4
                                                i4IsisISAdjProtSuppProtocol)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisISAdjProtSuppTable ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE) ||
        (i4IsisCircIndex <= 0) ||
        (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS) ||
        (i4IsisISAdjProtSuppIndex <= 0) || (i4IsisISAdjProtSuppProtocol <= 0))
    {

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid indices.The indices are SysInstance = "
                 "%d\n" " CircIndex = %d\n, AdjProtocolIndex = %d\n, "
                 "AdjProtocol : %d\n", i4IsisSysInstance, i4IsisCircIndex,
                 i4IsisISAdjProtSuppIndex, i4IsisISAdjProtSuppProtocol));

        i1RetCode = SNMP_FAILURE;
    }

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            i4RetVal = IsisAdjGetAdjRec (pCktEntry,
                                         (UINT4) i4IsisISAdjProtSuppIndex,
                                         &pAdjEntry);
            if (i4RetVal == ISIS_SUCCESS)
            {
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                i1RetCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d\n CircIndex: %d\n AdjIndex "
                         ":%d\n", i4IsisSysInstance, i4IsisCircIndex,
                         i4IsisISAdjProtSuppIndex));
            }
        }
        else
        {
            i1RetCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4IsisSysInstance, i4IsisCircIndex));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisISAdjAreaAddrTable ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisISAdjProtSuppTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisISAdjProtSuppIndex
                IsisISAdjProtSuppProtocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisISAdjProtSuppTable (INT4 *pi4IsisSysInstance,
                                        INT4 *pi4IsisCircIndex,
                                        INT4 *pi4IsisISAdjProtSuppIndex,
                                        INT4 *pi4IsisISAdjProtSuppProtocol)
{
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered "
             "nmhGetFirstIndexIsisISAdjProtSuppTable ()\n"));

    return (nmhGetNextIndexIsisISAdjProtSuppTable (0, pi4IsisSysInstance, 0,
                                                   pi4IsisCircIndex, 0,
                                                   pi4IsisISAdjProtSuppIndex,
                                                   0,
                                                   pi4IsisISAdjProtSuppProtocol));
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhGetFirstIndexIsisISAdjProtSuppTable ()\n"));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisISAdjProtSuppTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisCircIndex
                nextIsisCircIndex
                IsisISAdjProtSuppIndex
                nextIsisISAdjProtSuppIndex
                IsisISAdjProtSuppProtocol
                nextIsisISAdjProtSuppProtocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisISAdjProtSuppTable (INT4 i4IsisSysInstance,
                                       INT4 *pi4NextIsisSysInstance,
                                       INT4 i4IsisCircIndex,
                                       INT4 *pi4NextIsisCircIndex,
                                       INT4 i4IsisISAdjProtSuppIndex,
                                       INT4 *pi4NextIsisISAdjProtSuppIndex,
                                       INT4 i4IsisISAdjProtSuppProtocol,
                                       INT4 *pi4NextIsisISAdjProtSuppProtocol)
{
    tIsisSysContext    *pContext = NULL;
    INT4                i4RetVal = ISIS_FAILURE;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered "
             "nmhGetNextIndexIsisISAdjProtSuppTable ()\n"));

    if ((i4IsisSysInstance < 0) || (i4IsisCircIndex < 0) ||
        (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS) ||
        (i4IsisISAdjProtSuppIndex < 0) || (i4IsisISAdjProtSuppProtocol < 0))
    {

        NMP_PT ((ISIS_LGST,
                 "NMP <E> :No Next indices available.The indices are "
                 "SysInstance :  %d\n, CircIndex : %d\n"
                 "AdjProtSuppAdjIndex :%d\n AdjProtSuppProtocol : %d",
                 i4IsisSysInstance, i4IsisCircIndex,
                 i4IsisISAdjProtSuppIndex, i4IsisISAdjProtSuppProtocol));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting "
                 "nmhGetNextIndexIsisISAdjProtSuppTable ()\n"));
        return (SNMP_FAILURE);
    }

    if (IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4IsisSysInstance,
                                            (UINT4 *) &i4IsisSysInstance)
            == ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> :No Next indices available.The indices are "
                     "SysInstance :  %d\n, CircIndex : %d\n"
                     "AdjProtSuppAdjIndex :%d\n AdjProtSuppProtocol : %d",
                     i4IsisSysInstance, i4IsisCircIndex,
                     i4IsisISAdjProtSuppIndex, i4IsisISAdjProtSuppProtocol));

            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting "
                     "nmhGetNextIndexIsisISAdjProtSuppTable ()\n"));

            return (SNMP_FAILURE);
        }
        else
        {
            i4IsisCircIndex = 0;
            i4IsisISAdjProtSuppIndex = 0;
            i4IsisISAdjProtSuppProtocol = 0;
        }
    }

    if (pContext == NULL)
    {
        i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    }

    while (pContext != NULL)
    {
        if (nmhUtlGetNextIndexIsisISAdjProtSuppTable (i4IsisSysInstance,
                                                      i4IsisCircIndex,
                                                      i4IsisISAdjProtSuppIndex,
                                                      i4IsisISAdjProtSuppProtocol,
                                                      &i4IsisISAdjProtSuppProtocol)
            == ISIS_FAILURE)
        {
            if (nmhUtlGetNextIndexIsisISAdjTable
                (i4IsisSysInstance, i4IsisCircIndex,
                 i4IsisISAdjProtSuppIndex,
                 &i4IsisISAdjProtSuppIndex) == ISIS_FAILURE)
            {
                if (nmhUtlGetNextIndexIsisCircTable
                    ((UINT4) i4IsisSysInstance, (UINT4) i4IsisCircIndex,
                     (UINT4 *) &i4IsisCircIndex) == ISIS_FAILURE)
                {
                    if (nmhUtlGetNextIndexIsisSysTable
                        ((UINT4) i4IsisSysInstance,
                         (UINT4 *) &i4IsisSysInstance) == ISIS_FAILURE)
                    {

                        NMP_PT ((ISIS_LGST,
                                 "NMP <E> :No Further indices available "));

                        NMP_EE ((ISIS_LGST,
                                 "NMP <X> : Exiting "
                                 "nmhGetNextIndexIsisISAdjProtSuppTable ()"
                                 "\n"));

                        return (SNMP_FAILURE);
                    }
                    else
                    {
                        i4IsisCircIndex = 0;
                        i4IsisISAdjProtSuppIndex = 0;
                        i4IsisISAdjProtSuppProtocol = 0;
                        continue;
                    }
                }
                else
                {
                    i4IsisISAdjProtSuppIndex = 0;
                    i4IsisISAdjProtSuppProtocol = 0;
                    continue;
                }
            }
            else
            {
                i4IsisISAdjProtSuppProtocol = 0;
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = i4IsisSysInstance;
            *pi4NextIsisCircIndex = i4IsisCircIndex;
            *pi4NextIsisISAdjProtSuppIndex = i4IsisISAdjProtSuppIndex;
            *pi4NextIsisISAdjProtSuppProtocol = i4IsisISAdjProtSuppProtocol;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> :Next indices available.The "
                     "indices are SysInstance :  %d\n, "
                     "CircIndex : %d\n AdjProtSuppAdjIndex : "
                     "%d\n AdjProtSuppProtocol : %d",
                     i4IsisSysInstance, i4IsisCircIndex,
                     i4IsisISAdjProtSuppIndex, i4IsisISAdjProtSuppProtocol));

            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexIsisISAdjProtSuppTable ()\n"));
    UNUSED_PARAM (i4RetVal);

    return (SNMP_SUCCESS);

}
