/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isnmhutl.c,v 1.13 2017/09/11 13:44:08 siva Exp $
 *
 * Description: This file contains the utility functions for SNMP Low Level 
 *              Routines.
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/******************************************************************************
 * Function    : nmhUtlGetNextIndexIsisSysTable ()
 * Description : This function gets the next Instance Index.
 * Input(s)    : u4CurrIdx    - The Current Instance Index 
 * Outputs(s)  : pu4NextIdx   - Pointer to the next Instance Index
 * Globals     : None                                  
 * Returns     : ISIS_SUCCESS - if next index is fetched successfully
 *               ISIS_FAILURE - if next index cannot be fetched
 *****************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisSysTable (UINT4 u4CurrIdx, UINT4 *pu4NextIdx)
{
    INT4                i4RetCode = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhUtlGetNextIndexIsisSysTable ()\n"));

    *pu4NextIdx = ISIS_LL_MAX_IDX;
    pContext = ISIS_GET_CONTEXT ();

    while (pContext != NULL)
    {
        /* if the Current Index is greater than the given Index and 
         * lesser than the Next Index, copy the current index to Next 
         * Index. As the System context table is not in the sorted order,
         * All the instances in the System table are checked in this while
         * loop. Hence there is no break statement after the Success case
         */

        if ((pContext->u4SysInstIdx > u4CurrIdx)
            && (pContext->u4SysInstIdx <= *pu4NextIdx))
        {
            *pu4NextIdx = pContext->u4SysInstIdx;
            i4RetCode = ISIS_SUCCESS;
        }
        pContext = pContext->pNext;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhUtlGetNextIndexIsisSysTable ()\n"));

    return i4RetCode;
}

/******************************************************************************
 * Function    : nmhUtlGetNextMAAddr ()
 * Description : This function gets the next Manual Area Address in the 
 *               Manual Area Address Table.
 * Input(s)    : pContext  - Pointer to the System Context
 *               pMAAEntry - Pointer to the current entry in the 
 *                           Manual Area Address Table
 * Outputs(s)  : pNextMAA  - Pointer to the next Manual Area Address
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
 ******************************************************************************/

PUBLIC INT4
nmhUtlGetNextMAAddr (tIsisSysContext * pContext,
                     tSNMP_OCTET_STRING_TYPE * pMAAEntry,
                     tSNMP_OCTET_STRING_TYPE * pNextMAA)
{
    INT4                i4RetCode = ISIS_FAILURE;
    INT4                i4MemOut = -1;
    INT4                i4MemCur = -1;
    INT4                i4MemNext = -1;
    tIsisMAAEntry      *pInputMA = NULL;
    tIsisMAAEntry      *pTempMA = NULL;
    tIsisMAAEntry      *pTempMinGreaterMA = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGetNextMAAddr ()\n"));

    MEMSET (pNextMAA->pu1_OctetList, 0xff, ISIS_AREA_ADDR_LEN);

    pTempMA = pContext->ManAddrTable.pMAARec;

    /* Below piece of code is added for obtaining the manual address
       in the lexicographical order */

    while (pTempMA != NULL)
    {
        if (pTempMinGreaterMA == NULL)
        {

            i4MemOut = MEMCMP (pTempMA->ManAreaAddr.au1AreaAddr,
                               pMAAEntry->pu1_OctetList, ISIS_AREA_ADDR_LEN);
            /* i4MemOut is expected to be greater than 1 or equal to 0
               at any instant */
            if (pTempMA->ManAreaAddr.u1Length > pMAAEntry->i4_Length)
            {
                pTempMinGreaterMA = pTempMA;
            }
            else if (pTempMA->ManAreaAddr.u1Length == pMAAEntry->i4_Length)
            {

                if (i4MemOut > 0)
                {
                    pTempMinGreaterMA = pTempMA;
                }
                else if (i4MemOut == 0)
                {
                    pInputMA = pTempMA;
                }
            }
        }
        /*Process the next element in the list */
        pTempMA = pTempMA->pNext;
        /* Find the minimum greater element in the list */
        if ((pTempMinGreaterMA != NULL) && (pTempMA != NULL))
        {
            i4MemCur = MEMCMP (pTempMinGreaterMA->ManAreaAddr.au1AreaAddr,
                               pTempMA->ManAreaAddr.au1AreaAddr,
                               ISIS_AREA_ADDR_LEN);
            i4MemNext = MEMCMP (pTempMA->ManAreaAddr.au1AreaAddr,
                                pMAAEntry->pu1_OctetList, ISIS_AREA_ADDR_LEN);

            if ((pTempMinGreaterMA->ManAreaAddr.u1Length >
                 pTempMA->ManAreaAddr.u1Length)
                && (pTempMA->ManAreaAddr.u1Length > pMAAEntry->i4_Length))
            {
                pTempMinGreaterMA = pTempMA;
            }
            else if (pTempMinGreaterMA->ManAreaAddr.u1Length ==
                     pTempMA->ManAreaAddr.u1Length)
            {
                if ((i4MemCur > 0) && (i4MemNext > 0))
                {
                    pTempMinGreaterMA = pTempMA;
                }
            }
        }

        if ((pInputMA != NULL) && (pTempMA != NULL)
            && (pTempMinGreaterMA == NULL))
        {
            if (pInputMA->ManAreaAddr.u1Length == pTempMA->ManAreaAddr.u1Length)
            {
                if (MEMCMP (pInputMA->ManAreaAddr.au1AreaAddr,
                            pTempMA->ManAreaAddr.au1AreaAddr,
                            ISIS_AREA_ADDR_LEN) < 0)
                {
                    pTempMinGreaterMA = pTempMA;
                }
            }
            else if (pInputMA->ManAreaAddr.u1Length <
                     pTempMA->ManAreaAddr.u1Length)
            {
                pTempMinGreaterMA = pTempMA;
            }
        }

    }                            /* End While */

    /* Copy the required output in pNextMAA */
    if (pTempMinGreaterMA != NULL)
    {
        MEMCPY (pNextMAA->pu1_OctetList,
                pTempMinGreaterMA->ManAreaAddr.au1AreaAddr, ISIS_AREA_ADDR_LEN);
        pNextMAA->i4_Length = pTempMinGreaterMA->ManAreaAddr.u1Length;
        i4RetCode = ISIS_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextMAAddr ()\n"));
    return i4RetCode;
}

/******************************************************************************
 * Function    : nmhUtlGetNextAreaAddr ()
 * Description : This function gets the next  Area Address in the Area Address
 *               Table.
 * Input(s)    : pContext - Pointer to System Context
 *               pAAEntry - Pointer to the current entry in the Area Address
 *                          Table
 *               pNextAA  - Pointer to the Next Area Address Record
 * Outputs(s)  : pNextAA  - Pointer to the Next Area Address Record
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
 ******************************************************************************/

PUBLIC INT4
nmhUtlGetNextAreaAddr (tIsisSysContext * pContext,
                       tSNMP_OCTET_STRING_TYPE * pAAEntry,
                       tSNMP_OCTET_STRING_TYPE * pNextAA)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4MemOut = -1;
    INT4                i4MemCur = -1;
    INT4                i4MemNext = -1;
    tIsisAAEntry       *pInputAA = NULL;
    tIsisAAEntry       *pTempAA = NULL;
    tIsisAAEntry       *pTempMinGreaterAA = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGetNextAreaAddr ()\n"));

    MEMSET (pNextAA->pu1_OctetList, 0xff, ISIS_AREA_ADDR_LEN);

    pTempAA = pContext->AreaAddrTable.pAARec;

    while (pTempAA != NULL)
    {
        /* Below piece of code is added for obtaining the manual address
           in the lexicographical order */

        if (pTempMinGreaterAA == NULL)
        {
            /* i4MemOut is expected to be greater than 1 or equal to 0
               at any instant */

            i4MemOut = MEMCMP (pTempAA->AreaAddr.au1AreaAddr,
                               pAAEntry->pu1_OctetList, ISIS_AREA_ADDR_LEN);
            if (pTempAA->AreaAddr.u1Length > pAAEntry->i4_Length)
            {
                pTempMinGreaterAA = pTempAA;
            }
            else if (pTempAA->AreaAddr.u1Length == pAAEntry->i4_Length)
            {

                if (i4MemOut > 0)
                {
                    pTempMinGreaterAA = pTempAA;
                }
                else if (i4MemOut == 0)
                {
                    pInputAA = pTempAA;
                }
            }
        }
        /*Process the next element in the list */
        pTempAA = pTempAA->pNext;
        /* Find the minimum greater element in the list */

        if ((pTempMinGreaterAA != NULL) && (pTempAA != NULL))
        {
            i4MemCur = MEMCMP (pTempMinGreaterAA->AreaAddr.au1AreaAddr,
                               pTempAA->AreaAddr.au1AreaAddr,
                               ISIS_AREA_ADDR_LEN);
            i4MemNext = MEMCMP (pTempAA->AreaAddr.au1AreaAddr,
                                pAAEntry->pu1_OctetList, ISIS_AREA_ADDR_LEN);

            if ((pTempMinGreaterAA->AreaAddr.u1Length >
                 pTempAA->AreaAddr.u1Length)
                && (pTempAA->AreaAddr.u1Length > pAAEntry->i4_Length))
            {
                pTempMinGreaterAA = pTempAA;
            }
            else if (pTempMinGreaterAA->AreaAddr.u1Length ==
                     pTempAA->AreaAddr.u1Length)
            {
                if ((i4MemCur > 0) && (i4MemNext > 0))
                {
                    pTempMinGreaterAA = pTempAA;
                }
            }
        }

        if ((pInputAA != NULL) && (pTempAA != NULL)
            && (pTempMinGreaterAA == NULL))
        {
            if (pInputAA->AreaAddr.u1Length == pTempAA->AreaAddr.u1Length)
            {
                if (MEMCMP (pInputAA->AreaAddr.au1AreaAddr,
                            pTempAA->AreaAddr.au1AreaAddr,
                            ISIS_AREA_ADDR_LEN) < 0)
                {
                    pTempMinGreaterAA = pTempAA;
                }
            }
            else if (pInputAA->AreaAddr.u1Length < pTempAA->AreaAddr.u1Length)
            {
                pTempMinGreaterAA = pTempAA;
            }
        }

    }                            /* End While */
    /* Copy the required output in pNextMAA */
    if (pTempMinGreaterAA != NULL)
    {
        MEMCPY (pNextAA->pu1_OctetList,
                pTempMinGreaterAA->AreaAddr.au1AreaAddr, ISIS_AREA_ADDR_LEN);
        pNextAA->i4_Length = pTempMinGreaterAA->AreaAddr.u1Length;
        i4RetVal = ISIS_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextAreaAddr ()\n"));

    return i4RetVal;
}

/******************************************************************************
 * Function    : nmhUtlGetNextPSEntry ()
 * Description : This function gets the next Protocol Support Entry in 
 *               the Protocol Support Table.
 * Input(s)    : pContext - Pointer to System Context
 *               u1CurrPS - Current entry in the Protocol Support Table
 * Outputs(s)  : pNextPS  - Pointer to the next Protocol Support Entry
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
 ******************************************************************************/

PUBLIC INT4
nmhUtlGetNextPSEntry (tIsisSysContext * pContext,
                      UINT1 u1CurrPS, UINT1 *pu1NextPS)
{
    UINT1               u1PSCount = 0;
    INT4                i4RetVal = ISIS_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGetNextPSEntry ()\n"));

    *pu1NextPS = 0xff;

    for (u1PSCount = 0; u1PSCount < ISIS_MAX_PROTS_SUPP; u1PSCount++)
    {
        /* if the Current Index is greater than the given Index and 
         * lesser than the Next Index, copy the current index to 
         * Next Index 
         */

        if ((pContext->aProtSupp[u1PSCount].u1ProtSupp > u1CurrPS)
            && (pContext->aProtSupp[u1PSCount].u1ProtSupp <= *pu1NextPS))
        {
            *pu1NextPS = pContext->aProtSupp[u1PSCount].u1ProtSupp;
            i4RetVal = ISIS_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextPSEntry ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function     : nmhUtlGetNextSummAddr ()
 * Description  : This function gets the next Summary Address in the Summary
 *                Address Table.
 * Input(s)     : pContext     - Pointer to System Context
 *                u1SAType     - Type of Summary Address : ipv4 or ipv6
 *                pSAEntry     - Pointer to the current Summary Address
 *                               in the Summary Address Table
 *                u1SAMaskType - Type of Summary Address Mask : ipv4 or ipv6
 *                pSAMask      - Pointer to the current Summary Address 
 *                               Mask in the Summary Address Table
 * Outputs(s)   : pu1NextSAType- Pointer to the type of next Summary Address 
 *                pNextSA      - Pointer to the next Summary Address
 *                pu1NextSAMask- Pointer to the type of next Summary Address
 *                               Mask
 *                pNextSAMask  - Pointer to the next Summary Address Mask
 * Globals      : None
 * Returns      : ISIS_SUCCESS - if next entry is fetched successfully
 *                ISIS_FAILURE - if next entry cannot be fetched
 *****************************************************************************/

PUBLIC INT4
nmhUtlGetNextSummAddr (tIsisSysContext * pContext, UINT1 u1SAType,
                       UINT1 *pu1NextSAType,
                       tSNMP_OCTET_STRING_TYPE * pSAEntry,
                       tSNMP_OCTET_STRING_TYPE * pNextSA,
                       UINT1 u1PrefLen, UINT1 *pu1NextPrefLen)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSAEntry       *pTravSA = NULL;
    UINT1               u1AddrLen;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGetNextSummAddr ()\n"));

    if (u1SAType == 0)
    {
        pTravSA = pContext->SummAddrTable.pSAEntry;
    }
    else if (IsisCtrlGetSA
             (pContext, pSAEntry->pu1_OctetList, u1SAType, u1PrefLen,
              &pTravSA) == ISIS_SUCCESS)
    {
        pTravSA = pTravSA->pNext;
    }
    if (pTravSA != NULL)
    {
        if (pTravSA->u1AddrType == ISIS_ADDR_IPV4)
        {
            u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
        }
        else
        {
            u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
        }
        *pu1NextSAType = pTravSA->u1AddrType;
        MEMCPY (pNextSA->pu1_OctetList, pTravSA->au1SummAddr, u1AddrLen);
        pNextSA->i4_Length = u1AddrLen;
        *pu1NextPrefLen = pTravSA->u1PrefixLen;
        i4RetVal = ISIS_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextSummAddr ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : nmhUtlGetNextIndexIsisCircTable ()
 * Description : The routine gets the next index of circuit table having the
 *               same instance index.
 * Input       : u4IsisSysInstance    - The Index to the system instance
 *               u4IsisCircIndex      - The current Circuit Index
 * Output      : pu4NextIsisCircIndex - Pointer to the next circuit index
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
******************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisCircTable (UINT4 u4IsisSysInstance,
                                 UINT4 u4IsisCircIndex,
                                 UINT4 *pu4NextIsisCircIndex)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4RetCode = ISIS_FAILURE;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhUtlGetNextIndexIsisCircTable () \n"));

    /* setting the return value pu4IsisCircIndex to Maximum value 
     */

    *pu4NextIsisCircIndex = ISIS_MAX_CKTS;

    i4RetCode = IsisCtrlGetSysContext (u4IsisSysInstance, &pContext);

    if (i4RetCode == ISIS_SUCCESS)
    {
        pCktEntry = pContext->CktTable.pCktRec;
        while (pCktEntry != NULL)
        {
            if ((pCktEntry->u4CktIdx > u4IsisCircIndex)
                && (pCktEntry->u4CktIdx <= *pu4NextIsisCircIndex))
            {
                *pu4NextIsisCircIndex = pCktEntry->u4CktIdx;
                i4RetVal = ISIS_SUCCESS;
            }
            pCktEntry = pCktEntry->pNext;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhUtlGetNextIndexIsisCircTable ()\n"));

    return i4RetVal;
}

/****************************************************************************
 * Function    : nmhUtlGetNextPktCntTable ()
 * Description : The routine gets the next Packet Count index 
 * Input       : u4InstIdx      - The Index of the system instance
 *               u4CktIdx       - The current Circuit index
 *               u1CktLvlIdx    - The Current Circuit level index
 *               u1PktDir       - The Current Packet Count Direction
 * Output      : pu1NextPktDir  - Pointer to the next Packet Count Direction
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
******************************************************************************/

PUBLIC INT4
nmhUtlGetNextPktCntTable (UINT4 u4InstIdx, UINT4 u4CktIdx,
                          UINT1 u1CktLvlIdx, UINT1 u1PktDir,
                          UINT1 *pu1NextPktDir)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    INT4                i4TempRetVal = ISIS_FAILURE;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGetNextPktCntTable () \n"));

    i4TempRetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry);

    if (i4RetVal != ISIS_FAILURE)
    {
        if (u1CktLvlIdx == ISIS_LEVEL1)
        {
            pCktLvlEntry = pCktEntry->pL1CktInfo;
        }
        else if (u1CktLvlIdx == ISIS_LEVEL2)
        {
            pCktLvlEntry = pCktEntry->pL2CktInfo;
        }

        if (pCktLvlEntry != NULL)
        {
            if (u1PktDir == 0)
            {
                *pu1NextPktDir = ISIS_LL_SENDING;
                i4RetVal = ISIS_SUCCESS;
            }
            else if (u1PktDir == ISIS_LL_SENDING)
            {
                *pu1NextPktDir = ISIS_LL_RECEIVING;
                i4RetVal = ISIS_SUCCESS;
            }
            else if (u1PktDir == ISIS_LL_RECEIVING)
            {
                *pu1NextPktDir = 0;
                i4RetVal = ISIS_FAILURE;
            }
        }
        else
        {
            i4RetVal = ISIS_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextPktCntTable ()\n"));
    UNUSED_PARAM (i4TempRetVal);
    return (i4RetVal);
}

/****************************************************************************
 * Function    : nmhUtlGetNextIndexIsisCircLevelTable ()
 * Description : The routine gets the next CircLevel index 
 * Input       : u4InstIdx                 - The Index of the system instance
 *               u4CktIdx                  - The current Circuit index
 *               u1CircLevelIndex          - The Current Circuit level index
 * Output      : pu1NextIsisCircLevelIndex - Pointer to the next Circuit level
 *                                           Index
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
****************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisCircLevelTable (UINT4 u4InstIdx,
                                      UINT4 u4CktIdx,
                                      UINT1 u1CircLevelIndex,
                                      UINT1 *pu1NextIsisCircLevelIndex)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    INT4                i4TempRetVal = ISIS_FAILURE;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhUtlGetNextIndexIsisCircLevelTable () \n"));

    i4TempRetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry);

    if (i4RetVal != ISIS_FAILURE)
    {
        if (u1CircLevelIndex == 0)
        {
            if (pCktEntry->pL1CktInfo != NULL)
            {
                *pu1NextIsisCircLevelIndex = ISIS_LEVEL1;
            }
            else if (pCktEntry->pL2CktInfo != NULL)
            {
                *pu1NextIsisCircLevelIndex = ISIS_LEVEL2;
            }
            else
            {
                i4RetVal = ISIS_FAILURE;
            }
        }
        else if ((u1CircLevelIndex == ISIS_LEVEL1)
                 && (pCktEntry->pL2CktInfo != NULL))
        {
            *pu1NextIsisCircLevelIndex = ISIS_LEVEL2;
            i4RetVal = ISIS_SUCCESS;
        }
        else
        {
            i4RetVal = ISIS_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhUtlGetNextIndexIsisCircLevelTable ()\n"));
    UNUSED_PARAM (i4TempRetVal);
    return (i4RetVal);
}

/****************************************************************************
 * Function    : nmhUtlGetNextIndexIsisISAdjTable ()
 * Description : This function gets the next adjacency Index in lexicographic
 *               order
 * Input       : i4IsisSysInstance      - The System instance index
 *               i4IsisCircIndex        - The Current Circuit Index
 *               i4IsisISAdjIndex       - The Current Adjacency Index
 * Output      : pi4NextIsisISAdjIndex  - Pointer to the next Adjacency index
 * Returns     : ISIS_SUCCESS or ISIS_FAILURE
 *****************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisISAdjTable (INT4 i4IsisSysInstance,
                                  INT4 i4IsisCircIndex,
                                  INT4 i4IsisISAdjIndex,
                                  INT4 *pi4NextIsisISAdjIndex)
{
    INT1                i1Flag = ISIS_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    INT4                i4TempRetVal = ISIS_FAILURE;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhUtlGetNextIndexIsisISAdjTable\n"));

    /* setting the i4IsisISAdjIndex to Maximum value */

    *pi4NextIsisISAdjIndex = ISIS_LL_MAX_IDX;

    i4TempRetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex, &pCktEntry);

    if (i4RetVal == ISIS_SUCCESS)
    {
        pAdjEntry = pCktEntry->pAdjEntry;
        while (pAdjEntry != NULL)
        {
            if ((pAdjEntry->u4AdjIdx > (UINT4) i4IsisISAdjIndex) &&
                (pAdjEntry->u4AdjIdx <= (UINT4) *pi4NextIsisISAdjIndex))
            {
                *pi4NextIsisISAdjIndex = pAdjEntry->u4AdjIdx;
                /* Update isisAdjTable cache-pointer */
                pCktEntry->pLastAdjEntry = pAdjEntry;
                i1Flag = ISIS_SUCCESS;
            }
            pAdjEntry = pAdjEntry->pNext;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhUtlGetNextIndexIsisISAdjTable\n"));
    UNUSED_PARAM (i4TempRetVal);
    return (i1Flag);
}

/****************************************************************************
 * Function    : nmhUtlGetNextIndexIsisISIPRATable ()
 * Description : The routine gets the next lexicographic IPRA Index
 * Input       : i4IsisSysInstance    - The System Instance Index
 *               i4IsisCircIndex      - The Circuit Index
 *               i4IsisIPRAType       - The IPRA type
 *               i4IsisIPRAIndex      - The IPRA Index
 * Output      : pi4NextIsisIPRAType  - Pointer to the next IPRA Type
 *               pi4NextIsisIPRAIndex - Pointer to the next IPRA Index
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
******************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisIPRATable (INT4 i4IsisSysInstance,
                                 INT4 i4IsisIPRAType,
                                 INT4 *pi4NextIsisIPRAType,
                                 INT4 i4IsisIPRAIndex,
                                 INT4 *pi4NextIsisIPRAIndex)
{
    INT1                i1Flag = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRAEntry = NULL;
    INT4                i4TempRetVal = ISIS_FAILURE;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhUtlGetNextIndexIsisIPRATable ()\n"));

    /* setting the pi4IsisIsisIPRAIndex to Maximum value */

    *pi4NextIsisIPRAIndex = ISIS_LL_MAX_IDX;

    if (i4IsisIPRAType == 0)
    {
        i4IsisIPRAType = ISIS_LL_MANUAL;
    }

    i4TempRetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    pIPRAEntry = pContext->IPRAInfo.pIPRAEntry;

    if (NULL != pContext->pLastIPRAEntry)
    {
        for (;;)                /* Scan through entire IPRA IPRA list */
        {
            if (NULL == pContext->pLastIPRAEntry)
            {
                if ((i1Flag != ISIS_SUCCESS)
                    && (i4IsisIPRAType != ISIS_LL_AUTOMATIC))
                {
                    /* Restart serching from head of the list for auto nodes
                     * [XXX]: try ...*/
                    i4IsisIPRAType = ISIS_LL_AUTOMATIC;
                    i4IsisIPRAIndex = 0;
                    /* After this, loop does not break */
                    pContext->pLastIPRAEntry = pContext->IPRAInfo.pIPRAEntry;
                }
                else
                {
                    /* Reach end of the list and not found entry -
                     * break and return */
                    break;
                }
            }
            /* Check nodes with corresponding IPRAType only */
            if (pContext->pLastIPRAEntry->u1IPRAType == i4IsisIPRAType)
            {                    /* Entries are sorted by IPRAIdx */
                if ((pContext->pLastIPRAEntry->u4IPRAIdx >
                     (UINT4) i4IsisIPRAIndex)
                    && (pContext->pLastIPRAEntry->u4IPRAIdx <=
                        (UINT4) *pi4NextIsisIPRAIndex))
                {
                    *pi4NextIsisIPRAIndex =
                        (INT4) pContext->pLastIPRAEntry->u4IPRAIdx;
                    *pi4NextIsisIPRAType =
                        (INT4) pContext->pLastIPRAEntry->u1IPRAType;
                    i1Flag = ISIS_SUCCESS;
                    /* We can break here (founded).Cache pointer equal 
                     *  this position as we will expect in the next call */
                    break;
                }
            }
            pContext->pLastIPRAEntry = pContext->pLastIPRAEntry->pNext;
        }

    }
    else
    {
        /* Normal processing branch */
        while (pIPRAEntry != NULL)
        {
            if (pIPRAEntry->u1IPRAType != i4IsisIPRAType)
            {
                pIPRAEntry = pIPRAEntry->pNext;
            }
            else
            {

                if ((pIPRAEntry->u4IPRAIdx > (UINT4) i4IsisIPRAIndex) &&
                    (pIPRAEntry->u4IPRAIdx <= (UINT4) *pi4NextIsisIPRAIndex))
                {
                    *pi4NextIsisIPRAIndex = (INT4) pIPRAEntry->u4IPRAIdx;
                    *pi4NextIsisIPRAType = (INT4) pIPRAEntry->u1IPRAType;
                    /* Update cache-pointer for isiIPRATable */
                    pContext->pLastIPRAEntry = pIPRAEntry;
                    i1Flag = ISIS_SUCCESS;
                }
                pIPRAEntry = pIPRAEntry->pNext;
            }

            if ((pIPRAEntry == NULL) && (i1Flag != ISIS_SUCCESS)
                && (i4IsisIPRAType != ISIS_LL_AUTOMATIC))
            {
                i4IsisIPRAType = ISIS_LL_AUTOMATIC;
                i4IsisIPRAIndex = 0;
                pIPRAEntry = pContext->IPRAInfo.pIPRAEntry;

            }
        }

    }
    UNUSED_PARAM (i4TempRetVal);
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextIndexIsisIPRATable()"));
    return (i1Flag);
}

/****************************************************************************
 * Function    : nmhUtlGetNextIndexIsisISAdjAreaAddrTable
 * Description : The routine gets the next index of AdjAreaAddr
 * Input       : i4IsisSysInstance            - The System instance index
 *               i4IsisCircIndex              - The Circuit Index
 *               i4IsisISAdjIndex             - The Adjacency Index
 *               i4IsisISAdjAreaAddrIndex     - The Adjacent Area Address index
 *               pi4NextIsisISAdjAreaAddrIndex- 
 *                                          The Next Adjacent Area Address index
 * Output      : pNextIsisISAdjAreaAddress   - Pointer to the Next Adjacent
 *                                             Area Address Entry
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
****************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisISAdjAreaAddrTable (INT4 i4IsisSysInstance,
                                          INT4 i4IsisCircIndex,
                                          INT4 i4IsisISAdjIndex,
                                          INT4 i4IsisISAdjAreaAddrIndex,
                                          INT4 *pi4NextIsisISAdjAreaAddrIndex,
                                          tSNMP_OCTET_STRING_TYPE
                                          * pNextIsisISAdjAreaAddress)
{
    INT4                i4Flag = ISIS_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisAdjAAEntry    *pAreaAddress = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X>: Entered mhUtlGetNextIndexIsisISAdjAreaAddrTablen\n"));

    /* setting the pi4NextIsisISAdjAreaAddrIndex  to Maximum value */

    /* Here why 0xffff is the area address is max 2 bytes so max area address
     * cannot go more than 0xffff even if this is an impossible situation.
     * Usually a value of 4 would be suffcient as MaxAreaAddr supported usually
     * be 3 */

    *pi4NextIsisISAdjAreaAddrIndex = 0xffff;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            i4RetVal = IsisAdjGetAdjRec (pCktEntry,
                                         (UINT4) i4IsisISAdjIndex, &pAdjEntry);

            if (i4RetVal == ISIS_SUCCESS)
            {
                pAreaAddress = pAdjEntry->pAdjAreaAddr;

                while (pAreaAddress != NULL)
                {
                    if (pAreaAddress->ISAdjAreaAddress.u1Length <
                        ISIS_AREA_ADDR_LEN)
                    {
                        if ((pAreaAddress->ISAdjAreaAddress.u1Length <
                             ISIS_AREA_ADDR_LEN)
                            && (pAreaAddress->u4AdjAreaAddrIdx >
                                (UINT4) i4IsisISAdjAreaAddrIndex)
                            && (pAreaAddress->u4AdjAreaAddrIdx <
                                (UINT4) (*pi4NextIsisISAdjAreaAddrIndex)))
                        {

                            *pi4NextIsisISAdjAreaAddrIndex =
                                pAreaAddress->u4AdjAreaAddrIdx;
                            MEMCPY (pNextIsisISAdjAreaAddress->pu1_OctetList,
                                    pAreaAddress->ISAdjAreaAddress.au1AreaAddr,
                                    pAreaAddress->ISAdjAreaAddress.u1Length);
                            pNextIsisISAdjAreaAddress->i4_Length =
                                pAreaAddress->ISAdjAreaAddress.u1Length;
                            i4Flag = ISIS_SUCCESS;

                        }
                    }
                    pAreaAddress = pAreaAddress->pNext;
                }

            }
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhUtlGetNextIndexIsisISAdjAreaAddrTable()\n"));

    return (i4Flag);
}

/****************************************************************************
 * Function    : nmhUtlGetNextIndexIsisISAdjIPAddrTable
 * Description : The routine gets the next lexicographic index of
 *               AdjIPAddrTable
 * Input       : i4IsisSysInstance               - The System Instance Index
 *               i4IsisCircIndex                 - The Circuit Index
 *               i4IsisISAdjIPAddrAdjIndex       - The Adjacent Index
 * Output      : pi4iNextIsisISAdjIPAddrAdjIndex - The Next Adjacent Index
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
 *****************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisISAdjIPAddrTable (INT4 i4IsisSysInstance,
                                        INT4 i4IsisCircIndex,
                                        INT4 i4IsisISAdjIPAddrAdjIndex,
                                        INT4 *pi4iNextIsisISAdjIPAddrAdjIndex)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT1                i1Flag = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    INT4                i4TempRetVal = ISIS_FAILURE;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhUtlGetNextIndexIsisISAdjIPAddrTable \n"));

    /* setting the i4IsisISAdjIndex to Maximum value */

    *pi4iNextIsisISAdjIPAddrAdjIndex = ISIS_LL_MAX_IDX;

    i4TempRetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex, &pCktEntry);

    if (i4RetVal == ISIS_SUCCESS)
    {
        pAdjEntry = pCktEntry->pAdjEntry;

        while (pAdjEntry != NULL)
        {
            if ((pAdjEntry->u4AdjIdx > (UINT4) i4IsisISAdjIPAddrAdjIndex)
                && (pAdjEntry->u4AdjIdx <=
                    (UINT4) *pi4iNextIsisISAdjIPAddrAdjIndex))
            {
                *pi4iNextIsisISAdjIPAddrAdjIndex = pAdjEntry->u4AdjIdx;
                i1Flag = ISIS_SUCCESS;
            }

            pAdjEntry = pAdjEntry->pNext;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhUtlGetNextIndexIsisISAdjIPAddrTable \n"));
    UNUSED_PARAM (i4TempRetVal);
    return (i1Flag);
}

/****************************************************************************
* Function    :  nmhUtlGetNextIndexIsisISAdjProtSuppTable
* Description :  The routine gets the next index of AdjProtSuppTable
* Input       :  i4IsisSysInstance                - The System Instance Index
*                i4IsisCircIndex                  - The Circuit index
*                i4IsisISAdjProtSuppAdjIndex      - The Adjacent Index 
*                i4IsisISAdjProtSuppProtocol      - The Adjacent Supported 
*                                                   Protocol
* Output      :  pi4NextIsisISAdjProtSuppProtocol - The Next Adjacent Supported
*                                                   Protocol
* Globals     :  None
* Returns     :  ISIS_SUCCESS or ISIS_FAILURE
****************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisISAdjProtSuppTable (INT4 i4IsisSysInstance,
                                          INT4 i4IsisCircIndex,
                                          INT4 i4IsisISAdjProtSuppAdjIndex,
                                          INT4 i4IsisISAdjProtSuppProtocol,
                                          INT4
                                          *pi4NextIsisISAdjProtSuppProtocol)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT1                i1Flag = ISIS_FAILURE;
    UINT1               u1Idx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    INT4                i4TempRetVal = ISIS_FAILURE;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhUtlGetNextIndexIsisISAdjProtSuppTable\n"));

    /* setting the i4IsisISAdjIndex to Maximum value */

    *pi4NextIsisISAdjProtSuppProtocol = 0xff;

    i4TempRetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex, &pCktEntry);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetAdjRec (pCktEntry,
                                     (UINT4) i4IsisISAdjProtSuppAdjIndex,
                                     &pAdjEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {
            for (u1Idx = 0; u1Idx < ISIS_MAX_PROTS_SUPP; u1Idx++)
            {

                if ((i4IsisISAdjProtSuppProtocol <
                     pAdjEntry->au1AdjProtSupp[u1Idx]) &&
                    (pAdjEntry->au1AdjProtSupp[u1Idx] <=
                     *pi4NextIsisISAdjProtSuppProtocol))
                {
                    *pi4NextIsisISAdjProtSuppProtocol =
                        pAdjEntry->au1AdjProtSupp[u1Idx];
                    i1Flag = ISIS_SUCCESS;
                }
            }
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhUtlGetNextIndexIsisISAdjProtSuppTable()\n"));
    UNUSED_PARAM (i4TempRetVal);
    return (i1Flag);

}

/****************************************************************************
* Function    :  nmhUtlGetNextAreaRxPasswd
* Description :  This routine returns the next domain receive password
*                comparing lexicographically.
* Input       :  pContext -                -  Pointer to the Syscontext  
*                pFsIsisExtSysAreaRxPasswd -  Pointer to the SysAreaRxPaaswd 
* Output      :  pNextAreaRxPasswd         -  Pointer to the NextAreaRxPasswd
*                                             in this context
* Globals     :  None 
* Returns     :  ISIS_SUCCESS or ISIS_FAILURE
****************************************************************************/
PUBLIC INT4
nmhUtlGetNextAreaRxPasswd (tIsisSysContext * pContext,
                           tSNMP_OCTET_STRING_TYPE
                           * pFsIsisExtSysAreaRxPasswd,
                           tSNMP_OCTET_STRING_TYPE * pNextAreaRxPasswd)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PwCnt = 0;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGetNextAreaRxPasswd ()\n"));

    MEMSET (pNextAreaRxPasswd->pu1_OctetList, 0xff, ISIS_MAX_PASSWORD_LEN);

    for (u1PwCnt = 0; u1PwCnt < ISIS_MAX_RX_PASSWDS; u1PwCnt++)
    {
        /* if the Current Index is greater than the given Index and 
         * lesser than the Next Index, copy the current index to 
         * Next Index
         */
        if ((MEMCMP (pContext->SysActuals.aSysAreaRxPasswd[u1PwCnt].au1Password,
                     pFsIsisExtSysAreaRxPasswd->pu1_OctetList,
                     pContext->SysActuals.aSysAreaRxPasswd[u1PwCnt].u1Len) > 0)
            && (MEMCMP (pNextAreaRxPasswd->pu1_OctetList,
                        pContext->SysActuals.aSysAreaRxPasswd[u1PwCnt].
                        au1Password,
                        pContext->SysActuals.aSysAreaRxPasswd[u1PwCnt].
                        u1Len)) >= 0)
        {
            MEMCPY (pNextAreaRxPasswd->pu1_OctetList,
                    pContext->SysActuals.aSysAreaRxPasswd[u1PwCnt].au1Password,
                    pContext->SysActuals.aSysAreaRxPasswd[u1PwCnt].u1Len);
            pNextAreaRxPasswd->i4_Length =
                pContext->SysActuals.aSysAreaRxPasswd[u1PwCnt].u1Len;
            i4RetVal = ISIS_SUCCESS;
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextAreaRxPasswd ()\n"));

    return i4RetVal;
}

/****************************************************************************
 * Function    :  nmhUtlGetNextDomainRxPasswd
 * Description :  This routine returns the next domain receive password
 *                comparing lexicographically.
 * Input       :  pContext                     - Pointer to the Context 
 *                pFsIsisExtSysDomainRxPasswd  - Pointer to the Domain RxPasswd
 * Output      :  pNextDomainRxPasswd          - Pointer to the Next Domain 
 *                                               RxPasswd for this context
 * Globals     :  None 
 * Returns     :  ISIS_SUCCESS or ISIS_FAILURE
 ****************************************************************************/

PUBLIC INT4
nmhUtlGetNextDomainRxPasswd (tIsisSysContext * pContext,
                             tSNMP_OCTET_STRING_TYPE
                             * pFsIsisExtSysDomainRxPasswd,
                             tSNMP_OCTET_STRING_TYPE * pNextDomainRxPasswd)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PwCnt;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGetNextDomainRxPasswd ()\n"));

    MEMSET (pNextDomainRxPasswd->pu1_OctetList, 0xff, ISIS_MAX_PASSWORD_LEN);

    for (u1PwCnt = 0; u1PwCnt < ISIS_MAX_RX_PASSWDS; u1PwCnt++)
    {
        /* if the Current Index is greater than the given Index and 
         * lesser than the Next Index, copy the current index to 
         * Next Index */
        if ((MEMCMP (pContext->SysActuals.
                     aSysDomainRxPasswd[u1PwCnt].au1Password,
                     pFsIsisExtSysDomainRxPasswd->pu1_OctetList,
                     pContext->SysActuals.aSysDomainRxPasswd[u1PwCnt].u1Len) >
             0)
            &&
            (MEMCMP
             (pNextDomainRxPasswd->pu1_OctetList,
              pContext->SysActuals.aSysDomainRxPasswd[u1PwCnt].au1Password,
              pContext->SysActuals.aSysDomainRxPasswd[u1PwCnt].u1Len)) >= 0)
        {
            MEMCPY (pNextDomainRxPasswd->pu1_OctetList,
                    pContext->SysActuals.aSysDomainRxPasswd[u1PwCnt].
                    au1Password,
                    pContext->SysActuals.aSysDomainRxPasswd[u1PwCnt].u1Len);
            pNextDomainRxPasswd->i4_Length =
                pContext->SysActuals.aSysDomainRxPasswd[u1PwCnt].u1Len;
            i4RetVal = ISIS_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextDomainRxPasswd ()\n"));

    return i4RetVal;

}

/****************************************************************************
 * Function    :  nmhUtlGetNextCktLvlRxPasswd
 * Description :  This routine returns the next Circuit Level Receive 
 *                password, comparing lexicographically.
 * Input       :  pContext            - Pointer to the Context 
 *                u4CktIdx            - Circuit Index 
 *                pu1CktLvlIdx        - Circuit Level Index
 *                pCktLvlRxPasswd     - Pointer to the Circuit RxPasswd
 * Output      :  pNextDomainRxPasswd - Pointer to the Next Circuit 
 *                                      RxPasswd for this context
 * Globals     :  None 
 * Returns     :  ISIS_SUCCESS or ISIS_FAILURE
 ****************************************************************************/

PUBLIC INT4
nmhUtlGetNextCktLvlRxPasswd (tIsisSysContext * pContext,
                             UINT4 u4CktIdx, UINT1 *pu1CktLvlIdx,
                             tSNMP_OCTET_STRING_TYPE * pCktLvlRxPasswd,
                             tSNMP_OCTET_STRING_TYPE * pNextLvlRxPasswd)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PwCnt = 0;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisCktLevel      *pCktLvl = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGetNextCktLvlRxPasswd ()\n"));

    MEMSET (pNextLvlRxPasswd->pu1_OctetList, 0xff, ISIS_MAX_PASSWORD_LEN);

    if (IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry) == ISIS_SUCCESS)
    {
        if ((*pu1CktLvlIdx == ISIS_LEVEL1) || (*pu1CktLvlIdx == ISIS_LEVEL2))
        {
            pCktLvl = (*pu1CktLvlIdx == ISIS_LEVEL1)
                ? (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);
        }
    }

    if (pCktLvl != NULL)
    {
        for (u1PwCnt = 0; u1PwCnt < ISIS_MAX_RX_PASSWDS; u1PwCnt++)
        {
            if (pCktLvl->aCktRxPasswd[u1PwCnt].u1ExistState != ISIS_ACTIVE)
            {
                continue;
            }

            /* if the Current Index is greater than the given Index and 
             * lesser than the Next Index, copy the current index to 
             * Next Index 
             */
            if ((MEMCMP (pCktLvl->aCktRxPasswd[u1PwCnt].au1Password,
                         pCktLvlRxPasswd->pu1_OctetList,
                         pCktLvl->aCktRxPasswd[u1PwCnt].u1Len) > 0)
                && (MEMCMP (pNextLvlRxPasswd->pu1_OctetList,
                            pCktLvl->aCktRxPasswd[u1PwCnt].au1Password,
                            pCktLvl->aCktRxPasswd[u1PwCnt].u1Len) >= 0))
            {
                MEMCPY (pNextLvlRxPasswd->pu1_OctetList,
                        pCktLvl->aCktRxPasswd[u1PwCnt].au1Password,
                        pCktLvl->aCktRxPasswd[u1PwCnt].u1Len);
                pNextLvlRxPasswd->i4_Length =
                    pCktLvl->aCktRxPasswd[u1PwCnt].u1Len;
                i4RetVal = ISIS_SUCCESS;
            }
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGetNextCktLvlRxPasswd ()\n"));

    return i4RetVal;
}

/******************************************************************************
 *  Function    : nmhUtlGNIfAddr () 
 *  Description : This function gets the next IP Interface Address
 *                lexicographically for the given context 
 *  Input(s)    : pContext                - Pointer to the System Context
 *                i4FsIsisExtIPIfIndex    - IP interface index 
 *                i4FsIsisExtIPIfSubIndex - IP interface Sub Index
 *                i4FsIsisExtIPIfAddrType - IP Interface Address Type
 *                pFsIsisExtIPIfAddr      - IP Interface Address
 *  Outputs     : pNextIPIfAddr           - Next IP interface address
 *  Globals     : None                                  
 *  Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *                ISIS_FAILURE - if next entry cannot be fetched
 ******************************************************************************/

PUBLIC INT4
nmhUtlGNIfAddr (tIsisSysContext * pContext,
                INT4 i4FsIsisExtIPIfIndex,
                INT4 i4FsIsisExtIPIfSubIndex,
                INT4 i4FsIsisExtIPIfAddrType,
                tSNMP_OCTET_STRING_TYPE * pFsIsisExtIPIfAddr,
                tSNMP_OCTET_STRING_TYPE * pNextIPIfAddr)
{
    INT4                i4RetCode = ISIS_FAILURE;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    UINT1               u1AddrLen;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGNIfAddr ()\n"));

    MEMSET (((tIsisIPIfAddr *) (VOID *) (pNextIPIfAddr->pu1_OctetList)),
            0xFF, ISIS_MAX_IP_ADDR_LEN);

    pIPIfRec = pContext->IPIfTable.pIPIfAddr;

    while (pIPIfRec != NULL)
    {
        /* if the Current Index is greater than the given Index and 
         * lesser than the Next Index, copy the current index to 
         * Next Index
         */
        if (pIPIfRec->u1AddrType == ISIS_ADDR_IPV4)
        {
            u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
        }
        else
        {
            u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
        }
        if (((pIPIfRec->u4CktIfIdx == (UINT4) i4FsIsisExtIPIfIndex)
             && (pIPIfRec->u4CktIfSubIdx == (UINT4) i4FsIsisExtIPIfSubIndex)
             && (pIPIfRec->u1AddrType == (UINT1) i4FsIsisExtIPIfAddrType))
            && ((MEMCMP (pIPIfRec->au1IPAddr, pFsIsisExtIPIfAddr->pu1_OctetList,
                         u1AddrLen) > 0)
                && (MEMCMP (pNextIPIfAddr->pu1_OctetList,
                            pIPIfRec->au1IPAddr, u1AddrLen) > 0)))
        {
            MEMCPY (pNextIPIfAddr->pu1_OctetList,
                    pIPIfRec->au1IPAddr, u1AddrLen);
            pNextIPIfAddr->i4_Length = u1AddrLen;
            i4RetCode = ISIS_SUCCESS;
        }

        pIPIfRec = pIPIfRec->pNext;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGNIfAddr ()\n"));

    return i4RetCode;
}

/******************************************************************************
 * Function    : nmhUtlGNIfSubIdx () 
 * Description : This function gets the next IP Interface sub index
 *               lexicographically for the given context 
 * Input(s)    : pContext                     -  Pointer to the System Context
 *               i4FsIsisExtIPIfIndex         - IP interface index 
 *               i4FsIsisExtIPIfSubIndex      - IP interface sub index
 * Output(s)   : pi4NextFsIsisExtIPIfSubIndex - The next IP interface Sub index
 * Globals     : None                                  
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
 ******************************************************************************/

PUBLIC INT4
nmhUtlGNIfSubIdx (tIsisSysContext * pContext,
                  INT4 i4FsIsisExtIPIfIndex,
                  INT4 i4FsIsisExtIPIfSubIndex,
                  INT4 *pi4NextFsIsisExtIPIfSubIndex)
{
    INT4                i4RetCode = ISIS_FAILURE;
    tIsisIPIfAddr      *pIPIfRec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGNIfSubIdx ()\n"));

    pIPIfRec = pContext->IPIfTable.pIPIfAddr;
    *pi4NextFsIsisExtIPIfSubIndex = 0x7FFFFFFF;

    while (pIPIfRec != NULL)
    {
        /* if the Current Index is greater than the given Index and 
         * lesser than the Next Index, copy the current index to 
         * Next Index
         */

        if ((pIPIfRec->u4CktIfIdx == (UINT4) i4FsIsisExtIPIfIndex)
            && ((pIPIfRec->u4CktIfSubIdx > (UINT4) i4FsIsisExtIPIfSubIndex)
                && (pIPIfRec->u4CktIfSubIdx <=
                    (UINT4) *pi4NextFsIsisExtIPIfSubIndex)))
        {
            i4RetCode = ISIS_SUCCESS;
            *pi4NextFsIsisExtIPIfSubIndex = pIPIfRec->u4CktIfSubIdx;
        }

        pIPIfRec = pIPIfRec->pNext;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGNIfSubIdx ()\n"));

    return i4RetCode;
}

/******************************************************************************
 * Function    : nmhUtlGNIfIdx () 
 * Description : This function gets the next IP Interface sub index
 *               lexicographically for the given context 
 * Input(s)    : pContext                  -  Pointer to the System Context
 *               i4FsIsisExtIPIfIndex      - IP interface index 
 * Outputs     : pi4NextFsIsisExtIPIfIndex - The next IP interface Sub index
 * Globals     : None                                  
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
 ******************************************************************************/

PUBLIC INT4
nmhUtlGNIfIdx (tIsisSysContext * pContext, INT4 i4FsIsisExtIPIfIndex,
               INT4 *pi4NextFsIsisExtIPIfIndex)
{
    INT4                i4RetCode = ISIS_FAILURE;
    tIsisIPIfAddr      *pIPIfRec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGNIfIdx ()\n"));

    pIPIfRec = pContext->IPIfTable.pIPIfAddr;
    *pi4NextFsIsisExtIPIfIndex = 0x7FFFFFFF;

    while (pIPIfRec != NULL)
    {
        /* if the Current Index is greater than the given Index and 
         * lesser than the Next Index, copy the current index to 
         * Next Index
         */

        if ((pIPIfRec->u4CktIfIdx > (UINT4) i4FsIsisExtIPIfIndex)
            && (pIPIfRec->u4CktIfIdx <= (UINT4) (*pi4NextFsIsisExtIPIfIndex)))
        {
            *pi4NextFsIsisExtIPIfIndex = pIPIfRec->u4CktIfIdx;
            i4RetCode = ISIS_SUCCESS;
        }

        pIPIfRec = pIPIfRec->pNext;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGNIfIdx ()\n"));

    return i4RetCode;
}

/******************************************************************************
 * Function    : nmhUtlGNIfAddrType() 
 * Description : This function gets the next IP Interface Type 
 *               lexicographically for the given context 
 * Input(s)    : pContext                     -  Pointer to the System Context
 *               i4FsIsisExtIPIfIndex         - IP interface index 
 *               i4FsIsisExtIPIfSubIndex      - IP interface sub index
 *               i4FsIsisExtIPIfAddrType      - IP interface addr Type 
 * Output(s)   : pi4NextFsIsisExtIPIfAddrType - The next IP interface addr Type 
 * Globals     : None                                  
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
 ******************************************************************************/

PUBLIC INT4
nmhUtlGNIfAddrType (tIsisSysContext * pContext,
                    INT4 i4FsIsisExtIPIfIndex,
                    INT4 i4FsIsisExtIPIfSubIndex,
                    INT4 i4FsIsisExtIPIfAddrType,
                    INT4 *pi4NextFsIsisExtIPIfAddrType)
{
    INT4                i4RetCode = ISIS_FAILURE;
    tIsisIPIfAddr      *pIPIfRec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhUtlGNIfAddrType()\n"));

    pIPIfRec = pContext->IPIfTable.pIPIfAddr;
    *pi4NextFsIsisExtIPIfAddrType = 0x7FFFFFFF;

    while (pIPIfRec != NULL)
    {
        /* if the Current Index is greater than the given Index and 
         * lesser than the Next Index, copy the current index to 
         * Next Index
         */

        if ((pIPIfRec->u4CktIfIdx == (UINT4) i4FsIsisExtIPIfIndex)
            && (pIPIfRec->u4CktIfSubIdx == (UINT4) i4FsIsisExtIPIfSubIndex)
            && ((pIPIfRec->u1AddrType > (UINT4) i4FsIsisExtIPIfAddrType)
                && (pIPIfRec->u1AddrType <=
                    (UINT4) *pi4NextFsIsisExtIPIfAddrType)))
        {
            i4RetCode = ISIS_SUCCESS;
            *pi4NextFsIsisExtIPIfAddrType = pIPIfRec->u1AddrType;
        }

        pIPIfRec = pIPIfRec->pNext;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhUtlGNIfAddrType()\n"));

    return i4RetCode;
}

/******************************************************************************
 * Function    : IsisNmhValSysTable () 
 * Description : This function 
 * Input(s)    : i4IsisSysInstance - Instance Index
 * Outputs     : pu4ErrorCode      - Returns one of the following error
 *                       SNMP_ERR_INCONSISTENT_NAME
 *                       SNMP_ERR_INCONSISTENT_VALUE
 *                       SNMP_ERR_NO_CREATION
 * Globals     : Not Referred of Modified
 * Returns     : SNMP_SUCCESS - If the Indices are Valid and if the Entry exists
 *                              for the Table
 *               SNMP_FAILURE - Otherwise               
 ******************************************************************************/

PUBLIC INT1
IsisNmhValSysTable (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered IsisNmhValSysTable ()\n"));

    i1RetCode = nmhValidateIndexInstanceIsisSysTable (i4IsisSysInstance);

    if (i1RetCode == SNMP_SUCCESS)
    {
        i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

        if (i4RetVal == ISIS_FAILURE)
        {
            i1RetCode = SNMP_FAILURE;
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }
    }
    else
    {
        i1RetCode = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting IsisNmhValSysTable () \n"));

    return i1RetCode;
}

/******************************************************************************
 * Function    : IsisNmhValSATable () 
 * Description : This function 
 * Input(s)    : i4IsisSysInstance - Instance Index
 *               i4IsisSummAddressType - Type of the Summary Address
 *               i4IsisSummA
 * Outputs     : pu4ErrorCode      - Returns one of the following error
 *                       SNMP_ERR_INCONSISTENT_NAME
 *                       SNMP_ERR_INCONSISTENT_VALUE
 *                       SNMP_ERR_NO_CREATION
 * Globals     : Not Referred of Modified
 * Returns     : SNMP_SUCCESS - If the Indices are Valid and if the Entry exists
 *                              for the Table
 *               SNMP_FAILURE - Otherwise               
 ******************************************************************************/

PUBLIC INT1
IsisNmhValSATable (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                   INT4 i4IsisSummAddressType,
                   tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                   INT4 i4IsisSummAddrPrefixLen, UINT1 u1ExistFlag)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered IsisNmhValSATable ()\n"));

    i1RetCode =
        nmhValidateIndexInstanceIsisSummAddrTable (i4IsisSysInstance,
                                                   i4IsisSummAddressType,
                                                   pIsisSummAddress,
                                                   i4IsisSummAddrPrefixLen);
    if (i1RetCode == SNMP_SUCCESS)
    {
        i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

        if (i4RetVal == ISIS_FAILURE)
        {
            i1RetCode = SNMP_FAILURE;
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }
        else if (u1ExistFlag == ISIS_FALSE)
        {
            i4RetVal =
                IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                               (UINT1) i4IsisSummAddressType,
                               (UINT1) i4IsisSummAddrPrefixLen, &pSAEntry);
            if (i4RetVal == ISIS_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetCode = SNMP_FAILURE;
            }
        }
    }
    else
    {
        i1RetCode = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting IsisNmhValSATable () \n"));

    return i1RetCode;
}

/******************************************************************************
 * Function    : IsisNmhValCircTable () 
 * Description : This function 
 * Input(s)    : i4IsisSysInstance - Instance Index
 * Outputs     : pu4ErrorCode      - Returns one of the following error
 *                       SNMP_ERR_INCONSISTENT_NAME
 *                       SNMP_ERR_INCONSISTENT_VALUE
 *                       SNMP_ERR_NO_CREATION
 * Globals     : Not Referred of Modified
 * Returns     : SNMP_SUCCESS - If the Indices are Valid and if the Entry exists
 *                              for the Table
 *               SNMP_FAILURE - Otherwise               
 ******************************************************************************/

PUBLIC INT1
IsisNmhValCircTable (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                     INT4 i4IsisCircIndex, UINT1 u1ExistFlag,
                     UINT1 u1ActiveFlag)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered IsisNmhValCircTable ()\n"));

    i1RetCode = nmhValidateIndexInstanceIsisCircTable (i4IsisSysInstance,
                                                       i4IsisCircIndex);
    if (i1RetCode == SNMP_SUCCESS)
    {
        i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

        if (i4RetVal == ISIS_FAILURE)
        {
            i1RetCode = SNMP_FAILURE;
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }
        else if (u1ExistFlag == ISIS_FALSE)
        {
            i4RetVal =
                IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                  &pCktEntry);
            if (i4RetVal == ISIS_FAILURE)
            {
                i1RetCode = SNMP_FAILURE;
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            }
            else if (u1ActiveFlag == ISIS_TRUE)
            {
                if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
                {
                    i1RetCode = SNMP_FAILURE;
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                }
            }
        }
    }
    else
    {
        i1RetCode = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting IsisNmhValCircTable () \n"));

    return i1RetCode;
}

/******************************************************************************
 * Function    : IsisNmhValCircLevelTable () 
 * Description : This function 
 * Input(s)    : i4IsisSysInstance - Instance Index
 * Outputs     : pu4ErrorCode      - Returns one of the following error
 *                       SNMP_ERR_INCONSISTENT_NAME
 *                       SNMP_ERR_INCONSISTENT_VALUE
 *                       SNMP_ERR_NO_CREATION
 * Globals     : Not Referred of Modified
 * Returns     : SNMP_SUCCESS - If the Indices are Valid and if the Entry exists
 *                              for the Table
 *               SNMP_FAILURE - Otherwise               
 ******************************************************************************/

PUBLIC INT1
IsisNmhValCircLevelTable (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                          INT4 i4IsisCircIndex, INT4 i4IsisCircLevelIndex)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisCktLevel      *pCktLvlEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered IsisNmhValCircLevelTable ()\n"));

    i1RetCode =
        nmhValidateIndexInstanceIsisCircLevelTable (i4IsisSysInstance,
                                                    i4IsisCircIndex,
                                                    i4IsisCircLevelIndex);
    if (i1RetCode == SNMP_SUCCESS)
    {
        i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

        if (i4RetVal == ISIS_FAILURE)
        {
            i1RetCode = SNMP_FAILURE;
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }
        else
        {
            i4RetVal =
                IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex,
                                  &pCktEntry);
            if (i4RetVal == ISIS_FAILURE)
            {
                i1RetCode = SNMP_FAILURE;
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            }
            else
            {
                if (i4IsisCircLevelIndex == ISIS_LEVEL1)
                {
                    pCktLvlEntry = pCktEntry->pL1CktInfo;
                }
                else if (i4IsisCircLevelIndex == ISIS_LEVEL2)
                {
                    pCktLvlEntry = pCktEntry->pL2CktInfo;
                }
                if (pCktLvlEntry == NULL)
                {
                    i1RetCode = SNMP_FAILURE;
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does "
                             "not exist for the Index,  %d\n",
                             i4IsisCircLevelIndex));
                }
            }
        }
    }
    else
    {
        i1RetCode = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting IsisNmhValCircLevelTable () \n"));

    return i1RetCode;
}

/******************************************************************************
 * Function    : IsisNmhValIPRATable () 
 * Description : This function Validates the Indices during nmhTest call.
 * Input(s)    : i4IsisSysInstance - Instance Index
 * Outputs     : pu4ErrorCode      - Returns one of the following error
 *                       SNMP_ERR_INCONSISTENT_NAME
 *                       SNMP_ERR_INCONSISTENT_VALUE
 *                       SNMP_ERR_NO_CREATION
 * Globals     : Not Referred of Modified
 * Returns     : SNMP_SUCCESS - If the Indices are Valid and if the Entry exists
 *                              for the Table
 *               SNMP_FAILURE - Otherwise               
 ******************************************************************************/

PUBLIC INT1
IsisNmhValIPRATable (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                     INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                     UINT1 u1ExistFlag)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered IsisNmhValIPRATable ()\n"));

    i1RetCode = nmhValidateIndexInstanceIsisIPRATable (i4IsisSysInstance,
                                                       i4IsisIPRAType,
                                                       i4IsisIPRAIndex);
    if (i1RetCode == SNMP_SUCCESS)
    {
        if (i4IsisIPRAType != ISIS_MANUAL_TYPE)
        {
            /* This Manager should not be able to change the IPRA Entries
             * Learnt through RRD. Hence if the IPRA Type is not manual
             * return failure.
             */

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            NMP_EE ((ISIS_LGST, "NMP <X> : Exiting IsisNmhValIPRATable () \n"));
            return SNMP_FAILURE;
        }

        i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

        if (i4RetVal == ISIS_FAILURE)
        {
            i1RetCode = SNMP_FAILURE;
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        }
        else
        {
            /* The following check is to validate all the objects of IPRA table
             * except ExistState. If u1ExistFlag is ISIS_TRUE, then IPRA
             * Record is not fetched and SNMP_SUCCESS is returned 
             */

            if (u1ExistFlag == ISIS_FALSE)
            {
                i4RetVal =
                    IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                        (UINT1) i4IsisIPRAType, &pIPRARec);
                if (i4RetVal == ISIS_FAILURE)
                {
                    i1RetCode = SNMP_FAILURE;
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                }
            }
        }
    }
    else
    {
        i1RetCode = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting IsisNmhValIPRATable () \n"));

    return i1RetCode;
}

/******************************************************************************
 * Function    : nmhUtlGetNextIndexIsisSysStatLevel ()
 * Description : The routine gets the next index of System Stats table having 
 *               the same instance index.
 * Input       : u4IsisSysInstance       - The Index to the system instance
 *               u4IsisSysStatLevel      - The current Stats Level
 * Output      : pu4NextIsisSysStatLevel - Pointer to the next Stats Level
 * Globals     : None
 * Returns     : ISIS_SUCCESS - if next entry is fetched successfully
 *               ISIS_FAILURE - if next entry cannot be fetched
******************************************************************************/

PUBLIC INT4
nmhUtlGetNextIndexIsisSysStatLevel (UINT4 u4IsisSysInstance,
                                    UINT4 u4IsisSysStatLevel,
                                    UINT4 *pu4NextIsisSysStatLevel)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT4                i4RetCode = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhUtlGetNextIndexIsisSysStatLevel () \n"));

    /* setting the return value pu4IsisCircIndex to Maximum value 
     */

    i4RetCode = IsisCtrlGetSysContext (u4IsisSysInstance, &pContext);

    if ((i4RetCode == ISIS_SUCCESS) && (pContext != NULL))
    {
        switch (pContext->SysActuals.u1SysType)
        {
            case ISIS_LEVEL1:
                if (u4IsisSysStatLevel == 0)
                {
                    *pu4NextIsisSysStatLevel = ISIS_LEVEL1;
                }
                else
                {
                    i4RetVal = ISIS_FAILURE;
                }
                break;
            case ISIS_LEVEL2:
                if (u4IsisSysStatLevel == 0)
                {
                    *pu4NextIsisSysStatLevel = ISIS_LEVEL2;
                }
                else
                {
                    i4RetVal = ISIS_FAILURE;
                }
                break;
            case ISIS_LEVEL12:
                if (u4IsisSysStatLevel == 0)
                {
                    *pu4NextIsisSysStatLevel = ISIS_LEVEL1;
                }
                else if (u4IsisSysStatLevel == ISIS_LEVEL1)
                {
                    *pu4NextIsisSysStatLevel = ISIS_LEVEL2;
                }
                else if (u4IsisSysStatLevel == ISIS_LEVEL2)
                {
                    i4RetVal = ISIS_FAILURE;
                }
                break;
            default:
                i4RetVal = ISIS_FAILURE;

        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhUtlGetNextIndexIsisSysStatLevel ()\n"));

    return i4RetVal;
}
