 /*****************************************************************************
  * 
  *  Copyright (C) Future Software Limited,  2001
  *
  *  $Id: isgenutl.c,v 1.41 2017/09/18 12:26:13 siva Exp $
  *
  *  Description: This file contains the general utility routines.
  *
  *****************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE INT4        IsisUtlVerifyPasswd (tIsisPasswd *, tIsisPasswd *,
                                         UINT1 *, UINT1);

/******************************************************************************
 * Function    : IsisUtlSetBPat ()
 * Description : This function sets the bit corresponding to the given Index
 *               in the bit pattern
 * Input(s)    : pu1Info - Address of the Pointer which points to the Bit 
 *                         Pattern
 *               u4Id    - Index whose corresponsing bit is supposed to be set
 *                         in 'pu1Info'.
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUtlSetBPat (UINT1 **pu1Info, UINT4 u4Id)
{
    UINT1               u1Mask = 0;
    UINT1              *pu1OldInfo = NULL;
    UINT1              *pu1NewInfo = NULL;
    UINT4               u4Pos = 0;
    UINT4               u4InfoLen = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlSetBPat ()\n"));

    /* Algorithm:
     * 'pu1Info' is an Octet String whose bits represent a particular index i.e.
     * bit '1' represent index '1' and so on. This routine is used for setting
     * bits for Circuits, Directions. Adjacencies etc. This means that each of
     * the circuit/direction/adjacency will have its corresponding bit set in 
     * the 'pu1Info' maintained for the circuit/direction/adjacency 
     * respectively. In order to avoid allocating maximum memory for 'pu1Info' 
     * only required memory will be allocated each time, based on the bits that
     * are currently set. For e.g., if circuits 1 and 3 are active then 
     * 'pu1Info' will have just 1 byte since bit '0' and bit '2' can represent 
     * these indices. If directions 5 and 26 are active, then 'pu1Info' must 
     * have 4 bytes i.e. (CEIL (26/8) bits) to represent these indices. This
     * means that if 'pu1Info' does not have enough memory, then extra memory
     * must be allocated based on the new requirement, contents of the existing
     * memory copied to the new memory, and the existig memory freed. That's the
     * reason 'pu1Info' is passed as a pointer-to-pointer.
     *
     * Example:
     *
     * Length of pu1Info = 2, bits 3 and 12 are set.
     *
     * Request for bit 26 to be set
     *
     * Expected Length = 4, which is more than existing length which is 2. 
     *
     * Allocate 4 bytes, say pNewInfo. Copy pu1Info to pNewInfo. Free pu1Info.
     * Update pu1Info to pNewInfo. Set bit 26 in pu1Info. 
     */

    /* Forming the Bit Mask Appropriately, for eg: if the
     * third bit is to be set, then Mask = 0010 000
     */

    /* NOTE: The first 4 bytes of pu1Info contains the length of the Mask.
     * Extract the Mask Length first
     */

    if (*pu1Info != NULL)
    {
        ISIS_GET_4_BYTES (*pu1Info, 0, u4InfoLen);
        ISIS_DBG_PRINT_ID (*pu1Info, (UINT1) u4InfoLen,
                           "UTL <T> : Current Mask\t", ISIS_OCTET_STRING);
    }

    ISIS_BIT_MASK (u4Id, u1Mask);

    /* Now finding the appropriate byte in the bit pattern
     * (Consider Also the Length of Mask which is Included in the
     *  Mask)
     */

    u4Pos = ISIS_BYTE_POS (u4Id) + 4;    /* 4 Bytes for Length of the Mask */

    if (*pu1Info == NULL)
    {
        /* No memory has been allocated for pu1Info till this point. allocate
         * memory for holding the required bit and also the Length of the mask -
         * which is gievn by u4Pos
         */

        pu1NewInfo = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_ECTI, (u4Pos + 1));
        if (pu1NewInfo == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Timer Info\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlSetBPat ()\n"));
            return;
        }
        ISIS_ASSIGN_4_BYTES (pu1NewInfo, 0, (u4Pos + 1));
        *pu1Info = pu1NewInfo;
    }
    else if (u4InfoLen < (u4Pos + 1))
    {
        /* Current Mask Length is less than the required length. Allocate memory
         * as per the new requirement, copy the old contents, free the old
         * memory
         */

        pu1OldInfo = *pu1Info;
        pu1NewInfo = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_ECTI, (u4Pos + 1));
        if (pu1NewInfo == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Timer Info\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlSetBPat ()\n"));
            return;
        }

        /* Ignore the first 4 bytes, it will get updated anyway
         */

        MEMCPY (pu1NewInfo + 4, pu1OldInfo + 4, u4InfoLen - 4);
        ISIS_MEM_FREE (ISIS_BUF_ECTI, pu1OldInfo);

        /* Update the New Length 
         */

        ISIS_ASSIGN_4_BYTES (pu1NewInfo, 0, (u4Pos + 1));
        *pu1Info = pu1NewInfo;
    }
    else
    {
        /* The current length is sufficient - just proceed with setting the bit
         * appropriately
         */

        pu1NewInfo = *pu1Info;
    }

    *(pu1NewInfo + u4Pos) |= u1Mask;

    ISIS_DBG_PRINT_ID (*pu1Info, (UINT1) (u4Pos + 1),
                       "UTL <T> : Updated Mask\t", ISIS_OCTET_STRING);
    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlSetBPat ()\n"));
}

/******************************************************************************
 * Function    : IsisUtlReSetBPat ()
 * Description : This function Resets a bit corresponding to the given Index
 *               'u4Id' in the bit pattern 'pu1Info'
 * Input(s)    : pu1Info - Address of the Pointer which points to the Bit 
 *                         Pattern
 *               u4Id    - Index whose corresponsing bit is supposed to be set
 *                         in 'pu1Info'.
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PUBLIC UINT1
IsisUtlReSetBPat (UINT1 **pu1Info, UINT4 u4Id)
{
    UINT1               u1DelFlag = ISIS_TRUE;
    UINT1               u1Mask = 0;
    UINT1              *pu1NewMask = NULL;
    UINT1              *pu1BitFlag = NULL;
    UINT4               u4Pos = 0;
    UINT4               u4MaskLen = 0;
    UINT4               u4ByteCnt = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlReSetBPat ()\n"));

    if (*pu1Info == NULL)
    {
        UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlReSetBPat ()\n"));
        return ISIS_FALSE;
    }

    ISIS_GET_4_BYTES (*pu1Info, 0, u4MaskLen);

    ISIS_DBG_PRINT_ID (*pu1Info, (UINT1) u4MaskLen, "UTL <T> : Current Mask\t",
                       ISIS_OCTET_STRING);

    ISIS_BIT_MASK (u4Id, u1Mask);

    /* First 4 bytes of Mask should hold the length of the Mask
     */

    u4Pos = ISIS_BYTE_POS (u4Id) + 4;

    if (u4Pos > (u4MaskLen - 1))
    {
        return ISIS_FALSE;
    }

    pu1BitFlag = (*pu1Info) + u4Pos;
    *pu1BitFlag &= ~u1Mask;

    ISIS_DBG_PRINT_ID (*pu1Info, (UINT1) u4MaskLen, "UTL <T> : New Mask\t",
                       ISIS_OCTET_STRING);

    pu1BitFlag = (*pu1Info) + 4;

    for (u4ByteCnt = 0; u4ByteCnt < (u4MaskLen - 4); u4ByteCnt++)
    {
        if (*(pu1BitFlag + u4ByteCnt) != 0)
        {
            u1DelFlag = ISIS_FALSE;
            break;
        }
    }

    if (u1DelFlag == ISIS_TRUE)
    {
        ISIS_MEM_FREE (ISIS_BUF_ECTI, *pu1Info);
        *pu1Info = NULL;
    }
    else
    {
        /* Try to resize the mask. If the last byte has become ZERO due to 
         * restting of a bit, we can resize the current mask to include 
         * only the minimum number of bytes that are required to hold
         * the current mask.
         */

        pu1BitFlag = *pu1Info;

        for (u4ByteCnt = (u4MaskLen - 1); u4ByteCnt > 4; u4ByteCnt--)
        {
            if (*(pu1BitFlag + u4ByteCnt) != 0)
            {
                break;
            }
        }

        if (u4ByteCnt != (u4MaskLen - 1))
        {
            /* We can compress the Mask
             */

            pu1NewMask = ISIS_MEM_ALLOC (ISIS_BUF_ECTI, (u4ByteCnt + 1));
            if (pu1NewMask == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Timer Info\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlReSetBPat ()\n"));
                return ISIS_MEM_FAILURE;
            }
            MEMCPY (pu1NewMask + 4, *pu1Info + 4, (u4ByteCnt - 3));
            ISIS_ASSIGN_4_BYTES (pu1NewMask, 0, (u4ByteCnt + 1));
            ISIS_MEM_FREE (ISIS_BUF_ECTI, *pu1Info);
            *pu1Info = pu1NewMask;

        }
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlReSetBPat ()\n"));
    return u1DelFlag;
}

/******************************************************************************
 * Function    : IsisUtlGetNextIdFromBPat ()
 * Description : This routine extracts non-zero bytes from pu1BitPat, and
 *               retrieves the right most bit that is set in the extracted byte.
 *               It then computes the value of the index corresponding to the
 *               extracted bit. It finally resets the bit that has already been
 *               handled
 * Input(s)    : pu1BitPat  - Address of the Pointer to the Bit Pattern. The Bit
 *                            pattern itself will be freed if all the bits are
 *                            reset and hence a double pointer
 * Output(s)   : pu4Id      - Index value represented by the extracted bit
 * Globals     : gau1IdMask [] Referred but Not Modified
 * Return(s)   : VOID
 ******************************************************************************/

PUBLIC INT4
IsisUtlGetNextIdFromBPat (UINT1 **pu1BitPat, UINT4 *pu4Id)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Byte = 0;
    UINT1               u1DelFlag = ISIS_FALSE;
    UINT4               u4BPatLen = 0;
    UINT4               u4ByteCnt = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlGetNextIdFromBPat ()\n"));

    if (*pu1BitPat == NULL)
    {
        UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetNextIdFromBPat ()\n"));
        return (i4RetVal);
    }

    ISIS_GET_4_BYTES (*pu1BitPat, 0, u4BPatLen);

    ISIS_DBG_PRINT_ID (*pu1BitPat, (UINT1) u4BPatLen,
                       "UTL <T> : Bit Pattern Before Extraction\t",
                       ISIS_OCTET_STRING);

    for (u4ByteCnt = 4; u4ByteCnt < u4BPatLen; u4ByteCnt++)
    {
        if (*(*pu1BitPat + u4ByteCnt) == 0)
        {
            continue;
        }

        /* Taking the first non zero byte in the bit pattern
         */

        u1Byte = *(*pu1BitPat + u4ByteCnt);

        /* Following statement computes the valueof the index corresponding to
         * the rightmost bit that is set in the extracted byte. The logic is as
         * follows:
         *
         * u1Byte & (u1Byte -1) will always reset the rightmost bit in u1Byte.
         * Let us call this value 'X'.
         * u1Byte - X is a value which is the value of the bit position that has
         * been reset. This means if bit 'i' is reset then the value u1Byte - X
         * is nothing but 2 POWER i. For e.g., 
         * consider 0000 1000(8) & 0000 0111(7 = (8 - 1)), the resulting value 
         * is '0', and hence 8 - 0 = 8 which is 2 POWER 3 which indicates that
         * bit 3 is reset. This means that the value of circuit index will be 8
         * - 3 which is '5' (Note that in this example the bit we have reset is
         * '5' counting from left. Actual value of the circuit index depends on
         * the byte number where we have found the required bit. If the above
         * compited value is found in Byte number '2', then we have to add '16'
         * to the computed value since we are counting from left which gives an
         * index of '21'.
         */

        *pu4Id = (8 - ISIS_BIT_VALUE (((u1Byte - (u1Byte & (u1Byte - 1))))));

        /* Multiply by Byte position, which is u4ByteCnt, to get the correct
         * index
         */

        *pu4Id += ((u4ByteCnt - 4) * 8);    /* Excluding 4 bytes of length field */

        u1DelFlag = IsisUtlReSetBPat (pu1BitPat, *pu4Id);

        if (u1DelFlag == ISIS_MEM_FAILURE)
        {
            return ISIS_FAILURE;
        }

        if (u1DelFlag != ISIS_TRUE)
        {
            ISIS_DBG_PRINT_ID (*pu1BitPat, (UINT1) u4BPatLen,
                               "UTL <T> : Bit Pattern After Extraction\t",
                               ISIS_OCTET_STRING);
        }
        i4RetVal = ISIS_SUCCESS;
        break;
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetNextIdFromBPat ()\n"));
    return (i4RetVal);
}

/*****************************************************************************
 * Function     : IsisUtlCalcChkSum ()
 * Description  : This function calculates the checksum for the given PDU
 * Input (s)    : pu1Pdu   - Pointer to the Sequence number octet in the PDU
 *                u2Len    - Length of the PDU for which Checksum needs to be 
 *                           calculated
 *                pu1CkSum - Pointer to the Checksum octet in the PDU
 * Output (s)   : None
 * Globals      : Not Referred or Modified
 * RETURN (s)   : Calculated Checksum value
 ******************************************************************************/

PUBLIC UINT2
IsisUtlCalcChkSum (UINT1 *pu1Pdu, UINT2 u2Len, UINT1 *pu1CkSum)
{
    register INT4       i4c0;
    register INT4       i4c1;

    register UINT1     *pu1Cp;
    register INT4       i4Len1;
    register INT4       i4Len2;

    i4c0 = i4c1 = 0;
    pu1Cp = pu1Pdu;
    i4Len2 = u2Len;

    /*
     * Initialize checksum to zero if there is one
     */

    if (pu1CkSum)
    {
        *pu1CkSum = *(pu1CkSum + 1) = 0;
    }

    /*
     * Process enough of the packet to make the remaining length an even
     * multiple of ISIS_MAXITER (4096).  The switch() adds a lot of code, but
     * trying to do this with less results in a big slowdown for short packets.
     */

    i4Len1 = i4Len2 & ISIS_ITERMASK;

    switch (i4Len1 & ISIS_INLINEMASK)
    {
        case 7:
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            break;

        case 6:
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            break;

        case 5:
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            break;

        case 4:
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            break;

        case 3:
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            break;

        case 2:
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            break;

        case 1:
            i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            break;
        default:
            break;
    }

    i4Len1 >>= ISIS_INSHIFT;
    while (i4Len1-- > 0)
    {
        i4c1 += (i4c0 += (INT4) (*pu1Cp++));
        i4c1 += (i4c0 += (INT4) (*pu1Cp++));
        i4c1 += (i4c0 += (INT4) (*pu1Cp++));
        i4c1 += (i4c0 += (INT4) (*pu1Cp++));
        i4c1 += (i4c0 += (INT4) (*pu1Cp++));
        i4c1 += (i4c0 += (INT4) (*pu1Cp++));
        i4c1 += (i4c0 += (INT4) (*pu1Cp++));
        i4c1 += (i4c0 += (INT4) (*pu1Cp++));
    }

    /*
     * Now process the remainder in ISIS_MAXITER chunks, with a mod beforehand 
     * to avoid overflow.
     */
    /*i4Len1 = i4Len1 >> ISIS_ITERSHIFT; */

    i4Len1 = i4Len2 >> ISIS_ITERSHIFT;

    if (i4Len1 > 0)
    {
        do
        {
            register int        iter = ISIS_MAXINLINE;

            if (pu1Cp != (UINT1 *) pu1CkSum)
            {
                i4c0 %= ISIS_MODULUS;
                i4c1 %= ISIS_MODULUS;
            }
            do
            {
                i4c1 += (i4c0 += (INT4) (*pu1Cp++));
                i4c1 += (i4c0 += (INT4) (*pu1Cp++));
                i4c1 += (i4c0 += (INT4) (*pu1Cp++));
                i4c1 += (i4c0 += (INT4) (*pu1Cp++));
                i4c1 += (i4c0 += (INT4) (*pu1Cp++));
                i4c1 += (i4c0 += (INT4) (*pu1Cp++));
                i4c1 += (i4c0 += (INT4) (*pu1Cp++));
                i4c1 += (i4c0 += (INT4) (*pu1Cp++));
            }
            while (--iter > 0);
        }
        while (--i4Len1 > 0);
    }

    /*
     * Take the modulus of c0 now to avoid overflow during the 
     * multiplication below.  If we're computing a checksum for the
     * packet, do it and insert it.
     */
    i4c0 %= ISIS_MODULUS;

    if (pu1CkSum)
    {
        /*
         * c1 used for Y.  Can't overflow since we're taking the difference 
         * between two positive numbers
         */
        i4c1 =
            (i4c1 -
             ((INT4) (((UINT1 *) pu1Pdu + i4Len2) - pu1CkSum) * i4c0)) %
            ISIS_MODULUS;

        if (i4c1 <= 0)
        {
            i4c1 += ISIS_MODULUS;
        }

        /*
         * Here we know c0 has a value in the range 0-254, and c1 has a value
         * in the range 1-255.  If we subtract the sum
         * from 255 we end up with something in the range -254-254, and only 
         * need correct the -ve value.
         */
        i4c0 = ISIS_MODULUS - i4c1 - i4c0;    /* c0 used for X */

        if (i4c0 <= 0)
        {
            i4c0 += ISIS_MODULUS;
        }
        *pu1CkSum++ = (UINT1) i4c0;
        *pu1CkSum = (UINT1) i4c1;

        return (UINT2) ((i4c0 << 8) | i4c1);
    }

    /*
     * Here we were just doing a check.  Return the results in a single
     * value, they should both be zero.
     */
    return (UINT2) (((i4c1 % ISIS_MODULUS) << 8) | i4c0);
}

/******************************************************************************
 * Function    : IsisUtlGetIPMask ()
 * Description : This function computes the IP Address Mask from the given
 *               prefix length
 * Input (s)   : u1Len     - Prefix Length
 * Output (s)  : pu1IPMask - Pointer to the IP Address Mask computed from the
 *                           given prefix length
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUtlGetIPMask (UINT1 u1Len, UINT1 *pu1IPMask)
{
    UINT1               u1ByteCnt = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlGetIPMask ()\n"));

    MEMSET (pu1IPMask, 0x00, ISIS_MAX_IP_ADDR_LEN);

    u1ByteCnt = (u1Len / 8);
    MEMSET (pu1IPMask, 0xff, u1ByteCnt);
    u1Len %= 8;

    if (u1Len != 0)
    {
        *(pu1IPMask + u1ByteCnt) = (UINT1) (0xff << (8 - u1Len));
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetIPMask ()\n"));
}

/******************************************************************************
 * Function    : IsisUtlComputePrefixLen ()
 * Description : This function computes the Prefix Length from the given IP 
 *               Address Mask
 * Input (s)   : pu1IPMask  - Pointer to the IP Mask
 *               u1Len      - Length of the IP Mask
 * Output (s)  : pu1PrefLen - Prefix length computed from the mask
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC INT4
IsisUtlComputePrefixLen (UINT1 *pu1IPMask, UINT1 u1Len, UINT1 *pu1PrefLen)
{
    INT4                i4Len = -1;
    UINT1               u1Cnt = 0;

    UTP_EE ((ISIS_LGST, "UTL <E> : Entered IsisUtlComputePrefixLen ()\n"));

    *pu1PrefLen = 0;

    for (u1Cnt = 0; u1Cnt < u1Len; u1Cnt++)
    {
        /* Retrieve the number of bits set from the given mask value
         */

        i4Len = ISIS_GET_PREFIX_LEN ((*(pu1IPMask + u1Cnt)));
        if (i4Len == -1)
        {
            ISIS_DBG_PRINT_ADDR (pu1IPMask, (UINT1) ISIS_MAX_IP_ADDR_LEN,
                                 "UTL <T> : Invalid Mask\t", ISIS_OCTET_STRING);
            *pu1PrefLen = 0;
            UTP_EE ((ISIS_LGST,
                     "UTL <E> : Exiting IsisUtlComputePrefixLen ()\n"));
            return (ISIS_FAILURE);
        }
        else
        {
            if (i4Len != 0)
            {
                *pu1PrefLen = (UINT1) (*pu1PrefLen + i4Len);
            }
            else
            {
                break;
            }
        }
    }

    UTP_EE ((ISIS_LGST, "UTL <E> : Exiting IsisUtlComputePrefixLen ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function     : IsisUtlSendEvent ()
 * Description  : This function enqueues the Event 'pEvt' to the 
 *                Control Module for further processing.
 * Input (s)    : pContext - Pointer to System Context
 *                pEvt     - Pointer to Event
 *                u1Len    - Length of Event
 * Output (s)   : None 
 * Globals      : Not  Referred or Modified
 * Returns      : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUtlSendEvent (tIsisSysContext * pContext, UINT1 *pEvt, UINT1 u1Len)
{
    tIsisMsg            IsisMsg;

    UTP_EE ((ISIS_LGST, "UTL <E> : Entered IsisUtlSendEvent ()\n"));

    if (pContext != NULL)
    {                            /*Incase of Reset all EVENT, triggered by testsuite at the beggining */
        IsisMsg.u4CxtOrIfindex = pContext->u4SysInstIdx;
    }
    else
    {
        IsisMsg.u4CxtOrIfindex = 0;
    }
    IsisMsg.u4Length = (UINT4) u1Len;
    IsisMsg.pu1Msg = pEvt;
    IsisMsg.u1MsgType = ISIS_MSG_EVENT;

    if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                            gIsisCtrlQId, ISIS_SYS_INT_EVT) == ISIS_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_EVTS, pEvt);
        return;
    }

    UTP_EE ((ISIS_LGST, "UTL <E> : Exiting IsisUtlSendEvent ()\n"));
    return;
}

/******************************************************************************
 * Function     : IsisUtlFillComHdr ()
 * Description  : This function encodes the fixed common header into the given
 *                buffer 'puPDU'
 * Input (s)    : pContext - Pointer to the system context
 *                pu1PDU   - Pointer to the PDU 
 *                uPDUType - Type of the PDU 
 * Output (s)   : pu1PDU   - PDU updated with the common header
 * Globals      : Not Referred or Modified
 * Return (s)   : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUtlFillComHdr (tIsisSysContext * pContext, UINT1 *pu1PDU, UINT1 u1PDUType)
{
    tIsisComHdr        *pComHdr = NULL;

    UTP_EE ((ISIS_LGST, "UTL <E> : Entered IsisUtlFillComHdr ()\n"));

    pComHdr = (tIsisComHdr *) pu1PDU;

    pComHdr->u1IDRPDisc = ISIS_IDRP_DISC;
    pComHdr->u1HdrLen = (UINT1) ISIS_GET_PDU_HDR_LEN (u1PDUType);
    pComHdr->u1VerProtoId = ISIS_PROT_ID_EXT;
    pComHdr->u1IdLen = ISIS_SYS_ID_LEN;
    pComHdr->u1PDUType = u1PDUType;
    pComHdr->u1Version = ISIS_VERSION;
    pComHdr->u1Reserved = 0;
    pComHdr->u1MaxAreaAddr = pContext->SysActuals.u1SysMaxAA;

    UTP_EE ((ISIS_LGST, "UTL <E> : Exiting IsisUtlFillComHdr ()\n"));
}

/*****************************************************************************
 * Function    : IsisUtlAuthenticatePDU ()
 * Description : This function Authenticates the Received PDUs. If the received
 *               PDU is a HELLO, the authentication is performed based on the
 *               information included in the Circuits. Otherwise the
 *               authentication is performed based on the information included
 *               in the System Context.
 * Input(s)    : pContext   - Pointer to the system context
 *               pCktRec    - Pointer to circuit record
 *               pu1RcvdPDU - Pointer to the received PDU
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS - If Authentication of the PDU is sucess
 *               ISIS_FAILURE - Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisUtlAuthenticatePDU (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                        UINT1 *pu1RcvdPDU)
{
    INT4                i4AuthStat = ISIS_FAILURE;
    UINT1               u1Len = 0;
    UINT1               u1PduType = 0;
    UINT1               u1Level = 0;
    UINT1               u1Code = 0;
    UINT1               u1Authenticate = FALSE;
    UINT1               u1AuthType = ISIS_AUTH_PASSWD;
    UINT1               u1RxAuthType = ISIS_AUTH_PASSWD;
    UINT1               u1Cause = ISIS_AUTH_MISSING;
    UINT2               u2PDULen = 0;
    UINT2               u2Offset = 0;
    UINT2               u2TrapPduLen = 0;
    UINT1               au1RcvDigest[16];
    UINT1               au1CalcDigest[16];
    UINT2               u2Rlt = 0;
    UINT2               u2Cs = 0;
    unUtilAlgo          UtilAlgo;
    tIsisPasswd        *pTxPasswd = NULL;
    tIsisPasswd        *pRxPasswd = NULL;
    tIsisEvtAuthFail   *pAuthFailEvt = NULL;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlAuthenticatePDU ()\n"));

    MEMSET (au1CalcDigest, 0, sizeof (au1CalcDigest));
    MEMSET (au1RcvDigest, 0, sizeof (au1RcvDigest));
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    u2Offset = (UINT2) ((tIsisComHdr *) pu1RcvdPDU)->u1HdrLen;

    if ((((tIsisComHdr *) pu1RcvdPDU)->u1PDUType == ISIS_L1LAN_IIH_PDU)
        || (((tIsisComHdr *) pu1RcvdPDU)->u1PDUType == ISIS_L2LAN_IIH_PDU)
        || (((tIsisComHdr *) pu1RcvdPDU)->u1PDUType == ISIS_P2P_IIH_PDU))
    {
        /* Extracting the PDU Length in the case of HELLOs
         */

        ISIS_GET_2_BYTES (pu1RcvdPDU,
                          sizeof (tIsisComHdr) + ISIS_SYS_ID_LEN + 3, u2PDULen);
    }
    else
    {
        /* Extracting PDU Length in the case of LSPs, CSNPs, PSNPs
         */

        ISIS_GET_2_BYTES (pu1RcvdPDU, sizeof (tIsisComHdr), u2PDULen);
    }

    u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1RcvdPDU);
    u1PduType &= ISIS_PDU_TYPE_MASK;

    switch (u1PduType)
    {
        case ISIS_L1LSP_PDU:
        case ISIS_L1PSNP_PDU:
        case ISIS_L1CSNP_PDU:

            u1Level = ISIS_LEVEL1;
            pTxPasswd = &pContext->SysActuals.SysAreaTxPasswd;
            pRxPasswd = pContext->SysActuals.aSysAreaRxPasswd;
            u1AuthType = pContext->SysActuals.u1SysAreaAuthType;
            if (u1PduType == ISIS_L1LSP_PDU)
            {
                ISIS_EXTRACT_RLT_FROM_LSP (pu1RcvdPDU, u2Rlt);
                ISIS_EXTRACT_CS_FROM_LSP (pu1RcvdPDU, u2Cs);
            }

            break;

        case ISIS_L2LSP_PDU:
        case ISIS_L2PSNP_PDU:
        case ISIS_L2CSNP_PDU:

            u1Level = ISIS_LEVEL2;
            pTxPasswd = &pContext->SysActuals.SysDomainTxPasswd;
            pRxPasswd = pContext->SysActuals.aSysDomainRxPasswd;
            u1AuthType = pContext->SysActuals.u1SysDomainAuthType;
            if (u1PduType == ISIS_L2LSP_PDU)
            {
                ISIS_EXTRACT_RLT_FROM_LSP (pu1RcvdPDU, u2Rlt);
                ISIS_EXTRACT_CS_FROM_LSP (pu1RcvdPDU, u2Cs);
            }

            break;

        case ISIS_P2P_IIH_PDU:
            if (pCktRec != NULL)    /*klocwork */
            {
                u1Level = (pCktRec->u1CktLevel == ISIS_LEVEL2) ? ISIS_LEVEL2
                    : ISIS_LEVEL1;
                pRxPasswd = (pCktRec->u1CktLevel == ISIS_LEVEL2)
                    ? pCktRec->pL2CktInfo->aCktRxPasswd
                    : pCktRec->pL1CktInfo->aCktRxPasswd;
                pTxPasswd = (pCktRec->u1CktLevel == ISIS_LEVEL2)
                    ? &pCktRec->pL2CktInfo->CktTxPasswd
                    : &pCktRec->pL1CktInfo->CktTxPasswd;
                u1AuthType = (pCktRec->u1CktLevel == ISIS_LEVEL2)
                    ? pCktRec->pL2CktInfo->u1CircLevelAuthType
                    : pCktRec->pL1CktInfo->u1CircLevelAuthType;

            }
            break;

        case ISIS_L1LAN_IIH_PDU:
            if (pCktRec != NULL)
            {
                u1Level = ISIS_LEVEL1;
                pRxPasswd = pCktRec->pL1CktInfo->aCktRxPasswd;
                pTxPasswd = &pCktRec->pL1CktInfo->CktTxPasswd;
                u1AuthType = pCktRec->pL1CktInfo->u1CircLevelAuthType;
            }
            break;

        case ISIS_L2LAN_IIH_PDU:
            if (pCktRec != NULL)    /*klocwork */
            {
                u1Level = ISIS_LEVEL2;
                pRxPasswd = pCktRec->pL2CktInfo->aCktRxPasswd;
                pTxPasswd = &pCktRec->pL2CktInfo->CktTxPasswd;
                u1AuthType = pCktRec->pL2CktInfo->u1CircLevelAuthType;
            }
            break;

        default:

            WARNING ((ISIS_LGST,
                      "UTL <E> : Unknown PDU to Authenticate [ %s ]\n",
                      ISIS_GET_PDU_TYPE_STR (u1PduType)));
            UTP_EE ((ISIS_LGST,
                     "UTL <X> : Exiting IsisUtlAuthenticatePDU ()\n"));
            return ISIS_FAILURE;
    }
    if (((pTxPasswd != NULL) && (pTxPasswd->u1Len != 0)) && (u1AuthType == 0))
    {
        UTP_PT ((ISIS_LGST,
                 "Authentication type is not set for configured password\n"));
    }
    else if ((u1AuthType != 0)
             && ((pTxPasswd == NULL) || (pTxPasswd->u1Len == 0)))
    {
        UTP_PT ((ISIS_LGST,
                 "MD5/clear text authentication password is not configured\n"));
    }

    if ((pTxPasswd != NULL) && (pTxPasswd->u1Len != 0) && (u1AuthType != 0))
    {
        u1Authenticate = TRUE;
    }

    if (u1Authenticate == FALSE)
    {
        return ISIS_SUCCESS;
    }

    while (u2Offset < u2PDULen)
    {
        ISIS_GET_1_BYTE (pu1RcvdPDU, u2Offset, u1Code);
        u2Offset++;

        ISIS_GET_1_BYTE (pu1RcvdPDU, u2Offset, u1Len);
        u2Offset++;

        switch (u1Code)
        {
            case ISIS_AUTH_INFO_TLV:

                ISIS_GET_1_BYTE (pu1RcvdPDU, u2Offset, u1RxAuthType);

                if (((u1AuthType == ISIS_AUTH_PASSWD)
                     && (u1RxAuthType == ISIS_MD5_PASS))
                    || ((u1AuthType == ISIS_AUTH_MD5)
                        && (u1RxAuthType == ISIS_CLR_TXT_PASS)))
                {
                    WARNING ((ISIS_LGST,
                              "AUTH <E> : Authentication failed for PDU [%s] : Auth type mismatch\n",
                              ISIS_GET_PDU_TYPE_STR (u1PduType)));

                    return ISIS_FAILURE;
                }

                if (u1RxAuthType == ISIS_AUTH_PASSWD)
                {
                    if (pTxPasswd != NULL)    /*Kloc Work */
                    {
                        i4AuthStat =
                            IsisUtlVerifyPasswd (pTxPasswd, pRxPasswd,
                                                 (pu1RcvdPDU + u2Offset + 1),
                                                 (UINT1) (u1Len - 1));
                        if (i4AuthStat == ISIS_FAILURE)
                        {
                            u1Cause = ISIS_PASSWD_MISMTCH;
                        }
                        /* If the authentication status is failure, then the 
                         * reason for failure in this case is only password mismatch
                         */
                    }
                }

                else if (u1RxAuthType == ISIS_MD5_PASS)
                {
                    if ((u1PduType == ISIS_L1LSP_PDU)
                        || (u1PduType == ISIS_L2LSP_PDU))
                    {
                        ISIS_ASSIGN_2_BYTES (pu1RcvdPDU, ISIS_OFFSET_CHKSUM, 0);
                        ISIS_ASSIGN_2_BYTES (pu1RcvdPDU, ISIS_OFFSET_RLT, 0);

                    }
                    MEMCPY (au1RcvDigest, (pu1RcvdPDU + u2Offset + 1),
                            ISIS_MD5_DIGEST_LEN);
                    MEMSET ((pu1RcvdPDU + u2Offset + 1), 0,
                            ISIS_MD5_DIGEST_LEN);

                    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr =
                        (unsigned char *) pu1RcvdPDU;
                    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = u2PDULen;
                    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pTxPasswd->au1Password;
                    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = pTxPasswd->u1Len;
                    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest =
                        (unsigned char *) &au1CalcDigest;

                    UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);

                    MEMCPY ((pu1RcvdPDU + u2Offset + 1), au1RcvDigest, 16);

                    if ((u1PduType == ISIS_L1LSP_PDU)
                        || (u1PduType == ISIS_L2LSP_PDU))
                    {
                        ISIS_ASSIGN_2_BYTES (pu1RcvdPDU, ISIS_OFFSET_CHKSUM,
                                             u2Cs);
                        ISIS_ASSIGN_2_BYTES (pu1RcvdPDU, ISIS_OFFSET_RLT,
                                             u2Rlt);

                    }
                    if (MEMCMP
                        (au1RcvDigest, au1CalcDigest, ISIS_MD5_DIGEST_LEN) == 0)
                    {
                        i4AuthStat = ISIS_SUCCESS;
                    }
                    else
                    {
                        i4AuthStat = ISIS_FAILURE;
                        u1Cause = ISIS_PASSWD_MISMTCH;
                    }
                }
                else
                {
                    /* Authentication type not supported
                     */

                    u1Cause = ISIS_AUTH_NOT_SUPP;

                    i4AuthStat = ISIS_FAILURE;
                }
                if (i4AuthStat == ISIS_SUCCESS)
                {
                    return ISIS_SUCCESS;
                }

                break;

            default:
                break;
        }
        u2Offset = (UINT2) (u2Offset + u1Len);
    }
    if (i4AuthStat != ISIS_SUCCESS)
    {
        ISIS_INCR_SYS_AUTH_FAILS (pContext, u1Level);

        if (u1Cause == ISIS_AUTH_NOT_SUPP)
        {
            WARNING ((ISIS_LGST,
                      "UTL <E> : Authentication Failure - Received Auth Type [ %u ] Not Supported\n",
                      u1RxAuthType));
            /* Increment the AuthType FailCount
             */
            if (u1Level == ISIS_LEVEL1)
            {
                pContext->SysL1Stats.u4SysAuthTypeFails++;
            }
            else
            {
                pContext->SysL2Stats.u4SysAuthTypeFails++;
            }
        }
        else if (u1Cause == ISIS_PASSWD_MISMTCH)
        {

            WARNING ((ISIS_LGST,
                      "UTL <E> : Authentication failure-Password mismatch\n"));
        }

        pAuthFailEvt = (tIsisEvtAuthFail *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtAuthFail));

        if (pAuthFailEvt != NULL)
        {

            u2TrapPduLen =
                (UINT2) (ISIS_MIN (ISIS_TRAP_PDU_FRAG_SIZE, u2PDULen));
            /* Allocate Memory for PDU Fragment 
             */
            pAuthFailEvt->pu1PduFrag = (UINT1 *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, ISIS_TRAP_PDU_FRAG_SIZE);
            if (pAuthFailEvt->pu1PduFrag != NULL)
            {
                pAuthFailEvt->u1EvtID = ISIS_EVT_AUTH_FAIL;
                pAuthFailEvt->u1Cause = u1Cause;
                pAuthFailEvt->u1AuthType = u1AuthType;
                MEMCPY (pAuthFailEvt->pu1PduFrag, pu1RcvdPDU, u2TrapPduLen);

                IsisUtlSendEvent (pContext, (UINT1 *) pAuthFailEvt,
                                  sizeof (tIsisEvtAuthFail));
            }
            else
            {
                /*Release Memory for pAuthFailEvt */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pAuthFailEvt);
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Auth Fail\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
        }
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlAuthenticatePDU ()\n"));
    return (i4AuthStat);
}

/******************************************************************************
 * Function    : IsisUtlMaxAreaAddrCheck () 
 * Description : This function checks for a match between Maximum Area 
 *               Addresses supported by the local IS and the value 
 *               included in the received PDU
 * Input(s)    : pContext - Pointer to System Context
 *               pu1PDU   - Pointer to the received PDU
 * Output(s)   : None
 * Globals     : None Referred or Modified
 * Returns     : ISIS_SUCCESS - if both the systems support the same number of
 *                              Area Addresses
 *               ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUtlMaxAreaAddrCheck (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                         UINT1 *pu1PDU)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MaxAA = 0;
    UINT2               u2TrapPduLen = 0;
    UINT2               u2PduLen = 0;
    tIsisComHdr        *pComHdr = NULL;
    tIsisEvtMaxAAMismatch *pAAEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisUtlMaxAreaAddrCheck ()\n"));

    pComHdr = (tIsisComHdr *) pu1PDU;
    u1MaxAA = pComHdr->u1MaxAreaAddr;

    /* Comparing the Max Area Address reported in the PDU with that 
     * present in the SysActuals
     */

    if (((u1MaxAA != 0) && (u1MaxAA != pContext->SysActuals.u1SysMaxAA))
        || ((u1MaxAA == 0)
            && (pContext->SysActuals.u1SysMaxAA != ISIS_DEF_MAX_AALEN)))
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <E> : Max Area Address Mismatch - Self MAA [ %u ], Peer MAA [ %u ]\n",
                 pContext->SysActuals.u1SysMaxAA, u1MaxAA));
        ISIS_INCR_MAA_MISMATCH_STAT (pContext);

        /* Form MaxAreaAddress Mismatch Event and send to control queue
         */

        pAAEvt = (tIsisEvtMaxAAMismatch *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtMaxAAMismatch));

        if (pAAEvt != NULL)
        {

            ISIS_EXTRACT_HELLO_PDU_LEN (pu1PDU, u2PduLen);
            u2TrapPduLen = ISIS_MIN (ISIS_TRAP_PDU_FRAG_SIZE, u2PduLen);

            /*Allocate Memory for PDU Fragment */
            pAAEvt->pu1PduFrag = (UINT1 *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, ISIS_TRAP_PDU_FRAG_SIZE);
            if (pAAEvt->pu1PduFrag != NULL)
            {
                pAAEvt->u1EvtID = ISIS_EVT_MAX_AA_MISMATCH;
                pAAEvt->u1SelfMAA = pContext->SysActuals.u1SysMaxAA;
                pAAEvt->u1RxdMAA = u1MaxAA;
                pAAEvt->u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);
                pAAEvt->u4CktIfIdx = pCktRec->u4CktIfIdx;
                MEMCPY (pAAEvt->pu1PduFrag, pu1PDU, u2TrapPduLen);
                IsisUtlSendEvent (pContext, (UINT1 *) pAAEvt,
                                  sizeof (tIsisEvtMaxAAMismatch));
            }
            else
            {
                ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pAAEvt);
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Max Area Addr Event\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }

        }
        i4RetVal = ISIS_FAILURE;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisUtlMaxAreaAddrCheck ()\n"));
    return i4RetVal;
}

/*****************************************************************************
 *  Function    : IsisUtlVerifyPasswd ()
 *  Description : This function verifies the password included in the received
 *                PDU with Circuit, Area or Domain password based on the type
 *                of PDU.
 *  Input(s)    : pContext    - Pointer to the system context
 *                pCktRec     - Pointer to the Circuit record
 *                pu1PDU      - Pointer to the PDU
 *                u2Offset    - Offset into the PSU where the Password exist
 *                u1PasswdLen - Length of the password
 *  Output(s)   : None
 *  Globals     : Not Referred or Modified              
 *  RETURN(s)   : ISIS_SUCCESS - if the received password matches one of the
 *                               configured passwords
 *                ISIS_FAILURE, otherwise  
 *****************************************************************************/

PRIVATE INT4
IsisUtlVerifyPasswd (tIsisPasswd * pTxPasswd, tIsisPasswd * pRxPasswd,
                     UINT1 *pu1Password, UINT1 u1PasswdLen)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1RxPswCnt = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlVerifyPasswd ()\n"));

    /* Received PDUs must be authenticated against both the receive and transmit
     * passwords. Authentictae against transmit password first
     */

    if (pTxPasswd != NULL)
    {                            /*klocwork */
        if (((u1PasswdLen <= ISIS_MAX_PASSWORD_LEN) && (pTxPasswd->u1Len != 0)
             && (pTxPasswd->u1Len == u1PasswdLen))
            && (MEMCMP (pTxPasswd->au1Password, pu1Password, u1PasswdLen) == 0))
        {
            i4RetVal = ISIS_SUCCESS;
        }
    }

    if (i4RetVal != ISIS_SUCCESS)
    {
        /* The Password in the received PDU did not match the Transmit Password.
         * Let us verify the same with the receive passwords
         */

        for (u1RxPswCnt = 0; u1RxPswCnt < ISIS_MAX_RX_PASSWDS; u1RxPswCnt++)
        {
            /*klocwork */
            if ((u1PasswdLen <= ISIS_MAX_PASSWORD_LEN)
                && ((pRxPasswd + u1RxPswCnt)->u1ExistState == ISIS_ACTIVE)
                && (((pRxPasswd + u1RxPswCnt)->u1Len != 0)
                    && ((pRxPasswd + u1RxPswCnt)->u1Len == u1PasswdLen))
                &&
                (MEMCMP
                 ((pRxPasswd + u1RxPswCnt)->au1Password, pu1Password,
                  u1PasswdLen) == 0))
            {
                i4RetVal = ISIS_SUCCESS;
                break;
            }
        }
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlVerifyPasswd ()\n"));
    return (i4RetVal);
}

/*****************************************************************************
 *  Function    : IsisUtlValidatePDU ()
 *  Description : 1. This function checks whether the PDU is received over a
 *                   circuit whose External Domain is FALSE. 
 *                2. Verifies that the System ID length of the received PDU 
 *                   is not different from the Local System
 *                3. Verifies the ISIS version   
 *                3. for Hellos and LSPs it checks for protocol supported.   
 *  Globals     : Not Referred or Modified              
 *  Input(s)    : pContext   - Pointer to the system context
 *                pCktRec    - Pointer to the Circuit record
 *                pu1RcvdPDU - Pointer to the PDU
 *  Output(s)   : None
 *  RETURN(s)   : ISIS_SUCCESS - If the PDU is valid  
 *                ISIS_FAILURE - Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisUtlValidatePDU (tIsisSysContext * pContext,
                    tIsisCktEntry * pCktRec, UINT1 *pu1RcvdPDU)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2TrapPduLen = 0;
    UINT2               u2PDULen = 0;
    UINT2               u2MTId = 0;
    UINT2               u2Offset = 0;
    UINT1               u1IdLen = 0;
    UINT1               u1PduType = 0;
    UINT1               u1Level = 0;
    UINT1               u1PSChk = ISIS_TRUE;
    UINT1               u1Version = 0;
    UINT1               u1MTLen = 0;
    UINT1               u1MTTlv = 0;
    UINT1               u1SuppMtFound = ISIS_FALSE;
    UINT2               u2RLTime = 0;
    UINT1               u1Len = 0;
    UINT1               u1ThreeWayState = 5;

    tIsisEvtIDLenMismatch *pEvtIDLenMismatch = NULL;
    tIsisEvtVerMismatch *pVerMMEvt = NULL;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlValidatePDU ()\n"));

    if (pCktRec->bCktPassiveCkt == ISIS_TRUE)
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <X> : Interface is Passive. Reception not Allowed - Circuit Index [ %u ]\n",
                 pCktRec->u4CktIdx));
        return ISIS_FAILURE;
    }

    u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1RcvdPDU);
    u1PduType &= ISIS_PDU_TYPE_MASK;

    switch (u1PduType)
    {
        case ISIS_P2P_IIH_PDU:
            u1Level = (pCktRec->u1CktLevel == ISIS_LEVEL2) ? ISIS_LEVEL2
                : ISIS_LEVEL1;
            break;

        case ISIS_L1LSP_PDU:

            if ((*(pu1RcvdPDU + ISIS_OFFSET_LSPID + ISIS_SYS_ID_LEN +
                   ISIS_PNODE_ID_LEN) != 0) ||
                (*(pu1RcvdPDU + ISIS_OFFSET_LSPID + ISIS_SYS_ID_LEN) != 0))
            {
                /* Disable the protocol supported check for non-zero LSPs
                 * And Pseudo-node LSPs
                 */

                u1PSChk = ISIS_FALSE;
            }
            u1Level = ISIS_LEVEL1;
            break;

        case ISIS_L1LAN_IIH_PDU:

            u1Level = ISIS_LEVEL1;
            break;

        case ISIS_L2LSP_PDU:

            if ((*(pu1RcvdPDU + ISIS_OFFSET_LSPID + ISIS_SYS_ID_LEN +
                   ISIS_PNODE_ID_LEN) != 0) ||
                (*(pu1RcvdPDU + ISIS_OFFSET_LSPID + ISIS_SYS_ID_LEN) != 0))
            {
                /* Disable the protocol supported check for non-zero LSPs
                 * And Pseudo-node LSPs
                 */

                u1PSChk = ISIS_FALSE;
            }
            u1Level = ISIS_LEVEL2;
            break;

        case ISIS_L2LAN_IIH_PDU:

            u1Level = ISIS_LEVEL2;
            break;

        case ISIS_L1PSNP_PDU:
        case ISIS_L1CSNP_PDU:

            u1Level = ISIS_LEVEL1;
            u1PSChk = ISIS_FALSE;
            break;

        case ISIS_L2PSNP_PDU:
        case ISIS_L2CSNP_PDU:

            u1Level = ISIS_LEVEL2;
            u1PSChk = ISIS_FALSE;
            break;

        default:

            UTP_PT ((ISIS_LGST,
                     "UTL <E> : Unknown PDU [ %u ] Received in Circuit [ %u ]  - Discarding\n",
                     u1PduType, pCktRec->u4CktIdx));
            return ISIS_FAILURE;
    }

    /* extract the PDU Len if the Event has to be sent
     */
    if ((((tIsisComHdr *) pu1RcvdPDU)->u1PDUType == ISIS_L1LAN_IIH_PDU)
        || (((tIsisComHdr *) pu1RcvdPDU)->u1PDUType == ISIS_L2LAN_IIH_PDU)
        || (((tIsisComHdr *) pu1RcvdPDU)->u1PDUType == ISIS_P2P_IIH_PDU))
    {
        /* Extracting the PDU Length in the case of HELLOs
         */

        ISIS_GET_2_BYTES (pu1RcvdPDU,
                          sizeof (tIsisComHdr) + ISIS_SYS_ID_LEN + 3, u2PDULen);
    }
    else
    {
        /* Extracting PDU Length in the case of LSPs, CSNPs, PSNPs
         */

        ISIS_GET_2_BYTES (pu1RcvdPDU, sizeof (tIsisComHdr), u2PDULen);
    }

    if (pContext->u1IsisMTSupport == ISIS_FALSE)
    {
        if ((IsisUtlGetNextTlvOffset
             (pu1RcvdPDU, ISIS_MT_TLV, &u2Offset, &u1MTLen)) == ISIS_SUCCESS)
        {
            return ISIS_FAILURE;
        }
    }
    else
    {
        if ((u1PduType == ISIS_L1LAN_IIH_PDU) ||
            (u1PduType == ISIS_L2LAN_IIH_PDU) ||
            (u1PduType == ISIS_P2P_IIH_PDU))
        {
            if ((IsisUtlGetNextTlvOffset
                 (pu1RcvdPDU, ISIS_MT_TLV, &u2Offset,
                  &u1MTLen)) == ISIS_SUCCESS)
            {
                while (u1MTTlv < u1MTLen)
                {
                    MEMCPY (&u2MTId, (pu1RcvdPDU + u2Offset), ISIS_MT_ID_LEN);
                    u2MTId = OSIX_HTONS (u2MTId);
                    u2MTId = (u2MTId & ISIS_MTID_MASK);
                    if ((u2MTId == ISIS_IPV4_UNICAST_MT_ID)
                        || (u2MTId == ISIS_IPV6_UNICAST_MT_ID))
                    {
                        u1SuppMtFound = ISIS_TRUE;
                        break;
                        /*MISIS - MT validated (atleast one supported MT present */
                    }
                    u1MTTlv += (UINT1) 2;    /*MISIS - SIZE OF MTID + FLAGS */
                    u2Offset = u2Offset + u1MTTlv;
                }
                /* when MT is enabled in the local router, it is expected that the other ISIS 
                 * routers also enabled with MT support. So when a IIH received without any MT-TLV
                 * it is assumed that it supports Multi-topology and it supports MT-ID 0,
                 * since it is optional to include MT-0 in IIH
                 */

                if (u1SuppMtFound != ISIS_TRUE)
                {
                    return ISIS_FAILURE;
                }

            }
        }
    }

    /*Validation of three-way handshake TLV */
    if ((u1PduType == ISIS_P2P_IIH_PDU)
        && (pCktRec->u1IsP2PThreeWayEnabled == ISIS_TRUE))
    {
        u2Offset = 0;
        if ((IsisUtlGetNextTlvOffset
             (pu1RcvdPDU, ISIS_P2P_THREE_WAY_ADJ_TLV, &u2Offset,
              &u1Len)) == ISIS_SUCCESS)
        {
            /*check if the length is in valid range (1-17) */
            if (!
                ((u1Len >= ISIS_P2P_THREE_WAY_STATE_LEN)
                 && (u1Len <= ISIS_P2P_THREE_WAY_HS_MAX_LEN)))
            {
                /*invalid length - discard the packet */
                return ISIS_FAILURE;
            }
            /*TLV Length is valid - check if the 3-way state is valid */
            u1ThreeWayState = *(pu1RcvdPDU + u2Offset + 2);
            if (!(u1ThreeWayState <= 3))
            {
                /*invalid 3-way state - discard the packet */
                return ISIS_FAILURE;
            }
            pCktRec->u1P2PDynHshkMachanism = ISIS_P2P_THREE_WAY;
        }
        else
        {
            pCktRec->u1P2PDynHshkMachanism = ISIS_P2P_TWO_WAY;
        }
    }
    else if ((u1PduType == ISIS_P2P_IIH_PDU)
             && (pCktRec->u1IsP2PThreeWayEnabled == ISIS_FALSE))
    {
        pCktRec->u1P2PDynHshkMachanism = ISIS_P2P_TWO_WAY;
    }

    u1Version = ((tIsisComHdr *) pu1RcvdPDU)->u1VerProtoId;

    if (u1Version != ISIS_PROT_ID_EXT)
    {
        UTP_PT ((ISIS_LGST,
                 "UTL <W> : The Version/Protocol ID Extension Mismatch - Peer [ %u ], Self [ %u ]\n",
                 u1Version, ISIS_PROT_ID_EXT));

        pVerMMEvt = (tIsisEvtVerMismatch *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtVerMismatch));

        if (pVerMMEvt != NULL)
        {

            u2TrapPduLen = ISIS_MIN (ISIS_TRAP_PDU_FRAG_SIZE, u2PDULen);
            pVerMMEvt->pu1PduFrag = ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                    ISIS_TRAP_PDU_FRAG_SIZE);
            if (pVerMMEvt->pu1PduFrag != NULL)
            {
                pVerMMEvt->u1EvtID = ISIS_EVT_VER_MISMATCH;
                pVerMMEvt->u1ProtVer = u1Version;
                pVerMMEvt->u1SysLevel = u1Level;
                pVerMMEvt->u4CktIfIdx = pCktRec->u4CktIfIdx;
                MEMCPY (pVerMMEvt->pu1PduFrag, pu1RcvdPDU, u2TrapPduLen);
                IsisUtlSendEvent (pContext, (UINT1 *) pVerMMEvt,
                                  sizeof (tIsisEvtVerMismatch));
            }
            else
            {
                ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pVerMMEvt);
            }
        }
        UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlValidatePDU ()\n"));
        return ISIS_FAILURE;
    }

    u1IdLen = ((tIsisComHdr *) pu1RcvdPDU)->u1IdLen;
    if (pCktRec->bCktExtDomain == ISIS_TRUE)
    {
        /* The circuit is connected to External Domain and the local system is
         * not supposed to process any packets received on such connections
         */

        UTP_PT ((ISIS_LGST,
                 "UTL <E> : Packet From External Domain - Circuit [ %u ]\n",
                 pCktRec->u4CktIdx));
        i4RetVal = ISIS_FAILURE;
    }
    else if (((u1IdLen != 0) && (u1IdLen != ISIS_SYS_ID_LEN))
             || ((u1IdLen == 0) && (ISIS_SYS_ID_LEN != ISIS_DEF_SYS_ID_LEN)))
    {
        /* System ID lengths must be the same in the entire domain. If not
         * ignore therecived PDU
         */

        UTP_PT ((ISIS_LGST,
                 "UTL <E> : ID Length Mismatch - Received [ %u ] Expected [ %u ]\n",
                 ((tIsisComHdr *) pu1RcvdPDU)->u1IdLen, ISIS_SYS_ID_LEN));

        ISIS_INCR_ID_LEN_MISMATCH_STATS (pContext, u1Level);
        ISIS_INCR_CIRC_ID_LEN_MISMATCH_STATS (pCktRec);

        pEvtIDLenMismatch = (tIsisEvtIDLenMismatch *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIDLenMismatch));

        if (pEvtIDLenMismatch != NULL)
        {

            u2TrapPduLen = ISIS_MIN (ISIS_TRAP_PDU_FRAG_SIZE, u2PDULen);
            /*Allocate Memory for PDU Fragment */
            pEvtIDLenMismatch->pu1PduFrag = (UINT1 *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, ISIS_TRAP_PDU_FRAG_SIZE);
            if (pEvtIDLenMismatch->pu1PduFrag != NULL)
            {
                pEvtIDLenMismatch->u1EvtID = ISIS_EVT_ID_LEN_MISMATCH;
                pEvtIDLenMismatch->u1RxdIdLen =
                    ((tIsisComHdr *) pu1RcvdPDU)->u1IdLen;
                pEvtIDLenMismatch->u1SelfIdLen = ISIS_SYS_ID_LEN;
                pEvtIDLenMismatch->u1PduType =
                    (UINT1) ((((tIsisComHdr *) pu1RcvdPDU)->u1PDUType &
                              ISIS_PDU_TYPE_MASK));
                pEvtIDLenMismatch->u4CktIfIdx = pCktRec->u4CktIfIdx;
                MEMCPY (pEvtIDLenMismatch->pu1PduFrag, pu1RcvdPDU,
                        u2TrapPduLen);

                IsisUtlSendEvent (pContext, (UINT1 *) pEvtIDLenMismatch,
                                  sizeof (tIsisEvtIDLenMismatch));
            }
            else
            {
                /*Release Memory for pEvtIDLenMismatch */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtIDLenMismatch);
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : ID Length Mismatch\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
        }
        else
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : ID Length Mismatch Event\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
        i4RetVal = ISIS_FAILURE;
    }
    ISIS_EXTRACT_RLT_FROM_LSP (pu1RcvdPDU, u2RLTime);
    if ((u1PSChk == ISIS_TRUE) && (i4RetVal == ISIS_SUCCESS) && (u2RLTime != 0))
    {
        i4RetVal = IsisUtlChkProtSupported (pCktRec, pu1RcvdPDU, u1Level);
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlValidatePDU ()\n"));
    return i4RetVal;
}

/*****************************************************************************
 *  Function    : IsisUtlCloneMsg ()
 *  Description : This function duplicates the given message. Since both
 *                Update and Adjacency modules require the processing of some
 *                events, Control Module calls this function to clone the
 *                received event, as both the Update and Adjacency module 
 *                release the Event once the processing is complete 
 *  Input(s)    : pMsg - Pointer to the message
 *  Output(s)   : None
 *  Globals     : Not Referred or Modified              
 *  RETURN(s)   : Pointer to the duplicated message
 *****************************************************************************/

PUBLIC tIsisMsg    *
IsisUtlCloneMsg (tIsisMsg * pMsg)
{
    tIsisMsg           *pCloneMsg = NULL;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlCloneMsg ()\n"));

    pCloneMsg = (tIsisMsg *) ISIS_MEM_ALLOC (ISIS_BUF_MSGS, sizeof (tIsisMsg));

    if (pCloneMsg != NULL)
    {
        pCloneMsg->u1MsgType = pMsg->u1MsgType;
        pCloneMsg->u4CxtOrIfindex = pMsg->u4CxtOrIfindex;
        pCloneMsg->u4Length = pMsg->u4Length;
        pCloneMsg->u1LenOrStat = pMsg->u1LenOrStat;

        if (pMsg->pu1Msg != NULL)
        {
            pCloneMsg->pu1Msg = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                          pMsg->u4Length);
            if (pCloneMsg->pu1Msg != NULL)
            {
                MEMCPY (pCloneMsg->pu1Msg, pMsg->pu1Msg, pMsg->u4Length);
                UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlCloneMsg ()\n"));
                return (pCloneMsg);
            }
            else
            {
                WARNING ((ISIS_LGST, "UTL <W> : Unable To Clone Message!!!\n"));
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pCloneMsg);
                UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlCloneMsg ()\n"));
            }
        }
        else
        {
            return (pCloneMsg);
        }
    }
    else
    {
        WARNING ((ISIS_LGST, "UTL <W> : Unable To Clone Message!!!\n"));
        UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlCloneMsg ()\n"));
    }
    return (NULL);
}

PUBLIC VOID
IsisUtlGetMetric (tIsisSysContext * pContext, UINT1 *pu1Metric,
                  UINT1 *pu1TLVMetric)
{
    UINT1               u1MetIdx = 0;
    UINT1               u1TLVMetType = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlFormMetric ()\n"));

    MEMSET (pu1TLVMetric, 0, 4);

    /* Repeat the loop only for Number of Metrics supported
     */

    for (u1MetIdx = 0; u1MetIdx < ISIS_NUM_METRICS; u1MetIdx++)
    {
        /* Get the corresponding Metric Type for each of the metric index. Refer
         * to IsisUtlGetMetType () routine for details as to how this is done 
         */

        u1TLVMetType = IsisUtlGetMetType (pContext, u1MetIdx);

        if (u1TLVMetType == ISIS_DEF_MET)
        {
            /* DEFAULT always holds byte 0 as per specs
             */

            pu1TLVMetric[0] = pu1Metric[u1MetIdx];
        }
        else if (u1TLVMetType == ISIS_DEL_MET)
        {
            /* DELAY always holds byte 0 as per specs
             */

            pu1TLVMetric[1] = pu1Metric[u1MetIdx];
        }
        else if (u1TLVMetType == ISIS_EXP_MET)
        {
            /* EXPENSE always holds byte 0 as per specs
             */

            pu1TLVMetric[2] = pu1Metric[u1MetIdx];
        }
        else if (u1TLVMetType == ISIS_ERR_MET)
        {
            /* ERROR always holds byte 0 as per specs
             */

            pu1TLVMetric[3] = pu1Metric[u1MetIdx];
        }
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetMetric () \n"));
}

/*****************************************************************************
 * Function    : IsisUtlFormMetric ()
 * Description : This routine constructs a Metric Array of 4 bytes in the same
 *               order as required by the ISIS PDUs viz. DEFAULT, DELAY,
 *               EXPENSE and ERROR. Since the local system may not always
 *               support all the 4 metrics, this routine decodes the argument
 *               'pu1Metric' which actually includes only the supported
 *               metrics and constructs the required array of 4 bytes, each
 *               byte specifying one of the 4 metrics in order.
 * Input (s)   : pContext     - Pointer to System Context
 *               pu1Metric    - Array specifying the Metrics supported by the
 *                              local system
 * Output (s)  : pu1TLVMetric - Pointer to the TLV Metric Structure which
 *                              includes all the 4 metrics in order as specified
 *                              by ISO 10589
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUtlFormMetric (tIsisSysContext * pContext, UINT1 *pu1Metric,
                   UINT1 *pu1TLVMetric)
{
    UINT1               u1MetIdx = 0;
    UINT1               u1TLVMetType = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlFormMetric ()\n"));

    /* Assume none of the 4 Metrics are supported, evenhough DEFAULT metric is
     * always supported. The follwing macro wil set 4 bytes as follows:
     *
     *           BYTE 0      BYTE 1      BYTE 2      BYTE 3
     * Bits  --> 8765 4321 | 8765 4321 | 8765 4321 | 8765 4321
     * Value --> 1000 0000 | 1000 0000 | 1000 0000 | 1000 0000
     *
     * where Byte 0 specifies DEFAULT metric, Byte 1 specifies DELAY metric,
     * Byte 2 specifies EXPENSE metric and Byte 3 specifies ERROR metric. Bit 8
     * if set indicates the metric is not supported, and if reset indicates that
     * the metric value specified in the rest of the 7 bits is supported
     */

    MEMSET (pu1TLVMetric, ISIS_METRIC_SUPP_MASK, 4);

    /* Repeat the loop only for Number of Metrics supported
     */

    for (u1MetIdx = 0; u1MetIdx < ISIS_NUM_METRICS; u1MetIdx++)
    {
        /* Get the corresponding Metric Type for each of the metric index. Refer
         * to IsisUtlGetMetType () routine for details as to how this is done 
         */

        u1TLVMetType = IsisUtlGetMetType (pContext, u1MetIdx);

        if (u1TLVMetType == ISIS_DEF_MET)
        {
            /* DEFAULT always holds byte 0 as per specs
             */

            pu1TLVMetric[0] = pu1Metric[u1MetIdx];
        }
        else if (u1TLVMetType == ISIS_DEL_MET)
        {
            /* DELAY always holds byte 0 as per specs
             */

            pu1TLVMetric[1] = pu1Metric[u1MetIdx];
        }
        else if (u1TLVMetType == ISIS_EXP_MET)
        {
            /* EXPENSE always holds byte 0 as per specs
             */

            pu1TLVMetric[2] = pu1Metric[u1MetIdx];
        }
        else if (u1TLVMetType == ISIS_ERR_MET)
        {
            /* ERROR always holds byte 0 as per specs
             */

            pu1TLVMetric[3] = pu1Metric[u1MetIdx];
        }
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlFormMetric () \n"));
}

/******************************************************************************
 * Function    : IsisUtlGetMetType ()
 * Description : This routine fetches the appropriate Metric Type for the
 *               given Metric Index
 * Input (s)   : pContext - Pointer to the System Context
 *               u1MetIdx - Metric Index
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : Appropriate Metric Type Viz. DEFAULT, DELAY, EXPENSE or ERROR
 ******************************************************************************/

PUBLIC UINT1
IsisUtlGetMetType (tIsisSysContext * pContext, UINT1 u1MetIdx)
{
    UINT1               u1MetType = ISIS_MET_INVALID;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlGetMetType () \n"));

    /* To minimize the amount of memory in the SPT nodes FutureISIS
     * implementation used Metric Indices instead of Metric Types. Each Metric
     * Type is mapped to one Metric Index. The mapping depends on the Number of
     * Metrics currently supported. The following lists all the possible
     * mappings exhaustively:
     *
     * 1] Num Metrics Supported = 4
     *
     *    Metric Index = 1, Metric Type = DEFAULT
     *    Metric Index = 2, Metric Type = DELAY
     *    Metric Index = 3, Metric Type = EXPENSE
     *    Metric Index = 4, Metric Type = ERROR
     *
     * 2] Num Metrics Supported = 3
     *
     *    a] DEFAULT, DELAY, EXPENSE
     *
     *       Metric Index = 1, Metric Type = DEFAULT
     *       Metric Index = 2, Metric Type = DELAY
     *       Metric Index = 3, Metric Type = EXPENSE
     *
     *    b] DEFAULT, DELAY, ERROR
     *
     *       Metric Index = 1, Metric Type = DEFAULT
     *       Metric Index = 2, Metric Type = DELAY
     *       Metric Index = 3, Metric Type = ERROR
     *
     *    c] DEFAULT, EXPENSE, ERROR
     *
     *       Metric Index = 1, Metric Type = DEFAULT
     *       Metric Index = 2, Metric Type = EXPENSE
     *       Metric Index = 3, Metric Type = ERROR
     *
     * 3] Num Metrics Supported = 2
     *
     *    a] DEFAULT, DELAY
     *
     *       Metric Index = 1, Metric Type = DEFAULT
     *       Metric Index = 2, Metric Type = DELAY
     *
     *    b] DEFAULT, ERROR
     *
     *       Metric Index = 1, Metric Type = DEFAULT
     *       Metric Index = 2, Metric Type = ERROR
     *
     *    c] DEFAULT, EXPENSE
     *
     *       Metric Index = 1, Metric Type = DEFAULT
     *       Metric Index = 2, Metric Type = EXPENSE
     *
     * This way all SPT nodes will have memory to hold NUM_METRICS only and
     * still can retrieve the type of metric with any combination which is
     * better than static mapping. 
     *
     * NOTE: DEFAULT metric s always supported
     */

    switch (u1MetIdx)
    {
        case 0:

            /* Default is always supported and hence we always return DEFAULT
             * for Metric Index '0'
             */

            u1MetType = ISIS_DEF_MET;
            break;

        case 1:

            /* If Delay Metric is Supported then Index '1' is always mapped to
             * DELAY
             */

            if (pContext->SysActuals.u1SysMetricSupp & ISIS_DEL_MET_SUPP_FLAG)
            {
                u1MetType = ISIS_DEL_MET;
            }

            /* DELAY metric is not supported and hence if Expense Metric is 
             * Supported then Index '1' is always mapped to EXPENSE
             */

            else if (pContext->SysActuals.u1SysMetricSupp &
                     ISIS_EXP_MET_SUPP_FLAG)
            {
                u1MetType = ISIS_EXP_MET;
            }

            /* DELAY and EXPENSE metrics are not supported and hence if ERROR 
             * Metric is Supported then Index '1' is always mapped to ERROR
             */

            else if (pContext->SysActuals.u1SysMetricSupp &
                     ISIS_ERR_MET_SUPP_FLAG)
            {
                u1MetType = ISIS_ERR_MET;
            }

            /* No 'else' case since this function will never be invoked with
             * this index if atleast two metrics are not supported
             */

            break;

        case 2:

            /* Index '0' is mapped to DEFAULT and Index '1' is mapped to DELAY
             * if delay metric is supported, else it is mapped to EXPENSE. 
             * Hence if DELAY metric is supported then Index '2'
             * is mapped to EXPENSE, else Index '2' is mapped to ERROR
             */

            if ((pContext->SysActuals.u1SysMetricSupp & ISIS_DEL_MET_SUPP_FLAG)
                && (pContext->SysActuals.u1SysMetricSupp &
                    ISIS_EXP_MET_SUPP_FLAG))
            {
                u1MetType = ISIS_EXP_MET;
            }

            /* Either DELAY metric is not supported, in which case Metric Index
             * '1' would have been mapped to EXPENSE and hence Metric Index '2'
             * will be mapped to ERROR or EXPENSE Metric is not supported, in
             * which case Metric Index '1' is mapped to DELAY and hence '2' must
             * be mapped to ERROR. So in any case we must map Metric Index '2'
             * to ERROR at his point
             */

            else if (pContext->SysActuals.
                     u1SysMetricSupp & ISIS_ERR_MET_SUPP_FLAG)
            {
                u1MetType = ISIS_ERR_MET;
            }

            /* No 'else' case since this function will never be invoked with
             * this index if atleast three metrics are not supported
             */

            break;

        case 3:

            if (pContext->SysActuals.u1SysMetricSupp & ISIS_ERR_MET_SUPP_FLAG)
            {
                /* Index '0', Index '1' and Index '2' are mapped to DEFAULT,
                 * DELAY and EXPENSE respectively. Hence Index '3' must be
                 * mapped only to ERROR
                 */

                u1MetType = ISIS_ERR_MET;
            }
            break;

        default:

            UTP_PT ((ISIS_LGST, "UTL <E> : Invalid Metric Index [ %u ]\n",
                     u1MetIdx));
            break;
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetMetType () \n"));
    return u1MetType;
}

/******************************************************************************
 *  Function     : IsisUtlGetMetIndex ()
 *  Description  : This routine fetches the appropriate Metric Index for the
 *                 given Metric Type
 *  Globals      : None is Referred or Modified               
 *  Input (s)    : pContext  - Pointer to the System Context
 *                 u1MetType - Metric Type
 *  Output (s)   : None
 *  RETURN (s)   : Metric Index
 ******************************************************************************/

PUBLIC UINT1
IsisUtlGetMetIndex (tIsisSysContext * pContext, UINT1 u1MetType)
{
    UINT1               u1MetIdx = ISIS_NUM_METRICS;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlGetMetIdx () \n"));

    /* The Supported Metrics are in the order of Default Metric, Delay Metric,
     * Expense Metric and Error Metric. Among the four, Default Metric should be
     * always supported. So index of Default Metric is always 0. And Index of
     * Delay Metric, if it is supported, is always 1. The indices of the other
     * two depends on whether their predecessors are being supported. So if all 
     * the metrics are supported, Expense Metric will take an index value of 2
     * and Error Metric will take an index value of 3
     */

    switch (u1MetType)
    {
        case ISIS_DEF_MET:

            u1MetIdx = 0;
            break;

        case ISIS_DEL_MET:

            u1MetIdx = 1;
            break;

        case ISIS_EXP_MET:

            u1MetIdx = (UINT1) (((pContext->SysActuals.u1SysMetricSupp &
                                  ISIS_DEL_MET_SUPP_FLAG)) ? 2 : 1);

            break;

        case ISIS_ERR_MET:

            /* Checking if Delay Metric is Supported
             */

            if ((pContext->SysActuals.u1SysMetricSupp & ISIS_DEL_MET_SUPP_FLAG))
            {
                u1MetIdx = (UINT1) (((pContext->SysActuals.u1SysMetricSupp &
                                      ISIS_EXP_MET_SUPP_FLAG)) ? 3 : 2);
            }

            else
            {
                u1MetIdx = (UINT1) (((pContext->SysActuals.u1SysMetricSupp &
                                      ISIS_EXP_MET_SUPP_FLAG)) ? 2 : 1);
            }

            break;

        default:

            UTP_PT ((ISIS_LGST, "UTL <E> : Invalid Metric Type \n"));
            break;
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetMetIdx () \n"));

    return u1MetIdx;
}

/******************************************************************************
 * Function     : IsisUtlGetHashIdx ()
 * Description  : This function calculates the Hash Index from the given Key. It
 *                does an EXOR operation with all the bytes of the Key to get a
 *                1 byte result. The final result is the obtained result modulo
 *                hash size
 * Input (s)    : pu1Key     - Pointer to the Hash Key
 *                u1KeyLen   - Length of the Hash Key
 *                u1HashSize - Size of the Hash Table
 * Output (s)   : None
 * Globals      : Not Referred or Modified               
 * Returns      : Hash Index
 ******************************************************************************/

PUBLIC UINT1
IsisUtlGetHashIdx (UINT1 *pu1Key, UINT1 u1KeyLen, UINT1 u1HashSize)
{
    UINT1               u1Idx = 0;
    UINT1               u1Cnt = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlGetHashIdx () \n"));

    u1Idx = *pu1Key;
    for (u1Cnt = 1; u1Cnt < u1KeyLen; u1Cnt++)
    {
        u1Idx ^= *(pu1Key + u1Cnt);
    }

    u1Idx = (UINT1) (u1Idx % u1HashSize);

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetHashIdx () \n"));
    return (u1Idx);
}

/******************************************************************************
 *  Function     : IsisUtlAllocBuf ()
 *  Description  : This function allocates the required memory from appropriate
 *                 memory pools based on the type of the buffer.
 *                 If the #define ISIS_MEM_DBG=1, then the name of the File and the
 *                 line number from which the memory allocation, is requested,
 *                 are printed
 *  Input (s)    : u1Type - Buffer Type
 *                 u4Len  - Number of octets to be allocated
 *                 File   - Name of the File from which the memory Allocation is
 *                          requested
 *                 Line   - Line Number of the file 'File' where the memory
 *                          Allocation is requested
 *  Output (s)   : None
 *  Globals      : gau1IsisBdyId Referred , not Modified               
 *  Returns      : Pointer to the allocated Buffer
 ******************************************************************************/

#if (ISIS_MEM_DBG == 1)

PUBLIC VOID        *
IsisUtlAllocBuf (UINT1 u1Type, UINT4 u4Len, const CHAR * File, const INT4 Line)
#else

PUBLIC UINT1       *
IsisUtlAllocBuf (UINT1 u1Type, UINT4 u4Len)
#endif
{
    UINT1              *pu1Buf = NULL;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlAllocBuf ()\n"));

    switch (u1Type)
    {
        case ISIS_BUF_LSPTX:

            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4LspTxQPid);
            break;

        case ISIS_BUF_MISC:
        case ISIS_BUF_RTMD:
        case ISIS_BUF_MDTP:
        case ISIS_BUF_TLVS:
        case ISIS_BUF_SLSP:
        case ISIS_BUF_EVTS:
        case ISIS_BUF_SNLI:
        case ISIS_BUF_NSLI:
        case ISIS_BUF_ADAA:
        case ISIS_BUF_ECTI:
        case ISIS_BUF_SRMF:
            if (u4Len > 200)
            {
                UTP_PT ((ISIS_LGST,
                         "UTL <T> : (AllocBuf) Invalid buffer length [%d] requested for 64 BLK.\n",
                         u4Len));
                return NULL;
            }

            pu1Buf = (UINT1 *) MemAllocMemBlk (gau1IsisBdyId[ISIS_BDY_64_BLK]);

            break;

        case ISIS_BUF_LTXQ:

            pu1Buf = (UINT1 *) IsisAllocLSPTxBlock ();
            break;

        case ISIS_BUF_LDBE:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4LSEPid);
            break;

        case ISIS_BUF_HTBL:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4HTblPid);
            break;

        case ISIS_BUF_SPTN:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4RoutesPid);
            break;

        case ISIS_BUF_IPRA:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4IPRAPid);
            break;

        case ISIS_BUF_ADIR:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4AdjDirPid);
            break;

        case ISIS_BUF_CTXT:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4InstsPid);
            break;

        case ISIS_BUF_CKTS:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4CktsPid);
            break;

        case ISIS_BUF_CKTL:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4CktLvlPid);
            break;

        case ISIS_BUF_ADJN:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4AdjsPid);
            break;

        case ISIS_BUF_EVTE:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4EventsPid);
            break;

        case ISIS_BUF_SATE:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4SAPid);
            break;

        case ISIS_BUF_MAAT:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4MAAPid);
            break;

        case ISIS_BUF_ARAD:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4AAPid);
            break;

        case ISIS_BUF_IPIF:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4IPIFPid);
            break;

        case ISIS_BUF_ECTS:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4ECBPid);
            break;

        case ISIS_BUF_MSGS:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4MsgBuffPid);
            break;

        case ISIS_BUF_LSPI:
        case ISIS_BUF_FLTR:
        case ISIS_BUF_HLPD:
        case ISIS_BUF_CSNP:
        case ISIS_BUF_PSNP:
            if (u4Len > 1536)
            {
                UTP_PT ((ISIS_LGST,
                         "UTL <T> : (AllocBuf) Invalid buffer length [%d] requested for 1500 BLK.\n",
                         u4Len));
                return NULL;
            }
            pu1Buf =
                (UINT1 *) MemAllocMemBlk (gau1IsisBdyId[ISIS_BDY_1500_BLK]);

            break;

        case ISIS_BUF_DLLI:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4DllPid);
            break;

        case ISIS_BUF_CHNS:

            pu1Buf = (UINT1 *) CRU_BUF_Allocate_MsgBufChain (u4Len, 0);
            break;

        case ISIS_RRD_ROUTE:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4RRDRoutesPid);
            break;

        case ISIS_BUF_HSTNME:
            pu1Buf = (UINT1 *) MemAllocMemBlk (gIsisMemPoolId.u4HostNmePid);
            break;

        default:
            break;
    }

    if ((pu1Buf != NULL) && (u1Type != ISIS_BUF_CHNS))
    {
        MEMSET (pu1Buf, 0x00, u4Len);
    }

    UNUSED_PARAM (File);
    UNUSED_PARAM (Line);
    return (VOID *) (pu1Buf);
}

/******************************************************************************
 *  Function     : IsisUtlReleaseBuf ()
 *  Description  : This function releases the memeory occupied by the Buffer
 *                 'pBuf'
 *                 If the #define ISIS_MEM_DBG=1, then the name of the File and the
 *                 line number from which the memory free, is requested,
 *                 are printed
 *  Input (s)    : u1Type - Buffer Type
 *                 pu1Buf - Buffer whose memory is to be freed
 *                 File   - Name of the File from where the memory free is
 *                          requested
 *                 Line   - Line Number of the file 'File' where the memory
 *                          free is requested
 *  Output (s)   : None
 *  Globals      : Not Referred or Modified
 *  RETURN (s)   : VOID
 ******************************************************************************/

#if (ISIS_MEM_DBG == 1)

PUBLIC VOID
IsisUtlReleaseBuf (UINT1 u1Type, UINT1 *pu1Buf, const CHAR * File,
                   const INT4 Line)
#else

PUBLIC VOID
IsisUtlReleaseBuf (UINT1 u1Type, UINT1 *pu1Buf)
#endif
{
    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlReleaseBuf () \n"));

    if (pu1Buf != NULL)
    {
        switch (u1Type)
        {
            case ISIS_BUF_LSPTX:

                MemReleaseMemBlock (gIsisMemPoolId.u4LspTxQPid, pu1Buf);
                break;

            case ISIS_BUF_MISC:
            case ISIS_BUF_RTMD:
            case ISIS_BUF_MDTP:
            case ISIS_BUF_TLVS:
            case ISIS_BUF_SLSP:
            case ISIS_BUF_EVTS:
            case ISIS_BUF_SNLI:
            case ISIS_BUF_NSLI:
            case ISIS_BUF_ADAA:
            case ISIS_BUF_ECTI:
            case ISIS_BUF_SRMF:

                MemReleaseMemBlock (gau1IsisBdyId[ISIS_BDY_64_BLK], pu1Buf);
                break;

            case ISIS_BUF_LTXQ:

                IsisDeAllocLSPTxBlock ((tIsisLSPTxEntry *) (VOID *) pu1Buf);
                break;

            case ISIS_BUF_LDBE:

                MemReleaseMemBlock (gIsisMemPoolId.u4LSEPid, pu1Buf);
                break;

            case ISIS_BUF_HTBL:

                MemReleaseMemBlock (gIsisMemPoolId.u4HTblPid, pu1Buf);
                break;

            case ISIS_BUF_SPTN:

                MemReleaseMemBlock (gIsisMemPoolId.u4RoutesPid, pu1Buf);
                break;

            case ISIS_BUF_IPRA:

                MemReleaseMemBlock (gIsisMemPoolId.u4IPRAPid, pu1Buf);
                break;

            case ISIS_BUF_ADIR:

                MemReleaseMemBlock (gIsisMemPoolId.u4AdjDirPid, pu1Buf);
                break;

            case ISIS_BUF_CTXT:
            {
                tIsisSysContext    *pContext =
                    (tIsisSysContext *) (VOID *) pu1Buf;

                IsisTmrStopTimer (&(pContext->SysTimers.SysECTimer));
                IsisTmrStopTimer (&(pContext->SysTimers.SysISSeqWrapTimer));
                IsisTmrStopTimer (&(pContext->SysTimers.SysDbaseNormTimer));
                IsisTmrStopTimer (&(pContext->SysTimers.SysSPFSchTimer));
                IsisTmrStopTimer (&(pContext->SysTimers.SysRestartT3Timer));

                IsisTmrStopTimer (&(pContext->SysL1Info.SysLSPGenTimer));
                IsisTmrStopTimer (&(pContext->SysL1Info.SysWaitingTimer));
                IsisTmrStopTimer (&(pContext->SysL1Info.SysRestartT2Timer));

                IsisTmrStopTimer (&(pContext->SysL2Info.SysLSPGenTimer));
                IsisTmrStopTimer (&(pContext->SysL2Info.SysWaitingTimer));
                IsisTmrStopTimer (&(pContext->SysL2Info.SysRestartT2Timer));

                MemReleaseMemBlock (gIsisMemPoolId.u4InstsPid, pu1Buf);
                break;
            }

            case ISIS_BUF_CKTS:

                MemReleaseMemBlock (gIsisMemPoolId.u4CktsPid, pu1Buf);
                break;

            case ISIS_BUF_CKTL:

                MemReleaseMemBlock (gIsisMemPoolId.u4CktLvlPid, pu1Buf);
                break;

            case ISIS_BUF_ADJN:

                MemReleaseMemBlock (gIsisMemPoolId.u4AdjsPid, pu1Buf);
                break;

            case ISIS_BUF_EVTE:

                MemReleaseMemBlock (gIsisMemPoolId.u4EventsPid, pu1Buf);
                break;

            case ISIS_BUF_SATE:

                MemReleaseMemBlock (gIsisMemPoolId.u4SAPid, pu1Buf);
                break;

            case ISIS_BUF_MAAT:

                MemReleaseMemBlock (gIsisMemPoolId.u4MAAPid, pu1Buf);
                break;

            case ISIS_BUF_ARAD:

                MemReleaseMemBlock (gIsisMemPoolId.u4AAPid, pu1Buf);
                break;

            case ISIS_BUF_IPIF:

                MemReleaseMemBlock (gIsisMemPoolId.u4IPIFPid, pu1Buf);
                break;

            case ISIS_BUF_ECTS:

                MemReleaseMemBlock (gIsisMemPoolId.u4ECBPid, pu1Buf);
                break;

            case ISIS_BUF_LSPI:
            case ISIS_BUF_FLTR:
            case ISIS_BUF_HLPD:
            case ISIS_BUF_CSNP:
            case ISIS_BUF_PSNP:

                MemReleaseMemBlock (gau1IsisBdyId[ISIS_BDY_1500_BLK], pu1Buf);
                break;

            case ISIS_BUF_MSGS:

                MemReleaseMemBlock (gIsisMemPoolId.u4MsgBuffPid, pu1Buf);
                break;

            case ISIS_BUF_DLLI:

                MemReleaseMemBlock (gIsisMemPoolId.u4DllPid, pu1Buf);
                break;

            case ISIS_BUF_CHNS:

                CRU_BUF_Release_MsgBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *)
                                             pu1Buf, 0);
                break;
            case ISIS_RRD_ROUTE:
                MemReleaseMemBlock (gIsisMemPoolId.u4RRDRoutesPid, pu1Buf);
                break;

            case ISIS_BUF_HSTNME:
                MemReleaseMemBlock (gIsisMemPoolId.u4HostNmePid, pu1Buf);
                break;

        }
    }
    UNUSED_PARAM (File);
    UNUSED_PARAM (Line);
    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlReleaseBuf () \n"));
}

/******************************************************************************
 * Function    : IsisUtlFormResFailEvt ()
 * Description : This routine forms the Resource Failure Event for the given
 *               memory type and sends it to the Control Queue
 *               This is called whenever there is a memory allocation
 *               failure in any module
 * Input(s)    : pContext  - Pointer to System Context
 *               u1ResName - Name of the Resource whose memory allocation
 *                           has failed.
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUtlFormResFailEvt (tIsisSysContext * pContext, UINT1 u1ResName)
{
    tIsisEvtResFail    *pResEvt = NULL;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlFormResFailEvt ()\n"));

    pResEvt = (tIsisEvtResFail *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                  sizeof (tIsisEvtResFail));
    if (pResEvt != NULL)
    {
        pResEvt->u1EvtID = ISIS_EVT_MEM_RES_FAIL;
        pResEvt->u1ResType = u1ResName;
        IsisUtlSendEvent (pContext, (UINT1 *) pResEvt,
                          sizeof (tIsisEvtResFail));
    }
    else
    {
        UTP_PT ((ISIS_LGST,
                 "UTL <E> : No Memory To Post Resource Fail Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlFormResFailEvt ()\n"));
}

/******************************************************************************
 *  Function     : IsisUtlCompIPAddr ()
 *  Description  : This Routine Compares two IP Addresses, represented as
 *                 SrcIPAddr and DestIPAddr, after Masking them with their
 *                 corresponding IP Address Masks and returns ISIS_TRUE if
 *                 any match is found
 *  Globals      : Not Referred or Modified
 *  Input (s)    : pu1SrcIPAddr  - Pointer to first (Source) IP Address
 *                 u1SrcPreLen   - Prefix Length of Source IP Address
 *                 pu1DestIPAddr - Pointer to second (Destination) IP Address
 *                 u1DestPreLen  - Prefix Length of Destination IP Address
 *  Output (s)   : None
 *  RETURN (s)   : ISIS_TRUE  - If the Masked IP Addresses Match
 *                 ISIS_FALSE - Otherwise
 ******************************************************************************/

PUBLIC UINT1
IsisUtlCompIPAddr (UINT1 *pu1SrcIPAddr, UINT1 u1SrcPreLen,
                   UINT1 *pu1DestIPAddr, UINT1 u1DestPreLen)
{
    UINT1               u1IPLen = 0;
    UINT1               u1Found = ISIS_TRUE;
    UINT1               u1IPAddrByte;
    UINT1               au1SrcMask[ISIS_MAX_IP_ADDR_LEN];
    UINT1               au1DestMask[ISIS_MAX_IP_ADDR_LEN];

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlCompIPAddr () \n"));

    IsisUtlGetIPMask (u1SrcPreLen, au1SrcMask);
    IsisUtlGetIPMask (u1DestPreLen, au1DestMask);

    /* Taking each byte of Source IP Address and doing logical AND with
     * corresponding byte of IP Address Mask. Then comparing that
     * value with the Destination IP Address ANDed with IP Address Mask
     */

    for (u1IPLen = 0; u1IPLen < ISIS_MAX_IP_ADDR_LEN; u1IPLen++)
    {
        u1IPAddrByte = (UINT1) (pu1SrcIPAddr[u1IPLen] & au1SrcMask[u1IPLen]);

        if (u1IPAddrByte != (pu1DestIPAddr[u1IPLen] & au1DestMask[u1IPLen]))
        {
            u1Found = ISIS_FALSE;
            break;
        }
    }
    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlCompIPAddr () \n"));

    return (u1Found);
}

/******************************************************************************
 * Function    : IsisUtlFillMetDefVal ()
 * Description : This routine fills the default values for the supported
 *               metrics. The default values differ for Circuit Level and IPRA
 *               and Summary Address Tables. The values to be copied is
 *               indicated by the argument 'u1MetFlag' entries.
 * Input (s)   : pContext   - Pointer to System Context
 *               pau1Met    - Metric array to be initialized
 *               u1MetFlag  - Flag indicating whether to fill the
 *                            ISIS_CKTL_DEF_METRIC_VALUE or 
 *                            ISIS_IPRA_DEF_METRIC_VALUE as the default
 *                            value for all the metrics.
 * Outputs (s) : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisUtlFillMetDefVal (tIsisSysContext * pContext, VOID *pRec, UINT1 u1MetFlag)
{
    UINT4              *pu4Metric = NULL;
    UINT1              *pau1Met = NULL;
    UINT1               u1MetIdx = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlFillMetDefVal () \n"));

    if (u1MetFlag == ISIS_CKT_LEVEL_REC)
    {
        pau1Met = ((tIsisCktLevel *) pRec)->Metric;
        pu4Metric = &((tIsisCktLevel *) pRec)->u4FullMetric;
    }
    else if (u1MetFlag == ISIS_IPRA_REC)
    {
        pau1Met = ((tIsisIPRAEntry *) pRec)->Metric;
        pu4Metric = &((tIsisIPRAEntry *) pRec)->u4FullMetric;
    }
    else
    {
        pau1Met = ((tIsisSAEntry *) pRec)->Metric;
        pu4Metric = &((tIsisSAEntry *) pRec)->u4FullMetric;
    }
    /* Default Metric is always supported. Fill that
     */

    pau1Met[0]
        =
        (UINT1) ((u1MetFlag ==
                  ISIS_CKT_LEVEL_REC) ? ISIS_CKTL_DEF_METRIC_VALUE :
                 ISIS_IPRA_DEF_METRIC_VALUE);

    *pu4Metric
        =
        (UINT1) ((u1MetFlag ==
                  ISIS_CKT_LEVEL_REC) ? ISIS_CKTL_DEF_METRIC_VALUE :
                 ISIS_IPRA_DEF_METRIC_VALUE);
    /* Fill in the Delay Metric if supported
     */

    if ((pContext->SysActuals.u1SysMetricSupp & ISIS_DEL_MET_SUPP_FLAG))
    {
        u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);
        pau1Met[u1MetIdx]
            = (UINT1) ((u1MetFlag == ISIS_TRUE) ? ISIS_CKTL_DEF_METRIC_VALUE
                       : ISIS_IPRA_DEF_METRIC_VALUE);
    }

    /* Fill in the Error Metric if supported. If Delay metric is supported,
     * Error Metric will occupy the 3rd position, otherwise it will be copied
     * to the second position which is taken care by the routine
     * IsisUtlGetMetIndex ()
     */

    if ((pContext->SysActuals.u1SysMetricSupp & ISIS_ERR_MET_SUPP_FLAG))
    {
        u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);
        pau1Met[u1MetIdx]
            = (UINT1) ((u1MetFlag == ISIS_TRUE) ? ISIS_CKTL_DEF_METRIC_VALUE
                       : ISIS_IPRA_DEF_METRIC_VALUE);
    }

    /* Fill in the Expense Metric if supported. It occupies 2nd, 3rd or 4th
     * position based on whether Delay and Error Metrics are supported which is
     * taken care by the routine IsisUtlGetMetIndex ()
     */

    if ((pContext->SysActuals.u1SysMetricSupp & ISIS_EXP_MET_SUPP_FLAG))
    {
        u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);
        pau1Met[u1MetIdx]
            = (UINT1) ((u1MetFlag == ISIS_TRUE) ? ISIS_CKTL_DEF_METRIC_VALUE
                       : ISIS_IPRA_DEF_METRIC_VALUE);
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlFillMetDefVal () \n"));
}

/******************************************************************************
 * Function    : IsisUtlChkProtSupported ()
 * Description : This routine checks whether Protocol Supported TLV is present
 *               in the PDU and the value matches the protocol supported by 
 *               local.
 * Input(s)    : pu1PDU    - Pointer to the PDU 
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS - if Protocol Support TLV Exist and supported
 *               ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUtlChkProtSupported (tIsisCktEntry * pCktRec, UINT1 *pu1PDU, UINT1 u1Level)
{
    UINT1               u1TlvLen = 0;
    UINT1               u1PDUType = 0;
    UINT1               u1ProtSup = 0;
    UINT1               u1NumUnsupProts = 0;
    UINT1               au1UnsupProt[ISIS_MAX_PROTS_SUPP];
    UINT2               u2PDULen = 0;
    UINT2               u2Offset = 0;
    tIsisEvtProtSuppMismatch *pProtMMEvt = NULL;

    MEMSET (au1UnsupProt, 0, ISIS_MAX_PROTS_SUPP);

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlChkProtSupported ()\n"));

    u1PDUType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);

    switch (u1PDUType)
    {
        case ISIS_L1LSP_PDU:
        case ISIS_L2LSP_PDU:

            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1PDU, u2PDULen);
            break;

        case ISIS_L1LAN_IIH_PDU:
        case ISIS_L2LAN_IIH_PDU:
        case ISIS_P2P_IIH_PDU:

            ISIS_EXTRACT_HELLO_PDU_LEN (pu1PDU, u2PDULen);
            break;

        default:

            UTP_EE ((ISIS_LGST,
                     "UTL <X> : Exiting IsisUtlChkProtSupported ()\n"));
            return ISIS_SUCCESS;
    }

    u2Offset = ((tIsisComHdr *) pu1PDU)->u1HdrLen;

    /* Loop till End-of-PDU
     */

    while (u2Offset < u2PDULen)
    {
        /* Retrieve the Length */

        u1TlvLen = *(pu1PDU + u2Offset + 1);

        /* Match the Code */

        if (*(UINT1 *) (pu1PDU + u2Offset) == ISIS_PROT_SUPPORT_TLV)
        {
            u2Offset += 2;
            while (u1TlvLen > 0)
            {

                u1ProtSup = *(pu1PDU + u2Offset);
                if (((u1ProtSup == ISIS_IPV4_SUPP) &&
                     (pCktRec->u1ProtoSupp & ISIS_ADDR_IPV4)) ||
                    ((u1ProtSup == ISIS_IPV6_SUPP) &&
                     (pCktRec->u1ProtoSupp & ISIS_ADDR_IPV6)))
                {
                    UTP_EE ((ISIS_LGST,
                             "UTL <T> : The Protocol Listed in Protocol"
                             "Supported TLV is supported by the Local\n"));
                    UTP_EE ((ISIS_LGST,
                             "UTL <X> : Exiting IsisUtlChkProtSupported ()\n"));
                    return ISIS_SUCCESS;
                }
                else if (u1NumUnsupProts < ISIS_MAX_PROTS_SUPP)
                {
                    au1UnsupProt[u1NumUnsupProts++] = u1ProtSup;
                }
                u1TlvLen--;
                u2Offset++;
            }
        }
        else
        {
            u2Offset = (UINT2) (u2Offset + u1TlvLen + 2);
        }
    }

    if (u1ProtSup == 0)
    {
        UTP_EE ((ISIS_LGST, "UTL <E> : No Protocol Support TLV Present\n"));
    }
    else
    {
        UTP_EE ((ISIS_LGST, "UTL <E> : The Protocols Listed in LSP are not"
                 "supported by local System\n"));
    }

    pProtMMEvt = (tIsisEvtProtSuppMismatch *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtProtSuppMismatch));

    if (pProtMMEvt != NULL)
    {
        pProtMMEvt->u1EvtID = ISIS_EVT_PROT_SUPP_MISMATCH;
        pProtMMEvt->u1SysLevel = u1Level;
        MEMCPY (pProtMMEvt->au1UProt, au1UnsupProt, ISIS_MAX_PROTS_SUPP);
        pProtMMEvt->u4CktIfIdx = pCktRec->u4CktIfIdx;
        if ((u1PDUType == ISIS_L1LSP_PDU) || (u1PDUType == ISIS_L2LSP_PDU))
        {
            MEMCPY (pProtMMEvt->au1TrapLspID, (pu1PDU + ISIS_OFFSET_LSPID),
                    ISIS_LSPID_LEN);
        }
        IsisUtlSendEvent (pCktRec->pContext, (UINT1 *) pProtMMEvt,
                          sizeof (tIsisEvtProtSuppMismatch));
    }
    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlChkProtSupported ()\n"));
    return ISIS_FAILURE;
}

/******************************************************************************
 * Function    : IsisUtlGetNextTlvOffset ()
 * Description : This routine returns the next Offset of the requested TLV 
 *               in the PDU i.e. if 'pu2Offset is '0', it will return in 
 *               'pu2Offset' the offset of the first occurence of the 
 *               requested TLV in the PDU.
 * Input(s)    : pu1PDU    - Pointer to the PDU 
 *               u1Tlv     - TLV for which the next offset needs
 *                           to be found out
 *               pu2Offset - Pointer to the Offset from where the search
 *                           should commence
 * Output(s)   : pu2Offset - Offset which is greater than the incoming
 *                           value and where the requested TLV is found
 *               pu1TlvLen - Pointer to length of the TLV 
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS - if Offset of the next TLV is fetched
 *               ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUtlGetNextTlvOffset (UINT1 *pu1PDU, UINT1 u1Tlv, UINT2 *pu2Offset,
                         UINT1 *pu1TlvLen)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PDUType = 0;
    UINT2               u2PDULen = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlGetNextTlvOffset ()\n"));

    u1PDUType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);

    switch (u1PDUType)
    {
        case ISIS_L1LSP_PDU:
        case ISIS_L2LSP_PDU:
        case ISIS_L1CSNP_PDU:
        case ISIS_L2CSNP_PDU:
        case ISIS_L1PSNP_PDU:
        case ISIS_L2PSNP_PDU:

            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1PDU, u2PDULen);
            break;

        case ISIS_L1LAN_IIH_PDU:
        case ISIS_L2LAN_IIH_PDU:
        case ISIS_P2P_IIH_PDU:

            ISIS_EXTRACT_HELLO_PDU_LEN (pu1PDU, u2PDULen);
            break;

        default:

            UTP_PT ((ISIS_LGST,
                     "UTL <E> : Invalid PDU Type [ %u ] to fetch TLV [ %d ]\n",
                     u1PDUType, u1Tlv));
            UTP_EE ((ISIS_LGST,
                     "UTL <X> : Exiting IsisUtlGetNextTlvOffset ()\n"));
            return ISIS_FAILURE;
    }

    /* Loop till End-of-PDU
     */

    if (*pu2Offset == 0)
    {
        *pu2Offset = (*(pu1PDU + 1));
    }
    while (*pu2Offset < u2PDULen)
    {
        /* Retrieve the Length */

        *pu1TlvLen = *(pu1PDU + *pu2Offset + 1);

        /* Match the Code */

        if (*(UINT1 *) (pu1PDU + *pu2Offset) == u1Tlv)
        {
            /* Matching Entry Found */

            *pu2Offset += 2;    /* '1' for Code and '1' for length */
            i4RetVal = ISIS_SUCCESS;

            UTP_EE ((ISIS_LGST,
                     "UTL <X> : Exiting IsisUtlGetNextTlvOffset ()\n"));
            return (i4RetVal);
        }
        else
        {
            *pu2Offset = (UINT2) (*pu2Offset + *pu1TlvLen + 2);
        }
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetNextTlvOffset ()\n"));
    return (i4RetVal);
}

PUBLIC VOID
IsisUtlFreePkt (UINT1 *pu1PDU)
{
    switch (ISIS_EXTRACT_PDU_TYPE (pu1PDU))
    {
        case ISIS_L1LSP_PDU:
        case ISIS_L2LSP_PDU:

            ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1PDU);
            break;

        case ISIS_L1LAN_IIH_PDU:
        case ISIS_L2LAN_IIH_PDU:
        case ISIS_P2P_IIH_PDU:
        case ISIS_ISH_PDU:

            ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1PDU);
            break;

        case ISIS_L1PSNP_PDU:
        case ISIS_L2PSNP_PDU:

            ISIS_MEM_FREE (ISIS_BUF_PSNP, pu1PDU);
            break;

        case ISIS_L1CSNP_PDU:
        case ISIS_L2CSNP_PDU:

            ISIS_MEM_FREE (ISIS_BUF_CSNP, pu1PDU);
            break;

        default:

            /* This Case should not happen.
             */

            ISIS_MEM_FREE (ISIS_BUF_BDDY, pu1PDU);
            break;
    }
}

/*VCM Utility Routines*/
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilIsisVcmIsVcExist                               */
/*                                                                           */
/*     DESCRIPTION      : This function will return whether the given        */
/*                        context exist or not.                              */
/*                                                                           */
/*     INPUT            : u4SysInstIdx     - Context-Id.                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : ISIS_SUCCESS/ISIS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
UtilIsisVcmIsVcExist (UINT4 u4SysInstIdx)
{
    INT4                i4RetVal = ISIS_SUCCESS;

    if (UtilIsisGetVcmSystemModeExt (ISIS_PROTOCOL_ID) == ISIS_MI_MODE)
    {
        if (VcmIsL3VcExist (u4SysInstIdx) == VCM_FALSE)
        {
            i4RetVal = ISIS_FAILURE;
        }
        else
        {
            i4RetVal = ISIS_SUCCESS;
        }
    }
    else
    {
        if (u4SysInstIdx == ISIS_DEFAULT_CXT_ID)
        {
            i4RetVal = ISIS_SUCCESS;
        }
        else
        {
            i4RetVal = ISIS_FAILURE;
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilIsisIsVcmSwitchExist                           */
/*                                                                           */
/*     DESCRIPTION      : This function check whether the entry is present   */
/*                        for the correponding Switch-name in VCM.           */
/*                        if yes it will                                     */
/*                        return the Context-Id of the Switch.               */
/*                                                                           */
/*     INPUT            : pu1Alias   - Name of the Switch.                   */
/*                                                                           */
/*     OUTPUT           : pu4VcNum   - Context-Id of the switch              */
/*                                                                           */
/*     RETURNS          : ISIS_SUCCESS/ISIS_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilIsisIsVcmSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{

    if (UtilIsisGetVcmSystemMode (ISIS_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmIsVrfExist (pu1Alias, pu4VcNum) == VCM_FALSE)
        {
            return ISIS_FAILURE;
        }
    }
    else
    {
        if (STRCMP (pu1Alias, "default") == 0)
        {
            *pu4VcNum = ISIS_DEFAULT_CXT_ID;
        }
        else
        {
            *pu4VcNum = ISIS_INVALID_CXT_ID;
            return ISIS_FAILURE;
        }
    }
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilIsisGetVcmAliasName                            */
/*                                                                           */
/*     DESCRIPTION      : This function will return the alias name of the    */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4SysInstIdx     - Context-Id.                      */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : ISIS_SUCCESS / ISIS_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilIsisGetVcmAliasName (UINT4 u4SysInstIdx, UINT1 *pu1Alias)
{
    if (UtilIsisGetVcmSystemMode (ISIS_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmGetAliasName (u4SysInstIdx, pu1Alias) == VCM_FAILURE)
        {
            return ISIS_FAILURE;
        }
    }
    else
    {
        if (u4SysInstIdx == ISIS_DEFAULT_CXT_ID)
        {
            STRCPY (pu1Alias, "default");
        }
        else
        {
            return ISIS_FAILURE;
        }
    }
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilIsisGetVcmSystemMode                           */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_SI_MODE or VCM_MI_MODE                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilIsisGetVcmSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilIsisGetVcmSystemModeExt                        */
/*                                                                           */
/*     DESCRIPTION      : This function calls the VCM Module to get the      */
/*                        mode of the system (SI / MI).                      */
/*                                                                           */
/*     INPUT            : u2ProtocolId - Protocol Identifier                 */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_SI_MODE or VCM_MI_MODE                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilIsisGetVcmSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilOspfSetContext                                         */
/*                                                                           */
/* Description  : This function sets the current ospf                        */
/*                context pointer (gOsRtr.pOspfCxt)                          */
/*                and current ospf context ID (gOsRtr.u4OspfCxtId)           */
/*                 for the given ospf Context ID                             */
/*                                                                           */
/* Input        :  u4OspfCxtId  : Ospf Context Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_FAILURE/SNMP_SUCCESS                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilIsisSetContext (UINT4 u4SysInstIdx)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4SysInst = u4SysInstIdx;

    /* If the system is operating in SI mode, the global pointer
     * is always set to the default context pointer. This function
     * should act as a dummy function in SI mode
     */
    if (UtilIsisGetVcmSystemModeExt (ISIS_PROTOCOL_ID) == ISIS_SI_MODE)
    {
        return i4RetVal;
    }
    /* Check whether the context id exists in VCM */

    if (UtilIsisVcmIsVcExist (u4SysInst) == ISIS_FAILURE)
    {
        i4RetVal = ISIS_FAILURE;
    }
    if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
    {
        i4RetVal = ISIS_FAILURE;
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIsisGetNextCxtId                                       */
/*                                                                           */
/* Description  : This function finds the first valid ISIS context ID        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pu4IsisCxtId                                               */
/*                                                                           */
/* Returns      : ISIS_FAILURE/ISIS_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
UtilIsisGetFirstCxtId (UINT4 *pu4IsisCxtId)
{
    tIsisSysContext    *pContext;

    pContext = gpIsisInstances;
    if (pContext != NULL)
    {
        *pu4IsisCxtId = pContext->u4SysInstIdx;
        return ISIS_SUCCESS;
    }
    return ISIS_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : UtilIsisGetNextCxtId                                       */
/*                                                                           */
/* Description  : This function finds the next valid ISIS context ID        */
/*                                                                           */
/* Input        : pu4IsisCxtId                                               */
/*                                                                           */
/* Output       : pu4IsisiNextCxtId                                          */
/*                                                                           */
/* Returns      : ISIS_FAILURE/ISIS_SUCCESS.                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
UtilIsisGetNextCxtId (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4CurrIdx = u4ContextId;
    UINT4               u4NextIdx = ISIS_SIZING_CONTEXT_COUNT;
    tIsisSysContext    *pContext = NULL;
    INT4                i4RetVal = ISIS_FAILURE;

    pContext = ISIS_GET_CONTEXT ();

    if (u4CurrIdx < ISIS_SIZING_CONTEXT_COUNT)
    {
        while (pContext != NULL)
        {
            if ((pContext->u4SysInstIdx > u4CurrIdx)
                && (pContext->u4SysInstIdx < u4NextIdx))
            {
                u4NextIdx = pContext->u4SysInstIdx;
                *pu4NextContextId = u4NextIdx;
                i4RetVal = ISIS_SUCCESS;
                break;
            }
            pContext = pContext->pNext;
        }
    }

    if (pContext == NULL)
    {
        /* No context is created */
        *pu4NextContextId = ISIS_INVALID_CXT_ID;
        i4RetVal = ISIS_FAILURE;
    }
    return i4RetVal;
}

#ifdef ROUTEMAP_WANTED
/**************************************************************************/
/*   Function Name   : IsisApplyInFilter                                  */
/*   Description     : This function will check whether the route         */
/*                     can be added or dropped.                           */
/*                     If route is permitted then returned                */
/*                     ISIS_SUCCESS else return ISIS_FAILURE.             */
/*                                                                        */
/*   Input(s)        : pFilterRMap      - route map data                  */
/*                     pRtInfo          - ptr to the routing update       */
/*                                                                        */
/*   Output(s)       : None.                                              */
/*                                                                        */
/*   Return Value    : ISIS_FAILURE / ISIS_SUCCESS                        */
/**************************************************************************/
PUBLIC INT1
IsisApplyInFilter (tFilteringRMap * pFilterRMap, tIsisRouteInfo * pRtInfo,
                   UINT1 u1MetricType)
{
    tRtMapInfo          RtInfoIn;
    tRtMapInfo          RtInfoOut;
    UINT4               u4Address = 0;

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));
    MEMSET (&RtInfoOut, 0, sizeof (tRtMapInfo));

    if ((pFilterRMap == NULL) || (pRtInfo == NULL))
    {
        return ISIS_SUCCESS;
    }

/*  If status of route map is disable then filtering is not invoked */
    if (pFilterRMap->u1Status == FILTERNIG_STAT_DISABLE)
    {
        return ISIS_SUCCESS;
    }
    if (pRtInfo->DestIpAddr.u1AddrType == ISIS_ADDR_IPV4)
    {
        /*Copying Destination IP */
        MEMCPY (RtInfoIn.DstXAddr.au1Addr, pRtInfo->DestIpAddr.au1IpAddr,
                IPVX_IPV4_ADDR_LEN);
        MEMCPY (&u4Address, RtInfoIn.DstXAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4Address = OSIX_HTONL (u4Address);
        MEMCPY (RtInfoIn.DstXAddr.au1Addr, &u4Address, IPVX_IPV4_ADDR_LEN);

        /*Copying Nexht-hop */
        MEMCPY (RtInfoIn.NextHopXAddr.au1Addr, pRtInfo->au1IPAddr,
                IPVX_IPV4_ADDR_LEN);
        MEMCPY (&u4Address, RtInfoIn.NextHopXAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4Address = OSIX_HTONL (u4Address);
        MEMCPY (RtInfoIn.NextHopXAddr.au1Addr, &u4Address, IPVX_IPV4_ADDR_LEN);
    }
    else if (pRtInfo->DestIpAddr.u1AddrType == ISIS_ADDR_IPV6)
    {
        MEMCPY (RtInfoIn.DstXAddr.au1Addr, pRtInfo->DestIpAddr.au1IpAddr,
                IPVX_IPV6_ADDR_LEN);

        /*Copying the next-hop address */
        MEMCPY (RtInfoIn.NextHopXAddr.au1Addr, pRtInfo->au1IPAddr,
                IPVX_IPV6_ADDR_LEN);
    }

    RtInfoIn.DstXAddr.u1Afi = pRtInfo->DestIpAddr.u1AddrType;
    RtInfoIn.u2DstPrefixLen = (UINT2) pRtInfo->DestIpAddr.u1PrefixLen;
    RtInfoIn.u1MetricType = u1MetricType;
    RtInfoIn.i4Metric = (INT4) pRtInfo->u4MetricVal;

    /*Copying next-hop details */
    RtInfoIn.NextHopXAddr.u1Afi = RtInfoIn.DstXAddr.u1Afi;
    RtInfoIn.NextHopXAddr.u1AddrLen = pRtInfo->DestIpAddr.u1Length;

/*  Ignore modification of route attributes by route map */

    if (RMAP_ROUTE_DENY != RMapApplyRule (&RtInfoIn, &RtInfoOut,
                                          pFilterRMap->
                                          au1DistInOutFilterRMapName))
    {
        pRtInfo->u4MetricVal = (UINT4) RtInfoOut.i4Metric;
        return ISIS_SUCCESS;
    }
    else
    {
        return ISIS_FAILURE;
    }
}

#endif /* ROUTEMAP_WANTED */
/*****************************************************************************/
/*                                                                           */
/* Function     : IsisUtlAllocMemGRInfoList                                  */
/*                                                                           */
/* Description  : This function allocates the memory from the required pool  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pu1GrCxtinfo                                               */
/*                                                                           */
/* Returns      : pu1GrCxtinfo                                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC tIsisGrCxtInfo **
IsisUtlAllocMemGRInfoList ()
{
    tIsisGrCxtInfo    **pu1GrCxtinfo = NULL;
    pu1GrCxtinfo =
        (tIsisGrCxtInfo **) MemAllocMemBlk (gIsisMemPoolId.u4GrCxtInfoPid);
    return pu1GrCxtinfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisUtlAllocMemGRInfo                                      */
/*                                                                           */
/* Description  : This function allocates the memory from the required pool  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pu1GrCxtinfo                                               */
/*                                                                           */
/* Returns      : pu1GrCxtinfo                                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC tIsisGrCxtInfo *
IsisUtlAllocMemGRInfo ()
{
    tIsisGrCxtInfo     *pu1GrCxtinfo = NULL;
    pu1GrCxtinfo =
        (tIsisGrCxtInfo *) MemAllocMemBlk (gIsisMemPoolId.u4GrCxtpid);
    return pu1GrCxtinfo;
}

/******************************************************************************
 * Function     : IsisUtlSPTNodeCmp
 * Description  : This routine is the RBTree comparison function used for
 *                containing the SPT nodes. The nodes are compared based on
 *                their Destination Id.
 * Inputs       : pRBNode   - Pointer to SPT node in the tree
 *                pRBNodeIn - Pointer to SPT node to be added
 * Outputs      : -1/0/1
 *****************************************************************************/
PUBLIC INT4
IsisUtlSPTNodeCmp (tRBElem * pRBNode, tRBElem * pRBNodeIn)
{
    tIsisSPTNode       *pSptNode = (tIsisSPTNode *) pRBNode;
    tIsisSPTNode       *pSptNodeIn = (tIsisSPTNode *) pRBNodeIn;
    INT4                i4RetVal;

    i4RetVal = MEMCMP (pSptNode->au1DestID, pSptNodeIn->au1DestID,
                       (2 * ISIS_MAX_IP_ADDR_LEN));
    return i4RetVal;
}

 /*MISIS*/
/******************************************************************************
 * Function     : IsisUtlSetMTIdfromLSP
 * Description  : This function is to set the MT IDs received in the LSP packet to the LSP Info
 * Inputs       : pContext - Context identifier
 *                pu1LSP   - LSP buffer
 *                pLSPRec  - LSP record
 * Outputs      : None
 * Returns      : None
 *****************************************************************************/
    PUBLIC VOID
IsisUtlSetMTIdfromLSP (tIsisSysContext * pContext, UINT1 *pu1LSP,
                       tIsisLSPEntry * pLSPRec)
{
    INT1                i1Len = 0;
    UINT1               u1TlvLen = 0;
    UINT2               u2TempMTId = 0;
    UINT2               u2Offset = 0;
    UINT1               u1MTId = 0;
    UINT1               u1MtTlvFound = ISIS_FALSE;

    if (pContext->u1IsisMTSupport == ISIS_FALSE)
    {
        pLSPRec->u1MtId = u1MTId;
        return;
    }
    /*check only in non pseudonode LSP */
    if (((UINT1) *(pu1LSP + ISIS_OFFSET_PNODEID)) == 0)
    {
        /*checkin ZEROLSP */
        if (((UINT1) *(pu1LSP + ISIS_OFFSET_LSPNUM)) == 0)
        {
            while (IsisUtlGetNextTlvOffset (pu1LSP, ISIS_MT_TLV, &u2Offset,
                                            &u1TlvLen) != ISIS_FAILURE)
            {
                /* More than one MT can be included in a single TLV.
                 *
                 */
                u1MtTlvFound = ISIS_TRUE;
                i1Len = (INT1) u1TlvLen;
                while (i1Len > 0)
                {
                    MEMCPY (&u2TempMTId, (pu1LSP + u2Offset), ISIS_MT_ID_LEN);
                    u2TempMTId = OSIX_HTONS (u2TempMTId);
                    u2TempMTId = (u2TempMTId & ISIS_MTID_MASK);

                    if (u2TempMTId == ISIS_IPV4_UNICAST_MT_ID)
                    {
                        u1MTId |= ISIS_MT0_BITMAP;
                    }
                    else if (u2TempMTId == ISIS_IPV6_UNICAST_MT_ID)
                    {
                        u1MTId |= ISIS_MT2_BITMAP;
                    }
                    u2Offset += (UINT2) 2;
                    i1Len = i1Len - 2;    /*Diab */
                }
            }
            if (u1MtTlvFound == ISIS_FALSE)
            {
                /* when MT is enabled in the local router, it is expected that the other ISIS 
                 * routers also enabled with MT support. So when a LSP received without any MT-TLV
                 * it is assumed that it supports Multi-topology and it supports MT-ID 0,
                 * since it is optional to include MT-0 in LSP
                 */
                u1MTId = ISIS_MT0_BITMAP;
            }
        }
        /*checkin NONZEROLSP */
        else
        {
            if (IsisUtlGetNextTlvOffset
                (pu1LSP, ISIS_EXT_IP_REACH_TLV, &u2Offset,
                 &u1TlvLen) != ISIS_FAILURE)
            {
                u1MTId |= ISIS_MT0_BITMAP;
            }
            u2Offset = 0;
            u1TlvLen = 0;
            if (IsisUtlGetNextTlvOffset
                (pu1LSP, ISIS_MT_IPV6_REACH_TLV, &u2Offset,
                 &u1TlvLen) != ISIS_FAILURE)
            {
                MEMCPY (&u2TempMTId, (pu1LSP + u2Offset), ISIS_MT_ID_LEN);
                u2TempMTId = OSIX_HTONS (u2TempMTId);
                u2TempMTId = (u2TempMTId & ISIS_MTID_MASK);

                if (u2TempMTId == ISIS_IPV6_UNICAST_MT_ID)
                {
                    u1MTId |= ISIS_MT2_BITMAP;
                }
            }

        }
    }
    pLSPRec->u1MtId = u1MTId;
    return;
}

/******************************************************************************
* Function     : IsisUtlUpdateMetrics
* Description  : This function is used to copy the default metric to full 
*                metric when enabling multi-topology.
*                If error/expense/delay metric is configured, those will
*                be set to default values.
* Inputs       : pContext - Context identifier
* Outputs      : None
* Returns      : None
*****************************************************************************/
PUBLIC VOID
IsisUtlUpdateMetrics (tIsisSysContext * pContext)
{
    INT4                i4CktIndx = 0;
    INT4                i4CktLvl = 0;
    INT4                i4CktLvlIndx = 0;
    INT4                i4Metric = 0;
    INT4                i4SysInstance = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Metric = 0;
    UINT4               u4Count = 0;
    UINT4               u4RrdMetricVal = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisIPRAEntry     *pIPRAEntry = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tSNMP_OCTET_STRING_TYPE SummaryAddr;
    INT1                i1Ret = SNMP_FAILURE;
    INT1                i1L1AndL2 = ISIS_FALSE;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlUpdateMetrics ()\n"));

    MEMSET (&SummaryAddr, 0, sizeof (SummaryAddr));

    i4SysInstance = (INT4) pContext->u4SysInstIdx;
    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        /* Resetting the error/expense/delay metrics, since those
         * will not be supported in Wide Metric Style */
        if ((pContext->SysActuals.u1SysMetricSupp & ISIS_DEL_MET_SUPP_FLAG))
        {
            pContext->SysActuals.u1SysMetricSupp &= ~ISIS_DEL_MET_SUPP_FLAG;
        }

        if ((pContext->SysActuals.u1SysMetricSupp & ISIS_ERR_MET_SUPP_FLAG))
        {
            pContext->SysActuals.u1SysMetricSupp &= ~ISIS_ERR_MET_SUPP_FLAG;
        }

        if ((pContext->SysActuals.u1SysMetricSupp & ISIS_EXP_MET_SUPP_FLAG))
        {
            pContext->SysActuals.u1SysMetricSupp &= ~ISIS_EXP_MET_SUPP_FLAG;
        }
    }
    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        pCktRec = pContext->CktTable.pCktRec;
        for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
             pCktRec = pCktRec->pNext)
        {
            i4CktIndx = (INT4) pCktRec->u4CktIdx;
            i4CktLvl = (INT4) pCktRec->u1CktLevel;
            if (i4CktLvl == ISIS_LEVEL12)
            {
                i1L1AndL2 = ISIS_TRUE;
                i4CktLvlIndx = ISIS_LEVEL1;
            }
            else
            {
                i4CktLvlIndx = i4CktLvl;
            }

            while (i4CktLvlIndx)
            {
                pCktLvlEntry = (i4CktLvlIndx == ISIS_LEVEL1) ?
                    (pCktRec->pL1CktInfo) : (pCktRec->pL2CktInfo);

                /* Verifying if default-metric is configured to any
                 * non-default value */
                i1Ret = nmhGetIsisCircLevelMetric (i4SysInstance, i4CktIndx,
                                                   i4CktLvlIndx, &i4Metric);
                if ((i1Ret != SNMP_FAILURE) &&
                    (i4Metric != 0) && (i4Metric != ISIS_CKTL_DEF_METRIC_VALUE))
                {
                    i1Ret = nmhSetFsIsisExtCircLevelWideMetric (i4SysInstance,
                                                                i4CktIndx,
                                                                i4CktLvlIndx,
                                                                (UINT4)
                                                                i4Metric);
                    if (i1Ret != SNMP_SUCCESS)
                    {
                        WARNING ((ISIS_LGST,
                                  "UTL <E> : Unable to set the WideMetric for "
                                  "Instance %d Circuit Indx %d Circuit Level %d\n",
                                  i4SysInstance, i4CktIndx, i4CktLvlIndx));
                        UTP_EE ((ISIS_LGST,
                                 "UTL <X> : Exiting IsisUtlUpdateMetrics()\n"));
                        return;
                    }
                }

                MEMSET (&pCktLvlEntry->Metric, 0, sizeof (tIsisMetric));
                /* updating the default-metric to default value */
                pCktLvlEntry->Metric[0] = ISIS_CKTL_DEF_METRIC_VALUE;

                if ((i4CktLvlIndx == ISIS_LEVEL2) || (i1L1AndL2 == ISIS_FALSE))
                {
                    break;
                }
                if (i1L1AndL2 == ISIS_TRUE)
                {
                    i1L1AndL2 = ISIS_FALSE;
                    i4CktLvlIndx = ISIS_LEVEL2;
                }
            }
        }

        i4Metric = 0;
        /* IPRA entries */
        pIPRAEntry = pContext->IPRAInfo.pIPRAEntry;
        while (pIPRAEntry != NULL)
        {
            if (pIPRAEntry->u1IPRAType != ISIS_AUTO_TYPE)
            {
                i1Ret = nmhGetIsisIPRAMetric (i4SysInstance,
                                              pIPRAEntry->u1IPRAType,
                                              pIPRAEntry->u4IPRAIdx, &i4Metric);
                if ((i1Ret != SNMP_FAILURE) &&
                    (i4Metric != 0) && (i4Metric != ISIS_IPRA_DEF_METRIC_VALUE))
                {
                    i1Ret = nmhSetFsIsisExtIPRAFullMetric (i4SysInstance,
                                                           pIPRAEntry->
                                                           u1IPRAType,
                                                           pIPRAEntry->
                                                           u4IPRAIdx,
                                                           (UINT4) i4Metric);
                    if (i1Ret != SNMP_SUCCESS)
                    {
                        WARNING ((ISIS_LGST,
                                  "UTL <E> : Unable to set IPRA full metric for "
                                  "Instance %d IPRA Indx %d\n",
                                  i4SysInstance, pIPRAEntry->u4IPRAIdx));
                        UTP_EE ((ISIS_LGST,
                                 "UTL <X> : Exiting IsisUtlUpdateMetrics()\n"));
                        return;
                    }
                }
                MEMSET (&(pIPRAEntry->Metric), 0, sizeof (tIsisMetric));
                /* updating the default-metric to default value */
                pIPRAEntry->Metric[0] = ISIS_IPRA_DEF_METRIC_VALUE;
            }
            pIPRAEntry = pIPRAEntry->pNext;
        }

        i4Metric = 0;
        /* Summary address entries */
        pSAEntry = pContext->SummAddrTable.pSAEntry;
        while (pSAEntry != NULL)
        {
            if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
            {
                SummaryAddr.i4_Length = ISIS_MAX_IPV4_ADDR_LEN;
            }
            else
            {
                SummaryAddr.i4_Length = ISIS_MAX_IPV6_ADDR_LEN;
            }
            SummaryAddr.pu1_OctetList = pSAEntry->au1SummAddr;

            i1Ret = nmhGetIsisSummAddrMetric (i4SysInstance,
                                              pSAEntry->u1AddrType,
                                              &SummaryAddr,
                                              pSAEntry->u1PrefixLen, &i4Metric);
            if ((i1Ret != SNMP_FAILURE) &&
                (i4Metric != 0) && (i4Metric != ISIS_IPRA_DEF_METRIC_VALUE))
            {
                i1Ret = nmhSetFsIsisExtSummAddrFullMetric (i4SysInstance,
                                                           pSAEntry->u1AddrType,
                                                           &SummaryAddr,
                                                           pSAEntry->
                                                           u1PrefixLen,
                                                           (UINT4) i4Metric);
                if (i1Ret != SNMP_SUCCESS)
                {
                    WARNING ((ISIS_LGST,
                              "UTL <E> : Unable to set SummAddr full metric "
                              "for Instance %d \n", i4SysInstance));
                    UTP_EE ((ISIS_LGST,
                             "UTL <X> : Exiting IsisUtlUpdateMetrics()\n"));
                    return;
                }
            }
            MEMSET (&(pSAEntry->Metric), 0, sizeof (tIsisMetric));
            /* updating the default-metric to default value */
            pSAEntry->Metric[0] = ISIS_IPRA_DEF_METRIC_VALUE;
            pSAEntry = pSAEntry->pNext;
        }

    }
    else
    {
        pCktRec = pContext->CktTable.pCktRec;
        for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
             pCktRec = pCktRec->pNext)
        {
            i4CktIndx = (INT4) pCktRec->u4CktIdx;
            i4CktLvl = (INT4) pCktRec->u1CktLevel;
            if (i4CktLvl == ISIS_LEVEL12)
            {
                i1L1AndL2 = ISIS_TRUE;
                i4CktLvlIndx = ISIS_LEVEL1;
            }
            else
            {
                i4CktLvlIndx = i4CktLvl;
            }

            while (i4CktLvlIndx)
            {
                pCktLvlEntry = (i4CktLvlIndx == ISIS_LEVEL1) ?
                    (pCktRec->pL1CktInfo) : (pCktRec->pL2CktInfo);

                /* Verifying if full-metric is configured to any
                 * non-default value */
                u4Metric = pCktLvlEntry->u4FullMetric;
                if ((u4Metric != 0) &&
                    (u4Metric != ISIS_CKTL_DEF_METRIC_VALUE) &&
                    (u4Metric <= ISIS_LL_CKTL_MAX_METRIC))
                {
                    i1Ret = nmhSetIsisCircLevelMetric (i4SysInstance,
                                                       i4CktIndx,
                                                       i4CktLvlIndx,
                                                       (INT4) u4Metric);
                    if (i1Ret != SNMP_SUCCESS)
                    {
                        WARNING ((ISIS_LGST,
                                  "UTL <E> : Unable to set the WideMetric for "
                                  "Instance %d Circuit Indx %d Circuit Level %d\n",
                                  i4SysInstance, i4CktIndx, i4CktLvlIndx));
                        UTP_EE ((ISIS_LGST,
                                 "UTL <X> : Exiting IsisUtlUpdateMetrics()\n"));
                        return;
                    }
                }
                else if (u4Metric > ISIS_LL_CKTL_MAX_METRIC)
                {
                    pCktLvlEntry->Metric[0] = ISIS_LL_CKTL_MAX_METRIC;
                }
                pCktLvlEntry->u4FullMetric = ISIS_CKTL_DEF_METRIC_VALUE;

                if ((i4CktLvlIndx == ISIS_LEVEL2) || (i1L1AndL2 == ISIS_FALSE))
                {
                    break;
                }
                if (i1L1AndL2 == ISIS_TRUE)
                {
                    i1L1AndL2 = ISIS_FALSE;
                    i4CktLvlIndx = ISIS_LEVEL2;
                }
            }
        }

        u4Metric = 0;
        /* IPRA entries */
        pIPRAEntry = pContext->IPRAInfo.pIPRAEntry;
        while (pIPRAEntry != NULL)
        {
            if (pIPRAEntry->u1IPRAType != ISIS_AUTO_TYPE)
            {
                u4Metric = pIPRAEntry->u4FullMetric;
                if ((u4Metric != 0) &&
                    (u4Metric != ISIS_IPRA_DEF_METRIC_VALUE) &&
                    (u4Metric <= ISIS_LL_CKTL_MAX_METRIC))
                {
                    i1Ret = nmhSetIsisIPRAMetric (i4SysInstance,
                                                  pIPRAEntry->u1IPRAType,
                                                  pIPRAEntry->u4IPRAIdx,
                                                  (INT4) u4Metric);
                    if (i1Ret != SNMP_SUCCESS)
                    {
                        WARNING ((ISIS_LGST,
                                  "UTL <E> : Unable to set IPRA full metric for "
                                  "Instance %d IPRA Indx %d\n",
                                  i4SysInstance, pIPRAEntry->u4IPRAIdx));
                        UTP_EE ((ISIS_LGST,
                                 "UTL <X> : Exiting IsisUtlUpdateMetrics()\n"));
                        return;
                    }
                }
                else if (u4Metric > ISIS_LL_CKTL_MAX_METRIC)
                {
                    pIPRAEntry->Metric[0] = ISIS_LL_CKTL_MAX_METRIC;
                }
                pIPRAEntry->u4FullMetric = ISIS_IPRA_DEF_METRIC_VALUE;
            }
            pIPRAEntry = pIPRAEntry->pNext;
        }

        u4Metric = 0;
        /* Summary address entries */
        pSAEntry = pContext->SummAddrTable.pSAEntry;
        while (pSAEntry != NULL)
        {
            if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
            {
                SummaryAddr.i4_Length = ISIS_MAX_IPV4_ADDR_LEN;
            }
            else
            {
                SummaryAddr.i4_Length = ISIS_MAX_IPV6_ADDR_LEN;
            }
            SummaryAddr.pu1_OctetList = pSAEntry->au1SummAddr;
            u4Metric = pSAEntry->u4FullMetric;
            if ((u4Metric != 0) &&
                (u4Metric != ISIS_IPRA_DEF_METRIC_VALUE) &&
                (u4Metric <= ISIS_LL_CKTL_MAX_METRIC))
            {
                i1Ret = nmhSetIsisSummAddrMetric (i4SysInstance,
                                                  pSAEntry->u1AddrType,
                                                  &SummaryAddr,
                                                  pSAEntry->u1PrefixLen,
                                                  (INT4) u4Metric);
                if (i1Ret != SNMP_SUCCESS)
                {
                    WARNING ((ISIS_LGST,
                              "UTL <E> : Unable to set SummAddr full metric "
                              "for Instance %d \n", i4SysInstance));
                    UTP_EE ((ISIS_LGST,
                             "UTL <X> : Exiting IsisUtlUpdateMetrics()\n"));
                    return;
                }
            }
            else if (u4Metric > ISIS_LL_CKTL_MAX_METRIC)
            {
                pSAEntry->Metric[0] = ISIS_LL_CKTL_MAX_METRIC;
            }
            pSAEntry->u4FullMetric = ISIS_IPRA_DEF_METRIC_VALUE;
            pSAEntry = pSAEntry->pNext;
        }
        /*Updating RRD Metric */
        for (u4Count = 0; u4Count <= ISIS_MAX_METRIC_INDEX; u4Count++)
        {
            i4RetVal = nmhGetFsIsisRRDMetricValue (i4SysInstance,
                                                   (INT4) u4Count,
                                                   &u4RrdMetricVal);
            if ((i4RetVal == SNMP_SUCCESS) &&
                (u4RrdMetricVal > ISIS_MAX_METRIC_ST))
            {
                nmhSetFsIsisRRDMetricValue (i4SysInstance,
                                            (INT4) u4Count,
                                            (UINT4) ISIS_MAX_METRIC_ST);
            }
        }
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlUpdateMetrics()\n"));
    return;
}

/******************************************************************************
* Function     : IsisUtlUpdateMetricStyle
* Description  : This function is used to update the metric style to
*                -> Wide metric - In case of multi topology
*                -> Narrow metric - In case of single topology
* Inputs       : pContext - Context identifier
*                u1MtSupport 
* Outputs      : None
* Returns      : None
*****************************************************************************/
PUBLIC VOID
IsisUtlUpdateMetricStyle (tIsisSysContext * pContext)
{
    INT4                i4MetricStyle = ISIS_STYLE_NARROW_METRIC;
    INT4                i4SysInstance = 0;
    UINT4               u4Error = SNMP_FAILURE;
    INT4                i4SysType = 0;
    INT4                i4InstIdx = 0;
    INT1                i1Ret = SNMP_FAILURE;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlUpdateMetricStyle ()\n"));

    i4SysInstance = (INT4) pContext->u4SysInstIdx;

    /* If multi-topology is to be enabled, set the metric style to wide metric
     * If multi-topology is to be disabled, set the metric sytle to narrow metric */
    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        i4MetricStyle = ISIS_STYLE_WIDE_METRIC;
    }

    i4InstIdx = (INT4) pContext->u4SysInstIdx;
    i1Ret = nmhGetIsisSysType (i4InstIdx, &i4SysType);

    if (i1Ret == SNMP_SUCCESS)
    {
        if ((i4SysType == ISIS_LEVEL1) || (i4SysType == ISIS_LEVEL12))
        {
            i1Ret = nmhTestv2IsisSysL1MetricStyle (&u4Error,
                                                   i4InstIdx, i4MetricStyle);
            if (i1Ret != SNMP_SUCCESS)
            {
                WARNING ((ISIS_LGST,
                          "UTL <E> : Invalid metric style "
                          "for Instance %d \n", i4SysInstance));
                UTP_EE ((ISIS_LGST,
                         "UTL <X> : Exiting IsisUtlUpdateMetricStyle()\n"));
                return;
            }
            i1Ret = nmhSetIsisSysL1MetricStyle (i4InstIdx, i4MetricStyle);
            if (i1Ret != SNMP_SUCCESS)
            {
                WARNING ((ISIS_LGST,
                          "UTL <E> : Unable to update metric style "
                          "for Instance %d \n", i4SysInstance));
                UTP_EE ((ISIS_LGST,
                         "UTL <X> : Exiting IsisUtlUpdateMetricStyle()\n"));
                return;
            }
        }
        if ((i4SysType == ISIS_LEVEL2) || (i4SysType == ISIS_LEVEL12))
        {
            i1Ret = nmhTestv2IsisSysL2MetricStyle (&u4Error,
                                                   i4InstIdx, i4MetricStyle);
            if (i1Ret != SNMP_SUCCESS)
            {
                WARNING ((ISIS_LGST,
                          "UTL <E> : Invalid metric style "
                          "for Instance %d \n", i4SysInstance));
                UTP_EE ((ISIS_LGST,
                         "UTL <X> : Exiting IsisUtlUpdateMetricStyle()\n"));
                return;
            }
            i1Ret = nmhSetIsisSysL2MetricStyle (i4InstIdx, i4MetricStyle);
            if (i1Ret != SNMP_SUCCESS)
            {
                WARNING ((ISIS_LGST,
                          "UTL <E> : Unable to update metric style "
                          "for Instance %d \n", i4SysInstance));
                UTP_EE ((ISIS_LGST,
                         "UTL <X> : Exiting IsisUtlUpdateMetricStyle()\n"));
                return;
            }
        }
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlUpdateMetricStyle ()\n"));
    return;
}

/******************************************************************************
* Function     : IsisUtlIsDirectlyConnected
* Description  : This function is used to find if the given IP address is
*                belongs to any of the directly connected interface or not.
* Inputs       : pCktRec - Pointer to the circuit record  
* Outputs      : None
* Returns      : ISIS_TRUE/ISIS_FALSE
*******************************************************************************/
PUBLIC INT1
IsisUtlIsDirectlyConnected (tIsisCktEntry * pCktRec, UINT1 *pu1Address)
{
    tNetIpv4IfInfo      NetIpv4IfInfo;
    UINT4               u4NetAddr;
    UINT4               u4IfIndex = 0;
    UINT4               u4Port = 0;

    u4IfIndex = pCktRec->u4CktIfIdx;
    MEMCPY (&u4NetAddr, pu1Address, ISIS_MAX_IPV4_ADDR_LEN);

    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) == NETIPV4_FAILURE)
    {
        return ISIS_FAILURE;
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) == NETIPV4_FAILURE)
    {
        return ISIS_FAILURE;
    }

    NetIpv4IfInfo.u4Addr = OSIX_HTONL (NetIpv4IfInfo.u4Addr);
    NetIpv4IfInfo.u4NetMask = OSIX_HTONL (NetIpv4IfInfo.u4NetMask);

    if ((NetIpv4IfInfo.u4Addr & NetIpv4IfInfo.u4NetMask) ==
        (u4NetAddr & NetIpv4IfInfo.u4NetMask))
    {
        return ISIS_TRUE;
    }
    return ISIS_FALSE;
}

#ifdef BFD_WANTED
/******************************************************************************
* Function     : IsisUtlUpdateBfdRequired
* Description  : This function is used to compute the ISIS_BFD_REQUIRED
*                variable for an ISIS neighbor.
* Inputs       : pContext - Context identifier
*                pCktEntry - Pointer to the Ckt Entry on which adj is added
*                pAdjEntry - Pointer to the adjacent entry
* Outputs      : None
* Returns      : None
*******************************************************************************/
PUBLIC VOID
IsisUtlUpdateBfdRequired (tIsisCktEntry * pCktEntry, tIsisAdjEntry * pAdjEntry)
{
    UINT4               u4SysInstance = 0;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1AdjMTId = 0;
    UINT1               u1Mt0BfdReqd = ISIS_BFD_FALSE;
    UINT1               u1Mt2BfdReqd = ISIS_BFD_FALSE;
    UINT1               u1Mt0Enabled = ISIS_BFD_FALSE;
    UINT1               u1Mt2Enabled = ISIS_BFD_FALSE;

    if (IsisRmGetNodeState () == RM_STANDBY)
    {
        return;
    }

    u4SysInstance = pCktEntry->pContext->u4SysInstIdx;

    u1Mt0Enabled = (pAdjEntry->u1IsisMT0BfdEnabled !=
                    ISIS_BFD_FALSE ? ISIS_BFD_TRUE : ISIS_BFD_FALSE);
    u1Mt2Enabled = (pAdjEntry->u1IsisMT2BfdEnabled !=
                    ISIS_BFD_FALSE ? ISIS_BFD_TRUE : ISIS_BFD_FALSE);

    if ((pCktEntry->u1IfMTId & ISIS_MT0_BITMAP) &&
        (pAdjEntry->u1AdjMTId & ISIS_MT0_BITMAP))
    {
        /* IPv4 is supported on both sides */
        ISIS_BFD_REQUIRED (pAdjEntry) = u1Mt0Enabled;
        u1Mt0BfdReqd = ISIS_BFD_TRUE;
    }
    if ((pCktEntry->u1IfMTId & ISIS_MT2_BITMAP) &&
        (pAdjEntry->u1AdjMTId & ISIS_MT2_BITMAP))
    {
        if (u1Mt0BfdReqd == ISIS_BFD_TRUE)
        {
            /* Both IPv4 and IPv6 are supported on both sides */
            ISIS_BFD_REQUIRED (pAdjEntry) = u1Mt0Enabled && u1Mt2Enabled;
        }
        else
        {
            /* Only IPv6 is supported on both sides */
            ISIS_BFD_REQUIRED (pAdjEntry) = u1Mt2Enabled;
        }
        u1Mt2BfdReqd = ISIS_BFD_TRUE;
    }

    /* Register to BFD only if it was not registered previously */
    if (ISIS_BFD_REQUIRED (pAdjEntry) == ISIS_BFD_TRUE)
    {
        if ((u1Mt0BfdReqd == ISIS_BFD_TRUE) &&
            (pAdjEntry->u1Mt0BfdRegd == ISIS_BFD_FALSE))
        {

            i4RetVal = IsisRegisterWithBfd (u4SysInstance, pCktEntry,
                                            pAdjEntry, ISIS_IPV4_UNICAST_MT_ID);
            if (i4RetVal == ISIS_FAILURE)
            {
                WARNING ((ISIS_LGST, "UTL <E> : BFD Registration failure for"
                          "Adjacency Index %u", pAdjEntry->u4AdjIdx));
            }
            u1AdjMTId |= ISIS_MT0_BITMAP;
            pAdjEntry->u1AdjMTId = u1AdjMTId;
        }
        if ((u1Mt2BfdReqd == ISIS_BFD_TRUE) &&
            (pAdjEntry->u1Mt2BfdRegd == ISIS_BFD_FALSE))
        {
            i4RetVal = IsisRegisterWithBfd (u4SysInstance, pCktEntry,
                                            pAdjEntry, ISIS_IPV6_UNICAST_MT_ID);
            if (i4RetVal == ISIS_FAILURE)
            {
                WARNING ((ISIS_LGST, "UTL <E> : BFD Registration failure for"
                          "Adjacency Index %u", pAdjEntry->u4AdjIdx));
            }
            u1AdjMTId |= ISIS_MT2_BITMAP;
            pAdjEntry->u1AdjMTId = u1AdjMTId;
        }
    }

    /* Deregister from BFD only if it was registered previously */
    else if (ISIS_BFD_REQUIRED (pAdjEntry) == ISIS_BFD_FALSE)
    {
        if ((u1Mt0BfdReqd == ISIS_BFD_TRUE) &&
            (pAdjEntry->u1Mt0BfdRegd == ISIS_BFD_TRUE))
        {

            i4RetVal = IsisDeRegisterWithBfd (u4SysInstance, pCktEntry,
                                              pAdjEntry,
                                              ISIS_IPV4_UNICAST_MT_ID,
                                              ISIS_TRUE);
            if (i4RetVal == ISIS_FAILURE)
            {
                WARNING ((ISIS_LGST, "UTL <E> : BFD Deregistration failure for"
                          "Adjacency Index %u", pAdjEntry->u4AdjIdx));
            }
        }
        if ((u1Mt2BfdReqd == ISIS_BFD_TRUE) &&
            (pAdjEntry->u1Mt2BfdRegd == ISIS_BFD_TRUE))
        {
            i4RetVal = IsisDeRegisterWithBfd (u4SysInstance, pCktEntry,
                                              pAdjEntry,
                                              ISIS_IPV6_UNICAST_MT_ID,
                                              ISIS_TRUE);
            if (i4RetVal == ISIS_FAILURE)
            {
                WARNING ((ISIS_LGST, "UTL <E> : BFD Deregistration failure for"
                          "Adjacency Index %u", pAdjEntry->u4AdjIdx));
            }
        }
        /* This is a special case in which the neighbor has disabled
         * multi-topology. In that scenario, the neighbor would be no longer
         * supporting MT-0 or MT-2. BFD IPv4 would be deregistered since u1Mt0BfdReqd 
         * would be true since the default MT ID will be assigned to MT-0 if 
         * MT TLV is not found. BFD IPv6 deregisteration should be done explicitly
         * if neighbor suddenly disables Multi-topology */
        if ((!(pAdjEntry->u1AdjMTId & ISIS_MT2_BITMAP)) &&
            (pAdjEntry->u1Mt2BfdRegd == ISIS_BFD_TRUE))
        {
            i4RetVal = IsisDeRegisterWithBfd (u4SysInstance, pCktEntry,
                                              pAdjEntry,
                                              ISIS_IPV6_UNICAST_MT_ID,
                                              ISIS_TRUE);
            if (i4RetVal == ISIS_FAILURE)
            {
                WARNING ((ISIS_LGST, "UTL <E> : BFD Deregistration failure for"
                          "Adjacency Index %u", pAdjEntry->u4AdjIdx));
            }
        }
    }

    return;
}

/******************************************************************************
* Function     : IsisUtlUpdateBfdNeighborUseable
* Description  : This function is used to compute the ISIS_BFD_NEIGHBOR_USEABLE
*                variable for an ISIS neighbor.
* Inputs       : pContext - Context identifier
*                pCktEntry - Pointer to the Ckt Entry on which adj is added
*                pAdjEntry - Pointer to the adjacent entry
* Outputs      : None
* Returns      : None
*******************************************************************************/
PUBLIC VOID
IsisUtlUpdateBfdNeighborUseable (tIsisCktEntry * pCktEntry,
                                 tIsisAdjEntry * pAdjEntry)
{
    if ((pCktEntry->u1IfMTId & ISIS_MT0_BITMAP) &&
        (pCktEntry->u1IfMTId & ISIS_MT2_BITMAP))
    {
        /* If both MT-0 and MT-2 are supported on the circuit */
        if ((pAdjEntry->u1IsisMT0BfdState == ISIS_BFD_SESS_UP) ||
            (pAdjEntry->u1IsisMT2BfdState == ISIS_BFD_SESS_UP))
        {
            ISIS_BFD_NEIGHBOR_USEABLE (pAdjEntry) = ISIS_BFD_TRUE;
        }
        else
        {
            ISIS_BFD_NEIGHBOR_USEABLE (pAdjEntry) = ISIS_BFD_FALSE;
        }
    }
    else if (pCktEntry->u1IfMTId & ISIS_MT0_BITMAP)
    {
        /* If only MT-0 is supported on the circuit */
        ISIS_BFD_NEIGHBOR_USEABLE (pAdjEntry) = (pAdjEntry->u1IsisMT0BfdState ==
                                                 ISIS_BFD_SESS_UP ?
                                                 ISIS_BFD_TRUE :
                                                 ISIS_BFD_FALSE);
    }
    else if (pCktEntry->u1IfMTId & ISIS_MT2_BITMAP)
    {
        /* If only MT-2 is supported on the circuit */
        ISIS_BFD_NEIGHBOR_USEABLE (pAdjEntry) = (pAdjEntry->u1IsisMT2BfdState ==
                                                 ISIS_BFD_SESS_UP ?
                                                 ISIS_BFD_TRUE :
                                                 ISIS_BFD_FALSE);
    }

    /* Reset the MT Ids if the BFD session becomes down */
    if (pAdjEntry->u1IsisMT0BfdState == ISIS_BFD_SESS_DOWN)
    {
        pAdjEntry->u1AdjMTId =
            (UINT1) (pAdjEntry->u1AdjMTId & ~ISIS_MT0_BITMAP);
    }
    if (pAdjEntry->u1IsisMT2BfdState == ISIS_BFD_SESS_DOWN)
    {
        pAdjEntry->u1AdjMTId =
            (UINT1) (pAdjEntry->u1AdjMTId & ~ISIS_MT2_BITMAP);
    }

    if (ISIS_BFD_NEIGHBOR_USEABLE (pAdjEntry) == ISIS_BFD_TRUE)
    {
        pAdjEntry->u1DoNotUpdHoldTmr = ISIS_BFD_FALSE;
    }

#ifdef ISIS_FT_ENABLED
    ISIS_FLTR_ADJ_LSU (pCktEntry->pContext, ISIS_CMD_ADD, pAdjEntry);
#endif
    return;
}

/******************************************************************************
* Function     : IsisUtlProcessAdjUpNotification
* Description  : This function is used to process the session up notification
*                from BFD. 
* Inputs       : pContext  - Pointer to the system instance
*                pAdjEntry - Pointer to the adjacent entry
*                pCktEntry - Pointer to the Ckt Entry
*                u1MtId    - MT ID for which BFD notification is received
* Outputs      : None
* Returns      : None
*******************************************************************************/
PUBLIC VOID
IsisUtlProcessAdjUpNotification (tIsisSysContext * pContext,
                                 tIsisAdjEntry * pAdjEntry,
                                 tIsisCktEntry * pCktEntry, UINT1 u1MtId)
{
    tIsisEvtDISElect   *pDISEvt = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    UINT1               u1SchedDIS = ISIS_FALSE;
    UINT1               u1Level = 0;

    /* Update the BFD session state in AdjEntry */
    UTP_PT ((ISIS_LGST, "UTL <T> :BFD Session state changes to UP on circuit"
             " [Id: %d IfIndex:%d] for MT [%d] \n", pCktEntry->u4CktIdx,
             pCktEntry->u4CktIfIdx, u1MtId));
    IsisUtlUpdateBfdState (pAdjEntry, u1MtId, ISIS_BFD_SESS_UP);

    /* Update the neighbor useable parameter for this AdjEntry */
    IsisUtlUpdateBfdNeighborUseable (pCktEntry, pAdjEntry);

    pCktLevel = (pCktEntry->u1CktType == ISIS_P2P_CKT)
        ? ISIS_GET_P2P_CKT_LEVEL (pCktEntry)
        : ((pAdjEntry->u1AdjUsage == ISIS_LEVEL1) ? pCktEntry->pL1CktInfo
           : pCktEntry->pL2CktInfo);

    pAdjEntry->u1DoNotUpdHoldTmr = ISIS_BFD_FALSE;
    if (ISIS_GET_ADJ_STATE (pAdjEntry) != ISIS_ADJ_UP)
    {
        /* Adjacency state has changed, update it
         */
        if ((pCktEntry->u1CktType == ISIS_BC_CKT) ||
            (pCktEntry->u1P2PDynHshkMachanism == ISIS_P2P_TWO_WAY))
        {
            pAdjEntry->u1AdjState = ISIS_ADJ_UP;

            /* Adjacency has changed state to UP, post Adjacency UP event to 
             * Control Module
             */
            ISIS_INCR_ADJ_CHG_STAT (pCktEntry);
            IsisAdjEnqueueAdjChgEvt (pCktEntry, pCktLevel, pAdjEntry,
                                     ISIS_ADJ_UP);
            pCktLevel->u4NumAdjs++;

            if (pCktLevel->u4NumAdjs == 1)
            {
                if (pAdjEntry->u1AdjUsage != ISIS_LEVEL2)
                {
                    IsisUtlSetBPat (&pContext->CktTable.pu1L1CktMask,
                                    pCktEntry->u4CktIdx);

                    /* First adjacency on the given circuit has come up. 
                     * We can now start the LSP transmission timer 
                     */

                    IsisTmrStartECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                         pCktEntry->u4CktIdx, 1,
                                         &pCktEntry->pL1CktInfo->u1LSPTxTmrIdx);
                }

                if (pAdjEntry->u1AdjUsage != ISIS_LEVEL1)
                {
                    IsisUtlSetBPat (&pContext->CktTable.pu1L2CktMask,
                                    pCktEntry->u4CktIdx);

                    /* First adjacency on the given circuit has come up. 
                     * We can now start the LSP transmission timer 
                     */

                    IsisTmrStartECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                         pCktEntry->u4CktIdx, 1,
                                         &pCktEntry->pL2CktInfo->u1LSPTxTmrIdx);
                }
            }

            if (pAdjEntry->u1AdjUsage != ISIS_LEVEL2)
            {
                (pContext->CktTable.u4NumL1Adjs)++;
            }
            if (pAdjEntry->u1AdjUsage != ISIS_LEVEL1)
            {
                (pContext->CktTable.u4NumL2Adjs)++;
            }
            if (pCktEntry->u1CktType == ISIS_BC_CKT)
            {
                u1SchedDIS = ISIS_TRUE;
            }
        }
        else
        {
            pAdjEntry->u1P2PThreewayState = ISIS_ADJ_INIT;
        }
    }

#ifdef ISIS_FT_ENABLED
    ISIS_FLTR_ADJ_LSU (pCktEntry->pContext, ISIS_CMD_ADD, pAdjEntry);
#endif

    /* Get the Direction Entry, which is required for restarting the Holding
     * Timer
     */
    if (pAdjEntry->u1IsisGRHelperStatus != ISIS_GR_HELPING)
    {
        pDirEntry = pContext->AdjDirTable.pDirEntry;

        while (pDirEntry != NULL)
        {
            if (pDirEntry->pAdjEntry == pAdjEntry)
            {
                /* Restarting the timer for Holding time. Timer Index stored
                 * in Adjacency Record is passed to the timer Module, so that
                 * its easier to locate the record in the Equivalence Class of Timer
                 * and the block where the record is held
                 */
                IsisTmrRestartECTimer (pCktEntry->pContext, ISIS_ECT_HOLDING,
                                       pDirEntry->u4DirIdx,
                                       pAdjEntry->u2HoldTime,
                                       &(pAdjEntry->u1TmrIdx));
                break;
            }
            pDirEntry = pDirEntry->pNext;
        }
        if ((pDirEntry == NULL) &&
            ((pCktEntry->u1CktType != ISIS_P2P_CKT)
             || (pCktEntry->u1P2PDynHshkMachanism == ISIS_P2P_TWO_WAY)))
        {
            /*Direction entry not found for the adjacency so create one */
            pDirEntry = (tIsisAdjDirEntry *)
                ISIS_MEM_ALLOC (ISIS_BUF_ADIR, sizeof (tIsisAdjDirEntry));

            if (pDirEntry != NULL)
            {
                IsisAdjAddDirEntry (pContext, pAdjEntry, pDirEntry);
            }
        }
    }

    if (u1SchedDIS == ISIS_TRUE)
    {
        /* Elect a DIS from the List of available adjacencies
         */

        pDISEvt = (tIsisEvtDISElect *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDISElect));
        if (pDISEvt != NULL)
        {
            pDISEvt->u1EvtID = ISIS_EVT_ELECT_DIS;
            u1Level = ((pAdjEntry->u1AdjUsage == ISIS_LEVEL1) ?
                       ISIS_LEVEL1 : ISIS_LEVEL2);
            pDISEvt->u1CktLevel = u1Level;
            pDISEvt->u4CktIdx = pCktEntry->u4CktIdx;

            /* Posting DIS Schedule Event
             */
            IsisUtlSendEvent (pContext, (UINT1 *) pDISEvt,
                              sizeof (tIsisEvtDISElect));
        }
        else
        {
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_EVTS);
        }
    }
    return;
}

/******************************************************************************
* Function     : IsisUtlProcessAdjDownNotification
* Description  : This function is used to process the session down notification
*                from BFD.
* Inputs       : pContext  - Pointer to the system instance
*                pAdjEntry - Pointer to the adjacent entry
*                pCktEntry - Pointer to the Ckt Entry
*                u1MtId    - MT ID for which BFD notification is received
* Outputs      : None
* Returns      : None
*******************************************************************************/
PUBLIC VOID
IsisUtlProcessAdjDownNotification (tIsisSysContext * pContext,
                                   tIsisAdjEntry * pAdjEntry,
                                   tIsisCktEntry * pCktEntry, UINT1 u1MtId)
{
    INT4                i4RetVal = 0;
    UINT4               u4DirIdx = 0;
    UINT1               u1TmrIdx = 0;
    UINT1               u1IsOnlyNbr = 0;

    UTP_PT ((ISIS_LGST, "UTL <T> :BFD Session state changes to DOWN on circuit"
             " [Id: %d IfIndex:%d] for MT [%d]\n", pCktEntry->u4CktIdx,
             pCktEntry->u4CktIfIdx, u1MtId));
    /* If multi-topology is enabled, 
     * Update the BFD session state in AdjEntry */
    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        IsisUtlUpdateBfdState (pAdjEntry, u1MtId, ISIS_BFD_SESS_DOWN);
    }

    if ((pAdjEntry->u1Mt0BfdRegd != ISIS_BFD_FALSE) &&
        (pAdjEntry->u1Mt2BfdRegd != ISIS_BFD_FALSE))
    {
        /* Both IPv4 and IPv6 neighbors are there in the adjacency */
        u1IsOnlyNbr = ISIS_FALSE;
    }
    else if ((pAdjEntry->u1Mt0BfdRegd != ISIS_BFD_FALSE) ||
             (pAdjEntry->u1Mt2BfdRegd != ISIS_BFD_FALSE))
    {
        /* Either IPv4 or IPv6 neighbor is there in the adjacency */
        u1IsOnlyNbr = ISIS_TRUE;
    }
    else
    {
        /* BFD notification is no more required */
        return;
    }

    u1TmrIdx = pAdjEntry->u1TmrIdx;

    if (u1IsOnlyNbr == ISIS_TRUE)
    {
        /* Since this is the only neighbor on the circuit,
         * delete the adjacency and generate a DOWN Event */
        ADP_PT ((ISIS_LGST, "ADJ <T> : Processing BFD Down Notification - "
                 "Deleting Adjacencies\n"));
        IsisAdjDelAdj (pContext, pCktEntry, pAdjEntry, ISIS_TRUE, &u4DirIdx);

        /* Stopping the Hold Timer started for this adjacency */
        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING, u4DirIdx, u1TmrIdx);
    }
    else
    {
        /* Update the neighbor useable parameter for this AdjEntry 
         * and clear the IP addr from the AdjNeighAddr for the
         * session which is down */

        ADP_PT ((ISIS_LGST, "ADJ <T> : Processing BFD Down Notification - "
                 "Deregistering MT [%u] adjacency alone\n", u1MtId));
        /* During processing of BFD Down notification, if multi-topology
         * is enabled, BFD Neighbor Useable parameters have to be updated
         * accordingly (as per RFC 6213) */
        if (pContext->u1IsisMTSupport == ISIS_TRUE)
        {
            IsisUtlUpdateBfdNeighborUseable (pCktEntry, pAdjEntry);
        }
        else
        {
            if (u1MtId == ISIS_IPV4_UNICAST_MT_ID)
            {
                pAdjEntry->u1AdjIpAddrType &= ~(ISIS_ADDR_IPV4);
            }
            else
            {
                pAdjEntry->u1AdjIpAddrType &= ~(ISIS_ADDR_IPV6);
            }
        }

        i4RetVal = IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktEntry,
                                          pAdjEntry, u1MtId, ISIS_TRUE);
        if (i4RetVal == ISIS_FAILURE)
        {
            WARNING ((ISIS_LGST, "UTL <E> : BFD Deregistration failure for"
                      "Adjacency Index [%u]", pAdjEntry->u4AdjIdx));
        }
#ifdef ISIS_FT_ENABLED
        ISIS_FLTR_ADJ_LSU (pContext, ISIS_CMD_ADD, pAdjEntry);
#endif
    }

    return;
}

/******************************************************************************
* Function     : IsisUtlProcessAdjAdminDownNotification
* Description  : This function is used to process the session admin down 
*                notification from BFD.
* Inputs       : pContext  - Pointer to the system instance
*                pAdjEntry - Pointer to the adjacent entry
*                pCktEntry - Pointer to the Ckt Entry
*                u1MtId    - MT ID for which BFD notification is received
* Outputs      : None
* Returns      : None
*******************************************************************************/
PUBLIC VOID
IsisUtlProcessAdjAdminDownNotification (tIsisSysContext * pContext,
                                        tIsisAdjEntry * pAdjEntry,
                                        tIsisCktEntry * pCktEntry, UINT1 u1MtId)
{
    /* Update the BFD session state in AdjEntry */
    UTP_PT ((ISIS_LGST,
             "UTL <T> :BFD Session state changes to ADMIN_DOWN on circuit"
             " [Id: %d IfIndex:%d] for MT [%d]\n", pCktEntry->u4CktIdx,
             pCktEntry->u4CktIfIdx, u1MtId));
    IsisUtlUpdateBfdState (pAdjEntry, u1MtId, ISIS_BFD_SESS_ADMIN_DOWN);

    /* Update the neighbor useable parameter for this AdjEntry */
    IsisUtlUpdateBfdNeighborUseable (pCktEntry, pAdjEntry);

    UNUSED_PARAM (pContext);

    return;
}

/******************************************************************************
* Function     : IsisUtlGetCktRecWithIfIndex
* Description  : This function is used to get the circuit record for a 
*                given interface index.
* Inputs       : pContext - Pointer to the system instance
*                u4IfIndex - Interface index
*                pCktRec - Pointer to the circuit record
* Outputs      : None
* Returns      : ISIS_SUCCESS/ISIS_FAILURE
*******************************************************************************/
PUBLIC INT4
IsisUtlGetCktRecWithIfIndex (tIsisSysContext * pContext, UINT4 u4IfIndex,
                             tIsisCktEntry ** pCktRec)
{
    tIsisCktEntry      *pTravCkt = NULL;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlGetCktRecWithIfIndex ()\n"));

    pTravCkt = pContext->CktTable.pCktRec;
    while (pTravCkt != NULL)
    {
        if (pTravCkt->u4CktIfIdx == u4IfIndex)
        {
            *pCktRec = pTravCkt;
            UTP_EE ((ISIS_LGST, "UTL <X> : Exiting "
                     "IsisUtlGetCktRecWithIfIndex ()\n"));
            return ISIS_SUCCESS;
        }
        pTravCkt = pTravCkt->pNext;
    }

    UTP_PT ((ISIS_LGST,
             "UTL <E> : Ckt Rec For IfIndex [ %u ] Not Found\n", u4IfIndex));
    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetCktRecWithIfIndex ()\n"));
    return ISIS_FAILURE;
}

/******************************************************************************
 * Function     : IsisUtlGetAdjRecWithIpAddr
 * Description  : This function is used to get the circuit record for a
 *                given interface index.
 * Inputs       : tIsisIpAddr - Pointer to the IP Address
 *                pCktRec - Pointer to the circuit record
 *                pAdjRec - Pointer to the Adjacency entry
 * Outputs      : None
 * Returns      : ISIS_SUCCESS/ISIS_FAILURE
 *******************************************************************************/
PUBLIC INT4
IsisUtlGetAdjRecWithIpAddr (tIsisIpAddr * pIpAddr, tIsisCktEntry * pCktRec,
                            tIsisAdjEntry ** pL1AdjRec,
                            tIsisAdjEntry ** pL2AdjRec)
{
    tIsisAdjEntry      *pTravAdj = NULL;
    INT1                i1RetVal = ISIS_FAILURE;
    UINT1               u1TravCount = 0;
    UINT1               u1MaxCount = 0;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlGetAdjRecWithIpAddr ()\n"));

    /* Travel through the entire list and fetch the matching record
     */
    u1MaxCount = (pCktRec->u1CktLevel == ISIS_LEVEL12 ? 2 : 1);

    pTravAdj = pCktRec->pAdjEntry;

    while (pTravAdj != NULL)
    {
        if ((ISIS_COMPARE_IP_ADDR_IN_ADJ (pIpAddr, pTravAdj)) == 0)
        {
            if (pTravAdj->u1AdjUsage == ISIS_LEVEL1)
            {
                *pL1AdjRec = pTravAdj;
                u1TravCount++;
            }
            else
            {
                *pL2AdjRec = pTravAdj;
                u1TravCount++;
            }
            i1RetVal = ISIS_SUCCESS;
        }
        if (u1TravCount == u1MaxCount)
        {
            break;
        }
        pTravAdj = pTravAdj->pNext;
    }

    if (i1RetVal == ISIS_FAILURE)
    {
        UTP_PT ((ISIS_LGST,
                 "UTL <T> : No Matching Adj Record Found - Circuit [ %u ]\n",
                 pCktRec->u4CktIdx));
    }

    UTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisUtlGetAdjRecWithIpAddr ()\n"));
    return (i1RetVal);

}

/******************************************************************************
* Function     : IsisUtlUpdateBfdState
* Description  : This function is used to update the BFD session state
*                in adjacent entry.
* Inputs       : pAdjEntry - Pointer to the adjacent entry
*                u1MtId    - MT ID for which the change notification is 
*                            received
*                u1BfdState - BFD state to be updated in Adjacent record
* Outputs      : None
* Returns      : None
*******************************************************************************/
PUBLIC VOID
IsisUtlUpdateBfdState (tIsisAdjEntry * pAdjEntry, UINT1 u1MtId,
                       UINT1 u1BfdState)
{
    if (u1MtId == ISIS_IPV4_UNICAST_MT_ID)
    {
        if (pAdjEntry->u1Mt0BfdRegd == ISIS_TRUE)
        {
            pAdjEntry->u1IsisMT0BfdState = u1BfdState;
        }
        else
        {
            pAdjEntry->u1IsisMT0BfdState = ISIS_BFD_FALSE;
        }
    }
    else if (u1MtId == ISIS_IPV6_UNICAST_MT_ID)
    {
        if (pAdjEntry->u1Mt2BfdRegd == ISIS_TRUE)
        {
            pAdjEntry->u1IsisMT2BfdState = u1BfdState;
        }
        else
        {
            pAdjEntry->u1IsisMT2BfdState = ISIS_BFD_FALSE;
        }
    }
    return;
}

/******************************************************************************
* Function     : IsisUtlComputeIfUpdateReqd
* Description  : This function is used compute if Updation is required or not
* Inputs       : pAdjRec - Pointer to the adjacency entry
*                pu1Mt0UpdReqd - Pointer to the MT0 update required variable
*                pu1Mt2UpdReqd - Pointer to the MT2 update required variable
* Outputs      : None
* Returns      : None
*******************************************************************************/
PUBLIC VOID
IsisUtlComputeIfUpdateReqd (tIsisAdjEntry * pAdjRec, UINT1 *pu1Mt0UpdReqd,
                            UINT1 *pu1Mt2UpdReqd)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;

    pContext = pAdjRec->pCktRec->pContext;
    pCktRec = pAdjRec->pCktRec;

    /* In case of BFD over multi-topology ISIS, the MT-ID
     * updation is done based on the ISIS_BFD_NEIGHBOR_USEABLE &
     * BFD_STATE variables inside the Adj Records.
     *
     * In case of BFD over single topology ISIS, the AdjRec's
     * AddrType and respective Addresses have to be 
     * updated based on the protocol being supported in the 
     * CktRec on which the AdjRec is added. */

    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        if (pAdjRec->u1Mt0BfdRegd != ISIS_BFD_FALSE)
        {
            if (ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) != ISIS_BFD_FALSE)
            {
                if (pAdjRec->u1IsisMT0BfdState == ISIS_BFD_SESS_UP)
                {
                    *pu1Mt0UpdReqd = ISIS_BFD_TRUE;
                }
            }
        }
        else
        {
            *pu1Mt0UpdReqd = ISIS_BFD_TRUE;
        }
        if (pAdjRec->u1Mt2BfdRegd != ISIS_BFD_FALSE)
        {
            if (ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) != ISIS_BFD_FALSE)
            {
                if (pAdjRec->u1IsisMT2BfdState == ISIS_BFD_SESS_UP)
                {
                    *pu1Mt2UpdReqd = ISIS_BFD_TRUE;
                }
            }
        }
        else
        {
            *pu1Mt2UpdReqd = ISIS_BFD_TRUE;
        }
    }
    else
    {
        if (pCktRec->u1IsisBfdStatus == ISIS_BFD_DISABLE)
        {
            return;
        }

        if (pCktRec->u1ProtoSupp & ISIS_ADDR_IPV4)
        {
            *pu1Mt0UpdReqd = ISIS_BFD_TRUE;
        }
        else
        {
            *pu1Mt0UpdReqd = ISIS_BFD_FALSE;
        }

        if (pCktRec->u1ProtoSupp & ISIS_ADDR_IPV6)
        {
            *pu1Mt2UpdReqd = ISIS_BFD_TRUE;
        }
        else
        {
            *pu1Mt2UpdReqd = ISIS_BFD_FALSE;
        }
    }
    return;
}

/******************************************************************************
* Function     : IsisUtlBfdDeRegister
* Description  : This function is used decide whether deregistration from BFD
*                is required or not
* Inputs       : pContext - Pointer to the system instance 
*                pAdjRec - Pointer to the adjacency entry
*                pCktRec - Pointer to the circuit record 
* Outputs      : None
* Returns      : None
*******************************************************************************/

PUBLIC VOID
IsisUtlBfdDeRegister (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec,
                      tIsisCktEntry * pCktRec, UINT1 u1SyncFlag)
{
    tIsisAdjEntry      *pTravAdj = NULL;
    UINT1               u1DoNotDeregister = ISIS_BFD_FALSE;

    pTravAdj = pCktRec->pAdjEntry;

    while (pTravAdj != NULL)
    {
        if (((pContext->u1IsisMTSupport == ISIS_FALSE) ||
             ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
              (pAdjRec->u1IsisMT0BfdEnabled != ISIS_BFD_FALSE))) &&
            (MEMCMP (pAdjRec->AdjNbrIpV4Addr.au1IpAddr,
                     pTravAdj->AdjNbrIpV4Addr.au1IpAddr,
                     ISIS_MAX_IPV4_ADDR_LEN) == 0))
        {
            if ((pAdjRec->u1AdjUsage != pTravAdj->u1AdjUsage) &&
                (pTravAdj->u1Mt0BfdRegd == ISIS_BFD_TRUE))
            {
                /* It implies that someother adjacent record 
                 * (of different level) is also
                 * dependent on the same bfd session */
                u1DoNotDeregister = ISIS_BFD_TRUE;
                pAdjRec->u1Mt0BfdRegd = ISIS_BFD_FALSE;
            }
        }
        if (((pContext->u1IsisMTSupport == ISIS_FALSE) ||
             ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
              (pAdjRec->u1IsisMT2BfdEnabled != ISIS_BFD_FALSE))) &&
            (MEMCMP (pAdjRec->AdjNbrIpV6Addr.au1IpAddr,
                     pTravAdj->AdjNbrIpV6Addr.au1IpAddr,
                     ISIS_MAX_IPV6_ADDR_LEN) == 0))
        {
            if ((pAdjRec->u1AdjUsage != pTravAdj->u1AdjUsage) &&
                (pTravAdj->u1Mt2BfdRegd == ISIS_BFD_TRUE))
            {
                /* It implies that someother adjacent record 
                 * (of different level) is also
                 * dependent on the same bfd session */
                u1DoNotDeregister = ISIS_BFD_TRUE;
                pAdjRec->u1Mt2BfdRegd = ISIS_BFD_FALSE;
            }
        }

        if (u1DoNotDeregister == ISIS_BFD_TRUE)
        {
            break;
        }

        pTravAdj = pTravAdj->pNext;
    }
    if (u1DoNotDeregister != ISIS_BFD_TRUE)
    {
        if ((pContext->u1IsisMTSupport == ISIS_FALSE) ||
            ((ISIS_BFD_REQUIRED (pAdjRec) == ISIS_BFD_TRUE) &&
             (pAdjRec->u1IsisMT0BfdEnabled != ISIS_BFD_FALSE)))
        {
            IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktRec,
                                   pAdjRec, ISIS_IPV4_UNICAST_MT_ID, ISIS_TRUE);
        }
        if ((pContext->u1IsisMTSupport == ISIS_FALSE) ||
            ((ISIS_BFD_REQUIRED (pAdjRec) == ISIS_BFD_TRUE) &&
             (pAdjRec->u1IsisMT2BfdEnabled != ISIS_BFD_FALSE)))
        {
            IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktRec,
                                   pAdjRec, ISIS_IPV6_UNICAST_MT_ID, ISIS_TRUE);
        }
    }

    if (u1SyncFlag == ISIS_TRUE)
    {
#ifdef ISIS_FT_ENABLED
        ISIS_FLTR_ADJ_LSU (pContext, ISIS_CMD_ADD, pAdjRec);
#endif
    }
}

/******************************************************************************
* Function     : IsisUtlBfdRegister
* Description  : This function is a wrapper for doing BFD registration
*                in case of single topology ISIS.
* Inputs       : pContext - Pointer to the system instance 
*                pAdjRec - Pointer to the adjacency entry
*                pCktRec - Pointer to the circuit record 
* Outputs      : None
* Returns      : None
*******************************************************************************/

PUBLIC VOID
IsisUtlBfdRegister (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec,
                    tIsisCktEntry * pCktRec)
{
    INT4                i4RetVal = ISIS_FAILURE;

    /* BFD registration for single topology ISIS can be done
     * only if the ISIS Adjacency is in UP state */
    if (pAdjRec->u1AdjState != ISIS_ADJ_UP)
    {
        return;
    }

    if ((pCktRec->u1ProtoSupp & ISIS_ADDR_IPV4) &&
        (pAdjRec->u1AdjIpAddrType & ISIS_ADDR_IPV4) &&
        (pAdjRec->u1Mt0BfdRegd == ISIS_BFD_FALSE))
    {
        i4RetVal = IsisRegisterWithBfd (pContext->u4SysInstIdx,
                                        pCktRec, pAdjRec,
                                        ISIS_IPV4_UNICAST_MT_ID);
        if (i4RetVal == ISIS_FAILURE)
        {
            PANIC ((ISIS_LGST, "UTL <E> : BFD Registration failure for"
                    "Adjacency Index %u", pAdjRec->u4AdjIdx));
        }
    }
    if ((pCktRec->u1ProtoSupp & ISIS_ADDR_IPV6) &&
        (pAdjRec->u1AdjIpAddrType & ISIS_ADDR_IPV6) &&
        (pAdjRec->u1Mt2BfdRegd == ISIS_BFD_FALSE))
    {
        i4RetVal = IsisRegisterWithBfd (pContext->u4SysInstIdx,
                                        pCktRec, pAdjRec,
                                        ISIS_IPV6_UNICAST_MT_ID);
        if (i4RetVal == ISIS_FAILURE)
        {
            PANIC ((ISIS_LGST, "UTL <E> : BFD Registration failure for"
                    "Adjacency Index %u", pAdjRec->u4AdjIdx));
        }
    }

    return;
}

#endif
/******************************************************************************
 * * Function     : IsisUtlValidatePurgeLSP
 * * Description  : This function validates the purge LSP PDU
 * *                If MD5 is enabled - Validates authentication and rejects purges
 * *                that contain TLVs other than the authentication TLV
 * * Inputs       : pContext - Context identifier
 * *                pu1RcvdPDU - Received PDU
 * * Outputs      : None
 * * Returns      : None
 * *****************************************************************************/
PUBLIC INT4
IsisUtlValidatePurgeLSP (tIsisSysContext * pContext, UINT1 *pu1RcvdPDU)
{
    INT4                i4AuthStat = ISIS_FAILURE;
    UINT1               u1Len = 0;
    UINT1               u1PduType = 0;
    UINT1               u1Level = 0;
    UINT1               u1Code = 0;
    UINT1               u1Authenticate = FALSE;
    UINT1               u1AuthType = ISIS_AUTH_PASSWD;
    UINT1               u1RxAuthType = ISIS_AUTH_PASSWD;
    UINT1               u1Cause = ISIS_AUTH_MISSING;
    UINT2               u2PDULen = 0;
    UINT2               u2Offset = 0;
    UINT2               u2TrapPduLen = 0;
    UINT1               au1RcvDigest[16];
    UINT1               au1CalcDigest[16];
    UINT2               u2Cs = 0;
    unUtilAlgo          UtilAlgo;
    tIsisPasswd        *pTxPasswd = NULL;
    tIsisEvtAuthFail   *pAuthFailEvt = NULL;

    UTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisUtlValidatePurgeLSP ()\n"));

    MEMSET (au1CalcDigest, 0, sizeof (au1CalcDigest));
    MEMSET (au1RcvDigest, 0, sizeof (au1RcvDigest));
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
    u2Offset = (UINT2) ((tIsisComHdr *) pu1RcvdPDU)->u1HdrLen;

    ISIS_GET_2_BYTES (pu1RcvdPDU, sizeof (tIsisComHdr), u2PDULen);

    u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1RcvdPDU);
    u1PduType &= ISIS_PDU_TYPE_MASK;

    if (u1PduType == ISIS_L1LSP_PDU)
    {

        u1Level = ISIS_LEVEL1;
        pTxPasswd = &pContext->SysActuals.SysAreaTxPasswd;
        u1AuthType = pContext->SysActuals.u1SysAreaAuthType;
        ISIS_EXTRACT_CS_FROM_LSP (pu1RcvdPDU, u2Cs);
    }
    else if (u1PduType == ISIS_L2LSP_PDU)
    {
        u1Level = ISIS_LEVEL2;
        pTxPasswd = &pContext->SysActuals.SysDomainTxPasswd;
        u1AuthType = pContext->SysActuals.u1SysDomainAuthType;
        ISIS_EXTRACT_CS_FROM_LSP (pu1RcvdPDU, u2Cs);
    }
    if ((u1AuthType == ISIS_AUTH_PASSWD))
    {
        return ISIS_SUCCESS;
    }
    if (((pTxPasswd != NULL) && (pTxPasswd->u1Len != 0)) && (u1AuthType == 0))
    {
        UTP_PT ((ISIS_LGST,
                 "UTL <T> : Authentication type is not set for configured password\n"));
    }
    else if ((u1AuthType != 0)
             && ((pTxPasswd == NULL) || (pTxPasswd->u1Len == 0)))
    {
        UTP_PT ((ISIS_LGST,
                 "UTL <T> : MD5/clear text authentication password is not configured\n"));
    }

    if ((pTxPasswd != NULL) && (pTxPasswd->u1Len != 0) && (u1AuthType != 0))
    {
        u1Authenticate = TRUE;
    }
    if (u1Authenticate == FALSE)
    {
        return ISIS_FAILURE;
    }
    while (u2Offset < u2PDULen)
    {
        ISIS_GET_1_BYTE (pu1RcvdPDU, u2Offset, u1Code);
        u2Offset++;
        ISIS_GET_1_BYTE (pu1RcvdPDU, u2Offset, u1Len);
        u2Offset++;

        switch (u1Code)
        {
            case ISIS_AUTH_INFO_TLV:

                ISIS_GET_1_BYTE (pu1RcvdPDU, u2Offset, u1RxAuthType);
                if (((u1AuthType == ISIS_AUTH_PASSWD)
                     && (u1RxAuthType == ISIS_MD5_PASS))
                    || ((u1AuthType == ISIS_AUTH_MD5)
                        && (u1RxAuthType == ISIS_CLR_TXT_PASS)))
                {
                    UTP_PT ((ISIS_LGST,
                             "UTL <T> : Authentication failed for PDU [%s] : Auth type mismatch\n",
                             ISIS_GET_PDU_TYPE_STR (u1PduType)));

                    return ISIS_FAILURE;
                }
                if (u1RxAuthType == ISIS_MD5_PASS)
                {
                    ISIS_ASSIGN_2_BYTES (pu1RcvdPDU, ISIS_OFFSET_CHKSUM, 0);
                    MEMCPY (au1RcvDigest, (pu1RcvdPDU + u2Offset + 1),
                            ISIS_MD5_DIGEST_LEN);
                    MEMSET ((pu1RcvdPDU + u2Offset + 1), 0,
                            ISIS_MD5_DIGEST_LEN);

                    UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr =
                        (unsigned char *) pu1RcvdPDU;
                    UtilAlgo.UtilHmacAlgo.i4HmacPktLength = u2PDULen;
                    UtilAlgo.UtilHmacAlgo.pu1HmacKey = pTxPasswd->au1Password;
                    UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = pTxPasswd->u1Len;
                    UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest =
                        (unsigned char *) &au1CalcDigest;

                    UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);

                    MEMCPY ((pu1RcvdPDU + u2Offset + 1), au1RcvDigest, 16);

                    MEMCPY ((pu1RcvdPDU + ISIS_OFFSET_CHKSUM), &u2Cs, 2);
                    if (MEMCMP
                        (au1RcvDigest, au1CalcDigest, ISIS_MD5_DIGEST_LEN) == 0)
                    {
                        i4AuthStat = ISIS_SUCCESS;
                    }
                    else
                    {
                        i4AuthStat = ISIS_FAILURE;
                        u1Cause = ISIS_PASSWD_MISMTCH;
                    }
                }
                else
                {
                    /* Authentication type not supported
                     *                      */

                    u1Cause = ISIS_AUTH_NOT_SUPP;

                    i4AuthStat = ISIS_FAILURE;
                }
                if (i4AuthStat == ISIS_SUCCESS)
                {
                    return ISIS_SUCCESS;
                }
                break;
            default:
                UTP_PT ((ISIS_LGST,
                         "UTL <E> : [purge LSP] TLV other than Auth TLV is present in LSP purge [%s] \n",
                         ISIS_GET_PDU_TYPE_STR (u1PduType)));
                return ISIS_FAILURE;
                break;
        }
        u2Offset = (UINT2) (u2Offset + u1Len);
    }

    if (i4AuthStat != ISIS_SUCCESS)
    {
        ISIS_INCR_SYS_AUTH_FAILS (pContext, u1Level);

        if (u1Cause == ISIS_AUTH_NOT_SUPP)
        {
            UTP_PT ((ISIS_LGST,
                     "UTL <E> : [purge LSP] Authentication failed for PDU [%s] : Received Auth Type Not Supported\n",
                     ISIS_GET_PDU_TYPE_STR (u1PduType)));
            /* Increment the AuthType FailCount
             *              */
            if (u1Level == ISIS_LEVEL1)
            {
                pContext->SysL1Stats.u4SysAuthTypeFails++;
            }
            else
            {
                pContext->SysL2Stats.u4SysAuthTypeFails++;
            }
        }
        else if (u1Cause == ISIS_PASSWD_MISMTCH)
        {

            UTP_PT ((ISIS_LGST,
                     "UTL <E> : [purge LSP] Authentication failed for PDU [%s]: Password mismatch\n",
                     ISIS_GET_PDU_TYPE_STR (u1PduType)));
        }
        pAuthFailEvt = (tIsisEvtAuthFail *)
            (VOID *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtAuthFail));

        if (pAuthFailEvt != NULL)
        {

            u2TrapPduLen =
                (UINT2) (ISIS_MIN (ISIS_TRAP_PDU_FRAG_SIZE, u2PDULen));
            /* Allocate Memory for PDU Fragment
             *              */
            pAuthFailEvt->pu1PduFrag = (UINT1 *)
                (VOID *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                         ISIS_TRAP_PDU_FRAG_SIZE);
            if (pAuthFailEvt->pu1PduFrag != NULL)
            {
                pAuthFailEvt->u1EvtID = ISIS_EVT_AUTH_FAIL;
                pAuthFailEvt->u1Cause = u1Cause;
                pAuthFailEvt->u1AuthType = u1AuthType;
                MEMCPY (pAuthFailEvt->pu1PduFrag, pu1RcvdPDU, u2TrapPduLen);

                IsisUtlSendEvent (pContext, (UINT1 *) pAuthFailEvt,
                                  sizeof (tIsisEvtAuthFail));
            }
            else
            {
                /*Release Memory for pAuthFailEvt */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pAuthFailEvt);
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Auth Fail Event\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
        }
    }
    UTP_EE ((ISIS_LGST,
             "UTL <X> : [purge LSP] Exiting IsisUtlValidatePurgeLSP ()\n"));
    return (i4AuthStat);
}
