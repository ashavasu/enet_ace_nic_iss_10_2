
/******************************************************************************
 *  Copyright (C) Future Software Limited, 2001
 *
 *  $Id: isupdevt.c,v 1.28 2017/09/11 13:44:09 siva Exp $
 *
 *  Description: This file contains the Routines associated with
 *               processing of events in Update Module. 
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE UINT1       IsisUpdSummSelfLSP (tIsisLSPInfo *, UINT1 *, UINT1);
PRIVATE VOID        IsisUpdPurgeLSPFromDB (tIsisSysContext *, UINT1 *, UINT1);
PRIVATE VOID        IsisUpdPurgeLSPFromTxQ (tIsisSysContext *, UINT1 *, UINT1);

/******************************************************************************
 * Function    : IsisUpdProcISUpEvt ()
 * Description : Self LSPs, both Pseudonode and Non-Pseudonode, are maintained 
 *               as TLVs to make additions and deletions to Self-LSPs simple. 
 *               This routine updates the Manual Area Address TLVs, Protocol 
 *               Support TLVs and the IP Interface Address TLVs into the
 *               Self-LSPs with the information configured by the manager. 
 * Input (s)   : pContext - Pointer to System Context
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcISUpEvt (tIsisSysContext * pContext)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisMDT           *pMDT = NULL;
    tIsisMAAEntry      *pMAARec = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisMDT           *pMDT1 = NULL;
    tIsisRRDRoutes     *pRRDRoute = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcISUpEvt ()\n"));

    pContext->SelfLSP.u1L1LSPFlags |=
        ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ?
         ISIS_LEVEL12 : pContext->SysActuals.u1SysType);

    pContext->SelfLSP.u1L2LSPFlags |= pContext->SelfLSP.u1L1LSPFlags;
    /* Allocate the required memory for Non-Pseudonode Self-LSPs, since the IS 
     * is coming UP. Pseudonode LSPs are maintained per circuit and these LSPs
     * will get initialised once the circuits and the associated adjacencies
     * come up and also based on the DIS status of the local system
     */

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL1))
    {
        /* Level1 information is required for both Level-12 and Level-1 routers.
         * LSP Headers hold information regarding Zero-LSPs and all Non-ZeroLSPs
         */

        i4RetVal = IsisUpdAllocLSPHdr (pContext,
                                       &(pContext->SelfLSP.pL1NSNLSP),
                                       ISIS_BUF_NSLI);
    }

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL2))
    {
        /* Level2 information is required for both Level-12 and Level-2 routers
         * LSP Headers hold information regarding Zero-LSPs and all Non-ZeroLSPs
         */

        i4RetVal = IsisUpdAllocLSPHdr (pContext,
                                       &(pContext->SelfLSP.pL2NSNLSP),
                                       ISIS_BUF_NSLI);
    }

    if (i4RetVal != ISIS_SUCCESS)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Headers\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcISUpEvt ()\n"));
        return;
    }
    pMAARec = pContext->ManAddrTable.pMAARec;

    /* No area address configured - hence return - no need to add any other TLVs as the 
     * the current system is not a part of any area until now */

    if (pMAARec == NULL)
    {
        return;
    }
    /* Multi Data Type structure is used for updating Self-LSPs. This structure
     * is filled with the appropriate TLV information that will be required to 
     * update Self-LSPs. 
     */

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcISUpEvt ()\n"));
        return;
    }

    /* Update Manual Area Address information into the Self-LSPs
     */

    while (pMAARec != NULL)
    {
        pMDT->u1TLVType = ISIS_AREA_ADDR_TLV;

        /* NOTE: Area Address structures are similar to octet strings and hence
         * will be as follows:
         * 
         * Area Address = { Area Address Length, Area Address }
         */

        MEMCPY (&(pMDT->AreaAddress), &(pMAARec->ManAreaAddr),
                sizeof (tIsisAreaAddr));

        if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
        {
            /* Both L1 and L12 systems maintain L1 Non-Pseudonode LSPs
             */

            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            IsisUpdAddAreaAddrTLV (pContext, pMDT);
        }

        if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
        {
            /* Both L2 and L12 systems maintain L2 Non-Pseudonode LSPs
             */

            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            IsisUpdAddAreaAddrTLV (pContext, pMDT);
        }

        pMAARec = pMAARec->pNext;
    }

    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

    /* Add the OriginatingLSPBufferSize TLV 
     */

    if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
    {
        IsisUpdAddLSPBufSizeTLV (pContext, ISIS_LEVEL1);
    }

    if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
    {
        IsisUpdAddLSPBufSizeTLV (pContext, ISIS_LEVEL2);
    }

    pIPRARec = pContext->IPRAInfo.pIPRAEntry;

    /* Scan the list of IPRAs configured and add all active entries to the
     * SelfLSP buffers
     */

    while (pIPRARec != NULL)
    {
        if ((pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
            && (pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
        {
            /* Only Active entries need to be considered
             */

            IsisUpdUpdateIPRA (pContext, pIPRARec, ISIS_CMD_ADD);
        }
        pIPRARec = pIPRARec->pNext;
    }

    pRRDRoute = (tIsisRRDRoutes *) RBTreeGetFirst (pContext->RRDRouteRBTree);
    while (pRRDRoute != NULL)
    {
        pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));
        if (pMDT == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            return;
        }
        pMDT->u1Cmd = ISIS_CMD_ADD;
        if (pRRDRoute->IPAddr.u1AddrType == ISIS_IPV4)
        {
            MEMCPY (pMDT->IPAddr.au1IpAddr, pRRDRoute->IPAddr.au1IpAddr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
        }
        else
        {
            MEMCPY (pMDT->IPAddr.au1IpAddr, pRRDRoute->IPAddr.au1IpAddr,
                    ISIS_MAX_IPV6_ADDR_LEN);
            pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
        }
        pMDT->IPAddr.u1PrefixLen = pRRDRoute->IPAddr.u1PrefixLen;

        if (pRRDRoute->IPAddr.u1AddrType == ISIS_IPV4)
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
            }
        }
        else
        {
            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
            }
        }
        ISIS_COPY_METRIC (pContext, pRRDRoute->u4Metric, pMDT);
        pMDT->u1SrcProtoId = pRRDRoute->u1Protocol;
        if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_1) ==
            ISIS_IMPORT_LEVEL_1)
        {
            pMDT1 =
                (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

            if (pMDT1 == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                return;
            }
            MEMCPY (pMDT1, pMDT, sizeof (tIsisMDT));
            pMDT1->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT1) == ISIS_FAILURE)
            {
                RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update"
                         " L1 RTM Route Into Self-LSP\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
            ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
        }
        if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_2) ==
            ISIS_IMPORT_LEVEL_2)
        {
            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) == ISIS_FAILURE)
            {
                RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update"
                         " L2 RTM Route Into Self-LSP\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
        }
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
        pRRDRoute =
            RBTreeGetNext (pContext->RRDRouteRBTree, (tRBElem *) pRRDRoute,
                           NULL);
    }
/*RFC 5301*/
    if (pContext->u1IsisDynHostNmeSupport == ISIS_DYNHOSTNME_ENABLE)
    {
        IsisHostNmeMDT (pContext, pMDT, ISIS_CMD_ADD);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcISUpEvt () \n"));
}

/******************************************************************************
 * Function    : IsisUpdProcISDownEvt ()
 * Description : This routine deletes the SelfLSP and the LSP database 
 *               information for the specified context
 * Input (s)   : pContext - Pointer to System Context
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcISDownEvt (tIsisSysContext * pContext)
{
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcISDownEvt ()\n"));

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL1))
    {
        IsisUpdDelSelfLSPInfo (pContext, ISIS_LEVEL1);
        IsisUpdPurgeLSPs (pContext, ISIS_LEVEL1);
    }
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL2))
    {
        IsisUpdDelSelfLSPInfo (pContext, ISIS_LEVEL2);
        IsisUpdPurgeLSPs (pContext, ISIS_LEVEL2);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcISDownEvt ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcMAAChgEvt ()
 * Description : This routine updates the Self Zero LSPs with the 
 *               changed Manual Area Address. This routine updates only the 
 *               NonPseudonode LSPs as the Area Address TLVs not supposed to be
 *               present in the Pseudonode LSPs
 * Input (s)   : pContext   - Pointer to System Context
 *               pEvtMAAChg - Pointer to the Manual Area Address change Event
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcMAAChgEvt (tIsisSysContext * pContext,
                      tIsisEvtManAAChange * pEvtMAAChg)
{
    tIsisMDT           *pMDT = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcMAAChgEvt ()\n"));

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if ((pMDT != NULL) && (pEvtMAAChg->u1Length < ISIS_AREA_ADDR_LEN))
    {
        pMDT->u1TLVType = ISIS_AREA_ADDR_TLV;
        MEMCPY (&(pMDT->AreaAddress.au1AreaAddr), pEvtMAAChg->au1AreaAddr,
                pEvtMAAChg->u1Length);
        pMDT->AreaAddress.u1Length = pEvtMAAChg->u1Length;
        pMDT->u1Cmd = (UINT1) ((pEvtMAAChg->u1Status == ISIS_MAA_ADDED)
                               ? ISIS_CMD_ADD : ISIS_CMD_DELETE);
    }
    else
    {
        ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtMAAChg);
        if (pMDT != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
        }
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcMAAChgEvt () \n"));
        return;
    }

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        /* Updating the L1 Non pseudo Node LSP
         */

        pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;

        /* Cicuit information is required only for Pseudonode cases, since more
         * than one Pseudonode LSP can exist in a system. Hence a '0' is passed
         * as the second argument
         */

        IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
    }

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        /* Updating the L2 Non pseudo Node LSP
         */

        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;

        /* Cicuit information is required only for Pseudonode cases, since more
         * than one Pseudonode LSP can exist in a system. Hence a '0' is passed
         * as the second argument
         */

        IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
    }
    if (pMDT != NULL)
    {
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    }
    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtMAAChg);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcMAAChgEvt ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcPSChgEvt ()
 * Description : This routine updates the both Pseudonode and Non-Pseudonode 
 *               Zero SelfLSPs with the Protocol Support information. 
 * Input (s)   : pContext  - Pointer to System Context
 *               pEvtPSChg - Pointer to the Protocol Supported Change Event
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcPSChgEvt (tIsisSysContext * pContext, tIsisEvtPSChange * pEvtPSChg)
{
    tIsisMDT           *pMDT = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcPSChgEvt ()\n"));

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT != NULL)
    {
        pMDT->u1TLVType = ISIS_PROT_SUPPORT_TLV;
        pMDT->u1ProtSupp = pEvtPSChg->u1ProtSupp;

        pMDT->u1Cmd = (UINT1) ((pEvtPSChg->u1Status == ISIS_PS_ADDED)
                               ? ISIS_CMD_ADD : ISIS_CMD_DELETE);

        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            /* Updating the Non Pseudo Node LSP
             */

            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            IsisUpdUpdateSelfLSP (pContext, 0, pMDT);

            /* Updating the Pseudo Node LSP
             */

            pMDT->u1LSPType = ISIS_L1_PSEUDO_LSP;
            IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
        }

        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            /* Updating the Non Pseudo Node LSP
             */

            pMDT->u1LSPType = ISIS_L2_PSEUDO_LSP;
            IsisUpdUpdateSelfLSP (pContext, 0, pMDT);

            /* Updating the Pseudo Node LSP
             */

            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
        }
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
    }

    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtPSChg);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcPSChgEvt ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcSummAddrChgEvt ()
 * Description : This routine 
 *               -- Summarizes the existing IPRA information if a new summary
 *                  address is added
 *               -- DeSummarizes the already summarized information in selfLSPs
 *                  if an existing SA entry is deleted
 *               -- Performs Summarization or DeSummarization of IPRAs based on
 *                  the previous and current Admin states of the SA entry if SA
 *                  entry's Admin state is modified. 
 *               It also modifies an existing IPRA entry if the metrics
 *               associated with the SA entry is modified   
 * Input (s)   : pContext  - Pointer to System Context
 *               pEvtSAChg - pointer to Summary Address Changed Event
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcSummAddrChgEvt (tIsisSysContext * pContext,
                           tIsisEvtSummAddrChg * pEvtSAChg)
{
    tIsisMDT           *pMDT = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcSummAddrChgEvt ()\n"));

    switch (pEvtSAChg->u1Status)
    {
        case ISIS_SUMM_ADDR_ADD:

            if (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L1)
            {
                /* SA entry with Admin state L1 is added, hence summarize all L1
                 * IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : L1 Summary Address Added. Summarizing L1 IPRAs\n"));
                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
            }
            else if (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L2)
            {
                /* SA entry with Admin state L2 is added, hence summarize all L2
                 * IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : L2 Summary Address Added. Summarizing L2 IPRAs\n"));
                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
            }
            else if (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L12)
            {
                /* SA entry with Admin state L12 is added, hence summarize all 
                 * L1 & L2 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summarizing L1 & L2 IPRAs.  Summarizing L1 & L2 IPRAs\n"));
                if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
                {
                    IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
                }
                if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
                {
                    IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
                }
            }
            break;

        case ISIS_SUMM_ADDR_DEL:

            if (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L1)
            {
                /* SA entry with Admin state L1 is deleted, hence de-summarize
                 * all L1 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : L1 Summary Address Deleted. De-Summarizing L1 IPRAs\n"));
                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
            }
            else if (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L2)
            {
                /* SA entry with Admin state L2 is deleted, hence de-summarize
                 * all L2 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : L2 Summary Address Deleted. De-Summarizing L2 IPRAs\n"));
                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
            }
            else if (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L12)
            {
                /* SA entry with Admin state L12 is deleted, hence de-summarize
                 * all L1 & L2 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : L12 Summary Address Deleted. De-Summarizing L1 & L2 IPRAs\n"));

                if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
                {
                    IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
                }
                if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
                {
                    IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
                }
            }
            break;

        case ISIS_SUMM_ADDR_ADMIN_CHG:

            if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L1)
                && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L2))
            {
                /* SA entry changed state from L1 to L2, hence de-summarize
                 * all L1 IPRAs & summarize L2 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L1 ], Current [ L2 ]\n"));
                UPP_PT ((ISIS_LGST,
                         "\t\tUPD <T> : De-Summarizing L1 IPRAs. Summarizing L2 IPRAs\n"));

                /* Reset the Usage count to Zero 
                 */
                IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                                        pEvtSAChg->u1PrefixLen, 0, ISIS_LEVEL1,
                                        ISIS_CMD_MODIFY);

                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);

            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L1)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L12))
            {
                /* SA entry changed state from L1 to L12, hence summarize all 
                 * L2 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L1 ], Current [ L12 ]\n"));
                UPP_PT ((ISIS_LGST, "\t\tUPD <T> : Summarizing L2 IPRAs\n"));
                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L1)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_OFF))
            {
                /* SA entry changed state from L1 to OFF, hence desummarize all
                 * L1 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L1 ], Current [ OFF ]\n"));
                UPP_PT ((ISIS_LGST, "\t\tUPD <T> : De-Summarizing L1 IPRAs\n"));

                IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                                        pEvtSAChg->u1PrefixLen, 0, ISIS_LEVEL1,
                                        ISIS_CMD_MODIFY);
                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L2)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L1))
            {
                /* SA entry changed state from L2 to L1, hence de-summarize
                 * all L2 IPRAs & summarize L1 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L2 ], Current [ L1 ]\n"));
                UPP_PT ((ISIS_LGST,
                         "\t\tUPD <T> : De-Summarizing L2 IPRAs. Summarizing L1 IPRAs\n"));

                IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                                        pEvtSAChg->u1PrefixLen, 0, ISIS_LEVEL2,
                                        ISIS_CMD_MODIFY);

                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L2)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L12))
            {
                /* SA entry changed state from L2 to L12, hence summarize all 
                 * L1 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L2 ], Current [ L12 ]\n"));
                UPP_PT ((ISIS_LGST, "\t\tUPD <T> : Summarizing L1 IPRAs\n"));
                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L2)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_OFF))
            {
                /* SA entry changed state from L2 to OFF, hence desummarize all
                 * L2 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L2 ], Current [ OFF ]\n"));
                UPP_PT ((ISIS_LGST, "\t\tUPD <T> : De-Summarizing L2 IPRAs\n"));
                IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                                        pEvtSAChg->u1PrefixLen, 0, ISIS_LEVEL2,
                                        ISIS_CMD_MODIFY);
                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L12)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L1))
            {
                /* SA entry changed state from L12 to L1, hence desummarize all 
                 * L2 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L12 ], Current [ L1 ]\n"));
                UPP_PT ((ISIS_LGST, "\t\tUPD <T> : De-Summarizing L2 IPRAs\n"));
                IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                                        pEvtSAChg->u1PrefixLen, 0, ISIS_LEVEL2,
                                        ISIS_CMD_MODIFY);
                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L12)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L2))
            {
                /* SA entry changed state from L12 to L2, hence desummarize all 
                 * L1 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L12 ], Current [ L2 ]\n"));
                UPP_PT ((ISIS_LGST, "\t\tUPD <T> : De-Summarizing L1 IPRAs\n"));
                IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                                        pEvtSAChg->u1PrefixLen, 0, ISIS_LEVEL1,
                                        ISIS_CMD_MODIFY);
                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_L12)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_OFF))
            {
                /* SA entry changed state from L12 to OFF, hence desummarize all
                 * L2 and L1 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ L12 ], Current [ OFF ]\n"));
                UPP_PT ((ISIS_LGST,
                         "\t\tUPD <T> : De-Summarizing L1 & L2 IPRAs\n"));
                IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                                        pEvtSAChg->u1PrefixLen, 0, ISIS_LEVEL1,
                                        ISIS_CMD_MODIFY);
                IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                                        pEvtSAChg->u1PrefixLen, 0, ISIS_LEVEL2,
                                        ISIS_CMD_MODIFY);

                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
                IsisUpdDeSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_OFF)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L1))
            {
                /* SA entry changed state from OFF to L1, hence summarize all
                 * L1 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ OFF ], Current [ L1 ]\n"));
                UPP_PT ((ISIS_LGST, "\t\tUPD <T> : Summarizing L1 IPRAs\n"));

                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_OFF)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L2))
            {
                /* SA entry changed state from OFF to L2, hence summarize all
                 * L2 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ OFF ], Current [ L2 ]\n"));
                UPP_PT ((ISIS_LGST, "\t\tUPD <T> : Summarizing L2 IPRAs\n"));

                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
            }
            else if ((pEvtSAChg->u1PrevAdminState == ISIS_SUMM_ADMIN_OFF)
                     && (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L12))
            {
                /* SA entry changed state from OFF to L12, hence summarize all
                 * L2 and L1 IPRAs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Change - Previous [ OFF ], Current [ L12 ]\n"));
                UPP_PT ((ISIS_LGST,
                         "\t\tUPD <T> : Summarizing L1 and L2 IPRAs\n"));

                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL1);
                IsisUpdSummarize (pContext, pEvtSAChg, ISIS_LEVEL2);
            }
            break;

        case ISIS_SUMM_ADDR_METRIC_CHG:

            pMDT =
                (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

            if (pMDT != NULL)
            {
                pMDT->u1Cmd = ISIS_CMD_MODIFY;
                ISIS_SET_METRIC (pContext, pMDT, pEvtSAChg);
                if (pEvtSAChg->u1AddrType == ISIS_ADDR_IPV6)
                {
                    if (pContext->u1IsisMTSupport == ISIS_TRUE)
                    {
                        pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
                    }
                    else
                    {
                        pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
                    }
                    MEMCPY (pMDT->IPAddr.au1IpAddr, pEvtSAChg->au1IPAddr,
                            ISIS_MAX_IPV6_ADDR_LEN);
                    pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
                }
                else
                {
                    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                    {
                        pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
                    }
                    else
                    {
                        pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
                    }

                    MEMCPY (pMDT->IPAddr.au1IpAddr, pEvtSAChg->au1IPAddr,
                            ISIS_MAX_IPV4_ADDR_LEN);
                    pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
                }
                pMDT->IPAddr.u1PrefixLen = pEvtSAChg->u1PrefixLen;

                if ((pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L12)
                    || (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L1))
                {
                    pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                    if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        IsisUpdModExtMtIPReachTLV (pContext, pMDT, ISIS_FALSE);
                    }
                    else
                    {
                        IsisUpdModIPRATLV (pContext, pMDT, ISIS_FALSE);
                    }
                }
                if ((pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L12)
                    || (pEvtSAChg->u1CurrAdminState == ISIS_SUMM_ADMIN_L2))
                {
                    pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                    if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        IsisUpdModExtMtIPReachTLV (pContext, pMDT, ISIS_FALSE);
                    }
                    else
                    {
                        IsisUpdModIPRATLV (pContext, pMDT, ISIS_FALSE);
                    }
                }

                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Updated Self-LSP For IPRA Metric Change - New Metric Value [ %u ]\n",
                             pMDT->u4FullMetric));
                }
                else
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Updated Self-LSP For IPRA Metric Change - New Metric Value [ %u ]\n",
                             pMDT->Metric[0]));
                }
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
            }
            else
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            }
            break;

        default:
            UPP_PT ((ISIS_LGST,
                     "UPD <E> : Invalid Summary Address Status [ %u ]\n",
                     pEvtSAChg->u1Status));
            break;
    }

    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtSAChg);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcSummAddrChgEvt ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcL1OrL2DecnCompleteEvt ()
 * Description : This Routine deletes the IP Reachability information
 *               in the Self LSPs from the List of deleted nodes 
 *               updated by L1 Decision process (SPF). This routine also
 *               adds New IPRAs (if any in the Path database) in Self LSPs
 * Input (s)   : pContext - Pointer to System Context
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcL1OrL2DecnCompleteEvt (tIsisSysContext * pContext, UINT1 u1EvtType)
{
    UINT1               u1MetCnt = 0;
    UINT1               u1Level;
    UINT1               u1MtIndex;
    tIsisMDT           *pMDT = NULL;
    tIsisSPTNode       *pNextSPTNode = NULL;
    tIsisSPTNode       *pDelSPTNodes = NULL;
    tIsisSPTNode       *pTravSPTNode = NULL;
    tRBTree             RBTree;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcL1DecnCompleteEvt ()\n"));

    u1Level =
        ((u1EvtType == ISIS_EVT_L1_DECN_COMPLETE) ? ISIS_LEVEL1 : ISIS_LEVEL2);
    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting IsisUpdProcL1DecnCompleteEvt ()\n"));
        return;
    }

    /* LOGIC: There are three lists that the decision module manipulates viz.,
     * Deleted Nodes (D), Previous Shortest Path (P), and the Current Shortest
     * Path (C). Decision module builds the shortest path in C. It then compares
     * C and P, marks all those nodes which are present in both the lists. All
     * those nodes which are present in P and not in C are moved to D from P. 
     * So what ever remains in P is a superset of C, which contains both changed
     * and unchanged information. The information from C will get updated into P
     * as a part of the decision process. 
     *
     * What we are interested here is the D and C, since D is now obsolete
     * information which must be deleted from our SelfLSPs, and C is the
     * modified information which must be updated into our Self LSPs. We will
     * first handle the 'D' list and then proceed to the 'C' list
     *
     * D is maintained in pContext->pL1DelNodes and C is maintained in
     * pContext->SysL1Info.pShortPathDB
     */

    /* Go through all the nodes in the Deleted List and delete them from
     * SelfLSPs
     */

    for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
    {

        if (u1Level == ISIS_LEVEL1)
        {
            RBTree = pContext->SysL1Info.PrevShortPath[u1MtIndex];
        }
        else
        {
            RBTree = pContext->SysL2Info.PrevShortPath[u1MtIndex];
        }

        /* ROUTE_LEAK */
        DETAIL_INFO (ISIS_UPD_MODULE, (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                       "UPD <T> : Processing Event [ %s ]\n",
                                       ISIS_GET_DECNC_EVT_STR (u1EvtType)));
        if (pContext->SysActuals.bSysL2ToL1Leak == ISIS_TRUE)
        {
            UPP_PT ((ISIS_LGST, "UPD <T> : Route Leak [ Enabled ]\n"));
            if (u1EvtType == ISIS_EVT_L2_DECN_COMPLETE)
            {
                pDelSPTNodes = pContext->pL2DelNodes;
                pContext->pL2DelNodes = NULL;
            }
            else
            {
                pDelSPTNodes = pContext->pL1DelNodes;
                pContext->pL1DelNodes = NULL;
            }
        }
        else
        {
            pDelSPTNodes = pContext->pL1DelNodes;
            pContext->pL1DelNodes = NULL;
        }

        while (pDelSPTNodes != NULL)
        {
            pTravSPTNode = RBTreeGet (RBTree, pDelSPTNodes);

            if (pTravSPTNode != NULL)
            {
                pNextSPTNode = pDelSPTNodes->pNext;
                pDelSPTNodes->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pDelSPTNodes);
                pDelSPTNodes = pNextSPTNode;
                continue;
            }

            if ((pDelSPTNodes->au2Metric[0] & ISIS_SPT_IPADDR_TYPE_MASK)
                == ISIS_IPV4_ADDR_TYPE)
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
                }
                else if ((pDelSPTNodes->au2Metric[0] & ISIS_SPT_EXT_MET_FLAG)
                         != ISIS_SPT_EXT_MET_FLAG)
                {
                    pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
                }
                else
                {
                    pMDT->u1TLVType = ISIS_IP_EXTERNAL_RA_TLV;
                }
            }
            else
            {
                if (pContext->u1IsisMTSupport == ISIS_TRUE)
                {
                    pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
                }
                else
                {
                    pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
                }
            }
            /* ROUTE_LEAK */
            if (pContext->SysActuals.bSysL2ToL1Leak == ISIS_TRUE)
            {
                if (u1EvtType == ISIS_EVT_L1_DECN_COMPLETE)
                {
                    pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                }
                else
                {
                    pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                }
            }
            else
            {
                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            }

            for (u1MetCnt = 0; u1MetCnt < ISIS_NUM_METRICS; u1MetCnt++)
            {
                if (pDelSPTNodes->au2Metric[u1MetCnt] == ISIS_SPT_INV_METRIC)
                {
                    pMDT->Metric[u1MetCnt] = (UINT1) ISIS_METRIC_SUPP_MASK;
                }
                else
                {
                    ISIS_COPY_METRIC (pContext, pDelSPTNodes->u4MetricVal,
                                      pMDT);
                }
            }
            if ((pMDT->u1TLVType == ISIS_IP_INTERNAL_RA_TLV) ||
                (pMDT->u1TLVType == ISIS_IP_EXTERNAL_RA_TLV) ||
                (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV))
            {
                MEMCPY (pMDT->IPAddr.au1IpAddr, pDelSPTNodes->au1DestID,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
                IsisUtlComputePrefixLen (&(pDelSPTNodes->
                                           au1DestID[ISIS_MAX_IPV4_ADDR_LEN]),
                                         ISIS_MAX_IPV4_ADDR_LEN,
                                         &(pMDT->IPAddr.u1PrefixLen));
            }
            else if ((pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                     || (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
            {
                MEMCPY (pMDT->IPAddr.au1IpAddr, pDelSPTNodes->au1DestID,
                        ISIS_MAX_IPV6_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
                ISIS_COPY_METRIC (pContext, pDelSPTNodes->u4MetricVal, pMDT);
                pMDT->IPAddr.u1PrefixLen = pDelSPTNodes->u1PrefixLen;
            }

            ISIS_DBG_PRINT_ADDR (pMDT->IPAddr.au1IpAddr,
                                 (UINT1) ISIS_MAX_IP_ADDR_LEN,
                                 "UPD <T> : Deleting Obsolete IP Addr\t",
                                 ISIS_OCTET_STRING);

            if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
            {
                IsisUpdDelExtMtIPReachTLV (pContext, pMDT, ISIS_TRUE,
                                           ISIS_FALSE);
            }
            else
            {
                IsisUpdDelIPRATLV (pContext, pMDT, ISIS_TRUE, ISIS_FALSE);
            }

            pNextSPTNode = pDelSPTNodes->pNext;
            pDelSPTNodes->pNext = NULL;

            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pDelSPTNodes);

            pDelSPTNodes = pNextSPTNode;
        }
    }
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

    /* Now that we have deleted all the Unwanted (D list) IP information from
     * the SelfLSPs, we now have to update the changed information from the 'C'
     * list into the SelfLSPs
     */

    IsisUpdAddIPRAFromPath (pContext, u1Level);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcL1DecnCompleteEvt ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcIPRAChgEvt ()
 * Description : This routine updates the SelfLSPs with the given IPRA
 *               information
 * Input (s)   : pContext - Pointer to System Context
 *               pEvtIPRA - Pointer to IPRA Change Event
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcIPRAChgEvt (tIsisSysContext * pContext,
                       tIsisEvtIPRAChange * pEvtIPRA)
{
    tIsisIPRAEntry     *pIPRARec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcIPRAChgEvt ()\n"));

    switch (pEvtIPRA->u1Status)
    {
        case ISIS_IPRA_UP:

            pIPRARec = pContext->IPRAInfo.pIPRAEntry;

            while (pIPRARec != NULL)
            {
                if ((pIPRARec->u4IPRAIdx == pEvtIPRA->u4IPRAIdx)
                    && (pIPRARec->u1IPRAType == pEvtIPRA->u1IPRAType))
                {
                    IsisUpdUpdateIPRA (pContext, pIPRARec, ISIS_CMD_ADD);
                    break;
                }
                pIPRARec = pIPRARec->pNext;
            }
            break;

        case ISIS_IPRA_DOWN:

            pIPRARec = (tIsisIPRAEntry *) ISIS_MEM_ALLOC (ISIS_BUF_IPRA,
                                                          sizeof
                                                          (tIsisIPRAEntry));
            if (pIPRARec != NULL)
            {
                pIPRARec->u4IPRAIdx = pEvtIPRA->u4IPRAIdx;
                pIPRARec->u1PrefixLen = pEvtIPRA->u1PrefixLen;
                ISIS_SET_METRIC (pContext, pIPRARec, pEvtIPRA);
                pIPRARec->u1IPRADestType = pEvtIPRA->u1IPRADestType;
                if (pEvtIPRA->u1IPRADestType == ISIS_ADDR_IPV6)
                {
                    MEMCPY (pIPRARec->au1IPRADest, pEvtIPRA->au1IPRADest,
                            ISIS_MAX_IPV6_ADDR_LEN);
                }
                else
                {
                    MEMCPY (pIPRARec->au1IPRADest, pEvtIPRA->au1IPRADest,
                            ISIS_MAX_IPV4_ADDR_LEN);
                }

                IsisUpdUpdateIPRA (pContext, pIPRARec, ISIS_CMD_DELETE);
                ISIS_MEM_FREE (ISIS_BUF_IPRA, (UINT1 *) pIPRARec);
            }
            else
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IPRA Info\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPRA);
            }

            break;

        case ISIS_IPRA_CHG:

            pIPRARec = pContext->IPRAInfo.pIPRAEntry;
            while (pIPRARec != NULL)
            {
                if ((pIPRARec->u4IPRAIdx == pEvtIPRA->u4IPRAIdx)
                    && (pIPRARec->u1IPRAType == pEvtIPRA->u1IPRAType))
                {
                    IsisUpdUpdateIPRA (pContext, pIPRARec, ISIS_CMD_MODIFY);
                    break;
                }
                pIPRARec = pIPRARec->pNext;
            }
            break;

        default:

            UPP_PT ((ISIS_LGST,
                     "UPD <E> : Invalid Status For IPRA Update in Self-LSP [ %u ]\n",
                     pEvtIPRA->u1Status));
            break;
    }

    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtIPRA);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcIPRAChgEvt ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcDISStatChgEvt ()
 * Description : This routine processes the change in DIS status of the local
 *               system. If the Local System is elected as the new DIS then 
 *               Pseudonode LSPs are generated on behalf of the local system. 
 *               If the local system is no more a DIS then it purges the 
 *               Pseudonode LSPs already generated by it.
 * Input (s)   : pContext - Pointer to System Context
 *               pCktRec  - Pointer to Circuit Record for which the Local
 *                          System is/was the DIS
 *               u1Level  - Level 1 or Level2 LSP
 *               u1Status - DIS Status of the Local IS, possible values
 *                          are ISIS_DIS_ELECTED or ISIS_DIS_RESIGNED
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcDISStatChgEvt (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                          UINT1 u1Level, UINT1 u1Status)
{
    tIsisMDT           *pMDT = NULL;
    tIsisSNLSP         *pPrevSNLSP = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNextLSP = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo      **pSNZeroLSP = NULL;
    tIsisLSPInfo      **pSNNZeroLSP = NULL;
    tIsisSPTNode       *pSPTNode = NULL;
    tIsisSPTNode       *pFreeNode = NULL;
    tIsisCktLevel      *pCktLvlRec = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisSortedHashTable *pDbHashTbl = NULL;
    tIsisSortedHashTable *pTxQHashTbl = NULL;
    tIsisHashBucket    *pHashBkt;
    tIsisLSPEntry      *pCurrentDbEntry = NULL;
    tIsisLSPEntry      *pNextDbEntry = NULL;
    tIsisLSPTxEntry    *pCurrentTxEntry = NULL;
    tIsisLSPTxEntry    *pNextTxEntry = NULL;
    UINT2               u2Len = 0;
    UINT1               u1ECTId = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcDISStatChgEvt ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        pCktLvlRec = pCktRec->pL1CktInfo;
    }
    else
    {
        pCktLvlRec = pCktRec->pL2CktInfo;
    }

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDISStatChgEvt ()\n"));
        return;
    }

    pMDT->u1Cmd = ISIS_CMD_ADD;

    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
    }
    else
    {
        pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
    }

    if (u1Level == ISIS_LEVEL1)
    {
        pMDT->u1LSPType = ISIS_L1_PSEUDO_LSP;
    }
    else if (u1Level == ISIS_LEVEL2)
    {
        pMDT->u1LSPType = ISIS_L2_PSEUDO_LSP;
    }
    else
    {
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDISStatChgEvt ()\n"));
        return;
    }
    if (u1Status == ISIS_DIS_ELECTED)
    {

        if (u1Level == ISIS_LEVEL1)
        {
            pSNLSP = pContext->SelfLSP.pL1SNLSP;
        }
        else
        {
            pSNLSP = pContext->SelfLSP.pL2SNLSP;
        }

        while (pSNLSP != NULL)
        {
            if ((pSNLSP->pCktEntry != NULL)
                && (pSNLSP->pCktEntry->u4CktIdx == pCktRec->u4CktIdx))
            {
                break;
            }
            pSNLSP = pSNLSP->pNext;
        }

        if (pSNLSP == NULL)
        {
            if (ISIS_FAILURE ==
                IsisUpdAllocLSPHdr (pContext, &pNSNLSP, ISIS_BUF_SNLI))
            {
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL
                        " : IsisUpdAllocLSPHdr Memory Allocation\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                return;
            }

            pSNLSP = (tIsisSNLSP *) pNSNLSP;

            if (u1Level == ISIS_LEVEL1)
            {
                pSNLSP->pNext = pContext->SelfLSP.pL1SNLSP;
                pContext->SelfLSP.pL1SNLSP = pSNLSP;
            }
            else
            {
                pSNLSP->pNext = pContext->SelfLSP.pL2SNLSP;
                pContext->SelfLSP.pL2SNLSP = pSNLSP;
            }
        }

        if (u1Level == ISIS_LEVEL1)
        {
            pSNZeroLSP = &(pSNLSP->pZeroLSP);
            pSNNZeroLSP = &(pSNLSP->pNonZeroLSP);
        }
        else
        {
            pSNZeroLSP = &(pSNLSP->pZeroLSP);
            pSNNZeroLSP = &(pSNLSP->pNonZeroLSP);
        }
        if (*pSNZeroLSP == NULL)
        {
            (*pSNZeroLSP) = (tIsisLSPInfo *) ISIS_MEM_ALLOC (ISIS_BUF_SLSP,
                                                             sizeof
                                                             (tIsisLSPInfo));
            if ((*pSNZeroLSP) == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Self LSP\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                /* Shutdown the Instance
                 */

                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdProcDISStatChgEvt ()\n"));
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                return;
            }
            (*pSNZeroLSP)->u1SNId = 0;
            (*pSNZeroLSP)->u1LSPNum = 0;
            (*pSNZeroLSP)->u2LSPLen = 0;
            (*pSNZeroLSP)->u2AuthStatus = ISIS_FALSE;
            (*pSNZeroLSP)->u4SeqNum = 1;
            (*pSNZeroLSP)->u1NumPSTLV = 0;
            (*pSNZeroLSP)->u1NumAATLV = 0;
            (*pSNZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
            (*pSNZeroLSP)->pNext = NULL;
            (*pSNZeroLSP)->pTLV = NULL;
        }

        if (*pSNNZeroLSP == NULL)
        {
            (*pSNNZeroLSP) = (tIsisLSPInfo *) ISIS_MEM_ALLOC (ISIS_BUF_SLSP,
                                                              sizeof
                                                              (tIsisLSPInfo));
            if ((*pSNNZeroLSP) == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Self LSP\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                /* Shutdown the Instance
                 */

                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdProcDISStatChgEvt ()\n"));
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                return;
            }
            (*pSNNZeroLSP)->u1SNId = 0;
            (*pSNNZeroLSP)->u1LSPNum = 1;
            (*pSNNZeroLSP)->u2LSPLen = 0;
            (*pSNNZeroLSP)->u2AuthStatus = ISIS_FALSE;
            (*pSNNZeroLSP)->u4SeqNum = 1;
            (*pSNNZeroLSP)->u1NumPSTLV = 0;
            (*pSNNZeroLSP)->u1NumAATLV = 0;
            (*pSNNZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
            (*pSNNZeroLSP)->pNext = NULL;
            (*pSNNZeroLSP)->pTLV = NULL;
        }

        /* Updating the Circuit pointer and the Pseudonode ID (with the
         * Circuit Local ID) in the newly allocated Pseudonode LSPs
         */

        pSNLSP->pCktEntry = pCktRec;
        pSNLSP->pZeroLSP->u1SNId = pCktRec->u1CktLocalID;
        pSNLSP->pNonZeroLSP->u1SNId = pCktRec->u1CktLocalID;
        pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        pSNLSP->pNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

        /* Pseudonode LSPs will include information about all the systems on the
         * LAN, including the local system. The information is included as IS
         * adjacencies. First add an adjacency for the local system with the
         * Pseudonode
         */

        MEMSET (pMDT->Metric, 0x00, sizeof (tIsisMetric));
        pMDT->u4FullMetric = 0;
        MEMCPY (pMDT->au1AdjSysID, pContext->SysActuals.au1SysID,
                ISIS_SYS_ID_LEN);
        pMDT->au1AdjSysID[ISIS_SYS_ID_LEN] = 0;

        IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);

        /* Get all the adjacencies that the local system currently has. This is
         * obtained as a list of SPT nodes 
         */

        IsisAdjGetCktAdjs (pContext, pCktRec, u1Level, &pSPTNode);

        while (pSPTNode != NULL)
        {
            /* We have a list of adjacencies. Add one by one
             */

            MEMSET (pMDT->Metric, 0x00, (sizeof (tIsisMetric)));
            pMDT->u4FullMetric = 0;
            MEMCPY (pMDT->au1AdjSysID, pSPTNode->au1DestID, ISIS_SYS_ID_LEN);

            /*Blocking suppressed adjacencies in LSP */
            pAdjRec = pCktRec->pAdjEntry;
            while (pAdjRec != NULL)
            {
                if (MEMCMP (pAdjRec->au1AdjNbrSysID, pSPTNode->au1DestID,
                            ISIS_SYS_ID_LEN) == 0)
                {
                    break;
                }
                pAdjRec = pAdjRec->pNext;
            }
            if ((pAdjRec != NULL)
                && (pAdjRec->u1IsisAdjGRState != ISIS_ADJ_SUPPRESSED))
            {
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }

            /* Free the SPT Nodes and start the CSNP timer
             */

            pFreeNode = pSPTNode;
            pSPTNode = pSPTNode->pNext;
            pFreeNode->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pFreeNode);
        }

        /* Stop the PSNP timers, since a DIS will have no necessity to
         * generate PSNPs. Free all the PSNPs queued on this circuit
         */

        /* This function will be called from Active as well as stand by side
         * also so timers needed to be taken care 
         */
#ifdef ISIS_FT_ENABLED
        if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
            (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
        {
#endif
            if (u1Level == ISIS_LEVEL1)
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Stopping L1 PSNP Timer - Circuit [%u]\n",
                         pCktRec->u4CktIdx));
                IsisTmrStopECTimer (pContext, ISIS_ECT_L1_PSNP,
                                    pCktRec->u4CktIdx, pCktLvlRec->u1ECTId);
            }
            else if (u1Level == ISIS_LEVEL2)
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Stopping L2 PSNP Timer - Circuit [%u]\n",
                         pCktRec->u4CktIdx));
                IsisTmrStopECTimer (pContext, ISIS_ECT_L2_PSNP,
                                    pCktRec->u4CktIdx, pCktLvlRec->u1ECTId);
            }

            TMP_PT ((ISIS_LGST, "TMR <T> : Deleting PSNPs - Circuit [%u]\n",
                     pCktRec->u4CktIdx));

            /* Freeing the PSNPs */

            IsisAdjDelCktLvlPSNPs (pCktLvlRec);

            TMP_PT ((ISIS_LGST,
                     "TMR <T> : Starting CSNP Timer - Circuit [%u]\n",
                     pCktRec->u4CktIdx));

            /* Start the CSNP timer since the local system has been elected a
             * DIS for the circuit
             */

            u1ECTId = IsisUpdStartCSNPTimer (pContext, pCktRec,
                                             (tIsisLSPTxEntry *) NULL,
                                             (tIsisLSPEntry *) NULL,
                                             pCktLvlRec->CSNPInterval, u1Level);
            ISIS_ADJ_SET_ECTID (pCktLvlRec, u1ECTId);
#ifdef ISIS_FT_ENABLED
        }
#endif

    }
    else if (u1Status == ISIS_DIS_RESIGNED)
    {
        if (u1Level == ISIS_LEVEL1)
        {
            pSNLSP = pContext->SelfLSP.pL1SNLSP;
        }
        else if (u1Level == ISIS_LEVEL2)
        {
            pSNLSP = pContext->SelfLSP.pL2SNLSP;
        }

        /* Delete the appropriate pseudonode LSPs pertaining to the circuit
         */

        while (pSNLSP != NULL)
        {
            if ((pSNLSP->pCktEntry != NULL)
                && (pSNLSP->pCktEntry->u4CktIdx == pCktRec->u4CktIdx))
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Purging Zero SNLSP - DIS Resigned - Circuit [%u]\n",
                         pCktRec->u4CktIdx));

                /* Delete the only Zero LSP we have over the circuit
                 */

                pZeroLSP = pSNLSP->pZeroLSP;
                IsisUpdFindAndPurgeSelfLSP (pContext, pZeroLSP, u1Level);
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pZeroLSP);

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Purging NonZero SNLSP - DIS Resigned - Circuit [%u]\n",
                         pCktRec->u4CktIdx));

                /* Delete each of the Non-Zero LSPs pertaining to the circuit
                 */

                pNonZeroLSP = pSNLSP->pNonZeroLSP;
                while (pNonZeroLSP != NULL)
                {
                    IsisUpdFindAndPurgeSelfLSP (pContext, pNonZeroLSP, u1Level);
                    pNextLSP = pNonZeroLSP->pNext;
                    ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pNonZeroLSP);
                    pNonZeroLSP = pNextLSP;
                }

                /* Now that the LSP is purged, remove the LSP from the list of
                 * SelfLSPs
                 */

                if (pPrevSNLSP != NULL)
                {
                    /* LSP being deleted is not the first in the chain
                     */

                    pPrevSNLSP->pNext = pSNLSP->pNext;
                }
                else
                {
                    /* LSP being deleted is the first in the chain, hence
                     * advance the head
                     */

                    if (u1Level == ISIS_LEVEL1)
                    {
                        pContext->SelfLSP.pL1SNLSP = pSNLSP->pNext;
                    }
                    else
                    {
                        pContext->SelfLSP.pL2SNLSP = pSNLSP->pNext;
                    }
                }
                pSNLSP->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_SNLI, (UINT1 *) pSNLSP);
                break;
            }
            pPrevSNLSP = pSNLSP;
            pSNLSP = pSNLSP->pNext;
        }

        /* Stop the CSNP Timer and Start the PSNP Timers, since the System is 
         * no more a DIS
         */

        TMP_PT ((ISIS_LGST, "TMR <T> : Stopping CSNP Timer - Circuit [%u]\n",
                 pCktRec->u4CktIdx));

        /* This function is called from both active and standby side so
         * timers need to be taken care
         */
#ifdef ISIS_FT_ENABLED
        if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
            (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
        {
#endif
            IsisUpdStopCSNPTimer (pContext, pCktRec, u1Level);

            if (u1Level == ISIS_LEVEL1)
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Starting L1 PSNP Timer - Circuit [%u]\n",
                         pCktRec->u4CktIdx));
                IsisTmrStartECTimer (pContext, ISIS_ECT_L1_PSNP,
                                     pCktRec->u4CktIdx,
                                     pCktLvlRec->PSNPInterval, &u1ECTId);
            }
            else
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Starting L2 PSNP Timer - Circuit [%u]\n",
                         pCktRec->u4CktIdx));
                IsisTmrStartECTimer (pContext, ISIS_ECT_L2_PSNP,
                                     pCktRec->u4CktIdx,
                                     pCktLvlRec->PSNPInterval, &u1ECTId);
            }
            ISIS_ADJ_SET_ECTID (pCktLvlRec, u1ECTId);
#ifdef ISIS_FT_ENABLED
        }
#endif
        /*MISIS - DIS resigned case */
        if (u1Level == ISIS_LEVEL1)
        {
            pDbHashTbl = &(pContext->SysL1Info.LspDB);
            pTxQHashTbl = &(pContext->SysL1Info.LspTxQ);
        }
        else if (u1Level == ISIS_LEVEL2)
        {
            pDbHashTbl = &(pContext->SysL2Info.LspDB);
            pTxQHashTbl = &(pContext->SysL2Info.LspTxQ);
        }
        if (pDbHashTbl->pHashBkts != NULL)
        {
            /* LSP records found in the given Hash Bucket */
            pHashBkt = pDbHashTbl->pHashBkts;
            pCurrentDbEntry = (tIsisLSPEntry *) (pHashBkt->pFirstElem);

            while (pCurrentDbEntry != NULL)
            {
                ISIS_EXTRACT_PDU_LEN_FROM_LSP (pCurrentDbEntry->pu1LSP, u2Len);
                IsisTrfrTxData (pCktRec, pCurrentDbEntry->pu1LSP, u2Len,
                                u1Level, ISIS_HOLD_BUF, ISIS_FALSE);
                pNextDbEntry = IsisUpdGetNextDbRec (pCurrentDbEntry, &pHashBkt);
                pCurrentDbEntry = pNextDbEntry;
            }
        }
        if (pTxQHashTbl->pHashBkts != NULL)
        {
            /* LSP records found in the given Hash Bucket */
            pHashBkt = pTxQHashTbl->pHashBkts;
            pCurrentTxEntry = (tIsisLSPTxEntry *) (pHashBkt->pFirstElem);

            while (pCurrentTxEntry != NULL)
            {
                ISIS_EXTRACT_PDU_LEN_FROM_LSP (pCurrentTxEntry->pLSPRec->pu1LSP,
                                               u2Len);
                IsisTrfrTxData (pCktRec, pCurrentTxEntry->pLSPRec->pu1LSP,
                                u2Len, u1Level, ISIS_HOLD_BUF, ISIS_FALSE);
                pNextTxEntry = IsisUpdGetNextTxRec (pCurrentTxEntry, &pHashBkt);
                pCurrentTxEntry = pNextTxEntry;
            }
        }

    }

    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDISStatChgEvt ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcDISChgEvt ()
 * Description : This routine processes the DIS change event. It deletes the
 *               adjacency information pertaining to the previous DIS from the
 *               pseudonode LSPs and adds a new adjacency information pertaining
 *               to the new DIS that has been elected
 * Input (s)   : pContext - Pointer to System Context
 *               pDISEvt  - Pointer to Event 
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcDISChgEvt (tIsisSysContext * pContext, tIsisEvtDISChg * pDISEvt)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               au1NullDIS[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
    UINT1               u1CktLevel = ISIS_LEVEL1;
    UINT4               u4CktIdx = 0;
    tIsisMDT           *pMDT = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisEvtISStatChange *pISEvt = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    UINT1               au1LSPId[ISIS_LSPID_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcDISChgEvt ()\n"));

    MEMSET (au1NullDIS, 0x00, ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
    u1CktLevel = pDISEvt->u1CktLevel;
    u4CktIdx = pDISEvt->u4CktIdx;

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

    if (i4RetVal != ISIS_SUCCESS)
    {
        /* The circuit does not exist anymore. Ignore the event
         */

        ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pDISEvt);
        UPP_PT ((ISIS_LGST, "UPD <T> : Circuit Does Not Exist - Index [ %u ]\n",
                 u4CktIdx));
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDISChgEvt ()\n"));
        return;
    }

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pDISEvt);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDISChgEvt () \n"));
        return;
    }

    /* The logic is we are removing the Previoud DIS LSPs from Our 
     * LSP Database and purging them out. To do that we need to get all
     * the previous DIS PseudoLSPs. 
     */

    MEMCPY (au1LSPId, pDISEvt->au1PrevDIS, ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
    au1LSPId[ISIS_LSPID_LEN - 1] = 0x00;

    IsisUpdPurgeLSPFromTxQ (pContext, au1LSPId, u1CktLevel);
    IsisUpdPurgeLSPFromDB (pContext, au1LSPId, u1CktLevel);

    pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
    pMDT->u1Cmd = ISIS_CMD_DELETE;

    if (u1CktLevel == ISIS_LEVEL1)
    {
        pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
        ISIS_SET_METRIC (pContext, pMDT, pCktRec->pL1CktInfo);
    }
    else
    {
        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
        ISIS_SET_METRIC (pContext, pMDT, pCktRec->pL2CktInfo);
    }

    /* We have to update our NonPseudonode LSPs by deleting the adjacency
     * information maintained for the Previous DIS and then update the
     * same LSPs with the adjacency information for the Current DIS
     */

    MEMCPY (pMDT->au1AdjSysID, pDISEvt->au1PrevDIS,
            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
        IsisUpdUpdateSelfLSP (pContext, u4CktIdx, pMDT);

        pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
        pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
        i4RetVal = IsisUpdUpdateSelfLSP (pContext, u4CktIdx, pMDT);
    }
    else
    {
        if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
        {
            pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
        }
        i4RetVal = IsisUpdUpdateSelfLSP (pContext, u4CktIdx, pMDT);
    }

    /* Add the Adjacency for the Current DIS
     */

    if (((u1CktLevel == ISIS_LEVEL1) && (pCktRec->pL1CktInfo->u4NumAdjs == 0))
        || ((u1CktLevel == ISIS_LEVEL2)
            && (pCktRec->pL2CktInfo->u4NumAdjs == 0)))

    {
        /* There is only one system on the LAN, and Hence current DIS is NULL.
         * No need to Add IS Adj TLV
         */

        ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pDISEvt);
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDISChgEvt ()\n"));
        return;
    }

    MEMCPY (pMDT->au1AdjSysID, pDISEvt->au1CurrDIS,
            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

    pMDT->u1Cmd = ISIS_CMD_ADD;

    pAdjRec = pCktRec->pAdjEntry;

    while (pAdjRec != NULL)
    {
        if (MEMCMP (pAdjRec->au1AdjNbrSysID, pMDT->au1AdjSysID,
                    ISIS_SYS_ID_LEN) == 0)
        {
            break;
        }
        pAdjRec = pAdjRec->pNext;
    }
    if (((pAdjRec != NULL)
         && (pAdjRec->u1IsisAdjGRState != ISIS_ADJ_SUPPRESSED))
        ||
        (MEMCMP
         (pDISEvt->au1CurrDIS, pContext->SysActuals.au1SysID,
          ISIS_SYS_ID_LEN) == 0))
    {
        if (pContext->u1IsisMTSupport == ISIS_TRUE)
        {
            if (pCktRec->u1IfMTId & ISIS_MT0_BITMAP)
            {
                pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                i4RetVal = IsisUpdUpdateSelfLSP (pContext, u4CktIdx, pMDT);
            }
            if (pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
            {
                pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                i4RetVal = IsisUpdUpdateSelfLSP (pContext, u4CktIdx, pMDT);
            }
        }
        else
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
            }
            i4RetVal = IsisUpdUpdateSelfLSP (pContext, u4CktIdx, pMDT);
        }
    }

    if (i4RetVal != ISIS_SUCCESS)
    {
        pISEvt = (tIsisEvtISStatChange *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtISStatChange));
        if (pISEvt != NULL)
        {
            pISEvt->u1EvtID = ISIS_EVT_IS_DOWN;
            MEMCPY (pISEvt->au1SysID, pContext->SysActuals.au1SysID,
                    ISIS_SYS_ID_LEN);
            IsisUtlSendEvent (pContext, (UINT1 *) pISEvt,
                              sizeof (tIsisEvtISStatChange));
        }
        else
        {
            /* Pitiable condition - Not even able to report errors, have every
             * reason to PANIC
             */

            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IS Down Event\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
    }

    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pDISEvt);
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDISChgEvt ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdPurgeLSPFromDB ()
 * Description : This routine processes the purges given LSP from the
 *               LSP Database
 * Input (s)   : pContext - Pointer to System Context
 *               pu1LSPId - Pointer to the LSP Identifier
 *               u1Level  - The Level of the database
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/
PRIVATE VOID
IsisUpdPurgeLSPFromDB (tIsisSysContext * pContext, UINT1 *pu1LSPId,
                       UINT1 u1Level)
{
    UINT1               u1Loc = 0;
    UINT1               u1Flag = 0;
    UINT1              *pu1LSP = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisLSPEntry      *pNextRec = NULL;
    tIsisLSPEntry      *pRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisHashBucket    *pNextHashBkt = NULL;

    pRec = IsisUpdGetLSPFromDB (pContext, pu1LSPId, u1Level, &pPrevRec,
                                &pHashBkt, &u1Loc, &u1Flag);
    pNextHashBkt = pHashBkt;
    while (pRec != NULL)
    {
        if (MEMCMP ((pRec->pu1LSP + ISIS_OFFSET_LSPID),
                    pu1LSPId, ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN) == 0)
        {
            pNextRec = IsisUpdGetNextDbRec (pRec, &pNextHashBkt);

            /* Remove the LSP Record from the Database
             */

            IsisUpdRemLSPFromDB (pContext, pPrevRec, pRec, pHashBkt,
                                 u1Level, u1Loc);

            ISIS_DEL_LSP_TIMER_NODE (pRec);

            /* Send a Lock Step Update to the standby node
             */

            ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE, pRec);

            pu1LSP = pRec->pu1LSP;

            /* Freeing the LSP associated information
             */

            ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pRec);

            if (ISIS_CONTEXT_ACTIVE (pContext) == ISIS_TRUE)
            {
                IsisUpdNWPurgeLSP (pContext, pu1LSP);
            }
            else
            {
                ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);
            }

            pRec = pNextRec;
            pHashBkt = pNextHashBkt;
        }
        else
        {
            break;
        }
    }
}

/*******************************************************************************
 * Function    : IsisUpdPurgeLSPFromTxQ ()
 * Description : This routine processes the purges given LSP from the
 *               transmission queue
 * Input (s)   : pContext - Pointer to System Context
 *               pu1LSPId - Pointer to the LSP Identifier
 *               u1Level  - The Level of the database
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/
PRIVATE VOID
IsisUpdPurgeLSPFromTxQ (tIsisSysContext * pContext, UINT1 *pu1LSPId,
                        UINT1 u1Level)
{
    UINT1               u1Loc = 0;
    UINT1               u1Flag = 0;
    UINT1              *pu1LSP = NULL;
    tIsisLSPTxEntry    *pPrevRec = NULL;
    tIsisLSPTxEntry    *pNextTxRec = NULL;
    tIsisLSPTxEntry    *pRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisHashBucket    *pNextHashBkt = NULL;

    pRec = IsisUpdGetLSPFromTxQ (pContext, pu1LSPId, u1Level, &pPrevRec,
                                 &pHashBkt, &u1Loc, &u1Flag);
    pNextHashBkt = pHashBkt;
    while (pRec != NULL)
    {
        if (MEMCMP ((pRec->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                    pu1LSPId, ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN) == 0)
        {
            pNextTxRec = IsisUpdGetNextTxRec (pRec, &pNextHashBkt);

            /* Remove the LSP Record from the TxQ
             */

            IsisUpdRemLSPFromTxQ (pContext, pPrevRec, pRec, pHashBkt,
                                  u1Level, u1Loc);

            ISIS_DEL_LSP_TIMER_NODE (pRec->pLSPRec);

            /* Send a Lock Step Update to the standby node
             */

            ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE, pRec->pLSPRec);

            pu1LSP = pRec->pLSPRec->pu1LSP;

            /* Freeing the LSP associated information
             */

            ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pRec->pLSPRec);
            ISIS_MEM_FREE (ISIS_BUF_SRMF, (UINT1 *) pRec->pu1SRM);
            ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pRec);

            if (ISIS_CONTEXT_ACTIVE (pContext) == ISIS_TRUE)
            {
                IsisUpdNWPurgeLSP (pContext, pu1LSP);
            }
            else
            {
                ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);
            }

            pRec = pNextTxRec;
            pHashBkt = pNextHashBkt;
        }
        else
        {
            break;
        }
    }
}

/*******************************************************************************
 * Function    : IsisUpdProcAdjChange ()
 * Description : This Routine updates Self LSPs with the given adjacency
 *               information based on the given status. If the status is UP, the
 *               given adjacency is added to the SelfLSPs, else the adjacency is
 *               deleted from the SelfLSPs
 * Input (s)   : pContext  - Pointer to the System Context
 *               pCktRec   - Pointer to the circuit to which the adjacency
 *                           belongs
 *               au1Metric - Metric pertaining to the circuit
 *               u1AdjLevel- Adjacency Level (L1, L2 or L12)
 *               pu1SysId  - Adjacency System ID
 *               u1Status  - Adjacency UP or DOWN
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful updation of IS Adj TLVs in Self
 *                             LSPs
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdProcAdjChange (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                      UINT1 *pu1Metric, UINT4 u4FullMetric, UINT1 u1AdjLevel,
                      UINT1 *pu1SysId, UINT1 u1Status)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisMDT           *pMDT = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcAdjChange ()\n"));

    ISIS_FORM_STR (ISIS_LSPID_LEN, pu1SysId, acLSPId);

    if (pCktRec->u1CktType == ISIS_BC_CKT)
    {
        if (((u1AdjLevel != ISIS_LEVEL1)
             && (pCktRec->pL2CktInfo != NULL)
             && (pCktRec->pL2CktInfo->bIsDIS != ISIS_TRUE))
            || ((u1AdjLevel != ISIS_LEVEL2) && (pCktRec->pL1CktInfo != NULL)
                && (pCktRec->pL1CktInfo->bIsDIS != ISIS_TRUE)))
        {
            /* Need not add the adjacency since the local system maintains 
             * adjacency only with the DIS on BC circuits
             */

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcAdjChange ()\n"));
            return i4RetVal;
        }
    }

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        i4RetVal = ISIS_FAILURE;
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcAdjChange ()\n"));
        return (i4RetVal);
    }

    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
    }
    else
    {
        pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
    }

    if (pCktRec->u1CktType == ISIS_BC_CKT)
    {
        MEMCPY (pMDT->au1AdjSysID, pu1SysId, ISIS_SYS_ID_LEN);
    }
    else
    {
        MEMCPY (pMDT->au1AdjSysID, pu1SysId, ISIS_SYS_ID_LEN);
        pMDT->au1AdjSysID[ISIS_SYS_ID_LEN] = 0;
    }

    if (u1Status == ISIS_ADJ_UP)
    {
        if (pCktRec->pAdjEntry == NULL)
        {
            /*Adj entry is not available, ignore Add call */
            ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
            return ISIS_FAILURE;
        }
        pMDT->u1Cmd = ISIS_CMD_ADD;
        ISIS_PASTE_METRIC (pContext, pMDT, u4FullMetric, pu1Metric);
    }
    else if (u1Status == ISIS_ADJ_DOWN)
    {
        pMDT->u1Cmd = ISIS_CMD_DELETE;
    }

    /* LOGIC:
     *
     * 1] Local system is a DIS
     *    a) Adjacency 'A' becomes UP on a P2P circuit
     *       ACTION: Add the adjacency to the Non-Pseudonode LSP
     *
     *    b) Adjacency 'A' becomes UP on a BC circuit
     *       ACTION: Add the adjacency to the Pseudonode LSP
     *
     * 2] Local system is not a DIS
     *    a) Adjacency 'A' becomes UP on a P2P circuit
     *       ACTION: Add the adjacency to the Non-Pseudonode LSP
     *
     *    b) Adjacency 'A' becomes UP on a BC circuit
     *       ACTION: Do nothing since on BC circuits we need not worry about
     *               adjacencies other than the DIS adjacency
     */

    if ((u1AdjLevel == ISIS_LEVEL1) || (u1AdjLevel == ISIS_LEVEL12))
    {
        if ((pCktRec->u1CktType == ISIS_BC_CKT)
            && (pCktRec->pL1CktInfo != NULL)
            && (pCktRec->pL1CktInfo->bIsDIS == ISIS_TRUE))
        {
            pMDT->u1LSPType = ISIS_L1_PSEUDO_LSP;

            /* Adjacencies added to the Pseudonode LSPs will always have Metric
             * '0'
             *
             * Refer Sec. 7.3.8 of ISO 10589
             */

            MEMSET (pMDT->Metric, 0x00, (sizeof (tIsisMetric)));
            pMDT->u4FullMetric = 0;
            i4RetVal = IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : LAN L1 Adjancency Updated In SNLSP - Circuit Index [ %u ], LSP ID [ %s ]\n",
                     pCktRec->u4CktIdx, acLSPId));
        }
        else if ((pCktRec->u1CktType == ISIS_P2P_CKT) && (pCktRec->pL1CktInfo))
        {
            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            ISIS_SET_METRIC (pCktRec->pContext, pMDT, pCktRec->pL1CktInfo);
            if ((u1Status == ISIS_ADJ_DOWN)
                ||
                ((((pCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                   && (pCktRec->pAdjEntry->u1AdjMTId & ISIS_MT0_BITMAP)
                   && (pContext->u1IsisMTSupport == ISIS_TRUE))
                  || (pContext->u1IsisMTSupport == ISIS_FALSE))
                 && (pCktRec->pAdjEntry->u1IsisAdjGRState !=
                     ISIS_ADJ_SUPPRESSED)))

            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                }
                else
                {
                    pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
                }
                i4RetVal =
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
            if ((u1Status == ISIS_ADJ_DOWN)
                || ((pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                    && (pCktRec->pAdjEntry->u1AdjMTId & ISIS_MT2_BITMAP)
                    && (pContext->u1IsisMTSupport == ISIS_TRUE)
                    && (pCktRec->pAdjEntry->u1IsisAdjGRState !=
                        ISIS_ADJ_SUPPRESSED)))
            {
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                i4RetVal =
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : P2P L1 Adjacency Updated In NSNLSP - Circuit Index [ %u ], LSP ID [ %s ]\n",
                     pCktRec->u4CktIdx, acLSPId));
        }
    }
    if ((u1AdjLevel == ISIS_LEVEL2) || (u1AdjLevel == ISIS_LEVEL12))
    {
        if ((pCktRec->u1CktType == ISIS_BC_CKT)
            && (pCktRec->pL2CktInfo != NULL)
            && (pCktRec->pL2CktInfo->bIsDIS == ISIS_TRUE))
        {
            pMDT->u1LSPType = ISIS_L2_PSEUDO_LSP;

            /* Adjacencies added to the Pseudonode LSPs will always have Metric
             * '0'
             *
             * Refer Sec. 7.3.10 of ISO 10589
             */

            MEMSET (pMDT->Metric, 0x00, (sizeof (tIsisMetric)));
            pMDT->u4FullMetric = 0;
            i4RetVal = IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : LAN L2 Adjacency Updated In SNLSP - Circuit Index [ %u ], LSP ID [ %s ]\n",
                     pCktRec->u4CktIdx, acLSPId));
        }
        else if ((pCktRec->u1CktType == ISIS_P2P_CKT) && (pCktRec->pL2CktInfo))
        {
            ISIS_SET_METRIC (pCktRec->pContext, pMDT, pCktRec->pL2CktInfo);
            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            if ((u1Status == ISIS_ADJ_DOWN)
                ||
                ((((pCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                   && (pCktRec->pAdjEntry->u1AdjMTId & ISIS_MT0_BITMAP)
                   && (pContext->u1IsisMTSupport == ISIS_TRUE))
                  || (pContext->u1IsisMTSupport == ISIS_FALSE))
                 && (pCktRec->pAdjEntry->u1IsisAdjGRState !=
                     ISIS_ADJ_SUPPRESSED)))
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                }
                else
                {
                    pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
                }
                i4RetVal =
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
            if ((u1Status == ISIS_ADJ_DOWN)
                || ((pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                    && (pCktRec->pAdjEntry->u1AdjMTId & ISIS_MT2_BITMAP)
                    && (pContext->u1IsisMTSupport == ISIS_TRUE)
                    && (pCktRec->pAdjEntry->u1IsisAdjGRState !=
                        ISIS_ADJ_SUPPRESSED)))
            {
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                i4RetVal =
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : P2P L2 Adjacency Updated In NSNLSP - Circuit Index [ %u ], LSP ID [ %s ]\n",
                     pCktRec->u4CktIdx, acLSPId));
        }
    }

    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcAdjChange ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdProcCktUp()
 * Description : This routine updates Self LSPs with IP Reachability
 *               information configured for the circuit. It also starts PSNP 
 *               Timer in the case of Broadcast Circuits and CSNP Timer for 
 *               Point to Point Circuits
 * Input (s)   : pContext - Pointer to the System Context
 *               pCktRec  - Pointer to the circuit entry
 *               u1Level  - Level of the Circuit
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcCktUp (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                  UINT1 u1Level)
{
    tIsisMDT           *pMDT = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    tIsisCktEntry      *pTempCktRec = NULL;
    UINT2               u2MTId = 0;
    UINT1               u1ECTId = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcCktUp ()\n"));

    /* The macros get the First LSP Record entries from the LSP Database and 
     * from the LSP Transmission Queue
     */

#ifdef ISIS_FT_ENABLED
    if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
        (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
    {
#endif
        if (pCktRec->u1CktType == ISIS_BC_CKT)
        {
            /* Start a PSNP timer as soon as the circuit comes up. 
             * It keeps ticking and when ever the timer fires, 
             * the Circuit is scanned to check whether any PSNP buffers exist. 
             * If any exist, the buffers are transmitted, else 
             * the processing routine returns without 
             * performing any action
             */

            if ((u1Level != ISIS_LEVEL2) && (pCktRec->pL1CktInfo != NULL))
            {
                IsisTmrStartECTimer (pContext, ISIS_ECT_L1_PSNP,
                                     pCktRec->u4CktIdx,
                                     pCktRec->pL1CktInfo->PSNPInterval,
                                     &u1ECTId);
                ISIS_ADJ_SET_ECTID (pCktRec->pL1CktInfo, u1ECTId);
            }
            if ((u1Level != ISIS_LEVEL1) && (pCktRec->pL2CktInfo != NULL))
            {
                IsisTmrStartECTimer (pContext, ISIS_ECT_L2_PSNP,
                                     pCktRec->u4CktIdx,
                                     pCktRec->pL2CktInfo->PSNPInterval,
                                     &u1ECTId);
                ISIS_ADJ_SET_ECTID (pCktRec->pL2CktInfo, u1ECTId);
            }
        }
#ifdef ISIS_FT_ENABLED
    }
#endif

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST,
                ISIS_MEM_ALLOC_FAIL " : IP Interface Address in Self LSP\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        return;
    }
    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        /* If MT #0 is alone supported don't add MT #0 TLV in LSP,
         * since it is optional*/
        if (pCktRec->u1IfMTId != ISIS_MT0_BITMAP)
        {
            pMDT->u1Cmd = ISIS_CMD_ADD;
            pMDT->u1TLVType = ISIS_MT_TLV;
            for (u2MTId = 0; u2MTId <= ISIS_MAX_MT; u2MTId++)
            {
                /* Left shift is done to get the corresponding MT_BIT_MAP values
                 * ex. for MT #0, (1 << 0) = 1 (ISIS_MT0_BITMAP)
                 *     for MT #2, (1 << 2) = 4 (ISIS_MT2_BITMAP)
                 */
                if (pCktRec->u1IfMTId & (1 << u2MTId))
                {
                    pMDT->u2MTId = u2MTId;
                    if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
                    {
                        pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                        IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
                    }

                    if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
                    {
                        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                        IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
                    }
                }
            }
            if (pCktRec->u1IfMTId == ISIS_MT2_BITMAP)
            {
                for (pTempCktRec = pContext->CktTable.pCktRec;
                     pTempCktRec != NULL; pTempCktRec = pTempCktRec->pNext)
                {
                    if (pTempCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                    {
                        pMDT->u2MTId = 0;
                        if (pTempCktRec->pContext->SysActuals.u1SysType !=
                            ISIS_LEVEL2)
                        {
                            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                            IsisUpdUpdateSelfLSP (pTempCktRec->pContext, 0,
                                                  pMDT);

                        }
                        if (pTempCktRec->pContext->SysActuals.u1SysType !=
                            ISIS_LEVEL1)
                        {
                            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                            IsisUpdUpdateSelfLSP (pTempCktRec->pContext, 0,
                                                  pMDT);
                        }
                        break;
                    }
                }
            }
        }
    }
    MEMSET (pMDT, 0, sizeof (tIsisMDT));

    pIPIfRec = pContext->IPIfTable.pIPIfAddr;

    while (pIPIfRec != NULL)
    {
        if ((pIPIfRec->u4CktIfIdx == pCktRec->u4CktIfIdx)
            && (pIPIfRec->u4CktIfSubIdx == pCktRec->u4CktIfSubIdx))
        {
            pMDT->u1Cmd = ISIS_CMD_ADD;

            if (pIPIfRec->u1AddrType == ISIS_ADDR_IPV4)
            {
                pMDT->u1TLVType = ISIS_IPV4IF_ADDR_TLV;
                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPIfRec->au1IPAddr,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;

            }
            else
            {
                pMDT->u1TLVType = ISIS_IPV6IF_ADDR_TLV;
                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPIfRec->au1IPAddr,
                        ISIS_MAX_IPV6_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;

            }
            if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
            {

                /* Updating Level1 NonPseudonode LSPs
                 */

                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }

            if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
            {

                /* Updating Level2 NonPseudonode LSPs
                 */

                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
        }
        pIPIfRec = pIPIfRec->pNext;
    }
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCktUp ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcCktDown()
 * Description : This routines deletes all the IPRAs and the adjacencies 
 *               associated with the circuit from the SelfLSP
 * Input (s)   : pContext  - Pointer to the System Context
 *               pCktRec   - Pointer to the circuit entry
 *               u1CktType - Broadcast or Point to Point
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Deletion of IPRAs and
 *                             Adjacencies from Self LSPs
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdProcCktDown (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                    UINT1 u1Level)
{
    tIsisMDT           *pMDT = NULL;
    tIsisCktEntry      *pTravCktEntry = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    UINT1               u1IfMTId = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcCktDown () \n"));

    /* Form MDT structure with Adjacency information and start deleting
     * one by one which are established over the circuit that has gone DOWN
     */

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCktDown ()\n"));
        return ISIS_FAILURE;
    }

    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        pTravCktEntry = pContext->CktTable.pCktRec;
        while (pTravCktEntry != NULL)
        {
            if (pTravCktEntry->u1IfMTId & ISIS_MT0_BITMAP)
            {
                u1IfMTId |= ISIS_MT0_BITMAP;
            }
            if (pTravCktEntry->u1IfMTId & ISIS_MT2_BITMAP)
            {
                u1IfMTId |= ISIS_MT2_BITMAP;
            }
            if ((u1IfMTId & ISIS_MT0_BITMAP) && (u1IfMTId & ISIS_MT2_BITMAP))
            {
                break;
            }
            pTravCktEntry = pTravCktEntry->pNext;
        }

        pMDT->u1Cmd = ISIS_CMD_DELETE;
        pMDT->u1TLVType = ISIS_MT_TLV;

        /* If v4 and v6 both are not supported OR if v4 alone is supported
         * we can remove all the MT-TLVs.
         * Since inclusion of MT-O TLV alone is optional*/
        if ((u1IfMTId == 0) || (u1IfMTId == ISIS_MT0_BITMAP))
        {
            if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
            {
                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                pMDT->u2MTId = 0;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }

            if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
            {
                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                pMDT->u2MTId = 0;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
        }
        /* If v6 alone is suported in any of the interface, then delete the MT-0 TLVs from LSP */
        else if (u1IfMTId == ISIS_MT2_BITMAP)
        {
            if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
            {
                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                pMDT->u2MTId = 0;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }

            if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
            {
                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                pMDT->u2MTId = 0;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
        }
    }

    pMDT->u1Cmd = ISIS_CMD_DELETE;
    pMDT->u1TLVType = ISIS_IS_ADJ_TLV;

    /* Processing for Broadcast circuits is different from P2P circuits
     */

    if (pCktRec->u1CktType == ISIS_BC_CKT)
    {
        if (((u1Level == ISIS_LEVEL1) || (u1Level == ISIS_LEVEL12)) &&
            (pCktRec->pL1CktInfo != NULL))
        {
            pAdjRec = pCktRec->pAdjEntry;
            if (pCktRec->pL1CktInfo->bIsDIS == ISIS_TRUE)
            {
                /* Local system is a DIS for the circuit which has gone DOWN.
                 * Hence we may have to delete all the Pseudonode LSPs we have
                 * in our SelfLSP database pertaining to this circuit, and also
                 * all the Adjacency information relating to adjacencies
                 * established over the circuit that has gone DOWN must be
                 * deleted
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Circuit DOWN - Local DIS - Deleting L1 SNLSP And Adjacency - Circuit [ %u ]\n",
                         pCktRec->u4CktIdx));
                pMDT->u1LSPType = ISIS_L1_PSEUDO_LSP;

                while (pAdjRec != NULL)
                {
                    if (pAdjRec->u1AdjUsage != ISIS_L2ADJ)
                    {
                        if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                        {
                            pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                        }
                        MEMCPY (pMDT->au1AdjSysID, pAdjRec->au1AdjNbrSysID,
                                ISIS_SYS_ID_LEN);
                        IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx,
                                              pMDT);
                    }
                    pAdjRec = pAdjRec->pNext;
                }
                MEMCPY (pMDT->au1AdjSysID, pContext->SysActuals.au1SysID,
                        ISIS_SYS_ID_LEN);
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);

                /* Stop the CSNP timer if active
                 */
#ifdef ISIS_FT_ENABLED
                if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
                    (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
                {
#endif
                    IsisUpdStopCSNPTimer (pContext, pCktRec, ISIS_LEVEL1);
#ifdef ISIS_FT_ENABLED
                }
#endif
            }
            else
            {
                /* Stopping the PSNP Timer if the local IS is not a DIS
                 */
#ifdef ISIS_FT_ENABLED
                if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
                    (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
                {
#endif
                    IsisTmrStopECTimer (pContext, ISIS_ECT_L1_PSNP,
                                        pCktRec->u4CktIdx,
                                        pCktRec->pL1CktInfo->u1ECTId);
#ifdef ISIS_FT_ENABLED
                }
#endif
            }

            /* Now Deleting the Adjacency information from the Non PseudoNode
             * LSP. Any system whether DIS or Non-DIS, will have only one
             * adjacency information included in its Non-Pseudonode LSPs i.e.
             * the adjacency with the DIS. Delete that adjacency since the
             * circuit itself is DOWN
             */

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Circuit DOWN - Deleting L1 Adjacencies From NSNLSP - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));

            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            MEMCPY (pMDT->au1AdjSysID, pCktRec->pL1CktInfo->au1CktLanDISID,
                    ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);

                pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
            else
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                }
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
        }
        if (((u1Level == ISIS_LEVEL2) || (u1Level == ISIS_LEVEL12)) &&
            (pCktRec->pL2CktInfo != NULL))
        {
            pAdjRec = pCktRec->pAdjEntry;
            if (pCktRec->pL2CktInfo->bIsDIS == ISIS_TRUE)
            {
                /* Local system is a DIS for the circuit which has gone DOWN.
                 * Hence we may have to delete all the Pseudonode LSPs we have
                 * in our SelfLSP database pertaining to this circuit, and also
                 * all the Adjacency information relating to adjacencies
                 * established over the circuit that has gone DOWN must be
                 * deleted
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Circuit DOWN - Deleting L2 SNLSP And Adjacencies - Circuit [ %u ]\n",
                         pCktRec->u4CktIdx));

                pMDT->u1LSPType = ISIS_L2_PSEUDO_LSP;

                while (pAdjRec != NULL)
                {
                    if (pAdjRec->u1AdjUsage != ISIS_L1ADJ)
                    {
                        if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                        {
                            pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                        }
                        MEMCPY (pMDT->au1AdjSysID, pAdjRec->au1AdjNbrSysID,
                                ISIS_SYS_ID_LEN);
                        IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx,
                                              pMDT);
                    }
                    pAdjRec = pAdjRec->pNext;
                }
                MEMCPY (pMDT->au1AdjSysID, pContext->SysActuals.au1SysID,
                        ISIS_SYS_ID_LEN);
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);

                /* Stop the CSNP timer if active
                 */
#ifdef ISIS_FT_ENABLED
                if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
                    (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
                {
#endif
                    IsisUpdStopCSNPTimer (pContext, pCktRec, ISIS_LEVEL2);
#ifdef ISIS_FT_ENABLED
                }
#endif
            }
            else
            {
                /* Stopping the PSNP Timer if the local IS is not a DIS
                 */
#ifdef ISIS_FT_ENABLED
                if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
                    (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
                {
#endif
                    IsisTmrStopECTimer (pContext, ISIS_ECT_L2_PSNP,
                                        pCktRec->u4CktIdx,
                                        pCktRec->pL2CktInfo->u1ECTId);
#ifdef ISIS_FT_ENABLED
                }
#endif
            }

            /* Now Deleting the Adjacency information from the Non PseudoNode
             * LSP. Any system whether DIS or Non-DIS, will have only one
             * adjacency information included in its Non-Pseudonode LSPs i.e.
             * the adjacency with the DIS. Delete that adjacency since the
             * circuit itself is DOWN
             */

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Circuit DOWN -  Deleting L2 Adjacencies From NSNLSP - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));

            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            MEMCPY (pMDT->au1AdjSysID, pCktRec->pL2CktInfo->au1CktLanDISID,
                    ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);

                pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
            else
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                }
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
        }
    }
    else if (pCktRec->u1CktType == ISIS_P2P_CKT)
    {
        /* P2P circuits have only one adjacency. 
         */
        pAdjRec = pCktRec->pAdjEntry;
        if (pAdjRec != NULL)
        {
            MEMCPY (pMDT->au1AdjSysID, pCktRec->pAdjEntry->au1AdjNbrSysID,
                    ISIS_SYS_ID_LEN);
        }
        else
        {
            ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCktDown ()\n"));
            return ISIS_SUCCESS;
        }

        if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
            || (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            /* P2P adjacencies are included only in the NonPseudonode LSPs
             */
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
            }
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Circuit Down - Deleting P2P Adjacency From L1 NSNLSP - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));

            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            /* Pseudonode ID is not stored for P2P circuits, assumed to be zero */
            pMDT->au1AdjSysID[ISIS_SYS_ID_LEN] = 0;
            IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }

            /* PSNP timer starts ticking once the circuit is UP and it is
             * independent of the adjacencies. Stop the timer associated with
             * the circuit
             */
#ifdef ISIS_FT_ENABLED
            if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
                (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
            {
#endif
                if (pCktRec->pL1CktInfo != NULL)
                {
                    IsisTmrStopECTimer (pContext, ISIS_ECT_L1_PSNP,
                                        pCktRec->u4CktIdx,
                                        pCktRec->pL1CktInfo->u1ECTId);
                    pCktRec->pL1CktInfo->u1ECTId = 0;
                }
#ifdef ISIS_FT_ENABLED
            }
#endif
        }
        if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
            || (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            /* P2P adjacencies are included only in the NonPseudonode LSPs
             */

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Circuit DOWN - Deleting P2P Adjacency From L2 NSNLSP - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
            }

            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            /* Pseudonode ID is not stored for P2P circuits, assumed to be zero */
            pMDT->au1AdjSysID[ISIS_SYS_ID_LEN] = 0;
            IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }

            /* PSNP timer starts ticking once the circuit is UP and it is
             * independent of the adjacencies. Stop the timer associated with
             * the circuit
             */
#ifdef ISIS_FT_ENABLED
            if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
                (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
            {
#endif
                if (pCktRec->pL2CktInfo != NULL)
                {
                    IsisTmrStopECTimer (pContext, ISIS_ECT_L2_PSNP,
                                        pCktRec->u4CktIdx,
                                        pCktRec->pL2CktInfo->u1ECTId);
                    pCktRec->pL2CktInfo->u1ECTId = 0;
                }
#ifdef ISIS_FT_ENABLED
            }
#endif
        }
        IsisUpdEnqueueP2PCktChgEvt (pCktRec->pContext, ISIS_CKT_DOWN,
                                    pCktRec->u4CktIdx);
    }

    pIPIfRec = pContext->IPIfTable.pIPIfAddr;

    while (pIPIfRec != NULL)
    {
        if ((pIPIfRec->u4CktIfIdx == pCktRec->u4CktIfIdx)
            && (pIPIfRec->u4CktIfSubIdx == pCktRec->u4CktIfSubIdx))
        {
            pMDT->u1Cmd = ISIS_CMD_DELETE;
            if (pIPIfRec->u1AddrType == ISIS_ADDR_IPV4)
            {
                pMDT->u1TLVType = ISIS_IPV4IF_ADDR_TLV;
                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPIfRec->au1IPAddr,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IPV6IF_ADDR_TLV;
                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPIfRec->au1IPAddr,
                        ISIS_MAX_IPV6_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
            }
            if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
            {
                /* Updating Level1 NonPseudonode LSPs
                 */

                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }

            if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
            {

                /* Updating Level2 NonPseudonode LSPs
                 */

                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
        }
        pIPIfRec = pIPIfRec->pNext;
    }

    /* Freeing the memory allocated for Multi Data Type Structure
     */

    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCktDown ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdProcCktModify()
 * Description : This routines updates the metric value for all the adjacencies
 *               that are associated with the circuit. The routine modifies the
 *               metric value for all the adjacencies, associated with the given
 *               circuit, included in the NonPseudonode LSPs. Pseudonode LSPs
 *               always carry a Metric of Zero and hence need not be considered 
 * Input (s)   : pContext  - Pointer to the System Context
 *               pCktRec   - Pointer to the circuit whose adjacency information
 *                           is to be updated
 *               pu1Metric - Pointer to modified Metric
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Updation of Circuit Metric
 *                             in all the IS Adj TLVs 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdProcCktModify (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                      UINT1 *pu1Metric, UINT4 u4FullMetric, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisMDT           *pMDT = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcCktModify ()\n"));

    pAdjEntry = pCktRec->pAdjEntry;

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCktModify ()\n"));
        return ISIS_FAILURE;
    }

    pMDT->u1Cmd = ISIS_CMD_MODIFY;
    pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
    ISIS_PASTE_METRIC (pContext, pMDT, u4FullMetric, pu1Metric);

    if (pCktRec->u1CktType == ISIS_BC_CKT)
    {
        /* Metric change will affect only NonPseudonode LSPs since Pseudonode
         * LSPs are always sent with Zero Metrics. So Pseudonode LSPs need not
         * be considered at all. Even in NonPseudonode case, we have to adjust
         * the metric for the adjacency with DIS, since the local system will
         * announce only the DIS adjacency in the case of BC circuits
         */

        if ((u1Level == ISIS_LEVEL1) || (u1Level == ISIS_LEVEL12))
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Updating Metric For L1 LAN DIS Adjacency - New Metric [ %u ]\n",
                         pMDT->u4FullMetric));
            }
            else
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Updating Metric For L1 LAN DIS Adjacency - New Metric [ %u ]\n",
                         pMDT->Metric[0]));
            }
            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            MEMCPY (pMDT->au1AdjSysID, pCktRec->pL1CktInfo->au1CktLanDISID,
                    ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                if (pCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
                }
                if (pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                {
                    pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                    pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
                }
            }
            else
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                }
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }

        }
        if ((u1Level == ISIS_LEVEL2) || (u1Level == ISIS_LEVEL12))
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Updating Metric For L2 LAN DIS Adjacency - New Metric [ %u ]\n",
                         pMDT->u4FullMetric));
            }
            else
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Updating Metric For L2 LAN DIS Adjacency - New Metric [ %u ]\n",
                         pMDT->Metric[0]));
            }
            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            MEMCPY (pMDT->au1AdjSysID, pCktRec->pL2CktInfo->au1CktLanDISID,
                    ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                if (pCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
                }
                if (pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                {
                    pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                    pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
                }
            }
            else
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                }
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }

        }
    }
    else if ((pCktRec->u1CktType == ISIS_P2P_CKT) && (pAdjEntry != NULL))
    {
        /* P2P circuits maintain only one adjacency
         */

        MEMCPY (pMDT->au1AdjSysID, pAdjEntry->au1AdjNbrSysID, ISIS_SYS_ID_LEN);
        /* Pseudonode ID is not stored for P2P circuits, assumed to be zero */
        pMDT->au1AdjSysID[ISIS_SYS_ID_LEN] = 0;

        if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
            || (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Updating Metric For L1 P2P Adjacency - New Metric [ %u ]\n",
                         pMDT->u4FullMetric));
            }
            else
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Updating Metric For L1 P2P Adjacency - New Metric [ %u ]\n",
                         pMDT->Metric[0]));
            }
            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                if (pCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
                }
                if (pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                {
                    pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                    pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
                }
            }
            else
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                }
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
        }
        if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
            || (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Updating Metric For L2 P2P Adjacency - New Metric [ %u ]\n",
                         pMDT->u4FullMetric));
            }
            else
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Updating Metric For L2 P2P Adjacency - New Metric [ %u ]\n",
                         pMDT->Metric[0]));
            }
            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                if (pCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
                }
                if (pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                {
                    pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                    pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                    IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
                }
            }
            else
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
                }
                IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
            }
        }
    }

    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

    /*Update the IPRA TLV with the updated Metric value. */

    i4RetVal = IsisUpdProcCktMetModify (pContext, pCktRec, pu1Metric,
                                        u4FullMetric, u1Level);
    if (i4RetVal == ISIS_FAILURE)
    {
        UPP_PT ((ISIS_LGST, "UPD <E> : Unable to Update Metric IPRA TLV\n"));
        return ISIS_FAILURE;
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCktModify ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdProcCktMetModify()
 * Description : This routines updates the metric value for all the adjacencies
 *               and IPRA Reachability routes associated with the circuit. 
 *               The routine modifies metric value for all the adjacencies & 
 *               IPRA Reachability address, associated with the given
 *               circuit, included in the NonPseudonode LSPs. Pseudonode LSPs
 *               always carry a Metric of Zero and hence need not be considered 
 * Input (s)   : pContext  - Pointer to the System Context
 *               pCktRec   - Pointer to the circuit whose adjacency information
 *                           is to be updated
 *               pu1Metric - Pointer to modified Metric
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Updation of Circuit Metric
 *                             in all the IS Adj TLVs 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdProcCktMetModify (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                         UINT1 *pu1Metric, UINT4 u4FullMetric, UINT1 u1Level)
{
    tIsisMDT           *pMDT = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcCktMetModify ()\n"));

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCktMetModify ()\n"));
        return ISIS_FAILURE;
    }

    pIPRARec = pContext->IPRAInfo.pIPRAEntry;
    while (pIPRARec != NULL)
    {
        if ((pIPRARec->u4IfIndex == pCktRec->u4CktIfIdx)
            && (pIPRARec->u1IPRAType == ISIS_AUTO_TYPE))
        {
            MEMSET (pMDT, 0x00, (sizeof (tIsisMDT)));
            pMDT->u1Cmd = ISIS_CMD_MODIFY;
            ISIS_PASTE_METRIC (pContext, pIPRARec, u4FullMetric, pu1Metric);
            ISIS_PASTE_METRIC (pContext, pMDT, u4FullMetric, pu1Metric);
            if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV6)
            {
                if (pContext->u1IsisMTSupport == ISIS_TRUE)
                {
                    pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
                }
                else
                {
                    pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
                }
                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPRARec->au1IPRADest,
                        ISIS_MAX_IPV6_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
            }
            else
            {
                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
                }
                else
                {
                    pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
                }

                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPRARec->au1IPRADest,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
            }
            pMDT->IPAddr.u1PrefixLen = pIPRARec->u1PrefixLen;
            if (u1Level == ISIS_LEVEL1)
            {
                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
            if (u1Level == ISIS_LEVEL2)
            {
                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
        }
        pIPRARec = pIPRARec->pNext;
    }
    MEMSET (pMDT, 0x00, (sizeof (tIsisMDT)));
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCktMetModify ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdProcIPIfChgEvt ()
 * Description : This routine updates the SelfLSPs with the given IP interface 
 *               address information.
 * Input (s)   : pContext    - Pointer to System Context
 *               pEvtIPIfChg - Pointer to IP I/f Address Change Event
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful updation of IP IF Address TLV
 *                             in Self LSP
 *               ISIS_FAILURE, Otherwise 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcIPIfChgEvt (tIsisSysContext * pContext,
                       tIsisEvtIPIFChange * pEvtIPIfChg)
{
    tIsisMDT           *pMDT = NULL;
    tIsisCktEntry      *pTravCktEntry = NULL;
    UINT1               u1IfMTId = 0;
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcIPIfChgEvt ()\n"));

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtIPIfChg);
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcIPIfChgEvt ()\n"));
        return;
    }

    if (pEvtIPIfChg->u1Status == ISIS_IP_IF_ADDR_ADD)
    {
        pMDT->u1Cmd = ISIS_CMD_ADD;
    }
    else if (pEvtIPIfChg->u1Status == ISIS_IP_IF_ADDR_DEL)
    {
        pMDT->u1Cmd = ISIS_CMD_DELETE;
    }

    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        pMDT->u1TLVType = ISIS_MT_TLV;
        pTravCktEntry = pContext->CktTable.pCktRec;
        while (pTravCktEntry != NULL)
        {
            if (pTravCktEntry->u1IfMTId & ISIS_MT0_BITMAP)
            {
                u1IfMTId |= ISIS_MT0_BITMAP;
            }
            if (pTravCktEntry->u1IfMTId & ISIS_MT2_BITMAP)
            {
                u1IfMTId |= ISIS_MT2_BITMAP;
            }
            if ((u1IfMTId & ISIS_MT0_BITMAP) && (u1IfMTId & ISIS_MT2_BITMAP))
            {
                break;
            }
            pTravCktEntry = pTravCktEntry->pNext;
        }

        if ((((u1IfMTId == 0) || (u1IfMTId == ISIS_MT0_BITMAP))
             && (pMDT->u1Cmd == ISIS_CMD_DELETE))
            || ((u1IfMTId & ISIS_MT0_BITMAP) && (u1IfMTId & ISIS_MT2_BITMAP)
                && (pMDT->u1Cmd == ISIS_CMD_ADD)))
        {
            if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
            {
                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                pMDT->u2MTId = 0;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }

            if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
            {
                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                pMDT->u2MTId = 0;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
        }
        else if ((u1IfMTId == ISIS_MT2_BITMAP)
                 && (pMDT->u1Cmd == ISIS_CMD_DELETE))
        {
            if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
            {
                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                pMDT->u2MTId = 0;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }

            if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
            {
                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                pMDT->u2MTId = 0;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
        }
        else if ((u1IfMTId == ISIS_MT2_BITMAP) && (pMDT->u1Cmd == ISIS_CMD_ADD))
        {
            if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
            {
                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }

            if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
            {
                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            }
        }

    }

    if (pEvtIPIfChg->u1AddrType == ISIS_ADDR_IPV4)
    {
        pMDT->u1TLVType = ISIS_IPV4IF_ADDR_TLV;
        MEMCPY (pMDT->IPAddr.au1IpAddr, pEvtIPIfChg->au1IPAddr,
                ISIS_MAX_IPV4_ADDR_LEN);
        pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
    }
    else
    {
        pMDT->u1TLVType = ISIS_IPV6IF_ADDR_TLV;
        MEMCPY (pMDT->IPAddr.au1IpAddr, pEvtIPIfChg->au1IPAddr,
                ISIS_MAX_IPV6_ADDR_LEN);
        pMDT->IPAddr.u1PrefixLen = pEvtIPIfChg->u1PrefixLen;
        pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
    }
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        /* Updating Level1 NonPseudonode LSPs
         */

        pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
        IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
    }

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        /* Updating Level2 NonPseudonode LSPs
         */

        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
        IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
    }

    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtIPIfChg);
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcIPIfChgEvt ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdAddIPRAFromPath ()
 * Description : This routines updates the L2 Self LSPs with IP Reachability
 *               information from the Current L1 Path Database which is updated 
 *               after the L1 Decision Process in an L12 System. It first
 *               tries to delete the information from the SelfLSPs before adding
 *               it subsequently
 * Input (s)   : pContext - Pointer to the System Context
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful updation of IPRA from Path
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC VOID
IsisUpdAddIPRAFromPath (tIsisSysContext * pContext, UINT1 u1Level)
{
    tIsisMDT           *pMDT = NULL;
    tIsisSPTNode       *pSPTNode = NULL;
    tIsisSPTNode       *pSPTNodeNext = NULL;
    tRBTree             PrevRBTree;
    UINT4               u4Status;
    UINT1               u1CtrlOctet = 0;
    UINT1               u1MtIndex = 0;
    UINT1               u1MetCnt = 0;
    UINT1               u1ChagStatus = 0;
    UINT1               u1AddIPRAFlag = ISIS_FALSE;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddIPRAFromPath () \n"));

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRAFromPath ()\n"));
        return;
    }
    pMDT->u1L1toL2Flag = ISIS_FALSE;
    /*If this function is called for at level2 
       and route leaking is not configured nothing needs to be done */
    if ((ISIS_LEVEL2 == u1Level)
        && (ISIS_TRUE != pContext->SysActuals.bSysL2ToL1Leak))
    {
        /* If route_leaking is not enabled,
           no need to take the L2 reachabilty and put it in L1 LSPs */
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
        return;
    }

    for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
    {
        /* Get the PrevShortPath corresponding to a particular MT.
         * PrevShortPath[0] will be considered irrespective of 
         * if multi-topology(MT) is supported or not since it refers to
         * ST path in case of ST and IPv4 unicast paths 
         * in case of MT.
         *
         * But PrevShortPath[1] will be considered only if 
         * MT is supported on a router. */

        if (u1Level == ISIS_LEVEL2)
        {
            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            PrevRBTree = pContext->SysL2Info.PrevShortPath[u1MtIndex];
        }
        else
        {
            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            PrevRBTree = pContext->SysL1Info.PrevShortPath[u1MtIndex];
        }

        pSPTNode = RBTreeGetFirst (PrevRBTree);

        while (pSPTNode != NULL)
        {
            u1CtrlOctet = pMDT->u1CtrlOctet;

            if (ISIS_LEVEL1 == u1Level)
            {
                /* L1 decision is complete, we need to update the L2 LSPs we wont be getting L2_UP/DOWN SPTNodes in here */
                if (SPT_GET_UPDOWN_FLAG (pSPTNode) == ISIS_RL_L1_DOWN)
                {
                    /* Hmm, we got leaked routes, dont send back into L2 domain....Reset the decn_flag to normal so that it wont be processed ... */
                    ISIS_DECN_RESET_MARK (pSPTNode);
                }
                else
                {
                    /* non-leaked routes .... put them in L2LSP as L2_UP */
                    pMDT->u1L1toL2Flag = ISIS_TRUE;
                    pMDT->u1CtrlOctet =
                        ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L2_UP);
                }
            }
            else
            {
                /* L2 decision is complete, we need to update the L2 LSPs we wont be getting L1_UP/DOWN SPTNodes in here also route leaking is ENABLED because we have checked above ... */
                if (SPT_GET_UPDOWN_FLAG (pSPTNode) == ISIS_RL_L2_UP)
                {

                    pMDT->u1CtrlOctet =
                        ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L1_DOWN);
                }
                else
                {
                    /* scenario unknown . but any way keep the reachability as L1_DOWN */
                    pMDT->u1CtrlOctet =
                        ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L1_DOWN);
                }
            }
            u1ChagStatus = ISIS_DECN_GET_MARK (pSPTNode);
            /* If route leak is enabled, L2 Short Path info should be added in L1 Short Path
             * even though if there is no change in L2 Short path database
             */
            if ((u1ChagStatus != ISIS_SPT_UNCHG_FLAG)
                || (u1Level == ISIS_LEVEL2))
            {
                if (pSPTNode->u1AddrType == ISIS_ADDR_IPV6)
                {
                    ISIS_IN6_IS_ADDR_LINKLOCAL (pSPTNode->au1DestID, u4Status);
                    /*Link Local Check */
                    if (u4Status == ISIS_TRUE)

                    {
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                        return;
                    }
                    ISIS_COPY_METRIC (pContext, pSPTNode->u4MetricVal, pMDT);
                    if (pContext->u1IsisMTSupport == ISIS_TRUE)
                    {
                        pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
                    }
                    else
                    {
                        pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
                    }
                    MEMCPY (pMDT->IPAddr.au1IpAddr, pSPTNode->au1DestID,
                            ISIS_MAX_IPV6_ADDR_LEN);
                    pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;

                    ISIS_DBG_PRINT_ADDR (pMDT->IPAddr.au1IpAddr,
                                         (UINT1) ISIS_MAX_IPV6_ADDR_LEN,
                                         "UPD <T> : Adding IP Addr From Path\t",
                                         ISIS_OCTET_STRING);

                    IsisUtlComputePrefixLen (&pSPTNode->
                                             au1DestID
                                             [ISIS_MAX_IPV6_ADDR_LEN],
                                             ISIS_MAX_IPV6_ADDR_LEN,
                                             &(pMDT->IPAddr.u1PrefixLen));

                    u1CtrlOctet = pMDT->u1CtrlOctet;

                    if ((pSPTNode->
                         au2Metric[0] & ISIS_SPT_EXT_MET_FLAG) !=
                        ISIS_SPT_EXT_MET_FLAG)
                    {
                        pMDT->u1CtrlOctet =
                            ISIS_SET_MET_TYPE (u1CtrlOctet,
                                               ISIS_INTERNAL_METRIC);
                    }
                    else
                    {
                        pMDT->u1CtrlOctet =
                            ISIS_SET_MET_TYPE (u1CtrlOctet,
                                               ISIS_EXTERNAL_METRIC);
                    }

                    if (u1Level == ISIS_LEVEL1)
                    {
                        u1CtrlOctet = pMDT->u1CtrlOctet;

                        pMDT->u1CtrlOctet =
                            ISIS_SET_UPDOWN_TYPE (u1CtrlOctet,
                                                  SPT_GET_UPDOWN_FLAG
                                                  (pSPTNode));
                    }
                }

                else
                {
                    for (u1MetCnt = 0; u1MetCnt < ISIS_NUM_METRICS; u1MetCnt++)
                    {
                        ISIS_COPY_METRIC (pContext, pSPTNode->u4MetricVal,
                                          pMDT);
                    }
                    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                    {
                        pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
                    }
                    else if ((pSPTNode->au2Metric[0] & ISIS_SPT_EXT_MET_FLAG)
                             != ISIS_SPT_EXT_MET_FLAG)
                    {
                        pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
                    }
                    else
                    {
                        pMDT->u1TLVType = ISIS_IP_EXTERNAL_RA_TLV;
                    }
                    MEMCPY (pMDT->IPAddr.au1IpAddr, pSPTNode->au1DestID,
                            ISIS_MAX_IPV4_ADDR_LEN);
                    pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
                    ISIS_DBG_PRINT_ADDR (pMDT->IPAddr.au1IpAddr,
                                         (UINT1) ISIS_MAX_IPV4_ADDR_LEN,
                                         "UPD <T> : Adding IP Addr From Path\t",
                                         ISIS_OCTET_STRING);
                    IsisUtlComputePrefixLen (&pSPTNode->
                                             au1DestID
                                             [ISIS_MAX_IPV4_ADDR_LEN],
                                             ISIS_MAX_IPV4_ADDR_LEN,
                                             &(pMDT->IPAddr.u1PrefixLen));

                }

                /* LOGIC: We have the current Shortest Path, from which we 
                 * are supposed to add the IPRA information into the 
                 * SelfLSPs. But the shortest path may have both modified 
                 * nodes as well as new nodes. We could have invoked 
                 * IsisUpdUpdateIPRATLV () here, but since we do not know 
                 * which of the nodes are modified and which are new, we 
                 * may have to try to delete the information
                 * from the SelfLSPs first before we add the information 
                 * from Shortest Path DB.
                 */

                /* NOTE: If the information already exist, 
                 * IsisUpdDelIPRATLV () will delete the information 
                 * before we add it again and thereby
                 * avoid duplication
                 */

                if (u1ChagStatus & ISIS_SPT_MOD_FLAG)
                {
                    pMDT->u1Cmd = ISIS_CMD_MODIFY;
                    if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) !=
                        ISIS_SUCCESS)
                    {
                        /* There is a possibility that ModifyIPRATLV
                         * fails because this IPRA information is not
                         * yet added to L1/L2 data base. A modification could have
                         * been done in L1 database or L2 Data base along with route leak,
                         * which has triggered the modification to the other database.
                         * In such case in the database where the modification failure
                         * has occured IPRA information addition has to be done
                         */

                        u1AddIPRAFlag = ISIS_TRUE;
                    }
                }
                else
                {
                    u1AddIPRAFlag = ISIS_TRUE;
                }

                if (u1AddIPRAFlag == ISIS_TRUE)
                {
                    pMDT->u1Cmd = ISIS_CMD_ADD;

                    if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        IsisUpdAddExtMtIPReachTLV (pContext, pMDT, ISIS_TRUE,
                                                   ISIS_FALSE);
                    }
                    else
                    {
                        IsisUpdAddIPRATLV (pContext, pMDT, ISIS_TRUE,
                                           ISIS_FALSE);
                    }
                }

                /* Reset the Flag to normal */
                ISIS_DECN_RESET_MARK (pSPTNode);
            }
            pSPTNodeNext = pSPTNode->pNext;
            if (pSPTNodeNext == NULL)
            {
                pSPTNodeNext = RBTreeGetNext (PrevRBTree, pSPTNode, NULL);
            }
            pSPTNode = pSPTNodeNext;
        }

        /* If multi-topology is not supported, PrevShortPath[0]
         * alone will be there. Hence breaking the loop. */
        if (pContext->u1IsisMTSupport == ISIS_FALSE)
        {
            break;
        }
    }

    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRAFromPath ()\n"));

}

/*******************************************************************************
 * Function    : IsisUpdSummarize ()
 * Description : This routine scans through the NonPseudonode SelfLSPs and
 *               Summarizes the IPRA TLVs that match the Summary Address entry.
 *               It then purges the SelfLSP if the LSP buffer becomes empty. It
 *               adds the new Summary Address to the SelfLSP in place of the 
 *               deleted IPRAs
 * Input (s)   : pContext  - Pointer to System Context
 *               pEvtSAChg - Pointer to Summary Address Change Event
 *               u1Level   - Level at which Summarization is required
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdSummarize (tIsisSysContext * pContext, tIsisEvtSummAddrChg * pEvtSAChg,
                  UINT1 u1Level)
{
    UINT1               u1RetCode = ISIS_FALSE;
    UINT1               u1RetVal = ISIS_FALSE;
    UINT1               u1Found = ISIS_FALSE;
    UINT2               u2DelCount = 0;
    tIsisMDT           *pMDT = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pPrevNZLSP = NULL;
    tIsisLSPInfo       *pTravNZeroLSP = NULL;
    tIsisIPRAEntry     *pIPRAEntry = NULL;
    UINT1               u1CtrlOctet = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdSummarize ()\n"));

    /* IPRA information is included only in the NonZero NonPseudonode LSPs
     */

    if (u1Level == ISIS_LEVEL1)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
    }
    else
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
    }

    /* We may have added IPRAs to the NonZero LSPs. Scan through the entire list
     * and try to remove matching IPRAs and replace all of them with the Summary
     * Address information
     */

    if (pNSNLSP != NULL)
    {
        pTravNZeroLSP = pNSNLSP->pNonZeroLSP;
    }

    while (pTravNZeroLSP != NULL)
    {
        u1RetVal = IsisUpdSummSelfLSP (pTravNZeroLSP, pEvtSAChg->au1IPAddr,
                                       pEvtSAChg->u1PrefixLen);
        if (u1RetVal == ISIS_TRUE)
        {
            u1RetCode = ISIS_TRUE;
        }

        if (pTravNZeroLSP->pTLV == NULL)
        {
            /* No more TLVs in the LSP buffer. We can now purge it
             */

            IsisUpdFindAndPurgeSelfLSP (pContext, pTravNZeroLSP, u1Level);

            /* Now that the LSP is purged, we can delete it from the
             * SelfLSPs
             */

            if (pPrevNZLSP == NULL)
            {
                /* The LSP to be deleted is the first in the chain
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Update - Deleting LSP Buffer From SelfLSP\n"));
                pNSNLSP->pNonZeroLSP = pTravNZeroLSP->pNext;
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pTravNZeroLSP);
                pTravNZeroLSP = pNSNLSP->pNonZeroLSP;
            }
            else
            {
                /* The LSP to be deleted is not the first in the chain
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Summary Address Update - Deleting LSP Buffer From SelfLSP\n"));
                pPrevNZLSP->pNext = pTravNZeroLSP->pNext;
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pTravNZeroLSP);
                pTravNZeroLSP = pPrevNZLSP->pNext;
            }
        }
        else
        {
            pPrevNZLSP = pTravNZeroLSP;
            pTravNZeroLSP = pTravNZeroLSP->pNext;
        }
    }

    UPP_PT ((ISIS_LGST, "UPD <T> : Adding Summary Address Entry To SelfLSP\n"));

    /* Finding the number of matching IPRAEntry with the given Summary address
     * to decrement the IPRA Count in SelfLSP 
     */

    if (u1RetCode == ISIS_TRUE)
    {
        pIPRAEntry = pContext->IPRAInfo.pIPRAEntry;
        while (pIPRAEntry != NULL)
        {
            if ((pIPRAEntry->u1IPRAAdminState == ISIS_STATE_ON)
                && (pIPRAEntry->u1IPRAExistState == ISIS_ACTIVE))
            {
                u1Found = IsisUtlCompIPAddr (pIPRAEntry->au1IPRADest,
                                             pEvtSAChg->u1PrefixLen,
                                             pEvtSAChg->au1IPAddr,
                                             pEvtSAChg->u1PrefixLen);
                if (u1Found == ISIS_TRUE)
                {
                    u2DelCount++;
                }
            }
            pIPRAEntry = pIPRAEntry->pNext;
        }

        /* Now Add The summary address to Self LSP
         */

        pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));
        if (pMDT == NULL)
        {
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdSummarize ()\n"));
            return;
        }

        pMDT->u1Cmd = ISIS_CMD_ADD;

        ISIS_SET_METRIC (pContext, pMDT, pEvtSAChg);
        pMDT->IPAddr.u1PrefixLen = pEvtSAChg->u1PrefixLen;
        if (pEvtSAChg->u1AddrType == ISIS_ADDR_IPV4)
        {
            MEMCPY (pMDT->IPAddr.au1IpAddr, pEvtSAChg->au1IPAddr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;

            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
            }
        }
        else
        {
            MEMCPY (pMDT->IPAddr.au1IpAddr, pEvtSAChg->au1IPAddr,
                    ISIS_MAX_IPV6_ADDR_LEN);
            pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;

            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
            }

            u1CtrlOctet = pMDT->u1CtrlOctet;
            pMDT->u1CtrlOctet =
                ISIS_SET_MET_TYPE (u1CtrlOctet, ISIS_INTERNAL_METRIC);
        }
        ISIS_DBG_PRINT_ADDR (pMDT->IPAddr.au1IpAddr,
                             (UINT1) ISIS_MAX_IP_ADDR_LEN,
                             "UPD <T> : Summarizing IPRAs With\t",
                             ISIS_OCTET_STRING);

        if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
             || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
            && (u1Level == ISIS_LEVEL1))
        {
            /* Update the L1 Self LSP with the reachability information
             */

            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            if ((pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                || (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
            {
                u1CtrlOctet = pMDT->u1CtrlOctet;
                pMDT->u1CtrlOctet =
                    ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L1_UP);
            }

            IsisCtrlUpdtSAUsageCnt (pContext, (UINT1 *) pMDT->IPAddr.au1IpAddr,
                                    pMDT->IPAddr.u1PrefixLen, u2DelCount,
                                    u1Level, ISIS_CMD_ADD);

            if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))

            {
                IsisUpdAddExtMtIPReachTLV (pContext, pMDT, ISIS_FALSE,
                                           ISIS_TRUE);
            }
            else
            {
                IsisUpdAddIPRATLV (pContext, pMDT, ISIS_FALSE, ISIS_TRUE);
            }
        }
        if (((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
             || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
            && (u1Level == ISIS_LEVEL2))
        {
            /* Update the L2 Self LSP with the reachability information
             */

            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            if ((pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                || (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
            {
                u1CtrlOctet = pMDT->u1CtrlOctet;
                pMDT->u1CtrlOctet =
                    ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L2_UP);
            }

            IsisCtrlUpdtSAUsageCnt (pContext, (UINT1 *) pMDT->IPAddr.au1IpAddr,
                                    pMDT->IPAddr.u1PrefixLen, u2DelCount,
                                    u1Level, ISIS_CMD_ADD);
            if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
            {
                IsisUpdAddExtMtIPReachTLV (pContext, pMDT, ISIS_FALSE,
                                           ISIS_TRUE);
            }
            else
            {
                IsisUpdAddIPRATLV (pContext, pMDT, ISIS_FALSE, ISIS_TRUE);
            }
        }

        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdSummarize ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdSummSelfLSP ()
 * Description : This routine deletes all the IPRA TLVs that match the given IP
 *               Address in the first 'u1PrefLen' bits.
 * Input (s)   : pTravNZeroLSP - Pointer to Non Zero LSP
 *               pu1IPAddr     - Pointer to IP Address
 *               u1PrefLen     - IP Address Prefix Length
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_TRUE if Matching IPRA TLV is deleted,
 *               ISIS_FALSE else.
 ******************************************************************************/

PRIVATE UINT1
IsisUpdSummSelfLSP (tIsisLSPInfo * pTravNZeroLSP, UINT1 *pu1IPAddr,
                    UINT1 u1PrefLen)
{
    UINT1               u1RetVal = ISIS_FALSE;
    UINT1               u1RetCode = ISIS_FALSE;
    tIsisIPRATLV       *pTLV = NULL;
    tIsisIPRATLV       *pNextTLV = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdSummSelfLSP ()\n"));

    pTLV = (tIsisIPRATLV *) RBTreeGetFirst (pTravNZeroLSP->IPRATLV);
    while (pTLV != NULL)
    {
        pNextTLV = (tIsisIPRATLV *) RBTreeGetNext
            (pTravNZeroLSP->IPRATLV, (tRBElem *) pTLV, NULL);

        if ((pTLV->u1Code == ISIS_IP_INTERNAL_RA_TLV) ||
            (pTLV->u1Code == ISIS_IPV6_RA_TLV) ||
            (pTLV->u1Code == ISIS_EXT_IP_REACH_TLV) ||
            (pTLV->u1Code == ISIS_MT_IPV6_REACH_TLV))

        {
            if (pTLV->u1Code == ISIS_IP_INTERNAL_RA_TLV)
            {
                u1RetVal = IsisUtlCompIPAddr ((pTLV->au1Value +
                                               ISIS_IPV4RA_PREFIX_OFFSET),
                                              u1PrefLen, pu1IPAddr, u1PrefLen);
            }
            else if (pTLV->u1Code == ISIS_IPV6_RA_TLV)
            {
                u1RetVal = IsisUtlCompIPAddr ((pTLV->au1Value +
                                               ISIS_IPV6RA_PREFIX_OFFSET),
                                              u1PrefLen, pu1IPAddr, u1PrefLen);
            }
            else if (pTLV->u1Code == ISIS_EXT_IP_REACH_TLV)
            {
                u1RetVal = IsisUtlCompIPAddr ((pTLV->au1Value +
                                               ISIS_EXT_IPV4RA_PREFIX_OFFSET),
                                              u1PrefLen, pu1IPAddr, u1PrefLen);
            }
            else
            {
                u1RetVal = IsisUtlCompIPAddr ((pTLV->au1Value +
                                               ISIS_MT_IPV6RA_PREFIX_OFFSET),
                                              u1PrefLen, pu1IPAddr, u1PrefLen);
            }

            if (u1RetVal == ISIS_TRUE)
            {
                u1RetCode = ISIS_TRUE;

                /* A matching IPRA ia found. Delete the TLV
                 */
                RBTreeRemove (pTravNZeroLSP->IPRATLV, (tRBElem *) pTLV);
                pTravNZeroLSP->u1NumIPRATLV--;
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTLV);
                pTLV = NULL;
            }
        }

        /*Continue with the next TLV */
        pTLV = pNextTLV;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdSummSelfLSP ()\n"));
    return (u1RetCode);
}

/*******************************************************************************
 * Function    : IsisUpdDeSummarize ()
 * Description : This routine deletes the given Summary Address entry from the
 *               SelfLSPs and adds all internal IPRAs that match the deleted 
 *               entry into the SelfLSP. Since External IPRAs are not
 *               summarized, they are not considered during DeSummarization
 * Input (s)   : pContext  - Pointer to System Context
 *               pEvtSAChg - Pointer to Summary Address Changed event
 *               u1Level   - Level at which De-summarization is required
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdDeSummarize (tIsisSysContext * pContext, tIsisEvtSummAddrChg * pEvtSAChg,
                    UINT1 u1Level)
{
    tIsisMDT           *pMDT = NULL;
    tIsisSPTNode       *pSPTNode = NULL;
    tIsisSPTNode       *pSPTNodeIn = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4MaxMetric = 0;
    UINT2               u2UsageCnt = 0;
    UINT1               u1CtrlOctet = 0;
    UINT1               u1MtIndex = 0;
    UINT1               u1MetCnt = 0;
    UINT1               u1PrefLen = 0;
    UINT1               u1RetVal = ISIS_FALSE;
    tIsisRRDRoutes     *pRRDRoute = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDeSummarize ()\n"));

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDeSummarize ()\n"));
        return;
    }

    /* Removing the Summary Address TLV from Self LSPs
     */

    pMDT->u1Cmd = ISIS_CMD_DELETE;

    ISIS_SET_METRIC (pContext, pMDT, pEvtSAChg);
    if (pEvtSAChg->u1AddrType == ISIS_IPV4)
    {
        if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
        {
            pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
        }
        else
        {
            pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
        }

        MEMCPY (pMDT->IPAddr.au1IpAddr, pEvtSAChg->au1IPAddr,
                ISIS_MAX_IPV4_ADDR_LEN);
        pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
    }
    else
    {
        if (pContext->u1IsisMTSupport == ISIS_TRUE)
        {
            pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
        }
        else
        {
            pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
        }

        MEMCPY (pMDT->IPAddr.au1IpAddr, pEvtSAChg->au1IPAddr,
                ISIS_MAX_IPV6_ADDR_LEN);
        pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
    }
    pMDT->IPAddr.u1PrefixLen = pEvtSAChg->u1PrefixLen;
    ISIS_DBG_PRINT_ADDR (pMDT->IPAddr.au1IpAddr, pMDT->IPAddr.u1Length,
                         "UPD <T> : Deleting Summary Address\t",
                         ISIS_OCTET_STRING);
    UPP_PT ((ISIS_LGST, "UPD <T> : Prefix Length Of Deleted SA Entry [ %u ]\n",
             pMDT->IPAddr.u1PrefixLen));

    if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
         || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        && (u1Level == ISIS_LEVEL1))
    {
        /* Delete the given Summary Address information from L1 NonPseudonode 
         * SelfLSP
         */

        pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
        if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
            (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
        {
            i4RetVal =
                IsisUpdDelExtMtIPReachTLV (pContext, pMDT, ISIS_FALSE,
                                           ISIS_TRUE);
        }
        else
        {
            i4RetVal =
                IsisUpdDelIPRATLV (pContext, pMDT, ISIS_FALSE, ISIS_TRUE);
        }
    }

    if (((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
         || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        && (u1Level == ISIS_LEVEL2))
    {
        /* Delete the given Summary Address information from L2 NonPseudonode 
         * SelfLSP
         */

        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;

        if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
            (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
        {
            i4RetVal =
                IsisUpdDelExtMtIPReachTLV (pContext, pMDT, ISIS_FALSE,
                                           ISIS_TRUE);
        }
        else
        {
            i4RetVal =
                IsisUpdDelIPRATLV (pContext, pMDT, ISIS_FALSE, ISIS_TRUE);
        }
    }
    /* Go through each of the circuits and add all the matching IPRAs to the
     * SelfLSP
     */

    for (pIPRARec = pContext->IPRAInfo.pIPRAEntry; pIPRARec != NULL;
         pIPRARec = pIPRARec->pNext)
    {
        u1RetVal = IsisUtlCompIPAddr (pIPRARec->au1IPRADest,
                                      pEvtSAChg->u1PrefixLen,
                                      pEvtSAChg->au1IPAddr,
                                      pEvtSAChg->u1PrefixLen);
        if (u1RetVal == ISIS_TRUE)
        {
            u2UsageCnt++;
            pMDT->u1Cmd = ISIS_CMD_ADD;
            ISIS_SET_METRIC (pContext, pMDT, pIPRARec);
            if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
            {
                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPRARec->au1IPRADest,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
            }
            else
            {
                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPRARec->au1IPRADest,
                        ISIS_MAX_IPV6_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
                u1CtrlOctet = pMDT->u1CtrlOctet;
                pMDT->u1CtrlOctet =
                    ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L2_UP);
            }
            pMDT->IPAddr.u1PrefixLen = pIPRARec->u1PrefixLen;

            if ((pIPRARec->Metric[0] & ISIS_METRIC_TYPE_MASK)
                == ISIS_METRIC_TYPE_MASK)
            {
                /* External Metric Type bit is set. We are not Summarizing
                 * External IPRAs and hence need not worry about
                 * Desummarizing now
                 */

                continue;
            }
            else
            {
                /* External Metric Type bit is not set. Hence the IPRA is of
                 * type Internal Reachable IPRA which can be added to
                 * both the Level1 and Level2 LSPs
                 */
                if (pIPRARec->u1IPRADestType == ISIS_IPV6)
                {
                    if (pContext->u1IsisMTSupport == ISIS_TRUE)
                    {
                        pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
                    }
                    else
                    {
                        pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
                    }
                    u1CtrlOctet = pMDT->u1CtrlOctet;
                    pMDT->u1CtrlOctet = ISIS_SET_MET_TYPE (u1CtrlOctet,
                                                           ISIS_INTERNAL_METRIC);

                }
                else
                {
                    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                    {
                        pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
                    }
                    else
                    {
                        pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
                    }

                }
                if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
                     || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
                    && (u1Level == ISIS_LEVEL1))
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Adding Internal L1 IPRA To SelfLSP\n"));
                    if ((pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                        || (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        u1CtrlOctet = pMDT->u1CtrlOctet;
                        pMDT->u1CtrlOctet =
                            ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L1_UP);
                    }
                    pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;

                    if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))

                    {
                        i4RetVal =
                            IsisUpdAddExtMtIPReachTLV (pContext, pMDT,
                                                       ISIS_FALSE, ISIS_TRUE);
                    }
                    else
                    {
                        i4RetVal =
                            IsisUpdAddIPRATLV (pContext, pMDT, ISIS_FALSE,
                                               ISIS_TRUE);
                    }
                }
                if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
                    && (u1Level == ISIS_LEVEL2))
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Adding Internal L2 IPRA To SelfLSP\n"));
                    pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;

                    if ((pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                        || (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        u1CtrlOctet = pMDT->u1CtrlOctet;
                        pMDT->u1CtrlOctet =
                            ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L2_UP);
                    }
                    if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        i4RetVal =
                            IsisUpdAddExtMtIPReachTLV (pContext, pMDT,
                                                       ISIS_FALSE, ISIS_TRUE);
                    }
                    else
                    {
                        i4RetVal =
                            IsisUpdAddIPRATLV (pContext, pMDT, ISIS_FALSE,
                                               ISIS_TRUE);
                    }
                }

            }
        }
    }

    pRRDRoute = (tIsisRRDRoutes *) RBTreeGetFirst (pContext->RRDRouteRBTree);
    while (pRRDRoute != NULL)
    {
        u1RetVal = IsisUtlCompIPAddr (pRRDRoute->IPAddr.au1IpAddr,
                                      pEvtSAChg->u1PrefixLen,
                                      pEvtSAChg->au1IPAddr,
                                      pEvtSAChg->u1PrefixLen);
        if (u1RetVal == ISIS_TRUE)
        {
            u2UsageCnt++;
            pMDT->u1Cmd = ISIS_CMD_ADD;
            pMDT->u1SrcProtoId = pRRDRoute->u1Protocol;
            if (pRRDRoute->IPAddr.u1AddrType == ISIS_ADDR_IPV4)
            {
                MEMCPY (pMDT->IPAddr.au1IpAddr, pRRDRoute->IPAddr.au1IpAddr,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
            }
            else
            {
                MEMCPY (pMDT->IPAddr.au1IpAddr, pRRDRoute->IPAddr.au1IpAddr,
                        ISIS_MAX_IPV6_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
                u1CtrlOctet = pMDT->u1CtrlOctet;
                pMDT->u1CtrlOctet =
                    ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L2_UP);
            }
            pMDT->IPAddr.u1PrefixLen = pRRDRoute->IPAddr.u1PrefixLen;

            {
                /* External Metric Type bit is not set. Hence the IPRA is of
                 * type Internal Reachable IPRA which can be added to
                 * both the Level1 and Level2 LSPs
                 */
                if (pRRDRoute->IPAddr.u1AddrType == ISIS_IPV6)
                {
                    if (pContext->u1IsisMTSupport == ISIS_TRUE)
                    {
                        pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
                    }
                    else
                    {
                        pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
                    }
                    u1CtrlOctet = pMDT->u1CtrlOctet;
                    pMDT->u1CtrlOctet = ISIS_SET_MET_TYPE (u1CtrlOctet,
                                                           ISIS_INTERNAL_METRIC);
                }
                else
                {
                    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                    {
                        pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
                    }
                    else
                    {
                        pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
                    }

                }
                ISIS_COPY_METRIC (pContext, pRRDRoute->u4Metric, pMDT);

                if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
                     || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
                    && (u1Level == ISIS_LEVEL1))
                {
                    RTP_PT ((ISIS_LGST,
                             "RTM <T> : Adding RRD Route To SelfLSP\n"));
                    if ((pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                        || (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        u1CtrlOctet = pMDT->u1CtrlOctet;
                        pMDT->u1CtrlOctet =
                            ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L1_UP);
                    }
                    pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;

                    if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))

                    {
                        i4RetVal =
                            IsisUpdAddExtMtIPReachTLV (pContext, pMDT,
                                                       ISIS_FALSE, ISIS_TRUE);
                    }
                    else
                    {
                        i4RetVal =
                            IsisUpdAddIPRATLV (pContext, pMDT, ISIS_FALSE,
                                               ISIS_TRUE);
                    }
                }
                if (((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
                     || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
                    && (u1Level == ISIS_LEVEL2))
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Adding Internal L2 IPRA To SelfLSP\n"));
                    pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;

                    if ((pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                        || (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        u1CtrlOctet = pMDT->u1CtrlOctet;
                        pMDT->u1CtrlOctet =
                            ISIS_SET_UPDOWN_TYPE (u1CtrlOctet, ISIS_RL_L2_UP);
                    }
                    if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        i4RetVal =
                            IsisUpdAddExtMtIPReachTLV (pContext, pMDT,
                                                       ISIS_FALSE, ISIS_TRUE);
                    }
                    else
                    {
                        i4RetVal =
                            IsisUpdAddIPRATLV (pContext, pMDT, ISIS_FALSE,
                                               ISIS_TRUE);
                    }
                }

            }
        }
        pRRDRoute =
            RBTreeGetNext (pContext->RRDRouteRBTree, (tRBElem *) pRRDRoute,
                           NULL);
    }

    IsisCtrlUpdtSAUsageCnt (pContext, pEvtSAChg->au1IPAddr,
                            pEvtSAChg->u1PrefixLen, u2UsageCnt, u1Level,
                            ISIS_CMD_DELETE);

    /* For Level2 Desummarization, we have to scan the L1 Shortest Path
     * Database, and add all the IP addresses that match the given Summary 
     * Address entry
     */

    if (u1Level == ISIS_LEVEL2)
    {
        /* Summarization is requested for Level2
         */

        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Adding Internal IPRAs From PATH To SelfLSP\n"));

        for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
        {
            pSPTNode =
                RBTreeGetFirst (pContext->SysL1Info.PrevShortPath[u1MtIndex]);
            pSPTNodeIn = pSPTNode;

            while (pSPTNode != NULL)
            {
                /* We have the mask, get the prefix length and compare 
                 * the given Summary Address entry with the IP address 
                 * in the PATH nodes
                 */
                if (pEvtSAChg->u1AddrType == ISIS_ADDR_IPV4)
                {
                    IsisUtlComputePrefixLen (&(pSPTNode->
                                               au1DestID
                                               [ISIS_MAX_IPV4_ADDR_LEN]),
                                             ISIS_MAX_IPV4_ADDR_LEN,
                                             &u1PrefLen);

                    u1RetVal = IsisUtlCompIPAddr (pSPTNode->au1DestID,
                                                  pEvtSAChg->
                                                  u1PrefixLen,
                                                  pEvtSAChg->au1IPAddr,
                                                  pEvtSAChg->u1PrefixLen);
                }
                else
                {
                    IsisUtlComputePrefixLen (&(pSPTNode->
                                               au1DestID
                                               [ISIS_MAX_IPV6_ADDR_LEN]),
                                             ISIS_MAX_IPV6_ADDR_LEN,
                                             &u1PrefLen);
                    u1RetVal =
                        IsisUtlCompIPAddr (pSPTNode->au1DestID,
                                           pEvtSAChg->u1PrefixLen,
                                           pEvtSAChg->au1IPAddr,
                                           pEvtSAChg->u1PrefixLen);
                }
                /* Even if the Address Matches (u1RetVal == ISIS_TRUE), we 
                 * have to consider entries which are of type Internal 
                 * IPRs only, since External IPRAs are never summarized
                 *
                 * NOTE: We are not verifying all metrics for Internal or
                 * External type because all metrics associated with an IP
                 * address must be of the same type i.e. either INTERNAL or
                 * ETERNAL. Hence we check only the Default metric, which 
                 * always ocupies Index '0' in the Metric arrays
                 */

                if ((u1RetVal == ISIS_TRUE)
                    && ((pSPTNode->au2Metric[0] & ISIS_SPT_EXT_MET_FLAG)
                        != ISIS_SPT_EXT_MET_FLAG))
                {
                    /* PATH node IP Address matched the given Summary 
                     * Address entry. Add the IPRA TLV corresponding to 
                     * the matched address and is an internal IPRA
                     */

                    pMDT->u1Cmd = ISIS_CMD_ADD;
                    if ((pSPTNode->
                         au2Metric[0] & ISIS_SPT_IPADDR_TYPE_MASK) ==
                        ISIS_IPV4_ADDR_TYPE)
                    {
                        if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                        {
                            pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
                        }
                        else
                        {
                            pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
                        }
                    }
                    else
                    {
                        if (pContext->u1IsisMTSupport == ISIS_TRUE)
                        {
                            pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
                        }
                        else
                        {
                            pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
                        }

                        u1CtrlOctet = pMDT->u1CtrlOctet;
                        pMDT->u1CtrlOctet =
                            ISIS_SET_MET_TYPE (u1CtrlOctet,
                                               ISIS_INTERNAL_METRIC);
                    }

                    pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;

                    u4MaxMetric =
                        ((pContext->u1MetricStyle ==
                          ISIS_STYLE_WIDE_METRIC) ? ISIS_MAX_LNK_MET_MT :
                         ISIS_MAX_LNK_MET);

                    for (u1MetCnt = 0; u1MetCnt < ISIS_NUM_METRICS; u1MetCnt++)
                    {
                        if (pSPTNode->u4MetricVal > u4MaxMetric)
                        {
                            /* Metric exceeding Max Link Metric, hence 
                             * set it to Max Link Metric value
                             */
                            ISIS_COPY_METRIC (pContext, u4MaxMetric, pMDT);
                        }
                        else
                        {
                            ISIS_COPY_METRIC (pContext, pSPTNode->u4MetricVal,
                                              pMDT);
                        }
                    }
                    if ((pMDT->u1TLVType == ISIS_IPV6_RA_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        MEMCPY (pMDT->IPAddr.au1IpAddr,
                                pSPTNode->au1DestID, ISIS_MAX_IPV6_ADDR_LEN);
                        pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
                    }
                    else
                    {
                        MEMCPY (pMDT->IPAddr.au1IpAddr,
                                pSPTNode->au1DestID, ISIS_MAX_IPV4_ADDR_LEN);
                        pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
                    }
                    pMDT->IPAddr.u1PrefixLen = u1PrefLen;

                    /* No need to check with Summary Address entries now. 
                     * Hence the third argument is 'ISIS_FALSE'
                     */

                    if ((pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV) ||
                        (pMDT->u1TLVType == ISIS_MT_IPV6_REACH_TLV))
                    {
                        IsisUpdAddExtMtIPReachTLV (pContext, pMDT, ISIS_FALSE,
                                                   ISIS_FALSE);
                    }
                    else
                    {
                        IsisUpdAddIPRATLV (pContext, pMDT, ISIS_FALSE,
                                           ISIS_FALSE);
                    }
                }
                pSPTNode = pSPTNode->pNext;
                if (pSPTNode == NULL)
                {
                    RBTreeGetNext (pContext->SysL1Info.PrevShortPath[u1MtIndex],
                                   pSPTNodeIn, NULL);
                    pSPTNodeIn = pSPTNode;
                }
            }
            if (pContext->u1IsisMTSupport == ISIS_FALSE)
            {
                break;
            }
        }
    }

    UNUSED_PARAM (i4RetVal);
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDeSummarize ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcDBOLEvt ()
 * Description : This function Sets the overload bit in LSP Flags and sets
 *               dirty flag for all ZeroSelfLSPs. IT is invoked either when the
 *               manager sets the OverLoad status flag or at run time when the
 *               Update module does not have enough resources to process
 *               received LSPs.
 * Input(s)    : pContext - Pointer to the System Context
 *               pDBOLEvt - Pointer to Database Overload Event
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcDBOLEvt (tIsisSysContext * pContext, tIsisEvtLSPDBOL * pDBOLEvt)
{
    UINT1              *pu1LSPFlags = NULL;
    UINT1              *pu1MTLSPFlags = NULL;
    UINT1               u1Level = 0;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcDBOLEvt ()\n"));

    u1Level = pDBOLEvt->u1Level;

    if (u1Level == ISIS_LEVEL1)
    {
        pu1LSPFlags = &(pContext->SelfLSP.u1L1LSPFlags);
        pu1MTLSPFlags = &(pContext->SelfLSP.au1L1MTLspFlag[ISIS_MT2_INDEX]);
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
    }
    else
    {
        pu1LSPFlags = &(pContext->SelfLSP.u1L2LSPFlags);
        pu1MTLSPFlags = &(pContext->SelfLSP.au1L2MTLspFlag[ISIS_MT2_INDEX]);
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
    }

    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pDBOLEvt);

    *pu1LSPFlags |= ISIS_DBOL_MASK;
    *pu1MTLSPFlags |= ISIS_MT_LSP_OFLAG_MASK;
    pNSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

    while (pSNLSP != NULL)
    {
        pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        pSNLSP = pSNLSP->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDBOLEvt ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcDBOLClearEvt ()
 * Description : This function clears the overload bit in LSP Flags and sets
 *               dirty flag for Zero SelfLSPs if Overload bit is not 
 *               administratively set.
 * Input(s)    : pContext - Pointer to the System Context
 *               pDBOLEvt - Pointer to Database Overload Event
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcDBOLClearEvt (tIsisSysContext * pContext, tIsisEvtLSPDBOL * pEvent)
{
    UINT1               u1Level = 0;
    UINT1              *pu1LSPFlags = NULL;
    UINT1              *pu1MTLSPFlags = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcDBOLClearEvt ()\n"));

    u1Level = pEvent->u1Level;
    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvent);

    if ((u1Level == ISIS_LEVEL1)
        && ((pContext->SysActuals.u1SysSetOL != ISIS_LL_SET_L1OVERLOAD)
            && ((pContext->SysActuals.u1SysSetOL != ISIS_LL_SET_L12OVERLOAD))))
    {
        pu1LSPFlags = &(pContext->SelfLSP.u1L1LSPFlags);
        pu1MTLSPFlags = &(pContext->SelfLSP.au1L1MTLspFlag[ISIS_MT2_INDEX]);
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
    }
    else if ((u1Level == ISIS_LEVEL2)
             && ((pContext->SysActuals.u1SysSetOL != ISIS_LL_SET_L2OVERLOAD)
                 &&
                 ((pContext->SysActuals.u1SysSetOL !=
                   ISIS_LL_SET_L12OVERLOAD))))
    {
        pu1LSPFlags = &(pContext->SelfLSP.u1L2LSPFlags);
        pu1MTLSPFlags = &(pContext->SelfLSP.au1L2MTLspFlag[ISIS_MT2_INDEX]);
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDBOLClearEvt ()\n"));
        return;
    }

    *pu1LSPFlags &= ~ISIS_DBOL_MASK;
    *pu1MTLSPFlags &= ~ISIS_MT_LSP_OFLAG_MASK;
    pNSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

    while (pSNLSP != NULL)
    {
        pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        pSNLSP = pSNLSP->pNext;
    }

    if ((pContext->SysActuals.u1SysType != ISIS_LEVEL1)
        && (u1Level == ISIS_LEVEL2))
    {
        IsisUpdAddIPRAFromPath (pContext, u1Level);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcDBOLClearEvt ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcLSPDBCorp ()
 * Description : This function Sends CSNPs on Point to Point Circuits and
 *               on Broadcast circuits on which the Local System is DIS.
 * Input(s)    : pContext    - Pointer to the System Context
 *               pDBCorpEvt - Pointer to Database Corruption Event
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcLSPDBCorp (tIsisSysContext * pContext, tIsisEvtCorrLsp * pDBCorpEvt)
{
    UINT1               u1Level = 0;
    UINT2               u2PDULen = 0;
    UINT1               u1PDUType = 0;
    UINT2               u2Offset = 0;
    UINT1              *pu1CSNP = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcLSPDBCorp ()\n"));

    u1Level = pDBCorpEvt->u1Level;
    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pDBCorpEvt);

    if (u1Level == ISIS_LEVEL1)
    {
        u1PDUType = ISIS_L1CSNP_PDU;
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
    }
    else
    {
        u1PDUType = ISIS_L2CSNP_PDU;
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
    }

    /* If Authentication is enabled we may have to add the Authentication
     * information in the PDU. So leave enough space for the Authentication
     * information in the PDU
     */

    u2PDULen = (UINT1) (((u1Level == ISIS_LEVEL1) ?
                         pContext->SysActuals.SysAreaTxPasswd.u1Len :
                         pContext->SysActuals.SysDomainTxPasswd.u1Len));

    if (u2PDULen != 0)
    {
        /* Reserve 1 Bytes each for 'CODE', 'LEN' and 'AUTHENTICATION TYPE'
         * fields as Authentication is enabled
         */

        u2PDULen += 3;
    }

    u2PDULen += ISIS_GET_PDU_HDR_LEN (u1PDUType);
    pu1CSNP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_CSNP, u2PDULen);

    if (pu1CSNP == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Constructing CSNP\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_CSNP);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSPDBCorp ()\n"));
        return;
    }

    IsisUtlFillComHdr (pContext, pu1CSNP, u1PDUType);

    u2Offset = sizeof (tIsisComHdr);

    ISIS_ASSIGN_2_BYTES (pu1CSNP, u2Offset, u2PDULen);
    u2Offset += 2;

    /* Fill the SourceID
     */

    ISIS_ASSIGN_STRING (pu1CSNP, u2Offset, pContext->SysActuals.au1SysID,
                        ISIS_SYS_ID_LEN);

    u2Offset = (UINT2) (u2Offset + ISIS_SYS_ID_LEN);

    /* Pseudonode ID must be filled as ZERO. Refer Sec. 9.10 of ISO 10589
     */

    ISIS_ASSIGN_1_BYTE (pu1CSNP, u2Offset, 0);
    u2Offset++;

    /* We fill Start LSPID with Minimum Number (i.e all Zeros)
     * and End LSPID with Maximum Number (i.e all 0xFF)
     * in the PDU. 
     */

    MEMSET ((pu1CSNP + u2Offset), 0x00, ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
    u2Offset += ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN;

    MEMSET ((pu1CSNP + u2Offset), 0xFF, ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
    u2Offset += ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN;

    IsisUpdAddAuthToPDU (pContext, pu1CSNP, &u2Offset, u1Level);
    ISIS_SET_CTRL_PDU_LEN (pu1CSNP, u2Offset);

    for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
         pCktRec = pCktRec->pNext)
    {
        if ((pCktRec->u1CktType == ISIS_P2P_CKT) ||
            ((pCktRec->u1CktLevel != u1Level)
             && (pCktRec->u1CktLevel != ISIS_LEVEL12)))
        {
            continue;
        }

        pCktLevel = (u1Level == ISIS_LEVEL1) ? pCktRec->pL1CktInfo
            : pCktRec->pL2CktInfo;

        /* Delete All PSNPs for the Circuit
         */

        IsisAdjDelCktLvlPSNPs (pCktLevel);

        if ((pCktRec->u1CktType == ISIS_BC_CKT)
            && (pCktLevel->bIsDIS != ISIS_TRUE))
        {
            continue;
        }

        IsisTrfrTxData (pCktRec, pu1CSNP, (UINT1) u2PDULen, u1Level,
                        ISIS_HOLD_BUF, ISIS_FALSE);
    }

    ISIS_MEM_FREE (ISIS_BUF_CSNP, pu1CSNP);

    /* Set the Dirty Flag of All the Self LSPs, so that they are generated again
     */

    pNSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

    pNonZeroLSP = pNSNLSP->pNonZeroLSP;
    while (pNonZeroLSP != NULL)
    {
        pNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        pNonZeroLSP = pNonZeroLSP->pNext;
    }

    while (pSNLSP != 0)
    {
        pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        pNonZeroLSP = pSNLSP->pNonZeroLSP;

        while (pNonZeroLSP != NULL)
        {
            pNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
            pNonZeroLSP = pNonZeroLSP->pNext;
        }
        pSNLSP = pSNLSP->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSPDBCorp ()\n"));

}

/*******************************************************************************
 * Function    : IsisUpdProcDistanceEvt ()
 * Description : This function updates Distance for existing routes in database 
 *               and updates to RTM
 * Input(s)    : pContext    - Pointer to the System Context
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcDistanceEvt (tIsisSysContext * pContext, UINT1 u1Level,
                        UINT1 u1MtIndex, UINT1 u1MetIdx)
{
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pCurrList = NULL;
    tRBTree             CurrRBTree;
#ifdef ROUTEMAP_WANTED
    tIsisRouteInfo      RouteInfo;
    tIsisSPTNode       *pNext = NULL;
    UINT1               u1MetricType = 0;
    UINT1               u1PrefLen = 0;

    MEMSET (&RouteInfo, 0, sizeof (tIsisRouteInfo));
#endif
    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisUpdProcDistanceEvt ()\n"));

    CurrRBTree =
        (u1Level ==
         ISIS_LEVEL1) ? pContext->SysL1Info.
        PrevShortPath[u1MtIndex] : pContext->SysL2Info.PrevShortPath[u1MtIndex];

    pTrav = (tIsisSPTNode *) RBTreeGetFirst (CurrRBTree);

    while (pTrav != NULL)
    {
#ifdef ROUTEMAP_WANTED
        /*Address Type */
        if (pTrav->u1AddrType == ISIS_ADDR_IPV4)
        {
            IsisUtlComputePrefixLen (&pTrav->au1DestID[ISIS_MAX_IPV4_ADDR_LEN],
                                     ISIS_MAX_IPV4_ADDR_LEN, &u1PrefLen);
            RouteInfo.DestIpAddr.u1PrefixLen = u1PrefLen;
            MEMCPY (RouteInfo.DestIpAddr.au1IpAddr, pTrav->au1DestID,
                    ISIS_MAX_IPV4_ADDR_LEN);
        }
        else if (pTrav->u1AddrType == ISIS_ADDR_IPV6)
        {
            RouteInfo.DestIpAddr.u1PrefixLen = pTrav->u1PrefixLen;
            MEMCPY (RouteInfo.DestIpAddr.au1IpAddr, pTrav->au1DestID,
                    ISIS_MAX_IPV6_ADDR_LEN);
        }
        RouteInfo.DestIpAddr.u1AddrType = pTrav->u1AddrType;

        /* Metric Type */
        if ((pTrav->au2Metric[0] & ISIS_SPT_EXT_MET_FLAG)
            == ISIS_SPT_EXT_MET_FLAG)
        {
            /* Metric type: Supported only External type1. */
            u1MetricType = RMAP_METRIC_TYPE_TYPE1EXTERNAL;
        }
        else if (u1Level == ISIS_LEVEL1)
        {
            u1MetricType = RMAP_METRIC_TYPE_INTRA_AREA;
        }
        else if (u1Level == ISIS_LEVEL2)
        {
            u1MetricType = RMAP_METRIC_TYPE_INTER_AREA;
        }

        /* Route map entries */
        if (ISIS_FAILURE ==
            IsisApplyInFilter (pContext->pDistributeInFilterRMap, &RouteInfo,
                               u1MetricType))
        {
            /* Stop processing this route and continue with next route */
            pNext = RBTreeGetNext (CurrRBTree, pTrav, NULL);
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTrav);
            pTrav = pNext;
            continue;
        }
#endif

        pCurrList = pTrav;

        while (pCurrList != NULL)
        {
            /* Mark these nodes so that these will get updated into the
             * routing table once the verification process is complete
             *
             * As these nodes are new nodes added for this Destination,
             * send these to the Routing Table as New Routes to the
             * Destination ID
             */

            IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_ADD,
                                  pCurrList, u1MetIdx, u1Level, u1MtIndex);

            pCurrList = pCurrList->pNext;
        }

        pTrav = RBTreeGetNext (CurrRBTree, pTrav, NULL);
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisUpdProcDistanceEvt ()\n"));
}

/****************************************************************************
 * Function    : IsisHostNmeSupportChgEvt ()
 * Description : The function is called when  Enable/disable of dynamic
 *               hostname
 * Input       : None
 * Output      : None
 * Globals     : None
 * Returns     :
 ****************************************************************************/

PUBLIC VOID
IsisHostNmeSupportChgEvt (tIsisMsg * pBuf)
{
    UINT1               au1OldSysID[ISIS_SYS_ID_LEN];
    UINT4               u4Status = 0;
    UINT4               u4SysInstIdx = 0;
    tIsisSysContext    *pContext;
    tIsisMDT           *pMDT = NULL;

    MEMSET (au1OldSysID, 0, ISIS_SYS_ID_LEN);
    u4SysInstIdx = pBuf->u4CxtOrIfindex;
    if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
    {
        /* Discard the Event */
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSPDBCorp ()\n"));
        return;
    }

    if ((pContext == NULL) || (pBuf->pu1Msg == NULL))
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSPDBCorp ()\n"));
        return;
    }

    /*** copy status to offset 0 ***/
    CRU_BUF_Copy_FromBufChain ((tOsixMsg *) (VOID *) pBuf->pu1Msg,
                               (UINT1 *) &u4Status, 0, sizeof (u4Status));
    if (u4Status == ISIS_CMD_ADD)
    {
        ISIS_SET_SELF_HSTNME ();
        MEMSET (au1OldSysID, 0, ISIS_SYS_ID_LEN);

        if (MEMCMP (pContext->SysConfigs.au1SysID,
                    au1OldSysID, ISIS_SYS_ID_LEN) != 0)
        {
            /*To  traverse the DB and Tx Q */
            if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
                || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
            {
                IsisUpdReFillHstNmeTable (pContext, ISIS_LEVEL1);
            }
            if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
                || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
            {
                IsisUpdReFillHstNmeTable (pContext, ISIS_LEVEL2);
            }
            IsisHostNmeMDT (pContext, pMDT, (UINT1) u4Status);
            IsisUpdHostNmeTable (pContext, pContext->SysConfigs.au1SysID,
                                 gau1IsisHstNme, ISIS_CMD_ADD, 0);
        }
    }
    else if (u4Status == ISIS_CMD_MODIFY)
    {
    /*** copy map name to offset 4 ***/
        CRU_BUF_Copy_FromBufChain ((tOsixMsg *) (VOID *) pBuf->pu1Msg,
                                   au1OldSysID, sizeof (u4Status),
                                   ISIS_SYS_ID_LEN);
        ISIS_SET_SELF_HSTNME ();
        IsisHostNmeMDT (pContext, pMDT, ISIS_CMD_DELETE);
        IsisUpdHostNmeTable (pContext, au1OldSysID,
                             gau1IsisHstNme, ISIS_CMD_DELETE, 0);
        MEMSET (au1OldSysID, 0, ISIS_SYS_ID_LEN);
        if (MEMCMP (pContext->SysConfigs.au1SysID,
                    au1OldSysID, ISIS_SYS_ID_LEN) != 0)
        {
            IsisHostNmeMDT (pContext, pMDT, ISIS_CMD_ADD);
            IsisUpdHostNmeTable (pContext, pContext->SysConfigs.au1SysID,
                                 gau1IsisHstNme, ISIS_CMD_ADD, 0);
        }
        else
        {
            IsisDelAllHstNmeInCxt (pContext);
        }
    }
    else
    {
        IsisHostNmeMDT (pContext, pMDT, (UINT1) u4Status);
        IsisDelAllHstNmeInCxt (pContext);
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSPDBCorp ()\n"));
    return;
}
