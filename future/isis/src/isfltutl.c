 /******************************************************************************
 * Copyright (C) Future Software Limited,  2001
 *
 * $Id: isfltutl.c,v 1.5 2014/10/28 11:27:24 siva Exp $
 *
 * Description: This file contains the Fault Tolerance Module utility routines.
 *
 *****************************************************************************/

#ifdef ISIS_FT_ENABLED

#include "isincl.h"
#include "isextn.h"

/* The FLTR Header Format which is included after the PNI/FTM Message header, 
 * in Bulk and Lock Step updates is as follows:
 *
 * ----------------------------------------------------------------- 
 * | Command (1)         | Database Type (1) |    Length (2)       |
 * -----------------------------------------------------------------
 * | Database Format (1) |  Num Entries (1)  |    Reserved (2)     |
 * ----------------------------------------------------------------- 
 *
 * where 
 * -- Command speifies whether the included Database is to be added,
 *    deleted or modified
 * -- Database Type specifies the Database being updated
 * -- Length indicates the lengthof the database encoded in the message
 *    including the header
 * -- Database Format specifies whether the Database is being updated as
 *    full entries or the message includes a special encoding where only the
 *    changed fields are encoded.
 * -- Num entries indicates the total number of entries of the specified
 *    database type included in the message
 * -- Reserved, currently not used and is reserved for future use. Helps in
 *    aligning the start of Data to a 4 byte boundary
 *
 * The actual data will be encoded after the reserved field   
 */

/****************************************************************************
 * Function    : IsisFltrFillHdr()
 * Description : The FLTR module invokes this function to fill the Isis
 *               specific Fault Tolerance Header. This is as described in the
 *               FutureISIS Design Document.
 * Input       : pu1Buf       - Pointer to the buffer where the Header is filled
 *               u1Command    - Command specifing whether whether the data is
 *                              added, deleted or modified
 *               u1DbType     - Type of Database
 *               u1EntryCount - Indicates the total number of entries of the
 *                              database Type sent as Update to the Peer Node
 * Output      : pu2Offset    - Pointer to the Offset of the Buffer 'pu1Buf'
 * Globals     : None
 * Returns     : VOID 
 ****************************************************************************/

PUBLIC VOID
IsisFltrFillHdr (tRmMsg * pRmMsg, UINT1 u1Command, UINT1 u1DbType,
                 UINT1 u1EntryCount, UINT2 *pu2Offset)
{
    ISIS_RED_PUT_1_BYTE (pRmMsg, *pu2Offset, u1Command);

    ISIS_RED_PUT_1_BYTE (pRmMsg, *pu2Offset, u1DbType);

    /* Length of the Buffer which is filled later after the 
     * construction of the buffer.
     */

    *pu2Offset += 2;

    /* Data Format to be filled in the Header,
     * ISIS_FMT_FULL is the data format if the Database is Added or deleted,
     * whereas ISIS_FMT_PARTIAL is the data format if there are modifications in
     * the Database.
     * Currently only LSP LSUs alone are sent as modifications with their
     * Remaining Life Time and Sequence Number as the modified Data
     */

    if ((u1Command == ISIS_CMD_MODIFY) && (u1DbType == ISIS_DB_LSPS))
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, *pu2Offset, ISIS_FMT_PARTIAL);
    }
    else
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, *pu2Offset, ISIS_FMT_FULL);
    }

    /* Copy the number of entries count into the buffer.
     */

    ISIS_RED_PUT_1_BYTE (pRmMsg, *pu2Offset, u1EntryCount);

    /* Reserved space
     */

    *pu2Offset += 2;
}

/*******************************************************************************
 * Function    : IsisFltrGetFirstCktEntry () 
 * Description : This routine fetches the first valid circuit record from the
 *               configured System Instances. It scans each of the instances in
 *               order to fetch the first valid circuit record
 * Input(s)    : pContext - Pointer to the System Context
 * Output(s)   : pCktRec  - Pointer to the first valid Circuit Record
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if a valid circuit record is available
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetFirstCktEntry (tIsisSysContext ** pContext, tIsisCktEntry ** pCktRec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetFirstCktEntry ()\n"));

    /* Get the first Circuit Record in the given context
     */

    *pCktRec = (*pContext)->CktTable.pCktRec;

    while (*pCktRec == NULL)
    {
        /* There are no valid circuits in the current context. Move to the
         * next context
         */

        *pContext = (*pContext)->pNext;

        if (*pContext == NULL)
        {
            /* We have exhausted all the contexts, and did not fetch a valid
             * circuit record. 
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetFirstCktEntry ()\n"));
            return (ISIS_FAILURE);
        }
        else
        {
            /* Get the first circuit record from the current context 
             */

            *pCktRec = (*pContext)->CktTable.pCktRec;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetFirstCktEntry ()\n"));
    return (ISIS_SUCCESS);
}

/******************************************************************************
 * Function    : IsisFltrGetFirstAdjEntry () 
 * Description : This routine fetches the first valid adjacency record from the
 *               configured System Instances. It scans each of the instances and
 *               circuits in order to fetch the first valid adjacency record
 * Input(s)    : pContext - Pointer to the System Context
 * Output(s)   : pCktRec  - Pointer to the Circuit Record updated while
 *                          retrieving a valid adjacency
 *               pAdjRec  - Pointer to the Adjacency Record updated while
 *                          retrieving a valid adjacency
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an adjacency is successfully retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetFirstAdjEntry (tIsisSysContext ** pContext,
                          tIsisCktEntry ** pCktRec, tIsisAdjEntry ** pAdjRec)
{

    INT4                i4RetVal = ISIS_SUCCESS;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetFirstAdjEntry ()\n"));

    i4RetVal = IsisFltrGetFirstCktEntry (pContext, pCktRec);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* No valid circuits available and hence there can be no adjacencies
         */

        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetFirstAdjEntry ()\n"));
        return i4RetVal;
    }
    else
    {
        /* Got hold of a valid circuit. Start from this circuit and fetch
         * adjacencies, if available
         */

        *pAdjRec = (*pCktRec)->pAdjEntry;
    }

    while (*pAdjRec == NULL)
    {
        /* We have a circuit, but the circuit still does not have any valid
         * adjacencies. Proceed till all the circuits are covered
         */

        i4RetVal = IsisFltrGetNextCktEntry (pContext, pCktRec);

        if (i4RetVal == ISIS_FAILURE)
        {
            /* None of the circuits scanned in all the available contexts
             * contain any valid adjacency. Get back and continue with other
             * databases, if any available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetFirstAdjEntry ()\n"));
            return i4RetVal;
        }
        else
        {
            /* Try the adjacencies on the current circuit
             */

            *pAdjRec = (*pCktRec)->pAdjEntry;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetFirstAdjEntry ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrGetFirstIPRAEntry () 
 * Description : This routine fetches the first valid IPRA record from the
 *               configured System Instances. It scans each of the instances and
 *               circuits in order to fetch the first valid IPRA record
 * Input(s)    : pContext - Pointer to the System Context
 * Output(s)   : pCktRec  - Pointer to the Circuit Record updated while
 *                          retrieving a valid IPRA
 *               pIPRARec - Pointer to the IPRA Record updated while
 *                          retrieving a valid IPRA
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an IPRA record is successfully retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetFirstIPRAEntry (tIsisSysContext ** pContext,
                           tIsisIPRAEntry ** pIPRARec)
{

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetFirstIPRAEntry ()\n"));

    *pIPRARec = (*pContext)->IPRAInfo.pIPRAEntry;

    while (*pIPRARec == NULL)
    {
        *pContext = (*pContext)->pNext;

        if (*pContext == NULL)
        {
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetFirstIPRAEntry ()\n"));
            return ISIS_FAILURE;
        }
        else
        {
            /* Try the IPRAs on the current circuit
             */

            *pIPRARec = (*pContext)->IPRAInfo.pIPRAEntry;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetFirstIPRAEntry ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrGetFirstIPIFRec () 
 * Description : This routine fetches the first valid IP I/f Address record from
 *               the configured System Instances. It scans each of the instances
 *               in order to fetch the first valid IP I/f Address record
 * Input(s)    : pContext - Pointer to the System Context
 * Output(s)   : pIPIfRec - Pointer to the IP I/f Address Record updated while
 *                          retrieving a valid IPRA
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an IP I/f Address record is successfully 
 *                             retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetFirstIPIFRec (tIsisSysContext ** pContext, tIsisIPIfAddr ** pIpIfRec)
{

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetFirstIPIFRec ()\n"));

    *pIpIfRec = (*pContext)->IPIfTable.pIPIfAddr;

    while (*pIpIfRec == NULL)
    {
        /* We have a Context, but the context still does not have any valid
         * IP I/f Addresses. Proceed till all the contexts are covered
         */

        *pContext = (*pContext)->pNext;

        if (*pContext == NULL)
        {
            /* None of the contexts scanned contain any valid IP Interface 
             * Address. Get back and continue with other databases, if any 
             * available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetFirstIPIFRec ()\n"));
            return (ISIS_FAILURE);
        }
        else
        {
            /* Try the IP Interface Addresses available in the current Context
             */

            *pIpIfRec = (*pContext)->IPIfTable.pIPIfAddr;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetFirstIPIFRec ()\n"));
    return (ISIS_SUCCESS);
}

/*******************************************************************************
 * Function    : IsisFltrGetFirstSAEntry () 
 * Description : This routine fetches the first valid Summary Area Address 
 *               record from the configured System Instances. It scans each of 
 *               the instances in order to fetch the first valid SAA record
 * Input(s)    : pContext - Pointer to the System Context
 * Output(s)   : pSARec   - Pointer to the Summary Area Address Record updated 
 *                          while retrieving a valid SAA
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an Summary Area Address record is successfully
 *                             retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetFirstSAEntry (tIsisSysContext ** pContext, tIsisSAEntry ** pSARec)
{

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetFirstSAEntry ()\n"));

    *pSARec = (*pContext)->SummAddrTable.pSAEntry;

    while (*pSARec == NULL)
    {
        /* We have a Context, but the context still does not have any valid
         * Summary Addresses. Proceed till all the contexts are covered
         */

        *pContext = (*pContext)->pNext;

        if (*pContext == NULL)
        {
            /* None of the contexts scanned contain any valid Summary 
             * Address. Get back and continue with other databases, if any 
             * available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetFirstSAEntry ()\n"));
            return (ISIS_FAILURE);
        }
        else
        {
            /* Try the Summary Addresses available in the current Context
             */

            *pSARec = (*pContext)->SummAddrTable.pSAEntry;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetFirstSAEntry ()\n"));
    return (ISIS_SUCCESS);
}

/*******************************************************************************
 * Function    : IsisFltrGetFirstMAAEntry () 
 * Description : This routine fetches the first valid Manual Area Address 
 *               record from the configured System Instances. It scans each of 
 *               the instances in order to fetch the first valid MAA record
 * Input(s)    : pContext - Pointer to the System Context
 * Output(s)   : pMAARec  - Pointer to the Manual Area Address Record updated 
 *                          while retrieving a valid MAA
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an Manual Area Address record is successfully
 *                             retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetFirstMAAEntry (tIsisSysContext ** pContext, tIsisMAAEntry ** pMAARec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetFirstMAAEntry ()\n"));

    *pMAARec = (*pContext)->ManAddrTable.pMAARec;

    while (*pMAARec == NULL)
    {
        /* We have a Context, but the context still does not have any valid
         * Manual Area Addresses. Proceed till all the contexts are covered
         */

        *pContext = (*pContext)->pNext;

        if (*pContext == NULL)
        {
            /* None of the contexts scanned contain any valid Manual Area 
             * Address. Get back and continue with other databases, if any 
             * available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetFirstMAAEntry ()\n"));
            return (ISIS_FAILURE);
        }
        else
        {
            /* Try the Manual Area Addresses available in the current Context
             */

            *pMAARec = (*pContext)->ManAddrTable.pMAARec;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetFirstMAAEntry ()\n"));
    return (ISIS_SUCCESS);
}

/*******************************************************************************
 * Function    : IsisFltrGetFirstAdjAreaEntry () 
 * Description : This routine fetches the first valid Adjacency Area Address 
 *               record from the configured System Instances. It scans each of 
 *               the instances in order to fetch the first valid Adjacency Area
 *               Address record
 * Input(s)    : pContext  - Pointer to the System Context
 *               pAdjRec   - Pointer to the Adjacency Record from where the
 *                           search for the Adjacency record to begin
 *               pAdjAARec - Pointer to the Adjacency Area Address Record 
 *                           from where search for the Adjacencies Area 
 *                           Address Record to begin
 * Output(s)   : pCktRec   - Pointer to the Circuit Record updated while
 *                           retrieving a valid adjacency area address
 *               pAdjRec   - Pointer to the Adjacency Record updated while
 *                           retrieving a valid adjacency area address
 *               pAdjAARec - Pointer to the Adjacency Area Address Record 
 *                           updated while retrieving a valid adjacency area 
 *                           address
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an Adjacency Area Address record is 
 *                             successfully retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetFirstAdjAreaEntry (tIsisSysContext ** pContext,
                              tIsisCktEntry ** pCktRec,
                              tIsisAdjEntry ** pAdjRec,
                              tIsisAdjAAEntry ** pAdjAARec)
{

    INT4                i4RetVal = ISIS_SUCCESS;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetFirstAdjAreaEntry ()\n"));

    /* First try to get a valid adjacency before proceeding to fetch Adjacency
     * Area Addresses
     */

    i4RetVal = IsisFltrGetFirstAdjEntry (pContext, pCktRec, pAdjRec);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* No valid adjacencies exist. Get back and proceed with any other
         * database, if available
         */

        FTP_EE ((ISIS_LGST,
                 "FLT <X> : Exiting IsisFltrGetFirstAdjAreaEntry ()\n"));
        return i4RetVal;
    }
    else
    {
        /* Got hold of a valid adjacency. Proceed encoding the Adjacency area
         * addresses available on the current adjacency
         */

        (*pAdjAARec) = (*pAdjRec)->pAdjAreaAddr;
    }

    while (*pAdjAARec == NULL)
    {
        /* We have an Adjacency, but the Adjacency still does not have any valid
         * Adjacency Area Addresses. Proceed till all the Adjacencies are 
         * covered
         */

        i4RetVal = IsisFltrGetNextAdjEntry (pContext, pCktRec, pAdjRec);

        if (i4RetVal == ISIS_FAILURE)
        {
            /* None of the Adjacencies scanned contain any valid Adjacency Area
             * Addresses. Get back and continue with other databases, if any 
             * available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetFirstAdjAreaEntry ()\n"));
            return i4RetVal;
        }
        else
        {
            /* Try the Adjacency Area Addresses available in the current
             * Adjacency
             */

            (*pAdjAARec) = (*pAdjRec)->pAdjAreaAddr;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetFirstAdjAreaEntry ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrGetNextCktEntry () 
 * Description : This routine fetches the next circuit record from the database
 *               in order. This means the routine will return pCktRec->pNext if
 *               possible. Otherwise it will scan the other contexts, if
 *               available, to get a valid circuit record
 * Input(s)    : pContext  - Pointer to the System Context containing the given
 *                           Circuit Record (pCktRec)
 *               pCktRec   - Pointer to the Circuit Record from where the search
 *                           for the next record is supposed to begin
 * Output(s)   : pContext  - Pointer to the System Context from where the next
 *                           circuit record is fetched. This may be the same as
 *                           the incoming value if circuits are available in the
 *                           same context
 *               pCktRec   - Pointer to the Circuit Record which is next to the
 *                           given pCktRec, in order
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if a next Circuit Record is fetched successfully
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextCktEntry (tIsisSysContext ** pContext, tIsisCktEntry ** pCktRec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextCktEntry ()\n"));

    *pCktRec = (*pCktRec)->pNext;

    while (*pCktRec == NULL)
    {
        /* There are no more circuits in the current context.Hence move over to
         * other contexts, if available, and start fetching circuits
         */

        *pContext = (*pContext)->pNext;

        if (*pContext == NULL)
        {
            /* We have exhausted all Contexts and were unable to fetch any more
             * circuits.
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetNextCktEntry ()\n"));
            return ISIS_FAILURE;
        }
        else
        {
            *pCktRec = (*pContext)->CktTable.pCktRec;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextCktEntry ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrGetNextCktLvlEntry () 
 * Description : This routine fetches the next circuit level record from the 
 *               database in order. It tries to fetch the next Circuit Level
 *               record from the same circuit if possible. If not it moves on to
 *               the next circuit. If all circuits are exhausted in the
 *               current context then it moves on to the next context and tries
 *               to fetch circuit level records. If all contexts and circuits
 *               are exhausted, it returns 
 * Input(s)    : pContext   - Pointer to the System Context containing the given
 *                            Circuit Record (pCktRec) and Circuit Level Record
 *                            (pCktLevelRec)
 *               pCktRec    - Pointer to the Circuit Record containing the given
 *                            Circuit Level Record (pCktLevelRec)
 *               pCktLvlRec - Pointer to the Circuit Level Record from where
 *                            the search for the next record is supposed to 
 *                            begin
 * Output(s)   : pContext     - Pointer to the System Context updated while
 *                              retrieving the next Circuit Level Record
 *               pCktRec      - Pointer to the Circuit Record updated while
 *                              retrieving the next circuit level record
 *               pCktLevelRec - Pointer to the Circuit Level Record which is 
 *                              next to the given pCktLevelRec, in order
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if a next Circuit Level Record is fetched 
 *                             successfully
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextCktLvlEntry (tIsisSysContext ** pContext,
                            tIsisCktEntry ** pCktRec,
                            tIsisCktLevel ** pCktLevelRec)
{
    INT4                i4RetVal = ISIS_SUCCESS;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextCktLvlEntry ()\n"));

    /* Very first time this routine is invoked, pCktLevelRec will be NULL, but
     * a proper circuit record would have been fetched. Try to get the Level1
     * or Level2 record from the given circuit, if available. If not move on to
     * the next available circuit and try the same thing. If all circuits are
     * done with, move to the next context and repeat the same till a valid
     * Circuit Level record is found or the complete database is exhausted
     */

    if ((*pCktLevelRec) == NULL)
    {
        /* Try to get the Circuit Level record from the given circuit
         */

        if ((*pCktRec)->pL1CktInfo != NULL)
        {
            *pCktLevelRec = (*pCktRec)->pL1CktInfo;
        }
        else if ((*pCktRec)->pL2CktInfo != NULL)
        {
            *pCktLevelRec = (*pCktRec)->pL2CktInfo;
        }
    }
    else if (((*pCktLevelRec)->u1CktLvlIdx == ISIS_LEVEL1)
             && ((*pCktRec)->pL2CktInfo != NULL))
    {
        /* We have already processed a level1 record. Now try to fetch a level2
         * if available
         */

        *pCktLevelRec = (*pCktRec)->pL2CktInfo;
    }
    else
    {
        /* Both Level1 and (possibly) Level2 records are processed. Nothing more
         * to be done on this circuit.
         */

        *pCktLevelRec = NULL;
    }

    while (*pCktLevelRec == NULL)
    {
        /* Finished with the given circuit. Try to fetch the next valid circuit
         * and try to fetch a circuit level record if possible. Loop till you
         * find a valid circuit with circuit level records or until we exhaust
         * the complete database
         */

        i4RetVal = IsisFltrGetNextCktEntry (pContext, pCktRec);

        if (i4RetVal == ISIS_FAILURE)
        {
            /* Exhausted th complete database. Just get back
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetNextCktLvlEntry ()\n"));
            return (i4RetVal);
        }
        if ((*pCktRec)->pL1CktInfo != NULL)
        {
            *pCktLevelRec = (*pCktRec)->pL1CktInfo;
        }
        else if ((*pCktRec)->pL2CktInfo != NULL)
        {
            *pCktLevelRec = (*pCktRec)->pL2CktInfo;
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextCktLvlEntry ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisFltrGetNextAdjEntry () 
 * Description : This routine fetches the next adjacency record from the
 *               database in order. It first tries to fetch the record from the
 *               given circuit. If not available, it will move on to the next
 *               circuit in the current context. It repeats the procedure till
 *               all circuits in the current contexts are exhausted. when
 *               current context is finished it moves on to the next context
 *               and continues the same extration procedure.
 * Input(s)    : pContext   - Pointer to the System Context containing the given
 *                            Circuit Record (pCktRec) and Adjacency Record
 *                            (pAdjRec)
 *               pCktRec    - Pointer to the Circuit Record containing the given
 *                            Adjacency Record (pAdjRec)
 *               pAdjRec    - Pointer to the Adjacency record from where the
 *                            search for the next record is supposed to begin  
 * Output(s)   : pContext   - Pointer to the System Context updated while
 *                            retrieving the next Adjacency Record
 *               pCktRec    - Pointer to the Circuit Record updated while
 *                            retrieving the next adjacency record
 *               pAdjRec    - Pointer to the Adjacency Record which is 
 *                            next to the given pAdjRec, in order
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an Adjacency Record is successfully retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextAdjEntry (tIsisSysContext ** pContext, tIsisCktEntry ** pCktRec,
                         tIsisAdjEntry ** pAdjRec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextAdjEntry ()\n"));

    *pAdjRec = (*pAdjRec)->pNext;

    while (*pAdjRec == NULL)
    {
        /* Either all adjacencies on the given circuit have been already encoded
         * or there are no adjacencies on the given circuit. In both cases move
         * on to the next circuit
         */

        if (IsisFltrGetNextCktEntry (pContext, pCktRec) == ISIS_FAILURE)
        {
            /* All circuits are exhausted. Get back
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetNextAdjEntry ()\n"));
            return ISIS_FAILURE;
        }

        *pAdjRec = (*pCktRec)->pAdjEntry;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextAdjEntry ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrGetNextIPRAEntry () 
 * Description : This routine fetches the next IPRA record from the database in
 *               order. It first tries to fetch the record from the given 
 *               circuit. If not available, it will move on to the next
 *               circuit in the current context. It repeats the procedure till
 *               all circuits in the current contexts are exhausted. When
 *               current context is finished it moves on to the next context
 *               and continues the same extration procedure.
 * Input(s)    : pContext   - Pointer to the System Context containing the given
 *                            Circuit Record (pCktRec) and IPRA Record
 *                            (pIPRARec)
 *               pCktRec    - Pointer to the Circuit Record containing the given
 *                            IPRA Record (pIPRARec)
 *               pIPRARec   - Pointer to the IPRA record from where the
 *                            search for the next record is supposed to begin  
 * Output(s)   : pContext   - Pointer to the System Context updated while
 *                            retrieving the next IPRA Record
 *               pCktRec    - Pointer to the Circuit Record updated while
 *                            retrieving the next IPRA record
 *               pIPRARec   - Pointer to the IPRA Record which is next to the 
 *                            given pIPRARec, in order
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an IPRA Record is successfully retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextIPRAEntry (tIsisSysContext ** pContext,
                          tIsisIPRAEntry ** pIpRARec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextIPRAEntry ()\n"));

    *pIpRARec = (*pIpRARec)->pNext;

    while (*pIpRARec == NULL)
    {
        /* Either all IPRAs on the given circuit have been already encoded
         * or there are no IPRAs on the given circuit. In both cases move
         * on to the next circuit
         */

        *pContext = (*pContext)->pNext;
        if (*pContext == NULL)
        {
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetNextIPRAEntry ()\n"));
            return ISIS_FAILURE;
        }

        *pIpRARec = (*pContext)->IPRAInfo.pIPRAEntry;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextIPRAEntry ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrGetNextIPIfRec () 
 * Description : This routine fetches the next IP I/f Address record from the 
 *               database in order. It first tries to fetch the record from the
 *               given context. If not available, it will move on to the next
 *               context. It repeats the procedure till all the contexts are 
 *               exhausted.
 * Input(s)    : pContext   - Pointer to the System Context containing the given
 *                            IP I/f Address  Record (pIPIfRec)
 *               pIPIfRec   - Pointer to the IP I/f Address record from where 
 *                            the search for the next record is supposed to 
 *                            begin  
 * Output(s)   : pContext   - Pointer to the System Context updated while
 *                            retrieving the next IP I/f Address Record
 *               pIPRARec   - Pointer to the IP I/f Address Record which is next
 *                            to the given pIPIfRec, in order
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an IP I/f Address Record is successfully 
 *                             retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextIPIfRec (tIsisSysContext ** pContext, tIsisIPIfAddr ** pIPIfRec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextIPIfRec ()\n"));

    *pIPIfRec = (*pIPIfRec)->pNext;

    while (*pIPIfRec == NULL)
    {
        /* Either all IP I/f Addresses in the given Context have been already 
         * encoded or there are no IP I/f Addresses configured at all. In both 
         * cases move on to the next Context
         */

        *pContext = (*pContext)->pNext;
        if (*pContext == NULL)
        {
            /* All contexts exhausted. Get back and proceed with any other 
             * database, if available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetNextIPIfRec ()\n"));

            return ISIS_FAILURE;
        }

        *pIPIfRec = (*pContext)->IPIfTable.pIPIfAddr;
    }
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextIPIfRec ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrGetNextSAEntry () 
 * Description : This routine fetches the next Summary Address record from the 
 *               database in order. It first tries to fetch the record from the
 *               given context. If not available, it will move on to the next
 *               context. It repeats the procedure till all the contexts are 
 *               exhausted.
 * Input(s)    : pContext   - Pointer to the System Context containing the given
 *                            Summary Address Record (pSARec)
 *               pSARec     - Pointer to the Summary Address record from where 
 *                            the search for the next record is supposed to 
 *                            begin  
 * Output(s)   : pContext   - Pointer to the System Context updated while
 *                            retrieving the next Summary Address Record
 *               pSARec     - Pointer to the Summary Address Record which is 
 *                            next to the given pSARec, in order
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if a Summary Address Record is successfully 
 *                             retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextSAEntry (tIsisSysContext ** pContext, tIsisSAEntry ** pSARec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextSAEntry ()\n"));

    *pSARec = (*pSARec)->pNext;

    while (*pSARec == NULL)
    {
        /* Either all Summary Addresses in the given Context have been already 
         * encoded or there are no Summary Addresses configured at all. In both 
         * cases move on to the next Context
         */

        *pContext = (*pContext)->pNext;
        if (*pContext == NULL)
        {
            /* All contexts exhausted. Get back and proceed with any other 
             * database, if available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetNextSAEntry ()\n"));
            return ISIS_FAILURE;
        }

        *pSARec = (*pContext)->SummAddrTable.pSAEntry;
    }
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextSAEntry ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrGetNextMAAEntry () 
 * Description : This routine fetches the next Manual Area Address record from 
 *               the database in order. It first tries to fetch the record from
 *               the given context. If not available, it will move on to the 
 *               next context. It repeats the procedure till all the contexts 
 *               are exhausted.
 * Input(s)    : pContext   - Pointer to the System Context containing the given
 *                            Manual Area Address Record (pMAARec)
 *               pMAARec    - Pointer to the Manual Area Address record from 
 *                            where the search for the next record is supposed 
 *                            to begin  
 * Output(s)   : pContext   - Pointer to the System Context updated while
 *                            retrieving the next Manual Area Address Record
 *               pMAARec    - Pointer to the Manual Area Address Record which is
 *                            next to the given pMAARec, in order
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if a Manual Area Address Record is successfully 
 *                             retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextMAAEntry (tIsisSysContext ** pContext, tIsisMAAEntry ** pMAARec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextMAAEntry ()\n"));

    *pMAARec = (*pMAARec)->pNext;

    while (*pMAARec == NULL)
    {
        /* Either all Manual Area Addresses in the given Context have been 
         * already encoded or there are no Manual Area Addresses configured at 
         * all. In both cases move on to the next Context
         */

        *pContext = (*pContext)->pNext;
        if (*pContext == NULL)
        {
            /* All contexts exhausted. Get back and proceed with any other 
             * database, if available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetNextMAAEntry ()\n"));

            return ISIS_FAILURE;
        }
        *pMAARec = (*pContext)->ManAddrTable.pMAARec;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextMAAEntry ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrGetNextAdjAreaEntry () 
 * Description : This routine fetches the next Adjacency Area Address record 
 *               from the database in order. It first tries to fetch the record
 *               from given Adjacency. If not found, it will move on to the next
 *               Adjacency till all adjacncies on the given circuit are
 *               exhausted. It then moves on to the next circuit and repeats the
 *               same procedure till all circuits in the given context are done
 *               with. The same procedure is repeated over all the contexts till
 *               all are exhausted
 * Input(s)    : pContext   - Pointer to the System Context containing the given
 *                            Adjacency Area Address Record (pAdjAARec)
 *               pCktRec    - Pointer to the Circuit Record containing the
 *                            adjacency whose Area Addresses are to be encoded
 *               pAdjRec    - Pointer to the Adjacency containing the Area
 *                            Addresses to be encoded
 *               pAdjAARec  - Pointer to the Adjacency Area Address record from 
 *                            where the search for the next record is supposed 
 *                            to begin  
 * Output(s)   : pContext   - Pointer to the System Context updated while
 *                            retrieving the next Adjacency Area Address Record
 *               pCktRec    - Pointer to the Circuit Record Updated while
 *                            retrieving the next Adjacency Area Address Record
 *               pAdjRec    - Pointer to the Adjacency Record Updated while 
 *                            retrieving the next Adjacency Area Address Record
 *               pAdjAARec  - Pointer to the Adjacency Area Address Record which
 *                            is next to the given pAdjAARec, in order
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an Adjacency Area Address Record is 
 *                             successfully retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextAdjAreaEntry (tIsisSysContext ** pContext,
                             tIsisCktEntry ** pCktRec, tIsisAdjEntry ** pAdjRec,
                             tIsisAdjAAEntry ** pAdjAARec)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextAdjAreaEntry ()\n"));

    *pAdjAARec = (*pAdjAARec)->pNext;

    while (*pAdjAARec == NULL)
    {
        /* Either all Adjacency Area Addresses in the given Context have been 
         * already encoded or there are no Adjacency Area Addresses on this
         * Adjacency. In both cases move on to the next Adjacency
         */

        if (IsisFltrGetNextAdjEntry (pContext, pCktRec,
                                     pAdjRec) == ISIS_FAILURE)
        {
            /* All adjacencies exhausted. Get back and proceed with any other 
             * database, if available
             */

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrGetNextAdjAreaEntry ()\n"));
            return ISIS_FAILURE;
        }

        *pAdjAARec = (*pAdjRec)->pAdjAreaAddr;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextAdjAreaEntry ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrGetNextLspEntry () 
 * Description : This routine fetches the next LSP record from the database in 
 *               order. It executes the following algorithm:
 *
 *               1. If LSPs are available in the given location at the given
 *                  level, fetch them. 
 *               2. If LSPs are not available in the given location, move to
 *                  next location if possible and repeat step '1', i.e. if the 
 *                  current location is Database then move to TxQ and repeat
 *                  step '1'. If both TxQ and Datbase are finished move to step
 *                  '3'.
 *               3. Move to next Level i.e. if current level is level1 move to
 *                  level2. Mark the location as Database and repeat step '1'.
 *                  If current level is level2, then move to next context, mark
 *                  level as level1, location as Database and repeat step '1'.
 *                  If all contexts are exhausted, then encoding is complete
 * Input(s)    : pContext   - Pointer to the System Context containing the given
 *                            LSP Record (pLspRec)
 *               pu1LspLvl  - Pointer to a location holding the Level of
 *                            database currently being processed. This value may
 *                            change once all LSPs from current level are
 *                            exhausted. Hence we have the pointer argument so
 *                            that next time this routine is invoked, we will
 *                            start with the changed level
 *                            adjacency whose Area Addresses are to be encoded
 *               pLspRec    - Pointer to the LSP records containing the LSPO
 *                            buffer to be encoded
 *               pu1Loc     - Pointer to a location holding the location of the
 *                            LSP records currently being processed. This value
 *                            may change once all LSPs from the current location
 *                            are exhausted. Hence we have a pointer argument so
 *                            that next time this routine is invoked, we will
 *                            start with the changed location
 * Output(s)   : pContext   - Pointer to the System Context updated while
 *                            retrieving the next LSP Record
 *               pu1LspLvl  - LSP Level, Updated while retrieving the next LSP 
 *                            Record
 *               pLspRec    - Pointer to the LSP Record which is next to the
 *                            given LSP Record, in order
 *                            retrieving the next LSP buffer
 *               pu1Loc     - Location of the LSP record, Updated while
 *                            retrieving the next LSP Record
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if an LSP Record is successfully retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrGetNextLspEntry (tIsisSysContext ** pContext, UINT1 *pu1LspLvl,
                         tIsisLSPEntry ** pLspRec, UINT1 *pu1Loc)
{
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrGetNextLspEntry ()\n"));

    if (*pLspRec != NULL)
    {
        /* We still can try to fetch LSPs from the current location and
         * current level
         */

        if (*pu1Loc == ISIS_LOC_TX)
        {
            /* Currently processing LSPs from TxQ. Continue to fetch from TxQ
             * if available
             */

            if ((*pLspRec)->pNext != NULL)
            {
                *pLspRec = ((tIsisLSPTxEntry *) ((*pLspRec)->pNext))->pLSPRec;
            }
            else
            {
                *pLspRec = (*pLspRec)->pNext;
            }
        }
        else
        {
            /* Currently processing LSPs from DB. Continue to fetch from DB
             * if availablre
             */

            *pLspRec = (*pLspRec)->pNext;
        }
    }

    while (*pLspRec == NULL)
    {
        if ((*pu1Loc == ISIS_LOC_NONE)
            && (((*pContext)->SysL1Info.LspDB.pHashBkts) != NULL))
        {
            /* At level2 we have valid LSPs in Database. First try
             * the DB before moving to TxQ
             */

            *pu1Loc = ISIS_LOC_DB;
            *pLspRec = ((tIsisLSPEntry *)
                        (*pContext)->SysL1Info.LspDB.pHashBkts->pFirstElem);
        }

        /* Exhausted all LSPs at the current location and level
         */

        else if (*pu1Loc == ISIS_LOC_DB)
        {
            /* Currently processing LSPs from DB and since we have exhausted all
             * LSPs in DB, we can move to TxQ
             */

            *pu1Loc = ISIS_LOC_TX;

            /* Since we have changed the location, we have to update the
             * 'pLspRec' to point to a valid LSP from the changed location,
             * but the level will remain the same till all LSPs from both
             * the locations are exhausted
             */

            if (*pu1LspLvl == ISIS_LEVEL1)
            {
                if ((*pContext)->SysL1Info.LspTxQ.pHashBkts != NULL)
                {
                    *pLspRec = ((tIsisLSPTxEntry *) (*pContext)->
                                SysL1Info.LspTxQ.pHashBkts->pFirstElem)->
                        pLSPRec;
                }
            }
            else
            {
                if ((*pContext)->SysL2Info.LspTxQ.pHashBkts != NULL)
                {
                    *pLspRec = ((tIsisLSPTxEntry *) (*pContext)->
                                SysL2Info.LspTxQ.pHashBkts->pFirstElem)->
                        pLSPRec;
                }
            }
        }
        else
        {
            /* Try to fetch LSPs from next level, since we reach this point
             * only after exhausting all LSPs from both the DB and TxQ at the
             * current level
             */

            if (*pu1LspLvl == ISIS_LEVEL1)
            {
                /* We have exhausted all LSPs at level1. Try to fetch from
                 * Level2 if possible
                 */

                *pu1LspLvl = ISIS_LEVEL2;

                if ((*pContext)->SysActuals.u1SysType == ISIS_LEVEL12)
                {
                    if (((*pContext)->SysL2Info.LspDB.pHashBkts) != NULL)
                    {
                        /* At level2 we have valid LSPs in Database. First try
                         * the DB before moving to TxQ
                         */

                        *pu1Loc = ISIS_LOC_DB;
                        *pLspRec = ((tIsisLSPEntry *)
                                    (*pContext)->SysL2Info.LspDB.pHashBkts->
                                    pFirstElem);
                    }
                    else if (((*pContext)->SysL2Info.LspTxQ.pHashBkts) != NULL)
                    {
                        /* No LSPs in DB at level2. Try fetching from TxQ.
                         */

                        *pu1Loc = ISIS_LOC_TX;
                        *pLspRec = ((tIsisLSPTxEntry *)
                                    (*pContext)->SysL2Info.LspTxQ.pHashBkts->
                                    pFirstElem)->pLSPRec;
                    }
                }
            }
            else
            {
                /* Finished with both the levels and locations in the current
                 * context. Try moving to next available context
                 */

                *pContext = (*pContext)->pNext;

                if (*pContext == NULL)
                {
                    /* Exhausted all LSPs from the LSP Database. Get back so
                     * that the calling routine can try with other available
                     * databases
                     */

                    FTP_EE ((ISIS_LGST,
                             "FLT <X> : Exiting IsisFltrGetNextLspEntry ()\n"));
                    return ISIS_FAILURE;
                }

                /* Now that we have moved to a new context, we can again
                 * start with the location "DB" and level "Level1" (if possible)
                 */

                *pu1Loc = ISIS_LOC_DB;

                if ((*pContext)->SysActuals.u1SysType != ISIS_LEVEL2)
                {
                    /* Local system is a L12/L1 system, hence try to start with
                     * the level1 LSPs if available
                     */

                    if (((*pContext)->SysL1Info.LspDB.pHashBkts) != NULL)
                    {
                        *pLspRec = (tIsisLSPEntry *)
                            (*pContext)->SysL1Info.LspDB.pHashBkts->pFirstElem;
                        *pu1LspLvl = ISIS_LEVEL1;
                    }
                }
                else
                {
                    /* We will reach this point only for L2 systems. Try to get
                     * a valid LSP record from the database
                     */

                    if (((*pContext)->SysL2Info.LspDB.pHashBkts) != NULL)
                    {
                        *pLspRec = (tIsisLSPEntry *)
                            (*pContext)->SysL2Info.LspDB.pHashBkts->pFirstElem;
                        *pu1LspLvl = ISIS_LEVEL2;
                    }
                }
            }
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrGetNextLspEntry ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 *
 * Function    : IsisFltrModifySelfLSPTLV ()
 * Description : This routine modifies the Sequence Number in Self LSP Info 
 *               This routine is invoked in the Standby Node alone, on reception
 *               of Lock Step Update of Self LSPs with command as 
 *               ISIS_CMD_MODIFY, to update the Self LSP Information.
 * Inputs      : pContext  - Pointer to System Context 
 *               pu1LspID  - Pointer to LSP ID of the LSP to be removed
 *               u1Level   - Level of LSP
 *               u4SeqNum  - Sequence Number to be updated
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisFltrModifySelfLSPTLV (tIsisSysContext * pContext, UINT1 *pu1LspID,
                          UINT1 u1Level, UINT4 u4SeqNum)
{
    UINT1               u1LspNum = 0;
    UINT1               u1SNId = 0;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrModifySelfLSPTLV ()\n"));

    u1LspNum = *(pu1LspID + ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
    u1SNId = *(pu1LspID + ISIS_SYS_ID_LEN);

    if (u1SNId != 0)
    {
	    pSNLSP = (u1Level == ISIS_LEVEL1) ? (pContext->SelfLSP.pL1SNLSP) :
		    (pContext->SelfLSP.pL2SNLSP);
	    if(pSNLSP == NULL)
	    {
		    return;
	    }
	    while (pSNLSP != NULL)
	    {
		    if (((pSNLSP->pZeroLSP != NULL)
					    && (u1SNId == pSNLSP->pZeroLSP->u1SNId))
				    || ((pSNLSP->pNonZeroLSP != NULL)
					    && (u1SNId == pSNLSP->pNonZeroLSP->u1SNId)))
		    {
			    pZeroLSP = pSNLSP->pZeroLSP;
			    pNonZeroLSP = pSNLSP->pNonZeroLSP;
			    break;
		    }
		    pSNLSP = pSNLSP->pNext;
	    }
	    if (pSNLSP == NULL)
	    {
		    /* still the Psuedo node LSP is not consctrcuted 
		     * so no point to update that now
		     */
		    return;
	    }

    }
    else
    {
	    pNSNLSP = (u1Level == ISIS_LEVEL1) ? (pContext->SelfLSP.pL1NSNLSP) :
		    (pContext->SelfLSP.pL2NSNLSP);
	    if(pNSNLSP == NULL)
	    {
		    return;
	    }
	    pZeroLSP = ((pNSNLSP)->pZeroLSP);
	    pNonZeroLSP = (pNSNLSP)->pNonZeroLSP;
    }

    if ((pZeroLSP) != NULL)
    {
        if ((pZeroLSP)->u1LSPNum == u1LspNum)
        {
            (pZeroLSP)->u4SeqNum = u4SeqNum;
        }
    }
    while ((pNonZeroLSP) != NULL)
    {
        if ((pNonZeroLSP)->u1LSPNum == u1LspNum)
        {
            (pNonZeroLSP)->u4SeqNum = u4SeqNum;
            break;
        }
        pNonZeroLSP = pNonZeroLSP->pNext;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrModifySelfLSPTLV ()\n"));
}

#ifdef RM_WANTED
/*****************************************************************************/
/* Function Name      : IsisGetShowCmdOutputAndCalcChkSum                    */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISIS_SUCESS/ISIS_FAILURE                             */
/*****************************************************************************/
INT4
IsisGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#ifdef CLI_WANTED

    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (IsisCliGetShowCmdOutputToFile ((UINT1 *) ISIS_AUDIT_FILE_ACTIVE) !=
            ISIS_SUCCESS)
        {
            FTP_PT ((ISIS_LGST, "GetShRunFile Failed\n"));
            return ISIS_FAILURE;
        }
        if (IsisCliCalcSwAudCheckSum
            ((UINT1 *) ISIS_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != ISIS_SUCCESS)
        {
            FTP_PT ((ISIS_LGST, "CalcSwAudChkSum Failed\n"));
            return ISIS_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (IsisCliGetShowCmdOutputToFile ((UINT1 *) ISIS_AUDIT_FILE_STDBY) !=
            ISIS_SUCCESS)
        {
            FTP_PT ((ISIS_LGST, "GetShRunFile Failed\n"));
            return ISIS_FAILURE;
        }
        if (IsisCliCalcSwAudCheckSum
            ((UINT1 *) ISIS_AUDIT_FILE_STDBY, pu2SwAudChkSum) != ISIS_SUCCESS)
        {
            FTP_PT ((ISIS_LGST, "CalcSwAudChkSum Failed\n"));
            return ISIS_FAILURE;
        }
    }
    else
    {
        FTP_PT ((ISIS_LGST, "Node is either Active or Standby\n"));
        return ISIS_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return ISIS_SUCCESS;
}
#endif

#endif
