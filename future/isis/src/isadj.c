/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isadj.c,v 1.35 2017/09/11 13:44:07 siva Exp $
 *
 *   Description: This file contains the main Routines associated with
 *                Adjacency Module. 
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE VOID        IsisAdjProcISH (tIsisCktEntry *, UINT1 *);

PRIVATE VOID        IsisAdjProcP2PIIH (tIsisCktEntry *, UINT1 *);

PRIVATE VOID        IsisAdjProcLANHello (tIsisCktEntry *, UINT1 *, UINT1 *);

PRIVATE VOID        IsisAdjProcEvt (tIsisMsg *);

PRIVATE VOID        IsisAdjProcStatInd (tIsisMsg *);

PRIVATE VOID        IsisAdjActivateCkt (tIsisSysContext *, tIsisCktEntry *,
                                        UINT1);

PRIVATE VOID        IsisAdjTxISH (tIsisSysContext *, tIsisCktEntry *);

PRIVATE VOID        IsisAdjProcCktStat (tIsisCktEntry *, UINT1);

PRIVATE VOID        IsisInsCktInHashTbl (tIsisCktEntry **, UINT1);

PRIVATE VOID        IsisDelCktInHashTbl (tIsisCktEntry **, UINT1);

/******************************************************************************
 * Function      : IsisAdjProcCtrlPkt ()
 * Description   : This is the entry point to the Adjacency module. It decodes
 *                 the received message and schedules the appropriate routines
 *                 for further processing
 * Input(s)      : pMsg - Pointer to the received Message
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjProcCtrlPkt (tIsisMsg * pIsisMsg)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1              *pu1Hello = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT2               u2LLIfIndex = 0;
#ifdef ISIS_FT_ENABLED
    UINT1               u1PrevHndShake = ISIS_P2P_TWO_WAY;
#endif

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcCtrlPkt ()\n"));

    switch (pIsisMsg->u1MsgType)
    {
        case ISIS_P2P_IIH_PDU:

            pu1Hello = ISIS_MEM_ALLOC (ISIS_BUF_HLPD, pIsisMsg->u4Length);

            if (pu1Hello == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : P2P Hello\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisFreeDLLBuf (pIsisMsg);
                break;
            }

            IsisCopyFromDLLBuf (pIsisMsg, pu1Hello, pIsisMsg->u4Length);
            IsisFreeDLLBuf (pIsisMsg);

            /* Context holds the Circuit Record, Retrieve that
             */

            u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
            i4RetVal = IsisAdjGetCktRecWithLLHandle (u2LLIfIndex, &pCktRec);
            if (i4RetVal != ISIS_SUCCESS)
            {
                ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1Hello);
                DLP_PT ((ISIS_LGST,
                         "ADJ <W> : Data Received on Erroneous Interface [ %u ]\n",
                         u2LLIfIndex));
                break;
            }

            /* Check for External Domain and the ID length mismatch conditions
             */
#ifdef ISIS_FT_ENABLED
            u1PrevHndShake = pCktRec->u1P2PDynHshkMachanism;
#endif
            i4RetVal = IsisUtlValidatePDU (pCktRec->pContext, pCktRec,
                                           pu1Hello);
            if (i4RetVal == ISIS_SUCCESS)
            {
                /* Point-To-Point circuits will hold all Level specific
                 * information w.r.t Adjacencies, in Level1 Circuit records,
                 * since the specification does not differentiate between 
                 * Level-1 and Level-2 Hello PDUs as such. But all other Level
                 * Specific information will be maintined in the respective
                 * Level records
                 */

                pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
                ISIS_INCR_RCVD_HELLO (pCktLevel);
                IsisAdjProcP2PIIH (pCktRec, pu1Hello);

#ifdef ISIS_FT_ENABLED
                /*This is to update the current handshake mechanism to standby */
                if (u1PrevHndShake != pCktRec->u1P2PDynHshkMachanism)
                {
                    ISIS_FLTR_CKT_LSU (pCktRec->pContext, ISIS_CMD_ADD, pCktRec,
                                       pCktRec->u1CktExistState);
                }
#endif
            }
            else
            {
                ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
                ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1Hello);
            }
            break;

        case ISIS_ISH_PDU:

            pu1Hello = ISIS_MEM_ALLOC (ISIS_BUF_HLPD, pIsisMsg->u4Length);

            if (pu1Hello == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : ISH PDU\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisFreeDLLBuf (pIsisMsg);
                break;
            }

            IsisCopyFromDLLBuf (pIsisMsg, pu1Hello, pIsisMsg->u4Length);
            IsisFreeDLLBuf (pIsisMsg);

            /* Context holds the Circuit Record, Retrieve that
             */
            u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
            i4RetVal = IsisAdjGetCktRecWithLLHandle (u2LLIfIndex, &pCktRec);
            if (i4RetVal != ISIS_SUCCESS)
            {
                ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1Hello);
                IsisFreeDLLBuf (pIsisMsg);
                DLP_PT ((ISIS_LGST,
                         "ADJ <W> : Data Received on Erroneous Interface [ %u ]\n",
                         u2LLIfIndex));
                break;
            }
            if (pCktRec->bCktPassiveCkt == ISIS_TRUE)
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <X> : Interface is Passive. Reception not Allowed - Circuit [ %u ]\n",
                         pCktRec->u4CktIdx));
                ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1Hello);
                IsisFreeDLLBuf (pIsisMsg);
                break;
            }

            ISIS_INCR_RCVD_ISH (pCktRec);
            IsisAdjProcISH (pCktRec, pu1Hello);
            break;

        case ISIS_L1LAN_IIH_PDU:
        case ISIS_L2LAN_IIH_PDU:

            pu1Hello = ISIS_MEM_ALLOC (ISIS_BUF_HLPD, pIsisMsg->u4Length);

            if (pu1Hello == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LAN IIH PDU\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisFreeDLLBuf (pIsisMsg);
                break;
            }

            IsisCopyFromDLLBuf (pIsisMsg, pu1Hello, pIsisMsg->u4Length);
            IsisFreeDLLBuf (pIsisMsg);

            u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
            i4RetVal = IsisAdjGetCktRecWithLLHandle (u2LLIfIndex, &pCktRec);
            if (i4RetVal != ISIS_SUCCESS)
            {
                ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1Hello);
                DLP_PT ((ISIS_LGST,
                         "ADJ <W> : Data Received on Erroneous Interface [ %u ]\n",
                         u2LLIfIndex));
                break;
            }
            i4RetVal = IsisUtlValidatePDU (pCktRec->pContext,
                                           pCktRec, pu1Hello);
            if (i4RetVal == ISIS_SUCCESS)
            {
                pCktLevel = (pIsisMsg->u1MsgType == ISIS_L1LAN_IIH_PDU)
                    ? pCktRec->pL1CktInfo : pCktRec->pL2CktInfo;
                if (pCktLevel != NULL)
                {
                    ISIS_INCR_RCVD_HELLO (pCktLevel);
                    IsisAdjProcLANHello (pCktRec, pu1Hello,
                                         pIsisMsg->au1PeerSNPAAddr);
                }
                else
                {
                    ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1Hello);
                }
            }
            else
            {
                ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
                ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1Hello);
            }
            break;

        case ISIS_MSG_EVENT:

            IsisAdjProcEvt (pIsisMsg);
            break;

        case ISIS_MSG_IF_STAT_IND:

            IsisAdjProcStatInd (pIsisMsg);
            break;

        case ISIS_MSG_SEC_IF_STAT_IND:

            IsisAdjProcStatInd (pIsisMsg);
            break;

        default:

            ADP_PT ((ISIS_LGST,
                     "ADJ <E> : Invalid PDU Received - Discarded\n"));
            IsisFreeDLLBuf (pIsisMsg);
            break;
    }
    pIsisMsg->pu1Msg = NULL;
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcCtrlPkt ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjProcISH ()
 * Description   : This function processes ISH PDU as specified in Sec. 8.2.2 of
 *                 ISO 10589.
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pu1PDU  - Pointer to the received Hello Pdu
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcISH (tIsisCktEntry * pCktRec, UINT1 *pu1PDU)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               au1SysId[ISIS_SYS_ID_LEN];
    UINT1               u1NetLen = 0;
    UINT2               u2RcvdCS = 0;
    tBool               bFirstIIH = ISIS_TRUE;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcISH ()\n"));

    MEMSET (au1SysId, 0, ISIS_SYS_ID_LEN);
    pAdjRec = pCktRec->pAdjEntry;
    pContext = pCktRec->pContext;

    ISIS_EXTRACT_NET_LEN (pu1PDU, u1NetLen);
    ISIS_EXTRACT_SYSID_FROM_NET (pu1PDU, au1SysId, u1NetLen);
    if (MEMCMP (au1SysId, pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN) == 0)
    {
        /* Ignore the Hello PDU as it has same System-ID of ours */
        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Self Hello Received - Ignored - Circuit [ %u ]\n",
                 pCktRec->u4CktIdx));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcISH ()\n"));
        ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
        return;
    }

    if (pCktRec->bCktExtDomain == ISIS_TRUE)
    {
        /* We can silently ignore PDUs received from External Domains
         */

        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Received ISH PDU From External Domain - Ignored - Circuit [ %u ]\n",
                 pCktRec->u4CktIdx));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcCtrlPkt ()\n"));
        ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
        return;
    }

    u2RcvdCS = IsisUtlCalcChkSum (pu1PDU, (UINT2) *(pu1PDU + 1), NULL);

    /* Note: The same function is used for generating and validating checksums.
     * According to Checksum generation algorithm, if the Checksum calculated is
     * ZERO, then we have to fill "0xFFFF" in the checksum field. Hence the
     * returned checksum will be "0xFFFF" instead of ZERO during validation also
     */

    if (u2RcvdCS != ISIS_CORRECT_CS)
    {
        /* Checksum problem - discard the PDU 
         */

        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Checksum Error in PDU [ %u ] - Discarding - Circuit [ %u ]\n",
                 u2RcvdCS, pCktRec->u4CktIdx));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcCtrlPkt ()\n"));
        ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
        return;
    }

    /* We always update Level-1 statistics (adjacency related alone) for P2P
     * adjacencies
     */

    pCktLevel = ((pCktRec->u1CktLevel == ISIS_LEVEL1)
                 || (pCktRec->u1CktLevel == ISIS_LEVEL12))
        ? pCktRec->pL1CktInfo : pCktRec->pL2CktInfo;

    /* Refer to 8.2.2 of ISO 10589 Spec
     */

    if (pAdjRec == NULL)
    {
        /* A new adjacency is coming UP, proceed and add a new adjacency in the
         * state Initialising
         */

        pAdjRec = (tIsisAdjEntry *) ISIS_MEM_ALLOC (ISIS_BUF_ADJN,
                                                    sizeof (tIsisAdjEntry));
        if (pAdjRec == NULL)
        {
            /* No Memory for holding the new adjacency
             */
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : ISH PDU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pCktRec->pContext, ISIS_BUF_ADJN);
        }
        else
        {
            /* Build an adjacency record with state Initialising, and usage
             * UNKNOWN
             */

            IsisAdjBldAdjRecFromISH (pCktRec, pu1PDU, ISIS_ADJ_INIT,
                                     ISIS_UNKNOWN, pAdjRec);

            /* Stop the ISH Timer
             */

            IsisTmrStopECTimer (pCktRec->pContext, ISIS_ECT_P2P_ISH,
                                pCktRec->u4CktIdx, pCktLevel->u1HelloTmrIdx);

            /* The last argument specifies that a Direction Table entry must be
             * created for the new adjacency
             */

            IsisAdjAddP2PThreewayAdj (pCktRec->pContext, pCktRec, pAdjRec);

            /* Now Transmitting IIH on the Circuit on which ISH was received
             */

            IsisAdjTxHello (pCktRec->pContext, pCktRec, pCktRec->u1CktLevel,
                            bFirstIIH);
            ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                           acNbrSysId);
            ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);

            ADP_PI ((ISIS_LGST,
                     "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                     " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                     ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                     pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                     au1IPv4Addr,
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pCktRec->au1IPV6Addr))));
        }
    }
    else
    {
        /* Refer Sec. 8.2.2 a) of ISO 10589 
         */

        /* Adjacency already exist
         */
        ISIS_EXTRACT_NET_LEN (pu1PDU, u1NetLen);
        ISIS_EXTRACT_SYSID_FROM_NET (pu1PDU, au1SysId, u1NetLen);
        i4RetVal = MEMCMP (au1SysId, pAdjRec->au1AdjNbrSysID, ISIS_SYS_ID_LEN);

        ISIS_DBG_PRINT_ID (au1SysId, (UINT1) ISIS_SYS_ID_LEN,
                           "ADJ <T> : Received ISH For Existing Adjacency From\t",
                           ISIS_OCTET_STRING);

        /* changing this check as per CISCO behaviour */
        if ((ISIS_GET_ADJ_STATE (pAdjRec) == ISIS_ADJ_DOWN) && (i4RetVal != 0))
        {
            ISIS_INCR_ADJ_CHG_STAT (pCktRec);

            /* Refer Sec. 8.2.2. a) of ISO 10589
             *
             * Remove the existing adjacency and create a new one with state
             * 'initialising' and usage 'unknown'
             */
            IsisAdjEnqueueAdjChgEvt (pCktRec, NULL, pAdjRec, ISIS_ADJ_DOWN);
            /*Remove does not free the AdjRec Buffer */
            IsisAdjRemAdj (pCktRec, pAdjRec);

            IsisAdjBldAdjRecFromISH (pCktRec, pu1PDU, ISIS_ADJ_INIT,
                                     ISIS_UNKNOWN, pAdjRec);
            IsisAdjAddP2PThreewayAdj (pCktRec->pContext, pCktRec, pAdjRec);

            IsisAdjTxHello (pCktRec->pContext, pCktRec, pCktRec->u1CktLevel,
                            bFirstIIH);
        }
        else if (pAdjRec->u1AdjState == ISIS_ADJ_INIT)
        {
            /* 8.2.2 b) of ISO 10589
             */
            if (pAdjRec->u1AdjNbrSysType == ISIS_IS)
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Received ISH PDU From An IS - Ignored - Circuit [ %u ]\n",
                         pCktRec->u4CktIdx));
            }
            else if (pAdjRec->u1AdjNbrSysType != ISIS_IS)
            {
                /* 8.2.2 c) of ISO 10589 
                 */

                IsisAdjTxHello (pCktRec->pContext, pCktRec, pCktRec->u1CktLevel,
                                bFirstIIH);
                /* Starting the Hello Timer after sending Hello
                 */

                IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_P2P_HELLO,
                                       pCktRec->u4CktIdx,
                                       pCktLevel->HelloTimeInt,
                                       &pCktLevel->u1HelloTmrIdx);
            }
        }
    }

    if (pAdjRec != NULL)
    {
        /* Setting Adjacency Usage as ISIS_IS, Refer Sec. 8.2.2 d)
         */

        pAdjRec->u1AdjNbrSysType = ISIS_IS;
    }
    ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcISH ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjPerformP2PTwoWayHndshk ()
 * Description   : This function processes Point-to-Point IIH PDU. It updates the
 *                 Adjacency Database as per Tables 5, 6 and 7 in Sec. 8.2.4
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pu1PDU  - Pointer to the received Hello Pdu
 *                 i4AAChkFlag - To know if the peer have a matching Area Address 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisAdjPerformP2PTwoWayHndshk (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                               INT4 i4AAChkFlag)
{
    UINT1               u1CktType = 0;
    tIsisSysContext    *pContext = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjPerformP2PTwoWayHndshk()\n"));

    pContext = pCktRec->pContext;
    u1CktType = ISIS_EXTRACT_HELLO_CKT_TYPE (pu1PDU);

    switch (pContext->SysActuals.u1SysType)
    {
        case ISIS_LEVEL1:

            if (i4AAChkFlag == ISIS_SUCCESS)
            {
                /* Refer Table - 5 Level1 State table for matching
                 * areas  (ISO 10589)
                 */

                IsisAdjValP2PHelloSelfL1 (pCktRec, pu1PDU, u1CktType);
            }
            else if (pCktRec->pAdjEntry != NULL)
            {
                /* If Area Addresses did not match, then the PDU and hence
                 * the adjacency must have been L2. but the local system is
                 * L1 and hence the new PDU indicates a change in the type
                 * of adjacency. We have to delete the existing adjacency
                 */

                /* Refer 8.2.4.2 b) 1, 2
                 */
                IsisP2PDelAdj (pCktRec);
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Attempt To Add A Wrong System\n"));
            }
            else
            {
                /* Incompatible Peer and adjacency with the Peer does not
                 * exist, Update the statistics and Drop the PDU
                 */

                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                IsisAdjEnqueueWrongSysEvt (pContext, pu1PDU);
            }
            break;

        case ISIS_LEVEL12:

            if (i4AAChkFlag == ISIS_SUCCESS)
            {
                /* Refer Table - 6 Level2 State table for matching
                 * areas  (ISO 10589)
                 */

                IsisAdjValP2PHelloSelfL12 (pCktRec, pu1PDU, u1CktType);
            }
            else
            {
                /* Refer Table - 8 Level2 only State table for non matching
                 * areas  (ISO 10589)
                 */

                IsisAdjValAAMismatchHello (pCktRec, pu1PDU, u1CktType);
            }
            break;

        case ISIS_LEVEL2:

            /* Local System is a Level2 system and hence we need not worry
             * about the Area Address Match check. Whether the Area
             * Addresses match or not, the PDU can be processed as if no
             * area addresses matched. The only difference that is mentioned
             * in the (Spec. Table 7. and Table 8.) is the Event to be
             * generated when an adjacency is brought down. For matching
             * areas case the Down event is Wrong System and non-matching
             * case it is Area Mismatch Event. Our implementation generates
             * only Wrong System Event for both the cases since that is what
             * is inferred from the PDU
             */

            IsisAdjValAAMismatchHello (pCktRec, pu1PDU, u1CktType);
            break;

        default:

            ISIS_INCR_SYS_DROPPED_PDUS (pContext);
            WARNING ((ISIS_LGST, "ADJ <W> : Unknown Local System Type\n"));
            break;
    }
    ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjPerformP2PTwoWayHndshk()\n"));
}

/******************************************************************************
 * Function      : IsisAdjProcP2PIIH ()
 * Description   : This function processes Point-to-Point IIH PDU. It first 
 *                    Validates the Area Address and then processes the IIH based on
 *                    peer's mode of Handshake.
 * Input(s)      : pCktRec  - Pointer to Circuit Record
 *                 pu1PDU   - Pointer to Hello Pdu
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcP2PIIH (tIsisCktEntry * pCktRec, UINT1 *pu1PDU)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4AAChkFlag = ISIS_FAILURE;

    i4RetVal = IsisAdjP2PChkandCmnProcIIH (pCktRec, pu1PDU, &i4AAChkFlag);

    if (i4RetVal == ISIS_FAILURE)
    {
        ADP_PT ((ISIS_LGST, "ADJ <T> : Processing of P2P IIH PDU Failed\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return;
    }

    if (pCktRec->u1P2PDynHshkMachanism == ISIS_P2P_TWO_WAY)
    {
        IsisAdjPerformP2PTwoWayHndshk (pCktRec, pu1PDU, i4AAChkFlag);
    }
    else if (pCktRec->u1P2PDynHshkMachanism == ISIS_P2P_THREE_WAY)
    {
        IsisAdjPerformP2PThreeWayHndshk (pCktRec, pu1PDU, i4AAChkFlag);
    }

    return;
}

/******************************************************************************
 * Function      : IsisAdjP2PChkandCmnProcIIH ()
 * Description   : This function does the common processing of Point-to-Point 
 *                    IIH PDU. It validates the IIH, finds area address match,
 *                    and processes Restart TLV which is common for both 2-way and 
 *                    3-way handshakes.
 * Input(s)      : pCktRec  - Pointer to Circuit Record
 *                 pu1PDU   - Pointer to Hello Pdu
 *                 pi4AAChkFlag - Pointer to the Flag denoting Area Address 
 *                                   Match/Mis-Match
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS (or) ISIS_FAILURE
 ******************************************************************************/

INT4
IsisAdjP2PChkandCmnProcIIH (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                            INT4 *pi4AAChkFlag)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4AAChkFlag = ISIS_FALSE;
    tIsisSysContext    *pContext = NULL;
    UINT1              *pu1SNPA = NULL;
    UINT1              *pSysId = NULL;
    UINT1              *pu1AdjSysID = NULL;
    UINT1               u1PduType = 0;
    tIsisComHdr        *pComHdr = NULL;

    /*This should be a minimal form of two way handshake */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjP2PChkandCmnProcIIH()\n"));

    pContext = pCktRec->pContext;

    pu1AdjSysID = ISIS_EXTRACT_HELLO_SYS_ID (pu1PDU);

    if (MEMCMP (pu1AdjSysID, pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN) ==
        0)
    {
        ISIS_INCR_SYS_DROPPED_PDUS (pContext);
        ISIS_INCR_REJ_ADJ_STAT (pCktRec);
        ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Self Hello Received - Ignored - Circuit [%u], Level [%u]\n",
                 pCktRec->u4CktIdx, pCktRec->u1CktLevel));
        ADP_EE ((ISIS_LGST,
                 "ADJ <X> : Exiting IsisAdjP2PChkandCmnProcIIH ()\n"));
        return ISIS_FAILURE;
    }

    /* Authenticate the Received PDU
     */

    i4RetVal = IsisUtlAuthenticatePDU (pContext, pCktRec, pu1PDU);

    if (i4RetVal != ISIS_SUCCESS)
    {
        u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);
        WARNING ((ISIS_LGST,
                  "ADJ <T> : Authentication Failed For The Received PDU - Discarded - Circuit [%u], Level [%u], PDU [%s]\n",
                  pCktRec->u4CktIdx, pCktRec->u1CktLevel,
                  ISIS_GET_PDU_TYPE_STR_FOR_TRACE (u1PduType)));
        ISIS_INCR_SYS_DROPPED_PDUS (pContext);
        ISIS_INCR_REJ_ADJ_STAT (pCktRec);
        ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
        ADP_EE ((ISIS_LGST,
                 "ADJ <X> : Exiting IsisAdjP2PChkandCmnProcIIH ()\n"));
        return ISIS_FAILURE;
    }
    else
    {
        /* The ISIS specification does not include a separate L1 and L2 PDUs for
         * the P2P case. If Area Address match check fails, then the PDU might
         * have been sent by an L2 system, i.e. Adjacency usage is L2. Otherwise
         * the Adjacency usage can either be L1 or L12 based on the Peer and
         * Self System Type.
         */
        if (pCktRec->pAdjEntry != NULL)
        {
            pSysId = ISIS_EXTRACT_HELLO_SYS_ID (pu1PDU);
            if (MEMCMP
                (pSysId, pCktRec->pAdjEntry->au1AdjNbrSysID,
                 ISIS_SYS_ID_LEN) != 0)
            {
                IsisAdjEnqueueWrongSysEvt (pCktRec->pContext, pu1PDU);
                ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
                return ISIS_FAILURE;
            }
        }

        if (pCktRec->u1CktLevel != ISIS_LEVEL1)
        {
            /* Area Address Mismatch Event Need not be posted if the
             * circuit type is L2 or L12
             */

            i4AAChkFlag = IsisAdjAreaAddrMatchCheck (pContext, pu1PDU,
                                                     ISIS_FALSE);
        }
        else
        {
            i4AAChkFlag = IsisAdjAreaAddrMatchCheck (pContext, pu1PDU,
                                                     ISIS_TRUE);
        }

        if (i4AAChkFlag == ISIS_SUCCESS)
        {
            /* One or More of Area Addresses matched. The PDU is treated as a 
             * L1 PDU
             */

            /* Since Area Addresses matched, both the systems should agree upon
             * the Maximum Area Addresses also.
             * Check for Max Area Addresse only if
             *  -- Configured to do so
             *  -- Maximum Area Addresses configured is a value other than '3'
             * Refer Sec. 8.2.4.2 a) 1) of ISO 10589 Specification
             */

            if ((pContext->SysActuals.u1SysMaxAA != 3)
                && (pContext->SysActuals.bSysMaxAACheck == ISIS_TRUE))
            {
                /* Local IS Configured to check for Max Area Addresses mismatch
                 */

                i4RetVal = IsisUtlMaxAreaAddrCheck (pContext, pCktRec, pu1PDU);
                if (i4RetVal == ISIS_FAILURE)
                {
                    u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);
                    pComHdr = (tIsisComHdr *) pu1PDU;
                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Maximum Area Address Check Failed - Circuit [%u], Level [%u], PDU [%s], MaxAA [%u]\n",
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel,
                             ISIS_GET_PDU_TYPE_STR_FOR_TRACE (u1PduType),
                             pComHdr->u1MaxAreaAddr));
                    ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                    ISIS_INCR_REJ_ADJ_STAT (pCktRec);
                    ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    ADP_EE ((ISIS_LGST,
                             "ADJ <X> : Exiting IsisAdjP2PChkandCmnProcIIH()\n"));
                    return ISIS_FAILURE;
                }
            }
        }
    }

    IsisAdjProcRestartTlv (pContext, pCktRec, pu1SNPA, pu1PDU);

    *pi4AAChkFlag = i4AAChkFlag;
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function      : IsisAdjP2PTwoWayDecision()
 * Description   : This function processes Point-to-Point IIH PDU and makes the 
 *                    2-way decision based on Tables 5, 6 and 7 in Sec. 8.2.4 which 
 *                    is inturn used to perform 3-way handshake processing. 
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pu1PDU  - Pointer to the received Hello Pdu
 *                 i4AAChkFlag - To know if the peer have a matching Area Address 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisAdjP2PTwoWayDecision (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                          INT4 i4AAChkFlag)
{
    UINT1               u1CktType = 0;
    tIsisSysContext    *pContext = NULL;

    /*This should be a minimal form of two way handshake */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjP2PTwoWayDecision()\n"));

    pContext = pCktRec->pContext;
    u1CktType = ISIS_EXTRACT_HELLO_CKT_TYPE (pu1PDU);

    pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_NONE;

    switch (pContext->SysActuals.u1SysType)
    {
        case ISIS_LEVEL1:

            if (i4AAChkFlag == ISIS_SUCCESS)
            {
                /* Refer Table - 5 Level1 State table for matching
                 * areas  (ISO 10589)
                 */

                IsisAdjValP2PThreewayHelloSelfL1MatAA (pCktRec, pu1PDU,
                                                       u1CktType);
            }
            else if (pCktRec->pAdjEntry != NULL)
            {
                /* If Area Addresses did not match, then the PDU and hence
                 * the adjacency must have been L2. but the local system is
                 * L1 and hence the new PDU indicates a change in the type
                 * of adjacency. We have to delete the existing adjacency
                 */

                /* Refer 8.2.4.2 b) 1, 2
                 */
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : P2P Hello - Ignored - Circuit [%u], Level [%u] - Area Address Mismatch\n",
                         pCktRec->u4CktIdx, pCktRec->u1CktLevel));

                IsisP2PDelAdj (pCktRec);
            }
            else
            {
                /* Incompatible Peer and adjacency with the Peer does not
                 * exist, Update the statistics and Drop the PDU
                 */

                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : P2P Hello - Ignored - Circuit [%u], Level [%u] - Wrong System\n",
                         pCktRec->u4CktIdx, pCktRec->u1CktLevel));
                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                IsisAdjEnqueueWrongSysEvt (pContext, pu1PDU);
            }
            break;

        case ISIS_LEVEL12:

            if (i4AAChkFlag == ISIS_SUCCESS)
            {
                /* Refer Table - 6 Level2 State table for matching
                 * areas  (ISO 10589)
                 */

                IsisAdjValP2PThreewayHelloSelfL12MatAA (pCktRec, pu1PDU,
                                                        u1CktType);
            }
            else
            {
                /* Refer Table - 8 Level2 only State table for non matching
                 * areas  (ISO 10589)
                 */

                IsisAdjValAAMismatchHelloThreeWay (pCktRec, pu1PDU, u1CktType);
            }
            break;

        case ISIS_LEVEL2:

            /* Local System is a Level2 system and hence we need not worry
             * about the Area Address Match check. Whether the Area
             * Addresses match or not, the PDU can be processed as if no
             * area addresses matched. The only difference that is mentioned
             * in the (Spec. Table 7. and Table 8.) is the Event to be
             * generated when an adjacency is brought down. For matching
             * areas case the Down event is Wrong System and non-matching
             * case it is Area Mismatch Event. Our implementation generates
             * only Wrong System Event for both the cases since that is what
             * is inferred from the PDU
             */

            IsisAdjValAAMismatchHelloThreeWay (pCktRec, pu1PDU, u1CktType);
            break;

        default:

            ISIS_INCR_SYS_DROPPED_PDUS (pContext);
            WARNING ((ISIS_LGST, "ADJ <W> : Unknown Local System Type\n"));
            break;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjP2PTwoWayDecision()\n"));
}

/******************************************************************************
 * Function      : IsisAdjUpdateNbrThreeWayInfoinAdj()
 * Description   : This function processes TLV 240 in the received Point-to-Point 
 *                    IIH PDU and updates the Adjacency Record.
 * Input(s)      : pAdjRec - Pointer to Adjacency Record
 *                 pu1PDU  - Pointer to the received Hello Pdu
 *                 i4AAChkFlag - To know if the peer have a matching Area Address 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisAdjUpdateNbrThreeWayInfoinAdj (tIsisAdjEntry * pAdjRec, UINT1 *pu1PDU)
{
    UINT1               u1Len = 0;
    UINT2               u2Offset = 0;

    if ((IsisUtlGetNextTlvOffset
         (pu1PDU, ISIS_P2P_THREE_WAY_ADJ_TLV, &u2Offset,
          &u1Len)) == ISIS_SUCCESS)
    {
        /* TLV 240 present - the TLV is already validated, so not to worry */
        /* 1). Get the three way state that is received in the PDU */
        pAdjRec->u1P2PPeerThreewayState = *(pu1PDU + u2Offset);    /*  T(1) + L(1) = 2 is already included in offset */
        if (u1Len > ISIS_P2P_THREE_WAY_STATE_LEN)
        {
            /* process otional TLVs */
            /* 2). Extended Local Circuit ID */
            ISIS_EXTRACT_P2P_NEIGHBOR_EXT_CIRC_ID (pu1PDU,
                                                   (u2Offset +
                                                    ISIS_P2P_THREE_WAY_STATE_LEN),
                                                   pAdjRec->
                                                   u4NeighExtCircuitID);
            /*Adj Record should definitely be available - anyway make a safety check */
            if (u1Len > ISIS_P2P_THREE_WAY_HS_MIN_LEN)
            {
                /* Neighbor System info present 
                 * If validation fails - necessary actions has to be taken */
                ISIS_EXTRACT_P2P_NEIGHBOR_SYS_ID (pu1PDU,
                                                  (u2Offset +
                                                   ISIS_P2P_THREE_WAY_STATE_LEN
                                                   + ISIS_P2P_EXT_CKT_ID_LEN),
                                                  pAdjRec->au1AdjPeerNbrSysID);
                ISIS_EXTRACT_P2P_NEIGHBOR_EXT_CIRC_ID (pu1PDU,
                                                       (u2Offset +
                                                        ISIS_P2P_THREE_WAY_STATE_LEN
                                                        +
                                                        ISIS_P2P_EXT_CKT_ID_LEN
                                                        + ISIS_SYS_ID_LEN),
                                                       pAdjRec->
                                                       u4AdjPeerNbrExtCircuitID);
                /* 3). Neighbor System ID */
                /* 4). Neighbor Extended Circuit ID */
            }
            pAdjRec->u1ThreeWayHndShkVersion = ISIS_P2P_THREE_WAY_V2;
        }
        else
        {
            pAdjRec->u1ThreeWayHndShkVersion = ISIS_P2P_THREE_WAY_V1;
        }
    }
    return;
}

/******************************************************************************
 * Function      : IsisAdjPerformP2PThreeWayHndshk ()
 * Description   : This function performs 3-way handshake state transtitions  based on 
 *                    TLV 240 in the received IIH PDU. It updates the Adjacency Database 
 *                    as per RFC 5303. 
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pu1PDU  - Pointer to the received Hello Pdu
 *                 i4AAChkFlag - To know if the peer have a matching Area Address 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisAdjPerformP2PThreeWayHndshk (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                                 INT4 i4AAChkFlag)
{
    tIsisAdjEntry      *pAdjRec = pCktRec->pAdjEntry;
    UINT1               u1Decision = ISIS_ADJ_DOWN;
    UINT1               u1PeerThreeWayState = ISIS_ADJ_DOWN;
    UINT1               u1PrevState = ISIS_ADJ_STATE_NONE;
    UINT1               u1PduType = 0;

    IsisAdjP2PTwoWayDecision (pCktRec, pu1PDU, i4AAChkFlag);
    if ((pAdjRec == NULL)
        || (pCktRec->u1TwoWayStateDecision == ISIS_ADJ_STATE_NONE))
    {
        /*Three-way processing not required for this state */
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : P2P Hello Processing failed! - Circuit [%u], Level [%u] Two-way State [%u]"
                 " and AdjRec May not exist\n", pCktRec->u4CktIdx,
                 pCktRec->u1CktLevel, pCktRec->u1TwoWayStateDecision));
        ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
        return;
    }

    IsisAdjUpdateNbrThreeWayInfoinAdj (pAdjRec, pu1PDU);

    /*                  3 - way state transition Table
     *
     *                              Down       Initializing    Up
     *                         --------------------------------------
     *            Down         |  Initialize        Up         Down
     *                         |
     *    Adj.    Initializing |  Initialize        Up         Up
     *    Three-               |
     *    Way     Up           |  Initialize        Accept     Accept
     *    State                |
     *
     *
     */

    if (((pAdjRec->u1P2PPeerThreewayState == ISIS_ADJ_UP)
         || (pAdjRec->u1P2PPeerThreewayState == ISIS_ADJ_INIT))
        && (pAdjRec->u1ThreeWayHndShkVersion == ISIS_P2P_THREE_WAY_V2))
    {
        if (((MEMCMP
              (pAdjRec->au1AdjPeerNbrSysID,
               pCktRec->pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN)) != 0)
            || (pAdjRec->u4AdjPeerNbrExtCircuitID != pCktRec->u4ExtLocalCircID))
        {
            /* Delete adjacency - and generate down event
             * This PDU should be simply discarded if an adjacency already exists, note that holdtimer is not restarted*/
            u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);
            WARNING ((ISIS_LGST,
                      "ADJ <T> : Adjacency Exists - PDU Discarded - Circuit [%u], Level [%u], PDU [%s]\n",
                      pCktRec->u4CktIdx, pCktRec->u1CktLevel,
                      ISIS_GET_PDU_TYPE_STR_FOR_TRACE (u1PduType)));
            ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
            return;
        }
    }

    /* At this point we have completely validated TLV 240 
     * So Go ahead with 3-way hand shake state transitions */
    u1PeerThreeWayState = pAdjRec->u1P2PPeerThreewayState;

    if (pAdjRec->u1P2PThreewayState == ISIS_ADJ_DOWN)
    {
        switch (u1PeerThreeWayState)
        {
            case ISIS_ADJ_DOWN:
                u1Decision = ISIS_ADJ_INIT;
                break;
            case ISIS_ADJ_INIT:
                u1Decision = ISIS_ADJ_UP;
                break;
            case ISIS_ADJ_UP:
                u1Decision = ISIS_ADJ_DOWN;
                break;
            default:
                u1Decision = ISIS_ADJ_DOWN;
        }
    }
    else if (pAdjRec->u1P2PThreewayState == ISIS_ADJ_INIT)
    {
        switch (u1PeerThreeWayState)
        {
            case ISIS_ADJ_DOWN:
                u1Decision = ISIS_ADJ_INIT;
                break;
            case ISIS_ADJ_INIT:
                u1Decision = ISIS_ADJ_UP;
                break;
            case ISIS_ADJ_UP:
                u1Decision = ISIS_ADJ_UP;
                break;
            default:
                u1Decision = ISIS_ADJ_DOWN;
        }
    }
    else if (pAdjRec->u1P2PThreewayState == ISIS_ADJ_UP)
    {
        switch (u1PeerThreeWayState)
        {
            case ISIS_ADJ_DOWN:
                u1Decision = ISIS_ADJ_INIT;
                break;
            case ISIS_ADJ_INIT:
                u1Decision = ISIS_ADJ_STATE_ACCEPT;
                break;
            case ISIS_ADJ_UP:
                u1Decision = ISIS_ADJ_STATE_ACCEPT;
                break;
            default:
                u1Decision = ISIS_ADJ_DOWN;
        }
    }

    /* Actions based on new three way state */
    u1PrevState = pAdjRec->u1P2PThreewayState;
    if ((u1PrevState != u1Decision) && (u1Decision != ISIS_ADJ_STATE_ACCEPT))
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : ADJ state changed from [%s] to [%s]  - Circuit [%u], Level [%u] - Area Address Mismatch\n",
                 ISIS_GET_ISIS_ADJ_STATUS (u1PrevState),
                 ISIS_GET_ISIS_ADJ_STATUS (u1Decision), pCktRec->u4CktIdx,
                 pCktRec->u1CktLevel));
    }
    switch (u1Decision)
    {
        case ISIS_ADJ_DOWN:
            /*Generate Down event and delete the adjacency */
            IsisP2PDelAdj (pCktRec);
            ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
            return;
        case ISIS_ADJ_INIT:
            pAdjRec->u1P2PThreewayState = ISIS_ADJ_INIT;
            if (u1PrevState == ISIS_ADJ_UP)
            {
                /* This is done because - adjacency is being reinitiated 
                 * and CSNP has to be sent again*/
                IsisUpdEnqueueP2PCktChgEvt (pCktRec->pContext, ISIS_CKT_DOWN,
                                            pCktRec->u4CktIdx);
            }
            IsisAdjP2PHandleStateChange (pCktRec, pAdjRec, pu1PDU);
            break;
        case ISIS_ADJ_UP:
            pAdjRec->u1P2PThreewayState = ISIS_ADJ_UP;
            /*Generate Up event */
            IsisAdjP2PHandleStateChange (pCktRec, pAdjRec, pu1PDU);
            break;
        case ISIS_ADJ_STATE_ACCEPT:
            IsisAdjP2PModifyThreeWayAdj (pCktRec, pAdjRec, pu1PDU);
            break;
    }
    if ((u1PrevState != pAdjRec->u1P2PThreewayState) &&
        ((pAdjRec->u1P2PThreewayState == ISIS_ADJ_INIT) ||
         (pAdjRec->u1P2PThreewayState == ISIS_ADJ_UP)))
    {
        IsisAdjTxHello (pCktRec->pContext, pCktRec,
                        pCktRec->u1CktLevel, ISIS_FALSE);
    }
    ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
    return;
}

#ifdef BFD_WANTED
/******************************************************************************
 * Function      : IsisAdjProcBfdinP2PIIH()
 * Description   : This function performs BFD checks on the received IIH PDU. 
 *                    It updates the Adjacency state or decides not to update the
 *                    adjacency holding timer based on the MT support between the 
 *                    peers and the neigbour useable state. 
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pu1PDU  - Pointer to the received Hello Pdu
 *                 i4AAChkFlag - To know if the peer have a matching Area Address 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS (or) ISIS_FAILURE
 ******************************************************************************/

INT4
IsisAdjProcBfdinP2PIIH (tIsisCktEntry * pCktRec, tIsisAdjEntry * pAdjRec,
                        UINT1 *pu1PDU, tBool * pbLSUFlag)
{
    tIsisSysContext    *pContext = pCktRec->pContext;
    UINT1               u1BfdReqd = ISIS_BFD_FALSE;
    UINT1               u1AdjState = ISIS_ADJ_DOWN;
    UINT1               u1NewAdjUpdStatus = ISIS_FALSE;
    UINT1               u12waystate;
    UINT1               u13waystate;
    UINT1               u1HoldtmrState;

    /*Inorder to update bLSUFlag - the previous states are noted 
     * If anychange is detected at the end update the flag 
     * so that sync can be sent*/

    /*This function should handle BFD for both 2-way and 3-way
     * If Session is treated as DOWN - 
     *      2-way state - INIT
     *      3-way state - DOWN */
    u12waystate = pAdjRec->u1AdjState;
    u13waystate = pAdjRec->u1P2PThreewayState;
    u1HoldtmrState = pAdjRec->u1DoNotUpdHoldTmr;

    /* Checking the BFD support status */
    if ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
        (pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE) &&
        (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
    {
        u1BfdReqd = ISIS_BFD_REQUIRED (pAdjRec);

        /* Read the BFD Enabled TLV is present */
        if (IsisAdjUpdateBfdEnabled (pu1PDU, pAdjRec, pbLSUFlag, &u1AdjState) !=
            ISIS_SUCCESS)
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <E> : Processing of BFD Enabled TLV has failed\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            ISIS_INCR_SYS_DROPPED_PDUS (pContext);
            ISIS_INCR_REJ_ADJ_STAT (pCktRec);
            ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcLANHello ()\n"));
            return ISIS_FAILURE;
        }

        /* Update the BFD reqd variable */
        IsisUtlUpdateBfdRequired (pCktRec, pAdjRec);

        /* There are 4 possibilities corresponding to ISIS_BFD_REQD when there
         * the Adjacent Entry is already present. 
         *      1. ISIS_BFD_REQD is different and ENABLED:
         *               ==> In this case check for the adj state.
         *                   If ADJ_INIT -> Proceed with ADJ_INIT itself, since 
         *                                  ADJ_UP will be done once session up
         *                                  notification comes from BFD.
         *                   If ADJ_UP   -> Do not update the hold timer 
         *                                  until ISIS_BFD_NEIGBOR_USEABLE
         *                                  is TRUE.
         *       2. ISIS_BFD_REQD is different and DISABLED:
         *               ==> In this case, ISIS has to be deregistered from
         *                                 BFD, which will be taken care in 
         *                                 IsisUtlUpdateBfdRequired.
         *                    Do not change the u1AdjState decided by IsisAdjSelfLANAddrListed.
         *       3. ISIS_BFD_REQD is same and ENABLED:
         *               ==> Check for the adj state. 
         *                   Same Adj State as the previous has to be maintained, since
         *                   Adj UP and DOWN will be taken care by BFD state notification
         *       4. ISIS_BFD_REQD is same and DISABLED:
         *               ==> No issue, proceed. Do not change the u1AdjState decided
         *                   by IsisAdjSelfLANAddrListed.
         */
        if ((pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE) &&
            (ISIS_BFD_REQUIRED (pAdjRec) == ISIS_BFD_TRUE) &&
            (ISIS_CHK_BFD_STATUS (pAdjRec) == ISIS_BFD_FALSE))
        {
            if (pCktRec->u1P2PDynHshkMachanism == ISIS_P2P_THREE_WAY)
            {
                if (pAdjRec->u1P2PThreewayState == ISIS_ADJ_UP)
                {
                    pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_TRUE;
                }
                else if (pAdjRec->u1P2PThreewayState == ISIS_ADJ_INIT)
                {
                    pAdjRec->u1P2PThreewayState = ISIS_ADJ_DOWN;
                    pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_FALSE;
                }
            }
            else
            {
                u1NewAdjUpdStatus = ISIS_TRUE;
                if (pAdjRec->u1AdjState == ISIS_ADJ_UP)
                {
                    pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_TRUE;
                }
                else if (pAdjRec->u1AdjState == ISIS_ADJ_INIT)
                {
                    pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_FALSE;
                }

            }
        }
        if ((ISIS_BFD_REQUIRED (pAdjRec) != u1BfdReqd) &&
            (ISIS_BFD_REQUIRED (pAdjRec) == ISIS_BFD_TRUE))
        {
            u1NewAdjUpdStatus = ISIS_TRUE;
            if (ISIS_GET_ADJ_STATE (pAdjRec) == ISIS_ADJ_UP)
            {
                pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_TRUE;
            }
            else
            {
                pAdjRec->u1AdjState = ISIS_ADJ_INIT;
                pAdjRec->u1P2PThreewayState = ISIS_ADJ_DOWN;
                pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_FALSE;
            }
        }
    }
    /*update the sync requirement status before leaving */

    if ((u1NewAdjUpdStatus == ISIS_FALSE)
        && (pCktRec->u1P2PDynHshkMachanism == ISIS_P2P_TWO_WAY))
    {
        pAdjRec->u1AdjState = ISIS_ADJ_UP;
    }
    if ((u12waystate != pAdjRec->u1AdjState) ||
        (u13waystate != pAdjRec->u1P2PThreewayState) ||
        (u1HoldtmrState != pAdjRec->u1DoNotUpdHoldTmr))
    {
        /*Atleast one of the variables is updated */
        *pbLSUFlag = ISIS_TRUE;
    }

    return ISIS_SUCCESS;
}
#endif

/******************************************************************************
 * Function      : IsisAdjP2PModifyThreeWayAdj()
 * Description   : This function updates the Adjcency Record that is already UP (3-way)
 *                    based on the newly received IIH PDU. 
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pAdjRec  - Pointer to the Adjacency Record that is to be updated 
 *                 pu1PDU  - Pointer to the received Hello Pdu
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisAdjP2PModifyThreeWayAdj (tIsisCktEntry * pCktRec, tIsisAdjEntry * pAdjRec,
                             UINT1 *pu1PDU)
{
    UINT4               u4DirIdx = 0;
#ifdef BFD_WANTED
    INT4                i4RetVal = ISIS_FAILURE;
#endif
    tBool               bLSUFlag = ISIS_FALSE;

#ifdef BFD_WANTED
    i4RetVal = IsisAdjProcBfdinP2PIIH (pCktRec, pAdjRec, pu1PDU, &bLSUFlag);
    if (i4RetVal == ISIS_FAILURE)
    {
        return;
    }
#endif

    ISIS_EXTRACT_HOLD_TIME (pu1PDU, pAdjRec->u2HoldTime);

    MEMCPY (pAdjRec->au1AdjNbrSysID, (pu1PDU + sizeof (tIsisComHdr) + 1),
            ISIS_SYS_ID_LEN);

    IsisAdjModifyAdjAreaAddr (pu1PDU, pCktRec, pAdjRec);

    if (pCktRec->pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        IsisAdjUpdateMTId (pu1PDU, pAdjRec, &bLSUFlag);
    }

    if ((pCktRec->pContext->u1IsisMTSupport == ISIS_FALSE) ||
        (pCktRec->pContext->u1IsisBfdSupport != ISIS_BFD_ENABLE) ||
        (pCktRec->u1IsisBfdStatus != ISIS_BFD_ENABLE))
    {
        /*This is done because, bfd also calls the same function */
        if (IsisAdjUpdateIpAddr (pu1PDU, pAdjRec, &bLSUFlag) != ISIS_SUCCESS)
        {
            return;
        }
    }

    u4DirIdx = IsisAdjGetDirEntryWithAdjIdx (pCktRec->pContext, pAdjRec);

    if ((pAdjRec->u1DoNotUpdHoldTmr == ISIS_BFD_FALSE) &&
        (pAdjRec->u1IsisGRHelperStatus != ISIS_GR_HELPING))
    {
        IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                               u4DirIdx, pAdjRec->u2HoldTime,
                               &pAdjRec->u1TmrIdx);
    }
#ifdef ISIS_FT_ENABLED
    if (bLSUFlag == ISIS_TRUE)
    {
        ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_ADD, pAdjRec);
    }
#endif
    return;
}

/******************************************************************************
 * Function      : IsisAdjP2PHandleStateChange ()
 * Description   : This function handles state change operations on an Adjcency Record 
 *                    for the updated 3-way Adjacency state.
 *                    based on the newly received IIH PDU. 
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pAdjRec  - Pointer to the Adjacency Record that is to be updated 
 *                 pu1PDU  - Pointer to the received Hello Pdu
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisAdjP2PHandleStateChange (tIsisCktEntry * pCktRec, tIsisAdjEntry * pAdjRec,
                             UINT1 *pu1PDU)
{
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4DirIdx = 0;
    tBool               bLSUFlag = ISIS_FALSE;
    UINT1               u1ECTId = 0;
#ifdef BFD_WANTED
    INT4                i4RetVal = ISIS_FAILURE;
#endif

    pContext = pCktRec->pContext;

    pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);

#ifdef BFD_WANTED
    i4RetVal = IsisAdjProcBfdinP2PIIH (pCktRec, pAdjRec, pu1PDU, &bLSUFlag);
    if (i4RetVal == ISIS_FAILURE)
    {
        return;
    }
#endif

    ISIS_INCR_ADJ_CHG_STAT (pCktRec);

    MEMCPY (pAdjRec->au1AdjNbrSysID, (pu1PDU + sizeof (tIsisComHdr) + 1),
            ISIS_SYS_ID_LEN);
    pAdjRec->u1AdjNbrSysType = ISIS_EXTRACT_HELLO_CKT_TYPE (pu1PDU);

    if (pAdjRec->u1P2PThreewayState == ISIS_ADJ_UP)
    {
        /* Form Adjacency Change Event and enqueue Event to
         * Control Queue. Update module will require this
         */

        if (ISIS_CONTEXT_ACTIVE (pContext) == ISIS_TRUE)
        {
            IsisAdjEnqueueAdjChgEvt (pCktRec, pCktLevel, pAdjRec, ISIS_ADJ_UP);
            pCktLevel->u4NumAdjs++;

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
            {
                IsisUtlSetBPat (&pContext->CktTable.pu1L1CktMask,
                                pCktRec->u4CktIdx);

                /* First adjacency on the given circuit has come up. We can now
                 * start the LSP transmission timer
                 */

                IsisTmrStartECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                     pCktRec->u4CktIdx, 1,
                                     &pCktLevel->u1LSPTxTmrIdx);
                IsisTmrRestartTimer (&(pContext->SysL1Info.SysLSPGenTimer),
                                     ISIS_L1LSP_GEN_TIMER, 1);
            }

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
            {
                IsisUtlSetBPat (&pContext->CktTable.pu1L2CktMask,
                                pCktRec->u4CktIdx);

                /* First adjacency on the given circuit has come up. We can now
                 * start the LSP transmission timer
                 */

                IsisTmrStartECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                     pCktRec->u4CktIdx, 1,
                                     &pCktLevel->u1LSPTxTmrIdx);
                IsisTmrRestartTimer (&(pContext->SysL2Info.SysLSPGenTimer),
                                     ISIS_L2LSP_GEN_TIMER, 1);
            }
        }
        if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
        {
            (pContext->CktTable.u4NumL1Adjs)++;
        }
        if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
        {
            (pContext->CktTable.u4NumL2Adjs)++;
        }
        /*Starting PSNP timers for the circuit */
        if (pAdjRec->u1AdjUsage & ISIS_LEVEL1)
        {
            if (pCktRec->pL1CktInfo->u1ECTId == 0)
            {
                IsisTmrStartECTimer (pContext, ISIS_ECT_L1_PSNP,
                                     pCktRec->u4CktIdx,
                                     pCktRec->pL1CktInfo->PSNPInterval,
                                     &u1ECTId);
                pCktRec->pL1CktInfo->u1ECTId = u1ECTId;
            }
            else
            {
                IsisTmrRestartECTimer (pContext, ISIS_ECT_L1_PSNP,
                                       pCktRec->u4CktIdx,
                                       pCktRec->pL1CktInfo->PSNPInterval,
                                       &pCktRec->pL1CktInfo->u1ECTId);
            }
        }

        if (pAdjRec->u1AdjUsage & ISIS_LEVEL2)
        {
            if (pCktRec->pL2CktInfo->u1ECTId == 0)
            {
                IsisTmrStartECTimer (pContext, ISIS_ECT_L2_PSNP,
                                     pCktRec->u4CktIdx,
                                     pCktRec->pL2CktInfo->PSNPInterval,
                                     &u1ECTId);
                pCktRec->pL2CktInfo->u1ECTId = u1ECTId;
            }
            else
            {
                IsisTmrRestartECTimer (pContext, ISIS_ECT_L2_PSNP,
                                       pCktRec->u4CktIdx,
                                       pCktRec->pL2CktInfo->PSNPInterval,
                                       &pCktRec->pL2CktInfo->u1ECTId);
            }
        }
    }
    else
    {
        pAdjRec->u1AdjState = ISIS_ADJ_INIT;
        /*This is to handle state change caused by bfd down */
        /*Even though the adjacency is down - adj rec exists and the CKT is UP
         * So restart hello interval for the circuit*/
        if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
        {
            IsisTmrStopECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                pCktRec->u4CktIdx, pCktLevel->u1LSPTxTmrIdx);

            if (pCktRec->pL1CktInfo->u1ECTId != 0)
            {
                IsisTmrStopECTimer (pContext, ISIS_ECT_L1_PSNP,
                                    pCktRec->u4CktIdx,
                                    pCktRec->pL1CktInfo->u1ECTId);
                pCktRec->pL1CktInfo->u1ECTId = 0;
            }
            if (pCktRec->pL1CktInfo->u1HelloTmrIdx != 0)
            {
                IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_P2P_HELLO,
                                       pCktRec->u4CktIdx,
                                       pCktRec->pL1CktInfo->HelloTimeInt,
                                       &pCktRec->pL1CktInfo->u1HelloTmrIdx);
            }
            else
            {
                IsisTmrStartECTimer (pCktRec->pContext, ISIS_ECT_P2P_HELLO,
                                     pCktRec->u4CktIdx,
                                     pCktRec->pL1CktInfo->HelloTimeInt,
                                     &pCktRec->pL1CktInfo->u1HelloTmrIdx);
            }
        }

        if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
        {
            IsisTmrStopECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                pCktRec->u4CktIdx, pCktLevel->u1LSPTxTmrIdx);
            if (pCktRec->pL2CktInfo->u1ECTId != 0)
            {
                IsisTmrStopECTimer (pContext, ISIS_ECT_L2_PSNP,
                                    pCktRec->u4CktIdx,
                                    pCktRec->pL2CktInfo->u1ECTId);
                pCktRec->pL2CktInfo->u1ECTId = 0;
            }
            if (pCktRec->pL2CktInfo->u1HelloTmrIdx != 0)
            {
                IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_P2P_HELLO,
                                       pCktRec->u4CktIdx,
                                       pCktRec->pL2CktInfo->HelloTimeInt,
                                       &pCktRec->pL2CktInfo->u1HelloTmrIdx);
            }
            else
            {
                IsisTmrStartECTimer (pCktRec->pContext, ISIS_ECT_P2P_HELLO,
                                     pCktRec->u4CktIdx,
                                     pCktRec->pL2CktInfo->HelloTimeInt,
                                     &pCktRec->pL2CktInfo->u1HelloTmrIdx);
            }
        }
    }

    u4DirIdx = IsisAdjGetDirEntryWithAdjIdx (pCktRec->pContext, pAdjRec);
    IsisAdjModifyAdjAreaAddr (pu1PDU, pCktRec, pAdjRec);

    if (pCktRec->pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        IsisAdjUpdateMTId (pu1PDU, pAdjRec, &bLSUFlag);
    }
    if ((pContext->u1IsisMTSupport == ISIS_FALSE) ||
        (pCktRec->pContext->u1IsisBfdSupport != ISIS_BFD_ENABLE) ||
        (pCktRec->u1IsisBfdStatus != ISIS_BFD_ENABLE))
    {
        /*This is done because, bfd also calls the same function */
        if (IsisAdjUpdateIpAddr (pu1PDU, pAdjRec, &bLSUFlag) != ISIS_SUCCESS)
        {
            return;
        }
    }

#ifdef BFD_WANTED
    if ((pAdjRec->u1P2PThreewayState == ISIS_ADJ_UP) &&
        (pContext->u1IsisMTSupport == ISIS_FALSE) &&
        (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
    {
        ISIS_BFD_REGISTER (pContext, pAdjRec, pCktRec);
    }
#endif

#ifdef ISIS_FT_ENABLED
    ISIS_FLTR_ADJ_LSU (pContext, ISIS_CMD_ADD, pAdjRec);
#endif

    if (pAdjRec->u1DoNotUpdHoldTmr == ISIS_BFD_FALSE)
    {
        u4DirIdx = IsisAdjGetDirEntryWithAdjIdx (pContext, pAdjRec);
        if (pAdjRec->u1TmrIdx != 0)
        {
            IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                                   u4DirIdx, pAdjRec->u2HoldTime,
                                   &pAdjRec->u1TmrIdx);
        }
        else
        {
            IsisTmrStartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                                 u4DirIdx, pAdjRec->u2HoldTime,
                                 &pAdjRec->u1TmrIdx);
        }
    }

    return;
}

/******************************************************************************
 * Function      : IsisP2PDelAdj()
 * Description   : This function handles deletion of a P2P Adjcency Record 
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pAdjRec  - Pointer to the Adjacency Record that is to be updated 
 *                 pu1PDU  - Pointer to the received Hello Pdu
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisP2PDelAdj (tIsisCktEntry * pCktRec)
{
    UINT1               u1TmrIdx = 0;
    UINT4               u4DirIdx = 0;
    tIsisSysContext    *pContext = pCktRec->pContext;
    tIsisAdjEntry      *pAdjRec = pCktRec->pAdjEntry;

    if (pAdjRec == NULL)
    {
        /*Already No adjacency */
        return;
    }

    /* Adjacency is deleted on a P2P circuit - so stop PSNP timer */
    if (pAdjRec->u1AdjUsage & ISIS_LEVEL1)
    {
        if (pCktRec->pL1CktInfo->u1ECTId != 0)
        {
            TMP_PT ((ISIS_LGST,
                     "TMR <T> : Stopping L1 PSNP Timer - Circuit [%u]\n",
                     pCktRec->u4CktIdx));
            IsisTmrStopECTimer (pContext, ISIS_ECT_L1_PSNP, pCktRec->u4CktIdx,
                                pCktRec->pL1CktInfo->u1ECTId);
            pCktRec->pL1CktInfo->u1ECTId = 0;
        }
        /*Also update the CSNPSent flag on the circuit LEVEL record */
        pCktRec->pL1CktInfo->u1CSNPSent = ISIS_ZERO;
    }

    if (pAdjRec->u1AdjUsage & ISIS_LEVEL2)
    {
        if (pCktRec->pL2CktInfo->u1ECTId != 0)
        {
            TMP_PT ((ISIS_LGST,
                     "TMR <T> : Stopping L2 PSNP Timer - Circuit [%u]\n",
                     pCktRec->u4CktIdx));
            IsisTmrStopECTimer (pContext, ISIS_ECT_L2_PSNP, pCktRec->u4CktIdx,
                                pCktRec->pL2CktInfo->u1ECTId);
            pCktRec->pL2CktInfo->u1ECTId = 0;
        }
        /*Also update the CSNPSent flag on the circuit LEVEL record */
        pCktRec->pL2CktInfo->u1CSNPSent = ISIS_ZERO;
    }

    u1TmrIdx = pAdjRec->u1TmrIdx;
    /* We have already indicated to Update module about this
     * adjacency being UP and hence have to indicate the 
     * DOWN event. Hence passing ISIS_TRUE
     */

    IsisAdjDelAdj (pContext, pCktRec, pAdjRec, ISIS_TRUE, &u4DirIdx);
    /* Stopping the Hold Timer started for this adjacency
     */

    IsisUpdEnqueueP2PCktChgEvt (pCktRec->pContext, ISIS_CKT_DOWN,
                                pCktRec->u4CktIdx);
    IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING, u4DirIdx, u1TmrIdx);
    return;
}

/******************************************************************************
 * Function      : IsisAdjProcLANHello ()
 * Description   : This function processes LAN Hello PDU, updates the 
 *                 adjacency database and schedules DIS Election if
 *                 required.
 * Input(s)      : pCktRec   - Pointer to Circuit Record
 *                 pu1PDU    - Pointer to the received Hello Pdu
 *                 pu1SNPA   - Pointer to SNPA address of the peer node
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcLANHello (tIsisCktEntry * pCktRec, UINT1 *pu1PDU, UINT1 *pu1SNPA)
{
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisAdjEntry      *pTempAdjRec = NULL;
    tIsisEvtAdjRej     *pAdjEvt = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtDISElect   *pDISEvt = NULL;
    tGrSemInfo          GrSemInfo;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1              *pu1AdjID = NULL;
    UINT1               u1PduType = ISIS_P2P_IIH_PDU;
    UINT1               u1AdjPrio = 0;
    UINT1               u1AdjState = ISIS_ADJ_INIT;
    UINT1               u1AdjUsage = 0;
    UINT1               u1SchedDIS = ISIS_FALSE;
    tBool               bListed = ISIS_FALSE;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    UINT4               u4Duration = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

#ifdef BFD_WANTED
    UINT1               u1BfdReqd = ISIS_BFD_FALSE;
    tBool               bLSUFlag = ISIS_FALSE;
#endif

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcLANHello ()\n"));

    pContext = pCktRec->pContext;
    u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);
    u1AdjUsage = ISIS_GET_ADJ_USAGE (u1PduType);

    /* Get the System ID offset */

    pu1AdjID = ISIS_EXTRACT_HELLO_SYS_ID (pu1PDU);
    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pu1AdjID, acNbrSysId);

    if (MEMCMP (pu1AdjID, pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN) == 0)
    {
        ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
        ADP_PT ((ISIS_LGST,
                 "ADJ <E> : Self Hello Received - Ignored - Neighbour ID [ %s ]\n",
                 acNbrSysId));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcLANHello ()\n"));
        return;
    }

    if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
         && (u1PduType != ISIS_L1LAN_IIH_PDU))
        || ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
            && (u1PduType != ISIS_L2LAN_IIH_PDU)))
    {
        /* Peer System generated an Incorrect PDU
         */

        WARNING ((ISIS_LGST,
                  "ADJ <E> : Incompatible Hello PDU Received - Dropped - "
                  "Circuit [%u], Self SysType [ %s ], PduType [ %s ]\n",
                  pCktRec->u4CktIdx,
                  ISIS_GET_SYS_TYPE_STR (pContext->SysActuals.u1SysType),
                  ISIS_GET_PDU_TYPE_STR (u1PduType)));
        ISIS_INCR_SYS_DROPPED_PDUS (pContext);
        ISIS_INCR_REJ_ADJ_STAT (pCktRec);
        ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcLANHello ()\n"));
        return;
    }

    i4RetVal = IsisUtlAuthenticatePDU (pContext, pCktRec, pu1PDU);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Perform the Area Address Check only for L1 LAN Hello PDUs and not for
         * L2LAN Hello PDUs
         */

        if (u1PduType == ISIS_L1LAN_IIH_PDU)
        {
            i4RetVal = IsisAdjAreaAddrMatchCheck (pContext, pu1PDU, ISIS_TRUE);

            if (i4RetVal != ISIS_SUCCESS)
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : Area Address Mismatch - IIH Ignored - Neighbour ID [ %s ]\n",
                         acNbrSysId));
            }
            /* Maximum Area Address Check is valid only 
             * -- For L1 IIH PDUs
             * -- If one or more Area Addresses match
             * -- Only if Local IS supports a value of 3 for Max Area Addresses
             *    (or '0' which means '3')
             * -- bSysMaxAreaCheck Flag is set
             */

            if ((i4RetVal == ISIS_SUCCESS)
                && ((pContext->SysActuals.u1SysMaxAA != 3)
                    || (pContext->SysActuals.bSysMaxAACheck == ISIS_TRUE)))
            {
                i4RetVal = IsisUtlMaxAreaAddrCheck (pContext, pCktRec, pu1PDU);
            }

            if (i4RetVal != ISIS_SUCCESS)
            {
                /* Freeing the memory Allocated for  PDU
                 */

                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                ISIS_INCR_REJ_ADJ_STAT (pCktRec);
                ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
                ADP_EE ((ISIS_LGST,
                         "ADJ <X> : Exiting IsisAdjProcLANHello ()\n"));
                return;
            }
        }
        IsisAdjProcRestartTlv (pContext, pCktRec, pu1SNPA, pu1PDU);
        /* Checking if Local System's LAN Address is listed in the PDU 
         * If the LAN Address is listed the adjacency state can be set to UP
         * directly since the 3-way handshake is complete
         */

        bListed = IsisAdjSelfLANAddrListed (pCktRec, pu1PDU);
        u1AdjState = (UINT1) ((bListed == ISIS_TRUE) ? ISIS_ADJ_UP
                              : ISIS_ADJ_INIT);
        if (bListed != ISIS_TRUE)
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <E> : Self SNPA not list in IIH - Neighbour ID [ %s ]\n",
                     acNbrSysId));
        }

        /* Check if the Adjacency already exists. If it exists update 
         * adjacency with the information retrieved from the PDU. Add 
         * the adjacency to the database otherwise
         */

        if ((IsisAdjCheckLANAdjExist (pCktRec, pu1SNPA, u1AdjUsage, &pAdjRec))
            == ISIS_SUCCESS)
        {
            /* Refer Sec 8.4.2.5.3 of ISO 10589 Specification
             *
             * LAN IIH PDU received for an existing adjacency
             */
#ifdef BFD_WANTED
            /* Checking the BFD support status */
            if ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
                (pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE) &&
                (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
            {
                u1BfdReqd = ISIS_BFD_REQUIRED (pAdjRec);

                /* Read the BFD Enabled TLV is present */
                if (IsisAdjUpdateBfdEnabled
                    (pu1PDU, pAdjRec, &bLSUFlag, &u1AdjState) != ISIS_SUCCESS)
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <E> : Processing of BFD Enabled TLV has failed\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                    ISIS_INCR_REJ_ADJ_STAT (pCktRec);
                    ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
                    ADP_EE ((ISIS_LGST,
                             "ADJ <X> : Exiting IsisAdjProcLANHello ()\n"));
                    return;
                }

                /* Update the BFD reqd variable */
                IsisUtlUpdateBfdRequired (pCktRec, pAdjRec);

                /* There are 4 possibilities corresponding to ISIS_BFD_REQD when there
                 * the Adjacent Entry is already present. 
                 *      1. ISIS_BFD_REQD is different and ENABLED:
                 *               ==> In this case check for the adj state.
                 *                   If ADJ_INIT -> Proceed with ADJ_INIT itself, since 
                 *                                  ADJ_UP will be done once session up
                 *                                  notification comes from BFD.
                 *                   If ADJ_UP   -> Do not update the hold timer 
                 *                                  until ISIS_BFD_NEIGBOR_USEABLE
                 *                                  is TRUE.
                 *       2. ISIS_BFD_REQD is different and DISABLED:
                 *               ==> In this case, ISIS has to be deregistered from
                 *                                 BFD, which will be taken care in 
                 *                                 IsisUtlUpdateBfdRequired.
                 *                    Do not change the u1AdjState decided by IsisAdjSelfLANAddrListed.
                 *       3. ISIS_BFD_REQD is same and ENABLED:
                 *               ==> Check for the adj state. 
                 *                   Same Adj State as the previous has to be maintained, since
                 *                   Adj UP and DOWN will be taken care by BFD state notification
                 *       4. ISIS_BFD_REQD is same and DISABLED:
                 *               ==> No issue, proceed. Do not change the u1AdjState decided
                 *                   by IsisAdjSelfLANAddrListed.
                 */
                if ((ISIS_BFD_REQUIRED (pAdjRec) != u1BfdReqd) &&
                    (ISIS_BFD_REQUIRED (pAdjRec) == ISIS_BFD_TRUE))
                {
                    if (pAdjRec->u1AdjState == ISIS_ADJ_INIT)
                    {
                        u1AdjState = ISIS_ADJ_INIT;
                    }
                    else if (pAdjRec->u1AdjState == ISIS_ADJ_UP)
                    {
                        pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_TRUE;
                    }
                }
            }
#ifdef ISIS_FT_ENABLED
            if (bLSUFlag == ISIS_TRUE)
            {
                ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_ADD, pAdjRec);
            }
#endif

#endif
            if ((pAdjRec->u1IsisGRHelperStatus == ISIS_GR_HELPING) ||
                (pAdjRec->u1IsisAdjGRState == ISIS_ADJ_SUPPRESSED))
            {
                bListed = ISIS_TRUE;
                u1AdjState = ISIS_ADJ_UP;
            }
            if (bListed == ISIS_FALSE)
            {
                /* Local systems SNPA Address is not included in the Neighbour
                 * Address TLV of the received PDU
                 */

                if (pAdjRec->u1AdjState == ISIS_ADJ_UP)
                {
                    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                   acNbrSysId);
                    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                         ISIS_MAX_IPV4_ADDR_LEN);

                    ADP_PI ((ISIS_LGST,
                             "ADJ <I> : Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                             " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                             pCktRec->u4CktIdx, pAdjRec->u1AdjUsage, acNbrSysId,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));

                    pAdjRec->u1AdjState = ISIS_ADJ_INIT;

                    /* Post an Adjacency DOWN Event
                     */

                    IsisAdjEnqueueAdjChgEvt (pCktRec, NULL, pAdjRec,
                                             ISIS_ADJ_DOWN);

                    if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
                    {
                        (pContext->CktTable.u4NumL1Adjs)--;
                        (pCktRec->pL1CktInfo->u4NumAdjs)--;
                        if (pCktRec->pL1CktInfo->u4NumAdjs == 0)
                        {
                            IsisUtlReSetBPat (&
                                              (pContext->CktTable.pu1L1CktMask),
                                              pCktRec->u4CktIdx);
                            IsisTmrStopECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                                pCktRec->u4CktIdx,
                                                pCktRec->pL1CktInfo->
                                                u1LSPTxTmrIdx);
                        }
                    }
                    if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
                    {
                        (pContext->CktTable.u4NumL2Adjs)--;
                        (pCktRec->pL2CktInfo->u4NumAdjs)--;
                        if (pCktRec->pL2CktInfo->u4NumAdjs == 0)
                        {
                            IsisUtlReSetBPat (&
                                              (pContext->CktTable.pu1L2CktMask),
                                              pCktRec->u4CktIdx);
                            IsisTmrStopECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                                pCktRec->u4CktIdx,
                                                pCktRec->pL2CktInfo->
                                                u1LSPTxTmrIdx);
                        }
                    }

                    ISIS_INCR_ADJ_CHG_STAT (pCktRec);
                    ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);

                    ADP_EE ((ISIS_LGST,
                             "ADJ <X> : Exiting IsisAdjProcLANHello ()\n"));
                    return;
                }

                pDirEntry = pContext->AdjDirTable.pDirEntry;
                while (pDirEntry != NULL)
                {
                    if (pDirEntry->pAdjEntry == pAdjRec)
                    {
                        /* Restarting the timer for Holding time. Timer Index stored
                         * in Adjacency Record is passed to the timer Module, so that
                         * its easier to locate the record in the Equivalence Class of Timer
                         * and the block where the record is held
                         */
                        u4Duration = (UINT4) pAdjRec->u2HoldTime;
                        IsisTmrRestartECTimer (pCktRec->pContext,
                                               ISIS_ECT_HOLDING,
                                               pDirEntry->u4DirIdx, u4Duration,
                                               &(pAdjRec->u1TmrIdx));
                        break;
                    }
                    pDirEntry = pDirEntry->pNext;
                }
            }
            else
            {
                /* Update the Adjacency Record with the Holding Timer, 
                 * Priority and Area Addresses of Neighbour. The u1SchedDIS 
                 * flag will be set if there is a change in the priority of 
                 * the existing adjacency
                 */

                IsisAdjModifyLANAdj (pu1PDU, pCktRec, pAdjRec, u1AdjState,
                                     &u1SchedDIS, pu1AdjID);
            }
        }
        else
        {
            pAdjRec = (tIsisAdjEntry *) ISIS_MEM_ALLOC (ISIS_BUF_ADJN,
                                                        sizeof (tIsisAdjEntry));

            if ((pCktRec->pAdjEntry == NULL) && (pAdjRec == NULL))
            {
                /* Misconfiguration. No memory to accept new adjacency
                 * eventhough there are no active adjacencies on the circuit
                 */
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL
                        " : New Adjacency - Cannot PRUNE\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : No Adjacencies On Circuit - Index [ %u ] \n",
                         pCktRec->u4CktIdx));
            }
            else if (pAdjRec == NULL)
            {
                /* Insufficient Space in Adj DataBase and hence try to prune
                 * existing adjacencies and verify whether the new adjacency
                 * that is coming up is eligible to be included in place of an
                 * existing adjacency
                 *
                 * Refer to 8.4.3 of Spec 10589
                 */

                /* The following routine retrieves an adjacency from the
                 * database meeting the following conditions: 
                 *
                 * -- is an adjacency with lowest priority
                 * -- if more than one adjacency exist with the same priority,
                 *    then it retrieves an adjacency with lowest SNPA
                 */

                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : Insufficient Space In Database\n"));

                /* Check whether there are any active adjacencies in the circuit
                 */

                IsisAdjPruneAdjs (pCktRec, &pAdjRec);

                /* The logic of pruning is, first prune existing adjacencies.
                 * Compare the pruned adjacency with the new adjacency being
                 * created. If the existing is to be removed, update the
                 * adjacency record with the information retrieved from the
                 * received IIH PDU and insert it back into the database.
                 * Otherwise discard the new adjacency 
                 */

                u1AdjPrio = ISIS_EXTRACT_PRIORITY (pu1PDU);

                if ((pAdjRec->u1AdjNbrPriority < u1AdjPrio)
                    || ((pAdjRec->u1AdjNbrPriority == u1AdjPrio)
                        && (MEMCMP (pu1SNPA, pAdjRec->au1AdjNbrSNPA,
                                    ISIS_SNPA_ADDR_LEN) > 0)))
                {
                    /* The new adjacency is preferred over the adjacency that is
                     * just pruned from the database. Remove the pruned
                     * adjacency from the database, build Adjacency information
                     * based on the details retrieved from the PDU and insert it
                     * back into the database
                     */

                    /* NOTE: IsisAdjRemAdj () will de-link the adjacency node
                     * from the database, but does not free the memory
                     */

                    ADP_PT ((ISIS_LGST,
                             "ADJ <E> : Existing Adjacency Dropped - Index [ %u ]\n",
                             pAdjRec->u4AdjIdx));
                    IsisAdjRemAdj (pCktRec, pAdjRec);

                    /* Overwrite the adjacency record with the new information.
                     * Adjacency usage is decided by the type of received PDU,
                     * and Adjacency state is set to INIT
                     */

                    IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, u1AdjState,
                                             pu1SNPA, u1AdjUsage, pAdjRec);

                    /* Eventhough this is a new adjacency we need not try to
                     * create a Direction Table Entry, since the new adjacency
                     * information is updated into an existing adjacency, which
                     * is pruned, and which is already held in a Direction Table
                     * Entry. Hence the last argument is ISIS_FALSE
                     */

                    IsisAdjAddAdj (pContext, pCktRec, pAdjRec, ISIS_FALSE);

                    /* Now that adjacency information in the database has got
                     * modified, we have to run the DIS election again to elect
                     * the appropriate node.
                     */

                    u1SchedDIS = ISIS_TRUE;
                }
                else
                {
                    /* The new adjacency is to be rejected since it is in no way
                     * preferred to existing adjacencies
                     */

                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : New Adjacency Rejected After Pruning - Circuit [ %u ]\n",
                             pCktRec->u4CktIfIdx));

                    ISIS_INCR_REJ_ADJ_STAT (pCktRec);

                    pAdjEvt = (tIsisEvtAdjRej *)
                        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtAdjRej));

                    if (pAdjEvt != NULL)
                    {
                        pAdjEvt->u1EvtID = ISIS_EVT_REJECTED_ADJ;
                        pAdjEvt->u4CktIdx = pCktRec->u4CktIfIdx;

                        MEMCPY (pAdjEvt->au1AdjSysID, pu1AdjID,
                                ISIS_SYS_ID_LEN);

                        IsisUtlSendEvent (pContext, (UINT1 *) pAdjEvt,
                                          sizeof (tIsisEvtAdjRej));
                    }
                }
            }
            else
            {
                if (pContext->u1IsisGRRestartMode == ISIS_GR_HELPER)
                {
                    pTempAdjRec = pCktRec->pAdjEntry;
                    /*To find ,to whom should we stop helping */
                    while (pTempAdjRec != NULL)
                    {
                        if (pTempAdjRec->u1IsisGRHelperStatus ==
                            ISIS_GR_HELPING)
                        {
                            IsisGrExitHelper (pContext, pTempAdjRec,
                                              ISIS_GR_HELPER_TOP_CHG);
                        }
                        pTempAdjRec = pTempAdjRec->pNext;
                    }

                }
                /* Enough space available in adjacency database.
                 */

                /* Update the adjacency record with the new information.
                 * Adjacency usage is decided by the type of received PDU,
                 * and Adjacency state is set to INIT
                 */
                if (pAdjRec != NULL)
                {
                    IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, u1AdjState,
                                             pu1SNPA, u1AdjUsage, pAdjRec);

                    IsisAdjAddAdj (pContext, pCktRec, pAdjRec, ISIS_TRUE);
                    /* If the adj state has been changed inside add adj,
                     * the u1AdjState has to updated accordingly */
                    u1AdjState = pAdjRec->u1AdjState;
                }

                /* A new adjacency is coming up, schedule DIS election procedure
                 * since the new neighbour may become the new DIS
                 */
                if (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART)
                {
                    IsisAdjProcRestartTlv (pContext, pCktRec, pu1SNPA, pu1PDU);
                    u1AdjState = ISIS_ADJ_UP;

                    MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
                    GrSemInfo.pContext = pContext;

                    if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER) &&
                        (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART) &&
                        (pContext->u1IsisGRRestartStatus ==
                         ISIS_STARTING_ROUTER))
                    {
                        IsisGrRunRestartSem (ISIS_GR_EVT_ADJ_UP,
                                             ISIS_GR_ADJ_RESTART, &GrSemInfo);
                    }

                }
                u1SchedDIS = (u1AdjState == ISIS_ADJ_UP) ? ISIS_TRUE
                    : ISIS_FALSE;
            }
        }

        if (u1SchedDIS == ISIS_TRUE)
        {
            /* Elect a DIS from the List of available adjacencies
             */

            pDISEvt = (tIsisEvtDISElect *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDISElect));
            if (pDISEvt != NULL)
            {
                pDISEvt->u1EvtID = ISIS_EVT_ELECT_DIS;
                pDISEvt->u1CktLevel = ISIS_GET_LEVEL (u1PduType);
                pDISEvt->u4CktIdx = pCktRec->u4CktIdx;

                /* Posting DIS Schedule Event
                 */

                IsisUtlSendEvent (pContext, (UINT1 *) pDISEvt,
                                  sizeof (tIsisEvtDISElect));
            }
            else
            {
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_EVTS);
            }
        }
    }
    else
    {
        /* authentication is failed Update the statistics
         */
        ISIS_INCR_SYS_DROPPED_PDUS (pContext);
        ISIS_INCR_REJ_ADJ_STAT (pCktRec);
    }

    /* Freeing the memory Allocated for the PDU
     */

    ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcLANHello ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjDISElect ()
 * Description   : This routine runs the DIS election process and elects one of
 *                 the nodes in the adjacency mesh as the Designated IS. Refer
 *                 Sec. 8.4.5 of ISO 10589. The election is applicable only for
 *                 broadcast circuits.
 * Input(s)      : pContext     - Pointer to System Context
 *                 pCktRec      - Pointer to Circuit Record
 *                 u1CktLevel   - Level of Circuit 
 *                                (Level1 or Level2)
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - if DIS is elected
 *                 ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjDISElect (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                 UINT1 u1CktLevel)
{
    INT4                i4RetVal1 = ISIS_FAILURE;
    INT4                i4RetVal2 = ISIS_FAILURE;
    UINT1               u1DISStatus = ISIS_DIS_NO_CHANGE;
    UINT1               u1TmrType = ISIS_ECT_L1LANHELLO;
    UINT1               u1DISPriority = 0;
    UINT1               au1DISSNPA[ISIS_SNPA_ADDR_LEN];
    UINT1               au1DISId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisAdjEntry      *pDISAdjEntry = NULL;
    CHAR                acCktLanDISID[(3 * ISIS_SYS_ID_LEN) + 1];
    CHAR                acSysID[(3 * ISIS_SYS_ID_LEN) + 1];
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acDISSNPA[ISIS_SNPA_ADDR_LEN];
    CHAR                acDISNbrSNPA[ISIS_SNPA_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (au1DISSNPA, 0, ISIS_SNPA_ADDR_LEN);    /*klocwork */
    MEMSET (acCktLanDISID, 0, ((3 * ISIS_SYS_ID_LEN) + 1));
    MEMSET (acSysID, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDISElect ()\n"));
    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);
    ADP_PT ((ISIS_LGST,
             "ADJ <T> : DIS Election Triggered For Circuit [%u], Level [%u], IPv4 [%s], IPv6 [%s]\n",
             pCktRec->u4CktIdx, u1CktLevel, au1IPv4Addr,
             Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pCktRec->au1IPV6Addr))));
    pAdjEntry = pCktRec->pAdjEntry;

    if (u1CktLevel == ISIS_LEVEL1)
    {
        pCktLevel = pCktRec->pL1CktInfo;
        u1TmrType = ISIS_ECT_L1LANHELLO;
    }
    else if (u1CktLevel == ISIS_LEVEL2)
    {
        pCktLevel = pCktRec->pL2CktInfo;
        u1TmrType = ISIS_ECT_L2LANHELLO;
    }
    else
    {
        return ISIS_FAILURE;    /*klocwork */
    }

    /* Initially Fill the DIS Priority with the Self priority, DIS SNPA with the
     * Self SNPA and assume that the Self system is the Designated IS 
     */

    u1DISPriority = pCktLevel->u1ISPriority;
    MEMCPY (au1DISSNPA, pCktRec->au1SNPA, ISIS_SNPA_ADDR_LEN);
    MEMCPY (au1DISId, pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
    au1DISId[ISIS_SYS_ID_LEN] = pCktRec->u1CktLocalID;
    /* Refer Sec 8.4.5 of ISO 10589 Specification
     */

    while (pAdjEntry != NULL)
    {
        /* Election is for the given Level only. Among the Adjacencies of 
         * the same level, only those Adjacencies whose State is UP are 
         * considered for Election
         */

        if ((pAdjEntry->u1AdjUsage == u1CktLevel)
            && (pAdjEntry->u1AdjState == ISIS_ADJ_UP))
        {
            /* High Numerical value implies highest priority. The one having 
             * highest priority is selected as DIS 
             */

            if (pAdjEntry->u1AdjNbrPriority > u1DISPriority)
            {
                /* Found a Node with priority which is higher than the currently
                 * selected one. Mark this node as the DIS
                 */

                u1DISPriority = pAdjEntry->u1AdjNbrPriority;
                MEMCPY (au1DISSNPA, pAdjEntry->au1AdjNbrSNPA,
                        ISIS_SNPA_ADDR_LEN);
                MEMCPY (au1DISId, pAdjEntry->au1AdjNbrSysID,
                        ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                pDISAdjEntry = pAdjEntry;
            }
            else if (pAdjEntry->u1AdjNbrPriority == u1DISPriority)
            {
                /* Priorities are same, hence consider SNPA addresses for 
                 * breaking the tie
                 */

                i4RetVal1 = MEMCMP (pAdjEntry->au1AdjNbrSNPA,
                                    au1DISSNPA, ISIS_SNPA_ADDR_LEN);

                if (i4RetVal1 > 0)
                {
                    /* Found a Node with SNPA which is lexicographically greater
                     * than the currently selected node. Mark this node as DIS
                     */

                    u1DISPriority = pAdjEntry->u1AdjNbrPriority;
                    MEMCPY (au1DISSNPA, pAdjEntry->au1AdjNbrSNPA,
                            ISIS_SNPA_ADDR_LEN);
                    MEMCPY (au1DISId, pAdjEntry->au1AdjNbrSysID,
                            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                    pDISAdjEntry = pAdjEntry;
                }
                else if (i4RetVal1 == 0)
                {
                    /* No two systems can have same SNPA Address */
                    ISIS_FORM_STR (ISIS_SNPA_ADDR_LEN, au1DISSNPA, acDISSNPA);
                    ISIS_FORM_STR (ISIS_SNPA_ADDR_LEN, pAdjEntry->au1AdjNbrSNPA,
                                   acDISNbrSNPA);
                    WARNING ((ISIS_LGST,
                              "ADJ <W> : DIS Not Elected - Duplicate SNPA Address - Self [ %s ], Neighbour [ %s ]\n",
                              acDISSNPA, acDISNbrSNPA));

                    ISIS_DBG_PRINT_ID (au1DISSNPA, (UINT1) ISIS_SNPA_ADDR_LEN,
                                       "ADJ <T> : SNPA\t", ISIS_OCTET_STRING);
                    ISIS_DBG_PRINT_ADDR (pAdjEntry->au1AdjNbrSNPA,
                                         (UINT1) ISIS_SNPA_ADDR_LEN,
                                         "ADJ <T> : Duplicate SNPA Address\t",
                                         ISIS_OCTET_STRING);

                    /* Post Duplicate System event to Control Module
                     */

                    IsisAdjEnqueueDupSysEvt (pAdjEntry);

                    /* Post DIS not elected event to Control Module
                     */

                    IsisAdjEnqueueDISNotElectEvt (pCktRec, pAdjEntry,
                                                  ISIS_DUP_SNPA);
                    ADP_EE ((ISIS_LGST,
                             "ADJ <X> : Exiting IsisAdjDISElect ()\n"));
                    return (i4RetVal1);
                }
            }
        }
        pAdjEntry = pAdjEntry->pNext;
    }

    /* At this point one of the systems is elected as a DIS. There are 4
     * possibilities, each of which triggers a different action
     */

    i4RetVal1 = MEMCMP (au1DISId, pContext->SysActuals.au1SysID,
                        ISIS_SYS_ID_LEN);
    i4RetVal2 = MEMCMP (au1DISId, pCktLevel->au1CktLanDISID, ISIS_SYS_ID_LEN);

    /* The flag 'bIsDIS' in pCktLevel, if TRUE, indicates that the local system
     * was the DIS
     */
    if ((pCktLevel->bIsDIS != ISIS_TRUE) &&
        (i4RetVal1 == 0) && (pCktLevel->u4NumAdjs != 0))
    {
        /* Local System which was not a DIS before is now elected DIS
         */

        u1DISStatus = ISIS_DIS_LOCAL_ELECTED;
    }

    else if ((pCktLevel->bIsDIS == ISIS_TRUE) && ((i4RetVal1 != 0)
                                                  || (pCktLevel->u4NumAdjs ==
                                                      0)))
    {
        /* Local System which was a DIS before has now Resigned
         */

        u1DISStatus = ISIS_DIS_LOCAL_RESIGNED;
    }
    else if (i4RetVal2 != 0)
    {
        /* Some Remote node X which was a DIS before has Resigned and some other
         * node Y is now elected as DIS
         */

        u1DISStatus = ISIS_DIS_REMOTE_CHANGED;
    }

    switch (u1DISStatus)
    {
        case ISIS_DIS_LOCAL_ELECTED:

            ISIS_FORM_STR (ISIS_SYS_ID_LEN, pCktLevel->au1CktLanDISID,
                           acCktLanDISID);
            ISIS_FORM_STR (ISIS_SYS_ID_LEN, pContext->SysActuals.au1SysID,
                           acSysID);

            ADP_PT ((ISIS_LGST, "ADJ <T> : Local DIS Elected - Circuit [ %u ],"
                     " Level [ %u ], Previous [ %s ], Current [ %s ]\n",
                     pCktRec->u4CktIdx, u1CktLevel, acCktLanDISID, acSysID));

            /* Post LAN DIS Changed Event so that update module can update the
             * LAN adjacencies corresponding to the given circuit with the new
             * DIS Id
             */

            IsisAdjEnqueueDISChgEvt (pCktRec, u1CktLevel,
                                     pCktLevel->au1CktLanDISID, au1DISId);
            /* Update the DIS as the local system for the given circuit
             */

            MEMCPY (pCktLevel->au1CktLanDISID, pContext->SysActuals.au1SysID,
                    ISIS_SYS_ID_LEN);
            pCktLevel->au1CktLanDISID[ISIS_SYS_ID_LEN] = pCktRec->u1CktLocalID;
            pCktLevel->bIsDIS = ISIS_TRUE;
            pCktLevel->u4DISDirIdx = 0;

            /* Transmitting Hello, since DIS status has changed. The last
             * argument is ISIS_FALSE which indicates that the Hello PDU need
             * not be padded
             */

            IsisAdjTxHello (pContext, pCktRec, u1CktLevel, ISIS_FALSE);

            /* Now since the local system has become a DIS, Hello PDUs must be
             * generated at DR Hello Interval and not Hello Interval. 
             * Restart the active Hello timer
             */

            IsisTmrRestartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                                   pCktLevel->DRHelloTimeInt,
                                   &(pCktLevel->u1HelloTmrIdx));

            /* Transmit a LSU to the standby node
             */

            ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec,
                                   pCktLevel, pCktRec->u1CktExistState);

            /* Post a DIS Status change event since the local system has now
             * become the DIS for the given circuit. Update module now must
             * generate pseudonode LSPs on behalf of the LAN for which the local
             * system is the DIS
             */

            IsisAdjEnqueueDISStatChgEvt (pCktRec, u1CktLevel, ISIS_DIS_ELECTED);

            ISIS_INCR_DIS_CHG_STAT (pCktLevel);
            i4RetVal1 = ISIS_SUCCESS;
            break;

        case ISIS_DIS_LOCAL_RESIGNED:

            ISIS_FORM_STR (ISIS_SYS_ID_LEN, au1DISId, acSysID);
            ADP_PT ((ISIS_LGST, "ADJ <T> : DIS Resigned - Circuit [ %u ],"
                     " Level [ %u ], DIS [ %s ]\n", pCktRec->u4CktIdx,
                     u1CktLevel, acSysID));

            pCktLevel->bIsDIS = ISIS_FALSE;

            /* Note the Direction index of the DIS which will be used by
             * Decision process later
             */

            if (pDISAdjEntry != NULL)
            {
                pCktLevel->u4DISDirIdx =
                    IsisAdjGetDirEntryWithAdjIdx (pContext, pDISAdjEntry);
            }

            /* Form LAN DIS Changed Event and Enqueue to Control Module
             */

            IsisAdjEnqueueDISChgEvt (pCktRec, u1CktLevel,
                                     pCktLevel->au1CktLanDISID, au1DISId);

            /* Post a DIS Status change event since the local system has now
             * resigned as DIS for the given circuit. Update module now must
             * remove all pseudonode LSPs pertaining to the LAN represented by
             * the given circuit on which the local system resigned as DIS
             */

            IsisAdjEnqueueDISStatChgEvt (pCktRec, u1CktLevel,
                                         ISIS_DIS_RESIGNED);

            /* Transmitting Hello, since DIS status has changed
             */

            IsisAdjTxHello (pContext, pCktRec, u1CktLevel, ISIS_FALSE);

            /* Since the local system is no more a DIs for the given LAN, it can
             * now start generating Hello's at Hello Time Interval instead of DR
             * Hello Interval
             */

            IsisTmrRestartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                                   pCktLevel->HelloTimeInt,
                                   &(pCktLevel->u1HelloTmrIdx));

            /* Update the LAN Designated ID for the given circuit
             */

            MEMCPY (pCktLevel->au1CktLanDISID, au1DISId,
                    ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

            /* Transmit a LSU to the standby node
             */

            ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec,
                                   pCktLevel, pCktRec->u1CktExistState);
            ISIS_INCR_DIS_CHG_STAT (pCktLevel);
            i4RetVal1 = ISIS_SUCCESS;
            break;

        case ISIS_DIS_REMOTE_CHANGED:

            ISIS_FORM_STR (ISIS_SYS_ID_LEN, pCktLevel->au1CktLanDISID,
                           acCktLanDISID);
            ISIS_FORM_STR (ISIS_SYS_ID_LEN, au1DISId, acSysID);

            ADP_PT ((ISIS_LGST, "ADJ <T> : Remote DIS Changed - Circuit [ %u ],"
                     " Level [ %u ], Previous [ %s ], Current [ %s ]\n",
                     pCktRec->u4CktIdx, u1CktLevel, acCktLanDISID, acSysID));

            if (pDISAdjEntry != NULL)
            {
                pCktLevel->u4DISDirIdx =
                    IsisAdjGetDirEntryWithAdjIdx (pContext, pDISAdjEntry);
            }

            /* Post LAN DIS Changed Event so that update module can update the
             * LAN adjacencies corresponding to the given circuit with the new
             * DIS Id
             */

            IsisAdjEnqueueDISChgEvt (pCktRec, u1CktLevel,
                                     pCktLevel->au1CktLanDISID, au1DISId);

            MEMCPY (pCktLevel->au1CktLanDISID, au1DISId,
                    ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

            /* Transmit a LSU to the standby node
             */

            ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec,
                                   pCktLevel, pCktRec->u1CktExistState);
            ISIS_INCR_DIS_CHG_STAT (pCktLevel);
            i4RetVal1 = ISIS_SUCCESS;
            break;

        default:
            break;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDISElect ()\n"));
    return (i4RetVal1);
}

/*******************************************************************************
 * Function      : IsisAdjProcEvt ()
 * Description   : This routine decodes the received event and schedules the
 *                 appropriate processing functions based on the type of the
 *                 received Event.
 * Input(s)      : pMsg  - Pointer to the received Event Message
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcEvt (tIsisMsg * pMsg)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1CktState = ISIS_CKT_UP;
    UINT1               u1CktType = ISIS_P2P_CKT;
    UINT1               u1Event = 0;
    UINT1               u1HashIdx = 0;
    UINT1               au1Hash[8];
    UINT4               u4CktIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLvlRec = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;
    tIsisEvtIPIFChange *pIFEvt = NULL;
    tIsisEvtPriorityChange *pPrioEvt = NULL;
    tIsisEvtDISElect   *pDISEvt = NULL;
    UINT4               u4SysInstIdx = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcEvt ()\n"));

    /* Extracting SysContext from the message. All Event Messages carry System
     * context in the 'u4Context' field
     */
    u4SysInstIdx = pMsg->u4CxtOrIfindex;
    i4RetVal = IsisCtrlGetSysContext (u4SysInstIdx, &pContext);
    if (i4RetVal == ISIS_FAILURE)
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : System Context Not Found - Context [ %u ]\n",
                 u4SysInstIdx));
        ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pMsg->pu1Msg);
        return;
    }

    ISIS_GET_1_BYTE (pMsg->pu1Msg, 0, u1Event);

    switch (u1Event)
    {
        case ISIS_EVT_CKT_CHANGE:

            pCktEvt = (tIsisEvtCktChange *) (VOID *) (pMsg->pu1Msg);

            /* Extracting CktIndex, Type of Change (UP or DOWN) and 
             * Type of Circuit from the event structure 
             */

            u4CktIdx = pCktEvt->u4CktIdx;
            u1CktState = pCktEvt->u1Status;
            u1CktType = pCktEvt->u1CktType;

            i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

            if (i4RetVal == ISIS_FAILURE)
            {
                /* This is possible, since after the event is posted, manager
                 * could have deleted the circuit. Hence do not process the
                 * event beyond this point
                 */

                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Circuit Change - Circuit Does Not Exist - Index [ %u ]\n",
                         u4CktIdx));
                break;
            }

            /* Circuit Table (global) is hased on IfIndex and IfSubIndex since
             * this table is accessed most of the times during data reception
             * from Lower Layers which provide only the IfIndex and IfSubIndex.
             */

            /* Form a single Key of 8 bytes IfIndex (4) + IfSubIndex (4) bytes.
             * The hashing function uses all the 8 bytes together and returns
             * the hash key
             */

            MEMCPY (au1Hash, (UINT1 *) &(pCktRec->u4CktIfIdx), sizeof (UINT4));
            MEMCPY (&au1Hash[4],
                    (UINT1 *) &(pCktRec->u4CktIfSubIdx), sizeof (UINT4));
            u1HashIdx =
                IsisUtlGetHashIdx ((UINT1 *) au1Hash, 2 * sizeof (UINT4),
                                   gIsisCktHashTable.u1HashSize);

            if (u1CktState == ISIS_CKT_UP)
            {
                /* Till this point the circuit record is held in the Circuit
                 * Table which is local to the Context. Since the Circuit has 
                 * now become Active, it can now be made available through 
                 * the Global Circuit Hash Table also. Insert the record into 
                 * the hash table
                 */

                /* Check the compatibility between the system and circuit.
                 * if the circuit and system are not compatible, then make
                 * the circuit compatible with the system.
                 * e.g  the System is LEVEL1 and circuit is configured as
                 * LEVEL2 or LEVEL12 then change the circuit type to LEVEL1 and 
                 * activate the LEVEL1 Circuit Level Record
                 */

                ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                     ISIS_MAX_IPV4_ADDR_LEN);
                ADP_PI ((ISIS_LGST,
                         "ADJ <I> : Circuit Change - State [ %s ], Circuit Index [ %u ], Type [ %s ],"
                         " Level [ %s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         ISIS_GET_ISIS_CKT_STATUS (u1CktState), u4CktIdx,
                         ISIS_GET_CKT_TYP_STR (u1CktType),
                         ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel),
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));

                if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
                     || (pContext->SysActuals.u1SysType == ISIS_LEVEL2))
                    && (pCktRec->u1CktLevel != pContext->SysActuals.u1SysType))
                {
                    pCktRec->u1CktLevel = pContext->SysActuals.u1SysType;
                    IsisAdjActivateCktLvl (pContext, pCktRec);
                }
                if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
                    && (pCktRec->pL2CktInfo != NULL))
                {
                    IsisAdjDelCktAdjs (pContext, pCktRec, ISIS_LEVEL2);
                    IsisAdjDelCktLvlRec (pContext, pCktRec, ISIS_LEVEL2);
                    pCktRec->pL2CktInfo = NULL;
                }
                if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
                    && (pCktRec->pL1CktInfo != NULL))
                {
                    IsisAdjDelCktAdjs (pContext, pCktRec, ISIS_LEVEL1);
                    IsisAdjDelCktLvlRec (pContext, pCktRec, ISIS_LEVEL1);
                    pCktRec->pL1CktInfo = NULL;
                }
                if ((pCktRec->pL1CktInfo == NULL)
                    && (pCktRec->pL2CktInfo == NULL))
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Circuit Change - No L1 and L2 Circuit Information Found\n"));
                    break;
                }

                IsisAdjActivateCktLvl (pContext, pCktRec);
                IsisInsCktInHashTbl (&pCktRec, u1HashIdx);
                IsisAdjActivateCkt (pContext, pCktRec, ISIS_CKT_UP);
                (pContext->CktTable.u4NumActCkts)++;

            }
            else if (u1CktState == ISIS_CKT_RESTART)
            {
                /* As soon as the System receives a GO_ACTIVE command from 
                 * the STAND_BY state, Circuit Change event for all teh Active
                 * Circuits are sent  with the status as ISIS_CKT_RESTART.
                 * Till this point the circuit record is held in the Circuit
                 * Table which is local to the Context. Since the Circuit has 
                 * now become Active, it can now be made available through 
                 * the Global Circuit Hash Table also. Insert the record into 
                 * the hash table
                 */

                ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                     ISIS_MAX_IPV4_ADDR_LEN);

                ADP_PI ((ISIS_LGST,
                         "ADJ <I> : Circuit Change - State [ %s ], Circuit Index [ %u ], Type [ %s ],"
                         " Level [ %s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         ISIS_GET_ISIS_CKT_STATUS (u1CktState), u4CktIdx,
                         ISIS_GET_CKT_TYP_STR (u1CktType),
                         ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel),
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));

                IsisInsCktInHashTbl (&pCktRec, u1HashIdx);
                IsisTrfrIfStatusInd (pContext, pCktRec, ISIS_CKT_RESTART);
                IsisAdjActivateCkt (pContext, pCktRec, ISIS_CKT_RESTART);

            }
            else if (u1CktState == ISIS_CKT_DOWN)
            {
                /* Temporarily the circuit may be DOWN. Delete all the
                 * adjacencies formed over this circuit, but do not delete the
                 * circuit
                 */

                ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                     ISIS_MAX_IPV4_ADDR_LEN);

                ADP_PI ((ISIS_LGST,
                         "ADJ <I> : Circuit Change - State [ %s ], Circuit Index [ %u ], Type [ %s ],"
                         " Level [ %s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         ISIS_GET_ISIS_CKT_STATUS (u1CktState), u4CktIdx,
                         ISIS_GET_CKT_TYP_STR (u1CktType),
                         ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel),
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));

                IsisAdjDelCktAdjs (pContext, pCktRec, ISIS_LEVEL12);
                if (pCktRec->pL1CktInfo != NULL)
                {
                    MEMSET (pCktRec->pL1CktInfo->au1CktLanDISID, 0,
                            (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));
                }
                if (pCktRec->pL2CktInfo != NULL)
                {
                    MEMSET (pCktRec->pL2CktInfo->au1CktLanDISID, 0,
                            (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));
                }
                pCktRec->u1IsisGRRestartHelloTxCount = 0;
                (pContext->CktTable.u4NumActCkts)--;

                if (ISIS_CONTEXT_ACTIVE (pContext) == ISIS_TRUE)
                {
                    if (pCktRec->u1CktType == ISIS_P2P_CKT)
                    {
                        pCktLvlRec = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
                        if (pCktLvlRec != NULL)
                        {
                            ADP_PT ((ISIS_LGST,
                                     "ADJ <T> : [P2P] Stopping to Send Hello PDU [Circuit DOWN] - Circuit [ %u ]\n",
                                     pCktRec->u4CktIdx));
                            IsisTmrStopECTimer (pContext, ISIS_ECT_P2P_HELLO,
                                                pCktRec->u4CktIdx,
                                                pCktLvlRec->u1HelloTmrIdx);
                        }
                        else
                        {
                            NMP_PT ((ISIS_LGST, "NMP <E> : Cannot derefrence"
                                     "a NULL pointer \n"));
                        }

                    }
                    else if (pCktRec->u1CktType == ISIS_BC_CKT)
                    {
                        if (pCktRec->pL1CktInfo != NULL)
                        {
                            ADP_PT ((ISIS_LGST,
                                     "ADJ <T> : [BC] Stopping to Send [L1] Hello PDU [Circuit DOWN] - Circuit [ %u ]\n",
                                     pCktRec->u4CktIdx));
                            IsisTmrStopECTimer (pContext, ISIS_ECT_L1LANHELLO,
                                                pCktRec->u4CktIdx,
                                                pCktRec->pL1CktInfo->
                                                u1HelloTmrIdx);
                        }
                        if (pCktRec->pL2CktInfo != NULL)
                        {
                            ADP_PT ((ISIS_LGST,
                                     "ADJ <T> : [BC] Stopping to Send [L2] Hello PDU [Circuit DOWN] - Circuit [ %u ]\n",
                                     pCktRec->u4CktIdx));
                            IsisTmrStopECTimer (pContext, ISIS_ECT_L2LANHELLO,
                                                pCktRec->u4CktIdx,
                                                pCktRec->pL2CktInfo->
                                                u1HelloTmrIdx);
                        }
                    }
                }

                IsisDelCktInHashTbl (&pCktRec, u1HashIdx);
            }
            else if (u1CktState == ISIS_CKT_DESTROY)
            {
                /* Circuit is destroyed. Delete the circuit from the hash table
                 * and re-initialize all information pertaining to the circuit
                 */

                ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                     ISIS_MAX_IPV4_ADDR_LEN);

                ADP_PI ((ISIS_LGST,
                         "ADJ <I> : Circuit Change - State [ %s ], Circuit Index [ %u ], Type [ %s ],"
                         " Level [ %s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         ISIS_GET_ISIS_CKT_STATUS (u1CktState), u4CktIdx,
                         ISIS_GET_CKT_TYP_STR (u1CktType),
                         ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel),
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));

                IsisDelCktInHashTbl (&pCktRec, u1HashIdx);
                IsisAdjDelCkt (pContext, u4CktIdx);
            }
            break;

        case ISIS_EVT_PRIORITY_CHANGE:

            pPrioEvt = (tIsisEvtPriorityChange *) (VOID *) (pMsg->pu1Msg);
            u4CktIdx = pPrioEvt->u4CktIdx;

            i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

            if (i4RetVal == ISIS_FAILURE)
            {
                /* This is possible, since after the event is posted, manager
                 * could have deleted the circuit. Hence do not process the
                 * event beyond this point
                 */
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Circuit Priority Change - Circuit Does Not Exist - Index [ %u ]\n",
                         u4CktIdx));
                break;
            }

            if (pCktRec->u1CktType == ISIS_BC_CKT)
            {
                /* Priority change event is applicable only for BC circuit */
                /* Schedule DIS election since priority of one of the circuits has
                 * changed
                 */
                IsisAdjDISElect (pContext, pCktRec, pPrioEvt->u1CktLevel);
            }
            break;

        case ISIS_EVT_ELECT_DIS:

            pDISEvt = (tIsisEvtDISElect *) (VOID *) (pMsg->pu1Msg);
            u4CktIdx = pDISEvt->u4CktIdx;
            i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

            if (i4RetVal == ISIS_FAILURE)
            {
                /* This is possible, since after the event is posted, manager
                 * could have deleted the circuit. Hence do not process the
                 * event beyond this point
                 */
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Scheduling DIS Event - Circuit Does Not Exist - Index [ %u ]\n",
                         u4CktIdx));
                break;
            }

            /* Schedule DIS election since priority of one of the circuits has
             * changed
             */

            IsisAdjDISElect (pContext, pCktRec, pDISEvt->u1CktLevel);
            break;

        case ISIS_EVT_MAN_ADDR_CHANGE:
        case ISIS_EVT_PROT_SUPP_CHANGE:

            pCktRec = pContext->CktTable.pCktRec;

            /* Transmit a Hello on each of the circuit since either Manual Area
             * Address information or Protocol support inofrmation has got
             * modified
             */

            while (pCktRec != NULL)
            {
                if ((pContext->SysActuals.u1SysType != ISIS_LEVEL2)
                    && (pCktRec->u1CktLevel != ISIS_LEVEL2)
                    && (pCktRec->u1OperState == ISIS_UP))
                {
                    /* The circuit is either Level1 or Level12 and hence is
                     * eligible for a Level1 Hello PDU
                     */
                    IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
                }
                if ((pContext->SysActuals.u1SysType != ISIS_LEVEL1)
                    && (pCktRec->u1CktLevel != ISIS_LEVEL1)
                    && (pCktRec->u1OperState == ISIS_UP))
                {
                    /* The circuit is either Level2 or Level12 and hence is
                     * eligible for a Level2 Hello PDU
                     */

                    IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
                }
                pCktRec = pCktRec->pNext;
            }
            break;

        case ISIS_EVT_IP_IF_ADDR_CHANGE:

            pIFEvt = (tIsisEvtIPIFChange *) (VOID *) (pMsg->pu1Msg);
            pCktRec = pContext->CktTable.pCktRec;

            /* A Hello PDU is transmitted on each of the circuit whose
             * corresponding IP Interface Address has changed
             */

            while (pCktRec != NULL)
            {
                if ((pCktRec->u4CktIfIdx == pIFEvt->u4CktIfIdx)
                    && (pCktRec->u4CktIfSubIdx == pIFEvt->u4CktIfSubIdx))
                {
                    if ((pContext->SysActuals.u1SysType != ISIS_LEVEL2)
                        && (pCktRec->u1CktLevel != ISIS_LEVEL2)
                        && (pCktRec->u1OperState == ISIS_UP))
                    {
                        /* The circuit is either Level1 or Level12 and hence is
                         * eligible for a Level1 Hello PDU
                         */

                        IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1,
                                        ISIS_FALSE);
                    }

                    if ((pContext->SysActuals.u1SysType != ISIS_LEVEL1)
                        && (pCktRec->u1CktLevel != ISIS_LEVEL1)
                        && (pCktRec->u1OperState == ISIS_UP))
                    {
                        /* The circuit is either Level2 or Level12 and hence is
                         * eligible for a Level2 Hello PDU
                         */

                        IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2,
                                        ISIS_FALSE);
                    }
                }
                pCktRec = pCktRec->pNext;
            }
            break;

        case ISIS_EVT_IS_DOWN:

            u4SysInstIdx = pMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                break;
            }
            IsisAdjProcISDownEvt (pContext);
            break;

        default:

            ADP_PT ((ISIS_LGST,
                     "ADJ <E> : Received Invalid Adjacency Processing Event [ %u ] \n",
                     u1Event));
            break;
    }

    /* Free the memory allocated for Event  
     */

    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pMsg->pu1Msg);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcEvt ()\n"));
}

/******************************************************************************
 * Function      : IsisInsCktInHashTbl ()
 * Description   : This routine inserts circuit record in hash table on
 *                 verification
 * Input(s)      : ppCktRec - Pointer to the Circuit Record
 *                   u1HashIdx - Hash Index    
 * Output(s)     : ppCktRec - Pointer to the Circuit Record 
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/
PRIVATE VOID
IsisInsCktInHashTbl (tIsisCktEntry ** ppCktRec, UINT1 u1HashIdx)
{
    INT1                i1Status = ISIS_FAILURE;
    tIsisCktEntry      *pTravCktEntry = NULL;

    if (u1HashIdx < ISIS_MAX_BUCKETS)
    {
        if (gIsisCktHashTable.apHashBkts[u1HashIdx] != NULL)
        {
            pTravCktEntry = gIsisCktHashTable.apHashBkts[u1HashIdx];
            do
            {
                if (((*ppCktRec)->u4CktIdx == pTravCktEntry->u4CktIdx) &&
                    ((*ppCktRec)->u4CktIfIdx == pTravCktEntry->u4CktIfIdx) &&
                    ((*ppCktRec)->u4CktIfSubIdx ==
                     pTravCktEntry->u4CktIfSubIdx))
                {
                    i1Status = ISIS_SUCCESS;
                    break;
                }
                pTravCktEntry = pTravCktEntry->pNextHashEntry;
            }
            while (pTravCktEntry != NULL);
        }
    }
    if (i1Status == ISIS_FAILURE)
    {
        ISIS_INS_CKT_IN_HASHTBL ((*ppCktRec), u1HashIdx);
    }

}

/******************************************************************************
 * Function      : IsisDelCktInHashTbl ()
 * Description   : This routine deletes circuit record in hash table on
 *                 verification
 * Input(s)      : ppCktRec - Pointer to the Circuit Record
 *                   u1HashIdx - Hash Index
 * Output(s)     : ppCktRec - Pointer to the Circuit Record
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/
PRIVATE VOID
IsisDelCktInHashTbl (tIsisCktEntry ** ppCktRec, UINT1 u1HashIdx)
{
    tIsisCktEntry      *pTravCktEntry = NULL;
    if (u1HashIdx < ISIS_MAX_BUCKETS)
    {
        if (gIsisCktHashTable.apHashBkts[u1HashIdx] != NULL)
        {
            pTravCktEntry = gIsisCktHashTable.apHashBkts[u1HashIdx];
            do
            {
                if (((*ppCktRec)->u4CktIdx == pTravCktEntry->u4CktIdx) &&
                    ((*ppCktRec)->u4CktIfIdx == pTravCktEntry->u4CktIfIdx) &&
                    ((*ppCktRec)->u4CktIfSubIdx ==
                     pTravCktEntry->u4CktIfSubIdx))
                {
                    ISIS_DEL_CKT_IN_HASHTBL ((*ppCktRec), u1HashIdx);
                    break;
                }
                pTravCktEntry = pTravCktEntry->pNextHashEntry;
            }
            while (pTravCktEntry != NULL);

        }
    }
}

/******************************************************************************
 * Function      : IsisAdjProcISDownEvt ()
 * Description   : This routine de-allocates all the adjacency related
 *                 information from the database. It does not modify or delete
 *                 any information that was configured by the manager
 * Input(s)      : pContext - Pointer to the System Context
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjProcISDownEvt (tIsisSysContext * pContext)
{
    UINT1               u1HashIdx = 0;
    UINT1               au1Hash[8];
    tIsisCktEntry      *pCktRec = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcISDownEvt ()\n"));

    pContext->CktTable.u4NumActCkts = 0;
    pCktRec = pContext->CktTable.pCktRec;

    while (pCktRec != NULL)
    {
        IsisAdjDelCktAdjs (pContext, pCktRec, ISIS_LEVEL12);
        pCktRec->u1OperState = ISIS_DOWN;
        MEMCPY (au1Hash, (UINT1 *) &(pCktRec->u4CktIfIdx), sizeof (UINT4));
        MEMCPY (&au1Hash[4],
                (UINT1 *) &(pCktRec->u4CktIfSubIdx), sizeof (UINT4));
        u1HashIdx =
            IsisUtlGetHashIdx ((UINT1 *) au1Hash, 2 * sizeof (UINT4),
                               gIsisCktHashTable.u1HashSize);
        ISIS_DEL_CKT_IN_HASHTBL (pCktRec, u1HashIdx);

        if (pCktRec->pL1CktInfo != NULL)
        {
            pCktRec->pL1CktInfo->bIsDIS = ISIS_FALSE;
        }
        if (pCktRec->pL2CktInfo != NULL)
        {
            pCktRec->pL2CktInfo->bIsDIS = ISIS_FALSE;
        }
        pCktRec = pCktRec->pNext;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcISDownEvt ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjProcStatInd ()
 * Description   : This routine updates the Adjacency Module Databases
 *                 (Circuit, Adjacency) based on the Node Status or 
 *                 Interface Status received from Control Module
 * Input(s)      : pIsisMsg - Pointer to the received Message
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcStatInd (tIsisMsg * pIsisMsg)
{
    tIsisLLInfo        *pIfInfo = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    UINT4               u4IpAddr = 0;
#ifdef BFD_WANTED
    tIsisAdjEntry      *pTravAdj = NULL;
    UINT4               u4SysInstIdx = 0;
#endif
    UINT2               u2LLIfIndex = 0;
    UINT1               u1Status = ISIS_DOWN;
    UINT1               au1IpAddr[ISIS_MAX_IP_ADDR_LEN];

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcStatInd ()\n"));
    MEMSET (&au1IpAddr, 0, sizeof (au1IpAddr));

    if ((pIsisMsg->u1MsgType == ISIS_MSG_IF_STAT_IND)
        || (pIsisMsg->u1MsgType == ISIS_MSG_SEC_IF_STAT_IND))
    {
        /* Interface Status Indication. Retrieve the Circuit and process the
         * indication with respect to that circuit
         */
        u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
        if (IsisAdjGetCktRecWithIfIdx ((UINT4) u2LLIfIndex, 0, &pCktEntry) ==
            ISIS_FAILURE)
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <E> : Failed To Fetch Circuit Record - Index [ %u ]\n",
                     u2LLIfIndex));
            DETAIL_FAIL (ISIS_CR_MODULE);
            return;
        }
        u1Status = pIsisMsg->u1LenOrStat;

        if (u1Status == ISIS_DOWN)
        {
            /* Interface delete event */
            IsisAdjProcCktStat (pCktEntry, u1Status);
            return;
        }

        pIfInfo = (tIsisLLInfo *) (VOID *) pIsisMsg->pu1Msg;
        pIsisMsg->pu1Msg = NULL;
        if (pIfInfo == NULL)
        {
            ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcStatInd ()\n"));
            return;
        }

        /* Update the SNPA Addresss of the Circuit into the Circuit Record
         */
        MEMCPY (pCktEntry->au1SNPA, pIfInfo->au1SNPA, ISIS_SNPA_ADDR_LEN);

        /* Update the Circuit MTU
         */
        pCktEntry->u4CktMtu = pIfInfo->u4Mtu;

        /* If IP Address is Not NULL, the update the IP Interface Address
         */
        if (u1Status == ISIS_STATE_OFF)
        {
            /* Remove the IPV4/IPV6/Both proto support */
            pCktEntry->u1ProtoSupp &= ~(pIfInfo->u1AddrType);
            ISIS_RESET_MT_ID (pCktEntry->u1IfMTId, pIfInfo->u1AddrType);
        }

        if (u1Status == ISIS_STATE_ON)
        {
            /* Remove the IPV4/IPV6/Both proto support */
            if ((((pIfInfo->u1AddrType & ISIS_ADDR_IPV4) == ISIS_ADDR_IPV4) &&
                 (MEMCMP
                  (au1IpAddr, pIfInfo->au1IpV4Addr,
                   ISIS_MAX_IPV4_ADDR_LEN) != 0)))
            {
                /* If the updated address is a secondary address, then do not 
                 * update the new address in Circuit record */
                MEMCPY (&u4IpAddr, pIfInfo->au1IpV4Addr,
                        ISIS_MAX_IPV4_ADDR_LEN);
                u4IpAddr = OSIX_HTONL (u4IpAddr);
                if (CfaIpIfIsSecondaryAddress ((UINT4) u2LLIfIndex,
                                               u4IpAddr) != CFA_SUCCESS)
                {
                    pCktEntry->u1ProtoSupp |= ISIS_ADDR_IPV4;
                    MEMCPY (pCktEntry->au1IPV4Addr, pIfInfo->au1IpV4Addr,
                            ISIS_MAX_IPV4_ADDR_LEN);
                    ISIS_SET_MT_ID (pCktEntry->u1IfMTId, ISIS_ADDR_IPV4);
                }
            }
            if ((pIfInfo->u1AddrType & ISIS_ADDR_IPV6) == ISIS_ADDR_IPV6)
            {
                pCktEntry->u1ProtoSupp |= ISIS_ADDR_IPV6;

                if (MEMCMP
                    (au1IpAddr, pIfInfo->au1IpV6Addr,
                     ISIS_MAX_IPV6_ADDR_LEN) != 0)
                {
#ifdef BFD_WANTED
                    if (MEMCMP (pCktEntry->au1IPV6Addr,
                                pIfInfo->au1IpV6Addr,
                                ISIS_MAX_IPV6_ADDR_LEN) != 0)
                    {
                        /* Deregister from BFD, before deleting the address from
                         * circuit record */
                        u4SysInstIdx = pCktEntry->pContext->u4SysInstIdx;
                        pTravAdj = pCktEntry->pAdjEntry;
                        while (pTravAdj != NULL)
                        {
                            if (pTravAdj->u1Mt2BfdRegd == ISIS_BFD_TRUE)
                            {
                                IsisDeRegisterWithBfd (u4SysInstIdx, pCktEntry,
                                                       pTravAdj,
                                                       ISIS_IPV6_UNICAST_MT_ID,
                                                       ISIS_TRUE);
                            }
#ifdef ISIS_FT_ENABLED
                            ISIS_FLTR_ADJ_LSU (pCktEntry->pContext,
                                               ISIS_CMD_ADD, pTravAdj);
#endif
                            pTravAdj = pTravAdj->pNext;
                        }
                    }
#endif
                    pCktEntry->u1ProtoSupp |= ISIS_ADDR_IPV6;
                    MEMCPY (pCktEntry->au1IPV6Addr, pIfInfo->au1IpV6Addr,
                            ISIS_MAX_IPV6_ADDR_LEN);
                    ISIS_SET_MT_ID (pCktEntry->u1IfMTId, ISIS_ADDR_IPV6);

                }
            }

#ifdef BFD_WANTED
            /* In case of single topology ISIS,
             * Register with BFD for all the adj entries in the
             * circuit record, for the new protocol support
             * being added in the circuit record.*/
            u4SysInstIdx = pCktEntry->pContext->u4SysInstIdx;
            pTravAdj = pCktEntry->pAdjEntry;
            while (pTravAdj != NULL)
            {
                if ((pCktEntry->pContext->u1IsisMTSupport == ISIS_FALSE) &&
                    (pCktEntry->u1IsisBfdStatus == ISIS_BFD_ENABLE))
                {
                    ISIS_BFD_REGISTER (pCktEntry->pContext, pTravAdj,
                                       pCktEntry);
                }
#ifdef ISIS_FT_ENABLED
                ISIS_FLTR_ADJ_LSU (pCktEntry->pContext, ISIS_CMD_ADD, pTravAdj);
#endif
                pTravAdj = pTravAdj->pNext;
            }
#endif
        }

        IsisAdjProcCktStat (pCktEntry, u1Status);
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
    }

    /* Status Indication Messages does not include any data
     */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcStatInd ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjProcCktStat ()
 * Description   : This routine processes the Circuit Status Indication.
 *                 The circuit status is marked UP or DOWN based on the received
 *                 status. It also posts an Event to the Control module for
 *                 further processing
 * Input(s)      : pCktEntry - Pointer to Circuit Record
 *                 u1Status  - Status of the Circuit, UP or DOWN
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcCktStat (tIsisCktEntry * pCktEntry, UINT1 u1Status)
{
    tIsisEvtCktChange  *pCktEvt = NULL;
#ifdef BFD_WANTED
    tIsisAdjEntry      *pTravAdj = NULL;
    UINT4               u4SysInstance = 0;
#endif
    UINT1               u1Event = ISIS_CKT_NO_EVT;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcCktStat ()\n"));

    /* I/f status is either the status reported by the LL or configured
     * by the manager. Lower Layers will have higher preference than the
     * manager, which means if manager configures the status to DOWN at time 'T'
     * and if LL indictaes the status as UP at time 'T + 1', then the status
     * indication received from LL will override the managers configuration
     */

    pCktEntry->u1CktIfStatus = u1Status;

    if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
    {

        if ((pCktEntry->bCktAdminState == ISIS_STATE_ON)
            && (pCktEntry->pContext->u1OperState == ISIS_UP))
        {
            if (pCktEntry->u1ProtoSupp == 0)
            {
                pCktEntry->u1OperState = (UINT1) ISIS_DOWN;
                u1Event = (UINT1) ISIS_CKT_DOWN;
            }
            else if (u1Status == ISIS_STATE_ON)
            {
                pCktEntry->u1OperState = (UINT1) ISIS_UP;
                u1Event = (UINT1) ISIS_CKT_UP;
            }
        }

        if (u1Status == ISIS_DOWN)
        {
            u1Event = ISIS_CKT_DESTROY;
        }

        if (u1Event != ISIS_CKT_NO_EVT)
        {
            pCktEvt = (tIsisEvtCktChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));

            if (pCktEvt != NULL)
            {
                pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                pCktEvt->u1Status = u1Event;
                pCktEvt->u1CktType = pCktEntry->u1CktType;
                pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;
                IsisUtlSendEvent (pCktEntry->pContext, (UINT1 *) pCktEvt,
                                  sizeof (tIsisEvtCktChange));
            }
            else
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Circuit Change Event\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
        }

#ifdef BFD_WANTED
        if (u1Event == ISIS_CKT_DESTROY)
        {
            /* Deregister from BFD, if regd previously */
            u4SysInstance = pCktEntry->pContext->u4SysInstIdx;
            pTravAdj = pCktEntry->pAdjEntry;
            while (pTravAdj != NULL)
            {
                if (pTravAdj->u1Mt0BfdRegd == ISIS_BFD_TRUE)
                {
                    IsisDeRegisterWithBfd (u4SysInstance, pCktEntry, pTravAdj,
                                           ISIS_IPV4_UNICAST_MT_ID, ISIS_TRUE);
#ifdef ISIS_FT_ENABLED
                    ISIS_FLTR_ADJ_LSU (pCktEntry->pContext, ISIS_CMD_ADD,
                                       pTravAdj);
#endif
                }
                if (pTravAdj->u1Mt2BfdRegd == ISIS_BFD_TRUE)
                {
                    IsisDeRegisterWithBfd (u4SysInstance, pCktEntry, pTravAdj,
                                           ISIS_IPV6_UNICAST_MT_ID, ISIS_TRUE);
#ifdef ISIS_FT_ENABLED
                    ISIS_FLTR_ADJ_LSU (pCktEntry->pContext, ISIS_CMD_ADD,
                                       pTravAdj);
#endif
                }
                pTravAdj = pTravAdj->pNext;
            }
        }
#endif

#ifdef ISIS_FT_ENABLED
        if (u1Event == ISIS_CKT_DESTROY)
        {
        ISIS_FLTR_CKT_LSU (pCktEntry->pContext, ISIS_CMD_DELETE, pCktEntry,
                               pCktEntry->u1CktExistState)}
        else
        {
        ISIS_FLTR_CKT_LSU (pCktEntry->pContext, ISIS_CMD_ADD, pCktEntry,
                               pCktEntry->u1CktExistState)}
#endif
    }
    else
    {
        /* Circuit is still not active, hence do not 
         * generate any events
         */

        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Inactive Circuit, Event Ignored - [%s]\n",
                 ISIS_GET_ISIS_ADJ_STATUS (u1Status)));
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcCktStat ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjActivateCkt ()
 * Description   : This routine activates the given circuit for the first time.
 *                 It transmits a ISH PDU in the case of a P2P circuit and a
 *                 IIH PDU in the case of BC circuits. It activates the
 *                 appropriate timers as required
 * Input(s)      : pContext  - Pointer to System Context
 *                 pCktRec   - Circuit to be activated
 *                 u1State   - State of the Circuit Action 
 *                                 - to be Restarted or
 *                                 - to be made up for the first time.
 * Output(s)     : None
 * Globals       : Referred to gIsisExtInfo, not Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisAdjActivateCkt (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                    UINT1 u1State)
{
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    UINT1               au1Tmp[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN] = { 0 };

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjActivateCkt ()\n"));

    pAdjRec = pCktRec->pAdjEntry;

    pCktRec->u4CktUpTime = ISIS_GET_CURR_TICK (pContext);

    /* Activate only if circuit is Operationally UP
     */

    if ((pCktRec->u1OperState == ISIS_UP)
        && (pCktRec->bCktExtDomain == ISIS_FALSE))
    {
        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);

            if ((u1State == ISIS_CKT_UP)
                || ((u1State == ISIS_CKT_RESTART)
                    && (pCktRec->pAdjEntry == NULL)))
            {
                /* Circuit coming up for the first time. An ISH PDU is 
                 * transmitted on a P2P circuit the very first time the 
                 * circuit comes up
                 */
                IsisAdjTxISH (pContext, pCktRec);

                /* Starting the ISH timer and incrementing the transmission
                 * Count */
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Starting to Send Hello PDU - Circuit [ %u ], Level [ %s ]\n",
                         pCktRec->u4CktIdx,
                         ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel)));
                IsisTmrRestartECTimer (pContext, ISIS_ECT_P2P_ISH,
                                       pCktRec->u4CktIdx,
                                       pCktLevel->HelloTimeInt,
                                       &pCktLevel->u1HelloTmrIdx);
                pCktRec->u1ISHTxCnt++;
            }
            else if ((u1State == ISIS_CKT_RESTART)
                     && (pCktRec->pAdjEntry != NULL))
            {
                IsisAdjTxHello (pContext, pCktRec, pCktRec->u1CktLevel,
                                ISIS_FALSE);

                if (pCktLevel->u4NumAdjs >= 1)
                {
                    if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
                    {
                        IsisUtlSetBPat (&(pContext->CktTable.pu1L1CktMask),
                                        pCktRec->u4CktIdx);

                        IsisTmrStartECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                             pCktRec->u4CktIdx, 1,
                                             &pCktLevel->u1LSPTxTmrIdx);
                        if (pCktRec->pL1CktInfo->u1HelloTmrIdx != 0)
                        {
                            IsisTmrRestartECTimer (pCktRec->pContext,
                                                   ISIS_ECT_P2P_HELLO,
                                                   pCktRec->u4CktIdx,
                                                   pCktRec->pL1CktInfo->
                                                   HelloTimeInt,
                                                   &pCktRec->pL1CktInfo->
                                                   u1HelloTmrIdx);
                        }
                        else
                        {
                            IsisTmrStartECTimer (pCktRec->pContext,
                                                 ISIS_ECT_P2P_HELLO,
                                                 pCktRec->u4CktIdx,
                                                 pCktRec->pL1CktInfo->
                                                 HelloTimeInt,
                                                 &pCktRec->pL1CktInfo->
                                                 u1HelloTmrIdx);
                        }
                        pCktRec->pL1CktInfo->u1CSNPSent = ISIS_ZERO;
                        pCktRec->pL1CktInfo->u1SetSRM = ISIS_TRUE;
                    }

                    if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
                    {
                        IsisUtlSetBPat (&(pContext->CktTable.pu1L2CktMask),
                                        pCktRec->u4CktIdx);

                        IsisTmrStartECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                             pCktRec->u4CktIdx, 1,
                                             &pCktLevel->u1LSPTxTmrIdx);
                        if (pCktRec->pL2CktInfo->u1HelloTmrIdx != 0)
                        {
                            IsisTmrRestartECTimer (pCktRec->pContext,
                                                   ISIS_ECT_P2P_HELLO,
                                                   pCktRec->u4CktIdx,
                                                   pCktRec->pL2CktInfo->
                                                   HelloTimeInt,
                                                   &pCktRec->pL2CktInfo->
                                                   u1HelloTmrIdx);
                        }
                        else
                        {
                            IsisTmrStartECTimer (pCktRec->pContext,
                                                 ISIS_ECT_P2P_HELLO,
                                                 pCktRec->u4CktIdx,
                                                 pCktRec->pL2CktInfo->
                                                 HelloTimeInt,
                                                 &pCktRec->pL2CktInfo->
                                                 u1HelloTmrIdx);
                        }
                        pCktRec->pL2CktInfo->u1CSNPSent = ISIS_ZERO;
                        pCktRec->pL2CktInfo->u1SetSRM = ISIS_TRUE;
                    }
                    IsisUpdEnqueueP2PCktChgEvt (pCktRec->pContext, ISIS_CKT_UP,
                                                pCktRec->u4CktIdx);
                }
            }
        }
        else if (pCktRec->u1CktType == ISIS_BC_CKT)
        {
            /* Level1 LAN Hello is transmitted by L1 as well as L12 System
             * having circuit configured as L1 or L12
             */

            if ((pCktRec->u1CktLevel != ISIS_LEVEL2)
                && (pContext->SysActuals.u1SysType != ISIS_LEVEL2))
            {
                pCktLevel = pCktRec->pL1CktInfo;

                if ((u1State != ISIS_CKT_RESTART)
                    || (pCktRec->pAdjEntry == NULL))
                {
                    /* Initialise the Circuit LAN ID with the System ID and the
                     * Local Circuit ID
                     * Refer 8.4.1 a) of ISO 10589
                     */

                    if (MEMCMP (pCktLevel->au1CktLanDISID, au1Tmp,
                                (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0)
                    {
                        MEMCPY (pCktLevel->au1CktLanDISID,
                                pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
                        pCktLevel->au1CktLanDISID[ISIS_SYS_ID_LEN] =
                            pCktRec->u1CktLocalID;
                    }

                    /* Starting the Hello Timer after sending Hello
                     */
                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Starting to Send Hello PDU - Circuit [ %u ], Level [ %s ]\n",
                             pCktRec->u4CktIdx,
                             ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel)));
                    IsisTmrRestartECTimer (pContext, ISIS_ECT_L1LANHELLO,
                                           pCktRec->u4CktIdx,
                                           pCktLevel->HelloTimeInt,
                                           &(pCktLevel->u1HelloTmrIdx));
                }

                if ((u1State == ISIS_CKT_RESTART)
                    && (pCktLevel->u4NumAdjs >= 1))
                {
                    IsisUtlSetBPat (&(pContext->CktTable.pu1L1CktMask),
                                    pCktRec->u4CktIdx);

                    IsisTmrStartECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                         pCktRec->u4CktIdx, 1,
                                         &pCktLevel->u1LSPTxTmrIdx);
                }
                IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
            }

            /* Level2 LAN Hello is transmitted by L2 as well as L12 System
             * having circuit configured as L2 or L12
             */

            if ((pCktRec->u1CktLevel != ISIS_LEVEL1)
                && (pContext->SysActuals.u1SysType != ISIS_LEVEL1))
            {
                pCktLevel = pCktRec->pL2CktInfo;

                if ((u1State != ISIS_CKT_RESTART)
                    || (pCktRec->pAdjEntry == NULL))
                {
                    /* Initialise the Circuit LAN ID with the System ID and the
                     * Local Circuit ID
                     * Refer 8.4.1 a) of ISO 10589
                     */

                    if (MEMCMP (pCktLevel->au1CktLanDISID, au1Tmp,
                                (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0)
                    {
                        MEMCPY (pCktLevel->au1CktLanDISID,
                                pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
                        pCktLevel->au1CktLanDISID[ISIS_SYS_ID_LEN] =
                            pCktRec->u1CktLocalID;
                    }

                    /* Starting the Hello Timer after sending Hello
                     */

                    IsisTmrRestartECTimer (pContext, ISIS_ECT_L2LANHELLO,
                                           pCktRec->u4CktIdx,
                                           pCktLevel->HelloTimeInt,
                                           &(pCktLevel->u1HelloTmrIdx));
                }

                if ((u1State == ISIS_CKT_RESTART)
                    && (pCktLevel->u4NumAdjs >= 1))
                {
                    IsisUtlSetBPat (&(pContext->CktTable.pu1L2CktMask),
                                    pCktRec->u4CktIdx);

                    IsisTmrStartECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                         pCktRec->u4CktIdx, 1,
                                         &pCktLevel->u1LSPTxTmrIdx);
                }
                IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
            }
        }
    }
    else
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Circuit Is Operationally Down - Cannot Activate - Index [ %u ]\n",
                 pCktRec->u4CktIdx));
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjActivateCkt ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjProcISHTimeOut ()
 * Description   : This routines extracts the circuit indices from the
 *                 'pTmrData' and transmits an ISH PDU over each of the circuit
 *                 associated with the indices.
 * Input(s)      : pContext   - Pointer to System Context
 *                 pTmrData   - Pointer to Timer Block associated with the timer
 *                              that has expired
 * Output(s)     : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjProcISHTimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrData)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TmrIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    UINT1              *pu1info = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcISHTimeOut ()\n"));

    /* Retrieve each bit, that is set, in order. The bit position will give the
     * Index value which will be used to retrieve the Circuit Record
     */
    pu1info = (UINT1 *) pTmrData->pInfo;
    while (IsisUtlGetNextIdFromBPat (&pu1info, &u4CktIdx) == ISIS_SUCCESS)
    {
        pTmrData->pInfo = (VOID *) pu1info;

        i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

        if (i4RetVal == ISIS_FAILURE)
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : Circuit Does Not Exist - Index [ %u ]\n",
                     u4CktIdx));
            continue;
        }

        pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);

        /* ISH is transmitted only 
         * -- ISIS_MAX_ISH_TX_COUNT times
         * -- if the adjacency state is DOWN i.e. once the local system receives
         *    either and ISH or an IIH from the Peer, the adjacency is moved to
         *    either Initialising or UP.
         * Otherwise ISH is not transmitted to the Adjacent System. The 
         * adjacency will be considered DOWN. 
         */
        if (pCktLevel == NULL)
        {
            continue;
        }
        if ((pCktRec->pAdjEntry == NULL)
            || (pCktRec->pAdjEntry->u1AdjState == ISIS_ADJ_DOWN))
        {
            pCktRec->u1ISHTxCnt++;

            IsisAdjTxISH (pContext, pCktRec);

            IsisTmrStartECTimer (pContext, ISIS_ECT_P2P_ISH, pCktRec->u4CktIdx,
                                 pCktLevel->HelloTimeInt, &u1TmrIdx);
        }
    }

    /* IsisUtlGetNextIdFromBPat () would have freed the memory pTmrData->pInfo
     */

    pTmrData->pInfo = NULL;
    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrData);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcISHTimeOut ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjTxISH ()
 * Description   : This routine encodes an ISH PDU and transmits the encoded PDU
 *                 on the specified P2P Circuit
 * Input(s)      : pContext  - Pointer to System Context
 *                 pCktRec   - Pointer to Circuit Record 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisAdjTxISH (tIsisSysContext * pContext, tIsisCktEntry * pCktRec)
{
    UINT1              *pu1PDU = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjTxISH ()\n"));

#ifdef ISIS_FT_ENABLED
    if (((pCktRec->bCktPassiveCkt) == ISIS_TRUE)
        || (ISIS_EXT_IS_FT_STATE () != ISIS_FT_ACTIVE))
#else
    if ((pCktRec->bCktPassiveCkt) == ISIS_TRUE)
#endif
    {
        return;
    }

    pu1PDU = (UINT1 *)
        ISIS_MEM_ALLOC (ISIS_BUF_HLPD,
                        pContext->SysActuals.u4SysOrigL1LSPBufSize);

    if (pu1PDU != NULL)
    {
        /* Encode an ISH PDU */

        IsisAdjEncodeISH (pContext, pCktRec, pu1PDU);
        IsisTrfrTxData (pCktRec, pu1PDU, *(UINT1 *) (pu1PDU + 1),
                        pCktRec->u1CktLevel, ISIS_FREE_BUF, ISIS_TRUE);
        ISIS_INCR_TXED_ISH (pCktRec);
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : ISH\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_HLPD);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjTxISH ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjProcHelloTimeOut ()
 * Description   : This routines extracts the circuit indices from the
 *                 'pTmrData' and transmits a Hello PDU over each of the circuit
 *                 associated with the indices.
 * Input(s)      : pContext   - Pointer to System Context
 *                 pTmrData   - Pointer to Timer Block associated with the timer
 *                              that has expired
 *                 u1Level    - IS Level which decides what type of Hello is
 *                              transmitted             
 * Output(s)     : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjProcHelloTimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrData,
                         UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TmrType = ISIS_ECT_P2P_HELLO;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    UINT1              *pu1info = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcHelloTimeOut ()\n"));

    /* Retrieve each bit, that is set, in order. The bit position will give the
     * Index value which will be used to retrieve the Circuit Record
     */
    pu1info = (UINT1 *) pTmrData->pInfo;
    while (IsisUtlGetNextIdFromBPat (&pu1info, &u4CktIdx) == ISIS_SUCCESS)
    {
        pTmrData->pInfo = (VOID *) pu1info;
        i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

        if (i4RetVal == ISIS_FAILURE)
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : Circuit Does Not Exist - Index [ %u ]\n",
                     u4CktIdx));
            continue;
        }
        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
            u1TmrType = ISIS_ECT_P2P_HELLO;
        }
        else if (pCktRec->u1CktType == ISIS_BC_CKT)
        {
            if (u1Level == ISIS_LEVEL1)
            {
                pCktLevel = pCktRec->pL1CktInfo;
                u1TmrType = ISIS_ECT_L1LANHELLO;
            }
            else if (u1Level == ISIS_LEVEL2)
            {
                pCktLevel = pCktRec->pL2CktInfo;
                u1TmrType = ISIS_ECT_L2LANHELLO;
            }
            else
            {
                break;            /*klocwork */
            }

        }
        else
        {
            break;                /*klocwork */
        }
        if (pCktLevel == NULL)
        {
            continue;            /*invalid level */
        }

        if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER)
            && (pCktRec->u1IsisGRRestartHelloTxCount <=
                pContext->u1IsisGRMaxT1RetryCount))
        {
            IsisTmrStartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                                 ISIS_GET_HELLO_INT (pCktLevel),
                                 &(pCktLevel->u1HelloTmrIdx));
        }
        else if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            if (pCktRec->pAdjEntry == NULL)
            {
                IsisAdjTxISH (pContext, pCktRec);
                IsisTmrStartECTimer (pContext, ISIS_ECT_P2P_ISH,
                                     pCktRec->u4CktIdx, pCktLevel->HelloTimeInt,
                                     &pCktLevel->u1HelloTmrIdx);
            }
            else
            {
                IsisAdjTxHello (pContext, pCktRec, u1Level, ISIS_FALSE);
                IsisTmrStartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                                     ISIS_GET_HELLO_INT (pCktLevel),
                                     &(pCktLevel->u1HelloTmrIdx));
            }
        }
        else
        {
            IsisAdjTxHello (pContext, pCktRec, u1Level, ISIS_FALSE);
            IsisTmrStartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                                 ISIS_GET_HELLO_INT (pCktLevel),
                                 &(pCktLevel->u1HelloTmrIdx));
        }
    }

    /* IsisUtlGetNextIdFromBPat () would have freed the memory pTmrData->pInfo
     */

    pTmrData->pInfo = NULL;
    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrData);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcHelloTimeOut ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjTxHello ()
 * Description   : This routine builds a Hello PDU and transmits the same over
 *                 the specified circuit. This routine pads the IIH PDU to 
 *                 Maximum Buffer size allowed on the Circuit, either if this
 *                 is the first P2P IIH PDU or if Small Hellos are disabled.
 * Input(s)      : pContext  - Pointer to System Context
 *                 pCktRec   - Pointer to Circuit Record
 *                 u1Level   - Level of the circuit (Level1 or Level2)
 *                 bFirstIIH - Flag indicating whether this PDU is the 
 *                             first IIH going to be transmitted over the 
 *                             given Circuit. Applicable only for P2P circuits
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjTxHello (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                UINT1 u1Level, tBool bFirstIIH)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PduType = ISIS_P2P_IIH_PDU;
    UINT1               u1CktType = ISIS_P2P_CKT;
    UINT1               u1PadFlag = ISIS_FALSE;
    UINT1              *pu1PDU = NULL;
    UINT2               u2Offset = 0;
    UINT4               u4BufSize = 0;
    UINT2               u2MD5DigestOffset = 0;
    UINT1               au1CalcDigest[16];
    unUtilAlgo          UtilAlgo;
    tIsisCktLevel      *pCktLevel = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjTxHello ()\n"));

    u1CktType = pCktRec->u1CktType;

    MEMSET (au1CalcDigest, 0, sizeof (au1CalcDigest));
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    /* Hello packet's cant be transmitted on a Passive Interface */

#ifdef ISIS_FT_ENABLED
    if (((pCktRec->bCktPassiveCkt) == ISIS_TRUE)
        || (ISIS_EXT_IS_FT_STATE () != ISIS_FT_ACTIVE))
#else
    if ((pCktRec->bCktPassiveCkt) == ISIS_TRUE)
#endif
    {
        return;
    }

    if (u1CktType == ISIS_BC_CKT)
    {
        u1PduType = (UINT1) ((u1Level == ISIS_LEVEL1) ? ISIS_L1LAN_IIH_PDU :
                             ISIS_L2LAN_IIH_PDU);
        pCktLevel = ((u1Level == ISIS_LEVEL1) ? pCktRec->pL1CktInfo :
                     pCktRec->pL2CktInfo);
        u4BufSize = ((u1Level == ISIS_LEVEL1) ?
                     (UINT2) pContext->SysActuals.u4SysOrigL1LSPBufSize :
                     (UINT2) pContext->SysActuals.u4SysOrigL2LSPBufSize);

        /* Checking whether LAN Hello needs to be padded
         */

        if (pCktRec->bCktSmallHello == ISIS_STATE_OFF)
        {
            u1PadFlag = ISIS_TRUE;
        }
    }
    else if (u1CktType == ISIS_P2P_CKT)
    {
        u1PduType = ISIS_P2P_IIH_PDU;
        pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
        u4BufSize = ISIS_MAX (pContext->SysActuals.u4SysOrigL1LSPBufSize,
                              pContext->SysActuals.u4SysOrigL2LSPBufSize);
    }
    else
    {
        return;
    }

    pu1PDU = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_HLPD, u4BufSize);

    if ((pu1PDU != NULL) && (pCktLevel != NULL))
    {
        i4RetVal = IsisAdjEncodeHelloPdu (pCktRec, pCktLevel, u1PduType,
                                          pu1PDU, (UINT2) u4BufSize, &u2Offset);

        /* If Hello PDU encoding fials then release the buffer and get back 
         * without transmitting anything.
         */

        if (i4RetVal == ISIS_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
            WARNING ((ISIS_LGST,
                      "ADJ <W> : Hello PDU Encoding Failed - Circuit [%u], Level [%u], Actual size [%u], Max Size [%u]\n",
                      pCktRec->u4CktIdx, u1Level, u2Offset, u4BufSize));
            DETAIL_FAIL (ISIS_CR_MODULE);
            return;
        }
        if ((pCktLevel->CktTxPasswd.u1Len != 0)
            && (pCktLevel->u1CircLevelAuthType == ISIS_AUTH_PASSWD))
        {
            i4RetVal = IsisAdjAddAuthToPDU (pCktLevel, pu1PDU, u4BufSize,
                                            &u2Offset);
            if (i4RetVal == ISIS_FAILURE)
            {
                WARNING ((ISIS_LGST,
                          "ADJ <W> : IIH Authentication Encoding Error - Circuit [%u], Level [%u]\n",
                          pCktRec->u4CktIdx, u1Level));
                ADP_EE ((ISIS_LGST,
                         "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
                return;
            }
        }
        else if ((pCktLevel->CktTxPasswd.u1Len != 0)
                 && (pCktLevel->u1CircLevelAuthType == ISIS_AUTH_MD5))
        {
            ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, ISIS_AUTH_INFO_TLV);
            u2Offset = (UINT2) (u2Offset + 1);
            ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, ISIS_MD5_DIGEST_LEN + 1);
            u2Offset = (UINT2) (u2Offset + 1);
            ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, ISIS_MD5_PASS);
            u2Offset = (UINT2) (u2Offset + 1);
            u2MD5DigestOffset = u2Offset;
            u2Offset = (UINT2) (u2Offset + ISIS_MD5_DIGEST_LEN);
        }

        /* If the IIH PDU is the first one to be out on this circuit, or if
         * Small Hello's are disabled then pad the remaining length available 
         * in the PDU with zeroes. 
         */

        /* NOTE: Comparing u2Offset with (u4BufSize - 2), since if the remaining
         * bytes to be padded is less than 2, we cannot even include the Code
         * and Length fields for the Padding TLV
         */

        if (((bFirstIIH == ISIS_TRUE) || (u1PadFlag == ISIS_TRUE))
            && (u2Offset <= (u4BufSize - 2)))
        {
            MEMSET ((pu1PDU + u2Offset), 0, (u4BufSize - u2Offset));
            while (u2Offset <= (u4BufSize - 2))
            {
                /* Assigning Code
                 */

                ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, ISIS_PADDING_TLV);
                u2Offset++;

                /* TLV length cannot exceed 255 bytes
                 */

                if ((u4BufSize - u2Offset) > 0xFF)
                {
                    /* Assign the length as 255. Remaiing bytes will be padded
                     * with one more TLV
                     */

                    ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, 0xFF);
                    u2Offset++;    /* For Length */

                    u2Offset = (UINT2) (u2Offset + 0xFF);    /* TLV Length */
                }
                else
                {
                    ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset,
                                        (u4BufSize - u2Offset - 1));
                    u2Offset++;    /* For Length */

                    u2Offset = (UINT2) (u2Offset + (u4BufSize - u2Offset));
                    break;
                }
            }

            /* Filling PDU Length in Hello Specific Header
             */
            ISIS_ASSIGN_2_BYTES (pu1PDU,
                                 (sizeof (tIsisComHdr) + ISIS_SYS_ID_LEN + 3),
                                 u2Offset);
        }
        else
        {
            /* Filling PDU Length in Hello Specific Header
             */
            ISIS_ASSIGN_2_BYTES (pu1PDU,
                                 (sizeof (tIsisComHdr) + ISIS_SYS_ID_LEN + 3),
                                 u2Offset);
        }

        /* Hello's are transmitted only on non-passive P2P or Broadcast circuits
         */

        if (((u1CktType == ISIS_BC_CKT) || (u1CktType == ISIS_P2P_CKT))
            && (pCktRec->bCktPassiveCkt == ISIS_FALSE))
        {
            /*Starting T1 Timer for ISIS Grace Hello */
            if ((pContext->u1IsisGRRestartStatus == ISIS_STARTING_ROUTER)
                || (pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER))
            {
                if (IsisGRHelloProcess (pContext, pCktRec, u1Level) ==
                    ISIS_FAILURE)
                {
                    ISIS_MEM_FREE (ISIS_BUF_HLPD, (UINT1 *) pu1PDU);
                    WARNING ((ISIS_LGST,
                              "ADJ <W> : Hello PDU Encoding Failed - Circuit [%u], Level [%u], Actual size [%u], Max Size [%u]\n",
                              pCktRec->u4CktIdx, u1Level, u2Offset, u4BufSize));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    return;
                }
            }
            if ((pCktLevel->CktTxPasswd.u1Len != 0)
                && (pCktLevel->u1CircLevelAuthType == ISIS_AUTH_MD5))
            {
                UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr =
                    (unsigned char *) pu1PDU;
                UtilAlgo.UtilHmacAlgo.i4HmacPktLength = u2Offset;
                UtilAlgo.UtilHmacAlgo.pu1HmacKey =
                    pCktLevel->CktTxPasswd.au1Password;
                UtilAlgo.UtilHmacAlgo.u4HmacKeyLen =
                    pCktLevel->CktTxPasswd.u1Len;
                UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest =
                    (unsigned char *) &au1CalcDigest;

                UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);

                MEMCPY ((pu1PDU + u2MD5DigestOffset), au1CalcDigest,
                        ISIS_MD5_DIGEST_LEN);
            }

            IsisTrfrTxData (pCktRec, pu1PDU, u2Offset, u1Level, ISIS_FREE_BUF,
                            ISIS_TRUE);
            ISIS_INCR_SENT_HELLO (pCktLevel);
        }
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Hello PDU\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        ISIS_MEM_FREE (ISIS_BUF_HLPD, pu1PDU);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_HLPD);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjTxHello ()\n"));
}

/******************************************************************************
 * Function      : IsisAdjProcHoldingTimeOut ()
 * Description   : This routines extracts the Direction indices from the
 *                 'pTmrData' and deletes the adjacencies associated with the 
 *                 indices.
 * Input(s)      : pContext   - Pointer to System Context
 *                 pTmrData   - Pointer to Timer Block associated with the timer
 *                              that has expired
 * Output(s)     : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjProcHoldingTimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrBlk)
{
    UINT4               u4DirIdx = 0;
    tIsisAdjEntry      *pAdjRec = NULL;
    UINT1              *pu1info = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));

    pu1info = (UINT1 *) pTmrBlk->pInfo;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcHoldingTimeOut ()\n"));

    /* Retrieve each bit, that is set, in order. The bit position will give the
     * Index value which will be used to retrieve the Circuit Record
     */

    while (IsisUtlGetNextIdFromBPat (&pu1info, &u4DirIdx) == ISIS_SUCCESS)

    {
        pTmrBlk->pInfo = (VOID *) pu1info;

        pAdjRec = (tIsisAdjEntry *) IsisAdjGetAdjRecFromDirIdx (pContext,
                                                                u4DirIdx);
        if (pAdjRec != NULL)
        {
            if (pAdjRec->u1IsisGRHelperStatus == ISIS_GR_HELPING)
            {
                IsisGrExitHelper (pContext, pAdjRec, ISIS_GR_HELPER_TIMEDOUT);
            }
            ISIS_FORM_IPV4_ADDR (pAdjRec->pCktRec->au1IPV4Addr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);

            if (pAdjRec->pCktRec->u1CktType == ISIS_P2P_CKT)
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Holding Timer Expired - Circuit [ %u ], Adj [ %u ], "
                         "Level [ %s ], Type [ %s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         pAdjRec->pCktRec->u4CktIdx, pAdjRec->u4AdjIdx,
                         ISIS_GET_SYS_TYPE_STR (pAdjRec->pCktRec->u1CktLevel),
                         ISIS_GET_CKT_TYP_STR (pAdjRec->pCktRec->u1CktType),
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pAdjRec->pCktRec->au1IPV6Addr))));

                IsisP2PDelAdj (pAdjRec->pCktRec);
            }
            else
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Holding Timer Expired - Circuit [ %u ], Adj [ %u ], "
                         "Level [ %s ], Type [ %s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         pAdjRec->pCktRec->u4CktIdx, pAdjRec->u4AdjIdx,
                         ISIS_GET_SYS_TYPE_STR (pAdjRec->pCktRec->u1CktLevel),
                         ISIS_GET_CKT_TYP_STR (pAdjRec->pCktRec->u1CktType),
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pAdjRec->pCktRec->au1IPV6Addr))));

                IsisAdjDelAdj (pContext, pAdjRec->pCktRec, pAdjRec, ISIS_TRUE,
                               &u4DirIdx);
            }
        }
        else
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : Direction Does Not Exist - Index [ %u ]\n",
                     u4DirIdx));
        }
    }

    /* IsisUtlGetNextIdFromBPat () would have freed the memory pTmrData->pInfo
     */

    pTmrBlk->pInfo = NULL;
    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrBlk);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcHoldingTimeOut ()\n"));
}

/*******************************************************************************
 * Function    : IsisAdjProcT1TimeOut ()
 * Description : This routine processes each of the LSPs included in the
 *               pTmrInfo and sets the RLT to '0' and length of the PDU to 
 *               header length. It deletes the LSP from the Remaining Life Time
 *               Timer list and inserts the same into the Zero Age Timer list
 * Input (s)   : pContext - Pointer to System context
 *             : pTmrInfo - Pointer to Timer Info
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisAdjProcT1TimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrData)
{

    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1              *pu1info = NULL;
    UINT1               u1Level = 0;
    tGrSemInfo          GrSemInfo;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcT1TimeOut ()\n"));

    /* Retrieve each bit, that is set, in order. The bit position will give the
     * Index value which will be used to retrieve the Circuit Record
     */
    pu1info = (UINT1 *) pTmrData->pInfo;
    pTmrData->pInfo = NULL;
    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrData);

    while (IsisUtlGetNextIdFromBPat (&pu1info, &u4CktIdx) == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

        if (i4RetVal == ISIS_FAILURE)
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : Circuit Does Not Exist - Index [ %u ]\n",
                     u4CktIdx));
            continue;
        }
        u1Level = pCktRec->u1CktLevel;

        MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
        if ((pCktRec->u1IsisGRRestartHelloTxCount) <
            (pContext->u1IsisGRMaxT1RetryCount))
        {
            GrSemInfo.pContext = pContext;
            GrSemInfo.pCktRec = pCktRec;
            GrSemInfo.u1Level = u1Level;

            IsisGrRunRestartSem (ISIS_GR_EVT_T1_EXP, ISIS_GR_ADJ_RESTART,
                                 &GrSemInfo);
        }
        else if (pCktRec->u1IsisGRRestartHelloTxCount ==
                 pContext->u1IsisGRMaxT1RetryCount)
        {

            GrSemInfo.pContext = pContext;
            GrSemInfo.pCktRec = pCktRec;
            GrSemInfo.u1Level = u1Level;

            IsisGrRunRestartSem (ISIS_GR_EVT_N_T1_EXP, ISIS_GR_ADJ_RESTART,
                                 &GrSemInfo);
        }

    }                            /*while end */

    if (pCktRec != NULL)
    {
        if ((TMO_SLL_Count (&(pContext->SysL1Info.AckLst)) == 0) &&
            (TMO_SLL_Count (&(pContext->SysL2Info.AckLst)) == 0)
            && (pContext->u1IsisGRRestarterState == ISIS_GR_SHUTDOWN) &&
            (pCktRec->u1IsisGRRestartHelloTxCount ==
             pContext->u1IsisGRMaxT1RetryCount))
        {
            pContext->u1IsisGRShutState = ISIS_GR_SHUT;
            IsisGrCheckAndShutdown ();
        }
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcT1TimeOut ()\n"));
}
