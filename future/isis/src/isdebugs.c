/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isdebugs.c,v 1.15 2017/09/11 13:44:08 siva Exp $
 *
 * Description: This file contains all the packet and table dumping routines
 *              which aid in debugging.
 *
 *****************************************************************************/

#include "isincl.h"
#include "isextn.h"

/*----------------------------------------------------------------------------*/

/* Static array declarations used by the Debug Module. These objects cannot be
 * read or modified by any other module
 */

#if (DBG_LOG == ISIS_LOG_ENABLE)
/*klocwork*/
#define ISIS_DEBUG_MAX_MET_TYPE    2
#define ISIS_DEBUG_MAX_MET_SUPP    2
#define ISIS_DEBUG_MAX_ADJ_USAGE    5
#define ISIS_DEBUG_MAX_ADJ_STATE    5
#define ISIS_DEBUG_MAX_CKT_TYPE        6
#define ISIS_DEBUG_MAX_CKT_LEVEL    4
#define ISIS_DEBUG_MAX_SYS_TYPE        6
#define ISIS_DEBUG_MAX_OPER_STATE    2
#define ISIS_DEBUG_MAX_SUMM_ADMIN    5

PRIVATE CHAR       *api1MetType[2] = {
    "INTER",
    "EXTER"
};

PRIVATE CHAR       *api1MetSupp[2] = {
    "SUPP",
    "NOT_SUPP"
};

PRIVATE CHAR       *api1AdjUsage[5] = {
    "OTHER",
    "LEVEL1",
    "LEVEL2",
    "LEVEL12",
    "UNDEFINED"
};

PRIVATE CHAR       *api1AdjState[5] = {
    "UP",
    "INIT",
    "DOWN",
    "FAILED",
    "OTHER"
};

PRIVATE CHAR       *api1CktType[6] = {
    "OTHER",
    "BCAST",
    "P2P",
    "ST_IN",
    "ST_OUT",
    "DA"
};

PRIVATE CHAR       *api1CktLevel[4] = {
    "OTHER",
    "LEVEL1",
    "LEVEL2",
    "LEVEL12"
};

PRIVATE CHAR       *api1SysType[6] = {
    "OTHER",
    "LEVEL1",
    "LEVEL2",
    "LEVEL12",
    "UNKNOWN",
    "IS"
};

PRIVATE CHAR       *api1ProtSupp[4] = {
    "OTHER",
    "ISO",
    "IP",
    "IPV6"
};

PRIVATE CHAR       *api1CktOperState[2] = {
    "DOWN",
    "UP"
};

PRIVATE CHAR       *api1SummAdminState[5] = {
    "INVALID",
    "SUMMARY-L1",
    "SUMMARY-L2",
    "SUMMARY-L12",
    "SUMMARY-OFF"
};

#endif

/* Function prototypes for all private functions - These functions cannot be
 * invoked by external modules.
 */

PRIVATE VOID        IsisDbgPrintVarFields (UINT1 *, UINT1 *, UINT2, UINT2);
PRIVATE VOID        IsisDbgPrintHelloHdr (tIsisSysContext *, UINT1 *, UINT1);
PRIVATE VOID        IsisDbgPrintLspHdr (tIsisSysContext *, UINT1 *);
PRIVATE VOID        IsisDbgPrintCSNPHdr (tIsisSysContext *, UINT1 *);
PRIVATE VOID        IsisDbgPrintPSNPHdr (tIsisSysContext *, UINT1 *);

/*******************************************************************************
 *
 * Function    : IsisDbgPrintVarFields ()
 * Description : This routine prints the variable length fields of a PDU
 * Inputs      : pPDU      - pointer to the received PDU
 *               pu1Buf    - pointer to the buffer containing the variable
 *                           length fields 
 *               u2Offset  - pointer to the PDU to be dumped
 *               u2PduLen  - length of the total PDU
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PRIVATE VOID
IsisDbgPrintVarFields (UINT1 *pPDU, UINT1 *pu1Buf, UINT2 u2Offset,
                       UINT2 u2PduLen)
{
    UINT1               u1Code;
    UINT1               u1AddrLen;
    UINT1               u1VirtFlag;
    UINT1               u1Length;
    UINT1               u1IdLength;
    tIsisComHdr        *pComHdr;

    pComHdr = (tIsisComHdr *) (pPDU);

    while (u2Offset < u2PduLen)
    {

        u1Code = *pu1Buf;
        u1Length = *(pu1Buf + 1);

        u2Offset += 2;
        pu1Buf += 2;

        switch (u1Code)
        {
            case ISIS_LSP_ENTRY_TLV:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                         "LSP Entries"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                while (u1Length)
                {

                    DBP_PD ((ISIS_LGST, "\t\tRem. Life Time\t\t[ %u ]\n",
                             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1Buf)));

                    pu1Buf += 2;
                    u1Length -= 2;
                    u2Offset += 2;

                    ISIS_DBG_PRINT_ID (pu1Buf, (UINT1) (pComHdr->u1IdLen + 2),
                                       "\t\tLSP ID\t\t\t\t",
                                       (INT4) ISIS_OCTET_STRING);

                    pu1Buf += pComHdr->u1IdLen + 2;
                    u1Length -= (pComHdr->u1IdLen + 2);
                    u2Offset += pComHdr->u1IdLen + 2;

                    DBP_PD ((ISIS_LGST, "\t\tSequence Number\t[ %u ]\n",
                             (UINT4) OSIX_NTOHL (*(UINT4 *) (VOID *) pu1Buf)));

                    pu1Buf += 4;
                    u1Length -= 4;
                    u2Offset += 4;

                    DBP_PD ((ISIS_LGST, "\t\tChecksum\t\t\t\t[ %x ]\n",
                             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1Buf)));

                    pu1Buf += 2;
                    u1Length -= 2;
                    u2Offset += 2;
                }

                DBP_PD ((ISIS_LGST, "%s", "\n"));
                break;

            case ISIS_IP_INTERNAL_RA_TLV:
            case ISIS_IP_EXTERNAL_RA_TLV:

                if (u1Code == ISIS_IP_INTERNAL_RA_TLV)
                {

                    DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                             "IP Internal Reachability"));
                    DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n",
                             u1Length));
                }
                else
                {

                    DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                             "IP External Reachability"));
                    DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n",
                             u1Length));
                }

                while (u1Length)
                {

                    DBP_PD ((ISIS_LGST, "\t\tDef. Metric\t\t\t[ %u %s %s ]\n",
                             (*pu1Buf & ISIS_METRIC_VAL_MASK),
                             api1MetSupp[(*pu1Buf & ISIS_METRIC_SUPP_MASK) >>
                                         7],
                             api1MetType[(*pu1Buf & ISIS_METRIC_TYPE_MASK) >>
                                         6]));
                    pu1Buf++;
                    u2Offset++;
                    u1Length--;

                    DBP_PD ((ISIS_LGST, "\t\tDel. Metric\t\t\t[ %u %s %s ]\n",
                             (*pu1Buf & ISIS_METRIC_VAL_MASK),
                             api1MetSupp[(*pu1Buf & ISIS_METRIC_SUPP_MASK) >>
                                         7],
                             api1MetType[(*pu1Buf & ISIS_METRIC_TYPE_MASK) >>
                                         6]));
                    pu1Buf++;
                    u2Offset++;
                    u1Length--;

                    DBP_PD ((ISIS_LGST, "\t\tExp. Metric\t\t\t[ %u %s %s ]\n",
                             (*pu1Buf & ISIS_METRIC_VAL_MASK),
                             api1MetSupp[(*pu1Buf & ISIS_METRIC_SUPP_MASK) >>
                                         7],
                             api1MetType[(*pu1Buf & ISIS_METRIC_TYPE_MASK) >>
                                         6]));
                    pu1Buf++;
                    u2Offset++;
                    u1Length--;

                    DBP_PD ((ISIS_LGST, "\t\tErr. Metric\t\t\t[ %u %s %s ]\n",
                             (*pu1Buf & ISIS_METRIC_VAL_MASK),
                             api1MetSupp[(*pu1Buf & ISIS_METRIC_SUPP_MASK) >>
                                         7],
                             api1MetType[(*pu1Buf & ISIS_METRIC_TYPE_MASK) >>
                                         6]));
                    pu1Buf++;
                    u2Offset++;
                    u1Length--;

                    ISIS_DBG_PRINT_ADDR (pu1Buf, (UINT1) ISIS_IPV4_ADDR_LEN,
                                         "\t\tIP Address\t\t\t",
                                         ISIS_OCTET_STRING);

                    u1Length -= ISIS_IPV4_ADDR_LEN;
                    u2Offset += ISIS_IPV4_ADDR_LEN;;
                    pu1Buf += ISIS_IPV4_ADDR_LEN;

                    ISIS_DBG_PRINT_ADDR (pu1Buf, (UINT1) ISIS_IPV4_ADDR_LEN,
                                         "\t\tIP Address Mask\t",
                                         ISIS_OCTET_STRING);

                    u1Length -= ISIS_IPV4_ADDR_LEN;
                    u2Offset += ISIS_IPV4_ADDR_LEN;;
                    pu1Buf += ISIS_IPV4_ADDR_LEN;
                }

                DBP_PD ((ISIS_LGST, "%s", "\n"));
                break;

            case ISIS_IS_ADJ_TLV:

                u1IdLength = ((tIsisComHdr *) (pPDU))->u1IdLen;

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                         "IS Adjacency"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                u1VirtFlag = *pu1Buf++;
                u2Offset++;
                u1Length--;

                DBP_PD ((ISIS_LGST, "\t\tVirtual Flag\t\t[ %u ]\n",
                         u1VirtFlag));

                while (u1Length)
                {

                    DBP_PD ((ISIS_LGST, "\t\tDef. Metric\t\t\t[ %u %s %s ]\n",
                             (*pu1Buf & ISIS_METRIC_VAL_MASK),
                             api1MetSupp[(*pu1Buf & ISIS_METRIC_SUPP_MASK) >>
                                         7],
                             api1MetType[(*pu1Buf & ISIS_METRIC_TYPE_MASK) >>
                                         6]));
                    pu1Buf++;
                    u2Offset++;
                    u1Length--;

                    DBP_PD ((ISIS_LGST, "\t\tDel. Metric\t\t\t[ %u %s %s ]\n",
                             (*pu1Buf & ISIS_METRIC_VAL_MASK),
                             api1MetSupp[(*pu1Buf & ISIS_METRIC_SUPP_MASK) >>
                                         7],
                             api1MetType[(*pu1Buf & ISIS_METRIC_TYPE_MASK) >>
                                         6]));
                    pu1Buf++;
                    u2Offset++;
                    u1Length--;

                    DBP_PD ((ISIS_LGST, "\t\tExp. Metric\t\t\t[ %u %s %s ]\n",
                             (*pu1Buf & ISIS_METRIC_VAL_MASK),
                             api1MetSupp[(*pu1Buf & ISIS_METRIC_SUPP_MASK) >>
                                         7],
                             api1MetType[(*pu1Buf & ISIS_METRIC_TYPE_MASK) >>
                                         6]));
                    pu1Buf++;
                    u2Offset++;
                    u1Length--;

                    DBP_PD ((ISIS_LGST, "\t\tErr. Metric\t\t\t[ %u %s %s ]\n",
                             (*pu1Buf & ISIS_METRIC_VAL_MASK),
                             api1MetSupp[(*pu1Buf & ISIS_METRIC_SUPP_MASK) >>
                                         7],
                             api1MetType[(*pu1Buf & ISIS_METRIC_TYPE_MASK) >>
                                         6]));
                    pu1Buf++;
                    u2Offset++;
                    u1Length--;

                    ISIS_DBG_PRINT_ID (pu1Buf, (UINT1) (u1IdLength + 1),
                                       "\t\tAdj Neighbour ID\t",
                                       ISIS_OCTET_STRING);

                    u1Length -= (u1IdLength + 1);
                    u2Offset += u1IdLength + 1;
                    pu1Buf += u1IdLength + 1;
                }

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;

            case ISIS_AREA_ADDR_TLV:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                         "Area Address"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                while (u1Length)
                {

                    u1AddrLen = *pu1Buf++;
                    u2Offset++;

                    ISIS_DBG_PRINT_ADDR (pu1Buf, (UINT1) u1AddrLen,
                                         "\t\tArea Addr \t\t\t",
                                         ISIS_OCTET_STRING);

                    u1Length -= (u1AddrLen + 1);
                    u2Offset += u1AddrLen;
                    pu1Buf += u1AddrLen;
                }

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;

            case ISIS_IS_NEIGH_ADDR_TLV:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                         "IS Neighbour Address"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                while (u1Length)
                {

                    ISIS_DBG_PRINT_ADDR (pu1Buf, (UINT1) ISIS_LAN_ADDR_LEN,
                                         "\t\tLAN Addr \t\t\t",
                                         ISIS_OCTET_STRING);

                    u1Length -= ISIS_LAN_ADDR_LEN;
                    u2Offset += ISIS_LAN_ADDR_LEN;
                    pu1Buf += ISIS_LAN_ADDR_LEN;
                }

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;

            case ISIS_PROT_SUPPORT_TLV:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                         "Protocol Supported"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                while (u1Length)
                {

                    DBP_PD ((ISIS_LGST, "\t\tNLPID\t\t\t\t\t[ %s ]\n",
                             api1ProtSupp[ISIS_DBG_GET_PS_IDX (*pu1Buf++)]));
                    u1Length--;
                    u2Offset++;
                }

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;

            case ISIS_IPV4IF_ADDR_TLV:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                         "IPV4 IF Address"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                while (u1Length)
                {

                    ISIS_DBG_PRINT_ADDR (pu1Buf, (UINT1) ISIS_IPV4_ADDR_LEN,
                                         "\t\tIP Addr \t\t\t\t",
                                         ISIS_OCTET_STRING);
                    u1Length -= ISIS_IPV4_ADDR_LEN;
                    u2Offset += ISIS_IPV4_ADDR_LEN;
                    pu1Buf += ISIS_IPV4_ADDR_LEN;
                }

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;

            case ISIS_IDRP_INFO_TLV:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n",
                         "Inter Domain Routing Prot Info - Ignored"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                u2Offset += u1Length;
                pu1Buf += u1Length;

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;

            case ISIS_AUTH_INFO_TLV:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n", "Auth Info"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                DBP_PD ((ISIS_LGST, "\t\tAuth Type \t\t\t[ %u ]\n", *pu1Buf));

                ISIS_DBG_PRINT_AUTH_INFO ((pu1Buf + 1), (UINT1) (u1Length - 1),
                                          "\t\tAuthInfo \t\t\t",
                                          ISIS_CHAR_STRING);

                u2Offset += u1Length;
                pu1Buf += u1Length;

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;

            case ISIS_PADDING_TLV:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %s ]\n", "Padding"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                u2Offset += u1Length;
                pu1Buf += u1Length;

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;

            default:

                DBP_PD ((ISIS_LGST, "\tCode\t\t\t\t\t\t[ %u %s ]\n",
                         u1Code, "Unknown - Ignored"));
                DBP_PD ((ISIS_LGST, "\tLength\t\t\t\t\t[ %u ]\n", u1Length));

                u2Offset += u1Length;
                pu1Buf += u1Length;

                DBP_PD ((ISIS_LGST, "%s", "\n"));

                break;
        }
    }
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintCommHdr ()
 * Description : This routine dumps the Common Header of a given PDU.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to whose header is to be 
 *                             dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintCommHdr (tIsisSysContext * pContext, tIsisComHdr * pComHdr)
{

    UNUSED_PARAM (pContext);

    PRINTF ("\tIntra-Domain Routing Protocol discriminator:ISIS\t[ %x ]\n",
            pComHdr->u1IDRPDisc);
    PRINTF ("\tPDU Header Length\t\t\t\t\t[ %u ]\n", pComHdr->u1HdrLen);
    PRINTF ("\tVersion\t\t\t\t\t\t\t[ %u ]\n", pComHdr->u1VerProtoId);
    PRINTF ("\tSystem ID length\t\t\t\t\t[ %u ]\n", pComHdr->u1IdLen);
    PRINTF ("\tPDU Type\t\t\t\t\t\t[ %s ]\n",
            ISIS_GET_PDU_TYPE_STR (pComHdr->u1PDUType));
    PRINTF ("\tVersion2(==1)\t\t\t\t\t\t[ %u ]\n", pComHdr->u1Version);
    PRINTF ("\tMax Area Addr\t\t\t\t\t\t[ %u ]\n\n", pComHdr->u1MaxAreaAddr);
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintHEXDMP ()
 * Description : This routine dumps the Common Header of a given PDU.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to whose header is to be 
 *                             dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintHEXDMP (UINT4 u4len, UINT1 *ppdu)
{

    UINT4               u4index1;
    /*Process every byte in the data. */
    for (u4index1 = 0; u4index1 < u4len; u4index1++)
    {
        /* Multiple of 8 means new line. */

        if ((u4index1 % 8) == 0)
        {
            if (u4index1 != 0)
                PRINTF (" ");
        }
        if ((u4index1 % 32) == 0)
        {
            if (u4index1 != 0)
                PRINTF ("\n");
        }

        /* Now the hex code for the specific character. */
        PRINTF (" %02x", ppdu[u4index1]);
    }
    PRINTF ("\n");
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintHelloHdr ()
 * Description : This routine dumps the Hello PDU Specific Header.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to whose header is to be 
 *                             dumped
 *               u1PduType   - Type of PDU to being dumped                
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PRIVATE VOID
IsisDbgPrintHelloHdr (tIsisSysContext * pContext, UINT1 *pPDU, UINT1 u1PduType)
{
    UINT1              *pu1BufOffset;
    tIsisComHdr        *pComHdr;

    UNUSED_PARAM (pContext);
    pComHdr = (tIsisComHdr *) (pPDU);
    pu1BufOffset = ((UINT1 *) (pPDU)) + sizeof (tIsisComHdr);

    DBP_PD ((ISIS_LGST, "\tCircuit Type\t\t\t[ %u ]\n",
             (*pu1BufOffset & ISIS_CKT_TYPE_MASK)));

    pu1BufOffset += 1;

    ISIS_DBG_PRINT_ID (pu1BufOffset, (UINT1) pComHdr->u1IdLen,
                       "\tSourceId\t\t\t\t\t", ISIS_OCTET_STRING);

    pu1BufOffset += pComHdr->u1IdLen;

    DBP_PD ((ISIS_LGST, "\tHolding Time\t\t\t[ %u ]\n",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    DBP_PD ((ISIS_LGST, "\tPDU Length\t\t\t\t[ %u ]\n",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    if ((u1PduType == ISIS_L1LAN_IIH_PDU) || (u1PduType == ISIS_L2LAN_IIH_PDU))
    {

        DBP_PD ((ISIS_LGST, "\tPriority\t\t\t\t\t[ %u ]\n",
                 (*pu1BufOffset & ISIS_PRIORITY_MASK)));

        pu1BufOffset += 1;

        ISIS_DBG_PRINT_ID (pu1BufOffset, (UINT1) (pComHdr->u1IdLen + 1),
                           "\tLAN ID\t\t\t\t\t", ISIS_OCTET_STRING);
    }
    else
    {

        DBP_PD ((ISIS_LGST, "\tLocal Ckt Id\t\t\t[ %u ]\n", *pu1BufOffset));
    }
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintLspHdr ()
 * Description : This routine dumps the Link State PDU Specific Header.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to whose header is to be 
 *                             dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PRIVATE VOID
IsisDbgPrintLspHdr (tIsisSysContext * pContext, UINT1 *pPDU)
{
    UINT1              *pu1BufOffset = NULL;
    tIsisComHdr        *pComHdr = NULL;

    UNUSED_PARAM (pContext);
    pComHdr = (tIsisComHdr *) (pPDU);
    pu1BufOffset = ((UINT1 *) (pPDU)) + sizeof (tIsisComHdr);

    DBP_PD ((ISIS_LGST, "\tPDU Length\t\t\t\t[ %u ]\n",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    DBP_PD ((ISIS_LGST, "\tRem. Life Time\t\t\t[ %u ]\n",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    ISIS_DBG_PRINT_ID (pu1BufOffset, (UINT1) (pComHdr->u1IdLen + 2),
                       "\tLSP ID\t\t\t\t\t", ISIS_OCTET_STRING);

    pu1BufOffset += pComHdr->u1IdLen + 2;

    DBP_PD ((ISIS_LGST, "\tSeq. Number\t\t\t\t[ %u ]\n",
             (UINT4) OSIX_NTOHL (*(UINT4 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 4;

    DBP_PD ((ISIS_LGST, "\tChecksum\t\t\t\t\t[ %x ]\n",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    DBP_PD ((ISIS_LGST, "\tPart. Repair\t\t\t[ %u ]\n",
             ((*pu1BufOffset & ISIS_PART_REPAIR_MASK)) >> 7));
    DBP_PD ((ISIS_LGST, "\tATT\t\t\t\t\t\t[ %x ]\n",
             ((*pu1BufOffset & ISIS_ATT_MASK)) >> 3));
    DBP_PD ((ISIS_LGST, "\tLSPDBOL\t\t\t\t\t[ %u ]\n",
             ((*pu1BufOffset & ISIS_DBOL_MASK)) >> 2));
    DBP_PD ((ISIS_LGST, "\tIS Type\t\t\t\t\t[ %u ]\n",
             (*pu1BufOffset & ISIS_ISTYPE_MASK)));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintCSNPHdr ()
 * Description : This routine dumps the CNSP Specific Header.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to whose header is to be 
 *                             dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PRIVATE VOID
IsisDbgPrintCSNPHdr (tIsisSysContext * pContext, UINT1 *pPDU)
{
    UINT1              *pu1BufOffset;
    tIsisComHdr        *pComHdr;

    UNUSED_PARAM (pContext);
    pComHdr = (tIsisComHdr *) (pPDU);
    pu1BufOffset = ((UINT1 *) (pPDU)) + sizeof (tIsisComHdr);

    DBP_PD ((ISIS_LGST, "\tPDU Length\t\t\t\t[ %u ]\n",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    ISIS_DBG_PRINT_ID (pu1BufOffset, (UINT1) (pComHdr->u1IdLen + 1),
                       "\tSource Id\t\t\t\t", ISIS_OCTET_STRING);

    pu1BufOffset += pComHdr->u1IdLen + 1;

    ISIS_DBG_PRINT_ID (pu1BufOffset, (UINT1) (pComHdr->u1IdLen + 2),
                       "\tStart LSP Id\t\t\t", ISIS_OCTET_STRING);

    pu1BufOffset += pComHdr->u1IdLen + 2;

    ISIS_DBG_PRINT_ID (pu1BufOffset, (UINT1) (pComHdr->u1IdLen + 2),
                       "\tEnd LSP Id\t\t\t\t", ISIS_OCTET_STRING);

    pu1BufOffset += pComHdr->u1IdLen + 2;
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintPSNPHdr ()
 * Description : This routine dumps the PNSP Specific Header.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to whose header is to be 
 *                             dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PRIVATE VOID
IsisDbgPrintPSNPHdr (tIsisSysContext * pContext, UINT1 *pPDU)
{
    UINT1              *pu1BufOffset;
    tIsisComHdr        *pComHdr;

    UNUSED_PARAM (pContext);
    pComHdr = (tIsisComHdr *) (pPDU);
    pu1BufOffset = ((UINT1 *) (pPDU)) + sizeof (tIsisComHdr);

    DBP_PD ((ISIS_LGST, "\tPDU Length\t\t\t\t[ %u ]\n",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    ISIS_DBG_PRINT_ID (pu1BufOffset, (UINT1) (pComHdr->u1IdLen + 1),
                       "\tSource Id\t\t\t\t", ISIS_OCTET_STRING);
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintHelloPDU ()
 * Description : This routine dumps the Hello PDU.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to be dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintHelloPDU (tIsisSysContext * pContext, UINT1 *pPDU)
{
    UINT1               u1PduType;
    UINT1              *pu1BufOffset;
    UINT2               u2PduLen = 0;
    UINT2               u2Offset;
    tIsisComHdr        *pComHdr;

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintHelloPDU ()\n"));

    if (pPDU == NULL)
    {

        DBP_PD ((ISIS_LGST, "DBG <E> : Cannot Dump NULL PDU\n"));
    }
    else
    {

        u1PduType = ISIS_EXTRACT_PDU_TYPE (pPDU);
        ISIS_EXTRACT_HELLO_PDU_LEN (pPDU, u2PduLen);

        pComHdr = (tIsisComHdr *) pPDU;

        DBP_PD ((ISIS_LGST, "\nHELLO PACKET\n"));
        DBP_PD ((ISIS_LGST, "\n\tCommon Header\n\n"));

        IsisDbgPrintCommHdr (pContext, pComHdr);

        DBP_PD ((ISIS_LGST, "\n\tHello Specific Header\n\n"));

        IsisDbgPrintHelloHdr (pContext, pPDU, u1PduType);

        /* Get to the point where the variable length fields start
         */

        if ((u1PduType == ISIS_L1LAN_IIH_PDU)
            || (u1PduType == ISIS_L2LAN_IIH_PDU))
        {

            u2Offset = (UINT2)
                (sizeof (tIsisComHdr) + 7 + (2 * pComHdr->u1IdLen));
            pu1BufOffset = ((UINT1 *) (pPDU)) + u2Offset;
        }
        else
        {

            u2Offset = (UINT2) (sizeof (tIsisComHdr) + 6 + pComHdr->u1IdLen);
            pu1BufOffset = ((UINT1 *) (pPDU)) + u2Offset;
        }

        DBP_PD ((ISIS_LGST, "\n\tVariable Length Fields\n\n"));

        IsisDbgPrintVarFields (pPDU, pu1BufOffset, u2Offset, u2PduLen);
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintHelloPDU ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintLinkStatePDU ()
 * Description : This routine dumps the Link State PDU.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to be dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintLinkStatePDU (tIsisSysContext * pContext, UINT1 *pPDU)
{
    UINT1               u1PduType;
    UINT1              *pu1BufOffset;
    UINT2               u2PduLen = 0;
    UINT2               u2Offset;
    tIsisComHdr        *pComHdr;

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintLinkStatePDU ()\n"));

    if (pPDU == NULL)
    {

        DBP_PD ((ISIS_LGST, "DBG <E> : Cannot Dump NULL PDU\n"));
    }
    else
    {
        u1PduType = ISIS_EXTRACT_PDU_TYPE (pPDU);
        UNUSED_PARAM (u1PduType);
        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pPDU, u2PduLen);

        pComHdr = (tIsisComHdr *) pPDU;

        DBP_PD ((ISIS_LGST, "\nLINK STATE PACKET\n"));
        DBP_PD ((ISIS_LGST, "\n\tCommon Header\n\n"));

        IsisDbgPrintCommHdr (pContext, pComHdr);

        DBP_PD ((ISIS_LGST, "\n\tLSP Specific Header\n\n"));

        IsisDbgPrintLspHdr (pContext, pPDU);

        /* Get to the point where the variable length fields start
         */

        u2Offset = (UINT2) (sizeof (tIsisComHdr) + 13 + pComHdr->u1IdLen);
        pu1BufOffset = ((UINT1 *) (pPDU)) + u2Offset;

        DBP_PD ((ISIS_LGST, "\n\tVariable Length Fields\n\n"));

        IsisDbgPrintVarFields (pPDU, pu1BufOffset, u2Offset, u2PduLen);
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintLinkStatePDU ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintLSPInfo ()
 * Description : This routine dumps the Link State PDU partially. It prints only
 *               the relevant fields viz. LSPID, RLT, PDU Len, Sequence Num.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to be dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintLSPInfo (tIsisSysContext * pContext, UINT1 *pPDU)
{
    UINT1              *pu1BufOffset;
    tIsisComHdr        *pComHdr;

    UNUSED_PARAM (pContext);
    pComHdr = (tIsisComHdr *) pPDU;
    pu1BufOffset = ((UINT1 *) (pPDU)) + sizeof (tIsisComHdr);

    DBP_PD ((ISIS_LGST, "\n%-20s\t\t[ %u ]\n", "PDU Length",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    DBP_PD ((ISIS_LGST, "%-20s\t\t[ %u ]\n", "Rem. Life Time",
             (UINT2) OSIX_NTOHS (*(UINT2 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 2;

    ISIS_DBG_PRINT_ID (pu1BufOffset, (UINT1) (pComHdr->u1IdLen + 2),
                       "LSP ID\t\t\t\t", ISIS_OCTET_STRING);

    pu1BufOffset += pComHdr->u1IdLen + 2;

    DBP_PD ((ISIS_LGST, "%-20s\t\t[ %u ]\n", "Seq. Number",
             (UINT4) OSIX_NTOHL (*(UINT4 *) (VOID *) pu1BufOffset)));

    pu1BufOffset += 4;

}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintCSNP
 * Description : This routine dumps the Complete Sequence Number PDU.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to be dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintCSNP (tIsisSysContext * pContext, UINT1 *pPDU)
{
    UINT1              *pu1BufOffset;
    UINT2               u2PduLen = 0;
    UINT2               u2Offset;
    tIsisComHdr        *pComHdr;

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintCSNP ()\n"));

    if (pPDU == NULL)
    {

        DBP_PD ((ISIS_LGST, "DBG <E> : Cannot Dump NULL PDU\n"));
    }
    else
    {
        ISIS_EXTRACT_SNP_PDU_LEN (pPDU, u2PduLen);

        pComHdr = (tIsisComHdr *) pPDU;

        DBP_PD ((ISIS_LGST, "\nCOMPLETE_SEQUENCE NUMBER PACKET\n"));
        DBP_PD ((ISIS_LGST, "\n\tCommon Header\n\n"));

        IsisDbgPrintCommHdr (pContext, pComHdr);

        DBP_PD ((ISIS_LGST, "\n\tCSNP Specific Header\n\n"));

        IsisDbgPrintCSNPHdr (pContext, pPDU);

        /* Get to the point where the variable length fields start
         */

        u2Offset = (UINT2) (sizeof (tIsisComHdr) + 7 + (3 * pComHdr->u1IdLen));
        pu1BufOffset = ((UINT1 *) (pPDU)) + u2Offset;

        DBP_PD ((ISIS_LGST, "\n\tVariable Length Fields\n\n"));

        IsisDbgPrintVarFields (pPDU, pu1BufOffset, u2Offset, u2PduLen);
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintCSNP ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintPSNP
 * Description : This routine dumps the Partial Sequence Number PDU.
 * Inputs      : pContext    - pointer to the current context
 *               pPDU        - pointer to the PDU to be dumped
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintPSNP (tIsisSysContext * pContext, UINT1 *pPDU)
{
    UINT1              *pu1BufOffset;
    UINT2               u2PduLen = 0;
    UINT2               u2Offset;
    tIsisComHdr        *pComHdr;

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintPSNP ()\n"));

    if (pPDU == NULL)
    {
        DBP_PD ((ISIS_LGST, "DBG <E> : Cannot Dump NULL PDU\n"));
    }
    else
    {
        ISIS_EXTRACT_SNP_PDU_LEN (pPDU, u2PduLen);

        pComHdr = (tIsisComHdr *) pPDU;

        DBP_PD ((ISIS_LGST, "\nPARTIAL SEQUENCE NUMBER PACKET\n"));
        DBP_PD ((ISIS_LGST, "\n\tCommon Header\n\n"));

        IsisDbgPrintCommHdr (pContext, pComHdr);

        DBP_PD ((ISIS_LGST, "\n\tPSNP Specific Header\n\n"));

        IsisDbgPrintPSNPHdr (pContext, pPDU);

        /* Get to the point where the variable length fields start
         */

        u2Offset = (UINT2) (sizeof (tIsisComHdr) + 3 + pComHdr->u1IdLen);
        pu1BufOffset = ((UINT1 *) (pPDU)) + u2Offset;

        DBP_PD ((ISIS_LGST, "\n\tVariable Length Fields\n\n"));

        IsisDbgPrintVarFields (pPDU, pu1BufOffset, u2Offset, u2PduLen);
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintPSNP ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintIsisInfo
 * Description : This routine dumps the details of the requested Tables, based
 *               upon the Input parameter
 * Inputs      : u1DBName - Name of the Database Table whose dump is to
 *                          be printed
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintIsisInfo (UINT1 u1DBName)
{
    tIsisSysContext    *pContext = NULL;

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintIsisInfo ()\n"));

    /* Enable the Print for the Debug Module alone
     */

    gau1IsisModLevelMask[ISIS_DBG_MODULE] = 0x17;

    pContext = gpIsisInstances;

    if (pContext == NULL)
    {
        DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintIsisInfo ()\n"));
        return;
    }

    switch (u1DBName)
    {
        case 0:

            DBP_TD ((ISIS_LGST, "\nISIS INFO - ALL Tables\n\n"));
            IsisDbgPrintIsisInfo (1);
            IsisDbgPrintIsisInfo (2);
            IsisDbgPrintIsisInfo (3);
            IsisDbgPrintIsisInfo (4);
            IsisDbgPrintIsisInfo (5);
            IsisDbgPrintIsisInfo (6);
            IsisDbgPrintIsisInfo (7);
            break;

        case 1:
            DBP_TD ((ISIS_LGST, "\nSYSTEM TABLE\n\n"));
            IsisDbgPrintSysTable ();
            break;

        case 2:
            DBP_TD ((ISIS_LGST, "\nCIRCUIT TABLE\n"));
            for (; pContext != NULL; pContext = pContext->pNext)
            {
                IsisDbgPrintCktTable (pContext);
            }
            break;

        case 3:
            DBP_TD ((ISIS_LGST, "\nADJACENCY TABLE\n"));
            for (; pContext != NULL; pContext = pContext->pNext)
            {
                IsisDbgPrintAdjTable (pContext);
            }
            break;

        case 4:
            DBP_TD ((ISIS_LGST, "\nIPRA TABLE\n"));
            for (; pContext != NULL; pContext = pContext->pNext)
            {
                IsisDbgPrintIPRATable (pContext);
            }
            break;

        case 5:
            DBP_TD ((ISIS_LGST, "\nSUMMARY ADDRESS TABLE\n"));
            for (; pContext != NULL; pContext = pContext->pNext)
            {
                IsisDbgPrintSATable (pContext);
            }
            break;

        case 6:
            DBP_TD ((ISIS_LGST, "\nLSP DATABASES\n"));
            for (; pContext != NULL; pContext = pContext->pNext)
            {
                if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
                {
                    IsisDbgPrintLSPDB (pContext, ISIS_LEVEL1);
                }
                if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
                {
                    IsisDbgPrintLSPDB (pContext, ISIS_LEVEL2);
                }
            }
            break;

        case 7:
            DBP_TD ((ISIS_LGST, "\nSHORTEST PATH DATABASE\n"));
            for (; pContext != NULL; pContext = pContext->pNext)
            {
                if (pContext->u1OperState != ISIS_UP)
                {
                    continue;
                }
                if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
                {
                    IsisDbgPrintPath (pContext, ISIS_PREV_IP_PATH, ISIS_LEVEL1);
                }
                if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
                {
                    IsisDbgPrintPath (pContext, ISIS_PREV_IP_PATH, ISIS_LEVEL2);
                }
            }
            break;

        default:
            DBP_TD ((ISIS_LGST, "\nIncorrect Data Base Name \n"));
            break;
    }

    gau1IsisModLevelMask[ISIS_DBG_MODULE] = 0x00;
    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintIsisInfo ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintSysTable
 * Description : This routine dumps the System Table .It prints only relevant 
 *               fields from the System table.
 * Inputs      : None
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintSysTable ()
{
    tIsisSysContext    *pContext = NULL;
    CHAR                acSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintSysTable ()\n"));

    pContext = gpIsisInstances;
    if (pContext == NULL)
    {
        DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintSysTable ()\n"));
        return;
    }

    DBP_TD ((ISIS_LGST,
             "SystemIdx   System-ID          SysType     OperState\n"));
    DBP_TD ((ISIS_LGST,
             "----------------------------------------------------\n"));

    for (; pContext != NULL; pContext = pContext->pNext)
    {
        if (pContext->SysActuals.u1SysType < ISIS_DEBUG_MAX_SYS_TYPE)
        {

            ISIS_FORM_STR (ISIS_SYS_ID_LEN, pContext->SysActuals.au1SysID,
                           acSysId);
            DBP_TD ((ISIS_LGST, "%-12u%-20s%-12s%-10u\n",
                     pContext->u4SysInstIdx, acSysId,
                     api1SysType[pContext->SysActuals.u1SysType],
                     pContext->u1OperState));
        }
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintSysTable ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintCktTable
 * Description : This routine dumps the Circuit Table associated with the given
 *               context. It prints only relevant fields from the circuit table.
 * Inputs      : * pContext    - pointer to the current context
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintCktTable (tIsisSysContext * pContext)
{
    tIsisCktEntry      *pPrintCktRec = NULL;

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintCktTable ()\n"));

    pPrintCktRec = pContext->CktTable.pCktRec;
    if (pPrintCktRec == NULL)
    {
        DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintCktTable ()\n"));
        return;
    }

    DBP_TD ((ISIS_LGST,
             "\nInstance [ %u ] NumEntries [ %u ] \n\n",
             pContext->u4SysInstIdx, pContext->CktTable.u4NumEntries));

    DBP_TD ((ISIS_LGST,
             "CktId     IfIdx     IfSubIdx  CktType   Level     OperState\n"));
    DBP_TD ((ISIS_LGST,
             "-----------------------------------------------------------\n"));

    for (; pPrintCktRec != NULL; pPrintCktRec = pPrintCktRec->pNext)
    {
        if ((pPrintCktRec->u1CktType < ISIS_DEBUG_MAX_CKT_TYPE)
            && (pPrintCktRec->u1CktLevel < ISIS_DEBUG_MAX_CKT_LEVEL)
            && (pPrintCktRec->u1OperState < ISIS_DEBUG_MAX_OPER_STATE))
        {
            DBP_TD ((ISIS_LGST,
                     "%-10u%-10u%-10u%-10s%-10s%-10s\n",
                     pPrintCktRec->u4CktIdx, pPrintCktRec->u4CktIfIdx,
                     pPrintCktRec->u4CktIfSubIdx,
                     api1CktType[pPrintCktRec->u1CktType],
                     api1CktLevel[pPrintCktRec->u1CktLevel],
                     api1CktOperState[pPrintCktRec->u1OperState]));
        }
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintCktTable ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintIPRATable
 * Description : This routine dumps the IPRA Table associated with the given
 *               circuit. It prints only relevant fields from the IPRA table.
 * Inputs      : * pCktRec    - pointer to the current Circuit Record
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintIPRATable (tIsisSysContext * pContext)
{
    UINT1               au1Mask[ISIS_MAX_IP_ADDR_LEN];
    CHAR                ac1IPRA[(3 * ISIS_IPV4_ADDR_LEN) + 1];
    CHAR                ac1IPRAMask[(3 * ISIS_IPV4_ADDR_LEN) + 1];
    tIsisIPRAEntry     *pIPRARec = NULL;

    MEMSET (au1Mask, 0, ISIS_MAX_IP_ADDR_LEN);

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintIPRATable ()\n"));

    if ((pContext == NULL) || (pContext->CktTable.pCktRec == NULL))
    {
        DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintIPRATable ()\n"));
        return;
    }

    DBP_TD ((ISIS_LGST, "\nInstance [ %u ] \n\n", pContext->u4SysInstIdx));
    DBP_TD ((ISIS_LGST,
             "IPRAIdx    IPRAAddr       IPRAMask       IPRAMetric   MetType\n"));
    DBP_TD ((ISIS_LGST,
             "------------------------------------------------------------\n"));

    pIPRARec = pContext->IPRAInfo.pIPRAEntry;
    for (; pIPRARec != NULL; pIPRARec = pIPRARec->pNext)
    {
        if (pIPRARec->u1PrefixLen == ISIS_IPV4_ADDR_LEN)
        {
            ISIS_FORM_STR (ISIS_IPV4_ADDR_LEN, pIPRARec->au1IPRADest, ac1IPRA);
            IsisUtlGetIPMask (pIPRARec->u1PrefixLen, au1Mask);
            ISIS_FORM_STR (ISIS_IPV4_ADDR_LEN, au1Mask, ac1IPRAMask);
            DBP_TD ((ISIS_LGST, "%-10u%-15s%-15s%-12u%-8s\n",
                     pIPRARec->u4IPRAIdx, ac1IPRA, ac1IPRAMask,
                     (pIPRARec->Metric[0] & ISIS_METRIC_VAL_MASK),
                     (((pIPRARec->Metric[0] &
                        ISIS_METRIC_TYPE_MASK) ==
                       0) ? "INTERNAL" : "EXTERNAL")));
        }
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintIPRATable ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintSATable
 * Description : This routine dumps the Summary Address Table associated with 
 *               the given circuit. It prints only relevant fields from the 
 *               Summary Address table.
 * Inputs      : * pContext    - pointer to the current Context
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintSATable (tIsisSysContext * pContext)
{
    UINT1               au1Mask[ISIS_MAX_IP_ADDR_LEN];
    CHAR                ac1SA[(3 * ISIS_IPV4_ADDR_LEN) + 1];
    CHAR                ac1SAMask[(3 * ISIS_IPV4_ADDR_LEN) + 1];
    tIsisSAEntry       *pSARec = NULL;

    MEMSET (ac1SA, 0, ((3 * ISIS_IPV4_ADDR_LEN) + 1));
    MEMSET (au1Mask, 0, ISIS_MAX_IP_ADDR_LEN);
    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintSATable ()\n"));

    pSARec = pContext->SummAddrTable.pSAEntry;
    if (pSARec == NULL)
    {
        DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintSATable ()\n"));
        return;
    }

    DBP_TD ((ISIS_LGST, "\nInstance [ %u ] \n\n", pContext->u4SysInstIdx));
    DBP_TD ((ISIS_LGST, "SumAddr        SumAddrMask    SumAddrMetric  "
             "AdminState \n"));
    DBP_TD ((ISIS_LGST, "---------------------------------------------"
             "---------- \n"));
    for (; pSARec != NULL; pSARec = pSARec->pNext)
    {
        if ((pSARec->u1PrefixLen == ISIS_IPV4_ADDR_LEN)
            && (pSARec->u1AdminState < ISIS_DEBUG_MAX_SUMM_ADMIN))
        {
            ISIS_FORM_STR (ISIS_IPV4_ADDR_LEN, pSARec->au1SummAddr, ac1SA);
            IsisUtlGetIPMask (pSARec->u1PrefixLen, au1Mask);
            ISIS_FORM_STR (ISIS_IPV4_ADDR_LEN, au1Mask, ac1SAMask);
            DBP_TD ((ISIS_LGST, "%-15s%-15s%-15u%-12s\n",
                     ac1SA, ac1SAMask, pSARec->Metric[0],
                     api1SummAdminState[pSARec->u1AdminState]));
        }
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintSATable ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintCktLvlTable
 * Description : This routine dumps the Circuit Level Table associated with the 
 *               given Circuit.
 * Inputs      : pCktRec     - pointer to the circuit record
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintCktLvlTable (tIsisCktLevel * pCktLevel)
{
    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintCktLvlTable ()\n"));

    if (pCktLevel == NULL)
    {
        DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintCktLvlTable ()\n"));
        return;
    }

    DBP_TD ((ISIS_LGST,
             "\tL1: CktLvlId\t\t[ %u ]\t\tL1: IS Prio\t\t[ %u ]\n",
             pCktLevel->u1CktLvlIdx, pCktLevel->u1ISPriority));
    DBP_TD ((ISIS_LGST,
             "\tL1: Hello X\t\t[ %u ]\t\tL1: Thrtl Count\t\t[ %u ]\n",
             pCktLevel->u2HelloMultiplier, pCktLevel->u2ThrtlCount));

    DBP_TD ((ISIS_LGST, "L1: Thrtl Int\t\t[ %u ]", pCktLevel->LspThrottleInt));

    ISIS_DBG_PRINT_ID (pCktLevel->au1CktLanDISID,
                       (UINT1) (ISIS_SYS_ID_LEN + 1), "\t\t\tDIS ID\t",
                       ISIS_OCTET_STRING);

    DBP_TD ((ISIS_LGST,
             "\tL1: DRH Time Int[ %u ]\t\tL1: Hello Time Int \t\t[ %u ]\n",
             pCktLevel->DRHelloTimeInt, pCktLevel->HelloTimeInt));
    DBP_TD ((ISIS_LGST,
             "\tL1: PSNP Int[ %u ]\t\tL1: CSNP Int \t\t[ %u ]\n",
             pCktLevel->CSNPInterval, pCktLevel->PSNPInterval));
    DBP_TD ((ISIS_LGST,
             "\tL1: Min LSP RTI [ %u ]\t\tL1: Metric-1\t\t[ %u ]\n",
             pCktLevel->u4MinLspReTxInt, pCktLevel->Metric[0]));
    DBP_TD ((ISIS_LGST,
             "\tL1: Metric-2 [ %u ]\t\tL1: Metric-3\t\t[ %u ]\n",
             pCktLevel->Metric[1], pCktLevel->Metric[2]));
    DBP_TD ((ISIS_LGST, "\tL1: Metric-3 [ %u ]", pCktLevel->Metric[3]));

    ISIS_DBG_PRINT_ID (pCktLevel->au1CLCktID, (UINT1) (ISIS_SYS_ID_LEN + 1),
                       "\t\t\tCKT ID\t", ISIS_OCTET_STRING);

    DBP_TD ((ISIS_LGST,
             "\tL1: PSNP [ %p ]\t\tL1: LSP Mark\t\t[ %p ]\n",
             (void *) pCktLevel->pPSNP, (void *) pCktLevel->pMarkTxLSP));

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintCktLvlTable ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintAdjTable
 * Description : This routine dumps the Adjacency Table associated with a given
 *               given circuit.
 * Inputs      : pCktRec     - pointer to the circuit record whose adjacencies
 *                             are to be printed
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintAdjTable (tIsisSysContext * pContext)
{
    CHAR                acSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    CHAR                acIPAddr[(3 * ISIS_IPV4_ADDR_LEN) + 1];
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisCktEntry      *pCktRec = NULL;

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintAdjTable ()\n"));

    if ((pContext == NULL) || (pContext->CktTable.pCktRec == NULL))
    {
        DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintAdjTable ()\n"));
        return;
    }

    DBP_TD ((ISIS_LGST,
             "\nInstance [ %u ] NumEntries [ %u ] \n\n",
             pContext->u4SysInstIdx, (pContext->CktTable.u4NumL1Adjs +
                                      pContext->CktTable.u4NumL2Adjs)));

    DBP_TD ((ISIS_LGST, "CktIdx  AdjSysType  AdjUsage    AdjState    AdjSysId"
             "            AdjIPAddr  \n"));
    DBP_TD ((ISIS_LGST, "----------------------------------------------------"
             "-----------------------\n"));

    for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
         pCktRec = pCktRec->pNext)
    {
        pAdjRec = pCktRec->pAdjEntry;
        for (; pAdjRec != NULL; pAdjRec = pAdjRec->pNext)
        {
            ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID, acSysId);
            ISIS_FORM_STR (ISIS_IPV4_ADDR_LEN,
                           pAdjRec->AdjNbrIpV4Addr.au1IpAddr, acIPAddr);
            if ((pAdjRec->u1AdjNbrSysType < ISIS_DEBUG_MAX_SYS_TYPE)
                && (pAdjRec->u1AdjUsage < ISIS_DEBUG_MAX_ADJ_USAGE)
                && (pAdjRec->u1AdjState < ISIS_DEBUG_MAX_ADJ_STATE))
            {
                DBP_TD ((ISIS_LGST, "%-8u%-12s%-12s%-12s%-20s%-12s\n",
                         pCktRec->u4CktIdx,
                         api1SysType[pAdjRec->u1AdjNbrSysType],
                         api1AdjUsage[pAdjRec->u1AdjUsage],
                         api1AdjState[pAdjRec->u1AdjState], acSysId, acIPAddr));
            }
        }
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintAdjTable ()\n"));
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintLSPDB
 * Description : This routine dumps the LSP Database associated with a given
 *               level.
 * Inputs      : * pContext    - pointer to the current context
 *               * u1Level     - database level
 * Outputs     : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintLSPDB (tIsisSysContext * pContext, UINT1 u1Level)
{
    UINT1              *pLSP = NULL;;

    DBP_EE ((ISIS_LGST, "DBG <X> : Entered IsisDbgPrintLSPDB ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        if (pContext->SysL1Info.LspDB.pHashBkts != NULL)
        {
            pLSP = (UINT1 *) ((tIsisLSPEntry *)
                              ((pContext->SysL1Info.LspDB.pHashBkts)->
                               pFirstElem));
        }
    }
    else
    {
        if (pContext->SysL2Info.LspDB.pHashBkts != NULL)
        {
            pLSP = (UINT1 *) ((tIsisLSPEntry *)
                              ((pContext->SysL2Info.LspDB.pHashBkts)->
                               pFirstElem));
        }
    }

    DBP_TD ((ISIS_LGST, "\nInstance [ %u ]  %s\n\n",
             pContext->u4SysInstIdx, api1SysType[u1Level]));

    while (pLSP != NULL)
    {
        IsisDbgPrintLSPInfo (pContext,
                             ((tIsisLSPEntry *) (VOID *) (pLSP))->pu1LSP);
        pLSP = (UINT1 *) ((tIsisLSPEntry *) (VOID *) (pLSP))->pNext;
    }

    if (u1Level == ISIS_LEVEL1)
    {
        if (pContext->SysL1Info.LspTxQ.pHashBkts != NULL)
        {
            pLSP = (UINT1 *) ((tIsisLSPTxEntry *)
                              pContext->SysL1Info.LspTxQ.pHashBkts->pFirstElem);
        }
    }
    else
    {
        if (pContext->SysL2Info.LspTxQ.pHashBkts != NULL)
        {
            pLSP = (UINT1 *) ((tIsisLSPTxEntry *)
                              pContext->SysL2Info.LspTxQ.pHashBkts->pFirstElem);
        }
    }

    while (pLSP != NULL)
    {
        IsisDbgPrintLSPInfo (pContext,
                             ((tIsisLSPEntry *)
                              ((tIsisLSPTxEntry *) (VOID *) (pLSP))->pLSPRec)->
                             pu1LSP);
        pLSP =
            (UINT1 *) ((tIsisLSPEntry *) ((tIsisLSPTxEntry *) (VOID *) (pLSP))->
                       pLSPRec)->pNext;
    }

    DBP_EE ((ISIS_LGST, "DBG <X> : Exiting IsisDbgPrintLSPDB ()\n"));
}

/*******************************************************************************
 * Function    : IsisDbgPrintOctets ()
 * Description : This routine prints the octects from the given string at the
 *               Information Level
 * Input(s)    : pu1Buf  - pointer to the buffer containing the octet string
 *               u1Len   - length of the buffer
 *               pStr    - leading string to be printed before the octet
 *                         string.
 *               u1Fmt   - Format of the output          
 * Output(s)   : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintOctets (UINT1 *pu1Buf, UINT1 u1Len, UINT1 *pStr, UINT1 u1Fmt)
{
    char               *pBuf = ISIS_LGST;
    pBuf += SPRINTF (pBuf, "%s[ ", pStr);
    if (u1Fmt == ISIS_DISPLAY_STRING)
    {
        pBuf += SPRINTF (pBuf, "%s\n", pu1Buf);
    }
    else
    {
        while (u1Len-- > 0)
        {

            if (u1Fmt == ISIS_OCTET_STRING)
            {
                pBuf += SPRINTF (pBuf, "%02x ", *pu1Buf++);
            }
            else if (u1Fmt == ISIS_CHAR_STRING)
            {
                pBuf += SPRINTF (pBuf, "%c", *pu1Buf++);
            }
        }
    }

    if (u1Fmt == ISIS_OCTET_STRING)
    {
        pBuf += SPRINTF (pBuf, "]\n");
    }
    else
    {
        /* Note that there is a blank character in " ]".
         */

        pBuf += SPRINTF (pBuf, " ]\n");
    }
}

/*******************************************************************************
 * Function    : IsisDbgPrintOctetString ()
 * Description : This routine prints an octet string
 * Input(s)    : pu1Buf  - pointer to the buffer containing the octet string
 *               u1Len   - length of the buffer
 *               pStr    - leading string to be printed before the octet
 *                         string.
 *               u1Fmt   - Format of the output                    
 * Output(s)   : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintOctetString (UINT1 *pu1Buf, UINT1 u1Len, UINT1 *pStr, UINT1 u1Fmt)
{
    if (!(gau1IsisModLevelMask[ISIS_DBG_MODULE] & ISIS_PT_MASK))
    {
        return;
    }
    PRINTF ("%s[ ", pStr);

    if (u1Fmt == ISIS_DISPLAY_STRING)
    {
        PRINTF ("%s\n", pu1Buf);
    }
    else
    {
        while (u1Len-- > 0)
        {

            if (u1Fmt == ISIS_OCTET_STRING)
            {
                PRINTF ("%02x ", *pu1Buf++);
            }
            else if (u1Fmt == ISIS_CHAR_STRING)
            {
                PRINTF ("%c", *pu1Buf++);
            }
        }
    }

    if (u1Fmt == ISIS_OCTET_STRING)
    {
        PRINTF ("]\n");
    }
    else
    {
        /* Note that there is a blank character in " ]".
         */

        PRINTF (" ]\n");
    }
}

/*******************************************************************************
 * Function    : IsisDbgPrintPath ()
 * Description : This routine prints the entire database identified by the 
 *               'u1DBName'
 * Input(s)    : pContext  - Pointer to the Context
 *               u1DBame   - Identifies the database viz. TENT, Previous IP
 *                           PATH, Current IP PATH or OSI PATH
 *               u1Level   - Level of the database to be printed
 * Output(s)   : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisDbgPrintPath (tIsisSysContext * pContext, UINT1 u1DBName, UINT1 u1Level)
{
    CHAR                au1DestId[(6 * ISIS_IPV4_ADDR_LEN) + 1];
    UINT1               u1MetIdx = 0;
    UINT1               u1MetType = 0;
    UINT1               u1UtlPrnt = 0;
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pTravNext = NULL;
    tRBTree             RBTree;
    UINT1               u1MtIndex = 0;

    for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
    {
        switch (u1DBName)
        {
            case ISIS_OSI_PATH:
                RBTree = pContext->OSIPath;
                DBP_TD ((ISIS_LGST,
                         "\nInstance [ %u ] OSI PATH Database %s\n\n",
                         pContext->u4SysInstIdx, api1SysType[u1Level]));
                break;
            case ISIS_CURR_IP_PATH:
                RBTree =
                    (u1Level ==
                     ISIS_LEVEL1) ? pContext->SysL1Info.
                    ShortPath[u1MtIndex] : pContext->SysL2Info.
                    ShortPath[u1MtIndex];

                DBP_TD ((ISIS_LGST,
                         "\nInstance [ %u ] CURR IP PATH Database %s\n\n",
                         pContext->u4SysInstIdx, api1SysType[u1Level]));
                break;

            case ISIS_PREV_IP_PATH:
                RBTree = (u1Level == ISIS_LEVEL1)
                    ? pContext->SysL1Info.PrevShortPath[u1MtIndex]
                    : pContext->SysL2Info.PrevShortPath[u1MtIndex];
                DBP_TD ((ISIS_LGST,
                         "\nInstance [ %u ] IP PATH Database %s\n\n",
                         pContext->u4SysInstIdx, api1SysType[u1Level]));
                break;

            case ISIS_TENT:
                RBTree = pContext->Tent;
                DBP_TD ((ISIS_LGST,
                         "\nInstance [ %u ] TENT Database %s\n\n",
                         pContext->u4SysInstIdx, api1SysType[u1Level]));
                break;
            default:

                DBP_PT ((ISIS_LGST, "DBG <E> : Invalid Input for Database\n"));
                return;
        }

        DBP_TD ((ISIS_LGST, "Dest ID                  Metric  MetType        "
                 "DirIdx    MPS  \n"));
        DBP_TD ((ISIS_LGST, "------------------------------------------------"
                 "---------------\n"));

        /* Disable the UTL Module Trace Print while printing the PATH information
         * alone
         */

        u1UtlPrnt = gau1IsisModLevelMask[ISIS_UTL_MODULE];
        gau1IsisModLevelMask[ISIS_UTL_MODULE] = 0;

        pTrav = RBTreeGetFirst (RBTree);

        while (pTrav != NULL)
        {
            for (u1MetIdx = 0; u1MetIdx < ISIS_NUM_METRICS; u1MetIdx++)
            {
                if (pTrav->au2Metric[u1MetIdx] != ISIS_SPT_INV_METRIC)
                {
                    u1MetType = IsisUtlGetMetType (pContext, u1MetIdx);
                    ISIS_FORM_STR ((2 * ISIS_IPV4_ADDR_LEN), pTrav->au1DestID,
                                   au1DestId);
                    DBP_TD ((ISIS_LGST, "%-25s%-8u%-15s%-10u%-5u%-5s\n",
                             au1DestId,
                             pTrav->u4MetricVal,
                             ISIS_GET_METRIC_TYPE_STR (pContext, u1MetType),
                             pTrav->au4DirIdx[u1MetIdx],
                             pTrav->au1MaxPaths[u1MetIdx],
                             ISIS_GET_MPS_VAL_STR (pTrav->
                                                   au1MaxPaths[u1MetIdx])));
                }
            }
            pTravNext = pTrav->pNext;
            if (pTravNext == NULL)
            {
                pTravNext = RBTreeGetNext (RBTree, pTrav, NULL);
            }
            pTrav = pTravNext;
        }

        /* enable the UTL Module Trace Print as it was before
         */

        gau1IsisModLevelMask[ISIS_UTL_MODULE] = u1UtlPrnt;

        /* If multi-topology is not supported, Curr IP path and Prev IP path
         * will be there for single topology alone. Hence breaking the loop. */
        if (pContext->u1IsisMTSupport == ISIS_FALSE)
        {
            break;
        }
    }
}

/*******************************************************************************
   * Function    : IsisDbgPrintTime ()
   * Description : This routine prints the entire database identified by the
   *               'u1DBName'
   * Input(s)    : pContext  - Pointer to the Context
   *               u1DBame   - Identifies the database viz. TENT, Previous IP
   *                           PATH, Current IP PATH or OSI PATH
   *               u1Level   - Level of the database to be printed
   * Output(s)   : None
   * Globals     : Not referred or modified
   * Returns     : VOID
   ******************************************************************************/

PUBLIC VOID
IsisDbgPrintTime (char *pBuf)
{
    time_t              T;
    struct tm          *info = NULL;
    time (&T);
    info = localtime (&T);
    if (info != NULL)
    {
        SPRINTF (pBuf, "\r %02d:%02d:%02d ", info->tm_hour, info->tm_min,
                 info->tm_sec);
    }
    return;
}
