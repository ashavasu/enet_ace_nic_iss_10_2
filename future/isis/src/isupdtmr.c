/******************************************************************************
 * 
 *  Copyright (C) Future Software Limited, 2001
 *
 *  $Id: isupdtmr.c,v 1.20 2017/09/11 13:44:09 siva Exp $
 *
 *  Description: This file contains the Timer Routines associated with
 *               Update Module. 
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE VOID        IsisUpdChkAndTxPurgedLSPs (tIsisSysContext *,
                                               tIsisCktEntry *, UINT1, UINT2 *);

/*******************************************************************************
 * Function    : IsisUpdProcMaxAgeTimeOut ()
 * Description : This routine processes each of the LSPs included in the
 *               pTmrInfo and sets the RLT to '0' and length of the PDU to 
 *               header length. It deletes the LSP from the Remaining Life Time
 *               Timer list and inserts the same into the Zero Age Timer list
 * Input (s)   : pContext - Pointer to System context
 *             : pTmrInfo - Pointer to Timer Info
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcMaxAgeTimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrInfo)
{
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1               u1Level = ISIS_UNKNOWN;
    UINT1               u1AuthType = 0;
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    UINT2               u2Len = 0;
    UINT4               u4RcvdSeqNum = 0;
    tIsisTmrHdr        *pTmrHdr = NULL;
    tIsisLSPEntry      *pLSPRec = NULL;
    tIsisLSPEntry      *pRec = NULL;
    tIsisLSPEntry      *pNextRec = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    INT4                i4Ret = ISIS_FAILURE;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcMaxAgeTimeOut ()\n"));

    /* Refer Sec 7.3.16.3 of ISO 10589 */

    /* Detach the Info from Timer EC Block
     */

    pTmrHdr = (tIsisTmrHdr *) pTmrInfo->pInfo;
    pLSPRec = pTmrHdr->pNxtTimerLink;

    pTmrInfo->pInfo = NULL;
    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrInfo);

    while (pLSPRec != NULL)
    {
/* Remaining Life Time Timer has expired. Set the RLT to '0'
 *          */
        ISIS_FORM_STR (ISIS_LSPID_LEN, pLSPRec->pu1LSP, acLSPId);
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : LSP Max Age Timer expired for [%s] [%s]\n", acLSPId,
                 ISIS_GET_LEVEL_STR (u1Level)));
        ISIS_EXTRACT_SEQNUM_FROM_LSP (pLSPRec->pu1LSP, u4RcvdSeqNum);

        u1Level = (UINT1)
            ((ISIS_EXTRACT_PDU_TYPE (pLSPRec->pu1LSP) == ISIS_L1LSP_PDU)
             ? ISIS_LEVEL1 : ISIS_LEVEL2);

        u1AuthType =
            IsUpdAddAuthTlvPrgLSP (pContext, pLSPRec->pu1LSP, u1Level,
                                   u4RcvdSeqNum);

        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pLSPRec->pu1LSP, u2Len);

        IsisUpdAddCS (pLSPRec->pu1LSP, u2Len);

        ISIS_SET_LSP_RLT (pLSPRec->pu1LSP, 0);

        /* Retain only the PDU Header by setting the length of the PDU to
         *          * header length
         *                   */
        if (u1AuthType == 0)
        {
            ISIS_SET_CTRL_PDU_LEN (pLSPRec->pu1LSP,
                                   ((UINT2)
                                    (((tIsisComHdr *) pLSPRec->pu1LSP)->
                                     u1HdrLen)));
        }

        ISIS_EXTRACT_LSPID_FROM_LSP (pLSPRec->pu1LSP, au1LSPId);

        pNextRec = pLSPRec->pNxtTimerLink;

        /* Delete the LSP Entry from the Max Age List and Insert into the 
         * zero age List since this LSP must be maintained till ZERO age
         */

        ISIS_DEL_LSP_TIMER_NODE (pLSPRec);

        /* start the zero timer 
         */

        IsisUpdStartLSPTimer (pContext, pLSPRec, ISIS_ECT_ZERO_AGE,
                              ISIS_ZERO_AGE_INT);

        /* Get the LSP record along with its location and the Hash bucket
         * pointers which are required for further processing
         */

        pRec = IsisUpdGetLSP (pContext, au1LSPId, u1Level, &pPrevRec,
                              &pHashBkt, &u1Loc);

        /* Schedule the LSP for transmision on All circuits
         */
        if (pRec != NULL)
        {
            i4Ret =
                IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec, (VOID *) pRec,
                                    NULL, u1Loc, pHashBkt, ISIS_ALL_CKT);
            if (i4Ret == ISIS_MEM_FAILURE)
            {
                IsisUpdDeleteLSP (pContext, u1Level, au1LSPId);
            }
        }
        ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) ISIS_LSPID_LEN,
                           "UPD <T> : RLT Expired For LSP\t",
                           ISIS_OCTET_STRING);
        pLSPRec = pNextRec;
    }

    pTmrHdr->pNxtTimerLink = NULL;
    pTmrHdr->pPrevTimerLink = NULL;

    ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pTmrHdr);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcMaxAgeTimeOut ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcZeroAgeTimeOut ()
 * Description : This routine deletes all the LSPs linked in the given pTmrRec. 
 * Input (s)   : pContext - Pointer to System Context
 *               pTmrRec  - Pointer to Timer Equivalence Class Timer Info
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcZeroAgeTimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrRec)
{
    UINT1               u1Level = ISIS_UNKNOWN;
    tIsisTmrHdr        *pHead = NULL;
    tIsisLSPEntry      *pZeroAgeLSP = NULL;
    tIsisLSPEntry      *pNextLSP = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcZeroAgeTimeOut ()\n"));

    /* Refer Sec 7.3.16.4 of ISO 10589 */

    /* NOTE: Whenever LSPs are linked we will have a header record. Hence get
     * the first LSP linked after the header record
     */

    pHead = (tIsisTmrHdr *) (pTmrRec->pInfo);
    pZeroAgeLSP = pHead->pNxtTimerLink;
    pTmrRec->pInfo = NULL;

    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrRec);

    /* Travel through the list of LSPs using the Timer Links and delete each of
     * the LSPs found, since LSPs can be deleted once Zero Age Time has elapsed
     */

    while (pZeroAgeLSP != NULL)
    {
        pNextLSP = pZeroAgeLSP->pNxtTimerLink;

        u1Level = (UINT1)
            ((ISIS_EXTRACT_PDU_TYPE (pZeroAgeLSP->pu1LSP) == ISIS_L1LSP_PDU)
             ? ISIS_LEVEL1 : ISIS_LEVEL2);
        ISIS_DBG_PRINT_ID ((pZeroAgeLSP->pu1LSP + ISIS_OFFSET_LSPID),
                           (UINT1) ISIS_LSPID_LEN,
                           "UPD <T> : Deleted LSP\t", ISIS_OCTET_STRING);
        ISIS_FORM_STR (ISIS_LSPID_LEN, pZeroAgeLSP->pu1LSP, acLSPId);
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Zero Age Timer expired for LSP [%s] [%s]\n",
                 acLSPId, ISIS_GET_LEVEL_STR (u1Level)));
        IsisUpdDeleteLSP (pContext, u1Level,
                          (pZeroAgeLSP->pu1LSP + ISIS_OFFSET_LSPID));
        pZeroAgeLSP = pNextLSP;
    }

    ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pHead);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcZeroAgeTimeOut" " ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcL1LSPGenTimeOut ()
 * Description : This routine triggers the transmission of L1 SelfLSPs. If LSP
 *               Generation count equals the Max LSP Generation Interval count,
 *               then it also triggers the validation of LSP databases
 * Input (s)   : pContext - Pointer to System Context
 * Output (s)  : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcL1LSPGenTimeOut (tIsisSysContext * pContext)
{
    UINT2               u2Time = 0;
    tIsisTimer         *pTimer = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNZLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcL1LSPGenTimeOut ()\n"));

    pTimer = &pContext->SysL1Info.SysLSPGenTimer;
    u2Time = pContext->SysActuals.u2SysMinL1LSPGenInt;
    pTimer->pContext = pContext;

    /* if the gu1RelqFlag is set then don't process this timer just restart it
     * because the LSP Database should not be disturbed while i am still in the
     * process of computing the SPT
     */
    if (gu1RelqFlag != ISIS_DECN_WAIT)
    {
        /* If there are no active adjacencies on the system,
         * Just restart the L1 LSP Generation Timer.
         */

        if (pContext->CktTable.u4NumL1Adjs == 0)
        {
            IsisTmrStartTimer (pTimer, ISIS_L1LSP_GEN_TIMER, u2Time);
            return;
        }

        /* Refer Sec 7.3.7 and 7.3.8 of ISO 10589 Specification
         */

        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;

        /* Increment the count, which is also used for keeping track of Max LSP
         * Generation Interval
         */

        pContext->SysL1Info.u1LSPGenCount++;

        /* Transmit all Non-Pseudonode LSPs
         */

        if (pNSNLSP != NULL)
        {
            /* REFERENCE : Sec 7.3.2 - a, b, & c.
             */

            IsisUpdChkAndTxSelfLSP (pContext, pNSNLSP->pZeroLSP, ISIS_LEVEL1);

            pNZLSP = pNSNLSP->pNonZeroLSP;
            while (pNZLSP != NULL)
            {
                IsisUpdChkAndTxSelfLSP (pContext, pNZLSP, ISIS_LEVEL1);
                pNZLSP = pNZLSP->pNext;
            }
        }

        /* Transmit all Pseudonode LSPs
         */

        while (pSNLSP != NULL)
        {
            /* We can have more than one Pseudonode LSP if the local system is a DIS
             * on more than one Broadcast circuit
             */

            if ((pSNLSP->pCktEntry != NULL)
                && (pSNLSP->pCktEntry->pL1CktInfo != NULL)
                && (pSNLSP->pCktEntry->pL1CktInfo->bIsDIS == ISIS_TRUE))
            {
                IsisUpdChkAndTxSelfLSP (pContext, pSNLSP->pZeroLSP,
                                        ISIS_LEVEL1);

                pNZLSP = pSNLSP->pNonZeroLSP;
                while (pNZLSP != NULL)
                {
                    IsisUpdChkAndTxSelfLSP (pContext, pNZLSP, ISIS_LEVEL1);
                    pNZLSP = pNZLSP->pNext;
                }
            }
            pSNLSP = pSNLSP->pNext;
        }

        /* We keep track of the number of times the Min LSP Generation Timer
         * expires. A specified numberof Min LSP Generation intervals adds up to Max
         * LSP Generation interval, which can be calculated as 
         *
         * Max Gen Count (M) = Max Gen Interval / Min Gen Interval
         *
         * If LSP Generation count is 'M' then it means that the Maximum Generation
         * interval is reached. If so we will reset the Generation Count, and also
         * proceed with the Validation of databases
         * 
         * Refer Sec 7.3.18 of ISO 10589
         */

        if (pContext->SysL1Info.u1LSPGenCount ==
            ISIS_MAX_GEN_COUNT (pContext, ISIS_LEVEL1))
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Max L1 LSP Generation Interval Reached [ %u ] seconds\n",
                     pContext->SysL1Info.u1LSPGenCount *
                     pContext->SysActuals.u2SysMinL1LSPGenInt));
            pContext->SysL1Info.u1LSPGenCount = 0;

            IsisUpdValLSPDataBase (pContext, ISIS_LEVEL1);
        }
    }

    /* Restart the Min LSP Generation timer
     */

    IsisTmrStartTimer (pTimer, ISIS_L1LSP_GEN_TIMER, u2Time);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcL1LSPGenTimeOut ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcL2LSPGenTimeOut ()
 * Description : This routine triggers the transmission of L2 SelfLSPs. If LSP
 *               Generation count equals the Max LSP Generation Interval count,
 *               then it also triggers the validation of LSP databases
 * Input (s)   : pContext - Pointer to System Context
 * Output (s)  : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcL2LSPGenTimeOut (tIsisSysContext * pContext)
{
    UINT2               u2Time = 0;
    tIsisTimer         *pTimer = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNZLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcL2LSPGenTimeOut ()\n"));

    pTimer = &pContext->SysL2Info.SysLSPGenTimer;
    u2Time = pContext->SysActuals.u2SysMinL2LSPGenInt;
    pTimer->pContext = pContext;

    /* If there are no active adjacencies on the system,
     * Just restart the L1 LSP Generation Timer.
     */

    /* if the gu1RelqFlag is set then don't process this timer just restart it
     * because the LSP Database should not be disturbed while i am still in the
     * process of computing the SPT
     */
    if (gu1RelqFlag != ISIS_DECN_WAIT)
    {
        if (pContext->CktTable.u4NumL2Adjs == 0)
        {
            IsisTmrStartTimer (pTimer, ISIS_L2LSP_GEN_TIMER, u2Time);
            return;
        }

        /* Refer Sec 7.3.9 and 7.3.10 of ISO 10589 Specification
         */

        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;

        /* Increment the count, which is also used for keeping track of Max LSP
         * Generation Interval
         */

        pContext->SysL2Info.u1LSPGenCount++;

        /* Transmit all Non-Pseudonode LSPs
         */

        if (pNSNLSP != NULL)
        {
            /* REFERENCE : Sec 7.3.2 - a, b, & c.
             */

            IsisUpdChkAndTxSelfLSP (pContext, pNSNLSP->pZeroLSP, ISIS_LEVEL2);

            pNZLSP = pNSNLSP->pNonZeroLSP;
            while (pNZLSP != NULL)
            {
                IsisUpdChkAndTxSelfLSP (pContext, pNZLSP, ISIS_LEVEL2);
                pNZLSP = pNZLSP->pNext;
            }
        }

        /* Transmit all Pseudonode LSPs
         */

        while (pSNLSP != NULL)
        {
            /* We can have more than one Pseudonode LSP if the local system is a DIS
             * on more than one Broadcast circuit
             */
            if ((pSNLSP->pCktEntry != NULL)
                && (pSNLSP->pCktEntry->pL2CktInfo != NULL)
                && (pSNLSP->pCktEntry->pL2CktInfo->bIsDIS == ISIS_TRUE))
            {
                IsisUpdChkAndTxSelfLSP (pContext, pSNLSP->pZeroLSP,
                                        ISIS_LEVEL2);

                pNZLSP = pSNLSP->pNonZeroLSP;
                while (pNZLSP != NULL)
                {
                    IsisUpdChkAndTxSelfLSP (pContext, pNZLSP, ISIS_LEVEL2);
                    pNZLSP = pNZLSP->pNext;
                }
            }
            pSNLSP = pSNLSP->pNext;
        }

        /* We keep track of the number of times the Min LSP Generation Timer
         * expires. A specified numberof Min LSP Generation intervals adds up to Max
         * LSP Generation interval, which can be calculated as 
         *
         * Max Gen Count (M) = Max Gen Interval / Min Gen Interval
         *
         * If LSP Generation count is 'M' then it means that the Maximum Generation
         * interval is reached. If so we will reset the Generation Count, and also
         * proceed with the Validation of databases
         * 
         * Refer Sec 7.3.18 of ISO 10589
         */

        if (pContext->SysL2Info.u1LSPGenCount ==
            ISIS_MAX_GEN_COUNT (pContext, ISIS_LEVEL2))
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Max L2 LSP Generation Interval Reached [ %u ] seconds\n",
                     pContext->SysL2Info.u1LSPGenCount *
                     pContext->SysActuals.u2SysMinL2LSPGenInt));
            pContext->SysL2Info.u1LSPGenCount = 0;
            IsisUpdValLSPDataBase (pContext, ISIS_LEVEL2);
        }
    }

    IsisTmrStartTimer (pTimer, ISIS_L2LSP_GEN_TIMER, u2Time);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcL2LSPGenTimeOut ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcL1WaitingTimeOut ()
 * Description : This routine clears the L1 Overload bit and Marks LSP 0 as
 *               modified so that it will be scheduled for transmission in the
 *               next interval
 * Input(s)    : pContext - Pointer to System Context
 * Output(s)   : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcL1WaitingTimeOut (tIsisSysContext * pContext)
{
    tIsisEvtLSPDBOL    *pEvtLSPDBOLR = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcL1WaitingTimeOut ()\n"));

    pEvtLSPDBOLR = (tIsisEvtLSPDBOL *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtLSPDBOL));

    if (pEvtLSPDBOLR != NULL)
    {
        pEvtLSPDBOLR->u1EvtID = ISIS_EVT_LSP_DBOL_RECOV;
        pEvtLSPDBOLR->u1Level = ISIS_LEVEL1;

        IsisUtlSendEvent (pContext, (UINT1 *) pEvtLSPDBOLR,
                          sizeof (tIsisEvtLSPDBOL));
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcL1WaitingTimeOut ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcL2WaitingTimeOut ()
 * Description : This routine clears the L2 Overload bit and Marks LSP 0 as
 *               modified so that it will be scheduled for transmission in the
 *               next interval
 * Input(s)    : pContext - Pointer to System Context
 *               u1Level  - Level of the LSP Database 
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcL2WaitingTimeOut (tIsisSysContext * pContext)
{
    tIsisEvtLSPDBOL    *pEvtLSPDBOLR = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcL2WaitingTimeOut ()\n"));

    pEvtLSPDBOLR = (tIsisEvtLSPDBOL *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtLSPDBOL));
    if (pEvtLSPDBOLR != NULL)
    {
        pEvtLSPDBOLR->u1EvtID = ISIS_EVT_LSP_DBOL_RECOV;
        pEvtLSPDBOLR->u1Level = ISIS_LEVEL2;

        IsisUtlSendEvent (pContext, (UINT1 *) pEvtLSPDBOLR,
                          sizeof (tIsisEvtLSPDBOL));
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcL2WaitingTimeOut ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcPSNPTimeOut ()
 * Description : This routine retrieves the bit mask in the pTmrData, extracts
 *               the circuit Id from the set bits. If the extracted circuit is a
 *               BC circuit and if the local system is a DIS on that cicuit, all
 *               the pending PSNPs on the circuit will be released since a DIS
 *               need not transmit PSNPs. Otherwise PSNPs are constructed from
 *               the LETLVs held in the circuit and transmitted over the circuit
 * Input(s)    : pContext - Pointer to System Context
 *               pTmrData - Pointer to Timer data 
 *               u1Level  - Level of the PSNP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcPSNPTimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrData,
                        UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1              *pu1RcvdBitFlag = NULL;
    UINT4               u4CktId = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevelRec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcPSNPTimeOut ()\n"));

    /* Refer Sec 7.3.15.4 of ISO 10589 Specification
     */

    pu1RcvdBitFlag = pTmrData->pInfo;
    pTmrData->pInfo = NULL;

    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrData);

    /* pTmrData->pInfo holds a bit mask which has bits SET corresponding to all
     * the circuits over which PSNPs are to be transmitted.Extract bit by bit,
     * calculate the circuit Index from the bit and transmit PSNPs, if any, over
     * the circuit
     */

    while ((i4RetVal = IsisUtlGetNextIdFromBPat (&pu1RcvdBitFlag,
                                                 &u4CktId)) == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktId, &pCktRec))
            != ISIS_SUCCESS)
        {
            /* Possible, since manager could have deleted the circuit
             */

            TMP_PT ((ISIS_LGST,
                     "TMR <T> : PSNP Timer Fired, Circuit Does Not Exist - Index [ %u ]\n",
                     u4CktId));
            continue;
        }

        if (u1Level == ISIS_LEVEL1)
        {
            pCktLevelRec = pCktRec->pL1CktInfo;
        }
        else
        {
            pCktLevelRec = pCktRec->pL2CktInfo;
        }
        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            if ((pCktLevelRec) && (pCktLevelRec->u1CSNPSent == ISIS_ZERO))
            {
                IsisUpdEnqueueP2PCktChgEvt (pCktRec->pContext, ISIS_CKT_UP,
                                            pCktRec->u4CktIdx);
                if (pCktLevelRec != NULL)
                {
                    ISIS_ADJ_SET_ECTID (pCktLevelRec, 0);
                }
                continue;
            }
        }
        if ((pCktLevelRec != NULL) && (pCktLevelRec->pPSNP != NULL))
        {
            if ((pCktRec->u1CktType != ISIS_BC_CKT)
                || (pCktLevelRec->bIsDIS == ISIS_FALSE))
            {
                IsisUpdConstructAndTxPSNP (pContext, &(pCktLevelRec->pPSNP),
                                           pCktRec, u1Level);
            }
            else
            {
                /* The circuit is a BC circuit and the local system is a DIS on
                 * the circuit. We don't need to send any PSNPs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Ignoring All PSNPs - Reason [ Circuit [ BC ], DIS [ Local Sys ] ], Circuit Index [ %u ]\n",
                         u4CktId));
                IsisAdjDelCktLvlPSNPs (pCktLevelRec);
            }
        }
        if (pCktLevelRec != NULL)
        {
            ISIS_ADJ_SET_ECTID (pCktLevelRec, 0);
        }

    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcPSNPTimeOut ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcLSPTxTimeOut ()
 * Description : This routine transmits LSPs from the transmission queue. 
 *               The number of LSPs transmitted is limited by the throttle
 *               interval. The LSP from where the transmission starts is
 *               different for each circuit and is held in a pointer
 * Input(s)    : pContext  - Pointer to System Context
 *               pTmrData  - Pointer to timer data 
 *               u1Level   - Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcLSPTxTimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrData,
                         UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Mask = 0;
    UINT1               u1Loc = ISIS_LOC_TX;
    UINT1               u1Flag = 0;
    UINT1              *pu1TIdx = 0;
    UINT1               u1TmrType = 0;
    UINT1              *pu1RcvdBitFlag = NULL;
    UINT2               u2RLT = 0;
    UINT2               u2Len = 0;
    UINT2               u2TxQLen = 0;
    UINT2               u2LSPTxCount = 0;
    UINT2               u2MaxLSPTxCount = 0;
    UINT4               u4CktId = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLvlRec = NULL;
    tIsisLSPTxEntry    *pTxLSP = NULL;
    tIsisLSPTxEntry    *pPrevRec = NULL;
    tIsisLSPTxEntry    *pNextTxLSP = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisHashBucket    *pNextHash = NULL;
    tIsisEvtLspSizeTooLarge *pEvtLargeLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcLSPTxTimeOut ()\n"));

    /* Refer Sec 7.3.15.4 of ISO 10589 Specification
     */

    /* pTmrData->pInfo holds the bit mask associated with the circuits over
     * which LSPs are to be transmitted. Using this bit mask, get the
     * appropriate circuit indices and start transmitting LSPsover each of the
     * circuit
     */

    pu1RcvdBitFlag = (UINT1 *) pTmrData->pInfo;
    pTmrData->pInfo = NULL;
    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrData);

    while ((i4RetVal = IsisUtlGetNextIdFromBPat (&pu1RcvdBitFlag,
                                                 &u4CktId)) == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktId, &pCktRec))
            != ISIS_SUCCESS)
        {
            TMP_PT ((ISIS_LGST,
                     "TMR <T> : LSP Tx Timer Fired, Circuit Does Not Exist -Index [ %u ] \n",
                     u4CktId));
            continue;
        }

        if (u1Level == ISIS_LEVEL1)
        {
            pCktLvlRec = pCktRec->pL1CktInfo;
            u1TmrType = ISIS_ECT_L1_LSP_TX;
            u2TxQLen = (UINT2) pContext->SysL1Info.LspTxQ.u4TotalMbrCount;
            pHashBkt = pContext->SysL1Info.LspTxQ.pHashBkts;
        }
        else
        {
            pCktLvlRec = pCktRec->pL2CktInfo;
            u1TmrType = ISIS_ECT_L2_LSP_TX;
            u2TxQLen = (UINT2) pContext->SysL2Info.LspTxQ.u4TotalMbrCount;
            pHashBkt = pContext->SysL2Info.LspTxQ.pHashBkts;
        }
        /* pu1TIdx is a combination of the Equivalence Class Timr Id and the
         * block Index, which is useful while stopping timers.
         */
        if (pCktLvlRec == NULL)
        {
            continue;            /*invalid level */
        }

        pu1TIdx = &pCktLvlRec->u1LSPTxTmrIdx;
        if (pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER)
        {
            IsisTmrStartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                                 1, pu1TIdx);
            continue;
        }
        pCktLvlRec->u2ThrtlCount++;

        if ((pCktLvlRec->u2ThrtlCount % pCktLvlRec->u4MinLspReTxInt) == 0)
        {
            IsisUpdChkAndTxPurgedLSPs (pContext, pCktRec, u1Level,
                                       &u2LSPTxCount);
        }

        if (pHashBkt == NULL)
        {
            IsisTmrStartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                                 1, pu1TIdx);
            continue;
        }

        /* Start transmitting LSPs from the marked LSP. This is because, if the
         * LSP database is huge, we may not be able to transmit all the LSPs in
         * a single shot due to Throttle restrictions. Hence we would transmit
         * only a limited number of LSPs based on the throttle and mark the LSP
         * to be transmitted in the next interval. Once all the LSPs are done,
         * we will wrap around to the first LSP and continue the same process
         */

        pTxLSP = pCktLvlRec->pMarkTxLSP;

        /* Get the number of LSPs that can be tranmsitted in one second
         *
         * Calculate the Maximum number of LSPs that can be sent in 1 sec.
         * Due to throtle restrictions, FutureISIS transmits 98% of the total
         * packets that can be transmitted in an interval as LSPs, 1% as either
         * CSNPs/PSNPs and 1% as Hello. This means in a total of 100 packets, 98
         * will be LSPs, either 1 CSNP/PSNP, and 1 Hello (which is a must).
         * Total number of packets that can be transmitted in an Interval is
         * given by the formula 1000 / Throttle Interval.
         *
         * E.g., If Throttle interval is 5 millisec, then in one second we can
         * send (1000/5) = 200 packets. Out of these 200 packets, we can send 
         * (98% of 200) = 196 LSPs, (1% of 200) = 2 CSNPs/PSNPs and (1 of 200) =
         * 2 Hello packets
         *
         * Refer Sec. 7.3.15.6, Notes 31 of ISO 10589
         */

        u2MaxLSPTxCount
            = (UINT2) (ISIS_MAX_LSP_TX_COUNT (pCktLvlRec->LspThrottleInt));

        if (pTxLSP == NULL)
        {
            /* pTxLSP becomes NULL when we have completed one loop through the
             * database tranmsitting all LSPs. Whether we can now wrap around
             * and start tranmsitting from the first LSP depends on the time
             * difference between the first transmitted and the last transmitted
             * (this is because of the minimum time delay between transmitting
             * the LSPs imposed by the specs). If the time difference is less
             * than the Minimum LSP Transmission interval then we cannot
             * transmit the first LSP again. We may have to wait till we reach
             * the Minimum LSP Transmission interval. Otherwise we can wrap
             * around and start transmitting from the beginning
             */

            if (((pCktLvlRec->u2ThrtlCount % pCktLvlRec->u4MinLspReTxInt) == 0)
                || ((u2TxQLen / u2MaxLSPTxCount)
                    >= (UINT2) pCktLvlRec->u4MinLspReTxInt))
            {
                /* We are now at the Retranmsit Interval and we have more LSPs
                 * than that will fit into a single interval. This means we
                 * would have taken more than 1 throttle interval for completely
                 * transmitting all LSPs from the database. Hence the very first
                 * LSP would have been transmitted before atleast two throttle
                 * intervals. Hence we can now wrap around and start
                 * transmitting from the first LSP again
                 */

                pTxLSP = pHashBkt->pFirstElem;
                u1Loc = ISIS_LOC_TX_FIRST;
            }
            else
            {
                /* This condition arises only if we have LSPs that can all be
                 * transmitted in a single interval. This means we cannot now
                 * transmit the first LSP till we reach the Min LSP Generation
                 * Interval. So start the timer and wait till we bang the
                 * interval line
                 */

                IsisTmrStartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                                     1, pu1TIdx);
                continue;
            }
        }
        else
        {
            /* Still some LSPs left in the queue for transmission. Continue
             * transmitting
             */

            IsisUpdGetLSPFromTxQ (pContext, (pTxLSP->pLSPRec->pu1LSP +
                                             ISIS_OFFSET_LSPID), u1Level,
                                  &pPrevRec, &pHashBkt, &u1Loc, &u1Flag);
        }

        while ((u2LSPTxCount <= u2MaxLSPTxCount) && (pTxLSP != NULL))
        {
            /* LSP Generation timers will be ticking even if a particular
             * circuit does not have any LSPs to be transmitted. Hence we may
             * have to check whether SRM Flag bit for the particular circuit is
             * set before transmitting LSPs on the circuit.
             */

            if (ISIS_NUM_BYTES (pCktRec->u4CktIdx) <= pTxLSP->u1SRMLen)
            {
                /* Number of Bytes required to hold the circuit index is less
                 * than the total number of bytes available in the SRM flag
                 * mask. Hence it may be posibble that the LSP is eligible for
                 * transmission on this circuit. Check whether the SRM flag bit
                 * corresponding to the circuit is set
                 */

                ISIS_BIT_MASK (pCktRec->u4CktIdx, u1Mask);

                if ((*(pTxLSP->pu1SRM + ISIS_BYTE_POS (pCktRec->u4CktIdx))
                     & u1Mask) != 0)
                {
                    /* The SRM Flag bit for the circuit is set. LSPs can be
                     * transmitted on this circuit
                     */

                    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pTxLSP->pLSPRec->pu1LSP,
                                                   u2Len);
                    /* Transmit LSP only when the LSP Len does not exceed
                     * circuit MTU
                     */

                    if (u2Len > pCktRec->u4CktMtu)
                    {
                        pEvtLargeLSP = (tIsisEvtLspSizeTooLarge *)
                            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                            sizeof (tIsisEvtLspSizeTooLarge));
                        if (pEvtLargeLSP != NULL)
                        {
                            pEvtLargeLSP->u1EvtID = ISIS_EVT_LSP_TOO_LARGE;
                            pEvtLargeLSP->u1SysLevel = u1Level;
                            pEvtLargeLSP->u2LSPSize = u2Len;
                            pEvtLargeLSP->u4CktIfIdx = pCktRec->u4CktIfIdx;
                            MEMCPY (pEvtLargeLSP->au1TrapLspID,
                                    (pTxLSP->pLSPRec->pu1LSP +
                                     ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);
                            IsisUtlSendEvent (pContext, (UINT1 *) pEvtLargeLSP,
                                              sizeof (tIsisEvtLspSizeTooLarge));
                        }
                        pPrevRec = pTxLSP;
                        pTxLSP = IsisUpdGetNextTxRec (pTxLSP, &pHashBkt);
                        u1Loc = ISIS_LOC_TX;
                        continue;
                    }

                    /* Note down the original RLT, since we may have to update
                     * it back once the transmission is complete
                     */

                    ISIS_EXTRACT_RLT_FROM_LSP (pTxLSP->pLSPRec->pu1LSP, u2RLT);

                    /* Remaining Life Time must be decremented by atleast the
                     * amount of time the LSP had been in memory before
                     * transmission. Update RLT before transmitting only if RLT
                     * is greater than zero
                     */

                    if (u2RLT > 0)
                    {
                        IsisUpdUpdateRLTime (pContext, pTxLSP->pLSPRec);
                    }
                    IsisTrfrTxData (pCktRec, pTxLSP->pLSPRec->pu1LSP, u2Len,
                                    u1Level, ISIS_HOLD_BUF, ISIS_FALSE);
                    ISIS_INCR_OUT_CTRL_PDU_STAT (pCktRec);
                    ISIS_INCR_SENT_LSP (pCktRec, u1Level);

                    /* Restore the RLT back into the LSP
                     */

                    ISIS_SET_LSP_RLT (pTxLSP->pLSPRec->pu1LSP, u2RLT);

                    /* Get the next LSP to be transmitted from the TxQ
                     */

                    pNextHash = pHashBkt;
                    pNextTxLSP = IsisUpdGetNextTxRec (pTxLSP, &pNextHash);

                    if (pCktRec->u1CktType == ISIS_BC_CKT)
                    {
                        /* LOGIC: u1Flag is used to indicate whether the LSP we
                         * are processing is a first element in the TxQ or it is
                         * any other element other than the first. If it is the
                         * first element, then u1Loc will be ISIS_LOC_TX_FIRST,
                         * else it will be ISIS_LOC_TX. 
                         *
                         * Suppose we have now transmitted the very first LSP
                         * whose location is ISIS_LOC_TX_FIRST. Then
                         * IsisUpdClearSRM () might have moved the LSP to
                         * Database if no more SRM flag bits are set. Then the
                         * current routine proceses the Next LSP 'pNextTxLSP'
                         * which is now the first element and hence u1Loc is
                         * still ISIS_LOC_TX_FIRST. If not we have transmitted 
                         * the first element and moved on to the next element 
                         * and hence u1Loc is now ISIS_LOC_TX.
                         */

                        u1Flag = IsisUpdClearSRM (pContext, pTxLSP, pPrevRec,
                                                  pCktRec, u1Loc, pHashBkt,
                                                  u1Level);
                        if (u1Flag != ISIS_TRUE)
                        {
                            /* The LSP still remains in the Transmission Queue
                             * and we still continue our transmissions from TxQ.
                             * Hence 'u1Loc' will still be ISIS_LOC_TX
                             */

                            u1Loc = ISIS_LOC_TX;
                            pPrevRec = pTxLSP;
                        }
                    }
                    else
                    {
                        /* This is a P2P circuit case, and we have transmitted
                         * the LSP. We don't clear the SRM flags on P2P circuits
                         * till we receive an Ack. Now we can mark u1Loc as
                         * ISIS_LOC_TX
                         */

                        u1Loc = ISIS_LOC_TX;
                        pPrevRec = pTxLSP;
                    }
                    pTxLSP = pNextTxLSP;
                    pHashBkt = pNextHash;
                    u2LSPTxCount++;
                }
                else
                {
                    /* The SRM Flag bit for the circuit is not set in the LSP.
                     * We can skip this LSP and proceed to the next one and
                     * hence update u1Loc to ISIS_LOC_TX
                     */

                    pPrevRec = pTxLSP;
                    pTxLSP = IsisUpdGetNextTxRec (pTxLSP, &pHashBkt);
                    u1Loc = ISIS_LOC_TX;
                }
            }
            else
            {
                /* Number of Bytes required to hold a bit in SRM flags for this
                 * circuit is greater than the current length of the SRM flags
                 * which means that the bit corresponding to this circuit is not
                 * set in the LSP under consideration. Skip and proceed with the
                 * next LSP and update u1Loc to ISIS_LOC_TX
                 */

                pPrevRec = pTxLSP;
                pTxLSP = IsisUpdGetNextTxRec (pTxLSP, &pHashBkt);
                u1Loc = ISIS_LOC_TX;
            }

        }

        /* Now that we have transmitted the maximum number of LSPs allowed in
         * this interval, mark the last LSP transmitted since we may have to
         * start the transmissions from this LSP in the next interval
         */

        pCktLvlRec->pMarkTxLSP = pTxLSP;
        IsisTmrStartECTimer (pContext, u1TmrType, pCktRec->u4CktIdx,
                             1, pu1TIdx);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSPTxTimeOut ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcCSNPTimeOut ()
 * Description : This routine based on the throttle count and Minimum
 *               Retransmission interval, schedules LSPs from Transmit Queue and
 *               Database for CSNP buffer construction. 
 *               P2P Circuits
 *               ------------
 *               In the case of P2P circuits, it starts the PSNP timer if all 
 *               the CSNPs pertaining to the circuit had been transmitted and 
 *               if any PSNPs are pending over the circuit. 
 *
 *               Broadcast Circuits
 *               ------------------
 *               If all CSNPs have been transmitted, then if throttle count is 
 *               an exact multiple of Minimum Retransmit interval, then it 
 *               starts a timer for the remaining duration (Throttle Count % 
 *               Minimum Retransmit Interval). If the total time required for
 *               transmitting all CSNPs is greater than the Minimum Retransmit
 *               interval, then when the TxQ and Database Marks become NULL, it
 *               resets the LSP Marks back to the first elements in the
 *               respective queues and resumes transmission
 *          
 *               Broadcast and P2P Circuits
 *               --------------------------
 *               It calculates the total number of CSNPs that can be 
 *               transmitted in the interval and transmits that many CSNPs over
 *               the circuit. 
 * Input(s)    : pContext - Pointer to System Context
 *               pTmrData - Pointer to timer data 
 *               u1Level  - Level of the CSNP
 * Output(s)   : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcCSNPTimeOut (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrData,
                        UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT2               u2Time = 0;
    UINT1               u1ECTId = 0;
    UINT1               u1Loc = 0;
    UINT1               u1Flag = 0;
    UINT1              *pu1CSNP = NULL;
    UINT1               au1Empty[ISIS_LSPID_LEN];
    UINT2               u2Len = 0;
    UINT2               u2CSNPInt = 0;
    UINT2               u2MaxCSNP = 0;
    UINT2               u2NumCSNP = 0;
    UINT2               u2CSNPTxCount = 0;
    tIsisCktLevel      *pCktLevelRec = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisLSPEntry      *pLSPDBMark = NULL;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisTmrCSNPHdr    *pCSNPInfo = NULL;
    tIsisLSPTxEntry    *pLSPTxQMark = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    UINT4               u4Duration = 0;

    /* Refer to Sec 7.3.15.3 of ISO 10589 Specification
     */

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcCSNPTimeOut ()\n"));

    /* pTmrData->pInfo holds CSNP related information. It includes the LSP
     * pointers from both TxQ and Database from where the current transmission
     * is supposed to proceed. It also has the pointer to circuit over which the
     * CSNPs are to be transmitted
     */

    pCSNPInfo = pTmrData->pInfo;
    pTmrData->pInfo = NULL;

    ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrData);

    /* Start transmitting from the LSP where we might have left off, due to
     * throttle restrictions, in the previous interval
     */

    /* NOTE: LSPs can be present either in TxQ and Database. Sec. 9.10 mentions
     * that LSPIDs included in the CSNP must be in a sorted order and hence we
     * may have to alternate between the Database and TxQ for getting LSPs in a
     * sorted order. That is why we need both pLSPTxQMark and pLSPDBMark
     */

    MEMSET (au1Empty, 0x00, ISIS_LSPID_LEN);
    if (MEMCMP (pCSNPInfo->au1LastTxId, au1Empty, ISIS_LSPID_LEN) != 0)
    {
        pLSPTxQMark = IsisUpdGetLSPFromTxQ (pContext, pCSNPInfo->au1LastTxId,
                                            u1Level, &pPrevTxRec, &pHashBkt,
                                            &u1Loc, &u1Flag);
        pLSPDBMark = IsisUpdGetLSPFromDB (pContext, pCSNPInfo->au1LastTxId,
                                          u1Level, &pPrevDbRec, &pHashBkt,
                                          &u1Loc, &u1Flag);
    }

    i4RetVal = IsisAdjGetCktRec (pContext, pCSNPInfo->u4CktIdx, &pCktRec);

    if (i4RetVal == ISIS_FAILURE)
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <E> : Invalid Circuit In CSNP Info - Circuit Index [ %u ]\n",
                 pCSNPInfo->u4CktIdx));
        return;
    }

    ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pCSNPInfo);

    if (pCktRec->u1CktType == ISIS_P2P_CKT)
    {
        /* P2P circuits will hold circuit level information in Level1 records,
         * except for statistics which is maintained in the respective records
         */

        pCktLevelRec = ISIS_GET_P2P_CKT_LEVEL (pCktRec);

        if (pCktLevelRec == NULL)
        {
            return;
        }
        /* Calculate the Maximum number of CSNPs that can be sent in 1 sec.
         * Due to throtle restrictions, FutureISIS transmits 98% of the total
         * packets that can be transmitted in an interval as LSPs, 1% as either
         * CSNPs/PSNPs and 1% as Hello. This means in a total of 100 packets, 98
         * will be LSPs, 1 CSNP/PSNP (either), and 1 Hello (which is a must).
         * Total number of packets that can be transmitted in an Interval is
         * given by the formula 1000 / Throttle Interval.
         *
         * E.g., If Throttle interval is 5 millisec, then in one second we can
         * send (1000/5) = 200 packets. Out of these 200 packets, we can send 
         * (98% of 200) = 196 LSPs, (1% of 200) = 2 CSNPs/PSNPs and (1 of 200) =
         * 2 Hello packets
         */

        u2NumCSNP = (UINT1) (ISIS_GET_MAX_CSNP_COUNT (pCktLevelRec->
                                                      LspThrottleInt));

        /* Calculate the total number of CSNPs required for all the LSPs in the
         * database
         */

        u2MaxCSNP = (UINT2) ((pContext->SysL1Info.LspDB.u4TotalMbrCount +
                              pContext->SysL1Info.LspTxQ.u4TotalMbrCount) /
                             ISIS_NUM_LSP_PER_CSNP (pContext, u1Level));
    }
    else if (pCktRec->u1CktType == ISIS_BC_CKT)
    {
        pCktLevelRec = (u1Level == ISIS_LEVEL1) ? pCktRec->pL1CktInfo
            : pCktRec->pL2CktInfo;

        if (pCktLevelRec == NULL)
        {
            return;
        }
        /* Calculate the Maximum number of CSNPs that can be sent in 1 sec.
         * Due to throtle restrictions, FutureISIS transmits 98% of the total
         * packets that can be transmitted in an interval as LSPs, 1% as either
         * CSNPs/PSNPs and 1% as Hello. This means in a total of 100 packets, 98
         * will be LSPs, 1 CSNP/PSNP (either), and 1 Hello (which is a must).
         * Total number of packets that can be transmitted in an Interval is
         * given by the formula 1000 / Throttle Interval.
         *
         * E.g., If Throttle interval is 5 millisec, then in one second we can
         * send (1000/5) = 200 packets. Out of these 200 packets, we can send 
         * (98% of 200) = 196 LSPs, (1% of 200) = 2 CSNPs/PSNPs and (1 of 200) =
         * 2 Hello packets
         */
        if (pCktLevelRec->LspThrottleInt > 0)
        {
            u2NumCSNP = (UINT1)
                (ISIS_GET_MAX_CSNP_COUNT (pCktLevelRec->LspThrottleInt));
        }

        /* Calculate the total number of CSNPs required for all the LSPs in the
         * database
         */

        u2MaxCSNP = (UINT2) ((pContext->SysL2Info.LspDB.u4TotalMbrCount +
                              pContext->SysL2Info.LspTxQ.u4TotalMbrCount) /
                             ISIS_NUM_LSP_PER_CSNP (pContext, u1Level));
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCSNPTimeOut ()\n"));
        return;

    }

    if ((pCktRec->u1CktType == ISIS_BC_CKT)
        && ((pCktLevelRec->bIsDIS != ISIS_TRUE)
            && (pContext->u1IsisGRRestartMode != ISIS_GR_HELPER)))
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Non-DIS On BC Circuit, CSNP TimeOut - Ignored - Circuit [%u]\n",
                 pCktRec->u4CktIdx));
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCSNPTimeOut ()\n"));
        return;
    }

    /* The interval at which CSNPs are supposed to be generated. This value is
     * used for calculating the wrap around conditions
     */

    u2CSNPInt = pCktLevelRec->CSNPInterval;

    if ((pLSPTxQMark == NULL) && (pLSPDBMark == NULL))
    {
        /* Either there are no LSPs in both the Transmit Queue or Database or
         * one cycle of transmission of CSNPs is over
         */

        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            /* CSNPs are transmitted over P2P circuits during initialisation.
             * Since the pLSPTxQMark and pLSPDBMark have both become NULL, we
             * can now start PSNP timer if there are any pending PSNP requests
             */

            if ((u1Level == ISIS_LEVEL1)
                && (pCktRec->pL1CktInfo->pPSNP != NULL))
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Starting PSNP Timer For L1 P2P Ckt\n"));
                u4Duration = (UINT4) pCktRec->pL1CktInfo->PSNPInterval;
                IsisTmrStartECTimer (pContext, ISIS_ECT_L1_PSNP,
                                     pCktRec->u4CktIdx, u4Duration, &u1ECTId);
                ISIS_ADJ_SET_ECTID (pCktRec->pL1CktInfo, u1ECTId);
            }
            else if ((u1Level == ISIS_LEVEL2)
                     && (pCktRec->pL2CktInfo->pPSNP != NULL))
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Starting PSNP Timer For L2 P2P Ckt\n"));

                u4Duration = (UINT4) pCktRec->pL2CktInfo->PSNPInterval;
                IsisTmrStartECTimer (pContext, ISIS_ECT_L2_PSNP,
                                     pCktRec->u4CktIdx, u4Duration, &u1ECTId);
                ISIS_ADJ_SET_ECTID (pCktRec->pL2CktInfo, u1ECTId);
            }
            else
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    ISIS_ADJ_SET_ECTID (pCktRec->pL1CktInfo, 0);
                }
                else if (u1Level == ISIS_LEVEL2)
                {
                    ISIS_ADJ_SET_ECTID (pCktRec->pL2CktInfo, 0);
                }
            }

            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdProcCSNPTimeOut ()\n"));
            return;
        }

        /* LOGIC: CSNPs can be transmitted every CNSP Interval specified by
         * u2CSNP Interval. Instead of having a separate timer, we use the
         * global one second timer and keep tranmsitting a chunk of CSNPs that
         * can be transmitted in a second. ISO Specification restricts the
         * transmission of CSNPs with a time difference i.e. a pre-configured
         * time interval must elapse before the same CSNP is transmitted.
         *
         * (u2MaxCSNP/u2NumCSNP) gives the total time required for transmitting
         * all the CSNPs we have. If this is more than the CSNP interval, then
         * we can start wrapping around and transmit the very first CSNP that we
         * might have transmitted a few CSNP intervals back. If not we have to
         * wait till the CSNP interval, before we start transmitting the very
         * first CSNP again
         */
        if (((pCktLevelRec->u2ThrtlCount % u2CSNPInt) == 0)
            || ((u2MaxCSNP / u2NumCSNP) > u2CSNPInt))
        {
            /* We are exactly at the CSNP Interval boundary and we have CSNPs
             * which might have taken more than CNSP Interval time for complete
             * transmission. This means we can wrap around and start
             * transmitting from the beginning again since the first CSNP would
             * have been transmitted more than CSNP interval seconds back
             */

            pLSPTxQMark = ISIS_UPD_FIRST_LSP_TX_ENTRY (pContext, u1Level);
            pLSPDBMark = ISIS_UPD_FIRST_LSP_DB_ENTRY (pContext, u1Level);
        }
        else
        {
            /* We have completely transmitted all CSNPs, but we are in the 
             * middle of a CSNP interval. This case will arise if time required
             * for transmitting all CNSPs is less than CSNP interval. Then we 
             * have to start a timer for the remaining time and wait till it 
             * expires before we can resume transmissions
             */

            u2Time = (UINT2) (u2CSNPInt -
                              (pCktLevelRec->u2ThrtlCount % u2CSNPInt));
            TMP_PT ((ISIS_LGST,
                     "TMR <T> : Starting CSNP Timer For [ %u ] Sec. Duration\n",
                     u2Time));
            u1ECTId = IsisUpdStartCSNPTimer (pContext, pCktRec, pLSPTxQMark,
                                             pLSPDBMark, u2Time, u1Level);
            ISIS_ADJ_SET_ECTID (pCktLevelRec, u1ECTId);

            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdProcCSNPTimeOut ()\n"));
            return;
        }
    }

    /* Go through the LSPs in TxQ and Database, construct and transmit CSNPs
     * upto to 'u2NumCSNP' which is the maximum CSNPs that can be transmitted in
     * this interval
     */

    while ((u2CSNPTxCount++ <= u2NumCSNP)
           && ((pLSPTxQMark != NULL) || (pLSPDBMark != NULL)))
    {
        /* Construct CSNPs from LSPs pointed to by pLSPDBMark and pLSPTxQMark
         * and transmit then over the circuit
         */

        i4RetVal = IsisUpdConstructCSNP (pContext, &pLSPTxQMark,
                                         &pLSPDBMark, &pu1CSNP, u1Level);

        if (i4RetVal == ISIS_SUCCESS)
        {
            ISIS_GET_2_BYTES (pu1CSNP, ISIS_OFFSET_PDULEN, u2Len);
            IsisTrfrTxData (pCktRec, pu1CSNP, u2Len, u1Level, ISIS_FREE_BUF,
                            ISIS_FALSE);
            ISIS_INCR_OUT_CTRL_PDU_STAT (pCktRec);
            ISIS_INCR_SENT_CSNP (pCktRec, u1Level);
        }
    }
    if (pContext->u1IsisGRRestartMode != ISIS_GR_HELPER)
    {
        if (pCktLevelRec->bIsDIS == ISIS_FALSE)
        {
            return;
        }
    }
    u1ECTId = IsisUpdStartCSNPTimer (pContext, pCktRec, pLSPTxQMark,
                                     pLSPDBMark, 1, u1Level);

    ISIS_ADJ_SET_ECTID (pCktLevelRec, u1ECTId);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCSNPTimeOut ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdStartLSPTimer ()
 * Description : This routine Inserts the given LSP into the Zero Age
 *               or Max Age LSP Timer List based on 'u1Type'. It first checks
 *               whether a Timer Record exists for the given type, If the record
 *               exists, the new LSP record is inserted into the list. If not a
 *               Record of type 'u1Type' is created. a header added and the new
 *               LSP is inserted.
 * Input (s)   : pContext - Pointer to System Context
 *               pLSPRec  - Pointer to LSP 
 *               u1Type   - Type of Timer (Max Age or Zero Age) 
 *             : u2Dur    - Timer Duration 
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * RETURN (s)  : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdStartLSPTimer (tIsisSysContext * pContext, tIsisLSPEntry * pLSPRec,
                      UINT1 u1Type, UINT2 u2Dur)
{
    INT4                i4RetVal = 0;
    UINT1               u1TId = 0;
    UINT2               u2RemTime = 0;
    tIsisTmrHdr        *pHead = NULL;
    tIsisTmrECBlk      *pTmrECRec = NULL;
    tIsisTmrECBlk      *pFirstRec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdStartLSPTimer ()\n"));

    TMP_PT ((ISIS_LGST, "TMR <T> : Starting LSP Timer For Type [ %u ]\n",
             u1Type));

    /* If the Time for which the timer is started is greater than maximum
     * Time, then note the remaining time
     */

    if (u2Dur > (ISIS_EC3_MAX_DUR - ISIS_TMR_EC3_TIME_UNIT))
    {
        u2RemTime =
            (UINT2) (u2Dur - (ISIS_EC3_MAX_DUR - ISIS_TMR_EC3_TIME_UNIT));

        /* Round off the Remaining time to the nearest multiple of
         * ISIS_TMR_EC3_TIME_UNIT
         */

        u2RemTime = (UINT2) (ISIS_DIVR (u2RemTime, ISIS_TMR_EC3_TIME_UNIT));
        u2RemTime *= ISIS_TMR_EC3_TIME_UNIT;
        u2Dur -= u2RemTime;
    }

    /* Retrieve the Correct EC timer block and the exact index into the EC timer
     * array. The current LSP must be inserted into the appropriate block which
     * depends on the duration of the timer to be started
     */

    i4RetVal = IsisTmrGetECBlk (pContext, u2Dur, u1Type, &pFirstRec, &u1TId);

    if (i4RetVal == ISIS_MATCH_REC)
    {
        /* Found a matching record where the LSP can be inserted. Insert the
         * LSP at the head
         */

        pHead = (tIsisTmrHdr *) pFirstRec->pInfo;

        if (pHead->pNxtTimerLink == NULL)
        {
            /* There are no LSPEntries in this header so we are adding the
             * first Entry
             */
            pHead->u4TStamp = ISIS_GET_CURR_TICK (pContext);
            pHead->pNxtTimerLink = pLSPRec;
            pLSPRec->pPrevTimerLink = (tIsisLSPEntry *) pHead;
            pLSPRec->pNxtTimerLink = NULL;
        }
        else
        {
            pLSPRec->pNxtTimerLink = pHead->pNxtTimerLink;
            pLSPRec->pPrevTimerLink = (tIsisLSPEntry *) pHead;
            pHead->pNxtTimerLink->pPrevTimerLink = pLSPRec;
            pHead->pNxtTimerLink = pLSPRec;
        }
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStartLSPTimer ()\n"));
        return;
    }

    /* There are no blocks of the given type. Create a record of the given type
     * and add a header before inserting the LSP
     */

    pTmrECRec = (tIsisTmrECBlk *) ISIS_MEM_ALLOC (ISIS_BUF_ECTS,
                                                  sizeof (tIsisTmrECBlk));
    if ((pTmrECRec == NULL))
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Timer Record\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_ECTS);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStartLSPTimer ()\n"));
        return;
    }
    pHead = (tIsisTmrHdr *) ISIS_MEM_ALLOC (ISIS_BUF_ECTI,
                                            sizeof (tIsisTmrHdr));
    if (pHead == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Timer Header\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrECRec);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_ECTI);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStartLSPTimer ()\n"));
        return;
    }

    /* Record the Time Stamp which is useful for updating Remaining Life
     * Time values when ever the LSPs from this list are re-transmitted
     */

    pHead->u4TStamp = ISIS_GET_CURR_TICK (pContext);

    /* Insert the LSP into the timer List
     */

    pHead->pNxtTimerLink = pLSPRec;
    pHead->pPrevTimerLink = NULL;
    pLSPRec->pPrevTimerLink = (tIsisLSPEntry *) pHead;

    pLSPRec->pNxtTimerLink = NULL;

    /* Fill the timer record and insert the record 
     * at end
     */

    pTmrECRec->u1Type = u1Type;
    pTmrECRec->pInfo = (VOID *) pHead;
    pTmrECRec->u2RemTime = u2RemTime;
    pTmrECRec->pNext = NULL;

    if (pFirstRec != NULL)
    {
        /* pFirstRec Non-NULL means that we would have to travel till the list
         * and insert the current record at the end of the list
         */

        while (pFirstRec->pNext != NULL)
        {
            pFirstRec = pFirstRec->pNext;
        }

        pFirstRec->pNext = pTmrECRec;
    }
    else
    {
        /* Since there are no timer blocks available in the given EC timer
         * block at the selected index, initilaise the head of the EC block
         * with the new record inserted
         */

        IsisTmrSetECBlk (pContext, u1TId, pTmrECRec);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStartLSPTimer ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdStartCSNPTimer ()
 * Description : This routine Inserts the given LSP into the Zero Age
 *               or Max Age LSP Timer List
 * Input (s)   : pContext    - Pointer to System Context
 *               pCktRec     - Pointer to Circuit Record 
 *               pLSPTxQMark - Pointer to the last LSP included in the CSNP
 *                             from the Transmission Queue
 *               pLSPDBMark  - Pointer to the last LSP included in the CSNP
 *                             from the LSP Database
 *               u2Dur       - Timer Duration 
 *               u1Level     - Level of Circuit (Level1 oe Level2)
 * Output (s)  : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID
 ******************************************************************************/

PUBLIC UINT1
IsisUpdStartCSNPTimer (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                       tIsisLSPTxEntry * pLSPTxQMark,
                       tIsisLSPEntry * pLSPDBMark, UINT2 u2Dur, UINT1 u1Level)
{
    UINT1               u1ECTId = 0;
    tIsisTmrECBlk      *pTmrRec = NULL;
    tIsisTmrECBlk      *pTmrECRec = NULL;
    tIsisTmrCSNPHdr    *pCSNPInfo = NULL;
    UINT4               u4Duration = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdStartCSNPTimer ()\n"));

    if (((u1Level == ISIS_LEVEL2) && (pCktRec->pL2CktInfo->bIsDIS == ISIS_TRUE)
         && (pCktRec->pL2CktInfo->u1ECTId != 0)) ||
        ((u1Level == ISIS_LEVEL1) && (pCktRec->pL1CktInfo->bIsDIS == ISIS_TRUE)
         && (pCktRec->pL1CktInfo->u1ECTId != 0)))
    {
        IsisUpdStopCSNPTimer (pContext, pCktRec, u1Level);
    }

    pCSNPInfo = (tIsisTmrCSNPHdr *) ISIS_MEM_ALLOC (ISIS_BUF_ECTI,
                                                    sizeof (tIsisTmrCSNPHdr));

    if (pCSNPInfo == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : CSNP Header\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_ECTI);

        /* Timer Start up Failed. Hence ECTID is ZERO
         */

        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStartCSNPTimer ()\n"));
        return 0;
    }

    pTmrECRec = (tIsisTmrECBlk *) ISIS_MEM_ALLOC (ISIS_BUF_ECTS,
                                                  sizeof (tIsisTmrECBlk));

    if (pTmrECRec == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : EC Timer Record\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_ECTS);

        /* Timer Start up Failed. Hence ECTID is ZERO
         */
        ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pCSNPInfo);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStartCSNPTimer ()\n"));
        return 0;
    }

    /* Insert a separate CSNP header for each of the circuit
     */

    if ((pLSPTxQMark != NULL) && (pLSPDBMark != NULL))
    {
        if (MEMCMP ((pLSPTxQMark->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                    (pLSPDBMark->pu1LSP + ISIS_OFFSET_LSPID),
                    ISIS_LSPID_LEN) > 0)
        {
            MEMCPY (pCSNPInfo->au1LastTxId,
                    (pLSPDBMark->pu1LSP + ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);
        }
        else
        {
            MEMCPY (pCSNPInfo->au1LastTxId,
                    (pLSPTxQMark->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                    ISIS_LSPID_LEN);
        }
    }
    else if (pLSPTxQMark != NULL)
    {
        MEMCPY (pCSNPInfo->au1LastTxId,
                (pLSPTxQMark->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                ISIS_LSPID_LEN);
    }
    else if (pLSPDBMark != NULL)
    {
        MEMCPY (pCSNPInfo->au1LastTxId,
                (pLSPDBMark->pu1LSP + ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);
    }
    else
    {
        MEMSET (pCSNPInfo->au1LastTxId, 0x00, ISIS_LSPID_LEN);
    }

    pCSNPInfo->u4CktIdx = pCktRec->u4CktIdx;

    pTmrECRec->pInfo = (VOID *) pCSNPInfo;
    pTmrECRec->pNext = NULL;

    (u1Level == ISIS_LEVEL1) ? (pTmrECRec->u1Type = ISIS_ECT_L1_CSNP)
        : (pTmrECRec->u1Type = ISIS_ECT_L2_CSNP);

    /* Get the ECTID, which is a combination of the Equivalence Class Timer
     * ID and the Block Index, where the current CSNP block of duration
     * 'u2Dur' will fit. Get the pointer to the very first node in the chain
     * so that the current block can be inserted adjacent to the first node
     */
    u4Duration = (UINT4) u2Dur;

    IsisTmrGetECBlk (pContext, u4Duration, pTmrECRec->u1Type, &pTmrRec,
                     &u1ECTId);

    if (pTmrRec != NULL)
    {
        /* Both CSNPs and LSPs Timers use the same throttle count, for
         * scheduling CSNPs and LSPs respectively. But the throttle count
         * is incremented once every second in LSPTxTimeOut Routine. If
         * the LSPTxTimer Fires first, the throttle count will be
         * incremented and hence there is a probability that the throttle
         * count may not match the expected value for CSNP Transmissions.
         * Hence we make sure that CSNP timer is processed first by placing
         * CSNP Records at the head.
         *
         * For Eg. if CSNPs are to be scheduled every 10 secs. then at the
         * 10th second, during Time Out, the LSPTxTimeOut Routine increments
         * the throttle to 11, which will not match CSNP throttle count of
         * modulo 10.
         */

        pTmrECRec->pNext = pTmrRec;
    }
    IsisTmrSetECBlk (pContext, u1ECTId, pTmrECRec);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStartCSNPTimer ()\n"));
    return (u1ECTId);
}

/******************************************************************************
 * Function    : IsisUpdStopCSNPTimer ()
 * Description : This routine scans through the list of timers retrieved from 
 *               EC Timer Block and the Block Index (both extracted from the
 *               'u1ECTId') and finds the CSNP timer information corresponding
 *               to the given circuit. It deletes this timer information from
 *               the chain
 * Input (s)   : pContext - Pointer to System Context
 *               pCktRec  - Pointer to Circuit Record 
 *               u1Level  - Level of Circuit (Level1 oe Level2)
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdStopCSNPTimer (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                      UINT1 u1Level)
{
    UINT1               u1ECBlk = 0;
    UINT1               u1BlkIdx = 0;
    UINT1               u1ECTId = 0;
    UINT1               u1Type = 0;
    tIsisTmrECBlk     **ppTmrECRec = NULL;
    tIsisTmrECBlk      *pTravTmr = NULL;
    tIsisTmrECBlk      *pPrevTmr = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdStopCSNPTimer ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        u1Type = ISIS_ECT_L1_CSNP;
        u1ECTId = pCktRec->pL1CktInfo->u1ECTId;
    }
    else
    {
        u1Type = ISIS_ECT_L2_CSNP;
        u1ECTId = pCktRec->pL2CktInfo->u1ECTId;
    }

    /* The first two bits of u1ECTId represents the corresponding 
     * Equivalence Class Timer Blocks with
     * 
     * 01 -> ISIS_ECT_BLK_1
     * 10 -> ISIS_ECT_BLK_2
     * 11 -> ISIS_ECT_BLK_3
     * 
     * The next 6 bits represents the Index into the EC timer Block
     * 
     * Masking with ISIS_EC_BLK_MASK gives the appropriate Block
     * Maskinf with ISIS_EC_IDX_MASK gives the appropriate Block Index
     */

    u1ECBlk = (UINT1) (u1ECTId & ISIS_EC_BLK_MASK);
    u1BlkIdx = (UINT1) (u1ECTId & ISIS_EC_IDX_MASK);

    TMP_PT ((ISIS_LGST, "TMR <T> : EC Timer ID [ %x ], Block Index [ %u ]\n",
             u1ECBlk, u1BlkIdx));

    /* Retrieve the first pointer in the chain, which is used for scanning to
     * find the given CSNP information
     */

    switch (u1ECBlk)
    {
        case ISIS_ECT_BLK_1:
            if (u1BlkIdx < ISIS_MAX_EC1_BLKS)
            {

                ppTmrECRec =
                    &(pContext->SysTimers.TmrECInfo.apTmrEC1[u1BlkIdx]);
                pTravTmr = *ppTmrECRec;
            }
            break;

        case ISIS_ECT_BLK_2:
            if (u1BlkIdx < ISIS_MAX_EC2_BLKS)
            {

                ppTmrECRec =
                    &(pContext->SysTimers.TmrECInfo.apTmrEC2[u1BlkIdx]);
                pTravTmr = *ppTmrECRec;
            }
            break;

        case ISIS_ECT_BLK_3:
            if (u1BlkIdx < ISIS_MAX_EC3_BLKS)
            {
                ppTmrECRec =
                    &(pContext->SysTimers.TmrECInfo.apTmrEC3[u1BlkIdx]);
                pTravTmr = *ppTmrECRec;
            }
            break;

        default:

            TMP_PT ((ISIS_LGST, "TMR <E> : Invalid Timer Block ID [ %u ]\n",
                     u1ECBlk));
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStopCSNPTimer ()\n"));
            return;
    }

    TMP_PT ((ISIS_LGST, "TMR <T> : Stopping CSNP Timer\n"));

    while (pTravTmr != NULL)
    {
        if (pTravTmr->u1Type == u1Type)
        {
            /* Found a matching block of CSNP timer information
             */

            ISIS_MEM_FREE (ISIS_BUF_ECTI, pTravTmr->pInfo);
            /* Freeing the Timer Record which held the CSNP information
             */

            if (pPrevTmr != NULL)
            {
                /* The timer node to be deleted is not the first in the chain
                 */

                pPrevTmr->pNext = pTravTmr->pNext;
                pTravTmr->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTravTmr);
            }
            else
            {
                /* The timer node to be deleted is the first in the chain
                 */

                *ppTmrECRec = pTravTmr->pNext;
                pTravTmr->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTravTmr);
            }
            break;
        }
        pPrevTmr = pTravTmr;
        pTravTmr = pTravTmr->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdStopCSNPTimer ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdChkAndTxPurgedLSPs ()
 * Description : This routine Transmits LSPs from the Purge Queue on the given
 *               circuit if the corresponding SRM Flag bit is set. The SRM Flag
 *               bit corresponding to the circuit is reset once the LSP is
 *               transmitted. If all the SRM Flag bits are cleared, the LSP and
 *               its associated information is Freed.
 * Input (s)   : pContext   - Pointer to System Context
 *               pCktRec    - Pointer to Circuit Record 
 *               u1Level    - Level of Circuit (Level1 oe Level2)
 * Output (s)  : pu2TxCount - The Number of LSPs Transmitted
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdChkAndTxPurgedLSPs (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                           UINT1 u1Level, UINT2 *pu2TxCount)
{
    UINT1               u1RelFlag = 0;
    UINT1               u1Mask = 0;
    UINT2               u2Len = 0;
    UINT4               u4ByteCnt = 0;
    tIsisLSPTxEntry    *pPrgRec = NULL;
    tIsisLSPTxEntry    *pNxtPrgRec = NULL;
    tIsisLSPTxEntry    *pPrevPrgRec = NULL;
    UINT1               u1AuthType = ISIS_AUTH_PASSWD;
    tIsisPasswd        *pTxPasswd = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdChkAndTxPurgedLSPs ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        pPrgRec = pContext->pL1PurgeQueue;
        pTxPasswd = &pContext->SysActuals.SysAreaTxPasswd;
        u1AuthType = pContext->SysActuals.u1SysAreaAuthType;

    }
    else
    {
        pPrgRec = pContext->pL2PurgeQueue;
        pTxPasswd = &pContext->SysActuals.SysDomainTxPasswd;
        u1AuthType = pContext->SysActuals.u1SysDomainAuthType;
    }

    while (pPrgRec != NULL)
    {
        pNxtPrgRec = (tIsisLSPTxEntry *) (pPrgRec->pLSPRec->pNext);

        if (ISIS_NUM_BYTES (pCktRec->u4CktIdx) <= pPrgRec->u1SRMLen)
        {
            /* Number of Bytes required to hold the circuit index is less
             * than the total number of bytes available in the SRM flag
             * mask. Hence it may be posibble that the LSP is eligible for
             * transmission on this circuit. Check whether the SRM flag bit
             * corresponding to the circuit is set
             */

            ISIS_BIT_MASK (pCktRec->u4CktIdx, u1Mask);

            if ((*(pPrgRec->pu1SRM + ISIS_BYTE_POS (pCktRec->u4CktIdx))
                 & u1Mask) != 0)
            {
                /* The SRM Flag bit for the circuit is set. LSPs can be
                 * transmitted on this circuit
                 */

                ISIS_EXTRACT_PDU_LEN_FROM_LSP (pPrgRec->pLSPRec->pu1LSP, u2Len);
                if ((u1AuthType == ISIS_AUTH_MD5) && (pTxPasswd != NULL)
                    && (pTxPasswd->u1Len != 0))
                {
                    MEMSET (pPrgRec->pLSPRec->pu1LSP + ISIS_LSP_HDR_LEN, 0,
                            u2Len - ISIS_LSP_HDR_LEN);
                    u2Len = ISIS_LSP_HDR_LEN;
                    IsisUpdAddAuthToPDU (pContext, pPrgRec->pLSPRec->pu1LSP,
                                         &u2Len, u1Level);
                }

                IsisTrfrTxData (pCktRec, pPrgRec->pLSPRec->pu1LSP, u2Len,
                                u1Level, ISIS_HOLD_BUF, ISIS_FALSE);

                /* Once LSP is transmitted on a circuit, SRM bit for 
                 * corresponding circuit will be cleared. Check whether any 
                 * other bits are set in the SRM flags for this LSP. If no
                 * bits are set then the LSP can be Freed.
                 */

                *pu2TxCount = (UINT2) (*pu2TxCount + 1);
                *(pPrgRec->pu1SRM + ISIS_BYTE_POS (pCktRec->u4CktIdx))
                    &= ~u1Mask;

                u1RelFlag = ISIS_TRUE;
                for (u4ByteCnt = 0; u4ByteCnt < pPrgRec->u1SRMLen; u4ByteCnt++)
                {
                    if (*(pPrgRec->pu1SRM + u4ByteCnt) != 0)
                    {
                        u1RelFlag = ISIS_FALSE;
                        break;
                    }
                }

                if (u1RelFlag == ISIS_TRUE)
                {
                    if (pPrevPrgRec != NULL)
                    {
                        pPrevPrgRec->pLSPRec->pNext =
                            (tIsisLSPEntry *) pNxtPrgRec;
                    }
                    else if (u1Level == ISIS_LEVEL1)
                    {
                        pContext->pL1PurgeQueue = pNxtPrgRec;
                    }
                    else
                    {
                        pContext->pL2PurgeQueue = pNxtPrgRec;
                    }

                    ISIS_MEM_FREE (ISIS_BUF_SRMF, pPrgRec->pu1SRM);
                    pPrgRec->pu1SRM = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_LSPI,
                                   (UINT1 *) pPrgRec->pLSPRec->pu1LSP);
                    pPrgRec->pLSPRec->pu1LSP = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pPrgRec->pLSPRec);
                    pPrgRec->pLSPRec = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pPrgRec);
                }
                else
                {
                    pPrevPrgRec = pPrgRec;
                }
            }
            else
            {
                pPrevPrgRec = pPrgRec;
            }
        }
        else
        {
            pPrevPrgRec = pPrgRec;
        }
        pPrgRec = pNxtPrgRec;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdChkAndTxPurgedLSPs ()\n"));
}

/*******************************************************************************
 * Function    : IsisGRUpdProcT3TimeOut ()
 * Description : This routine processes each of the LSPs included in the
 *               pTmrInfo and sets the RLT to '0' and length of the PDU to 
 *               header length. It deletes the LSP from the Remaining Life Time
 *               Timer list and inserts the same into the Zero Age Timer list
 * Input (s)   : pContext - Pointer to System context
 *             : pTmrInfo - Pointer to Timer Info
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisGRUpdProcT3TimeOut (tIsisSysContext * pContext)
{

    tGrSemInfo          GrSemInfo;
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisAdjProcT1TimeOut ()\n"));

    MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
    GrSemInfo.pContext = pContext;

    IsisGrRunRestartSem (ISIS_GR_EVT_T3_EXP, ISIS_GR_ADJ_RESTART, &GrSemInfo);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisAdjProcT1TimeOut ()\n"));
}

/*******************************************************************************
 * Function    : IsisGRUpdProcL1T2TimeOut ()
 * Description : This routine processes each of the LSPs included in the
 *               pTmrInfo and sets the RLT to '0' and length of the PDU to 
 *               header length. It deletes the LSP from the Remaining Life Time
 *               Timer list and inserts the same into the Zero Age Timer list
 * Input (s)   : pContext - Pointer to System context
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisGRUpdProcL1T2TimeOut (tIsisSysContext * pContext)
{

    tGrSemInfo          GrSemInfo;

    MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
    GrSemInfo.pContext = pContext;
    GrSemInfo.u1Level = ISIS_LEVEL1;
    IsisGrRunRestartSem (ISIS_GR_EVT_T2_EXP, ISIS_GR_ADJ_RESTART, &GrSemInfo);

}

/*******************************************************************************
 * Function    : IsisGRUpdProcL2T2TimeOut ()
 * Description : This routine processes each of the LSPs included in the
 *               pTmrInfo and sets the RLT to '0' and length of the PDU to 
 *               header length. It deletes the LSP from the Remaining Life Time
 *               Timer list and inserts the same into the Zero Age Timer list
 * Input (s)   : pContext - Pointer to System context
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisGRUpdProcL2T2TimeOut (tIsisSysContext * pContext)
{
    tGrSemInfo          GrSemInfo;

    MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
    GrSemInfo.pContext = pContext;
    GrSemInfo.u1Level = ISIS_LEVEL2;
    IsisGrRunRestartSem (ISIS_GR_EVT_T2_EXP, ISIS_GR_ADJ_RESTART, &GrSemInfo);

}
