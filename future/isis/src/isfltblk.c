 /******************************************************************************
 * Copyright (C) Future Software Limited,  2001
 *
 * $Id: isfltblk.c,v 1.8 2017/09/11 13:44:08 siva Exp $
 *
 * Description: This file contains the Fault Tolerance Module Bulk Update
 *              routines.
 *
 ******************************************************************************/

#ifdef ISIS_FT_ENABLED

#include "isincl.h"
#include "isextn.h"

/*******************************************************************************
 * Function Name  : IsisFltrFormSysContextBulkData () 
 * Description    : This routine encodes the Context Database information into
 *                  FLTR messages.
 * Input(s)       : pContext - Address of the pointer to the System Context
 *                             which is to be encoded
 * Output(s)      : pContext - Pointer to the System Context to be encoded next
 *                  pu1Flag  - Flag which will be set to ISIS_EOD if the entire
 *                             database encoding is complete
 * Globals        : gIsisExtInfo Referred, Not Modified
 * Returns        : pRmMsg    - Buffer Containing a part (or full) Bulk 
 *                            Update message of the System Context
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormSysContextBulkData (tIsisSysContext ** pContext, UINT2 *pu2Len)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST,
             "FLT <X> : Entered IsisFltrFormSysContextBulkData ()\n"));

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Context Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST,
                 "FLT <X> : Exiting IsisFltrFormSysContextBulkData ()\n"));
        return NULL;
    }

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_CTXT, u1EntryCount,
                     &u2Offset);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);

    IsisFltrBldSysContextRec (pRmMsg, *pContext, &u2Offset);

    u1EntryCount = (UINT1) (u1EntryCount + 1);

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));
    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    FTP_EE ((ISIS_LGST,
             "FLT <X> : Exiting IsisFltrFormSysContextBulkData ()\n"));
    *pu2Len = u2Offset;
    return pRmMsg;
}

/*******************************************************************************
 * Function    : IsisFltrFormCktBulkData () 
 * Description : This routine encodes the Circuit Database information into
 *               FLTR messages.
 * Input(s)    : pContext - Address of the pointer to the System Context from
 *                          where the circuits are to be extracted for 
 *                          encoding
 *               pCktRec  - Address of the Pointer to the Circuit Record to 
 *                          be encoded
 * Output(s)   : pCktRec  - Pointer to the Circuit Record to be encoded next
 *               pu1Flag  - Flag which will be set to ISIS_EOD if the entire
 *                          databse encoding is complete
 * Globals     : gIsisExtInfo Referred Not Modified
 * Returns     : pRmMsg    - Buffer Containing a part (or full) Bulk 
 *                         Update message of the Circuit Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormCktBulkData (tIsisSysContext ** pContext, tIsisCktEntry ** pCktRec,
                         UINT1 *pu1Flag, UINT2 *pu2Len)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormCktBulkData ()\n"));

    /* The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Circuit Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormCktBulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);

    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_CKTS, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold a 
         * Circuit record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since a
         * Circuit record is identified by 2 indices, one a System
         * Instance Index which is '4' bytes and the other a Circuit Index which
         * is also ;'4' bytes, we may have to include these '8' bytes in the
         * calculation for buffer space
         */

        if ((u2Offset + sizeof (tIsisCktEntry) + 8) <= gIsisExtInfo.u2FTBufSize)
        {
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pCktRec)->u4CktIdx);

            IsisFltrBldCktRec (pRmMsg, *pCktRec, &u2Offset);
            u1EntryCount = (UINT1) (u1EntryCount + 1);
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need to 
             * fill the value of the Length of the buffer filled 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormCktBulkData ()\n"));
            *pu2Len = u2Offset;
            return pRmMsg;
        }
    }
    while (IsisFltrGetNextCktEntry (pContext, pCktRec) == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the Circuits
     * from all the contexts. Hence we can set the 'u1Flag' to ISIS_EOD so that
     * the calling routine proceeds with the next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormCktBulkData ()\n"));
    *pu2Len = u2Offset;
    return pRmMsg;
}

/*******************************************************************************
 * Function     : IsisFltrFormCktLevelBulkData () 
 * Description  : This routine encodes the Circuit level Database information
 *                into FLTR messages.
 * Input(s)     : pContext     - Address of the Pointer to the System Context
 *                               which includes the Circuit Level Database (as 
 *                               part of Circuits) to be encoded
 *                pCktRec      - Address of the Pointer to the Circuit Record
 *                               which includes the circuit level database to be
 *                               encoded
 *                pCktLevelRec - Address of the Pointer to the Circuit Level 
 *                               Record which is to be encoded
 * Output(s)    : pCktLevelRec - Pointer to the Circuit Level Record which is to
 *                               be encoded next
 *                pu1Flag      - Flag which will be set to ISIS_EOD if the 
 *                               entire databse encoding is complete
 * Globals      : gIsisExtInfo Referred, Not Modified
 * Returns      : pRmMsg - Buffer Containing a part (or full) Bulk Update message
 *                       of the Circuit Level Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormCktLevelBulkData (tIsisSysContext ** pContext,
                              tIsisCktEntry ** pCktRec,
                              tIsisCktLevel ** pCktLevelRec, UINT1 *pu1Flag,
                              UINT2 *pu2Len)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormCktLevelBulkData ()\n"));

    /* The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST,
                ISIS_MEM_ALLOC_FAIL " : Circuit Level Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST,
                 "FLT <X> : Exiting IsisFltrFormCktLevelBulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);

    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_CKTL, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold a 
         * Circuit Level record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since a
         * Circuit record is identified by 3 indices, a System Instance Index 
         * which is '4' bytes, a Circuit Index which is '4' bytes and Circuit
         * Level Index which is '1' byte, we may have to include these '9' bytes
         * in the calculation for buffer space
         */

        if ((u2Offset + sizeof (tIsisCktLevel) + 9) <= gIsisExtInfo.u2FTBufSize)
        {
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pCktRec)->u4CktIdx);

            ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                                 (*pCktLevelRec)->u1CktLvlIdx);

            IsisFltrBldCktLevelRec (pRmMsg, *pCktLevelRec, &u2Offset);

            u1EntryCount = (UINT1) (u1EntryCount + 1);
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need 
             * to fill the value of the Length of the buffer filled 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormCktLvlBulkData\n"));

            *pu2Len = u2Offset;
            return pRmMsg;
        }
    }
    while (IsisFltrGetNextCktLvlEntry (pContext, pCktRec,
                                       pCktLevelRec) == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the Circuit
     * Level records of every circuit from all the contexts. Hence we can set 
     * the 'u1Flag' to ISIS_EOD so that he calling routine proceeds with the 
     * next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormCktLvlBulkData\n"));
    *pu2Len = u2Offset;
    return pRmMsg;
}

/*******************************************************************************
 * Function     : IsisFltrFormAdjBulkData () 
 * Description  : This routine encodes the Adjacency Database information
 *                into FLTR messages.
 * Input(s)     : pContext - Address of the Pointer to the System Context which
 *                           includes the Adjacency information (as part of 
 *                           Circuit database) to be encoded
 *                pCktRec  - Address of the Pointer to the Circuit Record which
 *                           includes the Adjacency information to be encoded
 *                pAdjRec  - Address of the Pointer to the Adjacency Record
 *                           which is to be encoded
 * Output(s)    : pAdjRec  - Pointer to the Adjacency Record which is to be 
 *                           encoded next
 *                pu1Flag  - Flag which will be set to ISIS_EOD if the entire 
 *                           databse encoding is complete
 * Globals      : gIsisExtInfo Referred, Not Modified
 * Returns      : pRmMsg  - Buffer Containing a part (or full) Bulk Update message
 *                        of the Adjacency Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormAdjBulkData (tIsisSysContext ** pContext, tIsisCktEntry ** pCktRec,
                         tIsisAdjEntry ** pAdjRec, UINT1 *pu1Flag,
                         UINT2 *pu2Len)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormAdjBulkData () \n"));

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);

    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_ADJN, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold an 
         * Adjacency record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since an
         * Adjacency record is identified by 3 indices, a System Instance Index
         * which is '4' bytes, a Circuit Index which is '4' bytes and the 
         * Adjacency Index which is '4' bytes, we may have to include these '12'
         * bytes in the calculation for buffer space
         */

        if ((u2Offset + sizeof (tIsisAdjEntry) + 12) <=
            gIsisExtInfo.u2FTBufSize)
        {
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pCktRec)->u4CktIdx);

            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pAdjRec)->u4AdjIdx);

            IsisFltrBldAdjRec (pRmMsg, *pAdjRec, &u2Offset);

            u1EntryCount = (UINT1) (u1EntryCount + 1);
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need 
             * to fill the value of the Length of the buffer filled 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormAdjBulkData () \n"));

            *pu2Len = u2Offset;
            return pRmMsg;
        }
    }
    while (IsisFltrGetNextAdjEntry (pContext, pCktRec, pAdjRec)
           == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the
     * adjacencies from all the contexts. Hence we can set the 'u1Flag' to 
     * ISIS_EOD so that the calling routine proceeds with the next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormAdjBulkData () \n"));
    *pu2Len = u2Offset;
    return pRmMsg;
}

/*******************************************************************************
 * Function     : IsisFltrFormLSPBulkData () 
 * Description  : This routine encodes the LSP Database information into FLTR 
 *                messages.
 * Input(s)     : pContext  - Address of Pointer to the System Context which
 *                            includes the LSP information 
 *                pLspRec   - Address of Pointer to the LSP Record which
 *                            includes the LSP information to be encoded
 *                pu1LspLvl - Address of Pointer to the LSP Type which
 *                            indicates the level of the LSP to be encoded
 *                pu1Loc    - Address of Pointer to the location of the LSP to
 *                            be encoded           
 * Output(s)    : pLSPRec  - Pointer to the LSP Record which is to be 
 *                           encoded next
 *                pu1Flag  - Flag which will be set to ISIS_EOD if the entire 
 *                           databse encoding is complete
 * Globals      : gIsisExtInfo Referred, Not Modified
 * Returns      : pRmMsg  - Buffer Containing a part (or full) Bulk Update message
 *                        of the LSP Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormLSPBulkData (tIsisSysContext ** pContext, tIsisLSPEntry ** pLspRec,
                         UINT1 *pu1LspLvl, UINT1 *pu1Loc, UINT1 *pu1Flag,
                         UINT2 *pu2Len)
{
    INT4                i4RetVal = 0;
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT1               u1SNId = 0;
    UINT2               u2Offset = 0;
    UINT2               u2LspLen = 0;
    UINT4               u4CktIdx = 0;
    tIsisSNLSP         *pSNLSP = NULL;

    /* NOTE: LSP bulk updates are handled in a different manner than the other
     *       databases, since LSPs can exist for level1 and/or level2 and either
     *       in TxQ or Database. Hence getting the next LSP must ensure that it
     *       retrieves the next LSP, in order, either from TxQ or Database. If
     *       all LSPs in the given level are done, then the get next routine
     *       must start fetching from the other level if possible. It must also
     *       move across all the contexts and repeat the same procedure for
     *       extracting the LSPs in order
     */

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormLSPBulkData ()\n"));

    /* The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_LSPS, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold an LSP 
         * record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since entire
         * LSPs are being backed up, we will prepend the LSP buffer with '4'
         * bytes of System Instance Index, '1' byte of Level of the database,
         * and '2' bytes of LSP length, which adds up to '7'. Apart from these
         * the entire LSP will also be included into the buffer.
         */

        ISIS_EXTRACT_PDU_LEN_FROM_LSP ((*pLspRec)->pu1LSP, u2LspLen);

        if ((u2Offset + u2LspLen + 11) <= gIsisExtInfo.u2FTBufSize)
        {
            /* Circuit Index needs to be passed to the Standby Node along with
             * the PseudoNode LSPs generated by local IS. In the Standby Node
             * on reception of PseudoNode LSPs of local IS, while constructing`
             * Self LSP Buffers, Circuit Record is retrieved using the passed
             * Circuit Index and linked to PseudoNode LSP Buffer
             */

            i4RetVal = MEMCMP (((*pLspRec)->pu1LSP + ISIS_OFFSET_LSPID),
                               (*pContext)->SysActuals.au1SysID,
                               ISIS_SYS_ID_LEN);
            if (i4RetVal == 0)
            {
                u1SNId = *((*pLspRec)->pu1LSP + ISIS_OFFSET_PNODEID);
                if (u1SNId != 0)
                {
                    pSNLSP = ((*pu1LspLvl) == ISIS_LEVEL1) ?
                        (*pContext)->SelfLSP.pL1SNLSP :
                        (*pContext)->SelfLSP.pL2SNLSP;

                    while (pSNLSP != NULL)
                    {
                        if (u1SNId == pSNLSP->pZeroLSP->u1SNId)
                        {
                            u4CktIdx = pSNLSP->pCktEntry->u4CktIdx;
                            break;
                        }
                        pSNLSP = pSNLSP->pNext;
                    }
                }
            }

            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, u4CktIdx);

            ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, *pu1LspLvl);

            /* Since LSP length can vary from buffer to buffer, we must
             * encapsulate the LSP records with the appropriate length
             */

            ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, u2LspLen);
            ISIS_RED_PUT_N_BYTE (pRmMsg, ((*pLspRec)->pu1LSP), u2Offset,
                                 u2LspLen);

            u1EntryCount++;
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need 
             * to fill the value of the Length of the buffer filled 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormLSPBulkData ()\n"));
            *pu2Len = u2Offset;
            return pRmMsg;
        }
    }
    while (IsisFltrGetNextLspEntry (pContext, pu1LspLvl, pLspRec, pu1Loc)
           == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the LSPs
     * from all the contexts. Hence we can set the 'u1Flag' to ISIS_EOD so that
     * the calling routine proceeds with the next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormLSPBulkData ()\n"));

    *pu2Len = u2Offset;
    return pRmMsg;
}

/*******************************************************************************
 * Function    : IsisFltrFormIPRABulkData () 
 * Description : This routine encodes the IPRA Database information into FLTR 
 *               messages.
 * Input(s)    : pContext - Address of the Pointer to the System Context which
 *                          includes the IPRA information (as part of 
 *                          Circuit database) to be encoded
 *               pCktRec  - Address of the Pointer to the Circuit Record which
 *                          includes the IPRA information to be encoded
 *               pIPRARec - Address of the Pointer to the IPRA Record
 *                          which is to be encoded
 * Output(s)   : pIPRARec - Pointer to the IPRA Record which is to be 
 *                          encoded next
 *               pu1Flag  - Flag which will be set to ISIS_EOD if the entire 
 *                          databse encoding is complete
 * Globals     : gIsisExtInfo Referred, Not Modified
 * Returns     : pRmMsg  - Buffer Containing a part (or full) Bulk Update message
 *                       of the IPRA Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormIPRABulkData (tIsisSysContext ** pContext,
                          tIsisIPRAEntry ** pIPRARec, UINT1 *pu1Flag,
                          UINT2 *pu2Len)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormIPRABulkData ()\n"));

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_IPRA, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold an 
         * IPRA record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since an
         * IPRA record is identified by 4 indices, a System Instance Index
         * which is '4' bytes, a Circuit Index which is '4' bytes, a IPRA Index
         * which is '2' bytes and the IPRA Type which is '1' bytes, we may have
         * to include these '11' bytes in the calculation for buffer space
         */

        if ((u2Offset + sizeof (tIsisIPRAEntry) + 11) <=
            gIsisExtInfo.u2FTBufSize)
        {
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pIPRARec)->u4IPRAIdx);
            ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, (*pIPRARec)->u1IPRAType);

            IsisFltrBldIPRARec (pRmMsg, *pIPRARec, &u2Offset);
            u1EntryCount++;
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need 
             * to fill the value of the Length of the buffer filled 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where 
             * we need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormIPRABulkData ()\n"));

            *pu2Len = u2Offset;

            return pRmMsg;
        }
    }
    while (IsisFltrGetNextIPRAEntry (pContext, pIPRARec) == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the IPRAs
     * from all the contexts. Hence we can set the 'u1Flag' to ISIS_EOD so that
     * the calling routine proceeds with the next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormIPRABulkData ()\n"));

    *pu2Len = u2Offset;

    return pRmMsg;
}

/*******************************************************************************
 * Function    : IsisFltrFormIPIfBulkData () 
 * Description : This routine encodes the IP I/f Address Database information 
 *               into FLTR messages.
 * Input(s)    : pContext - Address of the Pointer to the System Context which
 *                          includes the IP I/f Address information to be 
 *                          encoded
 *               pIPIfRec - Address of the Pointer to the IP I/f Address Record
 *                          which is to be encoded
 * Output(s)   : pIPIfRec - Pointer to the IP I/f Address Record which is to be 
 *                          encoded next
 *               pu1Flag  - Flag which will be set to ISIS_EOD if the entire 
 *                          databse encoding is complete
 * Globals     : gIsisExtInfo Referred, Not Modified
 * Returns     : pRmMsg  - Buffer Containing a part (or full) Bulk Update message
 *                       of the IP I/f Address Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormIPIfBulkData (tIsisSysContext ** pContext,
                          tIsisIPIfAddr ** pIPIfRec, UINT1 *pu1Flag,
                          UINT2 *pu2Len)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormIPIfBulkData ()\n"));

    /* The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_IPIF, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold an 
         * IP Interface record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since an
         * IP I/f record is identified by a System Instance Index
         * which is '4' bytes, we may have to include these '4' bytes in the 
         * calculation for buffer space
         */

        if ((u2Offset + sizeof (tIsisIPIfAddr) + 4) <= gIsisExtInfo.u2FTBufSize)
        {
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);

            IsisFltrBldIPIfRec (pRmMsg, *pIPIfRec, &u2Offset);
            u1EntryCount++;
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need 
             * to fill the value of the Length of the buffer filled 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where we 
             * need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormIPIfBulkData ()\n"));
            *pu2Len = u2Offset;
            return pRmMsg;
        }
    }
    while (IsisFltrGetNextIPIfRec (pContext, pIPIfRec) == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the IP I/f's
     * from all the contexts. Hence we can set the 'u1Flag' to ISIS_EOD so that
     * the calling routine proceeds with the next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormIPIfBulkData () \n"));
    *pu2Len = u2Offset;
    return pRmMsg;
}

/*******************************************************************************
 * Function    : IsisFltrFormSAABulkData () 
 * Description : This routine encodes the Summary Address Database information 
 *               into FLTR messages.
 * Input(s)    : pContext - Address of the Pointer to the System Context which
 *                          includes the Summary Address information to be 
 *                          encoded
 *               pSARec   - Address of the Pointer to the Summary Address Record
 *                          which is to be encoded
 * Output(s)   : pSARec   - Pointer to the Summary Address Record which is to 
 *                          be encoded next
 *               pu1Flag  - Flag which will be set to ISIS_EOD if the entire 
 *                          databse encoding is complete
 * Globals     : gIsisExtInfo Referred, Not Modified
 * Returns     : pRmMsg  - Buffer Containing a part (or full) Bulk Update message
 *                       of the Summary Address Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormSAABulkData (tIsisSysContext ** pContext, tIsisSAEntry ** pSARec,
                         UINT1 *pu1Flag, UINT2 *pu2Len)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormSAABulkData ()\n"));

    /* The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_SAAD, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold a 
         * Summary Area Address record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since a
         * Summary Area Address record is identified by a System Instance Index
         * which is '4' bytes, we may have to include these '4' bytes in the 
         * calculation for buffer space 
         *
         */

        if ((u2Offset + sizeof (tIsisSAEntry) + 4) <= gIsisExtInfo.u2FTBufSize)
        {
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);

            IsisFltrBldSARec (pRmMsg, *pSARec, &u2Offset);
            u1EntryCount++;
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need 
             * to fill the value of the Length of the buffer filled 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where 
             * we need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormSAABulkData ()\n"));
            *pu2Len = u2Offset;
            return pRmMsg;
        }
    }
    while (IsisFltrGetNextSAEntry (pContext, pSARec) == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the SAA
     * records from all the contexts. Hence we can set the 'u1Flag' to ISIS_EOD
     * so that the calling routine proceeds with the next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));

    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormSAABulkData ()\n"));
    *pu2Len = u2Offset;
    return pRmMsg;
}

/*******************************************************************************
 * Function    : IsisFltrFormMAABulkData () 
 * Description : This routine encodes the Manual Address Database information 
 *               into FLTR messages.
 * Input(s)    : pContext - Address of the Pointer to the System Context which
 *                          includes the Manual Address information to be 
 *                          encoded
 *               pMAARec  - Address of the Pointer to the Manual Area Address 
 *                          Record which is to be encoded
 * Output(s)   : pMAARec  - Pointer to the Manual Area Address Record which is 
 *                          to be encoded next
 *               pu1Flag  - Flag which will be set to ISIS_EOD if the entire 
 *                          databse encoding is complete
 * Globals     : gIsisExtInfo Referred, Not Modified
 * Returns     : pRmMsg  - Buffer Containing a part (or full) Bulk Update message
 *                       of the Manual Area Address Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormMAABulkData (tIsisSysContext ** pContext, tIsisMAAEntry ** pMAARec,
                         UINT1 *pu1Flag, UINT2 *pu2Len)
{
    /*INT1              *pRmMsg = NULL; */
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormMAABulkData ()\n"));

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);

    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_MAAD, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold an 
         * MAA record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since an
         * MAA record is identified by a System Instance Index which is '4' 
         * bytes, we may have to include these '4' bytes in the calculation for
         * buffer space
         */

        if ((u2Offset + sizeof (tIsisMAAEntry) + 4) <= gIsisExtInfo.u2FTBufSize)
        {
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);

            IsisFltrBldMAARec (pRmMsg, *pMAARec, &u2Offset);

            u1EntryCount++;
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need 
             * to fill the value of the Length of the buffer filled 
             */
            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */
            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            *pu2Len = u2Offset;

            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
            return pRmMsg;
        }
    }
    while (IsisFltrGetNextMAAEntry (pContext, pMAARec) == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the MAA
     * records from all the contexts. Hence we can set the 'u1Flag' to ISIS_EOD
     * so that the calling routine proceeds with the next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
    *pu2Len = u2Offset;
    return pRmMsg;
}

/*******************************************************************************
 * Function    : IsisFltrFormAdjAABulkData () 
 * Description : This routine encodes the Adjhacency Area Address Database 
 *               information into FLTR messages.
 * Input(s)    : pContext  - Address of the Pointer to the System Context which
 *                           includes the Adjacency Area Address information to 
 *                           be encoded
 *               pCktRec   - Address of the Pointer to the Circuit Record which
 *                           includes the Adjacency whose Area Address are to be
 *                           encoded 
 *               pAdjRec   - Address of the Pointer to the Adjacency Record 
 *                           which includes the Area Address are to be encoded 
 *               pAdjAARec - Pointer to the Adjacency Area Address record which
 *                           is to be encoded
 * Output(s)   : pAdjAARec - Pointer to the Adjacency Area Address Record which
 *                           is to be encoded next
 *               pu1Flag   - Flag which will be set to ISIS_EOD if the entire 
 *                           databse encoding is complete
 * Globals     : gIsisExtInfo Referred, Not Modified
 * Returns     : pRmMsg  - Buffer Containing a part (or full) Bulk Update message
 *                       of the Adjacency Area Address Database 
 ******************************************************************************/

PUBLIC tRmMsg      *
IsisFltrFormAdjAABulkData (tIsisSysContext ** pContext,
                           tIsisCktEntry ** pCktRec, tIsisAdjEntry ** pAdjRec,
                           tIsisAdjAAEntry ** pAdjAARec, UINT1 *pu1Flag,
                           UINT2 *pu2Len)
{
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormAdjAABulkData ()\n"));

    /* The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    pRmMsg =
        IsisRmAllocForRmMsg ((UINT2)
                             (ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr) +
                              gIsisExtInfo.u2FTBufSize));

    if (pRmMsg == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
        *pu1Flag = ISIS_EOD;
        return NULL;
    }
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, RM_BULK_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     */

    IsisFltrFillHdr (pRmMsg, ISIS_CMD_ADD, ISIS_DB_ADAD, u1EntryCount,
                     &u2Offset);

    do
    {
        /* Check whether there is enough space in the buffer to hold an 
         * Adjacency Area Address record. 
         *
         * Every record encoded is of the form <Indices, Record>. Since an
         * Adjacency Area Address record is identified by 3 indices, a System 
         * Instance Index which is '4' bytes, a Circuit Index which is '4' bytes
         * and the Adjacency Index which is '4' bytes, we may have to include 
         * these '12' bytes in the calculation for buffer space
         */

        if ((u2Offset + sizeof (tIsisAdjAAEntry) + 12) <=
            gIsisExtInfo.u2FTBufSize)
        {
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pContext)->u4SysInstIdx);
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pCktRec)->u4CktIdx);
            ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, (*pAdjRec)->u4AdjIdx);

            IsisFltrBldAdjAARec (pRmMsg, *pAdjAARec, &u2Offset);
            u1EntryCount++;
        }
        else
        {
            /* ISIS_FT_ENTRYCOUNT_POS is the position in the pRmMsg where we need 
             * to fill the value of the EntryCount
             */

            ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

            /* ISIS_FT_LENGTH_POS is the position in the pRmMsg where we need 
             * to fill the value of the Length of the buffer filled 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pRmMsg where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrFormAdjAABulkData () \n"));
            *pu2Len = u2Offset;
            return pRmMsg;
        }
    }
    while (IsisFltrGetNextAdjAreaEntry (pContext, pCktRec,
                                        pAdjRec, pAdjAARec) == ISIS_SUCCESS);

    /* We reach this point only if we have completely encoded all the Adjacency
     * Area Address records from all the contexts. Hence we can set the 'u1Flag'
     * to ISIS_EOD so that the calling routine proceeds with the next Database
     */

    *pu1Flag = ISIS_EOD;

    /* The last message may have been partially filled. We have to encode the
     * Lengths, entry count etc appropriately for the last message
     */

    ISIS_RED_COPY_1_BYTE (pRmMsg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));
    ISIS_RED_COPY_2_BYTE (pRmMsg, ISIS_PEERHDR_LENGTH_POS, u2Offset);
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormAdjAABulkData ()\n"));
    *pu2Len = u2Offset;
    return pRmMsg;
}

#endif
