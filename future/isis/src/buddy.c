/*******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: buddy.c,v 1.3 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file contains the routines for smart buddy algorithm
 *
 ******************************************************************************/

#include "isincl.h"
#include "buddy.h"

PRIVATE INT4        BuddyAddIntoBlk (UINT1, UINT1 *, UINT4);
PRIVATE UINT1      *BuddyDeleteFirstBlock (UINT1, UINT4);
PRIVATE INT4        BuddyDeleteInMiddle (UINT1, UINT1 *, UINT4);
PRIVATE VOID        BuddySetACBits (UINT1, UINT1 *, UINT1 *, UINT4);
PRIVATE VOID        BuddyResetACBits (UINT1, UINT1 *, UINT1 *, UINT4 *);
PRIVATE INT4        BuddySetBACBits (UINT1, UINT1 *, UINT1 *, UINT4);
PRIVATE INT4        BuddyTRAndMerge (UINT1, UINT1 *, UINT1 *, UINT4 *);
PRIVATE VOID        BuddyTLAndMerge (UINT1, UINT1 *, UINT1 **, UINT4 *);

tBuddyTable         gBuddyTable[BUDDY_MAX_INST];
char                gai1BuddyLogStr[BUDDY_LS_LEN];

/*******************************************************************************
 * Function    : BuddyInit ()
 * Description : This function initializes the global Buddy table.
 * Input (s)   : None 
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred and Modified 
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
BuddyInit (VOID)
{
    UINT1               u1Id = 0;

    for (u1Id = 0; u1Id < BUDDY_MAX_INST; u1Id++)
    {
        gBuddyTable[u1Id].u1BuddyStatus = BUDDY_INACTIVE;
        gBuddyTable[u1Id].u4MemAlloc = 0;
        gBuddyTable[u1Id].u4NumBlks = 0;
        gBuddyTable[u1Id].u4MaxBlkSize = 0;
        gBuddyTable[u1Id].u4MinBlkSize = 0;
        gBuddyTable[u1Id].u2HdrSize = 0;
    }
}

/*******************************************************************************
 * Function    :  BuddyCreateInst ()
 * Description :  This function allocates the buddy table, initialises all the
 *                buffers and the associated header information.
 * Input (s)   :  u4MaxBlkSize - The maximum block size which can 
 *                               be allocated to application
 *                u4MinBlkSize - The minimum block size which can 
 *                               be allocated to application
 *                u4NumBlks    - The number of Buddy buffers to be 
 *                               allocated.
 * Outputs(s)   : None
 * Globals      : gBuddyTable Referred and Modified 
 * Returns      : u1Id                - Identifier of the Buddy, if the 
 *                                      initialization is successful
 *                BUDDY_INVALID_SIZE  - if the sizes passed are invalid
 *                BUDDY_ALLOC_FAILURE - if the memory allocation fails
 *                BUDDY_NO_FREE_INST  - if there are no free Buddy Instances
 ******************************************************************************/

PUBLIC INT4
BuddyCreateInst (UINT4 u4MaxBlkSize, UINT4 u4MinBlkSize, UINT4 u4NumBlks,
                 UINT1 u1CFlag)
{
    UINT1               u1Id = 0;
    UINT1               u1Status = BUDDY_FALSE;
    UINT2               u2HdrSize = 0;
    UINT4               u4Cnt = 0;
    UINT4               u4BuddyBufSize = 0;
    tBuddyBuf          *pBuf = NULL;

    SBP_EE ((BUDDY_LS, "DBG <X> : Entered BuddyCreate ()\n"));

    if (((u4MaxBlkSize % u4MinBlkSize) != 0) || ((u4MinBlkSize % 4) != 0))
    {
        SBP_WARNING ((BUDDY_LS, "SBD <W> : Invalid Sizes\n"));
        SBP_WARNING ((BUDDY_LS, "Maximum Block Size =\t\t %u \n",
                      u4MaxBlkSize));
        SBP_WARNING ((BUDDY_LS, "Minimum Block Size =\t\t %u \n",
                      u4MinBlkSize));
        SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyInit ()\n"));
        return BUDDY_INVALID_SIZE;
    }

    /* Get the free Buddy Identifier
     */

    for (u1Id = 0; u1Id < BUDDY_MAX_INST; u1Id++)
    {
        if (gBuddyTable[u1Id].u1BuddyStatus == BUDDY_INACTIVE)
        {
            u1Status = BUDDY_TRUE;
            break;
        }
    }

    if (u1Status == BUDDY_FALSE)
    {
        SBP_WARNING ((BUDDY_LS, "SBD <W> : No Free Buddy Instances\n"));
        SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyInit ()\n"));
        return BUDDY_NO_FREE_INST;
    }

    /* Initialize the Buddy Table with the sizes specified
     */

    gBuddyTable[u1Id].u1BuddyStatus = BUDDY_ACTIVE;
    gBuddyTable[u1Id].u4MaxBlkSize = u4MaxBlkSize;
    gBuddyTable[u1Id].u4MinBlkSize = u4MinBlkSize;
    gBuddyTable[u1Id].u4NumBlks = u4NumBlks;
    gBuddyTable[u1Id].u4MemAlloc = 0;
    gBuddyTable[u1Id].u1CFlag = u1CFlag;

    /*  Allocate the memory for the Queues
     */

    gBuddyTable[u1Id].pu4FreeQ = (UINT4 *)
        BUDDY_ALLOC_BUF ((u4MaxBlkSize / u4MinBlkSize), sizeof (UINT4),
                         UINT4 *);

    if (gBuddyTable[u1Id].pu4FreeQ == NULL)
    {
        SBP_PANIC ((BUDDY_LS, "SBD <P> : NO MEMORY AVAILABLE \n"));
        SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyInit ()\n"));
        return BUDDY_ALLOC_FAILURE;
    }

    /* The size of header can be calculated as sum of
     * pointer to next buffer and AC bits
     * 
     * pointer to next buffer -  4 Bytes
     * AC bits  = 2 * number of minimum blocks bits
     *          = 2 * (u4MaxBlkSize / u4MinBlkSize) bits
     *          = CEIL (2 * (u4MaxBlkSize / u4MinBlkSize) / 32) * 4 Bytes
     *
     * NOTE: Pointer to Next Buffer is not necessary if entire Buffer is
     * allocated as a single Block
     */

    if (u1CFlag == BUDDY_CONT_BUF)
    {
        u2HdrSize = (UINT2)
            (4 * (((((u4MaxBlkSize / u4MinBlkSize) * 2) - 1) / 32) + 1));
    }
    else
    {
        u2HdrSize = (UINT2)
            (4 * (((((u4MaxBlkSize / u4MinBlkSize) * 2) - 1) / 32) + 2));
    }

    gBuddyTable[u1Id].u2HdrSize = u2HdrSize;

    /* The size of buddy buffers to be allocated is sum of MaxBlkSize
     * and header size
     */

    u4BuddyBufSize = u4MaxBlkSize + u2HdrSize;

    /* Allocate the Buddy Buffers and insert them into Max Buddy Block
     * List
     */

    if (u1CFlag == BUDDY_CONT_BUF)
    {

        pBuf = BUDDY_ALLOC_BUF ((u4BuddyBufSize * u4NumBlks), sizeof (UINT1),
                                tBuddyBuf);
        if (pBuf == NULL)
        {
            /* if the memory allocation fails, release all the
             * previously allcoated buffers 
             */

            SBP_PANIC ((BUDDY_LS, "SBD <P> : NO MEMORY AVAILABLE \n"));
            SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyInit ()\n"));

            BuddyDestroyInst (u1Id);
            return BUDDY_ALLOC_FAILURE;
        }

        gBuddyTable[u1Id].pBuddyBuf = pBuf;

        for (u4Cnt = 0; u4Cnt < u4NumBlks; u4Cnt++)
        {
            /* Ensuring Header Bits are initially set to 0's
             */

            MEMSET (pBuf, 0x00, u2HdrSize);

            BuddyAddIntoBlk (u1Id, ((UINT1 *) pBuf + u2HdrSize), u4MaxBlkSize);
            BuddySetBACBits (u1Id, (UINT1 *) pBuf, ((UINT1 *) pBuf + u2HdrSize),
                             u4MaxBlkSize);
            pBuf = (tBuddyBuf *) ((UINT1 *) pBuf + u4BuddyBufSize);
        }
    }
    else
    {

        for (u4Cnt = 0; u4Cnt < u4NumBlks; u4Cnt++)
        {
            pBuf = BUDDY_ALLOC_BUF (u4BuddyBufSize, sizeof (UINT1), tBuddyBuf);

            if (pBuf == NULL)
            {
                /* if the memory allocation fails, release all the
                 * previously allcoated buffers 
                 */

                SBP_PANIC ((BUDDY_LS, "SBD <P> : NO MEMORY AVAILABLE \n"));
                SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyInit ()\n"));

                BuddyDestroyInst (u1Id);
                return BUDDY_ALLOC_FAILURE;
            }

            /* Ensure Header Bits are initially set to 0's
             */

            MEMSET (pBuf, 0x00, (gBuddyTable[u1Id].u2HdrSize));

            /* insert the buffer into buffer list and also in the max block list
             */

            if (gBuddyTable[u1Id].pBuddyBuf != NULL)
            {
                pBuf->pNext = gBuddyTable[u1Id].pBuddyBuf;
            }
            gBuddyTable[u1Id].pBuddyBuf = pBuf;

            /* since the complete buffer is free, the block must be held in the
             * maximum block size queue also
             */

            BuddyAddIntoBlk (u1Id, ((UINT1 *) pBuf + u2HdrSize), u4MaxBlkSize);
            BuddySetBACBits (u1Id, (UINT1 *) pBuf, ((UINT1 *) pBuf + u2HdrSize),
                             u4MaxBlkSize);
        }
    }

    return u1Id;
}

/*******************************************************************************
 * Function    : BuddyDestroyInst ()
 * Description : This function de-initlizes the Buddy table, frees all the
 *               associated resources.
 * Input(s)    : u1Id - The Identifier of Buddy
 * Output(s)   : None
 * Globals     : gBuddyTable Referred, and Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
BuddyDestroyInst (UINT1 u1Id)
{
    tBuddyBuf          *pBuddyBuf = NULL;
    tBuddyBuf          *pNextBuf = NULL;

    /* Reset the buddy constants
     */

    gBuddyTable[u1Id].u1BuddyStatus = BUDDY_INACTIVE;
    gBuddyTable[u1Id].u4NumBlks = 0;
    gBuddyTable[u1Id].u4MaxBlkSize = 0;
    gBuddyTable[u1Id].u4MinBlkSize = 0;
    gBuddyTable[u1Id].u2HdrSize = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddyDeInit ()\n"));

    if (gBuddyTable[u1Id].pu4FreeQ != NULL)
    {
        BUDDY_FREE_BUF (gBuddyTable[u1Id].pu4FreeQ);
        gBuddyTable[u1Id].pu4FreeQ = NULL;
    }

    pBuddyBuf = gBuddyTable[u1Id].pBuddyBuf;
    gBuddyTable[u1Id].pBuddyBuf = NULL;

    if ((gBuddyTable[u1Id].u1CFlag == BUDDY_CONT_BUF) && (pBuddyBuf != NULL))
    {
        BUDDY_FREE_BUF (pBuddyBuf);
        pBuddyBuf = NULL;
    }
    else
    {
        while (pBuddyBuf != NULL)
        {
            pNextBuf = pBuddyBuf->pNext;
            BUDDY_FREE_BUF (pBuddyBuf);
            pBuddyBuf = pNextBuf;
        }
    }

    SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyDeInit ()\n"));
}

/*******************************************************************************
 * Function    : BuddyAlloc ()
 * Description : This function allocates buddy block to the application.
 * Input(s)    : u1Id   - The Buddy Identifier
 *               u4Size - Size of the block to be allocated
 * Output(s)   : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : Pointer to a buddy block, if available
 *               NULL, Otherwise 
 ******************************************************************************/

PUBLIC UINT1       *
BuddyAlloc (UINT1 u1Id, UINT4 u4Size)
{
    UINT1              *pu1BuddyBlk = NULL;
    UINT1              *pu1BuddyBuf = NULL;
    UINT1              *pu1FragBlk = NULL;
    UINT4               u4BuddySize = 0;
    UINT4               u4NrstSize = 0;
    UINT4               u4FragSize = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddyAlloc ()\n"));

    if ((u4Size > gBuddyTable[u1Id].u4MaxBlkSize) || (u4Size == 0))
    {
        /* if the requested size is greater than maximum block size or '0'
         * buffer cannot be allocated
         */

        SBP_WARNING ((BUDDY_LS, "SBD <E> : Invalid Buffer Size\n"));
        SBP_PT ((BUDDY_LS, "Requested size = \t\t %u\n", u4Size));
        SBP_PT ((BUDDY_LS, "Maximum size = \t\t %u\n",
                 gBuddyTable[u1Id].u4MaxBlkSize));
        SBP_PT ((BUDDY_LS, "Minimum size = \t\t %u\n",
                 gBuddyTable[u1Id].u4MinBlkSize));
        SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyAlloc ()\n"));
        return NULL;
    }

    /* Get the nearest buddy size 
     */

    u4BuddySize = ((u4Size - 1) / gBuddyTable[u1Id].u4MinBlkSize) + 1;
    u4BuddySize *= gBuddyTable[u1Id].u4MinBlkSize;

    u4NrstSize = u4BuddySize;

    /* Try to get the buffer in the same size queue 
     */

    while ((pu1BuddyBlk = BuddyDeleteFirstBlock (u1Id, u4BuddySize)) == NULL)
    {
        u4BuddySize += gBuddyTable[u1Id].u4MinBlkSize;
        if (u4BuddySize > gBuddyTable[u1Id].u4MaxBlkSize)
        {
            SBP_WARNING ((BUDDY_LS, "SBD <W> : Memory Allocation Failed\n"));
            SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyAlloc ()\n"));
            return (NULL);
        }
    }

    /* Get the appropriate buffer
     */

    pu1BuddyBuf = (UINT1 *) gBuddyTable[u1Id].pBuddyBuf;

    if (gBuddyTable[u1Id].u1CFlag == BUDDY_CONT_BUF)
    {
        pu1BuddyBuf = pu1BuddyBlk -
            (((UINT4) pu1BuddyBlk - (UINT4) pu1BuddyBuf) %
             (gBuddyTable[u1Id].u2HdrSize + gBuddyTable[u1Id].u4MaxBlkSize));
    }
    else
    {
        while ((pu1BuddyBuf != NULL)
               && (BUDDY_COMPARE_BUFFS (u1Id, pu1BuddyBuf +
                                        gBuddyTable[u1Id].u2HdrSize,
                                        pu1BuddyBlk) != BUDDY_EQUAL))
        {
            pu1BuddyBuf = (UINT1 *) ((tBuddyBuf *) pu1BuddyBuf)->pNext;
        }
    }

    /* Fragment the bigger block, retain the requested block size and insert
     * the remaining block in the appropriate list
     */

    /* Nearest size refers to the size of the buddy buffer that is the best fit.
     * If such a buffer does not exist, we try to grab a buffer whose size is a
     * multiple of the minimum block size and which is greater than the
     * requested size (also greater than the Nearest Size). Hence we have to
     * fragment the bigger size buffer, into the requested block and keep the
     * remaining in the appropriate queue. For example, if 64 is the min block
     * size, and the user application has requested for, say 126 bytes, the best
     * fit would be 128 byte buffer. If the 128 byte buffer queue is empty, then
     * we may have to allocate a buffer from the higher queues, viz 192, 256
     * etc. Say we allocated a buffer of 256 bytes. Then we return the 128 bytes
     * to the application and keep the remaining 128 bytes in the 128 size
     * queue.
     */

    if (u4NrstSize != u4BuddySize)
    {
        u4FragSize = (u4BuddySize - u4NrstSize);
        pu1FragBlk = pu1BuddyBlk + u4NrstSize;
        BuddyAddIntoBlk (u1Id, pu1FragBlk, u4FragSize);
    }

    BuddySetACBits (u1Id, pu1BuddyBuf, pu1BuddyBlk, u4NrstSize);
    gBuddyTable[u1Id].u4MemAlloc += u4NrstSize;

    SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyAlloc ()\n"));
    return (pu1BuddyBlk);
}

/*******************************************************************************
 * Function    : BuddyFree ()
 * Description : This function moves the given block back to the buddy free
 *               pools
 * Input(s)    : u1Id        - The Buddy pool identifier
 *               pu1BuddyBlk - Pointer to the buddy block which is to be
 *                             released
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred and Modified
 * Returns     : BUDDY_SUCCESS     - if the buffer is successfully released
 *               BUDDY_RE_RELEASE  - if the block is already freed
 *               BUDDY_NO_SUCH_BLK - if the block is not part of buddy buffer
 *                                   pools
 ******************************************************************************/

PUBLIC INT4
BuddyFree (UINT1 u1Id, UINT1 *pu1BuddyBlk)
{
    INT4                i4RetCode = BUDDY_FAILURE;
    UINT4               u4BlkSize = 0;
    UINT1              *pu1BuddyBuf = NULL;
    UINT4               u4RelSize = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddyFree ()\n"));

    /* get the matching buffer 
     */

    pu1BuddyBuf = (UINT1 *) gBuddyTable[u1Id].pBuddyBuf;

    if (gBuddyTable[u1Id].u1CFlag == BUDDY_CONT_BUF)
    {
        pu1BuddyBuf = pu1BuddyBlk -
            (((UINT4) pu1BuddyBlk - (UINT4) pu1BuddyBuf) %
             (gBuddyTable[u1Id].u2HdrSize + gBuddyTable[u1Id].u4MaxBlkSize));
    }
    else
    {
        while ((pu1BuddyBuf != NULL)
               && (BUDDY_COMPARE_BUFFS (u1Id, pu1BuddyBuf +
                                        gBuddyTable[u1Id].u2HdrSize,
                                        pu1BuddyBlk) != BUDDY_EQUAL))
        {
            pu1BuddyBuf = (UINT1 *) ((tBuddyBuf *) pu1BuddyBuf)->pNext;
        }
    }

    if (pu1BuddyBuf == NULL)
    {
        SBP_PT ((BUDDY_LS, "SBD <E> : The Buffer does not exist \n"));
        i4RetCode = BUDDY_NO_SUCH_BLK;
    }
    else
    {
        BuddyResetACBits (u1Id, pu1BuddyBuf, pu1BuddyBlk, &u4BlkSize);
        gBuddyTable[u1Id].u4MemAlloc -= u4BlkSize;
        u4RelSize = u4BlkSize;

        /* If the block to the right of the freed block is free, Then
         * Merge it with the Free Block
         */

        BuddyTRAndMerge (u1Id, pu1BuddyBuf, pu1BuddyBlk, &u4BlkSize);

        /* If the block to the left of the freed/merged block is free, then
         * Merge it with the free block and get the pointer of the 
         * left free block
         */

        BuddyTLAndMerge (u1Id, pu1BuddyBuf, &pu1BuddyBlk, &u4BlkSize);

        /* Add the Merged Block to the Free Q
         */

        BuddyAddIntoBlk (u1Id, pu1BuddyBlk, u4BlkSize);
        i4RetCode = BUDDY_SUCCESS;
    }

    SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyFree ()\n"));

    return i4RetCode;
}

/*******************************************************************************
 * Function    : BuddyAddIntoBlk ()
 * Description : This function inserts the given block into a queue which
 *               matches the size of the buffer. It does not actually free the
 *               buffer. This buffer may get allocated if a request for a buffer
 *               of this size is made subsequently. This buffer gets merged with
 *               its buddies periodically.
 * Input(s)    : u1Id        - The Buddy Identifier
 *               pu1BuddyBlk - Pointer to the buddy block to be released
 *               u4BuddySize - Size of the buddy block which identifies the
 *                             queue where the buddy block is to be placed
 * Outputs(s   : None
 * Globals     : gBuddyTable Referred and Modified
 * Returns     : BUDDY_SUCCESS 
 ******************************************************************************/

PRIVATE INT4
BuddyAddIntoBlk (UINT1 u1Id, UINT1 *pu1Buf, UINT4 u4BuddySize)
{
    UINT4               u4BuddyIdx;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddyAddIntoBlk ()\n"));

    u4BuddyIdx = ((u4BuddySize / gBuddyTable[u1Id].u4MinBlkSize) - 1);

    if ((tBuddyBuf *) (gBuddyTable[u1Id].pu4FreeQ[u4BuddyIdx]) != NULL)
    {
        ((tBuddyBuf *) pu1Buf)->pNext =
            (tBuddyBuf *) (gBuddyTable[u1Id].pu4FreeQ[u4BuddyIdx]);
    }
    else
    {
        ((tBuddyBuf *) pu1Buf)->pNext = NULL;
    }
    gBuddyTable[u1Id].pu4FreeQ[u4BuddyIdx] = (UINT4) pu1Buf;
    return BUDDY_SUCCESS;
}

/******************************************************************************
 * Function    : BuddyDeleteFirstBlock ()
 * Description : This function deletes the first buddy block from corresponding 
 *               buddy queue which matches the given size
 * Input(s)    : u1Id        - The Buddy Identifier
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : BuddyBlock which is deleted 
 ******************************************************************************/

PRIVATE UINT1      *
BuddyDeleteFirstBlock (UINT1 u1Id, UINT4 u4BuddySize)
{
    UINT1              *pu1Buf = NULL;
    UINT4               u4BuddyIdx = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddyDeleteFirstBlock ()\n"));

    u4BuddyIdx = ((u4BuddySize / gBuddyTable[u1Id].u4MinBlkSize) - 1);

    pu1Buf = (UINT1 *) gBuddyTable[u1Id].pu4FreeQ[u4BuddyIdx];

    if (pu1Buf != NULL)
    {
        gBuddyTable[u1Id].pu4FreeQ[u4BuddyIdx] =
            (UINT4) ((tBuddyBuf *) pu1Buf)->pNext;
    }

    SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyDeleteFirstBlock ()\n"));

    return (pu1Buf);
}

/*******************************************************************************
 * Function    : BuddyDeleteInMiddle ()
 * Description : This function delinks the given buddy block from the queue
 *               which matches the given size
 * Input(s)    : u1Id        - The Buddy Identifier
 *               pu1BuddyBlk - Pointer to the Block to be deleted
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified
 * Returns     : BUDDY_SUCCESS, if the given block is successfully De-linked
 *               BUDDY_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
BuddyDeleteInMiddle (UINT1 u1Id, UINT1 *pu1BuddyBlk, UINT4 u4BuddySize)
{
    UINT1               u1DelFlag = BUDDY_FALSE;
    INT4                i4RetVal = BUDDY_FAILURE;
    UINT4               u4BuddyIdx = 0;
    tBuddyBuf          *pPrevBlk = NULL;
    tBuddyBuf          *pBuddyBlk = NULL;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddyDeleteFirstBlock ()\n"));

    /* Get the Queue Index
     */

    u4BuddyIdx = ((u4BuddySize / gBuddyTable[u1Id].u4MinBlkSize) - 1);

    pBuddyBlk = (tBuddyBuf *) gBuddyTable[u1Id].pu4FreeQ[u4BuddyIdx];

    /* If the queue pointer is not equal to Null then go ahead
     */

    while (pBuddyBlk != NULL)
    {
        if (pu1BuddyBlk == (UINT1 *) pBuddyBlk)
        {
            if (pPrevBlk == NULL)
            {
                gBuddyTable[u1Id].pu4FreeQ[u4BuddyIdx] =
                    (UINT4) (pBuddyBlk->pNext);
            }
            else
            {
                pPrevBlk->pNext = pBuddyBlk->pNext;
            }
            u1DelFlag = BUDDY_TRUE;
            break;
        }
        pPrevBlk = pBuddyBlk;
        pBuddyBlk = pBuddyBlk->pNext;
    }
    if (u1DelFlag != BUDDY_TRUE)
    {
        SBP_WARNING ((BUDDY_LS, "SBD <W> : Illegal Deletion\n"));
        getchar ();
    }

    SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyDeleteInMiddle ()\n"));

    return (i4RetVal);
}

/*******************************************************************************
 * Function    : BuddySetACBits ()
 * Description : This function set the AC (Allocation and Continuation) bits
 *               in the Header of the buffer whenever a Block is allocated from 
 *               the Buddy Buffer.
 * Input(s)    : u1Id        - The Buddy Identifier
 *               pu1BuddyBuf - Pointer to the Buddy Buffer which includes the
 *                             given block
 *               pu1BuddyBlk - Pointer to the Block which is being allocated
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified 
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
BuddySetACBits (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 *pu1BuddyBlk,
                UINT4 u4BuddySize)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1StartBit = 0;
    UINT1               u1ChkMask = 0;
    UINT1               u1BitPos = 0;
    UINT2               u2BitOffset = 0;
    UINT2               u2NumBitsToSet = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddySetACBits ()\n"));

    pu1Buf = pu1BuddyBuf;

    /* BUDDY_BIT_OFFSET gives the bit offset in the header which corresponds to
     * the buddy block. 
     * Note: Each minimum size block will have 2 bits in the header, one A =
     * alloc bit and C = continue bit. BUDDY_BIT_OFFSET is used to calculate the
     * appropriate bit corresponding to the given buddy block
     */

    u2BitOffset = (UINT2) BUDDY_BIT_OFFSET (u1Id, pu1Buf, pu1BuddyBlk);

    /* BUDDY_START_BIT gives the offset of bit in a given byte 
     */

    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);

    /* Increment the pu1Buf to the position in the header to reset 
     * the AC bits
     */

    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));

    u2NumBitsToSet = (UINT2)
        ((u4BuddySize / gBuddyTable[u1Id].u4MinBlkSize) * 2);

    u1ChkMask = (UINT1) (0xC0 >> u1StartBit);
    u1BitPos = u1StartBit;

    while (u2NumBitsToSet > 0)
    {
        if ((u2NumBitsToSet <= 8) || (u1StartBit != 0))
        {
            *pu1Buf &= (UINT1) (~u1ChkMask);
            if (u2NumBitsToSet == 2)
            {
                *pu1Buf |= (0xC0 >> u1BitPos);
            }
            else
            {
                *pu1Buf |= (0x80 >> u1BitPos);
            }
            u2NumBitsToSet -= 2;
            u1StartBit += 2;
            if (u1StartBit == 8)
            {
                pu1Buf = pu1Buf + 1;
                u1BitPos = 0;
                u1StartBit = 0;
                u1ChkMask = 0xC0;
            }
            else
            {
                u1BitPos += 2;
                u1ChkMask >>= 2;
            }
        }
        else
        {
            *pu1Buf = BUDDY_GET_MID_SET_MASK ();
            pu1Buf = pu1Buf + 1;
            u2NumBitsToSet -= 8;
            u1BitPos = 0;
        }
    }

    SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddySetACBits ()\n"));
}

/*******************************************************************************
 * Function    : BuddyResetACBits ()
 * Description : This function set the AC (Allocation and Continuation) bits
 *               in the Header of the buffer whenever a Block is allocated from 
 *               the Buddy Buffer.
 * Input(s)    : pu1BuddyBuf - Pointer to the Buddy Buffer which includes the
 *                             given block
 *               pu1BuddyBlk - Pointer to the Block which is being allocated
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified 
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
BuddyResetACBits (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 *pu1BuddyBlk,
                  UINT4 *pu4BuddySize)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1StartBit = 0;
    UINT1               u1SetMask = 0;
    UINT1               u1ChkMask = 0;
    UINT1               u1BitPos = 0;
    UINT2               u2BitOffset = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddySetACBits ()\n"));

    pu1Buf = pu1BuddyBuf;

    /* BUDDY_BIT_OFFSET gives the offset of bit from the starting of the
     * NextPointer field in the pu1BuddyBuf 
     */

    u2BitOffset = (UINT2) BUDDY_BIT_OFFSET (u1Id, pu1Buf, pu1BuddyBlk);

    /* BUDDY_START_BIT gives the offset of bit ina  given byte 
     */

    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);

    /* Increment the pu1Buf to the position in the header to reset 
     * the AC bits
     */

    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));
    u1ChkMask = (UINT1) (0xC0 >> u1StartBit);
    u1SetMask = (UINT1) (0x80 >> u1StartBit);
    u1BitPos = u1StartBit;
    *pu4BuddySize = 0;

    while (1)
    {
        if ((*pu1Buf & u1ChkMask) == u1ChkMask)
        {
            *pu1Buf &= ~u1ChkMask;
            *pu1Buf |= (0x40 >> u1BitPos);
            (*pu4BuddySize)++;
            break;
        }
        else if ((*pu1Buf & u1ChkMask) == u1SetMask)
        {
            *pu1Buf &= ~u1ChkMask;
            (*pu4BuddySize)++;
        }
        u1BitPos += 2;

        if (u1BitPos == 8)
        {
            u1BitPos = 0;
            u1ChkMask = 0xC0;
            u1SetMask = 0x80;
            pu1Buf = pu1Buf + 1;
        }
        else
        {
            u1ChkMask >>= 2;
            u1SetMask >>= 2;
        }
    }

    *pu4BuddySize *= gBuddyTable[u1Id].u4MinBlkSize;

    SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddySetACBits ()\n"));
}

/*******************************************************************************
 * Function    : BuddySetBACBits ()
 * Description : This function sets the BAC (Boundary Allocation and
 *               Continuation) bits in the Header of the buffer when the
 *               associated blocks are fragmented.
 * Input(s)    : pu1BuddyBuf - Pointer to the buddy buffer
 *               pu1BuddyBlk - Pointer to the Block which is being fragmented
 *               u4BuddySize - Size of the buddy block
 * Outputs(s)  : None
 * Globals     : gBuddyTable Referred, Not Modified 
 * Returns     : BUDDY_SUCCESS
 ******************************************************************************/

PRIVATE INT4
BuddySetBACBits (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 *pu1BuddyBlk,
                 UINT4 u4BuddySize)
{
    UINT1              *pu1Buf = NULL;
    UINT1               u1StartBit = 0;
    UINT2               u2BitOffset = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddySetBACBits ()\n"));

    pu1Buf = pu1BuddyBuf;

    /* move to the last offset, i.e the boundary of the block
     */

    u2BitOffset = (UINT2)
        BUDDY_BIT_OFFSET (u1Id, pu1Buf, (pu1BuddyBlk +
                                         (u4BuddySize -
                                          gBuddyTable[u1Id].u4MinBlkSize)));

    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);

    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));

    *pu1Buf &= ~(0xC0 >> u1StartBit);
    *pu1Buf |= (0x40 >> u1StartBit);

    SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddySetBACBits ()\n"));

    return BUDDY_SUCCESS;
}

/******************************************************************************
 *  Function Name   : BuddyTRAndMerge
 *  Description     : This function merges the released blocks with the other
 *                    released blocks at the right if any.
 *  Input (s)        : pu1BuddyBuf - The pointer the buddy buffer
 *                    u2BlkSize - The released block size
 *                    pu2RBitOffset - The right offset 
 *  Outputs (s)      : pu2RBitOffset - The new right offset after merger
 *                    pu2NewBlkSize - The Merged block size
 *                    pbRMFlag      - The Right merge flag, if true there are 
 *                                    blocks at the right which can be merged. 
 *  Globals          : refers to global Buddy table 
 *  Returns         : BUDDY_SUCCESS 
 ******************************************************************************/

PRIVATE INT4
BuddyTRAndMerge (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 *pu1BuddyBlk,
                 UINT4 *pu4BlkSize)
{
    UINT1               u1StartBit = 0;
    UINT1               u1SetPat = BUDDY_FALSE;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1FreeBlk = NULL;
    UINT1               u1BitMask = 0;
    UINT1               u1EndChkMask = 0;
    UINT4               u4FreeBlkSize = 0;
    UINT2               u2BitOffset = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddyTRAndMerge ()\n"));

    pu1Buf = pu1BuddyBuf;
    u2BitOffset = (UINT2) BUDDY_BIT_OFFSET (u1Id, pu1Buf,
                                            (pu1BuddyBlk + *pu4BlkSize));

    /* If the given Block is the rightmost Block, then return as it cannot be
     * merged
     */

    if (u2BitOffset == (gBuddyTable[u1Id].u4MaxBlkSize /
                        gBuddyTable[u1Id].u4MinBlkSize) * 2)
    {
        SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyTRAndMerge ()\n"));
        return BUDDY_FAILURE;
    }

    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));
    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);
    u1BitMask = (UINT1) (0xC0 >> u1StartBit);
    u1EndChkMask = (UINT1) (0x40 >> u1StartBit);

    /* If the Block right to the given block is Allocated block, Then
     * return
     */

    if (((*pu1Buf & u1BitMask) != 0) && ((*pu1Buf & u1BitMask) != u1EndChkMask))
    {
        SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyTRAndMerge ()\n"));
        return BUDDY_FAILURE;
    }

    /* Since the Buffer right to the given block is free, reset the boundary
     * bit of the free block
     */

    if (u1StartBit != 0)
    {
        *pu1Buf &= ~(u1BitMask << 2);
        u4FreeBlkSize = BUDDY_GET_FREE_RIGHT_BLKS ((*pu1Buf &
                                                    (0xFF >> u1StartBit)));
        u4FreeBlkSize -= (u1StartBit / 2);
    }
    else
    {
        *(pu1Buf - 1) &= 0xFC;
        u4FreeBlkSize = BUDDY_GET_FREE_RIGHT_BLKS (*pu1Buf);
    }

    if ((*pu1Buf & (0xFF >> u1StartBit)) == 0)
    {
        u1SetPat = BUDDY_TRUE;
        pu1Buf = pu1Buf + 1;
    }

    while (u1SetPat == BUDDY_TRUE)
    {
        if (*pu1Buf != 0)
        {
            u4FreeBlkSize += BUDDY_GET_FREE_RIGHT_BLKS (*pu1Buf);
            u1SetPat = BUDDY_FALSE;
        }
        else
        {
            u4FreeBlkSize += 4;
            pu1Buf = pu1Buf + 1;
        }
    }

    u4FreeBlkSize *= gBuddyTable[u1Id].u4MinBlkSize;
    pu1FreeBlk = pu1BuddyBlk + *pu4BlkSize;

    BuddyDeleteInMiddle (u1Id, pu1FreeBlk, u4FreeBlkSize);
    *pu4BlkSize = (*pu4BlkSize) + u4FreeBlkSize;
    return BUDDY_SUCCESS;
}

/******************************************************************************
 *  Function Name   : BuddyTLAndMerge
 *  Description     : This function merges the released blocks with the other
 *                    released blocks at the right if any.
 *  Input (s)        : pu1BuddyBuf - The pointer the buddy buffer
 *                    u2BlkSize - The released block size
 *                    pu2RBitOffset - The right offset 
 *  Outputs (s)      : pu2RBitOffset - The new right offset after merger
 *                    pu2NewBlkSize - The Merged block size
 *                    pbRMFlag      - The Right merge flag, if true there are 
 *                                    blocks at the right which can be merged. 
 *  Globals          : refers to global Buddy table 
 *  Returns         : BUDDY_SUCCESS 
 ******************************************************************************/

PRIVATE VOID
BuddyTLAndMerge (UINT1 u1Id, UINT1 *pu1BuddyBuf, UINT1 **pu1BuddyBlk,
                 UINT4 *pu4BlkSize)
{
    UINT1               u1StartBit = 0;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1FreeBlk = NULL;
    UINT1               u1BitMask = 0;
    UINT1               u1SetPat = BUDDY_FALSE;
    UINT2               u2NumBlks = 0;
    UINT4               u4FreeBlkSize = 0;
    UINT2               u2BitOffset = 0;

    SBP_EE ((BUDDY_LS, "SBD <X> : Entered BuddyTRAndMerge ()\n"));

    pu1Buf = pu1BuddyBuf;
    u2BitOffset = (UINT2) BUDDY_BIT_OFFSET (u1Id, pu1Buf, (*pu1BuddyBlk));

    if (u2BitOffset == 0)
    {
        SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyTRAndMerge ()\n"));
        return;
    }
    pu1Buf = BUDDY_INCR_BUF (u1Id, pu1Buf, BUDDY_START_BYTE (u2BitOffset));
    u1StartBit = (UINT1) BUDDY_START_BIT (u2BitOffset);
    u2BitOffset -= u1StartBit;

    if (u1StartBit == 0)
    {
        pu1Buf = pu1Buf - 1;
        u1StartBit = 8;
        u2BitOffset -= 8;
    }

    u1BitMask = (UINT1) (0xC0 >> (u1StartBit - 2));

    if ((*pu1Buf & u1BitMask) != (0x40 >> (u1StartBit - 2)))
    {
        SBP_EE ((BUDDY_LS, "SBD <X> : Exiting BuddyTLAndMerge ()\n"));
        return;
    }

    *pu1Buf &= ~u1BitMask;
    u2NumBlks = BUDDY_GET_FREE_LEFT_BLKS ((*pu1Buf &
                                           (0xFF << (8 - u1StartBit))));
    u2NumBlks -= ((8 - u1StartBit) / 2);

    if ((*pu1Buf & (0xFF << (8 - u1StartBit))) == 0)
    {
        u1SetPat = BUDDY_TRUE;
        pu1Buf = pu1Buf - 1;
    }

    while ((u1SetPat == BUDDY_TRUE) && (u2BitOffset > 0))
    {
        if (*pu1Buf != 0)
        {
            u2NumBlks += BUDDY_GET_FREE_LEFT_BLKS (*pu1Buf);
            u1SetPat = BUDDY_FALSE;
        }
        else
        {
            u2NumBlks += 4;
            pu1Buf = pu1Buf - 1;
            u2BitOffset -= 8;
        }
    }

    u4FreeBlkSize = u2NumBlks * gBuddyTable[u1Id].u4MinBlkSize;
    pu1FreeBlk = (*pu1BuddyBlk) - u4FreeBlkSize;

    BuddyDeleteInMiddle (u1Id, pu1FreeBlk, u4FreeBlkSize);
    *pu1BuddyBlk = pu1FreeBlk;
    *pu4BlkSize = (*pu4BlkSize) + u4FreeBlkSize;
}

PUBLIC VOID
BuddyPrintStatistics (UINT1 u1Id)
{
    UINT2              *pu1FreeMem = NULL;
    UINT2               u2Cnt = 0;
    UINT2               u2NumBlks = 0;
    UINT4               u4FreeBuf = 0;
    tBuddyBuf          *pBuf = NULL;

    u2NumBlks = (UINT2) ((gBuddyTable[u1Id].u4MaxBlkSize) /
                         (gBuddyTable[u1Id].u4MinBlkSize));
    pu1FreeMem = (UINT2 *) BUDDY_ALLOC_BUF (u2NumBlks, sizeof (UINT2), UINT2 *);
    for (u2Cnt = 0; u2Cnt < u2NumBlks; u2Cnt++)
    {
        pu1FreeMem[u2Cnt] = 0;
        pBuf = (tBuddyBuf *) (gBuddyTable[u1Id].pu4FreeQ[u2Cnt]);
        while (pBuf != NULL)
        {
            pu1FreeMem[u2Cnt]++;
            pBuf = pBuf->pNext;
        }
    }

    for (u2Cnt = 0; u2Cnt < u2NumBlks; u2Cnt++)
    {
        SBP_PT ((BUDDY_LS, "SBD <T> : Buffer size %u, NumBlks Free %u\n",
                 (gBuddyTable[u1Id].u4MinBlkSize * (u2Cnt + 1)),
                 pu1FreeMem[u2Cnt]));
        u4FreeBuf +=
            (gBuddyTable[u1Id].u4MinBlkSize * (u2Cnt + 1)) * pu1FreeMem[u2Cnt];
    }

    SBP_PT ((BUDDY_LS, "SBD <T> : Total Free Buffer = %u,"
             " Total Allocated Buffer = %u\n",
             u4FreeBuf, gBuddyTable[u1Id].u4MemAlloc));
}
