
/*******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isdecutl.c,v 1.3 2009/08/04 14:29:16 prabuc-iss Exp $
 *
 * Description: This file contains decision Module utilities
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/****************************************************************************
 * Function    : IsisUtlGetNextMetric ()
 * Description : This function retrives the next Metric index supported by
 *               the system
 * Input       : u1MetIdx         - The current Metric Index
 * Output      : pu1NextMetricIdx - The pointer to Next Metric Index
 * Returns     : ISIS_SUCCESS or ISIS_FAILURE
 ****************************************************************************/

PUBLIC INT4
IsisUtlGetNextMetric (INT1 i1MetIdx, UINT1 *pu1NextMetricIdx)
{
    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisUtlGetNextMetric () \n"));

    if (i1MetIdx < (ISIS_NUM_METRICS - 1))
    {
        *pu1NextMetricIdx = ++i1MetIdx;

        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecGetNextSPTNode () \n"));

        return ISIS_SUCCESS;
    }

    DEP_PT ((ISIS_LGST, "DEC <T> : Next Metric Index not supported \n"));
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecGetNextSPTNode () \n"));

    return ISIS_FAILURE;
}

/*******************************************************************************
 * Function    : IsisUtlGetNextLevel ()
 * Description : This function retrives the next supported level of the context 
 * Input       : pContext      - Pointer to the system context
 *               u1Level       - The current Level 
 * Output      : pu1NextLevel  - The pointer to the next Level
 * Returns     : ISIS_SUCCESS or ISIS_FAILURE
 ******************************************************************************/

PUBLIC INT4
IsisUtlGetNextLevel (tIsisSysContext * pContext,
                     UINT1 u1Level, UINT1 *pu1NextLevel)
{
    INT4                i4RetVal = ISIS_FAILURE;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisUtlGetNextLevel () \n"));

    if (u1Level == 0)
    {
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            *pu1NextLevel = ISIS_LEVEL1;
        }
        else
        {
            *pu1NextLevel = ISIS_LEVEL2;
        }
        i4RetVal = ISIS_SUCCESS;
    }
    else if ((u1Level == 1) && (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        *pu1NextLevel = ISIS_LEVEL2;
        i4RetVal = ISIS_SUCCESS;
    }
    else
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Unable to get next level \n"));

        i4RetVal = ISIS_FAILURE;
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecGetNextSPTNode () \n"));

    return i4RetVal;
}
