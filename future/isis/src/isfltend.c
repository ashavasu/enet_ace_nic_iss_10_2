 /*****************************************************************************
  * 
  *  Copyright (C) Future Software Limited,  2001
  *
  *  $Id: isfltend.c,v 1.14 2016/03/26 09:55:17 siva Exp $
  *
  *  Description: This file contains the Fault Tolerance Module Endian
  *  conversion routines.
  *
  ****************************************************************************/

#ifdef ISIS_FT_ENABLED
#include "isincl.h"
#include "isextn.h"

/*******************************************************************************
 * Function    : IsisFltrBldCktRec ()
 * Description : This routine encodes the given circuit record 'pCktRec' into 
 *               the given buffer 'pBuf' starting from offset 'pu2Offset'
 * Input(s)    : pRmMsg    - Pointer to the buffer where the record is to
 *                           be copied
 *               pu2Offset - Offset where the encoding starts
 * Output(s)   : pu2Offset - Offset from where the subsequent encoding 
 *                           starts i.e. current encoding ends
 *               pCktRec   - Pointer to the Circuit Record
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldCktRec (tRmMsg * pRmMsg, tIsisCktEntry * pCktRec, UINT2 *pu2Offset)
{
    UINT2               u2Offset = *pu2Offset;
    /* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->u4CktIdx);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->u4CktIfIdx);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->u4CktIfSubIdx);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->u4LLHandle);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1CktLocalID);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->bCktAdminState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1CktExistState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1CktType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->bCktExtDomain);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1CktLevel);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1CktMCAddr);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->bCktPassiveCkt);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1CktMeshGrpEnabled);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->bCktSmallHello);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pCktRec->u2CktMeshGrps);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1CktIfStatus);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->bTxEnable);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->bRxEnable);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1QFlag);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->u4CktUpTime);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->u4CktMtu);

    /* Pointer to the L1 CktLevel Record
     */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to the L2 CktLevel Record */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Circuit Stats Table */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->CktStats.u4CktAdjChgs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->CktStats.u4CktInitFails);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->CktStats.u4TxISHPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->CktStats.u4RxISHPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->CktStats.u4CktRejAdj);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->CktStats.u4CktOutCtrlPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->CktStats.u4CktInCtrlPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->CktStats.u4CktIdLenMsmtchs);

    /* Pointer to the Adjacency Record */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to Context */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to Next Circuit Record */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to Next Hash Entry */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to Prev Hash Entry */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pCktRec->au1P2PCktID, u2Offset,
                         (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));

    ISIS_RED_PUT_N_BYTE (pRmMsg, pCktRec->au1SNPA, u2Offset,
                         ISIS_SNPA_ADDR_LEN);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1ISHTxCnt);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1NodeID);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1OperState);
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1IfMTId);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pCktRec->au1IPV4Addr, u2Offset,
                         ISIS_MAX_IPV4_ADDR_LEN);

    /* BFD enable/disable status for a CktRec */
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1IsisBfdStatus);
	
	ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktRec->u1P2PDynHshkMachanism);
	ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktRec->u4ExtLocalCircID);
    /* Reserved field 
     */

#if ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN + ISIS_SNPA_ADDR_LEN \
      + ISIS_MAX_IPV4_ADDR_LEN + 3) % 4)
    for (u1IsMetric = 0;
         u1IsMetric < ((4 - ((ISIS_SYS_ID_LEN +
                              ISIS_PNODE_ID_LEN + ISIS_MAX_IPV4_ADDR_LEN +
                              ISIS_SNPA_ADDR_LEN + 3) % 4))); u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif
    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function    : IsisFltrBldCktLevelRec ()
 * Description : This routine encodes the given circuit level record 
 *               'pCktLvlRec' into the given buffer 'pBuf' starting from offset
 *               'pu2Offset'
 * Input(s)    : pRmMsg     - Pointer to the buffer where the record is to
 *                            be copied
 *               pu2Offset  - Offset where the encoding starts
 * Output(s)   : pu2Offset  - Offset from where the subsequent encoding 
 *                            starts i.e. current encoding ends
 *               pCktLvlRec - Pointer to the Circuit Level Record to be copied
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldCktLevelRec (tRmMsg * pRmMsg, tIsisCktLevel * pCktLvlRec,
                        UINT2 *pu2Offset)
{
    UINT2               u2Offset = 0;
    UINT1               u1Count = 0;
    UINT1               u1IsMetric = 0;
    /* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */

    u2Offset = *pu2Offset;

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktLvlRec->u1CktLvlIdx);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktLvlRec->u1ISPriority);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktLvlRec->u1CircLevelAuthType);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pCktLvlRec->u2HelloMultiplier);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->LspThrottleInt);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->DRHelloTimeInt);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->HelloTimeInt);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktLvlRec->bIsDIS);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pCktLvlRec->u2ThrtlCount);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pCktLvlRec->PSNPInterval);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pCktLvlRec->CSNPInterval);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktLvlRec->u1ECTId);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktLvlRec->u1HelloTmrIdx);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->u4MinLspReTxInt);

    /* Received Stats Table */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pCktLvlRec->RcvdPktStats.u4HelloPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pCktLvlRec->RcvdPktStats.u4LinkStatePDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->RcvdPktStats.u4CSNPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->RcvdPktStats.u4PSNPDUs);

    /* Sent Stats Table */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pCktLvlRec->SentPktStats.u4HelloPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pCktLvlRec->SentPktStats.u4LinkStatePDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->SentPktStats.u4CSNPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->SentPktStats.u4PSNPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->u4CktLanDISChgs);

    /* Pointer to the PSNP TLVs */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0x00);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->u4NumAdjs);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktLvlRec->u1LSPTxTmrIdx);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pCktLvlRec->Metric, u2Offset,
                         sizeof (tIsisMetric));

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->u4FullMetric);
#if ((ISIS_NUM_METRICS) % 4)
    ISIS_RED_PUT_N_BYTE (pRmMsg, pCktLvlRec->au1MetRsvd, u2Offset,
                         (4 - ((ISIS_NUM_METRICS) % 4)));

#endif

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pCktLvlRec->u4DISDirIdx);

    /* Ckt Tx Password Table */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pCktLvlRec->CktTxPasswd.u1Len);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pCktLvlRec->CktTxPasswd.u1ExistState);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pCktLvlRec->CktTxPasswd.au1Password, u2Offset,
                         ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
    for (u1IsMetric = 0; u1IsMetric < (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);

    }
#endif

    /* Ckt Rx Password Table */

    for (u1Count = 0; u1Count < ISIS_MAX_RX_PASSWDS; u1Count++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                             pCktLvlRec->aCktRxPasswd[u1Count].u1Len);

        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                             pCktLvlRec->aCktRxPasswd[u1Count].u1ExistState);

        ISIS_RED_PUT_N_BYTE (pRmMsg,
                             pCktLvlRec->aCktRxPasswd[u1Count].au1Password,
                             u2Offset, ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
        for (u1IsMetric = 0;
             u1IsMetric < (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)); u1IsMetric++)
        {
            ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
        }
#endif
    }

    /* Pointer to pMarkTxLSP */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0x00);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pCktLvlRec->au1CLCktID, u2Offset,
                         ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pCktLvlRec->au1CktLanDISID, u2Offset,
                         ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

    /* Reserved Bytes */

#if (((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN) * 2) % 4)
    for (u1IsMetric = 0;
         u1IsMetric < ((4 - (((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN) * 2) % 4)));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }

#endif
    *pu2Offset = u2Offset;

}

/*******************************************************************************
 * Function    : IsisFltrBldAdjRec()
 * Description : This routine encodes the given adjacency record 
 *               'pAdjRec' into the given buffer 'pBuf' starting from offset
 *               'pu2Offset'
 * Input(s)    : pRmMsg     - Pointer to the buffer where the record is to
 *                            be copied
 *               pu2Offset  - Offset where the encoding starts
 * Output(s)   : pu2Offset  - Offset from where the subsequent encoding 
 *                            starts i.e. current encoding ends
 *               pAdjRec    - Pointer to the Adjacency Record to be copied
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldAdjRec (tRmMsg * pRmMsg, tIsisAdjEntry * pAdjRec, UINT2 *pu2Offset)
{
     UINT1               u1IsMetric = 0;
    UINT2               u2Offset = *pu2Offset;
    UINT1               u1Cnt = 0;
	UINT4 				u4ElapsedTime = 0;
	UINT4				u4CurrentTime = 0;
    tUtlTm              utlTm;
	/* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */
    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pAdjRec->u4AdjIdx);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1AdjState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1AdjNbrSysType);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pAdjRec->u2HoldTime);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1TmrIdx);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1AdjUsage);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1AdjNbrPriority);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1AdjIpAddrType);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pAdjRec->u4AdjUpTime);

    /* Pointer to Circuit Record */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to AdjAreaAddr */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Adj Neighbour IP Address */

    ISIS_RED_PUT_N_BYTE (pRmMsg, pAdjRec->AdjNbrIpV4Addr.au1IpAddr, u2Offset,
                         ISIS_MAX_IPV4_ADDR_LEN);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->AdjNbrIpV4Addr.u1Length);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->AdjNbrIpV4Addr.u1PrefixLen);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pAdjRec->AdjNbrIpV6Addr.au1IpAddr, u2Offset,
            ISIS_MAX_IPV6_ADDR_LEN);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->AdjNbrIpV6Addr.u1Length);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->AdjNbrIpV6Addr.u1PrefixLen);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1AdjMTId);
    /* Reserved field */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to next Adjacency record */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    for (u1Cnt = 0; u1Cnt < ISIS_MAX_PROTS_SUPP; u1Cnt++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->au1AdjProtSupp[u1Cnt]);
    }

    ISIS_RED_PUT_N_BYTE (pRmMsg, pAdjRec->au1AdjNbrSNPA, u2Offset,
                         ISIS_SNPA_ADDR_LEN);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pAdjRec->au1AdjNbrSysID, u2Offset,
                         ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

	ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pAdjRec->u2AdjGRTime);

	ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1IsisGRHelperStatus);
    
    /* BFD related variables */
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1IsisMT0BfdEnabled);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1IsisMT2BfdEnabled);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1IsisBfdRequired);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1IsisMT0BfdState);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1IsisMT2BfdState);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1IsisBfdNeighborUseable);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1DoNotUpdHoldTmr);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1Mt0BfdRegd);
    
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1Mt2BfdRegd);
	/*Calculate Elapsed Time*/
	if(pAdjRec->u1IsisGRHelperStatus == ISIS_GR_HELPING)
	{
		MEMSET (&utlTm, 0, sizeof (tUtlTm));
		UtlGetTime (&utlTm);
		/* Get the seconds since the base year (2000) */
		u4CurrentTime = IsisGrGetSecondsSinceBase (utlTm);
		u4ElapsedTime = u4CurrentTime - pAdjRec->u4StartTime;
		if(u4ElapsedTime > pAdjRec->u2AdjGRTime)
		{
			u4ElapsedTime = (UINT4) pAdjRec->u2AdjGRTime;
		}
	}
	ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, u4ElapsedTime);

	ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pAdjRec->u4NeighExtCircuitID);
	ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pAdjRec->u4AdjPeerNbrExtCircuitID);
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1P2PThreewayState);
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1P2PPeerThreewayState);
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjRec->u1ThreeWayHndShkVersion);
    ISIS_RED_PUT_N_BYTE (pRmMsg, pAdjRec->au1AdjPeerNbrSysID, u2Offset,
                         									ISIS_SYS_ID_LEN);

#if ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN + ISIS_SNPA_ADDR_LEN + \
			ISIS_MAX_PROTS_SUPP) % 4)
    for (u1IsMetric = 0;
         u1IsMetric != ((4 - ((ISIS_SYS_ID_LEN +
                               ISIS_PNODE_ID_LEN + ISIS_SNPA_ADDR_LEN
                               + ISIS_MAX_PROTS_SUPP) % 4))); u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#else
    UNUSED_PARAM (u1IsMetric);
#endif
    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function    : IsisFltrBldIPRARec ()
 * Description : This routine encodes the given IPRA record 'pIPRARec' into the
 *               given buffer 'pBuf' starting from offset 'pu2Offset'
 * Input(s)    : pRmMsg    - Pointer to the buffer where the record is to
 *                           be copied
 *               pu2Offset - Offset where the encoding starts
 * Output(s)   : pu2Offset - Offset from where the subsequent encoding 
 *                           starts i.e. current encoding ends
 *               pIPRARec  - Pointer to the IPRA Record to be copied
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldIPRARec (tRmMsg * pRmMsg, tIsisIPRAEntry * pIPRARec,
                    UINT2 *pu2Offset)
{

    UINT1               u1IsMetric = 0;
    UINT2               u2Offset = *pu2Offset;

    /* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pIPRARec->u4IPRAIdx);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPRARec->u1IPRAType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPRARec->u1IPRADestType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPRARec->u1IPRAExistState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPRARec->u1IPRAAdminState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPRARec->u1QFlag);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPRARec->u1PrefixLen);

    /* Pointer to next IPRA Rec */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pIPRARec->Metric, u2Offset,
                         sizeof (tIsisMetric));

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pIPRARec->u4FullMetric);
#if (ISIS_NUM_METRICS % 4)

    for (u1IsMetric = 0; u1IsMetric != (4 - (ISIS_NUM_METRICS % 4));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif

    ISIS_RED_PUT_N_BYTE (pRmMsg, pIPRARec->au1IPRADest, u2Offset,
                         ISIS_MAX_IP_ADDR_LEN);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pIPRARec->au1IPRASNPA, u2Offset,
                         ISIS_SNPA_ADDR_LEN);

#if ((ISIS_MAX_IP_ADDR_LEN + ISIS_SNPA_ADDR_LEN) % 4)

    for (u1IsMetric = 0;
         u1IsMetric !=
         ((4 - ((ISIS_MAX_IP_ADDR_LEN + ISIS_SNPA_ADDR_LEN) % 4)));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif
    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function    : IsisFltrBldAdjAARec()
 * Description : This routine encodes the given Adjacency Area Address record 
 *               'pAdjAA' into the given buffer 'pBuf' starting from offset 
 *               'pu2Offset'
 * Input(s)    : pRmMsg    - Pointer to the buffer where the record is to
 *                           be copied
 *               pu2Offset - Offset where the encoding starts
 * Output(s)   : pu2Offset - Offset from where the subsequent encoding 
 *                           starts i.e. current encoding ends
 *               pAdjAA    - Pointer to the Adjacency Area Address Record to be
 *                           copied
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldAdjAARec (tRmMsg * pRmMsg, tIsisAdjAAEntry * pAdjAA,
                     UINT2 *pu2Offset)
{
    UINT1               u1IsMetric = 0;
    UINT2               u2Offset = *pu2Offset;;
    /* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pAdjAA->u4AdjAreaAddrIdx);

    ISIS_RED_PUT_N_BYTE (pRmMsg,
                         pAdjAA->ISAdjAreaAddress.au1AreaAddr, u2Offset,
                         ISIS_AREA_ADDR_LEN);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pAdjAA->ISAdjAreaAddress.u1Length);

#if ((ISIS_AREA_ADDR_LEN + 1) % 4)

    for (u1IsMetric = 0; u1IsMetric != (4 - ((ISIS_AREA_ADDR_LEN + 1) % 4));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif

    /* Pointer to Next AdjAA */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    *pu2Offset = u2Offset;

}

/*******************************************************************************
 * Function Name : IsisFltrBldSysContextRec ()
 * Description   : This routine encodes the given context 'pContext' into the
 *                 given buffer 'pBuf' starting from offset 'u2Offset'
 * Input(s)      : pRmMsg     - Pointer to the buffer where the encoded
 *                              information is updated
 *                 pu2Offset  - Offset where the encoding starts
 * Output(s)     : pu2Offset  - Offset from where the subsequent encoding 
 *                              starts i.e. current encoding ends
 *                 pContext   - Pointer to the Context which is to be encoded
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldSysContextRec (tRmMsg * pRmMsg, tIsisSysContext * pContext,
                          UINT2 *pu2Offset)
{
    /* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */

    UINT1               u1PSCnt = 0;
    UINT2               u2Offset = *pu2Offset;;

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->u4SysInstIdx);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->u1QFlag);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->u1OperState);

    /* Level 1 Info */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysL1Info.u1LSPGenCount);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysL1Info.u1SpfTrgCount);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pContext->SysL1Info.u2TrgCount);

 
    /* Level 2 Info */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysL2Info.u1LSPGenCount);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysL2Info.u1SpfTrgCount);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pContext->SysL2Info.u2TrgCount);


    /* Router ID */

    ISIS_RED_PUT_N_BYTE (pRmMsg, pContext->SysRouterID.au1IpAddr, u2Offset,
                         ISIS_MAX_IP_ADDR_LEN);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysRouterID.u1Length);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysRouterID.u1PrefixLen);

    /* Reserved */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    IsisFltrBldSysActualsRec (pRmMsg, pContext, &u2Offset);

    IsisFltrBldSysConfRec (pRmMsg, pContext, &u2Offset);

    /* System Statistics Table */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->SysStats.u4SysDroppedPDUs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysStats.u4SysAreaMismatches);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysStats.u4SysMaxAAMismatches);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysStats.u4SysMAADropFromAreas);

    /* Sys L1 Level Stats */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->SysL1Stats.u4SysCorrLSPs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->SysL1Stats.u4SysLSPDBOLs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL1Stats.u4SysAttemptToExcdMaxSeqNo);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL1Stats.u4SysSeqNumSkips);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL1Stats.u4SysOwnLSPPurges);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL1Stats.u4SysIDFieldLenMismatches);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->SysL1Stats.u4SysAuthFails);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL1Stats.u4SysAuthTypeFails);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL1Stats.u4SysPartChanges);

    /* Sys L2 Level Stats */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->SysL2Stats.u4SysCorrLSPs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->SysL2Stats.u4SysLSPDBOLs);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL2Stats.u4SysAttemptToExcdMaxSeqNo);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL2Stats.u4SysSeqNumSkips);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL2Stats.u4SysOwnLSPPurges);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL2Stats.u4SysIDFieldLenMismatches);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->SysL2Stats.u4SysAuthFails);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL2Stats.u4SysAuthTypeFails);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysL2Stats.u4SysPartChanges);

    /* Adjacency Direction Table */

    /* Num Entries */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to Dir Entry */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Adjacency Area Address Table */

    /* Pointer to AA Rec */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Num Entries */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* u2Reserved */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* Summary Address Table */

    /* Pointer to SA Entry */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Num Entries */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* u2Reserved */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* Manual Area Address Table */

    /* Pointer to MAA Record */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Num Entries */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* u2Reserved */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* Protocol Support Table */

    for (u1PSCnt = 0; u1PSCnt < ISIS_MAX_PROTS_SUPP; u1PSCnt++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                             pContext->aProtSupp[u1PSCnt].u1ProtSupp);

        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                             pContext->aProtSupp[u1PSCnt].u1ExistState);

        /* Reserved */
        ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);
    }

    /* Circuit Table */

    /* Num Entries */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Num Active Circuits */
    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Num L1 Adjacencies */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Num L2 Adjacencies */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Next Available Circuit Index */
    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 1);

    /* Pointer to Ckt Record */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to L1 Ckt Mask */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to L2 Ckt Mask */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* IP If Address Table */

    /* Pointer to IP Interface Addr */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Num Entries */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* Reserved */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* IPRA Table */

    /* Pointer to IP Interface Addr */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* Num Entries */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* Reserved */

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    /* Pointer to Next */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    /* BFD Support */
    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->u1IsisBfdSupport);

    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function     : IsisFltrBldIPIfRec ()
 * Description  : This routine encodes the given IP I/f Address 'pIPIfRec' into
 *                the given buffer 'pBuf' starting from offset 'u2Offset'
 * Input(s)     : pRmMsg     - Pointer to the buffer where the encoded
 *                             information is updated
 *                pu2Offset  - Offset where the encoding starts
 * Output(s)    : pu2Offset  - Offset from where the subsequent encoding 
 *                             starts i.e. current encoding ends
 *                pIPIfRec   - Pointer to the IP I/f Address record which is to
 *                             be encoded
 * Globals      : Not Referred or Modified
 * Returns      : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldIPIfRec (tRmMsg * pRmMsg, tIsisIPIfAddr * pIPIfRec, UINT2 *pu2Offset)
{
#if ((ISIS_MAX_IP_ADDR_LEN) % 4)
    UINT1               u1IsMetric = 0;
#endif
    UINT2               u2Offset = *pu2Offset;
    /* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */
    /* Pointer to Next */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pIPIfRec->u4CktIfIdx);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pIPIfRec->u4CktIfSubIdx);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pIPIfRec->au1IPAddr, u2Offset,
                         ISIS_MAX_IP_ADDR_LEN);

    /* Reserved */
#if ((ISIS_MAX_IP_ADDR_LEN) % 4)
    for (u1IsMetric = 0; u1IsMetric != ((4 - ((ISIS_MAX_IP_ADDR_LEN) % 4)));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPIfRec->u1AddrType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPIfRec->u1ExistState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pIPIfRec->u1SecondaryFlag);

    /* Reserved */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);

    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function     : IsisFltrBldMAARec()
 * Description  : This routine encodes the given Manual Area Address 'pMAARec' 
 *                into the given buffer 'pBuf' starting from offset 'u2Offset'
 * Input(s)     : pRmMsg     - Pointer to the buffer where the encoded
 *                             information is updated
 *                pu2Offset  - Offset where the encoding starts
 * Output(s)    : pu2Offset  - Offset from where the subsequent encoding 
 *                             starts i.e. current encoding ends
 *                pMAARec    - Pointer to the Manual Area Address record which 
 *                             is to be encoded
 * Globals      : Not Referred or Modified
 * Returns      : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldMAARec (tRmMsg * pRmMsg, tIsisMAAEntry * pMAARec, UINT2 *pu2Offset)
{
    UINT1               u1IsMetric = 0;
    UINT2               u2Offset = *pu2Offset;;
    /* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */

    ISIS_RED_PUT_N_BYTE (pRmMsg, pMAARec->ManAreaAddr.au1AreaAddr, u2Offset,
                         ISIS_AREA_ADDR_LEN);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pMAARec->ManAreaAddr.u1Length);

#if ((ISIS_AREA_ADDR_LEN + 1) % 4)
    for (u1IsMetric = 0; u1IsMetric != (4 - ((ISIS_AREA_ADDR_LEN + 1) % 4));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif

    /* Pointer to Next */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pMAARec->u1ExistState);

    /* Reserved 3 Bytes */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, 0);

    *pu2Offset = u2Offset;

}

/*******************************************************************************
 * Function     : IsisFltrBldSARec()
 * Description  : This routine encodes the given Summary Address 'pSARec' into
 *                the given buffer 'pBuf' starting from offset 'u2Offset'
 * Input(s)     : pRmMsg     - Pointer to the buffer where the encoded
 *                             information is updated
 *                pu2Offset  - Offset where the encoding starts
 * Output(s)    : pu2Offset  - Offset from where the subsequent encoding 
 *                             starts i.e. current encoding ends
 *                pSARec     - Pointer to the Summary Address record which is to
 *                             be encoded
 * Globals      : Not Referred or Modified
 * Returns      : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrBldSARec (tRmMsg * pRmMsg, tIsisSAEntry * pSARec, UINT2 *pu2Offset)
{
    /* NOTE: 
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     * 
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to 
     * encode the Data in Network Order into the buffer. Otherwise we can 
     * directly copy the contents as is
     */
    UINT2               u2Offset = *pu2Offset;

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pSARec->u1AddrType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pSARec->u1PrefixLen);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pSARec->u1ExistState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pSARec->u1AdminState);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pSARec->u2L1UsageCnt);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pSARec->u2L2UsageCnt);

    /* Pointer to next */

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, 0);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pSARec->au1SummAddr, u2Offset,
                         ISIS_MAX_IP_ADDR_LEN);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pSARec->Metric, u2Offset,
                         sizeof (tIsisMetric));

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pSARec->u4FullMetric);
#if ((ISIS_MAX_IP_ADDR_LEN + ISIS_NUM_METRICS) % 4)

    for (u1IsMetric = 0;
         u1IsMetric < ((4 - ((ISIS_MAX_IP_ADDR_LEN + ISIS_NUM_METRICS) % 4)));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif

    *pu2Offset = u2Offset;

}

/* Get Routines for building the Records form the received buffer
 */

/*******************************************************************************
 * Function    : IsisFltrGetCktEntry ()
 * Description : This routine constructs the circuit record from the information
 *               available in the received FLTR message
 * Input(s)    : pMsg     - Pointer to the message containing the circuit
 *                          information
 * Output(s)   : pCktRec  - Pointer to the Circuit Record which is to be
 *                          updated with the information retrieved from 'pMsg'
 *                          is copied
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetCktEntry (tRmMsg * pMsg, tIsisCktEntry * pCktRec, UINT2 *pu2Offset)
{
    UINT2               u2Offset = *pu2Offset;

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->u4CktIdx);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->u4CktIfIdx);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->u4CktIfSubIdx);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->u4LLHandle);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1CktLocalID);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->bCktAdminState);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1CktExistState);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1CktType);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->bCktExtDomain);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1CktLevel);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1CktMCAddr);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->bCktPassiveCkt);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1CktMeshGrpEnabled);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->bCktSmallHello);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pCktRec->u2CktMeshGrps);
    u2Offset =(UINT2)(u2Offset + 2);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1CktIfStatus);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->bTxEnable);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->bRxEnable);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1QFlag);
    u2Offset =(UINT2)(u2Offset + 1);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->u4CktUpTime);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->u4CktMtu);
    u2Offset =(UINT2)(u2Offset + 4);

    /* Ckt Level Pointer */

    pCktRec->pL1CktInfo = NULL;
    u2Offset =(UINT2)(u2Offset + 4);

    pCktRec->pL2CktInfo = NULL;
    u2Offset =(UINT2)(u2Offset + 4);

    /* Ckt Stats Table */

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->CktStats.u4CktAdjChgs);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->CktStats.u4CktInitFails);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->CktStats.u4TxISHPDUs);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->CktStats.u4RxISHPDUs);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->CktStats.u4CktRejAdj);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->CktStats.u4CktOutCtrlPDUs);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->CktStats.u4CktInCtrlPDUs);
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->CktStats.u4CktIdLenMsmtchs);
    u2Offset =(UINT2)(u2Offset + 4);

    /* Adj Rec Pointer  */

    pCktRec->pAdjEntry = NULL;
    u2Offset =(UINT2)(u2Offset + 4);

    /* Pointer to Context */

    pCktRec->pContext = NULL;
    u2Offset =(UINT2)(u2Offset + 4);

    /* Pointer to next Circuit Record */

    pCktRec->pNext = NULL;
    u2Offset =(UINT2)(u2Offset + 4);

    /* Pointer to next Hash Entry */

    pCktRec->pNextHashEntry = NULL;
    u2Offset =(UINT2)(u2Offset + 4);

    /* Pointer to previous Hash Entry */

    pCktRec->pPrevHashEntry = NULL;
    u2Offset =(UINT2)(u2Offset + 4);

    ISIS_RED_READ_N_BYTE (pMsg, pCktRec->au1P2PCktID, u2Offset,
                          (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));
    u2Offset = (UINT2)(u2Offset + (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));

    ISIS_RED_READ_N_BYTE (pMsg, pCktRec->au1SNPA, u2Offset, ISIS_SNPA_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_SNPA_ADDR_LEN);

    ISIS_RED_READ_1_BYTE (pMsg, *pu2Offset, pCktRec->u1ISHTxCnt);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1NodeID);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1OperState);
    u2Offset = (UINT2)(u2Offset + 1);
    
	ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1IfMTId);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_N_BYTE (pMsg, pCktRec->au1IPV4Addr, u2Offset,
                          ISIS_MAX_IPV4_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_IPV4_ADDR_LEN);

    /* BFD enable/disable status for a CktRec */
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1IsisBfdStatus);
    u2Offset = (UINT2) (u2Offset + 1);

	ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktRec->u1P2PDynHshkMachanism);
	u2Offset = (UINT2) (u2Offset + 1);
	
	ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktRec->u4ExtLocalCircID);
	u2Offset =(UINT2)(u2Offset + 4);

#if ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN + ISIS_SNPA_ADDR_LEN \
          + ISIS_MAX_IPV4_ADDR_LEN + 3) % 4)
    u2Offset = u2Offset + ((4 - ((ISIS_SYS_ID_LEN +
                                  ISIS_PNODE_ID_LEN + ISIS_MAX_IPV4_ADDR_LEN +
                                  ISIS_SNPA_ADDR_LEN + 3) % 4)));
#endif
    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function    : IsisFltrGetCktLevelRec ()
 * Description : This routine constructs a circuit level record from the
 *               information retrieved from the given FLTR message
 * Input(s)    : pMsg       - Pointer to the FLTR message from where the
 *                            information is retrieved to build the circuit
 *                            level record
 * Output(s)   : pCktLvlRec - Pointer to the circuit level record to be
 *                            constructed with information retrieved from 'pMsg'
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetCktLevelRec (tRmMsg * pMsg, tIsisCktLevel * pCktLvlRec,
                        UINT2 *pu2Offset)
{

    UINT1               u1Count = 0;
    UINT2               u2Offset = *pu2Offset;

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->u1CktLvlIdx);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->u1ISPriority);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->u1CircLevelAuthType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pCktLvlRec->u2HelloMultiplier);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->LspThrottleInt);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->DRHelloTimeInt);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->HelloTimeInt);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->bIsDIS);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->u2ThrtlCount);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pCktLvlRec->PSNPInterval);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pCktLvlRec->CSNPInterval);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->u1ECTId);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->u1HelloTmrIdx);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->u4MinLspReTxInt);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->RcvdPktStats.u4HelloPDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pCktLvlRec->RcvdPktStats.u4LinkStatePDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->RcvdPktStats.u4CSNPDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->RcvdPktStats.u4PSNPDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->SentPktStats.u4HelloPDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pCktLvlRec->SentPktStats.u4LinkStatePDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->SentPktStats.u4CSNPDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->SentPktStats.u4PSNPDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->u4CktLanDISChgs);
    u2Offset = (UINT2)(u2Offset + 4);

    /* Pointer to the PSNP TLVS */

    pCktLvlRec->pPSNP = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->u4NumAdjs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->u1LSPTxTmrIdx);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_N_BYTE (pMsg, pCktLvlRec->Metric, u2Offset,
                          sizeof (tIsisMetric));
    u2Offset = (UINT2)(u2Offset + sizeof (tIsisMetric));

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->u4FullMetric);
    u2Offset = (UINT2)(u2Offset + 4);

#if ((ISIS_NUM_METRICS) % 4)
    u2Offset = u2Offset + (4 - ((ISIS_NUM_METRICS) % 4));
#endif

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pCktLvlRec->u4DISDirIdx);
    u2Offset = (UINT2)(u2Offset + 4);

    /* Ckt Tx Password */

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->CktTxPasswd.u1Len);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pCktLvlRec->CktTxPasswd.u1ExistState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_N_BYTE (pMsg, pCktLvlRec->CktTxPasswd.au1Password, u2Offset,
                          ISIS_MAX_PASSWORD_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
    u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)));
#endif

    /* Ckt Rx Password */

    for (u1Count = 0; u1Count < ISIS_MAX_RX_PASSWDS; u1Count++)
    {
        ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                              pCktLvlRec->aCktRxPasswd[u1Count].u1Len);
        u2Offset = (UINT2)(u2Offset + 1);

        ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                              pCktLvlRec->aCktRxPasswd[u1Count].u1ExistState);
        u2Offset = (UINT2)(u2Offset + 1);

        ISIS_RED_READ_N_BYTE (pMsg,
                              pCktLvlRec->aCktRxPasswd[u1Count].au1Password,
                              u2Offset, ISIS_MAX_PASSWORD_LEN);
        u2Offset = (UINT2)(u2Offset + ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
        u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)));
#endif
    }

    /* Pointer  pMarkTxLSP */

    pCktLvlRec->pMarkTxLSP = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_N_BYTE (pMsg, pCktLvlRec->au1CLCktID, u2Offset,
                          (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));
    u2Offset = (UINT2)(u2Offset + ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

    ISIS_RED_READ_N_BYTE (pMsg, pCktLvlRec->au1CktLanDISID, u2Offset,
                          (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));
    u2Offset = (UINT2)(u2Offset + ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

#if (((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN) * 2) % 4)
    u2Offset = (UINT2)(u2Offset + ((4 - (((ISIS_SYS_ID_LEN +
                                   ISIS_PNODE_ID_LEN) * 2) % 4))));
#endif

    *pu2Offset = u2Offset;

}

/*******************************************************************************
 * Function    : IsisFltrGetAdjRec()
 * Description : This routine constructs an adjacency record from the
 *               information retrieved from the received FLTR message 'pMsg'
 * Input(s)    : pMsg     - Pointer to the received FLTR message which includes
 *                          adjacency information to be retrieved
 * Output(s)   : pAdjRec  - Pointer to the Adjacency Record to be built with the
 *                          information retrieved from the received FLTR message
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetAdjRec (tRmMsg * pMsg, tIsisAdjEntry * pAdjRec, UINT2 *pu2Offset)
{
    UINT1               u1Cnt = 0;
    UINT2               u2Offset = *pu2Offset;
    UINT4               u4CurrentTime = 0;
    UINT4               u4ElapsedTime = 0;
    tUtlTm              utlTm;

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pAdjRec->u4AdjIdx);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1AdjState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1AdjNbrSysType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pAdjRec->u2HoldTime);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1TmrIdx);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1AdjUsage);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1AdjNbrPriority);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1AdjIpAddrType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pAdjRec->u4AdjUpTime);
    u2Offset = (UINT2)(u2Offset + 4);

    /* Pointer to Circuit Record */

    pAdjRec->pCktRec = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    /* Pointer to AdjAreaAddr */

    pAdjRec->pAdjAreaAddr = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_N_BYTE (pMsg, pAdjRec->AdjNbrIpV4Addr.au1IpAddr, u2Offset,
                          ISIS_MAX_IPV4_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_IPV4_ADDR_LEN);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->AdjNbrIpV4Addr.u1Length);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->AdjNbrIpV4Addr.u1PrefixLen);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_N_BYTE (pMsg, pAdjRec->AdjNbrIpV6Addr.au1IpAddr, u2Offset,
                          ISIS_MAX_IPV6_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_IPV6_ADDR_LEN);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->AdjNbrIpV6Addr.u1Length);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->AdjNbrIpV6Addr.u1PrefixLen);
    u2Offset = (UINT2)(u2Offset + 1);

   
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1AdjMTId);
	
    u2Offset = (UINT2)(u2Offset + 1);

    /* Reserved field
     */

    u2Offset = (UINT2)(u2Offset + 2);

    /* Next Adjacency pointer */

    pAdjRec->pNext = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    for (u1Cnt = 0; u1Cnt < ISIS_MAX_PROTS_SUPP; u1Cnt++)
    {
        ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->au1AdjProtSupp[u1Cnt]);
        u2Offset = (UINT2)(u2Offset + 1);
    }

    ISIS_RED_READ_N_BYTE (pMsg, pAdjRec->au1AdjNbrSNPA, u2Offset,
                          ISIS_SNPA_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_SNPA_ADDR_LEN);

    ISIS_RED_READ_N_BYTE (pMsg, pAdjRec->au1AdjNbrSysID, u2Offset,
                          (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));
    u2Offset = (UINT2)(u2Offset + (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));

	ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pAdjRec->u2AdjGRTime);
    u2Offset = (UINT2)(u2Offset + 2);
	
	ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1IsisGRHelperStatus);
    u2Offset = (UINT2)(u2Offset + 1);
	
    /* BFD related variables */
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1IsisMT0BfdEnabled);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1IsisMT2BfdEnabled);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1IsisBfdRequired);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1IsisMT0BfdState);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1IsisMT2BfdState);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1IsisBfdNeighborUseable);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1DoNotUpdHoldTmr);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1Mt0BfdRegd);
    u2Offset = (UINT2)(u2Offset + 1);
    
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1Mt2BfdRegd);
    u2Offset = (UINT2)(u2Offset + 1);
	
    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, u4ElapsedTime);
    u2Offset = (UINT2)(u2Offset + 4);
	
	if(pAdjRec->u1IsisGRHelperStatus == ISIS_GR_HELPING)
	{
		if (u4ElapsedTime < pAdjRec->u2AdjGRTime)
		{
			MEMSET (&utlTm, 0, sizeof (tUtlTm));
			UtlGetTime (&utlTm);
			/* Get the seconds since the base year (2000) */
			u4CurrentTime = IsisGrGetSecondsSinceBase (utlTm);
			pAdjRec->u4StartTime = u4CurrentTime - (UINT4) u4ElapsedTime;
		}
	}

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pAdjRec->u4NeighExtCircuitID);
    u2Offset = (UINT2)(u2Offset + 4);
    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pAdjRec->u4AdjPeerNbrExtCircuitID);
    u2Offset = (UINT2)(u2Offset + 4);
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1P2PThreewayState);
    u2Offset = (UINT2)(u2Offset + 1);
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1P2PPeerThreewayState);
    u2Offset = (UINT2)(u2Offset + 1);
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAdjRec->u1ThreeWayHndShkVersion);
    u2Offset = (UINT2)(u2Offset + 1);
    ISIS_RED_READ_N_BYTE (pMsg, pAdjRec->au1AdjPeerNbrSysID, u2Offset,
                          (ISIS_SYS_ID_LEN));
    u2Offset = (UINT2)(u2Offset + (ISIS_SYS_ID_LEN));


#if ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN + ISIS_SNPA_ADDR_LEN + \
      ISIS_MAX_PROTS_SUPP) % 4)
    u2Offset = (UINT2)(u2Offset + ((4 - ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN +
                                  ISIS_SNPA_ADDR_LEN +
                                  ISIS_MAX_PROTS_SUPP) % 4))));
#endif

    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function    : IsisFltrGetMAARec ()
 * Description : This routine builds a MAA record from the information
 *               retrieved from the received FLTR message. 
 * Input(s)    : pMsg    - Pointer to the received FLTR message which includes
 *                         the encoded MAA information 
 * Output(s)   : pMAARec - Pointer to the MAA record which is built based on
 *                         the information retrieved from the FLTR message
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetMAARec (tRmMsg * pMsg, tIsisMAAEntry * pMAARec, UINT2 *pu2Offset)
{
    UINT2               u2Offset = *pu2Offset;

    ISIS_RED_READ_N_BYTE (pMsg, pMAARec->ManAreaAddr.au1AreaAddr, u2Offset,
                          ISIS_AREA_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_AREA_ADDR_LEN);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pMAARec->ManAreaAddr.u1Length);
    u2Offset = (UINT2)(u2Offset + 1);

#if ((ISIS_AREA_ADDR_LEN + 1) % 4)
    u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_AREA_ADDR_LEN + 1) % 4)));
#endif

    pMAARec->pNext = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pMAARec->u1ExistState);
    u2Offset = (UINT2)(u2Offset + 1);
    /* Advance the buffer 3 bytes reserved */
    u2Offset = (UINT2)(u2Offset + 3);

    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function    : IsisFltrGetSARec ()
 * Description : This routine builds a SA record from the information
 *                retrieved from the received FLTR message.
 * Input(s)    : pMsg   - Pointer to the received FLTR message which includes
 *                        the encoded SAA information 
 * Output(s)   : pSARec - Pointer to the SAA Record which is to be built using
 *                        the information retrieved from the FLTR message
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetSARec (tRmMsg * pMsg, tIsisSAEntry * pSARec, UINT2 *pu2Offset)
{
    UINT2               u2Offset = *pu2Offset;

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSARec->u1AddrType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSARec->u1PrefixLen);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSARec->u1ExistState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSARec->u1AdminState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSARec->u2L1UsageCnt);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSARec->u2L2UsageCnt);
    u2Offset = (UINT2)(u2Offset + 2);

    pSARec->pNext = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_N_BYTE (pMsg, pSARec->au1SummAddr, u2Offset,
                          ISIS_MAX_IP_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_IP_ADDR_LEN);

    ISIS_RED_READ_N_BYTE (pMsg, pSARec->Metric, u2Offset, sizeof (tIsisMetric));
    u2Offset = (UINT2) (u2Offset + sizeof (tIsisMetric));

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pSARec->u4FullMetric);
    u2Offset = (UINT2)(u2Offset + 4);
#if ((ISIS_MAX_IP_ADDR_LEN + ISIS_NUM_METRICS) % 4)
    u2Offset = (UINT2)(u2Offset +
        ((4 - ((ISIS_MAX_IP_ADDR_LEN + ISIS_NUM_METRICS) % 4))));
#endif

    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function    : IsisFltrGetIPIfRec ()
 * Description : This routine builds an IP Interface Address record from the
 *               information retrieved from the received FLTR message and adds
 *               it to the System Context
 * Input(s)    : pMsg     - Pointer to the message which includes the encoded IP
 *                          Interface Address information
 * Output(s)   : pIPIfRec - Pointer to the IP Interface Address record built
 *                          from the information retrieved from FLTR message
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetIPIfRec (tRmMsg * pMsg, tIsisIPIfAddr * pIPIfRec, UINT2 *pu2Offset)
{

    UINT2               u2Offset = *pu2Offset;

    pIPIfRec->pNext = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pIPIfRec->u4CktIfIdx);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pIPIfRec->u4CktIfSubIdx);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_N_BYTE (pMsg, pIPIfRec->au1IPAddr, u2Offset,
                          ISIS_MAX_IP_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_IP_ADDR_LEN);

    /* Move the offset */

#if ((ISIS_MAX_IP_ADDR_LEN) % 4)
    u2Offset = (UINT2)(u2Offset + ((4 - ((ISIS_MAX_IP_ADDR_LEN) % 4))));
#endif

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPIfRec->u1AddrType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPIfRec->u1ExistState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPIfRec->u1SecondaryFlag);
    u2Offset = (UINT2)(u2Offset + 1);
    
    /* Reserved  */
    u2Offset = (UINT2)(u2Offset + 1);

    *pu2Offset = u2Offset;
}

/*******************************************************************************
 * Function    : IsisFltrGetSysContextRec ()
 * Description : This routine builds a Context record from the information
 *               retrieved from the received FLTR message
 * Input(s)    : pMsg     - Pointer to the message which includes the encoded
 *                          context information
 *               pContext - Pointer to the Context Record which is to be updated
 *                          with the information retrieved from the FLTR message
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetSysContextRec (tRmMsg * pMsg, tIsisSysContext * pContext,
                          UINT2 *pu2Offset)
{

    UINT1               u1PSCnt = 0;
    UINT2               u2Offset = *pu2Offset;
    UINT1               u1MtIndex = 0;

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->u4SysInstIdx);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->u1QFlag);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->u1OperState);
    u2Offset = (UINT2)(u2Offset + 1);

    /* Disable the Oper State in the Standby Node irrespective of the Oper State
     * obtained from the Active Node. The Operational State of the Standby Node
     * is made ISIS_UP only when the GO_ACTIVE command is issued by FutureRM
     */

    pContext->u1OperState = ISIS_DOWN;
    /* Level 1 Info */

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->SysL1Info.u1LSPGenCount);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->SysL1Info.u1SpfTrgCount);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pContext->SysL1Info.u2TrgCount);
    u2Offset = (UINT2)(u2Offset + 2);

    for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
    {
        pContext->SysL1Info.PrevShortPath[u1MtIndex] = NULL;

        pContext->SysL1Info.ShortPath[u1MtIndex] = NULL;
    }

    /* Level 2 Info */

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->SysL2Info.u1LSPGenCount);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->SysL2Info.u1SpfTrgCount);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pContext->SysL2Info.u2TrgCount);
    u2Offset = (UINT2)(u2Offset + 2);

    for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
    {
        pContext->SysL2Info.PrevShortPath[u1MtIndex] = NULL;

        pContext->SysL2Info.ShortPath[u1MtIndex] = NULL;
    }

    /* Router ID */

    ISIS_RED_READ_N_BYTE (pMsg, pContext->SysRouterID.au1IpAddr, u2Offset,
                          ISIS_MAX_IP_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_IP_ADDR_LEN);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->SysRouterID.u1Length);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->SysRouterID.u1PrefixLen);
    u2Offset = (UINT2)(u2Offset + 1);

    /* reserved */

    u2Offset = (UINT2)(u2Offset + 2);

    IsisFltrGetSysActualsRec (pMsg, &pContext->SysActuals, &u2Offset);
    IsisFltrGetSysConfRec (pMsg, &pContext->SysConfigs, &u2Offset);

    /* System Statistics Table */

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->SysStats.u4SysDroppedPDUs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysStats.u4SysAreaMismatches);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysStats.u4SysMaxAAMismatches);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysStats.u4SysMAADropFromAreas);
    u2Offset = (UINT2)(u2Offset + 4);

    /* Sys L1 Stats */

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->SysL1Stats.u4SysCorrLSPs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->SysL1Stats.u4SysLSPDBOLs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL1Stats.u4SysAttemptToExcdMaxSeqNo);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL1Stats.u4SysSeqNumSkips);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL1Stats.u4SysOwnLSPPurges);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL1Stats.u4SysIDFieldLenMismatches);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->SysL1Stats.u4SysAuthFails);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL1Stats.u4SysAuthTypeFails);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL1Stats.u4SysPartChanges);
    u2Offset = (UINT2)(u2Offset + 4);

    /* Sys L2 Stats */

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->SysL2Stats.u4SysCorrLSPs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->SysL2Stats.u4SysLSPDBOLs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL2Stats.u4SysAttemptToExcdMaxSeqNo);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL2Stats.u4SysSeqNumSkips);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL2Stats.u4SysOwnLSPPurges);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL2Stats.u4SysIDFieldLenMismatches);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->SysL2Stats.u4SysAuthFails);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL2Stats.u4SysAuthTypeFails);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset,
                          pContext->SysL2Stats.u4SysPartChanges);
    u2Offset = (UINT2)(u2Offset + 4);

    /* Adjacency Direction Table */

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->AdjDirTable.u4NumEntries);
    u2Offset = (UINT2)(u2Offset + 4);

    pContext->AdjDirTable.pDirEntry = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    /* Adjacency Area Address Table */

    pContext->AreaAddrTable.pAARec = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pContext->AreaAddrTable.u2NumEntries);
    u2Offset = (UINT2)(u2Offset + 2);

    /* Reserved */

    u2Offset = (UINT2)(u2Offset + 2);

    /* Summary Address Table */

    pContext->SummAddrTable.pSAEntry = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pContext->SummAddrTable.u2NumEntries);
    u2Offset = (UINT2)(u2Offset + 2);

    /* Reserved */

    u2Offset = (UINT2)(u2Offset + 2);

    /* Manual Area Address Table */

    pContext->ManAddrTable.pMAARec = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pContext->ManAddrTable.u2NumEntries);
    u2Offset = (UINT2)(u2Offset + 2);

    /* Reserved */

    u2Offset = (UINT2)(u2Offset + 2);

    /* Protocol Support Table */

    for (u1PSCnt = 0; u1PSCnt < ISIS_MAX_PROTS_SUPP; u1PSCnt++)
    {
        ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                              pContext->aProtSupp[u1PSCnt].u1ProtSupp);
        u2Offset = (UINT2)(u2Offset + 1);

        ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                              pContext->aProtSupp[u1PSCnt].u1ExistState);
        u2Offset = (UINT2)(u2Offset + 1);

        /* Reserved */
        u2Offset = (UINT2)(u2Offset + 2);
    }

    /* Circuit Table */

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->CktTable.u4NumEntries);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->CktTable.u4NumActCkts);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->CktTable.u4NumL1Adjs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->CktTable.u4NumL2Adjs);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pContext->CktTable.u4NextCktIndex);
    u2Offset = (UINT2)(u2Offset + 4);

    pContext->CktTable.pCktRec = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    pContext->CktTable.pu1L1CktMask = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    pContext->CktTable.pu1L2CktMask = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    /* IP I/f Address Table */

    pContext->IPIfTable.pIPIfAddr = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pContext->IPIfTable.u2NumEntries);
    u2Offset = (UINT2)(u2Offset + 2);

    /* Reserved */

    u2Offset = (UINT2)(u2Offset + 2);

    /* IPRA Table */

    pContext->IPRAInfo.pIPRAEntry = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pContext->IPRAInfo.u2NumEntries);
    u2Offset = (UINT2)(u2Offset + 2);

    /* Reserved */

    u2Offset = (UINT2)(u2Offset + 2);

    /* Pointer to Next */

    pContext->pNext = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    /* BFD Support */
    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pContext->u1IsisBfdSupport);
    u2Offset = (UINT2)(u2Offset + 1);

    *pu2Offset = u2Offset;

}

/*******************************************************************************
 * Function    : IsisFltrGetSysConfRec ()
 * Description : This routine builds the SysConfigs record from the information
 *               retrieved from the received FLTR message
 * Input(s)    : pMsg        - Pointer to the message which includes the encoded
 *                             System Configs information
 *               pSysConfigs - Pointer to the System Configs Record which is to
 *                             be updated with the information retrieved from 
 *                             the FLTR message
 *               pu2Offset   - Offset from which the buffer needs to be read 
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetSysConfRec (tRmMsg * pMsg, tIsisSysConfigs * pSysConfigs,
                       UINT2 *pu2Offset)
{

    UINT2               u2Offset = *pu2Offset;

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysConfigs->u1SysType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysConfigs->u1SysMPS);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysConfigs->u1SysMaxAA);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysConfigs->u1SysMetricSupp);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pSysConfigs->u4SysOrigL1LSPBufSize);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pSysConfigs->u4SysOrigL2LSPBufSize);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysConfigs->bSysL2ToL1Leak);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysConfigs->u1SysL1MetricStyle);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysConfigs->u1SysL2MetricStyle);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_N_BYTE (pMsg, pSysConfigs->au1SysID, u2Offset,
                          ISIS_SYS_ID_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_SYS_ID_LEN);

#if ((ISIS_SYS_ID_LEN + 3) % 4)
    u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_SYS_ID_LEN + 3) % 4)));
#endif

    *pu2Offset = u2Offset;

}

/****************************************************************************
 * Function    : IsisFltrGetSysActualsRec ()
 * Description : This routine builds the SysActuals record from the information
 *               retrieved from the received FLTR message
 * Input(s)    : pMsg        - Pointer to the message which includes the encoded
 *                             System Actuals information
 *               pSysConfigs - Pointer to the System Actuals Record which is to
 *                             be updated with the information retrieved from 
 *                             the FLTR message
 *               pu2Offset    - Offset which is to be updated.              
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ***************************************************************************/

PUBLIC VOID
IsisFltrGetSysActualsRec (tRmMsg * pMsg, tIsisSysActuals * pSysActuals,
                          UINT2 *pu2Offset)
{
    UINT2               u2Offset = *pu2Offset;
    UINT1               u1PwdCnt = 0;

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysVer);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysMPS);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysMaxAA);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysAdminState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysIDLen);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysAuthSupp);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysAreaAuthType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysDomainAuthType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->bSysLSPIgnoreErr);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->bSysLogAdjChanges);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysSetOL);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysL1State);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysL2State);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysExistState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->bSysL2ToL1Leak);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSysActuals->u2MinSPFSchTime);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSysActuals->u2MaxSPFSchTime);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSysActuals->u2MinLSPMark);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSysActuals->u2MaxLSPMark);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSysActuals->u2SysMaxLSPGenInt);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSysActuals->u2SysMinL1LSPGenInt);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSysActuals->u2SysMinL2LSPGenInt);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_2_BYTE (pMsg, u2Offset, pSysActuals->u2SysWaitTime);
    u2Offset = (UINT2)(u2Offset + 2);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pSysActuals->u4SysOrigL1LSPBufSize);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pSysActuals->u4SysOrigL2LSPBufSize);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->bSysMaxAACheck);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysMetricSupp);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysL1MetricStyle);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysL2MetricStyle);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysL1SPFConsiders);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysL2SPFConsiders);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->u1SysTEEnabled);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_N_BYTE (pMsg, pSysActuals->au1SysID, u2Offset,
                          ISIS_SYS_ID_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_SYS_ID_LEN);

#if ((ISIS_SYS_ID_LEN + 3) % 4)
    u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_SYS_ID_LEN + 3) % 4)));
#endif

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pSysActuals->u4SysMaxAge);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pSysActuals->u4SysRxLSPBufSize);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->SysAreaTxPasswd.u1Len);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                          pSysActuals->SysAreaTxPasswd.u1ExistState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_N_BYTE (pMsg, pSysActuals->SysAreaTxPasswd.au1Password,
                          u2Offset, ISIS_MAX_PASSWORD_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
    u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)));
#endif

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pSysActuals->SysDomainTxPasswd.u1Len);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                          pSysActuals->SysDomainTxPasswd.u1ExistState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_N_BYTE (pMsg, pSysActuals->SysDomainTxPasswd.au1Password,
                          u2Offset, ISIS_MAX_PASSWORD_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
    u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)));
#endif

    for (u1PwdCnt = 0; u1PwdCnt < ISIS_MAX_RX_PASSWDS; u1PwdCnt++)
    {
        ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                              pSysActuals->aSysAreaRxPasswd[u1PwdCnt].u1Len);
        u2Offset = (UINT2)(u2Offset + 1);

        ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                              pSysActuals->aSysAreaRxPasswd[u1PwdCnt].
                              u1ExistState);
        u2Offset = (UINT2)(u2Offset + 1);

        ISIS_RED_READ_N_BYTE (pMsg,
                              pSysActuals->aSysAreaRxPasswd[u1PwdCnt].
                              au1Password, u2Offset, ISIS_MAX_PASSWORD_LEN);

        u2Offset = (UINT2)(u2Offset + ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
        u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)));
#endif
    }

    for (u1PwdCnt = 0; u1PwdCnt < ISIS_MAX_RX_PASSWDS; u1PwdCnt++)
    {
        ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                              pSysActuals->aSysDomainRxPasswd[u1PwdCnt].u1Len);
        u2Offset = (UINT2)(u2Offset + 1);

        ISIS_RED_READ_1_BYTE (pMsg, u2Offset,
                              pSysActuals->aSysDomainRxPasswd[u1PwdCnt].
                              u1ExistState);
        u2Offset = (UINT2)(u2Offset + 1);

        ISIS_RED_READ_N_BYTE (pMsg,
                              pSysActuals->aSysDomainRxPasswd[u1PwdCnt].
                              au1Password, u2Offset, ISIS_MAX_PASSWORD_LEN);
        u2Offset = (UINT2)(u2Offset + ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
        u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)));
#endif
    }
    *pu2Offset = u2Offset;

}

/*******************************************************************************
 * Function    : IsisFltrGetAdjAARec()
 * Description : This routine constructs the Adjacency Area Address record from
 *               the information retrieved from the received FLTR Message
 * Input(s)    : pMsg       - Pointer to the received FLTR message which
 *                            includes the Adjacency Area Address Information
 *                            present 
 *               pAdjAARec  - Pointer to the Adjacency Area Address Record which
 *                            is to be built from the information retrieved from
 *                            the received FLTR message 'pMsg'
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetAdjAARec (tRmMsg * pMsg, tIsisAdjAAEntry * pAARec, UINT2 *pu2Offset)
{
    UINT2               u2Offset = *pu2Offset;

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pAARec->u4AdjAreaAddrIdx);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_N_BYTE (pMsg, pAARec->ISAdjAreaAddress.au1AreaAddr, u2Offset,
                          ISIS_AREA_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_AREA_ADDR_LEN);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pAARec->ISAdjAreaAddress.u1Length);
    u2Offset = (UINT2)(u2Offset + 1);

#if ((ISIS_AREA_ADDR_LEN + 1) % 4)
    u2Offset = (UINT2)(u2Offset + (4 - ((ISIS_AREA_ADDR_LEN + 1) % 4)));
#endif

    pAARec->pNext = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    *pu2Offset = u2Offset;

}

/*******************************************************************************
 * Function    : IsisFltrGetIPRARec ()
 * Description : This routine constructs an IPRA record from the information
 *               retrieved from the received FLTR message
 * Input(s)    : pMsg  - Pointer to the FLTR message which includes the IPRA
 *                       information to be encoded
 *               pIPRA - Pointer to the IPRA Record which is to be built using
 *                       the information encoded in the received FLTR message
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltrGetIPRARec (tRmMsg * pMsg, tIsisIPRAEntry * pIPRARec, UINT2 *pu2Offset)
{
    UINT2               u2Offset = *pu2Offset;

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pIPRARec->u4IPRAIdx);
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPRARec->u1IPRAType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPRARec->u1IPRADestType);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPRARec->u1IPRAExistState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPRARec->u1IPRAAdminState);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPRARec->u1QFlag);
    u2Offset = (UINT2)(u2Offset + 1);

    ISIS_RED_READ_1_BYTE (pMsg, u2Offset, pIPRARec->u1PrefixLen);
    u2Offset = (UINT2)(u2Offset + 1);

    pIPRARec->pNext = NULL;
    u2Offset = (UINT2)(u2Offset + 4);

    ISIS_RED_READ_N_BYTE (pMsg, pIPRARec->Metric, u2Offset,
                          sizeof (tIsisMetric));
    u2Offset = (UINT2)(u2Offset + sizeof (tIsisMetric));

    ISIS_RED_READ_4_BYTE (pMsg, u2Offset, pIPRARec->u4FullMetric);
    u2Offset = (UINT2)(u2Offset + 4);
#if (ISIS_NUM_METRICS % 4)
    u2Offset = (UINT2)(u2Offset + (4 - (ISIS_NUM_METRICS % 4)));
#endif

    ISIS_RED_READ_N_BYTE (pMsg, pIPRARec->au1IPRADest, u2Offset,
                          ISIS_MAX_IP_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_MAX_IP_ADDR_LEN);

    ISIS_RED_READ_N_BYTE (pMsg, pIPRARec->au1IPRASNPA, u2Offset,
                          ISIS_SNPA_ADDR_LEN);
    u2Offset = (UINT2)(u2Offset + ISIS_SNPA_ADDR_LEN);
#if ((ISIS_MAX_IP_ADDR_LEN + ISIS_SNPA_ADDR_LEN) % 4)
    u2Offset = (UINT2)(u2Offset + ((4 - ((ISIS_MAX_IP_ADDR_LEN +
                                  ISIS_SNPA_ADDR_LEN) % 4))));
#endif
    *pu2Offset = u2Offset;
}

/****************************************************************************
 * Function Name  : IsisFltrBldSysActualsRec ()
 * Description    : The Fltr module calls this function to build
 *                  System Actuals  Record
 * Input(s)       : pRmMsg    - Pointer to the buffer to which the record is to
 *                              be copied
 *                  pContext  - Pointer to System Context
 *                  pu2Offset - Offset where the encoding starts
 * Output(s)      : pu2Offset - Offset from where the subsequent encoding 
 *                               starts i.e. current encoding ends
 * Globals        : Not Referred or Modified
 * Returns        : VOID
****************************************************************************/

PUBLIC VOID
IsisFltrBldSysActualsRec (tRmMsg * pRmMsg, tIsisSysContext * pContext,
                          UINT2 *pu2Offset)
{
    UINT1               u1IsMetric = 0;
    UINT1               u1PwdCnt = 0;
    UINT2               u2Offset = *pu2Offset;;
    /* NOTE:
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     *
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to
     * encode the Data in Network Order into the buffer. Otherwise we can
     * directly copy the contents as is
     */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysVer);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysMPS);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysMaxAA);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysAdminState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysIDLen);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysAuthSupp);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysAreaAuthType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysDomainAuthType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.bSysLSPIgnoreErr);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.bSysLogAdjChanges);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysSetOL);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysL1State);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysL2State);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysExistState);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.bSysL2ToL1Leak);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u2MinSPFSchTime);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u2MaxSPFSchTime);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u2MinLSPMark);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u2MaxLSPMark);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u2SysMaxLSPGenInt);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u2SysMinL1LSPGenInt);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u2SysMinL2LSPGenInt);

    ISIS_RED_PUT_2_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u2SysWaitTime);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u4SysOrigL1LSPBufSize);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u4SysOrigL2LSPBufSize);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.bSysMaxAACheck);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysMetricSupp);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysL1MetricStyle);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysL2MetricStyle);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysL1SPFConsiders);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u1SysL2SPFConsiders);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u1SysTEEnabled);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pContext->SysActuals.au1SysID, u2Offset,
                         ISIS_SYS_ID_LEN);

#if ((ISIS_SYS_ID_LEN + 3) % 4)
    for (u1IsMetric = 0; u1IsMetric < (4 - ((ISIS_SYS_ID_LEN + 3) % 4));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset, pContext->SysActuals.u4SysMaxAge);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.u4SysRxLSPBufSize);

    /* System Area Tx Password */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.SysAreaTxPasswd.u1Len);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.SysAreaTxPasswd.u1ExistState);

    ISIS_RED_PUT_N_BYTE (pRmMsg,
                         pContext->SysActuals.SysAreaTxPasswd.au1Password,
                         u2Offset, ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
    for (u1IsMetric = 0; u1IsMetric < (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif

    /* System Domain Tx Password */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.SysDomainTxPasswd.u1Len);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysActuals.SysDomainTxPasswd.u1ExistState);

    ISIS_RED_PUT_N_BYTE (pRmMsg,
                         pContext->SysActuals.SysDomainTxPasswd.au1Password,
                         u2Offset, ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
    for (u1IsMetric = 0; u1IsMetric < (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }

#endif

    /* System Area Rx Password */

    for (u1PwdCnt = 0; u1PwdCnt < ISIS_MAX_RX_PASSWDS; u1PwdCnt++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                             pContext->
                             SysActuals.aSysAreaRxPasswd[u1PwdCnt].u1Len);

        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                             pContext->SysActuals.aSysAreaRxPasswd[u1PwdCnt].
                             u1ExistState);

        ISIS_RED_PUT_N_BYTE (pRmMsg,
                             pContext->SysActuals.aSysAreaRxPasswd[u1PwdCnt].
                             au1Password, u2Offset, ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
        for (u1IsMetric = 0;
             u1IsMetric < (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)); u1IsMetric++)
        {
            ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
        }
#endif
    }

    /* System Domain Rx Password */

    for (u1PwdCnt = 0; u1PwdCnt < ISIS_MAX_RX_PASSWDS; u1PwdCnt++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                             pContext->SysActuals.aSysDomainRxPasswd[u1PwdCnt].
                             u1Len);

        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                             pContext->SysActuals.aSysDomainRxPasswd[u1PwdCnt].
                             u1ExistState);

        ISIS_RED_PUT_N_BYTE (pRmMsg,
                             pContext->SysActuals.aSysDomainRxPasswd[u1PwdCnt].
                             au1Password, u2Offset, ISIS_MAX_PASSWORD_LEN);

#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)
        for (u1IsMetric = 0;
             u1IsMetric < (4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4)); u1IsMetric++)
        {
            ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
        }
#endif
    }

    *pu2Offset = u2Offset;

}

/****************************************************************************
 * Function Name  : IsisFltrBldSysConfRec ()
 * Description    : The Fltr module calls this function to build
 *                  System Actuals  Record
 * Input(s)       : pRmMsg    - Pointer to the buffer to which the record is to
 *                              be copied
 *                  pContext  - Pointer to System Context
 *                  pu2Offset - Offset where the encoding starts
 * Output(s)      : pu2Offset - Offset from where the subsequent encoding 
 *                               starts i.e. current encoding ends
 * Globals        : Not Referred or Modified
 * Returns        : VOID
****************************************************************************/

PUBLIC VOID
IsisFltrBldSysConfRec (tRmMsg * pRmMsg, tIsisSysContext * pContext,
                       UINT2 *pu2Offset)
{
    UINT1               u1IsMetric = 0;
    UINT2               u2Offset = *pu2Offset;
    /* NOTE:
     *
     * We always fill the Number of Elements as '0' since the ISIS module
     * on the Standby node will take care of incrementing this number when
     * entries are added to the table
     *
     * Pointers are always passed as NULL. These pointers are updated when the
     * corresponding records are created on the standby node
     */

    /* If the peer node, where the backup information is held, happens to be a
     * different Endian compared to the current node, then we may have to
     * encode the Data in Network Order into the buffer. Otherwise we can
     * directly copy the contents as is
     */

    /* System Configs */

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysConfigs.u1SysType);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysConfigs.u1SysMPS);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysConfigs.u1SysMaxAA);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysConfigs.u1SysMetricSupp);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysConfigs.u4SysOrigL1LSPBufSize);

    ISIS_RED_PUT_4_BYTE (pRmMsg, u2Offset,
                         pContext->SysConfigs.u4SysOrigL2LSPBufSize);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, pContext->SysConfigs.bSysL2ToL1Leak);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysConfigs.u1SysL1MetricStyle);

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset,
                         pContext->SysConfigs.u1SysL2MetricStyle);

    ISIS_RED_PUT_N_BYTE (pRmMsg, pContext->SysConfigs.au1SysID, u2Offset,
                         ISIS_SYS_ID_LEN);

#if ((ISIS_SYS_ID_LEN + 3) % 4)
    for (u1IsMetric = 0; u1IsMetric < (4 - ((ISIS_SYS_ID_LEN + 3) % 4));
         u1IsMetric++)
    {
        ISIS_RED_PUT_1_BYTE (pRmMsg, u2Offset, 0);
    }
#endif
    *pu2Offset = u2Offset;
}
#endif
