/*****************************************************************************
 *
 *  Copyright (C) Future Software Limited,  2001
 *
 *  $Id: iscli.c,v 1.74.4.1 2018/05/22 10:50:17 siva Exp $
 *
 *  Description: This file contains the CLI module routines.
 *
 ****************************************************************************/

#ifndef __ISISCLI_C__
#define __ISISCLI_C__

#include "iscli.h"
#include "isincl.h"
#include "cli.h"
#include "isclipt.h"
#include "isextn.h"
#include "snmputil.h"
#include "fsisiscli.h"
#include "stdisicli.h"
#define ISIS_CLI_DEF_INST_IDX 0

extern UINT4        gu4IsIvrEnabled;

/*klocwork*/
#define ISIS_CLI_MAX_SYS_TYPE 6
#define ISIS_CLI_MAX_CKT_TYPE 6
#define ISIS_CLI_MAX_CKT_LEVEL 4
#define ISIS_CLI_MAX_ADJ_STATE 5
#define ISIS_CLI_MAX_CKT_STATE 2

/* Array Declarations used by protcli module. These objects cannot be
 * modified or read by any other modules */
PRIVATE CHAR       *api1MTSupport[3] = {
    "OTHER",
    "ENABLED",
    "DISABLED"
};

PRIVATE CHAR       *api1CktOperState[2] = {
    "DOWN",
    "UP"
};

PRIVATE UINT1       api1SysType[6][20] = {
    "OTHER",
    "LEVEL1",
    "LEVEL2",
    "LEVEL12",
    "UNKNOWN",
    "IS"
};

PRIVATE UINT1       api1CktType[6][20] = {
    "OTHER",
    "BCAST",
    "P2P",
    "ST_IN",
    "ST_OUT",
    "DA"
};

PRIVATE UINT1       api1CktLevel[4][20] = {
    "OTHER",
    "LEVEL1",
    "LEVEL2",
    "LEVEL12"
};

PRIVATE UINT1       api1AdjUsage[5][20] = {
    "OTHER",
    "LEVEL1",
    "LEVEL2",
    "LEVEL12",
    "UNDEFINED"
};

PRIVATE UINT1       api1AdjState[5][20] = {
    "UP",
    "INIT",
    "DOWN",
    "FAILED",
    "OTHER"
};

#ifdef BFD_WANTED
PRIVATE CHAR       *api1BfdSupport[3] = {
    "OTHER",
    "ENABLED",
    "DISABLED"
};

#endif

/***************************************************************************  
 * FUNCTION NAME : cli_process_isis_cmd
 * DESCRIPTION   : This function process the cli commands and calls the
 *                 protocol action routine. 
 * INPUT         : va_alist
 * OUTPUT        : NONE
 * RETURNS       : NONE
 ****************************************************************************/

INT4
cli_process_isis_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    INT4                i4RetValue = ISIS_FAILURE;
    va_list             ap;
    INT1                argno = 0;
    UINT1              *args[CLI_MAX_ARGS];

    INT4                i4IfIndex = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT4               u4InstIdx = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               au1ContextName[] = "default";
    UINT2               u2ShowCmdFlag = ISIS_FALSE;
    UINT1              *pu1IsisCxtName = NULL;
    UINT1               au1TaskName[] = "ISIS";
    UINT1              *pu1TaskName = au1TaskName;
#ifdef ROUTEMAP_WANTED
    UINT1               u1Distance = 0;
#endif
    INT4                i4default_metric = 0;
    INT4                i4delay_metric = 0;
    INT4                i4error_metric = 0;
    INT4                i4expense_metric = 0;
    INT4                i4ipraindex = 0;
    UINT4               u4Metric = 0;
    UINT4               u4Interval = 0;
    UINT4               u4Priority = 0;
    UINT4               u4InterfaceIndex = 0;
    UINT4               u4PrefixLen = 0;
    UINT1               au1Ipv6Nexthop[ISIS_MAX_IPV6_ADDR_LEN];
    UINT1               au1Ipv6Addr[ISIS_MAX_IPV6_ADDR_LEN];
    tIsisSysContext    *pContext = NULL;
    VOID               *pArgs = NULL;
    UINT4               u4MetricVal = 0;
    UINT4               u4full_metric = 0;
    UINT4               u4wide_metric = 0;

    CliRegisterLock (CliHandle, IsisLock, IsisUnlock);
    IsisLock ();

    if (IsisCliGetContext (CliHandle, u4Command, &u4InstIdx, &i4IfIndex,
                           &u2ShowCmdFlag) == CLI_FAILURE)
    {
        CLI_SET_CMD_STATUS (CLI_FAILURE);
        IsisUnlock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if (gu1IsisStatus != ISIS_IS_UP)
    {
        CLI_SET_CMD_STATUS (CLI_FAILURE);
        CliPrintf (CliHandle, "\r%% System NOT in ISIS_IS_UP Status\r\n");
        IsisUnlock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    /* Variable Argument extraction */
    va_start (ap, u4Command);

    /* second arguement is always interface name/index */
    u4InterfaceIndex = va_arg (ap, INT4);

    /* If u2ShowCmdFlag is set and for router enable/disable the third
     * argument is always context name */
    if ((u2ShowCmdFlag == ISIS_TRUE || u4Command == ISIS_CLI_CMD_CREATE_INST
         || u4Command == ISIS_CLI_CMD_DEST_INST)
        && (u4Command != ISIS_CLI_SHOW_DBG_INFO))
    {
        pu1IsisCxtName = va_arg (ap, UINT1 *);
        if (UtilIsisGetVcmSystemMode (ISIS_PROTOCOL_ID) == ISIS_SI_MODE)
        {
            pu1IsisCxtName = au1ContextName;
        }
    }

    /* Walk through the rest of the arguements and store in args array     */
    MEMSET (au1Ipv6Nexthop, 0x0, ISIS_MAX_IPV6_ADDR_LEN);
    MEMSET (au1Ipv6Addr, 0x0, ISIS_MAX_IPV6_ADDR_LEN);
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == 8)
            break;
    }
    va_end (ap);
    if (u2ShowCmdFlag != ISIS_TRUE)
    {
        if ((u4Command == ISIS_CLI_CMD_CREATE_INST) ||
            (u4Command == ISIS_CLI_CMD_DEST_INST))
        {
            /* Get the Context Id from the Context name */
            if (pu1IsisCxtName != NULL)
            {
                i4RetVal = IsisCliGetCxtIdFromCxtName (CliHandle,
                                                       pu1IsisCxtName,
                                                       &u4InstIdx);
                if (i4RetVal == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid isis Context Id\r\n");
                    CliUnRegisterLock (CliHandle);
                    IsisUnlock ();
                    return CLI_FAILURE;
                }

            }
            else
            {
                u4InstIdx = ISIS_DEFAULT_CXT_ID;
            }

        }
        if (((u4Command != ISIS_CLI_CMD_CREATE_INST) &&
             (u4Command != ISIS_CLI_CMD_DEST_INST)) &&
            (CLI_SUCCESS != is_debug_cmd (u4Command)) &&
            (UtilIsisSetContext (u4InstIdx) == ISIS_FAILURE))
        {
            CliPrintf (CliHandle, "\r%% Invalid isis Context Id\r\n");
            CliUnRegisterLock (CliHandle);
            IsisUnlock ();
            return CLI_FAILURE;
        }
        switch (u4Command)
        {
            case ISIS_CLI_CMD_CREATE_INST:
                i4RetVal = IsisCliAddContext (CliHandle, u4InstIdx);
                break;
            case ISIS_CLI_CMD_DEST_INST:
                i4RetVal = IsisCliDelContext (CliHandle, u4InstIdx);
                break;

            case ISIS_CLI_CMD_ADD_NET:
                i4RetVal =
                    IsisCliAddMAA (CliHandle, u4InstIdx, (UINT1 *) args[0],
                                   CLI_PTR_TO_I4 (args[1]));
                break;

            case ISIS_CLI_CMD_DEL_NET:
                i4RetVal =
                    IsisCliDelMAA (CliHandle, u4InstIdx, (UINT1 *) args[0],
                                   CLI_PTR_TO_I4 (args[1]));
                break;

            case ISIS_CLI_CMD_ENABLE_ROUTE_LEAK:
                i4RetVal = IsisCliSetRouteLeak (CliHandle, u4InstIdx);
                break;

            case ISIS_CLI_CMD_DISABLE_ROUTE_LEAK:

                i4RetVal = IsisCliResetRouteLeak (CliHandle, u4InstIdx);
                break;

            case ISIS_CLI_CMD_INST_TYPE:
                i4RetVal =
                    IsisCliSetSysType (CliHandle, u4InstIdx,
                                       CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_CMD_ADD_SUMM_ADDR:
                if (args[2] == NULL)
                {
                    u4Metric = (UINT4) ISIS_CLI_INVALID_METRIC;
                }
                else
                {
                    u4Metric = (*(UINT4 *) (VOID *) (args[2]));
                }
                i4RetVal =
                    IsisCliAddSa (CliHandle, u4InstIdx, args[0], args[1],
                                  u4Metric, CLI_PTR_TO_U4 (args[3]));

                break;

            case ISIS_CLI_CMD_ADD_SUMM_IPV6_ADDR:
                if (args[2] == NULL)
                {
                    u4Metric = ISIS_IPRA_DEF_METRIC_VALUE;
                }
                else
                {
                    u4Metric = (*(UINT4 *) (VOID *) (args[2]));
                }

                u4PrefixLen =
                    (args[1] ==
                     NULL) ? ISIS_CLI_INVALID_PREFIXLENGTH : CLI_ATOI (args[1]);
                i4RetVal =
                    IsisCliAddIPV6Sa (CliHandle, u4InstIdx, args[0],
                                      u4PrefixLen, u4Metric,
                                      CLI_PTR_TO_U4 (args[3]));

                break;

            case ISIS_CLI_CMD_DEL_SUMM_ADDR:
                i4RetVal =
                    IsisCliDelSa (CliHandle, u4InstIdx, args[0], args[1]);
                break;

            case ISIS_CLI_CMD_DEL_SUMM_IPV6_ADDR:

                u4PrefixLen =
                    (args[1] ==
                     NULL) ? ISIS_CLI_INVALID_PREFIXLENGTH : CLI_ATOI (args[1]);
                i4RetVal =
                    IsisCliDelIPV6Sa (CliHandle, u4InstIdx, args[0],
                                      u4PrefixLen);
                break;

            case ISIS_CLI_CMD_ADD_IPRA:
                i4RetVal =
                    IsisCliAddIpRa (CliHandle, u4InstIdx,
                                    *(UINT4 *) (VOID *) (args[0]), args[1],
                                    args[2], args[3], CLI_PTR_TO_U4 (args[4]));
                break;

            case ISIS_CLI_CMD_ADD_IPV6_IPRA:
                u4PrefixLen =
                    (args[2] ==
                     NULL) ? ISIS_CLI_INVALID_PREFIXLENGTH : (*(UINT4 *) (VOID
                                                                          *)
                                                              (args[2]));

                if (INET_ATON6 (args[1], &au1Ipv6Addr) == 0)
                {
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                if (INET_ATON6 (args[3], &au1Ipv6Nexthop) == 0)
                {
                    i4RetVal = CLI_FAILURE;
                    break;
                }

                i4RetVal =
                    IsisCliAddIpV6Ra (CliHandle, u4InstIdx,
                                      *(UINT4 *) (VOID *) (args[0]),
                                      au1Ipv6Addr, u4PrefixLen, au1Ipv6Nexthop,
                                      CLI_PTR_TO_U4 (args[4]));
                break;

            case ISIS_CLI_CMD_IPRA_METRIC:
                i4ipraindex = *(INT4 *) (VOID *) (args[0]);

                i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);

                if ((i4RetVal == ISIS_SUCCESS) && (pContext != NULL))
                {
                    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                    {
                        /* Copy the full metric */
                        u4full_metric = (args[1] == NULL) ? 0 :
                            (*(UINT4 *) (VOID *) (args[1]));
                    }
                    else
                    {
                        /* Copy the default metric */
                        i4default_metric = (args[1] ==
                                            NULL) ? ISIS_CLI_INVALID_METRIC :
                            (*(INT4 *) (VOID *) (args[1]));
                    }
                }

                i4RetVal =
                    IsisCliUpdIPRA (CliHandle, u4InstIdx, i4ipraindex,
                                    i4default_metric, u4full_metric);
                break;

            case ISIS_CLI_CMD_DEL_IPRA:
                i4RetVal =
                    IsisCliDelIpRa (CliHandle, u4InstIdx,
                                    *(UINT4 *) (VOID *) (args[0]));
                break;

            case ISIS_CLI_CMD_SET_OL_BIT:
                i4RetVal = IsisCliSetDBOL (CliHandle, u4InstIdx);
                break;

            case ISIS_CLI_CMD_RESET_OL_BIT:
                i4RetVal = IsisCliResetDBOL (CliHandle, u4InstIdx);
                break;

            case ISIS_CLI_CMD_SET_AREA_PASSWD:
                i4RetVal = IsisCliSetAreaPasswd (CliHandle, u4InstIdx, args[0]);
                break;

            case ISIS_CLI_CMD_RESET_AREA_PASSWD:
                i4RetVal =
                    IsisCliResetAreaPasswd (CliHandle, u4InstIdx, args[0]);
                break;

            case ISIS_CLI_CMD_SET_DOMAIN_PASSWD:
                i4RetVal =
                    IsisCliSetDomainPasswd (CliHandle, u4InstIdx, args[0]);
                break;

            case ISIS_CLI_CMD_RESET_DOMAIN_PASSWD:
                i4RetVal =
                    IsisCliResetDomainPasswd (CliHandle, u4InstIdx, args[0]);
                break;
                /*ISIs GR CLI Routines */
            case ISIS_CLI_CMD_GR_SUPPORT:
                i4RetVal = IsisCliSetGRRestartSupport (CliHandle, u4InstIdx,
                                                       CLI_PTR_TO_I4 (args[0]));
                break;

            case ISIS_CLI_CMD_T1_INTERVAL:

                pArgs = args[0];
                i4RetVal = IsisCliSetGRT1Interval (CliHandle, u4InstIdx,
                                                   *(INT4 *) pArgs);
                break;

            case ISIS_CLI_CMD_NO_T1_INTERVAL:
                i4RetVal = IsisCliSetGRT1Interval (CliHandle, u4InstIdx,
                                                   ISIS_GR_DEF_T1_INTERVAL);
                break;

            case ISIS_CLI_CMD_T1_RETRY:

                pArgs = args[0];
                i4RetVal = IsisCliSetGRT1RetryCount (CliHandle, u4InstIdx,
                                                     *(INT4 *) pArgs);
                break;

            case ISIS_CLI_CMD_NO_T1_RETRY:
                i4RetVal = IsisCliSetGRT1RetryCount (CliHandle, u4InstIdx,
                                                     ISIS_GR_DEF_ACK_RETRY_COUNT);
                break;

            case ISIS_CLI_CMD_T2_LEVEL1:

                pArgs = args[0];
                i4RetVal = IsisCliSetGRT2Interval (CliHandle, u4InstIdx,
                                                   *(INT4 *) pArgs,
                                                   ISIS_LEVEL1);
                break;

            case ISIS_CLI_CMD_T2_LEVEL2:

                pArgs = args[0];
                i4RetVal = IsisCliSetGRT2Interval (CliHandle, u4InstIdx,
                                                   *(INT4 *) pArgs,
                                                   ISIS_LEVEL2);
                break;

            case ISIS_CLI_CMD_T2_LEVEL12:

                pArgs = args[0];
                i4RetVal = IsisCliSetGRT2Interval (CliHandle, u4InstIdx,
                                                   *(INT4 *) pArgs,
                                                   ISIS_LEVEL12);
                break;

            case ISIS_CLI_CMD_NO_T2_LEVEL1:

                i4RetVal = IsisCliSetGRT2Interval (CliHandle, u4InstIdx,
                                                   ISIS_GR_DEF_T2_INTERVAL,
                                                   ISIS_LEVEL1);
                break;

            case ISIS_CLI_CMD_NO_T2_LEVEL2:

                i4RetVal = IsisCliSetGRT2Interval (CliHandle, u4InstIdx,
                                                   ISIS_GR_DEF_T2_INTERVAL,
                                                   ISIS_LEVEL2);
                break;

            case ISIS_CLI_CMD_NO_T2_LEVEL12:

                i4RetVal = IsisCliSetGRT2Interval (CliHandle, u4InstIdx,
                                                   ISIS_GR_DEF_T2_INTERVAL,
                                                   ISIS_LEVEL12);
                break;

            case ISIS_CLI_CMD_T3_MANUAL:

                pArgs = args[0];
                i4RetVal = IsisCliSetGRT3Interval (CliHandle, u4InstIdx,
                                                   *(INT4 *) pArgs);
                break;

            case ISIS_CLI_CMD_NO_T3_MANUAL:
                i4RetVal = IsisCliSetGRT3Interval (CliHandle, u4InstIdx,
                                                   ISIS_GR_DEF_T3_INTERVAL);
                break;

            case ISIS_CLI_CMD_RESTART_REASON:
                i4RetVal = IsisCliSetGRRestartReason (CliHandle, u4InstIdx,
                                                      CLI_PTR_TO_I4 (args[0]));
                break;

            case ISIS_CLI_CMD_HELPER_SUPPORT:
                i4RetVal = IsisCliSetGRHelperSupport (CliHandle, u4InstIdx,
                                                      CLI_PTR_TO_I4 (args[0]));
                break;

            case ISIS_CLI_CMD_NO_HELPER_SUPPORT:
                i4RetVal = IsisCliSetGRHelperSupport (CliHandle, u4InstIdx,
                                                      CLI_PTR_TO_I4 (args[0]));
                break;

            case ISIS_CLI_CMD_HELPER_GRACE_LIMIT:

                pArgs = args[0];
                i4RetVal = IsisCliSetGRHelperMaxGraceTime (CliHandle, u4InstIdx,
                                                           *(INT4 *) pArgs);
                break;

            case ISIS_CLI_CMD_NO_HELPER_GRACE_LIMIT:
                i4RetVal = IsisCliSetGRHelperMaxGraceTime (CliHandle, u4InstIdx,
                                                           ISIS_GR_DEF_HELPER_TIME);
                break;

            case ISIS_CLI_CMD_GR_NO_SUPPORT:
                i4RetVal = IsisCliSetGRRestartSupport (CliHandle, u4InstIdx,
                                                       CLI_PTR_TO_I4 (args[0]));
                break;

            case ISIS_CLI_CMD_DEBUG_ADJN:
                IsisCliDebugAdjn (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_DECN:
                IsisCliDebugDecn (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_UPDT:
                IsisCliDebugUpdt (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_INTERFACE:
                IsisCliDebugInterface (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_CONTROL:
                IsisCliDebugControl (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_TIMER:
                IsisCliDebugTimer (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_FAULT:
                IsisCliDebugFault (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_RTMI:
                IsisCliDebugRtmi (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_DLLI:
                IsisCliDebugDlli (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_TRFR:
                IsisCliDebugTrfr (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_SNMP:
                IsisCliDebugSnmp (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_UTIL:
                IsisCliDebugUtil (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_CRITICAL:
                IsisCliDebugCritical (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_ENTRYEXIT:
                IsisCliDebugEntryExit (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_DUMP:
                IsisCliDebugPacketDump (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_DETAIL:
                IsisCliDebugDetail (CliHandle);
                break;

            case ISIS_CLI_CMD_DEBUG_ALL:
                IsisCliDebugAll (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_ADJN:
                IsisCliNoDebugAdjn (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_DECN:
                IsisCliNoDebugDecn (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_UPDT:
                IsisCliNoDebugUpdt (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_INTERFACE:
                IsisCliNoDebugInterface (CliHandle);
                break;
            case ISIS_CLI_CMD_NO_DEBUG_CONTROL:
                IsisCliNoDebugControl (CliHandle);
                break;
            case ISIS_CLI_CMD_NO_DEBUG_TIMER:
                IsisCliNoDebugTimer (CliHandle);
                break;
            case ISIS_CLI_CMD_NO_DEBUG_FAULT:
                IsisCliNoDebugFault (CliHandle);
                break;
            case ISIS_CLI_CMD_NO_DEBUG_RTMI:
                IsisCliNoDebugRtmi (CliHandle);
                break;
            case ISIS_CLI_CMD_NO_DEBUG_DLLI:
                IsisCliNoDebugDlli (CliHandle);
                break;
            case ISIS_CLI_CMD_NO_DEBUG_TRFR:
                IsisCliNoDebugTrfr (CliHandle);
                break;
            case ISIS_CLI_CMD_NO_DEBUG_SNMP:
                IsisCliNoDebugSnmp (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_UTIL:
                IsisCliNoDebugUtil (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_CRITICAL:
                IsisCliNoDebugCritical (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_ENTRYEXIT:
                IsisCliNoDebugEntryExit (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_DUMP:
                IsisCliNoDebugPacketDump (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_DETAIL:
                IsisCliNoDebugDetail (CliHandle);
                break;

            case ISIS_CLI_CMD_NO_DEBUG_ALL:
                IsisCliNoDebugAll (CliHandle);
                break;

            case ISIS_CLI_CMD_CREATE_CKT:
                i4RetVal =
                    IsisCliAddCkt (CliHandle, u4InstIdx, i4IfIndex,
                                   (UINT1) CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_CMD_DELETE_CKT:
                IsisCliDelCkt (CliHandle, u4InstIdx, i4IfIndex);
                break;

            case ISIS_CLI_CMD_SET_CKT_TYPE:
                i4RetVal =
                    IsisCliSetCktType (CliHandle, i4IfIndex,
                                       (UINT1) CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_CMD_PASSIVE_INTERFACE:
                i4RetVal = IsisCliPassiveInterface (CliHandle, u4InterfaceIndex,
                                                    u4InstIdx,
                                                    CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_CMD_PASSIVE_INTERFACE_DEFAULT:
                i4RetVal = IsisCliPassiveInterfaceDefault (CliHandle, u4InstIdx,
                                                           CLI_PTR_TO_U4 (args
                                                                          [0]));
                break;

            case ISIS_CLI_CMD_SET_METRIC_STYLE:
                i4RetVal = IsisCliSetMetricStyle (CliHandle, u4InstIdx,
                                                  CLI_PTR_TO_I4 (args[0]));

                break;

            case ISIS_CLI_CMD_CKT_METRIC:

                i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);
                u4wide_metric = ISIS_CLI_INVALID_METRIC;
                i4default_metric = ISIS_CLI_INVALID_METRIC;
                if ((i4RetVal == ISIS_SUCCESS) && (pContext != NULL))
                {
                    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                    {
                        /* Copy the wide metric */
                        u4wide_metric =
                            (args[0] ==
                             NULL) ? (UINT4) ISIS_CLI_INVALID_METRIC : (*(UINT4
                                                                          *)
                                                                        (VOID
                                                                         *)
                                                                        (args
                                                                         [0]));
                    }
                    else
                    {
                        /* Copy the default metric */
                        i4default_metric = (args[0] ==
                                            NULL) ? ISIS_CLI_INVALID_METRIC :
                            (*(INT4 *) (VOID *) (args[0]));
                    }
                }
                /* Copy the delay metric */
                i4delay_metric =
                    (args[1] == NULL) ? ISIS_CLI_INVALID_METRIC : *(args[1]);
                /* Copy the error metric */
                i4error_metric =
                    (args[2] == NULL) ? ISIS_CLI_INVALID_METRIC : *(args[2]);
                /* Copy the expense metric */
                i4expense_metric =
                    (args[3] == NULL) ? ISIS_CLI_INVALID_METRIC : *(args[3]);

                i4RetVal =
                    IsisCliSetCktMetric (CliHandle, i4IfIndex, i4default_metric,
                                         i4delay_metric, i4error_metric,
                                         i4expense_metric, u4wide_metric,
                                         (UINT1) CLI_PTR_TO_U4 (args[4]));
                break;

            case ISIS_CLI_CMD_NO_CKT_METRIC:
                i4RetVal =
                    IsisCliResetCktMetric (CliHandle, i4IfIndex,
                                           (UINT1) CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_CMD_HELLO_INT:

                if (args[0] != NULL)
                {
                    pArgs = args[0];

                    i4RetVal =
                        IsisCliSetCktHelloInt (CliHandle, i4IfIndex,
                                               *(UINT4 *) pArgs,
                                               (UINT1) CLI_PTR_TO_U4 (args[1]));
                }
                else
                {
                    u4Interval = (UINT4) ISIS_CLI_INVALID_INTERVAL;
                    i4RetVal =
                        IsisCliSetCktHelloInt (CliHandle, i4IfIndex, u4Interval,
                                               (UINT1) CLI_PTR_TO_U4 (args[1]));
                }

                break;

            case ISIS_CLI_CMD_HELLO_MULTI:

                if (args[0] != NULL)
                {
                    pArgs = args[0];

                    i4RetVal =
                        IsisCliSetCktHelloMulti (CliHandle, i4IfIndex,
                                                 *(UINT4 *) pArgs,
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (args[1]));
                }
                else
                {
                    u4Interval = (UINT4) ISIS_CLI_INVALID_INTERVAL;
                    i4RetVal =
                        IsisCliSetCktHelloMulti (CliHandle, i4IfIndex,
                                                 u4Interval,
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (args[1]));
                }

                break;

            case ISIS_CLI_CMD_CKT_ADD_PASSWD:
                i4RetVal =
                    IsisCliSetCktAddPasswd (CliHandle, i4IfIndex, args[0],
                                            (UINT1) CLI_PTR_TO_U4 (args[1]));
                break;

            case ISIS_CLI_CMD_CKT_DEL_PASSWD:
                i4RetVal =
                    IsisCliSetCktDelPasswd (CliHandle, i4IfIndex, args[0],
                                            (UINT1) CLI_PTR_TO_U4 (args[1]));
                break;

            case ISIS_CLI_CMD_RETR_INT:
                if (args[0] != NULL)
                {
                    pArgs = args[0];
                    i4RetVal =
                        IsisCliSetCktRetrInt (CliHandle, i4IfIndex,
                                              *(UINT4 *) pArgs,
                                              (UINT1) CLI_PTR_TO_U4 (args[1]));
                }
                else
                {
                    u4Interval = (UINT4) ISIS_CLI_INVALID_INTERVAL;
                    i4RetVal =
                        IsisCliSetCktRetrInt (CliHandle, i4IfIndex,
                                              u4Interval,
                                              (UINT1) CLI_PTR_TO_U4 (args[1]));
                }
                break;

            case ISIS_CLI_CMD_LSP_INT:

                if (args[0] != NULL)
                {
                    pArgs = args[0];
                    i4RetVal =
                        IsisCliSetCktLSPInt (CliHandle, i4IfIndex,
                                             *(UINT4 *) pArgs,
                                             (UINT1) CLI_PTR_TO_U4 (args[1]));
                }
                else
                {
                    u4Interval = (UINT4) ISIS_LSP_THROTTLE;
                    i4RetVal =
                        IsisCliSetCktLSPInt (CliHandle, i4IfIndex,
                                             u4Interval,
                                             (UINT1) CLI_PTR_TO_U4 (args[1]));
                }
                break;

            case ISIS_CLI_CMD_CKT_PRIO:
                u4Priority = (args[0] ==
                              NULL) ? ISIS_IS_PRIORITY : CLI_ATOI (args[0]);

                i4RetVal =
                    IsisCliSetCktPriority (CliHandle, i4IfIndex, u4Priority,
                                           (UINT1) CLI_PTR_TO_U4 (args[1]));

                break;

            case ISIS_CLI_CMD_CKT_CSNP_INT:
                if (args[0] != NULL)
                {
                    pArgs = args[0];

                    i4RetVal =
                        IsisCliSetCktCSNPInt (CliHandle, i4IfIndex,
                                              *(UINT4 *) pArgs,
                                              (UINT1) CLI_PTR_TO_U4 (args[1]));
                }
                else
                {
                    u4Interval = (UINT4) ISIS_CLI_INVALID_INTERVAL;
                    i4RetVal =
                        IsisCliSetCktCSNPInt (CliHandle, i4IfIndex,
                                              u4Interval,
                                              (UINT1) CLI_PTR_TO_U4 (args[1]));
                }

                break;
            case ISIS_CLI_REDISTRIBUTE:
                /* args[0] -> contains the Protocol Value */
                /* args[1] -> ImportType  */
                /* args[2] -> route-map name */
                /* args[3] -> metric value */
                if (args[3] != NULL)
                {
                    u4MetricVal = *(UINT4 *) (VOID *) (args[3]);
                }
                else
                {
                    u4MetricVal = ISIS_CLI_INVALID_METRIC;
                }
                i4RetVal = IsisConfigRedistribute (CliHandle,
                                                   (INT4)
                                                   CLI_PTR_TO_U4 (args[0]),
                                                   ISIS_CLI_VAL_SET,
                                                   (UINT1 *) args[2],
                                                   u4MetricVal,
                                                   (INT4)
                                                   CLI_PTR_TO_U4 (args[1]),
                                                   (INT4) u4InstIdx);
                break;
            case ISIS_CLI_NO_REDISTRIBUTE:
                /* args[1] -> contains the Protocol Value */
                /* args[3] -> route-map name */
                /* args[4] -> metric value */
                if (args[3] != NULL)
                {
                    u4MetricVal = *(UINT4 *) (VOID *) (args[3]);
                }
                else
                {
                    u4MetricVal = ISIS_CLI_INVALID_METRIC;
                }
                i4RetVal = IsisConfigRedistribute (CliHandle,
                                                   (INT4)
                                                   CLI_PTR_TO_U4 (args[0]),
                                                   ISIS_CLI_VAL_NO,
                                                   (UINT1 *) args[2],
                                                   u4MetricVal,
                                                   (INT4)
                                                   CLI_PTR_TO_U4 (args[1]),
                                                   (INT4) u4InstIdx);
                break;

            case ISIS_CLI_CMD_MULTI_TOPOLOGY:
                i4RetVal = IsisCliSetMultiTopology (CliHandle, u4InstIdx,
                                                    (UINT1)
                                                    CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_CMD_DYN_HOSTNAME:
                i4RetVal = IsisCliSetDynHostname (CliHandle, u4InstIdx,
                                                  (UINT1)
                                                  CLI_PTR_TO_U4 (args[0]));
                break;

#ifdef BFD_WANTED
            case ISIS_CLI_CMD_BFD:
                i4RetVal = IsisCliSetBfdSupport ((INT4) u4InstIdx,
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_CMD_BFD_ALL_IF:
                i4RetVal = IsisCliSetBfdAllIfStatus ((INT4) u4InstIdx,
                                                     (UINT1)
                                                     CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_CMD_BFD_IF:
                i4RetVal = IsisCliSetBfdIfStatus (i4IfIndex,
                                                  (UINT1)
                                                  CLI_PTR_TO_U4 (args[0]));
                break;
#endif

#ifdef ROUTEMAP_WANTED
            case ISIS_CLI_CMD_DISTRIBUTE_LIST:
                /* args[0] -  Route map name */
                /* args[1] -  Enable/Disable. */

                i4RetVal = IsisCliSetDistribute (CliHandle, u4InstIdx, args[0],
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (args[1]));
                break;

            case ISIS_CLI_CMD_ROUTE_DISTANCE:
                /* args[0] - distance value (1-255)
                 * args[1] - route map name (optional) */
                pArgs = args[0];

                u1Distance = (UINT1) (*(UINT4 *) pArgs);

                i4RetVal =
                    IsisSetRouteDistance (CliHandle, u4InstIdx,
                                          u1Distance, args[1]);
                break;

            case ISIS_CLI_CMD_NO_ROUTE_DISTANCE:
                /* args[0] - route map name (optional) */
                i4RetVal =
                    IsisSetNoRouteDistance (CliHandle, u4InstIdx, args[0]);
                break;

            case ISIS_CLI_CMD_SET_AUTH_MODE:
                i4RetVal =
                    IsisCliSetAuthMode (CliHandle, u4InstIdx,
                                        CLI_PTR_TO_U4 (args[0]),
                                        (UINT1) CLI_PTR_TO_U4 (args[1]));
                break;
            case ISIS_CLI_CMD_DEL_AUTH_MODE:
                i4RetVal = IsisCliDelAuthMode (CliHandle, u4InstIdx,
                                               (UINT1) CLI_PTR_TO_U4 (args[0]));
                break;
            case ISIS_CLI_CMD_ISIS_IF_AUTH_MODE:
                i4RetVal =
                    IsisCliSetIfAuthMode (CliHandle, (UINT4) i4IfIndex,
                                          CLI_PTR_TO_U4 (args[0]),
                                          (UINT1) CLI_PTR_TO_U4 (args[1]));
                break;
            case ISIS_CLI_CMD_DEL_ISIS_IF_AUTH_MODE:
                i4RetVal = IsisCliDelIfAuthMode (CliHandle, (UINT4) i4IfIndex,
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (args[0]));

#endif /* ROUTEMAP_WANTED */
        }
    }
    else
    {
        if (pu1IsisCxtName == NULL)
        {
            pu1IsisCxtName = au1ContextName;
        }
        switch (u4Command)
        {
            case ISIS_CLI_SHOW_INSTS:
                i4RetVal = IsisCliShowInsts (CliHandle);
                break;

            case ISIS_CLI_SHOW_INTFS:
                i4RetVal = IsisCliShowCkts (CliHandle);
                break;

            case ISIS_CLI_SHOW_DBG_INFO:
                IsisCliShowDbgInfo (CliHandle);
                break;

            case ISIS_CLI_SHOW_ADJS:

                if (pu1IsisCxtName != NULL)
                {
                    i4RetVal = IsisCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1IsisCxtName,
                                                           &u4InstIdx);
                }
                if (i4RetVal == CLI_SUCCESS)
                {
                    i4RetVal =
                        IsisCliShowAdjs (CliHandle, u4InterfaceIndex,
                                         u4InstIdx);
                }

                break;

            case ISIS_CLI_SHOW_ROUTES:
                /*Get CxtId from Cxtname */
                if (pu1IsisCxtName != NULL)
                {
                    i4RetVal = IsisCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1IsisCxtName,
                                                           &u4InstIdx);
                }
                if (i4RetVal == CLI_SUCCESS)
                {
                    i4RetVal =
                        IsisCliShowRoutes (CliHandle,
                                           (UINT1) CLI_PTR_TO_U4 (args[0]),
                                           u4InstIdx);

                }
                break;

            case ISIS_CLI_SHOW_IPV6_ROUTES:
                /*Get CxtId from Cxtname */
                if (pu1IsisCxtName != NULL)
                {
                    i4RetVal = IsisCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1IsisCxtName,
                                                           &u4InstIdx);
                }
                if (i4RetVal == CLI_SUCCESS)
                {
                    i4RetVal = IsisCliShowIPV6Routes (CliHandle,
                                                      (UINT1)
                                                      CLI_PTR_TO_U4 (args[0]),
                                                      u4InstIdx);
                }
                break;
            case ISIS_CLI_SHOW_LSPDB:
                /*Get CxtId from Cxtname */
                if (pu1IsisCxtName != NULL)
                {
                    i4RetVal = IsisCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1IsisCxtName,
                                                           &u4InstIdx);
                }
                if (i4RetVal == CLI_SUCCESS)
                {
                    i4RetVal =
                        IsisCliShowLSPDB (CliHandle, u4InstIdx,
                                          (UINT1) CLI_PTR_TO_U4 (args[0]),
                                          CLI_PTR_TO_U4 (args[1]), args[2],
                                          CLI_PTR_TO_I4 (args[3]));

                }
                break;
#ifdef RM_WANTED
            case ISIS_SYNC_STATUS:

                i4RetVal = IsisCliShowIsisSyncStatus (CliHandle);
                break;

            case ISIS_CLI_SHOW_ACTUAL_DB:

                if (pu1IsisCxtName != NULL)
                {
                    i4RetVal = IsisCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1IsisCxtName,
                                                           &u4InstIdx);
                }
                if (i4RetVal == CLI_SUCCESS)
                {
                    i4RetVal = IsisCliShowActualDB (CliHandle, u4InstIdx);
                }
                break;

            case ISIS_CLI_SHOW_CONF_DB:

                if (pu1IsisCxtName != NULL)
                {
                    i4RetVal = IsisCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1IsisCxtName,
                                                           &u4InstIdx);
                }
                if (i4RetVal == CLI_SUCCESS)
                {
                    i4RetVal = IsisCliShowConfDB (CliHandle, u4InstIdx);
                }
                break;
#endif

            case ISIS_CLI_SHOW_IF_INFO:
                i4RetVal =
                    IsisCliShowCktInfo (CliHandle, u4InterfaceIndex,
                                        (UINT1) CLI_PTR_TO_U4 (args[0]));
                break;

            case ISIS_CLI_SHOW_PKT_STATS:
                i4RetVal = IsisCliShowPktStats (CliHandle, u4InterfaceIndex);
                break;

            case ISIS_CLI_SHOW_NSF:
                u4InstIdx = ISIS_CLI_DEF_INST_IDX;
                i4RetVal = IsisCliShowRestartStatus (CliHandle, u4InstIdx);
                break;

            case ISIS_CLI_SHOW_HOSTNAME:
                if (pu1IsisCxtName != NULL)
                {
                    i4RetVal = IsisCliGetCxtIdFromCxtName (CliHandle,
                                                           pu1IsisCxtName,
                                                           &u4InstIdx);
                }
                if (i4RetVal == CLI_SUCCESS)
                {
                    i4RetVal = IsisCliShowHostNmeTable (CliHandle, u4InstIdx);
                }
                break;

#ifdef ROUTEMAP_WANTED
            case ISIS_CLI_CMD_DISTRIBUTE_LIST:
                /* args[0] -  Route map name */
                /* args[1] -  Enable/Disable. */

                i4RetVal = IsisCliSetDistribute (CliHandle, u4InstIdx, args[0],
                                                 (UINT1)
                                                 CLI_PTR_TO_U4 (args[1]));
                break;
            case ISIS_CLI_CMD_ROUTE_DISTANCE:
            {
                /* args[0] - distance value (1-255)
                 * args[1] - route map name (optional) */

                pArgs = args[0];

                u1Distance = (UINT1) (*(UINT4 *) pArgs);

                i4RetVal =
                    IsisSetRouteDistance (CliHandle, u4InstIdx,
                                          u1Distance, args[1]);
                break;
            }
            case ISIS_CLI_CMD_NO_ROUTE_DISTANCE:
            {
                /* args[0] - route map name (optional)     */
                i4RetVal =
                    IsisSetNoRouteDistance (CliHandle, u4InstIdx, args[0]);
                break;
            }
#endif /* ROUTEMAP_WANTED */
        }
    }
    /* Display Error Meassge */
    if ((i4RetVal == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < CLI_ISIS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", IsisCliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetVal);
    i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    /*Saving all cli commands for Graceful restart */
    if ((pContext != NULL) && (i4RetVal == CLI_SUCCESS) &&
        (pContext->u1IsisGRRestartExitReason != ISIS_GR_RESTART_INPROGRESS) &&
        (pContext->u1IsisGRRestartExitReason != ISIS_GR_RESTART_TIMEDOUT))
    {
        IssCsrSaveCli (pu1TaskName);
    }

    CliUnRegisterLock (CliHandle);
    IsisUnlock ();
    UNUSED_PARAM (i4RetValue);

    return i4RetVal;

}

/*****************************************************************************
 * Function Name :  IsisCliAddContext                                      
 * Description   :  This function creates the context with the given values.   
 *                  If the context already exists then update the values. 
 * Input (s)     :  CliHandle      - CliContext
 *                     u4Inst           - Instance Id    
 * Output (s)    :   -                
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.                                                  
 ******************************************************************************/

INT4
IsisCliAddContext (tCliHandle CliHandle, UINT4 u4Inst)
{
    INT4                i4IsisExistState;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrorCode = 0;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_MODE_ROUTER_ISIS, u4Inst);

    if (nmhValidateIndexInstanceIsisSysTable (u4Inst) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    if (nmhGetIsisSysExistState ((INT4) u4Inst, &i4IsisExistState) !=
        SNMP_SUCCESS)
    {
        /* no Context exists with the given indices so add it   */

        if (nmhTestv2IsisSysExistState (&u4ErrorCode, (INT4) u4Inst, ISIS_CR_WT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysExistState ((INT4) u4Inst, ISIS_CR_WT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysType ((INT4) u4Inst, ISIS_LEVEL12) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetIsisSysAdminState ((INT4) u4Inst, ISIS_STATE_ON) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CLI_SET_ERR (CLI_ISIS_ROUTER_MODE_ERR);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name : IsisCliDelContext                                       
 * Description   : This function deletes the particulat context           
 * Input (s)     : CliHandle      - CliContext       
 *                   u4Inst       - Instance Id                                    
 * Output (s)    : -                
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 ******************************************************************************/
INT4
IsisCliDelContext (tCliHandle CliHandle, UINT4 u4Inst)
{
    INT4                i4IsisExistState = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetIsisSysExistState ((INT4) u4Inst, &i4IsisExistState) ==
        SNMP_SUCCESS)
    {
        if (nmhTestv2IsisSysExistState
            (&u4ErrorCode, (INT4) u4Inst, ISIS_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysExistState ((INT4) u4Inst, ISIS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetIsisSysAdminState ((INT4) u4Inst, ISIS_STATE_OFF) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
    }

    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name : IsisCliSetSysType                                       
 * Description   : This function deletes the particulat context           
 * Input (s)     : CliHandle      - CliContext      
 *                 u4Inst            - Instance Id
 *                 u4SysType        - System Level                                  
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 ******************************************************************************/
INT4
IsisCliSetSysType (tCliHandle CliHandle, UINT4 u4Inst, UINT4 u4SysType)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetValue = 0;
    tIsisSysContext    *pContext = NULL;

    i4RetValue = IsisCtrlGetSysContext (u4Inst, &pContext);
    UNUSED_PARAM (i4RetValue);

    if (pContext == NULL)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    if (pContext->SysConfigs.u1SysType == u4SysType)
    {
        return CLI_SUCCESS;
    }

    if (nmhTestv2IsisSysType (&u4ErrCode, (INT4) u4Inst, u4SysType) ==
        SNMP_SUCCESS)
    {
        if (nmhSetIsisSysType ((INT4) u4Inst, u4SysType) == SNMP_SUCCESS)
        {
            return CLI_SUCCESS;
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return CLI_FAILURE;

}

/*******************************************************************************
 * Function Name :  IsisCliAddCkt                                          
 * Description   :  This function Adds the Ckt Record to the context       
 *                  or updates the entry with new values if it already     
 *                  exists                                                 
 * Input (s)     : CliHandle     - CliContext      
 *                  u4Inst       - Instance Id
 *                  i4IfIndex    - Interface Index                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliAddCkt (tCliHandle CliHandle, UINT4 u4InstIdx, INT4 i4IfIndex,
               UINT1 u1CircType)
{
    INT4                i4RetVal = 0;
    UINT1               u1CktLocalId = 0;
    INT4                i4CktLevel = 0;
    INT4                i4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;
    UINT4               u4Error = 0;

    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if (CfaIfIsBridgedInterface (i4IfIndex) == CFA_TRUE)
        {
            CLI_SET_ERR (CLI_ISIS_BRIDGED_INTER);
            return CLI_FAILURE;
        }
    }

    if (u1CircType == ISIS_P2P_CKT)
    {
        u1CktLocalId = 0;
    }
    else
    {
        u1CktLocalId = (UINT1) ((i4IfIndex % ISIS_LL_MAX_CIRCUIT_LOCAL_ID) + 1);
    }

    u1CktLocalId = (UINT1) ((i4IfIndex % ISIS_LL_MAX_CIRCUIT_LOCAL_ID) + 1);

    nmhGetIsisSysType ((INT4) u4InstIdx, &i4CktLevel);

    i4RetVal = IsisAdjGetCktRecWithIfIdx ((UINT4) i4IfIndex, 0, &pCktEntry);

    if (i4RetVal == ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INS_EN_INTER);
        return CLI_FAILURE;
    }
    else
    {
        i4RetVal = nmhGetIsisSysNextCircIndex ((INT4) u4InstIdx, &i4CktIdx);

        if (i4RetVal != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
            return CLI_FAILURE;
        }

        if (nmhTestv2IsisCircExistState
            (&u4Error, (INT4) u4InstIdx, i4CktIdx, ISIS_CR_WT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4RetVal =
            nmhSetIsisCircExistState ((INT4) u4InstIdx, i4CktIdx, ISIS_CR_WT);

        if (i4RetVal != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2IsisCircIfIndex
        (&u4Error, (INT4) u4InstIdx, i4CktIdx, i4IfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisCircIfSubIndex (&u4Error, (INT4) u4InstIdx, i4CktIdx, 0) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisCircType
        (&u4Error, (INT4) u4InstIdx, i4CktIdx, u1CircType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisCircLevel
        (&u4Error, (INT4) u4InstIdx, i4CktIdx, i4CktLevel) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IsisCircLocalID
        (&u4Error, (INT4) u4InstIdx, i4CktIdx, u1CktLocalId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIsisCircIfIndex ((INT4) u4InstIdx, i4CktIdx, i4IfIndex) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircIfSubIndex ((INT4) u4InstIdx, i4CktIdx, 0) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircType ((INT4) u4InstIdx, i4CktIdx, u1CircType) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircLevel ((INT4) u4InstIdx, i4CktIdx, i4CktLevel) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircLocalID ((INT4) u4InstIdx, i4CktIdx, u1CktLocalId) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Loopback address should always be Passive */
    if (CfaIsLoopBackIntf ((UINT4) i4IfIndex) == TRUE)
    {
        if (nmhTestv2IsisCircPassiveCircuit (&u4Error, (INT4) u4InstIdx,
                                             i4CktIdx,
                                             ISIS_TRUE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisCircPassiveCircuit ((INT4) u4InstIdx, i4CktIdx,
                                          ISIS_TRUE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2IsisCircExistState
        (&u4Error, (INT4) u4InstIdx, i4CktIdx, ISIS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircExistState ((INT4) u4InstIdx, i4CktIdx, ISIS_ACTIVE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisCircAdminState
        (&u4Error, (INT4) u4InstIdx, i4CktIdx, ISIS_STATE_ON) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIsisCircAdminState ((INT4) u4InstIdx, i4CktIdx, ISIS_STATE_ON) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliDelCkt                                          
 * Description   :  This function deletes the ckt entry  for the given     
 *                  indices if the record exists.                          
 * Input (s)     : CliHandle     - CliContext      
 *                  u4Inst       - Instance Id
 *                  i4IfIndex    - Interface Index                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *
 ******************************************************************************/

INT4
IsisCliDelCkt (tCliHandle CliHandle, UINT4 u4InstIdx, INT4 i4IfIndex)
{
    INT4                i4RetVal = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4Error = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx ((UINT4) i4IfIndex, 0, &pCktEntry);

    if (i4RetVal != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }
    u4CktIdx = pCktEntry->u4CktIdx;

    if (nmhTestv2IsisCircExistState
        (&u4Error, (INT4) u4InstIdx, (INT4) u4CktIdx,
         ISIS_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircExistState
        ((INT4) u4InstIdx, (INT4) u4CktIdx, ISIS_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliAddSa                                           
 * Description   :  This function the is used to Add the  Summary Address for
 *                  the given indices. If the record already exists it updates 
 *                  the metric value
 * Input (s)     : CliHandle     - CliContext      
 *                  u4Inst       - Instance Id
 *                  pu1Address    - Summary Address
 *                    pu1Mask          - Summary Mask
 *                    u4Level            - IsIs level info    
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ***************************************************************************/

INT4
IsisCliAddSa (tCliHandle CliHandle, UINT4 u4Inst, VOID *pAddress,
              VOID *pMask, UINT4 u4Metric, UINT4 u4Level)
{
    INT4                i4ExistState = 0;
    tSNMP_OCTET_STRING_TYPE SaAddr;
    UINT4               u4Mask = 0;
    UINT1               u1PrefLen = 0;
    INT4                i4AdminState = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    UINT4               u4IpAddr = 0;
    tIsisSysContext    *pContext = NULL;

    if (IsisCtrlGetSysContext (u4Inst, &pContext) == ISIS_FAILURE)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    SaAddr.pu1_OctetList = ISIS_MEM_ALLOC (ISIS_BUF_MISC, ISIS_IPV4_ADDR_LEN);

    u4IpAddr = OSIX_NTOHL (*(UINT4 *) pAddress);

    if (SaAddr.pu1_OctetList == NULL)
    {
        return CLI_FAILURE;
    }

    MEMCPY (SaAddr.pu1_OctetList, &u4IpAddr, sizeof (UINT4));

    SaAddr.i4_Length = ISIS_IPV4_ADDR_LEN;

    u4Mask = OSIX_NTOHL (*(UINT4 *) pMask);
    IsisUtlComputePrefixLen ((UINT1 *) &u4Mask, ISIS_IPV4_ADDR_LEN, &u1PrefLen);

    if (nmhValidateIndexInstanceIsisSummAddrTable
        (u4Inst, ISIS_IPV4, &SaAddr, u1PrefLen) != SNMP_SUCCESS)

    {
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        return CLI_FAILURE;
    }

    if (u4Level == ISIS_LEVEL1)
    {
        i4AdminState = ISIS_SUMM_ADMIN_L1;
    }
    else if (u4Level == ISIS_LEVEL2)
    {
        i4AdminState = ISIS_SUMM_ADMIN_L2;
    }
    else
    {
        i4AdminState = ISIS_SUMM_ADMIN_L12;
    }

    i4RetVal = nmhGetIsisSummAddrExistState (u4Inst,
                                             ISIS_IPV4, &SaAddr,
                                             u1PrefLen, &i4ExistState);
    if (i4RetVal != SNMP_SUCCESS)
    {
        if (nmhTestv2IsisSummAddrExistState (&u4Error,
                                             u4Inst,
                                             ISIS_IPV4, &SaAddr,
                                             u1PrefLen,
                                             ISIS_CR_GO) == SNMP_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
            return CLI_FAILURE;
        }

        if (nmhSetIsisSummAddrExistState (u4Inst,
                                          ISIS_IPV4, &SaAddr,
                                          u1PrefLen,
                                          ISIS_CR_GO) == SNMP_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4Metric != (UINT4) ISIS_CLI_INVALID_METRIC)
    {
        if ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
        {
            if (nmhTestv2FsIsisExtSummAddrFullMetric (&u4Error,
                                                      u4Inst,
                                                      ISIS_IPV4, &SaAddr,
                                                      u1PrefLen,
                                                      u4Metric) == SNMP_FAILURE)
            {
                if (u4Error == SNMP_ERR_WRONG_VALUE)
                {
                    CliPrintf (CliHandle, "%% Invalid Default Metric\n");
                }
                if (i4ExistState == 0)
                {
                    nmhSetIsisSummAddrExistState (u4Inst,
                                                  ISIS_IPV4, &SaAddr,
                                                  u1PrefLen, ISIS_DESTROY);
                }
                ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
                return CLI_FAILURE;
            }
            if (nmhSetFsIsisExtSummAddrFullMetric (u4Inst,
                                                   ISIS_IPV4, &SaAddr,
                                                   u1PrefLen,
                                                   u4Metric) == SNMP_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
                return CLI_FAILURE;
            }
        }
        else
        {
            if (nmhTestv2IsisSummAddrMetric (&u4Error,
                                             u4Inst,
                                             ISIS_IPV4, &SaAddr,
                                             u1PrefLen,
                                             ((INT4) u4Metric)) == SNMP_FAILURE)
            {
                if (i4ExistState == 0)
                {
                    nmhSetIsisSummAddrExistState (u4Inst,
                                                  ISIS_IPV4, &SaAddr,
                                                  u1PrefLen, ISIS_DESTROY);
                }
                ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
                return CLI_FAILURE;
            }
            if (nmhSetIsisSummAddrMetric (u4Inst,
                                          ISIS_IPV4, &SaAddr,
                                          u1PrefLen,
                                          ((INT4) u4Metric)) == SNMP_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
                return CLI_FAILURE;
            }
        }
    }
    if (nmhTestv2IsisSummAddrAdminState (&u4Error,
                                         u4Inst,
                                         ISIS_IPV4, &SaAddr,
                                         u1PrefLen,
                                         i4AdminState) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        return CLI_FAILURE;
    }
    if (nmhSetIsisSummAddrAdminState (u4Inst,
                                      ISIS_IPV4, &SaAddr,
                                      u1PrefLen, i4AdminState) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliAddIPV6Sa                                           
 * Description   :  This function the is used to Add the  Summary Address for
 *                  the given indices. If the record already exists it updates 
 *                  the metric value
 * Input (s)     :  CliHandle      - CliContext      
 *                  u4Inst         - Instance Id
 *                  pu1Address     - Summary Address
 *                  u4PrefixLen    - Prefix Length
 *                  u4Level        - IsIs level info    
 * Output (s)    : -                 
 *
 ***************************************************************************/

INT4
IsisCliAddIPV6Sa (tCliHandle CliHandle, UINT4 u4Inst, UINT1 *pu1Address,
                  UINT4 u4PrefixLength, UINT4 u4Metric, UINT4 u4Level)
{
    INT4                i4ExistState;
    tSNMP_OCTET_STRING_TYPE SaAddr;
    tIp6Addr            Ip6SummAddr;
    INT4                i4AdminState = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    tIsisSysContext    *pContext = NULL;

    if (IsisCtrlGetSysContext (u4Inst, &pContext) == ISIS_FAILURE)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    MEMSET (&Ip6SummAddr, 0, sizeof (tIp6Addr));
    SaAddr.pu1_OctetList =
        ISIS_MEM_ALLOC (ISIS_BUF_MISC, ISIS_MAX_IPV6_ADDR_LEN);

    if (SaAddr.pu1_OctetList == NULL)
    {
        return CLI_FAILURE;
    }

    if (INET_ATON6 (pu1Address, &Ip6SummAddr) == 0)
    {
    NMP_PT ((ISIS_LGST, "NMP <E> : INET_ATON6" "function failed \n"))}
    MEMCPY (SaAddr.pu1_OctetList, &Ip6SummAddr, ISIS_MAX_IPV6_ADDR_LEN);

    SaAddr.i4_Length = ISIS_MAX_IPV6_ADDR_LEN;

    if (nmhValidateIndexInstanceIsisSummAddrTable
        ((INT4) u4Inst, ISIS_IPV6, &SaAddr, u4PrefixLength) != SNMP_SUCCESS)

    {
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        return CLI_FAILURE;
    }

    if (u4Level == ISIS_LEVEL1)
    {
        i4AdminState = ISIS_SUMM_ADMIN_L1;
    }
    else if (u4Level == ISIS_LEVEL2)
    {
        i4AdminState = ISIS_SUMM_ADMIN_L2;
    }
    else
    {
        i4AdminState = ISIS_SUMM_ADMIN_L12;
    }

    i4RetVal = nmhGetIsisSummAddrExistState ((INT4) u4Inst,
                                             ISIS_IPV6, &SaAddr,
                                             u4PrefixLength, &i4ExistState);
    if (i4RetVal != SNMP_SUCCESS)
    {
        if (nmhTestv2IsisSummAddrExistState (&u4Error,
                                             u4Inst,
                                             ISIS_IPV6, &SaAddr,
                                             u4PrefixLength,
                                             ISIS_CR_GO) == SNMP_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
            return CLI_FAILURE;
        }

        if (nmhSetIsisSummAddrExistState (u4Inst,
                                          ISIS_IPV6, &SaAddr,
                                          u4PrefixLength,
                                          ISIS_CR_GO) == SNMP_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2IsisSummAddrAdminState (&u4Error,
                                         u4Inst,
                                         ISIS_IPV6, &SaAddr,
                                         u4PrefixLength,
                                         i4AdminState) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        return CLI_FAILURE;
    }
    if (nmhSetIsisSummAddrAdminState (u4Inst,
                                      ISIS_IPV6, &SaAddr,
                                      u4PrefixLength,
                                      i4AdminState) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    if (u4Metric != 0)
    {
        if ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
        {

            if (nmhTestv2FsIsisExtSummAddrFullMetric (&u4Error,
                                                      u4Inst,
                                                      ISIS_IPV6, &SaAddr,
                                                      u4PrefixLength,
                                                      u4Metric) == SNMP_FAILURE)
            {
                if (u4Error == SNMP_ERR_WRONG_VALUE)
                {
                    CliPrintf (CliHandle, "%% Invalid Default Metric\n");
                }

                ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
                return CLI_FAILURE;
            }
            if (nmhSetFsIsisExtSummAddrFullMetric (u4Inst,
                                                   ISIS_IPV6, &SaAddr,
                                                   u4PrefixLength,
                                                   u4Metric) == SNMP_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
                return CLI_FAILURE;
            }
        }
        else
        {
            if (nmhTestv2IsisSummAddrMetric (&u4Error,
                                             u4Inst,
                                             ISIS_IPV6, &SaAddr,
                                             u4PrefixLength,
                                             (INT4) (u4Metric)) == SNMP_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
                return CLI_FAILURE;
            }
            if (nmhSetIsisSummAddrMetric (u4Inst,
                                          ISIS_IPV6, &SaAddr,
                                          u4PrefixLength,
                                          (INT4) (u4Metric)) == SNMP_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        CliPrintf (CliHandle, "%% Invalid Default Metric\n");
    }

    ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliDelSa                                           
 * Description   :  This function the is used to Delete SA Address with given
 *                  indices    
 * Input (s)     : CliHandle     - CliContext      
 *                  u4Inst       - Instance Id
 *                  pu1Address    - Summary Address                                
 *                    pu1Mask           - Mask for the Summary Address    
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *******************************************************************************/

INT4
IsisCliDelSa (tCliHandle CliHandle, UINT4 u4Inst, VOID *pAddress, VOID *pMask)
{
    tSNMP_OCTET_STRING_TYPE SaAddr;
    UINT4               u4Mask = 0;
    UINT1               u1PrefLen = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    UINT4               u4IpAddr = 0;
    SaAddr.pu1_OctetList = ISIS_MEM_ALLOC (ISIS_BUF_MISC, ISIS_IPV4_ADDR_LEN);

    u4IpAddr = OSIX_NTOHL (*(UINT4 *) pAddress);

    if (SaAddr.pu1_OctetList == NULL)
    {
        return CLI_FAILURE;
    }

    MEMCPY (SaAddr.pu1_OctetList, &u4IpAddr, sizeof (UINT4));

    SaAddr.i4_Length = ISIS_IPV4_ADDR_LEN;

    u4Mask = OSIX_NTOHL (*(UINT4 *) pMask);
    IsisUtlComputePrefixLen ((UINT1 *) &u4Mask, ISIS_IPV4_ADDR_LEN, &u1PrefLen);

    i4RetVal =
        nmhValidateIndexInstanceIsisSummAddrTable (u4Inst,
                                                   ISIS_IPV4, &SaAddr,
                                                   u1PrefLen);
    if (i4RetVal != SNMP_SUCCESS)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisSummAddrExistState (&u4Error,
                                         u4Inst,
                                         ISIS_IPV4, &SaAddr,
                                         u1PrefLen,
                                         ISIS_DESTROY) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        return CLI_FAILURE;
    }
    if (nmhSetIsisSummAddrExistState (u4Inst,
                                      ISIS_IPV4, &SaAddr,
                                      u1PrefLen, ISIS_DESTROY) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliDelIPV6Sa                                           
 * Description   :  This function the is used to Delete SA Address with given
 *                  indices    
 * Input (s)     : CliHandle     - CliContext      
 *                  u4Inst       - Instance Id
 *                  pu1Address    - Summary Address                                
 *                  pu1Mask           - Mask for the Summary Address    
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *******************************************************************************/

INT4
IsisCliDelIPV6Sa (tCliHandle CliHandle, UINT4 u4Inst, UINT1 *pu1Address,
                  UINT4 u4Prefixlength)
{
    tSNMP_OCTET_STRING_TYPE SaAddr;
    tIp6Addr            Ip6SummAddr;
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;

    MEMSET (&Ip6SummAddr, 0, sizeof (tIp6Addr));
    SaAddr.pu1_OctetList =
        ISIS_MEM_ALLOC (ISIS_BUF_MISC, ISIS_MAX_IPV6_ADDR_LEN);

    if (SaAddr.pu1_OctetList == NULL)
    {
        return CLI_FAILURE;
    }

    if (INET_ATON6 (pu1Address, &Ip6SummAddr) == 0)
    {
    NMP_PT ((ISIS_LGST, "NMP <E> : INET_ATON6" "function failed \n"))}
    MEMCPY (SaAddr.pu1_OctetList, &Ip6SummAddr, ISIS_MAX_IPV6_ADDR_LEN);

    SaAddr.i4_Length = ISIS_MAX_IPV6_ADDR_LEN;

    i4RetVal =
        nmhValidateIndexInstanceIsisSummAddrTable (u4Inst,
                                                   ISIS_IPV6, &SaAddr,
                                                   u4Prefixlength);
    if (i4RetVal != SNMP_SUCCESS)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisSummAddrExistState (&u4Error,
                                         u4Inst,
                                         ISIS_IPV6, &SaAddr,
                                         u4Prefixlength,
                                         ISIS_DESTROY) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        return CLI_FAILURE;
    }
    if (nmhSetIsisSummAddrExistState (u4Inst,
                                      ISIS_IPV6, &SaAddr,
                                      u4Prefixlength,
                                      ISIS_DESTROY) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    ISIS_MEM_FREE (ISIS_BUF_MISC, SaAddr.pu1_OctetList);

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliAddMAA                                          
 * Description   :  This function adds the Manual Area Address with the    
 *                  given values   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4Inst       - Instance Id
 *                  pu1NetId     - NET info
 *                  i4NetDotComp - Seperator of Net id.
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *****************************************************************************/

INT4
IsisCliAddMAA (tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1NetId,
               INT4 i4NetDotComp)
{
    tSNMP_OCTET_STRING_TYPE OctetStr;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4Error = 0;
    UINT1               u1NetLen = 0;
    UINT1               au1NET[ISIS_AREA_ADDR_LEN + ISIS_SYS_ID_LEN + 1];
    UINT1               au1SysID[ISIS_SYS_ID_LEN + 1];
    INT4                i4ExistState = 0;

    MEMSET (au1NET, 0, ISIS_AREA_ADDR_LEN + ISIS_SYS_ID_LEN + 1);
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (i4NetDotComp == ISIS_DOT_TRUE)
    {
        u1NetLen = (UINT1) IsisNetOctetLen (pu1NetId);
    }
    else
    {
        u1NetLen = (UINT1) OctetLen (pu1NetId);
    }

    if ((u1NetLen <= (ISIS_SYS_ID_LEN + 1))
        || (u1NetLen >= (ISIS_AREA_ADDR_LEN + (ISIS_SYS_ID_LEN + 1))))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEN);
        return CLI_FAILURE;
    }

    if (i4NetDotComp == ISIS_DOT_TRUE)
    {
        ISIS_CONVERT_DOT_NET_TO_ARRAY (pu1NetId, au1NET);
    }
    else
    {
        CLI_CONVERT_DOT_STR_TO_ARRAY (pu1NetId, au1NET);
    }

    if (au1NET[u1NetLen - 1] != 0)
    {
        CLI_SET_ERR (CLI_ISIS_INV_SEL);
        return CLI_FAILURE;
    }

    if ((i4NetDotComp == ISIS_DOT_TRUE)
        && (au1NET[0] > ISIS_MAX_FIRST_BYTE_AREA_ID))
    {
        CLI_SET_ERR (CLI_ISIS_INVALID_AREA_ID);
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIsisSysExistState ((INT4) u4InstIdx, &i4ExistState);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_CREATE_INS);
        return CLI_FAILURE;
    }

    /* Setting the Compliance of NET ID based on the seperator */
    if (nmhTestv2FsIsisDotCompliance (&u4Error, u4InstIdx, i4NetDotComp) !=
        SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_COMPLIANCE_EXIST);
        return CLI_FAILURE;
    }

    if (i4ExistState != ISIS_ACTIVE)
    {
        if (nmhSetFsIsisDotCompliance (u4InstIdx, i4NetDotComp) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%% Compliance not Set\r\n");
            return CLI_FAILURE;
        }

        OctetStr.pu1_OctetList = &au1NET[u1NetLen - (ISIS_SYS_ID_LEN + 1)];
        OctetStr.i4_Length = ISIS_SYS_ID_LEN;

        if (nmhTestv2IsisSysID (&u4Error, u4InstIdx, &OctetStr) != SNMP_SUCCESS)
        {
            /* This can not happen, as the only reason for failure is 
             * invalid instance index */
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysID ((INT4) u4InstIdx, &OctetStr) != SNMP_SUCCESS)
        {
            /* This should not happen at all */
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;

        }

        if (nmhTestv2IsisSysProtSuppExistState
            (&u4Error, (INT4) u4InstIdx, ISIS_IPV4_SUPP,
             ISIS_CR_GO) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysProtSuppExistState
            ((INT4) u4InstIdx, ISIS_IPV4_SUPP, ISIS_CR_GO) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2IsisSysProtSuppExistState
            (&u4Error, (INT4) u4InstIdx, ISIS_IPV6_PROTO_SUPP,
             ISIS_CR_GO) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysProtSuppExistState
            ((INT4) u4InstIdx, ISIS_IPV6_PROTO_SUPP,
             ISIS_CR_GO) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    else
    {
        OctetStr.pu1_OctetList = au1SysID;
        OctetStr.i4_Length = 0;
        nmhGetIsisSysID ((INT4) u4InstIdx, &OctetStr);
        if (MEMCMP (OctetStr.pu1_OctetList,
                    &au1NET[u1NetLen - (ISIS_SYS_ID_LEN + 1)],
                    ISIS_SYS_ID_LEN) != 0)
        {
            CLI_SET_ERR (CLI_ISIS_INV_SYS_ID);
            return CLI_FAILURE;
        }
    }

    OctetStr.pu1_OctetList = au1NET;
    OctetStr.i4_Length = u1NetLen - (ISIS_SYS_ID_LEN + 1);

    if (nmhTestv2IsisManAreaAddrExistState (&u4Error,
                                            (INT4) u4InstIdx,
                                            &OctetStr,
                                            ISIS_CR_GO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisManAreaAddrExistState ((INT4) u4InstIdx,
                                         &OctetStr, ISIS_CR_GO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;

    }

    nmhGetIsisSysExistState ((INT4) u4InstIdx, &i4ExistState);
    if (i4ExistState != ISIS_ACTIVE)
    {
        /* now all the mandatory parameters are set, so the call should be 
         * successful            */

        if (nmhTestv2IsisSysExistState (&u4Error, (INT4) u4InstIdx, ISIS_ACTIVE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysExistState ((INT4) u4InstIdx, ISIS_ACTIVE) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;

        }

        if (nmhTestv2IsisSysAdminState
            (&u4Error, (INT4) u4InstIdx, ISIS_STATE_ON) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIsisSysAdminState ((INT4) u4InstIdx, ISIS_STATE_ON) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : IsisCliDelMAA                                           
 * Description   : This function deletes the MAA with the given indices   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 *                  pu1NetId    - NET info
 *                  i4NetDotComp - Seperator of Net id. 
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliDelMAA (tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1NetId,
               INT4 i4NetDotComp)
{
    INT4                i4RetVal = 0;
    INT4                i4ExistState = 0;
    UINT1               u1NetLen = 0;
    UINT1               au1NET[ISIS_AREA_ADDR_LEN + ISIS_SYS_ID_LEN + 1];
    UINT1               au1SysID[ISIS_SYS_ID_LEN];
    UINT4               u4Error = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE tempOctetStr;
    UINT1               au1BlankSpace[ISIS_SYS_ID_LEN];
    INT4                i4DotCompliance = ISIS_DOT_FALSE;
    tIsisSysContext    *pContext = NULL;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&tempOctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1BlankSpace, 0, ISIS_SYS_ID_LEN);
    MEMSET (au1SysID, 0, ISIS_SYS_ID_LEN);
    MEMSET (au1NET, 0, ISIS_AREA_ADDR_LEN + ISIS_SYS_ID_LEN + 1);

    i4RetVal = nmhGetIsisSysExistState ((INT4) u4InstIdx, &i4ExistState);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_CRE_INS);
        return CLI_FAILURE;
    }
    if (i4ExistState == ISIS_NOT_IN_SER)
    {
        CLI_SET_ERR (CLI_ISIS_NO_ENTRY);
        return CLI_FAILURE;
    }

    /* Get the format of the stored netid */
    i4RetVal = nmhGetFsIsisDotCompliance ((INT4) u4InstIdx, &i4DotCompliance);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Failure in Getting compliance\r\n");
        return CLI_FAILURE;
    }

    /* If the formats of the netids are not matching */
    if ((INT4) i4NetDotComp != i4DotCompliance)
    {
        CliPrintf (CliHandle, "%% Invalid NET ID format\r\n");
        return CLI_FAILURE;
    }

    if (i4DotCompliance == ISIS_DOT_TRUE)
    {
        u1NetLen = (UINT1) IsisNetOctetLen (pu1NetId);
    }
    else
    {
        u1NetLen = (UINT1) OctetLen (pu1NetId);
    }

    if ((u1NetLen < (ISIS_SYS_ID_LEN + 1))
        || (u1NetLen > (ISIS_AREA_ADDR_LEN + (ISIS_SYS_ID_LEN + 1))))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEN);
        return CLI_FAILURE;
    }

    if (i4DotCompliance == ISIS_DOT_TRUE)
    {
        ISIS_CONVERT_DOT_NET_TO_ARRAY (pu1NetId, au1NET);
    }
    else
    {
        CLI_CONVERT_DOT_STR_TO_ARRAY (pu1NetId, au1NET);
    }

    if (au1NET[u1NetLen - 1] != 0)
    {
        CLI_SET_ERR (CLI_ISIS_INV_SEL);
        return CLI_FAILURE;
    }

    OctetStr.pu1_OctetList = au1SysID;
    OctetStr.i4_Length = 0;

    nmhGetIsisSysID ((INT4) u4InstIdx, &OctetStr);

    if (MEMCMP
        (OctetStr.pu1_OctetList, &au1NET[u1NetLen - (ISIS_SYS_ID_LEN + 1)],
         ISIS_SYS_ID_LEN) != 0)
    {
        CLI_SET_ERR (CLI_ISIS_INV_SYS_ID);
        return CLI_FAILURE;
    }

    i4RetVal =
        nmhTestv2IsisSysExistState (&u4Error, (INT4) u4InstIdx,
                                    ISIS_NOT_IN_SER);

    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    tempOctetStr.pu1_OctetList = au1BlankSpace;
    tempOctetStr.i4_Length = ISIS_SYS_ID_LEN;

    if (nmhTestv2IsisSysID (&u4Error, (INT4) u4InstIdx, &tempOctetStr) !=
        SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_SYS_ID);
        return CLI_FAILURE;
    }

    OctetStr.pu1_OctetList = au1NET;
    OctetStr.i4_Length = u1NetLen - (ISIS_SYS_ID_LEN + 1);

    i4RetVal = nmhGetIsisManAreaAddrExistState ((INT4) u4InstIdx,
                                                &OctetStr, &i4ExistState);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_NO_AREA_ADDR);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisManAreaAddrExistState (&u4Error,
                                            (INT4) u4InstIdx,
                                            &OctetStr,
                                            ISIS_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisManAreaAddrExistState ((INT4) u4InstIdx,
                                         &OctetStr,
                                         ISIS_DESTROY) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST,
                 "CLI <E> : System Entry does not exist "
                 "for the given Index\n"));
        return CLI_FAILURE;
    }

    /*Check Any ManAreaAddress exist if not delete the protosupport also */
    /*if no manual area address is found, then dot compliance set to default */
    if ((pContext != NULL) && (pContext->ManAddrTable.pMAARec == NULL))
    {
        /* Setting the Compliance of NET ID seperator OID to default value */
        if (nmhSetFsIsisDotCompliance ((INT4) u4InstIdx,
                                       ISIS_DOT_FALSE) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%% Compliance not Set\r\n");
            return CLI_FAILURE;
        }

        i4RetVal = nmhSetIsisSysExistState ((INT4) u4InstIdx, ISIS_NOT_IN_SER);

        if (i4RetVal != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysID ((INT4) u4InstIdx, &tempOctetStr) != SNMP_SUCCESS)
        {
            /* This should not happen at all */
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2IsisSysProtSuppExistState
            (&u4Error, (INT4) u4InstIdx, ISIS_IPV4_SUPP,
             ISIS_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysProtSuppExistState
            ((INT4) u4InstIdx, ISIS_IPV4_SUPP, ISIS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2IsisSysProtSuppExistState
            (&u4Error, (INT4) u4InstIdx, ISIS_IPV6_PROTO_SUPP,
             ISIS_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisSysProtSuppExistState
            ((INT4) u4InstIdx, ISIS_IPV6_PROTO_SUPP,
             ISIS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliAddIpRa                                         
 * Description   :  This function adds the IPRA Address with the given values   
 *                  If the IPRA already exists then update the given values 
 * Input (s)     : CliHandle     - CliContext      
 *                  u4Inst       - Instance Id
 *                  i4IfIndex    - Interface Index                                
 *                  pu1Address     - IPRA Address
 *                  pu1Mask         - Mask for the IPRA address
 *                  pu1NextHop     - Next Hop info        
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliAddIpRa (tCliHandle CliHandle, UINT4 u4Inst, UINT4 u4IfIndex,
                VOID *pAddress, VOID *pMask, VOID *pNextHop, UINT4 u4MetType)
{
    tSNMP_OCTET_STRING_TYPE IpRaAddr;
    tSNMP_OCTET_STRING_TYPE IpNextHopAddr;
    UINT4               u4Error = 0;
    UINT1               u1PrefixLen;
    INT4                i4RetVal = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4IpNextHop = 0;
    UINT4               u4Mask = 0;

    u4Mask = OSIX_NTOHL (*(UINT4 *) pMask);
    IsisUtlComputePrefixLen ((UINT1 *) &u4Mask, 4, &u1PrefixLen);

    IpRaAddr.pu1_OctetList = ISIS_MEM_ALLOC (ISIS_BUF_MISC, ISIS_IPV4_ADDR_LEN);

    if (IpRaAddr.pu1_OctetList == NULL)
    {
        return CLI_FAILURE;
    }
    IpNextHopAddr.pu1_OctetList =
        ISIS_MEM_ALLOC (ISIS_BUF_MISC, ISIS_IPV4_ADDR_LEN);

    if ((IpRaAddr.pu1_OctetList == NULL)
        || (IpNextHopAddr.pu1_OctetList == NULL))
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);
        CliPrintf (CliHandle, "OctetList is null\n");
        return CLI_FAILURE;
    }
    u4IpAddr = OSIX_NTOHL (*(UINT4 *) pAddress);
    if (IpNextHopAddr.pu1_OctetList == NULL)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        return CLI_FAILURE;
    }
    MEMCPY (IpRaAddr.pu1_OctetList, &u4IpAddr, sizeof (UINT4));

    u4IpNextHop = OSIX_NTOHL (*(UINT4 *) pNextHop);
    MEMCPY (IpNextHopAddr.pu1_OctetList, &u4IpNextHop, sizeof (UINT4));

    IpRaAddr.i4_Length = ISIS_IPV4_ADDR_LEN;

    IpNextHopAddr.i4_Length = ISIS_IPV4_ADDR_LEN;

    if (nmhTestv2IsisIPRAExistState (&u4Error, u4Inst,
                                     ISIS_MANUAL_TYPE,
                                     u4IfIndex, ISIS_CR_WT) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    i4RetVal = nmhSetIsisIPRAExistState (u4Inst,
                                         ISIS_MANUAL_TYPE,
                                         u4IfIndex, ISIS_CR_WT);
    if (i4RetVal == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2IsisIPRADestType
        (&u4Error, u4Inst, ISIS_MANUAL_TYPE, u4IfIndex,
         ISIS_IPV4) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2IsisIPRADest
        (&u4Error, u4Inst, ISIS_MANUAL_TYPE, u4IfIndex,
         &IpRaAddr) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2IsisIPRADestPrefixLen (&u4Error, u4Inst, ISIS_MANUAL_TYPE,
                                        u4IfIndex, u1PrefixLen) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }
    if (nmhTestv2FsIsisExtIPRANextHopType (&u4Error, u4Inst,
                                           ISIS_MANUAL_TYPE, u4IfIndex,
                                           ISIS_IPV4) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2FsIsisExtIPRANextHop (&u4Error, u4Inst,
                                       ISIS_MANUAL_TYPE, u4IfIndex,
                                       &IpNextHopAddr) == SNMP_FAILURE)
    {

        nmhSetIsisIPRAExistState (u4Inst,
                                  ISIS_MANUAL_TYPE, u4IfIndex, ISIS_DESTROY);
        CliPrintf (CliHandle, "%% Invalid Next-Hop Address\n\r");
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }
    if (nmhTestv2IsisIPRAMetricType
        (&u4Error, u4Inst, ISIS_MANUAL_TYPE, u4IfIndex,
         u4MetType) == SNMP_FAILURE)
    {

        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);
        return CLI_FAILURE;

    }
    if (nmhSetIsisIPRADestType (u4Inst, ISIS_MANUAL_TYPE, u4IfIndex, ISIS_IPV4)
        == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisIPRADest (u4Inst, ISIS_MANUAL_TYPE, u4IfIndex, &IpRaAddr) ==
        SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhSetIsisIPRAMetricType
        (u4Inst, ISIS_MANUAL_TYPE, u4IfIndex, u4MetType) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisIPRADestPrefixLen (u4Inst, ISIS_MANUAL_TYPE,
                                     u4IfIndex, u1PrefixLen) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtIPRANextHop (u4Inst,
                                    ISIS_MANUAL_TYPE, u4IfIndex,
                                    &IpNextHopAddr) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisIPRAExistState (&u4Error, u4Inst,
                                     ISIS_MANUAL_TYPE,
                                     u4IfIndex, ISIS_ACTIVE) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhSetIsisIPRAExistState
        (u4Inst, ISIS_MANUAL_TYPE, u4IfIndex, ISIS_ACTIVE) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisIPRAAdminState (&u4Error, u4Inst, ISIS_MANUAL_TYPE,
                                     u4IfIndex, ISIS_STATE_ON) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }
    if (nmhSetIsisIPRAAdminState (u4Inst, ISIS_MANUAL_TYPE,
                                  u4IfIndex, ISIS_STATE_ON) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
    ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : IsisCliAddIpV6Ra                                         
 * Description   :  This function adds the IPRA Address with the given values   
 *                  If the IPRA already exists then update the given values 
 * Input (s)     : CliHandle     - CliContext      
 *                  u4Inst       - Instance Id
 *                  i4IfIndex    - Interface Index                                
 *                  pu1Address     - IPRA Address
 *                  u4PrefixLen    - Prefix Length
 *                  pu1NextHop     - Next Hop info        
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliAddIpV6Ra (tCliHandle CliHandle, UINT4 u4Inst, UINT4 u4IfIndex,
                  UINT1 *pu1Address, UINT4 u4PrefixLen, UINT1 *pu1NextHop,
                  UINT4 u4MetType)
{
    tSNMP_OCTET_STRING_TYPE IpRaAddr;
    tSNMP_OCTET_STRING_TYPE IpNextHopAddr;
    UINT4               u4Error = 0;
    INT4                i4RetVal = 0;

    IpRaAddr.pu1_OctetList =
        ISIS_MEM_ALLOC (ISIS_BUF_MISC, ISIS_MAX_IPV6_ADDR_LEN);

    if (IpRaAddr.pu1_OctetList == NULL)
    {
        return CLI_FAILURE;
    }

    IpNextHopAddr.pu1_OctetList =
        ISIS_MEM_ALLOC (ISIS_BUF_MISC, ISIS_MAX_IPV6_ADDR_LEN);

    if (IpNextHopAddr.pu1_OctetList == NULL)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        return CLI_FAILURE;
    }

    MEMCPY (IpRaAddr.pu1_OctetList, pu1Address, ISIS_MAX_IPV6_ADDR_LEN);

    MEMCPY (IpNextHopAddr.pu1_OctetList, pu1NextHop, ISIS_MAX_IPV6_ADDR_LEN);

    IpRaAddr.i4_Length = ISIS_MAX_IPV6_ADDR_LEN;

    IpNextHopAddr.i4_Length = ISIS_MAX_IPV6_ADDR_LEN;

    if (nmhTestv2IsisIPRAExistState (&u4Error, u4Inst,
                                     ISIS_MANUAL_TYPE,
                                     u4IfIndex, ISIS_CR_WT) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    i4RetVal = nmhSetIsisIPRAExistState (u4Inst,
                                         ISIS_MANUAL_TYPE,
                                         u4IfIndex, ISIS_CR_WT);
    if (i4RetVal == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2IsisIPRADestType
        (&u4Error, u4Inst, ISIS_MANUAL_TYPE, u4IfIndex,
         ISIS_IPV4) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2IsisIPRADest
        (&u4Error, u4Inst, ISIS_MANUAL_TYPE, u4IfIndex,
         &IpRaAddr) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2IsisIPRAMetricType
        (&u4Error, u4Inst, ISIS_MANUAL_TYPE, u4IfIndex,
         u4MetType) == SNMP_FAILURE)
    {

        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);
        return CLI_FAILURE;

    }

    if (nmhTestv2IsisIPRADestPrefixLen (&u4Error, u4Inst, ISIS_MANUAL_TYPE,
                                        u4IfIndex, u4PrefixLen) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2FsIsisExtIPRANextHopType (&u4Error, u4Inst,
                                           ISIS_MANUAL_TYPE, u4IfIndex,
                                           ISIS_IPV6) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhTestv2FsIsisExtIPRANextHop (&u4Error, u4Inst,
                                       ISIS_MANUAL_TYPE, u4IfIndex,
                                       &IpNextHopAddr) == SNMP_FAILURE)
    {
        nmhSetIsisIPRAExistState (u4Inst,
                                  ISIS_MANUAL_TYPE, u4IfIndex, ISIS_DESTROY);
        CliPrintf (CliHandle, "%% Invalid Next-Hop Address\n\r");
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }

    if (nmhSetIsisIPRADestType (u4Inst, ISIS_MANUAL_TYPE, u4IfIndex, ISIS_IPV6)
        == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisIPRADest (u4Inst, ISIS_MANUAL_TYPE, u4IfIndex, &IpRaAddr) ==
        SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhSetIsisIPRAMetricType
        (u4Inst, ISIS_MANUAL_TYPE, u4IfIndex, u4MetType) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisIPRADestPrefixLen (u4Inst, ISIS_MANUAL_TYPE,
                                     u4IfIndex, u4PrefixLen) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtIPRANextHop (u4Inst,
                                    ISIS_MANUAL_TYPE, u4IfIndex,
                                    &IpNextHopAddr) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2IsisIPRAExistState (&u4Error, u4Inst,
                                     ISIS_MANUAL_TYPE,
                                     u4IfIndex, ISIS_ACTIVE) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }
    if (nmhTestv2IsisIPRAAdminState (&u4Error, u4Inst, ISIS_MANUAL_TYPE,
                                     u4IfIndex, ISIS_STATE_ON) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        return CLI_FAILURE;
    }
    if (nmhSetIsisIPRAAdminState (u4Inst, ISIS_MANUAL_TYPE,
                                  u4IfIndex, ISIS_STATE_ON) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIsisIPRAExistState
        (u4Inst, ISIS_MANUAL_TYPE, u4IfIndex, ISIS_ACTIVE) == SNMP_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
        ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    ISIS_MEM_FREE (ISIS_BUF_MISC, IpRaAddr.pu1_OctetList);
    ISIS_MEM_FREE (ISIS_BUF_MISC, IpNextHopAddr.pu1_OctetList);

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliUpdIPRA                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 *                  i4default_metric - metric value
 *                  u4full_metric - metric value
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliUpdIPRA (tCliHandle CliHandle, UINT4 u4InstIdx, INT4 i4ipraIndex,
                INT4 i4default_metric, UINT4 u4full_metric)
{
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    INT4                i4ExistState = 0;
    tIsisSysContext    *pContext = NULL;

    i4RetVal =
        nmhGetIsisIPRAExistState ((INT4) u4InstIdx, ISIS_MANUAL_TYPE,
                                  i4ipraIndex, &i4ExistState);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%% Required to add the IPRA address\n\r");
        return CLI_FAILURE;
    }

    /* Get the context */
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal != ISIS_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%% Context does not exist\n\r");
        return CLI_FAILURE;
    }

    if (u4full_metric != 0)
    {
        if (nmhTestv2FsIsisExtIPRAFullMetric (&u4Error,
                                              (INT4) u4InstIdx,
                                              ISIS_MANUAL_TYPE,
                                              i4ipraIndex,
                                              u4full_metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2IsisIPRAMetric (&u4Error, (INT4) u4InstIdx,
                                     ISIS_MANUAL_TYPE,
                                     i4ipraIndex,
                                     i4default_metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (u4full_metric != 0)
    {
        if (nmhSetFsIsisExtIPRAFullMetric ((INT4) u4InstIdx,
                                           ISIS_MANUAL_TYPE,
                                           i4ipraIndex,
                                           u4full_metric) == SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhSetIsisIPRAMetric ((INT4) u4InstIdx,
                                  ISIS_MANUAL_TYPE,
                                  i4ipraIndex,
                                  i4default_metric) == SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliDelIpRa                                          
 * Description   :  This function deletes the IpRa with the given indices  
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 *                  i4ipraIndex    - Index for IPRA table                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliDelIpRa (tCliHandle CliHandle, UINT4 u4InstIdx, INT4 i4ipraIndex)
{
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;

    i4RetVal = nmhTestv2IsisIPRAExistState (&u4Error,
                                            (INT4) u4InstIdx,
                                            ISIS_MANUAL_TYPE,
                                            i4ipraIndex, ISIS_DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    i4RetVal = nmhSetIsisIPRAExistState ((INT4) u4InstIdx,
                                         ISIS_MANUAL_TYPE,
                                         i4ipraIndex, ISIS_DESTROY);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : IsIsGetRouterCfgPrompt                                 */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/* Input (s)     :       
 *                  pi1ModeName  - Router mode name
 *                  pi1DispStr    - Display String                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *                                                                            */
/******************************************************************************/

INT1
IsIsGetRouterCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    UINT4               u4Index;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_MODE_ROUTER_ISIS);
    if (STRNCMP (pi1ModeName, CLI_MODE_ROUTER_ISIS, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4Index = CLI_ATOI (pi1ModeName);

    CLI_SET_CXT_ID (u4Index);

    STRCPY (pi1DispStr, "(config-router)#");
    return TRUE;
}

/*******************************************************************************
 * Function Name :  IsisCliSetDBOL                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *****************************************************************************/

INT4
IsisCliSetDBOL (tCliHandle CliHandle, UINT4 u4InstIdx)
{
    INT4                i4RetVal = 0;
    INT4                i4SysType = 0;
    INT4                i4OlType = 0;
    UINT4               u4Error = 0;

    i4RetVal = nmhGetIsisSysType ((INT4) u4InstIdx, &i4SysType);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    switch (i4SysType)
    {
        case ISIS_LEVEL1:
            i4OlType = ISIS_LL_SET_L1OVERLOAD;
            break;

        case ISIS_LEVEL2:
            i4OlType = ISIS_LL_SET_L2OVERLOAD;
            break;

        default:
            i4OlType = ISIS_LL_SET_L12OVERLOAD;
            break;
    }

    if (nmhTestv2IsisSysSetOverload (&u4Error, (INT4) u4InstIdx, i4OlType) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisSysSetOverload ((INT4) u4InstIdx, i4OlType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliResetDBOL                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliResetDBOL (tCliHandle CliHandle, UINT4 u4InstIdx)
{
    UINT4               u4Error = 0;
    INT4                i4RetVal = 0;

    i4RetVal = nmhTestv2IsisSysSetOverload (&u4Error, (INT4) u4InstIdx,
                                            ISIS_LL_CLEAR_OVERLOAD);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIsisSysSetOverload ((INT4) u4InstIdx, ISIS_LL_CLEAR_OVERLOAD) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetAreaPasswd                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 *                  pu1passwd    - Area password                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetAreaPasswd (tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1passwd)
{
    UINT4               u4Error = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (STRLEN (pu1passwd) > ISIS_MAX_PASSWORD_LEN)
    {
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return CLI_FAILURE;
    }

    OctetStr.pu1_OctetList = pu1passwd;
    OctetStr.i4_Length = STRLEN (pu1passwd);

    if (nmhTestv2FsIsisExtSysAreaTxPasswd (&u4Error,
                                           (INT4) u4InstIdx,
                                           &OctetStr) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtSysAreaTxPasswd ((INT4) u4InstIdx, &OctetStr) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliResetAreaPasswd                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 *                  pu1passwd    - Area password                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliResetAreaPasswd (tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1passwd)
{
    INT4                i4RetVal = 0;
    UINT1               au1Passwd[ISIS_MAX_PASSWORD_LEN];
    tSNMP_OCTET_STRING_TYPE OctetStr;

    OctetStr.pu1_OctetList = au1Passwd;
    OctetStr.i4_Length = ISIS_MAX_PASSWORD_LEN;
    i4RetVal = nmhGetFsIsisExtSysAreaTxPasswd ((INT4) u4InstIdx, &OctetStr);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    if ((OctetStr.i4_Length == (INT4) STRLEN (pu1passwd))
        && (MEMCMP (OctetStr.pu1_OctetList, pu1passwd,
                    OctetStr.i4_Length) == 0))
    {
        OctetStr.i4_Length = 0;

        if (nmhSetFsIsisExtSysAreaTxPasswd ((INT4) u4InstIdx, &OctetStr) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        CLI_SET_ERR (CLI_ISIS_NOTCONFIG_PASS);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetDomainPasswd                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 *                  pu1passwd    - Domain password                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliSetDomainPasswd (tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1passwd)
{
    UINT4               u4Error = 0;
    INT4                i4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (STRLEN (pu1passwd) > ISIS_MAX_PASSWORD_LEN)
    {
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return CLI_FAILURE;
    }

    OctetStr.pu1_OctetList = pu1passwd;
    OctetStr.i4_Length = STRLEN (pu1passwd);

    i4RetVal = nmhTestv2FsIsisExtSysDomainTxPasswd (&u4Error,
                                                    (INT4) u4InstIdx,
                                                    &OctetStr);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtSysDomainTxPasswd ((INT4) u4InstIdx, &OctetStr) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliResetDomainPasswd
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 *                  pu1passwd    - Domain passwaord                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliResetDomainPasswd (tCliHandle CliHandle, UINT4 u4InstIdx,
                          UINT1 *pu1passwd)
{
    INT4                i4RetVal = 0;
    UINT1               au1Passwd[ISIS_MAX_PASSWORD_LEN];
    tSNMP_OCTET_STRING_TYPE OctetStr;

    OctetStr.pu1_OctetList = au1Passwd;
    OctetStr.i4_Length = ISIS_MAX_PASSWORD_LEN;

    i4RetVal = nmhGetFsIsisExtSysDomainTxPasswd ((INT4) u4InstIdx, &OctetStr);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    if ((OctetStr.i4_Length == (INT4) STRLEN (pu1passwd))
        && (MEMCMP (OctetStr.pu1_OctetList, pu1passwd,
                    OctetStr.i4_Length) == 0))
    {

        OctetStr.i4_Length = 0;

        if (nmhSetFsIsisExtSysDomainTxPasswd ((INT4) u4InstIdx, &OctetStr) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;

        }
    }
    else
    {
        CLI_SET_ERR (CLI_ISIS_NOTCONFIG_PASS);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function Name : IsisCtrlGetMAACount ()
 * Description   : This routine retrieves the Number of Manual Area Adress
 *                 entries for the given instance index
 * Input (s)     :       
 *                  u4InstIdx       - Instance Id
 *                  pu2MAACnt    - MAA count                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *****************************************************************************/
PUBLIC INT4
IsisCtrlUtlGetMAACnt (UINT4 u4InstIdx, UINT2 *pu2MAACnt)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext;

    if (IsisCtrlGetSysContext ((INT4) u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pu2MAACnt = pContext->ManAddrTable.u2NumEntries;
        i4RetVal = ISIS_SUCCESS;
    }
    return i4RetVal;
}

/*******************************************************************************
 * Function Name :  IsisCliChkOctetStr                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     :  pu1OctetStr   - Octet string
 *                     u1Len - String length
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliChkOctetStr (UINT1 *pu1OctetStr, UINT1 u1Len)
{
    UINT1               u1StrLen = 0;

    for (u1StrLen = 1; u1StrLen <= u1Len; u1StrLen++)
    {
        if ((u1StrLen % 3) == 0)
        {
            if (pu1OctetStr[u1StrLen - 1] != '.')
            {
                return ISIS_CLI_ERROR;
            }
        }
        else
        {
            if (!CLI_ISXDIGIT (pu1OctetStr[u1StrLen - 1]))
            {
                return ISIS_CLI_ERROR;
            }
        }
    }
    return ISIS_CLI_NO_ERROR;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktType                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  u1Level         -IsIs level
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktType (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    INT4                i4ExistState = 0;
    UINT1               u1SysLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;
    UINT4               u4Error = 0;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1SysLevel = pCktEntry->pContext->SysActuals.u1SysType;

    if ((u1Level != u1SysLevel) && (u1SysLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INCORRECT_LEVEL);
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIsisCircExistState (u4InstIdx, u4CktIdx, &i4ExistState);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_RETRIEVE_RECORD_ERR);
        return CLI_FAILURE;
    }
    if (i4ExistState == ISIS_ACTIVE)
    {

        if (nmhTestv2IsisCircExistState (&u4Error, u4InstIdx, u4CktIdx,
                                         ISIS_NOT_IN_SER) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4RetVal = nmhSetIsisCircExistState (u4InstIdx, u4CktIdx,
                                             ISIS_NOT_IN_SER);
        if (i4RetVal != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    if (nmhTestv2IsisCircLevel (&u4Error, u4InstIdx, u4CktIdx, u1Level)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircLevel (u4InstIdx, u4CktIdx, u1Level) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisCircExistState (&u4Error, u4InstIdx, u4CktIdx,
                                     ISIS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircExistState (u4InstIdx, u4CktIdx, ISIS_ACTIVE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktMetric                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                               
 *                  i4default_metric - metric value
 *                  i4delay_metric- metric value
 *                  i4error_metric - metric value
 *                  i4expense_metric - metric value
 *                  u1Level  - IsIs level
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktMetric (tCliHandle CliHandle, INT4 i4IfIndex,
                     INT4 i4default_metric, INT4 i4delay_metric,
                     INT4 i4error_metric, INT4 i4expense_metric,
                     UINT4 u4wide_metric, UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    INT1                u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4Error = 0;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }
    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal != ISIS_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if ((i4delay_metric != ISIS_CLI_INVALID_METRIC) ||
        (i4error_metric != ISIS_CLI_INVALID_METRIC) ||
        (i4expense_metric != ISIS_CLI_INVALID_METRIC))
    {
        /* Delay/error/expense metric is not supported for 
         * Multi-topology ISIS. Hence verifying if MT support 
         * is enabled */
        if ((pContext != NULL) &&
            (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
        {
            CLI_SET_ERR (CLI_ISIS_MET_NOT_SUPP_MT);
            return CLI_FAILURE;
        }
    }

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if (u4wide_metric != (UINT4) ISIS_CLI_INVALID_METRIC)
    {
        if (nmhTestv2FsIsisExtCircLevelWideMetric (&u4Error,
                                                   (INT4) u4InstIdx,
                                                   u4CktIdx,
                                                   u1Level,
                                                   u4wide_metric) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else if (i4default_metric != ISIS_CLI_INVALID_METRIC)
    {
        if (nmhTestv2IsisCircLevelMetric (&u4Error, u4InstIdx, u4CktIdx,
                                          u1Level,
                                          i4default_metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (i4delay_metric != ISIS_CLI_INVALID_METRIC)
    {
        if (nmhTestv2FsIsisExtCircLevelDelayMetric
            (&u4Error, u4InstIdx, u4CktIdx, u1Level,
             i4delay_metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (i4error_metric != ISIS_CLI_INVALID_METRIC)
    {
        if (nmhTestv2FsIsisExtCircLevelErrorMetric
            (&u4Error, u4InstIdx, u4CktIdx, u1Level,
             i4error_metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (i4expense_metric != ISIS_CLI_INVALID_METRIC)
    {
        if (nmhTestv2FsIsisExtCircLevelExpenseMetric
            (&u4Error, u4InstIdx, u4CktIdx, u1Level,
             i4expense_metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (u4wide_metric != (UINT4) ISIS_CLI_INVALID_METRIC)
    {
        if (nmhSetFsIsisExtCircLevelWideMetric ((INT4) u4InstIdx,
                                                (INT4) u4CktIdx,
                                                (INT4) u1Level,
                                                u4wide_metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else if (i4default_metric != ISIS_CLI_INVALID_METRIC)
    {
        if (nmhSetIsisCircLevelMetric (u4InstIdx, u4CktIdx,
                                       u1Level,
                                       i4default_metric) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4delay_metric != ISIS_CLI_INVALID_METRIC)
    {
        if (nmhSetFsIsisExtCircLevelDelayMetric (u4InstIdx, u4CktIdx,
                                                 u1Level,
                                                 i4delay_metric) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4error_metric != ISIS_CLI_INVALID_METRIC)
    {
        if (nmhSetFsIsisExtCircLevelErrorMetric (u4InstIdx, u4CktIdx,
                                                 u1Level,
                                                 i4error_metric) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if (i4expense_metric != ISIS_CLI_INVALID_METRIC)
    {
        if (nmhSetFsIsisExtCircLevelExpenseMetric (u4InstIdx, u4CktIdx,
                                                   u1Level,
                                                   i4expense_metric) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliResetCktMetric                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  u1Level       - IsIs Level
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliResetCktMetric (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    UINT1               u1CktLevel = 0;
    UINT1               u1SysMetSup = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }
    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1SysMetSup = pCktEntry->pContext->SysActuals.u1SysMetricSupp;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if ((i4RetVal != ISIS_SUCCESS) || (pContext == NULL))
    {
        return CLI_FAILURE;
    }

    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        if (nmhSetFsIsisExtCircLevelWideMetric (u4InstIdx, u4CktIdx,
                                                u1Level,
                                                ISIS_CKTL_DEF_METRIC_VALUE) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        i4RetVal = nmhSetIsisCircLevelMetric (u4InstIdx, u4CktIdx,
                                              u1Level,
                                              ISIS_CKTL_DEF_METRIC_VALUE);
        if (i4RetVal != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u1SysMetSup & ISIS_DEL_MET_SUPP_FLAG) == ISIS_DEL_MET_SUPP_FLAG)
    {
        if (nmhSetFsIsisExtCircLevelDelayMetric (u4InstIdx, u4CktIdx,
                                                 u1Level,
                                                 ISIS_CKTL_DEF_METRIC_VALUE) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u1SysMetSup & ISIS_ERR_MET_SUPP_FLAG) == ISIS_ERR_MET_SUPP_FLAG)
    {
        if (nmhSetFsIsisExtCircLevelErrorMetric (u4InstIdx, u4CktIdx,
                                                 u1Level,
                                                 ISIS_CKTL_DEF_METRIC_VALUE) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    if ((u1SysMetSup & ISIS_EXP_MET_SUPP_FLAG) == ISIS_EXP_MET_SUPP_FLAG)
    {
        if (nmhSetFsIsisExtCircLevelExpenseMetric (u4InstIdx, u4CktIdx,
                                                   u1Level,
                                                   ISIS_CKTL_DEF_METRIC_VALUE)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktHelloInt                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                 i4IfIndex    - Interface Index                                
 *                  u4Interval   - HelloInterval value
 *                  u1Level       - IsIs level
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktHelloInt (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval,
                       UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }
    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }
    if (u4Interval != ISIS_CLI_INVALID_INTERVAL)
    {
        if (nmhTestv2IsisCircLevelHelloTimer (&u4Error, u4InstIdx,
                                              u4CktIdx,
                                              u1Level,
                                              u4Interval) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        u4Interval = ISIS_HELLO_INT;
    }

    if (nmhSetIsisCircLevelHelloTimer (u4InstIdx, u4CktIdx,
                                       u1Level, u4Interval) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktHelloMulti
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  u4Interval   - HelloMultiplier value
 *                  u1Level       - IsIs level
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktHelloMulti (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval,
                         UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);

    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if (u4Interval != ISIS_CLI_INVALID_INTERVAL)
    {
        if (nmhTestv2IsisCircLevelHelloMultiplier (&u4Error, u4InstIdx,
                                                   u4CktIdx,
                                                   u1Level,
                                                   u4Interval) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        u4Interval = ISIS_HELLO_MULT;
    }

    if (nmhSetIsisCircLevelHelloMultiplier (u4InstIdx, u4CktIdx,
                                            u1Level,
                                            u4Interval) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktAddPasswd                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                 i4IfIndex    - Interface Index                                
 *                  pu1passwd      -Circuit password
 *                  u1Level        -  IsIs level    
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktAddPasswd (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1passwd,
                        UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4Error = 0;
    tIsisCktEntry      *pCktEntry = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);

    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }
    OctetStr.i4_Length = STRLEN (pu1passwd);
    OctetStr.pu1_OctetList = pu1passwd;

    if (nmhTestv2FsIsisExtCircLevelTxPassword (&u4Error,
                                               u4InstIdx,
                                               u4CktIdx,
                                               u1Level,
                                               &OctetStr) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtCircLevelTxPassword (u4InstIdx, u4CktIdx,
                                            u1Level, &OctetStr) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* This routine will fail only if the receive password already exist
     */

    nmhSetFsIsisExtCircLevelRxPasswordExistState (u4InstIdx, u4CktIdx,
                                                  u1Level,
                                                  &OctetStr, ISIS_CR_GO);
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktDelPasswd                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  pu1passwd      -Circuit password
 *                  u1Level        -  IsIs level    
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktDelPasswd (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1passwd,
                        UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    UINT1               au1Passwd[ISIS_MAX_PASSWORD_LEN];
    tIsisCktEntry      *pCktEntry = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);

    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    OctetStr.i4_Length = ISIS_MAX_PASSWORD_LEN;
    OctetStr.pu1_OctetList = au1Passwd;

    i4RetVal = nmhGetFsIsisExtCircLevelTxPassword (u4InstIdx,
                                                   u4CktIdx,
                                                   u1Level, &OctetStr);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_CKT_LVL_NO_EXIST);
        return CLI_FAILURE;
    }

    if ((OctetStr.i4_Length != (INT4) STRLEN (pu1passwd))
        || (MEMCMP (OctetStr.pu1_OctetList, pu1passwd,
                    STRLEN (pu1passwd)) != 0))
    {
        CLI_SET_ERR (CLI_ISIS_INCORRECT_PASS);
        return CLI_FAILURE;
    }

    nmhSetFsIsisExtCircLevelRxPasswordExistState (u4InstIdx, u4CktIdx,
                                                  u1Level,
                                                  &OctetStr, ISIS_DESTROY);

    OctetStr.i4_Length = 0;
    if (nmhSetFsIsisExtCircLevelTxPassword (u4InstIdx, u4CktIdx,
                                            u1Level, &OctetStr) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktLSPInt                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  u4Interval    - Circuit LSP interval
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktLSPInt (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval,
                     UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4Error = 0;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }
    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisCircLevelLSPThrottle (&u4Error,
                                           u4InstIdx,
                                           u4CktIdx,
                                           u1Level, u4Interval) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircLevelLSPThrottle (u4InstIdx, u4CktIdx,
                                        u1Level, u4Interval) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktRetrInt                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  u4Interval   - Retransmission interval
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktRetrInt (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval,
                      UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4Error = 0;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if (u4Interval != ISIS_CLI_INVALID_INTERVAL)
    {
        if (nmhTestv2IsisCircLevelMinLSPRetransInt (&u4Error,
                                                    u4InstIdx,
                                                    u4CktIdx,
                                                    u1Level,
                                                    u4Interval) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        u4Interval = ISIS_LSP_RETXN_INT;
    }

    if (nmhSetIsisCircLevelMinLSPRetransInt (u4InstIdx, u4CktIdx,
                                             u1Level,
                                             u4Interval) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktPriority                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  u4Priority   - Priority info
 *                  u1Level       - IsIs Level
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktPriority (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Priority,
                       UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }
    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }
    if (nmhTestv2IsisCircLevelISPriority (&u4Error, u4InstIdx,
                                          u4CktIdx,
                                          u1Level, u4Priority) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_PRIO);
        return CLI_FAILURE;
    }
    if (nmhSetIsisCircLevelISPriority (u4InstIdx, u4CktIdx,
                                       u1Level, u4Priority) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliSetCktCSNPInt                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  u4Interval   - Interval
 *                  u1Level       - ISIS level
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetCktCSNPInt (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Interval,
                      UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if (u4Interval != ISIS_CLI_INVALID_INTERVAL)
    {
        if (nmhTestv2IsisCircLevelCSNPInterval (&u4Error, u4InstIdx,
                                                u4CktIdx,
                                                u1Level,
                                                u4Interval) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        u4Interval = ISIS_CSNP_INT;
    }
    if (nmhSetIsisCircLevelCSNPInterval (u4InstIdx, u4CktIdx,
                                         u1Level, u4Interval) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : IsisCliSetMultiTopology                
 * Description   : This function ENABLE/DISABLE the MT support   
 * Input(s)      : CliHandle     - CliContext      
 *                 u4InstIdx    - Instance index                                
 * Output(s)     :                  
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliSetMultiTopology (tCliHandle CliHandle, INT4 i4InstIdx,
                         UINT1 u1IsisMTSupport)
{
    UINT4               u4Error = SNMP_FAILURE;

    if (nmhTestv2FsIsisMultiTopologySupport
        (&u4Error, i4InstIdx, u1IsisMTSupport) != SNMP_SUCCESS)
    {
        if (u4Error == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Value\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%% Context not created\n");
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisMultiTopologySupport (i4InstIdx, u1IsisMTSupport) !=
        SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : IsisCliSetDynHostname
 * Description   : This function ENABLE/DISABLE the Dynamic Hostname support
 * Input(s)      : CliHandle     - CliContext
 *                 u4InstIdx    - Instance index
 * Output(s)     :
 * Returns       : CLI_SUCCESS/CLI_FAILURE.
 *
 ******************************************************************************/
INT4
IsisCliSetDynHostname (tCliHandle CliHandle, INT4 i4InstIdx,
                       UINT1 u1IsisDynHostNmeSupport)
{
    UINT4               u4Error = SNMP_FAILURE;

    if (nmhTestv2FsIsisExtSysDynHostNameSupport
        (&u4Error, i4InstIdx, u1IsisDynHostNmeSupport) != SNMP_SUCCESS)
    {
        if (u4Error == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Value\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%% Context not created\n");
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtSysDynHostNameSupport
        (i4InstIdx, u1IsisDynHostNmeSupport) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#ifdef BFD_WANTED
/*******************************************************************************
 * Function Name : IsisCliSetBfdSupport
 * Description   : This function is enables or disables BFD support for M-ISIS
 * Input(s)      : u4InstIdx            - Instance index
 *                 u1IsisBfdSupport     - ISIS_TRUE/ISIS_FALSE
 * Output(s)     :
 * Returns       : CLI_SUCCESS/CLI_FAILURE.
 *
 ******************************************************************************/
INT4
IsisCliSetBfdSupport (INT4 i4InstIdx, UINT1 u1IsisBfdSupport)
{
    UINT4               u4Error = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4InstIdx, &pContext);
    if (i4RetVal != ISIS_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIsisExtSysBfdSupport
        (&u4Error, i4InstIdx, (INT4) u1IsisBfdSupport) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtSysBfdSupport
        (i4InstIdx, (INT4) u1IsisBfdSupport) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : IsisCliSetBfdAllIfStatus
 * Description   : This function is enables or disables BFD support
 *                 for all interfaces on which MT is enabled.
 * Input(s)      : u4InstIdx            - Instance index
 *                 u1IsisBfdAllIfStatus - ISIS_TRUE/ISIS_FALSE
 * Output(s)     :
 * Returns       : CLI_SUCCESS/CLI_FAILURE.
 *
 ******************************************************************************/
INT4
IsisCliSetBfdAllIfStatus (INT4 i4InstIdx, UINT1 u1IsisBfdAllIfStatus)
{
    UINT4               u4Error = SNMP_FAILURE;

    if (nmhTestv2FsIsisExtSysBfdAllIfStatus
        (&u4Error, i4InstIdx, (INT4) u1IsisBfdAllIfStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtSysBfdAllIfStatus
        (i4InstIdx, (INT4) u1IsisBfdAllIfStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : IsisCliSetBfdIfStatus
 * Description   : This function is enables or disables BFD support on a 
 *                 particular interface on which MT is enabled.
 * Input(s)      : i4IfIndex            - Interface index
 *                 u1IsisBfdIfStatus - ISIS_TRUE/ISIS_FALSE
 * Output(s)     :
 * Returns       : CLI_SUCCESS/CLI_FAILURE.
 *
 ******************************************************************************/
INT4
IsisCliSetBfdIfStatus (INT4 i4IfIndex, UINT1 u1IsisBfdIfStatus)
{
    UINT4               u4Error = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4CktIdx = 0;
    INT4                i4InstIdx = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx ((UINT4) i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    i4CktIdx = (INT4) pCktEntry->u4CktIdx;
    i4InstIdx = (INT4) pCktEntry->pContext->u4SysInstIdx;

    if (nmhTestv2FsIsisExtCircBfdStatus
        (&u4Error, i4InstIdx, i4CktIdx,
         (INT4) u1IsisBfdIfStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsIsisExtCircBfdStatus
        (i4InstIdx, i4CktIdx, (INT4) u1IsisBfdIfStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
#endif
/*******************************************************************************
 * Function Name :  IsisCliShowInsts                                      
 * Description   :  This function shows the  instances in ISIS 
 *                     
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 ******************************************************************************/
INT4
IsisCliShowInsts (tCliHandle CliHandle)
{
    CHAR                acSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    INT4                i4RetVal;
    tIsisSysContext    *pContext = NULL;
    INT4                i4DotCompliance = ISIS_DOT_FALSE;
    UINT1               au1HstNme[ISIS_HSTNME_LEN];

    MEMSET (au1HstNme, 0, ISIS_HSTNME_LEN);
    pContext = gpIsisInstances;
    if (pContext == NULL)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle,
               "\n\rSystemIdx   System-ID           SysType     OperState    MTSupport");

#ifdef BFD_WANTED
    CliPrintf (CliHandle, "    BFD Support\r\n");
    CliPrintf (CliHandle, "---------------");
#else
    CliPrintf (CliHandle, "\r\n");
#endif
    CliPrintf (CliHandle,
               "------------------------------------------------------------------\r\n");

    for (; pContext != NULL; pContext = pContext->pNext)
    {
        if (pContext->u1IsisDynHostNmeSupport != ISIS_DYNHOSTNME_DISABLE)
        {
            ISIS_GET_HSTNME (au1HstNme);
        }
        if (STRLEN (au1HstNme) <= 0)
        {
            i4RetVal =
                nmhGetFsIsisDotCompliance ((INT4) pContext->u4SysInstIdx,
                                           &i4DotCompliance);
            if (i4RetVal != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "%% Failure in Getting compliance\r\n");
                return CLI_FAILURE;
            }

            if (i4DotCompliance == ISIS_DOT_TRUE)
            {
                ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN,
                                   pContext->SysActuals.au1SysID, acSysId);
            }
            else
            {
                ISIS_FORM_STR (ISIS_SYS_ID_LEN, pContext->SysActuals.au1SysID,
                               acSysId);
            }
        }
        else
        {
            MEMSET (acSysId, 0, (3 * ISIS_SYS_ID_LEN) + 1);
            MEMCPY (acSysId, au1HstNme, STRLEN (au1HstNme));
        }
        if ((pContext->SysActuals.u1SysType < ISIS_CLI_MAX_SYS_TYPE)
            && (pContext->u1OperState < ISIS_CLI_MAX_CKT_STATE))
        {
            CliPrintf (CliHandle, "\n\r%-12lu%-20s%-12s%-13s%-13s",
                       pContext->u4SysInstIdx, acSysId,
                       api1SysType[pContext->SysActuals.u1SysType],
                       api1CktOperState[pContext->u1OperState],
                       api1MTSupport[pContext->u1IsisMTSupport]);
#ifdef BFD_WANTED
            CliPrintf (CliHandle, "%-11s\r\n",
                       api1BfdSupport[pContext->u1IsisBfdSupport]);
#else
            CliPrintf (CliHandle, "\r\n");
#endif

        }

    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliShowCkts                                      
 * Description   :  This function show circuit information 
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *******************************************************************************/
INT4
IsisCliShowCkts (tCliHandle CliHandle)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pPrintCktRec = NULL;
    CHAR                acMtid[2 * ISIS_MAX_MT + 2] = { 0 };
    UINT1               au1ContextName[VCMALIAS_MAX_ARRAY_LEN];

    CliPrintf (CliHandle,
               "CktIdx    Vrf       IfIdx     CktType   Level     OperState   MTIDs");

#ifdef BFD_WANTED
    CliPrintf (CliHandle, "   BFD Status\n");
    CliPrintf (CliHandle, "-------------");
#else
    CliPrintf (CliHandle, "\n");
#endif
    CliPrintf (CliHandle,
               "-------------------------------------------------------------------\n");

    for (pContext = gpIsisInstances; pContext != NULL;
         pContext = pContext->pNext)
    {
        pPrintCktRec = pContext->CktTable.pCktRec;
        for (pPrintCktRec = pContext->CktTable.pCktRec; pPrintCktRec != NULL;
             pPrintCktRec = pPrintCktRec->pNext)
        {
            MEMSET (acMtid, 0, sizeof (acMtid));
            if ((pContext->u1IsisMTSupport == ISIS_TRUE)
                && (pPrintCktRec->u1IfMTId != 0))
            {
                ISIS_FORM_MTID (acMtid, pPrintCktRec->u1IfMTId);
            }
            else
            {
                STRCPY (acMtid, " --");
            }

            if ((pPrintCktRec->u1CktType < ISIS_CLI_MAX_CKT_TYPE)
                && (pPrintCktRec->u1CktLevel < ISIS_CLI_MAX_CKT_LEVEL)
                && (pPrintCktRec->u1OperState < ISIS_CLI_MAX_CKT_STATE))
            {
                VcmGetAliasName (pContext->u4SysInstIdx, au1ContextName);
                CliPrintf (CliHandle,
                           "%-10lu%-10s%-10lu%-10s%-10s%-12s%-8s",
                           pPrintCktRec->u4CktIdx, au1ContextName,
                           pPrintCktRec->u4CktIfIdx,
                           api1CktType[pPrintCktRec->u1CktType],
                           api1CktLevel[pPrintCktRec->u1CktLevel],
                           api1CktOperState[pPrintCktRec->u1OperState], acMtid);
#ifdef BFD_WANTED
                CliPrintf (CliHandle, "%-10s\n",
                           api1BfdSupport[pPrintCktRec->u1IsisBfdStatus]);
#else
                CliPrintf (CliHandle, "\n");
#endif
            }
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliShowAdjs                                      
 * Description   :  This function displays the adjacency formed by ISIS 
 *                     
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *****************************************************************************/
INT4
IsisCliShowAdjs (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4Inst)
{
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4RetVal = 0;
    tIsisCktEntry      *pPrintCktRec = NULL;
    tIsisSysContext    *pContext = NULL;

    pi1InterfaceName = &ai1InterfaceName[0];
    if (i4IfIndex != 0)
    {
        i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pPrintCktRec);
        if (i4RetVal != ISIS_SUCCESS)
        {
            CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
            return CLI_FAILURE;
        }
        if (pPrintCktRec->pAdjEntry == NULL)
        {
            CLI_SET_ERR (CLI_ISIS_NO_ADJ);
            return CLI_FAILURE;
        }
        CfaCliGetIfName ((UINT4) i4IfIndex, pi1InterfaceName);
        CliPrintf (CliHandle, "\n\r Adjacency on interface %s\n",
                   pi1InterfaceName);

        for (pContext = gpIsisInstances; pContext != NULL;
             pContext = pContext->pNext)
        {
            if (pContext->u4SysInstIdx == u4Inst)
            {
                i4RetVal =
                    IsisCliShowIfAdjs (CliHandle, pPrintCktRec, pContext);
                break;
            }
        }
    }
    else
    {
        for (pContext = gpIsisInstances; pContext != NULL;
             pContext = pContext->pNext)
        {
            if (pContext->u4SysInstIdx == u4Inst)
            {
                pPrintCktRec = pContext->CktTable.pCktRec;

                while (pPrintCktRec != NULL)
                {
                    if (pPrintCktRec->pAdjEntry != NULL)
                    {
                        CfaCliGetIfName (pPrintCktRec->u4CktIfIdx,
                                         pi1InterfaceName);
                        CliPrintf (CliHandle,
                                   "\n\r Adjacency on interface %s\n",
                                   pi1InterfaceName);

                        IsisCliShowIfAdjs (CliHandle, pPrintCktRec, pContext);
                    }
                    pPrintCktRec = pPrintCktRec->pNext;
                }
            }
        }                        /* End of for Loop */
    }
    return CLI_SUCCESS;
}

INT4
IsisCliShowIfAdjs (tCliHandle CliHandle, tIsisCktEntry * pPrintCktRec,
                   tIsisSysContext * pContext)
{
    INT4                i4RetVal = 0;
    INT4                i4DotCompliance = ISIS_DOT_FALSE;
    tIsisAdjEntry      *pAdjRec = NULL;
    CHAR                acSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    CHAR                acDISId[(3 * ISIS_SYS_ID_LEN) + 1];
    UINT1              *pAddr = NULL;
    UINT1               u1PrintIpv4Addr = ISIS_TRUE;
    UINT1               u1PrintIpv6Addr = ISIS_TRUE;
    CHAR                acMtid[2 * ISIS_MAX_MT + 1] = { 0 };
#ifdef BFD_WANTED
    UINT1               u1PrintMt = ISIS_FALSE;
#endif
    UINT1               au1HstNme[ISIS_HSTNME_MAX_LEN];
    UINT1               au1HstIdNme[ISIS_HSTNMEWITHID_LEN];
    UINT1               au1TempSysID[ISIS_SYS_ID_LEN];
    UINT1               u1ApdFrgID = 0;
    UINT1               u1ApdNodeId = 0;

    MEMSET (au1HstNme, 0, ISIS_HSTNME_MAX_LEN);
    pAdjRec = pPrintCktRec->pAdjEntry;
    for (; pAdjRec != NULL; pAdjRec = pAdjRec->pNext)
    {
        u1PrintIpv4Addr = ISIS_TRUE;
        u1PrintIpv6Addr = ISIS_TRUE;

        i4RetVal =
            nmhGetFsIsisDotCompliance ((INT4) pContext->u4SysInstIdx,
                                       &i4DotCompliance);
        if (i4RetVal != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "%% Failure in Getting compliance\r\n");
            return CLI_FAILURE;
        }
        MEMSET (au1HstNme, 0, ISIS_HSTNME_MAX_LEN);
        if (pContext->u1IsisDynHostNmeSupport != ISIS_DYNHOSTNME_DISABLE)
        {
            ISIS_GET_HSTNME (au1HstNme);
        }
        if (STRLEN (au1HstNme) <= 0)
        {

            if (i4DotCompliance == ISIS_DOT_TRUE)
            {
                ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN,
                                   pContext->SysActuals.au1SysID, acSysId);
            }
            else
            {
                ISIS_FORM_STR (ISIS_SYS_ID_LEN, pContext->SysActuals.au1SysID,
                               acSysId);
            }
        }
        else
        {
            MEMSET (acSysId, 0, (3 * ISIS_SYS_ID_LEN) + 1);
            MEMCPY (acSysId, au1HstNme,
                    MEM_MAX_BYTES ((STRLEN (au1HstNme)),
                                   (3 * ISIS_SYS_ID_LEN) + 1));
        }
        if (pPrintCktRec->u1CktType == ISIS_BC_CKT)
        {
            CliPrintf (CliHandle, "\n\r DIS information\n");
            CliPrintf (CliHandle, "\r ----------------\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r -----------------------------\n");
        }
        if ((pPrintCktRec->u1CktType == ISIS_P2P_CKT)
            && (pAdjRec->u1AdjUsage == ISIS_LEVEL12))
        {
            CliPrintf (CliHandle, "\n\r LEVEL12 Adjacency\n");
            CliPrintf (CliHandle, "\r ------------------\n");
            if (i4DotCompliance == ISIS_DOT_TRUE)
            {
                ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                   acSysId);
            }
            else
            {
                ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                               acSysId);
            }

            CliPrintf (CliHandle, "\r AdjSysId          : %-18s\n", acSysId);
        }
        if ((pPrintCktRec->u1CktLevel != ISIS_LEVEL2)
            && (pAdjRec->u1AdjUsage == ISIS_LEVEL1))
        {
            if (pPrintCktRec->u1CktType == ISIS_BC_CKT)
            {
                MEMSET (au1TempSysID, 0, ISIS_SYS_ID_LEN);
                MEMCPY (au1TempSysID, pPrintCktRec->pL1CktInfo->au1CktLanDISID,
                        ISIS_SYS_ID_LEN);
                MEMSET (au1HstNme, 0, ISIS_HSTNME_MAX_LEN);
                ISIS_HST_FRMSYSID (au1TempSysID, au1HstNme, pContext);
                if (STRLEN (au1HstNme) <= 0)
                {

                    if (i4DotCompliance == ISIS_DOT_TRUE)
                    {
                        ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN,
                                           pPrintCktRec->pL1CktInfo->
                                           au1CktLanDISID, acDISId);
                    }
                    else
                    {
                        ISIS_FORM_STR (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN,
                                       pPrintCktRec->pL1CktInfo->au1CktLanDISID,
                                       acDISId);
                    }
                    CliPrintf (CliHandle, "\r LAN DIS Id        : %-18s\n",
                               acDISId);
                }
                else
                {
                    MEMSET (au1HstIdNme, 0, ISIS_HSTNMEWITHID_LEN);
                    u1ApdNodeId = 1;
                    u1ApdFrgID = 0;
                    STRNCPY (au1HstIdNme, au1HstNme, ISIS_HSTNME_DISPLAY);
                    ISIS_FORM_HSTNMEID (au1HstIdNme,
                                        pPrintCktRec->pL1CktInfo->
                                        au1CktLanDISID, u1ApdFrgID,
                                        u1ApdNodeId);
                    CliPrintf (CliHandle, "\r LAN DIS Id        : %-18s\n",
                               au1HstIdNme);
                }
            }
            CliPrintf (CliHandle, "\n\r LEVEL1 Adjacency\n");
            CliPrintf (CliHandle, "\r ----------------\n");
            MEMSET (au1HstNme, 0, ISIS_HSTNME_MAX_LEN);
            ISIS_HST_FRMSYSID (pAdjRec->au1AdjNbrSysID, au1HstNme, pContext);
            if (STRLEN (au1HstNme) <= 0)
            {
                if (i4DotCompliance == ISIS_DOT_TRUE)
                {
                    ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                       acSysId);
                }
                else
                {
                    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                   acSysId);
                }
                CliPrintf (CliHandle, "\r AdjSysId          : %-18s\n",
                           acSysId);
            }
            else
            {
                CliPrintf (CliHandle, "\r AdjSysId          : %-18.14s\n",
                           au1HstNme);
            }

        }
        else if ((pPrintCktRec->u1CktLevel != ISIS_LEVEL1)
                 && (pAdjRec->u1AdjUsage == ISIS_LEVEL2))
        {
            MEMSET (au1TempSysID, 0, ISIS_SYS_ID_LEN);
            MEMCPY (au1TempSysID, pPrintCktRec->pL2CktInfo->au1CktLanDISID,
                    ISIS_SYS_ID_LEN);
            MEMSET (au1HstNme, 0, ISIS_HSTNME_MAX_LEN);
            ISIS_HST_FRMSYSID (au1TempSysID, au1HstNme, pContext);
            if (pPrintCktRec->u1CktType == ISIS_BC_CKT)
            {
                if (STRLEN (au1HstNme) <= 0)
                {
                    if (i4DotCompliance == ISIS_DOT_TRUE)
                    {
                        ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN,
                                           pPrintCktRec->pL2CktInfo->
                                           au1CktLanDISID, acDISId);
                    }
                    else
                    {
                        ISIS_FORM_STR (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN,
                                       pPrintCktRec->pL2CktInfo->au1CktLanDISID,
                                       acDISId);
                    }
                    CliPrintf (CliHandle, "\r LAN DIS Id        : %-18s\n",
                               acDISId);
                }
                else
                {
                    MEMSET (au1HstIdNme, 0, ISIS_HSTNMEWITHID_LEN);
                    STRNCPY (au1HstIdNme, au1HstNme, ISIS_HSTNME_DISPLAY);
                    u1ApdNodeId = 1;
                    u1ApdFrgID = 0;
                    ISIS_FORM_HSTNMEID (au1HstIdNme,
                                        pPrintCktRec->pL2CktInfo->
                                        au1CktLanDISID, u1ApdFrgID,
                                        u1ApdNodeId);
                    CliPrintf (CliHandle, "\r LAN DIS Id        : %-18s\n",
                               au1HstIdNme);
                }
            }
            CliPrintf (CliHandle, "\n\r LEVEL2 Adjacency\n");
            CliPrintf (CliHandle, "\r -----------------\n");
            MEMSET (au1HstNme, 0, ISIS_HSTNME_MAX_LEN);
            ISIS_HST_FRMSYSID (pAdjRec->au1AdjNbrSysID, au1HstNme, pContext);
            if (STRLEN (au1HstNme) <= 0)
            {
                if (i4DotCompliance == ISIS_DOT_TRUE)
                {
                    ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                       acSysId);
                }
                else
                {
                    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                   acSysId);
                }
                CliPrintf (CliHandle, "\r AdjSysId          : %-18s\n",
                           acSysId);

            }
            else
            {
                CliPrintf (CliHandle, "\r AdjSysId          : %-18.14s\n",
                           au1HstNme);
            }

        }

        if ((pAdjRec->u1AdjState < ISIS_CLI_MAX_ADJ_STATE)
            && (pAdjRec->u1AdjUsage < ISIS_CLI_MAX_ADJ_STATE))
        {
            if (pPrintCktRec->u1CktType == ISIS_P2P_CKT)
            {
                CliPrintf (CliHandle, "\r Adjacency Type    : POINT-TO-POINT");
                if (pPrintCktRec->u1P2PDynHshkMachanism == ISIS_P2P_THREE_WAY)
                {
                    CliPrintf (CliHandle, " (3-WAY)\n");
                    CliPrintf (CliHandle, "\r Adjacency State IS: %-18s\n",
                               api1AdjState[pAdjRec->u1P2PThreewayState]);
                }
                else
                {
                    CliPrintf (CliHandle, " (2-WAY)\n");
                    CliPrintf (CliHandle, "\r Adjacency State IS: %-18s\n",
                               api1AdjState[pAdjRec->u1AdjState]);
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r Adjacency State IS: %-18s\n",
                           api1AdjState[pAdjRec->u1AdjState]);
            }
            CliPrintf (CliHandle, "\r Adjacency Usage IS: %-18s\n",
                       api1AdjUsage[pAdjRec->u1AdjUsage]);
        }
#ifdef BFD_WANTED
        /* If BFD is for this neighbor, then check has
         * to be done if the BFD session is UP for this 
         * neighbor */
        if ((pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE) &&
            (pPrintCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
        {
            if ((pAdjRec->u1Mt0BfdRegd == ISIS_BFD_TRUE) &&
                (ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) == ISIS_BFD_TRUE) &&
                (pAdjRec->u1IsisMT0BfdState != ISIS_BFD_SESS_UP))
            {
                u1PrintIpv4Addr = ISIS_FALSE;
            }
            if ((pAdjRec->u1Mt2BfdRegd == ISIS_BFD_TRUE) &&
                (ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) == ISIS_BFD_TRUE) &&
                (pAdjRec->u1IsisMT2BfdState != ISIS_BFD_SESS_UP))
            {
                u1PrintIpv6Addr = ISIS_FALSE;
            }
        }
#endif
        pAddr = pAdjRec->AdjNbrIpV4Addr.au1IpAddr;
        if (pAdjRec->u1AdjIpAddrType == ISIS_ADDR_IPV4)
        {
            if (u1PrintIpv4Addr == ISIS_TRUE)
            {
                CliPrintf (CliHandle, "\r IPV4 Address IS   : %d.%d.%d.%d\n",
                           pAddr[0], pAddr[1], pAddr[2], pAddr[3]);
            }
        }
        else if (pAdjRec->u1AdjIpAddrType == ISIS_ADDR_IPV6)
        {
            if (u1PrintIpv6Addr == ISIS_TRUE)
            {
                CliPrintf (CliHandle, "\r IPV6 Address IS   : %-32s\n",
                           Ip6PrintAddr ((tIp6Addr *) (VOID *) &
                                         (pAdjRec->AdjNbrIpV6Addr.au1IpAddr)));
            }
        }
        else
        {
            if ((u1PrintIpv4Addr == ISIS_TRUE)
                && (pAdjRec->u1AdjIpAddrType != ISIS_UNKNOWN))
            {
                CliPrintf (CliHandle, "\r IPV4 Address IS   : %d.%d.%d.%d\n",
                           pAddr[0], pAddr[1], pAddr[2], pAddr[3]);
            }
            if ((u1PrintIpv6Addr == ISIS_TRUE)
                && (pAdjRec->u1AdjIpAddrType != ISIS_UNKNOWN))
            {
                CliPrintf (CliHandle, "\r IPV6 Address IS   : %-32s\n",
                           Ip6PrintAddr ((tIp6Addr *) (VOID *) &
                                         (pAdjRec->AdjNbrIpV6Addr.au1IpAddr)));
            }
        }
        if ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
            (pAdjRec->u1AdjMTId != 0))
        {
            MEMSET (acMtid, 0, sizeof (acMtid));
            ISIS_FORM_MTID (acMtid, pAdjRec->u1AdjMTId);
            CliPrintf (CliHandle, "\r Supported MTID    : %-15s\n", acMtid);
        }
#ifdef BFD_WANTED
        if ((pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE) &&
            (pPrintCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
        {
            if ((pAdjRec->u1Mt0BfdRegd == ISIS_BFD_TRUE) ||
                (pAdjRec->u1Mt2BfdRegd == ISIS_BFD_TRUE))
            {
                CliPrintf (CliHandle, "\n\r BFD Session Details\n");
                CliPrintf (CliHandle, "\r ---------------------\n");
                if (pContext->u1IsisMTSupport == ISIS_TRUE)
                {
                    CliPrintf (CliHandle, "\r MTID/NLPID     NbrAddr\n");
                    CliPrintf (CliHandle, "\r ----------------------\n");
                    u1PrintMt = ISIS_TRUE;
                }
                else
                {
                    CliPrintf (CliHandle, "\r NLPID     NbrAddr\n");
                    CliPrintf (CliHandle, "\r -----------------\n");
                }

                if (pAdjRec->u1Mt0BfdRegd == ISIS_BFD_TRUE)
                {
                    if ((pContext->u1IsisMTSupport == ISIS_FALSE) ||
                        ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
                         (pAdjRec->u1IsisMT0BfdEnabled != ISIS_BFD_FALSE)))
                    {
                        if (u1PrintMt == ISIS_TRUE)
                        {
                            CliPrintf (CliHandle,
                                       "\r MT-0/IPv4      %d.%d.%d.%d\n",
                                       pAddr[0], pAddr[1], pAddr[2], pAddr[3]);
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r IPv4      %d.%d.%d.%d\n",
                                       pAddr[0], pAddr[1], pAddr[2], pAddr[3]);
                        }

                    }
                }
                if (pAdjRec->u1Mt2BfdRegd == ISIS_BFD_TRUE)
                {
                    if ((pContext->u1IsisMTSupport == ISIS_FALSE) ||
                        ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
                         (pAdjRec->u1IsisMT2BfdEnabled != ISIS_BFD_FALSE)))
                    {
                        if (u1PrintMt == ISIS_TRUE)
                        {
                            CliPrintf (CliHandle, "\r MT-2/IPv6      %s\n",
                                       Ip6PrintAddr ((tIp6Addr *) (VOID *) &
                                                     (pAdjRec->AdjNbrIpV6Addr.
                                                      au1IpAddr)));
                        }
                        else
                        {
                            CliPrintf (CliHandle, "\r IPv6      %s\n",
                                       Ip6PrintAddr ((tIp6Addr *) (VOID *) &
                                                     (pAdjRec->AdjNbrIpV6Addr.
                                                      au1IpAddr)));
                        }
                    }
                }
            }
        }
#endif
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliShowRoutes                                      
 * Description   :  This function displays the isis routes 
 *                     
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx    - Instance Id
 *                  u1Level      - ISIS level                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *****************************************************************************/

INT4
IsisCliShowRoutes (tCliHandle CliHandle, UINT1 u1Level, UINT4 u4InstIdx)
{
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pTravIn = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT1               au1DestId[(6 * ISIS_MAX_IPV6_ADDR_LEN) + 1];
    UINT1               au1NextHop[(6 * ISIS_MAX_IPV6_ADDR_LEN) + 1];
    tRBTree             RBTree;
    UINT1               u1MtSupport = ISIS_FALSE;
    UINT1               u1MetIdx = 0;
    UINT1               u1MetType = 0;

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    if ((u1Level != pContext->SysActuals.u1SysType)
        && (pContext->SysActuals.u1SysType != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    u1MtSupport = pContext->u1IsisMTSupport;

    RBTree = (u1Level == ISIS_LEVEL1) ? pContext->SysL1Info.PrevShortPath[0] :
        pContext->SysL2Info.PrevShortPath[0];

    CliPrintf (CliHandle, "Dest ID             Metric         MetType        "
               "NextHop             \n");

    CliPrintf (CliHandle, "-------------------------------------------"
               "--------------------\n");

    pTravIn = pTrav = RBTreeGetFirst (RBTree);
    while (pTrav != NULL)
    {
        if ((u1MtSupport == ISIS_TRUE) ||
            ((u1MtSupport == ISIS_FALSE) &&
             (pTrav->u1AddrType == ISIS_ADDR_IPV4)))
        {
            for (u1MetIdx = 0; u1MetIdx < ISIS_NUM_METRICS; u1MetIdx++)
            {
                if (pTrav->au2Metric[u1MetIdx] != ISIS_SPT_INV_METRIC)
                {
                    u1MetType = IsisUtlGetMetType (pContext, u1MetIdx);
                    IsisCliFormIpAddrStr (pTrav->au1DestID,
                                          (CHR1 *) au1DestId,
                                          ISIS_MAX_IPV4_ADDR_LEN);
                    if ((pTrav->au2Metric[u1MetIdx] & ISIS_SPT_L12_SYS_MASK) ==
                        ISIS_NBR_IPRA_ADJ)
                    {
                        tIsisAdjEntry      *pAdjRec = NULL;

                        pAdjRec =
                            IsisAdjGetAdjRecFromDirIdx (pContext,
                                                        pTrav->
                                                        au4DirIdx[u1MetIdx]);
                        if (pAdjRec != NULL)
                        {
                            IsisCliFormIpAddrStr (pAdjRec->
                                                  AdjNbrIpV4Addr.au1IpAddr,
                                                  (CHR1 *) au1NextHop,
                                                  ISIS_MAX_IPV4_ADDR_LEN);
                        }
                        else
                        {
                            STRCPY (au1NextHop, "UNKNOWN");
                        }
                    }
                    else
                    {
                        STRCPY (au1NextHop, "LOCAL");
                    }
                    CliPrintf (CliHandle, "%-20s%-15u%-15s%-20s\n",
                               au1DestId,
                               pTrav->u4MetricVal,
                               ISIS_GET_METRIC_TYPE_STR (pContext, u1MetType),
                               au1NextHop);
                }
            }
        }
        pTrav = pTrav->pNext;
        if (pTrav == NULL)
        {
            pTrav = RBTreeGetNext (RBTree, pTravIn, NULL);
            pTravIn = pTrav;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliShowIPV6Routes                                      
 * Description   :  This function displays the isis routes 
 *                     
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx    - Instance Id
 *                  u1Level      - ISIS level                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *****************************************************************************/

INT4
IsisCliShowIPV6Routes (tCliHandle CliHandle, UINT1 u1Level, UINT4 u4InstIdx)
{
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pTravIn = NULL;
    tIsisSysContext    *pContext = NULL;
    CHAR                au1NextHop[(6 * ISIS_MAX_IPV6_ADDR_LEN) + 1];
    UINT1               au1IpV6Addr[ISIS_MAX_IPV6_ADDR_LEN];
    tRBTree             PrevRBTree;
    UINT1               u1MtSupport = ISIS_FALSE;
    UINT1               u1MetType = 0;
    UINT1               u1MtIndex = 0;

    MEMSET (au1IpV6Addr, 0x00, ISIS_MAX_IPV6_ADDR_LEN);

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    if ((u1Level != pContext->SysActuals.u1SysType)
        && (pContext->SysActuals.u1SysType != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if ((u1MtSupport = pContext->u1IsisMTSupport) == ISIS_TRUE)
    {
        u1MtIndex = ISIS_MT2_INDEX;
    }
    else
    {
        u1MtIndex = ISIS_ST_INDEX;
    }

    PrevRBTree =
        (u1Level ==
         ISIS_LEVEL1) ? pContext->SysL1Info.
        PrevShortPath[u1MtIndex] : pContext->SysL2Info.PrevShortPath[u1MtIndex];

    CliPrintf (CliHandle, "Dest ID             Metric         MetType        "
               "NextHop             \n");

    CliPrintf (CliHandle, "-------------------------------------------"
               "--------------------\n");

    pTravIn = pTrav = RBTreeGetFirst (PrevRBTree);

    while (pTrav != NULL)
    {
        if ((u1MtSupport == ISIS_TRUE) ||
            ((u1MtSupport == ISIS_FALSE) &&
             (pTrav->u1AddrType == ISIS_ADDR_IPV6)))
        {

            if (pTrav->au2Metric[0] != ISIS_SPT_INV_METRIC)
            {
                u1MetType = 0;
                if ((pTrav->au2Metric[0] & ISIS_SPT_SELF_IPRA_MASK)
                    == ISIS_NBR_IPRA_ADJ)
                {
                    tIsisAdjEntry      *pAdjRec = NULL;

                    pAdjRec =
                        IsisAdjGetAdjRecFromDirIdx (pContext,
                                                    pTrav->au4DirIdx[0]);
                    if (pAdjRec != NULL)
                    {

                        CliPrintf (CliHandle, "\r\n%-20s",
                                   Ip6PrintAddr ((tIp6Addr
                                                  *) (VOID *) (&(pTrav->
                                                                 au1DestID))));

                        CliPrintf (CliHandle, "%-15u%-15s%-20s\n",
                                   pTrav->u4MetricVal,
                                   ISIS_GET_METRIC_TYPE_STR (pContext,
                                                             u1MetType),
                                   Ip6PrintAddr ((tIp6Addr *) (VOID
                                                               *) (&(pAdjRec->
                                                                     AdjNbrIpV6Addr.
                                                                     au1IpAddr))));

                    }
                    else
                    {
                        STRCPY (au1NextHop, "UNKNOWN");
                    }
                }
                else if (((pTrav->au2Metric[0] & ISIS_SPT_SELF_IPRA_MASK)
                          == ISIS_SELF_IPRA_ADJ)
                         &&
                         (MEMCMP
                          (pTrav->au1Ipv6DirIdx, au1IpV6Addr,
                           ISIS_MAX_IPV6_ADDR_LEN) != 0))
                {
                    CliPrintf (CliHandle, "\r\n%-20s",
                               Ip6PrintAddr ((tIp6Addr
                                              *) (VOID *) (&(pTrav->
                                                             au1DestID))));
                    CliPrintf (CliHandle, "%-15u%-15s%-20s\n",
                               pTrav->u4MetricVal,
                               ISIS_GET_METRIC_TYPE_STR (pContext, u1MetType),
                               Ip6PrintAddr ((tIp6Addr *) (VOID
                                                           *) (&(pTrav->
                                                                 au1Ipv6DirIdx))));
                }
                else
                {
                    STRCPY (au1NextHop, "LOCAL");
                    CliPrintf (CliHandle, "\r\n%-20s",
                               Ip6PrintAddr ((tIp6Addr
                                              *) (VOID *) (&(pTrav->
                                                             au1DestID))));
                    CliPrintf (CliHandle, "%-15u%-15s%-20s\n",
                               pTrav->u4MetricVal,
                               ISIS_GET_METRIC_TYPE_STR (pContext, u1MetType),
                               au1NextHop);
                }
            }
        }
        pTrav = pTrav->pNext;
        if (pTrav == NULL)
        {
            pTrav = RBTreeGetNext (PrevRBTree, pTravIn, NULL);
            pTravIn = pTrav;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function Name : IsisCliFormIpAddrStr ()
 * Description   : This routine forms the IP Address string
 * Input (s)     :       
 *                  pu1Src       - Source address
 *                  pu1Dest      - Destination address                                
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ****************************************************************************/
VOID
IsisCliFormIpAddrStr (UINT1 *pu1Src, CHR1 * pu1Dest, UINT1 u1AddrLen)
{
    INT1                u1Cnt = 0;
    CHR1                u1TempStr[5];

    MEMSET (u1TempStr, 0, 5);

    pu1Dest[0] = '\0';
    for (u1Cnt = 0; u1Cnt < u1AddrLen; u1Cnt++)
    {
        SPRINTF (u1TempStr, "%d.", pu1Src[u1Cnt]);
        STRCAT (pu1Dest, u1TempStr);
    }
    pu1Dest[STRLEN (pu1Dest) - 1] = '\0';
}

/*******************************************************************************
 * Function Name :  IsisCliShowLSPDB                                      
 * Description   :  This function shows ISIS LSP Database  
 *                     
 * Input (s)     : CliHandle       - CliContext      
 *                  u4Inst         - Instance Id
 *                  u1level        - ISIS Level info
 *                  u4Detail       - Detailed info requirement state
 *                  u1lspid        - LSP id
 *                  i4NetDotComp - Seperator of Net id.                     
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/
INT4
IsisCliShowLSPDB (tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 u1level,
                  UINT4 u4Detail, UINT1 *u1lspid, INT4 i4NetDotComp)
{
    UINT1               au1LspId[ISIS_MAX_LSPID];
    UINT1               u1Level = 0;
    tIsisLSPEntry      *pLSP = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT1               bDetailed = (UINT1) u4Detail;
    UINT1               u1NetLen = 0;
    UINT2               u2HoldTime = 0;
    INT4                i4DotCompliance = ISIS_DOT_FALSE;
    INT4                i4RetVal = 0;

    MEMSET (au1LspId, 0, ISIS_MAX_LSPID);

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetFsIsisDotCompliance ((INT4) u4InstIdx, &i4DotCompliance);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Failure in Getting compliance\r\n");
        return CLI_FAILURE;
    }

    if (u1lspid != NULL && STRLEN (u1lspid) != 0)
    {
        tIsisLSPEntry      *pIsisLspEntry = NULL;
        tIsisHashBucket    *pHashBkt = NULL;
        UINT1               u1Location = ISIS_LOC_NONE;

        if (i4NetDotComp != i4DotCompliance)
        {
            CliPrintf (CliHandle, "%% Invalid NET ID format\r\n");
            return CLI_FAILURE;
        }

        if (i4DotCompliance == ISIS_DOT_TRUE)
        {
            u1NetLen = (UINT1) IsisNetOctetLen (u1lspid);
        }
        else
        {
            u1NetLen = (UINT1) OctetLen (u1lspid);
        }

        if (u1NetLen != ISIS_LSPID_LEN)
        {
            CLI_SET_ERR (CLI_ISIS_INV_LSP_ID);
            return CLI_FAILURE;
        }

        if (i4DotCompliance == ISIS_DOT_TRUE)
        {
            ISIS_CONVERT_DOT_NET_TO_ARRAY (u1lspid, au1LspId);
        }
        else
        {
            CLI_CONVERT_DOT_STR_TO_ARRAY (u1lspid, au1LspId);
        }
        if (u1level == ISIS_LEVEL12)
        {
            u1Level = ISIS_LEVEL1;
            pLSP = IsisUpdGetLSP (pContext, au1LspId, u1Level, &pIsisLspEntry,
                                  &pHashBkt, &u1Location);
            if (pLSP == NULL)
            {
                u1Level = ISIS_LEVEL2;
                pLSP =
                    IsisUpdGetLSP (pContext, au1LspId, u1Level, &pIsisLspEntry,
                                   &pHashBkt, &u1Location);
            }
        }
        else
        {
            pLSP = IsisUpdGetLSP (pContext, au1LspId, u1level, &pIsisLspEntry,
                                  &pHashBkt, &u1Location);
        }

        if (u1Location == ISIS_LOC_NONE)
        {
            CLI_SET_ERR (CLI_ISIS_LSP_NOT_FOUND);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\r%-25s%-15s%-15s%-15s%-10s\n\r", "LSP-ID",
                   "LSP Seq Num", "LSP Checksum", "LSP Hold time", "P/ATT/OL");

        while (pLSP != NULL)
        {
            if ((u1Location == ISIS_LOC_DB)
                || (u1Location == ISIS_LOC_DB_FIRST))
            {
                u2HoldTime = IsisCliGetHoldTime (pContext, pLSP);
                IsisCliPrintLSPInfo (CliHandle, pContext, pLSP, u2HoldTime,
                                     i4DotCompliance);
                if (bDetailed == TRUE)
                {
                    IsisCliPrintLSPTLVs (CliHandle, pLSP->pu1LSP,
                                         i4DotCompliance);
                }
                CliPrintf (CliHandle, "\n\n");
            }
            else
            {
                u2HoldTime =
                    IsisCliGetHoldTime (pContext,
                                        ((tIsisLSPTxEntry *) (pLSP))->pLSPRec);
                IsisCliPrintLSPInfo (CliHandle, pContext,
                                     ((tIsisLSPTxEntry *) (pLSP))->pLSPRec,
                                     u2HoldTime, i4DotCompliance);
                if (bDetailed == TRUE)
                {
                    IsisCliPrintLSPTLVs (CliHandle,
                                         ((tIsisLSPTxEntry *) (pLSP))->pLSPRec->
                                         pu1LSP, i4DotCompliance);
                }
                CliPrintf (CliHandle, "\n\n");
            }
            if ((u1level == ISIS_LEVEL12) && (u1Level == ISIS_LEVEL1))
            {
                u1Level = ISIS_LEVEL2;
                pLSP = IsisUpdGetLSP (pContext, au1LspId, u1Level,
                                      &pIsisLspEntry, &pHashBkt, &u1Location);
            }
            else
            {
                pLSP = NULL;
            }
        }
        return CLI_SUCCESS;
    }

    if ((u1level != ISIS_LEVEL2)
        && (pContext->SysActuals.u1SysType != ISIS_LEVEL2))
    {
        IsisCliShowL1DataBase (CliHandle, pContext, bDetailed, i4DotCompliance);
    }
    if ((u1level != ISIS_LEVEL1)
        && (pContext->SysActuals.u1SysType != ISIS_LEVEL1))
    {
        IsisCliShowL2DataBase (CliHandle, pContext, bDetailed, i4DotCompliance);
    }

    return CLI_SUCCESS;
}

#ifdef RM_WANTED
/*******************************************************************************
 * Function Name : IsisCliShowIsisSyncStatus
 * Description   : This function gets the ISIS synchronization status
 *
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    :  -
 * Returns       : CLI_SUCCESS/CLI_FAILURE.
 *******************************************************************************/
INT4
IsisCliShowIsisSyncStatus (tCliHandle CliHandle)
{
    INT4                i4RetValFsIsisStatus;

    if (nmhGetFsIsisStatus (&i4RetValFsIsisStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "Sync Status\r\n");
    if (i4RetValFsIsisStatus == ISIS_IS_UP)
    {
        CliPrintf (CliHandle, "ISIS Status - Enabled\r\n");
    }
    if (i4RetValFsIsisStatus == ISIS_SHUTDOWN)
    {
        CliPrintf (CliHandle, "ISIS Status - Disabled\r\n");
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliShowActualDB
 * Description   :  This function shows ISIS LSP Actual Database
 * 
 * Input (s)     : CliHandle       - CliContext
 *                  u4Inst         - Instance Id
 * Output (s)    : -
 * Returns       : CLI_SUCCESS/CLI_FAILURE.
 * 
 *******************************************************************************/
INT4
IsisCliShowActualDB (tCliHandle CliHandle, UINT4 u4InstIdx)
{
    tSNMP_OCTET_STRING_TYPE DisplayStruct;
    UINT1               au1DisplayString[ISIS_MAX_CONTEXT_STR_LEN];
    CHAR                acSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    tIsisSysContext    *pContext = NULL;
    INT4                i4CliCmn = 0;
    INT4                i4DotCompliance = ISIS_DOT_FALSE;
    INT4                i4RetVal = 0;

    MEMSET (acSysId, 0, (3 * ISIS_SYS_ID_LEN) + 1);
    MEMSET (au1DisplayString, 0, ISIS_MAX_CONTEXT_STR_LEN);
    MEMSET (&DisplayStruct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DisplayStruct.pu1_OctetList = au1DisplayString;

    CliPrintf (CliHandle, "\nISIS Actual Database\n");
    CliPrintf (CliHandle, "--------------------\n");
    if (SNMP_SUCCESS != nmhGetIsisSysType ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "SystemType              %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisProtocolVersion ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "ProtocolVersion         %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysActSysIDLen ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "ActualSystemIDLength    %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetFsIsisExtSysActMPS ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MaxPathSplits           %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysActMaxAA ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MaxAreaAddress          %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysAdminState ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "SystemAdminState        %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysAuthSupp ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "AuthSupport             %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysAreaAuthType ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "AreaAuthType            %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysDomainAuthType ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "DomainAuthType          %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysSetOverload ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "SetOverLoad             %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysL1State ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L1State                 %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysL2State ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L2State                 %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysExistState ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "SystemExistState        %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysL2toL1Leaking ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L2toL1Leaking           %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysMinSPFSchTime ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MinSPFSchedulingTime    %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysMaxSPFSchTime ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MaxSPFSchedulingTime    %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysMinLSPMark ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MinLSPMark              %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysMaxLSPMark ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MaxLSPMark              %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysMaxLSPGenInt ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MaxLSPGenerated         %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysMinL1LSPGenInt ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MinL1LSPGenInt          %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysMinL2LSPGenInt ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "SysMinL2LSPGenInt       %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysWaitTime ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "WaitTime                %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysOrigL1LSPBuffSize ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L1LSPBufferSize         %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysOrigL2LSPBuffSize ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L2LSPBuffersize         %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysMaxAreaCheck ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "Max AreaCheck           %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysL1MetricStyle ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L1MetricStyle           %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysL2MetricStyle ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L2MetricStyle           %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysL1SPFConsiders ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L1SPF                   %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysL2SPFConsiders ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L2SPF                   %d\n", i4CliCmn);
    if (SNMP_SUCCESS != nmhGetIsisSysMaxAge ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "MaximumAge              %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysReceiveLSPBufferSize ((INT4) u4InstIdx, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "BufferSize              %d\n", i4CliCmn);

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    i4RetVal =
        nmhGetFsIsisDotCompliance ((INT4) pContext->u4SysInstIdx,
                                   &i4DotCompliance);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Failure in Getting compliance\r\n");
        return CLI_FAILURE;
    }

    if (i4DotCompliance == ISIS_DOT_TRUE)
    {
        ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN, pContext->SysActuals.au1SysID,
                           acSysId);
    }
    else
    {
        ISIS_FORM_STR (ISIS_SYS_ID_LEN, pContext->SysActuals.au1SysID, acSysId);
    }
    CliPrintf (CliHandle, "ActualSysID             %17s\n", acSysId);

    MEMSET (au1DisplayString, 0, ISIS_MAX_CONTEXT_STR_LEN);
    MEMSET (&DisplayStruct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DisplayStruct.pu1_OctetList = au1DisplayString;
    DisplayStruct.i4_Length = 0;

    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysAreaTxPasswd ((INT4) u4InstIdx, &DisplayStruct))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle,
               "SysAreaTxPassword:      %s\nSysAreaTxPwLength:      %d\n",
               DisplayStruct.pu1_OctetList, DisplayStruct.i4_Length);

    MEMSET (au1DisplayString, 0, ISIS_MAX_CONTEXT_STR_LEN);
    MEMSET (&DisplayStruct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DisplayStruct.pu1_OctetList = au1DisplayString;
    DisplayStruct.i4_Length = 0;

    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysDomainRxPasswdExistState ((INT4) u4InstIdx,
                                                    &DisplayStruct, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "RxPasswd                %s\n",
               DisplayStruct.pu1_OctetList);

    MEMSET (au1DisplayString, 0, ISIS_MAX_CONTEXT_STR_LEN);
    MEMSET (&DisplayStruct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DisplayStruct.pu1_OctetList = au1DisplayString;
    DisplayStruct.i4_Length = 0;

    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysDomainTxPasswd ((INT4) u4InstIdx, &DisplayStruct))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle,
               "SysDomainPassword:     %s\nSysDomainPwLength:      %d\n",
               DisplayStruct.pu1_OctetList, DisplayStruct.i4_Length);

    MEMSET (au1DisplayString, 0, ISIS_MAX_CONTEXT_STR_LEN);
    MEMSET (&DisplayStruct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DisplayStruct.pu1_OctetList = au1DisplayString;
    DisplayStruct.i4_Length = 0;

    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysAreaRxPasswdExistState ((INT4) u4InstIdx,
                                                  &DisplayStruct, &i4CliCmn))
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "EntryExistingState:     %d\n", i4CliCmn);

    MEMSET (au1DisplayString, 0, ISIS_MAX_CONTEXT_STR_LEN);
    MEMSET (&DisplayStruct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DisplayStruct.pu1_OctetList = au1DisplayString;
    DisplayStruct.i4_Length = 0;

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return SNMP_FAILURE;
    }
    else
    {
        CliPrintf (CliHandle, "DelMetSupport           %d\n",
                   (INT4) (pContext->SysActuals.u1SysMetricSupp));
        CliPrintf (CliHandle, "BooleanLogChange:       %d\n",
                   pContext->SysActuals.bSysLSPIgnoreErr);
        CliPrintf (CliHandle, "SystemL1MetricStyle     %d\n",
                   (INT4) pContext->SysActuals.u1SysL1MetricStyle);
        if (pContext->SysActuals.u1SysTEEnabled == '\0')
        {
            CliPrintf (CliHandle, "TEEnabled Info:         %s\n", "No Entry");

        }
        else
        {
            CliPrintf (CliHandle, "TEEnabled Info:         %d\n",
                       pContext->SysActuals.u1SysTEEnabled);
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliShowConfDB
 * Description   :  This function shows ISIS configured Information
 *
 * Input (s)     : CliHandle       - CliContext
 *                  u4Inst         - Instance Id
 * Output (s)    : -
 * Returns       : CLI_SUCCESS/CLI_FAILURE.
 *
 *******************************************************************************/
INT4
IsisCliShowConfDB (tCliHandle CliHandle, UINT4 u4InstIdx)
{
    INT4                i4CliCmn = 0;
    tSNMP_OCTET_STRING_TYPE DisplayStruct;
    UINT1               au1DisplayString[ISIS_MAX_CONTEXT_STR_LEN];
    CHAR                acSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    tIsisSysContext    *pContext = NULL;
    INT4                i4DotCompliance = ISIS_DOT_FALSE;
    INT4                i4RetVal = 0;

    MEMSET (acSysId, 0, (3 * ISIS_SYS_ID_LEN) + 1);
    MEMSET (au1DisplayString, 0, ISIS_MAX_CONTEXT_STR_LEN);
    MEMSET (&DisplayStruct, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    DisplayStruct.pu1_OctetList = au1DisplayString;

    CliPrintf (CliHandle, "\nISIS Configured Database\n");
    CliPrintf (CliHandle, "------------------------\n");
    /* Hello rate has not been modified anywhere ,
     * hence ptinting the default Hello rate */
    CliPrintf (CliHandle, "Hello rate              %d\n",
               ISIS_DEF_ES_HELLORATE);

    /* L2 to L1 leaking is always False */
    CliPrintf (CliHandle, "L2 to L1 Leak           False\n");
    if (SNMP_SUCCESS != nmhGetIsisSysType ((INT4) u4InstIdx, &i4CliCmn))
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "SystemType              %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysMaxPathSplits ((INT4) u4InstIdx, &i4CliCmn))
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "ISIS Max Path Splits    %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysMaxAreaAddresses ((INT4) u4InstIdx, &i4CliCmn))
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "Max Area Address        %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetFsIsisExtSysDelMetSupp ((INT4) u4InstIdx, &i4CliCmn))
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "Delay Metric Support    %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysOrigL1LSPBuffSize ((INT4) u4InstIdx, &i4CliCmn))
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L1 LSP BufferSize       %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysOrigL2LSPBuffSize ((INT4) u4InstIdx, &i4CliCmn))
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L2 LSP Buffersize       %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysL1MetricStyle ((INT4) u4InstIdx, &i4CliCmn))
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L1MetricStyle           %d\n", i4CliCmn);
    if (SNMP_SUCCESS !=
        nmhGetIsisSysL2MetricStyle ((INT4) u4InstIdx, &i4CliCmn))
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "L2MetricStyle           %d\n", i4CliCmn);
    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    i4RetVal =
        nmhGetFsIsisDotCompliance ((INT4) pContext->u4SysInstIdx,
                                   &i4DotCompliance);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Failure in Getting compliance\r\n");
        return CLI_FAILURE;
    }

    if (i4DotCompliance == ISIS_DOT_TRUE)
    {
        ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN, pContext->SysConfigs.au1SysID,
                           acSysId);
    }
    else
    {
        ISIS_FORM_STR (ISIS_SYS_ID_LEN, pContext->SysConfigs.au1SysID, acSysId);
    }

    CliPrintf (CliHandle, "ConfigSystemID:         %14s\n", acSysId);

    return CLI_SUCCESS;
}

#endif
/*******************************************************************************
 * Function Name :  IsisCliShowL1DataBase                                      
 * Description   :  This function displays the L1 Database 
 * Input (s)     : CliHandle     - CliContext      
 *                  pContext       - Context info
 *                  bDetailed        - Detailed info enable/disable                                
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *                     
 ******************************************************************************/

VOID
IsisCliShowL1DataBase (tCliHandle CliHandle, tIsisSysContext * pContext,
                       UINT1 bDetailed, INT4 i4DotCompliance)
{

    tIsisLSPEntry      *pDbLSP = NULL;
    tIsisLSPEntry      *pCliLSP = NULL;
    tIsisLSPEntry      *pCliLSPHead = NULL;
    tIsisLSPTxEntry    *pTxLSP = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    UINT2               u2PduLen = 0;

    /*Getting the Head Node from Db */
    if (pContext->SysL1Info.LspDB.pHashBkts != NULL)
    {
        pHashBkt = pContext->SysL1Info.LspDB.pHashBkts;
        pDbLSP = pHashBkt->pFirstElem;
        if ((pDbLSP != NULL) && (pDbLSP->pu1LSP != NULL))
        {
            pCliLSP =
                (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                                  sizeof (tIsisLSPEntry));
            if (pCliLSP == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                return;
            }
            pCliLSPHead = pCliLSP;
            pCliLSP->u2HoldTime = IsisCliGetHoldTime (pContext, pDbLSP);
            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pDbLSP->pu1LSP, u2PduLen);
            pCliLSP->pu1LSP =
                (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2PduLen);
            if (pCliLSP->pu1LSP == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                IsisCliFreeLSPRecords (pCliLSPHead);
                return;
            }
            MEMCPY (pCliLSP->pu1LSP, pDbLSP->pu1LSP, u2PduLen);
        }
        else
        {
            pDbLSP = NULL;
        }
    }
    CliPrintf (CliHandle, "\n\rISIS : Level 1 LSP Database\n\r");
    CliPrintf (CliHandle, "\r%-25s%-15s%-15s%-15s%-10s\n\r\n", "LSP-ID",
               "LSP Seq Num", "LSP Checksum", "LSP Hold time", "P/ATT/OL");
    while (pDbLSP != NULL)
    {
        pDbLSP = IsisUpdGetNextDbRec (pDbLSP, &pHashBkt);
        if ((pDbLSP == NULL) || (pDbLSP->pu1LSP == NULL))
        {
            pCliLSP->pNext = NULL;
            break;
        }
        pCliLSP->pNext =
            (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                              sizeof (tIsisLSPEntry));
        if (pCliLSP->pNext == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
            IsisCliFreeLSPRecords (pCliLSPHead);
            return;
        }
        pCliLSP = pCliLSP->pNext;
        pCliLSP->u2HoldTime = IsisCliGetHoldTime (pContext, pDbLSP);

        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pDbLSP->pu1LSP, u2PduLen);
        pCliLSP->pu1LSP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2PduLen);
        if (pCliLSP->pu1LSP == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
            IsisCliFreeLSPRecords (pCliLSPHead);
            return;
        }
        MEMCPY (pCliLSP->pu1LSP, pDbLSP->pu1LSP, u2PduLen);
        pCliLSP->pNext = NULL;
    }
    /*Getting the Head Node from Tx */
    if (pContext->SysL1Info.LspTxQ.pHashBkts != NULL)
    {
        pHashBkt = pContext->SysL1Info.LspTxQ.pHashBkts;
        pTxLSP = pHashBkt->pFirstElem;
        if ((pTxLSP != NULL) && (pTxLSP->pLSPRec != NULL)
            && (pTxLSP->pLSPRec->pu1LSP != NULL))
        {
            if (pCliLSP != NULL)
            {
                pCliLSP->pNext =
                    (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                                      sizeof (tIsisLSPEntry));
                if (pCliLSP->pNext == NULL)
                {
                    PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                    IsisCliFreeLSPRecords (pCliLSPHead);
                    return;
                }
                pCliLSP = pCliLSP->pNext;
            }
            else
            {
                pCliLSP =
                    (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                                      sizeof (tIsisLSPEntry));
                if (pCliLSP == NULL)
                {
                    PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                    return;
                }
                pCliLSPHead = pCliLSP;
            }
            pCliLSP->u2HoldTime =
                IsisCliGetHoldTime (pContext, pTxLSP->pLSPRec);

            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pTxLSP->pLSPRec->pu1LSP, u2PduLen);
            pCliLSP->pu1LSP =
                (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2PduLen);
            if (pCliLSP->pu1LSP == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                IsisCliFreeLSPRecords (pCliLSPHead);
                return;
            }
            MEMCPY (pCliLSP->pu1LSP, pTxLSP->pLSPRec->pu1LSP, u2PduLen);
        }
        else
        {
            pTxLSP = NULL;
        }

    }
    while (pTxLSP != NULL)
    {
        pTxLSP = IsisUpdGetNextTxRec (pTxLSP, &pHashBkt);
        if ((pTxLSP == NULL) || (pTxLSP->pLSPRec == NULL)
            || (pTxLSP->pLSPRec->pu1LSP == NULL))
        {
            pCliLSP->pNext = NULL;
            break;
        }
        pCliLSP->pNext =
            (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                              sizeof (tIsisLSPEntry));
        if (pCliLSP->pNext == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
            IsisCliFreeLSPRecords (pCliLSPHead);
            return;
        }
        pCliLSP = pCliLSP->pNext;
        pCliLSP->u2HoldTime = IsisCliGetHoldTime (pContext, pTxLSP->pLSPRec);

        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pTxLSP->pLSPRec->pu1LSP, u2PduLen);
        pCliLSP->pu1LSP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2PduLen);
        if (pCliLSP->pu1LSP == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
            IsisCliFreeLSPRecords (pCliLSPHead);
            return;
        }
        MEMCPY (pCliLSP->pu1LSP, pTxLSP->pLSPRec->pu1LSP, u2PduLen);
        pCliLSP->pNext = NULL;
    }

    pCliLSP = pCliLSPHead;
    while (pCliLSP != NULL)
    {
        IsisCliPrintLSPInfo (CliHandle, pContext, pCliLSP, pCliLSP->u2HoldTime,
                             i4DotCompliance);
        if (bDetailed == TRUE)
        {
            IsisCliPrintLSPTLVs (CliHandle, pCliLSP->pu1LSP, i4DotCompliance);
        }
        pCliLSP = pCliLSP->pNext;
        CliPrintf (CliHandle, "\n\n");
    }
    IsisCliFreeLSPRecords (pCliLSPHead);
}

/*************************************************************************************/
UINT2
IsisCliGetHoldTime (tIsisSysContext * pContext, tIsisLSPEntry * pLSP)
{
    tIsisLSPEntry      *pTempLsp = NULL;
    UINT2               u2HoldTime = 0;
    UINT2               u2ETime = 0;

    pTempLsp = pLSP;
    ISIS_EXTRACT_RLT_FROM_LSP (pTempLsp->pu1LSP, u2HoldTime);

    if (pTempLsp->pPrevTimerLink != NULL)
    {
        /* Travel backward till the header is reached
         *  for LSPs other then self LSPs  */

        while (pTempLsp->pPrevTimerLink != NULL)
        {
            pTempLsp = pTempLsp->pPrevTimerLink;
        }
        u2ETime = (UINT2)
            ISIS_MAX ((ISIS_TIME_DIFF (pContext->SysTimers.TmrECInfo.u4TCount,
                                       ((tIsisTmrHdr *) pTempLsp)->u4TStamp)),
                      1);
        if (u2ETime > u2HoldTime)
        {
            /* This case arises only if the RLT is about to elapse and hence 
             * we can mark the RLT as '1'. This means the LSP will be processed 
             * shortly for Remaining Life Time Expiry during which time the 
             * proper processing of the LSP will take place
             *
             * NOTE: Should never happen in normal execution flow    */
            u2HoldTime = 0;
        }
        else
        {
            u2HoldTime -= u2ETime;
        }
#ifdef IS_FT_WANTED
        if (ISIS_EXT_IS_FT_STATE () == ISIS_FT_STANDBY)
        {
            u2HoldTime = 1200;
        }
#endif
    }
    return u2HoldTime;
}

/*************************************************************************************/

VOID
IsisCliFreeLSPRecords (tIsisLSPEntry * pHead)
{
    tIsisLSPEntry      *pCliLSP = NULL;
    tIsisLSPEntry      *pTempCliLSP = NULL;

    pCliLSP = pHead;
    while (pCliLSP != NULL)
    {
        if (pCliLSP->pu1LSP != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_LSPI, pCliLSP->pu1LSP);
        }
        pTempCliLSP = pCliLSP->pNext;
        pCliLSP->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pCliLSP);
        pCliLSP = pTempCliLSP;
    }
    return;
}

/*******************************************************************************
 * Function Name :  IsisCliShowL2DataBase                                      
 * Description   :  This function displays L2Database 
 *                     
 * Input (s)     : CliHandle     - CliContext      
 *                  pContext      - Context info
 *                  bDetailed     - Detailed info enable/disable                                
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliShowL2DataBase (tCliHandle CliHandle, tIsisSysContext * pContext,
                       UINT1 bDetailed, INT4 i4DotCompliance)
{

    tIsisLSPEntry      *pDbLSP = NULL;
    tIsisLSPEntry      *pCliLSP = NULL;
    tIsisLSPEntry      *pCliLSPHead = NULL;
    tIsisLSPTxEntry    *pTxLSP = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    UINT2               u2PduLen = 0;

    /*Getting the Head Node from Db */
    if (pContext->SysL2Info.LspDB.pHashBkts != NULL)
    {
        pHashBkt = pContext->SysL2Info.LspDB.pHashBkts;
        pDbLSP = pHashBkt->pFirstElem;
        if ((pDbLSP != NULL) && (pDbLSP->pu1LSP != NULL))
        {
            pCliLSP =
                (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                                  sizeof (tIsisLSPEntry));
            if (pCliLSP == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                return;
            }
            pCliLSPHead = pCliLSP;
            pCliLSP->u2HoldTime = IsisCliGetHoldTime (pContext, pDbLSP);
            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pDbLSP->pu1LSP, u2PduLen);
            pCliLSP->pu1LSP =
                (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2PduLen);
            if (pCliLSP->pu1LSP == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                IsisCliFreeLSPRecords (pCliLSPHead);
                return;
            }
            MEMCPY (pCliLSP->pu1LSP, pDbLSP->pu1LSP, u2PduLen);
        }
        else
        {
            pDbLSP = NULL;
        }

    }
    CliPrintf (CliHandle, "\n\rISIS : Level 2 LSP Database\n\r");
    CliPrintf (CliHandle, "\r%-25s%-15s%-15s%-15s%-10s\n\r\n", "LSP-ID",
               "LSP Seq Num", "LSP Checksum", "LSP Hold time", "P/ATT/OL");
    while (pDbLSP != NULL)
    {
        pDbLSP = IsisUpdGetNextDbRec (pDbLSP, &pHashBkt);
        if ((pDbLSP == NULL) || (pDbLSP->pu1LSP == NULL))
        {
            pCliLSP->pNext = NULL;
            break;
        }
        pCliLSP->pNext =
            (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                              sizeof (tIsisLSPEntry));
        if (pCliLSP->pNext == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
            IsisCliFreeLSPRecords (pCliLSPHead);
            return;
        }
        pCliLSP = pCliLSP->pNext;
        pCliLSP->u2HoldTime = IsisCliGetHoldTime (pContext, pDbLSP);

        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pDbLSP->pu1LSP, u2PduLen);
        pCliLSP->pu1LSP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2PduLen);
        if (pCliLSP->pu1LSP == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
            IsisCliFreeLSPRecords (pCliLSPHead);
            return;
        }
        MEMCPY (pCliLSP->pu1LSP, pDbLSP->pu1LSP, u2PduLen);
        pCliLSP->pNext = NULL;
    }
    /*Getting the Head Node from Tx */
    if (pContext->SysL2Info.LspTxQ.pHashBkts != NULL)
    {
        pHashBkt = pContext->SysL2Info.LspTxQ.pHashBkts;
        pTxLSP = pHashBkt->pFirstElem;
        if ((pTxLSP != NULL) && (pTxLSP->pLSPRec != NULL)
            && (pTxLSP->pLSPRec->pu1LSP != NULL))
        {
            if (pCliLSP != NULL)
            {
                pCliLSP->pNext =
                    (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                                      sizeof (tIsisLSPEntry));
                if (pCliLSP->pNext == NULL)
                {
                    PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                    IsisCliFreeLSPRecords (pCliLSPHead);
                    return;
                }
                pCliLSP = pCliLSP->pNext;
            }
            else
            {
                pCliLSP =
                    (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                                      sizeof (tIsisLSPEntry));
                if (pCliLSP == NULL)
                {
                    PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                    return;
                }
                pCliLSPHead = pCliLSP;
            }
            pCliLSP->u2HoldTime =
                IsisCliGetHoldTime (pContext, pTxLSP->pLSPRec);

            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pTxLSP->pLSPRec->pu1LSP, u2PduLen);
            pCliLSP->pu1LSP =
                (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2PduLen);
            if (pCliLSP->pu1LSP == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                IsisCliFreeLSPRecords (pCliLSPHead);
                return;
            }
            MEMCPY (pCliLSP->pu1LSP, pTxLSP->pLSPRec->pu1LSP, u2PduLen);
        }
        else
        {
            pTxLSP = NULL;
        }

    }
    while (pTxLSP != NULL)
    {
        pTxLSP = IsisUpdGetNextTxRec (pTxLSP, &pHashBkt);
        if ((pTxLSP == NULL) || (pTxLSP->pLSPRec == NULL)
            || (pTxLSP->pLSPRec->pu1LSP == NULL))
        {
            pCliLSP->pNext = NULL;
            break;
        }
        pCliLSP->pNext =
            (tIsisLSPEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LDBE,
                                              sizeof (tIsisLSPEntry));
        if (pCliLSP->pNext == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
            IsisCliFreeLSPRecords (pCliLSPHead);
            return;
        }
        pCliLSP = pCliLSP->pNext;
        pCliLSP->u2HoldTime = IsisCliGetHoldTime (pContext, pTxLSP->pLSPRec);

        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pTxLSP->pLSPRec->pu1LSP, u2PduLen);
        pCliLSP->pu1LSP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2PduLen);
        if (pCliLSP->pu1LSP == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
            IsisCliFreeLSPRecords (pCliLSPHead);
            return;
        }
        MEMCPY (pCliLSP->pu1LSP, pTxLSP->pLSPRec->pu1LSP, u2PduLen);
        pCliLSP->pNext = NULL;
    }

    pCliLSP = pCliLSPHead;
    while (pCliLSP != NULL)
    {
        IsisCliPrintLSPInfo (CliHandle, pContext, pCliLSP, pCliLSP->u2HoldTime,
                             i4DotCompliance);
        if (bDetailed == TRUE)
        {
            IsisCliPrintLSPTLVs (CliHandle, pCliLSP->pu1LSP, i4DotCompliance);
        }
        pCliLSP = pCliLSP->pNext;
        CliPrintf (CliHandle, "\n\n");
    }
    IsisCliFreeLSPRecords (pCliLSPHead);
}

/*******************************************************************************
 *
 * Function    : IsisDbgPrintLSPInfo ()
 * Description : This routine dumps the Link State PDU partially. It prints only
 *               the relevant fields viz. LSPID, RLT, PDU Len, Sequence Num.
 * Input (s)     : CliHandle     - CliContext      
 *                  pContext       - Context info
 *                  pLSP    - LSPentry info                                
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/

VOID
IsisCliPrintLSPInfo (tCliHandle CliHandle, tIsisSysContext * pContext,
                     tIsisLSPEntry * pLSP, UINT2 u2HoldTime,
                     INT4 i4DotCompliance)
{
    UINT1               u1Part = 0;
    UINT1               u1Att = 0;
    UINT1               u1OLoad = 0;
    UINT1              *pPDU = NULL;
    UINT1              *pu1BufOffset = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT1               au1HstNmeId[ISIS_HSTNMEWITHID_LEN];
    UINT1               au1HstNme[ISIS_HSTNME_MAX_LEN];
    UINT1               u1ApdFrgID = 0;
    UINT1               u1ApdNodeId = 0;

    pPDU = pLSP->pu1LSP;
    pu1BufOffset = ((UINT1 *) (pPDU)) + sizeof (tIsisComHdr);

    u1OLoad = (*(pPDU + ISIS_OFFSET_CHKSUM + 2) & ISIS_FLG_DBOL) ? 1 : 0;
    u1Att = (*(pPDU + ISIS_OFFSET_CHKSUM + 2) & ISIS_FLG_ATT_DEF) ? 1 : 0;
    u1Part = (*(pPDU + ISIS_OFFSET_CHKSUM + 2) & 0x80) ? 1 : 0;
    MEMSET (au1HstNme, 0, ISIS_HSTNME_MAX_LEN);
    MEMSET (au1HstNmeId, 0, ISIS_HSTNMEWITHID_LEN);

    /*Fetch Host Name */
    ISIS_HST_FRMSYSID ((pu1BufOffset + 4), au1HstNme, pContext);
    if (STRLEN (au1HstNme) <= 0)
    {
        if (i4DotCompliance == ISIS_DOT_TRUE)
        {
            ISIS_FORM_DOT_STR (ISIS_LSPID_LEN, (pu1BufOffset + 4), acLSPId);
        }
        else
        {
            ISIS_FORM_STR (ISIS_LSPID_LEN, (pu1BufOffset + 4), acLSPId);
        }

        CliPrintf (CliHandle, "\r%-25s%-15.8x%-15.4x%-15.4u%1u/%1u/%1u",
                   acLSPId,
                   (UINT4)
                   OSIX_NTOHL (*(UINT4 *) (VOID *) (pPDU + ISIS_OFFSET_SEQNO)),
                   (UINT2)
                   OSIX_NTOHS (*(UINT2 *) (VOID *) (pPDU + ISIS_OFFSET_CHKSUM)),
                   u2HoldTime, u1Part, u1Att, u1OLoad);
    }
    else
    {
        u1ApdFrgID = 1;
        u1ApdNodeId = 1;
        STRNCPY (au1HstNmeId, au1HstNme, ISIS_HSTNME_DISPLAY);
        ISIS_FORM_HSTNMEID (au1HstNmeId, (pPDU + ISIS_OFFSET_LSPID),
                            u1ApdFrgID, u1ApdNodeId);
        CliPrintf (CliHandle, "\r%-25s%-15.8x%-15.4x%-15.4u%1u/%1u/%1u",
                   au1HstNmeId,
                   (UINT4)
                   OSIX_NTOHL (*(UINT4 *) (VOID *) (pPDU + ISIS_OFFSET_SEQNO)),
                   (UINT2)
                   OSIX_NTOHS (*(UINT2 *) (VOID *) (pPDU + ISIS_OFFSET_CHKSUM)),
                   u2HoldTime, u1Part, u1Att, u1OLoad);
    }
}

/*******************************************************************************
 * Function Name :  IsisCliPrintLSPTLVs                                      
 * Description   :  This function displays the  Isis LSP TLV  
 * Input (s)     : CliHandle     - CliContext      
 *                  pPDU       - packet info
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliPrintLSPTLVs (tCliHandle CliHandle, UINT1 *pPDU, INT4 i4DotCompliance)
{
    UINT1              *pu1Buf = NULL;
    tIsisComHdr        *pComHdr = NULL;
    UINT1              *pu1String = NULL;
    UINT1              *pu1Ptr = NULL;
    CHR1                acArr[5 * (ISIS_SYS_ID_LEN + 1) + 1] = { 0 };
    UINT1               au1Tmp[ISIS_MAX_IPV4_ADDR_LEN] = { 0 };
    UINT4               u4DefMetric = 0;
    UINT2               u2PduLen = 0;
    UINT2               u2Offset = 0;
    UINT2               u2MtId = 0;
    UINT1               u1Code = 0;
    UINT1               u1AddrLen = 0;
    UINT1               u1Length = 0;
    UINT1               u1Prefix = 0;
    UINT1               u1OLoad = 0;
    UINT1               u1Att = 0;
    UINT1               u1LSPFlag = 0;
    UINT1               u1Flag = 0;
    UINT1               u1IsSubTlv = 0;
    UINT1               u1SubTlvLen = 0;

    pComHdr = (tIsisComHdr *) pPDU;
    u2Offset = pComHdr->u1HdrLen;
    pu1Buf = (pPDU) + u2Offset;
    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pPDU, u2PduLen);

    while (u2Offset < u2PduLen)
    {

        u1Code = *pu1Buf;
        u1Length = *(pu1Buf + 1);

        u2Offset += 2;
        pu1Buf += 2;

        switch (u1Code)
        {
            case ISIS_PROT_SUPPORT_TLV:

                CliPrintf (CliHandle, "\n%-20s:", "NLPID");
                while (u1Length)
                {
                    CliPrintf (CliHandle, " 0x%-2x", *pu1Buf);
                    pu1Buf++;
                    u1Length--;
                    u2Offset++;
                }
                break;
            case ISIS_IPV4IF_ADDR_TLV:

                while (u1Length)
                {
                    MEMSET (acArr, 0, sizeof (acArr));
                    IsisCliFormIpAddrStr (pu1Buf, acArr,
                                          ISIS_MAX_IPV4_ADDR_LEN);
                    CliPrintf (CliHandle, "\n\r%-20s: %-12s",
                               "Interface IP Addr", acArr);
                    pu1Buf += ISIS_IPV4_ADDR_LEN;
                    u1Length -= ISIS_IPV4_ADDR_LEN;
                    u2Offset += ISIS_IPV4_ADDR_LEN;
                }
                break;
            case ISIS_IPV6IF_ADDR_TLV:

                while (u1Length)
                {
                    MEMSET (acArr, 0, sizeof (acArr));
                    MEMCPY (acArr, pu1Buf, ISIS_MAX_IPV6_ADDR_LEN);
                    CliPrintf (CliHandle, "\n\r%-20s: %-12s",
                               "Interface IPV6 Addr",
                               Ip6PrintAddr ((tIp6Addr *) (VOID *) acArr));
                    pu1Buf += ISIS_IPV6_ADDR_LEN;
                    u1Length -= ISIS_IPV6_ADDR_LEN;
                    u2Offset += ISIS_IPV6_ADDR_LEN;
                }
                break;

            case ISIS_IP_INTERNAL_RA_TLV:
            case ISIS_IP_EXTERNAL_RA_TLV:

                while (u1Length)
                {
                    MEMSET (acArr, 0, sizeof (acArr));
                    IsisUtlComputePrefixLen ((UINT1 *) (pu1Buf + 8),
                                             ISIS_MAX_IPV4_ADDR_LEN, &u1Prefix);
                    IsisCliFormIpAddrStr ((pu1Buf + 4), acArr,
                                          ISIS_MAX_IPV4_ADDR_LEN);
                    CliPrintf (CliHandle, "\n\r%-20s: %s/%u   %12s:%4u",
                               "IP Reachability", acArr, u1Prefix,
                               "Def. Metric", *pu1Buf);
                    pu1Buf += ISIS_IPRA_TLV_LEN;
                    u1Length -= ISIS_IPRA_TLV_LEN;
                    u2Offset += ISIS_IPRA_TLV_LEN;
                }
                break;

            case ISIS_IPV6_RA_TLV:

                while (u1Length)
                {
                    u4DefMetric = 0;
                    MEMSET (acArr, 0, sizeof (acArr));
                    MEMCPY (&u4DefMetric, pu1Buf, ISIS_EXT_DEF_METRIC_LEN);
                    u4DefMetric = OSIX_HTONL (u4DefMetric);
                    /* Default metric + 1 byte reserved */
                    pu1Buf += (ISIS_EXT_DEF_METRIC_LEN + 1);

                    u1Prefix = *pu1Buf;
                    pu1Buf += 1;
                    MEMCPY (acArr, pu1Buf, ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));
                    pu1Buf += ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix);

                    CliPrintf (CliHandle, "\n\r%-20s: %s/%u   %12s:%4u",
                               "IPV6 Reachability",
                               Ip6PrintAddr ((tIp6Addr *) (VOID *) acArr),
                               u1Prefix, "Def. Metric", u4DefMetric);
                    u1Length -=
                        (ISIS_EXT_DEF_METRIC_LEN + 1 + 1 +
                         ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));
                    u2Offset +=
                        (ISIS_EXT_DEF_METRIC_LEN + 1 + 1 +
                         ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));
                }
                break;

            case ISIS_AREA_ADDR_TLV:

                while (u1Length)
                {
                    MEMSET (acArr, 0, sizeof (acArr));
                    u1AddrLen = *pu1Buf++;
                    u2Offset++;
                    u1Length--;
                    if (i4DotCompliance == ISIS_DOT_TRUE)
                    {
                        IsisDisplayAreaInDot (u1AddrLen, pu1Buf, acArr);
                    }
                    else
                    {
                        ISIS_FORM_STR (u1AddrLen, pu1Buf, acArr);
                    }
                    CliPrintf (CliHandle, "\n\r%-20s: %-12s", "Area Address",
                               acArr);
                    pu1Buf += u1AddrLen;
                    u1Length -= u1AddrLen;
                    u2Offset += u1AddrLen;
                }
                break;

            case ISIS_MT_TLV:

                while (u1Length)
                {
                    CliPrintf (CliHandle, "\n%-20s:", "Topology");
                    MEMCPY (&u2MtId, pu1Buf, ISIS_MT_ID_LEN);
                    u2MtId = OSIX_HTONS (u2MtId);

                    ISIS_GET_1_BYTE (pu1Buf, 0, u1LSPFlag);
                    u1OLoad = (u1LSPFlag & ISIS_MT_LSP_OFLAG_MASK) ? 1 : 0;
                    u1Att = (u1LSPFlag & ISIS_MT_LSP_ATT_MASK) ? 1 : 0;

                    u2MtId = (u2MtId & ISIS_MTID_MASK);

                    pu1Buf += ISIS_MT_ID_LEN;
                    u2Offset += ISIS_MT_ID_LEN;
                    u1Length -= ISIS_MT_ID_LEN;
                    if (u2MtId == ISIS_IPV4_UNICAST_MT_ID)
                    {
                        pu1String = (UINT1 *) " IPv4 (0x0)";
                    }
                    else if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                    {
                        pu1String = (UINT1 *) " IPv6 (0x2)";
                    }
                    CliPrintf (CliHandle, "%s%39s/%u/%u", pu1String, "-", u1Att,
                               u1OLoad);
                }
                break;

            case ISIS_IS_ADJ_TLV:

                pu1Buf++;        /* Ignore the Virtual Flag */
                u2Offset++;
                u1Length--;
                while (u1Length)
                {
                    MEMSET (acArr, 0, sizeof (acArr));
                    if (i4DotCompliance == ISIS_DOT_TRUE)
                    {
                        ISIS_FORM_DOT_STR ((ISIS_SYS_ID_LEN +
                                            ISIS_PNODE_ID_LEN), (pu1Buf + 4),
                                           acArr);
                    }
                    else
                    {
                        ISIS_FORM_STR ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN),
                                       (pu1Buf + 4), acArr);
                    }
                    CliPrintf (CliHandle, "\n\r%-20s: %-20s  %12s:%4u",
                               "IS Adjacency", acArr, "Def. Metric",
                               (*pu1Buf & 0x7F));
                    pu1Buf += ISIS_IS_ADJ_TLV_LEN;
                    u1Length -= ISIS_IS_ADJ_TLV_LEN;
                    u2Offset += ISIS_IS_ADJ_TLV_LEN;
                }
                break;

            case ISIS_EXT_IS_REACH_TLV:

                while (u1Length)
                {
                    u1SubTlvLen = 0;
                    u4DefMetric = 0;
                    MEMSET (acArr, 0, sizeof (acArr));
                    if (i4DotCompliance == ISIS_DOT_TRUE)
                    {
                        ISIS_FORM_DOT_STR ((ISIS_SYS_ID_LEN +
                                            ISIS_PNODE_ID_LEN), pu1Buf, acArr);
                    }
                    else
                    {
                        ISIS_FORM_STR ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN),
                                       pu1Buf, acArr);
                    }
                    pu1Buf += (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                    pu1Ptr = (UINT1 *) &u4DefMetric;
                    MEMCPY ((pu1Ptr + 1), pu1Buf, ISIS_IS_DEF_METRIC_LEN);
                    u4DefMetric = OSIX_HTONL (u4DefMetric);
                    /* 3 bytes Metric + 1 byte sub tlv length */
                    pu1Buf += ISIS_IS_DEF_METRIC_LEN;

                    CliPrintf (CliHandle, "\n\r%-20s: %-s %-7s: %u %s",
                               "IS Adjacency", acArr, "Metric", u4DefMetric,
                               "(Ext-IS)");
                    u2Offset +=
                        (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN +
                         ISIS_IS_DEF_METRIC_LEN);
                    u1Length -=
                        (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN +
                         ISIS_IS_DEF_METRIC_LEN);
                    u1SubTlvLen = *pu1Buf;
                    u2Offset += (1 + u1SubTlvLen);
                    u1Length -= (1 + u1SubTlvLen);
                    pu1Buf += (1 + u1SubTlvLen);
                }

                break;

            case ISIS_MT_IS_REACH_TLV:

                MEMCPY (&u2MtId, pu1Buf, ISIS_MT_ID_LEN);
                u2MtId = OSIX_HTONS (u2MtId);
                u2MtId = (u2MtId & ISIS_MTID_MASK);
                pu1Buf += ISIS_MT_ID_LEN;
                u2Offset += ISIS_MT_ID_LEN;
                u1Length -= ISIS_MT_ID_LEN;

                if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                {
                    pu1String = (UINT1 *) "IPV6 (MT-IS)";
                }

                while (u1Length)
                {
                    u1SubTlvLen = 0;
                    u4DefMetric = 0;
                    MEMSET (acArr, 0, sizeof (acArr));
                    if (i4DotCompliance == ISIS_DOT_TRUE)
                    {
                        ISIS_FORM_DOT_STR ((ISIS_SYS_ID_LEN +
                                            ISIS_PNODE_ID_LEN), pu1Buf, acArr);
                    }
                    else
                    {
                        ISIS_FORM_STR ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN),
                                       pu1Buf, acArr);
                    }
                    pu1Buf += (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                    pu1Ptr = (UINT1 *) &u4DefMetric;
                    MEMCPY ((pu1Ptr + 1), pu1Buf, ISIS_IS_DEF_METRIC_LEN);
                    u4DefMetric = OSIX_HTONL (u4DefMetric);
                    /* 3 bytes Metric + 1 byte sub tlv length */
                    pu1Buf += ISIS_IS_DEF_METRIC_LEN;

                    CliPrintf (CliHandle, "\n\r%-20s: %-s %-7s: %u %s",
                               "IS Adjacency", acArr, "Metric", u4DefMetric,
                               pu1String);
                    u2Offset +=
                        (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN +
                         ISIS_IS_DEF_METRIC_LEN);
                    u1Length -=
                        (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN +
                         ISIS_IS_DEF_METRIC_LEN);
                    u1SubTlvLen = *pu1Buf;
                    u2Offset += (1 + u1SubTlvLen);
                    u1Length -= (1 + u1SubTlvLen);
                    pu1Buf += (1 + u1SubTlvLen);
                }

                break;

            case ISIS_EXT_IP_REACH_TLV:
                while (u1Length)
                {
                    u1SubTlvLen = 0;
                    u1Flag = 0;
                    u1IsSubTlv = 0;
                    u4DefMetric = 0;
                    MEMSET (acArr, 0, sizeof (acArr));
                    MEMSET (au1Tmp, 0, sizeof (au1Tmp));

                    MEMCPY (&u4DefMetric, pu1Buf, ISIS_EXT_DEF_METRIC_LEN);
                    u4DefMetric = OSIX_HTONL (u4DefMetric);
                    pu1Buf += ISIS_EXT_DEF_METRIC_LEN;

                    u1Flag = *pu1Buf;
                    u1IsSubTlv = (u1Flag & ISIS_EXTN_SUBTLV_MASK);
                    u1Prefix = (u1Flag & 0x3F);
                    pu1Buf += 1;

                    MEMCPY (au1Tmp, pu1Buf,
                            ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));

                    IsisCliFormIpAddrStr (au1Tmp, acArr,
                                          ISIS_MAX_IPV4_ADDR_LEN);
                    pu1Buf += ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix);

                    CliPrintf (CliHandle, "\n\r%-20s: %s/%u   %8s:%4u %s",
                               "IP Reachability", acArr, u1Prefix, "Metric",
                               u4DefMetric, "(Ext-IP)");

                    u1Length -=
                        (ISIS_EXT_DEF_METRIC_LEN + 1 +
                         ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));
                    u2Offset +=
                        (ISIS_EXT_DEF_METRIC_LEN + 1 +
                         ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));
                    if (u1IsSubTlv)
                    {
                        u1SubTlvLen = *pu1Buf;
                        u2Offset += (1 + u1SubTlvLen);
                        u1Length -= (1 + u1SubTlvLen);
                        pu1Buf += (1 + u1SubTlvLen);
                    }
                }

                break;

            case ISIS_MT_IPV6_REACH_TLV:

                MEMCPY (&u2MtId, pu1Buf, ISIS_MT_ID_LEN);
                u2MtId = OSIX_HTONS (u2MtId);
                u2MtId = (u2MtId & ISIS_MTID_MASK);
                pu1Buf += ISIS_MT_ID_LEN;
                u2Offset += ISIS_MT_ID_LEN;
                u1Length -= ISIS_MT_ID_LEN;

                if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                {
                    pu1String = (UINT1 *) "IPV6 (MT-IPv6)";
                }

                while (u1Length)
                {
                    u1SubTlvLen = 0;
                    u1Flag = 0;
                    u1IsSubTlv = 0;
                    u4DefMetric = 0;
                    MEMSET (acArr, 0, sizeof (acArr));
                    MEMCPY (&u4DefMetric, pu1Buf, ISIS_EXT_DEF_METRIC_LEN);
                    u4DefMetric = OSIX_HTONL (u4DefMetric);
                    /* Default metric */
                    pu1Buf += (ISIS_EXT_DEF_METRIC_LEN);

                    u1Flag = *pu1Buf;
                    u1IsSubTlv = (u1Flag & ISIS_MT_IPV6_SUBTLV_MASK);
                    pu1Buf += 1;
                    u1Prefix = *pu1Buf;
                    pu1Buf += 1;
                    MEMCPY (acArr, pu1Buf, ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));
                    pu1Buf += ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix);

                    CliPrintf (CliHandle, "\n\r%-20s: %s/%u   %12s:%4u %s",
                               "IPV6 Reachability",
                               Ip6PrintAddr ((tIp6Addr *) (VOID *) acArr),
                               u1Prefix, "Metric", u4DefMetric, pu1String);
                    u1Length -=
                        (ISIS_EXT_DEF_METRIC_LEN + 1 + 1 +
                         ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));
                    u2Offset +=
                        (ISIS_EXT_DEF_METRIC_LEN + 1 + 1 +
                         ISIS_ROUNDOFF_PREFIX_LEN (u1Prefix));
                    if (u1IsSubTlv)
                    {
                        u1SubTlvLen = *pu1Buf;
                        u2Offset += (1 + u1SubTlvLen);
                        u1Length -= (1 + u1SubTlvLen);
                        pu1Buf += (1 + u1SubTlvLen);
                    }
                }

                break;
            case ISIS_DYN_HOSTNME_TLV:

                CliPrintf (CliHandle, "\n%-20s: ", "Hostname");

                while (u1Length)
                {
                    CliPrintf (CliHandle, "%c", *pu1Buf);
                    pu1Buf++;
                    u1Length--;
                    u2Offset++;
                }
                break;
            default:
                pu1Buf += u1Length;
                u2Offset += u1Length;
                break;
        }
    }
}

/*******************************************************************************
 * Function Name :  IsisCliShowCktInfo                                      
 * Description   :  This function how the statistics on the isis packets.
 * Input (s)     : CliHandle     - CliContext      
 *                  i4IfIndex    - Interface Index                                
 *                  u1Level       - ISIS level info
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *                    
 ******************************************************************************/
INT4
IsisCliShowCktInfo (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pPrintCktRec = NULL;
    tIsisCktLevel      *pCktLvl = NULL;

    pi1InterfaceName = &ai1InterfaceName[0];
    if (i4IfIndex != 0)
    {
        i4RetVal =
            IsisAdjGetCktRecWithIfIdx ((UINT4) i4IfIndex, 0, &pPrintCktRec);
        if (i4RetVal != ISIS_SUCCESS)
        {
            CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
            return CLI_FAILURE;
        }

        if ((u1Level != pPrintCktRec->u1CktLevel)
            && (pPrintCktRec->u1CktLevel != ISIS_LEVEL12))
        {
            CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
            return CLI_FAILURE;
        }
        pContext = pPrintCktRec->pContext;
        if (u1Level == ISIS_LEVEL1)
        {
            pCktLvl = pPrintCktRec->pL1CktInfo;
        }
        else
        {
            pCktLvl = pPrintCktRec->pL2CktInfo;
        }
        if (pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC)
        {
            CliPrintf (CliHandle, "\n\rMetric    : %u\r", pCktLvl->Metric[0]);
        }
        else
        {
            CliPrintf (CliHandle, "\n\rMetric    : %u\r",
                       pCktLvl->u4FullMetric);
        }
        CliPrintf (CliHandle, "\n\rPriority  : %u\r", pCktLvl->u1ISPriority);
        CliPrintf (CliHandle, "\n\rHello Interval  : %u\r",
                   pCktLvl->HelloTimeInt);
        CliPrintf (CliHandle, "\n\rHello Multiplier  : %u\r",
                   pCktLvl->u2HelloMultiplier);
        CliPrintf (CliHandle, "\n\rLSP-Throttle Interval  : %u\r",
                   pCktLvl->LspThrottleInt);
        CliPrintf (CliHandle, "\n\rLSP-Retransmit Interval  : %u\r",
                   pCktLvl->u4MinLspReTxInt);
        CliPrintf (CliHandle, "\n\rCSNP Interval  : %u\n\r",
                   pCktLvl->CSNPInterval);
    }
    else
    {
        for (pContext = gpIsisInstances; pContext != NULL;
             pContext = pContext->pNext)
        {
            pPrintCktRec = pContext->CktTable.pCktRec;

            while (pPrintCktRec != NULL)

            {
                if ((u1Level != pPrintCktRec->u1CktLevel)
                    && (pPrintCktRec->u1CktLevel != ISIS_LEVEL12))
                {
                    pPrintCktRec = pPrintCktRec->pNext;
                    continue;
                }
                CfaCliGetIfName (pPrintCktRec->u4CktIfIdx, pi1InterfaceName);
                CliPrintf (CliHandle,
                           "\n\r Statistics Information  on Interface %s\n",
                           pi1InterfaceName);
                CliPrintf (CliHandle,
                           "\r--------------------------------------------\n\r");
                if (u1Level == ISIS_LEVEL1)
                {
                    pCktLvl = pPrintCktRec->pL1CktInfo;
                }
                else
                {
                    pCktLvl = pPrintCktRec->pL2CktInfo;
                }
                if (pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC)
                {
                    CliPrintf (CliHandle, "\n\rMetric    : %u\r",
                               pCktLvl->Metric[0]);
                }
                else
                {
                    CliPrintf (CliHandle, "\n\rMetric    : %u\r",
                               pCktLvl->u4FullMetric);
                }
                CliPrintf (CliHandle, "\n\rPriority  : %u\r",
                           pCktLvl->u1ISPriority);
                CliPrintf (CliHandle, "\n\rHello Interval  : %u\r",
                           pCktLvl->HelloTimeInt);
                CliPrintf (CliHandle, "\n\rHello Multiplier  : %u\r",
                           pCktLvl->u2HelloMultiplier);
                CliPrintf (CliHandle, "\n\rLSP-Throttle Interval  : %u\r",
                           pCktLvl->LspThrottleInt);
                CliPrintf (CliHandle, "\n\rLSP-Retransmit Interval  : %u\r",
                           pCktLvl->u4MinLspReTxInt);
                CliPrintf (CliHandle, "\n\rCSNP Interval  : %u\n\r",
                           pCktLvl->CSNPInterval);

                pPrintCktRec = pPrintCktRec->pNext;
            }
        }                        /* End of for Loop */
    }
    CliPrintf (CliHandle, "\n");
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliShowPktStats                                      
 * Description   :  This function Validates and  sets the Isis Configuration 
 *                  Parameters   
 * Input (s)     :  pu1IsisInput - Contains the Configuration parameters    
 *                                 be set.                                  
 * Output (s)    :  ppu1Output  - Formated output message.                 
 *                                                                         
 * Returns       :  None.                                                  
 ******************************************************************************/
INT4
IsisCliShowPktStats (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisCktEntry      *pCktRec = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktRec);
    if (i4RetVal != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        CliPrintf (CliHandle, "\r\nISIS is not enabled on this "
                   "interface\r\n");
        return CLI_FAILURE;
    }

    if (pCktRec->pL1CktInfo != NULL)
    {
        IsisCliShowPktCount (CliHandle, pCktRec->pL1CktInfo, ISIS_LEVEL1,
                             pCktRec);
    }
    if (pCktRec->pL2CktInfo != NULL)
    {
        IsisCliShowPktCount (CliHandle, pCktRec->pL2CktInfo, ISIS_LEVEL2,
                             pCktRec);
    }
    return CLI_FAILURE;
}

/*******************************************************************************
 * Function Name :  IsisCliShowPktCount                                      
 * Description   :  This function shows the Received and sent packet count for both Level-1 and Level2  
 * Input (s)     : CliHandle     - CliContext      
 *                  tIsisCktLevel  -  circuit level info
 *                  u1Level    - ISIS level info                                
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 *                  
 ******************************************************************************/
VOID
IsisCliShowPktCount (tCliHandle CliHandle, tIsisCktLevel * pCktLevel,
                     UINT1 u1Level, tIsisCktEntry * pCktRec)
{
    if (u1Level == ISIS_LEVEL1)
    {
        CliPrintf (CliHandle, "\n\rReceived Packet Statistics for LEVEL1\n\r");
    }
    else
    {
        CliPrintf (CliHandle, "\n\rReceived Packet Statistics for LEVEL2\n\r");
    }

    if (pCktRec->u1CktType == ISIS_P2P_CKT)
    {
        CliPrintf (CliHandle, "\n\r%20s:%20u", "ISH PDUs",
                   pCktRec->CktStats.u4RxISHPDUs);
    }

    CliPrintf (CliHandle,
               "\n\r%20s:%20lu\n\r%20s:%20lu\n\r%20s:%20lu\n\r%20s:%20lu",
               "Hello PDUs",
               pCktLevel->RcvdPktStats.u4HelloPDUs,
               "Link State PDUs",
               pCktLevel->RcvdPktStats.u4LinkStatePDUs,
               "Complete SeqNo PDUs",
               pCktLevel->RcvdPktStats.u4CSNPDUs,
               "Partial SeqNo PDU", pCktLevel->RcvdPktStats.u4PSNPDUs);

    if (u1Level == ISIS_LEVEL1)
    {
        CliPrintf (CliHandle, "\n\rSent Packet Statistics for LEVEL1\n\r");
    }
    else
    {
        CliPrintf (CliHandle, "\n\rSent Packet Statistics for LEVEL2\n\r");
    }

    if (pCktRec->u1CktType == ISIS_P2P_CKT)
    {
        CliPrintf (CliHandle, "\n\r%20s:%20u", "ISH PDUs",
                   pCktRec->CktStats.u4TxISHPDUs);
    }

    CliPrintf (CliHandle,
               "\n\r%20s:%20lu\n\r%20s:%20lu\n\r%20s:%20lu\n\r%20s:%20lu",
               "Hello PDUs",
               pCktLevel->SentPktStats.u4HelloPDUs,
               "Link State PDUs",
               pCktLevel->SentPktStats.u4LinkStatePDUs,
               "Complete SeqNo PDUs",
               pCktLevel->SentPktStats.u4CSNPDUs,
               "Partial SeqNo PDU", pCktLevel->SentPktStats.u4PSNPDUs);
    CliPrintf (CliHandle, "\n\r");
}

/*******************************************************************************
 * Function Name :  IsisCliDebugAdjn                                      
 * Description   :  This function sets the Isis debug adjacent Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/

VOID
IsisCliDebugAdjn (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_ADJ_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_ADJ_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_ADJ_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_ADJ_MODULE, ISIS_TD_MASK);

}

/*******************************************************************************
 * Function Name :  IsisCliDebugDecn                                      
 * Description   :  This function sets the debug decision Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/

VOID
IsisCliDebugDecn (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_DEC_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DEC_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DEC_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DEC_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugUpdt                                      
 * Description   :  This function sets the Isis update debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugUpdt (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_UPD_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_UPD_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_UPD_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_UPD_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugInterface                                      
 * Description   :  This function sets the Isis interface debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugInterface (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_INT_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_INT_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_INT_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_INT_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugControl                                      
 * Description   :  This function sets the Isis control debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugControl (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_CTL_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_CTL_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_CTL_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_CTL_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugTimer                                      
 * Description   :  This function sets the Isis timer debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugTimer (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_TMR_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_TMR_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_TMR_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_TMR_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugFault                                      
 * Description   :  This function sets the Isis timer debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugFault (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_FLT_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_FLT_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_FLT_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_FLT_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugRtmi                                      
 * Description   :  This function sets the Isis Rtmi debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugRtmi (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_RTM_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_RTM_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_RTM_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_RTM_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugDlli                                     
 * Description   :  This function sets the Isis Dlli debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugDlli (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_DLL_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DLL_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DLL_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DLL_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugTrfr                                     
 * Description   :  This function sets the Isis Trfr  debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugTrfr (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_TRF_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_TRF_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_TRF_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_TRF_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugSnmp                                     
 * Description   :  This function sets the Isis Trfr  debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugSnmp (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_NMG_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_NMG_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_NMG_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_NMG_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugUtil                                     
 * Description   :  This function sets the Isis Util  debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -.                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugUtil (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_UTL_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_UTL_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_UTL_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_UTL_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name : IsisCliDebugCritical
 * Description   :  This function sets the Isis Util  debug Configuration                           
 *                  Parameters
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : -
 * Returns       : -.                 
 *
 ******************************************************************************/
VOID
IsisCliDebugCritical (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_CR_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_CR_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_CR_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_CR_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name : IsisCliDebugEntryExit
 * Description   :  This function sets the Isis Entry exit debug Configuration                           
 *                  Parameters
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : -
 * Returns       : -.                 
 *
 ******************************************************************************/
VOID
IsisCliDebugEntryExit (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);

    nmhSetFsIsisExtLogLevel (ISIS_EE_MODULE, ISIS_EE_MASK);
}

/*******************************************************************************
 * Function Name : IsisCliDebugPacketDump
 * Description   :  This function sets the Isis Packet Dump  debug Configuration                           
 *                  Parameters
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : -
 * Returns       : -.                 
 *
 ******************************************************************************/
VOID
IsisCliDebugPacketDump (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_PDMP_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_PDMP_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_PDMP_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_PDMP_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name : IsisCliDebugDetail
 * Description   : This function sets the Isis Detail Debug Configuration
 *                  Parameters
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : -
 * Returns       : -.
 *
 ******************************************************************************/
VOID
IsisCliDebugDetail (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_DT_MODULE, ISIS_PT_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DT_MODULE, ISIS_PI_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DT_MODULE, ISIS_PD_MASK);
    nmhSetFsIsisExtLogLevel (ISIS_DT_MODULE, ISIS_TD_MASK);
}

/*******************************************************************************
 * Function Name :  IsisCliDebugAll                                      
 * Description   :  This function  sets all the Isis Debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliDebugAll (tCliHandle CliHandle)
{
    IsisCliDebugDecn (CliHandle);
    IsisCliDebugUpdt (CliHandle);
    IsisCliDebugAdjn (CliHandle);
    IsisCliDebugTimer (CliHandle);
    IsisCliDebugInterface (CliHandle);
    IsisCliDebugControl (CliHandle);
    IsisCliDebugFault (CliHandle);
    IsisCliDebugRtmi (CliHandle);
    IsisCliDebugDlli (CliHandle);
    IsisCliDebugTrfr (CliHandle);
    IsisCliDebugSnmp (CliHandle);
    IsisCliDebugUtil (CliHandle);
    IsisCliDebugCritical (CliHandle);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugAdjn                                      
 * Description   :  This function resets the debug adjacent Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugAdjn (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_ADJ_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugDecn                                      
 * Description   :  This function resets the debug decision Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 *
 ******************************************************************************/
VOID
IsisCliNoDebugDecn (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_DEC_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugUpdt                                      
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugUpdt (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_UPD_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugControl                                      
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugControl (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_CTL_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugInterface                                     
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugInterface (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_INT_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugTimer                                     
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugTimer (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_TMR_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugFault                                     
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugFault (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_FLT_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugRtmi                                    
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugRtmi (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_RTM_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugDlli                                    
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugDlli (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_DLL_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugTrfr                                     
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugTrfr (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_TRF_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugSnmp                                    
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugSnmp (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_NMG_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugUtil                                   
 * Description   :  This function Resets the Debug update Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugUtil (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_UTL_MODULE, 0);
}

/*******************************************************************************
 * Function Name : IsisCliNoDebugCritical
 * Description   : This function Resets the Debug update Configuration
 *                 Parameters
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : - 
 * Returns       : -
 *******************************************************************************/
VOID
IsisCliNoDebugCritical (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_CR_MODULE, 0);
}

/*******************************************************************************
 * Function Name : IsisCliNoDebugEntryExit
 * Description   : This function Resets the Debug EntryExit Configuration
 *                 Parameters
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : - 
 * Returns       : -
 *******************************************************************************/

VOID
IsisCliNoDebugEntryExit (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_EE_MODULE, 0);
}

/*******************************************************************************
 * Function Name : IsisCliNoPacketDump
 * Description   : This function Resets the Debug Packet Dump Configuration
 *                 Parameters
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : - 
 * Returns       : -
 *******************************************************************************/
VOID
IsisCliNoDebugPacketDump (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_PDMP_MODULE, 0);
}

/*******************************************************************************
 * Function Name : IsisCliNoDebugDetail
 * Description   : This function Resets the Detail Debug Configuration
 *                 Parameters
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : -
 * Returns       : -
 *******************************************************************************/
VOID
IsisCliNoDebugDetail (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_DT_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliNoDebugAll                                      
 * Description   :  This function resets all the Isis Debug Configuration 
 *                  Parameters   
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliNoDebugAll (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    nmhSetFsIsisExtLogLevel (ISIS_DEC_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_UPD_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_ADJ_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_TMR_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_INT_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_CTL_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_FLT_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_RTM_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_DLL_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_TRF_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_NMG_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_UTL_MODULE, 0);
    nmhSetFsIsisExtLogLevel (ISIS_CR_MODULE, 0);
}

/*******************************************************************************
 * Function Name :  IsisCliShowDbgInfo                                      
 * Description   :  This function displays the debug information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsisCliShowDbgInfo (tCliHandle CliHandle)
{
    INT4                u1AdjMsk = 0;
    INT4                u1UpdMsk = 0;
    INT4                u1DecMsk = 0;
    INT4                u1IntMsk = 0;
    INT4                u1CtlMsk = 0;
    INT4                u1TmrMsk = 0;
    INT4                u1FltMsk = 0;
    INT4                u1RtmMsk = 0;
    INT4                u1DllMsk = 0;
    INT4                u1TrfMsk = 0;
    INT4                u1NmgMsk = 0;
    INT4                u1UtlMsk = 0;
    INT4                u1PdmpMsk = 0;
    INT4                u1EnExMsk = 0;
    INT4                u1DetlMsk = 0;

    nmhGetFsIsisExtLogLevel (ISIS_ADJ_MODULE, &u1AdjMsk);
    nmhGetFsIsisExtLogLevel (ISIS_UPD_MODULE, &u1UpdMsk);
    nmhGetFsIsisExtLogLevel (ISIS_DEC_MODULE, &u1DecMsk);
    nmhGetFsIsisExtLogLevel (ISIS_INT_MODULE, &u1IntMsk);
    nmhGetFsIsisExtLogLevel (ISIS_CTL_MODULE, &u1CtlMsk);
    nmhGetFsIsisExtLogLevel (ISIS_TMR_MODULE, &u1TmrMsk);
    nmhGetFsIsisExtLogLevel (ISIS_FLT_MODULE, &u1FltMsk);
    nmhGetFsIsisExtLogLevel (ISIS_RTM_MODULE, &u1RtmMsk);
    nmhGetFsIsisExtLogLevel (ISIS_DLL_MODULE, &u1DllMsk);
    nmhGetFsIsisExtLogLevel (ISIS_TRF_MODULE, &u1TrfMsk);
    nmhGetFsIsisExtLogLevel (ISIS_NMG_MODULE, &u1NmgMsk);
    nmhGetFsIsisExtLogLevel (ISIS_UTL_MODULE, &u1UtlMsk);
    nmhGetFsIsisExtLogLevel (ISIS_EE_MODULE, &u1EnExMsk);
    nmhGetFsIsisExtLogLevel (ISIS_PDMP_MODULE, &u1PdmpMsk);
    nmhGetFsIsisExtLogLevel (ISIS_DT_MODULE, &u1DetlMsk);
    if ((u1AdjMsk == 0) && (u1UpdMsk == 0) && (u1DecMsk == 0) &&
        (u1IntMsk == 0) && (u1CtlMsk == 0) && (u1TmrMsk == 0) &&
        (u1FltMsk == 0) && (u1RtmMsk == 0) && (u1DllMsk == 0) &&
        (u1TrfMsk == 0) && (u1NmgMsk == 0) && (u1UtlMsk == 0) &&
        (u1EnExMsk == 0) && (u1PdmpMsk == 0) && (u1DetlMsk == 0))
    {
        return;
    }
    else
    {
        CliPrintf (CliHandle, "\rISIS :");
        if (u1AdjMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS adjacency related debugging is on");
        }
        if (u1UpdMsk != 0)
        {
            CliPrintf (CliHandle, "\r\n  ISIS update related debugging is on");
        }
        if (u1DecMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS SPF decisions related debugging is on");
        }
        if (u1IntMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS interface related debugging is on");
        }
        if (u1CtlMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS control and event related debugging is on");
        }
        if (u1TmrMsk != 0)
        {
            CliPrintf (CliHandle, "\r\n  ISIS timer related debugging is on");
        }
        if (u1FltMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS fault tolerance related debugging is on");
        }
        if (u1RtmMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS rtmi interface related debugging is on");
        }
        if (u1DllMsk != 0)
        {
            CliPrintf (CliHandle, "\r\n  ISIS dlli related debugging is on");
        }
        if (u1TrfMsk != 0)
        {
            CliPrintf (CliHandle, "\r\n  ISIS trfr related debugging is on");
        }
        if (u1NmgMsk != 0)
        {
            CliPrintf (CliHandle, "\r\n  ISIS snmp related debugging is on");
        }
        if (u1UtlMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS general utility related debugging is on");
        }
        if (u1EnExMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS function entry exit related debugging is on");
        }

        if (u1PdmpMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS packet dump related debugging is on");
        }
        if (u1DetlMsk != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n  ISIS detailed debug related debugging is on");
        }

        CliPrintf (CliHandle, "\r\n");
    }
}

/*******************************************************************************
 * Function Name :  IsIsShowRunningConfigInCxt                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
INT4
IsIsShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Module, INT4 i4Inst)
{

    INT4                i4GlobalAdmin = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (u4Module);

    pContext = gpIsisInstances;
    CliRegisterLock (CliHandle, IsisLock, IsisUnlock);
    if (pContext == NULL)
    {
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    while (pContext != NULL)
    {
        if (pContext->u4SysInstIdx == (UINT4) i4Inst)
        {
            if (nmhGetIsisSysAdminState
                ((INT4) pContext->u4SysInstIdx, &i4GlobalAdmin) == SNMP_FAILURE)
            {
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }
            break;
        }
        pContext = pContext->pNext;
    }

    if (pContext == NULL)
    {
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;

    }

    if (i4GlobalAdmin == ISIS_STATE_ON)
    {
        IsIsShowRunningConfigScalarsInCxt (CliHandle, i4Inst);
        IsIsShowRunningConfigTablesInCxt (CliHandle, i4Inst);

        for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
             pCktRec = pCktRec->pNext)
        {
            IsIsShowRunningConfigInterfaceInCxt (CliHandle,
                                                 (INT4) pCktRec->u4CktIfIdx);
        }
        /* Display Passive interface details */
        i4RetVal = IsisIsAllinterfacePassive (pContext);

        if (i4RetVal == ISIS_TRUE)
        {
            IsisRouterIsisInfo (CliHandle, i4Inst);
            FilePrintf (CliHandle, "\rpassive-interface default\n");
            CliPrintf (CliHandle, "\rpassive-interface default\n");
            CliPrintf (CliHandle, "!\r\n");
        }
        else
        {
            IsisRouterIsisInfo (CliHandle, i4Inst);
            IsIsCktTablePassiveInterfaceInfo (CliHandle, pContext);
            CliPrintf (CliHandle, "!\r\n");
        }

    }
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsIsShowRunningConfigScalarsInCxt                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsShowRunningConfigScalarsInCxt (tCliHandle CliHandle, INT4 i4Instances)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Instances);
}

/*******************************************************************************
 * Function Name :  IsIsShowRunningConfigTablesInCxt                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsShowRunningConfigTablesInCxt (tCliHandle CliHandle, INT4 i4Inst)
{
    INT4                i4Instance = 0;
    INT4                i4PrevInstance = 0;
    INT1                i1OutCome = 0;
    INT4                i4IsisIPRAType = 0;
    INT4                i4PrevIsisIPRAType = 0;
    INT4                i4PrevIsisIPRAIndex = 0;
    INT4                i4IsisIPRAIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4IsisSummAddressType = 0;
    UINT4               u4IsisSummAddrPrefixLen = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4PrevIsisSummAddressType = 0;
    UINT4               u4PrevIsisSummAddrPrefixLen = 0;
    UINT4               u4Metric = 0;
    INT4                i4AdminState = 0;
    CHR1               *pu1String = NULL;
    INT1                ai1IpMask[ISIS_PRINT_BUF_SIZE];

    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrPrev;

    UINT1               au1AreaAddress[ISIS_AREA_ADDR_LEN];
    UINT1               au1PrevAreaAddress[ISIS_AREA_ADDR_LEN];

    UINT1               au1SummAddress[ISIS_MAX_IP_ADDR_LEN];
    UINT1               au1PrevSummAddress[ISIS_MAX_IP_ADDR_LEN];
    UINT1               au1IsisCxtName[ISIS_MAX_CONTEXT_STR_LEN];

    INT4                i4RetVal = 0;
    INT4                i4ResType = 0;
    tIsisSysContext    *pContext = NULL;

    MEMSET (ai1IpMask, 0, ISIS_PRINT_BUF_SIZE);
    /*System Info Table */
    i1OutCome = nmhGetFirstIndexIsisSysTable (&i4Instance);

    /* Get Restoration Type - whether CSR or MSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4Inst == i4Instance)
        {
            if (i4Inst == ISIS_DEFAULT_CXT_ID)
            {
                CliPrintf (CliHandle, "!\r\n");
                FilePrintf (CliHandle, "!\r\n");
                CliPrintf (CliHandle, "router isis \r\n");
                FilePrintf (CliHandle, "router isis \r\n");
                FilePrintf (CliHandle, "\r\n");
            }
            else
            {
                /*Getting The Context Name for corrosponding Context Id */
                UtilIsisGetVcmAliasName ((UINT4) i4Inst, au1IsisCxtName);
                CliPrintf (CliHandle, "!\r\n");
                FilePrintf (CliHandle, "!\r\n");
                CliPrintf (CliHandle, "\rrouter isis vrf  %s \n",
                           au1IsisCxtName);
                FilePrintf (CliHandle, "\rrouter isis vrf  %s \n",
                            au1IsisCxtName);
                FilePrintf (CliHandle, "\r\n");
            }
        }

        /* Get next context */
        i4PrevInstance = i4Instance;
        i1OutCome = nmhGetNextIndexIsisSysTable (i4PrevInstance, &i4Instance);
    }
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4Inst, &pContext);
    if (i4RetVal != ISIS_FAILURE)
    {
        if (pContext->u1MetricStyle != ISIS_STYLE_NARROW_METRIC)
        {
            CliPrintf (CliHandle, "\rmetric-style wide\n");
            FilePrintf (CliHandle, "\rmetric-style wide\n");
        }

        if (pContext->u1IsisMTSupport == ISIS_TRUE)
        {
            CliPrintf (CliHandle, "\rmulti-topology\n");
            FilePrintf (CliHandle, "multi-topology\n");
        }

#ifdef BFD_WANTED
        if (pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE)
        {
            CliPrintf (CliHandle, "\renable bfd\n");
            FilePrintf (CliHandle, "\renable bfd\n");
        }

        if (pContext->u1IsisBfdAllIfStatus == ISIS_BFD_ENABLE)
        {
            CliPrintf (CliHandle, "\rbfd all-interfaces\n");
            FilePrintf (CliHandle, "\rbfd all-interfaces\n");
        }
#endif
        if (pContext->SysActuals.u1SysAreaAuthType == ISIS_AUTH_MD5)
        {
            CliPrintf (CliHandle, "\rauthentication mode md5 level-1\n");
            FilePrintf (CliHandle, "\rauthentication mode md5 level-1\n");
            FilePrintf (CliHandle, "\r\n");
        }
        if (pContext->SysActuals.u1SysDomainAuthType == ISIS_AUTH_MD5)
        {
            CliPrintf (CliHandle, "\rauthentication mode md5 level-2\n");
            FilePrintf (CliHandle, "\rauthentication mode md5 level-2\n");
            FilePrintf (CliHandle, "\r\n");
        }

        if ((i4ResType == SET_FLAG) && (MsrGetSaveStatus () == ISS_TRUE))
        {
            if (pContext->SysActuals.SysAreaTxPasswd.au1Password[0] != '\0')
            {
                FilePrintf (CliHandle, "area-password  %s \n",
                            pContext->SysActuals.SysAreaTxPasswd.au1Password);
                CliPrintf (CliHandle, "\rarea-password %s \n",
                           pContext->SysActuals.SysAreaTxPasswd.au1Password);
                FilePrintf (CliHandle, "\r\n");
            }
            if ((pContext->SysActuals.SysDomainTxPasswd.au1Password)[0] != '\0')
            {
                FilePrintf (CliHandle, "domain-password  %s \n",
                            pContext->SysActuals.SysDomainTxPasswd.au1Password);
                CliPrintf (CliHandle, "\rdomain-password  %s \n",
                           pContext->SysActuals.SysDomainTxPasswd.au1Password);
                FilePrintf (CliHandle, "\r\n");
            }
        }
        else
        {
            if (pContext->SysActuals.SysAreaTxPasswd.au1Password[0] != '\0')
            {
                FilePrintf (CliHandle, "area-password  %s \n",
                            pContext->SysActuals.SysAreaTxPasswd.au1Password);
                CliPrintf (CliHandle, "\rarea-password  ****** \n");
                FilePrintf (CliHandle, "\r\n");
            }
            if ((pContext->SysActuals.SysDomainTxPasswd.au1Password)[0] != '\0')
            {
                FilePrintf (CliHandle, "domain-password  %s \n",
                            pContext->SysActuals.SysDomainTxPasswd.au1Password);
                CliPrintf (CliHandle, "\rdomain-password  ****** \n");
                FilePrintf (CliHandle, "\r\n");
            }
        }
        if (pContext->u1IsisDynHostNmeSupport == ISIS_DYNHOSTNME_ENABLE)
        {
            CliPrintf (CliHandle, "\rhostname dynamic\n");
            FilePrintf (CliHandle, "\rhostname dynamic\n");
        }

    }

    i1OutCome = SNMP_SUCCESS;
    i4Instance = 0;
    i4PrevInstance = 0;

    OctetStr.pu1_OctetList = au1AreaAddress;
    OctetStrPrev.pu1_OctetList = au1PrevAreaAddress;

    /*Area Table */
    i1OutCome = nmhGetFirstIndexIsisManAreaAddrTable (&i4Instance, &OctetStr);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4Inst == i4Instance)
        {
            IsIsAreaTableInfo (CliHandle, i4Instance, &OctetStr,
                               &u4PagingStatus, ISIS_FALSE);

            if (u4PagingStatus == CLI_FAILURE)
                break;
        }
        i4PrevInstance = i4Instance;
        OctetStrPrev.i4_Length = OctetStr.i4_Length;
        MEMCPY (OctetStrPrev.pu1_OctetList, OctetStr.pu1_OctetList,
                OctetStrPrev.i4_Length);

        i1OutCome =
            nmhGetNextIndexIsisManAreaAddrTable (i4PrevInstance, &i4Instance,
                                                 &OctetStrPrev, &OctetStr);
    }

    /* Printing System Table info after area table info */
    i4Instance = 0;
    i4PrevInstance = 0;
    i1OutCome = nmhGetFirstIndexIsisSysTable (&i4Instance);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (i4Inst == i4Instance)
        {
            IsIsSysTableInfo (CliHandle, i4Instance, &u4PagingStatus);
            break;
        }
        /* Get next context */
        i4PrevInstance = i4Instance;
        i1OutCome = nmhGetNextIndexIsisSysTable (i4PrevInstance, &i4Instance);
    }

    /* IPRA Table */
    i1OutCome = SNMP_SUCCESS;
    i4Instance = 0;
    i4PrevInstance = 0;

    i1OutCome = nmhGetFirstIndexIsisIPRATable (&i4Instance, &i4IsisIPRAType,
                                               &i4IsisIPRAIndex);

    while (i1OutCome != SNMP_FAILURE)
    {

        if (i4IsisIPRAType != ISIS_AUTO_TYPE)
        {
            if (i4Inst == i4Instance)
            {

                IsIsIpraTableInfo (CliHandle, i4Instance, i4IsisIPRAType,
                                   i4IsisIPRAIndex, &u4PagingStatus);

                IsIsIpraMetricTableInfo (CliHandle, i4Instance, i4IsisIPRAType,
                                         i4IsisIPRAIndex, &u4PagingStatus);

                if (u4PagingStatus == CLI_FAILURE)
                    break;
            }
        }
        i4PrevInstance = i4Instance;
        i4PrevIsisIPRAType = i4IsisIPRAType;
        i4PrevIsisIPRAIndex = i4IsisIPRAIndex;

        i1OutCome = nmhGetNextIndexIsisIPRATable (i4PrevInstance,
                                                  &i4Instance,
                                                  i4PrevIsisIPRAType,
                                                  &i4IsisIPRAType,
                                                  i4PrevIsisIPRAIndex,
                                                  &i4IsisIPRAIndex);
    }

    /*Summary Table */

    i1OutCome = SNMP_SUCCESS;
    i4Instance = 0;
    i4PrevInstance = 0;

    OctetStr.pu1_OctetList = au1SummAddress;
    OctetStrPrev.pu1_OctetList = au1PrevSummAddress;
    i1OutCome =
        nmhGetFirstIndexIsisSummAddrTable (&i4Instance, &i4IsisSummAddressType,
                                           &OctetStr, &u4IsisSummAddrPrefixLen);

    while (i1OutCome != SNMP_FAILURE)
    {
        i4AdminState = 0;

        if (ISIS_IPV4 == i4IsisSummAddressType)
        {
            MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, OctetStr.i4_Length);
            CLI_CONVERT_IPADDR_TO_STR (pu1String, OSIX_NTOHL (u4IpAddr));
            SPRINTF ((CHR1 *) ai1IpMask, "%s",
                     (CONST CHR1 *) IsIsPrintIpMask ((UINT1)
                                                     u4IsisSummAddrPrefixLen));
        }

        if (i4Inst == i4Instance)
        {

            nmhGetIsisSummAddrAdminState (i4Instance, i4IsisSummAddressType,
                                          &OctetStr, u4IsisSummAddrPrefixLen,
                                          &i4AdminState);
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                nmhGetFsIsisExtSummAddrFullMetric (i4Instance,
                                                   i4IsisSummAddressType,
                                                   &OctetStr,
                                                   u4IsisSummAddrPrefixLen,
                                                   &u4Metric);
            }
            else
            {
                nmhGetIsisSummAddrMetric (i4Instance, i4IsisSummAddressType,
                                          &OctetStr, u4IsisSummAddrPrefixLen,
                                          (INT4 *) &u4Metric);
            }

            if (i4AdminState == ISIS_LEVEL1)
            {
                if (i4IsisSummAddressType == ISIS_IPV6)
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle, "summary-address %s %d level-1 ",
                                   Ip6PrintAddr ((tIp6Addr *)
                                                 (VOID *) OctetStr.
                                                 pu1_OctetList),
                                   u4IsisSummAddrPrefixLen);

                    FilePrintf (CliHandle, "summary-address %s %d level-1 ",
                                Ip6PrintAddr ((tIp6Addr *)
                                              (VOID *) OctetStr.pu1_OctetList),
                                u4IsisSummAddrPrefixLen);
                }
                else
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle, "summary-address %s %s level-1 ",
                                   pu1String, ai1IpMask);
                    FilePrintf (CliHandle, "summary-address %s %s level-1 ",
                                pu1String, ai1IpMask);
                }

            }
            else if (i4AdminState == ISIS_LEVEL2)
            {
                if (i4IsisSummAddressType == ISIS_IPV6)
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle,
                                   "summary-address %s %d level-2-only  ",
                                   Ip6PrintAddr ((tIp6Addr *)
                                                 (VOID *) OctetStr.
                                                 pu1_OctetList),
                                   u4IsisSummAddrPrefixLen);

                    FilePrintf (CliHandle,
                                "summary-address %s %d level-2-only ",
                                Ip6PrintAddr ((tIp6Addr *)
                                              (VOID *) OctetStr.pu1_OctetList),
                                u4IsisSummAddrPrefixLen);
                }
                else
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle,
                                   "summary-address %s %s level-2-only ",
                                   pu1String, ai1IpMask);
                    FilePrintf (CliHandle,
                                "summary-address %s %s level-2-only ",
                                pu1String, ai1IpMask);
                }
            }
            else
            {
                if (i4IsisSummAddressType == ISIS_IPV6)
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle,
                                   "summary-address %s %d level1-2 ",
                                   Ip6PrintAddr ((tIp6Addr *)
                                                 (VOID *) OctetStr.
                                                 pu1_OctetList),
                                   u4IsisSummAddrPrefixLen);

                    FilePrintf (CliHandle,
                                "summary-address %s %d level1-2 ",
                                Ip6PrintAddr ((tIp6Addr *)
                                              (VOID *) OctetStr.pu1_OctetList),
                                u4IsisSummAddrPrefixLen);
                }
                else
                {
                    u4PagingStatus =
                        CliPrintf (CliHandle, "summary-address %s %s level1-2 ",
                                   pu1String, ai1IpMask);
                    FilePrintf (CliHandle, "summary-address %s %s level1-2 ",
                                pu1String, ai1IpMask);
                }
            }
            if (u4Metric != ISIS_IPRA_DEF_METRIC_VALUE)
            {
                u4PagingStatus =
                    CliPrintf (CliHandle, "default-metric %u \r\n", u4Metric);
                FilePrintf (CliHandle, "default-metric %u \r\n", u4Metric);
            }
            else
            {
                u4PagingStatus = CliPrintf (CliHandle, "\r\n");
                FilePrintf (CliHandle, "\r\n");
            }
            if (u4PagingStatus == CLI_FAILURE)
                break;
        }

        i4PrevInstance = i4Instance;
        i4PrevIsisSummAddressType = i4IsisSummAddressType;
        u4PrevIsisSummAddrPrefixLen = u4IsisSummAddrPrefixLen;
        OctetStrPrev.i4_Length = OctetStr.i4_Length;
        MEMCPY (OctetStrPrev.pu1_OctetList, OctetStr.pu1_OctetList,
                OctetStrPrev.i4_Length);

        i1OutCome = nmhGetNextIndexIsisSummAddrTable (i4PrevInstance,
                                                      &i4Instance,
                                                      i4PrevIsisSummAddressType,
                                                      &i4IsisSummAddressType,
                                                      &OctetStrPrev,
                                                      &OctetStr,
                                                      u4PrevIsisSummAddrPrefixLen,
                                                      &u4IsisSummAddrPrefixLen);

    }

#ifdef ROUTEMAP_WANTED
    /* RMap Filtering Table */
    IsIsRMapFilterTableInfo (CliHandle);
#endif /* ROUTEMAP_WANTED */

    /* Distance Table */
    IsIsDistanceTableInfo (CliHandle);

    CliPrintf (CliHandle, "!\r\n");
    FilePrintf (CliHandle, "!\r\n");
    return;

}

/*******************************************************************************
 * Function Name :  IsIsRRDImportLevelInfo                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsRRDImportLevelInfo (tCliHandle CliHandle, INT4 i4RrdImportType,
                        INT4 i4ProtoIndex, INT4 i4Instance)
{
    tSNMP_OCTET_STRING_TYPE tVal;
    UINT1               au1Temp[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4RrdMetricVal = 0;

    tVal.pu1_OctetList = au1Temp;
    MEMSET (au1Temp, 0, RMAP_MAX_NAME_LEN + 4);

    nmhGetFsIsisRRDRouteMapName (i4Instance, &tVal);
    if (i4RrdImportType == ISIS_LEVEL12)
    {
        CliPrintf (CliHandle, "level-1-2 ");
        FilePrintf (CliHandle, "level-1-2 ");
    }
    else if (i4RrdImportType == ISIS_LEVEL1)
    {
        CliPrintf (CliHandle, "level-1 ");
        FilePrintf (CliHandle, "level-1 ");
    }
    nmhGetFsIsisRRDMetricValue (i4Instance, i4ProtoIndex, &u4RrdMetricVal);
    if (tVal.i4_Length != 0)
    {
        tVal.pu1_OctetList[tVal.i4_Length] = 0;
        CliPrintf (CliHandle, "route-map %s ", tVal.pu1_OctetList);
        FilePrintf (CliHandle, "route-map %s ", tVal.pu1_OctetList);
    }
    if (u4RrdMetricVal != 0)
    {
        CliPrintf (CliHandle, "metric %u ", u4RrdMetricVal);
        FilePrintf (CliHandle, "metric %u ", u4RrdMetricVal);
    }
    CliPrintf (CliHandle, "\r\n");
    FilePrintf (CliHandle, "\n");
}

/*******************************************************************************
 * Function Name :  IsIsRedistributionInfo                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsRedistributionInfo (tCliHandle CliHandle, INT4 i4Instance)
{
    INT4                i4ScalarObject = 0;
    INT4                i4RrdImportType = 0;

    nmhGetFsIsisRRDProtoMaskForEnable (i4Instance, &i4ScalarObject);

    nmhGetFsIsisRRDImportLevel (i4Instance, &i4RrdImportType);
    if (i4ScalarObject & ISIS_IMPORT_STATIC)
    {
        CliPrintf (CliHandle, "redistribute static ");
        FilePrintf (CliHandle, "redistribute static ");
        IsIsRRDImportLevelInfo (CliHandle, i4RrdImportType,
                                ISIS_METRIC_INDEX_STATIC, i4Instance);
    }
    if (i4ScalarObject & ISIS_IMPORT_DIRECT)
    {
        CliPrintf (CliHandle, "redistribute connected ");
        FilePrintf (CliHandle, "redistribute connected ");
        IsIsRRDImportLevelInfo (CliHandle, i4RrdImportType,
                                ISIS_METRIC_INDEX_DIRECT, i4Instance);
    }
    if (i4ScalarObject & ISIS_IMPORT_RIP)
    {
        CliPrintf (CliHandle, "redistribute rip ");
        FilePrintf (CliHandle, "redistribute rip ");
        IsIsRRDImportLevelInfo (CliHandle, i4RrdImportType,
                                ISIS_METRIC_INDEX_RIP, i4Instance);
    }
    if (i4ScalarObject & ISIS_IMPORT_OSPF)
    {
        CliPrintf (CliHandle, "redistribute ospf ");
        FilePrintf (CliHandle, "redistribute ospf ");
        IsIsRRDImportLevelInfo (CliHandle, i4RrdImportType,
                                ISIS_METRIC_INDEX_OSPF, i4Instance);
    }
    if (i4ScalarObject & ISIS_IMPORT_BGP)
    {
        CliPrintf (CliHandle, "redistribute bgp ");
        FilePrintf (CliHandle, "redistribute bgp ");
        IsIsRRDImportLevelInfo (CliHandle, i4RrdImportType,
                                ISIS_METRIC_INDEX_BGP, i4Instance);
    }
}

/*******************************************************************************
 * Function Name :  IsIsSysTableInfo                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsSysTableInfo (tCliHandle CliHandle, INT4 i4Instance, UINT4 *pu4PagingStatus)
{

    INT4                i4IsisSysType = 0;
    INT4                i4OlType = 0;
    INT4                i4IsisGRRestartSupport = 0;
    INT4                i4IsisGRT1TimerInterval = 0;
    INT4                i4IsisGRMaxT1RetryCount = 0;
    INT4                i4IsisGRT2L1Interval = 0;
    INT4                i4IsisGRT2L2Interval = 0;
    INT4                i4IsisGRHelperGRTimeLimit = 0;
    INT4                i4IsisGRHelperSupport = 0;
    INT4                i4IsisGRRestartReason = 0;
    INT4                i4IsisGRT3TimerInterval = 0;
    INT4                i4IsisRouteLeak = 0;

    nmhGetIsisSysType (i4Instance, &i4IsisSysType);

    if (i4IsisSysType == ISIS_LEVEL1)
    {
        *pu4PagingStatus = CliPrintf (CliHandle, "is-type level-1 \r\n");
        FilePrintf (CliHandle, "is-type level-1 \r\n");
    }
    if (i4IsisSysType == ISIS_LEVEL2)
    {
        *pu4PagingStatus = CliPrintf (CliHandle, "is-type level-2 \r\n");
        FilePrintf (CliHandle, "is-type level-2 \r\n");
    }

    nmhGetIsisSysL2toL1Leaking (i4Instance, &i4IsisRouteLeak);
    if (i4IsisRouteLeak == ISIS_TRUE)
    {
        *pu4PagingStatus = (UINT4) (CliPrintf (CliHandle, "route-leak \r\n"));
        FilePrintf (CliHandle, "route-leak \r\n");
    }

    nmhGetIsisSysSetOverload (i4Instance, &i4OlType);

    if (i4OlType == ISIS_LL_SET_L1OVERLOAD || i4OlType == ISIS_LL_SET_L2OVERLOAD
        || i4OlType == ISIS_LL_SET_L12OVERLOAD)
    {
        *pu4PagingStatus = CliPrintf (CliHandle, "set-overload-bit \r\n");
        FilePrintf (CliHandle, "set-overload-bit \r\n");
    }

    nmhGetFsIsisExtRestartSupport (i4Instance, &i4IsisGRRestartSupport);

    if (i4IsisGRRestartSupport == ISIS_CLI_GR_RESTART_PLANNED)
    {
        *pu4PagingStatus = CliPrintf (CliHandle, "nsf ietf plannedOnly \r\n");
        FilePrintf (CliHandle, "nsf ietf plannedOnly \r\n");
    }

    if (i4IsisGRRestartSupport == ISIS_CLI_GR_RESTART_BOTH)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "nsf ietf plannedAndUnplanned \r\n");
        FilePrintf (CliHandle, "nsf ietf plannedAndUnplanned \r\n");
    }

    nmhGetFsIsisExtGRT1TimeInterval (i4Instance, &i4IsisGRT1TimerInterval);
    if (i4IsisGRT1TimerInterval != ISIS_GR_DEF_T1_INTERVAL)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "nsf t1 interval %d\r\n",
                       i4IsisGRT1TimerInterval);
        FilePrintf (CliHandle, "nsf t1 interval %d\r\n",
                    i4IsisGRT1TimerInterval);
    }

    nmhGetFsIsisExtGRT1RetryCount (i4Instance, &i4IsisGRMaxT1RetryCount);
    if (i4IsisGRMaxT1RetryCount != ISIS_GR_DEF_ACK_RETRY_COUNT)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "nsf t1 retry-count %d\r\n",
                       i4IsisGRMaxT1RetryCount);
        FilePrintf (CliHandle, "nsf t1 retry-count %d\r\n",
                    i4IsisGRMaxT1RetryCount);
    }

    nmhGetFsIsisExtGRT2TimeIntervalLevel1 (i4Instance, &i4IsisGRT2L1Interval);
    nmhGetFsIsisExtGRT2TimeIntervalLevel2 (i4Instance, &i4IsisGRT2L2Interval);

    if (i4IsisGRT2L1Interval != i4IsisGRT2L2Interval)
    {
        if (i4IsisGRT2L1Interval != ISIS_GR_DEF_T2_INTERVAL)
        {
            *pu4PagingStatus =
                CliPrintf (CliHandle, "nsf t2 level-1 %d\r\n",
                           i4IsisGRT2L1Interval);
            FilePrintf (CliHandle, "nsf t2 level-1 %d\r\n",
                        i4IsisGRT2L1Interval);
        }

        if (i4IsisGRT2L2Interval != ISIS_GR_DEF_T2_INTERVAL)
        {
            *pu4PagingStatus =
                CliPrintf (CliHandle, "nsf t2 level-2 %d\r\n",
                           i4IsisGRT2L2Interval);
            FilePrintf (CliHandle, "nsf t2 level-2 %d\r\n",
                        i4IsisGRT2L2Interval);
        }
    }
    else if ((i4IsisGRT2L1Interval != ISIS_GR_DEF_T2_INTERVAL)
             && (i4IsisGRT2L2Interval != ISIS_GR_DEF_T2_INTERVAL))
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "nsf t2 level1-2 %d\r\n",
                       i4IsisGRT2L1Interval);
        FilePrintf (CliHandle, "nsf t2 level1-2 %d\r\n", i4IsisGRT2L1Interval);
    }

    nmhGetFsIsisExtGRRestartTimeInterval (i4Instance, &i4IsisGRT3TimerInterval);
    if (i4IsisGRT3TimerInterval != ISIS_GR_DEF_T3_INTERVAL)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "nsf t3 manual %d\r\n",
                       i4IsisGRT3TimerInterval);
        FilePrintf (CliHandle, "nsf t3 manual %d\r\n", i4IsisGRT3TimerInterval);
    }

    nmhGetFsIsisExtRestartReason (i4Instance, &i4IsisGRRestartReason);

    if (i4IsisGRRestartReason == ISIS_CLI_GR_SW_RESTART)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle,
                       "nsf ietf restart-reason softwareRestart \r\n");
        FilePrintf (CliHandle, "nsf ietf restart-reason softwareRestart \r\n");
    }

    if (i4IsisGRRestartReason == ISIS_CLI_GR_SW_UPGRADE)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle,
                       "nsf ietf restart-reason swReloadUpgrade \r\n");
        FilePrintf (CliHandle, "nsf ietf restart-reason swReloadUpgrade \r\n");
    }

    if (i4IsisGRRestartReason == ISIS_CLI_GR_SW_RED)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle,
                       "nsf ietf restart-reason switchToRedundant \r\n");
        FilePrintf (CliHandle,
                    "nsf ietf restart-reason switchToRedundant \r\n");
    }

    nmhGetFsIsisExtHelperSupport (i4Instance, &i4IsisGRHelperSupport);

    if (i4IsisGRHelperSupport == ISIS_CLI_GR_HELPER_BOTH)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "nsf ietf helper-support \r\n");
        FilePrintf (CliHandle, "nsf ietf helper-support \r\n");
    }

    nmhGetFsIsisExtHelperGraceTimeLimit (i4Instance,
                                         &i4IsisGRHelperGRTimeLimit);
    /*silvercreek */
    if (i4IsisGRHelperGRTimeLimit != ISIS_GR_DEF_HELPER_TIME)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "nsf ietf helper gracetimelimit %d \r\n",
                       i4IsisGRHelperGRTimeLimit);
        FilePrintf (CliHandle, "nsf ietf helper gracetimelimit %d \r\n",
                    i4IsisGRHelperGRTimeLimit);
    }

    IsIsRedistributionInfo (CliHandle, i4Instance);
    return;

}

/*******************************************************************************
 * Function Name :  IsIsAreaTableInfo                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsAreaTableInfo (tCliHandle CliHandle, INT4 i4Instance,
                   tSNMP_OCTET_STRING_TYPE * pOctetStr, UINT4 *pu4PagingStatus,
                   UINT1 u1Flag)
{
    INT4                i4Index = 0;

    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1SysId[ISIS_SYS_ID_LEN];
    UINT4               u4Cnt = 0;
    UINT4               u4Byte = 0;
    INT4                i4RetVal = 0;
    INT4                i4DotCompliance = ISIS_DOT_FALSE;
    MEMSET (au1SysId, 0, ISIS_SYS_ID_LEN);
    OctetStr.pu1_OctetList = au1SysId;

    if (u1Flag != ISIS_TRUE)
    {
        CliPrintf (CliHandle, "net ");
        FilePrintf (CliHandle, "net ");
    }
    else
    {
        CliPrintf (CliHandle, "\rArea Id   : ");
    }
    i4RetVal = nmhGetFsIsisDotCompliance (i4Instance, &i4DotCompliance);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Failure in Getting Compliance\r\n");
    }
    for (i4Index = 0; i4Index < pOctetStr->i4_Length; i4Index++)
    {
        if (u1Flag != ISIS_TRUE)
        {
            if (i4DotCompliance == ISIS_DOT_TRUE)
            {
                u4Cnt++;
                if (u4Cnt % 2 == 0 || u4Byte == 0)
                {
                    u4Byte = 1;    /* To print the first byte alone in one byte format */
                    u4Cnt = 0;
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%02x.",
                                   pOctetStr->pu1_OctetList[i4Index]);
                    FilePrintf (CliHandle, "%02x.",
                                pOctetStr->pu1_OctetList[i4Index]);
                }
                else
                {
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, "%02x",
                                   pOctetStr->pu1_OctetList[i4Index]);
                    FilePrintf (CliHandle, "%02x",
                                pOctetStr->pu1_OctetList[i4Index]);
                }
            }
            else
            {
                *pu4PagingStatus =
                    CliPrintf (CliHandle, "%02x:",
                               pOctetStr->pu1_OctetList[i4Index]);
                FilePrintf (CliHandle, "%02x:",
                            pOctetStr->pu1_OctetList[i4Index]);
            }
        }
        else
        {
            *pu4PagingStatus =
                CliPrintf (CliHandle, "%02x",
                           pOctetStr->pu1_OctetList[i4Index]);
        }
    }

    nmhGetIsisSysID (i4Instance, &OctetStr);

    if (u1Flag != ISIS_TRUE)
    {
        if (i4DotCompliance == ISIS_DOT_TRUE)
        {
            if (u4Cnt == 0)
            {
                *pu4PagingStatus =
                    CliPrintf (CliHandle, "%02x%02x.%02x%02x.%02x%02x.00\r\n",
                               OctetStr.pu1_OctetList[0],
                               OctetStr.pu1_OctetList[1],
                               OctetStr.pu1_OctetList[2],
                               OctetStr.pu1_OctetList[3],
                               OctetStr.pu1_OctetList[4],
                               OctetStr.pu1_OctetList[5]);

                FilePrintf (CliHandle, "%02x%02x.%02x%02x.%02x%02x.00\r\n",
                            OctetStr.pu1_OctetList[0],
                            OctetStr.pu1_OctetList[1],
                            OctetStr.pu1_OctetList[2],
                            OctetStr.pu1_OctetList[3],
                            OctetStr.pu1_OctetList[4],
                            OctetStr.pu1_OctetList[5]);
            }
            else
            {
                *pu4PagingStatus =
                    CliPrintf (CliHandle, "%02x.%02x%02x.%02x%02x.%02x00\r\n",
                               OctetStr.pu1_OctetList[0],
                               OctetStr.pu1_OctetList[1],
                               OctetStr.pu1_OctetList[2],
                               OctetStr.pu1_OctetList[3],
                               OctetStr.pu1_OctetList[4],
                               OctetStr.pu1_OctetList[5]);

                FilePrintf (CliHandle, "%02x.%02x%02x.%02x%02x.%02x00\r\n",
                            OctetStr.pu1_OctetList[0],
                            OctetStr.pu1_OctetList[1],
                            OctetStr.pu1_OctetList[2],
                            OctetStr.pu1_OctetList[3],
                            OctetStr.pu1_OctetList[4],
                            OctetStr.pu1_OctetList[5]);
            }
        }
        else
        {
            *pu4PagingStatus =
                CliPrintf (CliHandle, "%02x:%02x:%02x:%02x:%02x:%02x:00\r\n",
                           OctetStr.pu1_OctetList[0], OctetStr.pu1_OctetList[1],
                           OctetStr.pu1_OctetList[2], OctetStr.pu1_OctetList[3],
                           OctetStr.pu1_OctetList[4],
                           OctetStr.pu1_OctetList[5]);

            FilePrintf (CliHandle, "%02x:%02x:%02x:%02x:%02x:%02x:00\r\n",
                        OctetStr.pu1_OctetList[0], OctetStr.pu1_OctetList[1],
                        OctetStr.pu1_OctetList[2], OctetStr.pu1_OctetList[3],
                        OctetStr.pu1_OctetList[4], OctetStr.pu1_OctetList[5]);
        }
    }
    else
    {
        CliPrintf (CliHandle, "\n\rSystem Id : ");
        if (i4DotCompliance == ISIS_DOT_TRUE)
        {
            *pu4PagingStatus =
                CliPrintf (CliHandle, "%02x%02x.%02x%02x.%02x%02x\r\n",
                           OctetStr.pu1_OctetList[0], OctetStr.pu1_OctetList[1],
                           OctetStr.pu1_OctetList[2], OctetStr.pu1_OctetList[3],
                           OctetStr.pu1_OctetList[4],
                           OctetStr.pu1_OctetList[5]);
        }
        else
        {
            *pu4PagingStatus =
                CliPrintf (CliHandle, "%02x:%02x:%02x:%02x:%02x:%02x\n",
                           OctetStr.pu1_OctetList[0], OctetStr.pu1_OctetList[1],
                           OctetStr.pu1_OctetList[2], OctetStr.pu1_OctetList[3],
                           OctetStr.pu1_OctetList[4],
                           OctetStr.pu1_OctetList[5]);
        }
    }

    return;

}

/*******************************************************************************
 * Function Name :  IsIsIpraTableInfo                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsIpraTableInfo (tCliHandle CliHandle, INT4 i4Instance, INT4 i4IsisIPRAType,
                   INT4 i4IsisIPRAIndex, UINT4 *pu4PagingStatus)
{

    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStr1;
    UINT1               au1IpAddress[ISIS_MAX_IPV6_ADDR_LEN];
    UINT1               auIpNextHop[ISIS_MAX_IPV6_ADDR_LEN];
    UINT4               u4IpAddr = 0;
    UINT4               u4IpNextHop = 0;
    INT4                i4Mettype = 0;
    CHR1               *pu1String = NULL;
    UINT4               u4PrefixLen = 0;
    INT1                ai1IpMask[ISIS_PRINT_BUF_SIZE];

    MEMSET (ai1IpMask, 0, ISIS_PRINT_BUF_SIZE);
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OctetStr1, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    nmhGetIsisIPRAMetricType (i4Instance, i4IsisIPRAType, i4IsisIPRAIndex,
                              &i4Mettype);
    i4Mettype = i4Mettype - 1;
    *pu4PagingStatus = CliPrintf (CliHandle, "ipra %d ", i4IsisIPRAIndex);
    FilePrintf (CliHandle, "ipra %d ", i4IsisIPRAIndex);

    OctetStr.pu1_OctetList = au1IpAddress;
    OctetStr1.pu1_OctetList = auIpNextHop;
    nmhGetIsisIPRADest (i4Instance, i4IsisIPRAType, i4IsisIPRAIndex, &OctetStr);

    nmhGetIsisIPRADestPrefixLen (i4Instance, i4IsisIPRAType, i4IsisIPRAIndex,
                                 &u4PrefixLen);

    if (OctetStr.i4_Length == ISIS_MAX_IPV6_ADDR_LEN)
    {
        CliPrintf (CliHandle, " %s",
                   Ip6PrintAddr ((tIp6Addr *) (VOID *) OctetStr.pu1_OctetList));
        FilePrintf (CliHandle, " %s",
                    Ip6PrintAddr ((tIp6Addr *) (VOID *) OctetStr.
                                  pu1_OctetList));

        *pu4PagingStatus = CliPrintf (CliHandle, " %d", u4PrefixLen);
        FilePrintf (CliHandle, " %d", u4PrefixLen);

    }
    else
    {
        MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, OctetStr.i4_Length);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, OSIX_NTOHL (u4IpAddr));

        *pu4PagingStatus = CliPrintf (CliHandle, " %s", pu1String);
        FilePrintf (CliHandle, " %s", pu1String);

        /* Take prefix info */
        SPRINTF ((CHR1 *) ai1IpMask, "%s",
                 (CONST CHR1 *) IsIsPrintIpMask ((UINT1) u4PrefixLen));

        *pu4PagingStatus = CliPrintf (CliHandle, " %s", ai1IpMask);
        FilePrintf (CliHandle, " %s", ai1IpMask);

    }

    nmhGetFsIsisExtIPRANextHop (i4Instance, i4IsisIPRAType, i4IsisIPRAIndex,
                                &OctetStr1);

    if (OctetStr1.i4_Length == ISIS_MAX_IPV6_ADDR_LEN)
    {
        CliPrintf (CliHandle, " %s ",
                   Ip6PrintAddr ((tIp6Addr *) (VOID *) OctetStr1.
                                 pu1_OctetList));
        FilePrintf (CliHandle, " %s ",
                    Ip6PrintAddr ((tIp6Addr *) (VOID *) OctetStr1.
                                  pu1_OctetList));
    }
    else
    {
        MEMCPY (&u4IpNextHop, OctetStr1.pu1_OctetList, OctetStr1.i4_Length);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, OSIX_NTOHL (u4IpNextHop));

        *pu4PagingStatus = CliPrintf (CliHandle, " %s ", pu1String);
        FilePrintf (CliHandle, " %s ", pu1String);
    }
    if (i4Mettype == ISIS_EXTERNAL)
    {
        CliPrintf (CliHandle, "met-type external\r\n");
        FilePrintf (CliHandle, "met-type external\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
        FilePrintf (CliHandle, "\r\n");
    }
    return;
}

/*******************************************************************************
 * Function Name :  IsIsIpraMetricTableInfo                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsIpraMetricTableInfo (tCliHandle CliHandle, INT4 i4Instances,
                         INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                         UINT4 *pu4PagingStatus)
{

    INT4                i4default_metric = 0;
    INT4                i4delay_metric = 0;
    INT4                i4expense_metric = 0;
    INT4                i4error_metric = 0;
    UINT4               u4full_metric = 0;
    tIsisSysContext    *pContext = NULL;

    if ((IsisCtrlGetSysContext (i4Instances, &pContext) == ISIS_SUCCESS)
        && (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
    {
        nmhGetFsIsisExtIPRAFullMetric (i4Instances, i4IsisIPRAType,
                                       i4IsisIPRAIndex, &u4full_metric);
    }
    else
    {
        nmhGetIsisIPRAMetric (i4Instances, i4IsisIPRAType, i4IsisIPRAIndex,
                              &i4default_metric);
    }
    nmhGetFsIsisExtIPRADelayMetric (i4Instances, i4IsisIPRAType,
                                    i4IsisIPRAIndex, &i4delay_metric);

    nmhGetFsIsisExtIPRAExpenseMetric (i4Instances, i4IsisIPRAType,
                                      i4IsisIPRAIndex, &i4expense_metric);

    nmhGetFsIsisExtIPRAErrorMetric (i4Instances, i4IsisIPRAType,
                                    i4IsisIPRAIndex, &i4error_metric);

    if ((i4default_metric != 0)
        && (i4default_metric != ISIS_IPRA_DEF_METRIC_VALUE))
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "ipra %d default-metric %d", i4IsisIPRAIndex,
                       i4default_metric);
        FilePrintf (CliHandle, "ipra %d default-metric %d", i4IsisIPRAIndex,
                    i4default_metric);
    }

    if ((u4full_metric != 0) && (u4full_metric != ISIS_IPRA_DEF_METRIC_VALUE))
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "ipra %d default-metric %u", i4IsisIPRAIndex,
                       u4full_metric);
        FilePrintf (CliHandle, "ipra %d default-metric %u", i4IsisIPRAIndex,
                    u4full_metric);
    }

    if (i4delay_metric != 0 && i4delay_metric != ISIS_IPRA_DEF_METRIC_VALUE)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, " delay-metric %d", i4delay_metric);
        FilePrintf (CliHandle, " delay-metric %d", i4delay_metric);
    }

    if (i4error_metric != 0 && i4error_metric != ISIS_IPRA_DEF_METRIC_VALUE)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, " error-metric %d", i4error_metric);
        FilePrintf (CliHandle, " error-metric %d", i4error_metric);
    }

    if (i4expense_metric != 0 && i4expense_metric != ISIS_IPRA_DEF_METRIC_VALUE)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, " expense-metric %d", i4expense_metric);
        FilePrintf (CliHandle, " expense-metric %d", i4expense_metric);
    }

    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    FilePrintf (CliHandle, "\r\n");
    return;
}

/*******************************************************************************
 * Function Name :  IsIsShowRunningConfigInterfaceInCxt                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsShowRunningConfigInterfaceInCxt (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4Instances = 0;
    INT4                i4IsisCircIndex = 0;
    INT4                i4PrevInstances = 0;
    INT4                i4PrevIsisCircIndex = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                i1OutCome = 0;
    INT4                i4IfIndex = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1NameStr, 0, sizeof (au1NameStr));

    if (CfaCliConfGetIfName ((UINT4) i4Index, (INT1 *) au1NameStr) !=
        CFA_SUCCESS)
    {
        return;
    }

    i1OutCome = nmhGetFirstIndexIsisCircTable (&i4Instances, &i4IsisCircIndex);

    while (i1OutCome != SNMP_FAILURE)
    {

        nmhGetIsisCircIfIndex (i4Instances, i4IsisCircIndex, &i4IfIndex);

        if (i4IfIndex == i4Index)
        {
            CliPrintf (CliHandle, "interface %s \r\n", au1NameStr);
            FilePrintf (CliHandle, "interface %s \r\n", au1NameStr);
            /*Circuit Entry Table */
            IsIsCktEntryTableInfo (CliHandle, i4Instances, i4IsisCircIndex,
                                   &u4PagingStatus);

            /* Circuit Level Table */
            IsIsCktLevelTableInfo (CliHandle, i4Instances, i4IsisCircIndex,
                                   &u4PagingStatus);
            FilePrintf (CliHandle, "exit\r\n");
            CliPrintf (CliHandle, "!\r\n");
        }

        i4PrevInstances = i4Instances;
        i4PrevIsisCircIndex = i4IsisCircIndex;
        i1OutCome = nmhGetNextIndexIsisCircTable (i4PrevInstances,
                                                  &i4Instances,
                                                  i4PrevIsisCircIndex,
                                                  &i4IsisCircIndex);

    }

    return;
}

/*******************************************************************************
 * Function Name :  IsIsCktEntryTableInfo                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsCktEntryTableInfo (tCliHandle CliHandle, INT4 i4Instances,
                       INT4 i4IsisCircIndex, UINT4 *pu4PagingStatus)
{
#ifdef BFD_WANTED
    tIsisSysContext    *pContext = NULL;
    INT4                i4IsisCktBfdStatus = 0;
#endif
    INT4                i4CktLevel = 0;
    INT4                i4RetValIsisCircType = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1Passwd[ISIS_MAX_PASSWORD_LEN];
    INT4                i4AuthMode = 0;
    INT4                i4IfIndex = 0;
    INT4                i4ResType = 0;

    /* Get Restoration Type - whether CSR or MSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    if (nmhGetIsisCircType (i4Instances, i4IsisCircIndex, &i4RetValIsisCircType)
        != SNMP_SUCCESS)
    {
        return;
    }
    if (nmhGetIsisCircIfIndex (i4Instances, i4IsisCircIndex, &i4IfIndex)
        != SNMP_SUCCESS)
    {
        return;
    }
    if (i4RetValIsisCircType == ISIS_BC_CKT)
    {
        *pu4PagingStatus = CliPrintf (CliHandle, "ip router isis \r\n");
        FilePrintf (CliHandle, "ip router isis \r\n");
    }
    if (i4RetValIsisCircType == ISIS_P2P_CKT)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "ip router isis point-to-point \r\n");
        FilePrintf (CliHandle, "ip router isis point-to-point \r\n");
    }
    MEMSET (au1Passwd, 0, ISIS_MAX_PASSWORD_LEN);
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    nmhGetIsisCircLevel (i4Instances, i4IsisCircIndex, &i4CktLevel);

    OctetStr.i4_Length = ISIS_MAX_PASSWORD_LEN;
    OctetStr.pu1_OctetList = au1Passwd;

    if (i4CktLevel == ISIS_LEVEL1)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "isis circuit-type level-1 \r\n");
        FilePrintf (CliHandle, "isis circuit-type level-1 \r\n");

    }
    if (i4CktLevel == ISIS_LEVEL2)
    {
        *pu4PagingStatus =
            CliPrintf (CliHandle, "isis circuit-type level-2-only \r\n");
        FilePrintf (CliHandle, "isis circuit-type level-2-only \r\n");

    }

#ifdef BFD_WANTED
    /* To print the BFD related configurations */
    if ((IsisCtrlGetSysContext ((UINT4) i4Instances, &pContext) ==
         ISIS_SUCCESS) && (pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE))
    {
        nmhGetFsIsisExtCircBfdStatus (i4Instances, i4IsisCircIndex,
                                      &i4IsisCktBfdStatus);
        if (pContext->u1IsisBfdAllIfStatus == ISIS_BFD_ENABLE)
        {
            if (i4IsisCktBfdStatus == ISIS_BFD_DISABLE)
            {
                *pu4PagingStatus = (UINT4)
                    CliPrintf (CliHandle, "isis bfd disable \r\n");
                FilePrintf (CliHandle, "isis bfd disable \r\n");
            }
        }
        else
        {
            if (i4IsisCktBfdStatus == ISIS_BFD_ENABLE)
            {
                *pu4PagingStatus = (UINT4)
                    CliPrintf (CliHandle, "isis bfd \r\n");
                FilePrintf (CliHandle, "isis bfd \r\n");
            }
        }
    }
#endif

    if ((i4CktLevel == ISIS_LEVEL1) || (i4CktLevel == ISIS_LEVEL12))
    {
        nmhGetFsIsisExtCircLevelAuthType (i4Instances, i4IsisCircIndex,
                                          ISIS_LEVEL1, &i4AuthMode);

        if (i4AuthMode == ISIS_AUTH_MD5)
        {
            CliPrintf (CliHandle, "\r isis authentication mode md5 level-1\n");
            FilePrintf (CliHandle,
                        "\r isis authentication mode md5 level-1 \r\n");
        }

        nmhGetFsIsisExtCircLevelTxPassword ((UINT4) i4Instances,
                                            (UINT4) i4IsisCircIndex,
                                            (UINT1) ISIS_LEVEL1, &OctetStr);
        if ((i4ResType == SET_FLAG) && (MsrGetSaveStatus () == ISS_TRUE))
        {
            if ((i4RetValIsisCircType == ISIS_P2P_CKT)
                && (i4CktLevel == ISIS_LEVEL12))
            {
                if (OctetStr.i4_Length != 0)
                {
                    FilePrintf (CliHandle, "\risis password %s  \n",
                                OctetStr.pu1_OctetList);
                    CliPrintf (CliHandle, "\risis password %s \n",
                               OctetStr.pu1_OctetList);
                }
            }
            else
            {
                if (OctetStr.i4_Length != 0)
                {
                    FilePrintf (CliHandle, "\risis password %s level-1 \n",
                                OctetStr.pu1_OctetList);
                    CliPrintf (CliHandle, "\risis password %s level-1 \n",
                               OctetStr.pu1_OctetList);
                }
            }
        }
        else
        {
            if ((i4RetValIsisCircType == ISIS_P2P_CKT)
                && (i4CktLevel == ISIS_LEVEL12))
            {
                if (OctetStr.i4_Length != 0)
                {
                    FilePrintf (CliHandle, "\risis password %s  \n",
                                OctetStr.pu1_OctetList);
                    CliPrintf (CliHandle, "\risis password ****** \n");
                }
            }
            else
            {
                if (OctetStr.i4_Length != 0)
                {
                    FilePrintf (CliHandle, "\risis password %s level-1 \n",
                                OctetStr.pu1_OctetList);
                    CliPrintf (CliHandle, "\risis password ****** level-1 \n");
                }
            }
        }
    }
    if ((i4CktLevel == ISIS_LEVEL2) || (i4CktLevel == ISIS_LEVEL12))
    {
        nmhGetFsIsisExtCircLevelAuthType (i4Instances, i4IsisCircIndex,
                                          ISIS_LEVEL2, &i4AuthMode);
        if (i4AuthMode == ISIS_AUTH_MD5)
        {
            CliPrintf (CliHandle, "\r isis authentication mode md5 level-2\n");
            FilePrintf (CliHandle,
                        "\r isis authentication mode md5 level-2 \r\n");
        }

        MEMSET (au1Passwd, 0, ISIS_MAX_PASSWORD_LEN);
        MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        OctetStr.i4_Length = ISIS_MAX_PASSWORD_LEN;
        OctetStr.pu1_OctetList = au1Passwd;
        nmhGetFsIsisExtCircLevelTxPassword ((UINT4) i4Instances,
                                            (UINT4) i4IsisCircIndex,
                                            (UINT1) ISIS_LEVEL2, &OctetStr);
        if (OctetStr.i4_Length != 0)
        {
            FilePrintf (CliHandle, "\risis password %s level-2 \n",
                        OctetStr.pu1_OctetList);
            CliPrintf (CliHandle, "\risis password ****** level-2 \n");
        }
    }
    return;
}

/*******************************************************************************
 * Function Name :  IsIsCktLevelTableInfo                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
VOID
IsIsCktLevelTableInfo (tCliHandle CliHandle, INT4 i4Instances,
                       INT4 i4IsisCircIndex, UINT4 *pu4PagingStatus)
{
    INT4                i4Inst = 0;
    INT4                i4CircIndex = 0;
    INT4                i4Level = 0;

    INT4                i4PrevInst = 0;
    INT4                i4PrevCircIndex = 0;
    INT4                i4PrevLevel = 0;

    INT1                i1OutCome = 0;
    INT4                i4Interval = 0;

    INT4                i4default_metric = 0;
    INT4                i4delay_metric = 0;
    INT4                i4error_metric = 0;
    INT4                i4expense_metric = 0;
    UINT4               u4wide_metric = 0;
    tIsisSysContext    *pContext = NULL;
    UINT1               bDisplay = ISIS_FALSE;
    INT4                i4CircType = 0;

    i1OutCome =
        nmhGetFirstIndexIsisCircLevelTable (&i4Inst, &i4CircIndex, &i4Level);

    while (i1OutCome != SNMP_FAILURE)
    {
        if ((i4Instances == i4Inst) && (i4IsisCircIndex == i4CircIndex))
        {
            /* Hello Timer */
            nmhGetIsisCircLevelHelloTimer (i4Inst, i4CircIndex,
                                           i4Level, &i4Interval);

            if ((nmhGetIsisCircType (i4Inst, i4CircIndex, &i4CircType)) !=
                SNMP_SUCCESS)
            {
                return;
            }
            bDisplay = ISIS_FALSE;

            if ((i4CircType == ISIS_BC_CKT) || (i4CircType == ISIS_P2P_CKT))
            {
                if (i4Interval != ISIS_HELLO_INT)
                {
                    bDisplay = ISIS_TRUE;
                }
            }

            if (bDisplay == TRUE)
            {
                if (i4Level == ISIS_LEVEL1)
                {
                    *pu4PagingStatus = (UINT4)
                        (CliPrintf (CliHandle,
                                    "isis hello-interval %d level-1\r\n",
                                    i4Interval));
                    FilePrintf (CliHandle, "isis hello-interval %d level-1\r\n",
                                i4Interval);
                }
                if (i4Level == ISIS_LEVEL2)
                {
                    *pu4PagingStatus =
                        CliPrintf (CliHandle,
                                   "isis hello-interval %d level-2\r\n",
                                   i4Interval);
                    FilePrintf (CliHandle, "isis hello-interval %d level-2\r\n",
                                i4Interval);

                }

            }

            i4Interval = 0;
            /*Hello Multipier */
            nmhGetIsisCircLevelHelloMultiplier (i4Inst, i4CircIndex,
                                                i4Level, &i4Interval);
            bDisplay = ISIS_FALSE;
            if ((i4CircType == ISIS_BC_CKT) || (i4CircType == ISIS_P2P_CKT))
            {
                if (i4Interval != ISIS_HELLO_MULT)
                {
                    bDisplay = ISIS_TRUE;
                }
            }

            if (bDisplay == TRUE)
            {
                if (i4Level == ISIS_LEVEL1)
                {
                    *pu4PagingStatus = (UINT4)
                        (CliPrintf (CliHandle,
                                    "isis hello-multiplier %d level-1\r\n",
                                    i4Interval));
                    FilePrintf (CliHandle,
                                "isis hello-multiplier %d level-1\r\n",
                                i4Interval);

                }
                if (i4Level == ISIS_LEVEL2)
                {
                    *pu4PagingStatus =
                        CliPrintf (CliHandle,
                                   "isis hello-multiplier %d level-2\r\n",
                                   i4Interval);
                    FilePrintf (CliHandle,
                                "isis hello-multiplier %d level-2\r\n",
                                i4Interval);
                }

            }

            i4Interval = 0;
            /*LSP Throttle interval */
            nmhGetIsisCircLevelLSPThrottle (i4Inst, i4CircIndex,
                                            i4Level, &i4Interval);
            if (i4Interval != ISIS_LSP_THROTTLE)
            {
                if (i4Level == ISIS_LEVEL1)
                {
                    *pu4PagingStatus = (UINT4)
                        (CliPrintf
                         (CliHandle, "isis lsp-interval %d level-1\r\n",
                          i4Interval));
                    FilePrintf (CliHandle, "isis lsp-interval %d level-1\r\n",
                                i4Interval);
                }
                if (i4Level == ISIS_LEVEL2)
                {
                    *pu4PagingStatus = (UINT4)
                        (CliPrintf
                         (CliHandle, "isis lsp-interval %d level-2\r\n",
                          i4Interval));
                    FilePrintf (CliHandle, "isis lsp-interval %d level-2\r\n",
                                i4Interval);
                }
            }

            i4Interval = 0;
            /*Retransmit interval */
            nmhGetIsisCircLevelMinLSPRetransInt (i4Inst, i4CircIndex,
                                                 i4Level, &i4Interval);
            if (i4Interval != ISIS_LSP_RETXN_INT)
            {
                if (i4Level == ISIS_LEVEL1)
                {
                    *pu4PagingStatus = (UINT4)
                        (CliPrintf
                         (CliHandle, "isis retransmit-interval  %d level-1\r\n",
                          i4Interval));
                    FilePrintf (CliHandle,
                                "isis retransmit-interval  %d level-1\r\n",
                                i4Interval);
                }
                if (i4Level == ISIS_LEVEL2)
                {
                    *pu4PagingStatus = (UINT4)
                        (CliPrintf
                         (CliHandle, "isis retransmit-interval  %d level-2\r\n",
                          i4Interval));
                    FilePrintf (CliHandle,
                                "isis retransmit-interval  %d level-2\r\n",
                                i4Interval);
                }
            }
            i4Interval = 0;
            /*Circuit Priority */
            nmhGetIsisCircLevelISPriority (i4Inst, i4CircIndex,
                                           i4Level, &i4Interval);

            if (i4Interval != ISIS_IS_PRIORITY)
            {
                if (i4Level == ISIS_LEVEL1)
                {
                    *pu4PagingStatus = (UINT4)
                        (CliPrintf (CliHandle, "isis priority %d level-1\r\n",
                                    i4Interval));

                    FilePrintf (CliHandle, "isis priority %d level-1\r\n",
                                i4Interval);
                }
                if (i4Level == ISIS_LEVEL2)
                {
                    *pu4PagingStatus = (UINT4)
                        (CliPrintf (CliHandle, "isis priority %d level-2\r\n",
                                    i4Interval));
                    FilePrintf (CliHandle, "isis priority %d level-2\r\n",
                                i4Interval);
                }
            }

            i4Interval = 0;
            /*CSNP Interval */
            nmhGetIsisCircLevelCSNPInterval (i4Inst, i4CircIndex,
                                             i4Level, &i4Interval);

            if (i4Interval != ISIS_CSNP_INT)
            {
                if (i4Level == ISIS_LEVEL1)
                {
                    *pu4PagingStatus =
                        CliPrintf (CliHandle,
                                   "isis csnp-interval %d level-1\r\n",
                                   i4Interval);
                    FilePrintf (CliHandle, "isis csnp-interval %d level-1\r\n",
                                i4Interval);
                }
                if (i4Level == ISIS_LEVEL2)
                {
                    *pu4PagingStatus =
                        CliPrintf (CliHandle,
                                   "isis csnp-interval %d level-2\r\n",
                                   i4Interval);
                    FilePrintf (CliHandle, "isis csnp-interval %d level-2\r\n",
                                i4Interval);
                }
            }

            i4Interval = 0;
            /*Metric Information */
            if ((IsisCtrlGetSysContext (i4Inst, &pContext) ==
                 ISIS_SUCCESS)
                && (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
            {
                nmhGetFsIsisExtCircLevelWideMetric (i4Inst, i4CircIndex,
                                                    i4Level, &u4wide_metric);
            }
            else
            {
                nmhGetIsisCircLevelMetric (i4Inst, i4CircIndex, i4Level,
                                           &i4default_metric);
            }
            nmhGetFsIsisExtCircLevelDelayMetric (i4Inst, i4CircIndex, i4Level,
                                                 &i4delay_metric);
            nmhGetFsIsisExtCircLevelErrorMetric (i4Inst, i4CircIndex, i4Level,
                                                 &i4error_metric);
            nmhGetFsIsisExtCircLevelExpenseMetric (i4Inst, i4CircIndex, i4Level,
                                                   &i4expense_metric);

            if (((pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC)
                 && (i4default_metric != 0)
                 && (i4default_metric != ISIS_CKTL_DEF_METRIC_VALUE))
                || ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                    && (u4wide_metric != 0)
                    && (u4wide_metric != ISIS_CKTL_DEF_METRIC_VALUE)))
            {

                *pu4PagingStatus =
                    CliPrintf (CliHandle, "isis metric %u",
                               ((pContext->u1MetricStyle ==
                                 ISIS_STYLE_NARROW_METRIC) ? (UINT4)
                                i4default_metric : u4wide_metric));
                FilePrintf (CliHandle, "isis metric %u",
                            ((pContext->u1MetricStyle ==
                              ISIS_STYLE_NARROW_METRIC) ? (UINT4)
                             i4default_metric : u4wide_metric));

                if (i4delay_metric != 0
                    && i4delay_metric != ISIS_CKTL_DEF_METRIC_VALUE)
                {
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, " delay-metric %d",
                                   i4delay_metric);
                    FilePrintf (CliHandle, " delay-metric %d", i4delay_metric);
                }

                if (i4error_metric != 0
                    && i4error_metric != ISIS_CKTL_DEF_METRIC_VALUE)
                {
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, " error-metric %d",
                                   i4error_metric);
                    FilePrintf (CliHandle, " error-metric %d", i4error_metric);
                }

                if (i4expense_metric != 0
                    && i4expense_metric != ISIS_CKTL_DEF_METRIC_VALUE)
                {
                    *pu4PagingStatus =
                        CliPrintf (CliHandle, " expense-metric %d",
                                   i4expense_metric);
                    FilePrintf (CliHandle, " expense-metric %d",
                                i4expense_metric);
                }
                if (i4Level == ISIS_LEVEL1)
                {
                    *pu4PagingStatus =
                        (UINT4) (CliPrintf (CliHandle, " level-1\r\n"));
                    FilePrintf (CliHandle, " level-1\r\n");
                }
                else if (i4Level == ISIS_LEVEL2)
                {
                    *pu4PagingStatus =
                        (UINT4) (CliPrintf (CliHandle, " level-2\r\n"));
                    FilePrintf (CliHandle, " level-2\r\n");
                }

            }
            *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
            /* FilePrintf (CliHandle, "\r\n"); */
        }

        i4PrevInst = i4Inst;
        i4PrevCircIndex = i4CircIndex;
        i4PrevLevel = i4Level;
        i1OutCome = nmhGetNextIndexIsisCircLevelTable (i4PrevInst,
                                                       &i4Inst,
                                                       i4PrevCircIndex,
                                                       &i4CircIndex,
                                                       i4PrevLevel, &i4Level);

    }

}

/*******************************************************************************
 * Function Name :  IsIsPrintIpMask                                      
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
UINT1              *
IsIsPrintIpMask (UINT1 u1PrefixLen)
{
    PRIVATE UINT1       au1IpMaskBuffer[ISIS_PRINT_BUF_SIZE] = { 0 };
    UINT1               au1IpMask[16] = { 0 };
    UINT4               u4Mask;

    u4Mask = IsIsGetSubnetmask (u1PrefixLen);
    PTR_ASSIGN4 (au1IpMask, u4Mask);
    SPRINTF ((CHR1 *) & au1IpMaskBuffer, "%d.%d.%d.%d",
             *au1IpMask, *(au1IpMask + 1), *(au1IpMask + 2), *(au1IpMask + 3));

    return ((UINT1 *) &au1IpMaskBuffer);

}

/*******************************************************************************
 * Function Name :  IsIsGetSubnetmask                                       
 * Description   :  This function displays the SRC information
 * Input (s)     : CliHandle     - CliContext      
 * Output (s)    : -                 
 * Returns       : -                                                  
 *
 ******************************************************************************/
UINT4
IsIsGetSubnetmask (UINT1 u1Prefixlen)
{
    UINT4               u4Subnetmask = 0;

    if (u1Prefixlen != 0)
    {
        u4Subnetmask = (ISIS_HOST_MASK << (ISIS_MAX_PREFIXLEN - u1Prefixlen));
    }
    return (u4Subnetmask);
}

/*******************************************************************************
 * Function Name :  IsisCliSetRouteLeak                                      
 * Description   :  This function enable the route leak option 
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 *****************************************************************************/

INT4
IsisCliSetRouteLeak (tCliHandle CliHandle, UINT4 u4InstIdx)
{

    UINT4               u4Error = 0;

    if (nmhTestv2IsisSysL2toL1Leaking (&u4Error, u4InstIdx,
                                       ISIS_CLI_ROUTE_LEAK_ENABLE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisSysL2toL1Leaking (u4InstIdx,
                                    ISIS_CLI_ROUTE_LEAK_ENABLE) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliResetRouteLeak                                      
 * Description   :  This function disable the route leak option   
 * Input (s)     : CliHandle     - CliContext      
 *                  u4InstIdx       - Instance Id
 * Output (s)    : -                 
 * Returns       : CLI_SUCCESS/CLI_FAILURE.                                                  
 *
 ******************************************************************************/

INT4
IsisCliResetRouteLeak (tCliHandle CliHandle, UINT4 u4InstIdx)
{

    UINT4               u4Error = 0;

    if (nmhTestv2IsisSysL2toL1Leaking (&u4Error, u4InstIdx,
                                       ISIS_CLI_ROUTE_LEAK_DISABLE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIsisSysL2toL1Leaking (u4InstIdx,
                                    ISIS_CLI_ROUTE_LEAK_DISABLE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : IsisCliGetContext                                      */
/*                                                                            */
/* Description       : This function calls a routine to set the               */
/*                     context id. The function sets a flag for show routines */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext ID                         */
/*                     u4Command      - CLI command                           */
/*                                                                            */
/* Output Parameters : pu4SysInstIdx   - Isis Context ID                      */
/*                     pi4IfIndex     - Interface If Index                    */
/*                     pu2ShowCmdFlag - Flag to indicate the show command     */
/*                                                                            */
/* Return Value      : ISIS_SUCCESS/ISIS_FAILURE                              */
/******************************************************************************/

INT4
IsisCliGetContext (tCliHandle CliHandle, UINT4 u4Command,
                   UINT4 *pu4SysInstIdx, INT4 *pi4IfIndex,
                   UINT2 *pu2ShowCmdFlag)
{
    UINT4               u4Port;
    INT4                i4IfIndex;

    *pu2ShowCmdFlag = ISIS_FALSE;

    /* Get the context Id from pCliContext structure */
    *pu4SysInstIdx = CLI_GET_CXT_ID ();

    /* Invalid context Id is obtained in Interface and Global mode */
    if ((*pu4SysInstIdx == ISIS_INVALID_CXT_ID) ||
        (u4Command == ISIS_CLI_CMD_CREATE_CKT) ||
        (u4Command == ISIS_CLI_CMD_DELETE_CKT) ||
        (u4Command == ISIS_CLI_CMD_SET_CKT_TYPE) ||
        (u4Command == ISIS_CLI_CMD_CKT_METRIC) ||
        (u4Command == ISIS_CLI_CMD_NO_CKT_METRIC) ||
        (u4Command == ISIS_CLI_CMD_HELLO_INT) ||
        (u4Command == ISIS_CLI_CMD_HELLO_MULTI) ||
        (u4Command == ISIS_CLI_CMD_CKT_ADD_PASSWD) ||
        (u4Command == ISIS_CLI_CMD_CKT_DEL_PASSWD) ||
        (u4Command == ISIS_CLI_CMD_RETR_INT) ||
        (u4Command == ISIS_CLI_CMD_LSP_INT) ||
        (u4Command == ISIS_CLI_CMD_CKT_PRIO) ||
        (u4Command == ISIS_CLI_CMD_CKT_CSNP_INT))

    {
        i4IfIndex = CLI_GET_IFINDEX ();

        /* CLI_GET_IFINDEX returns -1 for non-interface mode */
        if (i4IfIndex != ISIS_INVALID_IFINDEX)
        {
            *pi4IfIndex = i4IfIndex;

            /* Get IP port from IfIndex */
            if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
                != NETIPV4_SUCCESS)
            {
                CliPrintf (CliHandle, "%% Interface not available\r\n");
                return CLI_FAILURE;
            }

            /* Get the context Id from the port */
            if (NetIpv4GetCxtId (u4Port, pu4SysInstIdx) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Interface not mapped to any context\r\n");
                return CLI_FAILURE;
            }
        }
        else
        {

            /* This is the Global mode
             * Show flag is set for exec mode*/
            if ((u4Command == ISIS_CLI_CMD_CREATE_INST) ||
                (u4Command == ISIS_CLI_CMD_DEST_INST) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_ADJN) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_DECN) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_UPDT) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_INTERFACE) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_CONTROL) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_TIMER) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_FAULT) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_RTMI) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_DLLI) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_TRFR) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_SNMP) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_UTIL) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_CRITICAL) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_ENTRYEXIT) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_DUMP) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_DETAIL) ||
                (u4Command == ISIS_CLI_CMD_DEBUG_ALL) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_ADJN) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_DECN) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_UPDT) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_INTERFACE) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_CONTROL) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_TIMER) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_FAULT) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_RTMI) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_DLLI) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_TRFR) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_SNMP) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_UTIL) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_CRITICAL) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_ENTRYEXIT) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_DUMP) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_DETAIL) ||
                (u4Command == ISIS_CLI_CMD_NO_DEBUG_ALL))
            {
                *pu2ShowCmdFlag = ISIS_FALSE;
            }
            else
            {
                *pu2ShowCmdFlag = ISIS_TRUE;
            }

        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : IsisCliGetCxtIdFromCxtName                             */
/*                                                                            */
/* Description       : This function calls a VCM module to get                */
/*                     the context id with respect to Vrf name                */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext                            */
/*                     pu1IsisCxtName - vrf name                              */
/*                                                                            */
/* Output Parameters : pu4SysInstIdx    - Isis Context ID                     */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
IsisCliGetCxtIdFromCxtName (tCliHandle CliHandle,
                            UINT1 *pu1IsisCxtName, UINT4 *pu4SysInstIdx)
{
    UNUSED_PARAM (CliHandle);

    /* Call the vcm module to get the context id
     * with respect to the vrf name */
    if (UtilIsisIsVcmSwitchExist (pu1IsisCxtName, pu4SysInstIdx) !=
        ISIS_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsIsDistanceTableInfo
 * Description   :  This function displays the RMap filter info
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : -
 * Returns       : -
 *
 ******************************************************************************/
VOID
IsIsDistanceTableInfo (tCliHandle CliHandle)
{
    /* Distance */
    INT1                i1OutCome;
    INT4                i4Inst = 0;
    INT4                i4Value = 0;

    i1OutCome = nmhGetFirstIndexFsIsisPreferenceTable (&i4Inst);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (nmhGetFsIsisPreferenceValue (i4Inst, &i4Value) == SNMP_SUCCESS)
        {
            if (i4Value != ISIS_DEFAULT_PREFERENCE)
            {
                CliPrintf (CliHandle, "distance %d\r\n", i4Value);
            }
        }

        i1OutCome = nmhGetNextIndexFsIsisPreferenceTable (i4Inst, &i4Inst);
    }
}

#ifdef ROUTEMAP_WANTED
/*******************************************************************************
 * Function Name :  IsIsRMapFilterTableInfo
 * Description   :  This function displays the RMap filter info
 * Input (s)     : CliHandle     - CliContext
 * Output (s)    : -
 * Returns       : -
 *
 ******************************************************************************/
VOID
IsIsRMapFilterTableInfo (tCliHandle CliHandle)
{
    /* Filters */
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    INT1                i1OutCome;
    INT4                i4Inst = 0;
    INT4                i4FilterType;
    INT4                i4NextFilterType;
    INT4                i4Value = 0;
    tSNMP_OCTET_STRING_TYPE NextRouteMapName;
    UINT1               au1NextRMapName[RMAP_MAX_NAME_LEN + 4];

    MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
    RouteMapName.pu1_OctetList = au1RMapName;

    i1OutCome =
        nmhGetFirstIndexFsIsisDistInOutRouteMapTable (&i4Inst, &RouteMapName,
                                                      &i4FilterType);
    while (i1OutCome != SNMP_FAILURE)
    {
        switch (i4FilterType)
        {
            case FILTERING_TYPE_DISTANCE:
                if (nmhGetFsIsisDistInOutRouteMapValue
                    (i4Inst, &RouteMapName, i4FilterType,
                     &i4Value) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "distance %d route-map %s\r\n",
                               i4Value, RouteMapName.pu1_OctetList);
                }
                break;
            case FILTERING_TYPE_DISTRIB_IN:
                CliPrintf (CliHandle,
                           "distribute-list route-map %s in\r\n",
                           RouteMapName.pu1_OctetList);
                break;
        }

        MEMSET (&NextRouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1NextRMapName, 0, RMAP_MAX_NAME_LEN + 4);
        NextRouteMapName.pu1_OctetList = au1NextRMapName;

        i1OutCome =
            nmhGetNextIndexFsIsisDistInOutRouteMapTable (i4Inst, &i4Inst,
                                                         &RouteMapName,
                                                         &NextRouteMapName,
                                                         i4FilterType,
                                                         &i4NextFilterType);

        i4FilterType = i4NextFilterType;

        MEMCPY (RouteMapName.pu1_OctetList, NextRouteMapName.pu1_OctetList,
                RMAP_MAX_NAME_LEN + 4);
        RouteMapName.i4_Length = NextRouteMapName.i4_Length;
    }
}

/***************************************************************************/
/*  Function Name  : IsisCliSetDistribute                                  */
/*  Description    : This Routine Enable or Disable route map filtering.   */
/*  Input(s)       : 1. CliHandle        -  CLI context ID                 */
/*                   2. u4InstIdx        -  Instance index                 */
/*                   3. pu1RMapName      -  Route map name                 */
/*                   4. u1Status         -  Enable/Disable.                */
/*   Output(s)     : None.                                                 */
/*   Return Values : CLI_SUCCESS/CLI_FAILURE.                              */
/***************************************************************************/
INT4
IsisCliSetDistribute (tCliHandle CliHandle, UINT4 u4InstIdx,
                      UINT1 *pu1RMapName, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = CLI_ISIS_RMAP_ASSOC_FAILED;
    UINT1               u1FilterType;
    INT4                i4RowStatus;

    u1FilterType = FILTERING_TYPE_DISTRIB_IN;

    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);

        if (CLI_ENABLE == u1Status)
        {
            if (nmhGetFsIsisDistInOutRouteMapRowStatus
                ((INT4) u4InstIdx, &RouteMapName, u1FilterType,
                 &i4RowStatus) != SNMP_SUCCESS)
            {
                if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                    (&u4ErrorCode, (INT4) u4InstIdx, &RouteMapName,
                     u1FilterType, CREATE_AND_WAIT) == SNMP_SUCCESS)
                {
                    if (nmhSetFsIsisDistInOutRouteMapRowStatus
                        ((INT4) u4InstIdx, &RouteMapName, u1FilterType,
                         CREATE_AND_WAIT) == SNMP_SUCCESS)
                    {
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }
                }
            }
            else
            {
                u4ErrorCode = SNMP_ERR_NO_ERROR;
            }

            if (SNMP_ERR_NO_ERROR == u4ErrorCode)
            {
                if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                    (&u4ErrorCode, (INT4) u4InstIdx, &RouteMapName,
                     u1FilterType, ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsIsisDistInOutRouteMapRowStatus
                        ((INT4) u4InstIdx, &RouteMapName, u1FilterType,
                         ACTIVE) == SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
        else if (CLI_DISABLE == u1Status)
        {
            if (nmhGetFsIsisDistInOutRouteMapRowStatus
                ((INT4) u4InstIdx, &RouteMapName, u1FilterType,
                 &i4RowStatus) == SNMP_SUCCESS)
            {
                if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                    (&u4ErrorCode, (INT4) u4InstIdx, &RouteMapName,
                     u1FilterType, DESTROY) == SNMP_SUCCESS)
                {
                    if (nmhSetFsIsisDistInOutRouteMapRowStatus
                        ((INT4) u4InstIdx, &RouteMapName, u1FilterType,
                         DESTROY) == SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : IsisSetRouteDistance                                  */
/*                                                                         */
/*   Description   :                                                       */
/*                                                                         */
/*   Input(s)      : 1. CliHandle                                          */
/*                   2. u4InstIdx   -  Instance index                      */
/*                   3. u1Distance  - Distance value                       */
/*                   4. pu1RMapName - route map name                       */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
IsisSetRouteDistance (tCliHandle CliHandle, UINT4 u4InstIdx,
                      UINT1 u1Distance, UINT1 *pu1RMapName)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = CLI_ISIS_INV_ASSOC_ROUTE_MAP;
    INT4                i4Distance = 0x00FF & u1Distance;
    INT4                i4RowStatus;
    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);

        if (nmhGetFsIsisDistInOutRouteMapRowStatus
            ((INT4) u4InstIdx, &RouteMapName, FILTERING_TYPE_DISTANCE,
             &i4RowStatus) != SNMP_SUCCESS)
        {
            if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                (&u4ErrorCode, (INT4) u4InstIdx, &RouteMapName,
                 FILTERING_TYPE_DISTANCE, CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhSetFsIsisDistInOutRouteMapRowStatus
                    ((INT4) u4InstIdx, &RouteMapName, FILTERING_TYPE_DISTANCE,
                     CREATE_AND_WAIT) == SNMP_SUCCESS)
                {
                    u4ErrorCode = SNMP_ERR_NO_ERROR;
                }
            }
            else
            {
                u4CliError = CLI_ISIS_RMAP_ASSOC_FAILED;
            }
        }
        else
        {
            u4ErrorCode = SNMP_ERR_NO_ERROR;
        }

        if (SNMP_ERR_NO_ERROR == u4ErrorCode)
        {
            if (nmhTestv2FsIsisDistInOutRouteMapValue
                (&u4ErrorCode, (INT4) u4InstIdx, &RouteMapName,
                 FILTERING_TYPE_DISTANCE, i4Distance) == SNMP_SUCCESS)
            {
                if (nmhSetFsIsisDistInOutRouteMapValue
                    ((INT4) u4InstIdx, &RouteMapName, FILTERING_TYPE_DISTANCE,
                     i4Distance) == SNMP_SUCCESS)
                {
                    if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                        (&u4ErrorCode, (INT4) u4InstIdx, &RouteMapName,
                         FILTERING_TYPE_DISTANCE, ACTIVE) == SNMP_SUCCESS)
                    {
                        if (nmhSetFsIsisDistInOutRouteMapRowStatus
                            ((INT4) u4InstIdx, &RouteMapName,
                             FILTERING_TYPE_DISTANCE, ACTIVE) == SNMP_SUCCESS)
                        {
                            return CLI_SUCCESS;
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (nmhTestv2FsIsisPreferenceValue
            (&u4ErrorCode, (INT4) u4InstIdx, u1Distance) == SNMP_SUCCESS)
        {
            if (nmhSetFsIsisPreferenceValue ((INT4) u4InstIdx, u1Distance) ==
                SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4ErrorCode = CLI_ISIS_WRONG_DISTANCE_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : IsisSetNoRouteDistance                                */
/*                                                                         */
/*   Description   :                                                       */
/*                                                                         */
/*   Input(s)      : 1. CliHandle                                          */
/*                   2. u4InstIdx   -  Instance index                      */
/*                   2. pu1RMapName - route map name                       */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
IsisSetNoRouteDistance (tCliHandle CliHandle, UINT4 u4InstIdx,
                        UINT1 *pu1RMapName)
{
    UINT4               u4CliError = CLI_ISIS_INV_DISASSOC_ROUTE_MAP;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    INT4                i4RowStatus;
    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);
        if (nmhGetFsIsisDistInOutRouteMapRowStatus
            ((INT4) u4InstIdx, &RouteMapName, FILTERING_TYPE_DISTANCE,
             &i4RowStatus) == SNMP_SUCCESS)
        {
            if (nmhTestv2FsIsisDistInOutRouteMapValue
                (&u4ErrorCode, (INT4) u4InstIdx, &RouteMapName,
                 FILTERING_TYPE_DISTANCE,
                 ISIS_DEFAULT_PREFERENCE) == SNMP_SUCCESS)
            {
                if (nmhSetFsIsisDistInOutRouteMapValue
                    ((INT4) u4InstIdx, &RouteMapName, FILTERING_TYPE_DISTANCE,
                     ISIS_DEFAULT_PREFERENCE) == SNMP_SUCCESS)
                {
                    if (nmhTestv2FsIsisDistInOutRouteMapRowStatus
                        (&u4ErrorCode, (INT4) u4InstIdx, &RouteMapName,
                         FILTERING_TYPE_DISTANCE, DESTROY) == SNMP_SUCCESS)
                    {
                        if (nmhSetFsIsisDistInOutRouteMapRowStatus
                            ((INT4) u4InstIdx, &RouteMapName,
                             FILTERING_TYPE_DISTANCE, DESTROY) == SNMP_SUCCESS)
                        {
                            return CLI_SUCCESS;
                        }
                    }
                }
            }
        }
        else
        {
            u4CliError = CLI_ISIS_INV_ASSOC_ROUTE_MAP;
        }
    }
    else
    {                            /* 0 distance will set default value */
        if (nmhTestv2FsIsisPreferenceValue (&u4ErrorCode, (INT4) u4InstIdx, 0)
            == SNMP_SUCCESS)
        {
            if (nmhSetFsIsisPreferenceValue ((INT4) u4InstIdx, 0) ==
                SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4ErrorCode = CLI_ISIS_WRONG_DISTANCE_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

#endif /* ROUTEMAP_WANTED */

/*******************************************************************************
 *   Function Name : IsisCliSetAuthMode
 *   Description   : This function ENABLE the Authentication
 *   Input(s)      : CliHandle     - CliContext
 *                   u4InstIdx    - Instance index
 *   Output(s)     :
 *   Returns       : CLI_SUCCESS/CLI_FAILURE.
 *        
 *******************************************************************************/
INT4
IsisCliSetAuthMode (tCliHandle CliHandle, UINT4 u4InstIdx, UINT4 u4AuthMode,
                    UINT1 u1Level)
{
    UINT4               u4Error = SNMP_FAILURE;
    INT4                i4RetVal = 0;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if ((u1Level != ISIS_IMPORT_LEVEL_1) && (u1Level != ISIS_IMPORT_LEVEL_2))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if ((u1Level == ISIS_IMPORT_LEVEL_1) && (i4RetVal == ISIS_SUCCESS))
    {
        if (nmhTestv2FsIsisExtSysAreaAuthType (&u4Error, (INT4) u4InstIdx,
                                               (INT4) u4AuthMode) !=
            SNMP_SUCCESS)
        {
            if (u4Error == SNMP_ERR_WRONG_VALUE)
            {
                CliPrintf (CliHandle, "\r\n%%Invalid Value\n");
            }
            return CLI_FAILURE;
        }
        if (u4AuthMode == ISIS_AUTH_PASSWD)
        {

            if (nmhSetFsIsisExtSysAreaAuthType
                ((INT4) u4InstIdx, ISIS_AUTH_PASSWD) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        else if (u4AuthMode == ISIS_AUTH_MD5)
        {

            if (nmhSetFsIsisExtSysAreaAuthType ((INT4) u4InstIdx, ISIS_AUTH_MD5)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else if ((u1Level == ISIS_IMPORT_LEVEL_2) && (i4RetVal == ISIS_SUCCESS))
    {
        if (nmhTestv2FsIsisExtSysDomainAuthType (&u4Error, (INT4) u4InstIdx,
                                                 (INT4) u4AuthMode) !=
            SNMP_SUCCESS)
        {
            if (u4Error == SNMP_ERR_WRONG_VALUE)
            {
                CliPrintf (CliHandle, "\r\n%%Invalid Value\n");
            }
            return CLI_FAILURE;
        }
        if (u4AuthMode == ISIS_AUTH_PASSWD)
        {
            if (nmhSetFsIsisExtSysDomainAuthType
                ((INT4) u4InstIdx, ISIS_AUTH_PASSWD) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        else if (u4AuthMode == ISIS_AUTH_MD5)
        {

            if (nmhSetFsIsisExtSysDomainAuthType
                ((INT4) u4InstIdx, ISIS_AUTH_MD5) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *   Function Name : IsisCliDelAuthMode
 *   Description   : This function DISABLES the Authentication
 *   Input(s)      : CliHandle     - CliContext
 *                   u4InstIdx    - Instance index
 *   Output(s)     :
 *   Returns       : CLI_SUCCESS/CLI_FAILURE.
 *        
 *******************************************************************************/
INT4
IsisCliDelAuthMode (tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 u1Level)
{
    UINT4               u4Error = SNMP_FAILURE;
    INT4                i4RetVal = 0;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if ((u1Level != ISIS_IMPORT_LEVEL_1) && (u1Level != ISIS_IMPORT_LEVEL_2))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if (u1Level == ISIS_IMPORT_LEVEL_1)
    {
        if (nmhTestv2FsIsisExtSysAreaAuthType (&u4Error, (INT4) u4InstIdx,
                                               ISIS_AUTH_PASSWD) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsIsisExtSysAreaAuthType ((INT4) u4InstIdx, ISIS_AUTH_PASSWD)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u1Level == ISIS_IMPORT_LEVEL_2)
    {
        if (nmhTestv2FsIsisExtSysDomainAuthType (&u4Error, (INT4) u4InstIdx,
                                                 ISIS_AUTH_PASSWD) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsIsisExtSysDomainAuthType
            ((INT4) u4InstIdx, ISIS_AUTH_PASSWD) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    UNUSED_PARAM (i4RetVal);
    return CLI_SUCCESS;
}

/*******************************************************************************
 *   Function Name : IsisCliSetIfAuthMode
 *   Description   : This function ENABLE the Authentication
 *   Input(s)      : CliHandle     - CliContext
 *                   u4InstIdx    - Instance index
 *   Output(s)     :
 *   Returns       : CLI_SUCCESS/CLI_FAILURE.
 *        
 *******************************************************************************/
INT4
IsisCliSetIfAuthMode (tCliHandle CliHandle, UINT4 u4Ifdx, UINT4 u4AuthType,
                      UINT1 u1Level)
{
    UINT4               u4Error = SNMP_FAILURE;
    UINT1               u1CktLevel = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (u4Ifdx, 0, &pCktEntry);

    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsIsisExtCircLevelAuthType (&u4Error, (INT4) u4InstIdx,
                                             (INT4) u4CktIdx, (INT4) u1Level,
                                             (INT4) u4AuthType) != SNMP_SUCCESS)
    {
        if (u4Error == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Value\n");
        }
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisExtCircLevelAuthType ((INT4) u4InstIdx, (INT4) u4CktIdx,
                                          (INT4) u1Level,
                                          (INT4) u4AuthType) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *   Function Name : IsisCliDelIfAuthMode
 *   Description   : This function ENABLE the Authentication
 *   Input(s)      : CliHandle     - CliContext
 *                   u4InstIdx    - Instance index
 *   Output(s)     :
 *   Returns       : CLI_SUCCESS/CLI_FAILURE.
 *        
 *******************************************************************************/
INT4
IsisCliDelIfAuthMode (tCliHandle CliHandle, UINT4 u4IfIdx, UINT1 u1Level)
{
    UINT4               u4Error = SNMP_FAILURE;
    INT4                i4Result;
    UINT4               u4InstIdx = 0;
    UINT1               u1CktLevel = 0;
    UINT4               u4CktIdx = 0;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (u4IfIdx, 0, &pCktEntry);

    if (i4RetVal != ISIS_SUCCESS)
    {

        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    u4CktIdx = pCktEntry->u4CktIdx;
    u4InstIdx = pCktEntry->pContext->u4SysInstIdx;
    u1CktLevel = pCktEntry->u1CktLevel;

    if ((u1Level != u1CktLevel) && (u1CktLevel != ISIS_LEVEL12))
    {
        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsIsisExtCircLevelAuthType (&u4Error, (INT4) u4InstIdx,
                                             (INT4) u4CktIdx, (INT4) u1Level,
                                             ISIS_AUTH_PASSWD) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    i4Result =
        nmhSetFsIsisExtCircLevelAuthType ((INT4) u4InstIdx, (INT4) u4CktIdx,
                                          (INT4) u1Level, ISIS_AUTH_PASSWD);

    if (i4Result != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*ISIS GR CLI Routines*/

/*******************************************************************************
 * Function Name :  IsisCliSetGRRestartSupport                                      
 * Description   :  This function set and reset the restart support
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 *                  u4InstIdx     - Instance Id
 *                  i4IsisGRRestartSupport  - Restart Support
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliSetGRRestartSupport (tCliHandle CliHandle, UINT4 u4InstIdx,
                            INT4 i4IsisGRRestartSupport)
{

    UINT4               u4Error = 0;

    if (nmhTestv2FsIsisExtRestartSupport
        (&u4Error, (INT4) u4InstIdx, i4IsisGRRestartSupport) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisExtRestartSupport ((INT4) u4InstIdx, i4IsisGRRestartSupport)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisCliSetGRRestartReason                                      
 * Description   :  This function set the restart reason for planned restart
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 *                  u4InstIdx     - Instance Id
 *                  i4IsisGRRestartReason -Restart Reason
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliSetGRRestartReason (tCliHandle CliHandle, UINT4 u4InstIdx,
                           INT4 i4IsisGRRestartReason)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsIsisExtRestartReason
        (&u4Error, (INT4) u4InstIdx, i4IsisGRRestartReason) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisExtRestartReason ((INT4) u4InstIdx, i4IsisGRRestartReason)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisCliSetGRT1Interval                                       
 * Description   :  This function set th T1 Timer Interval
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 *                  u4InstIdx     - Instance Id
 *                  i4IsisGRT1TimerInterval -T1 timer Interval
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliSetGRT1Interval (tCliHandle CliHandle, UINT4 u4InstIdx,
                        INT4 i4IsisGRT1TimerInterval)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsIsisExtGRT1TimeInterval
        (&u4Error, (INT4) u4InstIdx, i4IsisGRT1TimerInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisExtGRT1TimeInterval
        ((INT4) u4InstIdx, i4IsisGRT1TimerInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisCliSetGRT2Interval                                       
 * Description   :  This function set th T2 Timer Interval
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 *                  u4InstIdx     - Instance Id
 *                  i4IsisGRT2TimerInterval  -T2 Timer Interval
 *                  u1Level   -Level
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliSetGRT2Interval (tCliHandle CliHandle, UINT4 u4InstIdx,
                        INT4 i4IsisGRT2TimerInterval, UINT1 u1Level)
{
    UINT4               u4Error = 0;

    if (u1Level == ISIS_LEVEL1)
    {
        if (nmhTestv2FsIsisExtGRT2TimeIntervalLevel1
            (&u4Error, (INT4) u4InstIdx,
             i4IsisGRT2TimerInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsIsisExtGRT2TimeIntervalLevel1
            ((INT4) u4InstIdx, i4IsisGRT2TimerInterval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (u1Level == ISIS_LEVEL2)
    {
        if (nmhTestv2FsIsisExtGRT2TimeIntervalLevel2
            (&u4Error, (INT4) u4InstIdx,
             i4IsisGRT2TimerInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsIsisExtGRT2TimeIntervalLevel2
            ((INT4) u4InstIdx, i4IsisGRT2TimerInterval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsIsisExtGRT2TimeIntervalLevel1
            (&u4Error, (INT4) u4InstIdx,
             i4IsisGRT2TimerInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsIsisExtGRT2TimeIntervalLevel1
            ((INT4) u4InstIdx, i4IsisGRT2TimerInterval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsIsisExtGRT2TimeIntervalLevel2
            ((INT4) u4InstIdx, i4IsisGRT2TimerInterval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisCliSetGRT3Interval                                        
 * Description   :  This function set th T3 Timer Interval
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 *                  u4InstIdx     - Instance Id
 *                  i4IsisGRT3TimerInterval -T3 Timer Interval 
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliSetGRT3Interval (tCliHandle CliHandle, UINT4 u4InstIdx,
                        INT4 i4IsisGRT3TimerInterval)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsIsisExtGRRestartTimeInterval
        (&u4Error, (INT4) u4InstIdx, i4IsisGRT3TimerInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisExtGRRestartTimeInterval
        (u4InstIdx, i4IsisGRT3TimerInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisCliSetGRT1RetryCount                                      
 * Description   :  This function set T1 Timer Retry Count
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 *                  u4InstIdx     - Instance Id
 *                  i4IsisGRMaxT1RetryCount  -T1 Timer Retry Count
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliSetGRT1RetryCount (tCliHandle CliHandle, UINT4 u4InstIdx,
                          INT4 i4IsisGRMaxT1RetryCount)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsIsisExtGRT1RetryCount
        (&u4Error, (INT4) u4InstIdx, i4IsisGRMaxT1RetryCount) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisExtGRT1RetryCount (u4InstIdx, i4IsisGRMaxT1RetryCount)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisCliSetGRHelperSupport                                      
 * Description   :  This function set and reset the helper support
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 *                  u4InstIdx     - Instance Id
 *                  i4IsisGRHelperSupport -Helper Support
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliSetGRHelperSupport (tCliHandle CliHandle, UINT4 u4InstIdx,
                           INT4 i4IsisGRHelperSupport)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsIsisExtHelperSupport
        (&u4Error, (INT4) u4InstIdx, i4IsisGRHelperSupport) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisExtHelperSupport (u4InstIdx, i4IsisGRHelperSupport)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisCliSetGRHelperMaxGraceTime                                       
 * Description   :  This function set the helper grace time limit
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 *                  u4InstIdx     - Instance Id
 *                  i4IsisGRHelperGRTimeLimit -Helper Grace time Limit
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliSetGRHelperMaxGraceTime (tCliHandle CliHandle, UINT4 u4InstIdx,
                                INT4 i4IsisGRHelperGRTimeLimit)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsIsisExtHelperGraceTimeLimit (&u4Error, (INT4) u4InstIdx,
                                                i4IsisGRHelperGRTimeLimit) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisExtHelperGraceTimeLimit
        (u4InstIdx, i4IsisGRHelperGRTimeLimit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisCliShowRestartStatus                                        
 * Description   :  This function
 *                     
 * Input (s)     :  CliHandle     - CliContext      
 * Output (s)    :  -                 
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/
INT4
IsisCliShowRestartStatus (tCliHandle CliHandle, UINT4 u4InstIdx)
{

    tIsisSysContext    *pContext = NULL;
    INT4                i4IsisGRRestartSupport;
    INT4                i4IsisGRInterval;
    INT4                i4IsisGRRestartStatus;
    INT4                i4ExitReason;
    INT4                i4RestartReason;
    INT4                i4HelperSupport;
    INT4                i4GrT1RetryCount;
    INT4                i4IsisGRMode;
    INT4                i4IsisExtRestartState;

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_FAILURE)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\n");
    if (nmhGetFsIsisExtRestartSupport
        ((INT4) u4InstIdx, &i4IsisGRRestartSupport) == SNMP_SUCCESS)
    {

        CliPrintf (CliHandle, "Restart Support :\r\n");
        if (i4IsisGRRestartSupport == ISIS_GR_RESTART_NONE)
        {
            CliPrintf (CliHandle, "    Non-Stop Forwarding disabled\r\n");
        }
        else if (i4IsisGRRestartSupport == ISIS_GR_RESTART_PLANNED)
        {
            CliPrintf (CliHandle,
                       "    Planned Non-Stop Forwarding enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "    Planned & Unplanned Non-Stop Forwarding enabled\r\n");
        }
    }
    if (nmhGetFsIsisExtGRT1TimeInterval ((INT4) u4InstIdx, &i4IsisGRInterval)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "T1 Timer Interval limit: %d\r\n",
                   i4IsisGRInterval);
    }
    if (nmhGetFsIsisExtGRT2TimeIntervalLevel1
        ((INT4) u4InstIdx, &i4IsisGRInterval) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "T2 Timer Interval limit for Level1: %d\r\n",
                   i4IsisGRInterval);
    }
    if (nmhGetFsIsisExtGRT2TimeIntervalLevel2
        ((INT4) u4InstIdx, &i4IsisGRInterval) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "T2 Timer Interval limit for Level2: %d\r\n",
                   i4IsisGRInterval);
    }
    if (nmhGetFsIsisExtGRRestartTimeInterval
        ((INT4) u4InstIdx, &i4IsisGRInterval) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Restart-interval(T3) limit: %d\r\n",
                   i4IsisGRInterval);
    }

    if (nmhGetFsIsisExtGRT1RetryCount ((INT4) u4InstIdx, &i4GrT1RetryCount) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "T1 Timer Retry Count: %d\r\n", i4GrT1RetryCount);
    }
    if (nmhGetFsIsisExtGRMode ((INT4) u4InstIdx, &i4IsisGRMode) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Restart Mode :\r\n");
        if (i4IsisGRMode == ISIS_GR_HELPER)
        {
            CliPrintf (CliHandle, "    Acting as Helper Router\r\n");
        }
        else if (i4IsisGRMode == ISIS_GR_RESTARTER)
        {
            CliPrintf (CliHandle, "    Acting as Restarting Router\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "    Acting as Normal Router\r\n");
        }
    }
    if (nmhGetFsIsisExtRestartSupport ((INT4) u4InstIdx, &i4IsisGRRestartStatus)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Restart Status :\r\n");
        if (i4IsisGRRestartSupport == ISIS_GR_RESTART_NONE)
        {
            CliPrintf (CliHandle, "    Non-Stop Forwarding disabled\r\n");
        }
        else if (i4IsisGRRestartSupport == ISIS_GR_RESTART_PLANNED)
        {
            CliPrintf (CliHandle, "    Planned Non-Stop Forwarding\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "    Unplanned Non-Stop Forwarding\r\n");
        }
    }
    if (nmhGetFsIsisExtRestartExitReason ((INT4) u4InstIdx, &i4ExitReason) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Last NSF restart:\r\n");
        switch (i4ExitReason)
        {
            case ISIS_GR_RESTART_COMPLETED:
                CliPrintf (CliHandle, "    Completed successfully\r\n");
                break;

            case ISIS_GR_RESTART_TIMEDOUT:
                CliPrintf (CliHandle, "    Timed out\r\n");
                break;

            case ISIS_GR_RESTART_TOP_CHG:
                CliPrintf (CliHandle, "    Resulted in topology change\r\n");
                break;

            default:
                CliPrintf (CliHandle, "    No Restart Happened\r\n");
                break;
        }
    }
    if (nmhGetFsIsisExtRestartReason ((INT4) u4InstIdx, &i4RestartReason) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Restart Reason is:\r\n");
        switch (i4RestartReason)
        {
            case ISIS_GR_SW_RESTART:
                CliPrintf (CliHandle, "    Software Restart \r\n");
                break;
            case ISIS_GR_SW_UPGRADE:
                CliPrintf (CliHandle, "    Software Upgrade/Reload\r\n");
                break;
            case ISIS_GR_SW_RED:
                CliPrintf (CliHandle, "    SwitchToRedundant \r\n");
                break;
            default:
                CliPrintf (CliHandle, "    Unknown\r\n");
                break;
        }
    }
    if (nmhGetFsIsisExtHelperSupport ((INT4) u4InstIdx, &i4HelperSupport) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Helper Support:\r\n");
        if (i4HelperSupport == ISIS_GR_HELPER_NONE)
        {
            CliPrintf (CliHandle, "    No Helper Support\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "    Support for both Restart and Start\r\n");
        }
    }
    if (nmhGetFsIsisExtHelperGraceTimeLimit
        ((INT4) u4InstIdx, &i4IsisGRInterval) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Helper Grace Time Limit: %d\r\n",
                   i4IsisGRInterval);
    }

    if (nmhGetFsIsisRestartState (&i4IsisExtRestartState) == SNMP_SUCCESS)
    {

        CliPrintf (CliHandle, "Graceful Restart State:");

        if (i4IsisExtRestartState == ISIS_GR_ADJ_RESTART)
        {
            CliPrintf (CliHandle, "ISIS_GR_ADJ_RESTART \r\n");
        }
        else if (i4IsisExtRestartState == ISIS_GR_ADJ_SEEN_RA)
        {
            CliPrintf (CliHandle, "ISIS_GR_ADJ_SEEN_RA \r\n");
        }
        else if (i4IsisExtRestartState == ISIS_GR_ADJ_SEEN_CSNP)
        {
            CliPrintf (CliHandle, "ISIS_GR_ADJ_SEEN_CSNP \r\n");
        }
        else if (i4IsisExtRestartState == ISIS_GR_SPF_L1WAIT
                 || i4IsisExtRestartState == ISIS_GR_SPF_L2WAIT)
        {
            CliPrintf (CliHandle, "ISIS_GR_SPF_WAIT \r\n");
        }
        else if (i4IsisExtRestartState == ISIS_GR_SPF_L1COMPLETE
                 || i4IsisExtRestartState == ISIS_GR_SPF_L2COMPLETE)
        {
            CliPrintf (CliHandle, "ISIS_GR_SPF_DONE \r\n");
        }
        else if (i4IsisExtRestartState == ISIS_GR_OVERLOAD)
        {
            CliPrintf (CliHandle, "ISIS_GR_OVERLOAD \r\n");
        }
        else if (i4IsisExtRestartState == ISIS_GR_NORMAL_ROUTER)
        {
            CliPrintf (CliHandle, "ISIS_GR_NORMAL_ROUTER \r\n");
        }

        CliPrintf (CliHandle, "\r\n");

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name : IsisConfigRedistribute                                    */
/* CLI Command   : (a) redistribute <static|connected|rip|ospf|bgp|all>      */
/*               : (b) no redistribute <static|connected|rip|ospf|bgp|all>   */
/* Description   : This command controls the redistribution of routes to     */
/*               : ISIS from various protocols. Command (a) specifies the     */
/*               : the protocols from which the routes can be redistributed  */
/*               : into ISIS. Command (b) disables redistribution of the      */
/*               : specified protocols routes into ISIS.                      */
/* Input(s)      : Protocol from which routes are redistributed into ISIS     */
/*               : (i4RedisProto). i4RedisProto can take the following value.*/
/*               : ISIS_IMPORT_DIRECT  - For redistributing Local    */
/*               :                       routes.                     */
/*               : ISIS_IMPORT_STATIC  - For redistributing static   */
/*               :                       routes.                     */
/*               : ISIS_IMPORT_RIP     - For redistributing RIP      */
/*               :                       routes.                     */
/*               : ISIS_IMPORT_OSPF    - For redistributing OSPF     */
/*               :                       routes.                     */
/*               : ISIS_IMPORT_BGP    -  For redistributing BGP     */
/*               :                       routes.                     */
/*               : ISIS_IMPORT_ALL     - For redistributing all RPS  */
/*                                               routes                      */
/*               : u1Set = ISIS_CLI_VAL_SET for command (a) and              */
/*                 u1Set = ISIS_CLI_VAL_NO for command (b)                   */
/* Output(s)     : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : CLI_SUCCESS    - if operation is success                 */
/*               : CLI_FAILURE - if operation fails  */
/*****************************************************************************/
INT4
IsisConfigRedistribute (tCliHandle CliHandle, INT4 i4RedisProto, UINT1 u1Set,
                        UINT1 *pu1RMapName, UINT4 u4Metric, INT4 i4ImportType,
                        INT4 i4InstIndx)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4RrdMetricVal = 0;
    UINT4               u4Count = 0;
    INT1                i1RetVal = 0;
    tSNMP_OCTET_STRING_TYPE RMapNameOct;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    INT4                i4ProtocolId = 0;
    INT4                i4Flag = 0;
    INT4                i4TempProto = 0;
    tIsisSysContext    *pContext = NULL;

    i1RetVal =
        nmhTestv2FsIsisRRDImportLevel (&u4ErrorCode, i4InstIndx, i4ImportType);
    if (i1RetVal == SNMP_FAILURE)
    {

        return CLI_FAILURE;
    }
    if (i4RedisProto == ISIS_IMPORT_STATIC)
    {
        i4ProtocolId = ISIS_METRIC_INDEX_STATIC;
    }
    else if (i4RedisProto == ISIS_IMPORT_DIRECT)
    {
        i4ProtocolId = ISIS_METRIC_INDEX_DIRECT;
    }
    else if (i4RedisProto == ISIS_IMPORT_RIP)
    {
        i4ProtocolId = ISIS_METRIC_INDEX_RIP;
    }
    else if (i4RedisProto == ISIS_IMPORT_OSPF)
    {
        i4ProtocolId = ISIS_METRIC_INDEX_OSPF;
    }
    else if (i4RedisProto == ISIS_IMPORT_BGP)
    {
        i4ProtocolId = ISIS_METRIC_INDEX_BGP;
    }
    else if (i4RedisProto == ISIS_IMPORT_ALL)
    {
        i4ProtocolId = ISIS_MAX_METRIC_INDEX + 1;
    }
    else
    {
        CLI_SET_ERR (CLI_ISIS_INVALID_REDIS_PROTO_ENABLE_MASK_ERR);
        return CLI_FAILURE;
    }
    if (u1Set == ISIS_CLI_VAL_NO)
    {
        if (u4Metric != (UINT4) ISIS_CLI_INVALID_METRIC)
        {
            if (i4ProtocolId == (ISIS_MAX_METRIC_INDEX + 1))
            {
                for (u4Count = 0; u4Count <= ISIS_MAX_METRIC_INDEX; u4Count++)
                {
                    i1RetVal =
                        nmhGetFsIsisRRDMetricValue (i4InstIndx, u4Count,
                                                    &u4RrdMetricVal);
                    if ((u4RrdMetricVal != u4Metric) || (u4Metric == 0)
                        || (u4Metric == 0))
                    {
                        i4Flag = CLI_FAILURE;
                        break;
                    }
                }
                if (i4Flag == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%% Previously configured RRD Metric did not match/Invalid Metric\n\r");
                    return CLI_FAILURE;
                }
            }
            else
            {
                i1RetVal =
                    nmhGetFsIsisRRDMetricValue (i4InstIndx, i4ProtocolId,
                                                &u4RrdMetricVal);
                if ((u4RrdMetricVal != u4Metric) || (u4Metric == 0))
                {
                    CliPrintf (CliHandle,
                               "\r\n%% Previously configured RRD Metric did not match/Invalid Metric\n\r");
                    return CLI_FAILURE;
                }
            }
        }
    }
    if (pu1RMapName != NULL)
    {
        RMapNameOct.pu1_OctetList = au1RMapName;
        RMapNameOct.i4_Length = (INT4) STRLEN (pu1RMapName);
        STRCPY (RMapNameOct.pu1_OctetList, pu1RMapName);

        if (nmhTestv2FsIsisRRDRouteMapName
            (&u4ErrorCode, i4InstIndx, &RMapNameOct) == SNMP_FAILURE)
        {
            if (u1Set == ISIS_CLI_VAL_SET)
            {
                CLI_SET_ERR (CLI_ISIS_RMAP_ASSOC_FAILED);
            }
            else
            {
                CLI_SET_ERR (CLI_ISIS_INV_DISASSOC_ROUTE_MAP);
            }
            return CLI_FAILURE;
        }

        if (u1Set == ISIS_CLI_VAL_NO)
        {
            MEMSET (&RMapNameOct, 0, sizeof (RMapNameOct));
        }
        if (nmhSetFsIsisRRDRouteMapName (i4InstIndx, &RMapNameOct) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if ((u4Metric != (UINT4) ISIS_CLI_INVALID_METRIC)
        || (u1Set == ISIS_CLI_VAL_NO))
    {
        if ((u4Metric < (UINT4) ISIS_MIN_METRIC_MT)
            && (u1Set == ISIS_CLI_VAL_SET))
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Metric\n\r");
            return CLI_FAILURE;
        }
        else if (u1Set == ISIS_CLI_VAL_NO)
        {
            u4Metric = 0;
        }
        if (i4ProtocolId == (ISIS_MAX_METRIC_INDEX + 1))
        {
            for (i4ProtocolId = ISIS_METRIC_INDEX_STATIC;
                 i4ProtocolId <= ISIS_MAX_METRIC_INDEX; i4ProtocolId++)
            {
                i1RetVal =
                    nmhTestv2FsIsisRRDMetricValue (&u4ErrorCode, i4InstIndx,
                                                   i4ProtocolId, u4Metric);
                if (i1RetVal == SNMP_FAILURE)
                {
                    if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                    {
                        CliPrintf (CliHandle, "\r\n%% Invalid Metric\n\r");
                    }
                    return CLI_FAILURE;
                }

                i1RetVal =
                    nmhSetFsIsisRRDMetricValue (i4InstIndx, i4ProtocolId,
                                                u4Metric);
                if (i1RetVal == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }
        else
        {
            i1RetVal =
                nmhTestv2FsIsisRRDMetricValue (&u4ErrorCode, i4InstIndx,
                                               i4ProtocolId, u4Metric);
            if (i1RetVal == SNMP_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                {
                    CliPrintf (CliHandle, "\r\n%% Invalid Metric\n\r");
                }
                return CLI_FAILURE;
            }

            i1RetVal =
                nmhSetFsIsisRRDMetricValue (i4InstIndx, i4ProtocolId, u4Metric);
            if (i1RetVal == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    if (u1Set != ISIS_CLI_VAL_NO)
    {
        if (IsisCtrlGetSysContext ((UINT4) i4InstIndx, &pContext) ==
            ISIS_SUCCESS)
        {
            if ((pContext->RRDInfo.u1RRDImportType != i4ImportType))
            {
                i4TempProto = (INT4) pContext->RRDInfo.u4RRDSrcProtoMask;
                i1RetVal = nmhTestv2FsIsisRRDProtoMaskForDisable (&u4ErrorCode,
                                                                  i4InstIndx,
                                                                  (INT4)
                                                                  pContext->
                                                                  RRDInfo.
                                                                  u4RRDSrcProtoMask);

                if (i1RetVal == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                i1RetVal =
                    nmhSetFsIsisRRDProtoMaskForDisable (i4InstIndx,
                                                        (INT4) pContext->
                                                        RRDInfo.
                                                        u4RRDSrcProtoMask);
                if (i1RetVal == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                i4RedisProto |= i4TempProto;
            }
        }
    }

    if (u1Set == ISIS_CLI_VAL_SET)
    {
        i1RetVal = nmhSetFsIsisRRDImportLevel (i4InstIndx, i4ImportType);
        if (i1RetVal == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    switch (u1Set)
    {
        case ISIS_CLI_VAL_SET:
            i1RetVal =
                nmhTestv2FsIsisRRDProtoMaskForEnable (&u4ErrorCode, i4InstIndx,
                                                      i4RedisProto);
            if (i1RetVal == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            i1RetVal =
                nmhSetFsIsisRRDProtoMaskForEnable (i4InstIndx, i4RedisProto);
            if (i1RetVal == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;

        case ISIS_CLI_VAL_NO:
            i1RetVal =
                nmhTestv2FsIsisRRDProtoMaskForDisable (&u4ErrorCode, i4InstIndx,
                                                       i4RedisProto);
            if (i1RetVal == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            i1RetVal =
                nmhSetFsIsisRRDProtoMaskForDisable (i4InstIndx, i4RedisProto);
            if (i1RetVal == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;
        default:
            break;
    }
    CliPrintf (CliHandle, "\rCAUTION: Configured ISIS level/Route-map applies"
               " to all protocols. \r\nDefault ISIS level is level-2.\r\n");
    return CLI_SUCCESS;
}

INT4
is_debug_cmd (UINT4 u4Command)
{
    if (((u4Command == ISIS_CLI_CMD_DEBUG_ADJN) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_DECN) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_UPDT) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_INTERFACE) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_CONTROL) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_TIMER) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_FAULT) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_RTMI) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_DLLI) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_TRFR) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_SNMP) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_UTIL) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_CRITICAL) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_ENTRYEXIT) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_DUMP) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_DETAIL) ||
         (u4Command == ISIS_CLI_CMD_DEBUG_ALL) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_ADJN) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_DECN) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_UPDT) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_INTERFACE) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_CONTROL) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_TIMER) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_FAULT) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_RTMI) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_DLLI) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_TRFR) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_SNMP) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_UTIL) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_CRITICAL) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_ENTRYEXIT) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_DUMP) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_DETAIL) ||
         (u4Command == ISIS_CLI_CMD_NO_DEBUG_ALL)))
    {
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/*******************************************************************************
 * Function Name :  IsisCliPassiveInterface
 * Description   :  This function enables/disables the Passive interface on a
 *                   IS-IS enabled Interface
 *
 * Input (s)     :  CliHandle           - CliContext
 *                  i4IfIndex           - Interface Index Id
 *                  u4InstId            - Instance ID
 *                  u4ValIsisCircPassiveCircuit - ENABLE/DISABLE
 * Output (s)    :  -
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/

INT4
IsisCliPassiveInterface (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4InstIdx,
                         UINT4 u4ValIsisCircPassiveCircuit)
{
    INT4                i4RetVal = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4TempInstIdx = 0;
    INT4                i4ExistState = 0;
    INT4                i4InstIdx = (INT4) u4InstIdx;
    INT4                i4CktIdx = 0;
    INT4                i4IsLoopBackAddress = 0;
    INT4                i4RetValIsisCircPassiveCircuit = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    i4RetVal = IsisAdjGetCktRecWithIfIdx ((UINT4) i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return CLI_FAILURE;
    }

    i4CktIdx = (INT4) pCktEntry->u4CktIdx;

    u4TempInstIdx = pCktEntry->pContext->u4SysInstIdx;

    if (u4TempInstIdx != u4InstIdx)
    {
        CLI_SET_ERR (CLI_ISIS_RETRIEVE_RECORD_ERR);
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIsisCircExistState (i4InstIdx, i4CktIdx, &i4ExistState);

    if (i4RetVal != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_RETRIEVE_RECORD_ERR);
        return CLI_FAILURE;
    }

    i4IsLoopBackAddress = CfaIsLoopBackIntf (i4IfIndex);

    /* Loop back Interfaces cannot be made active */

    if ((i4IsLoopBackAddress == TRUE)
        && (u4ValIsisCircPassiveCircuit == ISIS_FALSE))
    {
        CLI_SET_ERR (CLI_ISIS_NO_LOOPBACK_ACTIVE);
        return CLI_FAILURE;
    }

    if (nmhGetIsisCircPassiveCircuit (i4InstIdx, i4CktIdx,
                                      &i4RetValIsisCircPassiveCircuit)
        == SNMP_SUCCESS)
    {
        if (i4RetValIsisCircPassiveCircuit ==
            (INT4) u4ValIsisCircPassiveCircuit)
        {
            return CLI_SUCCESS;
        }
    }

    if (i4ExistState == ISIS_ACTIVE)
    {

        if (nmhTestv2IsisCircExistState (&u4ErrorCode, i4InstIdx, i4CktIdx,
                                         ISIS_NOT_IN_SER) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIsisCircExistState (i4InstIdx, i4CktIdx,
                                      ISIS_NOT_IN_SER) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    if (nmhTestv2IsisCircPassiveCircuit (&u4ErrorCode, i4InstIdx,
                                         i4CktIdx,
                                         (INT4) u4ValIsisCircPassiveCircuit) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircPassiveCircuit (i4InstIdx, i4CktIdx,
                                      (INT4) u4ValIsisCircPassiveCircuit) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IsisCircExistState (&u4ErrorCode, i4InstIdx, i4CktIdx,
                                     ISIS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIsisCircExistState (i4InstIdx, i4CktIdx,
                                  ISIS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name :  IsisCliPassiveInterfaceDefault
 * Description   :  This function enables/disables all the Passive interface on a
 *                   IS-IS enabled Interface
 *
 * Input (s)     :  CliHandle                   - CliContext
 *                  u4Inst                      -  Instance in which values 
 *                                                to configured
 *                  u4ValIsisCircPassiveCircuit - Value to be set
 * Output (s)    :  
 * Returns       :  CLI_SUCCESS/CLI_FAILURE.
 ******************************************************************************/

INT4
IsisCliPassiveInterfaceDefault (tCliHandle CliHandle, UINT4 u4Inst,
                                UINT4 u4ValIsisCircPassiveCircuit)
{

    INT4                i4Instances = 0;
    INT4                i4IsisCircIndex = 0;
    INT4                i4RetVal = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4ExistState = 0;
    INT4                i4RetValIsisCircPassiveCircuit = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisSysContext    *pTempContext = NULL;

    pTempContext = gpIsisInstances;
    if (pTempContext == NULL)
    {
        return CLI_FAILURE;
    }

    while (pTempContext != NULL)
    {
        if (pTempContext->u4SysInstIdx == u4Inst)
        {
            pContext = pTempContext;
            break;
        }
        pContext = pTempContext;
        pTempContext = pTempContext->pNext;
    }

    pCktRec = pContext->CktTable.pCktRec;

    if (pCktRec == NULL)
    {
        CLI_SET_ERR (CLI_ISIS_NO_ENTRY_ERROR);
        return CLI_FAILURE;
    }
    while (pCktRec != NULL)
    {

        i4Instances = (INT4) pContext->u4SysInstIdx;

        i4IsisCircIndex = (INT4) pCktRec->u4CktIdx;

        i4RetVal = nmhGetIsisCircExistState (i4Instances,
                                             i4IsisCircIndex, &i4ExistState);

        if (i4RetVal != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_ISIS_RETRIEVE_RECORD_ERR);
            return CLI_FAILURE;
        }

        i4RetVal = nmhGetIsisCircPassiveCircuit (i4Instances, i4IsisCircIndex,
                                                 &i4RetValIsisCircPassiveCircuit);

        if (i4RetVal != SNMP_SUCCESS)
        {
            CLI_SET_ERR (CLI_ISIS_RETRIEVE_RECORD_ERR);
            return CLI_FAILURE;
        }

        if (i4RetValIsisCircPassiveCircuit !=
            (INT4) u4ValIsisCircPassiveCircuit)
        {

            if (i4ExistState == ISIS_ACTIVE)
            {

                if (nmhTestv2IsisCircExistState (&u4ErrorCode, i4Instances,
                                                 i4IsisCircIndex,
                                                 ISIS_NOT_IN_SER) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetIsisCircExistState (i4Instances, i4IsisCircIndex,
                                              ISIS_NOT_IN_SER) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

            }

            if (nmhTestv2IsisCircPassiveCircuit (&u4ErrorCode,
                                                 i4Instances,
                                                 i4IsisCircIndex,
                                                 (INT4)
                                                 u4ValIsisCircPassiveCircuit) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetIsisCircPassiveCircuit (i4Instances,
                                              i4IsisCircIndex,
                                              (INT4)
                                              u4ValIsisCircPassiveCircuit) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhTestv2IsisCircExistState (&u4ErrorCode, i4Instances,
                                             i4IsisCircIndex, ISIS_ACTIVE)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetIsisCircExistState (i4Instances, i4IsisCircIndex,
                                          ISIS_ACTIVE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

        }
        pCktRec = pCktRec->pNext;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name :  IsisCliSetMetricStyle
  Description   :  This function set the Metric Style
 
  Input (s)     :  CliHandle     - CliContext
                   u4InstIdx     - Instance Id
                   i4IsisMetricStyle - Metric Style Value
  Output (s)    :  -
  Returns       :  CLI_SUCCESS/CLI_FAILURE.
*******************************************************************************/
INT4
IsisCliSetMetricStyle (tCliHandle CliHandle, UINT4 u4InstIdx,
                       INT4 i4IsisSetMetricStyle)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsIsisMetricStyle
        (&u4Error, (INT4) u4InstIdx, i4IsisSetMetricStyle) == SNMP_FAILURE)
    {
        if (u4Error == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Value\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n%% Only Wide Metric style is allowed in Multi-topology\n");
        }
        return CLI_FAILURE;
    }
    if (nmhSetFsIsisMetricStyle
        ((INT4) u4InstIdx, i4IsisSetMetricStyle) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (i4IsisSetMetricStyle == ISIS_STYLE_NARROW_METRIC)
    {
        CliPrintf (CliHandle, "\rCAUTION: If configured metric is > 63,"
                   "then it will be capped at 63.\r\n");
    }

    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name :  IsisIsAllinterfacePassive
 * Description   :  This function returns whether all the IS-IS Interfaces 
 *                  on the circuit table are Passive or not
 *
 * Input (s)     :  pContext  - Current Instance
 * Output (s)    :  -
 * Returns       :  i4RetVal ISIS_TRUE / ISIS_FALSE
 ******************************************************************************/

INT4
IsisIsAllinterfacePassive (tIsisSysContext * pContext)
{

    INT4                i4RetVal = ISIS_FALSE;
    INT4                i4IfIndex = 0;
    tIsisCktEntry      *pCktEntry = NULL;
    INT4                i4CurrInstances = 0;
    INT4                i4CurrIsisCircIndex = 0;
    tIsisCktEntry      *pCktRec = NULL;

    pCktRec = pContext->CktTable.pCktRec;

    while (pCktRec != NULL)
    {

        i4CurrInstances = pContext->u4SysInstIdx;
        i4CurrIsisCircIndex = pCktRec->u4CktIdx;

        nmhGetIsisCircIfIndex (i4CurrInstances, i4CurrIsisCircIndex,
                               &i4IfIndex);

        i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);

        if ((i4RetVal != ISIS_SUCCESS) || (pCktEntry == NULL))
        {
            CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
            return CLI_FAILURE;
        }
        if ((pCktEntry->bCktPassiveCkt) == ISIS_TRUE)
        {
            i4RetVal = ISIS_TRUE;
        }
        else
        {
            i4RetVal = ISIS_FALSE;
            break;
        }

        pCktRec = pCktRec->pNext;

    }
    return i4RetVal;

}

/******************************************************************************
 * Function Name :  IsIsCktTablePassiveInterfaceInfo
 * Description   :  This function displays the Passive interface in
 *                  the circuit table.
 *
 * Input (s)     :  CliHandle           - CliContext
 *                  pContext            - Current Instance
 * Output (s)    :  -
 * Returns       :  VOID
 ******************************************************************************/

VOID
IsIsCktTablePassiveInterfaceInfo (tCliHandle CliHandle,
                                  tIsisSysContext * pContext)
{

    tIsisCktEntry      *pCktEntry = NULL;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4CktIfIdx = 0;

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    pCktEntry = pContext->CktTable.pCktRec;

    while (pCktEntry != NULL)
    {
        if ((pCktEntry->bCktPassiveCkt) == ISIS_TRUE)
        {
            if (CfaCliConfGetIfName
                (pCktEntry->u4CktIfIdx, (INT1 *) au1NameStr) != CFA_SUCCESS)
            {
                pCktEntry = pCktEntry->pNext;
                continue;
            }
            i4CktIfIdx = (INT4) pCktEntry->u4CktIfIdx;
            if (CfaIsLoopBackIntf (i4CktIfIdx) == FALSE)
            {
                CliPrintf (CliHandle, "passive-interface %s \r\n", au1NameStr);
                FilePrintf (CliHandle, "passive-interface %s \r\n", au1NameStr);
            }
        }

        pCktEntry = pCktEntry->pNext;

    }
    return;
}

/******************************************************************************
 * Function Name :  IsisRouterIsisInfo
 * Description   :  This function displays the System Table Informations
 *
 * Input (s)     :  CliHandle           - CliContext
 *                  i4Inst              - Current Instance
 * Output (s)    :  -
 * Returns       :  VOID
 ******************************************************************************/

VOID
IsisRouterIsisInfo (tCliHandle CliHandle, INT4 i4Inst)
{
    UINT1               au1IsisCxtName[ISIS_MAX_CONTEXT_STR_LEN];

    if (i4Inst == ISIS_DEFAULT_CXT_ID)
    {
        CliPrintf (CliHandle, "!\r\n");
        FilePrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "router isis \r\n");
        FilePrintf (CliHandle, "router isis \r\n");
        FilePrintf (CliHandle, "\r\n");
    }
    else
    {
        UtilIsisGetVcmAliasName (i4Inst, au1IsisCxtName);
        CliPrintf (CliHandle, "!\r\n");
        FilePrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "\rrouter isis vrf  %s \n", au1IsisCxtName);
        FilePrintf (CliHandle, "\rrouter isis vrf  %s \n", au1IsisCxtName);
        FilePrintf (CliHandle, "\r\n");
    }

}

#ifdef RM_WANTED
/*****************************************************************************/
/* Function Name      : IsisCliGetShowCmdOutputToFile                        */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISIS_SUCCESS/ISIS_FAILURE                            */
/*****************************************************************************/
INT4
IsisCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    INT4                i4Cmd = 0;
    CHR1               *au1IsisShowCmdList[ISIS_DYN_MAX_CMDS]
        = { "show ip isis database detailed > ",
        "snmpwalk mib name isisCircTable >> ",
        "snmpwalk mib name isisISAdjTable >> ",
        "snmpwalk mib name fsIsisExtAdjTable >> ",
        "snmpwalk mib name isisISAdjIPAddrTable >> ",
        "snmpwalk mib name isisAreaAddrTable >> ",
        "snmpwalk mib name fsIsisExtIPRATable >> ",
        "snmpwalk mib name isisManAreaAddrTable >> ",
        "snmpwalk mib name isisSummAddrTable >> ",
        "snmpwalk mib name fsIsisExtSummAddrTable >> ",
        "snmpwalk mib name fsIsisExtIPIfAddrTable >> ",
        "snmpwalk mib name fsIsisExtSysTable >> ",
        "snmpwalk mib name isisSysTable >> ",
        "show ip isis config-database >> ",
        "show ip isis actual-database >> ",
        "show ip isis sync-status >> ",
        "snmpwalk mib name  isisSysProtSuppTable >> ",
        "show ip isis circuits >> "
    };
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return ISIS_FAILURE;
        }
    }
    for (i4Cmd = 0; i4Cmd < ISIS_DYN_MAX_CMDS; i4Cmd++)
    {
        if (CliGetShowCmdOutputToFile ((UINT1 *) pu1FileName,
                                       (UINT1 *) au1IsisShowCmdList[i4Cmd]) ==
            CLI_FAILURE)
        {
            return ISIS_FAILURE;
        }
    }
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IsisCliCalcSwAudCheckSum                             */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISIS_SUCCESS/ISIS_FAILURE                            */
/*****************************************************************************/
INT4
IsisCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[ISIS_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, ISIS_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return ISIS_FAILURE;
    }
    MEMSET (ai1Buf, 0, ISIS_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (IsisCliReadLineFromFile (i4Fd, ai1Buf, ISIS_CLI_MAX_GROUPS_LINE_LEN,
                                    &i2ReadLen) != ISIS_CLI_EOF)

    {
        if ((i2ReadLen > 0) &&
            (NULL == STRSTR (ai1Buf, "isisCircOutCtrlPDUs")) &&
            (NULL == STRSTR (ai1Buf, "isisCircInCtrlPDUs")) &&
            (NULL == STRSTR (ai1Buf, "isisCircUpTime")) &&
            (NULL == STRSTR (ai1Buf, "isisISAdjUpTime")) &&
            (NULL == STRSTR (ai1Buf, "isisAreaAddr")) &&
            (NULL == STRSTR (ai1Buf, "fsIsisExtSysActiveCkts")) &&
            (NULL == STRSTR (ai1Buf, "fsIsisExtSysOperState")) &&
            (NULL == STRSTR (ai1Buf, "fsIsisExtSysDroppedPDUs")))
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', ISIS_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);
    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return ISIS_FAILURE;
    }
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IsisCliReadLineFromFile                              */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISIS_SUCCESS/ISIS_FAILURE                            */
/*****************************************************************************/
INT1
IsisCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                         INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (ISIS_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (ISIS_CLI_EOF);
}
#endif
/******************************************************************************/
/* Function Name     : ShowIsisProtocolsInfo                                  */
/*                                                                            */
/* Description       : This function displays ISIS routing protocol           */
/*                     information                                            */
/*                                                                            */
/* Input Parameters  : CliHandle   - CliContext ID                            */
/*                     u4RtmCxtId - VRF Id                                    */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
ShowIsisProtocolsInfo (tCliHandle CliHandle)
{

    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pAddr = NULL;
    UINT1               au1AreaAddress[ISIS_AREA_ADDR_LEN];
    UINT1               au1PrevAreaAddress[ISIS_AREA_ADDR_LEN];
    UINT1               au1IsisCxtName[ISIS_MAX_CONTEXT_STR_LEN];

    INT1                i1OutCome = SNMP_SUCCESS;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];

    UINT4               u4PagingStatus = CLI_SUCCESS;

    INT4                i4PrevInstance = 0;
    INT4                i4Instance = 0;
    INT4                i4GlobalAdmin;
    INT4                i4RetVal = ISIS_TRUE;
    INT4                i4Value = 0;

    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrPrev;

    tIsisCktEntry      *pPrintCktRec = NULL;
    tIsisCktEntry      *pCktEntry = NULL;

    tIsisSysContext    *pContext = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;

    pContext = gpIsisInstances;
    pi1InterfaceName = &ai1InterfaceName[0];

    if (pContext == NULL)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n\rRouting Protocol is \"isis\" \r\n");
    do
    {
        if (nmhGetIsisSysAdminState
            ((INT4) pContext->u4SysInstIdx, &i4GlobalAdmin) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /***************************VRF ***********************************/
        if (UtilIsisGetVcmAliasName (pContext->u4SysInstIdx, au1IsisCxtName) !=
            ISIS_FAILURE)
        {
            CliPrintf (CliHandle, "\n\rVrf  %s \n", au1IsisCxtName);
        }
       /**************************Area Id & System Id ************************/

        i4Instance = 0;
        i4PrevInstance = 0;

        OctetStr.pu1_OctetList = au1AreaAddress;
        OctetStrPrev.pu1_OctetList = au1PrevAreaAddress;
        /*Area Table */
        i1OutCome =
            nmhGetFirstIndexIsisManAreaAddrTable (&i4Instance, &OctetStr);

        if (i1OutCome == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rArea Id :\n");
            CliPrintf (CliHandle, "\rSystem Id :\n:");
        }

        while (i1OutCome != SNMP_FAILURE)
        {
            if (pContext->u4SysInstIdx == (UINT4) i4Instance)
            {
                IsIsAreaTableInfo (CliHandle, i4Instance, &OctetStr,
                                   &u4PagingStatus, ISIS_TRUE);

                if (u4PagingStatus == CLI_FAILURE)
                    break;
            }
            i4PrevInstance = i4Instance;
            OctetStrPrev.i4_Length = OctetStr.i4_Length;
            MEMCPY (OctetStrPrev.pu1_OctetList, OctetStr.pu1_OctetList,
                    OctetStrPrev.i4_Length);

            i1OutCome =
                nmhGetNextIndexIsisManAreaAddrTable (i4PrevInstance,
                                                     &i4Instance, &OctetStrPrev,
                                                     &OctetStr);
        }

    /*************************Passive Interface ******************************/
        i4RetVal = IsisIsAllinterfacePassive (pContext);
        pCktEntry = pContext->CktTable.pCktRec;
        if (pCktEntry == NULL)
        {
            CliPrintf (CliHandle, "\rPassive-interface:\n");
        }

        if (i4RetVal == ISIS_TRUE)
        {
            CliPrintf (CliHandle, "\rpassive-interface default\n");
        }
        else
        {
            IsIsCktTablePassiveInterfaceInfo (CliHandle, pContext);
        }

        /***********************************Routing for Networks********************/

        CliPrintf (CliHandle, "\rRouting for Networks:\n");
        pCktEntry = pContext->CktTable.pCktRec;
        while (pCktEntry != NULL)
        {
            if (CfaCliConfGetIfName
                (pCktEntry->u4CktIfIdx, (INT1 *) au1NameStr) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%s \r\n", au1NameStr);
            }
            pCktEntry = pCktEntry->pNext;
        }

        /********************************Gateway and Last Update*************************/

        CliPrintf (CliHandle,
                   "\rGateway        Level        LastUpdate(Secs)\n");
        pPrintCktRec = pContext->CktTable.pCktRec;
        while (pPrintCktRec != NULL)
        {
            if (pPrintCktRec->pAdjEntry != NULL)
            {
                CfaCliGetIfName (pPrintCktRec->u4CktIfIdx, pi1InterfaceName);
                pAdjRec = pPrintCktRec->pAdjEntry;
                for (; pAdjRec != NULL; pAdjRec = pAdjRec->pNext)
                {
                    pAddr = pAdjRec->AdjNbrIpV4Addr.au1IpAddr;
                    CliPrintf (CliHandle,
                               "\r%d.%d.%d.%d       %-18s        %d\n",
                               pAddr[0], pAddr[1], pAddr[2], pAddr[3],
                               api1AdjUsage[pAdjRec->u1AdjUsage],
                               pAdjRec->u4AdjUpTime);

                }
            }
            pPrintCktRec = pPrintCktRec->pNext;
        }

          /***********************************Distance*******************************************/
        if (nmhGetFsIsisPreferenceValue
            ((INT4) pContext->u4SysInstIdx, &i4Value) == SNMP_SUCCESS)
        {
            if (i4Value != ISIS_DEFAULT_PREFERENCE)
            {
                CliPrintf (CliHandle, "\risis distance %d\r\n", i4Value);
            }
            else
            {
                CliPrintf (CliHandle, "\risis distance %d\r\n",
                           ISIS_DEFAULT_PREFERENCE);
            }
        }

        pContext = pContext->pNext;
    }
    while (pContext != NULL);

    return CLI_SUCCESS;

}

/******************************************************************************/
/* Function Name     : ShowIsisProtocolsInfo                                  */
/*                                                                            */
/* Description       : This function displays ISIS routing protocol           */
/*                     information                                            */
/*                                                                            */
/* Input Parameters  : CliHandle   - CliContext ID                            */
/*                     u4RtmCxtId - VRF Id                                    */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
ShowIsisProtocolsInfoInCxt (tCliHandle CliHandle, UINT4 u4IsisCxtId)
{

    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pAddr = NULL;
    UINT1               au1AreaAddress[ISIS_AREA_ADDR_LEN];
    UINT1               au1PrevAreaAddress[ISIS_AREA_ADDR_LEN];
    UINT1               au1IsisCxtName[ISIS_MAX_CONTEXT_STR_LEN];

    INT1                i1OutCome = SNMP_SUCCESS;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];

    UINT4               u4PagingStatus = CLI_SUCCESS;

    INT4                i4PrevInstance = 0;
    INT4                i4Instance = 0;
    INT4                i4GlobalAdmin;
    INT4                i4RetVal = ISIS_TRUE;
    INT4                i4Value = 0;

    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE OctetStrPrev;

    tIsisCktEntry      *pPrintCktRec = NULL;
    tIsisCktEntry      *pCktEntry = NULL;

    tIsisSysContext    *pContext = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;

    pContext = gpIsisInstances;
    pi1InterfaceName = &ai1InterfaceName[0];

    if (pContext == NULL)
    {
        return CLI_FAILURE;
    }

    while (pContext != NULL)
    {
        if (pContext->u4SysInstIdx == u4IsisCxtId)
        {
            break;
        }
        else
        {
            pContext = pContext->pNext;
        }
    }
    if (pContext == NULL)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\n\rRouting Protocol is \"isis\" \r\n");

    if (nmhGetIsisSysAdminState ((INT4) u4IsisCxtId, &i4GlobalAdmin)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
        /***************************VRF ***********************************/
    if (UtilIsisGetVcmAliasName (u4IsisCxtId, au1IsisCxtName) != ISIS_FAILURE)
    {
        CliPrintf (CliHandle, "\n\rVrf  %s \n", au1IsisCxtName);
    }

       /**************************Area Id & System Id ************************/

    i4Instance = 0;
    i4PrevInstance = 0;

    OctetStr.pu1_OctetList = au1AreaAddress;
    OctetStrPrev.pu1_OctetList = au1PrevAreaAddress;
    /*Area Table */
    i1OutCome = nmhGetFirstIndexIsisManAreaAddrTable (&i4Instance, &OctetStr);
    if (i1OutCome == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\rArea Id :\n");
        CliPrintf (CliHandle, "\rSystem Id :\n:");
    }
    while (i1OutCome != SNMP_FAILURE)
    {
        if (u4IsisCxtId == (UINT4) i4Instance)
        {
            IsIsAreaTableInfo (CliHandle, i4Instance, &OctetStr,
                               &u4PagingStatus, ISIS_TRUE);

            if (u4PagingStatus == CLI_FAILURE)
                break;
        }
        i4PrevInstance = i4Instance;
        OctetStrPrev.i4_Length = OctetStr.i4_Length;
        MEMCPY (OctetStrPrev.pu1_OctetList, OctetStr.pu1_OctetList,
                OctetStrPrev.i4_Length);

        i1OutCome =
            nmhGetNextIndexIsisManAreaAddrTable (i4PrevInstance, &i4Instance,
                                                 &OctetStrPrev, &OctetStr);
    }

    /*************************Passive Interface ******************************/
    i4RetVal = IsisIsAllinterfacePassive (pContext);
    pCktEntry = pContext->CktTable.pCktRec;
    if (pCktEntry == NULL)
    {
        CliPrintf (CliHandle, "\rPassive-interface:\n");
    }
    if (i4RetVal == ISIS_TRUE)
    {
        CliPrintf (CliHandle, "\rpassive-interface default\n");
    }
    else
    {
        IsIsCktTablePassiveInterfaceInfo (CliHandle, pContext);
    }

        /***********************************Routing for Networks********************/

    CliPrintf (CliHandle, "\rRouting for Networks:\n");
    pCktEntry = pContext->CktTable.pCktRec;
    while (pCktEntry != NULL)
    {
        if (CfaCliConfGetIfName
            (pCktEntry->u4CktIfIdx, (INT1 *) au1NameStr) == CFA_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%s \r\n", au1NameStr);
        }
        pCktEntry = pCktEntry->pNext;
    }

        /********************************Gateway and Last Update*************************/

    CliPrintf (CliHandle, "\rGateway        Level        LastUpdate(Secs)\n");
    pPrintCktRec = pContext->CktTable.pCktRec;
    while (pPrintCktRec != NULL)
    {
        if (pPrintCktRec->pAdjEntry != NULL)
        {
            CfaCliGetIfName (pPrintCktRec->u4CktIfIdx, pi1InterfaceName);
            pAdjRec = pPrintCktRec->pAdjEntry;
            for (; pAdjRec != NULL; pAdjRec = pAdjRec->pNext)
            {
                pAddr = pAdjRec->AdjNbrIpV4Addr.au1IpAddr;
                CliPrintf (CliHandle, "\r%d.%d.%d.%d       %-18s        %d\n",
                           pAddr[0], pAddr[1], pAddr[2], pAddr[3],
                           api1AdjUsage[pAdjRec->u1AdjUsage],
                           pAdjRec->u4AdjUpTime);
            }
        }
        pPrintCktRec = pPrintCktRec->pNext;
    }

          /***********************************Distance*******************************************/
    if (nmhGetFsIsisPreferenceValue ((INT4) u4IsisCxtId, &i4Value) ==
        SNMP_SUCCESS)
    {
        if (i4Value != ISIS_DEFAULT_PREFERENCE)
        {
            CliPrintf (CliHandle, "\risis distance %d\r\n", i4Value);
        }
        else
        {
            CliPrintf (CliHandle, "\risis distance %d\r\n",
                       ISIS_DEFAULT_PREFERENCE);
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *Function Name :  IsisCliShowHostNmeTable
 *Description   :  This function shows ISIS hostname database
 *
 *Input (s)     : CliHandle       - CliContext
 *                  u4Inst         - Instance Id
 *Output (s)    : -
 *Returns       : CLI_SUCCESS/CLI_FAILURE.
 *
 *******************************************************************************/
INT4
IsisCliShowHostNmeTable (tCliHandle CliHandle, UINT4 u4InstIdx)
{
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo = NULL;
    tIsisHostNmeNodeInfo *pIsisNxtHostNmeNodeInfo = NULL;
    tIsisSysContext    *pContext = NULL;
    CHAR                acSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    UINT1               au1SysID[ISIS_SYS_ID_LEN];
    UINT1               au1HostNme[ISIS_HSTNME_MAX_LEN + 1];
    INT4                i4DotCompliance = ISIS_DOT_FALSE;
    INT4                i4RetVal = SNMP_SUCCESS;

    MEMSET (acSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));
    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }
    if (pContext == NULL)
    {
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return CLI_FAILURE;
    }

    if (pContext->u1IsisDynHostNmeSupport == ISIS_DYNHOSTNME_DISABLE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "System ID            Hostname \n");
    CliPrintf (CliHandle, "------------------------------- \n");

    pIsisHostNmeNodeInfo = RBTreeGetFirst (pContext->HostNmeList);

    i4RetVal =
        nmhGetFsIsisDotCompliance ((INT4) pContext->u4SysInstIdx,
                                   &i4DotCompliance);
    if (i4RetVal != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Failure in Getting compliance\r\n");
        return CLI_FAILURE;
    }

    while (pIsisHostNmeNodeInfo != NULL)
    {
        MEMSET (au1SysID, 0, ISIS_SYS_ID_LEN);
        MEMSET (au1HostNme, 0, ISIS_HSTNME_MAX_LEN + 1);
        MEMCPY (au1SysID, pIsisHostNmeNodeInfo->au1SysID, ISIS_SYS_ID_LEN);
        MEMCPY (au1HostNme, pIsisHostNmeNodeInfo->au1HostNme,
                ISIS_HSTNME_MAX_LEN);
        pIsisNxtHostNmeNodeInfo =
            RBTreeGetNext (pContext->HostNmeList, pIsisHostNmeNodeInfo, NULL);
        if (i4DotCompliance == ISIS_DOT_TRUE)
        {
            ISIS_FORM_DOT_STR (ISIS_SYS_ID_LEN, au1SysID, acSysId);
        }
        else
        {
            ISIS_FORM_STR (ISIS_SYS_ID_LEN, au1SysID, acSysId);
        }
        CliPrintf (CliHandle, "%-20s %-40s\n", acSysId, au1HostNme);
        pIsisHostNmeNodeInfo = pIsisNxtHostNmeNodeInfo;
    }
    return CLI_SUCCESS;
}
#endif /* __ISISCLI_C__ */
