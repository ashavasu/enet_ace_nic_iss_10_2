/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isadjrxn.c,v 1.25 2017/09/11 13:44:07 siva Exp $ 
 *
 *   Description: This file contains the Routines for processing the Received
 *                Hello PDUs
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE VOID        IsisAdjProcRRRcvd (tIsisSysContext *, tIsisCktEntry *,
                                       UINT1, UINT2, tIsisAdjEntry *,
                                       UINT1 *pu1SysID);
PRIVATE VOID        IsisAdjProcRestartClear (tIsisSysContext * pContext,
                                             tIsisAdjEntry * pAdjRec);

PRIVATE VOID        IsisAdjProcRARcvd (tIsisSysContext *, tIsisCktEntry *,
                                       UINT1 *, UINT1, UINT2);
PRIVATE INT4        IsisAdjCalcHighPriority (tIsisCktEntry * pCktRec,
                                             UINT1 u1Level,
                                             UINT1 *pu1IsHighPriority,
                                             UINT1 *pu1SysID);
PRIVATE VOID
 
 
 
 IsisAdjProcAdjSuppress (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec,
                         tIsisCktEntry * pCktRec, UINT1 u1Level, UINT1 u1Cmd);

/******************************************************************************
 * Function Name : IsisAdjAreaAddrMatchCheck () 
 * Description   : This function checks for a match in Area Addresses of the 
 *                 local IS and the addresses included in the received PDU
 * Input(s)      : pContext     - Pointer to System Context
 *                 pu1PDU       - Pointer to the received PDU
 *                 u1EvtFlag    - The Flag indicating whether to post an Area
 *                                Address mismatch event or not
 * Output(s)     : None
 * Globals       : Not Referred or Modified 
 * Returns       : ISIS_SUCCESS - if a match is found
 *                 ISIS_FAILURE - if none of the addresses match
 ******************************************************************************/

PUBLIC INT4
IsisAdjAreaAddrMatchCheck (tIsisSysContext * pContext, UINT1 *pu1PDU,
                           UINT1 u1EvtFlag)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TlvLen = 0;
    UINT1               u1MAALen = 0;
    UINT2               u2Offset = 0;
    UINT2               u2PduLen = 0;
    UINT2               u2TrapPduLen = 0;
    tIsisMAAEntry      *pMAAEntry = NULL;
    tIsisEvtAAMismatch *pAAEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjAreaAddrMatchCheck ()\n"));

    /* Get to the correct offset in the PDU where Area Addresses are available.
     * This offset differs for P2P and LAN Hello PDUs
     */

    if (ISIS_EXTRACT_PDU_TYPE (pu1PDU) == ISIS_P2P_IIH_PDU)
    {
        u2Offset = sizeof (tIsisComHdr) + 6 + ISIS_SYS_ID_LEN;
    }
    else
    {
        u2Offset = sizeof (tIsisComHdr) + 7 + (2 * ISIS_SYS_ID_LEN);
    }

    /* All Area Addresses may not be listed together in a single TLV. Hence go
     * through the entire PDU and compare each of the Area Addresses with the
     * Addresses for the local IS. 
     */

    while (IsisUtlGetNextTlvOffset (pu1PDU, ISIS_AREA_ADDR_TLV, &u2Offset,
                                    &u1TlvLen) != ISIS_FAILURE)
    {
        while (u1TlvLen > 0)
        {
            /* Start from the beginning each time in the loop
             */

            pMAAEntry = pContext->ManAddrTable.pMAARec;
            u1MAALen = *(UINT1 *) (pu1PDU + u2Offset);
            u2Offset = (UINT2) (u2Offset + 1);
            u1TlvLen = (UINT1) (u1TlvLen - 1);

            /* Checking for any match 
             */

            while (pMAAEntry != NULL)
            {
                if (u1MAALen < ISIS_AREA_ADDR_LEN)
                {
                    if (u1MAALen == pMAAEntry->ManAreaAddr.u1Length)
                    {
                        if ((MEMCMP (pMAAEntry->ManAreaAddr.au1AreaAddr,
                                     (UINT1 *) (pu1PDU + u2Offset),
                                     u1MAALen)) == 0)
                        {
                            ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting "
                                     "IsisAdjAreaAddrMatchCheck()\n"));

                            return ISIS_SUCCESS;
                        }
                    }
                }
                pMAAEntry = pMAAEntry->pNext;
            }

            u2Offset = (UINT2) (u2Offset + u1MAALen);
            u1TlvLen = (UINT1) (u1TlvLen - u1MAALen);
        }
    }

    if (u1EvtFlag == ISIS_TRUE)
    {
        /* Incrementing the Statistics
         */

        ISIS_INCR_AA_MISMATCH_STAT (pContext);

        i4RetVal = ISIS_FAILURE;

        /* Form Area Address Mismatch event and Post the event 
         * to control queue
         */

        pAAEvt = (tIsisEvtAAMismatch *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtAAMismatch));

        if (pAAEvt != NULL)
        {

            ISIS_EXTRACT_HELLO_PDU_LEN (pu1PDU, u2PduLen);
            u2TrapPduLen = ISIS_MIN (ISIS_TRAP_PDU_FRAG_SIZE, u2PduLen);
            /* Allocate Memory for PDU Fragment 
             */
            pAAEvt->pu1PduFrag = (UINT1 *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, ISIS_TRAP_PDU_FRAG_SIZE);

            if (pAAEvt->pu1PduFrag != NULL)
            {
                pAAEvt->u1EvtID = ISIS_EVT_AA_MISMATCH;
                pAAEvt->u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);
                MEMCPY (pAAEvt->pu1PduFrag, pu1PDU, u2TrapPduLen);
                IsisUtlSendEvent (pContext, (UINT1 *) pAAEvt,
                                  sizeof (tIsisEvtAAMismatch));
            }
            else
            {
                ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pAAEvt);
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : PDU Fragment For "
                        "Address Area Mismatch Event\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
        }
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAreaAddrMatchCheck ()\n"));
    return (i4RetVal);
}

/*******************************************************************************
 * Function    : IsisAdjAddAdjAA ()
 * Description : This routine adds an Adjacency Area Address Record to the
 *               existing list of records in the given adjacency 
 * Input(s)    : pAdjRec - Pointer to Adjacency Record
 *               pAdjAA  - Pointer to Adjacency Area Address Record which is to
 *                         be added
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjAddAdjAA (tIsisAdjEntry * pAdjRec, tIsisAdjAAEntry * pAdjAA)
{
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjAddAdjAA ()\n"));

#ifdef IS_FT_WANTED
    if (ISIS_EXT_IS_FT_STATE () != ISIS_FT_STANDBY)
    {
#endif
        if (pAdjRec->pAdjAreaAddr == NULL)
        {
            pAdjAA->u4AdjAreaAddrIdx = 1;
        }
        else
        {
            pAdjAA->u4AdjAreaAddrIdx =
                ((pAdjRec->pAdjAreaAddr->u4AdjAreaAddrIdx) + 1);
        }
#ifdef IS_FT_WANTED
    }
#endif

    pAdjAA->pNext = pAdjRec->pAdjAreaAddr;
    pAdjRec->pAdjAreaAddr = pAdjAA;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddAdjAA ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjBldAdjRecFromISH ()
 * Description   : This routines builds the Adjacency record from the received
 *                 ISH PDU
 * Input(s)      : pCktRec    - Pointer to Circuit Record
 *                 pu1PDU     - Pointer to the received PDU
 *                 u1AdjState - State of the Adjacency
 *                 u1AdjUsage - Adjacency Usage
 * Globals       : Not Referred or Modified
 * Output(s)     : pAdjRec    - Updated Adjacency Record 
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjBldAdjRecFromISH (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                         UINT1 u1AdjState, UINT1 u1AdjUsage,
                         tIsisAdjEntry * pAdjRec)
{
    UINT1               u1NetLen = 0;
    UINT1               au1SysId[ISIS_SYS_ID_LEN];

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldAdjRecFromISH ()\n"));

    /* Retrieve the System ID from the Network Entity Title
     */
    ISIS_EXTRACT_NET_LEN (pu1PDU, u1NetLen);
    ISIS_EXTRACT_SYSID_FROM_NET (pu1PDU, au1SysId, u1NetLen);

    /* Get the next avaiable free Index for the new adjacency that is being
     * created
     */

    IsisAdjGetNextAdjIdx (pCktRec, &pAdjRec->u4AdjIdx);

    /* Adjacency State will be 'initialising' and Usage 'unknown' till IIH 
     * PDUs are exchanged 
     */
    pAdjRec->u1P2PThreewayState = ISIS_ADJ_DOWN;
    pAdjRec->u1AdjState = u1AdjState;
    pAdjRec->u1AdjNbrSysType = ISIS_UNKNOWN;
    pAdjRec->u1AdjUsage = u1AdjUsage;
    pAdjRec->pCktRec = NULL;
    pAdjRec->u1AdjIpAddrType = ISIS_UNKNOWN;
    pAdjRec->pNext = NULL;
    pAdjRec->pAdjAreaAddr = NULL;

    ISIS_EXTRACT_ISH_HOLD_TIME (pu1PDU, (pAdjRec->u2HoldTime));
    MEMCPY (pAdjRec->au1AdjNbrSysID, au1SysId, ISIS_SYS_ID_LEN);

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldAdjRecFromISH ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjBldAdjRecFromPdu ()
 * Description   : This routines builds the Adjacency record from the Hello PDU.
 *                 It updates the Area Address, Protocol Support and IPV4
 *                 Addresses included by the peer in the PDU into the newly
 *                 created adjacency.
 * Input(s)      : pCktRec    - Pointer to Circuit Record
 *                 pu1PDU     - Pointer to the received PDU
 *                 u1AdjState - Adjacency State
 *                 pu1SNPA    - Pointer to SNPA Address of adjacency - NULL in
 *                              case of P2P circuits
 *                 u1AdjUsage - Adjacency Usage - L1, L2 or L12
 * Output(s)     : pAdjRec    - Adjacency Record updated with peer information
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjBldAdjRecFromPdu (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                         UINT1 u1AdjState, UINT1 *pu1SNPA, UINT1 u1AdjUsage,
                         tIsisAdjEntry * pAdjRec)
{
    UINT1              *pSysId = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisAdjAAEntry    *pAdjAA = NULL;
    UINT1               au1IPV6Addr[ISIS_MAX_IPV6_ADDR_LEN];
    UINT4               u4Status;
    UINT2               u2MTId = 0;
    UINT2               u2PduLen = 0;
    UINT2               u2Offset = 0;
    UINT2               u2SNIDOff = 0;
    UINT2               u2AdjProtV4Supp = 0;
    UINT2               u2AdjProtV6Supp = 0;
    UINT1               u1MtTlvFound = ISIS_FALSE;
    UINT1               u1PSCount = 0;
    UINT1               u1PduType = 0;
    UINT1               u1Len = 0;
    UINT1               u1AALen = 0;
#ifdef BFD_WANTED
    UINT1               u1NLPId = 0;
    UINT1               u1IsisMT0BfdEnabled = 0;
    UINT1               u1IsisMT2BfdEnabled = 0;
    UINT1               u1BfdUpdReqd = ISIS_BFD_FALSE;
#endif

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldAdjRecFromPdu ()\n"));

    u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);
    pSysId = ISIS_EXTRACT_HELLO_SYS_ID (pu1PDU);
    ISIS_EXTRACT_HELLO_PDU_LEN (pu1PDU, u2PduLen);
    pContext = pCktRec->pContext;

    /* u2Offset will point to the Pseudonode Id in case of LAN IIH PDUs and
     * Local Circuit ID in case of P2P IIH PDUs. 
     */

    if (u1PduType == ISIS_P2P_IIH_PDU)
    {
        u2SNIDOff = (sizeof (tIsisComHdr) + 5 + ISIS_SYS_ID_LEN);
        u2Offset = (sizeof (tIsisComHdr) + 6 + ISIS_SYS_ID_LEN);
    }
    else
    {
        u2SNIDOff = (sizeof (tIsisComHdr) + 6 + (2 * ISIS_SYS_ID_LEN));
        u2Offset = (sizeof (tIsisComHdr) + 7 + (2 * ISIS_SYS_ID_LEN));
    }

    /* Retrieve the Peer System Type from the PDU
     */

    if (ISIS_EXTRACT_HELLO_CKT_TYPE (pu1PDU) == 0)
    {
        /* Unknown Peer System Type
         */

        pContext->SysStats.u4SysDroppedPDUs++;

        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldAdjRecFromPdu ()\n"));
        return;
    }

    /* Get one free Index for the newly created record and update the record
     * with appropriate values
     */
    pAdjRec->u1P2PThreewayState = ISIS_ADJ_DOWN;
    IsisAdjGetNextAdjIdx (pCktRec, &pAdjRec->u4AdjIdx);
    pAdjRec->u1AdjState = u1AdjState;    /* Mark it UP */
    pAdjRec->u1AdjUsage = u1AdjUsage;
    ISIS_EXTRACT_HOLD_TIME (pu1PDU, pAdjRec->u2HoldTime);

    MEMCPY (pAdjRec->au1AdjNbrSysID, pSysId, ISIS_SYS_ID_LEN);

    pAdjRec->u1AdjNbrSysType = ISIS_EXTRACT_HELLO_CKT_TYPE (pu1PDU);
    pAdjRec->u4AdjUpTime = pContext->SysTimers.TmrECInfo.u4TCount;
    pAdjRec->pCktRec = NULL;    /* We will fill this later */
    pAdjRec->pNext = NULL;
    pAdjRec->pAdjAreaAddr = NULL;    /* Filled down the line in this routine */
    pAdjRec->u1IsGRCapEnabled = GR_DISABLED;

    /* Initialise variables added for BFD M-ISIS */
    pAdjRec->u1IsisMT0BfdEnabled = ISIS_BFD_FALSE;
    pAdjRec->u1IsisMT2BfdEnabled = ISIS_BFD_FALSE;
    pAdjRec->u1IsisBfdRequired = ISIS_BFD_FALSE;
    pAdjRec->u1IsisMT0BfdState = ISIS_BFD_FALSE;
    pAdjRec->u1IsisMT2BfdState = ISIS_BFD_FALSE;
    pAdjRec->u1IsisBfdNeighborUseable = ISIS_BFD_FALSE;
    pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_FALSE;
    pAdjRec->u1Mt0BfdRegd = ISIS_BFD_FALSE;
    pAdjRec->u1Mt2BfdRegd = ISIS_BFD_FALSE;
    pAdjRec->u4StartTime = 0;

    pAdjRec->au1AdjNbrSysID[ISIS_SYS_ID_LEN] = *(UINT1 *) (pu1PDU + u2SNIDOff);
     /*MISIS*/ pAdjRec->u1AdjMTId = 0;    /*INITIALIZING with no MT support */

    if (((u1PduType == ISIS_L1LAN_IIH_PDU) || (u1PduType == ISIS_L2LAN_IIH_PDU))
        && (pu1SNPA != NULL))
    {
        /* Point to Point links do not have SNPA's and since they do not run DIS
         * election they do not have priorities either. We do not process ISHs
         * to this level
         */

        MEMCPY (pAdjRec->au1AdjNbrSNPA, pu1SNPA, ISIS_SNPA_ADDR_LEN);
        pAdjRec->u1AdjNbrPriority = *(UINT1 *) (pu1PDU + sizeof (tIsisComHdr) +
                                                5 + ISIS_SYS_ID_LEN);
    }

    /* Updating the Adjacency Record with information from Variable Length
     * fields of the PDU
     */

    while (u2Offset < u2PduLen)
    {
        switch (*(pu1PDU + u2Offset))
        {
            case ISIS_AREA_ADDR_TLV:

                u2Offset++;
                u1Len = *(pu1PDU + u2Offset);
                u2Offset++;

                /* More than one Area Address can be included in a single TLV.
                 * Process the TLV completely
                 */

                while (u1Len > 0)
                {
                    /* If the Received Area Address Length is greater than
                     * supported Area Address length, then ignore the Area
                     * Address
                     */

                    u1AALen = *(pu1PDU + u2Offset);
                    if (u1AALen <= ISIS_AREA_ADDR_LEN)
                    {
                        pAdjAA = (tIsisAdjAAEntry *)
                            ISIS_MEM_ALLOC (ISIS_BUF_ADAA,
                                            sizeof (tIsisAdjAAEntry));

                        if (pAdjAA == NULL)
                        {
                            /* Adjacency Area Addresses are informational.
                             * We can continue with the adjacency without
                             * the area addresses
                             */

                            PANIC ((ISIS_LGST,
                                    ISIS_MEM_ALLOC_FAIL
                                    " : Adjacency Area Address\n"));
                            DETAIL_FAIL (ISIS_CR_MODULE);
                            u2Offset += *(pu1PDU + u2Offset);
                            u2Offset++;    /* Increment the Offset for the Length
                                         */
                            break;
                        }

                        /* Copying Area Address Length and Area Address 
                         * together from the PDU
                         */

                        pAdjAA->ISAdjAreaAddress.u1Length = u1AALen;
                        u2Offset++;
                        u1Len--;
                        MEMCPY (pAdjAA->ISAdjAreaAddress.au1AreaAddr,
                                pu1PDU + u2Offset, u1AALen);

                        IsisAdjAddAdjAA (pAdjRec, pAdjAA);
                        u1Len = (UINT1) (u1Len - u1AALen);
                        u2Offset = (UINT2) (u2Offset + u1AALen);
                    }
                    else
                    {
                        u1Len = (UINT1) (u1Len - u1AALen - 1);
                        u2Offset = (UINT2) (u2Offset + u1AALen + 1);
                    }
                }
                break;

            case ISIS_PROT_SUPPORT_TLV:

                /* More than one Protocol can be included in a single TLV.
                 * Process the TLV completely
                 */

                u2Offset++;
                u1Len = *(pu1PDU + u2Offset);
                u2Offset++;

                while (u1Len > 0)
                {
                    if (u1PSCount < ISIS_MAX_PROTS_SUPP)
                    {
                        pAdjRec->au1AdjProtSupp[u1PSCount]
                            = *((UINT1 *) (pu1PDU + u2Offset));
                        u1PSCount++;
                        u2Offset++;
                        u1Len--;
                    }
                    else
                    {
                        u2Offset += u1Len;
                        u1Len = 0;
                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Number Of Protocols Included "
                                 "Exceeds Max Protocols supported\n"));
                    }
                }
                break;

            case ISIS_IPV4IF_ADDR_TLV:

                /* More than one IP Address can be included in a single TLV.
                 * copy only the one Interface Address
                 */

                u2Offset++;
                u1Len = *(pu1PDU + u2Offset);
                u2Offset++;

#ifdef BFD_WANTED
                if ((pContext->u1IsisMTSupport == ISIS_FALSE) &&
                    (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
                {
                    if (!(pCktRec->u1ProtoSupp & ISIS_ADDR_IPV4))
                    {
                        u2Offset += (UINT2) u1Len;
                        break;
                    }
                }
#endif

                MEMCPY (pAdjRec->AdjNbrIpV4Addr.au1IpAddr,
                        (pu1PDU + u2Offset), ISIS_MAX_IPV4_ADDR_LEN);
                pAdjRec->AdjNbrIpV4Addr.u1IpAddrIndex = ISIS_IPV4_ADDR_INDEX;
                u2AdjProtV4Supp = ISIS_TRUE;
                u2Offset += (UINT2) u1Len;
                break;
            case ISIS_IPV6IF_ADDR_TLV:
                /* More than one IP Address can be included in a single TLV.
                   copy only the one Interface Address */

                u2Offset++;
                u1Len = *(pu1PDU + u2Offset);
                u2Offset++;

#ifdef BFD_WANTED
                if ((pContext->u1IsisMTSupport == ISIS_FALSE) &&
                    (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
                {
                    if (!(pCktRec->u1ProtoSupp & ISIS_ADDR_IPV6))
                    {
                        u2Offset += u1Len;
                        break;
                    }
                }
#endif
                while (u1Len > 0)
                {

                    MEMCPY (&au1IPV6Addr,
                            (pu1PDU + u2Offset), ISIS_MAX_IPV6_ADDR_LEN);
                    ISIS_IN6_IS_ADDR_LINKLOCAL (au1IPV6Addr, u4Status);
                    /*Link Local Check */
                    if (u4Status == ISIS_TRUE)
                    {
                        MEMCPY (pAdjRec->AdjNbrIpV6Addr.au1IpAddr,
                                (pu1PDU + u2Offset), ISIS_MAX_IPV6_ADDR_LEN);
                        pAdjRec->AdjNbrIpV4Addr.u1IpAddrIndex =
                            ISIS_IPV6_ADDR_INDEX;
                        u2AdjProtV6Supp = ISIS_TRUE;
                        u2Offset += u1Len;
                        break;
                    }
                    else
                    {
                        u2Offset += ISIS_MAX_IPV6_ADDR_LEN;
                        u1Len = (UINT1) (u1Len - ISIS_MAX_IPV6_ADDR_LEN);    /*Diab */
                    }
                }
                break;
            case ISIS_MT_TLV:
                u1MtTlvFound = ISIS_TRUE;
                /*MISIS - more than one MTID can be presnt in the TLV - 
                   OR the BITMAP values for the MT-IDs */
                u2Offset++;
                u1Len = *(pu1PDU + u2Offset);
                u2Offset++;
                while (u1Len > 0)
                {
                    MEMCPY (&u2MTId, (pu1PDU + u2Offset), ISIS_MT_ID_LEN);
                    u2MTId = OSIX_HTONS (u2MTId);
                    u2MTId = (u2MTId & ISIS_MTID_MASK);
                    if (u2MTId == ISIS_IPV4_UNICAST_MT_ID)
                    {
                        pAdjRec->u1AdjMTId |= ISIS_MT0_BITMAP;
                    }
                    else if (u2MTId == ISIS_IPV6_UNICAST_MT_ID)
                    {
                        pAdjRec->u1AdjMTId |= ISIS_MT2_BITMAP;
                    }
                    u2Offset += (UINT2) 2;
                    u1Len -= (UINT1) 2;
                }
                break;
#ifdef BFD_WANTED
            case ISIS_BFD_ENABLED_TLV:
                u2Offset++;
                u1Len = *(pu1PDU + u2Offset);
                u2Offset++;
                /* Read the BFD Enabled TLV only if BFD support is
                 * enabled at the router and interface level */
                if ((pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE) &&
                    (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
                {
                    u1BfdUpdReqd = ISIS_BFD_TRUE;
                    while (u1Len > 0)
                    {
                        MEMCPY (&u2MTId, (pu1PDU + u2Offset), ISIS_MT_ID_LEN);
                        u2MTId = OSIX_HTONS (u2MTId);
                        u2MTId = (u2MTId & ISIS_MTID_MASK);

                        u2Offset = (UINT2) (u2Offset + 2);
                        u1Len = (UINT1) (u1Len - 2);

                        u1NLPId = *((UINT1 *) (pu1PDU + u2Offset));

                        u2Offset = (UINT2) (u2Offset + 1);
                        u1Len = (UINT1) (u1Len - 1);
                        if ((u2MTId == ISIS_IPV4_UNICAST_MT_ID) &&
                            (u1NLPId == ISIS_IPV4_SUPP))
                        {
                            /* check for bfd support in ckt and update correspondingly */
                            if ((pCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                                == ISIS_MT0_BITMAP)
                            {
                                u1IsisMT0BfdEnabled |= ISIS_BFD_IPV4_ENABLED;
                            }
                        }
                        else if ((u2MTId == ISIS_IPV6_UNICAST_MT_ID) &&
                                 (u1NLPId == ISIS_IPV6_SUPP))
                        {
                            if ((pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                                == ISIS_MT2_BITMAP)
                            {
                                u1IsisMT2BfdEnabled |= ISIS_BFD_IPV6_ENABLED;
                            }
                        }
                    }
                }
                else
                {
                    u2Offset = (UINT2) (u2Offset + (UINT2) u1Len);
                }
                break;
#endif
            case ISIS_P2P_THREE_WAY_ADJ_TLV:
                /*fetch all info and update in pAdjRec - from TLV 240 */
                u2Offset++;
                u1Len = *(pu1PDU + u2Offset);
                u2Offset++;

                if (pCktRec->u1IsP2PThreeWayEnabled == ISIS_TRUE)
                {
                    pAdjRec->u1P2PPeerThreewayState = *(pu1PDU + u2Offset);
                    u2Offset++;
                    u1Len--;

                    if (u1Len != ISIS_ZERO)
                    {
                        /* process otional TLVs */
                        /* 2). Extended Local Circuit ID */
                        ISIS_EXTRACT_P2P_NEIGHBOR_EXT_CIRC_ID (pu1PDU,
                                                               (u2Offset +
                                                                ISIS_P2P_THREE_WAY_STATE_LEN),
                                                               pAdjRec->
                                                               u4NeighExtCircuitID);
                        u2Offset = u2Offset + ISIS_P2P_EXT_CKT_ID_LEN;
                        u1Len = u1Len - ISIS_P2P_EXT_CKT_ID_LEN;

                        if (u1Len != ISIS_ZERO)
                        {
                            /* point of no return...
                             * Neighbor System info present 
                             * If validation fails - necessary actions has to be taken */
                            ISIS_EXTRACT_P2P_NEIGHBOR_SYS_ID (pu1PDU, u2Offset,
                                                              pAdjRec->
                                                              au1AdjPeerNbrSysID);
                            ISIS_EXTRACT_P2P_NEIGHBOR_EXT_CIRC_ID (pu1PDU,
                                                                   u2Offset,
                                                                   pAdjRec->
                                                                   u4AdjPeerNbrExtCircuitID);
                            u2Offset =
                                u2Offset + ISIS_P2P_EXT_CKT_ID_LEN +
                                ISIS_SYS_ID_LEN;
                            u1Len =
                                u1Len - (ISIS_P2P_EXT_CKT_ID_LEN +
                                         ISIS_SYS_ID_LEN);
                        }
                        pAdjRec->u1ThreeWayHndShkVersion =
                            ISIS_P2P_THREE_WAY_V2;
                    }
                    else
                    {
                        pAdjRec->u1ThreeWayHndShkVersion =
                            ISIS_P2P_THREE_WAY_V1;
                    }
                }
                else
                {
                    u2Offset = (UINT2) (u2Offset + (UINT2) u1Len);
                }
                break;

            default:

                u2Offset++;
                u1Len = *(pu1PDU + u2Offset);
                u2Offset++;
                u2Offset = (UINT2) (u2Offset + (UINT2) u1Len);
                break;
        }
    }

    if ((u1MtTlvFound == ISIS_FALSE)
        && (pContext->u1IsisMTSupport == ISIS_TRUE))
    {
        pAdjRec->u1AdjMTId |= ISIS_MT0_BITMAP;
    }

#ifdef BFD_WANTED
    /* Updated the BFD Reqd variable only after updating MT Id */
    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        if (u1BfdUpdReqd == ISIS_BFD_TRUE)
        {
            if ((pCktRec->u1IfMTId & ISIS_MT0_BITMAP) &&
                (!(pCktRec->u1IfMTId & ISIS_MT2_BITMAP)))
            {
                if ((pAdjRec->u1AdjMTId & ISIS_MT2_BITMAP) &&
                    (!(pAdjRec->u1AdjMTId & ISIS_MT0_BITMAP)))
                {
                    pAdjRec->u1AdjState = ISIS_ADJ_INIT;

                }
            }

            if ((pCktRec->u1IfMTId & ISIS_MT2_BITMAP) &&
                (!(pCktRec->u1IfMTId & ISIS_MT0_BITMAP)))
            {
                if ((pAdjRec->u1AdjMTId & ISIS_MT0_BITMAP) &&
                    (!(pAdjRec->u1AdjMTId & ISIS_MT2_BITMAP)))
                {
                    pAdjRec->u1AdjState = ISIS_ADJ_INIT;
                }
            }

            if (pAdjRec->u1IsisMT0BfdEnabled != u1IsisMT0BfdEnabled)
            {
                pAdjRec->u1IsisMT0BfdEnabled = u1IsisMT0BfdEnabled;
            }
            if (pAdjRec->u1IsisMT2BfdEnabled != u1IsisMT2BfdEnabled)
            {
                pAdjRec->u1IsisMT2BfdEnabled = u1IsisMT2BfdEnabled;
            }
        }
    }
#endif

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldAdjRecFromPdu ()\n"));

    if ((u2AdjProtV4Supp == TRUE) && (u2AdjProtV6Supp == TRUE))
    {
        pAdjRec->u1AdjIpAddrType = ISIS_ADDR_IPVX;
    }
    else if (u2AdjProtV4Supp == TRUE)
    {
        pAdjRec->u1AdjIpAddrType = ISIS_ADDR_IPV4;
    }
    else if (u2AdjProtV6Supp == TRUE)
    {
        pAdjRec->u1AdjIpAddrType = ISIS_ADDR_IPV6;
    }

}

/******************************************************************************
 * Function Name : IsisAdjAddAdj ()
 * Description   : This routine adds the given Adjacency to the Adjacency
 *                 Database. If Fault Tolerance is enabled a Lock Step Update 
 *                 is sent.
 * Input(s)      : pContext  - Pointer to System Context
 *                 pCktRec   - Pointer to Circuit Record
 *                 pAdjRec   - Pointer to the Adjacency to be added
 *                 u1DRAFlag - Flag Indicating whether to add a new Direction
 *                             Entry corresponding to the new Adjancency Entry
 * Output(s)     : pu4DirIdx - The Index of the Direction Entry. This
 *                             will be used by calling function to start or stop
 *                             timers.
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjAddAdj (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
               tIsisAdjEntry * pAdjRec, UINT1 u1DRAFlag)
{
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisAdjEntry      *pTravAdj = NULL;
    tIsisAdjEntry      *pPrevAdj = NULL;
#ifdef ISIS_FT_ENABLED
    tIsisAdjAAEntry    *pAdjAA = NULL;
#endif
    tIsisAdjDirEntry   *pDirEntry = NULL;
    UINT4               u4DirIdx = 0;
    UINT1               au1NullIP[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1TimerType = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjAddAdj ()\n"));

    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);

    pCktLevel = (pCktRec->u1CktType == ISIS_P2P_CKT)
        ? ISIS_GET_P2P_CKT_LEVEL (pCktRec)
        : ((pAdjRec->u1AdjUsage == ISIS_LEVEL1) ? pCktRec->pL1CktInfo
           : pCktRec->pL2CktInfo);

    if (pCktLevel == NULL)
    {
        return;
    }
    /* Inserting the record in the database sorted on Adjacency Index 
     */

    pTravAdj = pCktRec->pAdjEntry;
    if (pTravAdj == NULL)
    {
        /* This is the first adjacency being added to the circuit. Add it at the
         * head directly
         */

        pCktRec->pAdjEntry = pAdjRec;
        pAdjRec->pNext = NULL;

        /* The 'pContext->CktTable.pu1CktMask' will be used for transmitting
         * packets on ALL Circuits i.e. this mask indictaes which are all the
         * circuits which have atleast one active adjacency. At this point we
         * have added the first active adjacency and hence this circuit
         * qualifies to be included in the mask
         */

    }
    else if (pAdjRec->u4AdjIdx < pTravAdj->u4AdjIdx)
    {
        /* Active adjacencies already exist and the Index of the new adjacency
         * being inserted is the minimum most. Insert at the head
         */

        pAdjRec->pNext = pTravAdj;    /* Squeeze it in at first position */
        pCktRec->pAdjEntry = pAdjRec;    /* Point the head at the new record */
    }
    else
    {
        /* No luck, must travel the list to get to appropriate position for
         * insertion
         */

        pPrevAdj = pTravAdj;
        pTravAdj = pTravAdj->pNext;    /* We are already done with first node */

        while (pTravAdj != NULL)
        {
            if (pAdjRec->u4AdjIdx < pTravAdj->u4AdjIdx)
            {
                pAdjRec->pNext = pTravAdj;    /* Squeeze it in */
                pPrevAdj->pNext = pAdjRec;
                break;
            }
            pPrevAdj = pTravAdj;
            pTravAdj = pTravAdj->pNext;
        }
        if (pTravAdj == NULL)
        {
            /* No choice we have to add at the end of the list
             */

            pPrevAdj->pNext = pAdjRec;
        }
    }

    /* Setting a pointer back to the circuit record and updating statistics
     */

    pAdjRec->pCktRec = pCktRec;

    /* Reset cache-pointer for isisAdj Table */
    pCktRec->pLastAdjEntry = NULL;

    ISIS_INCR_ADJ_CHG_STAT (pCktRec);

#ifdef BFD_WANTED
    /* If multi-topology is enabled, 
     * Compution of BFD Required variable and BFD registration
     * based on the computed BFD reqd value has to be done here. */
    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        if ((pAdjRec->u1IsisMT0BfdEnabled != ISIS_BFD_FALSE) ||
            (pAdjRec->u1IsisMT2BfdEnabled != ISIS_BFD_FALSE))
        {
            IsisUtlUpdateBfdRequired (pCktRec, pAdjRec);
        }

        /* If ISIS_BFD_REQD is set for this neighbor, then the 
         * adjacency state can be set to ISIS_ADJ_UP only if
         * the BFD session is UP for this neighbor */
        if ((ISIS_BFD_REQUIRED (pAdjRec) == ISIS_BFD_TRUE) &&
            (pAdjRec->u1AdjState == ISIS_ADJ_UP))
        {
            if (ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) == ISIS_BFD_FALSE)
            {
                pAdjRec->u1AdjState = ISIS_ADJ_INIT;
            }
            else if ((ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) == ISIS_BFD_TRUE) &&
                     (pAdjRec->u1IsisMT0BfdState != ISIS_BFD_SESS_UP) &&
                     (pAdjRec->u1IsisMT2BfdState != ISIS_BFD_SESS_UP))
            {
                pAdjRec->u1AdjState = ISIS_ADJ_INIT;
            }
        }
    }
#endif

    if ((MEMCMP (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
                 ISIS_MAX_IPV4_ADDR_LEN) != 0) &&
        (IsisUtlIsDirectlyConnected (pCktRec,
                                     pAdjRec->AdjNbrIpV4Addr.au1IpAddr) ==
         ISIS_FALSE))
    {
        pAdjRec->u1AdjIpAddrType &= ~(ISIS_ADDR_IPV4);
        if (pAdjRec->u1AdjIpAddrType == 0)
        {
            pAdjRec->u1AdjState = ISIS_ADJ_INIT;
        }
    }

    if (pAdjRec->u1AdjState == ISIS_ADJ_UP)
    {
        ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID, acNbrSysId);
        ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                             ISIS_MAX_IPV4_ADDR_LEN);

        ADP_PI ((ISIS_LGST,
                 "ADJ <I> : Adjacency - State [ %s ], Circuit Index [ %u ],"
                 " Level [ %u ], Neighbour ID [ %s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                 ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                 pCktRec->u4CktIdx, pAdjRec->u1AdjUsage, acNbrSysId,
                 au1IPv4Addr,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pCktRec->au1IPV6Addr))));

        pCktLevel->u4NumAdjs++;

        if (pCktLevel->u4NumAdjs == 1)
        {
            if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
            {
                IsisUtlSetBPat (&(pContext->CktTable.pu1L1CktMask),
                                pCktRec->u4CktIdx);
            }

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
            {
                IsisUtlSetBPat (&(pContext->CktTable.pu1L2CktMask),
                                pCktRec->u4CktIdx);
            }
        }

        /* Form Adjacency Change Event and enqueue Event to 
         * Control Queue. Update module will require this
         */

        if (ISIS_CONTEXT_ACTIVE (pContext) == ISIS_TRUE)
        {
            IsisAdjEnqueueAdjChgEvt (pCktRec, pCktLevel, pAdjRec, ISIS_ADJ_UP);

            /* Set the Circuit Mask for the given Circuit Level when
             * the first adjacency is coming up
             */

            if (pCktLevel->u4NumAdjs == 1)
            {
                if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
                {
                    IsisTmrStartECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                         pCktRec->u4CktIdx, 1,
                                         &pCktLevel->u1LSPTxTmrIdx);
                }

                if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
                {
                    IsisTmrStartECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                         pCktRec->u4CktIdx, 1,
                                         &pCktLevel->u1LSPTxTmrIdx);
                }
            }
        }

        if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
        {
            (pContext->CktTable.u4NumL1Adjs)++;
        }
        if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
        {
            (pContext->CktTable.u4NumL2Adjs)++;
        }

        /* Setting the CircuitID, after negotiaiton. Refer Sec. 8.2.4.2 c) of 
         * ISO 10589 Specification
         */

        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            if (((MEMCMP (pContext->SysActuals.au1SysID,
                          pAdjRec->au1AdjNbrSysID, ISIS_SYS_ID_LEN)) >= 0))
            {
                /* We prefer the System ID and Local Circuit ID of the 
                 * local system
                 */

                MEMCPY (pCktRec->au1P2PCktID, pContext->SysActuals.au1SysID,
                        ISIS_SYS_ID_LEN);
                ISIS_ASSIGN_1_BYTE (pCktRec->au1P2PCktID, ISIS_SYS_ID_LEN,
                                    pCktRec->u1CktLocalID);
            }
            else
            {
                /* Prefer the Peers System ID and Local Circuit Id
                 */
                MEMCPY (pCktRec->au1P2PCktID, pAdjRec->au1AdjNbrSysID,
                        ISIS_SYS_ID_LEN + 1);
            }
        }

#ifdef BFD_WANTED
        /* If BFD is enabled over single topology ISIS, BFD registration
         * is done once the ISIS adjacency is transitioned to UP */
        if ((pContext->u1IsisMTSupport == ISIS_FALSE) &&
            (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
        {
            ISIS_BFD_REGISTER (pContext, pAdjRec, pCktRec);
        }
#endif
    }

    if (u1DRAFlag == ISIS_TRUE)
    {
        /* Adding Direction Entry corresponding to this Adjacency Record
         */

        pDirEntry = (tIsisAdjDirEntry *)
            ISIS_MEM_ALLOC (ISIS_BUF_ADIR, sizeof (tIsisAdjDirEntry));

        if (pDirEntry != NULL)
        {
            IsisAdjAddDirEntry (pContext, pAdjRec, pDirEntry);
            u4DirIdx = pDirEntry->u4DirIdx;
        }
        else
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Adjacency Direction Entry\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddAdj ()\n"));
            return;
        }
    }
    else
    {
        /* if no memeory is allocated for the Direction Entry, then update the
         * DirIdx for this Adjaceny Record
         */

        u4DirIdx = IsisAdjGetDirEntryWithAdjIdx (pContext, pAdjRec);
    }

    /* If FT is supported and Lock Step Update enabled, send the Adjacency Lock
     * Step Update
     */

#ifdef ISIS_FT_ENABLED

    /* Send a LSU for the newly added adjacency
     */

    ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_ADD, pAdjRec);

    /* Send each of the Adjacency Area Address Records
     */

    pAdjAA = pAdjRec->pAdjAreaAddr;
    while (pAdjAA != NULL)
    {
        ISIS_FLTR_ADJ_AA_LSU (pContext, ISIS_CMD_ADD, pAdjRec, pAdjAA);
        pAdjAA = pAdjAA->pNext;
    }

#endif

    /* Start the Hello timer if not already started, else restart the 
     * Hello timer
     */

    u1TimerType = (pCktRec->u1CktType == ISIS_P2P_CKT) ? ISIS_ECT_P2P_HELLO :
        ((pAdjRec->u1AdjUsage == ISIS_LEVEL1) ? ISIS_ECT_L1LANHELLO
         : ISIS_ECT_L2LANHELLO);

    if (pCktLevel->u1HelloTmrIdx != 0)
    {
        IsisTmrRestartECTimer (pCktRec->pContext, u1TimerType,
                               pCktRec->u4CktIdx, pCktLevel->HelloTimeInt,
                               &pCktLevel->u1HelloTmrIdx);
    }
    else
    {
        IsisTmrStartECTimer (pCktRec->pContext, u1TimerType,
                             pCktRec->u4CktIdx, pCktLevel->HelloTimeInt,
                             &pCktLevel->u1HelloTmrIdx);
    }
    if (pAdjRec->u1TmrIdx != 0)
    {
        IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                               u4DirIdx, pAdjRec->u2HoldTime,
                               &pAdjRec->u1TmrIdx);
    }
    else
    {
        IsisTmrStartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                             u4DirIdx, pAdjRec->u2HoldTime, &pAdjRec->u1TmrIdx);
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddAdj ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjAddP2PThreewayAdj ()
 * Description   : This routine adds the given P2P Adjacency to the Adjacency
 *                 Database.  
 * Input(s)      : pContext  - Pointer to System Context
 *                 pCktRec   - Pointer to Circuit Record
 *                 pAdjRec   - Pointer to the Adjacency to be added
 * Output(s)     : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjAddP2PThreewayAdj (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                          tIsisAdjEntry * pAdjRec)
{
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisAdjEntry      *pTravAdj = NULL;
    UINT1               u1TimerType = 0;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    UINT4               u4DirIdx = 0;

    UNUSED_PARAM (pContext);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjAddP2PThreewayAdj ()\n"));

    pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);

    if (pCktLevel == NULL)
    {
        return;
    }

    pTravAdj = pCktRec->pAdjEntry;
    if (pTravAdj == NULL)
    {
        /* This is the first adjacency being added to the circuit. Add it at the
         * head directly
         */

        pCktRec->pAdjEntry = pAdjRec;
        pAdjRec->pNext = NULL;

        /* The 'pContext->CktTable.pu1CktMask' will be used for transmitting
         * packets on ALL Circuits i.e. this mask indictaes which are all the
         * circuits which have atleast one active adjacency. At this point we
         * have added the first adjacency but this circuit doesn't
         * qualify to be included in the mask because the adjacency isn't up yet
         */
    }
    else
    {
        /*Three can be only one adjacency on a point-to-point circuit
         *This contion is invalid during normal operation 
         * Modify P2P Adjacecy API has to be called*/
        return;
    }
    /* Setting a pointer back to the circuit record and updating statistics
     */
    pAdjRec->pCktRec = pCktRec;

    /* Reset cache-pointer for isisAdj Table */
    pCktRec->pLastAdjEntry = NULL;

    u1TimerType = ISIS_ECT_P2P_HELLO;

    if (pCktLevel->u1HelloTmrIdx != 0)
    {
        IsisTmrRestartECTimer (pCktRec->pContext, u1TimerType,
                               pCktRec->u4CktIdx, pCktLevel->HelloTimeInt,
                               &pCktLevel->u1HelloTmrIdx);
    }
    else
    {
        IsisTmrStartECTimer (pCktRec->pContext, u1TimerType,
                             pCktRec->u4CktIdx, pCktLevel->HelloTimeInt,
                             &pCktLevel->u1HelloTmrIdx);
    }

    /* DirEntry is to be added whenever an adjacency is added 
     * This is because hold timer is based on the dirrction index assigned to the direction entry
     * However during SPF the state of the adj. has to be verifed for UP 
     * before considering it for route computation.
     * Also note that only one direction entry per adjacency is allowed. */

    /* Adding Direction Entry corresponding to this Adjacency Record
     */
    pDirEntry = (tIsisAdjDirEntry *)
        ISIS_MEM_ALLOC (ISIS_BUF_ADIR, sizeof (tIsisAdjDirEntry));

    if (pDirEntry != NULL)
    {
        IsisAdjAddDirEntry (pContext, pAdjRec, pDirEntry);
        u4DirIdx = pDirEntry->u4DirIdx;
    }
    else
    {
        PANIC ((ISIS_LGST,
                ISIS_MEM_ALLOC_FAIL " : Adjacency Direction Entry!\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddAdj ()\n"));
        return;
    }

    /* At this point, there should be a valid direction index and entry
     * Start Holdtimer for the direction entry */

    IsisTmrStartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                         u4DirIdx, pAdjRec->u2HoldTime, &pAdjRec->u1TmrIdx);

    return;
}

/*******************************************************************************
 * Function    : IsisAdjDelAdj ()
 * Description : This routine deletes the given Adjacency Record from the
 *               Database. It also deletes all the records associated with the
 *               Adjacency viz. Adjacent Area Addresses etc. If Fault 
 *               Tolerance is enabled, a Lock Step Update is sent.
 * Input(s)    : pContext  - Pointer to System Context
 *               pCktRec   - Pointer to Circuit Record
 *               pAdjRec   - Pointer to the Adjacency to be deleted
 *               u1EvtFlag - Flag indicating whether Adjacency
 *                           Down Event needs to be sent
 * Output(s)   : pu4DirIdx - Index of the Direction Entry which held the
 *                           deleted Adjacency information. This will be used
 *                           by the calling routine for stopping any active
 *                           timers if required
 * Globals     : None Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjDelAdj (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
               tIsisAdjEntry * pAdjRec, UINT1 u1EvtFlag, UINT4 *pu4DirIdx)
{
    tIsisAdjEntry      *pTrav = NULL;
    tIsisAdjEntry      *pPrev = NULL;
    tIsisCktLevel      *pCktLvl = NULL;
    UINT1               u1AdjState = ISIS_ADJ_DOWN;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDelAdj ()\n"));

    pPrev = pCktRec->pAdjEntry;
    pTrav = pCktRec->pAdjEntry;

    pCktLvl = (pCktRec->u1CktType == ISIS_P2P_CKT)
        ? ISIS_GET_P2P_CKT_LEVEL (pCktRec)
        : ((pAdjRec->u1AdjUsage == ISIS_LEVEL1) ? pCktRec->pL1CktInfo
           : pCktRec->pL2CktInfo);

    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID, acNbrSysId);
    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    if (pCktRec->pAdjEntry == pAdjRec)
    {
        /* Very first entry being deleted
         */

        ADP_PI ((ISIS_LGST,
                 "ADJ <I> : Adjacency Deleted - Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                 " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                 pCktRec->u4CktIdx, pAdjRec->u1AdjUsage, acNbrSysId,
                 au1IPv4Addr,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pCktRec->au1IPV6Addr))));

        pCktRec->pAdjEntry = pCktRec->pAdjEntry->pNext;
    }
    else
    {
        pTrav = pTrav->pNext;    /* Already done with the first element */

        while (pTrav != NULL)
        {
            if (pTrav == pAdjRec)
            {
                pPrev->pNext = pTrav->pNext;

                ADP_PI ((ISIS_LGST,
                         "ADJ <I> : Adjacency Deleted - Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                         " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         pCktRec->u4CktIdx, pAdjRec->u1AdjUsage, acNbrSysId,
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));

                break;
            }
            else
            {
                pPrev = pTrav;
                pTrav = pTrav->pNext;
            }
        }
    }
    if (pTrav != NULL)
    {
        /* Found the required adjacency and deleted it from the adjacency list
         * maintained for the given circuit
         */

        if (pCktRec->pAdjEntry == NULL)
        {
            /* Last adjacency is deleted for the circuit. The circuit can no
             * longer be used for transmitting or receiving packets. Hence it
             * has to be removed from the ALL Circuits Mask maintained in the
             * circuit table.
             */
            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : Last Adjacency Deleted - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));
        }

        ISIS_INCR_ADJ_CHG_STAT (pCktRec);

        /* Now Deleting the corresponding Adjacency Direction Entry from the
         * Direction Table since the adjacency no more exist
         */

        IsisAdjDelDirEntry (pContext, pAdjRec, pu4DirIdx);

        /* If FT is supported and Lock Step Update enabled, transmit a LSU for
         * the deleted adjacency
         */

#ifdef BFD_WANTED
        /* Deregister from BFD, if registered for path monitoring.
           The add sync in degistration flow is common, so adding before delete sync */
        ISIS_BFD_DEREGISTER (pContext, pAdjRec, pCktRec, ISIS_FALSE);
#endif

        ISIS_FLTR_ADJ_LSU (pContext, ISIS_CMD_DELETE, pAdjRec);

        /* All Adjacency Area Addresses being deleted
         */

        IsisAdjDelAdjAreaAddrs (pAdjRec);

        /* NOTE: Lock Step Updates need not be sent for Adjacency Area Addresses
         * since a LSU for deleted adjacency would have anyway removed the
         * adjacency record as well as all the associated records from the
         * database
         */

        /* Adjacency Change event will be enqueued to the control module only if
         * requested for. This is because, during a Circuit DOWN event, the
         * IsisAdjDelAdj () routine is invoked several times to delete all the
         * adjacencies, if it happens to be a broadcat circuit. Since there can
         * be 100's of adjacencies on a broadcast circuit, this will result in
         * 100's of events being posted. To avoid this if a broadcast circuit is
         * DOWN, the routine procesing the event will post a Circuit DOWN event
         * to the control module, which will be processed by the Update module
         * appropriately, and will invoke IsisAdjDelAdj () routine with
         * u1EvtFlag as ISIS_FALSE so that Adjacency DOWN events will not
         * unnecessarily get posted.
         */

        if ((ISIS_CONTEXT_ACTIVE (pContext) == ISIS_TRUE)
            && (u1EvtFlag == ISIS_TRUE))
        {
            /* While an adjacency is being deleted, Metric is immaterial. Update
             * module will delete the adjacency from the self LSPs irrespective
             * of the Metric value. Metric value is required only during Add
             * adjacency process and hence the second argument, which should 
             * have benn the Circuit Level pointer, is passed as NUL
             */

            if (pAdjRec->u1AdjState == ISIS_ADJ_UP)
            {
                IsisAdjEnqueueAdjChgEvt (pCktRec, NULL, pAdjRec, ISIS_ADJ_DOWN);
            }

            if (pCktRec->u1CktType == ISIS_BC_CKT)
            {
                IsisAdjEnqueueDISElectEvt (pCktRec, pAdjRec);
            }
        }

        /* Free the memory held by the Deleted Adjacency
         */
        u1AdjState = ISIS_GET_ADJ_STATE (pAdjRec);
        pAdjRec->pCktRec = NULL;

        if (u1AdjState == ISIS_ADJ_UP)
        {
            pCktLvl->u4NumAdjs--;

            if ((pCktLvl->u4NumAdjs == 0)
                && (ISIS_CONTEXT_ACTIVE (pContext) == ISIS_TRUE))
            {
                if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
                {
                    IsisUtlReSetBPat (&(pContext->CktTable.pu1L1CktMask),
                                      pCktRec->u4CktIdx);
                    IsisTmrStopECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                        pCktRec->u4CktIdx,
                                        pCktRec->pL1CktInfo->u1LSPTxTmrIdx);
                    TMP_PT ((ISIS_LGST, "TMR <T> : [L1] LSP Tx Timer Stopped - "
                             "Circuit [ %u ]\n", pCktRec->u4CktIdx));
                }
                if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
                {
                    IsisUtlReSetBPat (&(pContext->CktTable.pu1L2CktMask),
                                      pCktRec->u4CktIdx);
                    IsisTmrStopECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                        pCktRec->u4CktIdx,
                                        pCktRec->pL2CktInfo->u1LSPTxTmrIdx);
                    TMP_PT ((ISIS_LGST, "TMR <T> : [L2] LSP Tx Timer Stopped - "
                             "Circuit [ %u ]\n", pCktRec->u4CktIdx));
                }
            }

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
            {
                (pContext->CktTable.u4NumL1Adjs)--;
            }
            if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
            {
                (pContext->CktTable.u4NumL2Adjs)--;
            }
        }

        ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
    }
    /* Reset cache-pointer for isisAdjTable */
    pCktRec->pLastAdjEntry = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDelAdj ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjModifyAdjAreaAddr ()
 * Description   : This routine Verifies that all Area Addresses listed in the
 *                 PDU are included in the local database. If any new addresses
 *                 are included in the PDU, then those addresses are added to
 *                 the database. If any of the address included in the local
 *                 database is not found in the received PDU, these addresses
 *                 are removed from the database.
 * Input(s)      : pu1PDU  - Pointer to the received PDU
 *                 pCktRec - Pointer to the circuit record
 *                 pAdjRec - Pointer to the adjacency record to be modified
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisAdjModifyAdjAreaAddr (UINT1 *pu1PDU, tIsisCktEntry * pCktRec,
                          tIsisAdjEntry * pAdjRec)
{
    UINT1               u1TlvLen = 0;
    UINT1               u1AALen = 0;
    UINT2               u2Offset = 0;
    tIsisAdjAAEntry    *pEnd = NULL;
    tIsisAdjAAEntry    *pHead = NULL;
    tIsisAdjAAEntry    *pTrav = NULL;
    tIsisAdjAAEntry    *pPrev = NULL;
    tIsisAdjAAEntry    *pNewAA = NULL;
    tIsisAdjAAEntry    *pAreaAddr = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjModifyAdjAreaAddr ()\n"));

    if ((*(pu1PDU + 4)) == ISIS_P2P_IIH_PDU)
    {
        /* Get to the Variable Length Fields Offset */

        u2Offset = sizeof (tIsisComHdr) + 6 + ISIS_SYS_ID_LEN;
    }
    else
    {
        /* Get to the Variable Length Fields Offset */

        u2Offset = sizeof (tIsisComHdr) + 7 + (2 * ISIS_SYS_ID_LEN);
    }

    pAreaAddr = pAdjRec->pAdjAreaAddr;

    /* All nodes linked in the list woth head 'pHead' are Address
     * nodes which already exist in the database and also listed 
     * in the PDU and hence matched nodes. These nodes will be 
     * retained in the database
     */

    pHead = NULL;

    /* At the end all the records which are present after
     * this pointer are un-matched ones and hence will be
     * deleted from the database
     */

    pEnd = pAreaAddr;

    /* Loop through the PDU fetching Area Address TLVs one by one. For each TLV
     * so fetched process all the addresses in a single run. Continue till all
     * Area Address TLVs are processed
     */

    while (IsisUtlGetNextTlvOffset (pu1PDU, ISIS_AREA_ADDR_TLV, &u2Offset,
                                    &u1TlvLen) != ISIS_FAILURE)
    {
        /* Check whether the Area Addresses listed in the PDU are there
         * in the Adjacency Database. If not present, add the Area Address
         * from the PDU to the database, and then send Lock Step Update
         * Trav always must start from pEnd, since all the nodes
         * before pEnd are already matched.
         */

        pTrav = pEnd;

        /* whenever pTrav is assigned to pEnd, pPrev must be 
         * NULL, since we are starting from the beginning again
         */

        pPrev = NULL;
        while (u1TlvLen > 0)
        {
            /* Some more addresses remain in the TLV */

            while (pTrav != NULL)
            {
                if (pTrav->ISAdjAreaAddress.u1Length < ISIS_AREA_ADDR_LEN)
                {
                    if ((*(UINT1 *) (pu1PDU + u2Offset)
                         != pTrav->ISAdjAreaAddress.u1Length)
                        || ((MEMCMP (pTrav->ISAdjAreaAddress.au1AreaAddr,
                                     (UINT1 *) (pu1PDU + u2Offset + 1),
                                     pTrav->ISAdjAreaAddress.u1Length)) != 0))
                    {
                        /* TLV address and Database Address does not match - Keep
                         * moving
                         */

                        pPrev = pTrav;
                        pTrav = pTrav->pNext;
                    }
                    else
                    {
                        /* We have found a match */

                        if (pPrev == NULL)
                        {
                            /* Very first address matched. Advance the End pointer
                             * so that this node remains in the new list
                             */

                            pEnd = pEnd->pNext;
                        }
                        else
                        {
                            /* Address matched. Advance the End pointer so that 
                             * this node remains in the new list
                             */

                            pPrev->pNext = pTrav->pNext;
                        }

                        /* Insert the matched node at the head of the list pointed
                         * to by pHead
                         */

                        pTrav->pNext = pHead;
                        pHead = pTrav;

                        /* Advancing the search inside the PDU
                         */

                        u1TlvLen =
                            (UINT1) (u1TlvLen - *(pu1PDU + u2Offset) - 1);
                        u2Offset =
                            (UINT2) (u2Offset + *(pu1PDU + u2Offset) + 1);

                        /* We can start our next search in the database from pEnd,
                         * since all nodes prior to pEnd have already been matched
                         */

                        pTrav = pEnd;
                        pPrev = NULL;
                    }
                }
            }

            /* We come to this point only if one of the addresses in the TLV
             * does not match with any of the addresses in the Database. This
             * is a new address
             */

            if (u1TlvLen != 0)
            {
                u1AALen = *(UINT1 *) (pu1PDU + u2Offset);
                if (u1AALen <= ISIS_AREA_ADDR_LEN)
                {
                    pNewAA = (tIsisAdjAAEntry *)
                        ISIS_MEM_ALLOC (ISIS_BUF_ADAA,
                                        sizeof (tIsisAdjAAEntry));

                    if (pNewAA != NULL)
                    {
                        pNewAA->ISAdjAreaAddress.u1Length = u1AALen;
                        MEMCPY (pNewAA->ISAdjAreaAddress.au1AreaAddr,
                                (UINT1 *) (pu1PDU + u2Offset + 1), u1AALen);

                        pNewAA->pNext = pHead;
                        pHead = pNewAA;

                        ISIS_FLTR_ADJ_AA_LSU (pCktRec->pContext, ISIS_CMD_ADD,
                                              pAdjRec, pNewAA);
                    }
                    else
                    {
                        IsisUtlFormResFailEvt (pCktRec->pContext,
                                               ISIS_BUF_ADAA);
                        ADP_EE ((ISIS_LGST, "ADJ <X> : "
                                 "Exiting IsisAdjModifyAdjAreaAddr ()\n"));
                        return;
                    }
                }
                u1TlvLen = (UINT1) (u1TlvLen - *(pu1PDU + u2Offset) - 1);
                u2Offset = (UINT2) (u2Offset + *(pu1PDU + u2Offset) + 1);
            }
            pTrav = pEnd;
            pPrev = NULL;
        }
    }
    pAdjRec->pAdjAreaAddr = pHead;

    while (pEnd != NULL)
    {
        /* Remove the Area Addresses that are not to be included in the updated
         * list. These address nodes start from pEnd.
         */

        pTrav = pEnd;
        pEnd = pTrav->pNext;

        ISIS_DBG_PRINT_ADDR (pTrav->ISAdjAreaAddress.au1AreaAddr,
                             (UINT1) pTrav->ISAdjAreaAddress.u1Length,
                             "ADJ <T> : Deleting Unmatched Address\t",
                             ISIS_OCTET_STRING);

        ISIS_FLTR_ADJ_AA_LSU (pCktRec->pContext, ISIS_CMD_DELETE,
                              pAdjRec, pTrav);
        ISIS_MEM_FREE (ISIS_BUF_ADAA, (UINT1 *) pTrav);
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjModifyAdjAreaAddr ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjModifyP2PAdj ()
 * Description   : This routine modifies an existing Adjacency in the database
 *                 with information taken from the received PDU
 * Input(s)      : pu1PDU   - Pointer to the received PDU
 *                 pCktRec  - Pointer to Circuit Record
 *                 pAdjRec  - Pointer to the adjacency record to be modified
 *                 u1AdjUsage - Adjacency Usage - L1,L2 or L12
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjModifyP2PAdj (UINT1 *pu1PDU, tIsisCktEntry * pCktRec,
                     tIsisAdjEntry * pAdjRec, UINT1 u1AdjUsage)
{
#ifdef BFD_WANTED
    INT4                i4RetVal = ISIS_FAILURE;
#ifdef ISIS_FT_ENABLED
    UINT1               u1PrevMt0BfdRegdStatus = ISIS_FALSE;
    UINT1               u1PrevMt2BfdRegdStatus = ISIS_FALSE;
#endif
#endif
    UINT1               au1P2PCktID[ISIS_SYS_ID_LEN + 1];
    UINT4               u4DirIdx = 0;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisSysContext    *pContext = pCktRec->pContext;
    tBool               bLSUFlag = ISIS_FALSE;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    CHAR                acP2PCktID[(3 * ISIS_SYS_ID_LEN) + 1];
    CHAR                acCLCktID[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjModifyP2PAdj ()\n"));

    /* For Point to Point circuits circuit level record is always
     * pCktRec->pL1CktInfo irrespective of the PDU type or the Circuit Level
     */

    pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);

    /* Updating Hold Time, Adjacency Usage 
     */

    pAdjRec->u1AdjUsage = u1AdjUsage;
    ISIS_EXTRACT_HOLD_TIME (pu1PDU, pAdjRec->u2HoldTime);

    /* Calculate the P2P Circuit ID afresh based on the newly arrived PDU. If
     * the P2P circuit ID held in the Circuit Level record differs from the
     * newly calculated value, then the adjacency has changed. Remove the
     * existing adjecency, build a new record with the information available in
     * the PDU, and update the database
     */

    if ((MEMCMP (pContext->SysActuals.au1SysID, pAdjRec->au1AdjNbrSysID,
                 ISIS_SYS_ID_LEN)) >= 0)
    {
        /* Local system has preference over the neighbour
         */

        MEMCPY (au1P2PCktID, pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
        au1P2PCktID[ISIS_SYS_ID_LEN] = pCktRec->u1CktLocalID;
    }
    else
    {
        /* Neighbour is preferred over the Local system 
         */

        MEMCPY (au1P2PCktID, (pu1PDU + sizeof (tIsisComHdr) + 1),
                ISIS_SYS_ID_LEN);
        au1P2PCktID[ISIS_SYS_ID_LEN]
            = *(UINT1 *) (pu1PDU + sizeof (tIsisComHdr) + 5 + ISIS_SYS_ID_LEN);
    }

    /* Refer 8.2.4.2 c)
     */

    /* We accept the new adjacency
     */

    /* Checking if the Point to Point Circuit ID is matching.
     * Refer 8.2.4.2 d) of ISO 10589 Spec
     */

    if ((pAdjRec->u1AdjState == ISIS_ADJ_UP)
        && (MEMCMP (pCktRec->au1P2PCktID, au1P2PCktID,
                    ISIS_SYS_ID_LEN + 1) != 0))
    {
        /* P2P Circuit ID changed, remove the existing adjacency and add a new
         * adjacency based on information available in the PDU
         */

        /* IsisAdjRemAdj () removes the node from the list of adjacencies
         * but does not free the memory. It does not de-link the node from the
         * Direction table either. Use the same node to construct a new
         * adjacency
         */
        ISIS_FORM_STR (ISIS_SYS_ID_LEN, au1P2PCktID, acP2PCktID);
        ISIS_FORM_STR (ISIS_SYS_ID_LEN, pCktLevel->au1CLCktID, acCLCktID);
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Adjacency Deleted - P2P Circuit ID Changed - Circuit Index [ %u ], Old ID [ %s ], New ID [ %s ] \n",
                 pCktRec->u4CktIdx, acCLCktID, acP2PCktID));

        IsisAdjRemAdj (pCktRec, pAdjRec);

        /* Sending Adjacency Down Event so that Update Module can update Self
         * Lsps
         */

        IsisAdjEnqueueAdjChgEvt (pCktRec, pCktLevel, pAdjRec, ISIS_ADJ_DOWN);

        IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, ISIS_ADJ_INIT, NULL,
                                 u1AdjUsage, pAdjRec);

        ISIS_DBG_PRINT_ID (pAdjRec->au1AdjNbrSysID, (UINT1) (ISIS_SYS_ID_LEN),
                           "ADJ <T> : Adjacency Re-Initialised For Peer\t",
                           ISIS_OCTET_STRING);

        /* The last argument is ISIS_FALSE since the node is already present in
         * the Direction Table. If the argument is ISIS_TRUE, then the following
         * routine will try to add a new direction entry for the adjacency being
         * added
         */

        IsisAdjAddP2PThreewayAdj (pContext, pCktRec, pAdjRec);
    }

    /* If the action taken is to set the adjacency state UP
     */

    else if (pAdjRec->u1AdjState != ISIS_ADJ_UP)
    {

#ifdef BFD_WANTED
        i4RetVal = IsisAdjProcBfdinP2PIIH (pCktRec, pAdjRec, pu1PDU, &bLSUFlag);
        if (i4RetVal == ISIS_FAILURE)
        {
            return;
        }
#endif

        MEMCPY (pAdjRec->au1AdjNbrSysID, (pu1PDU + sizeof (tIsisComHdr) + 1),
                ISIS_SYS_ID_LEN);
        pAdjRec->u1AdjNbrSysType = ISIS_EXTRACT_HELLO_CKT_TYPE (pu1PDU);

        ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID, acNbrSysId);
        ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                             ISIS_MAX_IPV4_ADDR_LEN);

        ADP_PI ((ISIS_LGST,
                 "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                 " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                 ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                 pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                 au1IPv4Addr,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pCktRec->au1IPV6Addr))));

        if (pAdjRec->u1AdjState == ISIS_ADJ_UP)
        {
            ISIS_INCR_ADJ_CHG_STAT (pCktRec);

            /* Post Adjacency Change Event to the Control Module
             */
            bLSUFlag = ISIS_TRUE;
            IsisAdjEnqueueAdjChgEvt (pCktRec, pCktLevel, pAdjRec, ISIS_ADJ_UP);
            pCktLevel->u4NumAdjs++;

            if (pCktLevel->u4NumAdjs == 1)
            {
                if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
                {
                    IsisUtlSetBPat (&pContext->CktTable.pu1L1CktMask,
                                    pCktRec->u4CktIdx);

                    /* First adjacency on the given circuit has come up. We can now
                     * start the LSP transmission timer
                     */

                    IsisTmrStartECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                         pCktRec->u4CktIdx, 1,
                                         &pCktLevel->u1LSPTxTmrIdx);
                }

                if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
                {
                    IsisUtlSetBPat (&pContext->CktTable.pu1L2CktMask,
                                    pCktRec->u4CktIdx);

                    /* First adjacency on the given circuit has come up. We can now
                     * start the LSP transmission timer
                     */

                    IsisTmrStartECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                         pCktRec->u4CktIdx, 1,
                                         &pCktLevel->u1LSPTxTmrIdx);
                }
            }

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
            {
                (pContext->CktTable.u4NumL1Adjs)++;
            }
            if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
            {
                (pContext->CktTable.u4NumL2Adjs)++;
            }

            pDirEntry = (tIsisAdjDirEntry *)
                ISIS_MEM_ALLOC (ISIS_BUF_ADIR, sizeof (tIsisAdjDirEntry));

            if (pDirEntry != NULL)
            {
                IsisAdjAddDirEntry (pContext, pAdjRec, pDirEntry);
                u4DirIdx = pDirEntry->u4DirIdx;
            }
            else
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Adjacency Direction Entry!\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddAdj ()\n"));
                return;
            }
        }
    }
    else
    {
#ifdef BFD_WANTED
        i4RetVal = IsisAdjProcBfdinP2PIIH (pCktRec, pAdjRec, pu1PDU, &bLSUFlag);
        if (i4RetVal == ISIS_FAILURE)
        {
            return;
        }
#endif

    }

    /* Now Updating Adjacency Area Addresses
     */

    IsisAdjModifyAdjAreaAddr (pu1PDU, pCktRec, pAdjRec);

    if (pCktRec->pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        IsisAdjUpdateMTId (pu1PDU, pAdjRec, &bLSUFlag);
    }
    if ((pContext->u1IsisMTSupport == ISIS_FALSE) ||
        (pCktRec->pContext->u1IsisBfdSupport != ISIS_BFD_ENABLE) ||
        (pCktRec->u1IsisBfdStatus != ISIS_BFD_ENABLE))
    {
        if (IsisAdjUpdateIpAddr (pu1PDU, pAdjRec, &bLSUFlag) != ISIS_SUCCESS)
        {
            return;
        }
    }
#ifdef BFD_WANTED
    if ((pAdjRec->u1AdjState == ISIS_ADJ_UP) &&
        (pContext->u1IsisMTSupport == ISIS_FALSE) &&
        (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
    {
#ifdef ISIS_FT_ENABLED
        u1PrevMt0BfdRegdStatus = pAdjRec->u1Mt0BfdRegd;
        u1PrevMt2BfdRegdStatus = pAdjRec->u1Mt2BfdRegd;
#endif
        ISIS_BFD_REGISTER (pContext, pAdjRec, pCktRec);
#ifdef ISIS_FT_ENABLED
        if ((u1PrevMt0BfdRegdStatus != pAdjRec->u1Mt0BfdRegd) ||
            (u1PrevMt2BfdRegdStatus != pAdjRec->u1Mt2BfdRegd))
        {
            bLSUFlag = ISIS_TRUE;
        }
#endif
    }
#endif

#ifdef IS_FT_WANTED
    /* Send a Lock Step Update if FT Support is Enabled and if the local
     * system is in LSU ENABLE State
     */
    if (bLSUFlag == ISIS_TRUE)
    {
        ISIS_FLTR_ADJ_LSU (pContext, ISIS_CMD_ADD, pAdjRec);
        ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec,
                               pCktLevel, pCktRec->u1CktExistState);
    }
#endif
    MEMCPY (pCktRec->au1P2PCktID, au1P2PCktID,
            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

    /* Adjacency is modified and hence the Holding Timer is restarted.
     * pAdjRec->u1TmrIdx specifies the index as well as the timer class and
     * hence it is simpler to locate the timer block in the appropriate EC.
     * Restart is translated into Stop and the Start.
     */

    if (pAdjRec->u1DoNotUpdHoldTmr == ISIS_BFD_FALSE)
    {
        u4DirIdx = IsisAdjGetDirEntryWithAdjIdx (pContext, pAdjRec);

        IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                               u4DirIdx, pAdjRec->u2HoldTime,
                               &pAdjRec->u1TmrIdx);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjModifyP2PAdj ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjValP2PHelloSelfL1 ()
 * Description   : This routine validates Point-to-Point Hello PDU and
 *                 updates the Adjacency DataBase, the local IS being Level1 IS
 * Input(s)      : pCktRec   - Pointer to Circuit Record over which the Hello
 *                             PDU was received
 *                 pu1PDU    - Pointer to Hello PDU
 *                 u1CktType - Peer System Type (specifications refer to this as
 *                             Circuit Type in the Hello PDUs
 * Globals       : Not Referred or Modified
 * Output(s)     : None
 * Returns       : ISIS_SUCCESS - if the received PDU is valid and Adjacency 
 *                                DataBase is updated 
 *                 ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjValP2PHelloSelfL1 (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                          UINT1 u1CktType)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4DirIdx = 0;
    tBool               bAdjExist = ISIS_TRUE;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjValP2PHelloSelfL1 ()\n"));

    pContext = pCktRec->pContext;
    /* If the Adjacency Entry doesnt exist, build Adjacency record from the
     * information available in the received PDU. Note that a P2P circuit has
     * only one adjacency record unlike a Broadcast circuit which can have a
     * list of adjacencies
     */

    if (pCktRec->pAdjEntry == NULL)
    {
        /* This is the first request from the Peer. Create a new adjacency 
         */

        pAdjRec = (tIsisAdjEntry *)
            ISIS_MEM_ALLOC (ISIS_BUF_ADJN, sizeof (tIsisAdjEntry));

        if (pAdjRec != NULL)
        {
            bAdjExist = ISIS_FALSE;

            /* Building Adjacency Record with information from the PDU. Since
             * the Local System is L1, we can mark the Adjacency Usage as Level1
             * and later reject the adjacency if we find the peer is
             * incompatible. The following routine will build the complete
             * Adjacency record including Adjacent Area Addresses, Protocol
             * supported and IPV4 Addresses.
             */

            IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, ISIS_ADJ_UP, NULL,
                                     ISIS_L1ADJ, pAdjRec);
        }
        else
        {
            /* No Memory to hold the adjacency, Post an Event
             */

            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Adjacency\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_ADJN);
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjValP2PHelloSelfL1 ()\n"));
            return ISIS_FAILURE;
        }
    }
    else
    {
        /* Take the existing adjancency
         */

        pAdjRec = pCktRec->pAdjEntry;
    }

    /* 'u1CktType' is the Peer Node's System Type. Specifications refers to the
     * System Type as Circuit Type in the Control PDUs and thou shall use the
     * same
     */

    switch (u1CktType)
    {
        case ISIS_LEVEL1:
        case ISIS_LEVEL12:

            /* Local IS is L1 and the peer is either L1 or L12
             */

            /* For a valid adjacency to exist between two systems, they both
             * must have compatible System Types and also the Circuit over
             * which the adjacency exist need to be compatible. At this
             * point the systems are compatible, check for the circuit
             */

            if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {

                /* Everything is fine, Adjacency can be brought UP immediately
                 */

                if (bAdjExist == ISIS_TRUE)
                {
                    /* Since an adjacency already exist, there must have been a
                     * change in the priority. Just update those parameters that
                     * might have changed
                     */

                    IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec, ISIS_L1ADJ);
                }
                else
                {
                    /* New adjacency altogether. Add it to the Adjacency
                     * database and mark the adjacency UP and usage as L1.
                     * Transmit Hello and start Hello timer
                     */

                    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                   acNbrSysId);
                    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                         ISIS_MAX_IPV4_ADDR_LEN);

                    ADP_PI ((ISIS_LGST,
                             "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                             " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));
#ifdef BFD_WANTED
                    pAdjRec->u1AdjState = ISIS_ADJ_INIT;
                    IsisAdjAddP2PThreewayAdj (pCktRec->pContext, pCktRec,
                                              pAdjRec);
#endif
                    IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec, ISIS_L1ADJ);
                    IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                    pCktRec->u1CktLevel, ISIS_TRUE);
                }
                i4RetVal = ISIS_SUCCESS;
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : P2P Adjacency Rejected - Incompatible Level - Circuit Index [ %u ], Peer [ %u ], Self [ %s ]\n",
                         pCktRec->u4CktIdx, u1CktType, "ISIS_LEVEL1"));

                if (bAdjExist == ISIS_FALSE)
                {
                    /* Release the Adjancency Record that was built
                     */

                    IsisAdjDelAdjAreaAddrs (pAdjRec);
                    ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
                }

                IsisAdjEnqueueIncompCktEvt (pContext, pCktRec);
                i4RetVal = ISIS_FAILURE;
            }
            break;

        case ISIS_LEVEL2:

            /* Local IS is L1 and the peer is L2 and hence incompatible. 
             */

            if (bAdjExist == ISIS_TRUE)
            {
                /* An adjacency already exists. Delete the adjcency since the
                 * two peer systems have become incompatible
                 */

                IsisAdjDelAdj (pContext, pCktRec, pAdjRec, ISIS_TRUE,
                               &u4DirIdx);

                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : P2P Adjacency Deleted - Incompatible Peer - Circuit Index [ %u ], Self [ %s ], Peer [ %u ]\n",
                         pCktRec->u4CktIdx, "ISIS_LEVEL1", u1CktType));

                /* Stop the Hold Timer as the adjacency does not exist anymore
                 */

                IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING, u4DirIdx,
                                    pCktRec->pL1CktInfo->u1HelloTmrIdx);
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : IIH - Ignored - Incompatible Peer - Circuit Index [ %u ], Self [ %s ], Peer [ %u ]\n",
                         pCktRec->u4CktIdx, "ISIS_LEVEL2", u1CktType));

                /* Release the Adjancency Record that was built
                 */

                IsisAdjEnqueueWrongSysEvt (pContext, pu1PDU);
                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }

            i4RetVal = ISIS_FAILURE;
            break;

        default:

            /* System Type can never be anything other than L1, L2 or L12. Even
             * if a PDU with erroneous system type is received, do not bring
             * down the adjancency since this condition may be temporary.
             * According to ISO 10589, Sec 12.2.4.3 c). adjacencies are not
             * supposed to be dropped for trivial reasons
             */

            /* We have created one Adjacency record in the beginning of this
             * routine. Release that. If the adjacency was already existing then
             * do not do anything to disturb the setup.
             */

            if (bAdjExist == ISIS_FALSE)
            {
                /* Release the Adjancency Record that was built
                 */

                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }
            ADP_PT ((ISIS_LGST, "ADJ <T> : Invalid Peer System Type [ %u ]\n",
                     u1CktType));
            i4RetVal = ISIS_FAILURE;
    }

    /* Freeing the memory Allocated for holding the received PDU
     */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjValP2PHelloSelfL1 ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjValAAMismatchHello ()
 * Description   : This routine handles Point-to-Point Hello PDUs when Area
 *                 Addresses configured for the local IS do not match with 
 *                 any of the addresses included in the PDU. 
 * Input(s)      : pCktRec    - Pointer to Circuit Record
 *                 pu1PDU     - Pointer to ithe received Hello Pdu
 *                 u1CktType  - Peer System Type 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - if Adjacency DataBase is updated 
 *                 ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjValAAMismatchHello (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                           UINT1 u1CktType)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TmrIdx = 0;
    UINT4               u4DirIdx = 0;
    tBool               bAdjExist = ISIS_TRUE;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjValAAMismatchHello ()\n"));

    pContext = pCktRec->pContext;
    pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
    UNUSED_PARAM (pCktLevel);
    if (pCktRec->pAdjEntry == NULL)
    {
        /* Local system is L12 and Peer is either L12 or L2 and area addresses
         * did not match. Hence consider the PDU to be a Level2 PDU and proceed
         */

        pAdjRec = (tIsisAdjEntry *) ISIS_MEM_ALLOC (ISIS_BUF_ADJN,
                                                    sizeof (tIsisAdjEntry));
        if (pAdjRec != NULL)
        {
            bAdjExist = ISIS_FALSE;

            /* Build the adjacency as a Level2 adjacency since Area Addresses
             * did not match
             */

            IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, ISIS_ADJ_UP, NULL,
                                     ISIS_L2ADJ, pAdjRec);
        }
        else
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Adjacency\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_ADJN);
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjValAAMismatchHello ()\n"));
            return ISIS_FAILURE;
        }
    }
    else
    {
        pAdjRec = pCktRec->pAdjEntry;
    }

    switch (u1CktType)
    {
        case ISIS_LEVEL1:

            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : P2P Adjacency Rejected - Area Address Mismatch - Peer [ ISIS_LEVEL1 ]\n"));
            if (bAdjExist == ISIS_TRUE)
            {
                /* Peer is L1 and Area Addresses did not match */

                u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;

                IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                               ISIS_TRUE, &u4DirIdx);

                /* Stopping the Hold Timer started for this adjacency
                 */

                IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                    u4DirIdx, u1TmrIdx);

                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Attempt To Add A Wrong System\n"));
            }
            else
            {
                /* Free the adjacency record that was built */

                IsisAdjEnqueueWrongSysEvt (pContext, pu1PDU);
                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }
            ISIS_INCR_SYS_DROPPED_PDUS (pContext);
            break;

        case ISIS_LEVEL2:
        case ISIS_LEVEL12:

            if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {
                if (bAdjExist == ISIS_TRUE)
                {
                    if ((pAdjRec->u1AdjUsage == ISIS_UNKNOWN)
                        || (pAdjRec->u1AdjUsage == ISIS_L2ADJ))
                    {
                        /* Since Area Addresses did not match, the adjacency
                         * usage can only be ISIS_L2ADJ eventhough the Peer is a
                         * L12 System
                         */

                        IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec,
                                             ISIS_L2ADJ);
                    }
                    else
                    {
                        /* Adjacency Usage changed. Delete the existing
                         * adjacency
                         */

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : P2P Adjacency Modified - Usage Changed to L2\n"));
                        u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;
                        IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                                       ISIS_TRUE, &u4DirIdx);

                        /* Stopping the Hold Timer started for this adjacency
                         */

                        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                            u4DirIdx, u1TmrIdx);
                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Attempt To Add A Wrong System\n"));
                    }
                }
                else
                {
                    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                   acNbrSysId);
                    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                         ISIS_MAX_IPV4_ADDR_LEN);

                    ADP_PI ((ISIS_LGST,
                             "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                             " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));

#ifdef BFD_WANTED
                    pAdjRec->u1AdjState = ISIS_ADJ_INIT;
                    IsisAdjAddP2PThreewayAdj (pCktRec->pContext, pCktRec,
                                              pAdjRec);
#endif
                    /* Adjacency Usage already set to L2 */
                    IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec, ISIS_L2ADJ);
                    IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                    pCktRec->u1CktLevel, ISIS_TRUE);
                    i4RetVal = ISIS_SUCCESS;
                }
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : P2P Adjacency Rejected - Incompatible Level - Circuit Index [ %u ], Peer [ %u ], Self [ %s ]\n",
                         pCktRec->u4CktIdx, u1CktType, "ISIS_LEVEL1"));

                if (bAdjExist != ISIS_TRUE)
                {
                    /* Free the adjacency record that was built */

                    IsisAdjDelAdjAreaAddrs (pAdjRec);
                    ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
                }
                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                IsisAdjEnqueueIncompCktEvt (pContext, pCktRec);
                i4RetVal = ISIS_FAILURE;
            }
            break;

        default:

            if (bAdjExist != ISIS_TRUE)
            {
                /* Free the adjacency record that was built */

                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }
            ISIS_INCR_SYS_DROPPED_PDUS (pContext);
            ADP_PT ((ISIS_LGST, "ADJ <T> : Invalid Peer System Type"));
            i4RetVal = ISIS_FAILURE;
    }

    /* Freeing the memory Allocated for  PDU
     */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjValAAMismatchHello ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjValP2PHelloSelfL12 ()
 * Description   : This routine validates Point-to-Point Hello PDU and
 *                 updates the Adjacency DataBase, the local IS being Level2 IS
 * Input(s)      : pCktRec      - Pointer to Circuit Record
 *                 pu1PDU       - Pointer to the received Hello Pdu
 *                 u1CktType    - Peer System Type as reported in the PDU
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - if Adjacency DataBase is updated 
 *                 ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjValP2PHelloSelfL12 (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                           UINT1 u1CktType)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TmrIdx = 0;
    UINT4               u4DirIdx = 0;
    tBool               bAdjExist = ISIS_TRUE;
    tIsisSysContext    *pContext = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjValP2PHelloSelfL12 ()\n"));

    /* Retrieve the context from the Circuit Record
     */

    pContext = pCktRec->pContext;
    pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
    UNUSED_PARAM (pCktLevel);
    if (pCktRec->pAdjEntry == NULL)
    {
        /* Adjacency does not exist. Build the Adjacency information from the
         * information available in the PDU
         */

        pAdjRec = (tIsisAdjEntry *)
            ISIS_MEM_ALLOC (ISIS_BUF_ADJN, sizeof (tIsisAdjEntry));

        if (pAdjRec != NULL)
        {
            bAdjExist = ISIS_FALSE;

            /* Build the record and mark the adjacency as UNKNOWN. which can be
             * marked later with proper type depending on the Peer System Type
             * and the Circuit Type
             */

            IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, ISIS_ADJ_UP, NULL,
                                     ISIS_UNKNOWN, pAdjRec);
        }
        else
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Adjacency\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_ADJN);
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjValP2PHelloSelfL12 ()\n"));
            return ISIS_FAILURE;
        }
    }
    else
    {
        pAdjRec = pCktRec->pAdjEntry;
    }

    /* 'u1CktType' is the Peer Node's System Type. Specifications refers to the
     * System Type as Circuit Type in the Control PDUs and thou shall use the
     * same
     */

    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID, acNbrSysId);
    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    switch (u1CktType)
    {
        case ISIS_LEVEL1:

            /* Local IS is L12 and the peer is L1
             */

            /* For a valid adjacency to exist between two systems, they both
             * must have compatible System Types and also the Circuit over
             * which the adjacency exist need to be compatible. At this
             * point the systems are compatible, check for the circuit
             */

            if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {
                /* Everything is fine, Adjacency can be brought UP immediately
                 */

                if (bAdjExist == ISIS_TRUE)
                {
                    /* Since an adjacency already exist, there must have been a
                     * change in the priority. Just update those parameters that
                     * might have changed
                     */

                    if ((pAdjRec->u1AdjUsage == ISIS_UNKNOWN)
                        || (pAdjRec->u1AdjUsage == ISIS_L1ADJ))
                    {
                        /* The existing adjacency usage is compatible with the
                         * Peer System Type. Update the other parameters that
                         * might have changed
                         *
                         * Refer Table 6. Level 2 state table for matching areas
                         */

                        IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec,
                                             ISIS_L1ADJ);
                    }
                    else
                    {
                        /* Refer Table 6. Level 2 state table for 
                         * matching areas
                         */

                        u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;

                        IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                                       ISIS_TRUE, &u4DirIdx);

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Adjacency Deleted - Usage Changed To ISIS_ADJ_LEVEL1\n"));

                        /* Stopping the Hold Timer started for this adjacency
                         */

                        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                            u4DirIdx, u1TmrIdx);

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Attempt To Add A Wrong System\n"));
                    }
                }
                else
                {
                    ADP_PI ((ISIS_LGST,
                             "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                             " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));

                    pAdjRec->u1AdjUsage = ISIS_L1ADJ;
#ifdef BFD_WANTED
                    pAdjRec->u1AdjState = ISIS_ADJ_INIT;
                    IsisAdjAddP2PThreewayAdj (pCktRec->pContext, pCktRec,
                                              pAdjRec);
#endif
                    IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec, ISIS_L1ADJ);
                    IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                    pCktRec->u1CktLevel, ISIS_TRUE);
                }
                i4RetVal = ISIS_SUCCESS;
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : P2P Adjacency Rejected - Incompatible Level -"
                         " Circuit Index [ %u ], Peer [ %s ], Self [ %s ]\n",
                         pCktRec->u4CktIdx,
                         ISIS_GET_CKT_LVL_STR (pCktRec->u1CktLevel),
                         "ISIS_LEVEL2"));

                if (bAdjExist == ISIS_FALSE)
                {
                    /* Free the adjacency record that was built
                     */

                    IsisAdjDelAdjAreaAddrs (pAdjRec);
                    ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
                }
                IsisAdjEnqueueIncompCktEvt (pContext, pCktRec);
                i4RetVal = ISIS_FAILURE;
            }
            break;

        case ISIS_LEVEL2:

            /* Local IS is L12 and the peer is L2
             */

            /* For a valid adjacency to exist between two systems, they both
             * must have compatible System Types and also the Circuit over
             * which the adjacency exist need to be compatible. At this
             * point the systems are compatible, check for the circuit
             */

            if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {
                if (bAdjExist == ISIS_TRUE)
                {
                    /* Since an adjacency already exist, there must have been a
                     * change in the priority. Just update those parameters that
                     * might have changed
                     */

                    if ((pAdjRec->u1AdjUsage == ISIS_UNKNOWN)
                        || (pAdjRec->u1AdjUsage == ISIS_L2ADJ))
                    {
                        /* The existing adjacency usage is compatible with the
                         * Peer System Type. Update the other parameters that
                         * might have changed
                         *
                         * Refer Table 6. Level 2 state table for matching areas
                         */

                        IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec,
                                             ISIS_L2ADJ);
                    }
                    else
                    {
                        /* Refer Table 6. Level 2 state table for 
                         * matching areas
                         */

                        u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;

                        IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                                       ISIS_TRUE, &u4DirIdx);

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Adjacency Deleted - Usage Changed to ISIS_ADJ_LEVEL2 -"
                                 " Previous Adjacency Usage [ %s ]\n",
                                 ISIS_GET_ADJ_USAGE_STR (pAdjRec->u1AdjUsage)));

                        /* Stopping the Hold Timer started for this adjacency
                         */

                        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                            u4DirIdx, u1TmrIdx);

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Attempt To Add A Wrong System\n"));
                    }
                }
                else
                {
                    ADP_PI ((ISIS_LGST,
                             "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                             " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));

                    pAdjRec->u1AdjUsage = ISIS_L2ADJ;
#ifdef BFD_WANTED
                    pAdjRec->u1AdjState = ISIS_ADJ_INIT;
                    IsisAdjAddP2PThreewayAdj (pCktRec->pContext, pCktRec,
                                              pAdjRec);
#endif
                    IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec, ISIS_L2ADJ);
                    IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                    pCktRec->u1CktLevel, ISIS_TRUE);

                }
                i4RetVal = ISIS_SUCCESS;
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : P2P Adjacency Rejected - Incompatible Level -"
                         " Circuit Index [ %u ], Self [ %s ], Peer [ %s ]\n",
                         pCktRec->u4CktIdx,
                         ISIS_GET_CKT_LVL_STR (pCktRec->u1CktLevel),
                         "ISIS_LEVEL1"));

                if (bAdjExist == ISIS_FALSE)
                {
                    /* Free the adjacency record that was built
                     */

                    IsisAdjDelAdjAreaAddrs (pAdjRec);
                    ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
                }

                IsisAdjEnqueueIncompCktEvt (pContext, pCktRec);
                i4RetVal = ISIS_FAILURE;
            }
            break;

        case ISIS_LEVEL12:

            if (bAdjExist == ISIS_TRUE)
            {
                if ((pAdjRec->u1AdjUsage == ISIS_UNKNOWN)
                    || (pAdjRec->u1AdjUsage == pCktRec->u1CktLevel))
                {
                    /* The existing adjacency usage is compatible with the
                     * Peer System Type. Update the other parameters that
                     * might have changed. Both peers are L12 and the adjacency 
                     * usage now depends on the Circuit Level. 
                     *
                     * Refer Table 6. Level 2 state table for matching areas
                     */

                    IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec,
                                         ISIS_GET_ADJ_USAGE_WITH_CKTLVL
                                         (pCktRec->u1CktLevel));
                    i4RetVal = ISIS_SUCCESS;
                }
                else
                {
                    u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;

                    IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                                   ISIS_TRUE, &u4DirIdx);

                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Adjacency Deleted - Usage Changed To ISIS_ADJ_LEVEL12\n"));

                    /* Stopping the Hold Timer started for this adjacency
                     */

                    IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                        u4DirIdx, u1TmrIdx);

                    IsisAdjEnqueueWrongSysEvt (pContext, pu1PDU);

                    i4RetVal = ISIS_FAILURE;
                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Attempt To Add A Wrong System\n"));
                }
            }
            else
            {
                ADP_PI ((ISIS_LGST,
                         "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                         " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                         pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));

                pAdjRec->u1AdjUsage = pCktRec->u1CktLevel;
#ifdef BFD_WANTED
                pAdjRec->u1AdjState = ISIS_ADJ_INIT;
                IsisAdjAddP2PThreewayAdj (pCktRec->pContext, pCktRec, pAdjRec);
#endif

                IsisAdjModifyP2PAdj (pu1PDU, pCktRec, pAdjRec,
                                     ISIS_GET_ADJ_USAGE_WITH_CKTLVL
                                     (pCktRec->u1CktLevel));
                IsisAdjTxHello (pCktRec->pContext, pCktRec, pCktRec->u1CktLevel,
                                ISIS_TRUE);
                i4RetVal = ISIS_SUCCESS;
            }
            break;

        default:

            /* Erroneous Peer System Type. Ignore the Hello PSU received
             */

            if (bAdjExist == ISIS_FALSE)
            {
                /* Free the adjacency record that was built
                 */

                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }

            ADP_PT ((ISIS_LGST, "ADJ <T> : Invalid Peer System Type [ %u ]\n",
                     u1CktType));
            i4RetVal = ISIS_FAILURE;
    }

    /* Freeing the memory Allocated for  PDU
     */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjValP2PHelloSelfL12 ()\n"));
    return (i4RetVal);
}

/* The following functions are used for making 2-way decision when 3-way handshake mechanism 
 * is enabled*/

/******************************************************************************
 * Function Name : IsisAdjValP2PThreewayHelloSelfL1MatAA ()
 * Description   : This routine validates Point-to-Point Hello PDU and
 *                 updates the Adjacency DataBase, the local IS being Level1 IS
 * Input(s)      : pCktRec   - Pointer to Circuit Record over which the Hello
 *                             PDU was received
 *                 pu1PDU    - Pointer to Hello PDU
 *                 u1CktType - Peer System Type (specifications refer to this as
 *                             Circuit Type in the Hello PDUs
 * Globals       : Not Referred or Modified
 * Output(s)     : None
 * Returns       : ISIS_SUCCESS - if the received PDU is valid and Adjacency 
 *                                DataBase is updated 
 *                 ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjValP2PThreewayHelloSelfL1MatAA (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                                       UINT1 u1CktType)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4DirIdx = 0;
    tBool               bAdjExist = ISIS_TRUE;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjValP2PHelloSelfL1 ()\n"));

    pContext = pCktRec->pContext;
    /* If the Adjacency Entry doesnt exist, build Adjacency record from the
     * information available in the received PDU. Note that a P2P circuit has
     * only one adjacency record unlike a Broadcast circuit which can have a
     * list of adjacencies
     */

    if (pCktRec->pAdjEntry == NULL)
    {
        /* This is the first request from the Peer. Create a new adjacency 
         */

        pAdjRec = (tIsisAdjEntry *)
            ISIS_MEM_ALLOC (ISIS_BUF_ADJN, sizeof (tIsisAdjEntry));

        if (pAdjRec != NULL)
        {
            bAdjExist = ISIS_FALSE;

            /* Building Adjacency Record with information from the PDU. Since
             * the Local System is L1, we can mark the Adjacency Usage as Level1
             * and later reject the adjacency if we find the peer is
             * incompatible. The following routine will build the complete
             * Adjacency record including Adjacent Area Addresses, Protocol
             * supported and IPV4 Addresses.
             */

            IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, ISIS_ADJ_UP, NULL,
                                     ISIS_L1ADJ, pAdjRec);
        }
        else
        {
            /* No Memory to hold the adjacency, Post an Event
             */

            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Adjacency\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_ADJN);
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjValP2PHelloSelfL1 ()\n"));
            return ISIS_FAILURE;
        }
    }
    else
    {
        /* Take the existing adjancency
         */

        pAdjRec = pCktRec->pAdjEntry;
    }

    /* 'u1CktType' is the Peer Node's System Type. Specifications refers to the
     * System Type as Circuit Type in the Control PDUs and thou shall use the
     * same
     */

    switch (u1CktType)
    {
        case ISIS_LEVEL1:
        case ISIS_LEVEL12:

            /* Local IS is L1 and the peer is either L1 or L12
             */

            /* For a valid adjacency to exist between two systems, they both
             * must have compatible System Types and also the Circuit over
             * which the adjacency exist need to be compatible. At this
             * point the systems are compatible, check for the circuit
             */

            if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {

                /* Everything is fine, Adjacency can be brought UP immediately
                 */
                pAdjRec->u1AdjUsage = ISIS_L1ADJ;
                if (bAdjExist == ISIS_TRUE)
                {
                    if (pAdjRec->u1AdjState != ISIS_ADJ_UP)
                    {
                        pAdjRec->u1AdjState = ISIS_ADJ_UP;
                        pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                    }
                    else
                    {
                        pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_ACCEPT;
                    }
                }
                else
                {
                    /* New adjacency altogether. Add it to the Adjacency
                     * database and mark the adjacency UP and usage as L1.
                     * Transmit Hello and start Hello timer
                     */

                    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                   acNbrSysId);
                    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                         ISIS_MAX_IPV4_ADDR_LEN);

                    ADP_PI ((ISIS_LGST,
                             "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                             " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));

                    IsisAdjAddP2PThreewayAdj (pContext, pCktRec, pAdjRec);
                    IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                    pCktRec->u1CktLevel, ISIS_TRUE);
                    pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                }
                i4RetVal = ISIS_SUCCESS;
            }
            else
            {
                /*Ideally this should not occur */
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : Adjacency Rejected - Incompatible Level -"
                         " Circuit Index [ %u ], Peer [ %s ], Self [ %s ]\n",
                         pCktRec->u4CktIdx,
                         ISIS_GET_CKT_LVL_STR (pCktRec->u1CktLevel),
                         "ISIS_LEVEL1"));

                if (bAdjExist == ISIS_FALSE)
                {
                    /* Release the Adjancency Record that was built
                     */

                    IsisAdjDelAdjAreaAddrs (pAdjRec);
                    ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
                }

                IsisAdjEnqueueIncompCktEvt (pContext, pCktRec);
                i4RetVal = ISIS_FAILURE;
            }
            break;

        case ISIS_LEVEL2:

            /* Local IS is L1 and the peer is L2 and hence incompatible. 
             */

            if (bAdjExist == ISIS_TRUE)
            {
                /* An adjacency already exists. Delete the adjcency since the
                 * two peer systems have become incompatible
                 */

                IsisAdjDelAdj (pContext, pCktRec, pAdjRec, ISIS_TRUE,
                               &u4DirIdx);

                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : Adjacency Deleted - Incompatible Peer - Self [ %s ], Peer [ %u ]\n",
                         "ISIS_LEVEL1", u1CktType));
                /* Stop the Hold Timer as the adjacency does not exist anymore
                 */

                IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING, u4DirIdx,
                                    pCktRec->pL1CktInfo->u1HelloTmrIdx);
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : IIH - Ignored - Incompatible Peer - Self [ %s ], Peer [ %u ]\n",
                         "ISIS_LEVEL2", u1CktType));

                /* Release the Adjancency Record that was built
                 */

                IsisAdjEnqueueWrongSysEvt (pContext, pu1PDU);
                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }

            i4RetVal = ISIS_FAILURE;
            break;

        default:

            /* System Type can never be anything other than L1, L2 or L12. Even
             * if a PDU with erroneous system type is received, do not bring
             * down the adjancency since this condition may be temporary.
             * According to ISO 10589, Sec 12.2.4.3 c). adjacencies are not
             * supposed to be dropped for trivial reasons
             */

            /* We have created one Adjacency record in the beginning of this
             * routine. Release that. If the adjacency was already existing then
             * do not do anything to disturb the setup.
             */

            if (bAdjExist == ISIS_FALSE)
            {
                /* Release the Adjancency Record that was built
                 */

                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }
            ADP_PT ((ISIS_LGST, "ADJ <T> : Invalid Peer System Type [ %u ]\n",
                     u1CktType));
            i4RetVal = ISIS_FAILURE;
    }

    /* Freeing the memory Allocated for holding the received PDU
     */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjValP2PHelloSelfL1 ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjValAAMismatchHelloThreeWay ()
 * Description   : This routine handles Point-to-Point Hello PDUs when Area
 *                 Addresses configured for the local IS do not match with 
 *                 any of the addresses included in the PDU. 
 * Input(s)      : pCktRec    - Pointer to Circuit Record
 *                 pu1PDU     - Pointer to ithe received Hello Pdu
 *                 u1CktType  - Peer System Type 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - if Adjacency DataBase is updated 
 *                 ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjValAAMismatchHelloThreeWay (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                                   UINT1 u1CktType)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TmrIdx = 0;
    UINT4               u4DirIdx = 0;
    tBool               bAdjExist = ISIS_TRUE;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjValAAMismatchHello ()\n"));

    pContext = pCktRec->pContext;
    pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
    UNUSED_PARAM (pCktLevel);
    if (pCktRec->pAdjEntry == NULL)
    {
        /* Local system is L12 and Peer is either L12 or L2 and area addresses
         * did not match. Hence consider the PDU to be a Level2 PDU and proceed
         */

        pAdjRec = (tIsisAdjEntry *) ISIS_MEM_ALLOC (ISIS_BUF_ADJN,
                                                    sizeof (tIsisAdjEntry));
        if (pAdjRec != NULL)
        {
            bAdjExist = ISIS_FALSE;

            /* Build the adjacency as a Level2 adjacency since Area Addresses
             * did not match
             */

            IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, ISIS_ADJ_UP, NULL,
                                     ISIS_L2ADJ, pAdjRec);
        }
        else
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Adjacency\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_ADJN);
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjValAAMismatchHello ()\n"));
            return ISIS_FAILURE;
        }
    }
    else
    {
        pAdjRec = pCktRec->pAdjEntry;
    }

    switch (u1CktType)
    {
        case ISIS_LEVEL1:

            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : Adjacency Rejected - Area Address Mismatch - Peer [ ISIS_LEVEL1 ]\n"));
            if (bAdjExist == ISIS_TRUE)
            {
                /* Peer is L1 and Area Addresses did not match */

                u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;

                IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                               ISIS_TRUE, &u4DirIdx);

                /* Stopping the Hold Timer started for this adjacency
                 */

                IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                    u4DirIdx, u1TmrIdx);

                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Attempt To Add A Wrong System\n"));
            }
            else
            {
                /* Free the adjacency record that was built */

                IsisAdjEnqueueWrongSysEvt (pContext, pu1PDU);
                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }
            ISIS_INCR_SYS_DROPPED_PDUS (pContext);
            break;

        case ISIS_LEVEL2:
        case ISIS_LEVEL12:

            if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {
                pAdjRec->u1AdjUsage = ISIS_L2ADJ;
                if (bAdjExist == ISIS_TRUE)
                {
                    if ((pAdjRec->u1AdjUsage == ISIS_UNKNOWN)
                        || (pAdjRec->u1AdjUsage == ISIS_L2ADJ))
                    {
                        /* Since Area Addresses did not match, the adjacency
                         * usage can only be ISIS_L2ADJ eventhough the Peer is a
                         * L12 System
                         */

                        if (pAdjRec->u1AdjState != ISIS_ADJ_UP)
                        {
                            pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                            pAdjRec->u1AdjState = ISIS_ADJ_UP;
                        }
                        else
                        {
                            pCktRec->u1TwoWayStateDecision =
                                ISIS_ADJ_STATE_ACCEPT;
                        }
                    }
                    else
                    {
                        /* Adjacency Usage changed. Delete the existing
                         * adjacency
                         */

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : P2P Adjacency Modified - Usage Changed to L2\n"));
                        u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;
                        IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                                       ISIS_TRUE, &u4DirIdx);

                        /* Stopping the Hold Timer started for this adjacency
                         */

                        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                            u4DirIdx, u1TmrIdx);
                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Attempt To Add A Wrong System\n"));
                    }
                }
                else
                {
                    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                                   acNbrSysId);
                    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                         ISIS_MAX_IPV4_ADDR_LEN);

                    ADP_PI ((ISIS_LGST,
                             "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                             " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));

                    /* Adjacency Usage already set to L2 */
                    IsisAdjAddP2PThreewayAdj (pContext, pCktRec, pAdjRec);
                    IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                    pCktRec->u1CktLevel, ISIS_TRUE);
                    pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                    i4RetVal = ISIS_SUCCESS;
                }
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : Adjacency Rejected - Incompatible Level -"
                         " Circuit Index [ %u ], Peer [ %s ], Self [ %s ]\n",
                         pCktRec->u4CktIdx,
                         ISIS_GET_CKT_LVL_STR (pCktRec->u1CktLevel),
                         "ISIS_LEVEL1"));

                if (bAdjExist != ISIS_TRUE)
                {
                    /* Free the adjacency record that was built */

                    IsisAdjDelAdjAreaAddrs (pAdjRec);
                    ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
                }
                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                IsisAdjEnqueueIncompCktEvt (pContext, pCktRec);
                i4RetVal = ISIS_FAILURE;
            }
            break;

        default:

            if (bAdjExist != ISIS_TRUE)
            {
                /* Free the adjacency record that was built */

                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }
            ISIS_INCR_SYS_DROPPED_PDUS (pContext);
            ADP_PT ((ISIS_LGST, "ADJ <T> : Invalid Peer System Type"));
            i4RetVal = ISIS_FAILURE;
    }

    /* Freeing the memory Allocated for  PDU
     */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjValAAMismatchHello ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjValP2PThreewayHelloSelfL12MatAA ()
 * Description   : This routine validates Point-to-Point Hello PDU and
 *                 updates the Adjacency DataBase, the local IS being Level2 IS
 * Input(s)      : pCktRec      - Pointer to Circuit Record
 *                 pu1PDU       - Pointer to the received Hello Pdu
 *                 u1CktType    - Peer System Type as reported in the PDU
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - if Adjacency DataBase is updated 
 *                 ISIS_FAILURE - otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjValP2PThreewayHelloSelfL12MatAA (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                                        UINT1 u1CktType)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TmrIdx = 0;
    UINT4               u4DirIdx = 0;
    tBool               bAdjExist = ISIS_TRUE;
    tIsisSysContext    *pContext = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjValP2PHelloSelfL12 ()\n"));

    /* Retrieve the context from the Circuit Record
     */

    pContext = pCktRec->pContext;
    pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
    UNUSED_PARAM (pCktLevel);
    if (pCktRec->pAdjEntry == NULL)
    {
        /* Adjacency does not exist. Build the Adjacency information from the
         * information available in the PDU
         */

        pAdjRec = (tIsisAdjEntry *)
            ISIS_MEM_ALLOC (ISIS_BUF_ADJN, sizeof (tIsisAdjEntry));

        if (pAdjRec != NULL)
        {
            bAdjExist = ISIS_FALSE;

            /* Build the record and mark the adjacency as UNKNOWN. which can be
             * marked later with proper type depending on the Peer System Type
             * and the Circuit Type
             */

            IsisAdjBldAdjRecFromPdu (pCktRec, pu1PDU, ISIS_ADJ_UP, NULL,
                                     ISIS_UNKNOWN, pAdjRec);
        }
        else
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Adjacency\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_ADJN);
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjValP2PHelloSelfL12 ()\n"));
            return ISIS_FAILURE;
        }
    }
    else
    {
        pAdjRec = pCktRec->pAdjEntry;
    }

    /* 'u1CktType' is the Peer Node's System Type. Specifications refers to the
     * System Type as Circuit Type in the Control PDUs and thou shall use the
     * same
     */

    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID, acNbrSysId);
    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    switch (u1CktType)
    {
        case ISIS_LEVEL1:

            /* Local IS is L12 and the peer is L1
             */

            /* For a valid adjacency to exist between two systems, they both
             * must have compatible System Types and also the Circuit over
             * which the adjacency exist need to be compatible. At this
             * point the systems are compatible, check for the circuit
             */

            if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {
                /* Everything is fine, Adjacency can be brought UP immediately
                 */

                if (bAdjExist == ISIS_TRUE)
                {
                    /* Since an adjacency already exist, there must have been a
                     * change in the priority. Just update those parameters that
                     * might have changed
                     */

                    if ((pAdjRec->u1AdjUsage == ISIS_UNKNOWN)
                        || (pAdjRec->u1AdjUsage == ISIS_L1ADJ))
                    {
                        /* The existing adjacency usage is compatible with the
                         * Peer System Type. Update the other parameters that
                         * might have changed
                         *
                         * Refer Table 6. Level 2 state table for matching areas
                         */

                        pAdjRec->u1AdjUsage = ISIS_L1ADJ;
                        if (pAdjRec->u1AdjState != ISIS_ADJ_UP)
                        {
                            pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                            pAdjRec->u1AdjState = ISIS_ADJ_UP;
                        }
                        else
                        {
                            pCktRec->u1TwoWayStateDecision =
                                ISIS_ADJ_STATE_ACCEPT;
                        }
                    }
                    else
                    {
                        /* Refer Table 6. Level 2 state table for 
                         * matching areas
                         */

                        u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;

                        IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                                       ISIS_TRUE, &u4DirIdx);

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Adjacency Deleted - Usage Changed To ISIS_ADJ_LEVEL1\n"));

                        /* Stopping the Hold Timer started for this adjacency
                         */

                        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                            u4DirIdx, u1TmrIdx);

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Attempt To Add A Wrong System\n"));
                    }
                }
                else
                {
                    ADP_PI ((ISIS_LGST,
                             "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                             " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));

                    pAdjRec->u1AdjUsage = ISIS_L1ADJ;
                    IsisAdjAddP2PThreewayAdj (pContext, pCktRec, pAdjRec);
                    IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                    pCktRec->u1CktLevel, ISIS_TRUE);
                    pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                }
                i4RetVal = ISIS_SUCCESS;
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Adjacency Rejected - Incompatible Level -"
                         " Circuit Index [ %u], Peer [ %s ], Self [ %s ]\n",
                         pCktRec->u4CktIdx,
                         ISIS_GET_CKT_LVL_STR (pCktRec->u1CktLevel),
                         "ISIS_LEVEL1"));

                if (bAdjExist == ISIS_FALSE)
                {
                    /* Free the adjacency record that was built
                     */

                    IsisAdjDelAdjAreaAddrs (pAdjRec);
                    ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
                }
                IsisAdjEnqueueIncompCktEvt (pContext, pCktRec);
                i4RetVal = ISIS_FAILURE;
            }
            break;

        case ISIS_LEVEL2:

            /* Local IS is L12 and the peer is L2
             */

            /* For a valid adjacency to exist between two systems, they both
             * must have compatible System Types and also the Circuit over
             * which the adjacency exist need to be compatible. At this
             * point the systems are compatible, check for the circuit
             */

            if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {
                if (bAdjExist == ISIS_TRUE)
                {
                    /* Since an adjacency already exist, there must have been a
                     * change in the priority. Just update those parameters that
                     * might have changed
                     */

                    if ((pAdjRec->u1AdjUsage == ISIS_UNKNOWN)
                        || (pAdjRec->u1AdjUsage == ISIS_L2ADJ))
                    {
                        /* The existing adjacency usage is compatible with the
                         * Peer System Type. Update the other parameters that
                         * might have changed
                         *
                         * Refer Table 6. Level 2 state table for matching areas
                         */

                        pAdjRec->u1AdjUsage = ISIS_L2ADJ;
                        if (pAdjRec->u1AdjState != ISIS_ADJ_UP)
                        {
                            pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                            pAdjRec->u1AdjState = ISIS_ADJ_UP;
                        }
                        else
                        {
                            pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                        }
                    }
                    else
                    {
                        /* Refer Table 6. Level 2 state table for 
                         * matching areas
                         */

                        u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;

                        IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                                       ISIS_TRUE, &u4DirIdx);

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Adjacency Deleted - Usage Changed To ISIS_ADJ_LEVEL2\n"));

                        /* Stopping the Hold Timer started for this adjacency
                         */

                        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                            u4DirIdx, u1TmrIdx);

                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Attempt To Add A Wrong System\n"));
                    }
                }
                else
                {

                    pAdjRec->u1AdjUsage = ISIS_L2ADJ;
                    IsisAdjAddP2PThreewayAdj (pContext, pCktRec, pAdjRec);
                    IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                    pCktRec->u1CktLevel, ISIS_TRUE);
                    pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;

                }
                i4RetVal = ISIS_SUCCESS;
            }
            else
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <T> : Adjacency Rejected - Incompatible Level -"
                         " Circuit Index [ %u ], Peer [ %s ], Self [ %s ]\n",
                         pCktRec->u4CktIdx,
                         ISIS_GET_CKT_LVL_STR (pCktRec->u1CktLevel),
                         "ISIS_LEVEL2"));

                if (bAdjExist == ISIS_FALSE)
                {
                    /* Free the adjacency record that was built
                     */

                    IsisAdjDelAdjAreaAddrs (pAdjRec);
                    ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
                }

                IsisAdjEnqueueIncompCktEvt (pContext, pCktRec);
                i4RetVal = ISIS_FAILURE;
            }
            break;

        case ISIS_LEVEL12:

            if (bAdjExist == ISIS_TRUE)
            {
                if ((pAdjRec->u1AdjUsage == ISIS_UNKNOWN)
                    || (pAdjRec->u1AdjUsage == pCktRec->u1CktLevel))
                {
                    /* The existing adjacency usage is compatible with the
                     * Peer System Type. Update the other parameters that
                     * might have changed. Both peers are L12 and the adjacency 
                     * usage now depends on the Circuit Level. 
                     *
                     * Refer Table 6. Level 2 state table for matching areas
                     */

                    pAdjRec->u1AdjUsage = pCktRec->u1CktLevel;
                    if (pAdjRec->u1AdjState != ISIS_ADJ_UP)
                    {
                        pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                        pAdjRec->u1AdjState = ISIS_ADJ_UP;
                    }
                    else
                    {
                        pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_ACCEPT;
                    }
                    i4RetVal = ISIS_SUCCESS;
                }
                else
                {
                    u1TmrIdx = pCktRec->pAdjEntry->u1TmrIdx;

                    IsisAdjDelAdj (pContext, pCktRec, pCktRec->pAdjEntry,
                                   ISIS_TRUE, &u4DirIdx);

                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Adjacency Deleted - Usage Changed To ISIS_ADJ_LEVEL12\n"));

                    /* Stopping the Hold Timer started for this adjacency
                     */

                    IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                        u4DirIdx, u1TmrIdx);

                    IsisAdjEnqueueWrongSysEvt (pContext, pu1PDU);

                    i4RetVal = ISIS_FAILURE;
                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Attempt To Add A Wrong System\n"));
                }
            }
            else
            {
                ADP_PI ((ISIS_LGST,
                         "ADJ <I> : P2P Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                         " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         ISIS_GET_ISIS_ADJ_STATUS (pAdjRec->u1AdjState),
                         pCktRec->u4CktIdx, pCktRec->u1CktLevel, acNbrSysId,
                         au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));

                pAdjRec->u1AdjUsage = pCktRec->u1CktLevel;

                IsisAdjAddP2PThreewayAdj (pContext, pCktRec, pAdjRec);
                IsisAdjTxHello (pCktRec->pContext, pCktRec,
                                pCktRec->u1CktLevel, ISIS_TRUE);
                pCktRec->u1TwoWayStateDecision = ISIS_ADJ_STATE_UP;
                i4RetVal = ISIS_SUCCESS;
            }
            break;

        default:

            /* Erroneous Peer System Type. Ignore the Hello PSU received
             */

            if (bAdjExist == ISIS_FALSE)
            {
                /* Free the adjacency record that was built
                 */

                IsisAdjDelAdjAreaAddrs (pAdjRec);
                ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
            }

            ADP_PT ((ISIS_LGST, "ADJ <T> : Invalid Peer System Type [ %u ]\n",
                     u1CktType));
            i4RetVal = ISIS_FAILURE;
    }

    /* Freeing the memory Allocated for  PDU
     */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjValP2PHelloSelfL12 ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjModifyLANAdj ()
 * Description   : This routine modifies an already existing adjacency in the 
 *                 database with information retrieved from the received PDU. 
 * Input(s)      : pu1PDU       - Pointer to the received PDU
 *                 pCktRec      - Pointer to Circuit Record
 *                 pAdjRec      - Pointer to the Adjacency Record to be 
 *                                modified
 *                 u1AdjState   - Adjacency State 
 * Output(s)     : u1SchedDIS   - Flag indicating whether DIS Election
 *                                should be scheduled 
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjModifyLANAdj (UINT1 *pu1PDU, tIsisCktEntry * pCktRec,
                     tIsisAdjEntry * pAdjRec, UINT1 u1AdjState,
                     UINT1 *u1SchedDIS, UINT1 *pu1AdjID)
{
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisSysContext    *pContext = pCktRec->pContext;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    UINT4               u4DirIdx = 0;
    UINT4               u4Duration = 0;
    UINT1               au1NullIP[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1Prio = 0;
    tBool               bLSUFlag = ISIS_FALSE;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    CHAR                acAdjId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));
    MEMSET (acAdjId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjModifyLANAdj ()\n"));
    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);

    pCktLevel = ((pAdjRec->u1AdjUsage == ISIS_LEVEL1) ?
                 pCktRec->pL1CktInfo : pCktRec->pL2CktInfo);
    u1Prio = ISIS_EXTRACT_PRIORITY (pu1PDU);
    ISIS_EXTRACT_HOLD_TIME (pu1PDU, pAdjRec->u2HoldTime);

    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID, acNbrSysId);
    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pu1AdjID, acAdjId);

    if (MEMCMP (pAdjRec->au1AdjNbrSysID, pu1AdjID, ISIS_SYS_ID_LEN) != 0)
    {                            /*System Id is changed */
        IsisAdjDelAdj (pContext, pCktRec, pAdjRec, ISIS_FALSE, &u4DirIdx);
        ADP_PI ((ISIS_LGST,
                 "ADJ <I> : Adjacency Deleted - System ID Changed - Previous [ %s ], Current [ %s ]\n",
                 acNbrSysId, acAdjId));
        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING, u4DirIdx,
                            pAdjRec->u1TmrIdx);
        return;
    }

    if (pAdjRec->u1AdjState != u1AdjState)
    {
        /* Adjacency state has changed, update it
         */

#ifdef BFD_WANTED
        /* If BFD enabled for multi-topology ISIS neighbor, 
         * AdjState can be moved to ISIS_ADJ_UP only if 
         * ISIS_BFD_NEIGHBOR_USEABLE is TRUE */
        if ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
            (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE) &&
            (ISIS_BFD_REQUIRED (pAdjRec) == ISIS_BFD_TRUE) &&
            (u1AdjState == ISIS_ADJ_UP))
        {
            if (ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) == ISIS_BFD_FALSE)
            {
                u1AdjState = ISIS_ADJ_INIT;
            }
            else if ((ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) == ISIS_BFD_TRUE) &&
                     (pAdjRec->u1IsisMT0BfdState != ISIS_BFD_SESS_UP) &&
                     (pAdjRec->u1IsisMT2BfdState != ISIS_BFD_SESS_UP))
            {
                u1AdjState = ISIS_ADJ_INIT;
            }
        }
#endif

        if ((MEMCMP (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
                     ISIS_MAX_IPV4_ADDR_LEN) != 0) &&
            (IsisUtlIsDirectlyConnected (pCktRec,
                                         pAdjRec->AdjNbrIpV4Addr.au1IpAddr) ==
             ISIS_FALSE))
        {
            pAdjRec->u1AdjIpAddrType &= ~(ISIS_ADDR_IPV4);
            if (pAdjRec->u1AdjIpAddrType == 0)
            {
                u1AdjState = ISIS_ADJ_INIT;
            }
        }

        pAdjRec->u1AdjState = u1AdjState;

        if (u1AdjState == ISIS_ADJ_UP)
        {
            /* Adjacency has changed state to UP, post Adjacency UP event to 
             * Control Module
             */

            ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);

            ADP_PI ((ISIS_LGST,
                     "ADJ <I> : Adjacency - State [ %s ], Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                     " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                     ISIS_GET_ISIS_ADJ_STATUS (u1AdjState), pCktRec->u4CktIdx,
                     pAdjRec->u1AdjUsage, acNbrSysId, au1IPv4Addr,
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pCktRec->au1IPV6Addr))));

            ISIS_INCR_ADJ_CHG_STAT (pCktRec);
            IsisAdjEnqueueAdjChgEvt (pCktRec, pCktLevel, pAdjRec, ISIS_ADJ_UP);
            pCktLevel->u4NumAdjs++;

            if (pCktLevel->u4NumAdjs == 1)
            {
                if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
                {
                    IsisUtlSetBPat (&pContext->CktTable.pu1L1CktMask,
                                    pCktRec->u4CktIdx);

                    /* First adjacency on the given circuit has come up. 
                     * We can now start the LSP transmission timer 
                     */

                    IsisTmrStartECTimer (pContext, ISIS_ECT_L1_LSP_TX,
                                         pCktRec->u4CktIdx, 1,
                                         &pCktRec->pL1CktInfo->u1LSPTxTmrIdx);
                }

                if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
                {
                    IsisUtlSetBPat (&pContext->CktTable.pu1L2CktMask,
                                    pCktRec->u4CktIdx);

                    /* First adjacency on the given circuit has come up. 
                     * We can now start the LSP transmission timer 
                     */

                    IsisTmrStartECTimer (pContext, ISIS_ECT_L2_LSP_TX,
                                         pCktRec->u4CktIdx, 1,
                                         &pCktRec->pL2CktInfo->u1LSPTxTmrIdx);
                }
            }

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
            {
                (pContext->CktTable.u4NumL1Adjs)++;
            }
            if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
            {
                (pContext->CktTable.u4NumL2Adjs)++;
            }
#ifdef BFD_WANTED
            /* If BFD is enabled over single topology ISIS, BFD registration
             * is done once the ISIS adjacency is transitioned to UP */
            if ((pContext->u1IsisMTSupport == ISIS_FALSE) &&
                (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
            {
                ISIS_BFD_REGISTER (pContext, pAdjRec, pCktRec);
            }
#endif
            *u1SchedDIS = ISIS_TRUE;

        }

        /* Schedule a LSU to the standby node
         */

        bLSUFlag = ISIS_TRUE;
    }

    if (pAdjRec->u1AdjNbrPriority != u1Prio)
    {
        /* Priority got modified. Schedule DIS Election and also LSU to the
         * standby node
         */

        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Adjacency Priority Changed - Circuit Index [ %u ], Priority [ %u ]\n",
                 pCktRec->u4CktIdx, u1Prio));
        pAdjRec->u1AdjNbrPriority = u1Prio;
        *u1SchedDIS = ISIS_TRUE;
        bLSUFlag = ISIS_TRUE;
    }

    /* Sending LockStep Update if any one of the modifiable parameters 
     * (such as the Adjacency State and Adjacency Priority) has changed and FT
     * support is enabled
     */

    /* Now update the adjacency area addresses based on the information included
     * in the received PDU
     */

    IsisAdjModifyAdjAreaAddr (pu1PDU, pCktRec, pAdjRec);
    if (IsisAdjUpdateIpAddr (pu1PDU, pAdjRec, &bLSUFlag) != ISIS_SUCCESS)
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Updating IPv4/IPv6 Addr TLV Failed - Circuit [%u]\n",
                 pCktRec->u4CktIdx));
        return;
    }

    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        IsisAdjUpdateMTId (pu1PDU, pAdjRec, &bLSUFlag);
    }

#ifdef ISIS_FT_ENABLED
    if (bLSUFlag == ISIS_TRUE)
    {
        ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_ADD, pAdjRec);
    }
#endif

#ifdef BFD_WANTED
    /* If u1DoNotUpdHoldTmr is set to TRUE, it indicates that the
     * the ISIS Adjacency was already UP and now BFD monitoring
     * has been enabled for this neighbor. In this case, do not update
     * the hold timer till ISIS_NEIGHBOR_USEABLE becomes TRUE.
     * Refer RFC 6213 - Section 4 */
    if ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
        (pAdjRec->u1DoNotUpdHoldTmr == ISIS_BFD_TRUE))
    {
        /* Check the ISIS_BFD_NEIGHBOR_USEABLE */
        if (ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) != ISIS_BFD_FALSE)
        {
            pAdjRec->u1DoNotUpdHoldTmr = ISIS_BFD_FALSE;
        }
    }
#endif

    /* Get the Direction Entry, which is required for restarting the Holding
     * Timer
     */
    if (
#ifdef BFD_WANTED
           (((pContext->u1IsisMTSupport == ISIS_TRUE) &&
             (pAdjRec->u1DoNotUpdHoldTmr == ISIS_BFD_FALSE)) ||
            (pContext->u1IsisMTSupport == ISIS_FALSE)) &&
#endif
           (pAdjRec->u1IsisGRHelperStatus != ISIS_GR_HELPING))
    {
        pDirEntry = pContext->AdjDirTable.pDirEntry;

        while (pDirEntry != NULL)
        {
            if (pDirEntry->pAdjEntry == pAdjRec)
            {
                /* Restarting the timer for Holding time. Timer Index stored
                 * in Adjacency Record is passed to the timer Module, so that
                 * its easier to locate the record in the Equivalence Class of Timer
                 * and the block where the record is held
                 */
                u4Duration = (UINT4) pAdjRec->u2HoldTime;
                IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                                       pDirEntry->u4DirIdx,
                                       u4Duration, &(pAdjRec->u1TmrIdx));
                break;
            }
            pDirEntry = pDirEntry->pNext;
        }
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjModifyLANAdj ()\n"));
}

/*******************************************************************************
 * Function Name : IsisAdjCheckLANAdjExist ()
 * Description   : This routine checks whether a particular Adjacency
 *                 exists on a given broadcast circuit and if exists
 *                 the Adjacency Record will be the output 
 * Input(s)      : pCktRec      - Pointer to Circuit Record
 *                 pu1SNPA      - Pointer to SNPA Address
 *                 u1AdjUsage   - Level of Adjacency (Level1 or Level2)
 * Output(s)     : pAdjRec      - Pointer to the existing Adjacency 
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if Adjacency exists
 *                 ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjCheckLANAdjExist (tIsisCktEntry * pCktRec, UINT1 *pu1SNPA,
                         UINT1 u1AdjUsage, tIsisAdjEntry ** pAdjRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisAdjEntry      *pAdjEntry = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjCheckLANAdjExist ()\n"));

    pAdjEntry = pCktRec->pAdjEntry;

    while (pAdjEntry != NULL)
    {
        /* Checking for an existing adjacency at the given level
         */

        if (pAdjEntry->u1AdjUsage == u1AdjUsage)
        {
            if (MEMCMP (pAdjEntry->au1AdjNbrSNPA, pu1SNPA,
                        ISIS_SNPA_ADDR_LEN) == 0)
            {
                *pAdjRec = pAdjEntry;
                i4RetVal = ISIS_SUCCESS;
                break;
            }
        }
        pAdjEntry = pAdjEntry->pNext;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjCheckLANAdjExist ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjSelfLANAddrListed ()
 * Description   : This routine checks whether the Local ISs SNPA Address
 *                 is included in the received PDUs Neighbour Address TLV
 * Input(s)      : pCktRec    - Pointer to Circuit Record
 *                 pu1PDU     - Pointer to the received PDU
 * Output(s)     : None 
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_TRUE  - if SNPA Address is listed
 *                 ISIS_FALSE - otherwise
 ******************************************************************************/

PUBLIC              tBool
IsisAdjSelfLANAddrListed (tIsisCktEntry * pCktRec, UINT1 *pu1PDU)
{
    UINT1               u1TlvLen = 0;
    UINT2               u2Offset = 0;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjSelfLANAddrListed ()\n"));

    /* Get to the Variable Length Fields offset */

    u2Offset = sizeof (tIsisComHdr) + 7 + (2 * ISIS_SYS_ID_LEN);

    while (IsisUtlGetNextTlvOffset (pu1PDU, ISIS_IS_NEIGH_ADDR_TLV, &u2Offset,
                                    &u1TlvLen) != ISIS_FAILURE)
    {
        while (u1TlvLen > 0)
        {
            if (MEMCMP (pCktRec->au1SNPA, (UINT1 *) (pu1PDU + u2Offset),
                        ISIS_SNPA_ADDR_LEN) == 0)
            {
                ADP_EE ((ISIS_LGST,
                         "ADJ <X> : Exiting IsisAdjSelfLANAddrListed ()\n"));
                return (ISIS_TRUE);
            }

            u1TlvLen -= ISIS_SNPA_ADDR_LEN;
            u2Offset += ISIS_SNPA_ADDR_LEN;
        }
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjSelfLANAddrListed ()\n"));
    return (ISIS_FALSE);
}

/******************************************************************************
 * Function Name : IsisAdjPruneAdjs ()
 * Description   : This function scans the Adjacency database and fetches 
 *                 a set of adjacencies with least priority. If the set has more
 *                 than one element, then this routine fetches one adjacency
 *                 from the set which has got the least SNP address (there
 *                 cannot be more than one element with the same SNPA)
 * Input(s)      : pCktRec      - Pointer to Circuit Record
 * Outputs(s)    : pAdjRec      - Pointer to the Adjacency that can be
 *                                pruned
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjPruneAdjs (tIsisCktEntry * pCktRec, tIsisAdjEntry ** pAdjRec)
{
    tIsisAdjEntry      *pAdjTrav = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjPruneAdjs ()\n"));

    /* Assume the very first entry in the database is the adjacency to be pruned
     */

    *pAdjRec = pCktRec->pAdjEntry;
    pAdjTrav = pCktRec->pAdjEntry->pNext;

    while (pAdjTrav != NULL)
    {
        if ((*pAdjRec)->u1AdjNbrPriority > pAdjTrav->u1AdjNbrPriority)
        {
            /* pAdjTrav points to an entry with least priority. Make that the
             * adjacency to be pruned
             */

            *pAdjRec = pAdjTrav;
        }

        else if ((*pAdjRec)->u1AdjNbrPriority == pAdjTrav->u1AdjNbrPriority)
        {
            /* Priorities are same, check for the SNP Address
             */

            if ((MEMCMP ((*pAdjRec)->au1AdjNbrSNPA, pAdjTrav->au1AdjNbrSNPA,
                         ISIS_SNPA_ADDR_LEN)) > 0)
            {
                /* pAdjTrav points to an entry with least SNPA. Make that the
                 * adjacency to be pruned
                 */

                *pAdjRec = pAdjTrav;
            }
        }
        pAdjTrav = pAdjTrav->pNext;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjPruneAdjs ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjDelAdjAreaAddrs ()
 * Description   : This function Deletes all the adjacent area address entries
 *                 from the Adjacent Record
 * Input(s)      : pAdjRec - Pointer to the Adjacency whose adjacent area 
 *                 area addresses are to be deleted
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjDelAdjAreaAddrs (tIsisAdjEntry * pAdjRec)
{
    tIsisAdjAAEntry    *pAdjAA = NULL;
    tIsisAdjAAEntry    *pNextAdjAA = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDelAdjAreaAddrs ()\n"));

    pAdjAA = pAdjRec->pAdjAreaAddr;
    while (pAdjAA != NULL)
    {
        pNextAdjAA = pAdjAA->pNext;
        ISIS_MEM_FREE (ISIS_BUF_ADAA, (UINT1 *) pAdjAA);
        pAdjAA = pNextAdjAA;
    }
    pAdjRec->pAdjAreaAddr = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDelAdjAreaAddrs ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjProcRestartTlv ()
 * Description   : This function Process the Restart TLV and 
 *                 take necessary action
 * 
 * Input(s)      : pContext  -Context
 *                 pCktRec   -Circuit Record
 *                 pu1SNPA   -SNPA Address
 *                 pu1PDU    -Hello Pdu 
 *                 
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS / ISIS_FAILURE
 ******************************************************************************/

PUBLIC INT4
IsisAdjProcRestartTlv (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                       UINT1 *pu1SNPA, UINT1 *pu1PDU)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TlvLen = 0;
    UINT1               u1Flag = 0;
    UINT1               au1SysID[ISIS_SYS_ID_LEN];
    UINT1              *pu1AdjID = NULL;
    UINT1               u1PduType = 0;
    UINT1               u1Level = 0;
    UINT2               u2Offset = 0;
    UINT2               u2RemainingTime = 0;
    tIsisAdjEntry      *pAdjRec = NULL;

    pu1AdjID = ISIS_EXTRACT_HELLO_SYS_ID (pu1PDU);
    u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1PDU);
    u1Level = ISIS_GET_LEVEL (u1PduType);

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjRestartAckCheck ()\n"));
    if (ISIS_EXTRACT_PDU_TYPE (pu1PDU) == ISIS_P2P_IIH_PDU)
    {
        u2Offset = sizeof (tIsisComHdr) + 6 + ISIS_SYS_ID_LEN;
    }
    else
    {
        u2Offset = sizeof (tIsisComHdr) + 7 + (2 * ISIS_SYS_ID_LEN);
    }
    i4RetVal = IsisUtlGetNextTlvOffset (pu1PDU, ISIS_RESTART_TLV, &u2Offset,
                                        &u1TlvLen);
    if (i4RetVal == ISIS_SUCCESS)
    {
        u1Flag = *(UINT1 *) (pu1PDU + u2Offset);    /*Flag value */
        u2Offset = (UINT2) (u2Offset + FLAG);
        u1TlvLen = (UINT1) (u1TlvLen - FLAG);
        if (u1TlvLen >= REM_TIME)
        {
            ISIS_GET_2_BYTES (pu1PDU, u2Offset, u2RemainingTime);
            u2Offset = (UINT2) (u2Offset + REM_TIME);
            u1TlvLen = (UINT1) (u1TlvLen - REM_TIME);
        }
        if (u1TlvLen >= ISIS_SYS_ID_LEN)
        {
            MEMCPY (au1SysID, (UINT1 *) (pu1PDU + u2Offset), ISIS_SYS_ID_LEN);    /*System ID */
            u2Offset = (UINT2) (u2Offset + ISIS_SYS_ID_LEN);
            u1TlvLen = (UINT1) (u1TlvLen - ISIS_SYS_ID_LEN);
        }

        if ((pCktRec->u1CktType == ISIS_P2P_CKT) && (pu1SNPA == NULL))    /*PtoP */
        {
            if (pCktRec->pAdjEntry != NULL)
            {
                pAdjRec = pCktRec->pAdjEntry;
            }
            else
            {
                return ISIS_FAILURE;
            }
        }
        else
        {
            if (pu1SNPA != NULL)
            {
                if ((IsisAdjCheckLANAdjExist (pCktRec, pu1SNPA,
                                              u1Level,
                                              &pAdjRec)) == ISIS_FAILURE)
                {
                    return ISIS_FAILURE;
                }
            }
        }

        if (pAdjRec == NULL)
        {
            return ISIS_FAILURE;
        }
        else
        {
            pAdjRec->u1IsGRCapEnabled = GR_ENABLED;
        }
        /* Updating the hold time value, since this might be required
         * while processing the RR */

        ISIS_EXTRACT_HOLD_TIME (pu1PDU, pAdjRec->u2HoldTime);

        switch (u1Flag)
        {
            case RR_SA_RA_CLEAR:
                IsisAdjProcRestartClear (pContext, pAdjRec);

                break;
            case RR_SET:
                IsisAdjProcRRRcvd (pContext, pCktRec, u1Level,
                                   u2RemainingTime, pAdjRec, pu1AdjID);
                break;

            case RA_SET:
                IsisAdjProcRARcvd (pContext, pCktRec, au1SysID, u1Level,
                                   u2RemainingTime);
                break;
            case SA_SET:
                if (IsIsisGrHelping (pContext, u1Level) == ISIS_SUCCESS)
                {
                    if (pAdjRec->u1IsisAdjGRState != ISIS_ADJ_SUPPRESSED)
                    {
                        pAdjRec->u1IsisAdjGRState = ISIS_ADJ_SUPPRESSED;
                        IsisAdjProcAdjSuppress (pContext, pAdjRec, pCktRec,
                                                u1Level, ISIS_CMD_DELETE);
                    }
                }
                break;
            case RR_SA_SET:
                /*Setting the Adjacent GR state as ISIS_ADJ_SUPPRESSED */
                if (IsIsisGrHelping (pContext, u1Level) == ISIS_SUCCESS)
                {
                    if (pAdjRec->u1IsisAdjGRState != ISIS_ADJ_SUPPRESSED)
                    {
                        pAdjRec->u1IsisAdjGRState = ISIS_ADJ_SUPPRESSED;
                        IsisAdjProcAdjSuppress (pContext, pAdjRec, pCktRec,
                                                u1Level, ISIS_CMD_DELETE);
                    }
                }
                IsisAdjProcRRRcvd (pContext, pCktRec, u1Level,
                                   u2RemainingTime, pAdjRec, pu1AdjID);
                break;
            default:
                break;

        }

    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjRestartAckCheck ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjProcRRRcvd ()
 * Description   : This function will make the router as helping router 
 *                 depending on the helper support configured. 
 * 
 * Input(s)      : pContext  -Context
 *                 pCktRec   -Circuit Record
 *                 u1Level   -Level
 *                 u2RemainingTime  -GR Time
 *                 pAdjRec    -Adjacent Record     
 *                 
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcRRRcvd (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                   UINT1 u1Level, UINT2 u2RemainingTime,
                   tIsisAdjEntry * pAdjRec, UINT1 *pu1SysID)
{
    tIsisTmrCSNPHdr    *pCSNPInfo = NULL;
    tIsisEvtHelper     *pEvtHelper = NULL;
    UINT4               u4DirIdx = 0;
    UINT1               u1PrevHelpState;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    UINT1               u1Loc = 0;
    UINT1               u1Flag = 0;
    tIsisLSPEntry      *pLSPDBMark = NULL;
    tIsisLSPTxEntry    *pLSPTxQMark = NULL;
    tIsisCktLevel      *pCktLvlRec = NULL;
    UINT1               u1IsHighPriority = ISIS_FALSE;
    UINT1               u1ECTId = 0;
    UINT1               u1CurrLvl = 0;
    UINT1              *pu1CSNP = NULL;
    UINT2               u2Len = 0;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4StartTime = 0;
    UINT4               u4CurrentTime = 0;
    tUtlTm              utlTm;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcRRRcvd ()\n"));

    if (IsIsisGrHelping (pContext, u1Level) == ISIS_SUCCESS)
    {
        u1PrevHelpState = pAdjRec->u1IsisGRHelperStatus;
        pContext->u1IsisGRRestartMode = ISIS_GR_HELPER;
        /* When the router enters the ISIS_GR_HELPING mode for the first time,
         * calculate the GR time based on if GraceTimeLimit is set or not.
         */
        if (pContext->u2IsisGRHelperTimeLimit != 0)
        {
            if (u2RemainingTime == 0)
            {
                if (pAdjRec->u4StartTime > 0)
                {
                    /*sync systime to standby */
                    MEMSET (&utlTm, 0, sizeof (tUtlTm));
                    UtlGetTime (&utlTm);
                    /* Get the seconds since the base year (2000) */
                    u4CurrentTime = IsisGrGetSecondsSinceBase (utlTm);
                    pAdjRec->u2AdjGRTime = pAdjRec->u2HoldTime -
                        (UINT2) (u4CurrentTime - pAdjRec->u4StartTime);
                }
                else
                {
                    pAdjRec->u2AdjGRTime = pAdjRec->u2HoldTime;
                }
            }
            else if (u2RemainingTime > pContext->u2IsisGRHelperTimeLimit)
            {
                pAdjRec->u2AdjGRTime = pContext->u2IsisGRHelperTimeLimit;
            }
            else
            {
                pAdjRec->u2AdjGRTime = u2RemainingTime;
            }
        }
        else
        {
            if (u2RemainingTime != 0)
            {
                pAdjRec->u2AdjGRTime = u2RemainingTime;
            }
            else
            {
                if (pAdjRec->u4StartTime > 0)
                {
                    /*sync systime to standby */
                    MEMSET (&utlTm, 0, sizeof (tUtlTm));
                    UtlGetTime (&utlTm);
                    /* Get the seconds since the base year (2000) */
                    u4CurrentTime = IsisGrGetSecondsSinceBase (utlTm);
                    pAdjRec->u2AdjGRTime = pAdjRec->u2HoldTime -
                        (UINT2) (u4CurrentTime - pAdjRec->u4StartTime);
                }
                else
                {
                    pAdjRec->u2AdjGRTime = pAdjRec->u2HoldTime;
                }
            }
        }

        /*Send Ack */
        pAdjRec->u1IsisGRHelperStatus = ISIS_GR_HELPING;
        pAdjRec->u1HelperAckState = ISIS_HELPER_RA_SEND;

        if (u1PrevHelpState != ISIS_GR_HELPING)
        {
            /*Update Holding Timer */
            u4DirIdx = IsisAdjGetDirEntryWithAdjIdx (pContext, pAdjRec);
            IsisTmrRestartECTimer (pCktRec->pContext, ISIS_ECT_HOLDING,
                                   u4DirIdx, pAdjRec->u2AdjGRTime,
                                   &(pAdjRec->u1TmrIdx));
            /*sync systime to standby */
            MEMSET (&utlTm, 0, sizeof (tUtlTm));
            UtlGetTime (&utlTm);
            /* Get the seconds since the base year (2000) */
            u4StartTime = IsisGrGetSecondsSinceBase (utlTm);

            pAdjRec->u4StartTime = u4StartTime;
            ISIS_FLTR_ADJ_LSU (pContext, ISIS_CMD_DELETE, pAdjRec);
            ISIS_FLTR_ADJ_LSU (pContext, ISIS_CMD_ADD, pAdjRec);
            /*T3timer iniated (as Holding tmr) */

        }
        IsisAdjTxHello (pContext, pCktRec, u1Level, ISIS_FALSE);
        pEvtHelper = (tIsisEvtHelper *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtHelper));
        if (pEvtHelper != NULL)
        {
            pEvtHelper->u1EvtID = ISIS_EVT_HELPER;
            MEMCPY (pEvtHelper->au1SysID, pContext->SysActuals.au1SysID,
                    ISIS_SYS_ID_LEN);
            MEMCPY (pEvtHelper->au1AdjNbrSysID, pAdjRec->au1AdjNbrSysID,
                    ISIS_SYS_ID_LEN);
            pEvtHelper->u1IsisGRHelperStatus = pAdjRec->u1IsisGRHelperStatus;
            pEvtHelper->u2AdjGRTime = pAdjRec->u2AdjGRTime;
            pEvtHelper->u1IsisGRHelperExitReason = 0;
            IsisSnmpIfSendTrap ((UINT1 *) pEvtHelper, pEvtHelper->u1EvtID);
        }
        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            u1Level = pAdjRec->u1AdjUsage;
            if (u1Level == ISIS_LEVEL12)
            {
                u1CurrLvl = ISIS_LEVEL1;
            }
            else
            {
                u1CurrLvl = u1Level;
            }
            while (u1CurrLvl)
            {
                pCSNPInfo = (tIsisTmrCSNPHdr *) ISIS_MEM_ALLOC (ISIS_BUF_ECTI,
                                                                sizeof
                                                                (tIsisTmrCSNPHdr));
                if (pCSNPInfo == NULL)
                {
                    return;
                }
                MEMSET (pCSNPInfo, 0x00, ISIS_LSPID_LEN);
                pCSNPInfo->u4CktIdx = pCktRec->u4CktIdx;

                pLSPTxQMark =
                    IsisUpdGetLSPFromTxQ (pContext, pCSNPInfo->au1LastTxId,
                                          u1CurrLvl, &pPrevTxRec, &pHashBkt,
                                          &u1Loc, &u1Flag);
                pLSPDBMark =
                    IsisUpdGetLSPFromDB (pContext, pCSNPInfo->au1LastTxId,
                                         u1CurrLvl, &pPrevDbRec, &pHashBkt,
                                         &u1Loc, &u1Flag);

                ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pCSNPInfo);
                pCSNPInfo = NULL;

                while ((pLSPTxQMark != NULL) || (pLSPDBMark != NULL))
                {
                    i4RetVal = IsisUpdConstructCSNP (pContext, &pLSPTxQMark,
                                                     &pLSPDBMark, &pu1CSNP,
                                                     u1CurrLvl);

                    if (i4RetVal == ISIS_SUCCESS)
                    {
                        ISIS_GET_2_BYTES (pu1CSNP, ISIS_OFFSET_PDULEN, u2Len);
                        IsisTrfrTxData (pCktRec, pu1CSNP, u2Len, u1CurrLvl,
                                        ISIS_FREE_BUF, ISIS_FALSE);
                    }

                    if ((pCktRec->u1OperState == ISIS_UP) &&
                        (pContext->u1IsisGRRestartMode == ISIS_GR_HELPER))
                    {
                        IsisGrHelperTxLSP (pContext, pCktRec, pu1CSNP,
                                           u1CurrLvl);
                        pAdjRec->u1HelperAckState = ISIS_HELPER_NONE;
                    }
                }
                if ((u1Level == ISIS_LEVEL12) && (u1CurrLvl == ISIS_LEVEL1))
                {
                    u1CurrLvl = ISIS_LEVEL2;
                }
                else
                {
                    u1CurrLvl = ISIS_ZERO;
                }
            }
            if (pEvtHelper != NULL)
            {
                ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtHelper);
            }
            return;
        }
        /*Continuing for Broadcast LAN */
        if (u1Level == ISIS_LEVEL1)
        {
            pCktLvlRec = pCktRec->pL1CktInfo;
        }
        else if (u1Level == ISIS_LEVEL2)
        {
            pCktLvlRec = pCktRec->pL2CktInfo;
        }
        else
        {
            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtHelper);
            return;
        }
        IsisAdjCalcHighPriority (pCktRec, u1Level, &u1IsHighPriority, pu1SysID);
        pCSNPInfo = (tIsisTmrCSNPHdr *) ISIS_MEM_ALLOC (ISIS_BUF_ECTI,
                                                        sizeof
                                                        (tIsisTmrCSNPHdr));
        if (pCSNPInfo == NULL)
        {
            return;
        }
        if ((u1IsHighPriority == ISIS_TRUE)
            || (pCktLvlRec->bIsDIS == ISIS_TRUE))
        {
            /*Send CSNP & LSP */
            MEMSET (pCSNPInfo->au1LastTxId, 0x00, ISIS_LSPID_LEN);
            pCSNPInfo->u4CktIdx = pCktRec->u4CktIdx;

            pLSPTxQMark =
                IsisUpdGetLSPFromTxQ (pContext, pCSNPInfo->au1LastTxId, u1Level,
                                      &pPrevTxRec, &pHashBkt, &u1Loc, &u1Flag);
            pLSPDBMark =
                IsisUpdGetLSPFromDB (pContext, pCSNPInfo->au1LastTxId, u1Level,
                                     &pPrevDbRec, &pHashBkt, &u1Loc, &u1Flag);

            ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pCSNPInfo);
            pCSNPInfo = NULL;

            while ((pLSPTxQMark != NULL) || (pLSPDBMark != NULL))
            {
                i4RetVal = IsisUpdConstructCSNP (pContext, &pLSPTxQMark,
                                                 &pLSPDBMark, &pu1CSNP,
                                                 u1Level);

                if (i4RetVal == ISIS_SUCCESS)
                {
                    ISIS_GET_2_BYTES (pu1CSNP, ISIS_OFFSET_PDULEN, u2Len);
                    IsisTrfrTxData (pCktRec, pu1CSNP, u2Len, u1Level,
                                    ISIS_FREE_BUF, ISIS_FALSE);
                }

                if ((pCktRec->u1OperState == ISIS_UP) &&
                    (pContext->u1IsisGRRestartMode == ISIS_GR_HELPER))
                {
                    IsisGrHelperTxLSP (pContext, pCktRec, pu1CSNP, u1Level);
                    pAdjRec->u1HelperAckState = ISIS_HELPER_NONE;
                }
            }

        }
        /*As the Local router has next higher priority than DIS
         * Designate local router to send CSNP in that circuit*/
        if ((u1IsHighPriority == ISIS_TRUE)
            && (pCktLvlRec->bIsDIS != ISIS_TRUE))
        {
            if (u1Level == ISIS_LEVEL1)
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Stopping L1 PSNP Timer - Circuit Index [ %u ]\n",
                         pCktRec->u4CktIdx));
                IsisTmrStopECTimer (pContext, ISIS_ECT_L1_PSNP,
                                    pCktRec->u4CktIdx, pCktLvlRec->u1ECTId);
            }
            else if (u1Level == ISIS_LEVEL2)
            {
                TMP_PT ((ISIS_LGST,
                         "TMR <T> : Stopping L2 PSNP Timer - Circuit Index [ %u ]\n",
                         pCktRec->u4CktIdx));
                IsisTmrStopECTimer (pContext, ISIS_ECT_L2_PSNP,
                                    pCktRec->u4CktIdx, pCktLvlRec->u1ECTId);
            }

            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : Deleting PSNPs - Circuit Index [ %u ]\n",
                     pCktRec->u4CktIdx));

            /* Freeing the PSNPs */

            IsisAdjDelCktLvlPSNPs (pCktLvlRec);

            TMP_PT ((ISIS_LGST,
                     "TMR <T> : Starting CSNP Timer - Circuit Index [ %u ]\n",
                     pCktRec->u4CktIdx));

            /* Start the CSNP timer since the local system has been desginated 
             * to send CSNP for the circuit
             */
            gu1IsisGrCsnp = ISIS_TRUE;

            u1ECTId = IsisUpdStartCSNPTimer (pContext, pCktRec,
                                             (tIsisLSPTxEntry *)
                                             NULL,
                                             (tIsisLSPEntry *) NULL,
                                             pCktLvlRec->CSNPInterval, u1Level);
            ISIS_ADJ_SET_ECTID (pCktLvlRec, u1ECTId);
        }
        if (pCSNPInfo != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pCSNPInfo);
        }

        if (pEvtHelper != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtHelper);
        }
    }

/*Check and free the memory */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcRRRcvd ()\n"));

}

 /******************************************************************************
 * Function Name : IsisAdjProcAdjSuppress()
 * Description   : This function will suppress the IS adjacency 
 *                 of the restarting peer.
 * 
 * Input(s)      : pAdjRec  -Adjacent Record 
 *                 
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcAdjSuppress (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec,
                        tIsisCktEntry * pCktRec, UINT1 u1Level, UINT1 u1Cmd)
{
    tIsisCktLevel      *pCktInfo = NULL;
    tIsisMDT           *pMDT = NULL;
    tIsisEvtAdjChange  *pAdjEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcAdjSuppress ()\n"));

    if (pCktRec->u1CktType == ISIS_P2P_CKT)
    {
        pAdjEvt = (tIsisEvtAdjChange *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                        sizeof
                                                        (tIsisEvtAdjChange));
        if (pAdjEvt != NULL)
        {
            pAdjEvt->u1EvtID = ISIS_EVT_ADJ_CHANGE;
            if (u1Cmd == ISIS_CMD_ADD)
            {
                pAdjEvt->u1Status = ISIS_ADJ_UP;
            }
            else
            {
                pAdjEvt->u1Status = ISIS_ADJ_DOWN;
            }
            pAdjEvt->u4AdjIdx = pAdjRec->u4AdjIdx;
            pAdjEvt->u4CktIdx = pCktRec->u4CktIdx;
            pAdjEvt->u1AdjUsage = pAdjRec->u1AdjUsage;
            pAdjEvt->u1AdjType = pAdjRec->u1AdjNbrSysType;

            MEMCPY (pAdjEvt->au1AdjSysID, pAdjRec->au1AdjNbrSysID,
                    ISIS_SYS_ID_LEN);
            pAdjEvt->au1AdjSysID[ISIS_SYS_ID_LEN] = ISIS_ZERO;

            IsisUtlSendEvent (pCktRec->pContext, (UINT1 *) pAdjEvt,
                              sizeof (tIsisEvtAdjChange));
        }
        else
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL
                    " : Memory Allocation For Adjacency Event Failed\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
        return;
    }

    if (u1Level == ISIS_LEVEL1)
    {
        pCktInfo = pCktRec->pL1CktInfo;
    }
    else
    {
        pCktInfo = pCktRec->pL2CktInfo;
    }

    if (MEMCMP (pAdjRec->au1AdjNbrSysID,
                pCktInfo->au1CktLanDISID, ISIS_SYS_ID_LEN) == 0)
    {
        /*Adjacency suppression received from DIS.
           Remove the IS Adjacency from Non pseudoLSP */
        pMDT =
            (tIsisMDT *) (VOID *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                  sizeof (tIsisMDT));

        if (pMDT == NULL)
        {
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            return;
        }

        pMDT->u1TLVType = ISIS_IS_ADJ_TLV;
        pMDT->u1Cmd = u1Cmd;

        if (u1Level == ISIS_LEVEL1)
        {
            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            ISIS_SET_METRIC (pContext, pMDT, pCktRec->pL1CktInfo);
        }
        else
        {
            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            ISIS_SET_METRIC (pContext, pMDT, pCktRec->pL2CktInfo);
        }

        /* We have to update our NonPseudonode LSPs by deleting the adjacency
         * information maintained for the Previous DIS and then update the
         * same LSPs with the adjacency information for the Current DIS
         */

        MEMCPY (pMDT->au1AdjSysID, pCktInfo->au1CktLanDISID,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

        if (pContext->u1IsisMTSupport == ISIS_TRUE)
        {
            pMDT->u1TLVType = ISIS_EXT_IS_REACH_TLV;
            IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);

            pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
            pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
            IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
        }
        else
        {
            IsisUpdUpdateSelfLSP (pContext, pCktRec->u4CktIdx, pMDT);
        }
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcAdjSuppress ()\n"));
    return;
}

/******************************************************************************
 * Function Name : IsisAdjProcRestartClear()
 * Description   : This function will make the helper to exit from 
 *                 the Graceful restart
 * 
 * Input(s)      : pAdjRec  -Adjacent Record 
 *                 
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcRestartClear (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec)
{

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcRestartClear ()\n"));

    if (pAdjRec->u1IsisAdjGRState == ISIS_ADJ_SUPPRESSED)
    {
        pAdjRec->u1IsisAdjGRState = ISIS_ADJ_RUNNING;
        IsisAdjProcAdjSuppress (pContext, pAdjRec, pAdjRec->pCktRec,
                                pAdjRec->u1AdjUsage, ISIS_CMD_ADD);
    }
    if (pAdjRec->u1IsisGRHelperStatus == ISIS_GR_HELPING)
    {
        IsisGrExitHelper (pContext, pAdjRec, ISIS_GR_HELPER_COMPLETED);
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcRestartClear ()\n"));

}

/******************************************************************************
 * Function Name : IsisAdjProcRARcvd ()
 * Description   : This function process the RA Flag and do necessary action
 *
 * Input(s)      : pContext --Context
 *                 pCktRec  --Circuit Record
 *                 pu1SysID --System ID
 *
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/

PRIVATE VOID
IsisAdjProcRARcvd (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                   UINT1 *pu1SysID, UINT1 u1Level, UINT2 u2RemainingTime)
{

    tGrSemInfo          GrSemInfo;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjProcRARcvd ()\n"));

    /*If the received restart ack is for this router, then process else skip */
    if (((MEMCMP (pu1SysID, pContext->SysActuals.au1SysID,
                  ISIS_SYS_ID_LEN)) == 0))
    {
        MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
        GrSemInfo.pContext = pContext;
        GrSemInfo.pCktRec = pCktRec;
        GrSemInfo.u1Level = u1Level;
        GrSemInfo.u2RemainingTime = u2RemainingTime;
        /* GR SEM Function is executed now */
        if (((u1Level == ISIS_LEVEL1)
             && (pCktRec->u1IsisGRRestartL1State == ISIS_GR_CSNP_RCVD))
            || ((u1Level == ISIS_LEVEL2)
                && (pCktRec->u1IsisGRRestartL2State == ISIS_GR_CSNP_RCVD)))
        {
            IsisGrRunRestartSem (ISIS_GR_EVT_RX_RA, ISIS_GR_ADJ_SEEN_CSNP,
                                 &GrSemInfo);
        }
        else if (((u1Level == ISIS_LEVEL1)
                  && (pCktRec->u1IsisGRRestartL1State != ISIS_GR_ACK_RCVD))
                 || ((u1Level == ISIS_LEVEL2)
                     && (pCktRec->u1IsisGRRestartL2State != ISIS_GR_ACK_RCVD)))
        {
            IsisGrRunRestartSem (ISIS_GR_EVT_RX_RA, ISIS_GR_ADJ_RESTART,
                                 &GrSemInfo);
        }
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjProcRARcvd ()\n"));

}

/******************************************************************************
 * Function Name : IsisAdjCalcHighPriority ()
 * Description   : This function elects the next router to send CSNP 
 *                 information based onthe SNPA and priority information. 
 *                 This is done only if the adjacency router going down 
 *                 is identified as DIS
 *
 * Input(s)      : pContext --Context
 *                 pCktRec  --Circuit Record
 *                 pAdjRec  --Adjacency Record
 *                 u1Level  --Adjacency level
 *                 pu1SysID -- System ID of adjacency
 *
 * Outputs(s)    : pu1IsHighPriority
 *
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS or ISIS_FAILURE
 ******************************************************************************/
PRIVATE INT4
IsisAdjCalcHighPriority (tIsisCktEntry * pCktRec, UINT1 u1Level,
                         UINT1 *pu1IsHighPriority, UINT1 *pu1SysID)
{
    tIsisCktLevel      *pCktLvlRec = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    UINT1               u1LocPriority = 0;
    INT4                i4RetSNPA = ISIS_FAILURE;
    INT1                i1PrioritySet = ISIS_FALSE;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjCalcHighPriority ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        pCktLvlRec = pCktRec->pL1CktInfo;
    }
    else if (u1Level == ISIS_LEVEL2)
    {
        pCktLvlRec = pCktRec->pL2CktInfo;
    }
    else
    {
        return ISIS_FAILURE;
    }

    pAdjRec = pCktRec->pAdjEntry;

    if (MEMCMP (pu1SysID, pCktLvlRec->au1CktLanDISID, ISIS_SYS_ID_LEN) == 0)
    {
        u1LocPriority = pCktLvlRec->u1ISPriority;

        while (pAdjRec != NULL)
        {
            if (MEMCMP (pAdjRec->au1AdjNbrSysID, pCktLvlRec->au1CktLanDISID,
                        ISIS_SYS_ID_LEN) == 0)
            {
                /*skip DIS comparsion. Since it is going down */
                if ((pAdjRec = pAdjRec->pNext) == NULL)
                {
                    i1PrioritySet = ISIS_TRUE;
                    break;
                }
                continue;
            }

            if ((pAdjRec->u1AdjUsage == u1Level) &&
                (pAdjRec->u1AdjState == ISIS_ADJ_UP) &&
                (pAdjRec->u1IsGRCapEnabled == GR_ENABLED))
            {
                if (pAdjRec->u1AdjNbrPriority > u1LocPriority)
                {
                    /*Neighbour has higher priority than Local router */
                    i1PrioritySet = ISIS_FALSE;
                    break;
                }
                else if (pAdjRec->u1AdjNbrPriority == u1LocPriority)
                {
                    i4RetSNPA = MEMCMP (pAdjRec->au1AdjNbrSNPA,
                                        pCktRec->au1SNPA, ISIS_SNPA_ADDR_LEN);
                    if (i4RetSNPA > 0)
                    {
                        /*Neighbour has higher priority than Local router */
                        i1PrioritySet = ISIS_FALSE;
                        break;
                    }
                    else if (i4RetSNPA == 0)
                    {
                        return ISIS_FAILURE;
                    }
                    else
                    {
                        i1PrioritySet = ISIS_TRUE;
                    }
                }
                else
                {
                    i1PrioritySet = ISIS_TRUE;
                }
            }
            pAdjRec = pAdjRec->pNext;
        }

        if (i1PrioritySet == ISIS_TRUE)
        {
            *pu1IsHighPriority = ISIS_TRUE;
        }
    }
    else
    {
        return ISIS_FAILURE;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjCalcHighPriority ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjUpdateIpAddr ()
 * Description   : This routine updates the Adjacent database record with the 
 *                 new IP address information coming in the HELLO PDU.    
 *                 This will be used to update the adjacent record information
 *                 when there is any change in the IP address of the neighbor.
 * Input(s)      : pu1PDU  - Pointer to the received PDU
 *                 pAdjRec - Pointer to the adjacency record to be modified
 * Output(s)     : None
 * Returns       : VOID
 ******************************************************************************/
INT1
IsisAdjUpdateIpAddr (UINT1 *pu1PDU, tIsisAdjEntry * pAdjRec, tBool * pbLSUFlag)
{
    tIsisCktEntry      *pCktRec = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    UINT4               u4Status = 0;
    UINT4               u4DirIdx = 0;
    UINT2               u2Offset = 0;
    UINT2               u2TmpOffSet = 0;
    UINT1               au1IPV4Addr[ISIS_MAX_IPV4_ADDR_LEN] = { 0 };
    UINT1               au1IPV6Addr[ISIS_MAX_IPV6_ADDR_LEN] = { 0 };
    UINT1               au1NullIP[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1TlvLen = 0;
    UINT1               u1Len = 0;
    UINT1               u1IPv4TlvPresent = ISIS_FALSE;
    UINT1               u1IPv6TlvPresent = ISIS_FALSE;
    UINT1               u1TmrIdx = 0;
    UINT1               u1IsDelReqd = ISIS_FALSE;
    UINT1               u1IsAddReqd = ISIS_FALSE;
#ifdef BFD_WANTED
    UINT1               u1Ipv4UpdReqd = ISIS_FALSE;
    UINT1               u1Ipv6UpdReqd = ISIS_FALSE;
    UINT1               u1AdjAddrType = 0;
#endif

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjUpdateIpAddr ()\n"));
    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);

    if ((*(pu1PDU + 4)) == ISIS_P2P_IIH_PDU)
    {
        /* Get to the Variable Length Fields Offset */
        u2Offset = ISIS_P2P_IIH_HDR_LEN;
    }
    else
    {
        /* Get to the Variable Length Fields Offset */
        u2Offset = ISIS_LAN_IIH_HDR_LEN;
    }

    u2TmpOffSet = u2Offset;
    if (IsisUtlGetNextTlvOffset (pu1PDU, ISIS_IPV4IF_ADDR_TLV, &u2Offset,
                                 &u1TlvLen) != ISIS_FAILURE)
    {
        /* More than one IP Address can be included in a single TLV.
         * copy only the one Interface Address
         */

        u1Len = *(pu1PDU + (u2Offset - 1));
        MEMCPY (au1IPV4Addr, (pu1PDU + u2Offset), ISIS_MAX_IPV4_ADDR_LEN);
        pAdjRec->AdjNbrIpV4Addr.u1IpAddrIndex = ISIS_IPV4_ADDR_INDEX;
        u1IPv4TlvPresent = ISIS_TRUE;
        u2Offset += u1Len;
    }
    else
    {
        /* Else there is no IPV4 address TLV in the packet. Offset
         * would have moved to the end of the packet. Hence reset it
         * to point after the header */
        u2Offset = u2TmpOffSet;
    }

    if (IsisUtlGetNextTlvOffset (pu1PDU, ISIS_IPV6IF_ADDR_TLV, &u2Offset,
                                 &u1TlvLen) != ISIS_FAILURE)
    {
        /* More than one IP Address can be included in a single TLV.
         * copy only the one Interface Address
         */
        u1Len = *(pu1PDU + (u2Offset - 1));
        while (u1Len > 0)
        {
            MEMCPY (&au1IPV6Addr, (pu1PDU + u2Offset), ISIS_MAX_IPV6_ADDR_LEN);
            ISIS_IN6_IS_ADDR_LINKLOCAL (au1IPV6Addr, u4Status);
            /*Link Local Check */
            if (u4Status == ISIS_TRUE)
            {
                pAdjRec->AdjNbrIpV6Addr.u1IpAddrIndex = ISIS_IPV6_ADDR_INDEX;
                u1IPv6TlvPresent = ISIS_TRUE;
                break;
            }
            else
            {
                u2Offset += ISIS_MAX_IPV6_ADDR_LEN;
                u1Len = (UINT1) (u1Len - ISIS_MAX_IPV6_ADDR_LEN);    /*Diab */
            }
        }
    }
    pContext = pAdjRec->pCktRec->pContext;
    pCktRec = pAdjRec->pCktRec;
    u1TmrIdx = pAdjRec->u1TmrIdx;

#ifdef BFD_WANTED
    u1AdjAddrType = pAdjRec->u1AdjIpAddrType;
    /* If BFD is enabled over single topology ISIS, the updation
     * of AdjRec with a specific AddrType can be done only if the 
     * CktRec on which the Adjacency is added is having the
     * respective protocol supported */
    if ((pContext->u1IsisMTSupport == ISIS_FALSE) &&
        (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
    {
        IsisUtlComputeIfUpdateReqd (pAdjRec, &u1Ipv4UpdReqd, &u1Ipv6UpdReqd);
        if (u1Ipv4UpdReqd == ISIS_BFD_FALSE)
        {
            u1IPv4TlvPresent = ISIS_FALSE;
        }
        if (u1Ipv6UpdReqd == ISIS_BFD_FALSE)
        {
            u1IPv6TlvPresent = ISIS_FALSE;
        }
    }
#endif

    /* We need to check if the
     * IP Addr in the CktRec and AdjRec are on the same
     * network. If not, block the addition of Addr to the 
     * AdjRec, since BFD would not come up in such scenario
     * and BFD monitoring cannot be done. */

    if ((u1IPv4TlvPresent == ISIS_TRUE) &&
        (IsisUtlIsDirectlyConnected (pCktRec, au1IPV4Addr) == ISIS_FALSE))
    {
        u1IPv4TlvPresent = ISIS_FALSE;
    }

    if ((u1IPv4TlvPresent == ISIS_TRUE) && (u1IPv6TlvPresent == ISIS_TRUE))
    {
        if ((MEMCMP
             (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
              ISIS_MAX_IPV4_ADDR_LEN) != 0)
            &&
            (MEMCMP
             (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1IPV4Addr,
              ISIS_MAX_IPV4_ADDR_LEN) != 0))
        {
            u1IsDelReqd = ISIS_TRUE;
#ifdef BFD_WANTED
            IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktRec,
                                   pAdjRec, ISIS_IPV4_UNICAST_MT_ID, ISIS_TRUE);
#endif
        }
        if ((MEMCMP
             (pAdjRec->AdjNbrIpV6Addr.au1IpAddr, au1NullIP,
              ISIS_MAX_IPV6_ADDR_LEN) != 0)
            &&
            (MEMCMP
             (pAdjRec->AdjNbrIpV6Addr.au1IpAddr, au1IPV6Addr,
              ISIS_MAX_IPV6_ADDR_LEN) != 0))
        {
            u1IsDelReqd = ISIS_TRUE;
#ifdef BFD_WANTED
            IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktRec,
                                   pAdjRec, ISIS_IPV6_UNICAST_MT_ID, ISIS_TRUE);
#endif
        }

        if (u1IsDelReqd == ISIS_TRUE)
        {
            IsisAdjDelDirEntry (pContext, pAdjRec, &u4DirIdx);
#ifdef ISIS_FT_ENABLED
            ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_DELETE, pAdjRec);
#endif
            u1IsAddReqd = ISIS_TRUE;
        }

        MEMCPY (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1IPV4Addr,
                ISIS_MAX_IPV4_ADDR_LEN);
        MEMCPY (pAdjRec->AdjNbrIpV6Addr.au1IpAddr, au1IPV6Addr,
                ISIS_MAX_IPV6_ADDR_LEN);
        if (pAdjRec->u1AdjIpAddrType != ISIS_ADDR_IPVX)
        {
            pAdjRec->u1AdjIpAddrType = ISIS_ADDR_IPVX;
            *pbLSUFlag = ISIS_TRUE;
        }
    }
    else if (u1IPv4TlvPresent == ISIS_TRUE)
    {
        if ((MEMCMP
             (pAdjRec->AdjNbrIpV6Addr.au1IpAddr, au1NullIP,
              ISIS_MAX_IP_ADDR_LEN) != 0))
        {
            u1IsDelReqd = ISIS_TRUE;
#ifdef BFD_WANTED
            IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktRec,
                                   pAdjRec, ISIS_IPV6_UNICAST_MT_ID, ISIS_TRUE);
#endif
        }
        else
        {
            /*Adding the address newly */
            *pbLSUFlag = ISIS_TRUE;
        }
        if ((MEMCMP
             (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
              ISIS_MAX_IPV4_ADDR_LEN) != 0)
            &&
            (MEMCMP
             (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1IPV4Addr,
              ISIS_MAX_IPV4_ADDR_LEN) != 0))
        {
            u1IsDelReqd = ISIS_TRUE;
#ifdef BFD_WANTED
            IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktRec,
                                   pAdjRec, ISIS_IPV4_UNICAST_MT_ID, ISIS_TRUE);
#endif
        }

        if (u1IsDelReqd == ISIS_TRUE)
        {
            IsisAdjDelDirEntry (pContext, pAdjRec, &u4DirIdx);
#ifdef ISIS_FT_ENABLED
            ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_DELETE, pAdjRec);
#endif
            u1IsAddReqd = ISIS_TRUE;
            *pbLSUFlag = ISIS_TRUE;
        }

        MEMCPY (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1IPV4Addr,
                ISIS_MAX_IPV4_ADDR_LEN);
        pAdjRec->u1AdjIpAddrType = ISIS_ADDR_IPV4;
        if (u1IsAddReqd == ISIS_TRUE)
        {
            MEMCPY (pAdjRec->AdjNbrIpV6Addr.au1IpAddr, au1NullIP,
                    ISIS_MAX_IPV6_ADDR_LEN);
            MEMCPY (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1IPV4Addr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pAdjRec->u1AdjIpAddrType = ISIS_ADDR_IPV4;
        }
    }
    else if (u1IPv6TlvPresent == ISIS_TRUE)
    {
        if ((MEMCMP
             (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
              ISIS_MAX_IPV4_ADDR_LEN) != 0))
        {
            u1IsDelReqd = ISIS_TRUE;
#ifdef BFD_WANTED
            IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktRec,
                                   pAdjRec, ISIS_IPV4_UNICAST_MT_ID, ISIS_TRUE);
#endif
        }
        else
        {
            /*Adding the address newly */
            *pbLSUFlag = ISIS_TRUE;
        }

        if ((MEMCMP
             (pAdjRec->AdjNbrIpV6Addr.au1IpAddr, au1NullIP,
              ISIS_MAX_IPV6_ADDR_LEN) != 0)
            &&
            (MEMCMP
             (pAdjRec->AdjNbrIpV6Addr.au1IpAddr, au1IPV6Addr,
              ISIS_MAX_IPV6_ADDR_LEN) != 0))
        {                        /*The Neighbor had an IPv4 address before */
            u1IsDelReqd = ISIS_TRUE;
#ifdef BFD_WANTED
            IsisDeRegisterWithBfd (pContext->u4SysInstIdx, pCktRec,
                                   pAdjRec, ISIS_IPV6_UNICAST_MT_ID, ISIS_TRUE);
#endif
        }

        if (u1IsDelReqd == ISIS_TRUE)
        {
            IsisAdjDelDirEntry (pContext, pAdjRec, &u4DirIdx);
#ifdef ISIS_FT_ENABLED
            ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_DELETE, pAdjRec);
#endif
            u1IsAddReqd = ISIS_TRUE;
            *pbLSUFlag = ISIS_TRUE;
        }

        MEMCPY (pAdjRec->AdjNbrIpV6Addr.au1IpAddr, au1IPV6Addr,
                ISIS_MAX_IPV6_ADDR_LEN);
        pAdjRec->u1AdjIpAddrType = ISIS_ADDR_IPV6;
        if (u1IsAddReqd == ISIS_TRUE)
        {
            MEMCPY (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pAdjRec->u1AdjIpAddrType = ISIS_ADDR_IPV6;
        }
    }
    else
    {
        IsisAdjDelAdj (pContext, pCktRec, pAdjRec, ISIS_FALSE, &u4DirIdx);
        ADP_PI ((ISIS_LGST,
                 "ADJ <I> : Adjacency Deleted - IP Address in the Circuit Record and Adjacency Record are on the same network\n"));
        IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING, u4DirIdx, u1TmrIdx);
        return ISIS_FAILURE;
    }

    if (u1IsAddReqd == ISIS_TRUE)
    {
        /* Adding Direction Entry corresponding to this Adjacency Record
         */
        ADP_PI ((ISIS_LGST,
                 "ADJ <I> : IP Address change detected on peer IIH - updating ADJ Record\n"));
        pDirEntry =
            (tIsisAdjDirEntry *) ISIS_MEM_ALLOC (ISIS_BUF_ADIR,
                                                 sizeof (tIsisAdjDirEntry));

        if (pDirEntry != NULL)
        {
            IsisAdjAddDirEntry (pContext, pAdjRec, pDirEntry);
        }
        else
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Adjacency Direction Entry\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddAdj ()\n"));
        }
    }

#ifdef BFD_WANTED
    if (u1AdjAddrType != pAdjRec->u1AdjIpAddrType)
    {
        /* In case of single topology ISIS, register for BFD
         * monitoring if there is a change in the protocol 
         * supported in the AdjRec */
        if ((pContext->u1IsisMTSupport == ISIS_FALSE) &&
            (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
        {
            ISIS_BFD_REGISTER (pContext, pAdjRec, pCktRec);
        }
    }
#endif
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjUpdateIpAddr ()\n"));
    return ISIS_SUCCESS;
}

/*MISIS - to update MTID*/
/******************************************************************************
 * Function Name : IsisAdjUpdateMTId ()
 * Description   : This routine updates the Adjacent database record with the
 *                 new MT ID coming in the HELLO PDU.
 *                 This will be used to update the adjacent record information
 *                 when there is any change in the IP address of the neighbor.
 * Input(s)      : pu1PDU  - Pointer to the received PDU
 *                 pAdjRec - Pointer to the adjacency record to be modified
 * Output(s)     : None
 * Returns       : VOID
 ******************************************************************************/
PUBLIC VOID
IsisAdjUpdateMTId (UINT1 *pu1PDU, tIsisAdjEntry * pAdjRec, tBool * pbLSUFlag)
{
    UINT2               u2Offset = 0;
    UINT2               u2MTId = 0;
    UINT2               u1AdjMTId = 0;
    UINT1               u1TlvLen = 0;
    UINT1               u1Len = 0;
    UINT1               u1MtTlvFound = ISIS_FALSE;
    UINT1               u1Flag = ISIS_TRUE;
#ifdef BFD_WANTED
    UINT1               au1MtUpdReqd[ISIS_MAX_MT];

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjUpdateMTId ()\n"));

    MEMSET (au1MtUpdReqd, ISIS_BFD_FALSE, sizeof (au1MtUpdReqd));
#endif

    if ((*(pu1PDU + 4)) == ISIS_P2P_IIH_PDU)
    {
        /* Get to the Variable Length Fields Offset */
        u2Offset = ISIS_P2P_IIH_HDR_LEN;
    }
    else
    {
        /* Get to the Variable Length Fields Offset */
        u2Offset = ISIS_LAN_IIH_HDR_LEN;
    }

#ifdef BFD_WANTED
    /* If BFD is enabled and the neighbor is useable,
     * then the MT ID can be updated only if the 
     * session state is UP */
    IsisUtlComputeIfUpdateReqd (pAdjRec,
                                &au1MtUpdReqd[ISIS_MT0_INDEX],
                                &au1MtUpdReqd[ISIS_MT2_INDEX]);
    if ((au1MtUpdReqd[ISIS_MT0_INDEX] == ISIS_BFD_FALSE) &&
        (au1MtUpdReqd[ISIS_MT2_INDEX] == ISIS_BFD_FALSE))
    {
        return;
    }
#endif

    if (IsisUtlGetNextTlvOffset (pu1PDU, ISIS_MT_TLV, &u2Offset,
                                 &u1TlvLen) != ISIS_FAILURE)
    {
        /* More than one MT can be included in a single TLV.
         * 
         */
        u1MtTlvFound = ISIS_TRUE;
        u1Len = u1TlvLen;
        while (u1Len > 0)
        {
            MEMCPY (&u2MTId, (pu1PDU + u2Offset), ISIS_MT_ID_LEN);
            u2MTId = OSIX_HTONS (u2MTId);
            u2MTId = (u2MTId & ISIS_MTID_MASK);

            if (u2MTId == ISIS_IPV4_UNICAST_MT_ID)
            {
#ifdef BFD_WANTED
                if (au1MtUpdReqd[ISIS_MT0_INDEX] == ISIS_BFD_TRUE)
                {
                    if ((pAdjRec->u1Mt0BfdRegd == ISIS_BFD_TRUE) ||
                        (pAdjRec->u1Mt2BfdRegd == ISIS_BFD_TRUE))
                    {
                        if (!(pAdjRec->pCktRec->u1IfMTId & ISIS_MT0_BITMAP))
                        {
                            u1Flag = ISIS_FALSE;
                        }
                    }
                }
#endif
                if (u1Flag == ISIS_TRUE)
                {
                    u1AdjMTId |= ISIS_MT0_BITMAP;
                }
                u1Flag = ISIS_TRUE;
            }
            else if (u2MTId == ISIS_IPV6_UNICAST_MT_ID)
            {
#ifdef BFD_WANTED
                if (au1MtUpdReqd[ISIS_MT2_INDEX] == ISIS_BFD_TRUE)
                {
                    if ((pAdjRec->u1Mt0BfdRegd == ISIS_BFD_TRUE) ||
                        (pAdjRec->u1Mt2BfdRegd == ISIS_BFD_TRUE))
                    {
                        if (!(pAdjRec->pCktRec->u1IfMTId & ISIS_MT2_BITMAP))
                        {
                            u1Flag = ISIS_FALSE;
                        }
                    }
                }
#endif
                if (u1Flag == ISIS_TRUE)
                {
                    u1AdjMTId |= ISIS_MT2_BITMAP;
                }
                u1Flag = ISIS_TRUE;
            }

            u2Offset += 2;
            u1Len = (UINT1) (u1Len - 2);    /*Diab */
        }
    }

    if (u1MtTlvFound == ISIS_FALSE)
    {
        u1AdjMTId = ISIS_MT0_BITMAP;
    }
    if (pAdjRec->u1AdjMTId != u1AdjMTId)
    {
        pAdjRec->u1AdjMTId = u1AdjMTId;
        *pbLSUFlag = ISIS_TRUE;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjUpdateMTId ()\n"));
    return;
}

#ifdef BFD_WANTED
/******************************************************************************
 * Function Name : IsisAdjUpdateBfdEnabled ()
 * Description   : This routine updates the Adjacent database record with the
 *                 new MT-ID/NLPID pair coming in the BFD Enabled TLV of IIH.
 * Input(s)      : pu1PDU  - Pointer to the received PDU
 *                 pAdjRec - Pointer to the adjacency record to be modified
 * Output(s)     : None
 * Returns       : ISIS_SUCCESS/ISIS_FAILURE
 ******************************************************************************/
PUBLIC INT1
IsisAdjUpdateBfdEnabled (UINT1 *pu1PDU, tIsisAdjEntry * pAdjRec,
                         tBool * pbLSUFlag, UINT1 *pu1AdjState)
{
    tIsisCktEntry      *pCktRec = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MTId = 0;
    UINT1               u1TlvLen = 0;
    UINT1               u1Len = 0;
    UINT1               u1NLPId = 0;
    UINT1               u1IsisMT0BfdEnabled = 0;
    UINT1               u1IsisMT2BfdEnabled = 0;
    tBool               bLSUFlag = ISIS_FALSE;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjUpdateBfdEnabled ()\n"));

    pCktRec = pAdjRec->pCktRec;

    /* Updation of MT Id and IP Addresses are reqd for 
     * before computing the BFD Reqd variable */
    if ((pCktRec != NULL) && (pCktRec->pContext->u1IsisMTSupport == ISIS_TRUE))
    {
        IsisAdjUpdateMTId (pu1PDU, pAdjRec, &bLSUFlag);
    }
    if (IsisAdjUpdateIpAddr (pu1PDU, pAdjRec, &bLSUFlag) != ISIS_SUCCESS)
    {
        return ISIS_FAILURE;
    }

    if ((*(pu1PDU + 4)) == ISIS_P2P_IIH_PDU)
    {
        /* Get to the Variable Length Fields Offset */
        u2Offset = ISIS_P2P_IIH_HDR_LEN;
    }
    else
    {
        /* Get to the Variable Length Fields Offset */
        u2Offset = ISIS_LAN_IIH_HDR_LEN;
    }

    if (IsisUtlGetNextTlvOffset (pu1PDU, ISIS_BFD_ENABLED_TLV, &u2Offset,
                                 &u1TlvLen) != ISIS_FAILURE)
    {
        /* More than one MT can be included in a single TLV. */
        u1Len = u1TlvLen;
        while (u1Len > 0)
        {
            MEMCPY (&u2MTId, (pu1PDU + u2Offset), ISIS_MT_ID_LEN);
            u2MTId = OSIX_HTONS (u2MTId);
            u2MTId = (u2MTId & ISIS_MTID_MASK);

            u2Offset = (UINT2) (u2Offset + 2);
            u1Len = (UINT1) (u1Len - 2);

            u1NLPId = *((UINT1 *) (pu1PDU + u2Offset));

            u2Offset = (UINT2) (u2Offset + 1);
            u1Len = (UINT1) (u1Len - 1);
            if ((u2MTId == ISIS_IPV4_UNICAST_MT_ID) &&
                (u1NLPId == ISIS_IPV4_SUPP))
            {
                /* check for bfd support in ckt and update correspondingly */
                if ((pCktRec->u1IfMTId & ISIS_MT0_BITMAP) == ISIS_MT0_BITMAP)
                {
                    u1IsisMT0BfdEnabled |= ISIS_BFD_IPV4_ENABLED;
                }
            }
            else if ((u2MTId == ISIS_IPV6_UNICAST_MT_ID) &&
                     (u1NLPId == ISIS_IPV6_SUPP))
            {
                if ((pCktRec->u1IfMTId & ISIS_MT2_BITMAP) == ISIS_MT2_BITMAP)
                {
                    u1IsisMT2BfdEnabled |= ISIS_BFD_IPV6_ENABLED;
                }
            }
        }
    }

    if (pAdjRec->u1IsisMT0BfdEnabled != u1IsisMT0BfdEnabled)
    {
        pAdjRec->u1IsisMT0BfdEnabled = u1IsisMT0BfdEnabled;
        *pbLSUFlag = ISIS_TRUE;
    }
    if (pAdjRec->u1IsisMT2BfdEnabled != u1IsisMT2BfdEnabled)
    {
        pAdjRec->u1IsisMT2BfdEnabled = u1IsisMT2BfdEnabled;
        *pbLSUFlag = ISIS_TRUE;
    }

    /* We dont support an MTID, but the neighbor is
     * supporting the MTID and is sending BFD enabled 
     * TLV for that particular MT */

    if ((pCktRec->u1IfMTId & ISIS_MT0_BITMAP) &&
        (!(pCktRec->u1IfMTId & ISIS_MT2_BITMAP)))
    {
        if ((pAdjRec->u1AdjMTId & ISIS_MT2_BITMAP) &&
            (!(pAdjRec->u1AdjMTId & ISIS_MT0_BITMAP)))
        {
            ADP_PI ((ISIS_LGST,
                     "ADJ <I> : BFD - Topology Mismatch detected: Local [ 0 ], Peer [ 2 ] \n"));
            *pu1AdjState = ISIS_ADJ_INIT;
            *pbLSUFlag = ISIS_TRUE;
        }
    }

    if ((pCktRec->u1IfMTId & ISIS_MT2_BITMAP) &&
        (!(pCktRec->u1IfMTId & ISIS_MT0_BITMAP)))
    {
        if ((pAdjRec->u1AdjMTId & ISIS_MT0_BITMAP) &&
            (!(pAdjRec->u1AdjMTId & ISIS_MT2_BITMAP)))
        {
            ADP_PI ((ISIS_LGST,
                     "ADJ <I> : BFD - Topology Mismatch detected: Local [ 2 ], Peer [ 1 ] \n"));
            *pu1AdjState = ISIS_ADJ_INIT;
            *pbLSUFlag = ISIS_TRUE;
        }
    }

    if (*pbLSUFlag == ISIS_TRUE)
    {
        IsisUtlUpdateBfdRequired (pCktRec, pAdjRec);
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjUpdateBfdEnabled ()\n"));
    return ISIS_SUCCESS;
}
#endif
