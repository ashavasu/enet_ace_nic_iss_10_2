/******************************************************************************
 *
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isevt.c,v 1.12 2017/09/11 13:44:08 siva Exp $
 *
 *   Description: This file contains the routines to log the events to the 
 *                Log Table
 *
 *****************************************************************************/

#include "isincl.h"
#include "isextn.h"

/* Function prototypes
 */

PRIVATE tIsisEvent *IsisCtrlGetFreeEvtRec (tIsisSysContext *);

#ifdef ISIS_TRAPS

PRIVATE VOID        IsisCtrlCreateNotifyTable (tIsisNotifyTable *, UINT1 *);

#endif

/******************************************************************************
 * Function Name  : IsisCtrlLogEvent ()
 * Description    : This routine Logs the events in the log table in a string
 *                  format which can be read through SNMP.
 * Input (s)      : pContext  - Pointer to the System context
 *                  pu1Msg    - Pointer to the message to be logged
 * Output (s)     : None 
 * Globals        : Not Reffered or Modified
 * Returns        : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisCtrlLogEvent (tIsisSysContext * pContext, UINT1 *pu1Msg)
{
    CHAR                au1String1[50];
    CHAR                au1String2[ISIS_LOG_EVT_STR_LEN];
    CHAR                au1String3[ISIS_MAX_IPV6_ADDR_LEN];
    UINT1               u1EvtIdx = 0;
    UINT1               u1MetIdx = 0;
    UINT1               u1Length = 0;
    tIsisEvent         *pEvent = NULL;

    MEMSET (au1String1, 0, 50);
    MEMSET (au1String2, 0, ISIS_LOG_EVT_STR_LEN);
    MEMSET (au1String3, 0, ISIS_MAX_IPV6_ADDR_LEN);
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlLogEvent ()\n"));

    UNUSED_PARAM (u1MetIdx);
    /* In the case of ISIS_EVT_SHUTDOWN and ISIS_EVT_RESETALL, the context
     * itself will be removed or modified with new parameters. Hence the Log
     * table does not make any sense for these events.
     */

    if (*pu1Msg == ISIS_EVT_SHUTDOWN)
    {
        CTP_PT ((ISIS_LGST, "CTL <T> : Shutdown All Event\n"));
        return;
    }
    if (*pu1Msg == ISIS_EVT_RESETALL)
    {
        CTP_PT ((ISIS_LGST, "CTL <T> : ResetAll Event\n"));
        return;
    }
    if (pContext == NULL)
    {
        return;
    }
    u1EvtIdx = pContext->EventTable.u1NextEvtIdx;
    pEvent = (tIsisEvent *) IsisCtrlGetFreeEvtRec (pContext);

    if (pEvent == NULL)
    {
        WARNING ((ISIS_LGST, "CTL <E> : Unable to Log Event [ %u ] \n",
                  *pu1Msg));
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlLogEvent ()\n"));
        return;
    }

    switch (*pu1Msg)
    {
        case ISIS_EVT_MEM_RES_FAIL:

            pEvent->u1Event = ((tIsisEvtResFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_SYS_FAIL:

            pEvent->u1Event = ((tIsisEvtSysFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_REG_FAIL:

            pEvent->u1Event = ((tIsisEvtRegFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_DEREG_FAIL:

            pEvent->u1Event = ((tIsisEvtDeRegFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_TX_FAIL:

            pEvent->u1Event = ((tIsisEvtTxFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_RX_FAIL:

            pEvent->u1Event = ((tIsisEvtRxFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_FTM_DCOD_FAIL:

            pEvent->u1Event = ((tIsisEvtFTMDecFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_FTM_ABORT:

            pEvent->u1Event = ((tIsisEvtFTMAbort *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_RTM_UPDT_FAIL:

            pEvent->u1Event = ((tIsisEvtRTMUpdFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_MIBII_LOOK_UP_FAIL:

            pEvent->u1Event =
                ((tIsisEvtMIBIILookUpFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_DLL_CONN_FAIL:

            pEvent->u1Event =
                ((tIsisEvtDLLConnFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_DLL_IOCTL_FAIL:

            pEvent->u1Event =
                ((tIsisEvtDllIOCTLFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_INV_IFIDX:

            pEvent->u1Event = ((tIsisEvtInvIfIdx *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_INV_PDU:

            pEvent->u1Event = ((tIsisEvtInvPdu *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_DUP_SYS_DET:

            pEvent->u1Event = ((tIsisEvtDupSys *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            /* The Macro ISIS_FORM_STR copies the SysID or the SNPA to the
             * specified string in the proper format. The same string is
             * printed into the au1EventData as the Log String
             */

            ISIS_FORM_STR (ISIS_SYS_ID_LEN,
                           ((tIsisEvtDupSys *) pu1Msg)->au1SysID, au1String1);
            ISIS_FORM_STR (ISIS_SNPA_ADDR_LEN,
                           ((tIsisEvtDupSys *) pu1Msg)->au1SNPA, au1String2);

            break;

        case ISIS_EVT_ID_LEN_MISMATCH:

            pEvent->u1Event =
                ((tIsisEvtIDLenMismatch *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            /*Release PDU Fragment Memory */
            ISIS_MEM_FREE (ISIS_BUF_EVTS, ((tIsisEvtIDLenMismatch *) (VOID *)
                                           pu1Msg)->pu1PduFrag);
            break;

        case ISIS_EVT_AUTH_FAIL:

            pEvent->u1Event = ((tIsisEvtAuthFail *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;
            if (((tIsisEvtAuthFail *) (VOID *) pu1Msg)->u1Cause ==
                ISIS_PASSWD_MISMTCH)
            {
                u1Length = ((tIsisEvtAuthFail *) (VOID *) pu1Msg)->
                    RxdPasswd.u1Len;
                if (u1Length <= ISIS_MAX_PASSWORD_LEN)
                {
                    ISIS_FORM_STR (u1Length,
                                   ((tIsisEvtAuthFail *) (VOID *) pu1Msg)->
                                   RxdPasswd.au1Password, au1String1);
                }
            }

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            /*Release PDU Fragment Memory */
            ISIS_MEM_FREE (ISIS_BUF_EVTS, ((tIsisEvtAuthFail *) (VOID *)
                                           pu1Msg)->pu1PduFrag);

            break;

        case ISIS_EVT_IS_UP:

            pEvent->u1Event = ((tIsisEvtISStatChange *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_SYS_ID_LEN,
                           ((tIsisEvtISStatChange *) pu1Msg)->au1SysID,
                           au1String1);

            break;

        case ISIS_EVT_IS_DESTROY:

            WARNING ((ISIS_LGST,
                      "CTL <T> : Processing ISIS IS Destroy event \n"));
            pEvent->u1Event = ((tIsisEvtISStatChange *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_SYS_ID_LEN,
                           ((tIsisEvtISStatChange *) pu1Msg)->au1SysID,
                           au1String1);

            break;

        case ISIS_EVT_IS_DOWN:

            PANIC ((ISIS_LGST, "CTL <T> : Processing ISIS IS DOWN event \n"));
            pEvent->u1Event = ((tIsisEvtISStatChange *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_SYS_ID_LEN,
                           ((tIsisEvtISStatChange *) pu1Msg)->au1SysID,
                           au1String1);

            break;

        case ISIS_EVT_WRONG_SYS_TYPE:

            pEvent->u1Event = ((tIsisEvtWrongSys *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_SYS_ID_LEN,
                           ((tIsisEvtWrongSys *) pu1Msg)->au1AdjSysID,
                           au1String1);

            break;

        case ISIS_EVT_INCOMP_CKT:

            pEvent->u1Event = ((tIsisEvtIncompCkt *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_AA_MISMATCH:

            pEvent->u1Event = ((tIsisEvtAAMismatch *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            /*Release PDU Fragment Memory */
            ISIS_MEM_FREE (ISIS_BUF_EVTS, ((tIsisEvtAAMismatch *) (VOID *)
                                           pu1Msg)->pu1PduFrag);

            break;

        case ISIS_EVT_MAN_ADDR_CHANGE:

            pEvent->u1Event = ((tIsisEvtManAAChange *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;
            u1Length = ((tIsisEvtManAAChange *) pu1Msg)->u1Length;

            if (u1Length <= ISIS_AREA_ADDR_LEN)
            {
                ISIS_FORM_STR (u1Length,
                               ((tIsisEvtManAAChange *) pu1Msg)->au1AreaAddr,
                               au1String1);
            }

            break;

        case ISIS_EVT_MAN_ADDR_DROPPED:

            pEvent->u1Event = ((tIsisEvtManAADropped *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;
            u1Length = ((tIsisEvtManAADropped *) pu1Msg)->u1Length;

            if (u1Length <= ISIS_AREA_ADDR_LEN)
            {
                ISIS_FORM_STR (u1Length,
                               ((tIsisEvtManAADropped *) pu1Msg)->au1AreaAddr,
                               au1String1);
            }

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_MAX_AA_MISMATCH:

            pEvent->u1Event =
                ((tIsisEvtMaxAAMismatch *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            /*Release PDU Fragment Memory */
            ISIS_MEM_FREE (ISIS_BUF_EVTS, ((tIsisEvtMaxAAMismatch *) (VOID *)
                                           pu1Msg)->pu1PduFrag);

            break;

        case ISIS_EVT_PROT_SUPP_CHANGE:

            pEvent->u1Event = ((tIsisEvtPSChange *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_IP_IF_ADDR_CHANGE:

            pEvent->u1Event = ((tIsisEvtIPIFChange *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;
            if ((((tIsisEvtIPIFChange *) (VOID *) pu1Msg)->u1AddrType) ==
                ISIS_ADDR_IPV4)
            {
                MEMCPY (au1String1, ((tIsisEvtIPIFChange *) (VOID *) pu1Msg)->
                        au1IPAddr, ISIS_MAX_IPV4_ADDR_LEN);

            }
            else if ((((tIsisEvtIPIFChange *) (VOID *) pu1Msg)->u1AddrType) ==
                     ISIS_ADDR_IPV6)
            {
                MEMCPY (&au1String3,
                        ((tIsisEvtIPIFChange *) (VOID *) pu1Msg)->au1IPAddr,
                        ISIS_MAX_IPV6_ADDR_LEN);

            }
            else
            {
                break;
            }
            break;

        case ISIS_EVT_IPRA_CHANGE:
            pEvent->u1Event = ((tIsisEvtIPRAChange *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;
            if ((((tIsisEvtIPRAChange *) (VOID *) pu1Msg)->u1IPRADestType) ==
                ISIS_ADDR_IPV4)
            {
                ISIS_FORM_STR (ISIS_MAX_IPV4_ADDR_LEN,
                               ((tIsisEvtIPRAChange *) (VOID *) pu1Msg)->
                               au1IPRADest, au1String1);

                u1MetIdx = (UINT1) IsisUtlGetMetIndex (pContext,
                                                       (((tIsisEvtIPRAChange
                                                          *) (VOID *) pu1Msg)->
                                                        u1MetricType));
            }
            else if ((((tIsisEvtIPRAChange *) (VOID *) pu1Msg)->
                      u1IPRADestType) == ISIS_ADDR_IPV6)
            {
                ISIS_FORM_STR (ISIS_SNPA_ADDR_LEN,
                               ((tIsisEvtIPRAChange *) (VOID *) pu1Msg)->
                               au1IPRASNPA, au1String2);
                MEMCPY (&au1String3,
                        ((tIsisEvtIPRAChange *) (VOID *) pu1Msg)->au1IPRADest,
                        ISIS_MAX_IPV6_ADDR_LEN);
                u1MetIdx =
                    (UINT1) IsisUtlGetMetIndex (pContext,
                                                (((tIsisEvtIPRAChange *) (VOID
                                                                          *)
                                                  pu1Msg)->u1MetricType));
            }
            else
            {
                break;
            }
            break;
        case ISIS_EVT_SUMM_ADDR_CHANGE:

            pEvent->u1Event =
                ((tIsisEvtSummAddrChg *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            if ((((tIsisEvtSummAddrChg *) (VOID *) pu1Msg)->u1AddrType) ==
                ISIS_ADDR_IPV4)
            {

                u1MetIdx = (UINT1) IsisUtlGetMetIndex (pContext,
                                                       (((tIsisEvtSummAddrChg *)
                                                         (VOID *) pu1Msg)->
                                                        u1MetricType));
            }
            else if ((((tIsisEvtSummAddrChg *) (VOID *) pu1Msg)->u1AddrType) ==
                     ISIS_ADDR_IPV6)
            {
                MEMCPY (&au1String3,
                        ((tIsisEvtIPRAChange *) (VOID *) pu1Msg)->au1IPRADest,
                        ISIS_MAX_IPV6_ADDR_LEN);
                u1MetIdx =
                    (UINT1) IsisUtlGetMetIndex (pContext,
                                                (((tIsisEvtSummAddrChg *)
                                                  (VOID *) pu1Msg)->
                                                 u1MetricType));
            }
            break;
        case ISIS_EVT_CKT_CHANGE:

            pEvent->u1Event = ((tIsisEvtCktChange *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            u1MetIdx = (UINT1) IsisUtlGetMetIndex (pContext,
                                                   (((tIsisEvtCktChange *) (VOID
                                                                            *)
                                                     pu1Msg)->u1MetricType));
            break;

        case ISIS_EVT_ADJ_CHANGE:

            pEvent->u1Event = ((tIsisEvtAdjChange *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_SYS_ID_LEN,
                           ((tIsisEvtAdjChange *) (VOID *) pu1Msg)->au1AdjSysID,
                           au1String1);

            u1MetIdx = (UINT1) IsisUtlGetMetIndex (pContext,
                                                   (((tIsisEvtAdjChange *) (VOID
                                                                            *)
                                                     pu1Msg)->u1MetricType));
            break;

        case ISIS_EVT_REJECTED_ADJ:

            pEvent->u1Event = ((tIsisEvtAdjRej *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_SYS_ID_LEN,
                           ((tIsisEvtAdjRej *) (VOID *) pu1Msg)->au1AdjSysID,
                           au1String1);

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_DIS_NOT_ELECTED:

            pEvent->u1Event = ((tIsisEvtNoDISElect *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_SNPA_ADDR_LEN,
                           ((tIsisEvtNoDISElect *) (VOID *) pu1Msg)->au1SNPA,
                           au1String1);

            break;

        case ISIS_EVT_DIS_CHANGE:

            pEvent->u1Event = ((tIsisEvtDISChg *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN,
                           ((tIsisEvtDISChg *) (VOID *) pu1Msg)->au1PrevDIS,
                           au1String1);

            ISIS_FORM_STR (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN,
                           ((tIsisEvtDISChg *) (VOID *) pu1Msg)->au1CurrDIS,
                           au1String2);

            break;

        case ISIS_EVT_CHG_IN_DIS_STAT:

            pEvent->u1Event = ((tIsisEvtDISStatChg *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_PRIORITY_CHANGE:

            pEvent->u1Event =
                ((tIsisEvtPriorityChange *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_CORR_LSP_DET:

            pEvent->u1Event = ((tIsisEvtCorrLsp *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_INV_LSP:

            pEvent->u1Event = ((tIsisEvtInvalidLSP *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_LSPID_LEN,
                           ((tIsisEvtInvalidLSP *) (VOID *) pu1Msg)->au1LSPId,
                           au1String1);

            break;

        case ISIS_EVT_LSP_DBOL:

            pEvent->u1Event = ((tIsisEvtLSPDBOL *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_OWN_LSP_PURGE:

            pEvent->u1Event =
                ((tIsisEvtOwnLspPurge *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_LSP_DB_VALIDATED:

            pEvent->u1Event = ((tIsisEvtDBVal *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_MAX_SEQNO_EXCEED:

            pEvent->u1Event =
                ((tIsisEvtSeqNoExceed *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_SEQ_NO_SKIP:

            pEvent->u1Event = ((tIsisEvtSeqNoSkip *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_TX_Q_FULL:

            pEvent->u1Event = ((tIsisEvtTxQFull *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_FORM_STR (ISIS_LSPID_LEN,
                           ((tIsisEvtTxQFull *) pu1Msg)->au1LspID, au1String1);

            break;

        case ISIS_EVT_NEAR_L2_NOT_AVAIL:

            pEvent->u1Event = ((tIsisEvtNearL2NotAvail *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            break;

        case ISIS_EVT_L1_DECN_COMPLETE:
        case ISIS_EVT_L2_DECN_COMPLETE:

            ISIS_MEM_FREE (ISIS_BUF_EVTE, (UINT1 *) pEvent);
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlLogEvent ()\n"));
            return;

        case ISIS_EVT_VER_MISMATCH:

            pEvent->u1Event =
                ((tIsisEvtVerMismatch *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_LSP_TOO_LARGE:

            pEvent->u1Event =
                ((tIsisEvtLspSizeTooLarge *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_LSP_BUFSIZE_MISMATCH:

            pEvent->u1Event =
                ((tIsisEvtLspBuffSizeMismatch *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        case ISIS_EVT_PROT_SUPP_MISMATCH:

            pEvent->u1Event =
                ((tIsisEvtProtSuppMismatch *) (VOID *) pu1Msg)->u1EvtID;
            pEvent->u1EventIdx = u1EvtIdx;

            ISIS_SEND_TRAP (&pContext->TrapNotifyTable, pu1Msg);
            break;

        default:

            ISIS_MEM_FREE (ISIS_BUF_EVTE, (UINT1 *) pEvent);
            CTP_PT ((ISIS_LGST, "CTL <T> : Event [ %u ]\n", *pu1Msg));
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlLogEvent ()\n"));
            return;
    }

    pContext->EventTable.u1NextEvtIdx++;
    if (pContext->EventTable.pEvent == NULL)
    {
        /* Adding the first entry , first and last entry is the same */
        pEvent->pNext = NULL;
        pContext->EventTable.pEvent = pEvent;
        pContext->EventTable.pLastEvtEntry = pEvent;
    }

    pContext->EventTable.pLastEvtEntry->pNext = pEvent;
    pContext->EventTable.pLastEvtEntry = pEvent;
    pEvent->pNext = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlLogEvent ()\n"));
}

/******************************************************************************
 * Function Name  : IsisCtrlGetFreeEvtRec ()
 * Description    : This routine allocates the memory for the pEvent structure
 *                  and returns the same. If the memory allocation fails, then,
 *                  it has reached the end of the Log Table, (i.e., number of
 *                  Events logged have crossed the configured limit.), then
 *                  this routine, returns the last pEvent Record from the Log
 *                  table.
 * Input (s) N     : pContext - Pointer to the System context
 * Output (s)     : None
 * Returns        : pEvent   - Pointer to the Event Record allocated or
 *                             fetched from the end of the Log Table
 *****************************************************************************/

PRIVATE tIsisEvent *
IsisCtrlGetFreeEvtRec (tIsisSysContext * pContext)
{
    tIsisEvent         *pEvent = NULL;
    tIsisEvent         *pFirstRec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGetFreeEvtRec ()\n"));

    pEvent = (tIsisEvent *) ISIS_MEM_ALLOC (ISIS_BUF_EVTE, sizeof (tIsisEvent));

    if (pEvent == NULL)
    {
        /* Allocation Failed. Mem pool was allocated for MAX_EVENTS. So 
         * allocation can fail if MAX_EVENTS have already been allocated.
         * So delink the first event entry and use the allocated memory 
         * for the new event. */

        if (pContext->EventTable.pEvent != NULL)
        {
            /* Get the first entry */

            pFirstRec = pContext->EventTable.pEvent;

            /* Delink the Record from Existing Chain */
            pContext->EventTable.pEvent = pFirstRec->pNext;
            pFirstRec->pNext = NULL;

            pEvent = pFirstRec;
        }
    }

    if (pEvent != NULL)
    {
        MEMSET (pEvent, 0, sizeof (tIsisEvent));
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlGetFreeEvtRec ()\n"));
    return pEvent;
}

/*******************************************************************************
 * Function    : IsisCtrlGetEventStr ()
 * Description : This routine returns a string based on the given event. This is
 *               used purely for debugging purposes
 * Input(s)    : u1Event - Event whose corresponding string is to be returned
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : A Display String for the given event
 ******************************************************************************/

CHAR               *
IsisCtrlGetEventStr (UINT1 u1Event)
{
    switch (u1Event)
    {
        case ISIS_EVT_MEM_RES_FAIL:
            return "ISIS_EVT_MEM_RES_FAIL";

        case ISIS_EVT_SYS_FAIL:
            return "ISIS_EVT_SYS_FAIL";

        case ISIS_EVT_REG_FAIL:
            return "ISIS_EVT_REG_FAIL";

        case ISIS_EVT_DEREG_FAIL:
            return "ISIS_EVT_DEREG_FAIL";

        case ISIS_EVT_TX_FAIL:
            return "ISIS_EVT_TX_FAIL";

        case ISIS_EVT_RX_FAIL:
            return "ISIS_EVT_RX_FAIL";

        case ISIS_EVT_FTM_DCOD_FAIL:
            return "ISIS_EVT_FTM_DCOD_FAIL";

        case ISIS_EVT_FTM_ABORT:
            return "ISIS_EVT_FTM_ABORT";

        case ISIS_EVT_RTM_UPDT_FAIL:
            return "ISIS_EVT_RTM_UPDT_FAIL";

        case ISIS_EVT_MIBII_LOOK_UP_FAIL:
            return "ISIS_EVT_MIBII_LOOK_UP_FAIL";

        case ISIS_EVT_DLL_CONN_FAIL:
            return "ISIS_EVT_DLL_CONN_FAIL";

        case ISIS_EVT_DLL_IOCTL_FAIL:
            return "ISIS_EVT_DLL_IOCTL_FAIL";

        case ISIS_EVT_INV_IFIDX:
            return "ISIS_EVT_INV_IFIDX";

        case ISIS_EVT_INV_PDU:
            return "ISIS_EVT_INV_PDU";

        case ISIS_EVT_DUP_SYS_DET:
            return "ISIS_EVT_DUP_SYS_DET";

        case ISIS_EVT_ID_LEN_MISMATCH:
            return "ISIS_EVT_ID_LEN_MISMATCH";

        case ISIS_EVT_AUTH_FAIL:
            return "ISIS_EVT_AUTH_FAIL";

        case ISIS_EVT_IS_UP:
            return "ISIS_EVT_IS_UP";

        case ISIS_EVT_WRONG_SYS_TYPE:
            return "ISIS_EVT_WRONG_SYS_TYPE";

        case ISIS_EVT_INCOMP_CKT:
            return "ISIS_EVT_INCOMP_CKT";

        case ISIS_EVT_AA_MISMATCH:
            return "ISIS_EVT_AA_MISMATCH";

        case ISIS_EVT_MAN_ADDR_CHANGE:
            return "ISIS_EVT_MAN_ADDR_CHANGE";

        case ISIS_EVT_MAX_AA_MISMATCH:
            return "ISIS_EVT_MAX_AA_MISMATCH";

        case ISIS_EVT_PROT_SUPP_CHANGE:
            return "ISIS_EVT_PROT_SUPP_CHANGE";

        case ISIS_EVT_IP_IF_ADDR_CHANGE:
            return "ISIS_EVT_IP_IF_ADDR_CHANGE";

        case ISIS_EVT_IPRA_CHANGE:
            return "ISIS_EVT_IPRA_CHANGE";

        case ISIS_EVT_SUMM_ADDR_CHANGE:
            return "ISIS_EVT_SUMM_ADDR_CHANGE";

        case ISIS_EVT_IS_DOWN:
            return "ISIS_EVT_IS_DOWN";

        case ISIS_EVT_IS_DESTROY:
            return "ISIS_EVT_IS_DESTROY";

        case ISIS_EVT_MAN_ADDR_DROPPED:
            return "ISIS_EVT_MAN_ADDR_DROPPED";

        case ISIS_EVT_CKT_CHANGE:
            return "ISIS_EVT_CKT_CHANGE";

        case ISIS_EVT_ADJ_CHANGE:
            return "ISIS_EVT_ADJ_CHANGE";

        case ISIS_EVT_REJECTED_ADJ:
            return "ISIS_EVT_REJECTED_ADJ";

        case ISIS_EVT_DIS_NOT_ELECTED:
            return "ISIS_EVT_DIS_NOT_ELECTED";

        case ISIS_EVT_DIS_CHANGE:
            return "ISIS_EVT_DIS_CHANGE";

        case ISIS_EVT_CHG_IN_DIS_STAT:
            return "ISIS_EVT_CHG_IN_DIS_STAT";

        case ISIS_EVT_PRIORITY_CHANGE:
            return "ISIS_EVT_PRIORITY_CHANGE";

        case ISIS_EVT_ELECT_DIS:
            return "ISIS_EVT_ELECT_DIS";

        case ISIS_EVT_CORR_LSP_DET:
            return "ISIS_EVT_CORR_LSP_DET";

        case ISIS_EVT_INV_LSP:
            return "ISIS_EVT_INV_LSP";

        case ISIS_EVT_LSP_DBOL:
            return "ISIS_EVT_LSP_DBOL";

        case ISIS_EVT_LSP_DBOL_RECOV:
            return "ISIS_EVT_LSP_DBOL_RECOV";

        case ISIS_EVT_OWN_LSP_PURGE:
            return "ISIS_EVT_OWN_LSP_PURGE";

        case ISIS_EVT_LSP_DB_VALIDATED:
            return "ISIS_EVT_LSP_DB_VALIDATED";

        case ISIS_EVT_MAX_SEQNO_EXCEED:
            return "ISIS_EVT_MAX_SEQNO_EXCEED";

        case ISIS_EVT_SEQ_NO_SKIP:
            return "ISIS_EVT_SEQ_NO_SKIP";

        case ISIS_EVT_TX_Q_FULL:
            return "ISIS_EVT_TX_Q_FULL";

        case ISIS_EVT_NEAR_L2_NOT_AVAIL:
            return "ISIS_EVT_NEAR_L2_NOT_AVAIL";

        case ISIS_EVT_L1_DECN_COMPLETE:
            return "ISIS_EVT_L1_DECN_COMPLETE";

        case ISIS_EVT_L2_DECN_COMPLETE:
            return "ISIS_EVT_L2_DECN_COMPLETE";

        case ISIS_EVT_ATT_STATUS:
            return "ISIS_EVT_ATT_STATUS";

        default:
            return "ISIS_EVT_ERROR";
    }
}

#ifdef ISIS_TRAPS

/*******************************************************************************
 * Function    : IsisCtrlCreateNotifyTable ()
 * Description : This routine fills the tIsisNotifytable structure which will
 *                hold the most recent objects of the trap 
 * Input(s)    : pTrapNotifyTable - Pointer to the notification Table
 *             : pu1Msg - Pointer to the Event Message
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : None
 ******************************************************************************/

PRIVATE VOID
IsisCtrlCreateNotifyTable (tIsisNotifyTable * pTrapNotifyTable, UINT1 *pu1Msg)
{

    MEMSET (pTrapNotifyTable, sizeof (tIsisNotifyTable), 0);

    switch (*pu1Msg)
    {
        case ISIS_EVT_LSP_DBOL:
            pTrapNotifyTable->u1SysLevel =
                ((tIsisEvtLSPDBOL *) pu1Msg)->u1Level;
            break;

        case ISIS_EVT_MAN_ADDR_DROPPED:
            break;

        case ISIS_EVT_CORR_LSP_DET:
            pTrapNotifyTable->u1SysLevel =
                ((tIsisEvtCorrLsp *) pu1Msg)->u1Level;
            MEMCPY (pTrapNotifyTable->au1TrapLSPId,
                    ((tIsisEvtCorrLsp *) pu1Msg)->au1TrapLspID, ISIS_LSPID_LEN);
            break;

        case ISIS_EVT_MAX_SEQNO_EXCEED:
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtSeqNoExceed *)
                                            pu1Msg)->u1Level;
            MEMCPY (pTrapNotifyTable->au1TrapLSPId,
                    ((tIsisEvtSeqNoExceed *) pu1Msg)->au1TrapLspID,
                    ISIS_LSPID_LEN);
            break;

        case ISIS_EVT_ID_LEN_MISMATCH:
            pTrapNotifyTable->u1FieldLen = ((tIsisEvtIDLenMismatch *)
                                            pu1Msg)->u1RxdIdLen;

            MEMCPY (pTrapNotifyTable->au1IsisPduFrag,
                    ((tIsisEvtIDLenMismatch *) pu1Msg)->pu1PduFrag,
                    ISIS_TRAP_PDU_FRAG_SIZE);

            break;

        case ISIS_EVT_MAX_AA_MISMATCH:
            pTrapNotifyTable->u1MaxAreaAddr = ((tIsisEvtMaxAAMismatch *)
                                               pu1Msg)->u1RxdMAA;
            MEMCPY (pTrapNotifyTable->au1IsisPduFrag,
                    ((tIsisEvtMaxAAMismatch *) pu1Msg)->pu1PduFrag,
                    ISIS_TRAP_PDU_FRAG_SIZE);
            break;

        case ISIS_EVT_OWN_LSP_PURGE:
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtOwnLspPurge *)
                                            pu1Msg)->u1SysLevel;
            MEMCPY (pTrapNotifyTable->au1TrapLSPId,
                    ((tIsisEvtOwnLspPurge *) pu1Msg)->au1TrapLspID,
                    ISIS_LSPID_LEN);
            break;

        case ISIS_EVT_SEQ_NO_SKIP:
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtSeqNoSkip *)
                                            pu1Msg)->u1SysLevel;
            MEMCPY (pTrapNotifyTable->au1TrapLSPId,
                    ((tIsisEvtSeqNoSkip *) pu1Msg)->au1TrapLspID,
                    ISIS_LSPID_LEN);
            break;

        case ISIS_EVT_AUTH_FAIL:
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtAuthFail *)
                                            pu1Msg)->u1SysLevel;
            MEMCPY (pTrapNotifyTable->au1IsisPduFrag,
                    ((tIsisEvtAuthFail *) pu1Msg)->pu1PduFrag,
                    ISIS_TRAP_PDU_FRAG_SIZE);
            break;

        case ISIS_EVT_VER_MISMATCH:
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtVerMismatch *)
                                            pu1Msg)->u1SysLevel;
            pTrapNotifyTable->u1ProtVersion = ((tIsisEvtVerMismatch *)
                                               pu1Msg)->u1ProtVer;
            MEMCPY (pTrapNotifyTable->au1IsisPduFrag,
                    ((tIsisEvtVerMismatch *) pu1Msg)->pu1PduFrag,
                    ISIS_TRAP_PDU_FRAG_SIZE);
            break;

        case ISIS_EVT_AA_MISMATCH:
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtAAMismatch *)
                                            pu1Msg)->u1SysLevel;
            pTrapNotifyTable->u2LSPSize = ((tIsisEvtAAMismatch *)
                                           pu1Msg)->u2LSPSize;
            MEMCPY (pTrapNotifyTable->au1IsisPduFrag,
                    ((tIsisEvtAAMismatch *) pu1Msg)->pu1PduFrag,
                    ISIS_TRAP_PDU_FRAG_SIZE);
            break;

        case ISIS_EVT_REJECTED_ADJ:
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtAdjRej *)
                                            pu1Msg)->u1SysLevel;

            break;

        case ISIS_EVT_LSP_TOO_LARGE:
            pTrapNotifyTable->u2LSPSize = ((tIsisEvtLspSizeTooLarge *)
                                           pu1Msg)->u2LSPSize;
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtLspSizeTooLarge *)
                                            pu1Msg)->u1SysLevel;
            MEMCPY (pTrapNotifyTable->au1TrapLSPId,
                    ((tIsisEvtLspSizeTooLarge *) pu1Msg)->au1TrapLspID,
                    ISIS_LSPID_LEN);
            break;

        case ISIS_EVT_LSP_BUFSIZE_MISMATCH:
            pTrapNotifyTable->u2BuffSize = ((tIsisEvtLspBuffSizeMismatch *)
                                            pu1Msg)->u2BuffSize;
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtLspBuffSizeMismatch *)
                                            pu1Msg)->u1SysLevel;
            MEMCPY (pTrapNotifyTable->au1TrapLSPId,
                    ((tIsisEvtLspBuffSizeMismatch *) pu1Msg)->au1TrapLspID,
                    ISIS_LSPID_LEN);
            break;

        case ISIS_EVT_PROT_SUPP_MISMATCH:
            MEMCPY (pTrapNotifyTable->au1ProtSupp,
                    ((tIsisEvtProtSuppMismatch *) pu1Msg)->au1UProt,
                    ISIS_MAX_PROTS_SUPP);
            pTrapNotifyTable->u1SysLevel = ((tIsisEvtProtSuppMismatch *)
                                            pu1Msg)->u1SysLevel;
            MEMCPY (pTrapNotifyTable->au1TrapLSPId,
                    ((tIsisEvtProtSuppMismatch *) pu1Msg)->au1TrapLspID,
                    ISIS_LSPID_LEN);

    }
    return;
}

#endif
