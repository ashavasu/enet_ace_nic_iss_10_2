/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isport.c,v 1.25 2016/04/29 09:11:16 siva Exp $
 *
 * Description: This file contains the portable routines
 *              Routines.
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/****************************************************************************
 * Function    : IsisRegWithExtMod ()
 * Description : The function registers with the external modules viz. RM
 *               and RTM and IP.
 * Input       : None
 * Output      : None
 * Globals     : None
 * Returns     : ISIS_SUCCESS on successful registration with external modules.
 *               ISIS_FAILURE on unsuccessful registration.
 ****************************************************************************/

PUBLIC INT4
IsisRegWithExtMod (tIsisSysContext * pContext)
{
    /* Register with ip for ip interface deletion indication. */
    if ((IsisRegisterWithIP (pContext) == ISIS_FAILURE) &&
        (FlashFileExists (ISIS_GR_CONF) == ISS_FAILURE))
    {
        RTP_PT ((ISIS_LGST, "IP <T> : IP Registration Failed !!!  () \n"));
        return ISIS_FAILURE;
    }

    /* Register with RTM */
    if ((IsisRegisterWithRtm (pContext) == ISIS_FAILURE) &&
        (FlashFileExists (ISIS_GR_CONF) == ISS_FAILURE))

    {
        IsisDeRegisterFromIP (pContext->u4SysInstIdx);
        RTP_PT ((ISIS_LGST, "RTM <T> : Rtm Registration Failed !!!  () \n"));
        return ISIS_FAILURE;
    }

    return ISIS_SUCCESS;
}

/****************************************************************************
 * Function    : IsisDeRegFromExtMod 
 * Description : The function de-registers from the external modules viz. RM
 *               and RTM.
 * Input       : None
 * Output      : None
 * Globals     : None
 * Returns     : ISIS_SUCCESS on successful deregistration with external modules.
 *               ISIS_FAILURE on unsuccessful deregistration.
 ****************************************************************************/

PUBLIC INT4
IsisDeRegFromExtMod (UINT1 u1IsisGRRestarterState, UINT4 u4SysInstIdx)
{

    if (u1IsisGRRestarterState != ISIS_GR_SHUTDOWN)
    {
        /* Deregister from IP */
        IsisDeRegisterFromIP (u4SysInstIdx);

        IsisDeRegisterFromRtm (u4SysInstIdx);
    }
    return ISIS_SUCCESS;
}

/****************************************************************************
 * Function    : IsisRegWithVcm ()
 * Description : The function registers with the external modules VCM.
 * Input       : None
 * Output      : None
 * Globals     : None
 * Returns     : ISIS_SUCCESS on successful registration with external modules.
 *               ISIS_FAILURE on unsuccessful registration.
 ****************************************************************************/

PUBLIC INT4
IsisRegWithVcm (VOID)
{
    tVcmRegInfo         VcmRegInfo;

    /* Register With VCM Module */
    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = IsisVcmCallbackFn;
    VcmRegInfo.u1InfoMask |= (VCM_IF_MAP_CHG_REQ | VCM_CXT_STATUS_CHG_REQ);
    VcmRegInfo.u1ProtoId = ISIS_ID;
    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        return ISIS_FAILURE;
    }
    return ISIS_SUCCESS;
}

/****************************************************************************
 * Function    : IsisDeRegFromVcm ()
 * Description : The function deregister from  external module VCM.
 * Input       : None
 * Output      : None
 * Globals     : None
 * Returns     : ISIS_SUCCESS on successful registration with external modules.
 *               ISIS_FAILURE on unsuccessful registration.
 ****************************************************************************/

PUBLIC INT4
IsisDeRegFromVcm (VOID)
{
    /*De register from VCM */
    if (VcmDeRegisterHLProtocol (ISIS_ID) == VCM_FAILURE)
    {
        return ISIS_FAILURE;
    }
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisEnqueueMessage ()
 * Description : This routine enqueues the given message to the specified queue.
 *               The routine allocates a chain buffer and copies the given data
 *               into the chain and then queues the message to the specified
 *               queeue. If the target supports queues which can hold variable
 *               length messages, then the routine can directly post the message
 *               to the queue without copying to the chain buffer
 * Input       : pu1Msg      - Pointer to the buffer which is to be enqueued
 *               u4Length    - Length of the buffer to be enqueued
 *               QId         - Queue Name where the message is posted
 *               u4Event     - Event which is posted to indicate the receiving
 *                             task about the message posting
 *               u1BufType   - The type of buffer used for releasing the given
 *                             message back to the free pool             
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC INT4
IsisEnqueueMessage (UINT1 *pu1Msg, UINT4 u4Length, tOsixQId QId, UINT4 u4Event)
{
    tCRU_BUF_CHAIN_HEADER *pChainBuf = NULL;

    pChainBuf = CRU_BUF_Allocate_MsgBufChain (u4Length, 0);
    if (pChainBuf == NULL)
    {
        return ISIS_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pChainBuf, pu1Msg, 0, u4Length);

    if (OsixQueSend (QId, (UINT1 *) &pChainBuf, OSIX_DEF_MSG_LEN) ==
        OSIX_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pChainBuf, TRUE);
        return ISIS_FAILURE;
    }

    if (OsixEvtSend (gIsisTaskId, u4Event) == OSIX_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pChainBuf, TRUE);
        return ISIS_FAILURE;
    }

    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisCopyFromDLLBuf ()
 * Description : This routine copies the data received from the Data Link Layers
 *               to the given data buffer which is used by ISIS for further 
 *               procesing
 * Input       : pIsisMsg    - Pointer to the encapsulating Interface  message
 *               pu1Data     - Pointer to the Buffer to hold copied data
 *               u4DataLen   - Length of the Data 
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisCopyFromDLLBuf (tIsisMsg * pIsisMsg, UINT1 *pu1Data, UINT4 u4DataLen)
{
    /* Current implementation assumes that DLL buffers will be carried in CRU
     * buffers. If the target utilises a different strategy, then the 
     * routine should be modified to copy the data appropriately.
     */

    /* Before copying check whether the data includes Ethernet headers. If
     * present we may have to skip 17 bytes (14 bytes of Ethernet header + 3
     * bytes of LLC header
     */

    if (pIsisMsg->u1DataType == ISIS_DATA_NORMAL)
    {
        /* Normal Data - case where data starts from '0' byte offset. */
        CRU_BUF_Copy_FromBufChain ((tOsixMsg *) (VOID *) pIsisMsg->pu1Msg,
                                   pu1Data, 0, u4DataLen);
    }
    else if (pIsisMsg->u1DataType == ISIS_DATA_RAW)
    {
        /* Raw Data - case where actual data starts from '17' byte offset. */
        CRU_BUF_Copy_FromBufChain ((tOsixMsg *) (VOID *) pIsisMsg->pu1Msg,
                                   pu1Data, ISIS_RAW_DATA_HDR_LEN, u4DataLen);
    }
}

/*******************************************************************************
 * Function    : IsisFreeDLLBuf ()
 * Description : This routine frees the buffer received from the DLL module.
 *               Since data can come in two paths, one directly from the DLL
 *               module and the other viz Back Plane Communication interface,
 *               the routine checks the pMemFreeRoutine () in the received
 *               message and releases the DLL buffer appropriately. 
 *               the given buffer
 * Input       : pIfMsg      - Pointer to the received message
 *               u2PDULen    - Length of the PDU 
 *               pu1PDU      - Pointer to the PDU
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisFreeDLLBuf (tIsisMsg * pIfMsg)
{
    if (pIfMsg->pMemFreeRoutine != NULL)
    {
        /* The buffer is received from the DLL module directly */
        if (pIfMsg->pu1Msg != NULL)
        {
            (pIfMsg->pMemFreeRoutine) (pIfMsg->pu1Msg);
        }
    }
    pIfMsg->pu1Msg = NULL;
}

/*******************************************************************************
 * Function    : IsisGetDataFromQBuf ()
 * Description : This routine retrieves the Data from the Queue Buffer. If the
 *               Length is specified, it retrieves only the requested data, else
 *               it retrieves entire data.
 * Input       : pQMsg       - Pointer to the Queue message
 *               pu4Length   - Length of the Data to be retrieved 
 * Output      : pu1Data     - Pointer to the retrieved Data
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisGetDataFromQBuf (tIsisQBuf * pQMsg, UINT1 **pu1Data, UINT4 *pu4Length)
{
    /* If Length not specified copy the entire buffer and return the length */
    if (*pu4Length == 0)
    {
        *pu4Length = CRU_BUF_Get_ChainValidByteCount (pQMsg);
        CRU_BUF_Copy_FromBufChain (pQMsg, *pu1Data, 0, *pu4Length);
        ISIS_MEM_FREE (ISIS_BUF_CHNS, (UINT1 *) pQMsg);
        pQMsg = NULL;
    }
    else
    {
        /* Length specified, copy only what is asked for */
        CRU_BUF_Copy_FromBufChain (pQMsg, *pu1Data, 0, *pu4Length);
    }
}

/*******************************************************************************
 * Function    : IsisUpdateIfStatus ()
 * Description : This routine Updates the status of the Interface to ISIS.
 *               If the Interface status is UP then, it gives Interface UP event
 *               and sends the Interface related information.
 * Input       : u4IfIndex    - The Index of the Interface
 *               u4IfSubIndex - The Subindex of the Interface
 * Output      : NONE
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdateIfStatus (tIsisCktEntry * pCktRec)
{
    tIsisMsg            IsisMsg;
    tIsisLLInfo        *pLLInfo = NULL;
    tNetIpv6AddrInfo    ip6AddrInfo;
    tNetIpv6AddrInfo    ip6NextAddrInfo;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    UINT4               u4Port = 0;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               au1IpV4Addr[ISIS_MAX_IPV4_ADDR_LEN];
    UINT1               au1IpV6Addr[ISIS_MAX_IPV6_ADDR_LEN];
    UINT4               u4Offset = 0;
    UINT1              *pu1Ipv6Addr = NULL;

    pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                              sizeof (tIsisLLInfo));
    if (pLLInfo == NULL)
    {
        return;
    }

    i4RetVal = IsisGetIfInfo (pCktRec->u4CktIfIdx, pLLInfo);
    MEMSET (au1IpV4Addr, 0x00, ISIS_MAX_IPV4_ADDR_LEN);

    MEMSET (au1IpV6Addr, 0x00, ISIS_MAX_IPV6_ADDR_LEN);
    MEMSET (pLLInfo->au1IpV6Addr, 0x00, (ISIS_MAX_IPV6_ADDR_LEN * 3));
    MEMSET ((UINT1 *) &NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));

    if ((i4RetVal == ISIS_SUCCESS) && (pLLInfo->u1OperState == ISIS_UP))
    {

        if (((MEMCMP
              (pCktRec->au1IPV4Addr, au1IpV4Addr,
               ISIS_MAX_IPV4_ADDR_LEN) != 0)
             &&
             (MEMCMP
              (pCktRec->au1IPV6Addr, au1IpV6Addr,
               ISIS_MAX_IPV6_ADDR_LEN) != 0)))
        {
            pLLInfo->u1AddrType = ISIS_ADDR_IPVX;
            MEMCPY (pLLInfo->au1IpV4Addr, pCktRec->au1IPV4Addr, ISIS_MAX_IPV4_ADDR_LEN);
            if (NetIpv4GetPortFromIfIndex (pCktRec->u4CktIfIdx, &u4Port) ==
                NETIPV4_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
                pLLInfo = NULL;
                return;
            }
            if (NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) == NETIPV4_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
                pLLInfo = NULL;
                return;
            }
            NetIpv4IfInfo.u4NetMask = OSIX_HTONL (NetIpv4IfInfo.u4NetMask);
            MEMCPY (pLLInfo->au1IpMask, &NetIpv4IfInfo.u4NetMask,
                    sizeof (UINT4));

            pu1Ipv6Addr = pLLInfo->au1IpV6Addr;
            if (NetIpv6GetFirstIfAddr (pCktRec->u4CktIfIdx, &(ip6AddrInfo)) ==
                NETIPV6_SUCCESS)
            {
                /*Copy the Ipv6 link local address for the given circuit */
                MEMCPY (pu1Ipv6Addr, pCktRec->au1IPV6Addr,
                        ISIS_MAX_IPV6_ADDR_LEN);

                pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
                u4Offset += ISIS_MAX_IPV6_ADDR_LEN;
                /*Copy the primary Ipv6 address for the given circuit index */
                MEMCPY (pu1Ipv6Addr,
                        (UINT1 *) &(ip6AddrInfo.Ip6Addr),
                        ISIS_MAX_IPV6_ADDR_LEN);

                pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
                u4Offset += ISIS_MAX_IPV6_ADDR_LEN;
                while (u4Offset !=
                       (ISIS_MAX_IPV6_ADDR_LEN * ISIS_MAX_IPV6_ADDRESSES))
                {
                    if (NetIpv6GetNextIfAddr (pCktRec->u4CktIfIdx,
                                              &ip6AddrInfo,
                                              &(ip6NextAddrInfo)) ==
                        NETIPV6_SUCCESS)
                    {
                        MEMCPY (pu1Ipv6Addr,
                                (UINT1 *) &(ip6NextAddrInfo.Ip6Addr),
                                ISIS_MAX_IPV6_ADDR_LEN);
                        pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
                        u4Offset += ISIS_MAX_IPV6_ADDR_LEN;

                    }
                    else
                    {
                        break;
                    }
                    ip6AddrInfo = ip6NextAddrInfo;
                }
            }
        }
        else if (MEMCMP
                 (pCktRec->au1IPV4Addr, au1IpV4Addr,
                  ISIS_MAX_IPV4_ADDR_LEN) != 0)
        {
            MEMCPY (pLLInfo->au1IpV4Addr, pCktRec->au1IPV4Addr, ISIS_MAX_IPV4_ADDR_LEN);
            pLLInfo->u1AddrType = ISIS_ADDR_IPV4;
            if (NetIpv4GetPortFromIfIndex (pCktRec->u4CktIfIdx, &u4Port) ==
                NETIPV4_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
                pLLInfo = NULL;
                return;
            }
            if (NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) == NETIPV4_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
                pLLInfo = NULL;
                return;
            }
            NetIpv4IfInfo.u4NetMask = OSIX_HTONL (NetIpv4IfInfo.u4NetMask);
            MEMCPY (pLLInfo->au1IpMask, &NetIpv4IfInfo.u4NetMask,
                    sizeof (UINT4));
        }

        else if (MEMCMP
                 (pCktRec->au1IPV6Addr, au1IpV6Addr,
                  ISIS_MAX_IPV6_ADDR_LEN) != 0)
        {
            pu1Ipv6Addr = pLLInfo->au1IpV6Addr;
            if (NetIpv6GetFirstIfAddr (pCktRec->u4CktIfIdx, &(ip6AddrInfo)) ==
                NETIPV6_SUCCESS)
            {
                /*Copy the Ipv6 link local address for the given circuit */
                MEMCPY (pu1Ipv6Addr, pCktRec->au1IPV6Addr,
                        ISIS_MAX_IPV6_ADDR_LEN);

                pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
                u4Offset += ISIS_MAX_IPV6_ADDR_LEN;
                /*Copy the primary Ipv6 address for the given circuit index */
                MEMCPY (pu1Ipv6Addr,
                        (UINT1 *) &(ip6AddrInfo.Ip6Addr),
                        ISIS_MAX_IPV6_ADDR_LEN);

                pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
                u4Offset += ISIS_MAX_IPV6_ADDR_LEN;
                while (u4Offset !=
                       (ISIS_MAX_IPV6_ADDR_LEN * ISIS_MAX_IPV6_ADDRESSES))
                {
                    if (NetIpv6GetNextIfAddr (pCktRec->u4CktIfIdx,
                                              &ip6AddrInfo,
                                              &(ip6NextAddrInfo)) ==
                        NETIPV6_SUCCESS)
                    {
                        MEMCPY (pu1Ipv6Addr,
                                (UINT1 *) &(ip6NextAddrInfo.Ip6Addr),
                                ISIS_MAX_IPV6_ADDR_LEN);
                        pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
                        u4Offset += ISIS_MAX_IPV6_ADDR_LEN;

                    }
                    else
                    {
                        break;
                    }
                    ip6AddrInfo = ip6NextAddrInfo;
                }
                pLLInfo->u1AddrType = ISIS_ADDR_IPV6;
            }
        }
        if ((pLLInfo->u1AddrType == ISIS_ADDR_IPV6)
            || (pLLInfo->u1AddrType == ISIS_ADDR_IPVX))
        {
            if (NetIpv6GetFirstIfAddr (pCktRec->u4CktIfIdx, &(ip6AddrInfo)) ==
                NETIPV6_SUCCESS)
            {
                pLLInfo->u1IpV6PrefixLen = (UINT1) ip6AddrInfo.u4PrefixLength;
            }

        }
        IsisMsg.u4Length = sizeof (tIsisLLInfo);
        IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
        IsisMsg.u1MsgType = ISIS_MSG_IF_STAT_IND;
        IsisMsg.u1DataType = 0;
        IsisMsg.pMemFreeRoutine = NULL;
        IsisMsg.u1LenOrStat = ISIS_STATE_ON;
        IsisMsg.u4CxtOrIfindex = pCktRec->u4LLHandle;

        if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                gIsisCtrlQId,
                                ISIS_SYS_LL_DATA_EVT) == ISIS_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
            pLLInfo = NULL;
        }
    }
    else
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
        pLLInfo = NULL;
    }
    return;
}

/*******************************************************************************
 * Function    : IsisGetIfInfo ()
 * Description : This routine retrieves the Interface Infomation. 
 * Input       : u4IfIndex  - The Index of the Interface
 *               pIfInfo    - The pointer to Interface infomation
 * Output      : NONE
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS if Interface exist, else
 *               ISIS_FAILURE
 ******************************************************************************/

PUBLIC INT4
IsisGetIfInfo (UINT4 u4IfIndex, tIsisLLInfo * pIfInfo)
{
    tIsisIfInfo         IfInfo;

    MEMSET ((UINT1 *) &IfInfo, 0, sizeof (tIsisIfInfo));

    if (ISIS_GET_IF_INFO ((UINT2) u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return ISIS_FAILURE;
    }

    pIfInfo->u1IfType = IfInfo.u1IfType;
    pIfInfo->u1OperState = IfInfo.u1IfOperStatus;
    pIfInfo->u4Speed = IfInfo.u4IfSpeed;
    pIfInfo->u4Mtu = IfInfo.u4IfMtu;
    MEMCPY (pIfInfo->au1SNPA, IfInfo.au1MacAddr, ISIS_SNPA_ADDR_LEN);

    return ISIS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : IsisRmGetNodeState                                        */
/*                                                                          */
/* Description  : This  function is called by the protocol to get the       */
/*                current state in the RM module                            */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : RM node state                                             */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
IsisRmGetNodeState (VOID)
{
    INT4                i4RmState = RM_ACTIVE;

#ifdef RM_WANTED
    i4RmState = (INT4)RmGetNodeState ();
#endif

    return i4RmState;
}

#ifdef RM_WANTED
/*****************************************************************************/
/* Function Name      : IsisRmEnqChkSumMsgToRm                               */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISIS_SUCCESS/ISIS_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IsisRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef L2RED_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return ISIS_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    return ISIS_SUCCESS;
}
#endif

#ifdef BFD_WANTED
/**************************************************************************/
/* Function Name : IsisRegisterWithBfd                                    */
/*                                                                        */
/* Description   : This function is to call the BFD API to register with  */
/*                 BFD module to monitor the ISIS neighbor IP path        */
/* Inputs        : u4SysInstance - System Instance                        */
/*                 pAdjEntry  - Pointer to the ISIS adjacent entry        */
/*                 pCktEntry  - Pointer to the ISIS circuit entry         */
/*                 u1MtId - MT ID for which BFD registration is reqd      */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : ISIS_SUCCESS/ISIS_FAILURE                              */
/**************************************************************************/
PUBLIC INT4
IsisRegisterWithBfd (UINT4 u4SysInstance, tIsisCktEntry * pCktEntry,
                     tIsisAdjEntry * pAdjEntry, UINT1 u1MtId)
{
    static tBfdReqParams BfdInParams;
    UINT1                au1NullIP[ISIS_MAX_IP_ADDR_LEN];
    UINT4                u4Addr = 0;
    UINT1                au1EventMask[1] = { 0 };
    if (IsisRmGetNodeState () == RM_STANDBY)
    {
	    return ISIS_SUCCESS;
    }

    MEMSET (&BfdInParams, 0, sizeof (tBfdReqParams));
    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);

    if ((pCktEntry == NULL) || (pAdjEntry == NULL))
    {
        return ISIS_FAILURE;
    }

    /* Filling ISIS path info for Registering with BFD */
    BfdInParams.u4ReqType = BFD_CLIENT_REGISTER_FOR_IP_PATH_MONITORING;
    BfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_ISIS;
    BfdInParams.u4ContextId = u4SysInstance;

    if (u1MtId == ISIS_IPV4_UNICAST_MT_ID)
    {
        if ((MEMCMP (pAdjEntry->AdjNbrIpV4Addr.au1IpAddr, 
                     au1NullIP, ISIS_MAX_IPV4_ADDR_LEN) == 0) ||
            (MEMCMP (pCktEntry->au1IPV4Addr, 
                     au1NullIP, ISIS_MAX_IPV4_ADDR_LEN) == 0))
        {
            return ISIS_FAILURE;
        }
        BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType = ISIS_ADDR_IPV4;
        MEMCPY (&u4Addr, pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                ISIS_MAX_IPV4_ADDR_LEN);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                &u4Addr, ISIS_MAX_IPV4_ADDR_LEN);

        MEMCPY (&u4Addr, pCktEntry->au1IPV4Addr,
                ISIS_MAX_IPV4_ADDR_LEN);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                &u4Addr, ISIS_MAX_IPV4_ADDR_LEN);

    }
    else if (u1MtId == ISIS_IPV6_UNICAST_MT_ID)
    {
        if ((MEMCMP (pAdjEntry->AdjNbrIpV6Addr.au1IpAddr, 
                     au1NullIP, ISIS_MAX_IPV6_ADDR_LEN) == 0) ||
            (MEMCMP (pCktEntry->au1IPV6Addr, 
                     au1NullIP, ISIS_MAX_IPV6_ADDR_LEN) == 0))
        {
            return ISIS_FAILURE;
        }
        BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType = ISIS_ADDR_IPV6;
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                ISIS_MAX_IPV6_ADDR_LEN);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                pCktEntry->au1IPV6Addr,
                ISIS_MAX_IPV6_ADDR_LEN);
    }
    else
    {
        WARNING ((ISIS_LGST, "BFD Registration Failed - Erroneous"
                                 " MT ID  received () \n"));
        return ISIS_FAILURE;
    }

    BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex = 
                                 pCktEntry->u4CktIfIdx;

    /* Multihop sessions are not supported in ISIS */
    BfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;

    /* The Desired detection timer is not configurable. hence making
     * this value as default detection multipler as BFD 3Sec*/
    BfdInParams.BfdClntInfoParams.u4DesiredDetectionTime = BFD_DEFAULT_TIME_OUT;

    /* Since we need both the BFD status UP, DOWN & ADMIN DOWN notification,
     * both are set in the event mask.
     * OSIX_BITLIST_SET_BIT utility will not check the bit against Zero,
     * Hence the bit + 1 used to set/check the bit */
    OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_DOWN + 1), 1);

    /* Register for BFD Session UP notification only if Multi-topology
     * is enabled */
    if (pCktEntry->pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_UP + 1), 1);
    }

    MEMCPY (&(BfdInParams.BfdClntInfoParams.u1EventMask), au1EventMask, 1);

    BfdInParams.BfdClntInfoParams.pBfdNotifyPathStatusChange =
        IsisHandleAdjPathStatusChange;

    if (u1MtId == ISIS_IPV4_UNICAST_MT_ID)
    {
        pAdjEntry->u1Mt0BfdRegd = ISIS_BFD_TRUE;
    }
    else
    {
        pAdjEntry->u1Mt2BfdRegd = ISIS_BFD_TRUE;
    }

    /* BFD API call for registering ISIS Nbr path monitoring */
    if (BfdApiHandleExtRequest (u4SysInstance, &BfdInParams,
                                NULL) != OSIX_SUCCESS)
    {
        if (u1MtId == ISIS_IPV4_UNICAST_MT_ID)
        {
            pAdjEntry->u1Mt0BfdRegd = ISIS_BFD_FALSE;
        }
        else
        {
            pAdjEntry->u1Mt2BfdRegd = ISIS_BFD_FALSE;
        }
        WARNING ((ISIS_LGST, "BFD Registration Failed \n"));
        return ISIS_FAILURE;
    }

    return ISIS_SUCCESS;
}

/**************************************************************************/
/* Function Name : IsisDeRegisterWithBfd                                  */
/*                                                                        */
/* Description   : This function is to call the BFD API to Deregister or  */
/*                 Disable with BFD module for the ISIS neighbor IP path  */
/*                 which was already registered                           */
/* Inputs        : u4SysInstance - System Instance                        */
/*                 pAdjEntry  - Pointer to the ISIS Adjacent entry        */
/*                 pCktEntry  - Pointer to the ISIS Circuit entry         */
/*                 u1MtId - MT ID for which BFD de-registration is reqd   */
/*                 u1Flag - Flag to decide whether to de-register/disable */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : ISIS_SUCCESS/ISIS_FAILURE                              */
/**************************************************************************/
PUBLIC INT4
IsisDeRegisterWithBfd (UINT4 u4SysInstance, tIsisCktEntry * pCktEntry,
                       tIsisAdjEntry * pAdjEntry,
                       UINT1 u1MtId, UINT1 u1Flag)
{
    static tBfdReqParams BfdInParams;
    UINT4                u4Addr = 0;
    UINT1                au1NullIP[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (&BfdInParams, 0, sizeof (tBfdReqParams));
    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);

    if ((pCktEntry == NULL) || (pAdjEntry == NULL))
    {
        return ISIS_FAILURE;
    }
    if (IsisRmGetNodeState () == RM_STANDBY)
    {
	    return ISIS_SUCCESS;
    }


    pAdjEntry->u1DoNotUpdHoldTmr = ISIS_BFD_FALSE;

    if (u1Flag == ISIS_TRUE)
    {
        BfdInParams.u4ReqType = BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING;
    }
    else
    {
        BfdInParams.u4ReqType = BFD_CLIENT_DISABLE_FROM_IP_PATH_MONITORING;
    }

    BfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_ISIS;
    BfdInParams.u4ContextId = u4SysInstance;

    if (u1MtId == ISIS_IPV4_UNICAST_MT_ID)
    {
        if ((MEMCMP (pAdjEntry->AdjNbrIpV4Addr.au1IpAddr, 
                     au1NullIP, ISIS_MAX_IPV4_ADDR_LEN) == 0) ||
            (MEMCMP (pCktEntry->au1IPV4Addr, 
                     au1NullIP, ISIS_MAX_IPV4_ADDR_LEN) == 0))
        {
            return ISIS_FAILURE;
        }
        BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType = ISIS_ADDR_IPV4;
        MEMCPY (&u4Addr, pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                ISIS_MAX_IPV4_ADDR_LEN);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                &u4Addr, ISIS_MAX_IPV4_ADDR_LEN);

        MEMCPY (&u4Addr, pCktEntry->au1IPV4Addr,
                ISIS_MAX_IPV4_ADDR_LEN);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                &u4Addr, ISIS_MAX_IPV4_ADDR_LEN);
    }
    else if (u1MtId == ISIS_IPV6_UNICAST_MT_ID)
    {
        if ((MEMCMP (pAdjEntry->AdjNbrIpV6Addr.au1IpAddr, 
                     au1NullIP, ISIS_MAX_IPV6_ADDR_LEN) == 0) ||
            (MEMCMP (pCktEntry->au1IPV6Addr, 
                     au1NullIP, ISIS_MAX_IPV6_ADDR_LEN) == 0))
        {
            return ISIS_FAILURE;
        }
        BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType = ISIS_ADDR_IPV6;
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                ISIS_MAX_IPV6_ADDR_LEN);
        MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                pCktEntry->au1IPV6Addr,
                ISIS_MAX_IPV6_ADDR_LEN);
    }
    else
    {
        return ISIS_FAILURE;
    }

    BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex = 
                                            pCktEntry->u4CktIfIdx;

    /* Multihop sessions are not supported in ISIS */
    BfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;

    /* BFD API call for De-registering for the ISIS Nbr path monitoring */
    if (BfdApiHandleExtRequest (u4SysInstance, &BfdInParams,
                                NULL) != OSIX_SUCCESS)
    {
        return ISIS_FAILURE;
    }

    if (u1MtId == ISIS_IPV4_UNICAST_MT_ID)
    {
        pAdjEntry->u1Mt0BfdRegd = ISIS_BFD_FALSE;
        pAdjEntry->u1IsisMT0BfdState = ISIS_BFD_FALSE;
    }
    else
    {
        pAdjEntry->u1Mt2BfdRegd = ISIS_BFD_FALSE;
        pAdjEntry->u1IsisMT2BfdState = ISIS_BFD_FALSE;
    }

    return ISIS_SUCCESS;
}

/**************************************************************************/
/* Function Name : IsisHandleAdjPathStatusChange                          */
/*                                                                        */
/* Description   : This function is the call back function to indicate    */
/*                 BFD session UP/DOWN notification to ISIS.              */
/* Inputs        : u4ContextId - Context Identifier                       */
/*                 pNbrPathInfo - Neighbor Path Info monitored by BFD     */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : BGP4_SUCCESS/BGP4_FAILURE                              */
/**************************************************************************/
PUBLIC INT1
IsisHandleAdjPathStatusChange (UINT4 u4ContextId,
                               tBfdClientNbrIpPathInfo * pNbrPathInfo)
{
    tIsisMsg              IsisMsg;
    tIsisIpAddr          *pIpAddr;
    tIsisSysContext         *pContext = NULL;
    INT4                  i4RetVal = OSIX_FAILURE;

    MEMSET (&IsisMsg, 0, sizeof (tIsisMsg));

    if (gu1IsisStatus != ISIS_IS_UP)
    {
        /* ISIS task not initialised */
        return OSIX_FAILURE;
    }

    i4RetVal = IsisCtrlGetSysContext (u4ContextId, &pContext);
    if (i4RetVal == ISIS_FAILURE)
    {
        /* The context does not exist */
        return OSIX_FAILURE;
    }

    /* For single topology, ISIS should process only the session DOWN notification
     * from BFD.
     * For multi-topology, ISIS should process both session UP & DOWN notification
     * from BFD */
    if (((pContext->u1IsisMTSupport != ISIS_TRUE) &&
        (pNbrPathInfo->u4PathStatus != BFD_SESS_STATE_DOWN)) ||
        ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
        (pNbrPathInfo->u4PathStatus != BFD_SESS_STATE_DOWN) &&
        (pNbrPathInfo->u4PathStatus != BFD_SESS_STATE_ADMIN_DOWN) &&
        (pNbrPathInfo->u4PathStatus != BFD_SESS_STATE_UP)))
    {
        return OSIX_FAILURE;
    }

    /* Fetching the Adjacency record for given Destination IP */
    pIpAddr = (tIsisIpAddr *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                              sizeof (tIsisIpAddr));
    if (pIpAddr == NULL)
    {
        return OSIX_FAILURE;
    }

    pIpAddr->u1AddrType = pNbrPathInfo->u1AddrType;
    pIpAddr->u1Length = (pNbrPathInfo->u1AddrType == ISIS_ADDR_IPV4) ? 
                        ISIS_MAX_IPV4_ADDR_LEN : ISIS_MAX_IPV6_ADDR_LEN;
    MEMCPY (pIpAddr->au1IpAddr, &(pNbrPathInfo->NbrAddr), pIpAddr->u1Length);

    IsisMsg.pu1Msg = (UINT1 *) pIpAddr;
    IsisMsg.u4Length = sizeof (tIsisIpAddr);
    IsisMsg.u1LenOrStat = (UINT1) pNbrPathInfo->u4PathStatus;
    IsisMsg.u1MsgType = ISIS_MSG_BFD_SESSION_STATE_NOTIFICATION;
    IsisMsg.u4CxtOrIfindex = u4ContextId;
    IsisMsg.u4IfSubIdx = pNbrPathInfo->u4IfIndex;

    i4RetVal = IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
            gIsisCtrlQId, ISIS_SYS_BFD_STATE_EVT);
    if (i4RetVal != ISIS_SUCCESS)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, IsisMsg.pu1Msg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
#endif
#ifdef ISS_WANTED
/****************************************************************************
 * Function    : IsisRegWithIss ()
 * Description : The function registers with the external modules ISS.
 * Input       : None
 * Output      : None
 * Globals     : None
 * Returns     : None.
 ****************************************************************************/

PUBLIC VOID
IsisRegWithIss (VOID)
{
    tIssRegInfo         IssRegInfo;

    /* Register With ISIS Module */
    MEMSET ((UINT1 *) &IssRegInfo, 0, sizeof (tIssRegInfo));

    IssRegInfo.pHostNmeChng = IsisSendHostNmeUpdateMsg;
    IssRegInfo.u1ProtoId = ISIS_ID;
    IssRegInfo.u1InfoMask |= ISS_HOSTNME_CHG_REQ;
    IssRegisterProtocol (&IssRegInfo); 
}

/******************************************************************************
 * Function Name : IsisDeRegWithIss 
 * Description   : This function de registers the ISIS Module from ISS
 * Input (s)     : None
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/
PUBLIC VOID
IsisDeRegWithIss ()
{
    IssDeRegisterProtocol ();
}
#endif
/****************************************************************************
 * Function    : IsisHostNmeHdlr ()
 * Description : The function registers with the external modules ISS.
 * Input       : None
 * Output      : None
 * Globals     : None
 * Returns     : 
 ****************************************************************************/

PUBLIC VOID
IsisHostNmeHdlr (tIsisMsg * pBuf)
{
    UINT4               u4Status = 0;
    UINT1               au1HstNme[ISIS_HSTNME_LEN];
    UINT1               au1NewHstNme[ISIS_HSTNME_LEN];
    tIsisSysContext    *pContext;
    tIsisMDT           *pMDT = NULL;

    pContext = ISIS_GET_CONTEXT ();

    if ((pContext == NULL) || (pBuf == NULL) || (pBuf->pu1Msg == NULL))
    {
        UPP_EE ((ISIS_LGST,
                 "UPP <X> : Failed to get the context \n"
                 "UPP <X> : Exiting IsisHostNmeHdlr()\n"));
        return;
    }
    MEMSET (au1HstNme, 0, ISIS_HSTNME_LEN);
    ISIS_GET_HSTNME (au1HstNme);
    /*** copy status to offset 0 ***/
    CRU_BUF_Copy_FromBufChain ((tOsixMsg *) (VOID *) pBuf->pu1Msg,
                               (UINT1 *) &u4Status, 0, sizeof (u4Status));
    /*** copy map name to offset 4 ***/
    CRU_BUF_Copy_FromBufChain ((tOsixMsg *) (VOID *) pBuf->pu1Msg, au1NewHstNme,
                               sizeof (u4Status), ISIS_HSTNME_LEN);
    if (STRLEN (au1NewHstNme) <= 0)
    {
        if (STRLEN (au1HstNme) > 0)
        {
            u4Status = ISIS_CMD_DELETE;
        }
        else
        {
            UPP_EE ((ISIS_LGST,
                     "UPP <X> : No change in hostname\n"
                     "UPP <X> : Exiting IsisHostNmeHdlr()\n"));
            return;
        }
    }
    else
    {
        if (STRLEN (au1HstNme) <= 0)
        {
            u4Status = ISIS_CMD_ADD;
        }
        else if (MEMCMP (au1HstNme, au1NewHstNme, ISIS_HSTNME_LEN) != 0)
        {
            u4Status = ISIS_CMD_MODIFY;
        }
        else
        {
            UPP_EE ((ISIS_LGST,
                     "UPP <X> : No change in hostname\n"
                     "UPP <X> : Exiting IsisHostNmeHdlr()\n"));
            return;
        }
    }
    ISIS_SET_HSTNME (au1NewHstNme);
    while (pContext != NULL)
    {
        /* Scan through all contexts, update status */
        if (pContext->u1IsisDynHostNmeSupport == ISIS_DYNHOSTNME_ENABLE)
        {
            IsisUpdHostNmeTable (pContext, pContext->SysActuals.au1SysID,
                                 au1NewHstNme, ISIS_CMD_ADD, 0);
            IsisHostNmeMDT (pContext, pMDT, (UINT1) u4Status);
        }
        pContext = pContext->pNext;
    }
    UPP_EE ((ISIS_LGST, "UPP <X> : Exiting IsisHostNmeHdlr()\n"));
    return;
}

/*****************************************************************************/
/* Function     : IsisSendHostNmeUpdateMsg                                  */
/*                                                                           */
/* Description  : This function processes Host name Update Message from      */
/*                ISS module, this function posts an event to ISIS           */
/*                                                                           */
/* Input        : pu1HstName   - Host Name                                   */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : ISIS_SUCCESS if host name update Msg Sent to ISIS          */
/*                successfully                                               */
/*                ISIS_FAILURE otherwise                                     */
/*****************************************************************************/
PUBLIC INT4
IsisSendHostNmeUpdateMsg (UINT1 *pu1HstName)
{
    tIsisMsg            IsisMsg;
    tIsisQBuf          *pBuf = NULL;
    UINT1               au1HstNme[ISIS_HSTNME_LEN];
    UINT4               u4Size = 0;
    UINT4               u4Status = 0;

    UPP_EE ((ISIS_LGST, "UPP <X> : Exiting IsisSendHostNmeUpdateMsg()\n"));
    u4Size = sizeof (u4Status) + ISIS_HSTNME_LEN;
    MEMSET (au1HstNme, 0, ISIS_HSTNME_LEN);
    STRNCPY (au1HstNme, pu1HstName, MEM_MAX_BYTES(STRLEN (pu1HstName),ISIS_HSTNME_LEN));

    pBuf = (tIsisQBuf *) ISIS_MEM_ALLOC (ISIS_BUF_CHNS, u4Size);
    if (pBuf == NULL)
    {
        UPP_EE ((ISIS_LGST,
                 "UPP <X> : Failed to allocate buffer to dynamic hostname message\n"
                 "UPP <X> : Exiting IsisSendHostNmeUpdateMsg()\n"));

        return ISIS_FAILURE;
    }

    /*** copy status to offset 0 ***/
    ISIS_COPY_TO_CHAIN_BUF (pBuf, (UINT1 *) &u4Status, 0, sizeof (u4Status));

    /*** copy status to offset 4 ***/
    ISIS_COPY_TO_CHAIN_BUF (pBuf, au1HstNme, sizeof (u4Status),
                            ISIS_HSTNME_LEN);

    IsisMsg.u4Length = u4Size;
    IsisMsg.pu1Msg = (UINT1 *) pBuf;
    IsisMsg.u1MsgType = ISIS_EVT_HOST_NME_CHANGE;

    if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                            gIsisCtrlQId, ISIS_SYS_INT_EVT) == ISIS_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        UPP_EE ((ISIS_LGST,
                 "UPP <X> : Failed to enqueue of dynamic hostname message\n"
                 "UPP <X> : Exiting IsisSendHostNmeUpdateMsg()\n"));

        return ISIS_FAILURE;
    }
    UPP_EE ((ISIS_LGST, "UPP <X> : Exiting IsisSendHostNmeUpdateMsg()\n"));
    return ISIS_SUCCESS;
}
