# include  "lr.h"
# include  "fssnmp.h"
# include  "fsistlow.h"
# include  "fsistswr.h"
# include  "fsistsdb.h"

INT4
GetNextIndexFsIsisExtSysShortestPathTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsIsisExtSysShortestPathTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             &(pNextMultiIndex->pIndex[5].i4_SLongValue),
             &(pNextMultiIndex->pIndex[6].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsIsisExtSysShortestPathTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].i4_SLongValue,
             &(pNextMultiIndex->pIndex[5].i4_SLongValue),
             pFirstMultiIndex->pIndex[6].i4_SLongValue,
             &(pNextMultiIndex->pIndex[6].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSISTS ()
{
    SNMPRegisterMib (&fsistsOID, &fsistsEntry);
    SNMPAddSysorEntry (&fsistsOID, (const UINT1 *) "fsistst");
}

VOID
UnRegisterFSISTS ()
{
    SNMPUnRegisterMib (&fsistsOID, &fsistsEntry);
    SNMPDelSysorEntry (&fsistsOID, (const UINT1 *) "fsistst");
}

INT4
FsIsisExtSysShortestPathNextHopIPAddrGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsIsisExtSysShortestPathTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].i4_SLongValue,
         pMultiIndex->pIndex[6].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsIsisExtSysShortestPathNextHopIPAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].i4_SLongValue,
             pMultiIndex->pIndex[6].i4_SLongValue, pMultiData->pOctetStrValue));

}
