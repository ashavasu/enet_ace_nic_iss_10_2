 /******************************************************************************
 * Copyright (C) Future Software Limited,  2001
 *
 * $Id: isflti.c,v 1.7 2017/09/11 13:44:08 siva Exp $
 *
 * Description: This file contains the Fault Tolerance Interface 
 * routines.
 *
 ******************************************************************************/

#ifdef ISIS_FT_ENABLED

#include "isincl.h"
#include "isextn.h"

PRIVATE INT4        IsisRmRcvPktFromRm (UINT1, tRmMsg *, UINT2);
/*****************************************************************************/
/*                                                                           */
/* Function     : IsisRmRcvPktFromRm                                         */
/*                                                                           */
/* Description  : This API constructs a message containing the               */
/*                given RM event and RM message. And posts it to the         */
/*                Isis queue                                                 */
/*                                                                           */
/* Input        : u1Event   - Event type given by RM module                  */
/*              : pData     - RM Message to enqueue                          */
/*              : u2DataLen - Length of the message                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
IsisRmRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tIsisMsg            IsisMsg;

    /* If the received event is not any of the following, then just return
     * without processing the event
     */
    if ((u1Event != RM_MESSAGE) && (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) && (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT))
    {
        return OSIX_FAILURE;
    }

    /* pData message pointer is valid only if the event is
     * RM_MESSAGE/RM_PEER_UP/RM_PEER_DOWN. If the recevied
     * event is any of the above mentioned event then validate the
     * message pointer and message length
     */

    if ((u1Event == RM_MESSAGE) ||
        (u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
    {
        if (pData == NULL)
        {
            return OSIX_FAILURE;
        }

        if (u1Event == RM_MESSAGE)
        {
            if (u2DataLen < ISIS_RED_MSG_HDR_SIZE)
            {
                RM_FREE (pData);
                return OSIX_FAILURE;
            }
        }
        else
        {
            if (u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                IsisRmRelRmMsgMem ((UINT1 *) pData);
                return OSIX_FAILURE;
            }
        }
    }

    IsisMsg.u4Length = u2DataLen;
    IsisMsg.pu1Msg = (UINT1 *) pData;
    IsisMsg.u1MsgType = ISIS_MSG_FTM_DATA;
    IsisMsg.u1RmEvent = u1Event;
    IsisMsg.u1DataType = 0;
    IsisMsg.pMemFreeRoutine = NULL;
    IsisMsg.u1LenOrStat = 0;
    IsisMsg.u4CxtOrIfindex = 0;

    if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                            gIsisFltQId, ISIS_SYS_FT_DATA_EVT) == ISIS_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    if (u1Event == RM_MESSAGE)
    {
        RM_FREE (pData);
    }
    else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
    {
        IsisRmRelRmMsgMem ((UINT1 *) pData);
    }
    return OSIX_FAILURE;
}

/*******************************************************************************
 * Function    : IsisFltiRegWithRM() 
 * Description : This routing registers the ISIS with the Redundancy Manager
 *               Module 
 * Input       : None
 * Output      : None
 * Globals     : gIsisExtInfo Referred Not Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC INT4
IsisFltiRegWithRM ()
{
#ifdef RM_WANTED
    tRmRegParams        RmRegParams;
    UINT4               u4RetVal = RM_FAILURE;

    RmRegParams.u4EntId = RM_ISIS_APP_ID;
    RmRegParams.pFnRcvPkt = IsisRmRcvPktFromRm;

    u4RetVal = RmRegisterProtocols (&RmRegParams);

    if (u4RetVal != RM_SUCCESS)
    {
        return ISIS_FAILURE;
    }
#endif
    return ISIS_SUCCESS;
}

/****************************************************************************
 * Function    : IsisFltiDeRegWithRM () 
 * Description : This routing De-registers the ISIS from the Redundancy 
 *               Manager Module
 * Input       : None
 * Output      : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ****************************************************************************/

PUBLIC INT4
IsisFltiDeRegWithRM ()
{
#ifdef RM_WANTED
    UINT4               u4RetVal = RM_FAILURE;

    u4RetVal = RmDeRegisterProtocols (RM_ISIS_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        return ISIS_FAILURE;
    }
#endif

    return ISIS_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisRmSendMsgToRm                                          */
/*                                                                           */
/* Description  : This function calls the RM API to en-queue the             */
/*                message from ISIS to RM task.                              */
/*                                                                           */
/* Input        : pRmMsg - Message from ISIS.                                */
/*                u2DataLen - Length of message                              */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : ISIS_SUCCESS on Successfull en-queue                       */
/*                else returns ISIS_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IsisRmSendMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
#ifdef RM_WANTED
    UINT4               u4SrcEntId = (UINT4) RM_ISIS_APP_ID;
    UINT4               u4DestEntId = (UINT4) RM_ISIS_APP_ID;
    UINT4               u4RetVal = RM_FAILURE;

    u4RetVal = RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
                                     u4SrcEntId, u4DestEntId);
    if (u4RetVal != RM_SUCCESS)
    {
        /* Memory allocated for pRmMsg is freed here, only in failure case.
         * In success case RM will free the memory
         */
        RM_FREE (pRmMsg);
        return ISIS_FAILURE;
    }
#else
    UNUSED_PARAM (u2DataLen);
    RM_FREE (pRmMsg);
#endif
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisRmAllocForRmMsg                                        */
/*                                                                           */
/* Description  : This function allocates memory for the RM messages.        */
/*                If allocation fails the send the event to RM.              */
/*                                                                           */
/* Input        : u2Size - required memory size in bytes.                    */
/*                                                                           */
/* Output       : pRmMsg - pointer to the allocated memory.                  */
/*                                                                           */
/* Returns      : ISIS_SUCCESS on Successfull release.                       */
/*                else returns ISIS_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
tRmMsg             *
IsisRmAllocForRmMsg (UINT2 u2Size)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pRmMsg = NULL;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2Size);

    if (pRmMsg == NULL)
    {
        /* Send bulk update failure event to RM */
        return NULL;
    }

    return pRmMsg;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisRmRelRmMsgMem                                          */
/*                                                                           */
/* Description  : This function calls the RM API to release the memory       */
/*                allocated for RM message.                                  */
/*                                                                           */
/* Input        : pu1Block - Memory block to be released                     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : Isis_SUCCESS on Successfull release        .               */
/*                else returns Isis_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/

INT4
IsisRmRelRmMsgMem (UINT1 *pu1Block)
{
#ifdef RM_WANTED
    UINT4               u4RetVal = RM_FAILURE;

    u4RetVal = RmReleaseMemoryForMsg (pu1Block);

    if (u4RetVal != RM_SUCCESS)
    {
        return ISIS_FAILURE;
    }
#else
    UNUSED_PARAM (pu1Block);
#endif
    return ISIS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : IsisRmSendEventToRm                                       */
/*                                                                          */
/* Description  : This  function is called by the protocol to send an       */
/*                event to RM module                                        */
/*                                                                          */
/* Input        : pEvt     -     Pointer to tRmProtoEvt structure           */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
IsisRmSendEventToRm (UINT4 u4Event, UINT4 u4Error)
{
    /* The following are the event sent by the protocols to RM module
     * RM_PROTOCOL_BULK_UPDT_COMPLETION
     * RM_INITIATE_BULK_UPDATE
     * RM_BULK_UPDT_ABORT
     * RM_STANDBY_EVT_PROCESSED
     * RM_IDLE_TO_ACTIVE_EVT_PROCESSED
     * RM_STANDBY_TO_ACTIVE_EVT_PROCESSED
     * RM_PROTOCOL_SEND_EVENT
     * The following are the error message for bulk update abort
     * RM_MEMALLOC_FAIL
     * RM_SENDTO_FAIL
     * RM_PROCESS_FAIL
     * RM_NONE
     */
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4Event = u4Event;
    ProtoEvt.u4Error = u4Error;

    if (u4Event == RM_BULK_UPDT_ABORT)
    {
        ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_ABORTED);
    }

    if (u4Error != RM_NONE)
    {
        WARNING ((ISIS_LGST,
                  "IsisRmSendEventToRm : Error occurred during sync-up\r\n"));
    }

    ProtoEvt.u4AppId = RM_ISIS_APP_ID;
#ifdef RM_WANTED
    if (RmApiHandleProtocolEvent (&ProtoEvt) == RM_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif

    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : IsisRmSetBulkUpdateStatus                                 */
/*                                                                          */
/* Description  : This  function is called by the protocol to set the bulk  */
/*                update status to indicate the bulk update completion      */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
IsisRmSetBulkUpdateStatus (VOID)
{
#ifdef RM_WANTED
    RmSetBulkUpdatesStatus (RM_ISIS_APP_ID);
#endif
}

/****************************************************************************/
/*                                                                          */
/* Function     : IsisSendProtoAckToRM                                      */
/*                                                                          */
/* Description  : This function sends ack to RM module                      */
/*                                                                          */
/* Input        : u4SeqNum  -  Seq num for which ack needs to be sent       */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
IsisSendProtoAckToRM (UINT4 u4SeqNum)
{
    tRmProtoAck         ProtoAck;

    MEMSET (&ProtoAck, 0, sizeof (ProtoAck));
    ProtoAck.u4AppId = RM_ISIS_APP_ID;
    ProtoAck.u4SeqNumber = u4SeqNum;

    FTP_PT ((ISIS_LGST,
             "FLT <E> : Seq. No. [ %u ] - Sent Acknowledgement to RM\n",
             u4SeqNum));
#ifdef RM_WANTED
    RmApiSendProtoAckToRM (&ProtoAck);
#endif
}

/****************************************************************************/
/*                                                                          */
/* Function     : IsisRmGetStandbyNodeCount                                 */
/*                                                                          */
/* Description  : This function calls the RM API to get the current no      */
/*                standby peers                                             */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : No of peers attached to this active node                  */
/*                                                                          */
/****************************************************************************/

PUBLIC UINT1
IsisRmGetStandbyNodeCount (VOID)
{
    UINT1               u1StandbyCount = 0;

#ifdef RM_WANTED
    u1StandbyCount = RmGetStandbyNodeCount ();
#endif

    return u1StandbyCount;
}

#endif
