/******************************************************************************
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isdlli.c,v 1.19 2017/09/11 13:44:08 siva Exp $
 *
 *   Description: This file includes interface routines for interfacing with the
 *   Data Link Layers. It interacts with the CFA module which hides the details
 *   of the underlying driver implementations. 
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/*******************************************************************************
 * Function    : IsisDlliIfStatusIndication ()
 * Description : This routine processes the interface status indication message
 *               received from the DLL module, finds the appropriate DLL map
 *               entry and indicates the Higher layer modules. 
 * Inputs      : u2LLIfIndex - DLL specific interface index
 *               u1Status    - Status of the interface
 * Outputs     : None
 * Globals     : gIsisFwdContext Referred, Not Modified
 * Returns     : ISIS_SUCCESS if interface status handled successfully
 *               ISIS_FAILURE otherwise
 ******************************************************************************/

PUBLIC INT4
IsisDlliIfStatusIndication (UINT2 u2LLIfIndex, UINT1 u1Status)
{
    tIsisMsg            IsisMsg;
    tIsisLLInfo        *pLLInfo = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;

    if (ISIS_GET_CKT_HASH_SIZE () == 0)
    {
        /* Still no circuit records are configured in the ISIS Context so ignore
         * the Inidcation */
        return ISIS_FAILURE;
    }

    /* Get the  Circuit record for the given ifindex */
    IsisLock ();
    i4RetVal = IsisAdjGetCktRecWithIfIdx ((UINT4) u2LLIfIndex, 0, &pCktRec);

    if (i4RetVal != ISIS_SUCCESS)
    {
        /* Ignore the indication */

        DLP_PT ((ISIS_LGST, "DLLI <T> : Status for Erroneous I/f [ %u ]\n",
                 u2LLIfIndex));
        IsisUnlock ();
        return ISIS_FAILURE;
    }

    u1Status = (u1Status == CFA_IF_UP) ? ISIS_UP : ISIS_DOWN;

    if (u1Status == ISIS_UP)
    {
        pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                  sizeof (tIsisLLInfo));
        if (pLLInfo == NULL)
        {
            pLLInfo = NULL;
        }
        else
        {
            IsisGetIfInfo (pCktRec->u4CktIfIdx, pLLInfo);
        }
        u1Status = ISIS_STATE_ON;
    }
    IsisUnlock ();
    IsisMsg.u4Length = sizeof (tIsisLLInfo);
    IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
    IsisMsg.u1MsgType = ISIS_MSG_IF_STAT_IND;
    IsisMsg.u1DataType = 0;
    IsisMsg.pMemFreeRoutine = NULL;
    IsisMsg.u1LenOrStat = u1Status;
    IsisMsg.u4CxtOrIfindex = (UINT4) u2LLIfIndex;
    if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                            gIsisCtrlQId, ISIS_SYS_LL_DATA_EVT) == ISIS_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
        pLLInfo = NULL;
    }
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisDlliRecv ()
 * Description : This routine formats the received data as required by the ISIS
 *               module. It maps the incoming IfIndex (specific to the DLL
 *               module) to a combination of <IfIndex, SubIfIndex> which is
 *               specific to the ISIS module. It enqueues the data to the
 *               Control module . 
 * Inputs      : pQBuf       - Buffer received from the DLL module
 *               u2LLIfIndex - DLL specific interface index
 * Outputs     : None
 * Globals     : Not Modified
 * Returns     : ISIS_SUCCESS if data reception is successful
 *               ISIS_FAILURE otherwise
 ******************************************************************************/

PUBLIC INT4
IsisDlliRecv (tIsisQBuf * pQBuf, UINT2 u2LLIfIndex)
{
    tIsisMsg            IsisMsg;
    INT4                i4RetVal = ISIS_SUCCESS;

    /* Get the Circuit record for the given ifindex */

    if (gu1IsisStatus == ISIS_SHUTDOWN)
    {
        return ISIS_FAILURE;
    }

    if (i4RetVal != ISIS_SUCCESS)
    {
        DLP_PT ((ISIS_LGST, "DLLI <T> : Data Rx on Erroneous I/f [ %u ]\n",
                 u2LLIfIndex));
        return ISIS_FAILURE;
    }

    IsisMsg.u4Length = (UINT4) ISIS_GET_MSG_LEN (pQBuf);
    IsisMsg.pu1Msg = (void *) pQBuf;
    IsisMsg.u1MsgType = ISIS_MSG_LL_DATA;

    IsisMsg.u1DataType = (UINT1) ISIS_DATA_RAW;
    IsisMsg.u1LenOrStat = ISIS_SNPA_ADDR_LEN;
    CRU_BUF_Copy_FromBufChain (pQBuf, IsisMsg.au1PeerSNPAAddr,
                               ISIS_OFFSET_SRC_MAC_ADDR, ISIS_SNPA_ADDR_LEN);

    IsisMsg.pMemFreeRoutine = (tIsisFptr) CRU_BUF_Release_MsgBufChain;

    IsisMsg.u4CxtOrIfindex = (UINT4) u2LLIfIndex;

    if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg), gIsisCtrlQId,
                            ISIS_SYS_LL_DATA_EVT) == ISIS_FAILURE)
    {
        IsisFreeDLLBuf (&IsisMsg);
    }
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisDlliTxData ()
 * Description : This function is invoked by the protocol modules (TRFR) to
 *               transmit protocol data over the physical interfaces. This
 *               function is also invoked by the Forwarding module when it
 *               receives HL data through back plane (Control Module and DLLI
 *               reside on separate nodes). 
 * Inputs      : u4IfIndex  - Interface over which the data is to be transmitted
 *               pLLHdr     - Pointer to the structure containing Link Layer
 *                            Header information
 *               pu1Data    - Pointer to the buffer to be transmitted
 *               u4Length   - Length of the data
 *               u1RelFlag  - flag indicating whether the buffer is to be
 *                            released.
 * Outputs     : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS if initialiation is successful
 *               ISIS_FAILURE otherwise
 ******************************************************************************/

PUBLIC INT4
IsisDlliTxData (tIsisCktEntry * pCktRec, tIsisLLHdr * pLLHdr, UINT1 *pu1Data,
                UINT4 u4Length, UINT1 u1RelFlag)
{
    INT4                i4RetVal = CFA_FAILURE;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1LLCHdr[3] = { 0xFE, 0xFE, 0x03 };
    UINT2               u2PktLen = 0;
    UINT2               u2Offset = 0;
    tIsisQBuf          *pChainBuf = NULL;
    tIsisComHdr        *pIsisComHdr = NULL;

    /* The data must be copied to appropriate buffer before invoking the DLL's 
     * transmit routine. The MAC address is copied to the buffer which will
     * be prepended to the data buffer along with appropriate encapsulation by
     * the DLL module. In case the DLL module does not support such
     * functionality, this routine must be modified to include the appropriate
     * headers to the data before invoking the DLL's routine
     */

    /* NOTE: Current implementation utilises the services of CFA which expects 
     * CRU buffers
     */

    /*
     * Ethernet - Destination MAC and Source MAC addresses copied to ModuleData
     * field in the CRU buffer. CFA is expected to prepend the data with the
     * Ethernet encapsulation and the LLC header 0xFE, 0xFE 0x03 before
     * transmitting the data over any physical interface.
     *
     * FR - CFA is expected to prepend the data with <DLCI (4 bytes) + 0x03
     * (1byte)> header.
     *
     * ATM - CFA is expected to prepend the data with <0xFE, 0xFE, 0x03> header
     *
     * ETH - CFA is expected to prepend data with <Dest MAC, Src MAC, T/L, 0xFE,
     * 0xFE, 0x03> header.
     *
     * PPP - No encapsulations required.
     */

    pi1InterfaceName = &ai1InterfaceName[0];
    pIsisComHdr = (tIsisComHdr *) pu1Data;

    if ((pCktRec->u1CktType == ISIS_BC_CKT)
        || (pCktRec->u1CktType == ISIS_P2P_CKT))
    {
        pChainBuf = (tIsisQBuf *) ISIS_MEM_ALLOC (ISIS_BUF_CHNS,
                                                  (u4Length + ISIS_ETH_HDR_LEN +
                                                   ISIS_LLC_HDR_LEN));
        if (pChainBuf == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Chain Buffer\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            if (u1RelFlag == ISIS_FREE_BUF)
            {
                IsisUtlFreePkt (pu1Data);
            }
            return ISIS_FAILURE;
        }

        ISIS_COPY_TO_CHAIN_BUF (pChainBuf, pLLHdr->au1DestMACAddr,
                                u2Offset, ISIS_SNPA_ADDR_LEN);
        u2Offset += ISIS_SNPA_ADDR_LEN;

        ISIS_COPY_TO_CHAIN_BUF (pChainBuf, pLLHdr->au1SrcMACAddr,
                                u2Offset, ISIS_SNPA_ADDR_LEN);
        u2Offset += ISIS_SNPA_ADDR_LEN;

        u2PktLen = (UINT2) u4Length;
        u2PktLen += ISIS_LLC_HDR_LEN;
        u2PktLen = (UINT2) OSIX_HTONS (u2PktLen);

        ISIS_COPY_TO_CHAIN_BUF (pChainBuf, (UINT1 *) &u2PktLen, u2Offset,
                                sizeof (UINT2));
        u2Offset += sizeof (UINT2);

        ISIS_COPY_TO_CHAIN_BUF (pChainBuf, au1LLCHdr, u2Offset,
                                ISIS_LLC_HDR_LEN);
        u2Offset += ISIS_LLC_HDR_LEN;

    }
    else
    {
        pChainBuf = (tIsisQBuf *) ISIS_MEM_ALLOC (ISIS_BUF_CHNS, u4Length);
        if (pChainBuf == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Chain Buffer\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            if (u1RelFlag == ISIS_FREE_BUF)
            {
                IsisUtlFreePkt (pu1Data);
            }
            return ISIS_FAILURE;
        }
    }

    ISIS_COPY_TO_CHAIN_BUF (pChainBuf, pu1Data, u2Offset, u4Length);

    i4RetVal = ISIS_TX_DATA_TO_DLL ((UINT2) pCktRec->u4LLHandle, pChainBuf);

    if (i4RetVal != CFA_SUCCESS)
    {
        ISIS_MEM_FREE (ISIS_BUF_CHNS, (UINT1 *) pChainBuf);
        pChainBuf = NULL;
    }

    if ((INT_LOG == ISIS_LOG_ENABLE))
    {
        CfaCliGetIfName (pCktRec->u4CktIfIdx, pi1InterfaceName);
        if (gau1IsisModLevelMask[ISIS_INT_MODULE] & ISIS_PT_MASK)
        {

            PANIC ((ISIS_LGST, "Sending %s On Interface:%s \n",
                    ISIS_GET_PDU_TYPE_STR_FOR_TRACE (pIsisComHdr->u1PDUType),
                    pi1InterfaceName));
            IsisDbgPrintCommHdr (pCktRec->pContext, pIsisComHdr);
        }
    }

    if ((PKT_LOG == ISIS_LOG_ENABLE))
    {
        CfaCliGetIfName (pCktRec->u4CktIfIdx, pi1InterfaceName);
        if (gau1IsisModLevelMask[ISIS_PDMP_MODULE] & ISIS_PT_MASK)
        {
            PANIC ((ISIS_LGST, "Sending %s pkt dump On Interface:%s \n",
                    ISIS_GET_PDU_TYPE_STR_FOR_TRACE (pIsisComHdr->u1PDUType),
                    pi1InterfaceName));
            IsisDbgPrintHEXDMP (u4Length, pu1Data);
        }
    }
    /* Does the calling module allow us to return the buffer. If YES then free
     * up the memory.
     */

    if (u1RelFlag == ISIS_FREE_BUF)
    {
        IsisUtlFreePkt (pu1Data);
    }
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisDlliUpdateBindings ()
 * Description : This function registers/unregisters the Multicast Address
 *               information based on the Level.
 * Inputs      : pCktRec  - The Circuit Record 
 *               u1Level  - Circuit Level used for registering Multicast MAC
 *                          address which is different for Level1 and
 *                          Level2. Applicable only for Ethernet case.
 * Outputs     : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if MAC registrations successful
 *               ISIS_FAILURE, otherwise
 ******************************************************************************/

PRIVATE INT4
IsisDlliUpdateBindings (tIsisCktEntry * pCktRec, UINT1 u1Status)
{
    UINT1               au1L1MCAddr[6] = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x14 };
    UINT1               au1L2MCAddr[6] = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x15 };
    UINT1               au1MCAddr[40] = { 0 };

    MEMSET (au1MCAddr, 0, 40);
    DLP_PT ((ISIS_LGST, "DLLI <T> : Current Level [ %u ]\n",
             pCktRec->u1CktLevel));

    switch (pCktRec->u1CktLevel)
    {
        case ISIS_LEVEL1:

            /* Circuit changed to Level1 and the previous state was
             * Level2 */

            if ((u1Status == ISIS_CKT_UP) || (u1Status == ISIS_CKT_RESTART))
            {
                /* We do not require Level2 bindings now. Unregister
                 * the previous bindings and register for Level1
                 * Multicast binding */

                DLP_PT ((ISIS_LGST,
                         "DLLI <T> : Deregistering L2, Registering L1\n"));

                MEMCPY (au1MCAddr, au1L1MCAddr, ISIS_SNPA_ADDR_LEN);
                ISIS_CFG_MAC_ADDR (CFA_ADD_ENET, ((UINT2) pCktRec->u4CktIfIdx),
                                   0x00000000, au1MCAddr);
            }

            else if (u1Status == ISIS_CKT_DOWN)
            {

                MEMCPY (au1MCAddr, au1L1MCAddr, ISIS_SNPA_ADDR_LEN);
                ISIS_CFG_MAC_ADDR (CFA_DEL_ENET, ((UINT2) pCktRec->u4CktIfIdx),
                                   0x00000000, au1MCAddr);

            }

            break;

        case ISIS_LEVEL2:

            if ((u1Status == ISIS_CKT_UP) || (u1Status == ISIS_CKT_RESTART))
            {
                /* We do not require Level2 bindings now. Unregister
                 * the previous bindings and register for Level1
                 * Multicast binding
                 */

                DLP_PT ((ISIS_LGST,
                         "DLLI <T> : Deregistering L2, Registering L1\n"));

                MEMCPY (au1MCAddr, au1L2MCAddr, ISIS_SNPA_ADDR_LEN);
                ISIS_CFG_MAC_ADDR (CFA_ADD_ENET, ((UINT2) pCktRec->u4CktIfIdx),
                                   0x00000000, au1MCAddr);
            }

            else if (u1Status == ISIS_CKT_DOWN)
            {
                MEMCPY (au1MCAddr, au1L2MCAddr, ISIS_SNPA_ADDR_LEN);
                ISIS_CFG_MAC_ADDR (CFA_DEL_ENET, ((UINT2) pCktRec->u4CktIfIdx),
                                   0x00000000, au1MCAddr);

            }

            break;

        case ISIS_LEVEL12:

            if ((u1Status == ISIS_CKT_UP) || (u1Status == ISIS_CKT_RESTART))
            {
                /* We do not require Level2 bindings now. Unregister
                 * the previous bindings and register for Level1
                 * Multicast binding
                 */

                DLP_PT ((ISIS_LGST,
                         "DLLI <T> : Deregistering L2, Registering L1\n"));

                MEMCPY (au1MCAddr, au1L1MCAddr, ISIS_SNPA_ADDR_LEN);
                ISIS_CFG_MAC_ADDR (CFA_ADD_ENET, ((UINT2) pCktRec->u4CktIfIdx),
                                   0x00000000, au1MCAddr);

                MEMSET (au1MCAddr, 0, ISIS_SNPA_ADDR_LEN);

                MEMCPY (au1MCAddr, au1L2MCAddr, ISIS_SNPA_ADDR_LEN);
                ISIS_CFG_MAC_ADDR (CFA_ADD_ENET, ((UINT2) pCktRec->u4CktIfIdx),
                                   0x00000000, au1MCAddr);
            }

            else if (u1Status == ISIS_CKT_DOWN)
            {

                MEMCPY (au1MCAddr, au1L1MCAddr, ISIS_SNPA_ADDR_LEN);
                ISIS_CFG_MAC_ADDR (CFA_DEL_ENET, ((UINT2) pCktRec->u4CktIfIdx),
                                   0x00000000, au1MCAddr);

                MEMSET (au1MCAddr, 0, ISIS_SNPA_ADDR_LEN);

                MEMCPY (au1MCAddr, au1L2MCAddr, ISIS_SNPA_ADDR_LEN);
                ISIS_CFG_MAC_ADDR (CFA_DEL_ENET, ((UINT2) pCktRec->u4CktIfIdx),
                                   0x00000000, au1MCAddr);

            }

            break;

        default:

            /* Nothing to be done. Circuit Level is erroneous
             */

            DLP_PT ((ISIS_LGST, "DLLI <T> : Erroneous Circuit Level "
                     "in I/F status indication [ %u ]\n", pCktRec->u1CktLevel));

            return ISIS_FAILURE;
    }
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisDlliProcIfStatusChange ()
 * Description : This function is invoked by the protocol modules (TRFR) to
 *               indicate the change in status of an interface. This function is
 *               also invoked by the Forwarding module when it receives the
 *               Circuit status change message through back plane (Control 
 *               Module and DLLI reside on separate nodes). If a new interface 
 *               is coming up, it registers the interface (in case of ATM and FR
 *               links) with the CFA, and maintains a mapping between the
 *               <u4IfIndex, u4IfSubIndex> and the CFA specific handle. It is
 *               the responsibility of the DLLI module to map these indices
 *               appropriately when trasnferring data to and fro.
 * Inputs      : u4IfIndex  - Interface whose status has changed
 *               u4SubIndex - Identifies <VPI,VCI> in the case of ATM or DLCI
 *                            in the case of FR networks
 *               u1Status   - Status of the interface
 *               u1Level    - Circuit Level used for registering Multicast MAC
 *                            address which is different for Level1 and
 *                            Level2. Applicable only for Ethernet case.
 * Outputs     : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if Event handling is successful 
 *               ISIS_FAILURE. otherwise 
 ******************************************************************************/

PUBLIC INT4
IsisDlliProcIfStatusChange (tIsisCktEntry * pCktRec, UINT1 u1Status)
{
    tCfaIfInfo          IfInfo;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
#ifdef LANAI_WANTED
    tAtmVcConfigStruct  AtmConfig;
#endif
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4LLHandle = 0;
    UINT4               u4Port = 0;
    UINT1               u1IfType = 0;
    UINT1               au1ipv4Addr[ISIS_MAX_IP_ADDR_LEN];

    /* Now process for any I/F status changes
     */

    MEMSET ((UINT1 *) &NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET ((UINT1 *) &NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET ((UINT1 *) &IfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (au1ipv4Addr, 0, sizeof (au1ipv4Addr));

    if (ISIS_GET_IF_INFO ((UINT2) pCktRec->u4CktIfIdx, (&IfInfo)) ==
        CFA_FAILURE)
    {
        u1IfType = ISIS_IF_INVALID;
    }
    else
    {
        u1IfType = ISIS_GET_IF_TYPE ((&IfInfo));
    }

    switch (u1Status)
    {
        case ISIS_CKT_UP:
        case ISIS_CKT_RESTART:

            if (u1IfType != ISIS_IF_INVALID)
            {

                i4RetVal =
                    NetIpv4GetPortFromIfIndex (pCktRec->u4CktIfIdx, &u4Port);
                if (NETIPV4_SUCCESS ==
                    NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo))
                {
                    NetIpv4IfInfo.u4Addr = OSIX_NTOHL (NetIpv4IfInfo.u4Addr);
                    MEMCPY (pCktRec->au1IPV4Addr,
                            (UINT1 *) &(NetIpv4IfInfo.u4Addr),
                            ISIS_MAX_IPV4_ADDR_LEN);
                    if ((NetIpv4IfInfo.u4Admin == CFA_IF_UP) &&
                        (NetIpv4IfInfo.u4Oper == CFA_IF_UP) &&
                        (NetIpv4IfInfo.u4Addr != 0))
                    {
                        pCktRec->u1ProtoSupp |= ISIS_ADDR_IPV4;
                    }
                    i4RetVal = ISIS_SUCCESS;
                }
                else
                {
                    i4RetVal = ISIS_FAILURE;
                }
                if (NETIPV6_SUCCESS ==
                    NetIpv6GetIfInfo (pCktRec->u4CktIfIdx, &NetIpv6IfInfo))
                {
                    MEMCPY (pCktRec->au1IPV6Addr,
                            (UINT1 *) &(NetIpv6IfInfo.Ip6Addr.u1_addr),
                            ISIS_MAX_IPV6_ADDR_LEN);
                    if ((NetIpv6IfInfo.u4Admin == NETIPV6_ADMIN_UP) &&
                        (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP))
                    {
                        pCktRec->u1ProtoSupp |= ISIS_ADDR_IPV6;
                    }
                    i4RetVal = ISIS_SUCCESS;
                }
                else
                {
                    i4RetVal = ISIS_FAILURE;
                }
            }

            /* A new interface has become active. Register the i/f with the
             * DLL module and get back a Handle which will be used in all
             * subsequent communications with that module. 
             * The registration is necessary only for ATM and FR links
             * since Ethernet and PPP links are created by the CFA and 
             * hence the information is already available with CFA. 
             * A DLLI Entry is created to keep the mapping anyway.
             */
            if ((u1IfType == ISIS_IF_ETH_CSMACD)
                || (u1IfType == ISIS_IF_L3IPVLAN))
            {
                /* Hold the Hardware Address in the Circuit table only for the case
                 * of Ethernet links. This is required for Ethernet encapsulation
                 * during Data Transmission
                 */

                MEMCPY (pCktRec->au1SNPA, (ISIS_GET_HW_ADDR ((&IfInfo))),
                        ISIS_SNPA_ADDR_LEN);
                pCktRec->u4LLHandle = pCktRec->u4CktIfIdx;

                IsisDlliUpdateBindings (pCktRec, u1Status);

                ISIS_DBG_PRINT_ADDR (pCktRec->au1SNPA,
                                     (UINT1) ISIS_SNPA_ADDR_LEN,
                                     "DLLI <T> : MAC \n", ISIS_OCTET_STRING);

                MEMCPY (au1ipv4Addr, &NetIpv4IfInfo.u4Addr, 4);

                DLP_PT ((ISIS_LGST,
                         "DLLI <T> : Interface - [ %s ], IF Index [ %u ], Circuit Index [ %u ]"
                         " IPv4 Address [ %d.%d.%d.%d ], IPv6 Address [ %s ], Type [ %u ], Level [  %s ]\n",
                         ISIS_GET_ISIS_ADJ_STATUS (u1Status),
                         pCktRec->u4CktIfIdx, pCktRec->u4CktIdx, au1ipv4Addr[0],
                         au1ipv4Addr[1], au1ipv4Addr[2], au1ipv4Addr[3],
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(NetIpv6IfInfo.Ip6Addr.u1_addr)),
                         u1IfType,
                         ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel)));
            }

#ifdef LANAI_WANTED
            else if (u1IfType == ISIS_IF_ATM_AAL5)
            {

                /* u4LLHandle will be returned by CFA during
                 * registration. <u4IfIndex, u4IfSubIndex> tuple will
                 * be mapped to the u4LLHandle
                 */

                DLP_PT ((ISIS_LGST, "DLLI <T> : Reg [<Port %u VP %u VC %u>]\n",
                         pCktRec->u4CktIfIdx,
                         (UINT2) ((pCktRec->u4CktIfSubIdx & 0xFFFF0000) >> 16),
                         (UINT2) (pCktRec->u4CktIfSubIdx & 0x0000FFFF)));

                AtmConfig.u1EncapType = CFA_ENCAP_LLC_SNAP;
                i4LLHandle = ISIS_REG_ATM_VC (pCktRec->u4CktIfIdx,
                                              /* VPI */ (UINT2) ((pCktRec->u4CktIfSubIdx
                                                                  & 0xFFFF0000)
                                                                 >> 16),
                                              /* VCI */ (UINT2) (pCktRec->u4CktIfSubIdx
                                                                 & 0x0000FFFF),
                                              &AtmConfig);

                pCktRec->u4LLHandle = (UINT4) i4LLHandle;

                if (i4LLHandle == CFA_FAILURE)
                {
                    DLP_PT ((ISIS_LGST,
                             "DLLI <E> : ATM VC Registration Failed\n"));
                    /* Ckt Initialization is failed so update the statistics
                     */
                    ISIS_INCR_CKT_INIT_FAIL_STAT (pCktRec);
                    return (i4RetVal);
                }
                DLP_PT ((ISIS_LGST,
                         "DLLI <E> : ATM VC Registration Success\n"));
            }
#endif
            else if ((u1IfType == ISIS_IF_FR_DTE)
                     || (u1IfType == ISIS_IF_FR_DCE))
            {

                /* i4LLHandle will be returned by CFA during
                 * registration. <u4IfIndex, u4IfSubIndex> tuple will
                 * be mapped to the pDlliRec->u4LLHandle
                 */

                /* Note: CFA currently does not support FR links.
                 */

                DLP_PT ((ISIS_LGST, "DLLI <T> : Registration - DLCI [ %u ]\n",
                         pCktRec->u4CktIfSubIdx));
                /*klocwork */
                i4LLHandle = ISIS_REG_FR_DLCI (pCktRec->u4CktIfIdx,
                                               pCktRec->u4CktIfSubIdx,
                                               &AtmConfig);

                pCktRec->u4LLHandle = (UINT4) i4LLHandle;
                DLP_PT ((ISIS_LGST,
                         "DLLI <E> : FR DLCI Registration Success\n"));
            }

            IsisUpdateIfStatus (pCktRec);

            i4RetVal = ISIS_SUCCESS;
            break;

        case ISIS_CKT_DOWN:

            /* De-register the circuit from CFA
             */

            WARNING ((ISIS_LGST,
                      "DLLI <T> : Interface [ DOWN ] - IF Index [ %u ]\n",
                      pCktRec->u4CktIfIdx));

            switch (u1IfType)
            {
#ifdef LANAI_WANTED
                case ISIS_IF_ATM_AAL5:
                    i4RetVal = ISIS_DEREG_ATM_VC i4RetVal = ISIS_SUCCESS;
                    break;
#endif
                case ISIS_IF_FR_DCE:
                case ISIS_IF_FR_DTE:

                    i4RetVal = ISIS_DEREG_FR_DLCI
                        (((UINT2) pCktRec->u4LLHandle));

                    i4RetVal = ISIS_SUCCESS;
                    break;

                case ISIS_IF_L3IPVLAN:
                case ISIS_IF_ETH_CSMACD:

                    IsisDlliUpdateBindings (pCktRec, u1Status);
                    i4RetVal = ISIS_SUCCESS;
                    break;

                case ISIS_IF_PPP:

                    /* Nothing to be done here */
                    i4RetVal = ISIS_SUCCESS;
                    break;

                default:

                    /* Unrecognised i/f type - Ignore
                     */

                    DLP_PT ((ISIS_LGST, "DLLI <E> : Erroneous Interface [ %u ]",
                             u1IfType));
                    return ISIS_FAILURE;
            }

    }
    return (i4RetVal);
}
