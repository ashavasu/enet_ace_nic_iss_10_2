/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isadjtxn.c,v 1.15 2017/09/11 13:44:07 siva Exp $ 
 *
 *   Description: This file contains the Routines for encoding Hello Pdus
 *                before transmission 
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/******************************************************************************
 * Function Name : IsisAdjEncodeISH ()
 * Description   : This routine encodes an ISH Pdu with the details 
 *                 available in the local databases
 * Input(s)      : pContext  - Pointer to System Context
 *                 pCktRec   - Pointer to Circuit Record
 *                 pPDU      - Pointer to the PDU to be transmitted
 * Output(s)     : pPDU      - Pointer to the PDU updated with ISH information
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjEncodeISH (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                  UINT1 *pu1PDU)
{
    UINT1              *pu1NET = NULL;
    UINT1               u1NETLen = 0;
    UINT2               u2Offset = 0;
    tIsisCktLevel      *pCktLevel = NULL;
    UINT4               u4HoldTime = 0;
    UINT2               u2HoldTimeSec = 0;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEncodeISH ()\n"));

    /* NOTE: The common header for an ISH PDU is entirely different from the
     *       headers included for ISIS PDUs.
     */

    pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);

    /* Network Layer Protocol Identifier is 0x82 for ES-IS protocol
     */

    ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, 0x82);
    u2Offset++;

    /* Ignore the Length Indicator field for now. It will be filled once the
     * encoding of header is complete
     */

    u2Offset++;                    /* Not filling Length Indicator Field */

    /* Version Extension */

    ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, 0x01);
    u2Offset++;

    /* Reserved */

    ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, 0x00);
    u2Offset++;

    /* Type Code */

    ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, 0x04);
    u2Offset++;

    /* Holding Time */
    u4HoldTime =
        (UINT4) (pCktLevel->HelloTimeInt * pCktLevel->u2HelloMultiplier);
    u2HoldTimeSec = (UINT2) ISIS_CONV_MILLISEC_TO_SEC (u4HoldTime);
    ISIS_ASSIGN_2_BYTES (pu1PDU, u2Offset, u2HoldTimeSec);

    u2Offset += 2;

    u2Offset += 2;

    /* Network Entity Title Length Indicator, NET is made of 3 components, Area
     * Address + SystemID + Selector*/

    u1NETLen = (UINT1) (pContext->ManAddrTable.pMAARec->ManAreaAddr.u1Length +
                        ISIS_SYS_ID_LEN + 1);
    ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, u1NETLen);
    u2Offset++;

    /* Mark this point with pu1NET. We will use it for printing the NET
     * constructed finally. This is purely for debugging purposes only
     */

    pu1NET = pu1PDU + u2Offset;

    /* Copying the NET to the PDU. First Area Address is copied, then the System
     * ID and then finally the Selector
     */

    /* NOTE: There can be more than one area address configured for the Node. We
     *       can use any one of these addresses to construct the NET. The
     *       current implementation by default uses the very first address
     */
    ISIS_ASSIGN_STRING (pu1PDU, u2Offset,
                        pContext->ManAddrTable.pMAARec->ManAreaAddr.au1AreaAddr,
                        pContext->ManAddrTable.pMAARec->ManAreaAddr.u1Length);
    u2Offset += pContext->ManAddrTable.pMAARec->ManAreaAddr.u1Length;
    ISIS_ASSIGN_STRING (pu1PDU, u2Offset, pContext->SysActuals.au1SysID,
                        ISIS_SYS_ID_LEN);
    u2Offset += ISIS_SYS_ID_LEN;

    /* Selector field is used for identifying applications uniquely. Since the
     * current implementation supports only IP environments, the value of
     * selector (and also the NET) does not really matter. Randomly chosen the 
     * value to be '0'
     */

    ISIS_ASSIGN_1_BYTE (pu1PDU, u2Offset, 0x00);
    u2Offset++;

    ISIS_DBG_PRINT_ADDR (pu1NET, u1NETLen,
                         "ADJ <T> : Network Entity Title\t", ISIS_OCTET_STRING);

    /* Now Filling the PDU Length Indicator that we skipped in the beginning
     */

    *(pu1PDU + 1) = (UINT1) u2Offset;

    /* Calculate and fill the chesksum value. Checksum is calculated only for
     * the header and this does not include the NET which is included after the
     * header
     */

    IsisUtlCalcChkSum (pu1PDU, u2Offset, (pu1PDU + 7));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEncodeISH ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjEncodeHelloPdu ()
 * Description   : This routines encodes a Hello PSU based on the given level
 *                 and and circuit type.
 * Input(s)      : pCktRec      - Pointer to Circuit Record
 *                 pCktLevel    - Pointer to Circuit Level Record 
 *                 u1PduType    - Type of PDU. (LEVEL1, LEVEL2 or P2P Hello)
 *                 pu1PDU       - Pointer to the PDU to be constructed
 *                 u2BufSize    - Maximum size of the PDU
 * Output(s)     : pu1PDU       - Encoded IIH PDU 
 *                 pu2Offset    - Length of the PDU constructed
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, on successful encoding of Hello PDU
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjEncodeHelloPdu (tIsisCktEntry * pCktRec, tIsisCktLevel * pCktLevel,
                       UINT1 u1PduType, UINT1 *pu1PDU, UINT2 u2BufSize,
                       UINT2 *pu2Offset)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEncodeHelloPdu ()\n"));

    pContext = pCktRec->pContext;
    /* Encode the Common Header
     */

    IsisUtlFillComHdr (pContext, pu1PDU, u1PduType);
    *pu2Offset += sizeof (tIsisComHdr);

    /* Updating the hello specific header
     */

    IsisAdjBldHelloHdr (pContext, pCktRec, pCktLevel, pu1PDU, pu2Offset);

    /* Encoding of Variable Length fields starts */

    /* While encoding Variable Length fields make sure that the PDU does not
     * exceed Maximum Buffer size. If so abort transmission
     */
    i4RetVal = IsisAdjBldAreaAddrTlv (pContext, pu1PDU, u2BufSize, pu2Offset);

    if (i4RetVal == ISIS_FAILURE)
    {
        WARNING ((ISIS_LGST,
                  "ADJ <W> : IIH Area Address Encoding Error - Circuit [%u], Level [%u]\n",
                  pCktRec->u4CktIdx, pCktRec->u1CktLevel));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
        return ISIS_FAILURE;
    }
    if ((u1PduType == ISIS_L1LAN_IIH_PDU) || (u1PduType == ISIS_L2LAN_IIH_PDU))
    {
        i4RetVal = IsisAdjBldISNbrTlv (pCktRec, pu1PDU, u2BufSize, pu2Offset);
        if (i4RetVal == ISIS_FAILURE)
        {
            WARNING ((ISIS_LGST,
                      "ADJ <W> : IIH IS Neighbour Encoding Error - Circuit [%u]\n",
                      pCktRec->u4CktIdx));
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
            return ISIS_FAILURE;
        }
    }

/* Three way handshake for P2P*/
    if ((u1PduType == ISIS_P2P_IIH_PDU)
        && (pCktRec->u1IsP2PThreeWayEnabled == ISIS_TRUE))
    {
        i4RetVal =
            IsisAdjBldP2PThreeWayHndShkTLV (pCktRec, pu1PDU, u2BufSize,
                                            pu2Offset);
        if (i4RetVal == ISIS_FAILURE)
        {
            WARNING ((ISIS_LGST,
                      "ADJ <W> : IIH TLV 240 (3-way Handshake) Encoding Error - Circuit [ %u ]\n"
                      " Max Size [ %u ], Current Size [ %u ]\n",
                      pCktRec->u4CktIdx, u2BufSize, *pu2Offset));
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
            return ISIS_FAILURE;
        }
    }
/* Three way handshake for P2P*/

    i4RetVal = IsisAdjBldPSTlv (pu1PDU, pu2Offset, pCktRec->u1ProtoSupp);

    if (i4RetVal == ISIS_FAILURE)
    {
        WARNING ((ISIS_LGST, "ADJ <W> : IIH PS Encoding Error - Circuit [%u]\n"
                  " Max Size [ %u ], Current Size [ %u ]\n",
                  pCktRec->u4CktIdx, u2BufSize, *pu2Offset));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
        return ISIS_FAILURE;
    }

    i4RetVal = IsisAdjBldIPV4IfTlv (pContext, pCktRec, pu1PDU, u2BufSize,
                                    pu2Offset);
    if (i4RetVal == ISIS_FAILURE)
    {
        WARNING ((ISIS_LGST,
                  "ADJ <W> : IIH IPv4 Interface Address Encoding Error - Circuit [%u]\n"
                  " Max Size [ %u ], Current Size [ %u ]\n",
                  pCktRec->u4CktIdx, u2BufSize, *pu2Offset));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
        return ISIS_FAILURE;
    }

    i4RetVal = IsisAdjBldIPV6IfTlv (pContext, pCktRec, pu1PDU, u2BufSize,
                                    pu2Offset);
    if (i4RetVal == ISIS_FAILURE)
    {
        WARNING ((ISIS_LGST,
                  "ADJ <W> : IIH IPv6 Interface Address Encoding Error - Circuit [%u]\n"
                  " Max Size [ %u ], Current Size [ %u ]\n",
                  pCktRec->u4CktIdx, u2BufSize, *pu2Offset));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
        return ISIS_FAILURE;
    }

    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        i4RetVal = IsisAdjBldMTTlv (pCktRec, pu1PDU, u2BufSize, pu2Offset);
    }

    if (i4RetVal == ISIS_FAILURE)
    {
        WARNING ((ISIS_LGST,
                  "ADJ <W> : IIH MT TLV Encoding Error - Circuit [%u]\n"
                  " Max Size [ %u ], Current Size [ %u ]\n", pCktRec->u4CktIdx,
                  u2BufSize, *pu2Offset));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
        return ISIS_FAILURE;
    }

    if ((pContext->u1IsisGRRestartSupport != ISIS_GR_RESTART_NONE)
        || (pContext->u1IsisGRHelperSupport != ISIS_GR_HELPER_NONE))
    {
        i4RetVal = IsisAdjRestartTlv (pContext, pCktRec, pu1PDU, u2BufSize,
                                      pu2Offset);

        if (i4RetVal == ISIS_FAILURE)
        {
            WARNING ((ISIS_LGST,
                      "ADJ <W> : IIH Restart TLV Encoding Error - Circuit [%u]\n"
                      " Max Size [ %u ], Current Size [ %u ]\n",
                      pCktRec->u4CktIdx, u2BufSize, *pu2Offset));
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
            return ISIS_FAILURE;
        }
    }
#ifdef BFD_WANTED
    if ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
        (pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE) &&
        (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
    {
        i4RetVal =
            IsisAdjBldBFDEnabledTlv (pCktRec, pu1PDU, u2BufSize, pu2Offset);
    }

    if (i4RetVal == ISIS_FAILURE)
    {
        WARNING ((ISIS_LGST,
                  "ADJ <W> : IIH BFD Enabled TLV Encoding Error - Circuit [%u]\n"
                  " Max Size [ %u ], Current Size [ %u ]", pCktRec->u4CktIdx,
                  u2BufSize, *pu2Offset));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
        return ISIS_FAILURE;
    }
#endif

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEncodeHelloPdu ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjBldHelloHdr ()
 * Description   : This routines updates all the Hello Sepcific header fields
 *                 except the PDU length.
 * Input(s)      : pContext     - Pointer to System Context
 *                 pCktRec      - Pointer to Circuit Record
 *                 pu1PDU       - Pointer to the PDU to be formed
 *                 u1Level      - Level of the circuit(Level1 or Level2)
 * Output(s)     : pu2Offset    - Contains a value which specifies the offset
 *                                into the PDU from where subsequent encoding
 *                                can start
 *                 pu1PDU       - Updated PDU after filling the hello specific
 *                                header
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/

PUBLIC VOID
IsisAdjBldHelloHdr (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                    tIsisCktLevel * pCktLevel, UINT1 *pu1PDU, UINT2 *pu2Offset)
{
    UINT4               u4HoldTime = 0;
    UINT2               u2HoldTimeSec = 0;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldHelloHdr ()\n"));

    *(pu1PDU + *pu2Offset) = pCktRec->u1CktLevel;
    (*pu2Offset)++;

    MEMCPY (pu1PDU + *pu2Offset, pContext->SysActuals.au1SysID,
            ISIS_SYS_ID_LEN);
    *pu2Offset += ISIS_SYS_ID_LEN;

    u4HoldTime
        = (UINT4) (pCktLevel->HelloTimeInt * pCktLevel->u2HelloMultiplier);
    u2HoldTimeSec = (UINT2) ISIS_CONV_MILLISEC_TO_SEC (u4HoldTime);
    ISIS_ASSIGN_2_BYTES (pu1PDU, *pu2Offset, u2HoldTimeSec);
    *pu2Offset += 2;

    /* Do not fill the PDU length at this point. The PDU length will be filled
     * once the encoding is complete
     */

    *pu2Offset += 2;            /* Skipping the PDU length field */

    if (pCktRec->u1CktType == ISIS_P2P_CKT)
    {
        *(pu1PDU + *pu2Offset) = pCktRec->u1CktLocalID;
        (*pu2Offset)++;
    }
    else if (pCktRec->u1CktType == ISIS_BC_CKT)
    {
        *(pu1PDU + *pu2Offset) = pCktLevel->u1ISPriority;
        (*pu2Offset)++;

        /* Referring to Last Paragraph of section 8.4.5 of specification 
         * ISO 10589
         */

        MEMCPY (pu1PDU + *pu2Offset, pCktLevel->au1CktLanDISID,
                (ISIS_SYS_ID_LEN + 1));
        *pu2Offset += (ISIS_SYS_ID_LEN + 1);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldHelloHdr ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjBldAreaAddrTlv ()
 * Description   : This routines encodes the configured Area Addresses into the
 *                 PDU. If the length of the PDU exceeds the maximum allowed
 *                 buffer size at any point, the routine returns an Error code
 *                 and the calling routine must ensure that the PDU is freed
 * Input(s)      : pContext     - Pointer to System Context
 *                 pu1PDU       - Pointer to the PDU to which Area Address
 *                                TLVs should be added
 *                 u2Bufsize    - Maximum size of the Buffer               
 * Output(s)     : pu2Offset    - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU       - PDU updated with the Area Address TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjBldAreaAddrTlv (tIsisSysContext * pContext, UINT1 *pu1PDU,
                       UINT2 u2BufSize, UINT2 *pu2Offset)
{
    UINT1               u1TlvLen = 0;
    UINT1               u1AALen = 0;
    tIsisMAAEntry      *pMAAEntry = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldAreaAddrTlv ()\n"));

    pMAAEntry = pContext->ManAddrTable.pMAARec;

    while (pMAAEntry != NULL)
    {
        /* Only Active entries will be considered */

        if (pMAAEntry->u1ExistState == ISIS_ACTIVE)
        {
            u1AALen = pMAAEntry->ManAreaAddr.u1Length;

            /* First check whether there is enough space in the buffer to add
             * Area Addresses. If enough space not available just quit
             *
             * NOTE: (u1TlvLen == 0) or (u1TlvLen + u1AALen + 1) >
             * 0xFF means that we have to add a new TLV to hold Area Addresses.
             * As long as TLV Length is < 0xFF keep adding Area Addresses to the
             * same TLV
             */

            if ((((u1TlvLen == 0)
                  || ((u1TlvLen + u1AALen + 1) > 255))
                 && ((u2BufSize - *pu2Offset) < (u1AALen + 3)))
                || ((u2BufSize - *pu2Offset) < (u1AALen + 1)))
            {
                ISIS_DBG_PRINT_ADDR (pMAAEntry->ManAreaAddr.au1AreaAddr,
                                     u1AALen,
                                     "ADJ <T> : Failed While Encoding Addr\t",
                                     u1AALen);
                return ISIS_FAILURE;
            }

            /* Construct a new TLV if u1TlvLen is ZERO (starting condition)
             * or if TLV Length exceeds 0xFF (maximum size of a TLV)
             */

            if ((u1TlvLen == 0) || ((u1TlvLen + u1AALen + 1) > 0xFF))
            {
                if (u1TlvLen != 0)
                {
                    /* Since TLV length in non-zero, we have already added
                     * certain Area Addresses. Fill the Length of the TLV since
                     * we are now proceeding to construct a new TLV
                     */

                    *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
                    u1TlvLen = 0;
                }

                /* Start with a New TLV. Fill the Code, Length gets filled
                 * either after the TLV is completely filled or if we are done
                 * with all the area addresses
                 */

                *(pu1PDU + *pu2Offset) = ISIS_AREA_ADDR_TLV;

                /* Incrementing the offset for Code and Length of the new TLV,
                 * as mentioned Only Code gets filled here, Length later
                 */

                *pu2Offset = (UINT2) (*pu2Offset + 2);
            }

            /* Add the Area Address to the TLV. Area Addresses are of the form
             * <Length, Address>
             */

            *(pu1PDU + *pu2Offset) = u1AALen;
            u1TlvLen++;
            *pu2Offset += 1;

            ISIS_ASSIGN_STRING (pu1PDU, *pu2Offset,
                                pMAAEntry->ManAreaAddr.au1AreaAddr, u1AALen);
            u1TlvLen += u1AALen;
            *pu2Offset += u1AALen;
        }
        pMAAEntry = pMAAEntry->pNext;
    }

    /* Filling the length of the last TLV in the case of Area Addresses
     * occupying more than one TLV. 
     */

    if (u1TlvLen != 0)
    {
        *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldAreaAddrTlv()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjBldISNbrTLV ()
 * Description   : This routines encodes the configured IS Neighbour Addresses 
 *                 into the PDU. If the length of the PDU exceeds the maximum 
 *                 allowed buffer size at any point, the routine returns an 
 *                 Error code and the calling routine must ensure that the PDU 
 *                 is freed
 * Input(s)      : pCktRec      - Pointer to Circuit Record
 *                 pu1PDU       - Pointer to the PDU to which Area Address
 *                                TLVs should be added
 *                 u2BufSize    - Maximum size of the Buffer               
 * Output(s)     : pu2Offset    - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU       - PDU updated with the IS Neighbour Address TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjBldISNbrTlv (tIsisCktEntry * pCktRec, UINT1 *pu1PDU, UINT2 u2BufSize,
                    UINT2 *pu2Offset)
{
    UINT1               u1TlvLen = 0;
    UINT1               u1SysType = 0;
    tIsisAdjEntry      *pAdjRec = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldISNbrTlv ()\n"));

    pAdjRec = pCktRec->pAdjEntry;

    if (pAdjRec == NULL)
    {
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldISNbrTlv ()\n"));
        return ISIS_SUCCESS;
    }

    u1SysType = (ISIS_EXTRACT_PDU_TYPE (pu1PDU) == ISIS_L1LAN_IIH_PDU) ?
        ISIS_LEVEL1 : ISIS_LEVEL2;

    /* Encode all the Neighbour Addresses (SNPA Addresses) of the adjacent
     * neighbours,
     */

    while (pAdjRec != NULL)
    {
        if (pAdjRec->u1AdjUsage == u1SysType)
        {
            /* First check whether there is enough space in the buffer to add
             * IS Neighbour Addresses. If enough space not available just quit
             *
             * NOTE: (u1TlvLen == 0) or (u1TlvLen + ISIS_SNPA_ADDR_LEN) >
             * 0xFF means that we have to add a new TLV to hold SNPA Addresses.
             * As long as TLV Length is < 0xFF keep adding SNPA Addresses to the
             * same TLV
             */

            if ((((u1TlvLen == 0) || ((u1TlvLen + ISIS_SNPA_ADDR_LEN) > 255))
                 && (u2BufSize - *pu2Offset) < ISIS_SNPA_ADDR_LEN + 2)
                || ((u2BufSize - *pu2Offset) < ISIS_SNPA_ADDR_LEN))
            {
                ISIS_DBG_PRINT_ADDR (pAdjRec->au1AdjNbrSNPA,
                                     (UINT1) ISIS_SNPA_ADDR_LEN,
                                     "ADJ <T> : Failed While Encoding SNPA\t",
                                     ISIS_OCTET_STRING);
                return ISIS_FAILURE;
            }

            /* Construct a new TLV if u1TlvLen is ZERO (starting condition)
             * or if TLV Length exceeds 0xFF (maximum size of a TLV)
             */

            if ((u1TlvLen == 0) || (u1TlvLen + ISIS_SNPA_ADDR_LEN > 255))
            {
                if (u1TlvLen != 0)
                {
                    /* Since TLV length in non-zero, we have already added
                     * certain Neighbour Addresses. Fill the Length of the 
                     * TLV since we are now proceeding to construct a new TLV
                     */

                    *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
                    u1TlvLen = 0;
                }

                /* Start with a New TLV. Fill the Code, Length gets filled
                 * either after the TLV is completely filled or if we are done
                 * with all the neighbour addresses
                 */

                *(pu1PDU + *pu2Offset) = ISIS_IS_NEIGH_ADDR_TLV;

                /* Incrementing the offset for Code and Length of the new TLV,
                 * as mentioned Only Code gets filled here, Length later
                 */

                *pu2Offset = (UINT2) (*pu2Offset + 2);
            }

            /* Add the IS Neighbour Address to the TLV. 
             */

            ISIS_ASSIGN_STRING (pu1PDU, *pu2Offset,
                                pAdjRec->au1AdjNbrSNPA, ISIS_SNPA_ADDR_LEN);

            u1TlvLen += ISIS_SNPA_ADDR_LEN;
            *pu2Offset += ISIS_SNPA_ADDR_LEN;
        }
        pAdjRec = pAdjRec->pNext;
    }

    /* Filling the length of the last TLV in the case of IS Neighbour Addresses
     * occupying more than one TLV. 
     */

    if (u1TlvLen != 0)
    {
        *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldISNbrTlv ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjBldPSTlv ()
 * Description   : This routines encodes the configured Protocols into the PDU. 
 *                 If the length of the PDU exceeds the maximum allowed buffer 
 *                 size at any point, the routine returns an Error code and 
 *                 the calling routine must ensure that the PDU is freed
 * Input(s)      : pu1PDU       - Pointer to PDU to be updated  with
 *                                Protocol Support information
 *                 u1ProtoSupp  - Supported protocol in the interface
 * Output(s)     : pu2Offset    - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU       - PDU updated with the Protocol Support TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjBldPSTlv (UINT1 *pu1PDU, UINT2 *pu2Offset, UINT1 u1ProtoSupp)
{
    UINT1               u1TlvLen = 0;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldPSTlv ()\n"));

    /* Encode all the configured Protocols,
     */
    if (u1ProtoSupp == 0)
    {
        return ISIS_SUCCESS;
    }

    *(pu1PDU + *pu2Offset) = ISIS_PROT_SUPPORT_TLV;

    /* Incrementing the offset for Code and Length of the new TLV,
     * as mentioned Only Code gets filled here, Length later
     */

    *pu2Offset = (UINT2) (*pu2Offset + 2);

    if (u1ProtoSupp & ISIS_ADDR_IPV4)
    {
        *(pu1PDU + *pu2Offset) = ISIS_IPV4_SUPP;
        *pu2Offset += 1;
        u1TlvLen += 1;
    }

    if (u1ProtoSupp & ISIS_ADDR_IPV6)
    {
        *(pu1PDU + *pu2Offset) = ISIS_IPV6_SUPP;
        *pu2Offset += 1;
        u1TlvLen += 1;
    }

    if (u1TlvLen != 0)
    {
        *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldPSTlv ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjBldIPV4IfTlv ()
 * Description   : This routines encodes the IP Interface Addreses into the PDU.
 *                 If the length of the PDU exceeds the maximum allowed buffer 
 *                 size at any point, the routine returns an Error code and 
 *                 the calling routine must ensure that the PDU is freed
 * Input(s)      : pContext     - Pointer to System Context
 *                 pu1PDU       - Pointer to PDU to be updated  with
 *                                IP Interface Address information
 *                 u2BufSize    - Maximum size of the Buffer               
 * Output(s)     : pu2Offset    - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU       - PDU updated with the IP Interface Address TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjBldIPV4IfTlv (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                     UINT1 *pu1PDU, UINT2 u2BufSize, UINT2 *pu2Offset)
{
    UINT1               u1TlvLen = 0;
    UINT1               u1AddFlag = ISIS_FALSE;
    tIsisIPIfAddr      *pIPIfRec = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldIPIfTlv ()\n"));

    pIPIfRec = pContext->IPIfTable.pIPIfAddr;

    /* Encode all the configured IP Interface Addresses
     */

    while (pIPIfRec != NULL)
    {
        if ((pIPIfRec->u4CktIfIdx != pCktRec->u4CktIfIdx)
            || (pIPIfRec->u4CktIfSubIdx != pCktRec->u4CktIfSubIdx) ||
            (pIPIfRec->u1AddrType != ISIS_ADDR_IPV4))
        {
            pIPIfRec = pIPIfRec->pNext;
            continue;
        }

        u1AddFlag = ISIS_TRUE;

        /* First check whether there is enough space in the buffer to add
         * IP Interface Addresses. If enough space not available just quit
         *
         * NOTE: (u1TlvLen == 0) or (u1TlvLen + ISIS_MAX_IP_ADDR_LEN) > 0xFF 
         * means that we have to add a new TLV to hold IP/ I/f Address 
         * information. As long as TLV Length is < 0xFF keep adding additional 
         * IP Interface Addresses to the same TLV
         */

        if ((((u1TlvLen == 0) || ((u1TlvLen + ISIS_MAX_IPV4_ADDR_LEN) > 255))
             && (u2BufSize - *pu2Offset) < ISIS_MAX_IPV4_ADDR_LEN + 2)
            || ((u2BufSize - *pu2Offset) < ISIS_MAX_IPV4_ADDR_LEN))
        {
            ISIS_DBG_PRINT_ADDR (pIPIfRec->au1IPAddr,
                                 (UINT1) ISIS_MAX_IPV4_ADDR_LEN,
                                 "ADJ <T> : Failed While Encoding IPv4 Address\t",
                                 ISIS_OCTET_STRING);
            return ISIS_FAILURE;
        }

        /* Construct a new TLV if u1TlvLen is ZERO (starting condition)
         * or if TLV Length exceeds 0xFF (maximum size of a TLV)
         */

        if ((u1TlvLen == 0) || (u1TlvLen + ISIS_MAX_IPV4_ADDR_LEN > 255))
        {
            if (u1TlvLen != 0)
            {
                /* Since TLV length in non-zero, we have already added
                 * certain IP Interface Addresses. Fill the Length of the TLV 
                 * since we are now proceeding to construct a new TLV
                 */

                *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
                u1TlvLen = 0;
            }

            /* Start with a New TLV. Fill the Code, Length gets filled
             * either after the TLV is completely filled or if we are done
             * with all the IP Interface Addresses
             */
            *(pu1PDU + *pu2Offset) = ISIS_IPV4IF_ADDR_TLV;

            /* Incrementing the offset for Code and Length of the new TLV,
             * as mentioned Only Code gets filled here, Length later
             */

            *pu2Offset = (UINT2) (*pu2Offset + 2);
        }

        /* Add the IP Interface Address to the TLV. 
         */

        ISIS_ASSIGN_STRING (pu1PDU, *(pu2Offset), pIPIfRec->au1IPAddr,
                            ISIS_MAX_IPV4_ADDR_LEN);
        u1TlvLen += ISIS_MAX_IPV4_ADDR_LEN;
        *pu2Offset += ISIS_MAX_IPV4_ADDR_LEN;
        pIPIfRec = pIPIfRec->pNext;
    }

    if (u1AddFlag == ISIS_TRUE)
    {
        /* Filling the length of the last IP Interface Address TLV
         */

        *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldIPIfTlv ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjBldIPV6IfTlv ()
 * Description   : This routines encodes the IP Interface Addreses into the PDU.
 *                 If the length of the PDU exceeds the maximum allowed buffer 
 *                 size at any point, the routine returns an Error code and 
 *                 the calling routine must ensure that the PDU is freed
 * Input(s)      : pContext     - Pointer to System Context
 *                 pu1PDU       - Pointer to PDU to be updated  with
 *                                IP Interface Address information
 *                 u2BufSize    - Maximum size of the Buffer               
 * Output(s)     : pu2Offset    - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU       - PDU updated with the IP Interface Address TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjBldIPV6IfTlv (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                     UINT1 *pu1PDU, UINT2 u2BufSize, UINT2 *pu2Offset)
{
    UINT1               u1TlvLen = 0;
    UINT1               u1AddFlag = ISIS_FALSE;
    UINT4               u4Status;
    tIsisIPIfAddr      *pIPIfRec = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldIPIfTlv ()\n"));

    pIPIfRec = pContext->IPIfTable.pIPIfAddr;

    /* Encode all the configured IP Interface Addresses
     */

    while (pIPIfRec != NULL)
    {
        if ((pIPIfRec->u4CktIfIdx != pCktRec->u4CktIfIdx)
            || (pIPIfRec->u4CktIfSubIdx != pCktRec->u4CktIfSubIdx)
            || (pIPIfRec->u1AddrType != ISIS_ADDR_IPV6))

        {
            pIPIfRec = pIPIfRec->pNext;
            continue;
        }
        ISIS_IN6_IS_ADDR_LINKLOCAL (pIPIfRec->au1IPAddr, u4Status);
        /*Link Local Check */
        if (u4Status == ISIS_FALSE)
        {
            pIPIfRec = pIPIfRec->pNext;
            continue;
        }

        u1AddFlag = ISIS_TRUE;

        /* First check whether there is enough space in the buffer to add
         * IP Interface Addresses. If enough space not available just quit
         *
         * NOTE: (u1TlvLen == 0) or (u1TlvLen + ISIS_MAX_IP_ADDR_LEN) > 0xFF 
         * means that we have to add a new TLV to hold IP/ I/f Address 
         * information. As long as TLV Length is < 0xFF keep adding additional 
         * IP Interface Addresses to the same TLV
         */

        if ((((u1TlvLen == 0) || ((u1TlvLen + ISIS_MAX_IPV6_ADDR_LEN) > 255))
             && (u2BufSize - *pu2Offset) < ISIS_MAX_IPV6_ADDR_LEN + 2)
            || ((u2BufSize - *pu2Offset) < ISIS_MAX_IPV6_ADDR_LEN))
        {
            ISIS_DBG_PRINT_ADDR (pIPIfRec->au1IPAddr,
                                 (UINT1) ISIS_MAX_IPV6_ADDR_LEN,
                                 "ADJ <T> : Failed While Encoding IPv6 Address\t",
                                 ISIS_OCTET_STRING);
            return ISIS_FAILURE;
        }

        /* Construct a new TLV if u1TlvLen is ZERO (starting condition)
         * or if TLV Length exceeds 0xFF (maximum size of a TLV)
         */

        if ((u1TlvLen == 0) || (u1TlvLen + ISIS_MAX_IPV6_ADDR_LEN > 255))
        {
            if (u1TlvLen != 0)
            {
                /* Since TLV length in non-zero, we have already added
                 * certain IP Interface Addresses. Fill the Length of the TLV 
                 * since we are now proceeding to construct a new TLV
                 */

                *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
                u1TlvLen = 0;
            }

            /* Start with a New TLV. Fill the Code, Length gets filled
             * either after the TLV is completely filled or if we are done
             * with all the IP Interface Addresses
             */
            *(pu1PDU + *pu2Offset) = ISIS_IPV6IF_ADDR_TLV;

            /* Incrementing the offset for Code and Length of the new TLV,
             * as mentioned Only Code gets filled here, Length later
             */

            *pu2Offset = (UINT2) (*pu2Offset + 2);
        }

        /* Add the IP Interface Address to the TLV. 
         */

        ISIS_ASSIGN_STRING (pu1PDU, *(pu2Offset), pIPIfRec->au1IPAddr,
                            ISIS_MAX_IPV6_ADDR_LEN);
        u1TlvLen += ISIS_MAX_IPV6_ADDR_LEN;
        *pu2Offset += ISIS_MAX_IPV6_ADDR_LEN;
        pIPIfRec = pIPIfRec->pNext;
    }

    if (u1AddFlag == ISIS_TRUE)
    {
        /* Filling the length of the last IP Interface Address TLV
         */

        *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldIPIfTlv ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjRestartTlv ()
 * Description   : This routines encodes the restart information into the PDU.
 *                 If the length of the PDU exceeds the maximum allowed buffer 
 *                 size at any point, the routine returns an Error code and 
 *                 the calling routine must ensure that the PDU is freed
 * Input(s)      : pContext     - Pointer to System Context
 *                 pu1PDU       - Pointer to PDU to be updated  with
 *                                IP Interface Address information
 *                 u2BufSize    - Maximum size of the Buffer               
 * Output(s)     : pu2Offset    - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU       - PDU updated with the IP Interface Address TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjRestartTlv (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                   UINT1 *pu1PDU, UINT2 u2BufSize, UINT2 *pu2Offset)
{

    UINT1               u1Flag = 0;
    UINT4               u4T3TimerInterval = 0;
    tIsisTimer         *pTimer;
    UINT4               u4RemGracePeriod = 0;
    tIsisAdjEntry      *pAdjRec = NULL;
    UINT1               au1SysId[ISIS_SYS_ID_LEN];

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjRestartTlv ()\n"));

    /* Validation - to check whether buffer is sufficient to hold Restart TLV */

    MEMSET (au1SysId, 0, ISIS_SYS_ID_LEN);
    if ((u2BufSize - *pu2Offset) <
        (ISIS_RESTART_TLV_HDR + FLAG + REM_TIME + ISIS_SYS_ID_LEN))
    {
        return ISIS_FAILURE;
    }

    /* Type */
    ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, ISIS_RESTART_TLV);
    *pu2Offset = (UINT2) (*pu2Offset + 1);

    /*Value */

    /*Planned start */
    if (pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER)
    {
        if (pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER)
        {
            if ((pCktRec->u1IsisGRRestartHelloTxCount <
                 pContext->u1IsisGRMaxT1RetryCount)
                && ((pCktRec->u1IsisGRRestartL1State & ISIS_GR_ACK_RCVD) !=
                    ISIS_GR_ACK_RCVD))
            {
                u1Flag = RR_SET;    /*Sending Restart request */
                if ((pCktRec->u1IsisGRRestartHelloTxCount == 0)
                    && (pContext->u1IsisGRRestarterState == ISIS_GR_SHUTDOWN))
                {
                    u4T3TimerInterval = pContext->u2IsisGRT3TimerInterval;
                }
                else
                {
                    /*Except the first restart hello, before shutdown, rest of the
                       time rem period  *should be sent */
                    pTimer = &(pContext->SysTimers.SysRestartT3Timer);
                    TmrGetRemainingTime (gIsisTmrListID, &(pTimer->Timer),
                                         &u4RemGracePeriod);
                    u4T3TimerInterval = (UINT2) u4RemGracePeriod;
                }
            }
        }
        else if (pContext->u1IsisGRRestartStatus == ISIS_STARTING_ROUTER)
        {
            if ((pCktRec->u1IsisGRRestartL1State & ISIS_GR_ACK_RCVD) !=
                ISIS_GR_ACK_RCVD)
            {
                if ((pCktRec->u1IsisGRRestartHelloTxCount == 0)
                    || pCktRec->u1IsisGRRestartHelloTxCount >=
                    pContext->u1IsisGRMaxT1RetryCount)
                {
                    u1Flag = SA_SET;    /*Unplanned , suppress adjacency */
                }
                else if (pCktRec->u1IsisGRRestartHelloTxCount <
                         pContext->u1IsisGRMaxT1RetryCount)
                {
                    u1Flag = RR_SA_SET;    /*Unplanned with both suppress adjacency 
                                           and Restart request */
                }
            }
        }
    }
    else if (pContext->u1IsisGRRestartMode == ISIS_GR_HELPER)
    {
        pAdjRec = pCktRec->pAdjEntry;
        while (pAdjRec != NULL)
        {
            if ((pAdjRec->u1IsisGRHelperStatus == ISIS_GR_HELPING) &&
                (pAdjRec->u1HelperAckState == ISIS_HELPER_RA_SEND))
            {
                MEMCPY (au1SysId, pAdjRec->au1AdjNbrSysID, ISIS_SYS_ID_LEN);
                /*Get the Remaining time and update */
                u4T3TimerInterval = pAdjRec->u2AdjGRTime;
                u1Flag = RA_SET;    /*Updating Restart Ack */
                pAdjRec->u1HelperAckState = ISIS_HELPER_NONE;
                break;
            }
            pAdjRec = pAdjRec->pNext;
        }
    }
    /*Length */
    if (u1Flag == RA_SET)
    {
        ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset,
                            (FLAG + REM_TIME + ISIS_SYS_ID_LEN));
    }
    else
    {
        ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, (FLAG + REM_TIME));
    }
    *pu2Offset = (UINT2) (*pu2Offset + 1);

    ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, u1Flag);    /*Flag */
    *pu2Offset = (UINT2) (*pu2Offset + FLAG);
    ISIS_ASSIGN_2_BYTES (pu1PDU, *pu2Offset, u4T3TimerInterval);    /*Remaining time */
    *pu2Offset = (UINT2) (*pu2Offset + REM_TIME);
    if (u1Flag == RA_SET)
    {
        ISIS_ASSIGN_STRING (pu1PDU, *pu2Offset, au1SysId, ISIS_SYS_ID_LEN);
        *pu2Offset = (UINT2) (*pu2Offset + ISIS_SYS_ID_LEN);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldIPIfTlv ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisAdjBldMTTlv ()
 * Description   : This routines encodes the MT TLVs into the PDU.
 *                 If the interfaces supports
 *                 ipv4 alone  - nothing will be added
 *                 ipv4 & ipv6 - Two MT TLVs will be added (MT 0 & MT 2)
 *                 ipv6 alone  - MT 2 TLV alone will be addde
 * Input(s)      : pCktRec     - Pointer to the circuit record 
 *                 pu1PDU      - Pointer to PDU to be updated  with
 *                               MT TLV information
 *                 u2BufSize   - Maximum size of the Buffer               
 *                 pu2Offset   - Last written byte.
 * Output(s)     : pu2Offset   - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU      - PDU updated with the MT TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/
PUBLIC INT4
IsisAdjBldMTTlv (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                 UINT2 u2BufSize, UINT2 *pu2Offset)
{
    UINT1               u1TlvLen = 0;
    UINT2               u2MTId = 0;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldMTTlv ()\n"));

    /* If MT #0 is alone supported, then no need to add MT TLV */
    if (pCktRec->u1IfMTId == ISIS_MT0_BITMAP)
    {
        return ISIS_SUCCESS;
    }

    for (u2MTId = 0; u2MTId <= ISIS_MAX_MT; u2MTId++)
    {

        /* Left shift is done to get the corresponding MT_BIT_MAP values
         * ex. for MT #0, (1 << 0) = 1 (ISIS_MT0_BITMAP) 
         *     for MT #2, (1 << 2) = 4 (ISIS_MT2_BITMAP)
         */
        if (pCktRec->u1IfMTId & (1 << u2MTId))
        {
            /* Check whether there is enough space in the buffer to add
             * MT TLV. If enough space not available just quit
             */
            if (((u1TlvLen == 0)
                 && ((u2BufSize - *pu2Offset) < (ISIS_MT_ID_LEN + 2)))
                || ((u1TlvLen != 0)
                    && ((u2BufSize - *pu2Offset) < ISIS_MT_ID_LEN)))
            {
                return ISIS_FAILURE;
            }

            if (u1TlvLen == 0)
            {
                u1TlvLen = ISIS_MT_ID_LEN;

                /* Fill the Code for MT TLV */

                *(pu1PDU + *pu2Offset) = ISIS_MT_TLV;

                /* Incrementing the offset for Code and Length of the new TLV.
                 * Length will be filled later */
                *pu2Offset = (UINT2) (*pu2Offset + 2);

                ISIS_ASSIGN_2_BYTES (pu1PDU, *pu2Offset, u2MTId);
                *pu2Offset = (UINT2) (*pu2Offset + ISIS_MT_ID_LEN);
            }
            else
            {
                u1TlvLen = (UINT1) (u1TlvLen + ISIS_MT_ID_LEN);
                ISIS_ASSIGN_2_BYTES (pu1PDU, *pu2Offset, u2MTId);
                *pu2Offset = (UINT2) (*pu2Offset + ISIS_MT_ID_LEN);
            }
        }
    }

    *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldMTTlv ()\n"));
    return ISIS_SUCCESS;
}

/* P2P three way handshake*/
/******************************************************************************
 * Function Name : IsisAdjBldP2PThreeWayHndShkTLV ()
 * Description   : This routines encodes the 3-way handshake TLV (240) into the PDU.
 *                 If the interfaces supports
 *                 3-way handshake option and configured as Point-to-point
 * Input(s)      : pCktRec     - Pointer to the circuit record 
 *                 pu1PDU      - Pointer to PDU to be updated  with
 *                               MT TLV information
 *                 u2BufSize   - Maximum size of the Buffer               
 *                 pu2Offset   - Last written byte.
 * Output(s)     : pu2Offset   - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU      - PDU updated with the MT TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/
PUBLIC INT4
IsisAdjBldP2PThreeWayHndShkTLV (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                                UINT2 u2BufSize, UINT2 *pu2Offset)
{
    UINT1               u1TlvLen = 0;
    UINT1               u1State = ISIS_ADJ_DOWN;
    tIsisAdjEntry      *pAdjRec = NULL;

    ADP_EE ((ISIS_LGST,
             "ADJ <X> : Entered IsisAdjBldP2PThreeWayHndShkTLV ()\n"));

    if ((u2BufSize - *pu2Offset) < (ISIS_P2P_THREE_WAY_HS_MAX_LEN + 2))
    {
        return ISIS_FAILURE;
    }

    /*Mandatory fields to be encode (T (1) + L (1) + V 
     * (state + ext. local circuit ID = 5) */

    u1TlvLen = ISIS_P2P_THREE_WAY_STATE_LEN;

    /* Fill the Code for MT TLV */
    *(pu1PDU + *pu2Offset) = ISIS_P2P_THREE_WAY_ADJ_TLV;
    /* Incrementing the offset for Code and Length of the new TLV.
     * Length will be filled later */
    *pu2Offset = (UINT2) (*pu2Offset + 2);
    pAdjRec = pCktRec->pAdjEntry;
    if (pAdjRec != NULL)
    {
        u1State = pAdjRec->u1P2PThreewayState;
    }
    ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, u1State);
    *pu2Offset = (UINT2) (*pu2Offset + ISIS_P2P_THREE_WAY_STATE_LEN);

    ISIS_ASSIGN_4_BYTES (pu1PDU, *pu2Offset, pCktRec->u4ExtLocalCircID);
    *pu2Offset = (UINT2) (*pu2Offset + ISIS_P2P_EXT_CKT_ID_LEN);
    u1TlvLen += ISIS_P2P_EXT_CKT_ID_LEN;

    /*once adjacency is formed (Neighbor SysID (6) + neighbor ext. circuit ID (4) */
    /* In case of P2P circuit only one adjacency record would be present 
     * so we need not loop through the AdjRec - and sufficient to process only the first 
     * valid record */
    if ((pAdjRec != NULL) && (pAdjRec->u1P2PThreewayState != ISIS_ADJ_DOWN))
    {
        /* A valid Point to point adjacency is present so encode the neighbor related fields */
        ISIS_ASSIGN_STRING (pu1PDU, *pu2Offset, pAdjRec->au1AdjNbrSysID,
                            ISIS_SYS_ID_LEN);
        u1TlvLen += (ISIS_SNPA_ADDR_LEN + ISIS_P2P_EXT_CKT_ID_LEN);
        *pu2Offset += ISIS_SNPA_ADDR_LEN;
        ISIS_ASSIGN_4_BYTES (pu1PDU, *pu2Offset, pAdjRec->u4NeighExtCircuitID);
        *pu2Offset = (UINT2) (*pu2Offset + ISIS_P2P_EXT_CKT_ID_LEN);
    }

    /*update the length field for the TLV */
    *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;

    ADP_EE ((ISIS_LGST,
             "ADJ <X> : Exiting IsisAdjBldP2PThreeWayHndShkTLV ()\n"));
    return ISIS_SUCCESS;
}

#ifdef BFD_WANTED
/******************************************************************************
 * Function Name : IsisAdjBldBFDEnabledTlv ()
 * Description   : This routines encodes the BFD Enabled TLV into the PDU.
 *                 If the interfaces supports
 *                 ipv4 - MT 0 and NLPID for IPv4 will be added
 *                 ipv6 - MT 2 and NLPID for IPv6 will be added
 * Input(s)      : pCktRec     - Pointer to the circuit record 
 *                 pu1PDU      - Pointer to PDU to be updated  with
 *                               MT TLV information
 *                 u2BufSize   - Maximum size of the Buffer               
 *                 pu2Offset   - Last written byte.
 * Output(s)     : pu2Offset   - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU      - PDU updated with the MT TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 ******************************************************************************/
PUBLIC INT4
IsisAdjBldBFDEnabledTlv (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                         UINT2 u2BufSize, UINT2 *pu2Offset)
{
    UINT1               u1TlvLen = 0;
    UINT1               u1NLPId = 0;
    UINT2               u2MTId = 0;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjBldMTTlv ()\n"));

    for (u2MTId = 0; u2MTId <= ISIS_MAX_MT; u2MTId++)
    {
        /* Left shift is done to get the corresponding MT_BIT_MAP values
         * ex. for MT #0, (1 << 0) = 1 (ISIS_MT0_BITMAP) 
         *     for MT #2, (1 << 2) = 4 (ISIS_MT2_BITMAP)
         */
        if (pCktRec->u1IfMTId & (1 << u2MTId))
        {
            /* Check whether there is enough space in the buffer to add
             * MT TLV. If enough space not available just quit
             */
            u1NLPId = ((u2MTId == 0) ? ISIS_IPV4_SUPP : ISIS_IPV6_SUPP);
            if (((u1TlvLen == 0)
                 && ((u2BufSize - *pu2Offset) < (ISIS_BFD_MT_NLPID_LEN + 2)))
                || ((u1TlvLen != 0)
                    && ((u2BufSize - *pu2Offset) < ISIS_BFD_MT_NLPID_LEN)))
            {
                return ISIS_FAILURE;
            }

            if (u1TlvLen == 0)
            {
                u1TlvLen = ISIS_BFD_MT_NLPID_LEN;

                /* Fill the Code for BFD Enabled TLV */

                *(pu1PDU + *pu2Offset) = ISIS_BFD_ENABLED_TLV;

                /* Incrementing the offset for Code and Length of the new TLV.
                 * Length will be filled later */
                *pu2Offset = (UINT2) (*pu2Offset + 2);

                ISIS_ASSIGN_2_BYTES (pu1PDU, *pu2Offset, u2MTId);
                *pu2Offset = (UINT2) (*pu2Offset + 2);
                ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, u1NLPId);
                *pu2Offset = (UINT2) (*pu2Offset + 1);
            }
            else
            {
                u1TlvLen = (UINT1) (u1TlvLen + ISIS_BFD_MT_NLPID_LEN);
                ISIS_ASSIGN_2_BYTES (pu1PDU, *pu2Offset, u2MTId);
                *pu2Offset = (UINT2) (*pu2Offset + 2);
                ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, u1NLPId);
                *pu2Offset = (UINT2) (*pu2Offset + 1);
            }
        }
    }

    *(pu1PDU + (*pu2Offset) - u1TlvLen - 1) = u1TlvLen;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjBldBFDEnabledTlv ()\n"));
    return ISIS_SUCCESS;
}
#endif
