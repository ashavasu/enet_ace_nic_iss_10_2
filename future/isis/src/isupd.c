/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isupd.c,v 1.30 2017/09/11 13:44:08 siva Exp $
 *
 *   Description: This file contains the main Routines associated with
 *                Update Module. 
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE INT4        IsisUpdCheckIfAdjacent (tIsisSysContext * pContext,
                                            UINT1 *pu1LSPBuf, UINT1 *pu1SysId,
                                            UINT1);
PRIVATE VOID        IsisUpdProcSelfLSP (tIsisSysContext *, UINT1 *,
                                        tIsisCktEntry *, UINT1);
PRIVATE UINT1       IsisUpdProcLETLV (tIsisSysContext *, UINT1 *, VOID *,
                                      VOID *, tIsisCktEntry *, UINT1,
                                      tIsisHashBucket *, UINT1);
PRIVATE VOID        IsisUpdConstructPSNP (tIsisSysContext *, UINT1 *,
                                          tIsisCktEntry *, UINT1, UINT1);
PRIVATE VOID        IsisUpdEnterWaitingState (tIsisSysContext *, UINT1);
PRIVATE VOID        IsisUpdClearPSNP (tIsisSysContext *, UINT1 *,
                                      tIsisCktEntry *, UINT1);

PRIVATE VOID        IsisUpdProcessZeroRLTLSP (tIsisSysContext *,
                                              tIsisCktEntry *, UINT1 *, UINT1);

PRIVATE INT4        IsisUpdValidateLSP (tIsisSysContext *, tIsisCktEntry *,
                                        UINT1 *, UINT1);

PRIVATE INT4        IsisUpdChkCktMask (tIsisSysContext *, UINT1, UINT1);

PRIVATE VOID        IsisUpdProcCSNPEntries (tIsisSysContext *, UINT1 *,
                                            tIsisCktEntry *, UINT1);
PRIVATE VOID        IsisUpdProcEvent (tIsisSysContext *, tIsisMsg *);

PRIVATE VOID        IsisUpdGRLSPCheck (tIsisSysContext *, UINT1 *, UINT1);

/******************************************************************************
 * Function    : IsisUpdProcCtrlPkt ()
 * Description : This function is the entry point function to Update module,
 *               which decodes the message and schedules various update
 *               functions accordingly.
 * Input(s)    : pIsisMsg - Pointer to the received Message
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcCtrlPkt (tIsisMsg * pIsisMsg)
{
    tIsisCktEntry      *pCktRec = NULL;
    tIsisSysContext    *pContext = NULL;
    UINT2               u2LLIfIndex = 0;
    UINT4               u4SysInstIdx = 0;
    INT4                i4RetVal = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcCtrlPkt ()\n"));

    switch (pIsisMsg->u1MsgType)
    {
        case ISIS_L1LSP_PDU:
        case ISIS_L2LSP_PDU:

            u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
            i4RetVal = IsisAdjGetCktRecWithLLHandle (u2LLIfIndex, &pCktRec);
            if (i4RetVal != ISIS_SUCCESS)
            {
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                pIsisMsg = NULL;
                DLP_PT ((ISIS_LGST,
                         "UPD <T> : Data Received On Erroneous Interface [ %u ]\n",
                         u2LLIfIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
                break;
            }

            pContext = pCktRec->pContext;

            ISIS_INCR_RCVD_LSP (pCktRec,
                                ISIS_GET_LVL_FROM_PTYPE (pIsisMsg->u1MsgType));

            IsisUpdProcLSP (pContext, pIsisMsg,
                            (UINT1) (ISIS_GET_LVL_FROM_PTYPE
                                     (pIsisMsg->u1MsgType)), pCktRec);
            break;

        case ISIS_L1CSNP_PDU:
        case ISIS_L2CSNP_PDU:
            u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
            i4RetVal = IsisAdjGetCktRecWithLLHandle (u2LLIfIndex, &pCktRec);
            if (i4RetVal != ISIS_SUCCESS)
            {
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                pIsisMsg = NULL;
                DLP_PT ((ISIS_LGST,
                         "UPD <T> : Data Received On Erroneous Interface [ %u ]\n",
                         u2LLIfIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
                break;
            }

            pContext = pCktRec->pContext;

            if ((pIsisMsg->u1MsgType == ISIS_L1CSNP_PDU)
                && (pCktRec->u1CktType == ISIS_BC_CKT)
                && (pCktRec->pL1CktInfo->bIsDIS == ISIS_TRUE) &&
                (pContext->u1IsisGRRestartMode != ISIS_GR_RESTARTER))
            {
                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                UPP_PT ((ISIS_LGST,
                         "UPD <E> : Self DIS - L1 CSNP Dropped - Interface [%u]\n",
                         u2LLIfIndex));
                return;
            }
            else if ((pIsisMsg->u1MsgType == ISIS_L2CSNP_PDU)
                     && (pCktRec->u1CktType == ISIS_BC_CKT)
                     && (pCktRec->pL2CktInfo->bIsDIS == ISIS_TRUE) &&
                     (pContext->u1IsisGRRestartMode != ISIS_GR_RESTARTER))
            {
                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                UPP_PT ((ISIS_LGST,
                         "UPD <E> : Self DIS - L2 CSNP Dropped - Interface [%u]\n",
                         u2LLIfIndex));
                return;
            }

            ISIS_INCR_RCVD_CSNP (pCktRec,
                                 ISIS_GET_LVL_FROM_PTYPE (pIsisMsg->u1MsgType));

            IsisUpdProcCSNP (pContext, pIsisMsg, pCktRec,
                             (UINT1) ISIS_GET_LVL_FROM_PTYPE (pIsisMsg->
                                                              u1MsgType));
            break;

        case ISIS_L1PSNP_PDU:
        case ISIS_L2PSNP_PDU:
            u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
            i4RetVal =
                (UINT2) IsisAdjGetCktRecWithLLHandle (u2LLIfIndex, &pCktRec);
            if (i4RetVal != ISIS_SUCCESS)
            {
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

                DLP_PT ((ISIS_LGST,
                         "UPD <T> : Data Received On Erroneous Interface [ %u ]\n",
                         u2LLIfIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
                break;
            }

            pContext = pCktRec->pContext;

            if ((pIsisMsg->u1MsgType == ISIS_L1PSNP_PDU)
                && (pCktRec->u1CktType == ISIS_BC_CKT)
                && (pCktRec->pL1CktInfo->bIsDIS != ISIS_TRUE))
            {
                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

                UPP_PT ((ISIS_LGST,
                         "UPD <E> : [BC] Self Not DIS, PSNP Dropped - Interface [%u]\n",
                         u2LLIfIndex));
                return;
            }
            else if ((pIsisMsg->u1MsgType == ISIS_L2PSNP_PDU)
                     && (pCktRec->u1CktType == ISIS_BC_CKT)
                     && (pCktRec->pL2CktInfo->bIsDIS != ISIS_TRUE))
            {
                ISIS_INCR_SYS_DROPPED_PDUS (pContext);
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

                UPP_PT ((ISIS_LGST,
                         "UPD <E> : [BC] Self Not DIS, PSNP Dropped - Interface [%u]\n",
                         u2LLIfIndex));
                return;
            }

            ISIS_INCR_RCVD_PSNP (pCktRec,
                                 ISIS_GET_LVL_FROM_PTYPE (pIsisMsg->u1MsgType));

            IsisUpdProcPSNP (pContext, pIsisMsg, pCktRec,
                             (UINT1) ISIS_GET_LVL_FROM_PTYPE
                             (pIsisMsg->u1MsgType));
            break;

        case ISIS_MSG_EVENT:

            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_SUCCESS)
            {
                IsisUpdProcEvent (pContext, pIsisMsg);
            }
            else
            {
                ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            }

            break;

        default:

            /* Spurious Message - Ignore and Discard
             */

            WARNING ((ISIS_LGST,
                      "UPD <E> : Invalid Message - Discarded [ %u ]\n",
                      (pIsisMsg->u1MsgType)));
            IsisFreeDLLBuf (pIsisMsg);
            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCtrlPkt ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcEvent ()
 * Description : This function process the various events.
 * Input(s)    : pContext - Pointer to the system context.
 *               pIsisMsg - Pointer to the received Message
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PRIVATE VOID
IsisUpdProcEvent (tIsisSysContext * pContext, tIsisMsg * pIsisMsg)
{
    VOID               *pEvent = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    tIsisIPIfAddr      *pTempIfRec = NULL;
    UINT1               au1Metric[ISIS_NUM_METRICS];
    UINT4               u4FullMetric = 0;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4CktIdx = 0;
    UINT1               au1AdjSysId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
    UINT1               u1Level = 0;
    UINT1               u1Status = 0;
    UINT1               u1AdjUsage = 0;
    UINT1               u1MtIndex = 0;

    MEMSET (au1Metric, 0, sizeof (au1Metric));

    /* Some of the routines in the switch statement does not require
     * the actual Event structure since the data associated with the
     * event can be retrieved from the Context. In such cases the Event
     * and the ISIS message is released immediately after processing
     * the Event. In the other cases the Event structure is freed by
     * the called routines where as the ISIS message is freed in this
     * routine.
     */

    switch (*(UINT1 *) (pIsisMsg->pu1Msg))
    {
        case ISIS_EVT_IS_UP:

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : ISIS UP - Context [ %u ], System Type [ %s ]\n",
                     pContext->u4SysInstIdx,
                     ISIS_GET_SYS_TYPE_STR (pContext->SysActuals.u1SysType)));

            /* Process the Intermediate System UP Event which is
             * generated when the RowStatus for the specified context
             * is made ACTIVE
             */

            /* Standby Node need not process IS Up Event. TLVs in
             * pContext->SelfLSP will be updated when Self LSPs are
             * received in the Standby Node
             */

            IsisUpdProcISUpEvt (pContext);

            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_IS_DOWN:

            /* Process the Intermediate System DOWN Event which is
             * generated when the RowStatus for the specified context
             * is marked as DESTROY or NOT_IN_SERVICE or AdminState is
             * marked as DOWN
             */

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : ISIS DOWN - Context [ %u ], System Type [ %s ]\n",
                     pContext->u4SysInstIdx,
                     ISIS_GET_SYS_TYPE_STR (pContext->SysActuals.u1SysType)));

            IsisUpdProcISDownEvt (pContext);

            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_CKT_CHANGE:

            pEvent = (tIsisEvtCktChange *) (VOID *) (pIsisMsg->pu1Msg);
            u1Status = ((tIsisEvtCktChange *) pEvent)->u1Status;
            u1Level = ((tIsisEvtCktChange *) pEvent)->u1CktLevel;
            u4CktIdx = ((tIsisEvtCktChange *) pEvent)->u4CktIdx;

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Circuit Change - Index [ %u ], Status [ %s ], Level [ %s ]\n",
                     u4CktIdx, ISIS_GET_ISIS_CKT_STATUS (u1Status),
                     ISIS_GET_SYS_TYPE_STR (u1Level)));

            /* Everytime Circuit information changes (it can only be
             * Metric), all the four Metrics are updated in the 
             * Self-LSPs. 
             */
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                u4FullMetric = ((tIsisEvtCktChange *) pEvent)->u4FullMetric;
            }
            else
            {
                MEMCPY (au1Metric, ((tIsisEvtCktChange *) pEvent)->Metric,
                        ISIS_NUM_METRICS);
            }

            i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

            if (i4RetVal == ISIS_SUCCESS)
            {
                if (u1Status == ISIS_CKT_UP)
                {
                    IsisUpdProcCktUp (pContext, pCktRec, u1Level);
                }
                else if (u1Status == ISIS_CKT_DOWN)
                {
                    IsisUpdProcCktDown (pContext, pCktRec, u1Level);
                }
                else if (u1Status == ISIS_CKT_MODIFY)
                {
                    IsisUpdProcCktModify (pContext, pCktRec, au1Metric,
                                          u4FullMetric, u1Level);
                }
                else if (u1Status == ISIS_CKT_DESTROY)
                {
                    IsisUpdProcCktDown (pContext, pCktRec, u1Level);

                    if (pCktRec->u1ProtoSupp == ISIS_ADDR_IPV4)
                    {
                        IsisCtrlDelAutoIPRA (pCktRec->pContext,
                                             pCktRec->u4CktIfIdx,
                                             pCktRec->u4CktIfSubIdx,
                                             ISIS_ADDR_IPV4);
                    }
                    else if (pCktRec->u1ProtoSupp == ISIS_ADDR_IPV6)
                    {
                        IsisCtrlDelAutoIPRA (pCktRec->pContext,
                                             pCktRec->u4CktIfIdx,
                                             pCktRec->u4CktIfSubIdx,
                                             ISIS_ADDR_IPV6);
                    }
                    else
                    {
                        IsisCtrlDelAutoIPRA (pCktRec->pContext,
                                             pCktRec->u4CktIfIdx,
                                             pCktRec->u4CktIfSubIdx,
                                             ISIS_ADDR_IPV4);
                        IsisCtrlDelAutoIPRA (pCktRec->pContext,
                                             pCktRec->u4CktIfIdx,
                                             pCktRec->u4CktIfSubIdx,
                                             ISIS_ADDR_IPV6);
                    }
                    pIPIfRec = pContext->IPIfTable.pIPIfAddr;
                    while (pIPIfRec != NULL)
                    {
                        if ((pIPIfRec->u4CktIfIdx == pCktRec->u4CktIfIdx)
                            && (pIPIfRec->u4CktIfSubIdx ==
                                pCktRec->u4CktIfSubIdx))
                        {
                            pTempIfRec = pIPIfRec->pNext;
                            if (pIPIfRec->u1AddrType == ISIS_ADDR_IPV6)
                            {
                                i4RetVal =
                                    IsisCtrlDelIPIf (pContext,
                                                     pCktRec->u4CktIfIdx,
                                                     pCktRec->u4CktIfSubIdx,
                                                     pIPIfRec->au1IPAddr,
                                                     ISIS_ADDR_IPV6);
                            }
                            else
                            {
                                i4RetVal =
                                    IsisCtrlDelIPIf (pContext,
                                                     pCktRec->u4CktIfIdx,
                                                     pCktRec->u4CktIfSubIdx,
                                                     pIPIfRec->au1IPAddr,
                                                     ISIS_ADDR_IPV4);

                            }
                            pIPIfRec = pTempIfRec;
                            pTempIfRec = NULL;
                        }
                        else
                        {
                            pIPIfRec = pIPIfRec->pNext;
                        }
                    }
                }

                if ((u1Status == ISIS_CKT_DOWN)
                    || (u1Status == ISIS_CKT_DESTROY))
                {
                    /*Change in circuit */
                    /*Scanning the circuit to check the helping adjacencies */
                    for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
                         pCktRec = pCktRec->pNext)

                    {
                        pAdjRec = pCktRec->pAdjEntry;
                        /*To find ,to whom should we stop helping */
                        /*Interface shutdown or up, will hit the state change */

                        while (pAdjRec != NULL)
                        {
                            if (pAdjRec->u1IsisGRHelperStatus ==
                                ISIS_GR_HELPING)
                            {
                                IsisGrExitHelper (pContext, pAdjRec,
                                                  ISIS_GR_HELPER_TOP_CHG);
                            }
                            pAdjRec = pAdjRec->pNext;
                        }
                    }
                }

            }
            else
            {
                /* Spurious Event. Ignore. 
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Circuit Change - Circuit Doest Not Exist - Index [ %u ]\n",
                         u4CktIdx));
            }

            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_ADJ_CHANGE:

            pEvent = (tIsisEvtAdjChange *) (VOID *) (pIsisMsg->pu1Msg);

            u1Status = ((tIsisEvtAdjChange *) pEvent)->u1Status;
            u4CktIdx = ((tIsisEvtAdjChange *) pEvent)->u4CktIdx;
            u1AdjUsage = ((tIsisEvtAdjChange *) pEvent)->u1AdjUsage;

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adjacency Change - Index [ %u ], Status [ %s ], Level [ %u ]\n",
                     u4CktIdx, ISIS_GET_ISIS_ADJ_STATUS (u1Status),
                     ISIS_GET_ADJ_USAGE_WITH_CKTLVL (u1AdjUsage)));

            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                u4FullMetric = ((tIsisEvtAdjChange *) pEvent)->u4FullMetric;
            }
            else
            {
                MEMCPY (au1Metric, ((tIsisEvtAdjChange *) pEvent)->Metric,
                        ISIS_NUM_METRICS);
            }

            i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

            if (i4RetVal == ISIS_SUCCESS)
            {
                MEMCPY (au1AdjSysId,
                        ((tIsisEvtAdjChange *) pEvent)->au1AdjSysID,
                        ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                if ((u1Status == ISIS_ADJ_UP) || (u1Status == ISIS_ADJ_DOWN))
                {
                    i4RetVal
                        = IsisUpdProcAdjChange (pContext, pCktRec,
                                                au1Metric, u4FullMetric,
                                                u1AdjUsage, au1AdjSysId,
                                                u1Status);
                }
            }
            else
            {
                /* Spurious Event. Ignore
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Adjacency Change - Circuit Does Not Exist - Index [ %u ]\n",
                         u4CktIdx));
            }

            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_DIS_CHANGE:

            pEvent = (tIsisEvtDISChg *) (VOID *) (pIsisMsg->pu1Msg);
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Processing DIS Change - Circuit - Index [ %u ] Level [%s]\n",
                     ((tIsisEvtDISChg *) pEvent)->u4CktIdx,
                     ISIS_GET_LEVEL_STR (((tIsisEvtDISChg *) pEvent)->
                                         u1CktLevel)));
            IsisUpdProcDISChgEvt (pContext, (tIsisEvtDISChg *) pEvent);

            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_CHG_IN_DIS_STAT:

            pEvent = (tIsisEvtDISStatChg *) (VOID *) (pIsisMsg->pu1Msg);

            u1Status = ((tIsisEvtDISStatChg *) pEvent)->u1Status;
            u4CktIdx = ((tIsisEvtDISStatChg *) pEvent)->u4CktIdx;
            u1Level = ((tIsisEvtDISStatChg *) pEvent)->u1Level;

            i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

            if (i4RetVal == ISIS_SUCCESS)
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Processing DIS State Change - Circuit - Index [ %u ] Level [%s] State [%s]\n",
                         ((tIsisEvtDISChg *) pEvent)->u4CktIdx,
                         ISIS_GET_LEVEL_STR (((tIsisEvtDISChg *) pEvent)->
                                             u1CktLevel),
                         ISIS_GET_DIS_STATE (u1Status)));
                IsisUpdProcDISStatChgEvt (pContext, pCktRec, u1Level, u1Status);
            }
            else
            {
                /* Spurious Event. Ignore
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : DIS Change - Circuit Does Not Exist - Index [ %u ]\n",
                         u4CktIdx));
            }

            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_PROT_SUPP_CHANGE:

            pEvent = (tIsisEvtPSChange *) (pIsisMsg->pu1Msg);

            IsisUpdProcPSChgEvt (pContext, pEvent);

            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_IP_IF_ADDR_CHANGE:

            pEvent = (tIsisEvtIPIFChange *) (VOID *) (pIsisMsg->pu1Msg);

            IsisUpdProcIPIfChgEvt (pContext, (tIsisEvtIPIFChange *) pEvent);

            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_IPRA_CHANGE:

            pEvent = (tIsisEvtIPRAChange *) (VOID *) (pIsisMsg->pu1Msg);

            IsisUpdProcIPRAChgEvt (pContext, (tIsisEvtIPRAChange *) pEvent);

            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_SUMM_ADDR_CHANGE:

            pEvent = (tIsisEvtSummAddrChg *) (VOID *) (pIsisMsg->pu1Msg);

            IsisUpdProcSummAddrChgEvt (pContext,
                                       (tIsisEvtSummAddrChg *) pEvent);

            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_MAN_ADDR_CHANGE:

            pEvent = (tIsisEvtManAAChange *) (pIsisMsg->pu1Msg);

            IsisUpdProcMAAChgEvt (pContext, (tIsisEvtManAAChange *) pEvent);

            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_LSP_DBOL:

            pEvent = (tIsisEvtLSPDBOL *) (pIsisMsg->pu1Msg);
            IsisUpdProcDBOLEvt (pContext, (tIsisEvtLSPDBOL *) pEvent);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_LSP_DBOL_RECOV:

            pEvent = (tIsisEvtLSPDBOL *) (pIsisMsg->pu1Msg);
            IsisUpdProcDBOLClearEvt (pContext, (tIsisEvtLSPDBOL *) pEvent);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_CORR_LSP_DET:

            pEvent = (tIsisEvtCorrLsp *) (VOID *) (pIsisMsg->pu1Msg);
            IsisUpdProcLSPDBCorp (pContext, (tIsisEvtCorrLsp *) pEvent);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_EVT_L1_DECN_COMPLETE:

            /* Since the Decision process is complete fro Level1, we
             * may have to update the IPRAs taken from the PATH nodes
             * into the Level2 SelfLSPs. This process is applicable
             * only for L12 systems
             */

            if (pContext->SysActuals.u1SysType == ISIS_LEVEL12)
            {
                IsisUpdProcL1OrL2DecnCompleteEvt (pContext,
                                                  ISIS_EVT_L1_DECN_COMPLETE);
            }

            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;
        case ISIS_EVT_L2_DECN_COMPLETE:
            /* Since the Decision process is complete fro Level2, we
             * may have to update the IPRAs taken from the PATH nodes
             * into the Level1 SelfLSPs. This process is applicable
             * only for L12 systems
             */

            if (pContext->SysActuals.u1SysType == ISIS_LEVEL12)
            {
                IsisUpdProcL1OrL2DecnCompleteEvt (pContext,
                                                  ISIS_EVT_L2_DECN_COMPLETE);
            }

            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;
        case ISIS_EVT_DISTANCE_CHANGE:
            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                u1Status = ISIS_MAX_MT;
            }
            else
            {
                u1Status = 1;
            }
            for (u1MtIndex = 0; u1MtIndex < u1Status; u1MtIndex++)
            {

                if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
                    (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
                {
                    IsisUpdProcDistanceEvt (pContext, ISIS_LEVEL1, u1MtIndex,
                                            0);
                }

                if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
                    (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
                {
                    IsisUpdProcDistanceEvt (pContext, ISIS_LEVEL2, u1MtIndex,
                                            0);
                }
            }
            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;
        default:

            WARNING ((ISIS_LGST,
                      "UPD <E> : Invalid Update Process Event [ %u ]\n",
                      (*(UINT1 *) (pIsisMsg->pu1Msg))));
            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;
    }
}

/*******************************************************************************
 * Function    : IsisUpdProcLSP
 * Description : This function processes the received LSP. It first validates
 *               the PDU, then adds the LSP to the Database if the received LSP
 *               is either a new LSP or is later than the one present in the
 *               data base and schedules that for transmission on all ckts
 *               except on the one it is received. The LSP in the Database is 
 *               scheduled for transmission if the received LSP is older.
 * Input(s)    : pContext  - Pointer to the system context
 *               pMsg      - Pointer to the LSP
 *               u1Level   - Level of LSP
 *               pCktRec   - Pointer to circuit over which LSP was received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcLSP (tIsisSysContext * pContext,
                tIsisMsg * pMsg, UINT1 u1Level, tIsisCktEntry * pCktRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4RetCode = 0;
    UINT1              *pu1RcvdLSP = NULL;
    UINT1              *pu1LSP = NULL;
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1               au1SNPA[ISIS_SNPA_ADDR_LEN];
    UINT2               u2RcvdRemLT = 0;
    UINT4               u4RcvdSeqNum = 0;
    tIsisLSPEntry      *pRec = NULL;
    tIsisLSPEntry      *pLSPRec = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    UINT4               u4SeqNo = 0;
    INT4                i4Ret = ISIS_FAILURE;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT2               u2RcvdCS = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcLSP ()\n"));

    pu1RcvdLSP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, pMsg->u4Length);

    if (pu1RcvdLSP == NULL)
    {
        if ((u1Level == ISIS_LEVEL1)
            && (ISIS_IS_TIMER_ACTIVE (pContext->SysL1Info.SysWaitingTimer)))
        {
            /* No need to process the received LSPs as long as the waiting timer
             * is active
             */

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : L1 Waiting Timer Active, Ignoring LSP - Circuit [%u]\n",
                     pCktRec->u4CktIdx));
            IsisFreeDLLBuf (pMsg);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pMsg);
            ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
            IsisTmrRestartTimer (&pContext->SysL1Info.SysWaitingTimer,
                                 ISIS_L1WAITING_TIMER,
                                 pContext->SysActuals.u2SysWaitTime);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCtrlPkt ()\n"));
            return;
        }
        if ((u1Level == ISIS_LEVEL2)
            && (ISIS_IS_TIMER_ACTIVE (pContext->SysL2Info.SysWaitingTimer)))
        {
            /* No need to process the received LSPs as long as the waiting timer
             * is active
             */

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : L2 Waiting Timer Active, Ignoring LSP - Circuit [%u]\n",
                     pCktRec->u4CktIdx));
            IsisFreeDLLBuf (pMsg);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pMsg);
            ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
            IsisTmrRestartTimer (&pContext->SysL2Info.SysWaitingTimer,
                                 ISIS_L2WAITING_TIMER,
                                 pContext->SysActuals.u2SysWaitTime);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCtrlPkt ()\n"));
            return;
        }
        /* Cannot Validate the LSP. Enter Waiting State unconditionally */

        IsisUpdEnterWaitingState (pContext, u1Level);
        IsisFreeDLLBuf (pMsg);
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pMsg);
        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSP ()\n"));
        return;
    }

    MEMCPY (au1SNPA, pMsg->au1PeerSNPAAddr, ISIS_SNPA_ADDR_LEN);

    IsisCopyFromDLLBuf (pMsg, pu1RcvdLSP, pMsg->u4Length);

    /* Free the Recieved PDU since the PDU is now available in pu1RcvdLSP
     */
    IsisFreeDLLBuf (pMsg);
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pMsg);
    ISIS_FORM_STR (ISIS_LSPID_LEN, pu1RcvdLSP, acLSPId);
    /* Check for External Domain, Peer Adjacency and Authentication
     */

    ISIS_EXTRACT_RLT_FROM_LSP (pu1RcvdLSP, u2RcvdRemLT);
    if (u2RcvdRemLT != 0)
    {
        i4RetVal = IsisUpdValidatePDU (pContext, pu1RcvdLSP, pCktRec,
                                       au1SNPA, u1Level);
    }
    else
    {
        i4RetVal = IsisUtlValidatePurgeLSP (pContext, pu1RcvdLSP);
        UPP_PT ((ISIS_LGST,
                 "UPD <E> : Processing Zero RLT LSP [%s] - Circuit Index [ %u ], Level [ %s ]\n",
                 acLSPId, pCktRec->u4CktIdx, ISIS_GET_LEVEL_STR (u1Level)));

    }
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* The following routine returns 
         * - SELF_LSP - if the received LSP is one of the LSPs generated by the
         *   local system
         * - ZERO_RLT - if either the checksum validation fails or if the
         *   received  LSP has RLT '0'
         * - SUCCESS - if the received LSP is a valid one and the received LSP 
         *             does not satisfy the above two conditions 
         */

        i4RetVal = IsisUpdValidateLSP (pContext, pCktRec, pu1RcvdLSP, u1Level);

        if (i4RetVal == ISIS_FAILURE)
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <E> : Invalid LSP [%s] RLT [%d] - Circuit Index [ %u ], Level [ %s ] - Ignored\n",
                     acLSPId, u2RcvdRemLT, pCktRec->u4CktIdx,
                     ISIS_GET_LEVEL_STR (u1Level)));
            ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
            ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1RcvdLSP);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSP ()\n"));
            return;
        }
        /* Not Overloaded yet, continue processing the LSP further
         */

        if (i4RetVal == ISIS_SELF_LSP)
        {
            /* The received LSP is one of the LSPs generated by the local system
             * either in its previous run or may be in the current run
             */

            if (pContext->u1IsisGRRestartMode != ISIS_GR_RESTARTER)
            {
                IsisUpdProcSelfLSP (pContext, pu1RcvdLSP, pCktRec, u1Level);

                UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSP ()\n"));
                return;
            }

        }
        else if (i4RetVal == ISIS_ZERO_RLT_LSP)
        {
            /* REFERENCE : 7.3.16.4 2nd Para
             *
             * Received LSP has a ZERO Remaining Life Time
             */

            IsisUpdProcessZeroRLTLSP (pContext, pCktRec, pu1RcvdLSP, u1Level);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSP ()\n"));
            return;
        }

        if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER) &&
            (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART))
        {
            IsisUpdGRLSPCheck (pContext, pu1RcvdLSP, u1Level);    /*Updating the RB Tree */

            MEMCPY (au1LSPId, (pu1RcvdLSP + ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);
            ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1RcvdLSP, u4SeqNo);
            pLSPRec = IsisUpdGetLSP (pContext,
                                     (pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID),
                                     u1Level, &pPrevRec, &pHashBkt, &u1Loc);

            if (pLSPRec == NULL)
            {
                IsisUpdAddLSP (pContext, pu1RcvdLSP, ISIS_LOC_DB, u1Level);
                if (i4RetVal == ISIS_SELF_LSP)
                {
                    u4SeqNo++;
                    IsisUpdUpdateSelfLSPSeqNum (pContext, au1LSPId, u4SeqNo,
                                                u1Level);
                }
                return;
            }
        }
        /* The received LSP is a normal & valid LSP. Check whether the received
         * LSP is existing in the database. The flag 'u1Loc' states where the 
         * LSP exists (TxQ or DB)
         */

        ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1RcvdLSP, u4RcvdSeqNum);
        ISIS_EXTRACT_CS_FROM_LSP (pu1RcvdLSP, u2RcvdCS);
        pLSPRec = IsisUpdGetLSP (pContext,
                                 (pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID),
                                 u1Level, &pPrevRec, &pHashBkt, &u1Loc);
        if (pLSPRec != NULL)
        {
            /* We have found a matching LSP in either the DB or TxQ
             */

            if ((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST))
            {
                pRec = ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec;
                pu1LSP = pRec->pu1LSP;
            }
            else
            {
                pRec = pLSPRec;
                pu1LSP = pLSPRec->pu1LSP;
            }

            i4RetCode = IsisUpdCompareLSPs (pu1RcvdLSP, pu1LSP);
        }
        else
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <E> : New LSP [%s][%s] RLT [%d] Seq.No [%x] ChkSum [%x] Received! - On Circuit Index [ %u ]\n",
                     acLSPId, ISIS_GET_LEVEL_STR (u1Level), u2RcvdRemLT,
                     u4RcvdSeqNum, u2RcvdCS, pCktRec->u4CktIdx));
            /* Received LSP is not found in the Database
             */
            i4RetCode = ISIS_NEW_LSP;
            /*Exit Helper */
            /* Place the LSP in Trnasmission queue for transmission
             */

            i4RetVal = IsisUpdAddLSP (pContext, pu1RcvdLSP, ISIS_LOC_TX,
                                      u1Level);
            if (i4RetVal == ISIS_FAILURE)
            {
                IsisUpdEnterWaitingState (pContext, u1Level);

                ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
                ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1RcvdLSP);
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdProcCtrlPkt ()\n"));
                return;
            }

            /* Eventhough the LSP is just now added, we may have to get the
             * exact details viz. pPrevRec, pHashBkt and the location of the new
             * LSP. These details will be required for further processing
             */

            pLSPRec = IsisUpdGetLSP (pContext,
                                     (pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID),
                                     u1Level, &pPrevRec, &pHashBkt, &u1Loc);

            if (pLSPRec != NULL)
            {
                pRec = ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec;
                pu1LSP = pRec->pu1LSP;
            }
            else
            {
                return;
            }
        }

        ISIS_EXTRACT_RLT_FROM_LSP (pu1LSP, u2RcvdRemLT);
        UPP_PT ((ISIS_LGST,
                 "UPD <E> : Processing LSP [%s][%s] RLT [%d] Seq.No [%x] ChkSum [%x] Received! - On Circuit Index [ %u ] - Code [%s]\n",
                 acLSPId, ISIS_GET_LEVEL_STR (u1Level), u2RcvdRemLT,
                 u4RcvdSeqNum, u2RcvdCS, pCktRec->u4CktIdx,
                 ISIS_GET_LSPCMP_CODE_STR (i4RetCode)));
        switch (i4RetCode)
        {
            case ISIS_NEW_LSP:

                /* REFERENCE 7.3.15.1 Subsec - e-1 i, ii, iii, iv, v
                 *           7.3.1 2nd para
                 *           
                 * Increment the Count which will be used for scheduling the
                 * decision process when the SPF scheduling timer expires 
                 */

                ISIS_TRIGGER_DEC (pContext, u1Level);

                /* Schedule LSP Transmission on all circuits except the circuit
                 * over which the LSP was received
                 */

                i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                            (VOID *) pLSPRec, pCktRec, u1Loc,
                                            pHashBkt, ISIS_ALL_BUT_SPEC_CKT);
                if (i4Ret == ISIS_MEM_FAILURE)
                {
                    IsisUpdDeleteLSP (pContext, u1Level,
                                      (pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID));
                }
                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    /* If the LSP is received on Point to Point Ckt, Construct 
                     * PSNP which should be transmitted back to the originating 
                     * system as an acknowledgement
                     */

                    IsisUpdConstructPSNP (pContext,
                                          (pu1RcvdLSP + ISIS_OFFSET_RLT),
                                          pCktRec, u1Level, 0);
                }
                break;

            case ISIS_DIFF_CS_GRTR_SEQNO:
            case ISIS_DIFF_CS_SAME_SEQNO:

                /* Increment the Count which will be used for scheduling the
                 * decision process when the SPF scheduling timer expires 
                 */

                ISIS_TRIGGER_DEC (pContext, u1Level);

                /* Delete all the Area Addresses that were updated into the Area
                 * Address Table pertaining to the LSP that was found in the
                 * database
                 */

                IsisUpdUpdateAA (pContext, pu1LSP, ISIS_CMD_DELETE);

                /* Add the Area Address TLVs from the LSP that is received since
                 * some of the area addresses may have been modified
                 */

                IsisUpdUpdateAA (pContext, pu1RcvdLSP, ISIS_CMD_ADD);

                /* Overwrite the existing LSP with the New LSP received
                 */

                IsisUpdReplaceLSP (pContext, pu1RcvdLSP, pLSPRec, u1Loc);

                /* Schedule LSP Transmission on all circuits except the circuit
                 * over which the LSP was received
                 */

                i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                            (VOID *) pLSPRec, pCktRec, u1Loc,
                                            pHashBkt, ISIS_ALL_BUT_SPEC_CKT);
                if (i4Ret == ISIS_MEM_FAILURE)
                {
                    IsisUpdDeleteLSP (pContext, u1Level,
                                      (pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID));
                }

                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    /* If the LSP is received on Point to Point Ckt, Construct 
                     * PSNP which should be transmitted back to the originating 
                     * system as an acknowledgement
                     */

                    IsisUpdConstructPSNP (pContext,
                                          (pu1RcvdLSP + ISIS_OFFSET_RLT),
                                          pCktRec, u1Level, 0);
                }
                break;

            case ISIS_SAME_CS_GRTR_SEQNO:

                ISIS_DEL_LSP_TIMER_NODE (pRec);

                /* Start the Remaining Life Time timer with the value extracted
                 * from the received LSP 
                 */

                IsisUpdStartLSPTimer (pContext, pRec, ISIS_ECT_MAX_AGE,
                                      u2RcvdRemLT);

                /* Extract the sequence number from the received LSP which will
                 * be used for updating the LSP (either in Database or Transmit
                 * Queue) with the latest sequence number
                 */

                ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1RcvdLSP, u4RcvdSeqNum);

                /* Modify the LSP (either in Database or Transmit Q) with the
                 * extracted sequence number since sequence number is the only
                 * change
                 */

                IsisUpdModifyLSP (pContext, pLSPRec, u2RcvdRemLT,
                                  u4RcvdSeqNum, u1Loc);

                /* Schedule LSP Transmission on all circuits except the circuit
                 * over which the LSP was received
                 */

                i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                            (VOID *) pLSPRec, pCktRec, u1Loc,
                                            pHashBkt, ISIS_ALL_BUT_SPEC_CKT);
                if (i4Ret == ISIS_MEM_FAILURE)
                {
                    IsisUpdDeleteLSP (pContext, u1Level,
                                      (pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID));
                }
                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    /* If the LSP is received on Point to Point Ckt, Construct 
                     * PSNP which should be transmitted back to the originating 
                     * system as an acknowledgement
                     */

                    IsisUpdConstructPSNP (pContext,
                                          (pu1RcvdLSP + ISIS_OFFSET_RLT),
                                          pCktRec, u1Level, 0);
                }
                ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1RcvdLSP);
                break;

            case ISIS_SAME_CS_LESS_SEQNO:
            case ISIS_DIFF_CS_LESS_SEQNO:

                /* Peer has an LSP which is not the latest copy that the local
                 * system holds in either its Transmit queue or Database. 
                 * Schedule the LSP for transmission
                 */

                i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                            (VOID *) pLSPRec, pCktRec, u1Loc,
                                            pHashBkt, ISIS_SPEC_CKT);
                if (i4Ret == ISIS_MEM_FAILURE)
                {
                    IsisUpdDeleteLSP (pContext, u1Level,
                                      (pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID));
                }
                ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1RcvdLSP);
                break;

            case ISIS_SAME_CS_SAME_SEQNO:

                /* Received LSP is an exact copy of the LSP which is present in
                 * either the Transmit Queue or Database. No need to even
                 * acknowledge, since the peer has the same copy
                 */

                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    /* If the LSP is received on a Point to Point Link then
                     * clear the SRM flag bit corresponding to the circuit on
                     * which the LSP is received since the peer also has an
                     * exact copy of the LSP that the local system holds
                     */

                    IsisUpdClearSRM (pContext, (tIsisLSPTxEntry *) pLSPRec,
                                     (tIsisLSPTxEntry *) pPrevRec, pCktRec,
                                     u1Loc, pHashBkt, u1Level);

                    IsisUpdConstructPSNP (pContext,
                                          (pu1RcvdLSP + ISIS_OFFSET_RLT),
                                          pCktRec, u1Level, 0);
                }
                ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1RcvdLSP);
                break;

            default:

                /* Can never happen unless a peer system generates different
                 * LSPs for different peers
                 */

                ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1RcvdLSP);
                break;
        }
    }
    else
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <E> : LSP [%s] RLT [%d] Validation Failed - Circuit Index [ %u ], Level [ %s ]\n",
                 acLSPId, u2RcvdRemLT, pCktRec->u4CktIdx,
                 ISIS_GET_LEVEL_STR (u1Level)));
        DETAIL_FAIL (ISIS_CR_MODULE);
        /* LSP had been processed in normal state. Release back the memory
         * to the free pools
         */

        ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1RcvdLSP);
        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLSP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdValidateLSP ()
 * Description : This function Validates the Received LSP. It validates the
 *               checksum and also verifies whether the LSP has been generated 
 *               by the local system. It also verifies the LSP Buf size received
 * Input(s)    : pContext   - Pointer to the system context
 *               pCktRec    - Pointer to the circuit record
 *               pu1RcvdLSP - Pointer to the Received LSP
 *               u1Level    - The Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PRIVATE INT4
IsisUpdValidateLSP (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                    UINT1 *pu1RcvdLSP, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4RetCode = ISIS_SUCCESS;
    UINT2               u2RcvdCS = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2Offset = 0;
    UINT2               u2RcvdRemLT = 0;
    UINT1               u1MTLen = 0;
    UINT1               u1TlvLen = 0;
    UINT1               u1BufMMEvt = ISIS_TRUE;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    tIsisEvtLspBuffSizeMismatch *pBufMMEvt = NULL;

    ISIS_EXTRACT_RLT_FROM_LSP (pu1RcvdLSP, u2RcvdRemLT);

    i4RetVal = IsisUtlGetNextTlvOffset (pu1RcvdLSP, ISIS_LSP_BUF_SIZE_TLV,
                                        &u2Offset, &u1TlvLen);
    if (i4RetVal == ISIS_SUCCESS)
    {
        ISIS_GET_2_BYTES (pu1RcvdLSP, u2Offset, u2BufSize);

        if (u1Level == ISIS_LEVEL1)
        {
            if (u2BufSize == pContext->SysActuals.u4SysOrigL1LSPBufSize)
            {
                u1BufMMEvt = ISIS_FALSE;
            }
        }
        else
        {
            if (u2BufSize == pContext->SysActuals.u4SysOrigL2LSPBufSize)
            {
                u1BufMMEvt = ISIS_FALSE;
            }
        }
    }

    if ((u1BufMMEvt == ISIS_TRUE) && (u2BufSize != 0))
    {
        pBufMMEvt = (tIsisEvtLspBuffSizeMismatch *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                            sizeof (tIsisEvtLspBuffSizeMismatch));
        if (pBufMMEvt != NULL)
        {
            pBufMMEvt->u1EvtID = ISIS_EVT_LSP_BUFSIZE_MISMATCH;
            pBufMMEvt->u1SysLevel = u1Level;
            pBufMMEvt->u2BuffSize = u2BufSize;
            pBufMMEvt->u4CktIfIdx = pCktRec->u4CktIfIdx;
            MEMCPY (pBufMMEvt->au1TrapLspID, (pu1RcvdLSP + ISIS_OFFSET_LSPID),
                    ISIS_LSPID_LEN);
            IsisUtlSendEvent (pContext, (UINT1 *) pBufMMEvt,
                              sizeof (tIsisEvtLspBuffSizeMismatch));
        }
    }

    /* Validate the check sum of the received PDU. If checksum is '0' or
     * checksum is not valid, then set the Remaining Life time
     * and checksum to zero
     * REFERENCE : 7.3.14.2 e)
     */

    if (u2RcvdRemLT != 0)
    {
        i4RetVal = IsisUpdValidateCS (pContext, pu1RcvdLSP);

        if ((i4RetVal != ISIS_SUCCESS)
            && ((pContext->SysActuals.bSysLSPIgnoreErr != ISIS_TRUE)
                || (u2RcvdRemLT > ISIS_LSP_MAXAGE)))
        {
            /* REFERENCE: 7.3.16.3 of ISO 10589 - ver.1992 
             * Last Paragraph
             */
            ISIS_FORM_STR (ISIS_LSPID_LEN, pu1RcvdLSP, acLSPId);
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : LSP [%s] Checksum validation failed\n",
                     acLSPId));
            u2RcvdRemLT = 0;
            u2RcvdCS = 0;
            ISIS_SET_LSP_RLT (pu1RcvdLSP, u2RcvdRemLT);
            ISIS_SET_LSP_CS (pu1RcvdLSP, u2RcvdCS);
            i4RetVal = ISIS_ZERO_RLT_LSP;
        }
    }
    else
    {
        i4RetVal = ISIS_ZERO_RLT_LSP;
    }

    u2Offset = 0;
    /*MISIS - ISIS_MT_TLV not to be present in single topology mode is already validated */
    if (pContext->u1IsisMTSupport == ISIS_FALSE)
    {

        if (((IsisUtlGetNextTlvOffset (pu1RcvdLSP, ISIS_MT_IPV6_REACH_TLV,
                                       &u2Offset, &u1MTLen)) == ISIS_SUCCESS) ||
            ((IsisUtlGetNextTlvOffset (pu1RcvdLSP, ISIS_MT_IS_REACH_TLV,
                                       &u2Offset, &u1MTLen)) == ISIS_SUCCESS))
        {
            return ISIS_FAILURE;
        }
    }
    else
    {
        if (((((tIsisComHdr *) pu1RcvdLSP)->u1PDUType == ISIS_L1LSP_PDU) ||
             (((tIsisComHdr *) pu1RcvdLSP)->u1PDUType == ISIS_L2LSP_PDU)) &&
            ((UINT1) (*(pu1RcvdLSP + ISIS_OFFSET_LSPNUM)) != 0))
        {
            if ((IsisUtlGetNextTlvOffset
                 (pu1RcvdLSP, ISIS_MT_TLV, &u2Offset,
                  &u1MTLen)) == ISIS_SUCCESS)
            {
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4IsisSysLogId,
                              "MT-TLV received in Non-Zero LSP"));
                return ISIS_FAILURE;
                /*MISIS - invalid LSP with MT TLV in non-zero fragment */
            }
            /* when Multi-topology is enabled, any of the MT related TLV should be present in 
             * Non-Zero LSP, else return failure*/
        }
    }

    /* Compare the received LSP System ID with the Self System ID
     */

    i4RetCode = MEMCMP ((pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID),
                        pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);

    if (i4RetCode == 0)
    {
        /* REFERENCE : 7.3.15.1 - SubSec c, d, i
         *             7.3.16.4 - Subsec c ( 2nd Para)
         */

        /* Received LSP is orignated by the self system
         */

        i4RetVal = ISIS_SELF_LSP;
    }
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdProcPSNP ()
 * Description : This function Processes the received PSNP. If the information
 *               included in the PSNP is 
 *               -- Same Information: Treats it as an acknowledgement and clears
 *                  the SRM flags if any are set.
 *               -- Older information: It schedules the corresponding LSP for
 *                  transmission to update the peer
 *               -- Latest Information: It constructs a PSNP to acquire the LSP
 *                  from the peer. This should never happen since PSNPs ae used
 *                  only for acquiring LSPs or acknowledging received LSPs but
 *                  not for announcing latest information
 * Input(s)    : pContext  - Pointer to the system context
 *               pIsisMsg  - Pointer to the received Message which encapsulates
 *                           the received PSNP PDU
 *               pCktRec   - Pointer to the CktRec on which the PSNP is received
 *               u1Level   - Level of the PSNP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcPSNP (tIsisSysContext * pContext, tIsisMsg * pIsisMsg,
                 tIsisCktEntry * pCktRec, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Loc = 0;
    UINT1               u1Len = 0;
    UINT1               u1Code = 0;
    UINT1              *pu1PSNP = NULL;
    UINT1               au1SNPA[ISIS_SNPA_ADDR_LEN];
    UINT2               u2PDULen = 0;
    UINT2               u2Offset = 0;
    UINT2               u2RemLifeTime = 0;
    UINT4               u4SeqNum = 0;
    UINT2               u2CheckSum = 0;
    tIsisLSPEntry      *pRec = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcPSNP ()\n"));

    pu1PSNP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_PSNP, pIsisMsg->u4Length);

    if (pu1PSNP == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : PSNP\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_PSNP);
        IsisFreeDLLBuf (pIsisMsg);
        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcPSNP ()\n"));
        return;
    }

    /* Extract the SNPA of the peer from the received message, which will be
     * used during PDU validation
     */

    MEMCPY (au1SNPA, pIsisMsg->au1PeerSNPAAddr, ISIS_SNPA_ADDR_LEN);

    IsisCopyFromDLLBuf (pIsisMsg, pu1PSNP, pIsisMsg->u4Length);
    IsisFreeDLLBuf (pIsisMsg);
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

    /* Check for the following
     * -- Whether the packet is received from the same Domain
     * -- Whether the Authencation is enabled for the receiving direction and if
     *    enabled, whether the packet is successfully authenticated
     * -- Whether the System ID Lengths of the received packet and the local
     *    system matches   
     * -- Whether the packet came from a neighbour with whom the local system
     *    has a valid adjacency   
     */
    ISIS_FORM_STR (ISIS_LSPID_LEN, pu1PSNP, acLSPId);
    i4RetVal = IsisUpdValidatePDU (pContext, pu1PSNP, pCktRec, au1SNPA,
                                   u1Level);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* PDU Validation is Successful
         */

        /* IsisUpdSortSNP (pu1PSNP); */

        u2Offset = sizeof (tIsisComHdr);
        if (u2Offset < (pIsisMsg->u4Length))
        {
            ISIS_GET_2_BYTES (pu1PSNP, u2Offset, u2PDULen);
            u2Offset = (UINT2) (u2Offset + 2);
            /* Advance the offset so that it points to the variable length fields
             */

            u2Offset = (UINT2) (u2Offset + (ISIS_SYS_ID_LEN + 1));

        }

        while (u2Offset < u2PDULen)
        {
            ISIS_GET_1_BYTE (pu1PSNP, u2Offset, u1Code);
            u2Offset++;

            ISIS_GET_1_BYTE (pu1PSNP, u2Offset, u1Len);
            u2Offset++;

            switch (u1Code)
            {
                case ISIS_AUTH_INFO_TLV:

                    /* We don't need to consider authentication here
                     */

                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Authentication In PSNP - Ignored\n"));
                    u2Offset = (UINT2) (u2Offset + u1Len);
                    break;

                case ISIS_LSP_ENTRY_TLV:

                    while (u1Len >= ISIS_LSP_ENTRY_TLV_LEN)
                    {
                        /* Retrieve the LSP included in the PSNP
                         */

                        pRec =
                            IsisUpdGetLSP (pContext, (pu1PSNP + u2Offset + 2),
                                           u1Level, &pPrevRec, &pHashBkt,
                                           &u1Loc);
                        if (pRec != NULL)
                        {
                            /* Found the LSP either in Transmit Queue or
                             * Database
                             */

                            ISIS_DBG_PRINT_ID (pu1PSNP + u2Offset,
                                               (UINT1) ISIS_LSPID_LEN,
                                               "UPD <T> : Processing PSNP ID\t",
                                               ISIS_OCTET_STRING);

                            IsisUpdProcLETLV (pContext, (pu1PSNP + u2Offset),
                                              (VOID *) pRec, (VOID *) pPrevRec,
                                              pCktRec, u1Loc, pHashBkt,
                                              u1Level);
                        }
                        else
                        {
                            /* If the LSP is not found in either the Database or
                             * Transmit Queue, then construct a PSNP requesting
                             * for the same from the peer
                             */

                            ISIS_GET_2_BYTES (pu1PSNP, u2Offset, u2RemLifeTime);
                            ISIS_GET_4_BYTES (pu1PSNP,
                                              u2Offset + ISIS_SYS_ID_LEN + 4,
                                              u4SeqNum);
                            ISIS_GET_2_BYTES (pu1PSNP,
                                              u2Offset + ISIS_SYS_ID_LEN + 8,
                                              u2CheckSum);

                            if ((u2RemLifeTime != 0)
                                && (u4SeqNum != 0) && (u2CheckSum != 0))
                            {
                                ISIS_DBG_PRINT_ID ((pu1PSNP + u2Offset),
                                                   (UINT1) ISIS_LSPID_LEN,
                                                   "UPD <T> : Acquiring LSP\t",
                                                   ISIS_OCTET_STRING);
                                IsisUpdConstructPSNP (pContext,
                                                      (pu1PSNP + u2Offset),
                                                      pCktRec, u1Level,
                                                      ISIS_LSP_ACQUIRE);
                            }
                        }
                        u2Offset = (UINT2) (u2Offset + ISIS_LSP_ENTRY_TLV_LEN);
                        u1Len = (UINT1) (u1Len - ISIS_LSP_ENTRY_TLV_LEN);
                    }
                    break;

                default:
                    u2Offset = (UINT2) (u2Offset + u1Len);
                    break;
            }
        }
    }
    else
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Validation Failed For PSNP From [%s][%s]\n",
                 acLSPId, ISIS_GET_LEVEL_STR (u1Level)));
        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
    }

    ISIS_MEM_FREE (ISIS_BUF_PSNP, pu1PSNP);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcPSNP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcCSNP ()
 * Description : This function Processes the received CSNP.
 * Input(s)    : pContext  - Pointer to the system context
 *               pIsisMsg  - Pointer to received Message which encapsulates the
 *                           received PDU
 *               pCktRec   - Pointer to the Circuit record
 *               u1Level   - Level of the CSNP
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcCSNP (tIsisSysContext * pContext, tIsisMsg * pIsisMsg,
                 tIsisCktEntry * pCktRec, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1              *pu1CSNP = NULL;
    UINT1               au1SNPA[ISIS_SNPA_ADDR_LEN];
    tIsisCktLevel      *pCktLvlRec = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcCSNP ()\n"));

    pu1CSNP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_CSNP, pIsisMsg->u4Length);

    if (pu1CSNP == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : CSNP\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCSNP ()\n"));
        IsisFreeDLLBuf (pIsisMsg);
        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
        return;
    }

    /* Extract the SNPA of the peer from the received message, which will be
     * used during PDU validation
     */

    MEMCPY (au1SNPA, pIsisMsg->au1PeerSNPAAddr, ISIS_SNPA_ADDR_LEN);

    IsisCopyFromDLLBuf (pIsisMsg, pu1CSNP, pIsisMsg->u4Length);
    IsisFreeDLLBuf (pIsisMsg);
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

    /* Check for the following
     * -- Whether the packet is received from the same Domain
     * -- Whether the Authencation is enabled for the receiving direction and if
     *    enabled, whether the packet is successfully authenticated
     * -- Whether the System ID Lengths of the received packet and the local
     *    system matches   
     * -- Whether the packet came from a neighbour with whom the local system
     *    has a valid adjacency   
     */

    ISIS_FORM_STR (ISIS_LSPID_LEN, pu1CSNP, acLSPId);
    i4RetVal = IsisUpdValidatePDU (pContext, pu1CSNP, pCktRec, au1SNPA,
                                   u1Level);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* PDU Validation is Successful
         */

        if (pCktRec->u1CktType == ISIS_BC_CKT)
        {
            /* CSNPs can be generated only the DIS for the LAN. If the local
             * system receives a CNSP from any other IS which is not a DIS, the
             * PDU can be silently ignored
             */

            pCktLvlRec = (u1Level == ISIS_LEVEL1) ? pCktRec->pL1CktInfo
                : pCktRec->pL2CktInfo;

            i4RetVal = MEMCMP ((pu1CSNP + sizeof (tIsisComHdr) + 2),
                               pCktLvlRec->au1CktLanDISID, ISIS_SYS_ID_LEN);
            if ((i4RetVal != 0)
                && ((pContext->u1IsisGRRestartMode != ISIS_GR_RESTARTER)))
            {
                /* The received CSNP is not orginated by the DIS for this
                 * CktLevel, hence discard the CSNP 
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <E> : CSNP Received From Non-DIS [%s] - Discarded"
                         " Circuit [%u], Level [%u]\n", acLSPId,
                         pCktRec->u4CktIdx, u1Level));
                ISIS_MEM_FREE (ISIS_BUF_CSNP, (UINT1 *) pu1CSNP);
                UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCSNP ()\n"));
                return;
            }
        }
        IsisUpdProcCSNPEntries (pContext, pu1CSNP, pCktRec, u1Level);
        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            if (u1Level == ISIS_LEVEL1)
            {
                pCktRec->pL1CktInfo->u1SetSRM = ISIS_FALSE;
            }
            else if (u1Level == ISIS_LEVEL2)
            {
                pCktRec->pL2CktInfo->u1SetSRM = ISIS_FALSE;
            }
        }
    }
    else
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Validation failed for CSNP from [%s][%s]\n",
                 acLSPId, ISIS_GET_LEVEL_STR (u1Level)));
        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
    }

    ISIS_MEM_FREE (ISIS_BUF_CSNP, pu1CSNP);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcCSNP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcCSNPEntries ()
 * Description : This function Processes the received CSNP. If the LSPs 
 *               in the received CSNP are more recent than the ones in its
 *               database it marks those LSPs for aquisition. If the database
 *               LSPs are more recent, then it sets the SRM flags for those LSPs
 *               and schedules them for transmision. If any of the LSPs included
 *               in the CSNP are missing in the database, then those LSPs are
 *               requested from the peer through a PSNP.
 * Input(s)    : pContext  - Pointer to the system context
 *               pu1CSNP   - Pointer to the Received CSNP
 *               pCktRec   - Pointer to the Circuit record
 *               u1Level   - Level of the CSNP
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PRIVATE VOID
IsisUpdProcCSNPEntries (tIsisSysContext * pContext, UINT1 *pu1CSNP,
                        tIsisCktEntry * pCktRec, UINT1 u1Level)
{
    INT4                i4DbRetCode = 0xff;
    INT4                i4TxRetCode = 0xff;
    INT4                i4Ret = ISIS_FAILURE;
    UINT1               u1Len = 0;
    UINT1               u1Code = 0;
    UINT1               u1Flag = 0;
    UINT1               u1TxLoc = ISIS_LOC_NONE;
    UINT1               u1DbLoc = ISIS_LOC_NONE;
    UINT1               u1Status = ISIS_FALSE;
    UINT2               u2Offset = 0;
    UINT2               u2PDULen = 0;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisLSPEntry      *pLSPDbRec = NULL;
    tIsisLSPEntry      *pNextDbRec = NULL;
    tIsisLSPEntry      *pTransRec = NULL;
    tIsisLSPTxEntry    *pLSPTxRec = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisLSPTxEntry    *pNextTxRec = NULL;
    tIsisHashBucket    *pDbHashBkt = NULL;
    tIsisHashBucket    *pTxHashBkt = NULL;
    tIsisHashBucket    *pNextTxHashBkt = NULL;
    tIsisHashBucket    *pNextDbHashBkt = NULL;
    tGrSemInfo          GrSemInfo;
    UINT1               au1LspId[ISIS_LSPID_LEN];
    /* IsisUpdSortSNP (pu1CSNP); */

    MEMSET (au1LspId, 0, ISIS_LSPID_LEN);
    ISIS_GET_2_BYTES (pu1CSNP, (UINT2) sizeof (tIsisComHdr), u2PDULen);

    /* Get to the variable length fields where the LETLVs are included
     */

    u2Offset = (sizeof (tIsisComHdr) + 2 + (2 * ISIS_LSPID_LEN) +
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

    /* ALGO: Get the very first LSP records from both the Database and the
     * Transmit Queue. LSPs in the Database and Transmit Queue are sorted
     * based on LSPIDs. According to Specifications, LSPIDs listed in the
     * CNSP PDU are supposed to be sorted. If they are not sorted the LETLVs
     * in the PDU are to be sorted before proceeding further.
     *
     * Compare the first LSP from TxQ and DB with the very first LSP in the
     * CSNP. If they match process the TLV and proceed further. If the LSPID
     * from either the Database or the TxQ is greater than the LSPID listed
     * in the CSNP, then we can decide that we don't have the LSP
     * corresponding to the LSPID (since LSPs are sorted). Then we will
     * construct a PSNP requesting the missing LSP. If LSPID in either the
     * DB or TxQ is less than the LSPID of the CNSP, then the peer does not
     * have the LSP that the local system has. Schedule that particular LSP
     * for tranmission. Once this is done advance through the CSNP for the
     * next LSPID and also advance both the Database and Transmit Queue
     * pointers 
     */

    /* The following routines returns either the matching LSP or an LSP 
     * which is greater than the given LSP.
     */

    pLSPDbRec =
        IsisUpdGetLSPFromDB (pContext, au1LspId, u1Level, &pPrevDbRec,
                             &pDbHashBkt, &u1DbLoc, &u1Flag);
    pLSPTxRec =
        IsisUpdGetLSPFromTxQ (pContext, au1LspId,
                              u1Level, &pPrevTxRec,
                              &pTxHashBkt, &u1TxLoc, &u1Flag);

    while (u2Offset < u2PDULen)
    {
        ISIS_GET_1_BYTE (pu1CSNP, u2Offset, u1Code);
        u2Offset++;

        ISIS_GET_1_BYTE (pu1CSNP, u2Offset, u1Len);
        u2Offset++;

        switch (u1Code)
        {
            case ISIS_AUTH_INFO_TLV:

                /* We don't need to consider authentication here
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> :  Authentication In CSNP - Ignored\n"));
                u2Offset = (UINT2) (u2Offset + u1Len);
                break;

            case ISIS_LSP_ENTRY_TLV:

                while (u1Len >= ISIS_LSP_ENTRY_TLV_LEN)
                {
                    if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER) &&
                        (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART))
                    {
                        MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
                        GrSemInfo.pContext = pContext;
                        GrSemInfo.pCktRec = pCktRec;
                        GrSemInfo.u1Level = u1Level;
                        GrSemInfo.pu1CSNP = (pu1CSNP + u2Offset);
                        GrSemInfo.u1PktType = ISIS_GR_CSNP_RCVD;
                        /* GR SEM Function is executed now */
                        if (((u1Level == ISIS_LEVEL1)
                             &&
                             ((pCktRec->
                               u1IsisGRRestartL1State & ISIS_GR_ACK_RCVD) ==
                              ISIS_GR_ACK_RCVD)) || ((u1Level == ISIS_LEVEL2)
                                                     &&
                                                     ((pCktRec->
                                                       u1IsisGRRestartL2State &
                                                       ISIS_GR_ACK_RCVD) ==
                                                      ISIS_GR_ACK_RCVD)))
                        {
                            IsisGrRunRestartSem (ISIS_GR_EVT_RX_CSNP,
                                                 ISIS_GR_ADJ_SEEN_RA,
                                                 &GrSemInfo);
                        }
                        else
                        {
                            IsisGrRunRestartSem (ISIS_GR_EVT_RX_CSNP,
                                                 ISIS_GR_ADJ_RESTART,
                                                 &GrSemInfo);
                        }

                        u2Offset = (UINT2) (u2Offset + ISIS_LSPID_LEN + 8);
                        u1Len = (UINT1) (u1Len - (ISIS_LSPID_LEN + 8));
                        continue;
                    }

                    if (pLSPTxRec != NULL)
                    {
                        /* Entries available in the Transmit Queue
                         * Compare the first LSP Record in the Tx queue
                         * with the LSP ID in the LETLV of CSNP
                         */

                        i4TxRetCode
                            = (MEMCMP ((pLSPTxRec->pLSPRec->pu1LSP +
                                        ISIS_OFFSET_LSPID),
                                       (pu1CSNP + u2Offset + 2),
                                       ISIS_LSPID_LEN));
                    }

                    if ((i4TxRetCode != 0) && (pLSPDbRec != NULL))
                    {
                        /* Entries available in the Database and the given
                         * LSP did not match the LSP in the Transmit Queue
                         *
                         * NOTE: i4TxRetCode would be '0' if the LSP in CSNP
                         * mathced the LSP in TxQ
                         */
                        /* Compare the first LSP Record in the Database
                         * with the LSP ID in the LETLV of CSNP
                         */

                        i4DbRetCode
                            = (MEMCMP ((pLSPDbRec->pu1LSP + ISIS_OFFSET_LSPID),
                                       (pu1CSNP + u2Offset + 2),
                                       ISIS_LSPID_LEN));
                    }

                    if (i4TxRetCode == 0)
                    {
                        /* LSPIDs in the CSNP and the TxQ matched
                         */

                        pNextTxHashBkt = pTxHashBkt;
                        pNextTxRec = IsisUpdGetNextTxRec (pLSPTxRec,
                                                          &pNextTxHashBkt);
                        pTransRec = pLSPTxRec->pLSPRec;

                        ISIS_DBG_PRINT_ID (pu1CSNP + u2Offset,
                                           (UINT1) ISIS_LSPID_LEN,
                                           "UPD <T> : Processing CSNP ID)\t",
                                           ISIS_OCTET_STRING);

                        u1Status = IsisUpdProcLETLV (pContext,
                                                     (pu1CSNP + u2Offset),
                                                     pLSPTxRec, pPrevTxRec,
                                                     pCktRec, u1TxLoc,
                                                     pTxHashBkt, u1Level);

                        /* IsisUpdProcLETLV () might have moved the 
                         * pLSPTxRec from TxQ to DB, which is indicated by
                         * 'u1Status' as ISIS_MOVED_TO_DB. if u1Status is 
                         * not ISIS_MOVED_TO_DB, then We need not update 
                         * the Previous TxQ Record and also the location.
                         */

                        if (u1Status != ISIS_MOVED_TO_DB)
                        {
                            pPrevTxRec = pLSPTxRec;
                            u1TxLoc = ISIS_LOC_TX;
                        }
                        else if ((pDbHashBkt != NULL)
                                 && (((pPrevDbRec == NULL)
                                      && (ISIS_UPD_DB_FE (pDbHashBkt)
                                          == pTransRec))
                                     || ((pPrevDbRec != NULL)
                                         && (ISIS_UPD_DB_NE (pPrevDbRec)
                                             == pTransRec))))
                        {
                            pPrevDbRec = pTransRec;
                            u1DbLoc = ISIS_LOC_DB;
                        }

                        /* Update the pLSPTxRec and pHashBkt with the 
                         * pNextTxRec and pNextTxHashBkt, to make sure 
                         * that we point to the correct record in sequence
                         * even if the pLSPTxRec is moved to DB from TxQ.
                         */

                        pLSPTxRec = pNextTxRec;
                        pTxHashBkt = pNextTxHashBkt;
                        i4TxRetCode = 0xff;
                    }
                    else if (i4DbRetCode == 0)
                    {
                        /* LSPIDs in the CSNP and the Database matched
                         */

                        pNextDbHashBkt = pDbHashBkt;
                        pNextDbRec = IsisUpdGetNextDbRec (pLSPDbRec,
                                                          &pNextDbHashBkt);

                        ISIS_DBG_PRINT_ID (pu1CSNP + u2Offset,
                                           (UINT1) ISIS_LSPID_LEN,
                                           "UPD <T> : Processing CSNP ID\t",
                                           ISIS_OCTET_STRING);

                        u1Status = IsisUpdProcLETLV (pContext,
                                                     (pu1CSNP + u2Offset),
                                                     pLSPDbRec, pPrevDbRec,
                                                     pCktRec, u1DbLoc,
                                                     pDbHashBkt, u1Level);

                        /* IsisUpdProcLETLV () might have moved the 
                         * pLSPDbRec from DB to TxQ, which is indicated by
                         * 'u1Status' as ISIS_MOVED_TO_TXQ. if u1Status is 
                         * not ISIS_MOVED_TO_TXQ, then We need not update 
                         * the Previous DB Record and also the location.
                         */

                        if (u1Status != ISIS_MOVED_TO_TXQ)
                        {
                            pPrevDbRec = pLSPDbRec;
                            u1DbLoc = ISIS_LOC_DB;
                        }
                        else if ((pTxHashBkt != NULL)
                                 && ((pPrevTxRec == NULL)
                                     && (ISIS_UPD_TX_FLSP (pTxHashBkt)
                                         == pLSPDbRec)))
                        {
                            pPrevTxRec = ISIS_UPD_TX_FE (pTxHashBkt);
                            u1TxLoc = ISIS_LOC_TX;
                        }
                        else if ((pPrevTxRec != NULL)
                                 && (ISIS_UPD_TX_NLSP (pPrevTxRec) ==
                                     pLSPDbRec))
                        {
                            pPrevTxRec = ISIS_UPD_TX_NE (pPrevTxRec);
                            u1TxLoc = ISIS_LOC_TX;
                        }

                        /* Update the pLSPDbRec and pDbHashBkt with the 
                         * pNextDbRec and pNextDbHashBkt, to make sure 
                         * that we point to the correct record in sequence
                         * even if the pLSPDbRec is moved to TxQ from DB.
                         */

                        pLSPDbRec = pNextDbRec;
                        pDbHashBkt = pNextDbHashBkt;
                        i4DbRetCode = 0xff;
                    }
                    else if ((i4TxRetCode < 0) && (pLSPTxRec != NULL))
                    {
                        /* Peer system does not have the LSP that is
                         * available in the local systems Transmit Queue
                         */

                        ISIS_DBG_PRINT_ID (pLSPTxRec->
                                           pLSPRec->pu1LSP + ISIS_OFFSET_LSPID,
                                           (UINT1) ISIS_LSPID_LEN,
                                           "UPD <T> : Scheduling LSP\t",
                                           ISIS_OCTET_STRING);

                        i4Ret =
                            IsisUpdSchLSPForTx (pContext, (VOID *) pPrevTxRec,
                                                (VOID *) pLSPTxRec, pCktRec,
                                                u1TxLoc, pTxHashBkt,
                                                ISIS_SPEC_CKT);
                        if (i4Ret == ISIS_MEM_FAILURE)
                        {
                            IsisUpdDeleteLSP (pContext, u1Level, au1LspId);
                        }

                        pPrevTxRec = pLSPTxRec;
                        pLSPTxRec = IsisUpdGetNextTxRec (pLSPTxRec,
                                                         &pTxHashBkt);
                        u1TxLoc = ISIS_LOC_TX;
                    }
                    else if ((i4DbRetCode < 0) && (pLSPDbRec != NULL))
                    {
                        /* Peer system does not have the LSP that is
                         * available in the local systems Database
                         */

                        ISIS_DBG_PRINT_ID (pLSPDbRec->
                                           pu1LSP + ISIS_OFFSET_LSPID,
                                           (UINT1) ISIS_LSPID_LEN,
                                           "UPD <T> : Scheduling LSP\t",
                                           ISIS_OCTET_STRING);

                        pNextDbHashBkt = pDbHashBkt;
                        pNextDbRec = IsisUpdGetNextDbRec (pLSPDbRec,
                                                          &pNextDbHashBkt);
                        i4Ret =
                            IsisUpdSchLSPForTx (pContext, (VOID *) pPrevDbRec,
                                                (VOID *) pLSPDbRec, pCktRec,
                                                u1DbLoc, pDbHashBkt,
                                                ISIS_SPEC_CKT);
                        if (i4Ret == ISIS_MEM_FAILURE)
                        {
                            IsisUpdDeleteLSP (pContext, u1Level, au1LspId);
                        }

                        /* Update the pLSPDbRec and pDbHashBkt with the 
                         * pNextDbRec and pNextDbHashBkt, to make sure 
                         * that we point to the correct record in sequence
                         * since pLSPDbRec has moved to TxQ from DB.
                         *
                         * NOTE : We need not change the 'u1DbLoc' since
                         * the LSP Entry which is being scheduled for
                         * transmission is any way moved from the Database
                         * to TxQ and hence the location remains the same
                         */

                        if ((pTxHashBkt != NULL)
                            && ((pPrevTxRec == NULL)
                                && (ISIS_UPD_TX_FLSP (pTxHashBkt)
                                    == pLSPDbRec)))
                        {
                            pPrevTxRec = ISIS_UPD_TX_FE (pTxHashBkt);
                            u1TxLoc = ISIS_LOC_TX;
                        }
                        else if ((pPrevTxRec != NULL)
                                 && (ISIS_UPD_TX_NLSP (pPrevTxRec) ==
                                     pLSPDbRec))
                        {
                            pPrevTxRec = ISIS_UPD_TX_NE (pPrevTxRec);
                            u1TxLoc = ISIS_LOC_TX;
                        }

                        pLSPDbRec = pNextDbRec;
                        pDbHashBkt = pNextDbHashBkt;
                    }
                    else
                    {
                        /* Local system does not have the LSP listed in the
                         * CSNP. Requesting the missing LSP
                         */

                        ISIS_UPD_PRINT_ID (pu1CSNP + u2Offset + 2,
                                           (UINT1) ISIS_LSPID_LEN,
                                           "UPD <T> : Requesting missing LSP\t",
                                           ISIS_OCTET_STRING);
                        IsisUpdConstructPSNP (pContext,
                                              (pu1CSNP + u2Offset), pCktRec,
                                              u1Level, ISIS_LSP_ACQUIRE);

                    }

                    u2Offset = (UINT2) (u2Offset + ISIS_LSPID_LEN + 8);
                    u1Len = (UINT1) (u1Len - (ISIS_LSPID_LEN + 8));
                }
                while (pLSPTxRec != NULL)
                {
                    /* Peer system does not have the LSP that is
                     * available in the local systems Transmit Queue
                     */
                    ISIS_DBG_PRINT_ID (pLSPTxRec->
                                       pLSPRec->pu1LSP + ISIS_OFFSET_LSPID,
                                       (UINT1) ISIS_LSPID_LEN,
                                       "UPD <T> : Scheduling LSP\t",
                                       ISIS_OCTET_STRING);
                    i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevTxRec,
                                                (VOID *) pLSPTxRec, pCktRec,
                                                u1TxLoc, pTxHashBkt,
                                                ISIS_SPEC_CKT);
                    if (i4Ret == ISIS_MEM_FAILURE)
                    {
                        IsisUpdDeleteLSP (pContext, u1Level, au1LspId);
                    }

                    pPrevTxRec = pLSPTxRec;
                    pLSPTxRec = IsisUpdGetNextTxRec (pLSPTxRec, &pTxHashBkt);
                    u1TxLoc = ISIS_LOC_TX;
                }
                while (pLSPDbRec != NULL)
                {
                    /* Peer system does not have the LSP that is
                     * available in the local systems Database
                     */
                    ISIS_DBG_PRINT_ID (pLSPDbRec->
                                       pu1LSP + ISIS_OFFSET_LSPID,
                                       (UINT1) ISIS_LSPID_LEN,
                                       "UPD <T> : Scheduling LSP\t",
                                       ISIS_OCTET_STRING);
                    pNextDbHashBkt = pDbHashBkt;
                    pNextDbRec = IsisUpdGetNextDbRec (pLSPDbRec,
                                                      &pNextDbHashBkt);
                    i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevDbRec,
                                                (VOID *) pLSPDbRec, pCktRec,
                                                u1DbLoc, pDbHashBkt,
                                                ISIS_SPEC_CKT);
                    if (i4Ret == ISIS_MEM_FAILURE)
                    {
                        IsisUpdDeleteLSP (pContext, u1Level, au1LspId);
                    }

                    pLSPDbRec = pNextDbRec;
                    pDbHashBkt = pNextDbHashBkt;

                }
                break;

            default:

                /* Don't bother. Advance the offset as we are now interested
                 * only in the LETLVs
                 */

                u2Offset = (UINT2) (u2Offset + u1Len);
                break;
        }
    }
}

/*******************************************************************************
 * Function    : IsisUpdProcSelfLSP ()
 * Description : This function handles the scenario where the local system
 *               receives an LSP which is generated by itself either in its
 *               previous run or may be in the current run. 
 * Input(s)    : pContext  - Pointer to the system context
 *               pu1LSP    - Pointer to the received LSP
 *               pCktRec   - Pointer to the circuit record
 *               u1Level   - Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdProcSelfLSP (tIsisSysContext * pContext, UINT1 *pu1LSP,
                    tIsisCktEntry * pCktRec, UINT1 u1Level)
{
    INT4                i4RetCode = ISIS_SUCCESS;
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    UINT2               u2RLTime = 0;
    UINT2               u2Len = 0;
    UINT4               u4RcvdSeqNum = 0;
    INT4                i4Ret = ISIS_FAILURE;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisLSPEntry      *pLSPRec = NULL;
    tIsisEvtSeqNoSkip  *pSeqnoSkipEvt = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisEvtOwnLspPurge *pOwnLspPurgeEvt = NULL;
    UINT1               u1AuthType = 0;
    UINT2               u2AuthStatus = 0;
    UINT1              *pu1SelfLsp = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT2               u2RcvdCS = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcSelfLSP ()\n"));

    ISIS_EXTRACT_RLT_FROM_LSP (pu1LSP, u2RLTime);
    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSP, u2Len);
    ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1LSP, u4RcvdSeqNum);
    ISIS_EXTRACT_CS_FROM_LSP (pu1LSP, u2RcvdCS);

    ISIS_FORM_STR (ISIS_LSPID_LEN, pu1LSP, acLSPId);

    /* If the Remaining Life time is equal to zero then Increment own LSP Purge
     * count
     */

    if (u2RLTime == 0)
    {

        pOwnLspPurgeEvt = (tIsisEvtOwnLspPurge *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtOwnLspPurge));
        if (pOwnLspPurgeEvt != NULL)
        {
            pOwnLspPurgeEvt->u1EvtID = ISIS_EVT_OWN_LSP_PURGE;

            IsisUtlSendEvent (pContext, (UINT1 *) pOwnLspPurgeEvt,
                              sizeof (tIsisEvtOwnLspPurge));
        }

        ISIS_INCR_OWN_LSP_PURGE (pContext, u1Level);
    }

    /* The SysISSeqWrapTimer will be active only when Sequence numbers have
     * wrapped around. This means the local system must not generate any
     * LSPs till the timer expires. This will ensure that all the previous
     * LSPs generated by the local system with old sequence numbers expire
     * from the data base of the peer nodes.
     *
     * NOTE: The action on receiving Self-LSP is to generate the same LSP with
     * sequence number as one more than the number included in the PDU. This
     * cannot be done at this moment since we are waiting for the Sequence Wrap
     * Around Timer to fire before we can start transmitting LSPs again
     */

    i4RetCode = ISIS_IS_TIMER_ACTIVE (pContext->SysTimers.SysISSeqWrapTimer);

    if (i4RetCode == ISIS_TRUE)
    {
        ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Seq Wrap Timer Active - Self-LSP - Ignored"
                 " Circuit [%u], Level [%u]\n", pCktRec->u4CktIdx, u1Level));
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcSelfLSP ()\n"));
        return;
    }

    ISIS_EXTRACT_LSPID_FROM_LSP (pu1LSP, au1LSPId);

    /* In the following routine, pPrevRec will get the pointer to a node which
     * is present just before the matching node. This will ensure that we do not
     * search the database again if the matched LSP is to be deleted or moved
     * around for whatever reasons.
     */

    pLSPRec = IsisUpdGetLSP (pContext, au1LSPId, u1Level, &pPrevRec,
                             &pHashBkt, &u1Loc);

    if (pLSPRec != NULL)
    {
        if ((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST))
        {
            i4RetCode = IsisUpdCompareLSPs (pu1LSP,    /* Received LSP */
                                            ((tIsisLSPTxEntry *) pLSPRec)->
                                            pLSPRec->pu1LSP);
            pu1SelfLsp = ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec->pu1LSP;
        }
        else
        {
            i4RetCode = IsisUpdCompareLSPs (pu1LSP,    /* Received LSP */
                                            pLSPRec->pu1LSP /* LSP from DB */ );
            pu1SelfLsp = pLSPRec->pu1LSP;
        }
        UPP_PT ((ISIS_LGST,
                 "UPD <E> : Self LSP [%s][%s] RLT [%d] Seq.No [%x] ChkSum [%x] Received! - On Circuit Index [ %u ] - Code [%s]\n",
                 acLSPId, ISIS_GET_LEVEL_STR (u1Level), u2RLTime, u4RcvdSeqNum,
                 u2RcvdCS, pCktRec->u4CktIdx,
                 ISIS_GET_LSPCMP_CODE_STR (i4RetCode)));
        switch (i4RetCode)
        {
            case ISIS_SAME_CS_GRTR_SEQNO:
            case ISIS_DIFF_CS_GRTR_SEQNO:
            case ISIS_DIFF_CS_SAME_SEQNO:

                /* Recvd LSP has a greater Sequence number than the 
                 * one in the LSP Database. Don't care about Checksum.
                 */
                /* Sec 7.3.16.1 , Second paragraph for action on receipt of 
                 * Self LSP with greater sequence number
                 */

                ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1LSP, u4RcvdSeqNum);

                if (u4RcvdSeqNum == ISIS_MAX_SEQNO)
                {
                    /* We have to increment the sequence number by one more than
                     * the received sequence number and schedule the LSP for
                     * transmission. If the received sequence number is itself a
                     * maximum value, adding anything to that would make the
                     * sequence number wrap around. Hence start the Sequence
                     * Number Wrap Around Timer and ignore the received LSP
                     */

                    IsisUpdProcMSNExceeded (pContext, u1Level,
                                            (pu1LSP + ISIS_OFFSET_LSPID));
                    ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);

                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdProcSelfLSP ()\n"));
                    return;
                }

                /* Update the Self LSP Sequnece number by one greater than 
                 * that of the Recvd LSP Sequence number and shcedule the LSP in
                 * the database for transmission
                 */

                u4RcvdSeqNum++;

                IsisUpdUpdateSelfLSPSeqNum (pContext, au1LSPId, u4RcvdSeqNum,
                                            u1Level);

                /* Self LSPs are maintained as a list of TLVs for easy updation.
                 * A copy of the constructed LSP is maintained in the LSP
                 * database. IsisUpdUpdateSelfLSPSeqNum () will update the
                 * Self LSPs maintained as TLVs. Hence update the sequence 
                 * number for the LSP we have fetched from Database and schedule
                 * the LSP for transmission
                 */
                ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1SelfLsp, u2Len);
                u1AuthType = (UINT1) (((u1Level == ISIS_LEVEL1) ?
                                       pContext->SysActuals.u1SysAreaAuthType :
                                       pContext->SysActuals.
                                       u1SysDomainAuthType));

                u2AuthStatus = (UINT2) (((u1Level == ISIS_LEVEL1) ?
                                         pContext->SelfLSP.pL1NSNLSP->pZeroLSP->
                                         u2AuthStatus : pContext->SelfLSP.
                                         pL2NSNLSP->pZeroLSP->u2AuthStatus));

                if (u1AuthType == ISIS_AUTH_MD5)
                {
                    if (((u1Level != ISIS_LEVEL2)
                         && (pContext->SysActuals.SysAreaTxPasswd.u1Len != 0))
                        || ((u1Level != ISIS_LEVEL1)
                            && (pContext->SysActuals.SysDomainTxPasswd.u1Len !=
                                0)))
                    {
                        if (u2AuthStatus == ISIS_TRUE)
                        {
                            ISIS_SET_LSP_SEQNO (pu1SelfLsp, u4RcvdSeqNum);
                            IsisUpdRemoveAuthFromLSP (pu1SelfLsp, &u2Len);
                            IsisUpdAddAuthToPDU (pContext, pu1SelfLsp, &u2Len,
                                                 u1Level);
                        }
                    }
                }

                IsisUpdModifyLSP (pContext, pLSPRec, ISIS_LSP_MAXAGE,
                                  u4RcvdSeqNum, u1Loc);

                /* Send this LSP over all Circuits
                 */

                i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                            (VOID *) pLSPRec, NULL, u1Loc,
                                            pHashBkt, ISIS_ALL_CKT);
                if (i4Ret == ISIS_MEM_FAILURE)
                {
                    IsisUpdDeleteLSP (pContext, u1Level, au1LSPId);
                }

                /* Generate the Sequence number skip event and Post to Control 
                 * Queue
                 */

                pSeqnoSkipEvt = (tIsisEvtSeqNoSkip *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSeqNoSkip));
                if (pSeqnoSkipEvt != NULL)
                {
                    pSeqnoSkipEvt->u1EvtID = ISIS_EVT_SEQ_NO_SKIP;

                    IsisUtlSendEvent (pContext, (UINT1 *) pSeqnoSkipEvt,
                                      sizeof (tIsisEvtSeqNoSkip));
                }
                ISIS_INCR_SEQNO_SKIP_STAT (pContext, u1Level);
                ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);
                break;

            case ISIS_SAME_CS_LESS_SEQNO:
            case ISIS_DIFF_CS_LESS_SEQNO:

                /* The received LSP is having less sequence number than the one
                 * existing in the Database. Schedule the LSP present in the
                 * Database for transmission since what we have is the latest.
                 * This LSP must be transmitted only on the specified circuit
                 * over which we have received the LSP with less sequence number
                 */

                i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                            (VOID *) pLSPRec, pCktRec, u1Loc,
                                            pHashBkt, ISIS_SPEC_CKT);
                if (i4Ret == ISIS_MEM_FAILURE)
                {
                    IsisUpdDeleteLSP (pContext, u1Level, au1LSPId);
                }

                ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);
                break;

            case ISIS_SAME_CS_SAME_SEQNO:

                /* The received LSP is having Same Sequence Number as the one
                 * existing in Database. Transmit a PSNP and clear the SRM flags
                 * if already set for the circuit on which the LSP has been
                 * received
                 */

                if (u2RLTime != 0)
                {
                    /* We can acknowledge the receipt of the LSP since there is
                     * no difference between the received LSP and the one in the
                     * database
                     */

                    IsisUpdClearSRM (pContext, (tIsisLSPTxEntry *) pLSPRec,
                                     (tIsisLSPTxEntry *) pPrevRec,
                                     pCktRec, u1Loc, pHashBkt, u1Level);
                    if (pCktRec->u1CktType == ISIS_P2P_CKT)
                    {
                        /* If the LSP is received on Point to Point Ckt, Construct 
                         * PSNP which should be transmitted back to the originating 
                         * system as an acknowledgement
                         */

                        IsisUpdConstructPSNP (pContext,
                                              (pu1LSP + ISIS_OFFSET_RLT),
                                              pCktRec, u1Level, 0);
                    }
                }
                else
                {
                    /* Self LSP with Zero RLT, Schedule the LSP from the 
                     * database for transmission
                     */

                    i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                                (VOID *) pLSPRec, pCktRec,
                                                u1Loc, pHashBkt, ISIS_SPEC_CKT);
                    if (i4Ret == ISIS_MEM_FAILURE)
                    {
                        IsisUpdDeleteLSP (pContext, u1Level, au1LSPId);
                    }
                }
                ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);
                break;

            default:

                /* REFERENCE : 7.3.15.1 c
                 */

                /* Should never happen - If it happens purge the received LSP
                 * from the network
                 */

                ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1LSP, u4RcvdSeqNum);

                if (u4RcvdSeqNum == ISIS_MAX_SEQNO)
                {
                    ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1LSP);
                    UPP_PT ((ISIS_LGST, "UPD <T> : Max Seq. No. Exceeded -"
                             " Received Seq. No [ %u ], Max. Seq. No [ %u ]\n",
                             u4RcvdSeqNum, ISIS_MAX_SEQNO));
                    IsisUpdProcMSNExceeded (pContext, u1Level,
                                            (pu1LSP + ISIS_OFFSET_LSPID));
                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdProcSelfLSP ()\n"));
                    return;
                }
                u4RcvdSeqNum++;
                IsisUpdUpdateSelfLSPSeqNum (pContext, au1LSPId, u4RcvdSeqNum,
                                            u1Level);
                IsisUpdModifyLSP (pContext, pLSPRec, ISIS_LSP_MAXAGE,
                                  u4RcvdSeqNum, u1Loc);

                /* Send this LSP over all Circuits
                 */

                i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                            (VOID *) pLSPRec, NULL, u1Loc,
                                            pHashBkt, ISIS_ALL_CKT);
                if (i4Ret == ISIS_MEM_FAILURE)
                {
                    IsisUpdDeleteLSP (pContext, u1Level, au1LSPId);
                }

                /* Generate the Sequence number skip event and Post to Control 
                 * Queue
                 */

                pSeqnoSkipEvt = (tIsisEvtSeqNoSkip *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSeqNoSkip));
                if (pSeqnoSkipEvt != NULL)
                {
                    pSeqnoSkipEvt->u1EvtID = ISIS_EVT_SEQ_NO_SKIP;

                    IsisUtlSendEvent (pContext, (UINT1 *) pSeqnoSkipEvt,
                                      sizeof (tIsisEvtSeqNoSkip));
                }
                ISIS_INCR_SEQNO_SKIP_STAT (pContext, u1Level);
                IsisUpdNWPurgeLSP (pContext, pu1LSP);
                break;
        }
    }

    else
    {
        /* The Received Self LSP does not exist in the Databse. Purge the
         * received LSP
         */

        UPP_PT ((ISIS_LGST,
                 "UPD <T> : [Purging] Received Non-Existing Self LSP [%s]"
                 " Circuit [%u], Level [%s]\n", acLSPId, pCktRec->u4CktIdx,
                 ISIS_GET_LEVEL_STR (u1Level)));
        IsisUpdNWPurgeLSP (pContext, pu1LSP);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcSelfLSP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdProcLETLV ()
 * Description : This function Processes LSP Entries TLVs included in the
 *               received SNP. It reads the LSPID, CS and Sequence Number
 *               information included in the TLV and does the following:
 *               -- CASE: Same CS and Same SN - consider this as an ack
 *               -- CASE: CS (Don't Care) and 
 *                        Less SN             - Schedule the LSP for
 *                                              transmission
 *               -- CASE: CS (Don't Care) and
 *                        Greater SN          - Construct PSNP requesting the
 *                                              LSP
 * Input(s)    : pContext  - Pointer to the system context
 *               pu1SNP    - Pointer to received SNP
 *               pRec      - Pointer to the LSPEntry/LSPTxEntry
 *               pPrevRec  - Pointer to the Previous LSPEntry/LSPTxEntry
 *               pCktRec   - Pointer to circuit record
 *               u1Loc     - Location of LSP - either Transmit Queue or Database
 *               u1Level   - Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified             
 * Returns     : ISIS_MOVED_TO_DB  - if the Transmission Entry has moved to 
 *                                   LSP Database as indicated by 
 *                                   IsisUpdClearSRM (),
 *               ISIS_MOVED_TO_TXQ - if the Database Entry has moved to TxQ
 *                                   when IsisUpdSchLSPForTx () in invoked,
 *               ISIS_NOT_MOVED    - if Entry Location does not change
 ******************************************************************************/

PRIVATE UINT1
IsisUpdProcLETLV (tIsisSysContext * pContext, UINT1 *pu1SNP, VOID *pRec,
                  VOID *pPrevRec, tIsisCktEntry * pCktRec, UINT1 u1Loc,
                  tIsisHashBucket * pHashBkt, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4Ret = ISIS_FAILURE;
    UINT1              *pu1LSP = NULL;
    UINT1               u1Status = ISIS_NOT_MOVED;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcLETLV ()\n"));

    if ((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST))
    {
        /* An LSP corresponding to the LSPID included in the SNP has been taken
         * from the Transmit Queue 
         */

        pu1LSP = ((tIsisLSPTxEntry *) pRec)->pLSPRec->pu1LSP;
    }
    else
    {
        /* An LSP corresponding to the LSPID included in the SNP has been taken
         * from the Database
         */

        pu1LSP = ((tIsisLSPEntry *) pRec)->pu1LSP;
    }

    /* Compare the LSP information included in the SNP with the information
     * present in the LSP fetched either from the transmit queue or database
     */

    i4RetVal = IsisUpdCompLSPHdr (pu1LSP, pu1SNP);

    switch (i4RetVal)
    {
        case ISIS_SAME_CS_SAME_SEQNO:

            /* We have received information which exactly matches the LSP
             * information that we have in either in our database or
             * transmit queue. Consider this as an acknowledgement and clear
             * the SRM flags if any
             */

            u1Status = IsisUpdClearSRM (pContext, pRec, pPrevRec, pCktRec,
                                        u1Loc, pHashBkt, u1Level);
            if (u1Status == ISIS_TRUE)
            {
                u1Status = ISIS_MOVED_TO_DB;
            }
            break;

        case ISIS_SAME_CS_LESS_SEQNO:
        case ISIS_DIFF_CS_LESS_SEQNO:

            /* Peer system seems to have older information. Schedule the LSP
             * from our database for transmission
             */

            i4Ret =
                IsisUpdSchLSPForTx (pContext, pPrevRec, pRec, pCktRec, u1Loc,
                                    pHashBkt, ISIS_SPEC_CKT);
            if (i4Ret == ISIS_MEM_FAILURE)
            {
                IsisUpdDeleteLSP (pContext, u1Level, pu1SNP + 2);
            }

            if ((u1Loc == ISIS_LOC_DB) || (u1Loc == ISIS_LOC_DB_FIRST))
            {
                u1Status = ISIS_MOVED_TO_TXQ;
            }
            break;

        case ISIS_DIFF_CS_GRTR_SEQNO:
        case ISIS_SAME_CS_GRTR_SEQNO:

            /* Peer system seems to have latest information. Request for the
             * information from the peer
             */

            IsisUpdConstructPSNP (pContext, pu1SNP, pCktRec, u1Level,
                                  ISIS_LSP_ACQUIRE);
            u1Status =
                IsisUpdClearSRM (pContext, pRec, pPrevRec, pCktRec, u1Loc,
                                 pHashBkt, u1Level);
            if (u1Status == ISIS_TRUE)
            {
                u1Status = ISIS_MOVED_TO_DB;
            }
            break;

        case ISIS_DIFF_CS_SAME_SEQNO:

            /* This can never happen since if sequence number is same, we do not
             * expect the contents to change
             */

            i4Ret =
                IsisUpdSchLSPForTx (pContext, pPrevRec, pRec, pCktRec, u1Loc,
                                    pHashBkt, ISIS_SPEC_CKT);
            if (i4Ret == ISIS_MEM_FAILURE)
            {
                IsisUpdDeleteLSP (pContext, u1Level, pu1SNP + 2);
            }

            if ((u1Loc == ISIS_LOC_DB) || (u1Loc == ISIS_LOC_DB_FIRST))
            {
                u1Status = ISIS_MOVED_TO_TXQ;
            }
            break;
        default:
            break;

    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcLETLV ()\n"));
    return u1Status;
}

/******************************************************************************
 * Function    : IsisUpdConstructPSNP ()
 * Description : This function constructs an LETLV corresponding to the given
 *               LSP and adds the TLV to the existing list of TLVs. These TLVs
 *               will be used for constructing a PSNP buffer during PSNP
 *               timeout.
 * Input(s)    : pContext  - Pointer to the system context
 *               pu1LSPHdr - Pointer to the offset in the LSP where the
 *                           LETLV specific information starts
 *
 *               NOTE: This offset is 2 bytes after the PDU length in the LSP.
 *
 *               pCktRec   - Pointer to the circuit record 
 *               u1Level   - Level of the LSP
 *               u1Flag    - Flag indicates whether it is a request for LSP or
 *                           an acknowledgement
 *
 *               NOTE: PSNPs are used for both requesting LSPs as well as
 *               acknowledgements. For requesting an LSP the LSP Seq. No. wll be
 *               set to '0' in the PSNP being transmitted.
 *
 * Output(s)   : None
 * Globals     : Not Referred or Modified          
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdConstructPSNP (tIsisSysContext * pContext, UINT1 *pu1LSPHdr,
                      tIsisCktEntry * pCktRec, UINT1 u1Level, UINT1 u1Flag)
{
    tIsisLETLV         *pPSNPEntry = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdConstructPSNP ()\n"));

    pPSNPEntry = (tIsisLETLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS,
                                                sizeof (tIsisLETLV));

    if (pPSNPEntry != NULL)
    {
        ISIS_GET_2_BYTES (pu1LSPHdr, (UINT2) 0, pPSNPEntry->u2RLTime);
        ISIS_GET_DATA (pPSNPEntry->au1LSPId, (pu1LSPHdr + 2), ISIS_LSPID_LEN);
        ISIS_GET_4_BYTES (pu1LSPHdr, (2 + ISIS_LSPID_LEN),
                          pPSNPEntry->u4SeqNum);
        ISIS_GET_2_BYTES (pu1LSPHdr, (2 + ISIS_LSPID_LEN + 4),
                          pPSNPEntry->u2CheckSum);
        pPSNPEntry->pNext = NULL;

        /* If the PSNP is for requesting an LSP from the Peer, Mark the Seq. No
         * as ZERO
         */

        if (u1Flag == ISIS_LSP_ACQUIRE)
        {
            pPSNPEntry->u4SeqNum = 0;
            ISIS_DBG_PRINT_ID (pPSNPEntry->au1LSPId, (UINT1) ISIS_LSPID_LEN,
                               "UPD <T> : Requesting Information About LSPID\t",
                               ISIS_OCTET_STRING);
        }
    }
    else
    {
        /* Memory not available for costructing LETLV for the received LSP
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : PSNP LETLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdConstructPSNP ()\n"));
        return;
    }

    IsisAdjAddPSNP (pCktRec, pPSNPEntry, u1Level);

    /* if u1ECTId is non-zero, then either CSNP or PSNP timer is active. If a
     * CSNP timer is active, we cannotstart the PSNP timer. The PSNP timer will
     * be started once all the CSNPs are tranmsitted over the circuit
     */

    if ((u1Level == ISIS_LEVEL1) && (pCktRec->pL1CktInfo->u1ECTId == 0))
    {
        IsisTmrStartECTimer (pContext, ISIS_ECT_L1_PSNP, pCktRec->u4CktIdx,
                             pCktRec->pL1CktInfo->PSNPInterval,
                             &(pCktRec->pL1CktInfo->u1ECTId));
    }
    if ((u1Level == ISIS_LEVEL2) && (pCktRec->pL2CktInfo->u1ECTId == 0))
    {
        IsisTmrStartECTimer (pContext, ISIS_ECT_L2_PSNP, pCktRec->u4CktIdx,
                             pCktRec->pL2CktInfo->PSNPInterval,
                             &(pCktRec->pL2CktInfo->u1ECTId));
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdConstructPSNP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdEnterWaitingState ()
 * Description : This function Starts Waiting timer for the appropriate level 
 *               and Updates the statistics
 * Input(s)    : pContext - Pointer to the system context
 *               u1Level  - Level of the LSP Database 
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns   : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdEnterWaitingState (tIsisSysContext * pContext, UINT1 u1Level)
{
    eIsisTimerId        TimerId;
    tIsisTimer         *pTimer = NULL;
    tIsisEvtLSPDBOL    *pEvtLSPDBOL = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdEnterWaitingState ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        pTimer = &pContext->SysL1Info.SysWaitingTimer;
        TimerId = ISIS_L1WAITING_TIMER;
    }
    else
    {
        pTimer = &pContext->SysL2Info.SysWaitingTimer;
        TimerId = ISIS_L2WAITING_TIMER;
    }

    /* Check the state of the timer. If the timer is already started,
     * then restart the timer and return.
     */

    if ((ISIS_IS_TIMER_ACTIVE (*pTimer)) == ISIS_TRUE)
    {
        IsisTmrRestartTimer (pTimer, TimerId,
                             pContext->SysActuals.u2SysWaitTime);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdEnterWaitingState ()\n"));
        return;
    }
    else
    {
        /* Start the LSP Waiting timer 
         */

        pTimer->pContext = pContext;
        IsisTmrStartTimer (pTimer, TimerId, pContext->SysActuals.u2SysWaitTime);

        if (u1Level == ISIS_LEVEL1)
        {
            ISIS_INCR_L1DBOL_STAT (pContext, u1Level);
            pContext->SysActuals.u1SysL1State = ISIS_STATE_WAITING;
        }
        else
        {
            ISIS_INCR_L2DBOL_STAT (pContext);
            pContext->SysActuals.u1SysL2State = ISIS_STATE_WAITING;
        }
    }

    pEvtLSPDBOL = (tIsisEvtLSPDBOL *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                      sizeof (tIsisEvtLSPDBOL));
    if (pEvtLSPDBOL == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Posting DBOL Evt\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_EVTS);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdEnterWaitingState ()\n"));
        return;
    }
    pEvtLSPDBOL->u1EvtID = ISIS_EVT_LSP_DBOL;
    pEvtLSPDBOL->u1Level = u1Level;

    /* Post the LSPDBOL event to the Control Module
     */

    UPP_PT ((ISIS_LGST, "UPD <T> : Posting Database OverLoad Event\n"));
    IsisUtlSendEvent (pContext, (UINT1 *) pEvtLSPDBOL,
                      sizeof (tIsisEvtLSPDBOL));

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdEnterWaitingState ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdClearSRM ()
 * Description : This function clears SRM flag bit for the specified circuit. 
 *               If all the SRM Flag bits are cleared, then the LSP is moved
 *               back to the Database from the Transmission Queue since a '0'
 *               SRM Flag means that the LSP has received all acknowledgements
 *               it is waiting for.
 * Input(s)    : pContext  - Pointer to the system context
 *               pTxRec    - Pointer to the LSP Record
 *               pPrevRec  - Pointer to the previous LSP Record
 *               pCktRec   - Pointer to the circuit record 
 *               u1Loc     - Location of LSP Record
 *               pHashBkt  - Pointer to the Hash Bucket where the LSP is
 *                           located 
 *               u1Level   - Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_TRUE if the Transmission Entry has moved to LSP Database
 *               ISIS_FALSE otherwise
 ******************************************************************************/

PUBLIC UINT1
IsisUpdClearSRM (tIsisSysContext * pContext, tIsisLSPTxEntry * pTxRec,
                 tIsisLSPTxEntry * pPrevRec, tIsisCktEntry * pCktRec,
                 UINT1 u1Loc, tIsisHashBucket * pHashBkt, UINT1 u1Level)
{
    UINT1               u1Mask = 0;
    UINT1               u1RelFlag = ISIS_FALSE;
    UINT1               u4ByteCnt = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdClearSRM ()\n"));

    if (((u1Loc == ISIS_LOC_NONE) || (u1Loc == ISIS_LOC_DB))
        || (u1Loc == ISIS_LOC_DB_FIRST))
    {
        /* If the LSP is not present either in TxQ or Database or if it is 
         * present is Database, Clearing SRM flags does not make any sense,
         * since SRM flags are available only in TxQ.
         */
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdClearSRM ()\n"));
        return u1RelFlag;
    }

    /* Get the bit position corresponding to the Circuit Index of the specified
     * Circuit. Based on the Circuit Index calculate the Bit Mask which will be
     * used for clearing the corresponding bit in the SRM flags. 
     */

    ISIS_BIT_MASK (pCktRec->u4CktIdx, u1Mask);

    /* SRM flags are maintained as sequence of bits, each bit representing a
     * circuit. Hence a multi-octet array is used to represent these flags. In
     * order to clear a bit, we first have to move to the corresponding byte,
     * then calculate a mask value which will clear the required bit.
     * ISIS_BYTE_POS gets the corresponding byte value, and ISIS_BIT_MASK gets
     * the appropriate mask
     */

    if ((pTxRec != NULL) && (pTxRec->pu1SRM != NULL))
    {
        pTxRec->pu1SRM[ISIS_BYTE_POS (pCktRec->u4CktIdx)] =
            (UINT1) (pTxRec->
                     pu1SRM[ISIS_BYTE_POS (pCktRec->u4CktIdx)] & (~u1Mask));
    }
    /*
     * NOTE: SRM bits are set for LSPs to process acknowledgements. Once an
     * acknowledgement is received then the SRM bit for the corresponding
     * circuit will be cleared. LSPs will be maintained in the Transmission
     * queue till all acknowledgements are received i.e. all SRM bits are
     * cleared. Once all bits are cleared the LSP is moved to the Database
     */

    /* Check whether any other bits are set in the SRM flags for this LSP. If no
     * bits are set then the LSP can be moved to the Database since no other
     * acknowledgements are expected for this LSP.
     */

    u1RelFlag = ISIS_TRUE;

    if (pTxRec != NULL)
    {
        for (u4ByteCnt = 0; u4ByteCnt < pTxRec->u1SRMLen; u4ByteCnt++)
        {
            if ((pTxRec->pu1SRM != NULL)
                && (*(pTxRec->pu1SRM + u4ByteCnt) != 0))
            {
                /* Some bits are still set. Do not move the LSP back to Database
                 */

                u1RelFlag = ISIS_FALSE;
                break;
            }
        }

        if (u1RelFlag == ISIS_TRUE)
        {
            /* No more bits set in SRM flags. Remove from Transmit Queue
             */

            IsisUpdRemLSPFromTxQ (pContext, pPrevRec, pTxRec, pHashBkt, u1Level,
                                  u1Loc);

            /* Add the LSP back to the LSP Database
             */

            IsisUpdAddToDb (pContext, pTxRec->pLSPRec, u1Level);
            pTxRec->pLSPRec = NULL;

            /* Free both the SRM Flags and the TxQ entry */

            ISIS_MEM_FREE (ISIS_BUF_SRMF, pTxRec->pu1SRM);
            pTxRec->pu1SRM = NULL;
            ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pTxRec);
        }

    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdClearSRM ()\n"));
    return u1RelFlag;
}

/******************************************************************************
 * Function    : IsisUpdClearPSNP ()
 * Description : This function removes any information pertaining to the given
 *               LSP from the PSNP buffers.
 * Input (s)   : pContext  - Pointer to system context 
 *               pu1LSP    - Pointer to LSP
 *               pCktRec   - Pointer to Circuit Entry
 *               u1CktFlag - Circuit Flag specifying the circuits over which
 *                           the LSP was scheduled for transmission 
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdClearPSNP (tIsisSysContext * pContext, UINT1 *pu1LSP,
                  tIsisCktEntry * pCktRec, UINT1 u1CktFlag)
{
    UINT1               u1Level = ISIS_UNKNOWN;
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    tIsisCktLevel      *pCktLvlRec = NULL;
    tIsisCktEntry      *pRec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <E> : Entered IsisUpdClearPSNP () \n"));

    ISIS_EXTRACT_LSPID_FROM_LSP (pu1LSP, au1LSPId);

    ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) ISIS_LSPID_LEN,
                       "UPD <T> : Deleting Info From PSNP For\t",
                       ISIS_OCTET_STRING);
    u1Level =
        (UINT1) ((ISIS_EXTRACT_PDU_TYPE (pu1LSP) == ISIS_L1LSP_PDU) ?
                 ISIS_LEVEL1 : ISIS_LEVEL2);

    /* PSNPs are maintained for each circuit separately in the circuit level
     * records. LSP information pertaining to 'au1LSPId' must be deleted from
     * PSNPs held in each of the circuit depending on 'u1CktFlag'. 
     */

    switch (u1CktFlag)
    {
        case ISIS_SPEC_CKT:

            /* Delete specified LSP Information from the PSNP buffer held in the
             * specified circuit
             */

            if (u1Level == ISIS_LEVEL1)
            {
                pCktLvlRec = pCktRec->pL1CktInfo;
            }
            else
            {
                pCktLvlRec = pCktRec->pL2CktInfo;
            }

            IsisAdjDelPSNPNode (pCktLvlRec, au1LSPId);
            break;

        case ISIS_ALL_BUT_SPEC_CKT:

            /* Delete specified LSP Information from the PSNP buffers held in
             * all the circuits except the specified circuit
             */

            for (pRec = pContext->CktTable.pCktRec; pRec != NULL;
                 pRec = pRec->pNext)
            {
                if (pRec == pCktRec)
                {
                    continue;
                }
                if (u1Level == ISIS_LEVEL1)
                {
                    pCktLvlRec = pRec->pL1CktInfo;
                }
                else
                {
                    pCktLvlRec = pRec->pL2CktInfo;
                }

                if (pCktLvlRec != NULL)
                {
                    IsisAdjDelPSNPNode (pCktLvlRec, au1LSPId);
                }
            }
            break;

        case ISIS_ALL_CKT:

            /* Delete LSP Information from PSNP buffers held on all the circuits
             */

            for (pRec = pContext->CktTable.pCktRec; pRec != NULL;
                 pRec = pRec->pNext)
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    pCktLvlRec = pRec->pL1CktInfo;
                }
                else
                {
                    pCktLvlRec = pRec->pL2CktInfo;
                }

                if (pCktLvlRec != NULL)
                {
                    IsisAdjDelPSNPNode (pCktLvlRec, au1LSPId);
                }
            }
            break;

        default:

            /* Not possible */

            UPP_PT ((ISIS_LGST, "UPD <T> : Unknown Circuit Flag [ %u ]\n",
                     u1CktFlag));
            break;
    }

    UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdClearPSNP () \n"));
}

/******************************************************************************
 * Function    : IsisUpdSchLSPForTx ()
 * Description : This function marks the LSP for transmission on the circuits
 *               specified by 'u1CktFlag'. 
 * Input (s)   : pContext  - Pointer to the system context
 *             : pPrevRec  - Pointer to previous Record if available in 
 *                           Database/Transmission Queue, else NULL
 *             : pRec      - Pointer to LSP Record 
 *             : pCktRec   - Pointer to circuit record
 *             : u1Loc     - Location of the LSP Record which is either
 *                           ISIS_LOC_TX       : present in TxQ
 *                           ISIS_LOC_TX_FIRST : first entry in TxQ
 *                           ISIS_LOC_NONE     : not present in either TxQ or 
 *                                               DB,
 *                           ISIS_LOC_DB       : present in DB
 *                           ISIS_LOC_DB_FIRST : first entry in DB
 *             : pHaskBkt  - Pointer to the Hash Bucket where the LSP is located
 *             : u1CktFlag - Circuit flag indicating the circuits over which
 *                           the LSP has to be transmitted
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On successful marking of the LSP
 *               ISIS_FAILURE, On failure
 ******************************************************************************/

PUBLIC INT4
IsisUpdSchLSPForTx (tIsisSysContext * pContext, VOID *pPrevRec, VOID *pRec,
                    tIsisCktEntry * pCktRec, UINT1 u1Loc,
                    tIsisHashBucket * pHashBkt, UINT1 u1CktFlag)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Level = ISIS_UNKNOWN;
    UINT4               u4CktIdx = 0;
    tIsisLSPEntry      *pLSPRec = NULL;
    tIsisLSPTxEntry    *pLSPTxRec = NULL;
    tIsisEvtTxQFull    *pTxQFullEvt = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT1               au1LSPId[ISIS_LSPID_LEN];

    UPP_EE ((ISIS_LGST, "UPD <E> : Entered IsisUpdSchLSPForTx () \n"));

    u4CktIdx = (pCktRec == NULL) ? 0 : pCktRec->u4CktIdx;
    if (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART)
    {
        /* While router is in restarter state should not generate LSP */
        return ISIS_SUCCESS;
    }
    if ((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST))
    {
        /* Given LSP record is already in trasnmission queue. We may have to
         * just update the circuit bit (SRM flag) mask
         */

        pLSPTxRec = (tIsisLSPTxEntry *) (pRec);
        u1Level = (UINT1) (ISIS_EXTRACT_PDU_TYPE (pLSPTxRec->pLSPRec->pu1LSP));
        u1Level = (u1Level == ISIS_L1LSP_PDU) ? ISIS_LEVEL1 : ISIS_LEVEL2;

        /* See whether we can propogate the PDU on required circuits for the
         * given circuit flag
         */

        i4RetVal = IsisUpdChkCktMask (pContext, u1CktFlag, u1Level);

        /* LSP cannot be propagated as the circuit mask is not consistent with
         * the 'u1CktFlag'
         */

        if (i4RetVal == ISIS_FAILURE)
        {
            /* Remove the LSP from TxQ and put it back in the Database
             */

            IsisUpdRemLSPFromTxQ (pContext, pPrevRec, pLSPTxRec, pHashBkt,
                                  u1Level, u1Loc);
            IsisUpdAddToDb (pContext, pLSPTxRec->pLSPRec, u1Level);

            pLSPTxRec->pLSPRec = NULL;
            if (pLSPTxRec->pu1SRM != NULL)
            {
                ISIS_MEM_FREE (ISIS_BUF_SRMF, pLSPTxRec->pu1SRM);
                pLSPTxRec->pu1SRM = NULL;
            }
            ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pLSPTxRec);
            return i4RetVal;
        }
    }
    else
    {
        /* LSP is from the DB. Hence form the LSP Transmission record and 
         * Insert the Record into LSP Transmission Queue so that it will be
         * scheduled for transmission
         */

        pLSPRec = (tIsisLSPEntry *) pRec;

        u1Level =
            (UINT1) ((ISIS_EXTRACT_PDU_TYPE (pLSPRec->pu1LSP) == ISIS_L1LSP_PDU)
                     ? ISIS_LEVEL1 : ISIS_LEVEL2);

        /* See whether we can propogate the PDU on required circuits for the
         * given circuit flag
         */

        i4RetVal = IsisUpdChkCktMask (pContext, u1CktFlag, u1Level);

        /* LSP cannot be propagated as the circuit mask is not consistent with
         * the 'u1CktFlag'
         */

        if (i4RetVal == ISIS_FAILURE)
        {
            if (u1Loc == ISIS_LOC_NONE)
            {
                /* This LSP is received from Peer and is currently not in the
                 * database. If we are unable to propogate the LSP it must be
                 * deleted and must not be stored in the database
                 */

                IsisUpdDelLspTimerNode (pLSPRec);
                ISIS_MEM_FREE (ISIS_BUF_LSPI, pLSPRec->pu1LSP);
                pLSPRec->pu1LSP = NULL;
                ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pLSPRec);
            }
            return i4RetVal;
        }

        pLSPTxRec = (tIsisLSPTxEntry *)
            ISIS_MEM_ALLOC (ISIS_BUF_LTXQ, sizeof (tIsisLSPTxEntry));
        if (pLSPTxRec != NULL)
        {
            pLSPTxRec->pLSPRec = pLSPRec;
        }
        else
        {
            /* Allocation for Transmission Queue Entry failed. Post the LSP TXQ
             * Full event 
             */

            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Memory For TxQ Entry\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            pTxQFullEvt = (tIsisEvtTxQFull *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtTxQFull));
            if (pTxQFullEvt != NULL)
            {
                pTxQFullEvt->u1EvtID = ISIS_EVT_TX_Q_FULL;
                MEMCPY (pTxQFullEvt->au1LspID,
                        (pLSPRec->pu1LSP + ISIS_OFFSET_LSPID), ISIS_SYS_ID_LEN);

                IsisUtlSendEvent (pContext, (UINT1 *) pTxQFullEvt,
                                  sizeof (tIsisEvtTxQFull));
            }

            /* Enter Waiting state as LSP cannot be transmitted
             */

            IsisUpdEnterWaitingState (pContext, u1Level);

            ISIS_DBG_PRINT_ID ((pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                               (UINT1) ISIS_SYS_ID_LEN,
                               "UPD <E> : Unable To Tx LSP\t",
                               ISIS_OCTET_STRING);

            ISIS_EXTRACT_LSPID_FROM_LSP (pLSPRec->pu1LSP, au1LSPId);
            ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
            UPP_PT ((ISIS_LGST, "UPD <E> : Unable To Tx LSP - LSP ID [ %s ]\n",
                     acLSPId));
            UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdSchLSPForTx ()\n"));
            return ISIS_FAILURE;
        }

        /* Delete the LSP Entry from the LSP Database only if the LSP is
         * currently held in the DB
         */

        if ((u1Loc != ISIS_LOC_NONE) && (u1Loc != ISIS_LOC_SELF))
        {
            /* ISIS_LOC_NONE is used while purging Self LSPs received from Peer
             * nodes which are not present in the Database and hence these LSPs
             * need not be removed from the Database
             * ISIS_LOC_SELF is used when a Self LSP is generated for the first
             * time and hence will not be available in the Database, it will be
             * directly added to Transmission Queue
             */

            IsisUpdRemLSPFromDB (pContext, pPrevRec, pRec, pHashBkt, u1Level,
                                 u1Loc);
        }

        /* Insert the LSP into the Transmission Q 
         */

        IsisUpdAddToTxQ (pContext, pLSPTxRec, u1Level);
    }

    /* The LSP is already in the Transmission queue. Set the SRM flags
     * appropriately based on the 'u1CktFlag'
     */

    /* If Circuit Record is available then the 'u1CktFlag' must indicate either
     * SPECIFIED_CIRCUIT or ALL_BUT_SPECIFIED_CIRCUIT. Otherwise a value of '0'
     * is passed which indictaes ALL_CIRCUITS
     */

    i4RetVal = IsisUpdSetCktBitPat (pContext, &(pLSPTxRec->pu1SRM),
                                    ((u1Level == ISIS_LEVEL1)
                                     ? pContext->CktTable.pu1L1CktMask
                                     : pContext->CktTable.pu1L2CktMask),
                                    &(pLSPTxRec->u1SRMLen),
                                    u4CktIdx, u1CktFlag);

    if ((i4RetVal == ISIS_SUCCESS) && (pCktRec != NULL))    /*klocwork */
    {
        /* The LSP is being scheduled for transmission. Remove this Information
         * from PSNP if already included
         */

        IsisUpdClearPSNP (pContext, pLSPTxRec->pLSPRec->pu1LSP, pCktRec,
                          u1CktFlag);
    }
    if (i4RetVal == ISIS_MEM_FAILURE)
    {
        return ISIS_MEM_FAILURE;
    }

    UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdSchLSPForTx ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdChkCktMask ()
 * Description : This function verifies whether the given mask is consistent
 *               with the 'u1CktFlag' i.e. it verifies whether a PDU can be
 *               scheduled for transmission with the given 'u1CktFlag' and the
 *               Circuit Mask corresponding to the given level
 * Input (s)   : pContext  - Pointer to the system context
 *             : u1CktFlag - Circuit flag indicating the circuits over which
 *                           a PDU is scheduled to be transmitted
 *               u1Level   - Level for which the Mask had to be verified
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, if verification is successfu;
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisUpdChkCktMask (tIsisSysContext * pContext, UINT1 u1CktFlag, UINT1 u1Level)
{
    UINT1               u1BitFlag = 0;
    UINT1               u1Byte = 0;
    UINT1              *pu1CktMask = NULL;
    UINT4               u4MaskLen = 0;
    UINT4               u4ByteCnt = 0;

    UPP_EE ((ISIS_LGST, "UPD <E> : Entered IsisUpdChkCktMask ()\n"));

    pu1CktMask = (u1Level == ISIS_LEVEL1) ? pContext->CktTable.pu1L1CktMask
        : pContext->CktTable.pu1L2CktMask;

    if (pu1CktMask == NULL)
    {
        /* No circuits are active and we cannot schedule anything
         */

        UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdChkCktMask ()\n"));
        return ISIS_FAILURE;
    }

    if (u1CktFlag != ISIS_ALL_BUT_SPEC_CKT)
    {
        /* It is either ISIS_ALL_CKTS or ISIS_SPECIFIED_CKT. Both the cases
         * require that atleast one circuit is ACTIVE. This routine is called
         * either when we receive LSPs from Peers or when LSP Generation timer
         * expires. In the former case it is clear that we should have active
         * circuits since without active circuits we cannot receive any PDUs. In
         * the later case Generation Timer will be active only if we have an
         * Active circuit and an Active Adjacency over the circuit. Hence we can
         * assume that at least one circuit is Active when this routine is
         * invoked. Return success
         */

        UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdChkCktMask ()\n"));
        return ISIS_SUCCESS;
    }

    /* This is the case of ISIS_ALL_BUT_SPEC_CKT. In this case we need to send
     * a PSU on all the circuits other than the specified one. Hence we must
     * atleast have 2 circuits Active. Verify whether there are 2 Active
     * circuits by comparing the Circuit Mask which will have bits set for each
     * Active circuit
     */

    ISIS_GET_4_BYTES (pu1CktMask, 0, u4MaskLen);

    ISIS_DBG_PRINT_ID ((pu1CktMask + 4), (UINT1) (u4MaskLen - 4),
                       "UPD <T> : Circuit Mask\t", ISIS_OCTET_STRING);

    /* First 4 bytes of Mask specifies the length of the mask
     */

    for (u4ByteCnt = 4; u4ByteCnt < u4MaskLen; u4ByteCnt++)
    {
        u1Byte = *(pu1CktMask + u4ByteCnt);
        if (u1Byte == 0)
        {
            /* No bits set, we can continue with the scan
             */

            continue;
        }
        if ((u1Byte & (u1Byte - 1)) != 0)
        {
            /* NOTE: (A & (A - 1)) will be zero if and only if A is a power of 
             * 2, i.e. A has only one bit set. If (A & (A -1)) is non-zero then
             * that means we have more than one bit set in A, which is required
             * condition for success. Return success
             */

            UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdChkCktMask ()\n"));
            return ISIS_SUCCESS;
        }
        else
        {
            /* (A & (A - 1) is '0'. We cannot stop here since there may be other
             * butes where circuit bits are set. Not down that you have already
             * found one set bit in the mask so that the next time you see one
             * more bityou can return success
             */

            if (u1BitFlag == ISIS_SET)
            {
                /* We have already seen one set bit and now we have found one
                 * more bit and hence we can return success
                 */

                UPP_EE ((ISIS_LGST,
                         "UPD <E> : Exiting IsisUpdChkCktMask ()\n"));
                return ISIS_SUCCESS;
            }
            u1BitFlag = ISIS_SET;
        }
    }

    /* Scanned through the entire mask and did not satisfy the required
     * conditions. Check Mask failed
     */

    UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdChkCktMask ()\n"));
    return ISIS_FAILURE;
}

/*******************************************************************************
 * Function    : IsisUpdIsTwoWayConnected ()
 * Description : This function Checks whether the given systems are 2-Way 
 *               connected i.e. System whose ID is included in pu1SrcSysId
 *               has LSPs LSPs which lists pu1DestSysId as one of its adjacency.
 *
 *               NOTE: pDestSysId is the MC node and it has already listed
 *               pu1SrcSysId as its adjacent. Here we are verifying the reverse
 * Input(s)    : pContext     - Pointer to the system context
 *               u1Level      - Level of the database
 *               pu1SrcSysId  - Pointer to the source system ID
 *               pu1DestSysId - Pointer to the destination system ID
 *               u1MtIndex    - Mt Index (will be used only if MUlti-topology is supported
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, if the given systems are two way connected
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdIsTwoWayConnected (tIsisSysContext * pContext, UINT1 u1Level,
                          UINT1 *pu1SrcSysId, UINT1 *pu1DestSysId,
                          UINT1 u1MtIndex)
{
    INT4                i4RetCode = ISIS_FAILURE;
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1               u1Flag = 0;
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    tIsisLSPEntry      *pLSPEntry = NULL;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisHashBucket    *pDbHashBkt = NULL;
    tIsisHashBucket    *pTxHashBkt = NULL;
    tIsisLSPTxEntry    *pLSPTxEntry = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdIsTwoWayConnected ()\n"));

    MEMSET (&au1LSPId, 0, sizeof (au1LSPId));

    /* Get the LSP Id from the LSP
     */

    MEMCPY (&au1LSPId, pu1SrcSysId, ISIS_SYS_ID_LEN + 1);

    /* We don't bother about the LSP Number here
     */

    au1LSPId[ISIS_SYS_ID_LEN + 1] = 0x00;

    /* We have to verify each of the LSPs generated by pu1SrcSysId. These LSPs
     * may either be in TxQ or Database at any time. Hence verify both TxQ and
     * Database
     */

    pLSPEntry = IsisUpdGetLSPFromDB (pContext, au1LSPId, u1Level, &pPrevDbRec,
                                     &pDbHashBkt, &u1Loc, &u1Flag);
    pLSPTxEntry =
        IsisUpdGetLSPFromTxQ (pContext, au1LSPId, u1Level, &pPrevTxRec,
                              &pTxHashBkt, &u1Loc, &u1Flag);

    while ((pLSPEntry != NULL)
           && (MEMCMP (pu1SrcSysId, (pLSPEntry->pu1LSP + ISIS_OFFSET_LSPID),
                       ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0)
    {
        /* Check whether pu1DestID is listed in one of the IS Adjacency TLVs
         */

        i4RetCode = IsisUpdCheckIfAdjacent (pContext, pLSPEntry->pu1LSP,
                                            pu1DestSysId, u1MtIndex);

        if (i4RetCode == ISIS_SUCCESS)
        {
            return (i4RetCode);
        }
        pLSPEntry = IsisUpdGetNextDbRec (pLSPEntry, &pDbHashBkt);
    }

    while ((pLSPTxEntry != NULL)
           && (MEMCMP (pu1SrcSysId,
                       (pLSPTxEntry->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                       ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0)
    {
        /* Check whether pu1DestID is listed in one of the IS Adjacency TLVs
         */

        i4RetCode = IsisUpdCheckIfAdjacent (pContext,
                                            pLSPTxEntry->pLSPRec->pu1LSP,
                                            pu1DestSysId, u1MtIndex);

        if (i4RetCode == ISIS_SUCCESS)
        {
            return (i4RetCode);
        }
        pLSPTxEntry = IsisUpdGetNextTxRec (pLSPTxEntry, &pTxHashBkt);
    }

    return (i4RetCode);
}

/******************************************************************************
 * Function    : IsisUpdCheckIfAdjacent ()
 * Description : This function Checks whether the given system identified by
 *               pu1SysID is listed in one of the IS Adjacency TLVs of pu1LSPBuf
 * Input(s)    : pContext     - pointer to the system context
 *               u1Level      - The Level of the database
 *               pu1SrcSysId  - pointer to the source system ID
 *               pu1DestSysId - pointer to the destination system ID
 *               u1MtIndex    - Mt Index (will be used only if MUlti-topology is supported
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, if the given System Id is listed in one of the IS
 *                             Adjacency TLVs in pu1LSPBuf
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisUpdCheckIfAdjacent (tIsisSysContext * pContext, UINT1 *pu1LSPBuf,
                        UINT1 *pu1SysId, UINT1 u1MtIndex)
{
    INT4                i4RetCode = ISIS_FAILURE;
    UINT1               u1Code = 0;
    UINT1               u1TlvLen = 0;
    INT4                i4Flag = ISIS_FALSE;
    UINT2               u2Offset = 0;
    UINT2               u2PduLen = 0;
    UINT1               u1IsPseudoNode = 0;
    UINT1               u1SubTLVLen = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdCheckIfAdjacent ()\n"));

    UNUSED_PARAM (pContext);
    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSPBuf, u2PduLen);

    /* TLVs start from an offset immediately after the header
     */

    u2Offset = ((tIsisComHdr *) (pu1LSPBuf))->u1HdrLen;
    u1IsPseudoNode = (UINT1) *(pu1LSPBuf + ISIS_OFFSET_PNODEID);

    while (u2Offset < u2PduLen)
    {
        /* Get the Code and Length and skip them
         */

        ISIS_GET_1_BYTE (pu1LSPBuf, u2Offset, u1Code);
        u2Offset++;
        ISIS_GET_1_BYTE (pu1LSPBuf, u2Offset, u1TlvLen);
        u2Offset++;

        switch (u1Code)
        {
            case ISIS_IS_ADJ_TLV:

                if (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC)
                {
                    u2Offset++;    /* Skip Virtual Flag */
                    u1TlvLen--;

                    while (u1TlvLen > 0)
                    {
                        i4Flag = MEMCMP ((pu1LSPBuf + u2Offset + 4),
                                         pu1SysId,
                                         ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                        if (i4Flag == 0)
                        {
                            /* Found the required System ID listed in the LSP
                             * buffer. Hence the Given system identified by the
                             * pu1SysId is adjacent to the system which generated
                             * the LSP
                             */

                            return (ISIS_SUCCESS);
                        }
                        u2Offset = (UINT2) (u2Offset + ISIS_IS_ADJ_TLV_LEN);
                        u1TlvLen = (UINT1) (u1TlvLen - ISIS_IS_ADJ_TLV_LEN);
                    }
                }
                else
                {
                    u2Offset = (UINT2) (u2Offset + u1TlvLen);
                }
                break;

            case ISIS_EXT_IS_REACH_TLV:

                if (((pContext->u1IsisMTSupport != ISIS_TRUE) &&
                     (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)) ||
                    ((pContext->u1IsisMTSupport == ISIS_TRUE)
                     && ((u1MtIndex == ISIS_MT0_INDEX)
                         || (u1IsPseudoNode != 0))))
                {
                    while (u1TlvLen > 0)
                    {
                        i4Flag = MEMCMP ((pu1LSPBuf + u2Offset),
                                         pu1SysId,
                                         ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                        if (i4Flag == 0)
                        {
                            /* Found the required System ID listed in the LSP
                             * buffer. Hence the Given system identified by the
                             * pu1SysId is adjacent to the system which generated
                             * the LSP
                             */

                            return (ISIS_SUCCESS);
                        }
                        u2Offset =
                            (UINT2) (u2Offset + ISIS_EXT_IS_REACH_TLV_LEN);
                        u1TlvLen =
                            (UINT1) (u1TlvLen - ISIS_EXT_IS_REACH_TLV_LEN);
                        u1SubTLVLen = *(pu1LSPBuf + u2Offset - 1);
                        if (u1SubTLVLen != 0)
                        {
                            /*Sub-TLV present - skip it */
                            u2Offset = u2Offset + u1SubTLVLen;
                            u1TlvLen = u1TlvLen - u1SubTLVLen;
                        }
                    }
                }
                else
                {
                    u2Offset = (UINT2) (u2Offset + u1TlvLen);
                }
                break;

            case ISIS_MT_IS_REACH_TLV:

                if ((pContext->u1IsisMTSupport == ISIS_TRUE)
                    && (u1MtIndex == ISIS_MT2_INDEX))
                {
                    u2Offset = (UINT2) (u2Offset + ISIS_MT_ID_LEN);
                    u1TlvLen = (UINT1) (u1TlvLen - ISIS_MT_ID_LEN);
                    while (u1TlvLen > 0)
                    {
                        i4Flag = MEMCMP ((pu1LSPBuf + u2Offset),
                                         pu1SysId,
                                         ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                        if (i4Flag == 0)
                        {
                            /* Found the required System ID listed in the LSP
                             * buffer. Hence the Given system identified by the
                             * pu1SysId is adjacent to the system which generated
                             * the LSP
                             */

                            return (ISIS_SUCCESS);
                        }
                        /*MT Id is decremented in the beginning itself - so using ISIS_EXT_IS_REACH_TLV_LEN 
                         * to move the offset*/
                        u2Offset =
                            (UINT2) (u2Offset + ISIS_EXT_IS_REACH_TLV_LEN);
                        u1TlvLen =
                            (UINT1) (u1TlvLen - ISIS_EXT_IS_REACH_TLV_LEN);
                        u1SubTLVLen = *(pu1LSPBuf + u2Offset - 1);
                        if (u1SubTLVLen != 0)
                        {
                            /*Sub-TLV present - skip it */
                            u2Offset = u2Offset + u1SubTLVLen;
                            u1TlvLen = u1TlvLen - u1SubTLVLen;
                        }
                    }
                }
                else
                {
                    u2Offset = (UINT2) (u2Offset + u1TlvLen);
                }
                break;

            default:

                u2Offset = (UINT2) (u2Offset + u1TlvLen);
                break;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdCheckIfAdjacent ()\n"));
    return i4RetCode;
}

/******************************************************************************
 * Function    : IsisUpdGetAdjsWithSysId ()
 * Description : This function retrieves all the adjacencies based on the given
 *               System Id. It scans the LSPs generated by the given system and
 *               retrieves the Adjacency information from the LSPs
 * Input(s)    : pContext     - Pointer to the system context
 *               pu1SysId     - Pointer to the System ID
 *               u1MetricType - Metric type 
 *               u1Level      - Level of the database
 *               u1ATTFlag    - Flag indicating whether the attached status of
 *                              the given system is required
 * Output(s)   : pAdjList     - Pointer to the List of valid adjacencies
 *                              announced by the given system
 *               pu1ATTSt     - Pointer to the Attached Status of the given
 *                              system
 * Globals     : Not Referred or Modified              
 * Returns     : ISIS_SUCCESS, If Adjacencies successfully retrieved
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdGetAdjsWithSysId (tIsisSysContext * pContext, UINT1 *pu1SysId,
                         UINT1 u1MetricType, UINT1 u1Level, UINT1 u1ATTFlag,
                         tIsisSPTNode ** pAdjList, UINT1 *pu1ATTSt,
                         UINT1 u1MtIndex)
{
    UINT1              *pu1LSPBuf = NULL;
    tIsisSPTNode       *pNode = NULL;
    tIsisLSPEntry      *pLSPEntry = NULL;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisLSPTxEntry    *pLSPTxEntry = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisHashBucket    *pDbHashBkt = NULL;
    tIsisHashBucket    *pTxHashBkt = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    INT4                i4TxFlag = ISIS_LSP_NOT_EQUAL;
    INT4                i4DbFlag = ISIS_LSP_NOT_EQUAL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2Offset = 0;
    UINT2               u2PduLen = 0;
    UINT2               u2MtId = 0;
    UINT1               u1MtSupport = ISIS_FALSE;
    UINT1               u1MtBitMap = 0;
    UINT1               u1Prefixlen = 0;
    UINT1               u1SubTlvLen = 0;
    UINT1               u1Code = 0;
    UINT1               u1VirFlag = 0;
    UINT1               u1TlvLen = 0;
    UINT1               u1Len = 0;
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1               u1DbFlag = ISIS_LSP_NOT_EQUAL;
    UINT1               u1TxFlag = ISIS_LSP_NOT_EQUAL;
    UINT1               u1LSPFlags = 0;
    UINT1               u1OLFlag = ISIS_NOT_SET;
    UINT1               u1Flag = ISIS_TRUE;
    UINT1               u1IfMtId = 0;
    UINT1               u1AdjMtId = 0;
    UINT1               u1TmpFlag = 0;
    BOOL1               bIsPseudoLSP = ISIS_FALSE;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdGetAdjsWithSysId ()\n"));

    /* By default we assume that the given system belongs to a different Area.
     * This flag is updated down the line, when the actual status is determined
     * based on the Area Addresses. 
     *
     * NOTE: The attached status is significant only for L12 and L2 systems
     * since they form the default exit points from the Area. Hence we use Area
     * Addresses to find whether the given system belongs to the same Area or
     * some other Area. While L1 Decision process executes, u1ATTFlag will be
     * set to ISIS_NOT_SET anyway and hence we do not update the Attached status
     * flag
     */

    *pu1ATTSt = ISIS_SAME_AREA;
    u1MtSupport = pContext->u1IsisMTSupport;
    u1MtBitMap =
        (u1MtIndex == ISIS_MT0_INDEX) ? ISIS_MT0_BITMAP : ISIS_MT2_BITMAP;

    /* Copy the Given System ID and place a zero for the LSP Number to get 
     * Zero LSP
     */

    MEMSET (&au1LSPId, 0, ISIS_LSPID_LEN);
    MEMCPY (&au1LSPId, pu1SysId, (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));

    if (au1LSPId[ISIS_SYS_ID_LEN] != 0)
    {
        bIsPseudoLSP = ISIS_TRUE;
    }

    /* NOTE: All the LSPs generated by a particular system will be placed
     *       together both in TxQ as well as Database. This is because the Hash
     *       tables are sorted based on LSPIDs which include SystemID,
     *       PseudonodeID and LSP Number. Hence the LSP chain for any particular
     *       system will start with Zero LSP followed by others. Hence if Zero
     *       LSP is retrieved, we can follow the chain and retrieve others too
     */

    ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) ISIS_LSPID_LEN,
                       "UPD <T> : Retrieving Adjs For System\t",
                       ISIS_OCTET_STRING);

    pLSPEntry = IsisUpdGetLSPFromDB (pContext, au1LSPId, u1Level, &pPrevDbRec,
                                     &pDbHashBkt, &u1Loc, &u1DbFlag);
    pLSPTxEntry =
        IsisUpdGetLSPFromTxQ (pContext, au1LSPId, u1Level, &pPrevTxRec,
                              &pTxHashBkt, &u1Loc, &u1TxFlag);

    i4DbFlag = (INT4) u1DbFlag;
    i4TxFlag = (INT4) u1TxFlag;
    ISIS_FORM_STR (ISIS_LSPID_LEN, pu1SysId, acLSPId);
    /* MT ID will be checked for Non-pseudo-node LSPs only if MT support is enabled */
    if ((u1MtSupport == ISIS_TRUE) && (au1LSPId[ISIS_SYS_ID_LEN] == 0))
    {
        if ((i4TxFlag == ISIS_LSP_EQUAL) && (pLSPTxEntry != NULL))
        {
            if (!(pLSPTxEntry->pLSPRec->u1MtId & u1MtBitMap))
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <E> : Skipping LSP [%s] [%s]  as it does not support MT [%d]\n",
                         acLSPId, ISIS_GET_LEVEL_STR (u1Level), u1MtIndex));
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdGetAdjsWithSysId ()\n"));
                return ISIS_FAILURE;
            }
        }
        else if (pLSPEntry != NULL)
        {
            if (!(pLSPEntry->u1MtId & u1MtBitMap))
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <E> : Skipping LSP [%s] [%s] as it does not support MT [%d]\n",
                         acLSPId, ISIS_GET_LEVEL_STR (u1Level), u1MtIndex));
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdGetAdjsWithSysId ()\n"));
                return ISIS_FAILURE;
            }
        }
    }

    if (i4DbFlag != ISIS_LSP_EQUAL)
    {
        /* LSP Zero for the given system is not found in the Database. Try it
         * out in Transmission Queue
         */

        if ((i4TxFlag == ISIS_LSP_EQUAL) && (pLSPTxEntry != NULL))    /*klocwork */
        {
            /* Check for System Status in the LSP Overload bit of LSP Zero. 
             * If System is Overloaded, load only ES Adjacencies (IPRAs in our 
             * case), otherwise load both ES (IPRAs) and IS Adjacencies 
             */
            if ((pContext->u1IsisMTSupport == ISIS_TRUE) && (u1MtIndex != 0))
            {
                /* For MT-TLVs Overload bit and Attach bit will be set in the first 2 bits of MT TLV. */
                if (IsisUtlGetNextTlvOffset
                    (pLSPTxEntry->pLSPRec->pu1LSP, ISIS_MT_TLV, &u2Offset,
                     &u1TlvLen) != ISIS_FAILURE)
                {
                    while (u1TlvLen)
                    {
                        MEMCPY (&u2MtId,
                                (pLSPTxEntry->pLSPRec->pu1LSP + u2Offset),
                                ISIS_MT_ID_LEN);
                        u2MtId = OSIX_HTONS (u2MtId);
                        u2MtId = (u2MtId & ISIS_MTID_MASK);

                        if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                        {
                            ISIS_GET_1_BYTE (pLSPTxEntry->pLSPRec->pu1LSP,
                                             u2Offset, u1LSPFlags);
                            u1OLFlag =
                                ((UINT1) (u1LSPFlags & ISIS_MT_LSP_OFLAG_MASK))
                                ? ISIS_SET : ISIS_NOT_SET;
                            u1LSPFlags = (u1LSPFlags & ISIS_MT_LSP_FLAG_MASK);
                            break;
                        }
                        u1TlvLen -= ISIS_MT_ID_LEN;
                        u2Offset += ISIS_MT_ID_LEN;
                    }
                }
                /* System type flag L1/L2 will be set in the LSp header */
                ISIS_GET_1_BYTE (pLSPTxEntry->pLSPRec->pu1LSP,
                                 (UINT2) (ISIS_OFFSET_CHKSUM + 2), u1TmpFlag);
                u1LSPFlags |= (UINT1) (u1TmpFlag & ISIS_ISTYPE_MASK);

            }
            else
            {
                ISIS_GET_1_BYTE (pLSPTxEntry->pLSPRec->pu1LSP,
                                 (UINT2) (ISIS_OFFSET_CHKSUM + 2), u1LSPFlags);
                u1OLFlag =
                    ((UINT1) (u1LSPFlags & ISIS_DBOL_MASK)) ? ISIS_SET :
                    ISIS_NOT_SET;
            }
        }
        else
        {
            /* LSP Zero is not present in Transmission Queue also, hence
             * ignore all the LSPs of this particular system
             */

            ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) ISIS_LSPID_LEN,
                               "UPD <T> : LSP Zero Not Found For\t",
                               ISIS_OCTET_STRING);
            UPP_PT ((ISIS_LGST,
                     "UPD <E> : Ignoring LSP [%s] [%s] MT [%d] in SPF as the Zeroth Fragment is not present\n",
                     acLSPId, ISIS_GET_LEVEL_STR (u1Level), u1MtIndex));
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdGetAdjsWithSysId ()\n"));
            return ISIS_FAILURE;
        }
    }
    else
    {
        /* Check for System Status in the LSP Overload bit of LSP Zero. 
         * If System is Overloaded, load only ES Adjacencies (IPRAs in our 
         * case), otherwise load both ES (IPRAs) and IS Adjacencies 
         */
        if (pLSPEntry != NULL)    /*klocwork */
        {
            if ((pContext->u1IsisMTSupport == TRUE) && (u1MtIndex != 0))
            {
                /* For MT-TLVs Overload bit and Attach bit will be set in the first 2 bits of MT TLV. */
                if (IsisUtlGetNextTlvOffset
                    (pLSPEntry->pu1LSP, ISIS_MT_TLV, &u2Offset,
                     &u1TlvLen) != ISIS_FAILURE)
                {
                    while (u1TlvLen)
                    {
                        MEMCPY (&u2MtId, (pLSPEntry->pu1LSP + u2Offset),
                                ISIS_MT_ID_LEN);
                        u2MtId = OSIX_HTONS (u2MtId);
                        u2MtId = (u2MtId & ISIS_MTID_MASK);

                        if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                        {
                            ISIS_GET_1_BYTE (pLSPEntry->pu1LSP,
                                             u2Offset, u1LSPFlags);
                            u1OLFlag =
                                ((UINT1) (u1LSPFlags & ISIS_MT_LSP_OFLAG_MASK))
                                ? ISIS_SET : ISIS_NOT_SET;
                            u1LSPFlags = (u1LSPFlags & ISIS_MT_LSP_FLAG_MASK);
                            break;
                        }
                        u2Offset += ISIS_MT_ID_LEN;
                        u1TlvLen -= ISIS_MT_ID_LEN;

                    }

                }
                /* System type flag L1/L2 will be set in the LSp header */
                ISIS_GET_1_BYTE (pLSPEntry->pu1LSP,
                                 (UINT2) (ISIS_OFFSET_CHKSUM + 2), u1TmpFlag);
                u1LSPFlags |= (UINT1) (u1TmpFlag & ISIS_ISTYPE_MASK);

            }
            else
            {
                ISIS_GET_1_BYTE (pLSPEntry->pu1LSP,
                                 (UINT2) (ISIS_OFFSET_CHKSUM + 2), u1LSPFlags);
                u1OLFlag =
                    ((UINT1) (u1LSPFlags & ISIS_DBOL_MASK)) ? ISIS_SET :
                    ISIS_NOT_SET;
            }
        }
        else
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <E> : Ignoring LSP [%s] [%s] MT {%d] in SPF as the Zeroth Fragment is not present\n",
                     acLSPId, ISIS_GET_LEVEL_STR (u1Level), u1MtIndex));
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdGetAdjsWithSysId ()\n"));
            return ISIS_FAILURE;
        }
    }
    if (u1OLFlag == ISIS_SET)
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <E> : LSP [%s] [%s] has the Overload bit set\n", acLSPId,
                 ISIS_GET_LEVEL_STR (u1Level)));
    }

    u2Offset = 0;
    u1TlvLen = 0;
    while ((i4DbFlag == 0) || (i4TxFlag == 0))
    {
        /* i4DbFlag will be Zero, if the Zero LSP was found in Database and
         * i4TxFlag will be Zero if it is found in TxQ. Inside this loop, these
         * flags will get updated based on the availability of other non-zero
         * LSPs and their locations
         */

        /* LOGIC: We will first add all the information from LSPs available in
         * the database. Then we proceed to add information from LSPs available
         * in the transmission queue. Once all the information is processed both
         * the flags will be set to '1'
         */

        pu1LSPBuf = ((i4DbFlag == 0) ? pLSPEntry->pu1LSP
                     : pLSPTxEntry->pLSPRec->pu1LSP);
        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSPBuf, u2PduLen);

        /* What ever information we are interested in, is available only
         * after the LSP header. Hence advance the Offset to point to variable
         * length fields
         */

        u2Offset = ((tIsisComHdr *) pu1LSPBuf)->u1HdrLen;

        while (u2Offset < u2PduLen)
        {
            /* Retrieve the Code and Length fields
             */

            ISIS_GET_1_BYTE (pu1LSPBuf, u2Offset, u1Code);
            u2Offset++;
            ISIS_GET_1_BYTE (pu1LSPBuf, u2Offset, u1Len);
            u2Offset++;

            switch (u1Code)
            {
                case ISIS_AREA_ADDR_TLV:

                    if ((u1ATTFlag == ISIS_SET) && (u1Level == ISIS_LEVEL2))
                    {
                        /* While running Level2 decision process, we will
                         * calculate the nearest attached L2 system. This will
                         * be required during the L1 decision process. Hence we
                         * will mark all L12 and L2 systems, which belong to 
                         * other Areas and are attached, in the list of SPT
                         * nodes we return.
                         */

                        if (u1Flag == ISIS_TRUE)
                        {
                            /* If *pu1ATTSt had been marked as ISIS_SAME_AREA,
                             * then do not worry about comparing Area
                             * Addresses. This is because Area Address TLV may
                             * appear more than once in the Zero LSP, and some
                             * of the area addresses in one of the TLVs may
                             * match the local systems area addresses while the
                             * others may not. Even if one of the TLVs addresses
                             * match, the given system will be marked as
                             * belonging to the SAME AREA. We may have to
                             * continue checking Area Addresses till all the
                             * TLVs are exhausted and only then consider the
                             * given system as belonging to OTHER AREA
                             *
                             * NOTE: We cannot mark a system as belonging to
                             * OTHER AREA based on System Type, since several  
                             * L2 systems may belong to the same area. So the
                             * fool proof way of saying two systems belong to
                             * different areas is by comparing their Area
                             * addresses
                             */

                            *pu1ATTSt =
                                IsisUpdCompAAFromBuf (pContext,
                                                      (pu1LSPBuf + u2Offset),
                                                      u1Len);
                            if (*pu1ATTSt == ISIS_OTHER_AREA)
                            {
                                u1Flag = ISIS_TRUE;
                            }
                            else
                            {
                                u1Flag = ISIS_FALSE;
                            }
                        }
                    }
                    u2Offset = (UINT2) (u2Offset + u1Len);
                    break;

                case ISIS_PROT_SUPPORT_TLV:
                case ISIS_AUTH_INFO_TLV:
                case ISIS_IDRP_INFO_TLV:

                    /* Skip the TLV. we don't need these anyway
                     */

                    u2Offset = (UINT2) (u2Offset + u1Len);
                    break;

                case ISIS_IS_ADJ_TLV:
                    if ((u1OLFlag == ISIS_SET)
                        || (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
                    {
                        /* If a system is OverLoaded, the IS adjacencies will
                         * not be considered
                         */

                        u2Offset = (UINT2) (u2Offset + u1Len);
                        break;
                    }
                    else
                    {
                        ISIS_GET_1_BYTE (pu1LSPBuf, u2Offset, u1VirFlag);
                        UNUSED_PARAM (u1VirFlag);
                        u2Offset++;    /* Skip Virtual Flag */
                        u1TlvLen = (UINT1) (u1Len - 1);

                        while (u1TlvLen > 0)
                        {
                            pNode = NULL;
                            i4RetVal =
                                IsisUpdAddNbrInfo (pContext,
                                                   (pu1LSPBuf + u2Offset),
                                                   u1MetricType, u1LSPFlags,
                                                   ISIS_OSI_SYSTEM, &pNode,
                                                   u1Code, u1Level,
                                                   bIsPseudoLSP);

                            if (i4RetVal == ISIS_FAILURE)
                            {
                                /* Unable to add IS neighbours. Free the 
                                 * pAdjList which has adjacencies collected
                                 * till this point
                                 */

                                while (*pAdjList != NULL)
                                {
                                    pNode = *pAdjList;
                                    *pAdjList = (*pAdjList)->pNext;
                                    ISIS_MEM_FREE (ISIS_BUF_SPTN,
                                                   (UINT1 *) pNode);
                                }
                                break;
                            }

                            /* Insert the SPT Node into the Adj list
                             */

                            if (pNode != NULL)
                            {
                                if (*pAdjList != NULL)
                                {
                                    pNode->pNext = *pAdjList;
                                    *pAdjList = pNode;
                                }
                                else
                                {
                                    /* First node being inserted
                                     */

                                    *pAdjList = pNode;
                                }
                            }
                            u2Offset = (UINT2) (u2Offset + ISIS_IS_ADJ_TLV_LEN);
                            u1TlvLen = (UINT1) (u1TlvLen - ISIS_IS_ADJ_TLV_LEN);
                        }
                        break;
                    }

                case ISIS_IPV6_RA_TLV:
                case ISIS_IP_INTERNAL_RA_TLV:
                case ISIS_IP_EXTERNAL_RA_TLV:

                    if ((pContext->u1IsisMTSupport == ISIS_TRUE)
                        || ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                            && (u1Code != ISIS_IPV6_RA_TLV)))
                    {
                        u2Offset = (UINT2) (u2Offset + u1Len);
                    }
                    else
                    {
                        u1TlvLen = u1Len;
                        while (u1TlvLen > 0)
                        {
                            if (u1Code == ISIS_IPV6_RA_TLV)
                            {
                                u1Prefixlen = *(pu1LSPBuf + u2Offset + 5);
                                u1Prefixlen = (UINT1) ISIS_ROUNDOFF_PREFIX_LEN (u1Prefixlen);    /*diab */
                            }
                            pNode = NULL;
                            i4RetVal = IsisUpdAddNbrInfo (pContext,
                                                          (pu1LSPBuf +
                                                           u2Offset),
                                                          u1MetricType,
                                                          u1LSPFlags,
                                                          ISIS_IP_END_SYSTEM,
                                                          &pNode, u1Code,
                                                          u1Level, ISIS_FALSE);

                            if (i4RetVal == ISIS_FAILURE)
                            {
                                UPP_PT ((ISIS_LGST,
                                         "UPD <T> : Unable To Add IPRAs - MT [%u], Level [%u]\n",
                                         u1MtIndex, u1Level));
                                /* Unable to add IS neighbours. Free the pAdjList 
                                 * which has adjacencies collected till this point
                                 */

                                while (*pAdjList != NULL)
                                {
                                    pNode = *pAdjList;
                                    *pAdjList = (*pAdjList)->pNext;
                                    ISIS_MEM_FREE (ISIS_BUF_SPTN,
                                                   (UINT1 *) pNode);
                                }
                                break;
                            }

                            /* Insert the SPT Node into the Adj list
                             */

                            if (pNode != NULL)
                            {
                                if (*pAdjList != NULL)
                                {
                                    pNode->pNext = (*pAdjList)->pNext;
                                    (*pAdjList)->pNext = pNode;
                                }
                                else
                                {
                                    /* First node into the list */

                                    *pAdjList = pNode;
                                }
                            }
                            if (u1Code == ISIS_IPV6_RA_TLV)
                            {
                                /* As per Cisco one full byte in the control octet
                                 * is considered as the prefix length*/
                                /* Read the Prefix length and increment the Offset by
                                 *  the length and decrement the TLV len by 6 +prefix len*/
                                u2Offset =
                                    (UINT2) (u2Offset + (u1Prefixlen + 6));
                                u1TlvLen =
                                    (UINT1) (u1TlvLen - (u1Prefixlen + 6));
                            }
                            else
                            {

                                u2Offset =
                                    (UINT2) (u2Offset + ISIS_IPRA_TLV_LEN);
                                u1TlvLen =
                                    (UINT1) (u1TlvLen - ISIS_IPRA_TLV_LEN);
                            }
                        }
                    }
                    break;
                 /*MISIS*/ case ISIS_EXT_IS_REACH_TLV:    /*similar to ISIS_IS_ADJ_TLV */
                case ISIS_MT_IS_REACH_TLV:    /*similar to ISIS_IS_ADJ_TLV */

                    if (((u1OLFlag == ISIS_SET)
                         || ((pContext->u1IsisMTSupport == ISIS_FALSE)
                             && (u1Code == ISIS_MT_IS_REACH_TLV))
                         || (pContext->u1MetricStyle ==
                             ISIS_STYLE_NARROW_METRIC))
                        || ((u1MtIndex == ISIS_MT0_INDEX)
                            && (u1Code != ISIS_EXT_IS_REACH_TLV))
                        || ((u1MtIndex == ISIS_MT2_INDEX)
                            && (u1Code != ISIS_MT_IS_REACH_TLV)
                            && (au1LSPId[ISIS_SYS_ID_LEN] == 0)))
                    {
                        /* If a system is OverLoaded/MT not enabled, the IS_EXT_IS/MT_IS TLVs will
                         * not be considered
                         */
                        u2Offset = (UINT2) (u2Offset + u1Len);
                        break;
                    }
                    else
                    {
                        u1TlvLen = u1Len;
                        if (u1Code == ISIS_MT_IS_REACH_TLV)
                        {
                            u2Offset = (UINT2) (u2Offset + ISIS_MT_ID_LEN);    /*skipping MT TLV */
                            u1TlvLen = (UINT1) (u1TlvLen - ISIS_MT_ID_LEN);
                        }
                        while (u1TlvLen > 0)
                        {
                            i4RetVal =
                                IsisUpdAddNbrInfoFromMTEx (pContext,
                                                           (pu1LSPBuf +
                                                            u2Offset),
                                                           u1LSPFlags, &pNode,
                                                           u1Code, u1Level,
                                                           &u1SubTlvLen,
                                                           u1MtIndex,
                                                           bIsPseudoLSP);

                            if (i4RetVal == ISIS_FAILURE)
                            {
                                /* Unable to add IS neighbours. Free the
                                 * pAdjList which has adjacencies collected
                                 * till this point
                                 */

                                while (*pAdjList != NULL)
                                {
                                    pNode = *pAdjList;
                                    *pAdjList = (*pAdjList)->pNext;
                                    ISIS_MEM_FREE (ISIS_BUF_SPTN,
                                                   (UINT1 *) pNode);
                                }
                                break;
                            }

                            /* Insert the SPT Node into the Adj list
                             */

                            if (pNode != NULL)
                            {
                                if (*pAdjList != NULL)
                                {
                                    pNode->pNext = *pAdjList;
                                    *pAdjList = pNode;
                                }
                                else
                                {
                                    /* First node being inserted
                                     */

                                    *pAdjList = pNode;
                                }
                            }
                            /* For both MT-IS and EXt-IS, ISIS_EXT_IS_REACH_TLV_LEN is decremented.
                             * since ISIS_MT_ID_LEN is decremented at the beginning for MT-IS 
                             * ISIS_MT_IS_REACH_TLV_LEN = (ISIS_MT_ID_LEN + ISIS_EXT_IS_REACH_TLV_LEN)*/
                            u2Offset =
                                (UINT2) (u2Offset +
                                         (ISIS_EXT_IS_REACH_TLV_LEN +
                                          u1SubTlvLen));
                            u1TlvLen =
                                (UINT1) (u1TlvLen -
                                         (ISIS_EXT_IS_REACH_TLV_LEN +
                                          u1SubTlvLen));
                        }
                        break;
                    }

                case ISIS_EXT_IP_REACH_TLV:    /*similar to ISIS_IP_INTERNAL_RA_TLV */
                case ISIS_MT_IPV6_REACH_TLV:    /*similar to ISIS_IPV6_RA_TLV */

                    if (((pContext->u1IsisMTSupport == ISIS_FALSE)
                         &&
                         ((pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC)
                          || (u1Code != ISIS_EXT_IP_REACH_TLV)))
                        || ((u1MtIndex == ISIS_MT0_INDEX)
                            && (u1Code != ISIS_EXT_IP_REACH_TLV))
                        || ((u1MtIndex == ISIS_MT2_INDEX)
                            && (u1Code != ISIS_MT_IPV6_REACH_TLV)))
                    {
                        u2Offset = (UINT2) (u2Offset + u1Len);
                        break;
                    }
                    else
                    {
                        u1TlvLen = u1Len;
                        if (u1Code == ISIS_MT_IPV6_REACH_TLV)
                        {
                            u2Offset = (UINT2) (u2Offset + ISIS_MT_ID_LEN);    /*skipping MT TLV */
                            u1TlvLen = (UINT1) (u1TlvLen - ISIS_MT_ID_LEN);
                        }
                        while (u1TlvLen > 0)
                        {
                            if (u1Code == ISIS_MT_IPV6_REACH_TLV)
                            {
                                u1Prefixlen =
                                    *(pu1LSPBuf + u2Offset +
                                      ISIS_EXT_DEF_METRIC_LEN + 1);
                            }
                            else
                            {
                                u1Prefixlen =
                                    (UINT1) (*
                                             (pu1LSPBuf + u2Offset +
                                              ISIS_EXT_DEF_METRIC_LEN) &
                                             ISIS_PREFIX_MASK);
                            }
                            u1Prefixlen = (UINT1) ISIS_ROUNDOFF_PREFIX_LEN (u1Prefixlen);    /*diab */
                            i4RetVal =
                                IsisUpdAddNbrInfoFromMTEx (pContext,
                                                           (pu1LSPBuf +
                                                            u2Offset),
                                                           u1LSPFlags, &pNode,
                                                           u1Code, u1Level,
                                                           &u1SubTlvLen,
                                                           u1MtIndex,
                                                           ISIS_FALSE);

                            if (i4RetVal == ISIS_FAILURE)
                            {
                                UPP_PT ((ISIS_LGST,
                                         "UPD <T> : Unable To Add IPRAs - Level [%u]\n",
                                         u1Level));
                                /* Unable to add IS neighbours. Free the pAdjList
                                 * which has adjacencies collected till this point
                                 */

                                while (*pAdjList != NULL)
                                {
                                    pNode = *pAdjList;
                                    *pAdjList = (*pAdjList)->pNext;
                                    ISIS_MEM_FREE (ISIS_BUF_SPTN,
                                                   (UINT1 *) pNode);
                                }
                                break;
                            }
                            /* Insert the SPT Node into the Adj list
                             */

                            if (pNode != NULL)
                            {
                                if (*pAdjList != NULL)
                                {
                                    pNode->pNext = (*pAdjList)->pNext;
                                    (*pAdjList)->pNext = pNode;
                                }
                                else
                                {
                                    /* First node into the list */

                                    *pAdjList = pNode;
                                }
                            }
                            if (u1Code == ISIS_MT_IPV6_REACH_TLV)
                            {
                                u2Offset = (UINT2) (u2Offset + (u1Prefixlen +
                                                                ISIS_COM_MT_IPV6_REACH_TLV_LEN
                                                                + u1SubTlvLen));
                                u1TlvLen =
                                    (UINT1) (u1TlvLen -
                                             (u1Prefixlen +
                                              ISIS_COM_MT_IPV6_REACH_TLV_LEN +
                                              u1SubTlvLen));
                            }
                            else
                            {
                                u2Offset = (UINT2) (u2Offset + (u1Prefixlen +
                                                                ISIS_COM_EXTN_IP_REACH_TLV_LEN
                                                                + u1SubTlvLen));
                                u1TlvLen =
                                    (UINT1) (u1TlvLen -
                                             (u1Prefixlen +
                                              ISIS_COM_EXTN_IP_REACH_TLV_LEN +
                                              u1SubTlvLen));
                            }
                        }
                    }
                    break;

                default:        /* Skip TLV with unrecognised codes */
                    u2Offset = (UINT2) (u2Offset + u1Len);
                    break;
            }

            if (i4RetVal == ISIS_FAILURE)
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : LSP Parsing Failed, while processing [%s] in LSP [%s] [%s] MT [%d]\n",
                         ISIS_GET_TLV_TYPE_STR (u1Code), acLSPId,
                         ISIS_GET_LEVEL_STR (u1Level), u1MtIndex));
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdGetAdjsWithSysId ()\n"));
                return i4RetVal;
            }
        }

        if (i4DbFlag == 0)
        {
            /* We can still take nodes from the Database. Continue doing that
             */

            pPrevDbRec = pLSPEntry;
            pLSPEntry = IsisUpdGetNextDbRec (pLSPEntry, &pDbHashBkt);
        }
        else
        {
            /* We can still take nodes from the TxQ. Continue doing that
             */

            pPrevTxRec = pLSPTxEntry;
            pLSPTxEntry = IsisUpdGetNextTxRec (pLSPTxEntry, &pTxHashBkt);

        }

        if (pLSPEntry != NULL)
        {
            /* We have been going through the LSP chain starting from LSP Zero.
             * We have to stop once we reach an LSP whose system Id is different
             * from the given systems System ID
             */

            i4DbFlag = (MEMCMP (pu1SysId,
                                (pLSPEntry->pu1LSP + ISIS_OFFSET_LSPID),
                                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));
        }
        else
        {
            /* We have exhausted all LSPs of the given system in the Database.
             * Set i4DbFlag to '1' since we need not look into the Database
             * anymore
             */

            i4DbFlag = 1;
        }

        if (pLSPTxEntry != NULL)
        {
            /* We have been going through the LSP chain starting from LSP Zero.
             * We have to stop once we reach an LSP whose system Id is different
             * from the given systems System ID
             */

            i4TxFlag = (MEMCMP (pu1SysId, (pLSPTxEntry->pLSPRec->pu1LSP +
                                           ISIS_OFFSET_LSPID),
                                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));
        }
        else
        {
            /* We have exhausted all LSPs of the given system in the TxQ.
             * Set i4DbFlag to '1' since we need not look into the TxQ
             * anymore
             */

            i4TxFlag = 1;
        }
    }

    /* When Multi-topology is enabled, setting attach bit should be decided depends 
     * on the MT supported by the neigbor L2 system*/
    if ((pContext->u1IsisMTSupport == ISIS_TRUE)
        && (*pu1ATTSt == ISIS_OTHER_AREA))
    {
        *pu1ATTSt = ISIS_SAME_AREA;
        pDirEntry = pContext->AdjDirTable.pDirEntry;

        while ((pDirEntry != NULL) && (pDirEntry->pAdjEntry != NULL))
        {
            pAdjEntry = pDirEntry->pAdjEntry;

            u1IfMtId = pAdjEntry->pCktRec->u1IfMTId;
            u1AdjMtId = pAdjEntry->u1AdjMTId;

            if ((pAdjEntry->u1AdjUsage == ISIS_LEVEL2)
                && (u1IfMtId & u1MtBitMap) && (u1AdjMtId & u1MtBitMap))
            {
                *pu1ATTSt = ISIS_OTHER_AREA;
                break;
            }
            pDirEntry = pDirEntry->pNext;
        }
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetAdjsWithSysId ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function     : IsisUpdConstructSelfLSP ()
 * Description  : This function scans through the TLV chain maintained in the
 *                SelfLSP information database and constructs the SelfLSP
 *                buffer. 
 * Input (s)    : pContext  - Pointer to the system context
 *              : pLSP      - Pointer to the Self LSP which has the configured
 *                            information in the form of TLVs
 *              : u1Level   - Level of the LSP
 * Output (s)   : pu2Offset - Final offset after the buffer is built (Total 
 *                            Length of the PDU)
 * Globals      : Not Referred or Modified
 * Returns      : Pointer to the constructed LSP buffer
 ******************************************************************************/

PUBLIC UINT1       *
IsisUpdConstructSelfLSP (tIsisSysContext * pContext, tIsisLSPInfo * pLSP,
                         UINT1 u1Level, UINT2 *pu2Offset)
{
    UINT1              *pu1LSP = NULL;
    tIsisTLV           *pTLV = NULL;
    tIsisIPRATLV       *pIPRATLV = NULL;
    tIsisMDT           *pMDT = NULL;
    UINT4               u4Count = 0;
    UINT2               u2MtId = 0;
    UINT2               u2PrevMtId = 0;
    UINT1               u1PrevCode = 0;
    UINT1               u1VirtFlag = ISIS_TRUE;
    UINT1               u1Len = 0;
    UINT1               u1TlvLen = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdConstructSelfLSP ()\n"));

    pu1LSP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI,
                                       (pLSP->u2LSPLen +
                                        ISIS_LSP_HDR_LEN +
                                        ISIS_MAX_PASSWORD_LEN + 3));
    if (pu1LSP == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Building SelfLSP\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_LSPI);
        IsisUpdEnterWaitingState (pContext, u1Level);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdConstructSelfLSP ()\n"));
        return NULL;
    }

    if (u1Level == ISIS_LEVEL1)
    {
        IsisUtlFillComHdr (pContext, pu1LSP, (UINT1) ISIS_L1LSP_PDU);
    }
    else
    {
        IsisUtlFillComHdr (pContext, pu1LSP, (UINT1) ISIS_L2LSP_PDU);
    }

    *pu2Offset = sizeof (tIsisComHdr);

    IsisUpdFillLSPHdr (pContext, pLSP, u1Level, pu2Offset, pu1LSP);

    pTLV = pLSP->pTLV;            /* Pointer to the head of th TLV chain */
    u1PrevCode = 0;

    /* We keep scanning the TLV chain and collect all the nodes with the same
     * code. We try to accomodate all the information with the same code into a
     * single TLV. If the information exceeds maximum TLV length, we will add a
     * new TLV and keep updating. We will continue doing this till we exhaust
     * the complete chain
     *
     * NOTE: All information with the same CODE will appear together in the TLV
     * chain in the SelfLSPs
     */

    u2MtId = 0;

    while (pTLV != NULL)
    {
        if (pTLV->u1Code == ISIS_MT_IS_REACH_TLV)
        {
            MEMCPY (&u2MtId, pTLV->au1Value, ISIS_MT_ID_LEN);
            u2MtId = OSIX_HTONS (u2MtId);
        }

        if ((u1PrevCode != pTLV->u1Code) || (u2PrevMtId != u2MtId)
            || ((u1Len + pTLV->u1Len) > 255))
        {
            /* We have to add a new TLV if there is a change in CODE or if the
             * information is exceding the maximum length of the TLV.
             */

            if ((u1PrevCode != pTLV->u1Code)
                || ((pTLV->u1Code == ISIS_IS_ADJ_TLV)
                    && ((u1Len + pTLV->u1Len) > 255)))
            {
                /* IS Adjacency TLVs will include a Virtual Flag in every TLV.
                 * Since we are now about to start with a new TLV, we can note
                 * down that we need to add a Virtual Flag
                 */

                u1VirtFlag = ISIS_TRUE;
            }

            if ((u1PrevCode != 0) || (u2PrevMtId != 0))
            {
                /* Previous Code will be Zero only when the first TLV is being
                 * added. In that case we cannot fill the TLV length, since we
                 * will fill the length of the TLV only after the TLV is
                 * completely filled
                 */

                *(UINT1 *) (pu1LSP + (*pu2Offset) - u1Len - 1) = u1Len;
            }

            /* Reset the TLV length, since we are starting with a new TLV
             */

            u1Len = 0;
            *(pu1LSP + *pu2Offset) = pTLV->u1Code;

            /* Note down the previous code so that we can group all information
             * with the same code into a single TLV
             */

            u1PrevCode = pTLV->u1Code;
            *pu2Offset = (UINT2) ((*pu2Offset) + 2);    /* Incrementing for Code and ignoring length for 
                                                         * now, since length is filled once the TLV is 
                                                         * filled completely
                                                         */

            if (pTLV->u1Code == ISIS_IS_ADJ_TLV)
            {
                /* For IS Adjacency TLVs, add Virtual Flag. Since Partition 
                 * repair is not supported the Flag is set to 0 always
                 */

                if (u1VirtFlag == ISIS_TRUE)
                {
                    *(pu1LSP + *pu2Offset) = 0;
                    (*pu2Offset)++;
                    u1Len++;
                    u1VirtFlag = ISIS_FALSE;
                }
            }
            if (pTLV->u1Code == ISIS_MT_IS_REACH_TLV)
            {
                MEMCPY ((pu1LSP + *pu2Offset), pTLV->au1Value, ISIS_MT_ID_LEN);
                (*pu2Offset) = (UINT2) ((*pu2Offset) + ISIS_MT_ID_LEN);
                u1Len = (UINT1) (u1Len + ISIS_MT_ID_LEN);
                MEMCPY (&u2PrevMtId, pTLV->au1Value, ISIS_MT_ID_LEN);
                u2PrevMtId = OSIX_HTONS (u2PrevMtId);
            }
        }
        if (pTLV->u1Len >= ISIS_TLV_LEN)
        {
            u1Len = (UINT1) (u1Len + pTLV->u1Len);
            continue;
        }
        /* Fill the Variable Length Values
         */

        u1TlvLen = pTLV->u1Len;
        if (pTLV->u1Code == ISIS_MT_IS_REACH_TLV)
        {
            u1TlvLen = (UINT1) (u1TlvLen - ISIS_MT_ID_LEN);
            MEMCPY ((pu1LSP + *pu2Offset), &(pTLV->au1Value[2]), u1TlvLen);
        }
        else
        {
            MEMCPY ((pu1LSP + *pu2Offset), pTLV->au1Value, u1TlvLen);

            /* Add the MT-LSPFlags in the MT ID, for MT IDs other than MT ID 0 */
            if (pTLV->u1Code == ISIS_MT_TLV)
            {
                MEMCPY (&u2MtId, pTLV->au1Value, ISIS_MT_ID_LEN);
                u2MtId = OSIX_HTONS (u2MtId);

                if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                {
                    if (u1Level == ISIS_LEVEL1)
                    {
                        *(pu1LSP + *pu2Offset) = ((*(pu1LSP + *pu2Offset)) |
                                                  pContext->SelfLSP.
                                                  au1L1MTLspFlag
                                                  [ISIS_MT2_INDEX]);
                    }
                    else
                    {
                        *(pu1LSP + *pu2Offset) = ((*(pu1LSP + *pu2Offset)) |
                                                  pContext->SelfLSP.
                                                  au1L2MTLspFlag
                                                  [ISIS_MT2_INDEX]);
                    }
                }
            }
        }
        u1Len = (UINT1) (u1Len + u1TlvLen);
        *pu2Offset = (UINT2) (*pu2Offset + u1TlvLen);
        pTLV = pTLV->pNext;
    }

    /* Filling the last TLVs length
     */

    *(UINT1 *) (pu1LSP + *pu2Offset - u1Len - 1) = u1Len;

    u1PrevCode = 0;
    u1Len = 0;
    u2MtId = 0;
    u2PrevMtId = 0;
    /* Fetch IPRA TLV's from RBTree and add to the LSP */
    pIPRATLV = (tIsisIPRATLV *) RBTreeGetFirst (pLSP->IPRATLV);

    while (pIPRATLV != NULL)
    {
        if ((pContext->u1IsisMTSupport == ISIS_TRUE)
            && ((pIPRATLV->u1Code == ISIS_IP_INTERNAL_RA_TLV)
                || (pIPRATLV->u1Code == ISIS_IP_EXTERNAL_RA_TLV)
                || (pIPRATLV->u1Code == ISIS_IPV6_RA_TLV)))
        {
            /*Generating pMDT to delete the unwanted TLV */
            pMDT =
                (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

            if (pMDT == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdProcISUpEvt ()\n"));
                pIPRATLV =
                    (tIsisIPRATLV *) RBTreeGetNext (pLSP->IPRATLV,
                                                    (tRBElem *) pIPRATLV, NULL);
                continue;
            }
            IsisUpdFillMDTFromIPRATLV (pContext, pMDT, pIPRATLV);
            pIPRATLV = (tIsisIPRATLV *) RBTreeGetNext
                (pLSP->IPRATLV, (tRBElem *) pIPRATLV, NULL);
            pMDT->u1Cmd = ISIS_CMD_DELETE;
            pMDT->u1LSPType =
                (u1Level ==
                 ISIS_LEVEL1) ? ISIS_L1_NON_PSEUDO_LSP : ISIS_L2_NON_PSEUDO_LSP;
            IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
            continue;
        }
        if (pIPRATLV->u1Code == ISIS_MT_IPV6_REACH_TLV)
        {
            MEMCPY (&u2MtId, pIPRATLV->au1Value, ISIS_MT_ID_LEN);
            u2MtId = OSIX_HTONS (u2MtId);
        }

        if ((u1PrevCode != pIPRATLV->u1Code) || (u2PrevMtId != u2MtId)
            || ((u1Len + pIPRATLV->u1Len) > 255))
        {
            /* We have to add a new TLV if there is a change in CODE or if the
             * information is exceding the maximum length of the TLV.
             */
            if ((u1PrevCode != 0) || (u2PrevMtId != 0))
            {
                /* Previous Code will be Zero only when the first TLV is being
                 * added. In that case we cannot fill the TLV length, since we
                 * will fill the length of the TLV only after the TLV is
                 * completely filled
                 */

                *(UINT1 *) (pu1LSP + *pu2Offset - u1Len - 1) = u1Len;
            }

            /* Reset the TLV length, since we are starting with a new TLV
             */

            u1Len = 0;
            *(pu1LSP + *pu2Offset) = pIPRATLV->u1Code;

            /* Note down the previous code so that we can group all information
             * with the same code into a single TLV
             */

            u1PrevCode = pIPRATLV->u1Code;
            *pu2Offset = (UINT2) (*pu2Offset + 2);    /* Incrementing for Code and ignoring length for 
                                                     * now, since length is filled once the TLV is 
                                                     * filled completely
                                                     */
            if (pIPRATLV->u1Code == ISIS_MT_IPV6_REACH_TLV)
            {
                MEMCPY ((pu1LSP + *pu2Offset), pIPRATLV->au1Value,
                        ISIS_MT_ID_LEN);
                (*pu2Offset) = (UINT2) ((*pu2Offset) + ISIS_MT_ID_LEN);
                u1Len = (UINT1) (u1Len + ISIS_MT_ID_LEN);
                MEMCPY (&u2PrevMtId, pIPRATLV->au1Value, ISIS_MT_ID_LEN);
                u2PrevMtId = OSIX_HTONS (u2PrevMtId);
            }
        }
        /* Fill the Variable Length Values
         */

        u1TlvLen = pIPRATLV->u1Len;

        if (pIPRATLV->u1Code == ISIS_MT_IPV6_REACH_TLV)
        {
            u1TlvLen = (UINT1) (u1TlvLen - ISIS_MT_ID_LEN);
            MEMCPY ((pu1LSP + *pu2Offset), &(pIPRATLV->au1Value[2]), u1TlvLen);
        }
        else
        {
            MEMCPY ((pu1LSP + *pu2Offset), pIPRATLV->au1Value, u1TlvLen);
        }

        u1Len = (UINT1) (u1Len + u1TlvLen);
        *pu2Offset = (UINT2) (*pu2Offset + u1TlvLen);
        pIPRATLV = (tIsisIPRATLV *) RBTreeGetNext
            (pLSP->IPRATLV, (tRBElem *) pIPRATLV, NULL);
    }

    RBTreeCount (pLSP->IPRATLV, &u4Count);
    if (u4Count != 0)
    {
        /* Filling the last TLVs length
         */
        *(UINT1 *) (pu1LSP + *pu2Offset - u1Len - 1) = u1Len;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdConstructSelfLSP ()\n"));
    return (pu1LSP);
}

/******************************************************************************
 * Function    : IsisUpdProcMSNExceeded () 
 * Description : This function is called when an attempt is made to exceed the
 *               Max Sequence number. The routine posts an event to control 
 *               queue and disables the local IS for 
 *               MAX AGE + ZERO AGE LIFE TIME by starting the Sequence Number 
 *               Wrap Around Timer
 * Input (s)   : pContext - Pointer to the system context
 *             : u1Level  - The Level where the sequence number exceed happen 
 * Output (s)  : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcMSNExceeded (tIsisSysContext * pContext, UINT1 u1Level,
                        UINT1 *pu1LSP)
{
    tIsisEvtSeqNoExceed *pSeqnoExcdEvt;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcMSNExceeded ()\n"));

    ISIS_INCR_MAX_SEQ_EXCEED (pContext, u1Level);

    pSeqnoExcdEvt = (tIsisEvtSeqNoExceed *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSeqNoExceed));
    if (pSeqnoExcdEvt != NULL)
    {
        pSeqnoExcdEvt->u1EvtID = ISIS_EVT_MAX_SEQNO_EXCEED;
        pSeqnoExcdEvt->u1Level = u1Level;
        MEMCPY (pSeqnoExcdEvt->au1TrapLspID, pu1LSP, ISIS_LSPID_LEN);

        IsisUtlSendEvent (pContext, (UINT1 *) pSeqnoExcdEvt,
                          sizeof (tIsisEvtSeqNoExceed));
    }

    /* Now that we have disabled the local IS for the time being, we can reset
     * the Sequence Numbers to their initial values and start generating LSPs
     * with these new values
     */

    IsisUpdResetSeqNums (pContext);

    pContext->SysTimers.SysISSeqWrapTimer.pContext = pContext;
    IsisTmrStartTimer (&(pContext->SysTimers.SysISSeqWrapTimer),
                       ISIS_SEQ_REINT_TIMER,
                       ISIS_LSP_MAXAGE + ISIS_ZERO_AGE_INT);

    /* Restart the generation timers for ISIS_LSP_MAXAGE + ISIS_ZERO_AGE_INT
     * duration so that the generation timers will not fire as long as the
     * Sequence Wrap Around Timer is active
     */

    if ((pContext->SysActuals.u1SysType != ISIS_LEVEL2)
        && (ISIS_IS_TIMER_ACTIVE (pContext->SysL1Info.SysLSPGenTimer)
            == ISIS_TRUE))
    {
        IsisTmrRestartTimer (&(pContext->SysL1Info.SysLSPGenTimer),
                             ISIS_L1LSP_GEN_TIMER,
                             ISIS_LSP_MAXAGE + ISIS_ZERO_AGE_INT);
    }
    if ((pContext->SysActuals.u1SysType != ISIS_LEVEL1)
        && (ISIS_IS_TIMER_ACTIVE (pContext->SysL2Info.SysLSPGenTimer)
            == ISIS_TRUE))
    {

        IsisTmrRestartTimer (&(pContext->SysL2Info.SysLSPGenTimer),
                             ISIS_L2LSP_GEN_TIMER,
                             ISIS_LSP_MAXAGE + ISIS_ZERO_AGE_INT);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcMSNExceeded ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdProcessZeroRLTLSP ()
 * Description : This function processes the LSP received with zero remaining
 *               life time as specified in Sec 1.3.16.4 of ISO 10589.
 * Input(s)    : pContext  - Pointer to the system context
 *               pCktRec   - Pointer to the circuit record on which
 *                           LSP was received
 *               pu1ZLTLSP - Pointer to the received Zero RLT LSP
 *               u1Level   - Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdProcessZeroRLTLSP (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                          UINT1 *pu1ZLTLSP, UINT1 u1Level)
{
    INT4                i4RetCode = 0;
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1               u1AuthType = 0;
    UINT1               au1RcvdLSPId[ISIS_LSPID_LEN];
    UINT1              *pu1LSP = NULL;
    UINT2               u2RLTime = 0;
    UINT4               u4RcvdSeqNum = 0;
    INT4                i4Ret = ISIS_FAILURE;
    tIsisLSPEntry      *pLSPRec = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT2               u2RcvdCS = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcessZeroRLTLSP ()\n"));

    ISIS_EXTRACT_LSPID_FROM_LSP (pu1ZLTLSP, au1RcvdLSPId);
    ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1ZLTLSP, u4RcvdSeqNum);
    ISIS_DBG_PRINT_ID (au1RcvdLSPId, (UINT1) ISIS_LSPID_LEN,
                       "UPD <T> : Processing ZRLT-LSP\t", ISIS_OCTET_STRING);
    ISIS_FORM_STR (ISIS_LSPID_LEN, pu1ZLTLSP, acLSPId);
    ISIS_EXTRACT_CS_FROM_LSP (pu1ZLTLSP, u2RcvdCS);

    pLSPRec = IsisUpdGetLSP (pContext, au1RcvdLSPId, u1Level, &pPrevRec,
                             &pHashBkt, &u1Loc);

    if (pLSPRec == NULL)
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Received ZRLT-LSP Not In Database Or TxQ\n"));

        /* The received LSP does not exists in LSP data base
         */

        /* Refer to Sec 7.3.16.4 subsec a) for action on receipt of LSP 
         * with Zero remaining life time
         */

        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            /* Acknowledgements will be sent only in the case of LSPs received
             * over P2P circuits
             */

            /* The following routine expects the Offset where the LETLV specific
             * information starts. This is 2 bytes after the PDU Length in the
             * LSP header. The same routine is used for Constructing PSNP
             * requests during CSNP processing where only LETLVs are present and
             * LSP buffers are not available. Hence to make it a generic
             * routine, we advance the LSP pointer to point to the RLT field in
             * the LSP from where the LETLV specific information starts
             *
             * NOTE: The last argument for the following function is
             * '!ISIS_LSP_ACQUIRE' which means that the PSNP is being
             * constructed for the purpose of acknowledgement and not for
             * acquiring the LSP
             */

            IsisUpdConstructPSNP (pContext,
                                  (pu1ZLTLSP + sizeof (tIsisComHdr) + 2),
                                  pCktRec, u1Level, !ISIS_LSP_ACQUIRE);
        }

        /* We need not hold the LSP in our database
         */
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Ignoring Purge LSP [%s] [%s] - as it is not present in DB/TxQ\n",
                 acLSPId, ISIS_GET_LEVEL_STR (u1Level)));
        ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1ZLTLSP);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcessZeroRLTLSP ()\n"));
        return;
    }

    if ((u1Loc == ISIS_LOC_DB) || (u1Loc == ISIS_LOC_DB_FIRST))
    {
        /* The received LSP is in LSP Database
         */

        pu1LSP = pLSPRec->pu1LSP;
    }
    else
    {
        /* The received LSP is in LSP TX queue 
         */

        pu1LSP = ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec->pu1LSP;
    }

    /* Extract the RLT of the LSP in the Database
     */

    ISIS_EXTRACT_RLT_FROM_LSP (pu1LSP, u2RLTime);

    /* Compare the received LSP with the LSP extracted from the Database
     */

    i4RetCode = IsisUpdCompareLSPs (pu1ZLTLSP, pu1LSP);
    UPP_PT ((ISIS_LGST,
             "UPD <T> : Processing ZRLT LSP [%s] [%s] Seq.No [%x] ChkSum [%x] - Code [%s]\n",
             acLSPId, ISIS_GET_LEVEL_STR (u1Level), u4RcvdSeqNum, u2RcvdCS,
             ISIS_GET_LSPCMP_CODE_STR (i4RetCode)));
    switch (i4RetCode)
    {
        case ISIS_SAME_CS_GRTR_SEQNO:
        case ISIS_DIFF_CS_GRTR_SEQNO:
        case ISIS_DIFF_CS_SAME_SEQNO:

            /* The received LSP is having greater sequnece number than
             * the one in the LSP Database
             */

            /* REFERENCE : Sec 7.3.16.4 subsec b) -> 1 i,ii, iii
             */

            /* The Remaining Life Time of the LSP in DB is Non-Zero 
             */

            u1AuthType =
                IsUpdAddAuthTlvPrgLSP (pContext, pu1LSP, u1Level, u4RcvdSeqNum);

            if ((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST))
            {
                /* The LSP exists in the Transmission queue
                 */

                /* Overwrite the LSP in the Transmission Q with the received
                 * LSP and start the MAX_AGE timer so that the LSP can be
                 * purged later
                 */

                IsisUpdModifyLSP (pContext, (tIsisLSPEntry *) pLSPRec, 0,
                                  u4RcvdSeqNum, u1Loc);

                /* Stop the RemainingLifeTime Timer which is ticking
                 */

                ISIS_DEL_LSP_TIMER_NODE (((tIsisLSPTxEntry *) pLSPRec)->
                                         pLSPRec);

                /* Start the ZeroAgeLifeTime Timer
                 */

                IsisUpdStartLSPTimer (pContext,
                                      ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec,
                                      ISIS_ECT_ZERO_AGE, ISIS_ZERO_AGE_INT);
            }
            else
            {
                /* The LSP exists in the LSP Database
                 */

                /* Overwrite the LSP in the LSP Database with the received
                 * LSP and start the MAX_AGE timer so that the LSP can be
                 * purged later
                 */

                IsisUpdModifyLSP (pContext, pLSPRec, 0, u4RcvdSeqNum, u1Loc);

                /* Stop the RemainingLifeTime Timer
                 */

                ISIS_DEL_LSP_TIMER_NODE (pLSPRec);

                /* Start the ZeroAgeLifeTime Timer
                 */

                IsisUpdStartLSPTimer (pContext, pLSPRec, ISIS_ECT_ZERO_AGE,
                                      ISIS_ZERO_AGE_INT);
            }

            /* We are supposed to retain only the header of the LSP. Instead
             * we hold the entire LSP in the Database, but update the Length
             * of the LSP to Header Length
             */

            if (u1AuthType == 0)
            {
                ISIS_SET_CTRL_PDU_LEN (pu1LSP,
                                       ((UINT2)
                                        (((tIsisComHdr *) pu1LSP)->u1HdrLen)));
            }

            /* Set the RLT of the LSP to zero
             */

            ISIS_SET_LSP_RLT (pu1LSP, 0);

            /* Schedule the LSP For Transmission and clear the SSN flags
             * (PSNP information pertaining to this LSP)
             */

            i4Ret =
                IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                    (VOID *) pLSPRec, pCktRec, u1Loc, pHashBkt,
                                    ISIS_ALL_BUT_SPEC_CKT);
            if (i4Ret == ISIS_MEM_FAILURE)
            {
                IsisUpdDeleteLSP (pContext, u1Level, au1RcvdLSPId);
            }

            break;

        case ISIS_SAME_CS_LESS_SEQNO:
        case ISIS_DIFF_CS_LESS_SEQNO:

            /* REFERENCE : Sec 7.3.16.4 subsec b) -> 3 i, ii
             */

            /* The received LSP is having less sequnce number than the one in DB
             * Schedule the LSP, that exist in the database, for transmission
             * over the circuit on which the Zero RLT LSP was received
             */

            i4Ret =
                IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                    (VOID *) pLSPRec, pCktRec, u1Loc, pHashBkt,
                                    ISIS_SPEC_CKT);
            if (i4Ret == ISIS_MEM_FAILURE)
            {
                IsisUpdDeleteLSP (pContext, u1Level, au1RcvdLSPId);
            }

            break;

        case ISIS_SAME_CS_SAME_SEQNO:

            /* REFERENCE : Sec 7.3.16.4 subsec b) -> 2 i, ii
             *
             *
             * The received LSP is having the Same checksum and Same 
             * Sequnece number as the one in Database
             */

            if ((pCktRec->u1CktType == ISIS_P2P_CKT) && (u2RLTime == 0))
            {
                /* If the LSP was received over a P2P circuit and the RLT
                 * extracted from the database is zero, then transmit an
                 * acknowledgement
                 * Refer Sec. 7.3.16.4 2) ii) of ISO-10589
                 */

                /* The following routine expects the Offset where the LETLV 
                 * specific information starts. This is 2 bytes after the PDU 
                 * Length in the LSP header. The same routine is used for 
                 * Constructing PSNP requests during CSNP processing where only
                 * LETLVs are present and LSP buffers are not available. Hence 
                 * to make it a generic routine, we advance the LSP pointer to 
                 * point to the RLT field in the LSP from where the LETLV 
                 * specific information starts
                 *
                 * NOTE: The last argument for the following function is
                 * '!ISIS_LSP_ACQUIRE' which means that the PSNP is being
                 * constructed for the purpose of acknowledgement and not for
                 * acquiring the LSP
                 */

                IsisUpdConstructPSNP (pContext,
                                      (pu1ZLTLSP + sizeof (tIsisComHdr) + 2),
                                      pCktRec, u1Level, 0);

                /* Clearing SRM flags associated with the circuit for the 
                 * received LSP since an acknowledgement is being sent and 
                 * hence the LSP is no more required to be transmitted
                 */

                IsisUpdClearSRM (pContext, (tIsisLSPTxEntry *) pLSPRec,
                                 (tIsisLSPTxEntry *) pPrevRec, pCktRec,
                                 u1Loc, pHashBkt, u1Level);
            }
            else if (u2RLTime != 0)
            {
                u1AuthType =
                    IsUpdAddAuthTlvPrgLSP (pContext, pu1LSP, u1Level,
                                           u4RcvdSeqNum);
                if ((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST))
                {
                    /* The LSP exists in the Transmission queue
                     */

                    /* Overwrite the LSP in the Transmission Q with the received
                     * LSP and start the MAX_AGE timer so that the LSP can be
                     * purged later
                     */

                    IsisUpdModifyLSP (pContext, (tIsisLSPEntry *) pLSPRec, 0,
                                      u4RcvdSeqNum, u1Loc);

                    /* Stop the RemainingLifeTime Timer which is ticking
                     */

                    ISIS_DEL_LSP_TIMER_NODE (((tIsisLSPTxEntry *) pLSPRec)->
                                             pLSPRec);

                    /* Start the ZeroAgeLifeTime Timer
                     */

                    IsisUpdStartLSPTimer (pContext,
                                          ((tIsisLSPTxEntry *) pLSPRec)->
                                          pLSPRec, ISIS_ECT_ZERO_AGE,
                                          ISIS_ZERO_AGE_INT);
                }
                else
                {
                    /* The LSP exists in the LSP Database
                     */

                    /* Overwrite the LSP in the LSP Database with the received
                     * LSP and start the MAX_AGE timer so that the LSP can be
                     * purged later
                     */

                    IsisUpdModifyLSP (pContext, pLSPRec, 0, u4RcvdSeqNum,
                                      u1Loc);

                    /* Stop the RemainingLifeTime Timer
                     */

                    ISIS_DEL_LSP_TIMER_NODE (pLSPRec);

                    /* Start the ZeroAgeLifeTime Timer
                     */

                    IsisUpdStartLSPTimer (pContext, pLSPRec, ISIS_ECT_ZERO_AGE,
                                          ISIS_ZERO_AGE_INT);
                }

                /* We are supposed to retain only the header of the LSP. Instead
                 * we hold the entire LSP in the Database, but update the Length
                 * of the LSP to Header Length
                 */
                if (u1AuthType == 0)
                {
                    ISIS_SET_CTRL_PDU_LEN (pu1LSP,
                                           ((UINT2)
                                            (((tIsisComHdr *) pu1LSP)->
                                             u1HdrLen)));
                }

                /* Set the RLT of the LSP to zero
                 */

                ISIS_SET_LSP_RLT (pu1LSP, 0);

                /* Schedule the LSP For Transmission and clear the SSN flags
                 * (PSNP information pertaining to this LSP)
                 */

                i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                            (VOID *) pLSPRec, pCktRec, u1Loc,
                                            pHashBkt, ISIS_ALL_BUT_SPEC_CKT);
                if (i4Ret == ISIS_MEM_FAILURE)
                {
                    IsisUpdDeleteLSP (pContext, u1Level, au1RcvdLSPId);
                }
            }
            break;

        default:

            /* Should never happen - Ignore 
             */
            break;
    }
    ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1ZLTLSP);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcessZeroRLTLSP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdNWPurgeLSP ()
 * Description : This function Transmits the LSP on all active Circuits to Purge
 *               the LSP from the Network.
 * Input(s)    : pContext  - Pointer to the system context
 *               pu1LSP    - Pointer to the received LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdNWPurgeLSP (tIsisSysContext * pContext, UINT1 *pu1LSP)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Level = 0;
    UINT1              *pu1CktFlag = NULL;
    tIsisLSPEntry      *pLSPRec = NULL;
    tIsisLSPTxEntry    *pTxRec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdNWPurgeLSP ()\n"));

    /* Set the Remaining life time as zero */

    ISIS_SET_LSP_RLT (pu1LSP, 0);

    u1Level = ISIS_EXTRACT_PDU_TYPE (pu1LSP);
    u1Level = (u1Level == ISIS_L1LSP_PDU) ? ISIS_LEVEL1 : ISIS_LEVEL2;
    pu1CktFlag = (u1Level == ISIS_LEVEL1) ? pContext->CktTable.pu1L1CktMask
        : pContext->CktTable.pu1L2CktMask;

    if (pu1CktFlag == NULL)
    {
        ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1LSP);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdNWPurgeLSP ()\n"));
        return;
    }
    pLSPRec = (tIsisLSPEntry *)
        ISIS_MEM_ALLOC (ISIS_BUF_LDBE, sizeof (tIsisLSPEntry));

    pTxRec = (tIsisLSPTxEntry *)
        ISIS_MEM_ALLOC (ISIS_BUF_LTXQ, sizeof (tIsisLSPTxEntry));

    if ((pLSPRec == NULL) || (pTxRec == NULL))
    {
        if (pLSPRec != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pLSPRec);
        }
        if (pTxRec != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pTxRec);
        }
        ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1LSP);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdNWPurgeLSP ()\n"));
        return;
    }

    pLSPRec->pu1LSP = pu1LSP;
    pLSPRec->pPrevTimerLink = NULL;
    pLSPRec->pNxtTimerLink = NULL;
    pTxRec->pLSPRec = pLSPRec;
    pTxRec->pu1SRM = NULL;
    pTxRec->u1SRMLen = 0;

    i4RetVal = IsisUpdSetCktBitPat (pContext, &(pTxRec->pu1SRM), pu1CktFlag,
                                    &(pTxRec->u1SRMLen), 0, ISIS_ALL_CKT);

    if (i4RetVal == ISIS_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pLSPRec);
        ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pTxRec);
        ISIS_MEM_FREE (ISIS_BUF_LSPI, pu1LSP);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdNWPurgeLSP ()\n"));
        return;
    }

    if (u1Level == ISIS_LEVEL1)
    {
        pTxRec->pLSPRec->pNext = (tIsisLSPEntry *) pContext->pL1PurgeQueue;
        pContext->pL1PurgeQueue = pTxRec;
    }
    else
    {
        pTxRec->pLSPRec->pNext = (tIsisLSPEntry *) pContext->pL2PurgeQueue;
        pContext->pL2PurgeQueue = pTxRec;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdNWPurgeLSP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdGRCSNPCheck ()
 * Description : This function 
 *               
 * Input(s)    : pContext  - Pointer to the system context
 *               pu1CSNP    - Pointer to the received LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdGRCSNPCheck (tIsisSysContext * pContext,
                    tIsisCktEntry * pCktRec, UINT1 *pu1CSNP, UINT1 u1Level)
{
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT2               u2CheckSum;
    UINT2               u2RLTime;
    UINT4               u4SeqNum;
    UINT4               u4AdjFlag = ISIS_FALSE;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisLSPEntry      *pLSPRec = NULL;
    tLspInfo           *pCsnpInfo = NULL;
    tIsisGrCheck       *pLstNode = NULL;

    ISIS_GET_2_BYTES (pu1CSNP, (UINT2) 0, u2RLTime);
    ISIS_GET_DATA (au1LSPId, (pu1CSNP + 2), ISIS_LSPID_LEN);
    ISIS_GET_4_BYTES (pu1CSNP, (2 + ISIS_LSPID_LEN), u4SeqNum);
    ISIS_GET_2_BYTES (pu1CSNP, (2 + ISIS_LSPID_LEN + 4), u2CheckSum);

    /*Removing the CSNP entry from the Circuit List, as 
     * CSNP received from the circuit*/
    do
    {
        if ((u1Level == ISIS_LEVEL1) || (u1Level == ISIS_LEVEL12)
            || (u4AdjFlag == ISIS_TRUE))
        {
            TMO_SLL_Scan (&(pContext->SysL1Info.CSNPLst), pLstNode,
                          tIsisGrCheck *)
            {
                if (pLstNode->u4CktId == pCktRec->u4CktIdx)
                {
                    pCktRec->u1IsisGRRestartL1State =
                        pCktRec->u1IsisGRRestartL1State | ISIS_GR_CSNP_RCVD;

                    if (pCktRec->pL2CktInfo != NULL)
                    {
                        if (pCktRec->pL2CktInfo->u4NumAdjs == 0)
                        {
                            u4AdjFlag = ISIS_TRUE;
                        }
                    }
                    break;
                }
            }
            if (pLstNode != NULL)
            {
                TMO_SLL_Delete (&(pContext->SysL1Info.CSNPLst),
                                &pLstNode->next);
                MemReleaseMemBlock (gIsisMemPoolId.u4CSNPRxPid,
                                    (UINT1 *) pLstNode);
            }
        }
        if ((u1Level == ISIS_LEVEL2) || (u1Level == ISIS_LEVEL12)
            || (u4AdjFlag == ISIS_TRUE))
        {
            u4AdjFlag = ISIS_FALSE;

            TMO_SLL_Scan (&(pContext->SysL2Info.CSNPLst), pLstNode,
                          tIsisGrCheck *)
            {
                if (pLstNode->u4CktId == pCktRec->u4CktIdx)
                {
                    pCktRec->u1IsisGRRestartL2State =
                        pCktRec->u1IsisGRRestartL2State | ISIS_GR_CSNP_RCVD;

                    if (pCktRec->pL1CktInfo != NULL)
                    {
                        if (pCktRec->pL1CktInfo->u4NumAdjs == 0)
                        {
                            u4AdjFlag = ISIS_TRUE;
                        }
                    }
                    break;
                }
            }
            if (pLstNode != NULL)
            {
                TMO_SLL_Delete (&(pContext->SysL2Info.CSNPLst),
                                &pLstNode->next);
                MemReleaseMemBlock (gIsisMemPoolId.u4CSNPRxPid,
                                    (UINT1 *) pLstNode);

            }

        }
    }
    while (u4AdjFlag == ISIS_TRUE);

    if (((TMO_SLL_Count (&(pContext->SysL1Info.AckLst)) == 0) &&
         (TMO_SLL_Count (&(pContext->SysL2Info.AckLst)) == 0) &&
         (TMO_SLL_Count (&(pContext->SysL1Info.CSNPLst)) == 0) &&
         (TMO_SLL_Count (&(pContext->SysL2Info.CSNPLst)) == 0)))
    {

        if (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART)
        {
            if (gu1IsisGrState == ISIS_GR_ADJ_SEEN_RA)
            {
                gu1IsisGrState = ISIS_GR_ADJ_SEEN_CSNP;
            }
        }

    }

    /*Adding CSNP Entry in the CSNP RB Tree */
    if (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART)
    {
        pLSPRec = IsisUpdGetLSP (pContext, au1LSPId, u1Level, &pPrevRec,
                                 &pHashBkt, &u1Loc);
        if (pLSPRec == NULL)
        {
            pCsnpInfo =
                (tLspInfo *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tLspInfo));

            if (pCsnpInfo == NULL)
            {
                return;
            }

            /*Populating the structure */
            pCsnpInfo->u2RLTime = u2RLTime;
            MEMCPY (pCsnpInfo->au1LSPId, au1LSPId, ISIS_LSPID_LEN);
            pCsnpInfo->u4SeqNum = u4SeqNum;
            pCsnpInfo->u2CheckSum = u2CheckSum;

            if (u1Level == ISIS_LEVEL1)
            {
                /*Check if the entry already exist */
                if (RBTreeGet (pContext->SysL1Info.pGrCSNPTbl, pCsnpInfo) ==
                    NULL)
                {

                    /*Check Entry is available in LSP TXQ or Database. if not then add */
                    /* The LSP structure is added to the  RBTRee Table */
                    if (RBTreeAdd (pContext->SysL1Info.pGrCSNPTbl, pCsnpInfo) ==
                        RB_FAILURE)
                    {
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pCsnpInfo);
                        return;
                    }
                }
                else
                {
                    ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pCsnpInfo);
                    return;
                }
            }
            else if (u1Level == ISIS_LEVEL2)
            {
                /*Check if the entry already exist */
                if (RBTreeGet (pContext->SysL2Info.pGrCSNPTbl, pCsnpInfo) ==
                    NULL)
                {

                    /* The LSP structure is added to the  RBTRee Table */
                    if (RBTreeAdd (pContext->SysL2Info.pGrCSNPTbl, pCsnpInfo) ==
                        RB_FAILURE)
                    {
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pCsnpInfo);
                        return;
                    }
                }
                else
                {
                    ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pCsnpInfo);
                    return;
                }
            }
            else
            {
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pCsnpInfo);
            }
        }
    }
}

/******************************************************************************
 * Function    : IsisUpdGRLSPCheck ()
 * Description : This function 
 *               
 * Input(s)    : pContext  - Pointer to the system context
 *               pu1CSNP    - Pointer to the received LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdGRLSPCheck (tIsisSysContext * pContext, UINT1 *pu1RcvdLSP, UINT1 u1Level)
{
    tLspInfo           *pLspInfo = NULL;
    tLspInfo            LspInfo;
    tLspInfo           *pSearchLspInfo = NULL;

    UINT2               u2RcvdRemLT = 0;
    UINT4               u4RcvdSeqNum = 0;
    UINT2               u2RcvdCS = 0;
    UINT1               au1SysID[ISIS_LSPID_LEN];
    UINT1               u1ExitL1 = ISIS_TRUE;
    UINT1               u1ExitL2 = ISIS_TRUE;
    tGrSemInfo          GrSemInfo;

    pSearchLspInfo = &LspInfo;

    MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
    MEMSET (pSearchLspInfo, 0, sizeof (tLspInfo));

    /*Extracting the LSP information */
    ISIS_EXTRACT_LSPID_FROM_LSP (pu1RcvdLSP, au1SysID);
    ISIS_EXTRACT_RLT_FROM_LSP (pu1RcvdLSP, u2RcvdRemLT);
    ISIS_EXTRACT_CS_FROM_LSP (pu1RcvdLSP, u2RcvdCS);
    ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1RcvdLSP, u4RcvdSeqNum);
    /*    MEMCPY (au1SysID, (pu1RcvdLSP + (UINT2) ISIS_OFFSET_LSPID),
       ISIS_SYS_ID_LEN); */

    MEMCPY (pSearchLspInfo->au1LSPId, au1SysID, ISIS_LSPID_LEN);
    pSearchLspInfo->u2RLTime = u2RcvdRemLT;
    pSearchLspInfo->u2CheckSum = u2RcvdCS;
    pSearchLspInfo->u4SeqNum = u4RcvdSeqNum;

    /*Level-1 */
    if (u1Level == ISIS_LEVEL1)
    {

        if ((pLspInfo = RBTreeGet (pContext->SysL1Info.pGrCSNPTbl,
                                   pSearchLspInfo)) != NULL)
        {
            /* pLsaInfo is already added in the list, it is supposed to be deleted */
            if (RBTreeRem (pContext->SysL1Info.pGrCSNPTbl, pLspInfo) == NULL)
            {
                return;
            }
        }
    }
    /*Validation */
    if (RBTreeGetFirst (pContext->SysL1Info.pGrCSNPTbl) != NULL)
    {
        u1ExitL1 = ISIS_FALSE;
    }

    /*Level-2 */
    if (u1Level == ISIS_LEVEL2)
    {
        {

            if ((pLspInfo = RBTreeGet (pContext->SysL2Info.pGrCSNPTbl,
                                       pSearchLspInfo)) != NULL)
            {
                /* pLsaInfo is already added in the list, it is supposed to be deleted */
                if (RBTreeRem (pContext->SysL2Info.pGrCSNPTbl, pLspInfo) ==
                    NULL)
                {
                    return;
                }
            }
        }

    }
    /*Validation */
    if (RBTreeGetFirst (pContext->SysL2Info.pGrCSNPTbl) != NULL)
    {
        u1ExitL2 = ISIS_FALSE;
    }
    if ((u1ExitL1 == ISIS_TRUE) && (u1ExitL2 == ISIS_TRUE))
    {
        GrSemInfo.pContext = pContext;
        IsisGrRunRestartSem (ISIS_GR_EVT_LSP_DB_SYNC, ISIS_GR_ADJ_RESTART,
                             &GrSemInfo);
    }

}

VOID
IsisUpdHandleP2PCircuitChange (tIsisSysContext * pContext, UINT1 u1State,
                               UINT4 u4CktId)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    UINT1               au1LastTxId[ISIS_LSPID_LEN];
    UINT2               u2Len = 0;
    UINT1               u1Level;
    UINT1               u1TempLevel;
    UINT1              *pu1CSNP = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    UINT1               u1Loc = 0;
    UINT1               u1Flag = 0;
    tIsisLSPEntry      *pLSPDBMark = NULL;
    tIsisLSPTxEntry    *pLSPTxQMark = NULL;

    MEMSET (au1LastTxId, 0x00, ISIS_LSPID_LEN);
    i4RetVal = IsisAdjGetCktRec (pContext, u4CktId, &pCktRec);
    if ((i4RetVal == ISIS_FAILURE) || (pCktRec == NULL))
    {
        /*invalid circuit */
        return;
    }

    if (u1State == ISIS_CKT_UP)
    {
        if ((pCktRec->pAdjEntry == NULL) || ((pCktRec->pAdjEntry)
                                             && (pCktRec->
                                                 u1P2PDynHshkMachanism ==
                                                 ISIS_P2P_THREE_WAY)
                                             && (pCktRec->pAdjEntry->
                                                 u1P2PThreewayState !=
                                                 ISIS_ADJ_UP)))
        {
            return;
        }
        /*Get the level */
        u1Level = pCktRec->pAdjEntry->u1AdjUsage;
        /*Sending CSNP */
        if (u1Level == ISIS_LEVEL12)
        {
            u1TempLevel = ISIS_LEVEL1;
        }
        else
        {
            u1TempLevel = u1Level;
        }
        while (u1TempLevel)
        {
            /*fetch circuit Level Record */
            if (u1TempLevel == ISIS_LEVEL1)
            {
                pCktLevel = pCktRec->pL1CktInfo;
            }
            else if (u1TempLevel == ISIS_LEVEL2)
            {
                pCktLevel = pCktRec->pL2CktInfo;
            }
            if ((pCktLevel) && (pCktLevel->u1CSNPSent == ISIS_ZERO))
            {
                i4RetVal = IsisUpdSchLSPsForTxOnP2PCirc (pCktRec, u1TempLevel);
                if (i4RetVal == ISIS_SUCCESS)
                {
                    pLSPTxQMark =
                        IsisUpdGetLSPFromTxQ (pContext, au1LastTxId,
                                              u1TempLevel, &pPrevTxRec,
                                              &pHashBkt, &u1Loc, &u1Flag);
                    pLSPDBMark =
                        IsisUpdGetLSPFromDB (pContext, au1LastTxId, u1TempLevel,
                                             &pPrevDbRec, &pHashBkt, &u1Loc,
                                             &u1Flag);

                    while ((pLSPTxQMark != NULL) || (pLSPDBMark != NULL))
                    {
                        i4RetVal = IsisUpdConstructCSNP (pContext, &pLSPTxQMark,
                                                         &pLSPDBMark, &pu1CSNP,
                                                         u1TempLevel);

                        if (i4RetVal == ISIS_SUCCESS)
                        {
                            ISIS_GET_2_BYTES (pu1CSNP, ISIS_OFFSET_PDULEN,
                                              u2Len);
                            IsisTrfrTxData (pCktRec, pu1CSNP, u2Len,
                                            u1TempLevel, ISIS_FREE_BUF,
                                            ISIS_FALSE);
                            pCktLevel->u1CSNPSent = ISIS_TRUE;
                        }
                    }
                }
            }
            if ((u1Level == ISIS_LEVEL12) && (u1TempLevel == ISIS_LEVEL1))
            {
                u1TempLevel = ISIS_LEVEL2;
            }
            else
            {
                /*resetting temp level */
                u1TempLevel = ISIS_ZERO;
            }
        }

    }
    else if (u1State == ISIS_CKT_DOWN)
    {
        /*Get the level */
        u1Level = pContext->SysActuals.u1SysType;
        /*Stopping PSNP timers for the circuit */
        if ((u1Level & ISIS_LEVEL1) && (pCktRec->pL1CktInfo))
        {
            /*Also update the CSNPSent flag on the circuit LEVEL record */
            pCktRec->pL1CktInfo->u1CSNPSent = ISIS_ZERO;
            pCktRec->pL1CktInfo->u1SetSRM = ISIS_TRUE;
        }

        if ((u1Level & ISIS_LEVEL2) && (pCktRec->pL2CktInfo))
        {
            /*Also update the CSNPSent flag on the circuit LEVEL record */
            pCktRec->pL2CktInfo->u1CSNPSent = ISIS_ZERO;
            pCktRec->pL2CktInfo->u1SetSRM = ISIS_TRUE;
        }
    }
    return;
}

INT4
IsisUpdSchLSPsForTxOnP2PCirc (tIsisCktEntry * pCktRec, UINT1 u1Level)
{
    UINT1               u1Flag = 0;
    UINT1               u1TxLoc = ISIS_LOC_NONE;
    UINT1               u1DbLoc = ISIS_LOC_NONE;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisLSPEntry      *pLSPDbRec = NULL;
    tIsisLSPEntry      *pNextDbRec = NULL;
    tIsisLSPTxEntry    *pLSPTxRec = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisLSPTxEntry    *pNextTxRec = NULL;
    tIsisHashBucket    *pDbHashBkt = NULL;
    tIsisHashBucket    *pTxHashBkt = NULL;
    tIsisHashBucket    *pNextTxHashBkt = NULL;
    tIsisHashBucket    *pNextDbHashBkt = NULL;
    tIsisSysContext    *pContext = pCktRec->pContext;
    tIsisCktLevel      *pCktLevel = NULL;
    UINT1               au1LspId[ISIS_LSPID_LEN];
    UINT1               u1IsOtherLSP = ISIS_FALSE;
    INT4                i4RetVal = ISIS_FAILURE;
    MEMSET (au1LspId, 0, ISIS_LSPID_LEN);

    if (pContext == NULL)
    {
        return ISIS_FAILURE;
    }
    pCktLevel =
        (u1Level == ISIS_LEVEL2) ? pCktRec->pL2CktInfo : pCktRec->pL1CktInfo;

    if (pCktLevel->u1SetSRM == ISIS_FALSE)
    {
        return ISIS_SUCCESS;
    }

    pLSPDbRec =
        IsisUpdGetLSPFromDB (pContext, au1LspId, u1Level, &pPrevDbRec,
                             &pDbHashBkt, &u1DbLoc, &u1Flag);
    pLSPTxRec =
        IsisUpdGetLSPFromTxQ (pContext, au1LspId,
                              u1Level, &pPrevTxRec,
                              &pTxHashBkt, &u1TxLoc, &u1Flag);
    while (pLSPTxRec != NULL)
    {
        pNextTxHashBkt = pTxHashBkt;
        pNextTxRec = IsisUpdGetNextTxRec (pLSPTxRec, &pNextTxHashBkt);

        if ((MEMCMP ((pLSPTxRec->pLSPRec->pu1LSP + (UINT2) ISIS_OFFSET_LSPID),
                     pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN)) != 0)
        {
            /* Self LSP will be Txed automatically on circuit UP - so it is skipped */
            i4RetVal =
                IsisUpdSchLSPForTx (pContext, pPrevTxRec, pLSPTxRec, pCktRec,
                                    u1TxLoc, pTxHashBkt, ISIS_SPEC_CKT);
            u1IsOtherLSP = ISIS_TRUE;
        }

        pPrevTxRec = pLSPTxRec;
        u1TxLoc = ISIS_LOC_TX;
        pLSPTxRec = pNextTxRec;
        pTxHashBkt = pNextTxHashBkt;
    }

    pNextDbHashBkt = pDbHashBkt;
    while (pLSPDbRec != NULL)
    {
        pNextDbRec = IsisUpdGetNextDbRec (pLSPDbRec, &pNextDbHashBkt);

        if ((MEMCMP ((pLSPDbRec->pu1LSP + (UINT2) ISIS_OFFSET_LSPID),
                     pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN)) != 0)
        {
            /* Self LSP will be Txed automatically on circuit UP - so it is skipped */
            i4RetVal =
                IsisUpdSchLSPForTx (pContext, pPrevDbRec, pLSPDbRec, pCktRec,
                                    u1DbLoc, pDbHashBkt, ISIS_SPEC_CKT);
            u1IsOtherLSP = ISIS_TRUE;
        }

        pLSPDbRec = pNextDbRec;

        /*Made NULL because the previous LSP would have been removed from DB and placed in TxQ */
        pPrevDbRec = NULL;
        pDbHashBkt = NULL;
    }

    UNUSED_PARAM (i4RetVal);
    if (u1IsOtherLSP == ISIS_TRUE)
    {
        return ISIS_SUCCESS;
    }
    return ISIS_FAILURE;
}
