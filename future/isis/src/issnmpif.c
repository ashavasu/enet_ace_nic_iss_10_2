/******************************************************************************
 *  *
 *  *   Copyright (C) Future Software Limited, 2001
 *  *
 *  *   $Id: issnmpif.c,v 1.10 2014/04/15 12:49:41 siva Exp $
 *  *
 *  *************************************************************************/

#include "fssnmp.h"
#include "isincl.h"
#include "isis-mib.h"
#include "snmputil.h"
#ifdef SNMP_WANTED
# include  "extern.h"
# include  "snmcport.h"
#endif

/* Proto types of the functions private to this file only */

PRIVATE tSNMP_OID_TYPE *IsisGetOIDFromObjName (INT1 *textStr);
UINT4               SNMP_TRAPS_OID[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

UINT4               ISIS_TRAPS_OID[] = { 1, 3, 6, 1, 3, 37, 2, 0 };

/*******************************************************************************
 * Function    : IsisSnmpIfSendTrap ()
 * Description : This routine interfaces with the SNMP agent to send the
 *               trap.The function should be appropraitely ported with the
 *               target SNMP agent. 
 * Input(s)    : pIsisEvent - Pointer to the IsisEvent that holds the
 *               information to be sent in the trap message.
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : None
 ******************************************************************************/

PUBLIC VOID
IsisSnmpIfSendTrap (UINT1 *pIsisEvent, UINT1 u1TrapId)
{

    tIsisEvtIDLenMismatch *pEvtIDLenMismatch = NULL;
    tIsisEvtLSPDBOL    *pEvtLSPDBOL = NULL;
    tIsisEvtManAADropped *pEvtManAADropped = NULL;
    tIsisEvtCorrLsp    *pEvtCorrLsp = NULL;
    tIsisEvtSeqNoExceed *pEvtSeqNoExceed = NULL;
    tIsisEvtMaxAAMismatch *pEvtMaxAAMismatch = NULL;
    tIsisEvtOwnLspPurge *pEvtOwnLspPurge = NULL;
    tIsisEvtSeqNoSkip  *pEvtSeqNoSkip = NULL;
    tIsisEvtAuthFail   *pEvtAuthFail = NULL;
    tIsisEvtVerMismatch *pEvtVerMismatch = NULL;
    tIsisEvtAAMismatch *pEvtAAMismatch = NULL;
    tIsisEvtAdjRej     *pEvtAdjRej = NULL;
    tIsisEvtLspSizeTooLarge *pEvtLspSizeTooLarge = NULL;
    tIsisEvtLspBuffSizeMismatch *pEvtLspBuffSizeMismatch = NULL;
    tIsisEvtProtSuppMismatch *pEvtProtSuppMismatch = NULL;
    tIsisEvtRestarter  *pEvtRestart = NULL;
    tIsisEvtHelper     *pEvtHelper = NULL;

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE Counter64 = { 0, 0 };
    UINT1               au1Buf[256];
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };

    pEnterpriseOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, ISIS_TRAPS_OID,
            sizeof (ISIS_TRAPS_OID));
    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - 2;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = 0;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, SNMP_TRAPS_OID,
            SNMP_V2_TRAP_OID_LEN * sizeof (UINT4));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    switch (u1TrapId)
    {
        case ISIS_EVT_RESTARTER:

            pEvtRestart = (tIsisEvtRestarter *) (VOID *) pIsisEvent;

            SPRINTF ((char *) au1Buf, "isisSysID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtRestart->au1SysID,
                                                 ISIS_SYS_ID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "fsIsisExtRestartStatus");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      pEvtRestart->u1IsisGRRestartStatus, NULL,
                                      NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "fsIsisExtGRRestartTimeInterval");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      pEvtRestart->u2IsisGRT3TimerInterval,
                                      NULL, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "fsIsisExtRestartExitReason");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      pEvtRestart->u1IsisGRRestartExitReason,
                                      NULL, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            break;

        case ISIS_EVT_HELPER:

            pEvtHelper = (tIsisEvtHelper *) (VOID *) pIsisEvent;

            SPRINTF ((char *) au1Buf, "isisSysID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtHelper->au1SysID,
                                                 ISIS_SYS_ID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisISAdjNeighSysID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtHelper->au1AdjNbrSysID,
                                                 ISIS_SYS_ID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "fsIsisExtAdjHelperStatus");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      pEvtHelper->u1IsisGRHelperStatus, NULL,
                                      NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "fsIsisExtHelperGraceTimeLimit");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      pEvtHelper->u2AdjGRTime, NULL, NULL,
                                      Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "fsIsisExtAdjHelperExitReason");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER, 0,
                                      pEvtHelper->u1IsisGRHelperExitReason,
                                      NULL, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            break;

        case ISIS_EVT_LSP_DBOL:

            pEvtLSPDBOL = (tIsisEvtLSPDBOL *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLSPDBOL->u1Level, 0, NULL, NULL,
                                      Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisSysL1State");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLSPDBOL->u1SysL1State, 0, NULL, NULL,
                                      Counter64);
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisSysL2State");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLSPDBOL->u1SysL2State, 0, NULL, NULL,
                                      Counter64);

            break;

        case ISIS_EVT_MAN_ADDR_DROPPED:

            pEvtManAADropped = (tIsisEvtManAADropped *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }

            SPRINTF ((char *) au1Buf, "isisManAreaAddrExistState");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtManAADropped->u1MAAExistState, 0,
                                      NULL, NULL, Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;
            break;

        case ISIS_EVT_CORR_LSP_DET:
            pEvtCorrLsp = (tIsisEvtCorrLsp *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtCorrLsp->u1Level, 0, NULL, NULL,
                                      Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisTrapLSPID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtCorrLsp->au1TrapLspID,
                                                 ISIS_LSPID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            break;

        case ISIS_EVT_MAX_SEQNO_EXCEED:
            pEvtSeqNoExceed = (tIsisEvtSeqNoExceed *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtSeqNoExceed->u1Level, 0, NULL, NULL,
                                      Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisTrapLSPID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtSeqNoExceed->au1TrapLspID,
                                                 ISIS_LSPID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);
            break;

        case ISIS_EVT_ID_LEN_MISMATCH:

            pEvtIDLenMismatch = (tIsisEvtIDLenMismatch *) (VOID *) pIsisEvent;

            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisFieldLen");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtIDLenMismatch->u1RxdIdLen, 0, NULL,
                                      NULL, Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtIDLenMismatch->u4CktIfIdx, 0, NULL,
                                      NULL, Counter64);
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisPDUFragment");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtIDLenMismatch->pu1PduFrag,
                                                 ISIS_TRAP_PDU_FRAG_SIZE);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            break;

        case ISIS_EVT_MAX_AA_MISMATCH:

            pEvtMaxAAMismatch = (tIsisEvtMaxAAMismatch *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }

            SPRINTF ((char *) au1Buf, "isisMaxAreaAddress");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtMaxAAMismatch->u1RxdMAA, 0, NULL,
                                      NULL, Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtMaxAAMismatch->u4CktIfIdx, 0, NULL,
                                      NULL, Counter64);
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisPDUFragment");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtMaxAAMismatch->pu1PduFrag,
                                                 ISIS_TRAP_PDU_FRAG_SIZE);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            break;

        case ISIS_EVT_OWN_LSP_PURGE:

            pEvtOwnLspPurge = (tIsisEvtOwnLspPurge *) (VOID *) pIsisEvent;

            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtOwnLspPurge->u4CktIfIdx, 0, NULL,
                                      NULL, Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisTrapLSPID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtOwnLspPurge->au1TrapLspID,
                                                 ISIS_LSPID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtOwnLspPurge->u1SysLevel, 0, NULL,
                                      NULL, Counter64);
            break;

        case ISIS_EVT_SEQ_NO_SKIP:

            pEvtSeqNoSkip = (tIsisEvtSeqNoSkip *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }

            SPRINTF ((char *) au1Buf, "isisTrapLSPID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtSeqNoSkip->au1TrapLspID,
                                                 ISIS_LSPID_LEN);

            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtSeqNoSkip->u4CktIfIdx, 0, NULL, NULL,
                                      Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtSeqNoSkip->u1SysLevel, 0, NULL, NULL,
                                      Counter64);

            break;

        case ISIS_EVT_AUTH_FAIL:

            pEvtAuthFail = (tIsisEvtAuthFail *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtAuthFail->u1SysLevel, 0, NULL, NULL,
                                      Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisPDUFragment");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtAuthFail->pu1PduFrag,
                                                 ISIS_TRAP_PDU_FRAG_SIZE);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);
            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtAuthFail->u4CktIfIdx, 0, NULL, NULL,
                                      Counter64);
            break;

        case ISIS_EVT_VER_MISMATCH:

            pEvtVerMismatch = (tIsisEvtVerMismatch *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisProtocolVersion");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtVerMismatch->u1ProtVer, 0, NULL, NULL,
                                      Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtVerMismatch->u1SysLevel, 0, NULL,
                                      NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisPDUFragment");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtVerMismatch->pu1PduFrag,
                                                 ISIS_TRAP_PDU_FRAG_SIZE);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            break;

        case ISIS_EVT_AA_MISMATCH:

            pEvtAAMismatch = (tIsisEvtAAMismatch *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisLSPSize");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtAAMismatch->u2LSPSize, 0, NULL, NULL,
                                      Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtAAMismatch->u1SysLevel, 0, NULL, NULL,
                                      Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtAAMismatch->u4CktIfIdx, 0, NULL, NULL,
                                      Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisPDUFragment");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtAAMismatch->pu1PduFrag,
                                                 ISIS_TRAP_PDU_FRAG_SIZE);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            break;

        case ISIS_EVT_REJECTED_ADJ:

            pEvtAdjRej = (tIsisEvtAdjRej *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtAdjRej->u1SysLevel, 0, NULL, NULL,
                                      Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtAdjRej->u4CktIdx, 0, NULL, NULL,
                                      Counter64);

            break;

        case ISIS_TRAP_LSP_LARGE_TO_PURGE:

            pEvtLspSizeTooLarge =
                (tIsisEvtLspSizeTooLarge *) (VOID *) pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisLSPSize");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLspSizeTooLarge->u2LSPSize, 0, NULL,
                                      NULL, Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLspSizeTooLarge->u1SysLevel, 0, NULL,
                                      NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisTrapLSPID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtLspSizeTooLarge->
                                                 au1TrapLspID, ISIS_LSPID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLspSizeTooLarge->u4CktIfIdx, 0, NULL,
                                      NULL, Counter64);

            break;

        case ISIS_EVT_LSP_BUFSIZE_MISMATCH:

            pEvtLspBuffSizeMismatch = (tIsisEvtLspBuffSizeMismatch *) (VOID *)
                pIsisEvent;
            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisOriginatingBufferSize");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLspBuffSizeMismatch->u2BuffSize, 0,
                                      NULL, NULL, Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLspBuffSizeMismatch->u1SysLevel, 0,
                                      NULL, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisTrapLSPID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtLspBuffSizeMismatch->
                                                 au1TrapLspID, ISIS_LSPID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtLspBuffSizeMismatch->u4CktIfIdx, 0,
                                      NULL, NULL, Counter64);

            break;

        case ISIS_EVT_PROT_SUPP_MISMATCH:

            pEvtProtSuppMismatch =
                (tIsisEvtProtSuppMismatch *) (VOID *) pIsisEvent;

            if (pStartVb != NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
            }
            SPRINTF ((char *) au1Buf, "isisProtocolsSupported");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtProtSuppMismatch->
                                                 au1UProt, ISIS_MAX_PROTS_SUPP);

            pVbList = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);
            if (pVbList == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                return;
            }
            pStartVb = pVbList;

            SPRINTF ((char *) au1Buf, "isisSystemLevel");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtProtSuppMismatch->u1SysLevel, 0, NULL,
                                      NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisTrapLSPID");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pOstring = SNMP_AGT_FormOctetString (pEvtProtSuppMismatch->
                                                 au1TrapLspID, ISIS_LSPID_LEN);

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM,
                                      0L, 0, pOstring, NULL, Counter64);

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((char *) au1Buf, "isisCircIfIndex");
            pOid = IsisGetOIDFromObjName ((INT1 *) au1Buf);
            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER,
                                      pEvtProtSuppMismatch->u4CktIfIdx, 0, NULL,
                                      NULL, Counter64);

    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
}

/******************************************************************************
 * Function      : IsisGetOIDFromObjName ()
 * Description   : This Function gets the Object Identifier (OID) for the given
 *                 object in the stdisis.mib
 * Input(s)      : pObjName - Pointer to the Name of the Object
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : pointer to the object identifier if the object exist
 *                 else NULL.
 ******************************************************************************/

PRIVATE tSNMP_OID_TYPE *
IsisGetOIDFromObjName (INT1 *pi1ObjName)
{
    UINT4               u4ObjLen = 0;
    UINT2               u2Count = 0;
    UINT2               u2Size = 0;
    INT1                ai1pNumber[SNMP_MAX_OID_LENGTH];

    MEMSET (ai1pNumber, 0, SNMP_MAX_OID_LENGTH);
    u2Size = (sizeof (orig_mib_oid_table) / sizeof (struct MIB_OID));

    u4ObjLen = STRLEN (pi1ObjName);

    for (u2Count = 0; ((u2Count < u2Size) &&
                       orig_mib_oid_table[u2Count].pName != NULL); u2Count++)
    {
        if ((STRNCMP ((CHAR *) pi1ObjName, orig_mib_oid_table[u2Count].pName,
                      u4ObjLen) == 0)
            && (STRLEN (orig_mib_oid_table[u2Count].pName) == u4ObjLen))
        {
            STRNCPY (ai1pNumber, orig_mib_oid_table[u2Count].pNumber,
                     STRLEN (orig_mib_oid_table[u2Count].pNumber));
            return (SNMP_AGT_GetOidFromString (ai1pNumber));
        }
    }
    return NULL;
}
