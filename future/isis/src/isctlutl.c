/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isctlutl.c,v 1.30 2017/09/11 13:44:08 siva Exp $
 *
 * Description: This file contains the data base access routines for the Control
 *              Module
 *****************************************************************************/

#include "isincl.h"
#include "isextn.h"

/******************************************************************************
 * Function      : IsisCtrlInitSysContext ()
 * Description   : This routine Initialises the System Table with the 
 *                 default values when the System entry is created 
 * Input(s)      : pContext  - Pointer to the context
 *                 u4InstIdx - Instance Index of the Context
 * Output(s)     : None
 * Globals       : None refered nor modified
 * Returns       : VOID
 *****************************************************************************/

PUBLIC INT4
IsisCtrlInitSysContext (tIsisSysContext * pContext, UINT4 u4InstIdx)
{
    UINT4               u4CsrRestoreFlag = 0;
    UINT1               u1RtmFlag = (UINT1) ISIS_FAILURE;
    UINT1               u1Rtm6Flag = (UINT1) ISIS_FAILURE;
    UINT1               au1SemName[OSIX_NAME_LEN + 4];
    tRtmRegnId          RegnIdv4;
    tRtm6RegnId         RegnIdv6;

    MEMSET (&RegnIdv6, 0, sizeof (tRtm6RegnId));

    pContext->u4SysInstIdx = u4InstIdx;
    RegnIdv6.u4ContextId = pContext->u4SysInstIdx;
    RegnIdv6.u2ProtoId = ISIS_ID;

    MEMSET (&RegnIdv4, 0, sizeof (tRtmRegnId));
    RegnIdv4.u2ProtoId = ISIS_ID;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlInitSysContext ()\n"));

    MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
    IsisGetSemName (pContext, au1SemName);

    /*Create RBTree to store redistributed routes */
    pContext->RRDRouteRBTree =
        RBTreeCreateEmbeddedExtended (0, IsisUpdRRDRoutesRBTreeCmp,
                                      ((UINT1 *) au1SemName));

    if (pContext->RRDRouteRBTree == NULL)
    {
        PANIC ((ISIS_LGST,
                "CTL <P> : Failed RBTree creation for RRD routes\n"));

        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlInitSysContext ()\n"));
        return ISIS_FAILURE;
    }

    MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
    IsisGetSemName (pContext, au1SemName);

    /*Create RBTree to store learnt hostname */
    pContext->HostNmeList = RBTreeCreateEmbeddedExtended (0, IsisHostNmeListCmp,
                                                          ((UINT1 *)
                                                           au1SemName));

    if (pContext->HostNmeList == NULL)
    {
        PANIC ((ISIS_LGST,
                "CTL <P> : Failed RBTree creation for Dynamic hostname\n"));
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlInitSysContext ()\n"));
        RBTreeDelete (pContext->RRDRouteRBTree);
        pContext->RRDRouteRBTree = NULL;
        return ISIS_FAILURE;
    }

    /*Register With Ext Module */
    /* Perform all registrations required with the external modules viz. IP, RTM and RM */
    if (IsisRegWithExtMod (pContext) == ISIS_FAILURE)
    {
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlInitSysContext ()\n"));
        RBTreeDelete (pContext->RRDRouteRBTree);
        pContext->RRDRouteRBTree = NULL;
        RBTreeDelete (pContext->HostNmeList);
        pContext->HostNmeList = NULL;
        return ISIS_FAILURE;
    }
    /* Initialize the Context 
     */

    pContext->u1QFlag = 0;

    pContext->u1OperState = ISIS_DOWN;
    pContext->pL1PurgeQueue = NULL;
    pContext->pL2PurgeQueue = NULL;

    pContext->SysL1Info.u1IsisDbFullState = ISIS_DB_SYNC;
    pContext->SysL1Info.LspTxQ.u2HashSize = 0;
    pContext->SysL1Info.LspTxQ.u2MaxMbrPerBucket = ISIS_MAX_HASH_MBRS;
    pContext->SysL1Info.LspTxQ.pHashBkts = NULL;
    pContext->SysL1Info.LspDB.u2HashSize = 0;
    pContext->SysL1Info.LspDB.u2MaxMbrPerBucket = ISIS_MAX_HASH_MBRS;
    pContext->SysL1Info.LspDB.pHashBkts = NULL;

    pContext->SysL2Info.u1IsisDbFullState = ISIS_DB_SYNC;
    pContext->SysL2Info.LspTxQ.u2HashSize = 0;
    pContext->SysL2Info.LspTxQ.u2MaxMbrPerBucket = ISIS_MAX_HASH_MBRS;
    pContext->SysL2Info.LspTxQ.pHashBkts = NULL;
    pContext->SysL2Info.LspDB.u2HashSize = 0;
    pContext->SysL2Info.LspDB.u2MaxMbrPerBucket = ISIS_MAX_HASH_MBRS;
    pContext->SysL2Info.LspDB.pHashBkts = NULL;
    pContext->IPRAInfo.u4NxtAutoIdx = 1;
    /* Init cache-pointer for IPRA table */
    pContext->pLastIPRAEntry = NULL;

    /* Copy the Default values 
     */

    pContext->SysConfigs.u1SysMPS = ISIS_DEF_MPS;
    pContext->SysConfigs.u1SysMaxAA = DEF_ISIS_AREA_ADDRESSES;
    pContext->SysConfigs.u4SysOrigL1LSPBufSize = ISIS_DEF_L1_LSPBUF_SIZE;

    pContext->SysConfigs.u4SysOrigL2LSPBufSize = ISIS_DEF_L2_LSPBUF_SIZE;
    pContext->SysConfigs.u1SysMetricSupp = ISIS_DEF_MET_SUPP_FLAG;
    pContext->SysConfigs.bSysL2ToL1Leak = ISIS_FALSE;
    pContext->SysConfigs.u1SysL1MetricStyle = ISIS_STYLE_NARROW_METRIC;
    pContext->SysConfigs.u1SysL2MetricStyle = ISIS_STYLE_NARROW_METRIC;

    pContext->SysActuals.u1SysAdminState = ISIS_STATE_OFF;
    pContext->SysActuals.u1SysExistState = ISIS_NOT_READY;
    pContext->SysActuals.bSysLSPIgnoreErr = ISIS_FALSE;
    pContext->SysActuals.bSysLogAdjChanges = ISIS_FALSE;
    pContext->SysActuals.bSysMaxAACheck = ISIS_TRUE;

    pContext->SysActuals.u1SysVer = ISIS_DEF_SYS_VER;
    pContext->SysActuals.u2SysMaxLSPGenInt = ISIS_MAX_LSP_GEN_INT;
    pContext->SysActuals.u2SysMinL1LSPGenInt = ISIS_MIN_L1GEN_INT;
    pContext->SysActuals.u2SysMinL2LSPGenInt = ISIS_MIN_L2GEN_INT;

    pContext->SysActuals.u2MinSPFSchTime = ISIS_MIN_SPF_SCH_TIME;
    pContext->SysActuals.u2MaxSPFSchTime = ISIS_MAX_SPF_SCH_TIME;
    pContext->SysActuals.u2MinLSPMark = ISIS_MIN_LSP_MARK;
    pContext->SysActuals.u2MaxLSPMark = ISIS_MAX_LSP_MARK;

    pContext->SysActuals.u2SysWaitTime = ISIS_DEF_WAIT_TIME;
    pContext->SysActuals.u1SysIDLen = ISIS_SYS_ID_LEN;
    pContext->SysActuals.u1SysSetOL = ISIS_LL_CLEAR_OVERLOAD;

    pContext->SysActuals.u1SysAuthSupp = ISIS_AUTH_BOTH_DISABLED;
    pContext->SysActuals.u1SysAreaAuthType = 1;
    pContext->SysActuals.u1SysDomainAuthType = 1;

    pContext->SysConfigs.u1SysType = ISIS_LEVEL12;
    pContext->u1QFlag |= ISIS_SYSTYPE_FLAG;

    pContext->SysActuals.u1SysL2SPFConsiders = 1;
    pContext->SysActuals.u1SysL1SPFConsiders = 1;
    pContext->SysActuals.u4SysMaxAge = ISIS_LSP_MAXAGE;
    pContext->SysActuals.u4SysRxLSPBufSize = ISIS_DEF_L1_LSPBUF_SIZE;
    pContext->TrapNotifyTable.u2BuffSize = ISIS_DEF_L1_LSPBUF_SIZE;
    pContext->TrapNotifyTable.u2LSPSize = ISIS_DEF_L1_LSPBUF_SIZE;

    pContext->CktTable.u4NextCktIndex = 1;
    pContext->EventTable.u1NextEvtIdx = 1;
    pContext->pDelDirEntry = NULL;
    pContext->RRDInfo.u1RRDImportType = ISIS_LEVEL2;

    pContext->SysConfigs.u4ESHelloRate = ISIS_DEF_ES_HELLORATE;
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        pContext->SysActuals.u1SysL1State = ISIS_STATE_ON;
    }

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        pContext->SysActuals.u1SysL2State = ISIS_STATE_ON;
    }
#ifdef ROUTEMAP_WANTED
    pContext->pDistributeInFilterRMap = NULL;
    pContext->pDistanceFilterRMap = NULL;
#endif
    pContext->u1Distance = ISIS_DEFAULT_PREFERENCE;

    pContext->u1IsisGRRestartSupport = ISIS_GR_RESTART_NONE;
    pContext->u1IsisGRRestartReason = ISIS_GR_UNKNOWN;    /*silvercreek */
    pContext->u1IsisGRRestarterState = ISIS_GR_NONE;
    pContext->u1IsisGRRestartMode = ISIS_GR_NONE;
    pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_NONE;
    pContext->u1IsisGRRestartStatus = ISIS_RESTART_NONE;
    pContext->u1IsisGRMaxT1RetryCount = 3;
    /*silvercreek */
    pContext->u1IsisGRT1TimerInterval = ISIS_GR_DEF_T1_INTERVAL;
    pContext->u2IsisGRT2TimerL1Interval = ISIS_GR_DEF_T2_INTERVAL;
    pContext->u2IsisGRT2TimerL2Interval = ISIS_GR_DEF_T2_INTERVAL;
    pContext->u2IsisGRT3TimerInterval = ISIS_GR_DEF_T3_INTERVAL;
    pContext->u1IsisGRHelperSupport = ISIS_GR_HELPER_NONE;    /*silvercreek */
    pContext->u1IsisGRShutState = ISIS_GR_NO_SHUT;
    pContext->u2IsisGRHelperTimeLimit = ISIS_GR_DEF_HELPER_TIME;
    /* Initialize the Timers
     */

    IsisCtrlInitTimers (pContext);

    u4CsrRestoreFlag = IssGetCsrRestoresFlag (ISIS_MODULE_ID);
    /*Planned Start */
    if (FlashFileExists (ISIS_GR_CONF) == ISS_SUCCESS)
    {
        /*Query RTM for checking Forwarding Plane Preserverd or Not */
        u1RtmFlag = (UINT1) RtmIsForwPlanPreserved (&RegnIdv4);
        u1Rtm6Flag = (UINT1) Rtm6IsForwPlanPreserved (&RegnIdv6);

        if ((u1RtmFlag == ISIS_SUCCESS) || (u1Rtm6Flag == ISIS_SUCCESS))    /*Planned */
        {
            /*Restoring the restart info */
            IsisGrRestoreRestartInfo (pContext);
            /*Planned Restarting */
        }
        else
        {
            /*Planned Starting */
            pContext->u1IsisGRRestartStatus = ISIS_STARTING_ROUTER;
            pContext->u1IsisGRRestartMode = ISIS_GR_RESTARTER;
            pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_INPROGRESS;
            pContext->u1IsisGRRestarterState = ISIS_GR_RESTART;
        }
    }
    else if ((u4CsrRestoreFlag == 1)
             && (FlashFileExists (ISIS_GR_CONF) == ISS_FAILURE))
    {
        /*UnPlanned Start */
        pContext->u1IsisGRRestartStatus = ISIS_STARTING_ROUTER;
        pContext->u1IsisGRRestartMode = ISIS_GR_RESTARTER;
        pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_INPROGRESS;
        pContext->u1IsisGRRestarterState = ISIS_GR_RESTART;
    }
    /*Normal Start */
    else
    {
        pContext->u1IsisGRRestartStatus = ISIS_RESTART_NONE;
        pContext->u1IsisGRRestartMode = ISIS_GR_NONE;
    }
    pContext->u1IsisMTSupport = ISIS_FALSE;
    pContext->u1MetricStyle = ISIS_STYLE_NARROW_METRIC;
    pContext->u1IsisBfdSupport = ISIS_BFD_DISABLE;
    pContext->u1IsisBfdAllIfStatus = ISIS_BFD_DISABLE;
    pContext->u1IsisDynHostNmeSupport = ISIS_DYNHOSTNME_DISABLE;
    pContext->u4HostNmeTotEntries = 0;
    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlInitSysContext ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisCtrlCopyConfigsToActuals()
 * Description : This routine  Copies the Configuration values to the 
 *               Actuals values when the row status is made active
 * Input(s)    : pContext - pointer to the context 
 * Output(s)   : None
 * Globals     : None refered nor modified
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisCtrlCopyConfigsToActuals (tIsisSysContext * pContext)
{
    CTP_EE ((ISIS_LGST,
             "CTL <X> : Entered IsisCtrlCopyConfigsToActuals () \n"));

    pContext->SysActuals.u1SysType = pContext->SysConfigs.u1SysType;
    pContext->SysActuals.u1SysMPS = pContext->SysConfigs.u1SysMPS;
    pContext->SysActuals.u1SysMaxAA = pContext->SysConfigs.u1SysMaxAA;
    pContext->SysActuals.u1SysMetricSupp = pContext->SysConfigs.u1SysMetricSupp;

    pContext->SysActuals.u4ESHelloRate = pContext->SysConfigs.u4ESHelloRate;

    MEMCPY (pContext->SysActuals.au1SysID, pContext->SysConfigs.au1SysID,
            ISIS_SYS_ID_LEN);

    pContext->SysActuals.u4SysOrigL1LSPBufSize =
        pContext->SysConfigs.u4SysOrigL1LSPBufSize;
    pContext->SysActuals.u4SysOrigL2LSPBufSize =
        pContext->SysConfigs.u4SysOrigL2LSPBufSize;

    CTP_EE ((ISIS_LGST,
             "CTL <X> : Exiting IsisCtrlCopyConfigsToActuals () \n"));
}

/******************************************************************************
 * Function    : IsisCtrlAllocShortPath ()
 * Description : This routine allocates memory for Shortest Path Database and
 *               Previous Shortest Path Database, based on the Level of System
 * Input(s)    : pContext - Pointer to the context 
 * Output(s)   : None
 * Globals     : Not Refered or Modified
 * Returns     : ISIS_SUCCESS, If Memory Allocation is Successful 
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisCtrlAllocShortPath (tIsisSysContext * pContext)
{
    UINT1               u1MtIndex = 0;
    UINT1               au1SemName[OSIX_NAME_LEN + 4];

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlAllocShortPath ()\n"));

    if (pContext->Tent == NULL)
    {
        MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
        IsisGetSemName (pContext, au1SemName);

        pContext->Tent =
            RBTreeCreateEmbeddedExtended (FSAP_OFFSETOF (tIsisSPTNode, RbNode),
                                          IsisUtlSPTNodeCmp,
                                          ((UINT1 *) au1SemName));

        if (pContext->Tent == NULL)
        {
            CTP_PT ((ISIS_LGST, "CTL <E> : Cannot create RBTree for Tent "
                     "Database \n"));
            CTP_EE ((ISIS_LGST,
                     "CTL <X> : Exiting IsisCtrlAllocShortPath ()\n"));

            return ISIS_FAILURE;
        }
    }

    if (pContext->OSIPath == NULL)
    {
        MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
        IsisGetSemName (pContext, au1SemName);

        pContext->OSIPath =
            RBTreeCreateEmbeddedExtended (FSAP_OFFSETOF (tIsisSPTNode, RbNode),
                                          IsisUtlSPTNodeCmp,
                                          ((UINT1 *) au1SemName));
        if (pContext->OSIPath == NULL)
        {
            RBTreeDestroy (pContext->Tent, NULL, 0);
            pContext->Tent = NULL;
            CTP_PT ((ISIS_LGST, "CTL <E> : Cannot create RBTree for OSIPath "
                     "Database \n"));
            CTP_EE ((ISIS_LGST,
                     "CTL <X> : Exiting IsisCtrlAllocShortPath ()\n"));
            return ISIS_FAILURE;
        }
    }

    for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
    {
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            if (pContext->SysL1Info.PrevShortPath[u1MtIndex] == NULL)
            {
                MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
                IsisGetSemName (pContext, au1SemName);

                pContext->SysL1Info.PrevShortPath[u1MtIndex] =
                    RBTreeCreateEmbeddedExtended (FSAP_OFFSETOF
                                                  (tIsisSPTNode, RbNode),
                                                  IsisUtlSPTNodeCmp,
                                                  ((UINT1 *) au1SemName));

                if (pContext->SysL1Info.PrevShortPath[u1MtIndex] == NULL)
                {
                    RBTreeDestroy (pContext->Tent, NULL, 0);
                    RBTreeDestroy (pContext->OSIPath, NULL, 0);
                    pContext->Tent = NULL;
                    pContext->OSIPath = NULL;
                    PANIC ((ISIS_LGST,
                            "CTL <P> : L1 Prev Short PATH DB Not Initialized\n"));
                    CTP_EE ((ISIS_LGST,
                             "CTL <X> : Exiting IsisCtrlAllocShortPath ()\n"));
                    return ISIS_FAILURE;
                }
            }

            if (pContext->SysL1Info.ShortPath[u1MtIndex] == NULL)
            {
                MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
                IsisGetSemName (pContext, au1SemName);

                pContext->SysL1Info.ShortPath[u1MtIndex] =
                    RBTreeCreateEmbeddedExtended (FSAP_OFFSETOF
                                                  (tIsisSPTNode, RbNode),
                                                  IsisUtlSPTNodeCmp,
                                                  ((UINT1 *) au1SemName));
                if (pContext->SysL1Info.ShortPath[u1MtIndex] == NULL)
                {
                    RBTreeDestroy (pContext->Tent, NULL, 0);
                    RBTreeDestroy (pContext->OSIPath, NULL, 0);
                    RBTreeDestroy (pContext->SysL1Info.PrevShortPath[u1MtIndex],
                                   NULL, 0);
                    pContext->Tent = NULL;
                    pContext->OSIPath = NULL;
                    pContext->SysL1Info.PrevShortPath[u1MtIndex] = NULL;
                    PANIC ((ISIS_LGST,
                            "CTL <P> : L1 Shortest PATH DB Not Initialized\n"));
                    CTP_EE ((ISIS_LGST,
                             "CTL <X> : Exiting IsisCtrlAllocShortPath ()\n"));
                    return ISIS_FAILURE;
                }
            }

            if (pContext->SysActuals.u1SysType == ISIS_LEVEL1)
            {
                if (pContext->SysL2Info.PrevShortPath[u1MtIndex] != NULL)
                {
                    RBTreeDestroy (pContext->SysL2Info.PrevShortPath[u1MtIndex],
                                   NULL, 0);
                    pContext->SysL2Info.PrevShortPath[u1MtIndex] = NULL;
                }
                if (pContext->SysL2Info.ShortPath[u1MtIndex] != NULL)
                {
                    RBTreeDestroy (pContext->SysL2Info.ShortPath[u1MtIndex],
                                   NULL, 0);
                    pContext->SysL2Info.ShortPath[u1MtIndex] = NULL;
                }
            }
        }

        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            if (pContext->SysL2Info.PrevShortPath[u1MtIndex] == NULL)
            {
                MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
                IsisGetSemName (pContext, au1SemName);

                pContext->SysL2Info.PrevShortPath[u1MtIndex] =
                    RBTreeCreateEmbeddedExtended (FSAP_OFFSETOF
                                                  (tIsisSPTNode, RbNode),
                                                  IsisUtlSPTNodeCmp,
                                                  ((UINT1 *) au1SemName));
                if (pContext->SysL2Info.PrevShortPath[u1MtIndex] == NULL)
                {
                    RBTreeDestroy (pContext->Tent, NULL, 0);
                    RBTreeDestroy (pContext->OSIPath, NULL, 0);
                    pContext->Tent = NULL;
                    pContext->OSIPath = NULL;

                    if (pContext->SysActuals.u1SysType == ISIS_LEVEL12)
                    {
                        RBTreeDestroy (pContext->SysL1Info.
                                       PrevShortPath[u1MtIndex], NULL, 0);
                        RBTreeDestroy (pContext->SysL1Info.ShortPath[u1MtIndex],
                                       NULL, 0);
                        pContext->SysL1Info.PrevShortPath[u1MtIndex] = NULL;
                        pContext->SysL1Info.ShortPath[u1MtIndex] = NULL;
                    }
                    PANIC ((ISIS_LGST,
                            "CTL <P> : L2 Prev Short PATH DB Not Initialized\n"));
                    CTP_EE ((ISIS_LGST,
                             "CTL <X> : Exiting IsisCtrlAllocShortPath ()\n"));
                    return ISIS_FAILURE;
                }
            }

            if (pContext->SysL2Info.ShortPath[u1MtIndex] == NULL)
            {
                MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
                IsisGetSemName (pContext, au1SemName);

                pContext->SysL2Info.ShortPath[u1MtIndex] =
                    RBTreeCreateEmbeddedExtended (FSAP_OFFSETOF
                                                  (tIsisSPTNode, RbNode),
                                                  IsisUtlSPTNodeCmp,
                                                  ((UINT1 *) au1SemName));
                if (pContext->SysL2Info.ShortPath[u1MtIndex] == NULL)
                {
                    RBTreeDestroy (pContext->Tent, NULL, 0);
                    RBTreeDestroy (pContext->OSIPath, NULL, 0);
                    RBTreeDestroy (pContext->SysL2Info.PrevShortPath[u1MtIndex],
                                   NULL, 0);
                    pContext->Tent = NULL;
                    pContext->OSIPath = NULL;
                    pContext->SysL2Info.PrevShortPath[u1MtIndex] = NULL;

                    if (pContext->SysActuals.u1SysType == ISIS_LEVEL12)
                    {
                        RBTreeDestroy (pContext->SysL1Info.
                                       PrevShortPath[u1MtIndex], NULL, 0);
                        RBTreeDestroy (pContext->SysL1Info.ShortPath[u1MtIndex],
                                       NULL, 0);
                        pContext->SysL1Info.PrevShortPath[u1MtIndex] = NULL;
                        pContext->SysL1Info.ShortPath[u1MtIndex] = NULL;
                    }
                    PANIC ((ISIS_LGST,
                            "CTL <P> : L2 Shortest PATH DB Not Initialized\n"));
                    CTP_EE ((ISIS_LGST,
                             "CTL <X> : Exiting IsisCtrlAllocShortPath ()\n"));
                    return ISIS_FAILURE;
                }
            }

            if (pContext->SysActuals.u1SysType == ISIS_LEVEL2)
            {
                if (pContext->SysL1Info.PrevShortPath[u1MtIndex] != NULL)
                {
                    RBTreeDestroy (pContext->SysL1Info.PrevShortPath[u1MtIndex],
                                   NULL, 0);
                    pContext->SysL1Info.PrevShortPath[u1MtIndex] = NULL;
                }
                if (pContext->SysL1Info.ShortPath[u1MtIndex] != NULL)
                {
                    RBTreeDestroy (pContext->SysL1Info.ShortPath[u1MtIndex],
                                   NULL, 0);
                    pContext->SysL1Info.ShortPath[u1MtIndex] = NULL;
                }
            }
        }
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlAllocShortPath ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisCtrlGetSysContext ()
 * Description : This routine fetches the System Context for the given 
 *               Instance Index from the Global Context Table
 * Input(s)    : u4SysInstIdx - Instance Index variable whose Context is to
 *                              be fetched
 * Output(s)   : pContext     - Pointer to the System Context Table fetched
 * Globals     : gpIsisInstances
 * Returns     : ISIS_SUCCESS - When the System Context record is present
 *               ISIS_FAILURE - Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisCtrlGetSysContext (UINT4 u4SysInstIdx, tIsisSysContext ** pContext)
{
    INT4                i4RetVal = ISIS_FAILURE;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGetContext () \n"));

    (*pContext) = gpIsisInstances;
    while ((*pContext) != NULL)
    {
        if ((*pContext)->u4SysInstIdx == u4SysInstIdx)
        {
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        (*pContext) = (*pContext)->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlGetContext ()\n"));

    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisCtrlAddSysContext ()
 * Description : This routine adds the System Context Record to global pool of 
 *               instances 
 * Input(s)    : pContext  - Pointer to System Contex that is to be added
 *                           to the Database
 * Outputs(s)  : None
 * Globals     : gpIsisInstances Modified
 * Returns     : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisCtrlAddSysContext (tIsisSysContext * pContext)
{
    tIsisSysContext    *pTravContext = NULL;
    tIsisSysContext    *pPrevContext = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlAddSysContext () \n"));

    /*Sorting the context-id in Ascending order */
    pTravContext = ISIS_GET_CONTEXT ();

    if (pTravContext == NULL)
    {
        ISIS_PUT_CONTEXT (pContext);    /*first node */
    }
    else
    {
        while ((pTravContext != NULL)
               && (pContext->u4SysInstIdx > pTravContext->u4SysInstIdx))
        {
            pPrevContext = pTravContext;
            pTravContext = pTravContext->pNext;
        }

        if (pPrevContext == NULL)
        {
            pContext->pNext = pTravContext;
            ISIS_PUT_CONTEXT (pContext);
        }
        else
        {
            pPrevContext->pNext = pContext;
            pContext->pNext = pTravContext;
        }
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlAddSysContext ()\n"));
}

/******************************************************************************
 * Function    : IsisCtrlDelSysContext ()
 * Description : This routine deletes the System Context Record from
 *               the global pool of System Instances. It also releases all
 *               the information associated with the Context from the
 *               database
 * Input(s)    : u4SysInstIdx - Instance Index of the context that is to 
 *                              be deleted from the Database
 * Outputs(s)  : None
 * Globals     : gpIsisInstances Modified
 * Returns     : ISIS_SUCCESS, if the Context record is successfully removed
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisCtrlDelSysContext (UINT4 u4SysInstIdx)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pPrev = NULL;
    tIsisSysContext    *pTrav = NULL;
    UINT1               u1IsisGRRestarterState = 0;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlDelSysContext ()\n"));

    /* The Macro ISIS_GET_CONTEXT () gets the head of the Context pool
     */

    CTP_PT ((ISIS_LGST, "CTL <T> : Deleting Instance [ %u ]\n", u4SysInstIdx));

    pTrav = ISIS_GET_CONTEXT ();

    while (pTrav != NULL)
    {
        if (pTrav->u4SysInstIdx == u4SysInstIdx)
        {
            u1IsisGRRestarterState = pTrav->u1IsisGRRestarterState;
            CTP_PT ((ISIS_LGST, "CTL <T> : Matching Instance Found\n"));
            if (pPrev == NULL)
            {
                /* The context to be deleted is the very first record in the
                 * chain of records. Hence advance the head. ISIS_PUT_CONTEXT ()
                 * will assign the specified argument to the head of the chain
                 */

                pPrev = pTrav->pNext;
                ISIS_PUT_CONTEXT (pPrev);
            }
            else
            {
                /* The context record to be deleted is not the first one in the
                 * chain
                 */

                pPrev->pNext = pTrav->pNext;
            }

            pTrav->pNext = NULL;
            /*DeRegister From External module */
            if (pTrav->RRDInfo.au1RRDRMapName[0] != '\0')
            {
                MEMSET (pTrav->RRDInfo.au1RRDRMapName, '\0',
                        STRLEN (pTrav->RRDInfo.au1RRDRMapName));
            }
            IsisSendingMessageToRRDQueue (pTrav, ISIS_IMPORT_ALL,
                                          RTM_REDISTRIBUTE_DISABLE_MESSAGE);
            IsisSendingMessageToRRDQueue6 (pTrav, ISIS_IMPORT_ALL,
                                           RTM_REDISTRIBUTE_DISABLE_MESSAGE);

            IsisTmrStopTimer (&(pTrav->SysTimers.SysRestartT3Timer));
            IsisResetInst (pTrav);
            IsisDeRegFromExtMod (u1IsisGRRestarterState, u4SysInstIdx);
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        pPrev = pTrav;
        pTrav = pTrav->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlDelSysContext ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisCtrlGetMAA ()
 * Description : This routine fetches the Manual Area Address Record from
 *               the database for the given indices 
 * Input(s)    : pContext  - Pointer to System Context
 *               pMAA      - Pointer to the Manual Area address whose record
 *                           is to be obtained
 * Output(s)   : pMAARec   - Pointer to the Manual Area Address record that
 *                           is fetched from the System Table
 * Globals     : Not Refered or Modified
 * Returns     : ISIS_SUCCESS, if the Manual Area Address record is
 *                             successfully fetched
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisCtrlGetMAA (tIsisSysContext * pContext, UINT1 *pMAA, UINT1 u1Len,
                tIsisMAAEntry ** pMAARec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisMAAEntry      *pTravMAARec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGetMAA ()\n"));

    pTravMAARec = pContext->ManAddrTable.pMAARec;

    while (pTravMAARec != NULL)
    {
        if (pTravMAARec->ManAreaAddr.u1Length < ISIS_AREA_ADDR_LEN)
        {
            if ((pTravMAARec->ManAreaAddr.u1Length == u1Len)
                && (MEMCMP (pTravMAARec->ManAreaAddr.au1AreaAddr, pMAA,
                            pTravMAARec->ManAreaAddr.u1Length) == 0))
            {
                *pMAARec = pTravMAARec;
                i4RetVal = ISIS_SUCCESS;
                break;
            }
        }
        pTravMAARec = pTravMAARec->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlGetMAA ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisCtrlAddMAA ()
 * Description : This routine adds the given Manual Area Address Record to
 *               the Manual Area Address Table
 * Input(s)    : pContext  - Pointer to System Context
 *               pMAARec   - Pointer to the Manual Area Address table to be
 *                           added to the System Table
 * Outputs(s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlAddMAA (tIsisSysContext * pContext, tIsisMAAEntry * pMAARec)
{
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlAddMAA ()\n"));

    pMAARec->pNext = pContext->ManAddrTable.pMAARec;
    pContext->ManAddrTable.pMAARec = pMAARec;
    pContext->ManAddrTable.u2NumEntries++;

    /* Updating the Area Address Table with the Manual Area Address added
     */

    if (pContext->u1OperState == ISIS_UP)
    {
        IsisUpdAddAA (pContext, pMAARec->ManAreaAddr.au1AreaAddr,
                      pMAARec->ManAreaAddr.u1Length);
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlAddMAA ()\n"));
}

/******************************************************************************
 * Function    : IsisCtrlDelMAA ()
 * Description : This routine deletes the Manual Area Address Record from the 
 *               database 
 * Input(s)    : pContext  - Pointer to System Context
 *               pau1MAA   - Pointer to the Manual area address to be deleted
 * Outputs(s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if the Manual Area Address record is
 *                             removed successfully
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisCtrlDelMAA (tIsisSysContext * pContext, UINT1 u1Len, UINT1 *pau1MAA)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisMAAEntry      *pTrav = NULL;
    tIsisMAAEntry      *pPrev = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlDelMAA ()\n"));

    pTrav = pContext->ManAddrTable.pMAARec;

    while (pTrav != NULL)
    {
        if (pTrav->ManAreaAddr.u1Length < ISIS_AREA_ADDR_LEN)
        {

            if ((u1Len == pTrav->ManAreaAddr.u1Length)
                && (MEMCMP (pTrav->ManAreaAddr.au1AreaAddr, pau1MAA, u1Len) ==
                    0))
            {
                /* Found a matching MAA record
                 */

                if (pPrev == NULL)
                {
                    /* Record to be deleted is the first one in the list
                     */

                    pPrev = pTrav->pNext;
                    pContext->ManAddrTable.pMAARec = pPrev;
                }
                else
                {
                    /* Record to be deleted is the not the first one in the list
                     */

                    pPrev->pNext = pTrav->pNext;
                }

                pTrav->pNext = NULL;
                pContext->ManAddrTable.u2NumEntries--;

                ISIS_MEM_FREE (ISIS_BUF_MAAT, (UINT1 *) pTrav);
                i4RetVal = ISIS_SUCCESS;
                break;
            }
        }
        pPrev = pTrav;
        pTrav = pTrav->pNext;
    }

    if ((pContext->u1OperState == ISIS_UP) && (i4RetVal == ISIS_SUCCESS))
    {
        /* Area address table will not be populated till the Context is
         * Operationally UP
         */

        IsisUpdDelAA (pContext, pau1MAA, u1Len);
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlDelMAA ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisCtrlGetSA ()
 * Description : This routine fetches the Summary Area Address Record from
 *               the database 
 * Input(s)    : pContext    - Pointer to System Context
 *               pau1SA      - Pointer to the Summary address whose 
 *                             record is to be obtained
 *               u1SAType    - Type of the Summary Address
 *               u1PrefixLen - Prefix Length of the Summary Address passed
 * Output(s)   : pSARec      - Pointer to the Summary Address record 
 *                             that is fetched from the System Context
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if the Summary Address record is fetched
 *                             successfully
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisCtrlGetSA (tIsisSysContext * pContext, UINT1 *pau1SA, UINT1 u1SAType,
               UINT1 u1PrefixLen, tIsisSAEntry ** pSARec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1AddrLen;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGetSA ()\n"));

    *pSARec = pContext->SummAddrTable.pSAEntry;

    while (*pSARec != NULL)
    {
        if (u1SAType == ISIS_ADDR_IPV4)
        {
            u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
        }
        else
        {
            u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
        }
        if ((MEMCMP ((*pSARec)->au1SummAddr, pau1SA, u1AddrLen) == 0)
            && ((*pSARec)->u1AddrType == u1SAType)
            && ((*pSARec)->u1PrefixLen == u1PrefixLen))
        {
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        *pSARec = (*pSARec)->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlGetSA ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisCtrlAddSA ()
 * Description : This routine adds the Summary Address Record to the database 
 * Input(s)    : pContext - Pointer to System Context
 *               pSAEntry - Pointer to the Summary Address Record that 
 *                          is to be added to the System Table.
 * Output(s)   : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlAddSA (tIsisSysContext * pContext, tIsisSAEntry * pSAEntry)
{
    tIsisSAEntry       *pTempSAEntry = NULL;
    tIsisSAEntry       *pPrevSAEntry = NULL;
    UINT1               u1AddrLen = 0;
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlAddSA ()\n"));

    pTempSAEntry = pContext->SummAddrTable.pSAEntry;

    if (pTempSAEntry == NULL)
    {
        pSAEntry->pNext = pContext->SummAddrTable.pSAEntry;
        pContext->SummAddrTable.pSAEntry = pSAEntry;
        pContext->SummAddrTable.u2NumEntries++;
    }
    else
    {
        while (pTempSAEntry)
        {
            if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
            {
                u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
            }
            else
            {
                u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
            }
            if (pSAEntry->u1AddrType > pTempSAEntry->u1AddrType)
            {
                /* Entry to be added is Greater than the Current Entry (pTempSAEntry) */
                pPrevSAEntry = pTempSAEntry;
                pTempSAEntry = pTempSAEntry->pNext;
                continue;
            }
            else if (pSAEntry->u1AddrType < pTempSAEntry->u1AddrType)
            {
                break;
            }
            if (MEMCMP
                (pSAEntry->au1SummAddr, pTempSAEntry->au1SummAddr,
                 u1AddrLen) > 0)
            {
                /* Entry to be added is Greater than the Current Entry (pTempSAEntry) */
                pPrevSAEntry = pTempSAEntry;
                pTempSAEntry = pTempSAEntry->pNext;
                continue;
            }
            else if (MEMCMP
                     (pSAEntry->au1SummAddr, pTempSAEntry->au1SummAddr,
                      u1AddrLen) < 0)
            {
                break;
            }

            if (pSAEntry->u1PrefixLen > pTempSAEntry->u1PrefixLen)
            {
                /* Entry to be added is Greater than the Current Entry (pTempSAEntry) */
                pPrevSAEntry = pTempSAEntry;
                pTempSAEntry = pTempSAEntry->pNext;
                continue;
            }
            else if (pSAEntry->u1PrefixLen < pTempSAEntry->u1PrefixLen)
            {
                break;
            }
            else
            {
                /* Duplicate Entry should not be added */
                return;
            }
        }
        if (pPrevSAEntry != NULL)
        {
            pPrevSAEntry->pNext = pSAEntry;
            pSAEntry->pNext = pTempSAEntry;
        }
        else
        {
            pSAEntry->pNext = pContext->SummAddrTable.pSAEntry;
            pContext->SummAddrTable.pSAEntry = pSAEntry;
        }
        pContext->SummAddrTable.u2NumEntries++;
    }
    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlAddSA ()\n"));
}

/*******************************************************************************
 * Function    : IsisCtrlDelSA ()
 * Description : This routine deletes the Summary Address Record from the System
 *               database 
 * Input(s)    : pContext     - Pointer to System Context
 *               pau1SummAddr - Pointer to the Summary address whose record is 
 *                              to be  deleted. 
 *               u1SAType     - Type of the Summary Address
 *               u1PrefixLen  - Prefix Length of the Summary Address 
 * Output(s)   : None
 * Globals     : Not referred or modified
 * Returns     : ISIS_SUCCESS, if the Summary Address record is successfully
 *                             deleted 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisCtrlDelSA (tIsisSysContext * pContext, UINT1 *pau1SummAddr,
               UINT1 u1AddrType, UINT1 u1PrefixLen)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSAEntry       *pPrev = NULL;
    tIsisSAEntry       *pTrav = NULL;
    UINT1               u1AddrLen;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlDelSA ()\n"));

    pTrav = pContext->SummAddrTable.pSAEntry;

    while (pTrav != NULL)
    {
        if (u1AddrType == ISIS_ADDR_IPV4)
        {
            u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
        }
        else
        {
            u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
        }
        if ((MEMCMP (pTrav->au1SummAddr, pau1SummAddr,
                     u1AddrLen) == 0)
            && (pTrav->u1PrefixLen == u1PrefixLen)
            && (pTrav->u1AddrType == u1AddrType))
        {
            if (pPrev == NULL)
            {
                /* Record to be deleted is the first one in the list
                 */

                pPrev = pTrav->pNext;
                pContext->SummAddrTable.pSAEntry = pPrev;
            }
            else
            {
                /* Record to be deleted is not the first one in the list
                 */

                pPrev->pNext = pTrav->pNext;
            }

            pTrav->pNext = NULL;
            pContext->SummAddrTable.u2NumEntries--;

            ISIS_MEM_FREE (ISIS_BUF_SATE, (UINT1 *) pTrav);

            pTrav = NULL;
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        pPrev = pTrav;
        pTrav = pTrav->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlDelSA ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisCtrlUpdtSA ()
 * Description : This routine updates the SAA record with the new values.
 *               Only fields that are modifiable are Admin State and Exist
 *               State. This routine is called while Updating the Summary
 *               Address Record in the Standby Node
 * Input(s)    : pExistSARec - Pointer to the existing Summary Address
 *                             Record whose values are to be updated
 *               pSARec      - Pointer to the new Record that has the
 *                             modified values 
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlUpdtSA (tIsisSAEntry * pExistSARec, tIsisSAEntry * pSARec)
{
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlUpdtSA ()\n"));

    pExistSARec->u1AdminState = pSARec->u1AdminState;
    pExistSARec->u1ExistState = pSARec->u1ExistState;
    MEMCPY (pExistSARec->Metric, pSARec->Metric, (sizeof (tIsisMetric)));
    pExistSARec->u4FullMetric = pSARec->u4FullMetric;
    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlUpdtSA ()\n"));
}

/*******************************************************************************
 * Function       : IsisCtrlUpdtSAUsageCnt ()
 * Description    : This routine updates the SA Usage count when summarising 
 *                  the IPRA Address
 * Input(s)       : pContext    - Pointer to system context
 *                  pSumAddr    - Pointer to Summary address
 *                  u1PrefixLen - Prefix Length 
 *                  u1Count     - Value that has to be updated in the Summary
 *                                Address Usage Count
 *                  u1Level     - Level of the Summary Address Table
 *                  u1Cmd       - Command to do with the 'u2Count'
 *                                Possible values are 
 *                                 -  ISIS_CMD_ADD - u2Count value is added to
 *                                    the Usage Count of the Summary Address
 *                                    Table
 *                                 -  ISIS_CMD_DELETE - u2Count value is
 *                                    subtracted from the Usage Count from the
 *                                    Summary Address Table
 *                                 -  ISIS_CMD_MODIFY - u2Count value is updated
 *                                    in the Summary Address Table
 * Output(s)      : None
 * Globals        : Not Referred or Modified
 * Returns        : ISIS_SUCCESS if the usage count is updated or ISIS_FAILURE
 ******************************************************************************/

PUBLIC INT1
IsisCtrlUpdtSAUsageCnt (tIsisSysContext * pContext, UINT1 *pSAAddr,
                        UINT1 u1PrefLen, UINT2 u2Count, UINT1 u1Level,
                        UINT1 u1Cmd)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSAEntry       *pSARec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlUpdtSAUsageCnt ()\n"));

    i4RetVal = IsisCtrlGetSA (pContext, pSAAddr, ISIS_IPV4, u1PrefLen, &pSARec);
    if (i4RetVal == ISIS_FAILURE)
    {
        CTP_PT ((ISIS_LGST, "CTL <E> : No Matching SA Found !!! \n"));
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlUpdtSAUsageCnt ()\n"));

        return (ISIS_FAILURE);
    }
    if (u1Cmd == ISIS_CMD_ADD)
    {
        (u1Level == ISIS_LEVEL1) ? (pSARec->u2L1UsageCnt += u2Count)
            : (pSARec->u2L2UsageCnt += u2Count);
    }
    else if (u1Cmd == ISIS_CMD_DELETE)
    {
        (u1Level == ISIS_LEVEL1) ? (pSARec->u2L1UsageCnt -= u2Count)
            : (pSARec->u2L2UsageCnt -= u2Count);
    }
    else if (u1Cmd == ISIS_CMD_MODIFY)
    {
        (u1Level == ISIS_LEVEL1) ? (pSARec->u2L1UsageCnt = u2Count)
            : (pSARec->u2L2UsageCnt = u2Count);
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlUpdtSAUsageCnt ()\n"));
    return (ISIS_SUCCESS);
}

/*******************************************************************************
 * Function    : IsisCtrlUpdtIPIf ()
 * Description : This routine updates the values of the existing IP Interface 
 *               Address record 'pExistIPIfRec' with the values retrieved from 
 *               the new record 'pIPIfRec'
 * Input(s)    : pIPIfRec      - Pointer to the IP I/f Address Record that has 
 *                               the updated values
 *               pExistIPIfRec - Pointer to the existing IP I/f Address
 *                               Record whose values are to be updated
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlUpdtIPIf (tIsisIPIfAddr * pIPIfRec, tIsisIPIfAddr * pExistIPIfRec)
{

    pExistIPIfRec->u4CktIfIdx = pIPIfRec->u4CktIfIdx;
    pExistIPIfRec->u4CktIfSubIdx = pIPIfRec->u4CktIfSubIdx;
    if (pIPIfRec->u1AddrType == ISIS_ADDR_IPV4)
    {
        MEMCPY (pExistIPIfRec->au1IPAddr, pIPIfRec->au1IPAddr,
                ISIS_MAX_IPV4_ADDR_LEN);
    }
    else
    {
        MEMCPY (pExistIPIfRec->au1IPAddr, pIPIfRec->au1IPAddr,
                ISIS_MAX_IPV6_ADDR_LEN);
    }
    pExistIPIfRec->u1AddrType = pIPIfRec->u1AddrType;
    pExistIPIfRec->u1ExistState = pIPIfRec->u1ExistState;

}

/*******************************************************************************
 * Function    : IsisCtrlGetIPIfWithCktIdx ()
 * Description : This routine fetches the First IP Interface Address Record 
 *               from the database for the given Circuit Index.
 * Input(s)    : pContext      - Pointer to System Context
 *               u4CktIdx      - Circuit  index 
 *               u1IPIfType    - IPInterface Type 
 * Output(s)   : pIPIfRec      - Pointer to the IP Interface Address record that
 *                               is fetched from the System Table
 * Globals     : None
 * Returns     : ISIS_SUCCESS. If IP If Address record is fetched successfully
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisCtrlGetIPIfWithCktIdx (tIsisSysContext * pContext, UINT4 u4CktIdx,
                           UINT1 u1IPIfAType, tIsisIPIfAddr ** pIPIfRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisCktEntry      *pCktRec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGetIPIfWithCktIdx ()\n"));

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);
    if (i4RetVal != ISIS_SUCCESS)
    {
        CTP_PT ((ISIS_LGST, "CTL <E> : No Matching CktRec Found !!! \n"));
        CTP_EE ((ISIS_LGST,
                 "CTL <X> : Existing IsisCtrlGetIPIfWithCktIdx ()\n"));
        return i4RetVal;
    }

    i4RetVal = ISIS_FAILURE;
    *pIPIfRec = pContext->IPIfTable.pIPIfAddr;

    while (*pIPIfRec != NULL)
    {
        if (((*pIPIfRec)->u4CktIfIdx == pCktRec->u4CktIfIdx)
            && ((*pIPIfRec)->u4CktIfSubIdx == pCktRec->u4CktIfSubIdx)
            && ((*pIPIfRec)->u1AddrType == u1IPIfAType))
        {
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        *pIPIfRec = (*pIPIfRec)->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlGetIPIf ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisCtrlGetIPIf ()
 * Description : This routine fetches the IP Interface Address Record from 
 *               the database for the given indices
 * Input(s)    : pContext      - Pointer to System Context
 *               u4CktIfIdx    - Circuit Interface index 
 *               u4CktIfSubIdx - Circuit Interface Sub Index
 *               pu1IPIfAddr   - Pointer to the IP I/f Address
 *               u1IPIfType    - IPInterface Type 
 *               u1PrefixLen   - IPIf Prefix Len
 * Output(s)   : pIPIfRec      - Pointer to the IP Interface Address record that
 *                               is fetched from the System Table
 * Globals     : None
 * Returns     : ISIS_SUCCESS. If IP If Address record is fetched successfully
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisCtrlGetIPIf (tIsisSysContext * pContext, UINT4 u4CktIfIdx,
                 UINT4 u4CktIfSubIdx, UINT1 *pu1IPIfAddr, UINT1 u1IPIfAType,
                 tIsisIPIfAddr ** pIPIfRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1AddrLen;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGetIPIf ()\n"));

    *pIPIfRec = pContext->IPIfTable.pIPIfAddr;
    if (u1IPIfAType == ISIS_ADDR_IPV6)
    {
        u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
    }
    else
    {
        u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
    }
    while (*pIPIfRec != NULL)
    {
        if (((*pIPIfRec)->u4CktIfIdx == u4CktIfIdx)
            && ((*pIPIfRec)->u4CktIfSubIdx == u4CktIfSubIdx)
            && (MEMCMP ((*pIPIfRec)->au1IPAddr, pu1IPIfAddr,
                        u1AddrLen) == 0)
            && ((*pIPIfRec)->u1AddrType == u1IPIfAType))
        {
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        *pIPIfRec = (*pIPIfRec)->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlGetIPIf ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisCtrlAddIPIf ()
 * Description : This routine adds the IP Interface Address Record to the IP I/f
 *               Address Database
 * Input(s)    : pContext  - Pointer to System Context
 *               pIPIfAddr - Pointer to the IPIf Address to be added to the 
 *                           Database
 * Output(s)   : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlAddIPIf (tIsisSysContext * pContext, tIsisIPIfAddr * pIPIfAddr)
{

    tIsisIPIfAddr      *pTempIPIfAddr = pContext->IPIfTable.pIPIfAddr;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlAddIPIf ()\n"));

    pIPIfAddr->pNext = NULL;
    if (pIPIfAddr->u1SecondaryFlag == ISIS_TRUE)
    {
        while (pTempIPIfAddr != NULL)
        {
            if ((pTempIPIfAddr->u4CktIfIdx == pIPIfAddr->u4CktIfIdx)
                && (pTempIPIfAddr->u1AddrType == pIPIfAddr->u1AddrType))
            {
                pIPIfAddr->pNext = pTempIPIfAddr->pNext;
                pTempIPIfAddr->pNext = pIPIfAddr;
                pContext->IPIfTable.u2NumEntries++;
                break;
            }
            pTempIPIfAddr = pTempIPIfAddr->pNext;
        }
    }
    else
    {

        pIPIfAddr->pNext = pContext->IPIfTable.pIPIfAddr;
        pContext->IPIfTable.pIPIfAddr = pIPIfAddr;
        pContext->IPIfTable.u2NumEntries++;
    }
    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlAddIPIf ()\n"));
}

/*******************************************************************************
 * Function    : IsisCtrlDelIPIf ()
 * Description : This routine deletes the IP Interface Address Record from
 *               the IP I/f Address database 
 * Input(s)    : pContext      - Pointer to System Context
 *               u4CktIfIdx    - Circuit If Index corresponding to the IP I/f
 *               u4CktIfSubIdx - Circuit If SubIndex corresponding to the IP I/f
 *               pu1IpAddr     - IP Address of the Interface that is to be
 *                               deleted
 *               u1AddrType    - IP Address type             
 * Output(s)   : None
 * Globals     : Not referred or modified
 * Returns     : ISIS_SUCCESS, If the IP I/f Address record is successfully
 *                             deleted 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisCtrlDelIPIf (tIsisSysContext * pContext, UINT4 u4CktIfIdx,
                 UINT4 u4CktIfSubIdx, UINT1 *pu1IpAddr, UINT1 u1AddrType)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisIPIfAddr      *pPrev = NULL;
    tIsisIPIfAddr      *pTrav = NULL;
    UINT1               u1Length;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlDelIPIf ()\n"));

    pTrav = pContext->IPIfTable.pIPIfAddr;

    while (pTrav != NULL)
    {
        if (u1AddrType == ISIS_ADDR_IPV4)
        {
            u1Length = ISIS_MAX_IPV4_ADDR_LEN;
        }
        else
        {
            u1Length = ISIS_MAX_IPV6_ADDR_LEN;
        }
        if ((MEMCMP (pTrav->au1IPAddr, pu1IpAddr, u1Length) == 0)
            && (pTrav->u1AddrType == u1AddrType)
            && (pTrav->u4CktIfIdx == u4CktIfIdx)
            && (pTrav->u4CktIfSubIdx == u4CktIfSubIdx))
        {
            if (pPrev == NULL)
            {
                /* The record to be deleted is the first one in the list
                 */
                pPrev = pTrav->pNext;
                pContext->IPIfTable.pIPIfAddr = pPrev;
            }
            else
            {
                /* The record to be deleted is not the first one in the list
                 */
                pPrev->pNext = pTrav->pNext;
            }

            pContext->IPIfTable.u2NumEntries--;
            pTrav->pNext = NULL;

            ISIS_MEM_FREE (ISIS_BUF_IPIF, (UINT1 *) pTrav);

            i4RetVal = ISIS_SUCCESS;
            break;
        }
        pPrev = pTrav;
        pTrav = pTrav->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlDelIPIf ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function       : IsisCtrlGetPSEntry ()
 * Description    : This routine fetches the Protocol Support table Record 
 *                  from the database 
 * Input(s)       : pContext       - Pointer to System Context
 *                  u1ProtSupp     - Protocol Support whose record is 
 *                                   to be obtained
 * Output(s)      : *pProtSupp     - Pointer to the Protocol Support record 
 *                                   that is fetched from the System Table
 * Globals        : None
 * Returns        : ISIS_SUCCESS - When Protocol Support Entry is fetched
 *                  ISIS_FAILURE - Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisCtrlGetPSEntry (tIsisSysContext * pContext, UINT1 u1ProtSupp,
                    tIsisProtSupp ** pProtSupp)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Count = 0;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGetPSEntry () \n"));

    while (u1Count < ISIS_MAX_PROTS_SUPP)
    {
        if (pContext->aProtSupp[u1Count].u1ProtSupp == u1ProtSupp)
        {
            *pProtSupp = &(pContext->aProtSupp[u1Count]);
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        u1Count++;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlGetPSEntry ()\n"));

    return i4RetVal;
}

/******************************************************************************
 * Function       : IsisCtrlAddPSEntry ()
 * Description    : This routine adds the Protocol Support Record to
 *                  the database 
 * Input(s)       : *pContext  - Pointer to System Context
 *                  u1Protsupp - Protocol  entry i.e to be added to the
 *                  Context 
 * Output(s)      : None
 * Globals        : Not referred or modified
 * Returns        : VOID
 *****************************************************************************/

PUBLIC INT4
IsisCtrlAddPSEntry (tIsisSysContext * pContext, UINT1 u1ProtSupp)
{
    INT4                i4Count = 0;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlAddPSEntry () \n"));

    while (i4Count < ISIS_MAX_PROTS_SUPP)
    {
        if (pContext->aProtSupp[i4Count].u1ProtSupp == 0)
        {
            pContext->aProtSupp[i4Count].u1ProtSupp = u1ProtSupp;
            return i4Count;
        }
        i4Count++;
    }
    return ISIS_FAILURE;
    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlAddPSEntry ()\n"));
}

/******************************************************************************
 * Function       : IsisCtrlDelPSEntry ()
 * Description    : This routine deletes the Protocol Supported Entry from
 *                  the database 
 * Input(s)       : *pContext  - Pointer to System Context
 *                  u1ProtSupp - Protocol i.e to be deleted from the Database
 * Output(s)      : None
 * Globals        : Not referred or modified
 * Returns        : ISIS_SUCCESS - When the Protocol Support Entry is removed 
 *                  ISIS_FAILURE - Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisCtrlDelPSEntry (tIsisSysContext * pContext, UINT1 u1ProtSupp)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Count = 0;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlDelPSEntry () \n"));

    while (u1Count < ISIS_MAX_PROTS_SUPP)
    {
        if (pContext->aProtSupp[u1Count].u1ProtSupp == u1ProtSupp)
        {
            pContext->aProtSupp[u1Count].u1ProtSupp = 0;
            pContext->aProtSupp[u1Count].u1ExistState = ISIS_DESTROY;
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        u1Count++;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlDelPSEntry ()\n"));
    return i4RetVal;

}

/******************************************************************************
 * Function     : IsisCtrlProcISDestroyEvt ()
 * Description  : This routine destroys the specified System Context, from the
 *                database 
 * Input(s)     : pIsisMsg - Pointer to the Message
 * Output(s)    : None
 * Globals      : Not referred or modified
 * Returns      : VOID
 *****************************************************************************/

PUBLIC VOID
IsisCtrlProcISDestroyEvt (tIsisMsg * pIsisMsg)
{
    UINT4               u4InstIdx = 0;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlProcISDestroyEvt ()\n"));

    u4InstIdx = pIsisMsg->u4CxtOrIfindex;

    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) (pIsisMsg->pu1Msg));
    pIsisMsg->pu1Msg = NULL;
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

    IsisCtrlDelSysContext (u4InstIdx);

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlProcISDestroyEvt ()\n"));
}

/*******************************************************************************
 * Function      : IsisCtrlProcISDownEvt ()
 * Description   : This routine deletes all the Equivalence Class Timers, Stops
 *                 all the timers, removes all the Deleted Nodes List updated by
 *                 the Decision process, Deletes all the event table entries,
 *                 Resets the usage count of Summary Sddresses and initializes
 *                 all statistics. In brief, it resets all the information that
 *                 is learnt dynamically and leaves all the configured
 *                 information intact. It marks the operational state of the
 *                 instance as ISIS_DOWN
 * Input(s)      : pContext - Pointer to the Context which has become
 *                            Operationally Down
 * Output(s)     : None
 * Globals       : Not referred or modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlProcISDownEvt (tIsisSysContext * pContext)
{
    tIsisSAEntry       *pSARec = NULL;
    tIsisSPTNode       *pL1DelNodes = NULL;
    tIsisSPTNode       *pNextDelNode = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevel1 = NULL;
    tIsisCktLevel      *pCktLevel2 = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlProcISDownEvt ()\n"));

    IsisStopAllTimers (pContext);

    /* Moving all  ECB blocks in the ECBlock 1 to the free Pool
     */

    IsisTmrDeAllocTmrECB (pContext->SysTimers.TmrECInfo.apTmrEC1,
                          ISIS_MAX_EC1_BLKS);

    /* Moving all  ECB blocks in the ECBlock 2 to the free Pool
     */
    IsisTmrDeAllocTmrECB (pContext->SysTimers.TmrECInfo.apTmrEC2,
                          ISIS_MAX_EC2_BLKS);

    /* Moving all  ECB blocks in the ECBlock 3 to the free Pool
     */

    IsisTmrDeAllocTmrECB (pContext->SysTimers.TmrECInfo.apTmrEC3,
                          ISIS_MAX_EC3_BLKS);

    pL1DelNodes = pContext->pL1DelNodes;
    pContext->pL1DelNodes = NULL;
    while (pL1DelNodes != NULL)
    {
        pNextDelNode = pL1DelNodes->pNext;
        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pL1DelNodes);
        pL1DelNodes = pNextDelNode;
    }

    /* Moving all the Events to the free Pool
     */

    IsisDeAllocEvent (pContext);

    pSARec = pContext->SummAddrTable.pSAEntry;
    while (pSARec != NULL)
    {
        pSARec->u2L1UsageCnt = 0;
        pSARec->u2L2UsageCnt = 0;
        pSARec = pSARec->pNext;
    }

    /* Reset all the statistics in System Context, Circuit and Circuit Level
     * records
     */

    MEMSET (&(pContext->SysStats), 0, sizeof (tIsisSysStats));

    pCktRec = pContext->CktTable.pCktRec;
    while (pCktRec != NULL)
    {
        MEMSET (&(pCktRec->CktStats), 0, sizeof (tIsisCktStats));

        pCktLevel1 = pCktRec->pL1CktInfo;
        pCktLevel2 = pCktRec->pL2CktInfo;

        if (pCktLevel1 != NULL)
        {
            MEMSET (&(pCktLevel1->RcvdPktStats), 0, sizeof (tIsisPktCount));
            MEMSET (&(pCktLevel1->SentPktStats), 0, sizeof (tIsisPktCount));
            pCktLevel1->u1ECTId = 0;
            pCktLevel1->u1HelloTmrIdx = 0;
            pCktLevel1->u1LSPTxTmrIdx = 0;
            pCktLevel1->u4DISDirIdx = 0;
            pCktLevel1->pMarkTxLSP = 0;
            MEMSET (pCktLevel1->au1CLCktID, 0, ISIS_SYS_ID_LEN + 1);
            MEMSET (pCktLevel1->au1CktLanDISID, 0, ISIS_SYS_ID_LEN + 1);
        }
        if (pCktRec->pL2CktInfo != NULL)
        {
            MEMSET (&(pCktLevel2->RcvdPktStats), 0, sizeof (tIsisPktCount));
            MEMSET (&(pCktLevel2->SentPktStats), 0, sizeof (tIsisPktCount));
            pCktLevel2->u1ECTId = 0;
            pCktLevel2->u1HelloTmrIdx = 0;
            pCktLevel2->u1LSPTxTmrIdx = 0;
            pCktLevel2->u4DISDirIdx = 0;
            pCktLevel2->pMarkTxLSP = 0;
            MEMSET (pCktLevel2->au1CLCktID, 0, ISIS_SYS_ID_LEN + 1);
            MEMSET (pCktLevel2->au1CktLanDISID, 0, ISIS_SYS_ID_LEN + 1);
        }
        pCktRec = pCktRec->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlProcISDownEvt ()\n"));
}

/*******************************************************************************
 * Function      : IsisCtrlProcISUpEvt ()
 * Description   : This routine activates all the circuits whose Row Status is
 *                 ACTIVE, which are Administratively UP and whose Interface
 *                 status is UP. Once the circuits are activated, Adjacency
 *                 module starts generating Hellos over the circuits
 * Input(s)      : pContext -    Pointer to the Context which has become
 *                               Operationally UP
 *                 u1PrevState - Previous FT State of the System.
 *                 
 * Output(s)     : None
 * Globals       : Not referred or modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlProcISUpEvt (tIsisSysContext * pContext, UINT1 u1PrevState)
{
    tIsisCktEntry      *pCktRec = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;
    INT4                i4RetVal = ISIS_FAILURE;
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlProcISUpEvt ()\n"));

    pCktRec = pContext->CktTable.pCktRec;

    while (pCktRec != NULL)
    {
        /* Check the compatibility between the system and circuit.
         * if the circuit and system are not compatible, then make
         * the circuit compatible with the system.
         * e.g  the System is LEVEL1 and circuit is configured as
         * LEVEL2 or LEVEL12 then change the circuit type to LEVEL1 and 
         * activate the LEVEL1 Circuit Level Record
         */

        if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
             || (pContext->SysActuals.u1SysType == ISIS_LEVEL2))
            && (pCktRec->u1CktLevel != pContext->SysActuals.u1SysType))
        {
            pCktRec->u1CktLevel = pContext->SysActuals.u1SysType;
            i4RetVal = IsisAdjActivateCktLvl (pContext, pCktRec);
            if (i4RetVal == ISIS_FAILURE)
            {
                /* while initializing the Circuit level records 
                 * of circuits, Memory Allocation fails, so 
                 * continuing to next circuit record */
                pCktRec = pCktRec->pNext;
                continue;
            }
        }

        if (((pCktRec->u1CktExistState == ISIS_ACTIVE)
             && (pCktRec->bCktAdminState == ISIS_STATE_ON))
            && (pCktRec->u1CktIfStatus == ISIS_STATE_ON))
        {
            pCktRec->u1OperState = ISIS_UP;

            pCktEvt = (tIsisEvtCktChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));
            if (pCktEvt != NULL)
            {
                pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                pCktEvt->u1CktType = pCktRec->u1CktType;
                pCktEvt->u1CktLevel = pCktRec->u1CktLevel;
                pCktEvt->u4CktIdx = pCktRec->u4CktIdx;

                if (u1PrevState == ISIS_FT_INIT)
                {
                    pCktEvt->u1Status = ISIS_CKT_UP;
                }
                else if (u1PrevState == ISIS_FT_STANDBY)
                {
                    pCktEvt->u1Status = ISIS_CKT_RESTART;
                }

                IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                  sizeof (tIsisEvtCktChange));
            }
            else
            {
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_EVTS);
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Events\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                CTP_EE ((ISIS_LGST,
                         "CTL <X> : Exiting IsisCtrlProcISUpEvt ()\n"));
                return;
            }
        }
        pCktRec = pCktRec->pNext;
    }

    /* NOTE: Operational State of the instance will be marked as UP once the
     * Update module completes updating all the SelfLSPS
     */

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlProcISUpEvt ()\n"));
}

/*******************************************************************************
 * Function    : IsisCtrlInitTimers
 * Description : This function initialises all the Equivalence Class Timer array
 *               for the specified System Context
 * Input(s)    : pContext - Pointer to System Context
 * Outputs(s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlInitTimers (tIsisSysContext * pContext)
{
    UINT1               u1Index = 0;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlInitTimers ()\n"));

    ISIS_INIT_TIMER (pContext->SysTimers.SysECTimer);
    ISIS_INIT_TIMER (pContext->SysTimers.SysISSeqWrapTimer);
    ISIS_INIT_TIMER (pContext->SysTimers.SysDbaseNormTimer);
    ISIS_INIT_TIMER (pContext->SysTimers.SysSPFSchTimer);
    ISIS_INIT_TIMER (pContext->SysL1Info.SysLSPGenTimer);
    ISIS_INIT_TIMER (pContext->SysL2Info.SysLSPGenTimer);
    ISIS_INIT_TIMER (pContext->SysL1Info.SysWaitingTimer);
    ISIS_INIT_TIMER (pContext->SysL2Info.SysWaitingTimer);

    pContext->SysTimers.TmrECInfo.u1EC1StartIdx = 0;
    pContext->SysTimers.TmrECInfo.u1EC2StartIdx = 0;
    pContext->SysTimers.TmrECInfo.u1EC3StartIdx = 0;

    for (u1Index = 0; u1Index < ISIS_MAX_EC1_BLKS; u1Index++)
    {
        pContext->SysTimers.TmrECInfo.apTmrEC1[u1Index] = NULL;
    }
    for (u1Index = 0; u1Index < ISIS_MAX_EC2_BLKS; u1Index++)
    {
        pContext->SysTimers.TmrECInfo.apTmrEC2[u1Index] = NULL;
    }
    for (u1Index = 0; u1Index < ISIS_MAX_EC3_BLKS; u1Index++)
    {
        pContext->SysTimers.TmrECInfo.apTmrEC3[u1Index] = NULL;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlInitTimers ()\n"));
}

/*******************************************************************************
 * Function    : IsisCtrlGetIPRARec ()
 * Description : This routines fetches an IP Reachability Address record based
 *               on the given Index
 * Input(s)    : pContext   - Pointer to Context 
 *               u4IPRAIdx  - IP Reachability Address Index
 *               u1IPRAType - IP Reachability Address Type
 * Output(s)   : pIPRARec   - Pointer to IPRA Record that matched the given
 *                            index 
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if a matching IPRA Record is found 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisCtrlGetIPRARec (tIsisSysContext * pContext, UINT4 u4IPRAIdx,
                    UINT1 u1IPRAType, tIsisIPRAEntry ** pIPRARec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisIPRAEntry     *pTravIPRA = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlGetIPRARec ()\n"));

    /* Travel through the entire list and fetch the matching record
     */

    /* Check node pointed by cache-pointer, if it pointed to correct node
     * just return it.
     */
    if (NULL != pContext->pLastIPRAEntry)
    {
        if ((pContext->pLastIPRAEntry->u4IPRAIdx == u4IPRAIdx) &&
            (pContext->pLastIPRAEntry->u1IPRAType == u1IPRAType))
        {
            *pIPRARec = pContext->pLastIPRAEntry;
            i4RetVal = ISIS_SUCCESS;
            return i4RetVal;    /* Successfully found, do not need scan all list */
        }
    }

    pTravIPRA = pContext->IPRAInfo.pIPRAEntry;

    while (pTravIPRA != NULL)
    {
        if ((pTravIPRA->u4IPRAIdx == u4IPRAIdx)
            && (pTravIPRA->u1IPRAType == u1IPRAType))
        {
            *pIPRARec = pTravIPRA;
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        pTravIPRA = pTravIPRA->pNext;
    }

    if (i4RetVal == ISIS_FAILURE)
    {
        ADP_PT ((ISIS_LGST, "ADJ <T> : No Matching IPRA Record Found\n"));
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlGetIPRARec ()\n"));
    return (i4RetVal);
}

/*******************************************************************************
 * Function    : IsisCtrlAddIPRA ()
 * Description : This routine adds an IP Reachability Address to the given
 *               Circuit
 *               Record. 
 * Input(s)    : pContext - Pointer to Context 
 *               pIPRARec  - Pointer to the IPRA Record to be added
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlAddIPRA (tIsisSysContext * pContext, tIsisIPRAEntry * pIPRARec)
{
    tIsisIPRAEntry     *pCurrIPRA = NULL;
    tIsisIPRAEntry     *pPrevIPRA = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlAddIPRA ()\n"));

    pIPRARec->pNext = NULL;
    /* Start processing from Head, bcoz we dont have Tail pointer */
    pCurrIPRA = pContext->IPRAInfo.pIPRAEntry;
    if (NULL == pCurrIPRA)
    {
        /* Case of empty list, just update head pointer */
        pContext->IPRAInfo.pIPRAEntry = pIPRARec;
    }
    /* Ok, list has at least one node(node1), depends on it type, we do following:
     * 1) if (node1->type == AUTO) just insert newNode before node1
     * 2) OR if (node1->type == MANUAL && newNode->Idx < node1->Idx) 
     *       { put newNode befor node1}
     (newNode)--prepend-->[Manual=3]-..>[man=max]...>[auto=1]- ..->{auto=max}
     * 3) in other cases we need scan list :(
     */
    else if ((ISIS_AUTO_TYPE == pCurrIPRA->u1IPRAType) ||
             ((ISIS_MANUAL_TYPE == pCurrIPRA->u1IPRAType)
              && (pIPRARec->u4IPRAIdx < pCurrIPRA->u4IPRAIdx)))
    {
        pIPRARec->pNext = pCurrIPRA;
        pContext->IPRAInfo.pIPRAEntry = pIPRARec;
    }
    else                        /* In list exist other manual-type nodes(or MAN and AUTO), so we need 
                                   scan list and insert pIPRARec in appropriate place.
                                   Scan utill reach first auto node */
    {
        pPrevIPRA = pCurrIPRA;
        pCurrIPRA = pCurrIPRA->pNext;    /* Second node, first node already processed(i hope) */
        /* Second condition needed for stoping scan on the last manual IPRA entry */
        while ((NULL != pCurrIPRA) && (pCurrIPRA->u1IPRAType != ISIS_AUTO_TYPE))
        {
            if (pIPRARec->u4IPRAIdx < pCurrIPRA->u4IPRAIdx)
            {
                pIPRARec->pNext = pCurrIPRA;
                pPrevIPRA->pNext = pIPRARec;
                break;
            }
            pPrevIPRA = pCurrIPRA;
            pCurrIPRA = pCurrIPRA->pNext;

        }
        /* Indication the end of IPRA_MANUAL list is NULL or type == ISIS_AUTO_TYPE */
        if ((pPrevIPRA->pNext == NULL) ||
            ((pCurrIPRA != NULL) && (pCurrIPRA->u1IPRAType == ISIS_AUTO_TYPE)))
        {                        /* Append */
            pIPRARec->pNext = pCurrIPRA;
            pPrevIPRA->pNext = pIPRARec;
        }
    }
    /* Update counter */
    (pContext->IPRAInfo.u2NumEntries)++;
    /* Resettring of cache-pointer for IPRA */
    pContext->pLastIPRAEntry = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlAddIPRA ()\n"));
}

/*******************************************************************************
 * Function    : IsisCtrlAutoAddIPRA ()
 * Description : This routine adds an IP Reachability Address to the given
 *               Circuit
 *               Record. 
 * Input(s)    : pContext - Pointer to Context 
 *               pu1IpAddr - IP Address
 *               u1PrefixLen - Prefix length
 *               Metric - Metric
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlAddAutoIPRA (tIsisSysContext * pContext, UINT4 u4IfIdx, UINT4 u4SubIdx,
                     UINT1 *pu1IpAddr, UINT1 u1PrefixLen, tIsisMetric Metric,
                     UINT4 u4FullMetric, UINT1 u1AddrType)
{
    UINT4               u4NxtIdx = 1;
    UINT4               u4Port = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4TempAddr = 0;
    UINT4               u4NetMask = 0;
    UINT4               u4TempMask = 0;
    UINT1               u1Idx = 0;
    UINT1               au1Mask[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1AddrLen;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisIPRAEntry     *pPrevIPRA = NULL;    /* We use SLL, so we need remember */
    tIsisIPRAEntry     *pCurrIPRA = NULL;    /* pointer to the previous node.   */
    tIsisEvtIPRAChange *pIPRAEvt = NULL;
    UINT1               au1IPAddr[ISIS_MAX_IPV4_ADDR_LEN];
    UINT1               au1IPMask[ISIS_MAX_IPV4_ADDR_LEN];

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlAddIPRA ()\n"));

    if (u1AddrType == ISIS_ADDR_IPV6)
    {
        u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
    }
    else
    {
        u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
        if (NetIpv4GetPortFromIfIndex (u4IfIdx, &u4Port) == NETIPV4_FAILURE)
        {
            CTP_PT ((ISIS_LGST,
                     "CTL <E> : Unable to fetch port number from IfIndex [%u]\n",
                     u4IfIdx));
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisAdjProcStatInd ()\n"));
            return;
        }
    }

    u4IpAddr = 0;
    u4NetMask = 0;
    do
    {

        if (u1AddrLen == ISIS_MAX_IPV4_ADDR_LEN)
        {
            if (u4IpAddr == 0)
            {
                MEMCPY (&u4IpAddr, pu1IpAddr, ISIS_MAX_IPV4_ADDR_LEN);
                MEMSET (au1Mask, 0, u1AddrLen);
                IsisUtlGetIPMask (u1PrefixLen, au1Mask);
                MEMCPY (&u4NetMask, au1Mask, ISIS_MAX_IPV4_ADDR_LEN);
                MEMCPY (au1IPAddr, &u4IpAddr, ISIS_MAX_IPV4_ADDR_LEN);
                MEMCPY (au1IPMask, &u4NetMask, ISIS_MAX_IPV4_ADDR_LEN);
            }
            else
            {
                u4TempAddr = OSIX_HTONL (u4IpAddr);
                MEMCPY (au1IPAddr, &u4TempAddr, ISIS_MAX_IPV4_ADDR_LEN);
                u4TempMask = OSIX_HTONL (u4NetMask);
                MEMCPY (au1IPMask, &u4TempMask, ISIS_MAX_IPV4_ADDR_LEN);
            }
        }

        pIPRARec = (tIsisIPRAEntry *)
            ISIS_MEM_ALLOC (ISIS_BUF_IPRA, sizeof (tIsisIPRAEntry));
        if (pIPRARec == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IPIf Entry\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisAdjProcStatInd ()\n"));
            return;
        }
        pIPRARec->u1IPRAType = ISIS_AUTO_TYPE;
        pIPRARec->u1IPRADestType = u1AddrType;
        pIPRARec->u1PrefixLen = u1PrefixLen;
        pIPRARec->u4IfIndex = u4IfIdx;
        pIPRARec->u4IfSubIndex = u4SubIdx;

        if (u1AddrLen == ISIS_MAX_IPV4_ADDR_LEN)
        {
            MEMCPY (pIPRARec->au1IPRADest, au1IPAddr, u1AddrLen);
            if (u1AddrType == ISIS_ADDR_IPV4)
            {
                for (u1Idx = 0; u1Idx < u1AddrLen; u1Idx++)
                {
                    pIPRARec->au1IPRADest[u1Idx] &= au1IPMask[u1Idx];
                }
            }

            IsisUtlComputePrefixLen (au1IPMask, ISIS_IPV4_ADDR_LEN,
                                     &u1PrefixLen);
            pIPRARec->u1PrefixLen = u1PrefixLen;
        }
        else
        {
            MEMCPY (pIPRARec->au1IPRADest, pu1IpAddr, u1AddrLen);
        }

        pIPRARec->u4FullMetric = u4FullMetric;
        MEMCPY (pIPRARec->Metric, Metric, ISIS_NUM_METRICS);

        pIPRARec->u4IPRAIdx = pContext->IPRAInfo.u4NxtAutoIdx;
        pIPRARec->u1IPRAExistState = ISIS_ACTIVE;
        pIPRARec->u1IPRAAdminState = ISIS_STATE_ON;

        /* Insert current node to correct place in list. */

        pIPRARec->pNext = NULL;
        /* Start from the HEAD of the IPRA list
         * (we don't have pointer to the start of AUTO part of list)*/
        pCurrIPRA = pContext->IPRAInfo.pIPRAEntry;
        pPrevIPRA = NULL;

        /* Case of the  first element insertion */
        if (NULL == pCurrIPRA)
        {
            pContext->IPRAInfo.pIPRAEntry = pIPRARec;
        }
        else                    /* We should scan list. 1M->2M->...1A->2A->...->nA : list structure */
        {
            /* In any case, when insertion AUTO node we need scan through 
             * MANUAL part of the IPRA list */
            while (NULL != pCurrIPRA)
            {
                if ((pCurrIPRA->u1IPRAType == ISIS_AUTO_TYPE) &&
                    (pIPRARec->u4IPRAIdx < pCurrIPRA->u4IPRAIdx))
                {
                    /* Insert pIPRARec  before pCurrIPRA */
                    pIPRARec->pNext = pCurrIPRA;
                    if (pPrevIPRA == NULL)
                    {
                        pContext->IPRAInfo.pIPRAEntry = pIPRARec;
                    }
                    else
                    {
                        pPrevIPRA->pNext = pIPRARec;
                    }
                    break;
                }

                pPrevIPRA = pCurrIPRA;
                pCurrIPRA = pCurrIPRA->pNext;
            }
            /* If we reached end of the list, pIPRARec should be the last element */
            if (NULL == pCurrIPRA)
            {
                pPrevIPRA->pNext = pIPRARec;    /* Append after last */
            }
        }
        /* Node inserted, update counter */
        (pContext->IPRAInfo.u2NumEntries)++;
        /* Reset cache-pointer for IPRA Table */
        pContext->pLastIPRAEntry = NULL;

        pIPRAEvt = (tIsisEvtIPRAChange *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));

        if (pIPRAEvt != NULL)
        {
            pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
            pIPRAEvt->u1Status = ISIS_IPRA_UP;
            pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
            pIPRAEvt->u1IPRAType = ISIS_AUTO_TYPE;
            pIPRAEvt->u1PrefixLen = u1PrefixLen;
            pIPRAEvt->u1IPRADestType = pIPRARec->u1IPRADestType;
            IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                              sizeof (tIsisEvtIPRAChange));
        }
        else
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : IP Reachability Change Event\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }

        /* Update the next available IPRA Index.
         * As MANUAL nodes can have any unrepeated index(also sorted, like 1,3,10,...),
         * we update next index only for auto nodes.
         */
        pIPRARec = pContext->IPRAInfo.pIPRAEntry;
        while (pIPRARec != NULL)
        {
            if (ISIS_AUTO_TYPE == pIPRARec->u1IPRAType)
            {
                if (u4NxtIdx < pIPRARec->u4IPRAIdx)
                {
                    pContext->IPRAInfo.u4NxtAutoIdx = u4NxtIdx;
                    break;
                }
                u4NxtIdx++;
            }
            pIPRARec = pIPRARec->pNext;
        }
        if (pIPRARec == NULL)
        {
            pContext->IPRAInfo.u4NxtAutoIdx = u4NxtIdx;
        }
    }
    while ((u1AddrType == ISIS_ADDR_IPV4) &&
           (NetIpv4GetNextSecondaryAddress ((UINT2) u4Port, u4IpAddr, &u4IpAddr,
                                            &u4NetMask) == NETIPV4_SUCCESS));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlAddIPRA ()\n"));
}

/*******************************************************************************
 * Function    : IsisCtrlDelIPRA ()
 * Description : This routine deletes the specified IPRA record from the
 *               circuit record
 * Input(s)    : pContext   - Pointer to Context Record
 *               u4IPRAIdx  - Index of IPRA Entry
 *               u1IPRAType - IPRA Type
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if IPRA Record is deleted successfully
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisCtrlDelIPRA (tIsisSysContext * pContext, UINT4 u4IPRAIdx, UINT1 u1IPRAType)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisIPRAEntry     *pDelIPRA = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlDelIPRA ()\n"));

    ADP_PT ((ISIS_LGST,
             "CTL <T> : Deleting IPRA On Instance [ %u ]\n",
             pContext->u4SysInstIdx));

    pIPRARec = pContext->IPRAInfo.pIPRAEntry;

    if (pIPRARec == NULL)
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : No Active IPRA Records Exist - Context [%u]\n",
                 pContext->u4SysInstIdx));
        return (i4RetVal);
    }

    while (pIPRARec != NULL)
    {
        if ((pIPRARec->u4IPRAIdx == u4IPRAIdx)
            && (pIPRARec->u1IPRAType == u1IPRAType))
        {
            ISIS_DBG_PRINT_ADDR (pIPRARec->au1IPRADest,
                                 (UINT1) ISIS_MAX_IP_ADDR_LEN,
                                 "ADJ <T> : Deleted IPRA\t", ISIS_OCTET_STRING);

            if (pDelIPRA == NULL)
            {
                /* Very first entry in the list, advance the head */

                pContext->IPRAInfo.pIPRAEntry = pIPRARec->pNext;
                pDelIPRA = pIPRARec;
            }
            else
            {
                pDelIPRA->pNext = pIPRARec->pNext;
                pDelIPRA = pIPRARec;
            }
            pDelIPRA->pNext = NULL;
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        pDelIPRA = pIPRARec;
        pIPRARec = pIPRARec->pNext;
    }
    /* Reset cache-pointer for isisIPRA table */
    pContext->pLastIPRAEntry = NULL;

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Matching entry found */

        (pContext->IPRAInfo.u2NumEntries)--;

        ISIS_FLTR_IPRA_LSU (pContext, ISIS_CMD_DELETE, pDelIPRA, ISIS_ACTIVE);

        ISIS_MEM_FREE (ISIS_BUF_IPRA, (UINT1 *) pDelIPRA);
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlDelIPRA ()\n"));
    return (i4RetVal);
}

/*******************************************************************************
 * Function    : IsisCtrlDelAutoIPRA ()
 * Description : This routine deletes an IP Reachability Address from the
 *               Circuit Record. 
 * Input(s)    : pContext   - Pointer to Context 
 *               u4IfIdx    - Interface index
 *               u4SubIdx   - Sub interface index
 *               u1AddrType - Address type
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlDelAutoIPRA (tIsisSysContext * pContext, UINT4 u4IfIdx, UINT4 u4SubIdx,
                     UINT1 u1AddrType)
{
    tIsisIPRAEntry     *pRec = NULL;
    tIsisIPRAEntry     *pAllRec = NULL;
    tIsisIPRAEntry     *pTempRec = NULL;
    tIsisIPRAEntry     *pPrev = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;
    UINT1               u1Flag = ISIS_FALSE;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlDelAutoIPRA ()\n"));

    pRec = pContext->IPRAInfo.pIPRAEntry;
    while (pRec != NULL)
    {
        if ((pRec->u1IPRAType == ISIS_AUTO_TYPE) && (pRec->u4IfIndex == u4IfIdx)
            && (pRec->u4IfSubIndex == u4SubIdx)
            && (pRec->u1IPRADestType == u1AddrType))
        {

            if (pTempRec == NULL)
            {
                pTempRec = pRec;
                pAllRec = pTempRec;
            }
            else
            {
                pTempRec->pNext = pRec;
                pTempRec = pTempRec->pNext;
            }

            if (pPrev == NULL)
            {
                pContext->IPRAInfo.pIPRAEntry = pRec->pNext;
            }
            else
            {
                pPrev->pNext = pRec->pNext;
                u1Flag = ISIS_FALSE;
            }
        }
        else
        {
            u1Flag = ISIS_TRUE;
        }
        if (u1Flag == ISIS_TRUE)
        {
            pPrev = pRec;
        }
        pRec = pRec->pNext;
    }
    if (pTempRec != NULL)
    {
        pTempRec->pNext = NULL;
    }

    /* Reset cache-pointer for isisIPRA Table */
    pContext->pLastIPRAEntry = NULL;

    if (pAllRec == NULL)
    {
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlDelAutoIPRA ()\n"));
        return;
    }
    do
    {
        pRec = pAllRec;
        pAllRec = pAllRec->pNext;

        (pContext->IPRAInfo.u2NumEntries)--;

        pIPRAEvt = (tIsisEvtIPRAChange *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));

        if (pIPRAEvt != NULL)
        {
            pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
            pIPRAEvt->u1Status = ISIS_IPRA_DOWN;
            pIPRAEvt->u4IPRAIdx = pRec->u4IPRAIdx;
            pIPRAEvt->u1IPRAType = ISIS_AUTO_TYPE;
            pIPRAEvt->u1PrefixLen = pRec->u1PrefixLen;
            if (pRec->u1IPRADestType == ISIS_ADDR_IPV4)
            {
                MEMCPY (pIPRAEvt->au1IPRADest, pRec->au1IPRADest,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pIPRAEvt->u1IPRADestType = ISIS_ADDR_IPV4;
            }
            else
            {
                MEMCPY (pIPRAEvt->au1IPRADest, pRec->au1IPRADest,
                        ISIS_MAX_IPV6_ADDR_LEN);
                pIPRAEvt->u1IPRADestType = ISIS_ADDR_IPV6;
            }
            ISIS_SET_METRIC (pContext, pIPRAEvt, pRec);
            IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                              sizeof (tIsisEvtIPRAChange));
        }
        else
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : IP Reachability Change Event\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
        ISIS_MEM_FREE (ISIS_BUF_IPRA, (UINT1 *) pRec);

    }
    while (pAllRec != NULL);

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlDelAutoIPRA ()\n"));
}

/*******************************************************************************
 * Function    : IsisCtrlDelAutoSecIPRA ()
 * Description : This routine deletes an IP Reachability Address from the
 *               IPRA Table with the given Secondary IP address
 *               .
 * Input(s)    : pContext   - Pointer to Context
 *               u4IfIdx    - Interface index
 *               u4SubIdx   - Sub interface index
 *               u1AddrType - Address type
 *               pu1SecIP   - Secondary IP 
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlDelAutoSecIPRA (tIsisSysContext * pContext, UINT4 u4IfIdx,
                        UINT4 u4SubIdx, UINT1 u1AddrType, UINT1 *pu1SecIP)
{
    tIsisIPRAEntry     *pRec = NULL;
    tIsisIPRAEntry     *pPrev = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlDelAutoSecIPRA ()\n"));

    pRec = pContext->IPRAInfo.pIPRAEntry;
    while (pRec != NULL)
    {
        if ((pRec->u1IPRAType == ISIS_AUTO_TYPE) && (pRec->u4IfIndex == u4IfIdx)
            && (pRec->u4IfSubIndex == u4SubIdx)
            && (pRec->u1IPRADestType == u1AddrType)
            && (MEMCMP (pu1SecIP, pRec->au1IPRADest, ISIS_MAX_IPV4_ADDR_LEN) ==
                0))
        {
            if (pPrev == NULL)
            {
                pContext->IPRAInfo.pIPRAEntry = pRec->pNext;
            }
            else
            {
                pPrev->pNext = pRec->pNext;
            }
            break;
        }
        pPrev = pRec;
        pRec = pRec->pNext;
    }
    /* Reset cache-pointer for isisIPRA Table */
    pContext->pLastIPRAEntry = NULL;

    if (pRec == NULL)
    {
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlDelAutoSecIPRA ()\n"));
        return;
    }
    (pContext->IPRAInfo.u2NumEntries)--;

    pIPRAEvt = (tIsisEvtIPRAChange *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));

    if (pIPRAEvt != NULL)
    {
        pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
        pIPRAEvt->u1Status = ISIS_IPRA_DOWN;
        pIPRAEvt->u4IPRAIdx = pRec->u4IPRAIdx;
        pIPRAEvt->u1IPRAType = ISIS_AUTO_TYPE;
        pIPRAEvt->u1PrefixLen = pRec->u1PrefixLen;
        if (pRec->u1IPRADestType == ISIS_ADDR_IPV4)
        {
            MEMCPY (pIPRAEvt->au1IPRADest, pRec->au1IPRADest,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pIPRAEvt->u1IPRADestType = ISIS_ADDR_IPV4;
        }
        else
        {
            MEMCPY (pIPRAEvt->au1IPRADest, pRec->au1IPRADest,
                    ISIS_MAX_IPV6_ADDR_LEN);
            pIPRAEvt->u1IPRADestType = ISIS_ADDR_IPV6;
        }
        ISIS_SET_METRIC (pContext, pIPRAEvt, pRec);
        IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                          sizeof (tIsisEvtIPRAChange));
    }
    else
    {
        PANIC ((ISIS_LGST,
                ISIS_MEM_ALLOC_FAIL " : IP Reachability Change Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }
    ISIS_MEM_FREE (ISIS_BUF_IPRA, (UINT1 *) pRec);

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlDelAutoSecIPRA ()\n"));

}

/******************************************************************************
 * Function Name : IsisCtrlUpdtIPRA ()
 * Description   : This routine updates the existing IPRA 
 *                 Record with the given Record. Only the changeable values
 *                 are updated. This routine is called by the FLTR module in
 *                 the Standby node to update the LSU of the active node.
 *                 Only FLTR Module calls this function
 * Input(s)      : pExistIPRA - Pointer to the Existing IPRA Record
 *                 pNewIPRA   - Pointer to the new IPRA Record whose values
 *                              are to be updated in the Database
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlUpdtIPRA (tIsisIPRAEntry * pExistIPRA, tIsisIPRAEntry * pNewIPRA)
{
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlUpdtIPRA ()\n"));

    /* Only certain parameters can get modified after the IPRA is created. Those
     * parameters alone get updated and the rest remain the same
     */

    pExistIPRA->u1IPRADestType = pNewIPRA->u1IPRADestType;
    pExistIPRA->u1IPRAExistState = pNewIPRA->u1IPRAExistState;
    pExistIPRA->u1IPRAAdminState = pNewIPRA->u1IPRAAdminState;
    pExistIPRA->u1PrefixLen = pNewIPRA->u1PrefixLen;
    MEMCPY (pExistIPRA->Metric, pNewIPRA->Metric, ISIS_NUM_METRICS);
    pExistIPRA->u4FullMetric = pNewIPRA->u4FullMetric;

    if (pNewIPRA->u1IPRADestType == ISIS_ADDR_IPV4)
    {
        MEMCPY (pExistIPRA->au1IPRADest, pNewIPRA->au1IPRADest,
                ISIS_MAX_IPV4_ADDR_LEN);
    }
    else
    {
        MEMCPY (pExistIPRA->au1IPRADest, pNewIPRA->au1IPRADest,
                ISIS_MAX_IPV6_ADDR_LEN);
    }
    MEMCPY (pExistIPRA->au1IPRASNPA, pNewIPRA->au1IPRASNPA, ISIS_SNPA_ADDR_LEN);

    /* We don't require the new record once the existing record is updated
     */

    ISIS_MEM_FREE (ISIS_BUF_IPRA, (UINT1 *) pNewIPRA);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlUpdtIPRA ()\n"));
}

/****************************************************************************
 * Function Name : IsisCtrlCopyIPRADefVal ()
 * Description   : This routine initialises the given IPRA entry with default
 *                 values
 * Input (s)     : pContext - Pointer to System Context
 *                 pIPRARec - Pointer to the IPRA Record
 * Output (s)    : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
******************************************************************************/

PUBLIC VOID
IsisCtrlCopyIPRADefVal (tIsisSysContext * pContext, tIsisIPRAEntry * pIPRARec)
{
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlCopyIPRADefVal ()\n"));

    pIPRARec->u1IPRAAdminState = ISIS_STATE_OFF;

    /* Update the default values for all the Metrics in the IPRA
     * Information Record
     */

    IsisUtlFillMetDefVal (pContext, pIPRARec, ISIS_IPRA_REC);

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlCopyIPRADefVal ()\n"));
}

 /****************************************************************************
  * Function Name : IsisGetSemName ()
  * Description   : This routine is used to generate semaphore name.
  * Input (s)     : pContext - Pointer to System Context
  * Output (s)    : au1SE
  * Globals       : Not Referred or Modified
  * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisGetSemName (tIsisSysContext * pContext, UINT1 au1SemName[])
{

    gu4SemNameVar++;
    SNPRINTF ((CHR1 *) au1SemName, OSIX_NAME_LEN + 1, "I%03x", gu4SemNameVar);
    if (gu4SemNameVar == ISIS_MAX_SEM_VALUE)
    {
        gu4SemNameVar = 0;
    }
    UNUSED_PARAM (pContext);
}

/*******************************************************************************
 * Function    : IsisCtrlSecAutoAddIPRA ()
 * Description : This routine adds an IP Reachability Address to the given
 *               Circuit
 *               Record.
 * Input(s)    : pContext - Pointer to Context
 *               pu1IpAddr - IP Address
 *               u1PrefixLen - Prefix length
 *               Metric - Metric
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisCtrlAddSecAutoIPRA (tIsisSysContext * pContext, UINT4 u4IfIdx,
                        UINT4 u4SubIdx, UINT1 *pu1IpAddr, UINT1 u1PrefixLen,
                        tIsisMetric Metric, UINT4 u4FullMetric,
                        UINT1 u1AddrType)
{
    UINT4               u4NxtIdx = 1;
    UINT1               u1Idx = 0;
    UINT1               au1Mask[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1AddrLen;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisIPRAEntry     *pPrevIPRA = NULL;    /* We use SLL, so we need remember */
    tIsisIPRAEntry     *pCurrIPRA = NULL;    /* pointer to the previous node.   */
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisCtrlAddIPRA ()\n"));

    pIPRARec = (tIsisIPRAEntry *)
        ISIS_MEM_ALLOC (ISIS_BUF_IPRA, sizeof (tIsisIPRAEntry));
    if (pIPRARec == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " :  IPIf Entry\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisAdjProcStatInd ()\n"));
        return;
    }
    if (u1AddrType == ISIS_ADDR_IPV6)
    {
        u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
    }
    else
    {
        u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
    }
    pIPRARec->u1IPRAType = ISIS_AUTO_TYPE;
    pIPRARec->u1IPRADestType = u1AddrType;
    pIPRARec->u1PrefixLen = u1PrefixLen;
    pIPRARec->u4IfIndex = u4IfIdx;
    pIPRARec->u4IfSubIndex = u4SubIdx;
    MEMSET (au1Mask, 0, u1AddrLen);
    IsisUtlGetIPMask (u1PrefixLen, au1Mask);
    MEMCPY (pIPRARec->au1IPRADest, pu1IpAddr, u1AddrLen);
    if (u1AddrType == ISIS_ADDR_IPV4)
    {
        for (u1Idx = 0; u1Idx < u1AddrLen; u1Idx++)
        {
            pIPRARec->au1IPRADest[u1Idx] &= au1Mask[u1Idx];
        }
    }
    pIPRARec->u4FullMetric = u4FullMetric;
    MEMCPY (pIPRARec->Metric, Metric, ISIS_NUM_METRICS);

    pIPRARec->u4IPRAIdx = pContext->IPRAInfo.u4NxtAutoIdx;
    pIPRARec->u1IPRAExistState = ISIS_ACTIVE;
    pIPRARec->u1IPRAAdminState = ISIS_STATE_ON;

    /* Insert current node to correct place in list. */

    pIPRARec->pNext = NULL;
    /* Start from the HEAD of the IPRA list
     * (we don't have pointer to the start of AUTO part of list)*/
    pCurrIPRA = pContext->IPRAInfo.pIPRAEntry;
    pPrevIPRA = NULL;

    /* Case of the  first element insertion */
    if (NULL == pCurrIPRA)
    {
        pContext->IPRAInfo.pIPRAEntry = pIPRARec;
    }
    else                        /* We should scan list. 1M->2M->...1A->2A->...->nA : list structure */
    {
        /* In any case, when insertion AUTO node we need scan through
         * MANUAL part of the IPRA list */
        while (NULL != pCurrIPRA)
        {
            if ((pCurrIPRA->u1IPRAType == ISIS_AUTO_TYPE) &&
                (pIPRARec->u4IPRAIdx < pCurrIPRA->u4IPRAIdx))
            {
                /* Insert pIPRARec  before pCurrIPRA */
                pIPRARec->pNext = pCurrIPRA;
                if (pPrevIPRA == NULL)
                {
                    pContext->IPRAInfo.pIPRAEntry = pIPRARec;
                }
                else
                {
                    pPrevIPRA->pNext = pIPRARec;
                }
                break;
            }

            pPrevIPRA = pCurrIPRA;
            pCurrIPRA = pCurrIPRA->pNext;
        }
        /* If we reached end of the list, pIPRARec should be the last element */
        if (NULL == pCurrIPRA)
        {
            pPrevIPRA->pNext = pIPRARec;    /* Append after last */
        }
    }
    /* Node inserted, update counter */
    (pContext->IPRAInfo.u2NumEntries)++;
    /* Reset cache-pointer for IPRA Table */
    pContext->pLastIPRAEntry = NULL;

    pIPRAEvt = (tIsisEvtIPRAChange *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));

    if (pIPRAEvt != NULL)
    {
        pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
        pIPRAEvt->u1Status = ISIS_IPRA_UP;
        pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
        pIPRAEvt->u1IPRAType = ISIS_AUTO_TYPE;
        pIPRAEvt->u1PrefixLen = u1PrefixLen;
        pIPRAEvt->u1IPRADestType = pIPRARec->u1IPRADestType;
        IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                          sizeof (tIsisEvtIPRAChange));
    }
    else
    {
        PANIC ((ISIS_LGST,
                ISIS_MEM_ALLOC_FAIL " : IP Reachability Change Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }

    /* Update the next available IPRA Index.
     * As MANUAL nodes can have any unrepeated index(also sorted, like 1,3,10,...),
     * we update next index only for auto nodes.
     */
    pIPRARec = pContext->IPRAInfo.pIPRAEntry;
    while (pIPRARec != NULL)
    {
        if (ISIS_AUTO_TYPE == pIPRARec->u1IPRAType)
        {
            if (u4NxtIdx < pIPRARec->u4IPRAIdx)
            {
                pContext->IPRAInfo.u4NxtAutoIdx = u4NxtIdx;
                break;
            }
            u4NxtIdx++;
        }
        pIPRARec = pIPRARec->pNext;
    }
    if (pIPRARec == NULL)
    {
        pContext->IPRAInfo.u4NxtAutoIdx = u4NxtIdx;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisCtrlAddIPRA ()\n"));
}
