/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: ismbsm.c,v 1.1 2014/11/11 13:03:51 siva Exp $
 * *
 * * Description: ISIS MBSM Module related functions
 * *********************************************************************/


#include "cfa.h"
#include "npapi.h"
#include "ipnp.h"
#include "isincl.h"

extern UINT1 gu1IsisStatus;
extern tOsixQId gIsisCtrlQId;
extern tIsisSysContext    *gpIsisInstances;


/*****************************************************************************/
/*                                                                           */
/* Function     : IsisMbsmUpdateCardStatus                                */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the line card          */
/*                change status to the Isis task                             */
/*                                                                           */
/* Input        : pProtoMsg - Contains the slot and port information         */
/*                u1Cmd    - Line card status (UP/DOWN)                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : ISIS_SUCCESS or ISIS_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
IsisMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, UINT1 u1Cmd)
{
	tIsisMsg IsisMsg;
	tIsisQBuf          *pChainBuf = NULL;
	
	MEMSET (&IsisMsg, 0, sizeof (tIsisMsg));

	if (gu1IsisStatus != ISIS_IS_UP)
	{
		/*ISIS is not initialized*/
		return MBSM_FAILURE;

	}

	pChainBuf = (tIsisQBuf *) ISIS_MEM_ALLOC (ISIS_BUF_CHNS, (sizeof (tMbsmProtoMsg)));
	if (pChainBuf == NULL)
	{
		/*CRU Buffer Allocation Failed*/
		return MBSM_FAILURE;
	}

	ISIS_COPY_TO_CHAIN_BUF (pChainBuf,(UINT1 *) pProtoMsg, 0,
				sizeof (tMbsmProtoMsg));
	CRU_BUF_Set_U2Reserved (pChainBuf, u1Cmd);

	IsisMsg.pu1Msg = (UINT1 *) pChainBuf;
	IsisMsg.u1MsgType = ISIS_SYS_MSG_DATA;

	if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
				gIsisCtrlQId, ISIS_SYS_INT_EVT) == ISIS_FAILURE)
	{
		CRU_BUF_Release_MsgBufChain (pChainBuf, FALSE);
		return ISIS_FAILURE;
	}

	return MBSM_SUCCESS;
}


/*****************************************************************************/
/*                                                                           */
/* Function     : IsisMbsmUpdateLCStatus                                     */
/*                                                                           */
/* Description  : Initialise the line card, based on the line card status    */
/*                received                                                   */
/*                                                                           */
/* Input        : pBuf     - Buffer containing the protocol message info.    */
/*                u1Cmd    - Line card status (UP/DOWN)                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
IsisMbsmUpdateLCStatus (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd)
{
	INT4                i4RetStatus = MBSM_SUCCESS;
	tMbsmSlotInfo      *pSlotInfo = NULL;
	tMbsmProtoMsg       ProtoMsg;
	tMbsmProtoAckMsg    ProtoAckMsg;
	tIsisSysContext     *pContext = NULL;

	MEMSET (&ProtoMsg, 0, sizeof (tMbsmProtoMsg));
	MEMSET (&ProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));


	if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
				sizeof (tMbsmProtoMsg)) == 0)
	{
		/*Copy From CRU Buffer Failed*/
		i4RetStatus = MBSM_FAILURE;
	}


	if (i4RetStatus != MBSM_FAILURE)
	{
		pSlotInfo = &(ProtoMsg.MbsmSlotInfo);
		if (!MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
		{
			if (u1Cmd == MBSM_MSG_CARD_INSERT)
			{
				pContext = gpIsisInstances;
				while (pContext != NULL)
				{
					if (IsisFsMbsmHwProgram (pSlotInfo,
								pContext->SysActuals.u1SysAdminState) != ISIS_SUCCESS)
					{
						i4RetStatus = MBSM_FAILURE;
					}
					pContext = pContext->pNext;
				}
			}
		}
	}

	ProtoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
	ProtoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
	ProtoAckMsg.i4RetStatus = i4RetStatus;
	MbsmSendAckFromProto (&ProtoAckMsg);
}
