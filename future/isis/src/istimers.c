/*******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: istimers.c,v 1.10 2017/09/11 13:44:08 siva Exp $
 *
 * Description: This file contains the routines for Timer module
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE VOID        IsisTmrProcECTmrRec (tIsisSysContext *, tIsisTmrECBlk *);
PRIVATE VOID        IsisTmrResetECBit (tIsisTmrECBlk **, UINT1, UINT4);
PRIVATE INT4        IsisTmrGetECRec (tIsisSysContext *, UINT1,
                                     tIsisTmrECBlk **);

/*******************************************************************************
 * Function    : IsisTmrCreateTimerList ()
 * Description : This function creates the Timer List which will be used by ISIS
 *               Control module for maintaining various protocol timers.
 * Input(s)    : None
 * Outputs(s)  : None
 * Globals     : Modifies 'gIsisTmrListID' which will be used for all subsequent
 *               interactions with the Timer Module
 * Returns     : ISIS_SUCCESS on succesful creation of timer list,
 *               ISIS_FAILURE else
 ******************************************************************************/

PUBLIC INT4
IsisTmrCreateTimerList ()
{
    INT4                i4RetVal = ISIS_SUCCESS;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrCreateTimerList () \n"));

    i4RetVal = TmrCreateTimerList (ISIS_TASK_NAME, ISIS_SYS_TMR_EVT,
                                   NULL, &gIsisTmrListID);
    if (i4RetVal != TMR_SUCCESS)
    {
        PANIC ((ISIS_LGST, "TMR <P> : Timer List Creation Failed\n"));
        i4RetVal = ISIS_FAILURE;
    }
    else
    {
        i4RetVal = ISIS_SUCCESS;
    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrCreateTimerList () \n"));
    return (i4RetVal);
}

/*******************************************************************************
 * Function    : IsisTmrStartTimer
 * Description : This function starts the specified timer
 * Input(s)    : pTimer    - The pointer to the Timer block 
 *               TimerId   - Indicates the type of timer
 *               u4TimeOut - Time duration 
 * Outputs(s)  : None
 * Globals     : gIsisTmrListID Referred, but Not Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisTmrStartTimer (tIsisTimer * pTimer, eIsisTimerId TimerId, UINT4 u4TimeOut)
{
    INT4                i4RetVal = TMR_SUCCESS;

#ifdef ISIS_FT_ENABLED
    if (ISIS_EXT_IS_FT_STATE () != ISIS_FT_ACTIVE)
    {
        return;
    }
#endif

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrStartTimer ()\n"));
    u4TimeOut *= SYS_TIME_TICKS_IN_A_SEC;

    pTimer->TimerId = TimerId;

    TMP_PT ((ISIS_LGST, "TMR <T> : Starting Timer [ %s ]\n",
             ISIS_GET_TIMER_TYPE_STR (TimerId)));
    i4RetVal =
        (INT4) TmrStartTimer (gIsisTmrListID, &(pTimer->Timer), u4TimeOut);

    if (i4RetVal != TMR_SUCCESS)
    {
        WARNING ((ISIS_LGST, "TMR <W> : Unable To Start The Timer [ %s ]\n",
                  ISIS_GET_TIMER_TYPE_STR (TimerId)));
    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrStartTimer ()\n"));
}

/*******************************************************************************
 * Function    : IsisTmrStopTimer
 * Description : This function deletes the given timer block from the timer list
 * Input(s)    : pTimer - The pointer to the timer node
 * Outputs(s)  : None
 * Globals     : gIsisTmrListID Referred Not Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisTmrStopTimer (tIsisTimer * pTimer)
{
    INT4                i4RetVal = TMR_SUCCESS;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrStopTimer ()\n"));

    /* Check whether the timer is Active before deactivating the same
     */

    if (pTimer->TimerId != ISIS_INACTIVE_TIMER)
    {
        TMP_PT ((ISIS_LGST, "TMR <T> : Stopping Timer [ %s ]\n",
                 ISIS_GET_TIMER_TYPE_STR (pTimer->TimerId)));
        i4RetVal = TmrStopTimer (gIsisTmrListID, &pTimer->Timer);
        if (i4RetVal != TMR_SUCCESS)
        {
            WARNING ((ISIS_LGST,
                      "TMR <E> : Unable To Stop The Timer [ %s ]\n",
                      ISIS_GET_TIMER_TYPE_STR (pTimer->TimerId)));
            return;
        }
        ISIS_INIT_TIMER (*pTimer);
    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrStopTimer ()\n"));
    return;
}

/******************************************************************************
 * Function Name : IsisTmrRestartTimer
 * Description   : This functions stops the timer and restarts the timer
 *                 with new value
 * Input(s)      : pTimer  : Pointer to the Timer Node
 *                 TimerId : Timer Identifier
 *                 TimeOut : The new timeout value in seconds
 * Output(s)     : None
 * Return        : VOID
 ******************************************************************************/

PUBLIC VOID
IsisTmrRestartTimer (tIsisTimer * pTimer, eIsisTimerId TimerId, UINT4 u4TimeOut)
{
    INT4                i4RetVal = TMR_SUCCESS;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrRestartTimer () \n"));

    if (pTimer->TimerId != ISIS_INACTIVE_TIMER)
    {
        TMP_PT ((ISIS_LGST,
                 "TMR <T> : Stopping The Timer and Restarting [ %s ]\n",
                 ISIS_GET_TIMER_TYPE_STR (TimerId)));
        i4RetVal = TmrStopTimer (gIsisTmrListID, &pTimer->Timer);

        if (i4RetVal != TMR_SUCCESS)
        {
            TMP_PT ((ISIS_LGST, "TMR <E> : Could Not Stop Timer [%s]\n",
                     ISIS_GET_TIMER_TYPE_STR (TimerId)));
            return;

        }
    }

    pTimer->TimerId = TimerId;
    u4TimeOut *= 100;

    i4RetVal = (INT4) TmrStartTimer (gIsisTmrListID, &pTimer->Timer, u4TimeOut);
    if (i4RetVal != TMR_SUCCESS)
    {
        WARNING ((ISIS_LGST, "TMR <E> : Could Not Restart Timer [%s]\n",
                  ISIS_GET_TIMER_TYPE_STR (TimerId)));
        return;

    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrRestartTimer () \n"));

    return;
}

/*******************************************************************************
 * Function Name : IsisTmrProcTimeOut
 * Description   : This function processes expired timers. Each of the expired
 *                 timer is retrieved from the Expired List, and processed based
 *                 on the Timer ID.
 * Input(s)      : None
 * Outputs(s)    : None
 * Globals       : 'gIsisTmrRoutines' Referred, Not Modified 
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisTmrProcTimeOut ()
{
    tIsisTimer         *pListHead = NULL;
    tIsisTimer         *pTimer = NULL;
    eIsisTimerId        TimerId = ISIS_INACTIVE_TIMER;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrProcTimeOut ()\n"));

    /* Get the first node from the list of expired timers
     */

    pListHead = (tIsisTimer *) TmrGetNextExpiredTimer (gIsisTmrListID);

    /* Scan through the entire list of expired timers and process the same
     */

    while (pListHead != NULL)
    {
        pTimer = pListHead;
        TimerId = ((tIsisTimer *) pTimer)->TimerId;

        TMP_PT ((ISIS_LGST, "TMR <T> : Processing Timer [ %s ]\n",
                 ISIS_GET_TIMER_TYPE_STR (TimerId)));

        /* Since there is no processing for SEQ_REINT_TIMER, ignore
         */

        if (TimerId != ISIS_SEQ_REINT_TIMER)
        {
            (*gIsisTmrRoutines[TimerId]) (((tIsisTimer *) pTimer)->pContext);
        }

        /* Get the next node from the expired list
         */

        pListHead = (tIsisTimer *) TmrGetNextExpiredTimer (gIsisTmrListID);
    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrProcTimeOut ()\n"));
    return;
}

/*******************************************************************************
 * Function    : IsisTmrProcECTimeOut ()
 * Description : This function does the following:
 *               -- If the global Timer Tick is an exact multiple of
 *                  ISIS_TMR_EC1_TIME_UNIT then it processes the list of Timer
 *                  records held at the corresponding StartIdx based on the 
 *                  timer type of each of the record. If the StartIdx exceeds
 *                  the maximum index, then it is reset to the initial value
 *               -- If the global Timer Tick is an exact multiple of
 *                  ISIS_TMR_EC2_TIME_UNIT then it processes the list of Timer
 *                  records held at the corresponding StartIdx based on the 
 *                  timer type of each of the record. If the StartIdx exceeds
 *                  the maximum index, then it is reset to the initial value
 *               -- If the global Timer Tick is an exact multiple of
 *                  ISIS_TMR_EC3_TIME_UNIT then it processes the list of Timer
 *                  records held at the corresponding StartIdx based on the 
 *                  timer type of each of the record. If the StartIdx exceeds
 *                  the maximum index, then it is reset to the initial value
 *               The EC timer is re-started.
 * Input(s)    : pContext - Pointer to System Context
 * Outputs(s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisTmrProcECTimeOut (tIsisSysContext * pContext)
{
    UINT1               u1ECIdx = 0;
    UINT4               u4Dur = 0;
    tIsisTCInfo        *pTCInfo = NULL;
    tIsisTmrECBlk     **pTmrRec = NULL;
    tIsisTmrECBlk      *pProcTmrRec = NULL;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrProcECTimeOut ()\n"));

    /* LOGIC: Each EC timer block is an array holding pointers to lists of
     * different types of timers, possibly NULL. Initially Start Idx for all the
     * EC timers is '0' i.e. when the EC timer fires, timer records present at
     * index '0' (start idx) will be processed. Then start idx will be
     * incremented and when the index reaches the maximum defined for that
     * particular EC timer, it wraps around. When a timer of type 'T' is
     * started the following happens:
     *
     *    -- Let Start Index be 'S'
     *    -- Check the duration. If the duration is <= ISIS_TMR_EC1_TIME_UNIT,
     *       then the EC-I timer is selected
     *    -- Let 'D' be the duration. The timer record is inserted at an index
     *       'I' in EC-I block, where I = (S + (D / ISIS_TMR_EC1_TIME_UNIT))   
     *
     * This process is similar for other blocks also. 
     *
     * So when a timer fires, based on the Global tick count, each of the EC
     * timers are processed at the respective start indices and the start index
     * is incremented by '1' so that the next processing is done at that index.
     */

    /* A global tick count used for processing EC-I, EC-II and EC-III class of
     * timers apart from LSP time stamp purposes
     */

    pTCInfo = &(pContext->SysTimers.TmrECInfo);
    pTCInfo->u4TCount++;

    if ((pTCInfo->u4TCount % ISIS_TMR_EC1_TIME_UNIT) == 0)
    {
        /* EC-I class of timers will be processed every ISIS_TMR_EC1_TIME_UNIT 
         * seconds
         */

        /* u1EC1StartIdx is the index into the EC-I timer array block which
         * contains a list of records of various types to be processed in the
         * current tick. Once the list is processed, the start id is incremented
         * modulo (ISIS_EC1_MAX_DUR / ISIS_TMR_EC1_TIME_UNIT). 
         */

        TMP_PT ((ISIS_LGST, "TMR <T> : Processing EC-I Timers @ Index [ %u ]\n",
                 pTCInfo->u1EC1StartIdx));
        pTCInfo->u1EC1StartIdx++;
        pTCInfo->u1EC1StartIdx %=
            (UINT1) (ISIS_EC1_MAX_DUR / ISIS_TMR_EC1_TIME_UNIT);
        pTmrRec = &(pTCInfo->apTmrEC1[pTCInfo->u1EC1StartIdx]);

        while (*pTmrRec != NULL)
        {
            pProcTmrRec = *pTmrRec;
            *pTmrRec = (*pTmrRec)->pNext;
            pProcTmrRec->pNext = NULL;
            IsisTmrProcECTmrRec (pContext, pProcTmrRec);
        }
    }

    if ((pTCInfo->u4TCount % ISIS_TMR_EC2_TIME_UNIT) == 0)
    {
        /* EC-II class of timers will be processed every ISIS_TMR_EC2_TIME_UNIT 
         * seconds
         */

        /* u1EC2StartIdx is the index into the EC-II timer array block which
         * contains a list of records of various types to be processed in the
         * current tick. Once the list is processed, the start id is incremented
         * modulo (ISIS_EC2_MAX_DUR / ISIS_TMR_EC2_TIME_UNIT). 
         */

        TMP_PT ((ISIS_LGST,
                 "TMR <T> : Processing EC-II Timers @ Index [ %u ]\n",
                 pTCInfo->u1EC2StartIdx));
        pTCInfo->u1EC2StartIdx++;
        pTCInfo->u1EC2StartIdx %= (UINT1)
            (ISIS_EC2_MAX_DUR / ISIS_TMR_EC2_TIME_UNIT);
        pTmrRec = &(pTCInfo->apTmrEC2[pTCInfo->u1EC2StartIdx]);

        while (*pTmrRec != NULL)
        {
            pProcTmrRec = *pTmrRec;
            *pTmrRec = (*pTmrRec)->pNext;
            pProcTmrRec->pNext = NULL;
            IsisTmrProcECTmrRec (pContext, pProcTmrRec);
        }
    }

    if ((pTCInfo->u4TCount % ISIS_TMR_EC3_TIME_UNIT) == 0)
    {
        /* EC-III class of timers will be processed every ISIS_TMR_EC3_TIME_UNIT
         * seconds
         */

        /* u1EC3StartIdx is the index into the EC-III timer array block which
         * contains a list of records of various types to be processed in the
         * current tick. Once the list is processed, the start id is incremented
         * modulo (ISIS_EC3_MAX_DUR / ISIS_TMR_EC3_TIME_UNIT). 
         */

        TMP_PT ((ISIS_LGST,
                 "TMR <T> : Processing EC-III Timers @ Index [ %u ]\n",
                 pTCInfo->u1EC3StartIdx));
        pTCInfo->u1EC3StartIdx++;
        pTCInfo->u1EC3StartIdx %= (UINT1)
            (ISIS_EC3_MAX_DUR / ISIS_TMR_EC3_TIME_UNIT);
        pTmrRec = &(pTCInfo->apTmrEC3[pTCInfo->u1EC3StartIdx]);

        while (*pTmrRec != NULL)
        {
            pProcTmrRec = *pTmrRec;
            *pTmrRec = (*pTmrRec)->pNext;
            pProcTmrRec->pNext = NULL;

            /* Check the Remaining time for the Record. if remaining time
             * is non-zero, then start the timer for the remaining time,
             * else process the timer record.
             */

            if (pProcTmrRec->u2RemTime == 0)
            {
                IsisTmrProcECTmrRec (pContext, pProcTmrRec);
            }
            else
            {
                u4Dur = pProcTmrRec->u2RemTime;
                if (pProcTmrRec->u2RemTime >= (ISIS_EC3_MAX_DUR -
                                               ISIS_TMR_EC3_TIME_UNIT))
                {
                    /* The Remaining Time is still greater than Maximum
                     * duration, Hence decrement the remaining time by
                     * Maximum duration and start the timer for maximum duration
                     */

                    pProcTmrRec->u2RemTime -= (ISIS_EC3_MAX_DUR -
                                               ISIS_TMR_EC3_TIME_UNIT);

                    /* Insert the Record at the Head
                     */

                    u1ECIdx = (UINT1)
                        ISIS_TMR_INC (pContext->SysTimers.TmrECInfo.
                                      u1EC3StartIdx, (ISIS_EC3_MAX_DUR -
                                                      ISIS_TMR_EC3_TIME_UNIT),
                                      ISIS_TMR_EC3_TIME_UNIT);

                    u1ECIdx %=
                        (UINT1) (ISIS_EC3_MAX_DUR / ISIS_TMR_EC3_TIME_UNIT);
                    pProcTmrRec->pNext = pTCInfo->apTmrEC3[u1ECIdx];
                    pTCInfo->apTmrEC3[u1ECIdx] = pProcTmrRec;
                }

                else if (u4Dur < ISIS_EC1_MAX_DUR)
                {
                    u1ECIdx = pContext->SysTimers.TmrECInfo.u1EC1StartIdx;
                    u1ECIdx = (UINT1) ISIS_TMR_INC_EC1 (u1ECIdx, u4Dur);
                    u1ECIdx = (UINT1) (u1ECIdx %
                                       (ISIS_EC1_MAX_DUR /
                                        ISIS_TMR_EC1_TIME_UNIT));
                    pProcTmrRec->u2RemTime = 0;
                    pProcTmrRec->pNext =
                        pContext->SysTimers.TmrECInfo.apTmrEC1[u1ECIdx];
                    pContext->SysTimers.TmrECInfo.apTmrEC1[u1ECIdx] =
                        pProcTmrRec;
                }
                else if (u4Dur < ISIS_EC2_MAX_DUR)
                {
                    u1ECIdx = pContext->SysTimers.TmrECInfo.u1EC2StartIdx;
                    u1ECIdx = (UINT1) ISIS_TMR_INC (u1ECIdx, u4Dur,
                                                    ISIS_TMR_EC2_TIME_UNIT);
                    u1ECIdx = (UINT1)
                        (u1ECIdx % (ISIS_EC2_MAX_DUR / ISIS_TMR_EC2_TIME_UNIT));

                    pProcTmrRec->u2RemTime = 0;
                    pProcTmrRec->pNext =
                        pContext->SysTimers.TmrECInfo.apTmrEC2[u1ECIdx];
                    pContext->SysTimers.TmrECInfo.apTmrEC2[u1ECIdx] =
                        pProcTmrRec;
                    pProcTmrRec->u2RemTime = 0;
                }
                else
                {
                    u1ECIdx = pContext->SysTimers.TmrECInfo.u1EC3StartIdx;
                    u1ECIdx = (UINT1) ISIS_TMR_INC (u1ECIdx, u4Dur,
                                                    ISIS_TMR_EC3_TIME_UNIT);
                    u1ECIdx = (UINT1)
                        (u1ECIdx % (ISIS_EC3_MAX_DUR / ISIS_TMR_EC3_TIME_UNIT));
                    pProcTmrRec->u2RemTime = 0;
                    pProcTmrRec->pNext =
                        pContext->SysTimers.TmrECInfo.apTmrEC3[u1ECIdx];
                    pContext->SysTimers.TmrECInfo.apTmrEC3[u1ECIdx] =
                        pProcTmrRec;
                }

            }
        }
    }

    /* All the records have been processed. Noe start the EC timer again for the
     * next tick
     */

    pContext->SysTimers.SysECTimer.pContext = pContext;
    IsisTmrStartTimer (&(pContext->SysTimers.SysECTimer),
                       ISIS_EQUV_CLSS_TIMER, 1);

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrProcECTimeOut ()\n"));
    return;
}

/*******************************************************************************
 * Function    : IsisTmrProcECTmrRec ()
 * Description : This function processes the timers records included in
 *               'pTmrRec'. Each of the records is of a different type viz,
 *               PSNP, CSNP, Hello, Holding, LSP Transmission etc. appropriate
 *               handling functions are invoked based on the type of timer
 * Input(s)    : pContext - Pointer to the system context
 *               pTmrRec  - Pointer to Timer Record
 * Outputs(s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisTmrProcECTmrRec (tIsisSysContext * pContext, tIsisTmrECBlk * pTmrRec)
{
    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrProcECTmrRec ()\n"));

    switch (pTmrRec->u1Type)
    {
        case ISIS_ECT_L1LANHELLO:
        case ISIS_ECT_P2P_HELLO:
            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "LEVEL-1 HELLO TIMER"));
            IsisAdjProcHelloTimeOut (pContext, pTmrRec, ISIS_LEVEL1);
            break;

        case ISIS_ECT_L2LANHELLO:
            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "LEVEL-2 HELLO TIMER"));
            IsisAdjProcHelloTimeOut (pContext, pTmrRec, ISIS_LEVEL2);
            break;

        case ISIS_ECT_P2P_ISH:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "P2P ISH TIMER"));
            IsisAdjProcISHTimeOut (pContext, pTmrRec);
            break;

        case ISIS_ECT_HOLDING:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "HOLDING TIMER"));
            IsisAdjProcHoldingTimeOut (pContext, pTmrRec);
            break;

        case ISIS_ECT_L1_LSP_TX:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "LEVEL-1 LSP TX TIMER"));
            IsisUpdProcLSPTxTimeOut (pContext, pTmrRec, ISIS_LEVEL1);
            break;

        case ISIS_ECT_L2_LSP_TX:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "LEVEL-2 LSP TX TIMER"));
            IsisUpdProcLSPTxTimeOut (pContext, pTmrRec, ISIS_LEVEL2);
            break;

        case ISIS_ECT_L1_CSNP:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "LEVEL-1 CSNP TIMER"));
            IsisUpdProcCSNPTimeOut (pContext, pTmrRec, ISIS_LEVEL1);
            break;

        case ISIS_ECT_L2_CSNP:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "LEVEL-2 CSNP TIMER"));
            IsisUpdProcCSNPTimeOut (pContext, pTmrRec, ISIS_LEVEL2);
            break;

        case ISIS_ECT_L1_PSNP:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "LEVEL-1 PSNP TIMER"));
            IsisUpdProcPSNPTimeOut (pContext, pTmrRec, ISIS_LEVEL1);
            break;

        case ISIS_ECT_L2_PSNP:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "LEVEL-2 PSNP TIMER"));
            IsisUpdProcPSNPTimeOut (pContext, pTmrRec, ISIS_LEVEL2);
            break;

        case ISIS_ECT_ZERO_AGE:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "ZERO AGE TIMER"));
            IsisUpdProcZeroAgeTimeOut (pContext, pTmrRec);
            break;

        case ISIS_ECT_MAX_AGE:

            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "MAX AGE TIMER"));
            IsisUpdProcMaxAgeTimeOut (pContext, pTmrRec);
            break;

        case ISIS_GR_ECT_T1:
            TMP_PT ((ISIS_LGST, "TMR <T> : Processing [ %s ]\n",
                     "T1 TIMER for Level1"));
            IsisAdjProcT1TimeOut (pContext, pTmrRec);
            break;

    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrProcECTmrRec ()\n"));
    return;
}

/*****************************************************************************
 * Function     : IsisTmrStartECTimer ()
 * Description  : This routine checks for existence of a timer record which is
 *                of the same type as 'u1Type' based on the duration. If found 
 *                this routine sets the appropriate bit corresponding to 'u4Id'. 
 *                Otherwise it creates a new block and inserts the block into
 *                the list appropriately.  
 * Input (s)    : pContext    - Pointer to the system context
 *              : u1Type      - Type of the timer 
 *                u4Id        - ID whose corresponding Mask bit is to be set
 *                u2Dur       - Duration of the timer
 * Output (s)   : pu1ECTId    - An ID which is a combination of Equivalence
 *                              Class Timer ID and the block index where a timer
 *                              block of given duration can fit
 * Globals      : Not Referred or Modified             
 * Return (s)   : VOID
 ******************************************************************************/

PUBLIC VOID
IsisTmrStartECTimer (tIsisSysContext * pContext, UINT1 u1Type,
                     UINT4 u4Id, UINT4 u4Dur, UINT1 *pu1ECTId)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT2               u2RemTime = 0;
    tIsisTmrECBlk      *pTmrRec = NULL;
    tIsisTmrECBlk      *pTmrECRec = NULL;
    UINT1              *pu1info = NULL;

#ifdef ISIS_FT_ENABLED
    if (ISIS_EXT_IS_FT_STATE () != ISIS_FT_ACTIVE)
    {
        return;
    }
#endif

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrStartECTimer ()\n"));

    /* Get the Timer Index which is a combination of Equivalence Class Timer Id
     * and the Array index which holds a list of timer records where the current
     * block will fit. Also retrieve the first record of the list so that the
     * current block can be inserted at appropriate position
     */

    if ((u1Type == ISIS_ECT_L1LANHELLO) || (u1Type == ISIS_ECT_L2LANHELLO)
        || (u1Type == ISIS_ECT_P2P_ISH) || (u1Type == ISIS_ECT_P2P_HELLO))
    {
        u4Dur = ISIS_CONV_MILLISEC_TO_SEC (u4Dur);
    }

    /* If the Duration for which the timer is started is greater than 
     * ISIS_EC3_MAX_DUR, note the remaining time
     */

    if (u4Dur > (ISIS_EC3_MAX_DUR - ISIS_TMR_EC3_TIME_UNIT))
    {
        u2RemTime =
            (UINT2) (u4Dur - (ISIS_EC3_MAX_DUR - ISIS_TMR_EC3_TIME_UNIT));

        /* Round off the Remaining time to the nearest multiple of
         * ISIS_TMR_EC3_TIME_UNIT
         */

        u2RemTime = (UINT2) (ISIS_DIVR (u2RemTime, ISIS_TMR_EC3_TIME_UNIT));
        u2RemTime *= ISIS_TMR_EC3_TIME_UNIT;
        u4Dur = (UINT4) (u4Dur - u2RemTime);
    }

    i4RetVal = IsisTmrGetECBlk (pContext, u4Dur, u1Type, &pTmrRec, pu1ECTId);

    TMP_PT ((ISIS_LGST,
             "TMR <T> : Processing Timer Type [ %s ], Duration [ %u ]\n",
             ISIS_GET_EC_TIMER_TYPE (u1Type), u4Dur));

    if (i4RetVal == ISIS_MATCH_REC)
    {
        pu1info = (UINT1 *) pTmrRec->pInfo;
        IsisUtlSetBPat (&pu1info, u4Id);
        pTmrRec->pInfo = (VOID *) pu1info;
        TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrStartECTimer ()\n"));
        return;
    }

    /* No block of timer satisfying the given type exist. Create a new block and
     * update the Bit Mask appropriately.
     */

    pTmrECRec = (tIsisTmrECBlk *)
        ISIS_MEM_ALLOC (ISIS_BUF_ECTS, sizeof (tIsisTmrECBlk));
    if (pTmrECRec == NULL)
    {
        /* Now, Now, Now, We don't seem to have enough memory to complete the
         * assigned task. 
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : EC Timer Records\n"));
        TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrStartECTimer ()\n"));
        return;
    }
    else
    {
        /* Set the values for the timer block 
         */
        pTmrECRec->u1Type = u1Type;
        pTmrECRec->pInfo = NULL;
        pTmrECRec->u2RemTime = u2RemTime;

        /* The first 4 bytes of pInfo holds the length
         * of 'pInfo'. 'pInfo', along with the length,
         * is allocated when the very first bit is
         * being set
         */

        pTmrECRec->pNext = NULL;

        pu1info = (UINT1 *) pTmrECRec->pInfo;
        IsisUtlSetBPat (&pu1info, u4Id);
        pTmrECRec->pInfo = (VOID *) pu1info;

        if (pTmrRec != NULL)
        {
            /* A List of nodes is already present at the index. Insert at the
             * head
             */

            /* Both CSNPs and LSPs Timers use the same throttle count, for
             * scheduling CSNPs and LSPs respectively. But the throttle count
             * is incremented once every second in LSPTxTimeOut Routine. If
             * the LSPTxTimer Fires first, the throttle count will be
             * incremented and hence there is a probability that the throttle
             * count may not match the expected value for CSNP Transmissions.
             * Hence we make sure that CSNP timer is processed first by placing
             * CSNP Records at the head and the other Records next to CSNP
             * Records.
             *
             * For Eg. if CSNPs are to be scheduled every 10 secs. then at the
             * 10th second, during Time Out, the LSPTxTimeOut Routine increments             
             * the throttle to 11, which will not match CSNP throttle count of
             * modulo 10.
             */

            while ((pTmrRec->pNext != NULL)
                   && ((pTmrRec->u1Type == ISIS_ECT_L1_CSNP)
                       || (pTmrRec->u1Type == ISIS_ECT_L2_CSNP)))
            {
                pTmrRec = pTmrRec->pNext;
            }

            pTmrECRec->pNext = pTmrRec->pNext;
            pTmrRec->pNext = pTmrECRec;
        }
        else
        {
            IsisTmrSetECBlk (pContext, *pu1ECTId, pTmrECRec);
        }
    }
    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrStartECTimer ()\n"));
}

/*****************************************************************************
 * Function     : IsisTmrStopECTimer ()
 * Description  : This routine retrieves the Timer ID (EC1, EC2 or EC3) and the
 *                appropriate array index where the timer specified by 'u1Type'
 *                can be found. It then invokes IsisTmrResetECBit () to stop the
 *                timer.
 * Input (s)    : pContext  - Pointer to the system context
 *              : u1Type    - Type of the timer 
 *                u4Id      - Id associated with the record which is supposed to
 *                            be reset to stop the timer
 *                u1ECTId   - EC Timer ID
 * Output (s)   : None 
 * Globals      : Not Referred or Modified 
 * Returns      : VOID
 *****************************************************************************/

PUBLIC VOID
IsisTmrStopECTimer (tIsisSysContext * pContext, UINT1 u1Type, UINT4 u4Id,
                    UINT1 u1ECTId)
{
    UINT1               u1ECBlk = 0;
    UINT1               u1BlkIdx = 0;
    tIsisTmrECBlk     **ppu1TmrMask = NULL;

    /* Equivalence Class of timers are of 3 durations and are classified as EC1,
     * EC2 and EC3. When a EC timer is started the routine returns the EC timer
     * ID (either EC1, EC2 or EC3) and the array index where the timer block is
     * held (this is based on the duration of the timer). The combination of EC
     * timer ID and the array index is specified by the 'u1ECTId' argument. 
     * Using appropriate masks these two values are retrieved and the Reset
     * routine invoked
     */

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrStopECTimer ()\n"));

    u1ECBlk = (UINT1) (u1ECTId & ISIS_EC_BLK_MASK);
    u1BlkIdx = (UINT1) (u1ECTId & ISIS_EC_IDX_MASK);

    switch (u1ECBlk)
    {
        case ISIS_ECT_BLK_1:
            if (u1BlkIdx < ISIS_MAX_EC1_BLKS)
            {
                ppu1TmrMask =
                    &(pContext->SysTimers.TmrECInfo.apTmrEC1[u1BlkIdx]);
            }
            break;

        case ISIS_ECT_BLK_2:
            if (u1BlkIdx < ISIS_MAX_EC2_BLKS)
            {
                ppu1TmrMask =
                    &(pContext->SysTimers.TmrECInfo.apTmrEC2[u1BlkIdx]);
            }
            break;

        case ISIS_ECT_BLK_3:
            if (u1BlkIdx < ISIS_MAX_EC3_BLKS)
            {
                ppu1TmrMask =
                    &(pContext->SysTimers.TmrECInfo.apTmrEC3[u1BlkIdx]);
            }
            break;

        default:

            TMP_PT ((ISIS_LGST,
                     "TMR <E> : Invalid Timer Id [ %u ]\n", u1ECBlk));
            TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrStopECTimer ()\n"));
            return;
    }

    if (ppu1TmrMask != NULL)
    {
        IsisTmrResetECBit (ppu1TmrMask, u1Type, u4Id);
    }
    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrStopECTimer ()\n"));
}

/*******************************************************************************
 * Function     : IsisTmrResetECBit ()
 * Description  : This routine resets the appropriate bit in the pTmrECBlk based
 *                on the given 'u4Id'.
 * Input (s)    : pTmrECBlk   - Pointer to the Timer EC block 
 *              : u1Type      - The Type of the timer 
 *                u4Id        - Timer ID whosecorresponding is to be reset
 * Output (s)   : None 
 * Globals      : Not Referred or Modified              
 * RETURN (s)   : VOID
 ******************************************************************************/

PRIVATE VOID
IsisTmrResetECBit (tIsisTmrECBlk ** pTmrECBlk, UINT1 u1Type, UINT4 u4Id)
{
    UINT1               u1DelFlag = ISIS_FALSE;
    tIsisTmrECBlk      *pTmrECRec = NULL;
    tIsisTmrECBlk      *pPrevECRec = NULL;
    UINT1              *pu1info = NULL;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrResetECBit ()\n"));

    pTmrECRec = *pTmrECBlk;

    while (pTmrECRec != NULL)
    {
        /* Each bucket in an Equivalence Class Timer Array can hold various
         * types of timers. So travel the list and get the appropriate type
         */

        if (pTmrECRec->u1Type == u1Type)
        {
            /* Got to the correct block */
            pu1info = (UINT1 *) pTmrECRec->pInfo;
            u1DelFlag = IsisUtlReSetBPat (&pu1info, u4Id);
            pTmrECRec->pInfo = (VOID *) pu1info;
            break;
        }
        pPrevECRec = pTmrECRec;
        pTmrECRec = pTmrECRec->pNext;
    }
    if (u1DelFlag == ISIS_TRUE)
    {
        /* Entire Information field has been removed by IsisUtlReSetBPat ()
         * routine. Hence the record which held the information can be freed
         */

        if (pPrevECRec == NULL)
        {
            /* This is the very first record being deleted. Advance the head 
             */

            *pTmrECBlk = pTmrECRec->pNext;
        }
        else
        {
            /* A node somewhere in the middle being deleted
             */

            pPrevECRec->pNext = pTmrECRec->pNext;
        }
        pTmrECRec->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrECRec);
        pTmrECRec = NULL;
    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrResetECBit ()\n"));
}

/*****************************************************************************
 * Function     : IsisTmrRestartECTimer()
 * Description  : This routine Restarts the specified Timer. This is achieved
 *                by Stopping the timer firstand then starting it agian for the
 *                specified duration
 * Input (s)    : pContext    - Pointer to the Context
 *              : u1Type      - Type of the timer 
 *                u4Id        - Record Id associated with the given timer 
 *                u2Dur       - Duration of the timer  
 * Output (s)   : pu1ECTId    - An ID which is a combination of Equivalence
 *                              Class Timer ID and the block index where a timer
 *                              block of given duration can fit
 * Globals      : Not Referred or Modified            
 * RETURN (s)   : VOID
 ******************************************************************************/

PUBLIC VOID
IsisTmrRestartECTimer (tIsisSysContext * pContext, UINT1 u1Type,
                       UINT4 u4Id, UINT4 u4Dur, UINT1 *pu1ECTId)
{
    IsisTmrStopECTimer (pContext, u1Type, u4Id, *pu1ECTId);
    IsisTmrStartECTimer (pContext, u1Type, u4Id, u4Dur, pu1ECTId);
}

/*****************************************************************************
 * Function     : IsisTmrSetECBlk()
 * Description  : This routine sets the Timer Record of the Equivalence Timers
 *                given by the ECT Index
 * Input (s)    : pContext    - Pointer to the system context
 *                u1ECTId     - Timer ID which is a combination of 
 *                              ISIS_ECT_BLK_X and the array index 
 *              : pTmrRec     - The Timer Record which is to be set
 * Output (s)   : None
 * Globals      : Not Referred or Modified               
 * Returns      : VOID
 ******************************************************************************/

PUBLIC VOID
IsisTmrSetECBlk (tIsisSysContext * pContext, UINT1 u1ECTId,
                 tIsisTmrECBlk * pTmrRec)
{
    UINT1               u1ECBlk = 0;
    UINT1               u1BlkIdx = 0;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrSetECBlk ()\n"));

    u1ECBlk = (UINT1) (u1ECTId & ISIS_EC_BLK_MASK);
    u1BlkIdx = (UINT1) (u1ECTId & ISIS_EC_IDX_MASK);

    switch (u1ECBlk)
    {
        case ISIS_ECT_BLK_1:
            if (u1BlkIdx < ISIS_MAX_EC1_BLKS)
            {
                pContext->SysTimers.TmrECInfo.apTmrEC1[u1BlkIdx] = pTmrRec;
            }
            break;

        case ISIS_ECT_BLK_2:
            if (u1BlkIdx < ISIS_MAX_EC2_BLKS)
            {
                pContext->SysTimers.TmrECInfo.apTmrEC2[u1BlkIdx] = pTmrRec;
            }
            break;

        case ISIS_ECT_BLK_3:
            if (u1BlkIdx < ISIS_MAX_EC3_BLKS)
            {
                pContext->SysTimers.TmrECInfo.apTmrEC3[u1BlkIdx] = pTmrRec;
            }
            break;

        default:

            TMP_PT ((ISIS_LGST, "TMR <E> : Invalid TimerId\n"));
            break;
    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrSetECBlk ()\n"));
}

/*****************************************************************************
 * Function    : IsisTmrGetECBlk()
 * Description : This routine finds out the correct block in one of the 3
 *               equivalence class timers where the given duration fits
 *               appropriately. It also ouputs an ID which is a combination of
 *               EC Timer Id viz. ISIS_ECT_BLK_1, ISIS_ECT_BLK_2 or
 *               ISIS_ECT_BLK_3, and the index into the timer array where the
 *               given duration fits.
 * Input (s)   : pContext    - Pointer to the system context
 *             : u2Dur       - Duration of the timer to be started 
 * Output (s)  : pTmrRec     - First record of the list held in the array index
 *                             calculated
 *               pu1ECTId    - Timer ID which is a combination of ISIS_ECT_BLK_X
 *                             and the array index 
 * Globals     : Not Referred or Modified
 * Return (s)  : VOID
 ******************************************************************************/

PUBLIC INT4
IsisTmrGetECBlk (tIsisSysContext * pContext, UINT4 u4Dur, UINT1 u1Type,
                 tIsisTmrECBlk ** pTmrRec, UINT1 *pu1ECTId)
{
    INT4                i4RetVal = ISIS_NON_MATCH_REC;
    UINT1               u1Idx = 0;
    tIsisTmrECBlk      *pTmrECRec = NULL;

    /* Refer to Design Document for more details on Equivalence Class of Timers.
     * Each EC timer is an array whose indices hold list of timer blocks
     * separated in time by a specified duration. For e.g., EC-1 timers are of
     * 1 sec. intervals, EC-2 of 10 secs. and EC-3 of 100 secs. This means in
     * the case of EC-1, blocks present in index 'i' expires at time 'T' and
     * blocks present in index 'i + 1' expires at time 'T + 1', for EC-2 it
     * will be 'T' and 'T + 10' and for EC-3 it will be 'T' and 'T + 100'.
     */

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrGetECBlk ()\n"));

    if (u4Dur < ISIS_EC1_MAX_DUR)
    {
        /* The block fits into the first Equivalence Class of timers, whose
         * interval is of 1 Sec. duration and holds timers upto 30 Sec. duration
         */

        /* Retrieve the Array Index where a block of this duration can fit and
         * the record which matchs the given type.
         */

        u1Idx = pContext->SysTimers.TmrECInfo.u1EC1StartIdx;
        u1Idx = (UINT1) ISIS_TMR_INC_EC1 (u1Idx, u4Dur);
        u1Idx = (UINT1) (u1Idx % (ISIS_EC1_MAX_DUR / ISIS_TMR_EC1_TIME_UNIT));
        pTmrECRec = pContext->SysTimers.TmrECInfo.apTmrEC1[u1Idx];

        i4RetVal = IsisTmrGetECRec (pContext, u1Type, &pTmrECRec);
        (*pTmrRec) = pTmrECRec;

        /* Then get the Timer id which is a combination of 
         * Equilvalence Class ID & the Array Index where a block of given
         * duration will fit
         */

        *pu1ECTId = (UINT1) (ISIS_ECT_BLK_1 | (ISIS_EC_IDX_MASK & u1Idx));

    }
    else if (u4Dur < ISIS_EC2_MAX_DUR)
    {
        /* The block fits into the second Equivalence Class of timers, whose
         * interval is of 10 Sec. duration and holds timers upto 300 Sec. 
         * duration
         */

        /* Retrieve the Array Index where a block of this duration can fit and
         * the record which matchs the given type.
         */

        u1Idx = pContext->SysTimers.TmrECInfo.u1EC2StartIdx;
        u1Idx = (UINT1) ISIS_TMR_INC (u1Idx, u4Dur, ISIS_TMR_EC2_TIME_UNIT);
        u1Idx = (UINT1) (u1Idx % (ISIS_EC2_MAX_DUR / ISIS_TMR_EC2_TIME_UNIT));
        pTmrECRec = pContext->SysTimers.TmrECInfo.apTmrEC2[u1Idx];

        i4RetVal = IsisTmrGetECRec (pContext, u1Type, &pTmrECRec);
        (*pTmrRec) = pTmrECRec;

        /* Then get the Timer id which is a combination of 
         * Equilvalence Class ID & the Array Index where a block of given
         * duration will fit
         */

        *pu1ECTId = (UINT1) (ISIS_ECT_BLK_2 | (ISIS_EC_IDX_MASK & u1Idx));

    }
    else if (u4Dur < ISIS_EC3_MAX_DUR)
    {
        /* The block fits into the third Equivalence Class of timers, whose
         * interval is of 100 Sec. duration and holds timers upto 1200 Sec. 
         * duration
         */

        /* Retrieve the Array Index where a block of this duration can fit and
         * the record which matchs the given type.
         */

        u1Idx = pContext->SysTimers.TmrECInfo.u1EC3StartIdx;
        u1Idx = (UINT1) ISIS_TMR_INC (u1Idx, u4Dur, ISIS_TMR_EC3_TIME_UNIT);
        u1Idx = (UINT1) (u1Idx % (ISIS_EC3_MAX_DUR / ISIS_TMR_EC3_TIME_UNIT));
        pTmrECRec = pContext->SysTimers.TmrECInfo.apTmrEC3[u1Idx];

        i4RetVal = IsisTmrGetECRec (pContext, u1Type, &pTmrECRec);
        (*pTmrRec) = pTmrECRec;

        /* Then get the Timer id which is a combination of 
         * Equilvalence Class ID & the Array Index where a block of given
         * duration will fit
         */

        *pu1ECTId = (UINT1) (ISIS_ECT_BLK_3 | (ISIS_EC_IDX_MASK & u1Idx));

    }
    else
    {
        /* LOGIC : To start the timer whose duration exceeds ISIS_EC3_MAX_DUR.
         *
         * Now Scan all the Records in EC3 Block, and check for the record
         * whose remaining time is same as the given duration and and record
         * type matches. for example we have to start the timer for say 1500
         * seconds of type say ISIS_ECT_MAX_AGE. let the start index of  EC3
         * Block be '3'. Now scan for the record of type ISIS_ECT_MAX_AGE and
         * whose remaining time is (1500 - (1 * ISIS_EC3_MAX_DUR)), i.e. 1400
         * seconds. if no record is available, go the next index (5), and search
         * for record whose remaining time is (1500 - (2 * ISIS_EC3_MAX_DUR))
         * i.e 1300 seconds and so on till a matching record is found.
         * if no matching record is found, then retrieve a block for duration
         * ISIS_EC3_MAX_DUR, and return. The Calling function will allocate 
         * a new record and fill the remaining time with the value 
         * (1500 - ISIS_EC3_MAX_DUR).
         */

        /* Round of the Duration to nearest multiple of ISIS_TMR_EC3_TIME_UNIT
         */

        u4Dur = ISIS_DIVR (u4Dur, ISIS_TMR_EC3_TIME_UNIT) *
            ISIS_TMR_EC3_TIME_UNIT;

        /* search for matching records in all the rows
         */

        /* Always a new record is used for CSNP Hence get the matching
         * record only if the timer type is not CSNP
         */

        if ((u1Type != ISIS_ECT_L1_CSNP) && (u1Type != ISIS_ECT_L2_CSNP))
        {

            for (u1Idx =
                 (UINT1) (pContext->SysTimers.TmrECInfo.u1EC3StartIdx + 1);
                 (u1Idx < ISIS_MAX_EC3_BLKS)
                 && (u1Idx != pContext->SysTimers.TmrECInfo.u1EC3StartIdx);
                 (u1Idx =
                  (UINT1) ((u1Idx +
                            1) % (ISIS_EC3_MAX_DUR / ISIS_TMR_EC3_TIME_UNIT))))
            {
                u4Dur -= ISIS_TMR_EC3_TIME_UNIT;
                *pTmrRec = pContext->SysTimers.TmrECInfo.apTmrEC3[u1Idx];

                while ((*pTmrRec) != NULL)
                {
                    /* Search for the Timer Record of Given Type
                     */

                    if (((*pTmrRec)->u1Type == u1Type)
                        && ((*pTmrRec)->u2RemTime == u4Dur))
                    {
                        i4RetVal = ISIS_MATCH_REC;
                        *pu1ECTId = (UINT1)
                            (ISIS_ECT_BLK_3 | (ISIS_EC_IDX_MASK & u1Idx));
                        break;
                    }
                    (*pTmrRec) = (*pTmrRec)->pNext;
                }

                if (i4RetVal == ISIS_MATCH_REC)
                {
                    break;
                }
            }
        }
        if (i4RetVal != ISIS_MATCH_REC)
        {
            /* No Matching record found, Hence retrieve the block for
             * duration ISIS_EC3_MAX_DUR
             */

            u1Idx = (UINT1)
                ISIS_TMR_INC (pContext->SysTimers.TmrECInfo.u1EC3StartIdx,
                              ISIS_EC3_MAX_DUR, ISIS_TMR_EC3_TIME_UNIT);

            u1Idx %= (UINT1) (ISIS_EC3_MAX_DUR / ISIS_TMR_EC3_TIME_UNIT);

            /* Get the first record of the list held at the index 'u1Idx'
             */

            *pTmrRec = pContext->SysTimers.TmrECInfo.apTmrEC3[u1Idx];
            *pu1ECTId = (UINT1) (ISIS_ECT_BLK_3 | (ISIS_EC_IDX_MASK & u1Idx));
        }
    }
    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrGetECBlk ()\n"));
    return (i4RetVal);
}

/*****************************************************************************
 * Function    : IsisTmrGetECRec ()
 * Description : This routine searches the record in the given equivalence
 *               class timer block for the matching type (except CSNP).
 *               if no matching record is found, then the first record in the
 *               given equivalence class block is returned.
 * Input (s)   : pContext    - Pointer to the system context
 *               u1Type      - The type of the timer record
 *               u1Idx       - The pointer to the index in the timer 
 *                             block
 *               u4TimeUnit  - The unit of time in seconds for the given 
 *                             equivalence class block
 *               u4MaxDur    - The maximum time duration for the given
 *                             equivalence class block
 *               pTmrRec     - The pointer to equivalence class timer block
 * Output (s)  : pTmrRec     - The matching timer record if found, else the
 *                             first record in the timer block.
 * Globals     : Not Referred or Modified
 * Return (s)  : ISIS_MATCH_REC if the any of the record matches with the given
 *                              type in the given block
 *               ISIS_NON_MATCH_REC else.                 
 ******************************************************************************/

PRIVATE INT4
IsisTmrGetECRec (tIsisSysContext * pContext, UINT1 u1Type,
                 tIsisTmrECBlk ** pTmrRec)
{
    INT4                i4RetVal = ISIS_NON_MATCH_REC;
    tIsisTmrECBlk      *pTmrECRec = NULL;
    tIsisTmrECBlk      *pFirstRec = NULL;

    UNUSED_PARAM (pContext);

    pFirstRec = *pTmrRec;
    pTmrECRec = pFirstRec;

    if ((u1Type != ISIS_ECT_L1_CSNP) && (u1Type != ISIS_ECT_L2_CSNP))
    {
        while (pTmrECRec != NULL)
        {
            /* Search for the Timer Record of Given Type
             */

            if (pTmrECRec->u1Type == u1Type)
            {
                (*pTmrRec) = pTmrECRec;
                i4RetVal = ISIS_MATCH_REC;
                break;
            }
            pTmrECRec = pTmrECRec->pNext;
        }
    }

    if (i4RetVal != ISIS_MATCH_REC)
    {
        /* Matching Timer record not found, Hence note the first record
         */

        *pTmrRec = pFirstRec;
    }
    return i4RetVal;
}

/******************************************************************************
 * Function Name  : IsisTmrDelLSPTmrLinks ()
 * Description    : This routine Delinks all the records that are linked with
 *                  Equal class Timer blocks. 
 * Inputs         : pLspRec   - Pointer to the LSP Record
 * Outputs        : VOID 
 * Globals        : None 
 * Returns        : None 
 *****************************************************************************/

PUBLIC VOID
IsisTmrDelLSPTmrLinks (tIsisLSPEntry * pLspRec)
{
    tIsisLSPEntry      *pLsp;
    tIsisLSPEntry      *pNextLsp;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrDelLSPTmrLinks ()\n"));

    pLsp = pLspRec->pNxtTimerLink;

    while (pLsp != NULL)
    {
        pNextLsp = pLsp->pNxtTimerLink;
        IsisUpdDelLspTimerNode (pLsp);
        pLsp = pNextLsp;
    }

    ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pLspRec);
    pLspRec = NULL;

    TMP_EE ((ISIS_LGST, "TMR <X> : Exiting IsisTmrDelLSPTmrLinks ()\n"));
}

/******************************************************************************
 * Function Name  : IsisTmrDelinkECTimer ()
 * Description    : This routine Delinks all the records that are linked with
 *                  Equal class Timer blocks. 
 * Inputs         : apEC[] - pointer to the Equivalent Class Timer Blocks
 *                  u1Len  - Number of Blocks
 * Outputs        : VOID 
 * Globals        : None 
 * Returns        : VOID
 *****************************************************************************/

PUBLIC VOID
IsisTmrDelinkECTimer (tIsisTmrECBlk * apEC[], UINT1 u1Len)
{
    UINT1               u1Size = 0;
    tIsisTmrECBlk      *pTmrECBlk = NULL;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrDelinkECTimer ()\n"));

    for (u1Size = 0; u1Size < u1Len; u1Size++)
    {
        pTmrECBlk = apEC[u1Size];

        while (pTmrECBlk != NULL)
        {
            switch (pTmrECBlk->u1Type)
            {
                case ISIS_ECT_MAX_AGE:
                case ISIS_ECT_ZERO_AGE:

                    IsisTmrDelLSPTmrLinks ((tIsisLSPEntry *)
                                           (pTmrECBlk->pInfo));
                    break;

                case ISIS_ECT_L1_PSNP:
                case ISIS_ECT_L2_PSNP:
                case ISIS_ECT_L1_CSNP:
                case ISIS_ECT_L2_CSNP:
                case ISIS_ECT_P2P_ISH:
                case ISIS_ECT_P2P_HELLO:
                case ISIS_ECT_L1LANHELLO:
                case ISIS_ECT_L2LANHELLO:
                case ISIS_ECT_HOLDING:
                case ISIS_ECT_L1_LSP_TX:
                case ISIS_ECT_L2_LSP_TX:

                    ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pTmrECBlk->pInfo);
                    pTmrECBlk->pInfo = NULL;
                    break;
                default:
                    break;

            }
            pTmrECBlk = pTmrECBlk->pNext;
        }
    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exited IsisTmrDelinkECTimer ()\n"));
}

/*******************************************************************************
 * Function     : IsisTmrDeAllocTmrECB ()
 * Description  : This routine deletes all the Equivalence Class Timer blocks
 *                and the associated information.
 * Inputs       : pIsisTmrECBlk [] - Pointer to Timer Block
 *                u1Len            - Number of Blocks in the Equivalence
 *                                   Class Timer    
 * Outputs      : None 
 * Globals      : None 
 * Returns      : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisTmrDeAllocTmrECB (tIsisTmrECBlk * pIsisTmrECBlk[], UINT1 u1Len)
{
    tIsisTmrECBlk      *pTmrECBlk = NULL;
    tIsisTmrECBlk      *pNextTmrECBlk = NULL;
    UINT1               u1Size = 0;

    TMP_EE ((ISIS_LGST, "TMR <X> : Entered IsisTmrDeAllocTmrECB ()\n"));

    /* Each of the EC Timer has a different number of Blocks. Free all the EC
     * Timer records which are held in each of the Blocks
     */

    for (u1Size = 0; u1Size < u1Len; u1Size++)
    {
        pTmrECBlk = pIsisTmrECBlk[u1Size];
        while (pTmrECBlk != NULL)
        {
            pNextTmrECBlk = pTmrECBlk->pNext;
            pTmrECBlk->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_ECTI, (UINT1 *) pTmrECBlk->pInfo);
            pTmrECBlk->pInfo = NULL;
            ISIS_MEM_FREE (ISIS_BUF_ECTS, (UINT1 *) pTmrECBlk);
            pTmrECBlk = NULL;
            pTmrECBlk = pNextTmrECBlk;
        }
        pIsisTmrECBlk[u1Size] = NULL;
    }

    TMP_EE ((ISIS_LGST, "TMR <X> : Exited IsisTmrDeAllocTmrECB ()\n"));
}
