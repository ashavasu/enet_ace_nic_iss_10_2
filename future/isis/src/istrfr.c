/**************************************************************************
 *
 *  Copyright (C) Future Software Limited,  2001
 *
 *  $Id: istrfr.c,v 1.5 2017/09/11 13:44:08 siva Exp $
 *
 *  Description: This file contains the Traffic Router Interface Routines
 *
 *************************************************************************/

#include "isincl.h"
#include "isextn.h"

static VOID         IsisTrfrFillLLHdr (tIsisCktEntry *, UINT1, tIsisLLHdr *,
                                       UINT1);

/******************************************************************************
 * Function    : IsisTrfrIfStatusInd () 
 * Description : This routine is invoked by Adjacency Module to indicate the 
 *               Data Link Layer Interface module the change in status of 
 *               Circuit. TRFR module invokes the corresponding DLLI API to
 *               indicate the change. 
 * Input (s)   : pContext - Context to which this circuit belongs
 *               pCktRec  - The circuit whose status has changed
 *               u1Status - The status of the Interface - UP or DOWN
 * Output (s)  : None
 * Globals     : gIsisExtInfo Referred Not Modified
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisTrfrIfStatusInd (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                     UINT1 u1Status)
{
    TRP_EE ((ISIS_LGST, "TRF <X> : Entered IsisTrfrIfStatusInd () \n"));

    if (pContext == NULL)
    {
        TRP_PT ((ISIS_LGST, "TRF <E> : Cannot Process - Null Context\n"));
        return;
    }
    if (pCktRec != NULL)
    {
        TRP_PT ((ISIS_LGST,
                 "TRF <I> : Status change in Circuit [%d] with notification [%s]\n",
                 pCktRec->u4CktIdx, ISIS_GET_ISIS_CKT_STATUS (u1Status)));
        IsisDlliProcIfStatusChange (pCktRec, u1Status);
    }
    TRP_EE ((ISIS_LGST, "TRF <X> : Exiting IsisTrfrIfStatusInd () \n"));
}

/******************************************************************************
 * Function      : IsisTrfrTxData () 
 * Description   : This funtion routes the traffic received from other
 *                 modules to the Data Link Layer module. If the DLL module
 *                 resides on the same node as Core Module, then the DLLI
 *                 Transmit function is directly invoked to transfer the
 *                 data out over a physical interface. 
 * Input (s)     : pCktRec  - The circuit record 
 *                 pu1Data  - The Data to be sent 
 *                 u4Length - The length of the data to be sent 
 *                 u1Level  - The Circuit Level. Possible values are L1 or L2
 *                 u1Flag   - Says whether the Data to be released
 *                            or not after transmission.               
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, on successful transmission of data  
 *                 ISIS_FAILURE, otherwise  
 ******************************************************************************/

PUBLIC INT4
IsisTrfrTxData (tIsisCktEntry * pCktRec, UINT1 *pu1Data,
                UINT4 u4Length, UINT1 u1Level, UINT1 u1Flag, UINT1 u1IsHelloPkt)
{
    tIsisLLHdr          LLHdr;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    TRP_EE ((ISIS_LGST, "TRF <X> : Entered IsisTrfrTxData () \n"));

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));

    if ((pCktRec->u1OperState != ISIS_UP) || (pCktRec->bTxEnable != ISIS_TRUE))
    {
        if (u1Flag == ISIS_FREE_BUF)
        {
            /* This routine always assume that the buffer to be releases
             * is Buddy Buffer and hence release it to the Buddy memory pool
             */

            IsisUtlFreePkt (pu1Data);
        }
        if (pCktRec->u1OperState != ISIS_UP)
        {
            TRP_PT ((ISIS_LGST,
                     "TRF <E> : Transmission not Allowed - ISIS State is DOWN - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));
        }
        else if ((pCktRec->bTxEnable != ISIS_TRUE))
        {
            TRP_PT ((ISIS_LGST,
                     "TRF <E> : Transmission not Allowed - Packet Transmission not Enabled - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));
        }
        return ISIS_FAILURE;
    }

    IsisTrfrFillLLHdr (pCktRec, u1Level, &LLHdr, u1IsHelloPkt);

    IsisDlliTxData (pCktRec, &LLHdr, pu1Data, u4Length, u1Flag);

    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    TRP_PT ((ISIS_LGST,
             "TRF <I> : Transmitting Packets on Circuit [ %u ], Level [ %s ], "
             "IPv4 Address [ %s ], IPv6 Address [ %s ]\n", pCktRec->u4CktIdx,
             ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel), au1IPv4Addr,
             Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pCktRec->au1IPV6Addr))));

    TRP_EE ((ISIS_LGST, "TRF <X> : Exiting IsisTrfrTxData () \n"));

    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function        : IsisTrfrFillLLHdr () 
 * Description     : This routine fills the Link Layer header specific
 *                   information. 
 * Input (s)       : pCktRec    - The circuit record 
 *                   u1CktLevel - This is used for filling the appropriate
 *                                Multicast Address in the case of Ethernet.
 * Output (s)      : pLLHeader  - The link level specific information updated
 * Globals         : Not Referred or Modified
 * Returns         : VOID
 ******************************************************************************/

PRIVATE VOID
IsisTrfrFillLLHdr (tIsisCktEntry * pCktRec, UINT1 u1CktLevel,
                   tIsisLLHdr * pLLHeader, UINT1 u1IsHelloPkt)
{
    UINT1               au1L1MCAddr[6] = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x14 };
    UINT1               au1L2MCAddr[6] = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x15 };
    UINT1               au1AllISMCAddr[6] =
        { 0x09, 0x00, 0x2B, 0x00, 0x00, 0x05 };

    TRP_EE ((ISIS_LGST, "TRF <X> : Entered IsisTrfrFillLLHdr () \n"));

    /* NOTE: ATM and FR circuits are also configured as Point-to-Point links.
     * But these circuits require destination address (SNPA) for data to be
     * transmitted over physical links unlike PPP which does not require any
     * address
     */

    /* Since there is no field in the circuit record which can be used to
     * identify ATM or FR circuits, copy the u4SubIfIndex in all the cases. DLLI
     * module after ascertaining the type of circuit will retrieve appropriate
     * information from the LL header.
     */

    pLLHeader->u4SubIfIndex = pCktRec->u4CktIfSubIdx;

    if ((pCktRec->u1CktType == ISIS_P2P_CKT) && (u1IsHelloPkt == ISIS_TRUE))
    {
        MEMCPY (pLLHeader->au1DestMACAddr, au1AllISMCAddr, ISIS_SNPA_ADDR_LEN);
    }
    else
    {
        if (u1CktLevel == ISIS_LEVEL1)
        {
            MEMCPY (pLLHeader->au1DestMACAddr, au1L1MCAddr, ISIS_SNPA_ADDR_LEN);
        }
        else
        {
            MEMCPY (pLLHeader->au1DestMACAddr, au1L2MCAddr, ISIS_SNPA_ADDR_LEN);
        }
    }
    /* If no information is configured, this field will be all '0's.
     * IsisDlliTxData () routine must ensure that correct SRC MAC address is
     * prepended to the Data that is handed over to the DLL module
     */

    MEMCPY (pLLHeader->au1SrcMACAddr, pCktRec->au1SNPA, ISIS_SNPA_ADDR_LEN);

    TRP_EE ((ISIS_LGST, "TRF <X> : Exiting IsisTrfrFillLLHdr () \n"));
}
