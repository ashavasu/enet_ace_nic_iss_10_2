 /*****************************************************************************
  * 
  *  Copyright (C) Future Software Limited,  2001
  *
  *  $Id: isfltr.c,v 1.17 2017/09/11 13:44:08 siva Exp $
  *
  *  Description: This file contains the Fault Tolerance Module routines.
  *
  ****************************************************************************/

#ifdef ISIS_FT_ENABLED

#include "isincl.h"
#include "isextn.h"

/* Function prototype definitions 
 */

PRIVATE INT4        IsisFltrProcMemCfgUpdt (tRmMsg *);
PRIVATE INT4        IsisFltrProcMemUpdate (tRmMsg *);
PRIVATE INT4        IsisFltrProcFTMessage (tRmMsg *, UINT1, UINT2);
PRIVATE INT4        IsisFltrProcLspUpdate (tRmMsg *);
PRIVATE INT4        IsisFltrProcAdjUpdate (tRmMsg *);
PRIVATE INT4        IsisFltrProcSysUpdate (tRmMsg *);
PRIVATE INT4        IsisFltrProcLspAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcLspDelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcLspModify (tRmMsg *);
PRIVATE INT4        IsisFltrProcCktAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcCktDelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcCktLevelAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcCktLevelDelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcAdjAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcAdjDelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcAdjAAAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcAdjAADelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcIPRAAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcIPRADelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcMAAAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcMAADelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcSAAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcSADelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcIPIfAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcIPIfDelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcSysContextAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcSysContextDelete (tRmMsg *);
PRIVATE INT4        IsisFltrProcSysConfigsAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcSysActualsAdd (tRmMsg *);
PRIVATE INT4        IsisFltrProcSysPSModify (tRmMsg *);
PRIVATE INT4        IsisFltrProcIsStatus (tRmMsg * pu1Msg);
PRIVATE INT4        IsisFltrProcSchDecn (tRmMsg * pu1Msg);

/* NOTE: 
 *
 * Logic of Adding Entries to the Standby Database
 * -----------------------------------------------
 *
 * If an entry is to be added to the database its parent object must exist. This
 * imposes a restriction on the ACTIVE node to send Fault Tolerance updates in a
 * particular order. To avoid this FutureISIS implementation creates the parent
 * record, marks it as NOT_READY, if it does not already exist. This parent
 * record gets updated when the LSU or Bulk Update for this record is received.
 * This may happen if a manager, on the ACTIVE node, creates a Context, marks
 * it as CREATE_AND_WAIT, then creates a Circuit, configures it completely and
 * makes it ACTIVE. When RowStatus is made ACTIVE a LSU will be generated. The
 * standby node receives the Circuit LSU but does not find the Context since
 * ACTIVE node has not yet sent the Context because the RowStatus for the
 * Context, is not made Active yet.
 * 
 * For e.g., when a Circuit record is added, if the required context does not 
 * exist we will create a context, mark it as NOT_READY and add the Circuit to 
 * the context. This is because Circuits and Contexts can be created in any 
 * order and hence to handle such conditions parent records are created if they
 * do not exist. If addition of a record requires parents to be created at
 * multiple levels, all parents are created as before and linked appropriately.
 * Example, IPRA requires a Circuit which in turn requires a Context. 
 *
 * However Adjacencies can never exist without a valid Circuit. Hence there is 
 * an implicit order w.r.t Adjacencies and Adjacency Area Addresses and hence 
 * parent records must exist for these dynamically established objects. Thesame
 * logic applies for LSPs also
 *
 * Please note this implementation is for the Standby node and the actions are
 * entirely different when it comes to Active node.
 *
 * Objects for which parent records are created
 * --------------------------------------------
 *
 * S.No Object                 Parent
 * -----------------------------------
 * 1.   Circuit                Context
 * 2.   Circuit Level          Circuit/Context
 * 3.   MAA                    Context
 * 4.   SAA                    Context
 * 5.   IPRA                   Circuit/Context
 * 6.   IP I/f Address         Circuit/Context
 *
 * Objects for which parent records are not created
 * ------------------------------------------------
 *
 * S.No Object                 Parent
 * -----------------------------------
 * 1.   Adjacency              Circuit/Context
 * 2.   Adjacency Area Address Adjacency/Circuit/Context
 * 3.   LSP                    Context
 * 4.   Context                None
 */

/*******************************************************************************
 * Function    : IsisFltrProcCtrlPkt () 
 * Description : This routine processes the Fault Tolerance commands received
 *               from the Fault Tolerance Manager, handles Lock Step Updates 
 *               and Bulk Update requests received from the Peer node.
 * Input(s)    : pIsisMsg  - Pointer to the Received FLTR message 
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if the message is processed successfully
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrProcCtrlPkt (tIsisMsg * pIsisMsg)
{
    INT4                i4RetVal = ISIS_SUCCESS;

    i4RetVal = IsisFltrProcFTMessage
        ((tRmMsg *) (VOID *) pIsisMsg->pu1Msg, pIsisMsg->u1RmEvent,
         (UINT2) pIsisMsg->u4Length);

    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
    return i4RetVal;
}

PRIVATE INT4
IsisFltrProcFTMessage (tRmMsg * pRmMsg, UINT1 u1Type, UINT2 u2DataLen)
{

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisProcFTMessage ()\n"));

    switch (u1Type)
    {
            /* The Message Types in this switch statement are specific to 
             * FutureRM. These may have to be modified if any other 
             * Redundancy Manager is used
             */

        case GO_ACTIVE:
            FTP_PT ((ISIS_LGST, "FLT <T> : Processing [GO_ACTIVE]\n"));
            IsisCtrlGoActive ();
            break;

        case GO_STANDBY:
            FTP_PT ((ISIS_LGST, "FLT <T> : Processing [GO_STANDBY]\n"));
            IsisCtrlGoStandby ();
            break;

        case RM_STANDBY_UP:
            FTP_PT ((ISIS_LGST, "FLT <T> : Processing [RM_STANDBY_UP]\n"));
            IsisCtrlHandleStandbyUp (pRmMsg);
            break;

        case RM_STANDBY_DOWN:
            FTP_PT ((ISIS_LGST, "FLT <T> : Processing [RM_STANDBY_DOWN]\n"));
            IsisCtrlHandleStandbyDown (pRmMsg);
            break;

        case L2_INITIATE_BULK_UPDATES:
            FTP_PT ((ISIS_LGST,
                     "FLT <T> : Processing [INITIATE_BULK_UPDATES]\n"));
            IsisFltrFormBulkUpdateReq ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            FTP_PT ((ISIS_LGST,
                     "FLT <T> : Processing [RM_CONFIG_RESTORE_COMPLETE]\n"));
            IsisCtrlConfigRestoreComplete ();
            break;

        case RM_MESSAGE:
            IsisCtrlProcessRmMessage (pRmMsg, u2DataLen);
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            IsisFltrHandleDynSyncAudit ();
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid FLTR Message Type [ %u ]\n",
                     u1Type));
            break;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisProcFTMessage ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrProcData ()
 * Description : This routine processes the Lock Step and Bulk Update messages 
 *               received from the Active Node.
 * Input(s)    : pu1Msg  - Pointer to the recieved message 
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if processing of message is successful
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrProcData (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Type = 0;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcData ()\n"));

    /* Get the Type of the database that is included in the received message 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + 1;

    ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1Type);

    FTP_PT ((ISIS_LGST, "FLT <E> : Processing Lock Step/Bulk Update\n"));
    switch (u1Type)
    {
        case ISIS_DB_LSPS:

            i4RetVal = IsisFltrProcLspUpdate (pu1Msg);
            break;

        case ISIS_DB_CKTS:
        case ISIS_DB_CKTL:
        case ISIS_DB_ADJN:
        case ISIS_DB_ADAD:
        case ISIS_DB_IPRA:

            i4RetVal = IsisFltrProcAdjUpdate (pu1Msg);
            break;

        case ISIS_DB_MAAD:
        case ISIS_DB_SAAD:
        case ISIS_DB_IPIF:
        case ISIS_DB_CTXT:
        case ISIS_DB_CONF:
        case ISIS_DB_ACTS:
        case ISIS_DB_PRSU:

            i4RetVal = IsisFltrProcSysUpdate (pu1Msg);
            break;

        case ISIS_DB_MCFG:

            i4RetVal = IsisFltrProcMemUpdate (pu1Msg);
            break;

        case ISIS_IS_STAT:

            IsisFltrProcIsStatus (pu1Msg);
            i4RetVal = ISIS_NO_FREE;
            break;

        case ISIS_SD_DECN:
            i4RetVal = IsisFltrProcSchDecn (pu1Msg);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Unknown Database Received [ %u ]\n",
                     u1Type));
            break;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcData ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    :  IsisFltrFormBulkData () 
 * Description :  This routine triggers the formation of a Bulk Update. Each of
 *                the Database which requires a backup on the standby node will
 *                be encoded in a particular form, where indices replace the
 *                pointers, and encoded into a message as required by the RM.
 *                FutureISIS adds its own headers which will be decoded by the
 *                FutureISIS module on the standby node before processing the
 *                database records. Each Database is encoded separately into a
 *                sequence of messages and no message will include records of
 *                two different databases
 * Input(s)    :  None
 * Output(s)   :  None
 * Globals     :  Not Referred or Modified
 * Returns     :  ISIS_SUCCESS, On successful encoding of required databases 
 *                ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrFormBulkData ()
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2Len = 0;
    UINT1               u1Flag = ISIS_SOD;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1               u1LspLvl = ISIS_LEVEL1;
    tIsisCktLevel      *pCktLevelRec = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    tIsisSAEntry       *pSARec = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisLSPEntry      *pLspRec = NULL;
    tIsisAdjAAEntry    *pAdjAARec = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrFormBulkData ()\n"));

    pContext = ISIS_GET_CONTEXT ();
    if (pContext == NULL)
    {
        IsisFltrFormBulkUpdateTail ();
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
        return i4RetVal;
    }

    /* NOTE: Bulk Data will always be sent in order i.e. the parent records are
     * sent first before the child records are sent. This means if Context has a
     * pointer to Circuits, then the Context information will be sent first and
     * then the Circuit information so that re-building the information on the
     * standby node becomes simpler. This is just a convention and is not a
     * restriction
     *
     * NOTE : Adjacencies Bulk Updates Must be sent only after the Circuit and
     * Circuit Level bulk updates are completed.
     */

    /* The following loops starts building FLTR messages from the 
     * information available in the given Databases. They handles the 
     * restrictions imposed by the FLTR buffer sizes by breaking information
     * appropriately. If all the information is encoded and transferred,
     * then the routine returns ISIS_EOD, otherewise the loop has to continue to
     * transfer the remaining unencoded information. The routines returns one
     * message at a time.
     */
    /* SYSTEM CONTEXT
     */

    FTP_PT ((ISIS_LGST, "FLT <T> : Bulk Update Initiated\n"));
    pContext = ISIS_GET_CONTEXT ();
    while (pContext != NULL)
    {
        pRmMsg = IsisFltrFormSysContextBulkData (&pContext, &u2Len);

        if (pRmMsg != NULL)
        {
            i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2Len);
        }
        else
        {
            /* There is no point in proceeding further, since we were unable to
             * allocate memory to construct Bulk Update message. 
             */

            IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Bulk Update\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
            return ISIS_FAILURE;
        }

        pContext = pContext->pNext;
        u2Len = 0;
    }
    /* CIRCUIT DATABASE 
     */

    u1Flag = ISIS_SOD;
    pContext = ISIS_GET_CONTEXT ();

    i4RetVal = IsisFltrGetFirstCktEntry (&pContext, &pCktRec);

    while ((u1Flag != ISIS_EOD) && (i4RetVal != ISIS_FAILURE))
    {
        /* We have retrived successfully a circuit record. Proceed adding the
         * circuit database starting from the given circuit
         */

        pRmMsg = IsisFltrFormCktBulkData (&pContext, &pCktRec, &u1Flag, &u2Len);

        if (pRmMsg != NULL)
        {
            i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2Len);
        }
        else
        {
            /* There is no point in proceeding further, since we were unable to
             * allocate memory to construct Bulk Update message. 
             */

            IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Bulk Update\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
            return ISIS_FAILURE;
        }
        u2Len = 0;
    }

    /* CIRCUIT LEVEL DATABASE
     */

    u1Flag = ISIS_SOD;
    pContext = ISIS_GET_CONTEXT ();
    pCktRec = NULL;
    pCktLevelRec = NULL;

    /* Get the first valid circuit entry before proceeding with circuit level
     * entries, since circuit level entries are a part of the circuit records
     */

    i4RetVal = IsisFltrGetFirstCktEntry (&pContext, &pCktRec);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the first valid circuit level entry from where the search is
         * expected to start
         */

        i4RetVal = IsisFltrGetNextCktLvlEntry (&pContext, &pCktRec,
                                               &pCktLevelRec);
        while ((u1Flag != ISIS_EOD) && (i4RetVal != ISIS_FAILURE))
        {
            pRmMsg = IsisFltrFormCktLevelBulkData (&pContext, &pCktRec,
                                                   &pCktLevelRec, &u1Flag,
                                                   &u2Len);
            if (pRmMsg != NULL)
            {
                i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2Len);
            }
            else
            {
                /* There is no point in proceeding further, since we were unable
                 * to allocate memory to construct Bulk Update message. 
                 */

                IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
                return ISIS_FAILURE;
            }
            u2Len = 0;
        }
    }

    /* ADJACENCY DATABASE 
     */
    u1Flag = ISIS_SOD;
    pContext = ISIS_GET_CONTEXT ();
    pCktRec = NULL;
    pAdjRec = NULL;

    /* Get the first valid adjacency entry before proceeding further, since we
     * have to make sure that we have something valid. Otherwise we can proceed
     * with the next database
     */

    i4RetVal = IsisFltrGetFirstAdjEntry (&pContext, &pCktRec, &pAdjRec);

    while ((u1Flag != ISIS_EOD) && (i4RetVal != ISIS_FAILURE))
    {
        /* Found a valid adjacency. Proceed with the backup procedure
         */

        pRmMsg =
            IsisFltrFormAdjBulkData (&pContext, &pCktRec, &pAdjRec, &u1Flag,
                                     &u2Len);

        if (pRmMsg != NULL)
        {
            i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2Len);
        }
        else
        {
            /* There is no point in proceeding further, since we were unable to
             * allocate memory to construct Bulk Update message. 
             */

            IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Bulk Update\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
            return ISIS_FAILURE;
        }
        u2Len = 0;
    }

    /* LSP DATABASE
     */

    u1Flag = ISIS_SOD;
    pContext = ISIS_GET_CONTEXT ();
    pLspRec = NULL;

    /* Get the very first valid LSP before proceeding to retrieve LSPs from TxQ
     * and DB
     */

    i4RetVal = IsisFltrGetNextLspEntry (&pContext, &u1LspLvl, &pLspRec, &u1Loc);

    while ((u1Flag != ISIS_EOD) && (i4RetVal != ISIS_FAILURE))
    {
        pRmMsg = IsisFltrFormLSPBulkData (&pContext, &pLspRec, &u1LspLvl,
                                          &u1Loc, &u1Flag, &u2Len);
        if (pRmMsg != NULL)
        {
            i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2Len);
        }
        else
        {
            /* There is no point in proceeding further, since we were unable to
             * allocate memory to construct Bulk Update message. 
             */

            IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Bulk Update\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
            return ISIS_FAILURE;
        }
        u2Len = 0;
    }

    /* IP INTERFACE ADDRESSES DATABASE
     */

    u1Flag = ISIS_SOD;
    pContext = ISIS_GET_CONTEXT ();
    pCktRec = NULL;
    pIPIfRec = NULL;

    i4RetVal = IsisFltrGetFirstIPIFRec (&pContext, &pIPIfRec);

    while ((u1Flag != ISIS_EOD) && (i4RetVal != ISIS_FAILURE))
    {
        pRmMsg =
            IsisFltrFormIPIfBulkData (&pContext, &pIPIfRec, &u1Flag, &u2Len);

        if (pRmMsg != NULL)
        {
            i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2Len);
        }
        else
        {
            /* There is no point in proceeding further, since we were unable to
             * allocate memory to construct Bulk Update message. 
             */

            IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Bulk Update\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
            return ISIS_FAILURE;
        }
        u2Len = 0;
    }

    /* SUMMARY AREA ADDRESS DATABASE
     */

    u1Flag = ISIS_SOD;
    pContext = ISIS_GET_CONTEXT ();
    pSARec = NULL;

    i4RetVal = IsisFltrGetFirstSAEntry (&pContext, &pSARec);

    while ((u1Flag != ISIS_EOD) && (i4RetVal != ISIS_FAILURE))
    {
        pRmMsg = IsisFltrFormSAABulkData (&pContext, &pSARec, &u1Flag, &u2Len);

        if (pRmMsg != NULL)
        {
            i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2Len);
        }
        else
        {
            /* There is no point in proceeding further, since we were unable to
             * allocate memory to construct Bulk Update message. 
             */

            IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Bulk Update\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
            return ISIS_FAILURE;
        }
        u2Len = 0;
    }

    /* ADJACENCY AREA ADDRESS DATABASE
     */

    u1Flag = ISIS_SOD;
    pContext = ISIS_GET_CONTEXT ();
    pCktRec = NULL;
    pAdjRec = NULL;

    i4RetVal = IsisFltrGetFirstAdjAreaEntry (&pContext, &pCktRec, &pAdjRec,
                                             &pAdjAARec);
    while ((u1Flag != ISIS_EOD) && (i4RetVal != ISIS_FAILURE))
    {
        pRmMsg = IsisFltrFormAdjAABulkData (&pContext, &pCktRec, &pAdjRec,
                                            &pAdjAARec, &u1Flag, &u2Len);
        if (pRmMsg != NULL)
        {
            i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2Len);
        }
        else
        {
            /* There is no point in proceeding further, since we were unable to
             * allocate memory to construct Bulk Update message. 
             */

            IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Bulk Update\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
            return ISIS_FAILURE;
        }
        u2Len = 0;
    }
    IsisFltrFormBulkUpdateTail ();
    FTP_PT ((ISIS_LGST, "FLT <T> : Bulk Update Completed\n"));
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormBulkData ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrFormBulkUpdateReq ()
 * Description : This routine constructs a Bulk Update Request packet and
 *               enqueues the same to RM module   
 * Input(s)    : None
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, on successful formation of Bulk Update Request
 *               ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrFormBulkUpdateReq ()
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2Len = (UINT2) ISIS_RED_BULK_UPD_REQ_MSG_LEN;
    UINT2               u2OffSet = 0;
    UINT1               u1MsgType = (UINT1) RM_BULK_UPDT_REQ_MSG;

    pRmMsg = IsisRmAllocForRmMsg (u2Len);

    if (pRmMsg == NULL)
    {
        IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
        /* Allocation of RM Message failed */
        return ISIS_FAILURE;
    }

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2OffSet, u1MsgType);
    if (IsisRmSendMsgToRm (pRmMsg, u2Len) == ISIS_FAILURE)
    {
        IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_SENDTO_FAIL);
    }
    ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_IN_PROGRESS);

    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrFormBulkUpdateTail ()
 * Description : This routine constructs a Bulk Update Tail message and
 *               enqueues the same to RM module   
 * Input(s)    : None
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, on successful formation of Bulk Update Tail
 *               ISIS_FAILURE, otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrFormBulkUpdateTail ()
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2Len = (UINT2) ISIS_RED_BULK_UPD_REQ_MSG_LEN;
    UINT2               u2OffSet = 0;
    UINT1               u1MsgType = (UINT1) RM_BULK_UPDT_TAIL_MSG;

    pRmMsg = IsisRmAllocForRmMsg (u2Len);

    if (pRmMsg == NULL)
    {
        IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
        /* Allocation of RM Message failed */
        return ISIS_FAILURE;
    }

    ISIS_RED_PUT_1_BYTE (pRmMsg, u2OffSet, u1MsgType);

    ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_COMPLETED);
    IsisRmSetBulkUpdateStatus ();

    if (IsisRmSendMsgToRm (pRmMsg, u2Len) == ISIS_FAILURE)
    {
        IsisRmSendEventToRm (RM_BULK_UPDT_ABORT, RM_SENDTO_FAIL);
    }
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrProcBulkDataComplete () 
 * Description : This routine, on reception of BULK_DATA_COMPLETE from the 
 *               ACTIVE Node constructs a BULK_UPDATE_COMPLETE Message and
 *               enqueues it to FTM. 
 * Input(s)    : VOID 
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful processing of the Message
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrProcBulkDataComplete (VOID)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT2               u2MsgLen = (UINT2) ISIS_RED_BULK_UPD_REQ_MSG_LEN;
    UINT1               u1MsgType = (UINT1) ISIS_BULK_UPDATE_COMPLETE;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcBulkDataComplete()\n"));
    pRmMsg = IsisRmAllocForRmMsg (u2MsgLen);

    if (pRmMsg == NULL)
    {
        /* Log */
        IsisFltProcLSUFailure ();
        return ISIS_FAILURE;
    }

    ISIS_EXT_SET_FT_STATE (ISIS_FT_STANDBY);

    /* form bulk update request message */
    ISIS_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    ISIS_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);

    i4RetVal = IsisRmSendMsgToRm (pRmMsg, u2MsgLen);

    if (i4RetVal != ISIS_SUCCESS)
    {
        WARNING ((ISIS_LGST,
                  "FLT <X> : Sending bulk data to RM is failed ()\n"));
        return ISIS_FAILURE;
    }
    else
    {
        FTP_EE ((ISIS_LGST,
                 "FLT <X> : Sending bulk data to RM is success ()\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcBulkDataComplete ()\n"));
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IsisFltrHandleDynSyncAudit                            */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IsisFltrHandleDynSyncAudit ()
{
    /*On receiving this event, Isis should execute show cmd and calculate checksum */
    IsisExecuteCmdAndCalculateChkSum ();
}

/*******************************************************************************
 * Function    : IsisFltrProcLspUpdate () 
 * Description : This routine processes the Lock Step and Bulk Updates of the
 *               LSP Database received from the Active node
 * Input(s)    : pu1Msg - Pointer to the received LSU or Bulk Update Message
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful Updation of LSP Records 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcLspUpdate (tRmMsg * pu1Msg)
{
    UINT1               u1Command = 0;
    INT4                i4RetVal = ISIS_SUCCESS;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcLspUpdate ()\n"));

    /* Skip the Peer node header to get the FLTR header which includes
     * information regarding the data encoded
     */
    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_MSG_LENGTH_POS, u1Command);

    /* pFLTHdr = (tIsisFLTHdr *) (pu1Msg + ISIS_MSG_LENGTH_POS); */

    switch (u1Command)
    {
        case ISIS_CMD_ADD:

            FTP_PT ((ISIS_LGST, "FLT <T> : Adding LSP On Standby Node\n"));
            i4RetVal = IsisFltrProcLspAdd (pu1Msg);
            break;

        case ISIS_CMD_MODIFY:

            FTP_PT ((ISIS_LGST, "FLT <T> : Modifying LSP On Standby Node\n"));
            i4RetVal = IsisFltrProcLspModify (pu1Msg);
            break;

        case ISIS_CMD_DELETE:

            FTP_PT ((ISIS_LGST, "FLT <T> : Deleting LSP On Standby Node\n"));
            i4RetVal = IsisFltrProcLspDelete (pu1Msg);
            break;

        default:

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                     u1Command));
            break;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcLspUpdate ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcAdjUpdate () 
 * Description : This routine decodes the received Bulk/LSU packets from the
 *               Active node. It updates the appropriate database based on the
 *               type of database included in the message. 
 * Input(s)    : pu1Msg - Pointer to the received FLTR message  
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Adjacency Records 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcAdjUpdate (tRmMsg * pu1Msg)
{
    UINT1               u1Command = 0;
    UINT1               u1Type = 0;
    INT4                i4RetVal = ISIS_SUCCESS;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcAdjUpdate ()\n"));

    /* Skip the Peer node header to get the FLTR header which includes
     * information regarding the data encoded
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_MSG_LENGTH_POS, u1Command);
    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_MSG_LENGTH_POS + 1, u1Type);
    /*pFLTHdr = (tIsisFLTHdr *) (pu1Msg + ISIS_MSG_LENGTH_POS); */

    switch (u1Type)
    {
        case ISIS_DB_CKTS:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding Circuit On Standby Node\n"));
                    i4RetVal = IsisFltrProcCktAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleting Circuit On Standby Node\n"));
                    i4RetVal = IsisFltrProcCktDelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_CKTL:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding CktLvlRec On Standby Node\n"));
                    i4RetVal = IsisFltrProcCktLevelAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleted CktLvlRec On Standby Node\n"));
                    i4RetVal = IsisFltrProcCktLevelDelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_ADJN:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding Adj On Standby Node\n"));
                    i4RetVal = IsisFltrProcAdjAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleting Adj On Standby Node\n"));
                    i4RetVal = IsisFltrProcAdjDelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_ADAD:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding AdjAA On Standby Node\n"));
                    i4RetVal = IsisFltrProcAdjAAAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleting AdjAA On Standby Node\n"));
                    i4RetVal = IsisFltrProcAdjAADelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_IPRA:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding IPRA On Standby Node\n"));
                    i4RetVal = IsisFltrProcIPRAAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleting IPRA On Standby Node\n"));
                    i4RetVal = IsisFltrProcIPRADelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        default:

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Invalid Database Type In FLTR Msg [ %u ]\n",
                     u1Type));
            break;
    }
    UNUSED_PARAM (i4RetVal);
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcAdjUpdate ()\n"));
    return (ISIS_SUCCESS);
}

/*******************************************************************************
 * Function    : IsisFltrProcSysUpdate () 
 * Description : This routine decodes the received Bulk/LSU packets from the
 *               Active node. It updates the appropriate database based on the
 *               type of database included in the message. 
 * Input(s)    : pu1Msg - Pointer to the FLTR message received
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of System related
 *                             information 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSysUpdate (tRmMsg * pu1Msg)
{
    UINT1               u1Command = 0;
    UINT1               u1Type = 0;
    INT4                i4RetVal = ISIS_SUCCESS;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSysUpdate ()\n"));

    /* Skip the Peer node header to get the FLTR header which includes
     * information regarding the data encoded
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_MSG_LENGTH_POS, u1Command);
    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_MSG_LENGTH_POS + 1, u1Type);
    /*pFLTHdr = (tIsisFLTHdr *) (pu1Msg + ISIS_MSG_LENGTH_POS); */

    switch (u1Type)
    {
        case ISIS_DB_MAAD:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding MAA On Standby Node\n"));
                    i4RetVal = IsisFltrProcMAAAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleting MAA On Standby Node\n"));
                    i4RetVal = IsisFltrProcMAADelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_SAAD:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding SAA On Standby Node\n"));
                    i4RetVal = IsisFltrProcSAAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleting SAA On Standby Node\n"));
                    i4RetVal = IsisFltrProcSADelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_IPIF:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding IP I/f On Standby Node\n"));
                    i4RetVal = IsisFltrProcIPIfAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleting IP I/f  On Standby Node\n"));
                    i4RetVal = IsisFltrProcIPIfDelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_CTXT:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding Context On Standby Node\n"));
                    i4RetVal = IsisFltrProcSysContextAdd (pu1Msg);
                    break;

                case ISIS_CMD_DELETE:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Deleting Context On Standby Node\n"));
                    i4RetVal = IsisFltrProcSysContextDelete (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_CONF:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding Configs On Standby Node\n"));
                    i4RetVal = IsisFltrProcSysConfigsAdd (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_ACTS:

            switch (u1Command)
            {
                case ISIS_CMD_ADD:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Adding Actuals On Standby Node\n"));
                    i4RetVal = IsisFltrProcSysActualsAdd (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        case ISIS_DB_PRSU:

            switch (u1Command)
            {
                case ISIS_CMD_MODIFY:

                    FTP_PT ((ISIS_LGST,
                             "FLT <T> : Modify PS On Standby Node\n"));
                    i4RetVal = IsisFltrProcSysPSModify (pu1Msg);
                    break;

                default:

                    FTP_PT ((ISIS_LGST,
                             "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                             u1Command));
                    break;
            }
            break;

        default:

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Invalid Database Type In FLTR Msg [ %u ]\n",
                     u1Type));
            break;
    }

    UNUSED_PARAM (i4RetVal);
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSysUpdate ()\n"));
    return (ISIS_SUCCESS);
}

/*******************************************************************************
 * Function    : IsisFltrProcLspAdd ()
 * Description : This routine decodes the received FLTR message containing the
 *               LSPs to be backed up and constructs LSP buffers from the
 *               received information. It then adds the LSPs to the Database
 * Input(s)    : pu1Msg   - Pointer to the received FLTR Message
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of the LSP Record to the
 *                             LSP Database
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcLspAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Level = 0;
    UINT1               u1EntryCount = 0;
    UINT1              *pu1Lsp = NULL;
    UINT2               u2LspLen = 0;
    UINT2               u2Length = 0;
    UINT2               u2Offset = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4InstIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisLSPEntry      *pLsp = NULL;
    tIsisLSPEntry      *pLspRec = NULL;
    UINT1               au1LspId[ISIS_SYS_ID_LEN + 2];
    UINT1               u1Loc = ISIS_LOC_NONE;
    tIsisHashBucket    *pHashBkt = NULL;

    MEMSET (au1LspId, 0, ISIS_SYS_ID_LEN + 2);
    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcLspAdd ()\n"));

    /* Get the number of entries and the total length of the message. Entry
     * Count will specifiy how many LSP records are included in the message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);
    ISIS_RED_READ_2_BYTE (pu1Msg, ISIS_FT_LENGTH_POS, u2Length);

    FTP_PT ((ISIS_LGST, "FLT <T> : Number Of LSPs To Be Backed Up [ %u ]\n",
             u1EntryCount));
    FTP_PT ((ISIS_LGST, "FLT <T> : Length Of The Message [ %u ]\n", u2Length));

    /* Skip the FLTR header
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        /* Extracting the Circuit Index from the LSPs
         * This Circuit Index is used to get the Circuit Record pointer
         * for the Pseudonode Self LSPs
         */

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

        /* Extracting the Type of LSP i.e LEVEL1 or LEVEL2
         */

        ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1Level);

        /* Extracting the length of the LSP Received 
         */

        ISIS_RED_GET_2_BYTE (pu1Msg, u2Offset, u2LspLen);

        i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
        if (i4RetVal == ISIS_FAILURE)
        {
            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found - Ignore LSP\n",
                     u4InstIdx));

            u2Offset = (UINT2) (u2Offset + u2LspLen);    /* Skipping the entire LSP
                                                         */
            u1EntryCount--;
            continue;
        }

        /* Allocate memory for the LSP 
         */

        pu1Lsp = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_LSPI, u2LspLen);

        if (pu1Lsp == NULL)
        {
            i4RetVal = ISIS_FAILURE;
            IsisFltProcLSUFailure ();
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : LSP Updation During Bulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcLspAdd ()\n"));
            return i4RetVal;
        }

        ISIS_RED_READ_N_BYTE (pu1Msg, pu1Lsp, u2Offset, u2LspLen);

        /* On the standby node, we always add LSPs to the Database even those
         * LSPs are in TxQ in the Active Node. This is because Standby node does
         * not process any events other than those from RM and hence there is no
         * point in having LSPs in TxQ since they will never be transmitted in
         * the absence of timers which are started only when moving to FT_ACTIVE
         * state
         */
        MEMCPY (au1LspId, (pu1Lsp + ISIS_OFFSET_LSPID), ISIS_SYS_ID_LEN + 2);
        pLsp = IsisUpdGetLSP (pContext, au1LspId, u1Level, &pLspRec, &pHashBkt,
                              &u1Loc);
        if (pLsp != NULL)
        {
            IsisUpdDeleteLSP (pContext, u1Level, au1LspId);
        }
        IsisUpdAddLSP (pContext, pu1Lsp, ISIS_LOC_DB, u1Level);

        if (MEMCMP ((pu1Lsp + ISIS_OFFSET_LSPID),
                    pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN) == 0)
        {
            ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1Lsp, u4SeqNum);
            FTP_PT ((ISIS_LGST, "FLT <E> : Modifying Self LSP in flt\n"));
            IsisFltrModifySelfLSPTLV (pContext, (pu1Lsp + ISIS_OFFSET_LSPID),
                                      u1Level, u4SeqNum);
        }

        u2Offset = (UINT2) (u2Offset + u2LspLen);
        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcLspAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcLspDelete () 
 * Description : This routine deletes the LSP, whose LSPID is included in the
 *               received FLTR message, from the Database.
 * Input(s)    : *pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful deletion of the LSP Record 
 *                             from the LSP Database
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcLspDelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Level = 0;
    UINT4               u4InstIdx = 0;
    UINT2               u2Offset = 0;
    UINT1               au1LspId[ISIS_SYS_ID_LEN + 2];
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcLspDelete ()\n"));

    /* Skip the FLTR header
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_FAILURE)
    {
        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found - Ignore LSP\n",
                 u4InstIdx));
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcLspDelete ()\n"));
        return i4RetVal;
    }

    /* Extracting the Type of LSP i.e LEVEL1 or LEVEL2
     */

    ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1Level);

    ISIS_RED_GET_N_BYTE (pu1Msg, au1LspId, u2Offset, (ISIS_SYS_ID_LEN + 2));

    IsisUpdDeleteLSP (pContext, u1Level, au1LspId);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcLspDelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcLspModify () 
 * Description : This routine decodes the received FLTR message and retrieves
 *               the LSP information included. It then modifies the Remaining
 *               Life Time and Sequence Number values of the LSP in the 
 *               Database with the new values from the received LSP
 * Input(s)    : *pu1Msg  - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful modification of the LSP Records 
 *                             in the LSP Database
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcLspModify (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Level = 0;
    UINT1               u1Loc = 0;
    UINT1               au1LspId[ISIS_SYS_ID_LEN + 2];
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT2               u2RemLT = 0;
    UINT4               u4SeqNum = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisLSPEntry      *pLsp = NULL;
    tIsisLSPEntry      *pLspRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcLspModify () \n"));

    /* NOTE: Only Remaining Life Time and Sequence Number change regularly in 
     *       the case of LSPs. This means that every time a Seq. No or 
     *       Remaining Life Time changes we have to update the LSP Database on
     *       the standby node which is a very costly process since LSPs can be
     *       of varying sizes. To avoid sending each and every received LSP as
     *       an update to the standby node, the FLTR message will include only
     *       the updated Remaining Life Time and Sequence Numbers for each of 
     *       the LSP included there by saving the backplane bandwidth
     */

    /* Skip the FLTR Header
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_FAILURE)
    {
        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found - Ignore LSP\n",
                 u4InstIdx));
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcLspModify ()\n"));
        return i4RetVal;
    }

    /* Extracting the Type of LSP i.e LEVEL1 or LEVEL2
     */

    ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1Level);

    ISIS_RED_GET_N_BYTE (pu1Msg, au1LspId, u2Offset, (ISIS_SYS_ID_LEN + 2));

    ISIS_RED_GET_2_BYTE (pu1Msg, u2Offset, u2RemLT);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4SeqNum);

    /* Get the LSP from the Database
     */

    pLsp = IsisUpdGetLSP (pContext, au1LspId, u1Level, &pLspRec, &pHashBkt,
                          &u1Loc);

    if (pLsp != NULL)
    {
        /* Update the Remaining Life Time and Sequence Number fields in the
         * retrieved LSP
         */

        IsisUpdModifyLSP (pContext, pLsp, u2RemLT, u4SeqNum, u1Loc);

        if (MEMCMP (au1LspId, pContext->SysActuals.au1SysID,
                    ISIS_SYS_ID_LEN) == 0)
        {
            /* Modifying a Self LSP, we have to modify the Sequence Number of
             * the corresponding LSP Information being maintained as TLVs in 
             * the SelfLSP database
             */
            FTP_PT ((ISIS_LGST, "FLT <E> : Modifying Self LSP in flt\n"));
            IsisFltrModifySelfLSPTLV (pContext, au1LspId, u1Level, u4SeqNum);
        }
        else
        {
            if ((u1Loc == ISIS_LOC_DB) || (u1Loc == ISIS_LOC_DB_FIRST))
            {
                /* Restart the MaxAge Timer for the Received LSP if the LSP
                 * is other than SelfLSP
                 */

                ISIS_DEL_LSP_TIMER_NODE (pLsp);
                IsisUpdStartLSPTimer (pContext, pLsp, ISIS_ECT_MAX_AGE,
                                      u2RemLT);
            }
            else if ((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST))
            {
                /* Restart the MaxAge Timer for the Received LSP if the LSP
                 * is other than SelfLSP
                 */

                ISIS_DEL_LSP_TIMER_NODE (((tIsisLSPTxEntry *) pLsp)->pLSPRec);
                IsisUpdStartLSPTimer (pContext, ((tIsisLSPTxEntry *) pLsp)->
                                      pLSPRec, ISIS_ECT_MAX_AGE, u2RemLT);
            }
        }

    }
    else
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : Required LSP Not Found\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcLspModify ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcCktAdd ()
 * Description : This routine decodes the given FLTR message 'pu1Msg' and
 *               retrieves the circuit information. It then constructs a
 *               circuit record based on the information retrieved and adds it
 *               to the circuit database. If the context where the given circuit
 *               information is to be added is not available, then it creates a
 *               Context, marks it as NOT_READY and updates the circuit
 *               information into the newly created context.
 * Input(s)    : pu1Msg   - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of the Circuit Record 
 *                             to the Circuit Database 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcCktAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT1               u1PrevIfState = ISIS_STATE_OFF;
    UINT1               u1PrevAdminState = ISIS_STATE_OFF;
    UINT1               u1PrevExistState = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcCktAdd ()\n"));

    /* There may be more than one entry encoded in the message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR Header
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
            == ISIS_FAILURE)
        {
            /* The Context where the Circuit is to be added is not found. 
             * since a Context is always required to construct circuit database,
             * we shall create the Context and include the circuit information
             * into the newly created context. This context will get updated
             * with appropriate values when an update for this context is 
             * received later
             *
             * Refer to the beginning of "isfltr.c" for more details
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));

            pContext = (tIsisSysContext *) ISIS_MEM_ALLOC (ISIS_BUF_CTXT,
                                                           sizeof
                                                           (tIsisSysContext));
            if (pContext == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Context During Bulk/LSU\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisFltProcLSUFailure ();
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrProcCktAdd ()\n"));
                return ISIS_FAILURE;
            }

            /* Initialize the System Context with the default values.
             */

            IsisCtrlInitSysContext (pContext, u4InstIdx);
            IsisCtrlAddSysContext (pContext);
        }

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

        pCktRec = (tIsisCktEntry *) ISIS_MEM_ALLOC (ISIS_BUF_CKTS,
                                                    sizeof (tIsisCktEntry));
        if (pCktRec == NULL)
        {
            i4RetVal = ISIS_FAILURE;
            IsisFltProcLSUFailure ();
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Circuit During Bulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktAdd ()\n"));
            return i4RetVal;
        }
        /* Reset cache-pointer for isisAdjTable in this Ckt entry */
        pCktRec->pLastAdjEntry = NULL;

        /* Now decode the information from the message and construct a new
         * circuit record which is to be inserted into the database
         */

        IsisFltrGetCktEntry (pu1Msg, pCktRec, &u2Offset);

        pCktRec->pContext = pContext;

        /* If the Record doesn't exist in the database, add it to the
         * the Database else update the existing the record.
         */

        i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry);

        if (i4RetVal != ISIS_SUCCESS)
        {
            /* The required circuit is not yet included in the database. Hence
             * add it
             */

            IsisAdjAddCkt (pContext, pCktRec);
            if (pCktRec->u1CktExistState == ISIS_ACTIVE)
            {
                /* Number of Active circuits is to be incremented only if the
                 * Row Status is ACTIVE
                 */

                pContext->CktTable.u4NumActCkts++;
            }
            pCktEntry = pCktRec;
        }
        else
        {
            /* The circuit is already present in the database. Update the
             * existing record with the new values just now retrieved
             */

            u1PrevAdminState = pCktEntry->bCktAdminState;
            u1PrevExistState = pCktEntry->u1CktExistState;
            u1PrevIfState = pCktEntry->u1CktIfStatus;
            IsisAdjUpdtCktRec (pCktEntry, pCktRec);

            /* Free the new record built since the existing record itself is
             * updated
             */

            ISIS_MEM_FREE (ISIS_BUF_CKTS, (UINT1 *) pCktRec);
        }

        /* Before Processing CktUp Event, ensure that System Context
         * is Operationally UP, and all the Circuit Level Records
         * are added.
         */

        if ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
            && (((pCktEntry->u1CktLevel == ISIS_LEVEL1)
                 && (pCktEntry->pL1CktInfo != NULL))
                || ((pCktEntry->u1CktLevel == ISIS_LEVEL2)
                    && (pCktEntry->pL2CktInfo != NULL))
                || ((pCktEntry->u1CktLevel == ISIS_LEVEL12)
                    && (pCktEntry->pL1CktInfo != NULL)
                    && (pCktEntry->pL2CktInfo != NULL))))
        {
            /* Compare the Previous state and current state a
             * detect the Change in circuit status, and then
             * process the event accordingly
             */

            if (((u1PrevAdminState != ISIS_STATE_ON)
                 || (u1PrevIfState != ISIS_STATE_ON)
                 || (u1PrevExistState != ISIS_ACTIVE))
                && ((pCktEntry->bCktAdminState == ISIS_STATE_ON)
                    && (pCktEntry->u1CktIfStatus == ISIS_STATE_ON)
                    && (pCktEntry->u1CktExistState == ISIS_ACTIVE)))
            {
                IsisUpdProcCktUp (pContext, pCktEntry, pCktEntry->u1CktLevel);
            }
            else if (((u1PrevAdminState == ISIS_STATE_ON)
                      && (u1PrevIfState == ISIS_STATE_ON)
                      && (u1PrevExistState == ISIS_ACTIVE))
                     && ((pCktEntry->bCktAdminState != ISIS_STATE_ON)
                         || (pCktEntry->u1CktIfStatus != ISIS_STATE_ON)
                         || (pCktEntry->u1CktExistState != ISIS_ACTIVE)))
            {
                IsisUpdProcCktDown (pContext, pCktEntry, pCktEntry->u1CktLevel);
            }
        }

        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcCktDelete ()
 * Description : This routine retrieves the circuit information included in the
 *               received message and deletes the associated circuits from the
 *               database
 * Input(s)    : *pu1Msg   - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful Deletion of the Circuit Record 
 *                             from the circuit Database
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcCktDelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcCktDelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                           &pContext)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found - Ignore Ckt\n",
                 u4InstIdx));

        i4RetVal = ISIS_FAILURE;
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktDelete ()\n"));
        return i4RetVal;
    }

    /* For deletion of circuits we include indices only
     */

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (pCktRec->u1CktExistState == ISIS_ACTIVE)
        {
            pContext->CktTable.u4NumActCkts--;
        }

        if ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
        {
            if ((pCktRec->bCktAdminState == ISIS_STATE_ON)
                && (pCktRec->u1CktIfStatus == ISIS_STATE_ON)
                && (pCktRec->u1CktExistState == ISIS_ACTIVE))
            {
                IsisUpdProcCktDown (pContext, pCktRec, pCktRec->u1CktLevel);
            }
        }
        IsisAdjDelCkt (pContext, u4CktIdx);
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktDelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcCktLevelAdd ()
 * Description : This routine decodes the given FLTR message 'pu1Msg' and
 *               retrieves the circuit level information. It then constructs a
 *               circuit level record based on the information retrieved and 
 *               adds it to the circuit database. If the context where the given
 *               circuit level information is to be added is not available, then
 *               it creates a Context, marks it as NOT_READY and updates the 
 *               circuit level information into the newly created context.
 * Input(s)    : pu1Msg   - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of the Circuit Level 
 *                             Record to the Circuit Database 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcCktLevelAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT1               u1PrevSelfDIS = ISIS_FALSE;
    UINT1               u1CktLvlIdx = 0;
    UINT1               u1NewRec = ISIS_FALSE;
    UINT1               au1PrevDIS[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
    UINT2               u2Offset = 0;
    UINT2               u2OffsetMax = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4PrevFullMetric = 0;
    tIsisMetric         PrevMetric;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevelRec = NULL;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisEvtDISChg     *pDISEvt = NULL;
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcCktLevelAdd ()\n"));

    MEMSET (au1PrevDIS, 0x00, ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
    MEMSET (PrevMetric, 0x00, ISIS_NUM_METRICS);
    /* More than one circuit level entry can be encoded in a single message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);
    u2OffsetMax = u2Offset;

    while ((u1EntryCount != 0) && (u2Offset <= u2OffsetMax * u1EntryCount))
    {
        /* Get all the indices required for updating the circuit level record
         * Instance Index, Circuit Index and Circuit Level Index
         */

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

        ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1CktLvlIdx);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                               &pContext)) != ISIS_SUCCESS)
        {
            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));

            /* Circuit Level Records form a part of Circuit Database which in
             * turn form a part of Context. Since the required Context is not
             * available yet, create the required Context, Create the circuit
             * and add the circuit level record to the newly created circuit.
             * Mark both the circuit and the Context as NOT_READY which will get
             * updated with appropriate values when the updates for these
             * records are received
             *
             * Refer to the beginning of "isfltr.c" for more details
             */

            pContext = (tIsisSysContext *)
                ISIS_MEM_ALLOC (ISIS_BUF_CTXT, sizeof (tIsisSysContext));

            if (pContext == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Context During Bulk/LSU\n"));
                IsisFltProcLSUFailure ();
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
                return ISIS_FAILURE;
            }

            IsisCtrlInitSysContext (pContext, u4InstIdx);
            IsisCtrlAddSysContext (pContext);

            /* Create the circuit
             */

            pCktRec = (tIsisCktEntry *) ISIS_MEM_ALLOC (ISIS_BUF_CKTS,
                                                        sizeof (tIsisCktEntry));
            if (pCktRec == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : CktRec During Bulk/LSU\n"));
                IsisFltProcLSUFailure ();
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
                return ISIS_FAILURE;
            }

            pCktRec->u4CktIdx = u4CktIdx;
            pCktRec->u1CktExistState = ISIS_NOT_READY;
            pCktRec->u1QFlag = 0;
            /* Reset cache-pointer for isisAdjTable for newly created Ckt */
            pCktRec->pLastAdjEntry = NULL;

            IsisAdjAddCkt (pContext, pCktRec);

            pCktRec->pContext = pContext;
        }
        else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                               &pCktRec)) != ISIS_SUCCESS)
        {
            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Circuit Idx [ %u ] Not Found\n", u4CktIdx));

            /* Context is available but the circuit is not available. Create the
             * circuit, mark it as NOT_READY, and add it to the context. Then
             * add the circuit level record to the newly created circuit
             */

            pCktRec = (tIsisCktEntry *) ISIS_MEM_ALLOC (ISIS_BUF_CKTS,
                                                        sizeof (tIsisCktEntry));
            if (pCktRec == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : CktRec During Bulk/LSU\n"));
                IsisFltProcLSUFailure ();
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
                return ISIS_FAILURE;
            }
            pCktRec->u4CktIdx = u4CktIdx;
            pCktRec->u1CktExistState = ISIS_NOT_READY;
            pCktRec->u1QFlag = 0;

            /* Reset cache-pointer for isisAdjTable for newly created Ckt */
            pCktRec->pLastAdjEntry = NULL;

            IsisAdjAddCkt (pContext, pCktRec);

            pCktRec->pContext = pContext;
        }

        /* At this point we would haveeither retrieved appropriate Context and
         * Circuitb or we would have created new records. Insert the circuit
         * level information to the circuit database
         */

        pCktLevelRec = (tIsisCktLevel *) ISIS_MEM_ALLOC (ISIS_BUF_CKTL,
                                                         sizeof
                                                         (tIsisCktLevel));
        if (pCktLevelRec == NULL)
        {
            i4RetVal = ISIS_FAILURE;
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : CktLvlRec During Bulk/LSU\n"));
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
            return i4RetVal;
        }

        IsisFltrGetCktLevelRec (pu1Msg, pCktLevelRec, &u2Offset);

        /* If the Record doesn't exist in the database, add it to the
         * the Database else update the existing record.
         */

        if (u1CktLvlIdx == ISIS_LEVEL1)
        {
            pCktLvlEntry = pCktRec->pL1CktInfo;
        }
        else
        {
            pCktLvlEntry = pCktRec->pL2CktInfo;
        }

        if (pCktLvlEntry == NULL)
        {
            IsisAdjAddCktLvlRec (pCktRec, pCktLevelRec);
            pCktLvlEntry = pCktLevelRec;
            u1NewRec = ISIS_TRUE;
        }
        else
        {
            MEMCPY (au1PrevDIS, pCktLvlEntry->au1CktLanDISID,
                    ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
            MEMCPY (PrevMetric, pCktLvlEntry->Metric, (sizeof (tIsisMetric)));
            u4PrevFullMetric = pCktLvlEntry->u4FullMetric;
            u1PrevSelfDIS = pCktLvlEntry->bIsDIS;
            IsisAdjUpdtCktLvlRec (pCktLvlEntry, pCktLevelRec);
        }

        if ((u1NewRec == ISIS_TRUE)
            && (pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
            && (((pCktRec->u1CktLevel == ISIS_LEVEL1)
                 && (pCktRec->pL1CktInfo != NULL))
                || ((pCktRec->u1CktLevel == ISIS_LEVEL2)
                    && (pCktRec->pL2CktInfo != NULL))
                || ((pCktRec->u1CktLevel == ISIS_LEVEL12)
                    && (pCktRec->pL1CktInfo != NULL)
                    && (pCktRec->pL2CktInfo != NULL))))
        {
            if ((pCktRec->bCktAdminState == ISIS_STATE_ON)
                && (pCktRec->u1CktIfStatus == ISIS_STATE_ON)
                && (pCktRec->u1CktExistState == ISIS_ACTIVE))
            {
                IsisUpdProcCktUp (pContext, pCktRec, pCktRec->u1CktLevel);
                if ((pCktRec->pL1CktInfo != NULL)
                    && (MEMCMP (pContext->SysActuals.au1SysID,
                                pCktRec->pL1CktInfo->au1CktLanDISID,
                                ISIS_SYS_ID_LEN) == 0))
                {
                    pDISEvt = (tIsisEvtDISChg *)
                        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDISChg));
                    if (pDISEvt != NULL)
                    {
                        pDISEvt->u1EvtID = ISIS_EVT_DIS_CHANGE;
                        pDISEvt->u1CktLevel = ISIS_LEVEL1;
                        pDISEvt->u4CktIdx = pCktRec->u4CktIdx;
                        MEMSET (pDISEvt->au1PrevDIS, 0x00,
                                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                        MEMCPY (pDISEvt->au1CurrDIS,
                                pCktRec->pL1CktInfo->au1CktLanDISID,
                                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                        IsisUpdProcDISChgEvt (pContext, pDISEvt);
                    }
                }

                if ((pCktRec->pL2CktInfo != NULL)
                    && (MEMCMP (pContext->SysActuals.au1SysID,
                                pCktRec->pL2CktInfo->au1CktLanDISID,
                                ISIS_SYS_ID_LEN) == 0))
                {
                    pDISEvt = (tIsisEvtDISChg *)
                        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDISChg));
                    if (pDISEvt != NULL)
                    {
                        pDISEvt->u1EvtID = ISIS_EVT_DIS_CHANGE;
                        pDISEvt->u1CktLevel = ISIS_LEVEL2;
                        pDISEvt->u4CktIdx = pCktRec->u4CktIdx;
                        MEMSET (pDISEvt->au1PrevDIS, 0x00,
                                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                        MEMCPY (pDISEvt->au1CurrDIS,
                                pCktRec->pL2CktInfo->au1CktLanDISID,
                                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                        IsisUpdProcDISChgEvt (pContext, pDISEvt);
                    }
                }
                if ((pCktRec->pL1CktInfo != NULL)
                    && (pCktRec->pL1CktInfo->bIsDIS == ISIS_TRUE))
                {
                    IsisUpdProcDISStatChgEvt (pContext, pCktRec, ISIS_LEVEL1,
                                              ISIS_DIS_ELECTED);
                }
                if ((pCktRec->pL2CktInfo != NULL)
                    && (pCktRec->pL2CktInfo->bIsDIS == ISIS_TRUE))
                {
                    IsisUpdProcDISStatChgEvt (pContext, pCktRec, ISIS_LEVEL2,
                                              ISIS_DIS_ELECTED);
                }
            }
        }

        if ((u1NewRec == ISIS_FALSE)
            && (pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
            && (pCktRec->bCktAdminState == ISIS_STATE_ON)
            && (pCktRec->u1CktExistState == ISIS_ACTIVE)
            && (pCktRec->u1CktIfStatus == ISIS_STATE_ON))
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC)
            {
                if (MEMCMP (PrevMetric, pCktLvlEntry->Metric,
                            (sizeof (tIsisMetric))) != 0)
                {
                    IsisUpdProcCktModify (pContext, pCktRec,
                                          pCktLvlEntry->Metric,
                                          pCktLvlEntry->u4FullMetric,
                                          u1CktLvlIdx);
                }
            }
            else
            {
                if (u4PrevFullMetric == pCktLvlEntry->u4FullMetric)
                {
                    IsisUpdProcCktModify (pContext, pCktRec,
                                          pCktLvlEntry->Metric,
                                          pCktLvlEntry->u4FullMetric,
                                          u1CktLvlIdx);
                }

            }

            pDISEvt = (tIsisEvtDISChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDISChg));
            if (pDISEvt != NULL)
            {
                pDISEvt->u1EvtID = ISIS_EVT_DIS_CHANGE;
                pDISEvt->u1CktLevel = u1CktLvlIdx;
                pDISEvt->u4CktIdx = pCktRec->u4CktIdx;

                MEMCPY (pDISEvt->au1PrevDIS, au1PrevDIS,
                        ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                MEMCPY (pDISEvt->au1CurrDIS, pCktLvlEntry->au1CktLanDISID,
                        ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                IsisUpdProcDISChgEvt (pContext, pDISEvt);
            }

            if (pCktLvlEntry->bIsDIS == ISIS_TRUE)
            {
                IsisUpdProcDISStatChgEvt (pContext, pCktRec, u1CktLvlIdx,
                                          ISIS_DIS_ELECTED);
            }
            else if (u1PrevSelfDIS != pCktLvlEntry->bIsDIS)
            {
                IsisUpdProcDISStatChgEvt (pContext, pCktRec, u1CktLvlIdx,
                                          ISIS_DIS_RESIGNED);
            }

        }

        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcCktLevelDelete ()
 * Description : This routine retrieves the circuit level database indices
 *               included in the received message and deletes the corresponding
 *               records from the circuit database
 * Input(s)    : pu1Msg  - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful deletion of Circuit Level Record 
 *                             from the Circuit Database
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcCktLevelDelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1CktLevelIdx = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcCktLevelDelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                           &pContext)) != ISIS_SUCCESS)
    {
        /* Without an Instance Index we cannot delete anything
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
        FTP_EE ((ISIS_LGST,
                 "FLT <X> : Exiting IsisFltrProcCktLevelDelete ()\n"));
        return i4RetVal;
    }

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);
    if (i4RetVal != ISIS_SUCCESS)
    {
        /* Without a Circuit Index we cannot delete circuit level record
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Circuit Idx [ %u ] Not Found\n", u4CktIdx));
        FTP_EE ((ISIS_LGST,
                 "FLT <X> : Exiting IsisFltrProcCktLevelDelete ()\n"));
        return i4RetVal;
    }

    /* We have both Context and Circuit in hand. Delete the Circuit level record
     * whose index is included in the message
     */

    ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1CktLevelIdx);

    IsisAdjDelCktLvlRec (pContext, pCktRec, u1CktLevelIdx);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktLevelDelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcAdjAdd ()
 * Description : This routine 
 * Input(s)    : *pu1Msg  - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of the Adjacency Record 
 *                             to the Circuit database
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcAdjAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT1               u1PrevState = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4AdjIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisEvtDISChg     *pDISEvt = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcAdjAdd ()\n"));

    /* More than one adjacency information may be enocded in the received FLTR
     * message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                               &pContext)) != ISIS_SUCCESS)
        {
            /* Adjacencies can never exist without a context i.e. Context should
             * have already been Active before adjacencies are established and
             * hence Context should have been available before adjacency updates
             * are received. Ignore the adjacency update
             *
             * Refer to the beginning of "isfltr.c" for more details
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
            u2Offset = (UINT2) (u2Offset + (8 + sizeof (tIsisAdjEntry)));
            u1EntryCount--;
            continue;
        }

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

        i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

        if (i4RetVal != ISIS_SUCCESS)
        {
            /* Adjacencies can never exist without a circuit i.e. Circuit should
             * have already been Active before adjacencies are established and
             * hence Circuit should have been available before adjacency updates
             * are received. Ignore the adjacency update
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Circuit Idx [ %u ] Not Found\n", u4CktIdx));
            u2Offset = (UINT2) (u2Offset + (4 + sizeof (tIsisAdjEntry)));
            u1EntryCount--;
            continue;
        }

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4AdjIdx);

        pAdjRec = (tIsisAdjEntry *) ISIS_MEM_ALLOC (ISIS_BUF_ADJN,
                                                    sizeof (tIsisAdjEntry));
        if (pAdjRec == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Adj During Bulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcAdjAdd ()\n"));
            return (ISIS_FAILURE);
        }

        IsisFltrGetAdjRec (pu1Msg, pAdjRec, &u2Offset);

        /* Get the Circuit Level Record
         */

        if (pCktRec->u1CktType == ISIS_P2P_CKT)
        {
            pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
        }
        if (pAdjRec->u1AdjUsage == ISIS_LEVEL1)
        {
            pCktLevel = pCktRec->pL1CktInfo;
        }
        if (pAdjRec->u1AdjUsage == ISIS_LEVEL2)
        {
            pCktLevel = pCktRec->pL2CktInfo;
        }

        /* If the Record doesn't exist in the database, add it to the
         * the Database else update the existing the record.
         */

        i4RetVal = IsisAdjGetAdjRec (pCktRec, u4AdjIdx, &pAdjEntry);

        if (pAdjEntry == NULL)
        {
            IsisAdjAddAdj (pContext, pCktRec, pAdjRec, ISIS_TRUE);
            if ((pCktLevel != NULL)
                && (ISIS_GET_ADJ_STATE (pAdjRec) == ISIS_ADJ_UP))
            {
                IsisUpdProcAdjChange (pContext, pCktRec, pCktLevel->Metric,
                                      pCktLevel->u4FullMetric,
                                      pAdjRec->u1AdjUsage,
                                      pAdjRec->au1AdjNbrSysID, ISIS_ADJ_UP);
            }
            /* Elect a DIS from the List of available adjacencies
             */
            if (pCktRec->u1CktType == ISIS_BC_CKT)
            {
                pDISEvt = (tIsisEvtDISChg *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDISChg));
                if ((pDISEvt != NULL) && (pCktLevel != NULL))
                {
                    pDISEvt->u1EvtID = ISIS_EVT_DIS_CHANGE;
                    pDISEvt->u1CktLevel = (pAdjRec->u1AdjUsage == ISIS_LEVEL1) ?
                        ISIS_LEVEL1 : ISIS_LEVEL2;
                    pDISEvt->u4CktIdx = pCktRec->u4CktIdx;

                    MEMSET (pDISEvt->au1PrevDIS, 0x00,
                            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                    MEMCPY (pDISEvt->au1CurrDIS, pCktLevel->au1CktLanDISID,
                            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                    IsisUpdProcDISChgEvt (pContext, pDISEvt);
                }
                else
                {
                    ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pDISEvt);
                }
            }
        }
        else
        {
            u1PrevState = pAdjEntry->u1AdjState;

            IsisAdjUpdtAdjRec (pAdjEntry, pAdjRec);

            if ((u1PrevState != pAdjEntry->u1AdjState)
                && (pAdjEntry->u1AdjState == ISIS_ADJ_UP))
            {
                pCktLevel->u4NumAdjs++;

                if (pCktLevel->u4NumAdjs == 1)
                {
                    if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
                    {
                        IsisUtlSetBPat (&(pContext->CktTable.pu1L1CktMask),
                                        pCktRec->u4CktIdx);
                    }
                    if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
                    {
                        IsisUtlSetBPat (&(pContext->CktTable.pu1L2CktMask),
                                        pCktRec->u4CktIdx);
                    }
                }

                if (pAdjEntry->u1AdjUsage != ISIS_LEVEL2)
                {
                    (pContext->CktTable.u4NumL1Adjs)++;
                }

                if (pAdjEntry->u1AdjUsage != ISIS_LEVEL1)
                {
                    (pContext->CktTable.u4NumL2Adjs)++;
                }

                IsisUpdProcAdjChange (pContext, pCktRec, pCktLevel->Metric,
                                      pCktLevel->u4FullMetric,
                                      pAdjEntry->u1AdjUsage,
                                      pAdjEntry->au1AdjNbrSysID, ISIS_ADJ_UP);
            }
            ISIS_MEM_FREE (ISIS_BUF_ADJN, (UINT1 *) pAdjRec);
        }

        i4RetVal = ISIS_SUCCESS;
        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcAdjAdd ()\n"));
    return i4RetVal;
}

/****************************************************************************
 * Function    : IsisFltrProcAdjDelete ()
 * Description : This routine deletes the Adjacency record, whose indices are
 *               extracted from the received FLTR message 'pu1Msg', from the
 *               adjacency database
 * Input(s)    : *pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful deletion of Adjacency Record 
 *                             from the adjacency database
 *               ISIS_FAILURE, Otherwise
 ****************************************************************************/

PRIVATE INT4
IsisFltrProcAdjDelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4AdjIdx = 0;
    UINT4               u4DirIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcAdjDelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                           &pContext)) != ISIS_SUCCESS)
    {
        /* Adjacencies cannot be deleted without a Context. Ignore the message
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
        return i4RetVal;
    }

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4AdjIdx);

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetAdjRec (pCktRec, u4AdjIdx, &pAdjRec);

        if (i4RetVal == ISIS_SUCCESS)
        {
            if (pCktRec->u1CktType == ISIS_P2P_CKT)
            {
                pCktLevel = ISIS_GET_P2P_CKT_LEVEL (pCktRec);
            }
            else if (pCktRec->u1CktLevel == ISIS_LEVEL1)
            {
                pCktLevel = pCktRec->pL1CktInfo;
            }
            else
            {
                pCktLevel = pCktRec->pL2CktInfo;
            }
            if (ISIS_GET_ADJ_STATE (pAdjRec) == ISIS_ADJ_UP)
            {
                IsisUpdProcAdjChange (pContext, pCktRec, pCktLevel->Metric,
                                      pCktLevel->u4FullMetric,
                                      pAdjRec->u1AdjUsage,
                                      pAdjRec->au1AdjNbrSysID, ISIS_ADJ_DOWN);
            }

            IsisAdjDelAdj (pContext, pCktRec, pAdjRec, ISIS_FALSE, &u4DirIdx);
        }
        else
        {
            /* No such adjacency in the database. Ignore the message
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Adj Idx [ %u ] Not Found\n", u4AdjIdx));
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
            return i4RetVal;
        }
    }
    else
    {
        /* Adjacencies cannot be deleted without a Circuit. Ignore the message
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Circuit Idx [ %u ] Not Found\n", u4CktIdx));
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
        return i4RetVal;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcAdjDelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcAdjAAAdd ()
 * Description : This routine retireves the Adjacency Area Address information
 *               from the received FLTR message and adds it to the adjacency
 *               database.
 * Input(s)    : *pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of the Adjacency Area
 *                             Address Record to the Adjacency database
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcAdjAAAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4AdjIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisAdjAAEntry    *pAdjAARec = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcAdjAAAdd ()\n"));

    /* There may ba more than one Adjacency Area Address encoded in the FLTR
     * message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                               &pContext)) != ISIS_SUCCESS)
        {
            /* Adjacency Area Addresses can never exist without a context i.e. 
             * Context should have already been Active before adjacencies are 
             * established and hence Context should have been available before 
             * adjacency updates are received. Ignore the adjacency area address
             * update
             *
             * Refer to the beginning of "isfltr.c" for more details
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));

            /* Apart from the Adjacency Area Address record we include 3 indices
             * which identify the given record, a System Instance Index, Circuit
             * Index and an Adjacency Index. We have already adjusted the offset
             * for the system Instance Index, and hence adjust the offset for
             * Circuit and Adjacency Indices apart from Adjacency Area Address
             */

            u2Offset = (UINT2) (u2Offset + (8 + sizeof (tIsisAdjAAEntry)));
            u1EntryCount--;
            continue;
        }

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4AdjIdx);

        i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

        if (i4RetVal == ISIS_SUCCESS)
        {
            i4RetVal = IsisAdjGetAdjRec (pCktRec, u4AdjIdx, &pAdjRec);

            if (i4RetVal == ISIS_SUCCESS)
            {
                pAdjAARec = (tIsisAdjAAEntry *)
                    ISIS_MEM_ALLOC (ISIS_BUF_ADAA, sizeof (tIsisAdjAAEntry));
                if (pAdjAARec == NULL)
                {
                    PANIC ((ISIS_LGST,
                            ISIS_MEM_ALLOC_FAIL " :  AdjAA During Bulk/LSU\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    IsisFltProcLSUFailure ();
                    FTP_EE ((ISIS_LGST,
                             "FLT <X> : Exiting IsisFltrProcAdjAAAdd ()\n"));
                    return (ISIS_FAILURE);
                }

                IsisFltrGetAdjAARec (pu1Msg, pAdjAARec, &u2Offset);

                /* Since there is no case of updation for Adjacency Area
                 * Addresses, we need not check whether the address already
                 * exist
                 */

                IsisAdjAddAdjAA (pAdjRec, pAdjAARec);

                i4RetVal = ISIS_SUCCESS;
            }
            else
            {
                /* Adjacency Area Addresses cannot be added without a valid 
                 * adjacency index
                 */

                FTP_PT ((ISIS_LGST,
                         "FLT <E> : Adj Idx [ %u ] Not Found\n", u4AdjIdx));
            }
        }
        else
        {
            /* Adjacency Area Addresses cannot be added without a valid circuit
             * index
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Circuit Idx [ %u ] Not Found\n", u4CktIdx));
        }
        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcAdjAAAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcAdjAADelete ()
 * Description : This routine retrieves the indices corresponding to the
 *               Adjacency Area Address record that is to be deleted. It then
 *               deletes the record from the Adjacency Database
 * Input(s)    : pu1Msg  - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful deletion of Adjacency Area Address
 *                             Record from Adjacency Database
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcAdjAADelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               au1AreaAddr[ISIS_AREA_ADDR_LEN];
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIdx = 0;
    UINT4               u4AdjIdx = 0;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcAdjAADelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                           &pContext)) != ISIS_SUCCESS)
    {
        /* Adjacency Area Address cannot be deleted without a valid Context
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcAdjAADelete ()\n"));

        return i4RetVal;
    }

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIdx);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4AdjIdx);

    ISIS_RED_READ_N_BYTE (pu1Msg, au1AreaAddr, u2Offset, ISIS_AREA_ADDR_LEN);
    u2Offset = (UINT2) (u2Offset + ISIS_AREA_ADDR_LEN);

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);
    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetAdjRec (pCktRec, u4AdjIdx, &pAdjRec);

        if (i4RetVal == ISIS_SUCCESS)
        {
            IsisAdjDelAdjAA (pAdjRec, au1AreaAddr);
        }
        else
        {
            /* Adjacency Area Addresses cannot be deleted without an Adjacency.
             * Ignore the message
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Adjacency Idx [ %u ] Not Found\n", u4AdjIdx));
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
            return i4RetVal;
        }
    }
    else
    {
        /* Adjacency Area Addresses cannot be deleted without a Circuit. Ignore
         * the message
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Circuit Idx [ %u ] Not Found\n", u4CktIdx));
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcCktLevelAdd ()\n"));
        return i4RetVal;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcAdjAADelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcIPRAAdd ()
 * Description : This routine decodes the received FLTR message, extracts the
 *               IPRA information included and adds it to the Circuit database.
 * Input(s)    : pu1Msg  - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of IPRA Record to 
 *                             Circuit Database 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcIPRAAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1NewRec = 0;
    UINT1               u1IPRAType = 0;
    UINT1               u1EntryCount = 0;
    UINT1               u1PrevExistState = 0;
    UINT1               u1PrevAdminState = 0;
    UINT2               u2Offset = 0;
    UINT4               u4IPRAIdx = 0;
    UINT4               u4InstIdx = 0;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisIPRAEntry     *pIPRAEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcIPRAAdd ()\n"));

    /* More tha one IPRA entry can be encoded in the received FLTR message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        /* Extract all the required indices
         */

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4IPRAIdx);

        ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1IPRAType);

        i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

        if (i4RetVal != ISIS_SUCCESS)
        {
            /* Refer to the beginning of "isfltr.c" for more details
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));

            pContext = (tIsisSysContext *)
                ISIS_MEM_ALLOC (ISIS_BUF_CTXT, sizeof (tIsisSysContext));

            if (pContext == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Context During Bulk/LSU\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisFltProcLSUFailure ();
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrProcIPRAAdd ()\n"));
                return ISIS_FAILURE;
            }
            IsisCtrlInitSysContext (pContext, u4InstIdx);
            IsisCtrlAddSysContext (pContext);

        }

        pIPRARec = (tIsisIPRAEntry *) ISIS_MEM_ALLOC (ISIS_BUF_IPRA,
                                                      sizeof (tIsisIPRAEntry));
        if (pIPRARec == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IPRA DuringBulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcIPRAAdd ()\n"));
            return (ISIS_FAILURE);
        }

        IsisFltrGetIPRARec (pu1Msg, pIPRARec, &u2Offset);

        /* If the Record doesn't exist in the database, add it to the
         * the Database else update the existing the record.
         */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, u4IPRAIdx, u1IPRAType, &pIPRAEntry);
        if (i4RetVal == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST,
                     "IP <X> : IPRA record for the index not found\n"));
        }

        if (pIPRAEntry == NULL)
        {
            u1NewRec = ISIS_TRUE;
            IsisCtrlAddIPRA (pContext, pIPRARec);
        }
        else
        {
            u1PrevAdminState = pIPRAEntry->u1IPRAAdminState;
            u1PrevExistState = pIPRAEntry->u1IPRAExistState;
            IsisCtrlUpdtIPRA (pIPRAEntry, pIPRARec);
        }

        if ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
        {
            if ((u1NewRec == ISIS_TRUE)
                || (((u1PrevAdminState != ISIS_STATE_ON)
                     || (u1PrevExistState != ISIS_ACTIVE))
                    && ((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON)
                        && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_UP;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    IsisUpdProcIPRAChgEvt (pContext, pIPRAEvt);
                }
            }
            else if (((u1PrevAdminState == ISIS_STATE_ON)
                      && (u1PrevExistState == ISIS_ACTIVE))
                     && ((pIPRARec->u1IPRAAdminState != ISIS_STATE_ON)
                         || (pIPRARec->u1IPRAExistState != ISIS_ACTIVE)))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_DOWN;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    IsisUpdProcIPRAChgEvt (pContext, pIPRAEvt);
                }
            }
            else if (((u1PrevAdminState == ISIS_STATE_ON)
                      && (u1PrevExistState == ISIS_ACTIVE))
                     && ((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON)
                         && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)))
            {
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    IsisUpdProcIPRAChgEvt (pContext, pIPRAEvt);
                }
            }
        }
        i4RetVal = ISIS_SUCCESS;
        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcIPRAAdd ()\n"));
    return i4RetVal;
}

/****************************************************************************
 * Function    : IsisFltrProcIPRADelete ()
 * Description : This routine extracts the IPRA indices included in the received
 *               FLTR message and deletes the corresponding IPRA record from the
 *               circuit database
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful deletion of IPRA Record from
 *                             Circuit Database
 *               ISIS_FAILURE, Otherwise
 ****************************************************************************/

PRIVATE INT4
IsisFltrProcIPRADelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1IPRAType = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4IPRAIdx = 0;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcIPRADelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4IPRAIdx);

        ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1IPRAType);

        i4RetVal = IsisCtrlGetIPRARec (pContext, u4IPRAIdx, u1IPRAType,
                                       &pIPRARec);
        if (i4RetVal == ISIS_SUCCESS)
        {
            if ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
                && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_DOWN;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    IsisUpdProcIPRAChgEvt (pContext, pIPRAEvt);
                }
            }
            IsisCtrlDelIPRA (pContext, u4IPRAIdx, u1IPRAType);
        }
    }
    else
    {
        /* IPRAs cannot be deleted without a valid context index. Ignore the 
         * received message
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcIPRADelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcMAAAdd ()
 * Description : This routine retrieves the Manual Area Address information from
 *               the received FLTR message and adds it to the System Context
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of MAA Record to 
 *                             System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcMAAAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisMAAEntry      *pMAARec = NULL;
    tIsisMAAEntry      *pCurentMAARec = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtManAAChange *pEvtMAAChg = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcMAAAdd ()\n"));

    /* More than one MAA entry can be encoded in a single FLTR message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

        if (i4RetVal != ISIS_SUCCESS)
        {
            /* Refer to the beginning of "isfltr.c" for more details
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));

            pContext = (tIsisSysContext *)
                ISIS_MEM_ALLOC (ISIS_BUF_CTXT, sizeof (tIsisSysContext));

            if (pContext == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Context During Bulk/LSU\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisFltProcLSUFailure ();
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrProcMAAAdd ()\n"));
                return ISIS_FAILURE;
            }
            IsisCtrlInitSysContext (pContext, u4InstIdx);
            IsisCtrlAddSysContext (pContext);
        }

        pMAARec = (tIsisMAAEntry *) ISIS_MEM_ALLOC (ISIS_BUF_MAAT,
                                                    sizeof (tIsisMAAEntry));
        if (pMAARec == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA During Bulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcMAAAdd ()\n"));
            return (ISIS_FAILURE);
        }

        IsisFltrGetMAARec (pu1Msg, pMAARec, &u2Offset);

        /* If the Record doesn't exist in the database, add it to the
         * the Database else update the existing the record.
         */

        i4RetVal = IsisCtrlGetMAA (pContext, pMAARec->ManAreaAddr.au1AreaAddr,
                                   pMAARec->ManAreaAddr.u1Length,
                                   &pCurentMAARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST,
                     "IP <X> : Manual Area Address for the given index not found\n"));
        }

        if (pCurentMAARec == NULL)
        {
            IsisCtrlAddMAA (pContext, pMAARec);
        }
        else
        {
            /* This Condition Should not happen
             */
            ISIS_MEM_FREE (ISIS_BUF_MAAT, (UINT1 *) pMAARec);
        }

        if ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
        {
            pEvtMAAChg = (tIsisEvtManAAChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtManAAChange));
            if (pEvtMAAChg != NULL)
            {
                pEvtMAAChg->u1EvtID = ISIS_EVT_MAN_ADDR_CHANGE;
                pEvtMAAChg->u1Status = ISIS_MAA_ADDED;
                MEMCPY (pEvtMAAChg->au1AreaAddr,
                        pMAARec->ManAreaAddr.au1AreaAddr,
                        pMAARec->ManAreaAddr.u1Length);
                IsisUpdProcMAAChgEvt (pContext, pEvtMAAChg);
            }
        }
        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcMAAAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcMAADelete ()
 * Description : This routine retrieves the MAA indices encoded into the
 *               received FLTR message, fetches the MAA record and deletes it
 *               from the System Context
 * Input(s)    : pu1Msg  - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful deletion of the Manual Area
 *                             Address Record from the System Table 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcMAADelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               au1AreaAddr[ISIS_AREA_ADDR_LEN];
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisMAAEntry      *pMAARec = NULL;
    tIsisEvtManAAChange *pEvtMAAChg = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcMAADelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        ISIS_RED_READ_N_BYTE (pu1Msg, au1AreaAddr,
                              u2Offset, ISIS_AREA_ADDR_LEN);
        u2Offset = (UINT2) (u2Offset + ISIS_AREA_ADDR_LEN);

        i4RetVal =
            IsisCtrlGetMAA (pContext, au1AreaAddr, (UINT1) ISIS_AREA_ADDR_LEN,
                            &pMAARec);
        if (i4RetVal == ISIS_SUCCESS)
        {
            if ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
                && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
            {
                pEvtMAAChg = (tIsisEvtManAAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                    sizeof (tIsisEvtManAAChange));
                if (pEvtMAAChg != NULL)
                {
                    pEvtMAAChg->u1EvtID = ISIS_EVT_MAN_ADDR_CHANGE;
                    pEvtMAAChg->u1Status = ISIS_MAA_DROPPED;
                    MEMCPY (pEvtMAAChg->au1AreaAddr,
                            pMAARec->ManAreaAddr.au1AreaAddr,
                            pMAARec->ManAreaAddr.u1Length);
                    IsisUpdProcMAAChgEvt (pContext, pEvtMAAChg);
                }
            }
            IsisCtrlDelMAA (pContext, pMAARec->ManAreaAddr.u1Length,
                            pMAARec->ManAreaAddr.au1AreaAddr);
        }
    }
    else
    {
        /* MAA records cannot be deleted without a valid context index. Ignore
         * the received message
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcMAADelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcSAAdd ()
 * Description : This routine retrieves the SAA information encoded in the
 *               received FLTR message and constructs a new SAA record and adds
 *               it to the System Context
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of Summary Address record 
 *                             to the System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSAAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Status = 0;
    UINT1               u1EntryCount = 0;
    UINT1               u1PrevAdminState = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisSAEntry       *pSARec = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtSummAddrChg *pEvtSAChg = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSAAdd ()\n"));

    /* More than one SAA record can beencoded in a single FLTR message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                               &pContext)) != ISIS_SUCCESS)
        {
            /* Refer to the beginning of "isfltr.c" for more details
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));

            pContext = (tIsisSysContext *)
                ISIS_MEM_ALLOC (ISIS_BUF_CTXT, sizeof (tIsisSysContext));
            if (pContext == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Context During Bulk/LSU\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisFltProcLSUFailure ();
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrProcSAAdd ()\n"));
                return ISIS_FAILURE;
            }
            IsisCtrlInitSysContext (pContext, u4InstIdx);
            IsisCtrlAddSysContext (pContext);
        }

        pSARec = (tIsisSAEntry *) ISIS_MEM_ALLOC (ISIS_BUF_SATE,
                                                  sizeof (tIsisSAEntry));
        if (pSARec == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SAA During Bulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSAAdd ()\n"));
            return (ISIS_FAILURE);
        }

        IsisFltrGetSARec (pu1Msg, pSARec, &u2Offset);

        /* If the Record doesn't exist in the database, add it to the
         * the Database else update the existing the record.
         */

        i4RetVal = IsisCtrlGetSA (pContext, pSARec->au1SummAddr,
                                  pSARec->u1AddrType, pSARec->u1PrefixLen,
                                  &pSAEntry);

        if (i4RetVal != ISIS_SUCCESS)
        {
            IsisCtrlAddSA (pContext, pSARec);
            u1Status = ISIS_SUMM_ADDR_ADD;
            pSAEntry = pSARec;
        }
        else
        {
            if ((pSAEntry->u1ExistState == ISIS_ACTIVE)
                && (pSAEntry->u1AdminState != pSARec->u1AdminState))
            {
                u1Status = ISIS_SUMM_ADDR_ADMIN_CHG;
                u1PrevAdminState = pSAEntry->u1AdminState;
            }
            else if (pSAEntry->u1ExistState == ISIS_ACTIVE)
            {
                u1Status = ISIS_SUMM_ADDR_METRIC_CHG;
            }

            IsisCtrlUpdtSA (pSAEntry, pSARec);
            ISIS_MEM_FREE (ISIS_BUF_SATE, (UINT1 *) pSARec);
        }

        if ((pSAEntry->u1ExistState == ISIS_ACTIVE)
            && (pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
        {
            pEvtSAChg = (tIsisEvtSummAddrChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSummAddrChg));
            if (pEvtSAChg != NULL)
            {
                pEvtSAChg->u1Status = u1Status;
                pEvtSAChg->u1CurrAdminState = pSAEntry->u1AdminState;
                pEvtSAChg->u1PrevAdminState = u1PrevAdminState;
                pEvtSAChg->u1PrefixLen = pSAEntry->u1PrefixLen;
                ISIS_SET_METRIC (pContext, pEvtSAChg, pSAEntry);
                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pEvtSAChg->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV4_ADDR_LEN);
                }
                else
                {

                    MEMCPY (pEvtSAChg->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV6_ADDR_LEN);
                }
                IsisUpdProcSummAddrChgEvt (pContext, pEvtSAChg);
            }
        }
        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSAAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcSADelete ()
 * Description : This routine extracts the indices from the received FLTR
 *               message and deletes the corresponding SAA record from the
 *               System Context
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On Successful deletion of Summary Address from
 *                             System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSADelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1PrefixLen = 0;
    UINT1               u1AddrType = 0;
    UINT1               au1SummAddr[ISIS_MAX_IP_ADDR_LEN];
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisEvtSummAddrChg *pEvtSAChg = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSADelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                           &pContext)) != ISIS_SUCCESS)
    {
        /* SAA records cannot be deleted without a valid context index. Ignore
         * the received message
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSADelete ()\n"));
        return i4RetVal;
    }

    ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1AddrType);

    if (u1AddrType == ISIS_ADDR_IPV4)
    {
        ISIS_RED_READ_N_BYTE (pu1Msg, au1SummAddr,
                              u2Offset, ISIS_MAX_IPV4_ADDR_LEN);
        u2Offset = (UINT2) (u2Offset + ISIS_MAX_IPV4_ADDR_LEN);
    }
    else
    {
        ISIS_RED_READ_N_BYTE (pu1Msg, au1SummAddr, u2Offset,
                              ISIS_MAX_IPV6_ADDR_LEN);
        u2Offset = (UINT2) (u2Offset + ISIS_MAX_IPV6_ADDR_LEN);
    }

    ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1PrefixLen);

    i4RetVal = IsisCtrlGetSA (pContext, au1SummAddr, u1AddrType, u1PrefixLen,
                              &pSAEntry);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if ((pSAEntry->u1ExistState == ISIS_ACTIVE)
            && (pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
        {
            pEvtSAChg = (tIsisEvtSummAddrChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSummAddrChg));
            if (pEvtSAChg != NULL)
            {
                pEvtSAChg->u1Status = ISIS_SUMM_ADDR_DEL;
                pEvtSAChg->u1CurrAdminState = pSAEntry->u1AdminState;
                pEvtSAChg->u1PrefixLen = pSAEntry->u1PrefixLen;
                pEvtSAChg->u1AddrType = pSAEntry->u1AddrType;
                ISIS_SET_METRIC (pContext, pEvtSAChg, pSAEntry);
                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pEvtSAChg->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV4_ADDR_LEN);
                }
                else
                {
                    MEMCPY (pEvtSAChg->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV6_ADDR_LEN);
                }
                IsisUpdProcSummAddrChgEvt (pContext, pEvtSAChg);
            }
        }
        IsisCtrlDelSA (pContext, au1SummAddr, u1AddrType, u1PrefixLen);
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSADelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcIPIfAdd ()
 * Description : This routine retrieves the IP I/f information encoded in the
 *               received FLTR message and constructs a new IP I/f Address 
 *               record and adds it to the System Context
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of IP I/f Address record 
 *                             to the System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcIPIfAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;
    UINT2               u2OffsetMax = 0;
    UINT4               u4InstIdx = 0;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    tIsisIPIfAddr      *pExistIPIfRec = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtIPIFChange *pIPIfEvt = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcIPIfAdd ()\n"));

    /* More than one IP I/f Address record can be encoded in a single FLTR 
     * message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);
    u2OffsetMax = u2Offset;

    while ((u1EntryCount != 0) && (u2Offset <= u2OffsetMax * u1EntryCount))
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                               &pContext)) != ISIS_SUCCESS)
        {
            /* Refer to the beginning of "isfltr.c" for more details
             */

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));

            pContext = (tIsisSysContext *)
                ISIS_MEM_ALLOC (ISIS_BUF_CTXT, sizeof (tIsisSysContext));
            if (pContext == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Context During Bulk/LSU\n"));
                IsisFltProcLSUFailure ();
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrProcIPIfAdd ()\n"));
                return ISIS_FAILURE;
            }

            IsisCtrlInitSysContext (pContext, u4InstIdx);
            IsisCtrlAddSysContext (pContext);
        }

        pIPIfRec = (tIsisIPIfAddr *) ISIS_MEM_ALLOC (ISIS_BUF_IPIF,
                                                     sizeof (tIsisIPIfAddr));
        if (pIPIfRec == NULL)
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : IP I/f During Bulk/LSU\n"));
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcIPIfAdd ()\n"));
            return (ISIS_FAILURE);
        }

        IsisFltrGetIPIfRec (pu1Msg, pIPIfRec, &u2Offset);

        /* If the Record doesn't exist in the database, add it to the
         * the Database else update the existing the record.
         */

        i4RetVal = IsisCtrlGetIPIf (pContext, pIPIfRec->u4CktIfIdx,
                                    pIPIfRec->u4CktIfSubIdx,
                                    pIPIfRec->au1IPAddr, pIPIfRec->u1AddrType,
                                    &pExistIPIfRec);
        if (i4RetVal == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST,
                     "IP <X> : IP address not found for the given index\n"));
        }

        if (pExistIPIfRec == NULL)
        {
            IsisCtrlAddIPIf (pContext, pIPIfRec);
            i4RetVal = IsisAdjGetCktRecWithIfIdx (pIPIfRec->u4CktIfIdx,
                                                  pIPIfRec->u4CktIfSubIdx,
                                                  &pCktRec);

            if ((i4RetVal == ISIS_SUCCESS)
                && (pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
                && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
                && (pCktRec->bCktAdminState == ISIS_STATE_ON)
                && (pCktRec->u1CktExistState == ISIS_ACTIVE)
                && (pCktRec->u1CktIfStatus == ISIS_STATE_ON))
            {
                pIPIfEvt = (tIsisEvtIPIFChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPIFChange));
                if (pIPIfEvt != NULL)
                {
                    pIPIfEvt->u1EvtID = ISIS_EVT_IP_IF_ADDR_CHANGE;
                    pIPIfEvt->u1Status = ISIS_IP_IF_ADDR_ADD;
                    pIPIfEvt->u4CktIfIdx = pIPIfRec->u4CktIfIdx;
                    pIPIfEvt->u4CktIfSubIdx = pIPIfRec->u4CktIfSubIdx;
                    pIPIfEvt->u1AddrType = pIPIfRec->u1AddrType;
                    pIPIfEvt->u1SecondaryFlag = pIPIfRec->u1SecondaryFlag;
                    if (pIPIfEvt->u1AddrType == ISIS_ADDR_IPV4)
                    {
                        MEMCPY (pIPIfEvt->au1IPAddr, pIPIfRec->au1IPAddr,
                                ISIS_MAX_IPV4_ADDR_LEN);
                    }
                    else
                    {
                        MEMCPY (pIPIfEvt->au1IPAddr, pIPIfRec->au1IPAddr,
                                ISIS_MAX_IPV6_ADDR_LEN);
                    }
                    IsisUpdProcIPIfChgEvt (pContext, pIPIfEvt);
                }
            }

        }
        else
        {
            IsisCtrlUpdtIPIf (pIPIfRec, pExistIPIfRec);
            ISIS_MEM_FREE (ISIS_BUF_IPIF, (UINT1 *) pIPIfRec);
        }

        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcIPIfAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcIPIfDelete ()
 * Description : This routine extracts the indices from the received FLTR
 *               message and deletes the corresponding IP I/f record from the
 *               System Context
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On Successful deletion of IP I/f Address from
 *                             System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcIPIfDelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               au1IPAddr[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1AddrType = 0;
    UINT1               u1SecondaryFlag = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    UINT4               u4CktIfIdx = 0;
    UINT4               u4CktIfSubIdx = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtIPIFChange *pIPIfEvt = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcIPIfDelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        != ISIS_SUCCESS)
    {
        /* Refer to the beginning of "isfltr.c" for more details
         */

        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));

        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcIPIfDelete ()\n"));
        return i4RetVal;
    }

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIfIdx);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4CktIfSubIdx);

    ISIS_RED_READ_N_BYTE (pu1Msg, au1IPAddr, u2Offset, ISIS_MAX_IP_ADDR_LEN);
    u2Offset = (UINT2) (u2Offset + ISIS_MAX_IP_ADDR_LEN);

    ISIS_RED_READ_1_BYTE (pu1Msg, u2Offset, u1AddrType);

    ISIS_RED_READ_1_BYTE (pu1Msg, u2Offset, u1SecondaryFlag);

    i4RetVal = IsisAdjGetCktRecWithIfIdx (u4CktIfIdx, u4CktIfSubIdx, &pCktRec);

    if ((i4RetVal == ISIS_SUCCESS)
        && (pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
        && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
        && (pCktRec->bCktAdminState == ISIS_STATE_ON)
        && (pCktRec->u1CktExistState == ISIS_ACTIVE)
        && (pCktRec->u1CktIfStatus == ISIS_STATE_ON))
    {
        pIPIfEvt = (tIsisEvtIPIFChange *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPIFChange));
        if (pIPIfEvt != NULL)
        {
            pIPIfEvt->u1EvtID = ISIS_EVT_IP_IF_ADDR_CHANGE;
            pIPIfEvt->u1Status = ISIS_IP_IF_ADDR_DEL;
            pIPIfEvt->u4CktIfIdx = u4CktIfIdx;
            pIPIfEvt->u4CktIfSubIdx = u4CktIfSubIdx;
            pIPIfEvt->u1AddrType = u1AddrType;
            pIPIfEvt->u1SecondaryFlag = u1SecondaryFlag;
            if (pIPIfEvt->u1AddrType == ISIS_ADDR_IPV4)
            {
                MEMCPY (pIPIfEvt->au1IPAddr, au1IPAddr, ISIS_MAX_IPV4_ADDR_LEN);
            }
            else
            {
                MEMCPY (pIPIfEvt->au1IPAddr, au1IPAddr, ISIS_MAX_IPV6_ADDR_LEN);
            }

            IsisUpdProcIPIfChgEvt (pContext, pIPIfEvt);
        }
    }
    IsisCtrlDelIPIf (pContext, u4CktIfIdx, u4CktIfSubIdx, au1IPAddr,
                     u1AddrType);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcIPIfDelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcSysContextAdd ()
 * Description : This routine retrieves the Context information encoded in the
 *               received FLTR message and constructs a new Context record and 
 *               adds it to the global pool of system contexts
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful addition of System Context record 
 *                             to the global pool of system instances
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSysContextAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisSysContext    *pExistContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSysContextAdd ()\n"));

    /* More than one ontext record can be encoded in a single FLTR message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        pContext = (tIsisSysContext *)
            ISIS_MEM_ALLOC (ISIS_BUF_CTXT, sizeof (tIsisSysContext));

        if (pContext == NULL)
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Context During Bulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrProcSysContextAdd ()\n"));
            return (ISIS_FAILURE);
        }

        IsisFltrGetSysContextRec (pu1Msg, pContext, &u2Offset);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                               &pExistContext)) != ISIS_SUCCESS)
        {
            IsisCtrlAddSysContext (pContext);
            pExistContext = pContext;
        }
        else
        {

            /* Disable the Oper State in the Standby Node irrespective of the Oper State
             * obtained from the Active Node. The Operational State of the Standby Node
             * is made ISIS_UP only when the GO_ACTIVE command is issued by FutureRM
             */

            pExistContext->u1OperState = ISIS_DOWN;

            MEMCPY (&(pExistContext->SysActuals), &(pContext->SysActuals),
                    sizeof (tIsisSysActuals));
            MEMCPY (&(pExistContext->SysConfigs), &(pContext->SysConfigs),
                    sizeof (tIsisSysConfigs));
            MEMCPY (pExistContext->aProtSupp, pContext->aProtSupp,
                    sizeof (tIsisProtSupp) * ISIS_MAX_PROTS_SUPP);
            MEMCPY (&(pExistContext->SysRouterID), &(pContext->SysRouterID),
                    sizeof (tIsisIpAddr));
            ISIS_MEM_FREE (ISIS_BUF_CTXT, (UINT1 *) pContext);
        }

        i4RetVal = IsisCtrlAllocShortPath (pExistContext);
        if (i4RetVal == ISIS_FAILURE)
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL
                    " : Context Initialization Bulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrProcSysContextAdd ()\n"));
            return (ISIS_FAILURE);
        }

        if ((pExistContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
            && (pExistContext->SysActuals.u1SysAdminState == ISIS_STATE_ON))
        {
            IsisUpdProcISUpEvt (pExistContext);
            pCktRec = pExistContext->CktTable.pCktRec;
            while (pCktRec != NULL)
            {

                if (((pCktRec->u1CktLevel == ISIS_LEVEL1)
                     && (pCktRec->pL1CktInfo != NULL))
                    || ((pCktRec->u1CktLevel == ISIS_LEVEL2)
                        && (pCktRec->pL2CktInfo != NULL))
                    || ((pCktRec->u1CktLevel == ISIS_LEVEL12)
                        && (pCktRec->pL1CktInfo != NULL)
                        && (pCktRec->pL2CktInfo != NULL)))
                {
                    if ((pCktRec->bCktAdminState == ISIS_STATE_ON)
                        && (pCktRec->u1CktIfStatus == ISIS_STATE_ON)
                        && (pCktRec->u1CktExistState == ISIS_ACTIVE))
                    {
                        IsisUpdProcCktUp (pExistContext, pCktRec,
                                          pCktRec->u1CktLevel);
                    }
                }
                pCktRec = pCktRec->pNext;
            }
        }

        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSysContextAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcSysConfigsAdd ()
 * Description : This routine retrieves the System Configs encoded in the
 *               received FLTR message and updates the values of the SysConfigs
 *               structure present in the System Context
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful Updation of System Configs values 
 *                             in the System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSysConfigsAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSysConfigsAdd ()\n"));

    /* More than one Configs structure can be encoded in a single FLTR message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);
        u2Offset = (UINT2) (u2Offset + 4);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                               &pContext)) == ISIS_SUCCESS)
        {
            IsisFltrGetSysConfRec (pu1Msg, &(pContext->SysConfigs), &u2Offset);
        }
        else
        {
            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
        }
        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSysConfigsAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcSysActualsAdd ()
 * Description : This routine retrieves the System Actuals encoded in the
 *               received FLTR message and updates the values of the SysActuals
 *               structure present in the System Context
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of System Actuals values 
 *                             in the System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSysActualsAdd (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1EntryCount = 0;
    UINT1               u1PrevExistState = 0;
    UINT1               u1PrevAdminState = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSysActualsAdd ()\n"));

    /* More than one Configs structure can be encoded in a single FLTR message
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_FT_ENTRYCOUNT_POS, u1EntryCount);

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    while (u1EntryCount != 0)
    {
        ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

        if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                               &pContext)) == ISIS_SUCCESS)
        {
            /* Note the Previous ExistState and AdminState of the context
             */

            u1PrevExistState = pContext->SysActuals.u1SysExistState;
            u1PrevAdminState = pContext->SysActuals.u1SysAdminState;

            IsisFltrGetSysActualsRec (pu1Msg,
                                      &(pContext->SysActuals), &u2Offset);

            if (((u1PrevExistState != ISIS_ACTIVE)
                 || (u1PrevAdminState != ISIS_STATE_ON))
                && ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
                    && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)))
            {
                IsisUpdProcISUpEvt (pContext);
            }
            if (((u1PrevExistState == ISIS_ACTIVE)
                 && (u1PrevAdminState == ISIS_STATE_ON))
                && ((pContext->SysActuals.u1SysAdminState != ISIS_STATE_ON)
                    || (pContext->SysActuals.u1SysExistState != ISIS_ACTIVE)))
            {
                IsisUpdProcISDownEvt (pContext);
            }
        }
        else
        {
            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
            i4RetVal = ISIS_FAILURE;
        }
        u1EntryCount--;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSysActualsAdd ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcSysPSModify ()
 * Description : This routine extracts the information related to the Protocol
 *               support from the received FLTR message and updates the System
 *               Context with the retrieved information
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On Successful updation of Protocol Support
 *                             information in the System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSysPSModify (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1ProtSupp = 0;
    UINT1               u1ExistState = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtPSChange   *pEvtPSChg = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSysPSModify ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Context Idx [ %u ] Not Found\n", u4InstIdx));
        pContext = (tIsisSysContext *)
            ISIS_MEM_ALLOC (ISIS_BUF_CTXT, sizeof (tIsisSysContext));
        if (pContext == NULL)
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Context During Bulk/LSU\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisFltProcLSUFailure ();
            FTP_EE ((ISIS_LGST,
                     "FLT <X> : Exiting IsisFltrProcSysPSModify ()\n"));
            return ISIS_FAILURE;
        }
        IsisCtrlInitSysContext (pContext, u4InstIdx);
        IsisCtrlAddSysContext (pContext);
    }

    ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1ProtSupp);
    u2Offset = (UINT2) (u2Offset + 1);
    ISIS_RED_GET_1_BYTE (pu1Msg, u2Offset, u1ExistState);
    u2Offset = (UINT2) (u2Offset + 1);

    if (u1ExistState == ISIS_ACTIVE)
    {
        IsisCtrlAddPSEntry (pContext, u1ProtSupp);
    }
    else if (u1ExistState == ISIS_DESTROY)
    {
        IsisCtrlDelPSEntry (pContext, u1ProtSupp);
    }

    if ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
        && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
    {
        pEvtPSChg = (tIsisEvtPSChange *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtPSChange));
        if (pEvtPSChg != NULL)
        {
            pEvtPSChg->u1Status = (u1ExistState == ISIS_ACTIVE)
                ? ISIS_PS_ADDED : ISIS_PS_DELETED;
            pEvtPSChg->u1ProtSupp = u1ProtSupp;
            IsisUpdProcPSChgEvt (pContext, pEvtPSChg);
        }
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSysPSModify ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcSysContextDelete ()
 * Description : This routine extracts the indices from the received FLTR
 *               message and deletes the corresponding Context record from the
 *               global pool of System instances
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On Successful deletion of System Context from
 *                             global pool of System Instances
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSysContextDelete (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSysContextDelete ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        IsisUpdProcISDownEvt (pContext);
        IsisCtrlDelSysContext (u4InstIdx);
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSysContextDelete ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcMemUpdate() 
 * Description : This routine decodes the received LSU packets from the
 *               Active node. It updates the appropriate database based on the
 *               type of database included in the message. 
 * Input(s)    : pu1Msg - Pointer to the received FLTR message  
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Adjacency Records 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcMemUpdate (tRmMsg * pu1Msg)
{
    UINT1               u1Command = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcAdjUpdate ()\n"));

    /* Skip the Peer node header to get the FLTR header which includes
     * information regarding the data encoded
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_MSG_LENGTH_POS, u1Command);

    /*pFLTHdr = (tIsisFLTHdr *) (pu1Msg + ISIS_MSG_LENGTH_POS); */

    switch (u1Command)
    {
        case ISIS_CMD_ADD:

            FTP_PT ((ISIS_LGST, "FLT <T> : Adding Circuit On Standby Node\n"));
            IsisFltrProcMemCfgUpdt (pu1Msg);
            break;

        default:

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Invalid Command In FLTR Msg [ %u ]\n",
                     u1Command));
            break;
    }

    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisFltrProcMemCfgUpdt()
 * Description : This routine retrieves the MemCfgs encoded in the
 *               received FLTR message and updates the values of the MemConfigs
 *               structure present in the Active node 
 * Input(s)    : pu1Msg - Pointer to the FLTR Message Received
 * Output(s)   : None
 * Globals     : gIsisMemConfigs
 * Returns     : ISIS_SUCCESS, On successful updation of System Actuals values 
 *                             in the System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcMemCfgUpdt (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2Offset = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcMemCfgUpdt ()\n"));

    /* Skip the FLTR header 
     */

    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxInsts);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4Factor);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxLSP);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxCkts);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxAreaAddr);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxMAA);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxAdjs);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxRoutes);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxIPRA);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxSA);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxIPIF);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxEvents);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxLspTxQLen);

    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, gIsisMemConfigs.u4MaxMsgBuffs);

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcMemCfgUpdt ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcIsStatus() 
 * Description : This routine decodes the received LSU packets from the
 *               Active node. It updates the status of IS
 * Input(s)    : pu1Msg - Pointer to the received FLTR message  
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Adjacency Records 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcIsStatus (tRmMsg * pu1Msg)
{
    UINT1               u1Command = 0;
    INT4                i4RetVal = ISIS_SUCCESS;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcIsStatus ()\n"));

    /* Skip the Peer node header to get the FLTR header which includes
     * information regarding the data encoded
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_MSG_LENGTH_POS, u1Command);
    /*pFLTHdr = (tIsisFLTHdr *) (pu1Msg + ISIS_MSG_LENGTH_POS); */

    switch (u1Command)
    {
        case ISIS_SHUTDOWN:

            FTP_PT ((ISIS_LGST, "CTL <T> : Shutting Down All Instances...\n"));
            IsisShutDownAll ();
            break;

        case ISIS_RESET_ALL:

            CTP_PT ((ISIS_LGST, "CTL <T> : Resetting All Instances...\n"));
            IsisResetAll ();
            break;

        default:

            FTP_PT ((ISIS_LGST,
                     "FLT <E> : Invalid Status  [ %u ]\n", u1Command));
            i4RetVal = ISIS_FAILURE;
            break;
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcIsStatus ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProcSchDecn() 
 * Description : This routine decodes the received LSU packets from the
 *               Active node. It updates the status of IS
 * Input(s)    : pu1Msg - Pointer to the received FLTR message  
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Adjacency Records 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisFltrProcSchDecn (tRmMsg * pu1Msg)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Level = 0;
    UINT1               u1Command = 0;
    UINT2               u2Offset = 0;
    UINT4               u4InstIdx = 0;
    tIsisSysContext    *pContext = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProcSchDecn ()\n"));

    /* Skip the Peer node header to get the FLTR header which includes
     * information regarding the data encoded
     */

    ISIS_RED_READ_1_BYTE (pu1Msg, ISIS_MSG_LENGTH_POS, u1Command);
    /*pFLTHdr = (tIsisFLTHdr *) (((UINT1 *)pu1Msg) + ISIS_MSG_LENGTH_POS); */
    u2Offset = ISIS_MSG_LENGTH_POS + sizeof (tIsisFLTHdr);

    u1Level = u1Command;
    ISIS_RED_GET_4_BYTE (pu1Msg, u2Offset, u4InstIdx);
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u1Level == ISIS_LEVEL1)
        {
            DEP_PT ((ISIS_LGST,
                     "DEC <T> : SPF Scheduled - Reason [ TRIGGER FROM ACTIVE NODE ]\n"));
            pContext->SysL1Info.u1SpfTrgCount =
                (UINT1) (pContext->SysActuals.u2MaxSPFSchTime /
                         pContext->SysActuals.u2MinSPFSchTime);
        }
        else
        {
            pContext->SysL2Info.u1SpfTrgCount = (UINT1)
                (pContext->SysActuals.u2MaxSPFSchTime /
                 pContext->SysActuals.u2MinSPFSchTime);
        }
        IsisDecSchedSPF (pContext, u1Level);
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProcSchDecn ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltProcLSUFailure() 
 * Description : If the Node is Active, this function disables the LSU and posts
 *               LSU Failure Event. If the Node is Standby, then the function
 *               shuts down the system. 
 *
 *               NOTE: If Redundancy Manager (FTM) Supports dynamic Bulk updates
 *               then standby node can issue Bulk update request, and get
 *               synchronize with the Active node.
 *               
 * Input(s)    : None
 * Output(s)   : None.
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisFltProcLSUFailure (VOID)
{
    FTP_EE ((ISIS_LGST, "UTL <X> : Entered IsisFltProcLSUFailure ()\n"));

    if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_STANDBY) &&
        ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE)
    {
        /* While processing the Bulk Data information in standby,
           LSU Failure is noticed when parent database 
           of the current database to be updated doesn't exist */
        /*Currently this function is dummy, needs to be updated appropriately 
           while handling LSU Failures */
        return;
    }

    return;
    FTP_EE ((ISIS_LGST, "UTL <X> : Exiting IsisFltProcLSUFailure ()\n"));
}

/*****************************************************************************/
/* Function Name      : IsisExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IsisExecuteCmdAndCalculateChkSum (VOID)
{
#ifdef RM_WANTED
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_ISIS_APP_ID;
    UINT2               u2ChkSum = 0;

    IsisUnlock ();
    if (IsisGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == ISIS_FAILURE)
    {
        FTP_PT ((ISIS_LGST, "Checksum of calculation failed for ISIS\n"));
        IsisLock ();
        return;
    }

    if (IsisRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == ISIS_FAILURE)
    {
        FTP_PT ((ISIS_LGST, "Sending checkum to RM failed\n"));
        IsisLock ();
        return;
    }
    IsisLock ();
#endif
    return;
}

#endif
