/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isgrsem.c,v 1.10 2014/12/23 11:03:47 siva Exp $
 *
 *   Description: This file contains the routines associated with
 *                graceful restart. 
 *
 ******************************************************************************/
#include "isincl.h"
#include "isextn.h"
#include "isgr.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrRestart                                              */
/*                                                                           */
/* Description  : This routine will initialize Ack/Csnp list and create      */
/*                CSNP RB Tree for validation                                */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrRestart (tGrSemInfo * pGrSemInfo)
{

    tIsisTimer         *pTimer = NULL;
    UINT1               au1SemName[OSIX_NAME_LEN + 4];

    /*Initializing the SLL Ack and CSNP list - just for optimizing the check */
    /*Level-1 */
    if (((pGrSemInfo->pContext)->SysConfigs.u1SysType == ISIS_LEVEL1) ||
        ((pGrSemInfo->pContext)->SysConfigs.u1SysType == ISIS_LEVEL12))
    {
        TMO_SLL_Init (&((pGrSemInfo->pContext)->SysL1Info.AckLst));
        TMO_SLL_Init (&((pGrSemInfo->pContext)->SysL1Info.CSNPLst));
    }
    /*Level-2 */
    if (((pGrSemInfo->pContext)->SysConfigs.u1SysType == ISIS_LEVEL2) ||
        ((pGrSemInfo->pContext)->SysConfigs.u1SysType == ISIS_LEVEL12))
    {
        TMO_SLL_Init (&((pGrSemInfo->pContext)->SysL2Info.AckLst));
        TMO_SLL_Init (&((pGrSemInfo->pContext)->SysL2Info.CSNPLst));
    }

    /*Starting T2 Timer based on the Level */
    if (((pGrSemInfo->pContext)->SysConfigs.u1SysType == ISIS_LEVEL1) ||
        ((pGrSemInfo->pContext)->SysConfigs.u1SysType == ISIS_LEVEL12))
    {
        pTimer = &((pGrSemInfo->pContext)->SysL1Info.SysRestartT2Timer);
        pTimer->pContext = (pGrSemInfo->pContext);
        IsisTmrStartTimer (pTimer,
                           ISIS_GR_L1_T2_TIMER,
                           (pGrSemInfo->pContext)->u2IsisGRT2TimerL1Interval);
	    MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
    	IsisGetSemName (pTimer->pContext, au1SemName);

        /*Creating the CSNP table for validation */
        (pGrSemInfo->pContext)->SysL1Info.pGrCSNPTbl =
            RBTreeCreateEmbeddedExtended (ISIS_OFFSET (tLspInfo, RbNode),
                                  (tRBCompareFn) RbCompareLsp, ((UINT1 *) au1SemName));
    }
    if (((pGrSemInfo->pContext)->SysConfigs.u1SysType == ISIS_LEVEL2) ||
        ((pGrSemInfo->pContext)->SysConfigs.u1SysType == ISIS_LEVEL12))
    {
        pTimer = &((pGrSemInfo->pContext)->SysL2Info.SysRestartT2Timer);
        pTimer->pContext = (pGrSemInfo->pContext);
        IsisTmrStartTimer (pTimer,
                           ISIS_GR_L2_T2_TIMER,
                           (pGrSemInfo->pContext)->u2IsisGRT2TimerL2Interval);
    	MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
	    IsisGetSemName (pTimer->pContext, au1SemName);

        /*Creating the CSNP table for validation */
        (pGrSemInfo->pContext)->SysL2Info.pGrCSNPTbl =
            RBTreeCreateEmbeddedExtended (ISIS_OFFSET (tLspInfo, RbNode),
                                  (tRBCompareFn) RbCompareLsp, ((UINT1 *) au1SemName));
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGRSeenRA                                               */
/*                                                                           */
/* Description  : This routine will adjust T3 Timer and update global GR state*/
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrSeenRA (tGrSemInfo * pGrSemInfo)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1               u1Level;
    UINT2               u2RemainingTime;
    tIsisGrCheck       *pLstNode = NULL;
    tIsisTimer         *pTimer = NULL;
    UINT4               u4T3TimerInterval = 0;
    UINT4               u4AdjFlag = ISIS_FALSE;

    pContext = pGrSemInfo->pContext;
    pCktRec = pGrSemInfo->pCktRec;
    u1Level = pGrSemInfo->u1Level;
    u2RemainingTime = pGrSemInfo->u2RemainingTime;

    /*Delete entry from Ack list */
    /*Level-1 */
    do
    {
        if ((u1Level == ISIS_LEVEL1) || (u4AdjFlag == ISIS_TRUE))
        {
            TMO_SLL_Scan (&(pContext->SysL1Info.AckLst), pLstNode,
                          tIsisGrCheck *)
            {
                if (pLstNode->u4CktId == pCktRec->u4CktIdx)
                {
                    pCktRec->u1IsisGRRestartL1State =
                        pCktRec->u1IsisGRRestartL1State | ISIS_GR_ACK_RCVD;

                    if (pCktRec->pL2CktInfo != NULL)
                    {
                        if (pCktRec->pL2CktInfo->u4NumAdjs == 0)
                        {
                            u4AdjFlag = ISIS_TRUE;
                        }
                    }
                    break;
                }
            }
            if (pLstNode != NULL)
            {
                TMO_SLL_Delete (&(pContext->SysL1Info.AckLst), &pLstNode->next);
                MemReleaseMemBlock (gIsisMemPoolId.u4AckRxPid,
                                    (UINT1 *) pLstNode);
            }
        }

        /*Level-2 */
        if ((u1Level == ISIS_LEVEL2) || (u4AdjFlag == ISIS_TRUE))
        {
            u4AdjFlag = ISIS_FALSE;

            TMO_SLL_Scan (&(pContext->SysL2Info.AckLst), pLstNode,
                          tIsisGrCheck *)
            {
                if (pLstNode->u4CktId == pCktRec->u4CktIdx)
                {
                    pCktRec->u1IsisGRRestartL2State =
                        pCktRec->u1IsisGRRestartL2State | ISIS_GR_ACK_RCVD;

                    if (pCktRec->pL1CktInfo != NULL)
                    {
                        if (pCktRec->pL1CktInfo->u4NumAdjs == 0)
                        {
                            u4AdjFlag = ISIS_TRUE;
                        }
                    }
                    break;
                }
            }
            if (pLstNode != NULL)
            {
                TMO_SLL_Delete (&(pContext->SysL2Info.AckLst), &pLstNode->next);
                MemReleaseMemBlock (gIsisMemPoolId.u4AckRxPid,
                                    (UINT1 *) pLstNode);
            }

        }
    }
    while (u4AdjFlag == ISIS_TRUE);
    /* RFC 5306 : Section 3.1 
     * T3 timer value must be restarted, with the minimum received 
     * remaining time value - Applicable for only planned restart*/
    if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER) &&
        (pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER) &&
        (pContext->u1IsisGRRestarterState == ISIS_GR_SHUTDOWN))
    {
        /*These T3 timer updation will be done in planned restart,
         * before shutdown  */
        pTimer = &(pContext->SysTimers.SysRestartT3Timer);
        pTimer->pContext = pContext;

        /*Get the Remaining time and update */
        TmrGetRemainingTime (gIsisTmrListID,
                             &(pTimer->Timer), &u4T3TimerInterval);

        if (((UINT2) u4T3TimerInterval / NO_OF_TICKS_PER_SEC) > u2RemainingTime)
        {
            /*Re-starting T3 Timer */
            IsisTmrRestartTimer (pTimer,
                                 ISIS_GR_T3_TIMER, (UINT4) u2RemainingTime);
        }
    }

    if (pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER)
    {
        if ((TMO_SLL_Count (&(pContext->SysL1Info.AckLst)) == 0) &&
            (TMO_SLL_Count (&(pContext->SysL2Info.AckLst)) == 0))
        {
            /*If its before restart, then router can shutdown 
             * when it received ack from all its neighbor*/
            if (pContext->u1IsisGRRestarterState == ISIS_GR_SHUTDOWN)
            {
                pContext->u1IsisGRShutState = ISIS_GR_SHUT;
                IsisGrCheckAndShutdown ();
            }
            else
            {
                gu1IsisGrState = ISIS_GR_ADJ_SEEN_RA;
            }
        }

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGRAdjustT3                                             */
/*                                                                           */
/* Description  : This routine will stop T1 timer and adjust T3 timer        */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrAdjustT3 (tGrSemInfo * pGrSemInfo)
{
    tIsisSysContext    *pContext = NULL;
    tIsisTimer         *pTimer = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT4               u4T3TimerInterval = 0;
    UINT2               u2RemainingTime = 0;
    pCktRec = pGrSemInfo->pCktRec;
    pContext = pGrSemInfo->pContext;
    u2RemainingTime = pGrSemInfo->u2RemainingTime;

    /*Stopping T1 Timer: */
    IsisTmrStopECTimer (pContext, ISIS_GR_ECT_T1,
                        pCktRec->u4CktIdx, pCktRec->u1IsisGRT1TmrIdx);

    /* RFC 5306 : Section 3.1 
     * T3 timer value must be restarted, with the minimum received 
     * remaining time value - Applicable for only planned restart*/
    if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER) &&
        (pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER) &&
        (pContext->u1IsisGRRestarterState == ISIS_GR_SHUTDOWN))
    {
        /*These T3 timer updation will be done in planned restart,
         * before shutdown  */
        pTimer = &(pContext->SysTimers.SysRestartT3Timer);
        pTimer->pContext = pContext;

        /*Get the Remaining time and update */
        TmrGetRemainingTime (gIsisTmrListID,
                             &(pTimer->Timer), &u4T3TimerInterval);

        if (((UINT2) u4T3TimerInterval / NO_OF_TICKS_PER_SEC) > u2RemainingTime)
        {
            /*Re-starting T3 Timer */
            IsisTmrRestartTimer (pTimer,
                                 ISIS_GR_T3_TIMER, (UINT4) u2RemainingTime);
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGRSeenCsnp                                             */
/*                                                                           */
/* Description  : This routine will update CSNP RB Tree                  */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrSeenCsnp (tGrSemInfo * pGrSemInfo)
{
    IsisUpdGRCSNPCheck (pGrSemInfo->pContext, pGrSemInfo->pCktRec,
                        pGrSemInfo->pu1CSNP, pGrSemInfo->u1Level);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GRT1Cancel                                                 */
/*                                                                           */
/* Description  : This routine will stop T1 timer and populate CSNP RB Tree  */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrT1Cancel (tGrSemInfo * pGrSemInfo)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1               u1Level = 0;
    UINT1              *pu1CSNP = NULL;
    UINT1               u1PktType = 0;

    pContext = pGrSemInfo->pContext;
    pCktRec = pGrSemInfo->pCktRec;
    u1Level = pGrSemInfo->u1Level;
    pu1CSNP = pGrSemInfo->pu1CSNP;
    u1PktType = pGrSemInfo->u1PktType;

    if (((u1Level == ISIS_LEVEL1)
         && ((pCktRec->u1IsisGRRestartL1State & ISIS_GR_ACK_RCVD) ==
             ISIS_GR_ACK_RCVD)) || ((u1Level == ISIS_LEVEL2)
                                    &&
                                    ((pCktRec->
                                      u1IsisGRRestartL2State & ISIS_GR_ACK_RCVD)
                                     == ISIS_GR_ACK_RCVD)))
    {
        /*Stopping T1 Timer: */
        IsisTmrStopECTimer (pContext, ISIS_GR_ECT_T1,
                            pCktRec->u4CktIdx, pCktRec->u1IsisGRT1TmrIdx);
    }

    if (u1PktType == ISIS_GR_CSNP_RCVD)
    {
        IsisUpdGRCSNPCheck (pContext, pCktRec, pu1CSNP, u1Level);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : GrT1Expire                                                 */
/*                                                                           */
/* Description  : This routine will restart T1 timer and send GR request hello*/
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrT1Expire (tGrSemInfo * pGrSemInfo)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1               u1Level = 0;

    pContext = pGrSemInfo->pContext;
    pCktRec = pGrSemInfo->pCktRec;
    u1Level = pGrSemInfo->u1Level;

    /*if Ack not received and retry count is less than the max T1 count,
     *then resend the packet and increment the retry count  */
    /*Level-1 */
    if ((u1Level == ISIS_LEVEL1) || (u1Level == ISIS_LEVEL12))
    {
        IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
    }
    /*Level-2 */
    if ((u1Level == ISIS_LEVEL2) || (u1Level == ISIS_LEVEL12))
    {
        IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
    }
    /*Start T1 Timer */
    IsisTmrStartECTimer (pContext, ISIS_GR_ECT_T1, pCktRec->u4CktIdx,
                         pContext->u1IsisGRT1TimerInterval,
                         &(pCktRec->u1IsisGRT1TmrIdx));
    /*Incrementing the Transmit count */
    pCktRec->u1IsisGRRestartHelloTxCount++;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrT1ExpireNth                                          */
/*                                                                           */
/* Description  : This routine will Send normal Hello                        */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrT1ExpireNth (tGrSemInfo * pGrSemInfo)
{

    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1               u1Level = 0;
    tIsisGrCheck       *pLstNode = NULL;
    tIsisGrCheck       *pCsnpNode = NULL;

    pContext = pGrSemInfo->pContext;
    pCktRec = pGrSemInfo->pCktRec;
    u1Level = pGrSemInfo->u1Level;

    if ((u1Level == ISIS_LEVEL1) || (u1Level == ISIS_LEVEL12))
    {
        /*Removing the entry from Ack list */
        TMO_SLL_Scan (&(pContext->SysL1Info.AckLst), pLstNode, tIsisGrCheck *)
        {
            if (pLstNode->u4CktId == pCktRec->u4CktIdx)
            {
                break;
            }
        }
        if (pLstNode != NULL)
        {
            TMO_SLL_Delete (&(pContext->SysL1Info.AckLst), &pLstNode->next);
            MemReleaseMemBlock (gIsisMemPoolId.u4AckRxPid, (UINT1 *) pLstNode);
        }

        if (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART)
        {
            /*Removing the entry from CSNP list */
            TMO_SLL_Scan (&(pContext->SysL1Info.CSNPLst), pCsnpNode,
                          tIsisGrCheck *)
            {
                if (pCsnpNode->u4CktId == pCktRec->u4CktIdx)
                {
                    break;
                }
            }
            if (pCsnpNode != NULL)
            {
                TMO_SLL_Delete (&(pContext->SysL1Info.CSNPLst),
                                &pCsnpNode->next);
                MemReleaseMemBlock (gIsisMemPoolId.u4CSNPRxPid,
                                    (UINT1 *) pCsnpNode);
            }
        }
/*If the circuit does not contain any adjacency then
 */
        /*Sending Normal Hello */
        IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
    }
    if ((u1Level == ISIS_LEVEL2) || (u1Level == ISIS_LEVEL12))
    {

        /*Removing the entry from Ack list */
        TMO_SLL_Scan (&(pContext->SysL2Info.AckLst), pLstNode, tIsisGrCheck *)
        {
            if (pLstNode->u4CktId == pCktRec->u4CktIdx)
            {
                break;
            }
        }
        if (pLstNode != NULL)
        {

            TMO_SLL_Delete (&(pContext->SysL2Info.AckLst), &pLstNode->next);
            MemReleaseMemBlock (gIsisMemPoolId.u4AckRxPid, (UINT1 *) pLstNode);
        }

        if (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART)
        {
            /*Removing the entry from CSNP list */
            TMO_SLL_Scan (&(pContext->SysL2Info.CSNPLst), pLstNode,
                          tIsisGrCheck *)
            {
                if (pLstNode->u4CktId == pCktRec->u4CktIdx)
                {
                    break;
                }
            }
            if (pLstNode != NULL)
            {

                TMO_SLL_Delete (&(pContext->SysL2Info.CSNPLst),
                                &pLstNode->next);
                MemReleaseMemBlock (gIsisMemPoolId.u4CSNPRxPid,
                                    (UINT1 *) pLstNode);
            }
        }

        /*Sending Normal Hello */
        IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrT2Expire                                             */
/*                                                                           */
/* Description  : This routine will do SPF for restarting router,            */
/*                clear overload and normal hello for starting router        */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrT2Expire (tGrSemInfo * pGrSemInfo)
{

    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1               u1Level = 0;

    pContext = pGrSemInfo->pContext;
    u1Level = pGrSemInfo->u1Level;

    if (pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER)
    {
        if (pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        {
            if (gu1IsisGrState == ISIS_GR_SPF_L1WAIT
                || gu1IsisGrState == ISIS_GR_SPF_L2WAIT)
            {
                /*Trigger SPF & Goto SPF_WAIT */
                gu1IsisGrState = ISIS_GR_SPF_WAIT;
                IsisDecSchedSPF (pContext, ISIS_LEVEL1);
                IsisDecSchedSPF (pContext, ISIS_LEVEL2);
            }

            if (u1Level == ISIS_LEVEL1)
            {
                gu1IsisGrState = ISIS_GR_SPF_L1WAIT;
            }
            else if (u1Level == ISIS_LEVEL2)
            {
                gu1IsisGrState = ISIS_GR_SPF_L2WAIT;
            }
        }
        else
        {
            gu1IsisGrState = ISIS_GR_SPF_WAIT;
            IsisDecSchedSPF (pContext, u1Level);
        }
    }
    else if (pContext->u1IsisGRRestartStatus == ISIS_STARTING_ROUTER)
    {
        IsisGrSelfLspGen (pContext, u1Level, CLEAR_OVERLOAD);
        gu1IsisGrState = ISIS_GR_CLEAR_OVERLOAD;
    }

    /*Scanning the circuit for transmitting the Normal Hello */
    for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
         pCktRec = pCktRec->pNext)
    {
        if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
            || (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            /*Sending Normal Hello */
            IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
        }
        if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
            || (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            /*Sending Normal Hello */
            IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrT3Expire                                             */
/*                                                                           */
/* Description  : This routine will send overload bit set LSP                */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrT3Expire (tGrSemInfo * pGrSemInfo)
{
    tIsisSysContext    *pContext = NULL;

    pContext = pGrSemInfo->pContext;

    /*Changing the status */
    pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_TIMEDOUT;

    if (pContext->u1IsisGRRestarterState == ISIS_GR_SHUTDOWN)
    {
        IsisGrStoreRestartInfo ();
        IsisShutDownAll ();
        IsisDelSemandQue ();
        IssSetModuleSystemControl (ISIS_MODULE_ID, MODULE_SHUTDOWN);
        OsixDeleteTask (SELF, (const UINT1 *) "ISIS");
        return;
    }

    gu1IsisGrState = ISIS_GR_OVERLOAD;
    /*Set overload bit */
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        IsisGrSelfLspGen (pContext, ISIS_LEVEL1, OVERLOAD);
    }
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        IsisGrSelfLspGen (pContext, ISIS_LEVEL2, OVERLOAD);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrLspDbSync                                        */
/*                                                                           */
/* Description  : This routine will stop T2 and T3 timer, schedule SPF       */
/*                ans generate Self-LSP                                      */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrLspDbSync (tGrSemInfo * pGrSemInfo)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;

    pContext = pGrSemInfo->pContext;

    /*Stopping T2 Timer in both Planned and UnPlanned Case */
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        IsisTmrStopTimer (&(pContext->SysL1Info.SysRestartT2Timer));
    }
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        IsisTmrStopTimer (&(pContext->SysL2Info.SysRestartT2Timer));
    }

    if (pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER)
    {
        /*Trigger SPF & Goto SPF_WAIT */
        gu1IsisGrState = ISIS_GR_SPF_WAIT;

        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            IsisDecSchedSPF (pContext, ISIS_LEVEL1);
        }
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            IsisDecSchedSPF (pContext, ISIS_LEVEL2);
        }
    }
    else if (pContext->u1IsisGRRestartStatus == ISIS_STARTING_ROUTER)
    {
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            IsisGrSelfLspGen (pContext, ISIS_LEVEL1, CLEAR_OVERLOAD);
        }
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            IsisGrSelfLspGen (pContext, ISIS_LEVEL2, CLEAR_OVERLOAD);
        }

        /*Scanning the circuit for transmitting the Normal Hello */
        for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
             pCktRec = pCktRec->pNext)
        {
            pCktRec->u1IsisGRRestartHelloTxCount = 0;
            pCktRec->u1IsisGRRestartL1State = 0;
            pCktRec->u1IsisGRRestartL2State = 0;
            pCktRec->u1IsisGRL1HelloSend = 0;
            pCktRec->u1IsisGRL2HelloSend = 0;

            if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {
                /*Sending Normal Hello */
                IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
            }
            if ((pCktRec->u1CktLevel == ISIS_LEVEL2)
                || (pCktRec->u1CktLevel == ISIS_LEVEL12))
            {
                /*Sending Normal Hello */
                IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
            }
        }
        IsisGrCleanUp (pContext);

        gu1IsisGrState = ISIS_GR_NORMAL_ROUTER;
        /*Exit reason updation for both planned and unplanned case */
        pContext->u1IsisGRRestarterState = ISIS_GR_NONE;
        pContext->u1IsisGRRestartStatus = ISIS_RESTART_NONE;
        pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_COMPLETED;
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrRunning                                             */
/*                                                                           */
/* Description  : This routine will send clear overload LSP                  */
/*                clean up GR process                                        */
/*                                                                           */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrRunning (tGrSemInfo * pGrSemInfo)
{

    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisTimer         *pTimer = NULL;
    UINT4               u4T3TimerInterval = 0;
    UINT4               u4RetStat = 0;

    pContext = pGrSemInfo->pContext;
    pTimer = &(pContext->SysTimers.SysRestartT3Timer);
    pTimer->pContext = pContext;

    pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_COMPLETED;
    /*Get the Remaining time */
    u4RetStat = TmrGetRemainingTime (gIsisTmrListID,
                                     &(pTimer->Timer), &u4T3TimerInterval);

    /*T3 should have expired, so overload bit could have set */
    if ((u4RetStat == TMR_FAILURE) || (u4T3TimerInterval == 0))
    {
        gu1IsisGrState = ISIS_GR_CLEAR_OVERLOAD;
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            IsisGrSelfLspGen (pContext, ISIS_LEVEL1, CLEAR_OVERLOAD);
        }
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            IsisGrSelfLspGen (pContext, ISIS_LEVEL2, CLEAR_OVERLOAD);
        }
        pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_TIMEDOUT;
    }

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        IsisGrSelfLspGen (pContext, ISIS_LEVEL1, SELF_LSP);
    }
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        IsisGrSelfLspGen (pContext, ISIS_LEVEL2, SELF_LSP);
    }

    /*Stopping T3 Timer */
    IsisTmrStopTimer (&(pContext->SysTimers.SysRestartT3Timer));

    IsisGrCleanUp (pContext);

    gu1IsisGrState = ISIS_GR_NORMAL_ROUTER;
    /*Exit reason updation for both planned and unplanned case */
    pContext->u1IsisGRRestarterState = ISIS_GR_NONE;
    pContext->u1IsisGRRestartStatus = ISIS_RESTART_NONE;

    /*Scanning the circuit for transmitting the Normal Hello */
    for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
         pCktRec = pCktRec->pNext)
    {
        pCktRec->u1IsisGRRestartHelloTxCount = 0;
        pCktRec->u1IsisGRRestartL1State = 0;
        pCktRec->u1IsisGRL1HelloSend = 0;
        pCktRec->u1IsisGRL2HelloSend = 0;

        if ((pCktRec->u1CktLevel == ISIS_LEVEL1)
            || (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            /*Sending Normal Hello */
            IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
        }
        if ((pCktRec->u1CktLevel == ISIS_LEVEL2) ||
            (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            /*Sending Normal Hello */
            IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrInvalid                                              */
/*                                                                           */
/* Description  : This procedure  is invoked when an invalid event occurs.   */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrInvalid (tGrSemInfo * pGrSemInfo)
{
    UNUSED_PARAM (pGrSemInfo);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrCleanUp                                              */
/*                                                                           */
/* Description  : This routine will  clear RTM,remove flash file and         */
/*                CSNP RB Tree                                               */
/*                                                                           */
/* Input        : pContext - pointer to context structure                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrCleanUp (tIsisSysContext * pContext)
{
    tRtmRegnId          RegnIdv4;
    UINT4               u4Index = 0;
    tRtm6RegnId         RegnIdv6;
    MEMSET (&RegnIdv6, 0, sizeof (tRtm6RegnId));
    RegnIdv6.u2ProtoId = ISIS_ID;

    MEMSET (&RegnIdv4, 0, sizeof (tRtmRegnId));
    RegnIdv4.u2ProtoId = ISIS_ID;

    /*Clearing Unwanted Routes in RTM */
    RtmProcessGRRouteCleanUp (&RegnIdv4, IP_SUCCESS);
    Rtm6ProcessGRRouteCleanUp (&RegnIdv6, IP_SUCCESS);
    /*Remove the GR content */
    IssRemoveGraceContent (ISIS_MODULE);

    /*Deallocation of Global Structure */
    if (pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER)
    {
        while (u4Index < gIsisGrinfo->u4CxtCount)
        {
            MemReleaseMemBlock (gIsisMemPoolId.u4GrCxtpid,
                                (UINT1 *) gIsisGrinfo->pCxtinfo[u4Index]);
            u4Index++;
        }
        MemReleaseMemBlock (gIsisMemPoolId.u4GrCxtInfoPid,
                            (UINT1 *) gIsisGrinfo->pCxtinfo);
        MemReleaseMemBlock (gIsisMemPoolId.u4GrInfoPid, (UINT1 *) gIsisGrinfo);

    }

    pContext->u1IsisGRRestartMode = ISIS_GR_NONE;

    /*Deleting the RB Tree */
    if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
	&& (pContext->SysL1Info.pGrCSNPTbl != NULL))
    {
        RBTreeDestroy (pContext->SysL1Info.pGrCSNPTbl, NULL, 0);
	pContext->SysL1Info.pGrCSNPTbl = NULL;
    }

    if (((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
	&& (pContext->SysL2Info.pGrCSNPTbl != NULL))
    {
        RBTreeDestroy (pContext->SysL2Info.pGrCSNPTbl, NULL, 0);
	pContext->SysL2Info.pGrCSNPTbl = NULL;
    }
    /*Removing GR Conf file */
    FlashRemoveFile (ISIS_GR_CONF);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrAdjUp                                                */
/*                                                                           */
/* Description  : This routine will  send overload bit set LSP               */
/*                                                                           */
/* Input        : pGrSemInfo -Pointer to Gr State event machine structure    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisGrAdjUp (tGrSemInfo * pGrSemInfo)
{
    tIsisSysContext    *pContext = NULL;

    pContext = pGrSemInfo->pContext;

    gu1IsisGrState = ISIS_GR_OVERLOAD;

    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        IsisGrSelfLspGen (pContext, ISIS_LEVEL1, OVERLOAD);
    }
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        IsisGrSelfLspGen (pContext, ISIS_LEVEL2, OVERLOAD);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrRunRestartSem                                        */
/*                                                                           */
/* Description  : This procedure indexes into the state event table using the*/
/*                GR event and the GR state to get an integer. This          */
/*                integer when indexed into an array of function pointers    */
/*               gives the actual action routine to be called.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsisGrRunRestartSem (UINT2 u2Event, UINT2 u2State, tGrSemInfo * pGrSemInfo)
{
    if (pGrSemInfo->pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER)
    {
        /* SEM Function is Restarting router(Forwarding plane preserved) */
        if (!((u2Event <= 0 || u2Event > MAX_GR_EVENT) ||
              (u2State <= 0 || u2State > MAX_GR_STATE)))
        {
            (*aGrFunc[au1GrTable[u2Event - 1][u2State - 1]]) (pGrSemInfo);
        }

    }
    else if (pGrSemInfo->pContext->u1IsisGRRestartStatus ==
             ISIS_STARTING_ROUTER)
    {
        /* SEM Function is Starting router(Forwarding plane not preserved) */
        if (!((u2Event <= 0 || u2Event > MAX_GR_START_EVENT) ||
              (u2State <= 0 || u2State > MAX_GR_START_STATE)))
        {
            (*aGrStartFunc[au1GrStartTable[u2Event - 1][u2State - 1]])
                (pGrSemInfo);
        }

    }

}
