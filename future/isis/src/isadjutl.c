/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isadjutl.c,v 1.17.4.1 2018/03/16 13:40:57 siva Exp $
 *
 *   Description: This file contains the Routines related
 *                to Adjacency Module. 
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE INT4        IsisAdjGetIPRAAdjs (tIsisSysContext *, UINT1, UINT1,
                                        tIsisSPTNode **, UINT1);

/******************************************************************************
 * Function Name : IsisAdjEnqueueAdjChgEvt ()
 * Description   : This routine forms the Adjacncy Change Event and
 *                 posts this event to Control Module. This is generated when 
 *                 an adjacency changes state to UP or DOWN
 * Input(s)      : pCktRec    - Pointer to Circuit Record
 *                 pCktLevel  - Pointer to Circuit Level Record  
 *                 pAdjRec    - Pointer to Adjacency Record
 *                 u1AdjState - State of Adjacency (UP or DOWN)
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjEnqueueAdjChgEvt (tIsisCktEntry * pCktRec, tIsisCktLevel * pCktLevel,
                         tIsisAdjEntry * pAdjRec, UINT1 u1AdjState)
{
    tIsisEvtAdjChange  *pAdjEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEnqueueAdjChgEvt ()\n"));

    pAdjEvt = (tIsisEvtAdjChange *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                    sizeof (tIsisEvtAdjChange));
    if (pAdjEvt != NULL)
    {
        pAdjEvt->u1EvtID = ISIS_EVT_ADJ_CHANGE;
        pAdjEvt->u1Status = u1AdjState;
        pAdjEvt->u4AdjIdx = pAdjRec->u4AdjIdx;
        pAdjEvt->u4CktIdx = pCktRec->u4CktIdx;
        pAdjEvt->u1AdjUsage = pAdjRec->u1AdjUsage;
        pAdjEvt->u1AdjType = pAdjRec->u1AdjNbrSysType;

        MEMCPY (pAdjEvt->au1AdjSysID, pAdjRec->au1AdjNbrSysID,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

        if (pCktLevel != NULL)
        {
            ISIS_SET_METRIC (pCktRec->pContext, pAdjEvt, pCktLevel);
        }

        IsisUtlSendEvent (pCktRec->pContext, (UINT1 *) pAdjEvt,
                          sizeof (tIsisEvtAdjChange));
        if ((pCktRec->u1CktType == ISIS_P2P_CKT)
            && (pCktRec->u1P2PDynHshkMachanism == ISIS_P2P_TWO_WAY))
        {
            /*This is done to immediately generate LSP */
            if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
            {
                IsisTmrRestartTimer (&
                                     (pCktRec->pContext->SysL1Info.
                                      SysLSPGenTimer), ISIS_L1LSP_GEN_TIMER, 1);
            }

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
            {
                IsisTmrRestartTimer (&
                                     (pCktRec->pContext->SysL2Info.
                                      SysLSPGenTimer), ISIS_L2LSP_GEN_TIMER, 1);
            }
        }
    }
    else
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <E> : Memory Allocation For Adjacency Event Failed\n"));
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEnqueueAdjChgEvt ()\n"));
}

/*******************************************************************************
 * Function Name : IsisAdjEnqueueWrongSysEvt ()
 * Description   : This routine forms the Wrong System Event and
 *                 posts this event to Control Module. This is generated when 
 *                 a PDU is received from an incompatible system
 * Input(s)      : pContext - Pointer to System Context
 *                 pu1PDU   - Pointer to the received PDU 
 * Output(s)     : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjEnqueueWrongSysEvt (tIsisSysContext * pContext, UINT1 *pu1PDU)
{
    UINT1              *pSysId = NULL;
    tIsisEvtWrongSys   *pWrongSysEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEnqueueWrongSysEvt ()\n"));

    ISIS_INCR_WRONG_SYS_STAT (pContext);

    pSysId = ISIS_EXTRACT_HELLO_SYS_ID (pu1PDU);
    pWrongSysEvt = (tIsisEvtWrongSys *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtWrongSys));

    /* Filling the associated variables in the structure 
     * tIsisEvtWrongSys
     */

    if (pWrongSysEvt != NULL)
    {
        pWrongSysEvt->u1EvtID = ISIS_EVT_WRONG_SYS_TYPE;
        pWrongSysEvt->u1SysType = ISIS_EXTRACT_HELLO_CKT_TYPE (pu1PDU);
        MEMCPY (pWrongSysEvt->au1AdjSysID, pSysId, ISIS_SYS_ID_LEN);

        IsisUtlSendEvent (pContext, (UINT1 *) pWrongSysEvt,
                          sizeof (tIsisEvtWrongSys));
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Wrong System Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEnqueueWrongSysEvt ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjRemAdj ()
 * Description   : This routine will remove the adjacency from the Adjacency 
 *                 database and does not free the associated memory. This is 
 *                 to enable the calling routine to use the same memory for 
 *                 any further processing. In short this routine just
 *                 de-links the node from the database
 * Input(s)      : pCktRec - Pointer to Circuit Record
 *                 pAdjRec - Pointer to the adjacency record to be removed 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjRemAdj (tIsisCktEntry * pCktRec, tIsisAdjEntry * pAdjRec)
{
    tIsisSysContext    *pContext = NULL;
    tIsisAdjEntry      *pTravAdj = NULL;
    tIsisAdjEntry      *pPrevAdj = NULL;
    tIsisCktLevel      *pCktLvl = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];
    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjRemAdj ()\n"));

    /* NOTE : The adjacency record to be removed must be present in the
     * database, if not this routine would not have been invoked. Hence the
     * routine does not cover the case of non-existig adjacency.
     */

    pContext = pCktRec->pContext;
    pCktLvl = (pCktRec->u1CktType == ISIS_P2P_CKT)
        ? (ISIS_GET_P2P_CKT_LEVEL (pCktRec))
        : ((pAdjRec->u1AdjUsage == ISIS_LEVEL1) ? (pCktRec->pL1CktInfo)
           : (pCktRec->pL2CktInfo));
    pTravAdj = pCktRec->pAdjEntry;

    while (pTravAdj != NULL)
    {
        if (pTravAdj->u4AdjIdx == pAdjRec->u4AdjIdx)
        {
            if (pPrevAdj == NULL)
            {
                /* De-Linking the first record in sequence 
                 */

                pCktRec->pAdjEntry = pAdjRec->pNext;
            }
            else
            {
                /* Not the first record
                 */

                pPrevAdj->pNext = pTravAdj->pNext;
            }

            pTravAdj->pNext = NULL;

            /* All Adjacency Area Addresses being deleted
             */

            IsisAdjDelAdjAreaAddrs (pTravAdj);

            ISIS_FLTR_ADJ_LSU (pContext, ISIS_CMD_DELETE, pTravAdj);

            pCktLvl->u4NumAdjs--;

            if (pCktLvl->u4NumAdjs == 0)
            {
                if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
                {
                    IsisUtlReSetBPat (&(pContext->CktTable.pu1L1CktMask),
                                      pCktRec->u4CktIdx);
                }
                if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
                {
                    IsisUtlReSetBPat (&(pContext->CktTable.pu1L2CktMask),
                                      pCktRec->u4CktIdx);
                }
            }

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL2)
            {
                (pContext->CktTable.u4NumL1Adjs)--;
            }

            if (pAdjRec->u1AdjUsage != ISIS_LEVEL1)
            {
                (pContext->CktTable.u4NumL2Adjs)--;
            }

            ISIS_FORM_STR (ISIS_SYS_ID_LEN, pAdjRec->au1AdjNbrSysID,
                           acNbrSysId);
            ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);

            ADP_PI ((ISIS_LGST,
                     "ADJ <I> : Removed Adjacency - Circuit Index [ %u ], Level [ %u ], Neighbour ID [ %s ],"
                     " IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                     pCktRec->u4CktIdx, pAdjRec->u1AdjUsage, acNbrSysId,
                     au1IPv4Addr,
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pCktRec->au1IPV6Addr))));

            break;
        }
        pPrevAdj = pTravAdj;
        pTravAdj = pTravAdj->pNext;
    }
    /* Reset cache-pointer for isisAdjTable */
    pCktRec->pLastAdjEntry = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjRemAdj ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjEnqueueDupSysEvt ()
 * Description   : This routine forms the Duplicate System Event and
 *                 posts this event to Control Module. This is generated when 
 *                 two systems with same SNPA addresses are detected 
 * Input(s)      : pAdjEntry - Pointer to Adjacency Entry 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjEnqueueDupSysEvt (tIsisAdjEntry * pAdjEntry)
{
    tIsisEvtDupSys     *pDupSysEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEnqueueDupSysEvt ()\n"));

    pDupSysEvt = (tIsisEvtDupSys *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                    sizeof (tIsisEvtDupSys));
    if (pDupSysEvt != NULL)
    {
        pDupSysEvt->u1EvtID = ISIS_EVT_DUP_SYS_DET;
        pDupSysEvt->u1Cause = ISIS_DUP_SNPA;

        MEMCPY (pDupSysEvt->au1SysID, pAdjEntry->au1AdjNbrSysID,
                ISIS_SYS_ID_LEN);
        MEMCPY (pDupSysEvt->au1SNPA, pAdjEntry->au1AdjNbrSNPA,
                ISIS_SNPA_ADDR_LEN);

        IsisUtlSendEvent (pAdjEntry->pCktRec->pContext, (UINT1 *) pDupSysEvt,
                          sizeof (tIsisEvtDupSys));
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Duplicate System Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEnqueueDupSysEvt ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjEnqueueDISChgEvt ()
 * Description   : This routine posts a DIS Changed event to the control module
 *                 whenever the local system detects a change in the DIS for a
 *                 given LAN.
 * Input(s)      : pCktEntry  - Pointer to Circuit Entry
 *                 u1CktLevel - Level of the Circuit
 *                 pu1PrevDIS - Pointer to Previous DIS ID
 *                 pu1CurrDIS - Pointer to Current DIS ID
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjEnqueueDISChgEvt (tIsisCktEntry * pCktEntry, UINT1 u1CktLevel,
                         UINT1 *pu1PrevDIS, UINT1 *pu1CurrDIS)
{
    tIsisEvtDISChg     *pDISEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEnqueueDISChgEvt ()\n"));

    /* Form LAN DIS Changed Event and Enqueue to Control Queue
     */

    pDISEvt = (tIsisEvtDISChg *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                 sizeof (tIsisEvtDISChg));
    if (pDISEvt != NULL)
    {
        pDISEvt->u1EvtID = ISIS_EVT_DIS_CHANGE;
        pDISEvt->u1CktLevel = u1CktLevel;
        pDISEvt->u4CktIdx = pCktEntry->u4CktIdx;

        MEMCPY (pDISEvt->au1PrevDIS, pu1PrevDIS,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

        MEMCPY (pDISEvt->au1CurrDIS, pu1CurrDIS,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

        IsisUtlSendEvent (pCktEntry->pContext, (UINT1 *) pDISEvt,
                          sizeof (tIsisEvtDISChg));
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : DIS Change Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEnqueueDISChgEvt ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjEnqueueDISStatChgEvt ()
 * Description   : This routine posts a DIS Status changed event to the control
 *                 module. This routine is invoked whenever thelocal IS is
 *                 either Elected DIS for a given LAN or it resigns as a DIS
 *                 for a given LAN.
 * Input(s)      : pCktEntry - Pointer to Circuit Entry
 *                 u1Level   - Level of the Circuit (L1 or L12)
 *                 u1Status  - DIS Status of the Local IS
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjEnqueueDISStatChgEvt (tIsisCktEntry * pCktEntry,
                             UINT1 u1Level, UINT1 u1Status)
{
    tIsisEvtDISStatChg *pDISStatEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEnqueueDISStatChgEvt ()\n"));

    pDISStatEvt = (tIsisEvtDISStatChg *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                         sizeof
                                                         (tIsisEvtDISStatChg));
    if (pDISStatEvt != NULL)
    {
        pDISStatEvt->u1EvtID = ISIS_EVT_CHG_IN_DIS_STAT;
        pDISStatEvt->u1Level = u1Level;
        pDISStatEvt->u1Status = u1Status;
        pDISStatEvt->u4CktIdx = pCktEntry->u4CktIdx;

        IsisUtlSendEvent (pCktEntry->pContext, (UINT1 *) pDISStatEvt,
                          sizeof (tIsisEvtDISStatChg));
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : DIS Status Change Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEnqueueDISStatChgEvt ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjEnqueueDISNotElectEvt ()
 * Description   : This routine forms the DIS Not Elected Event and posts this 
 *                 event to Control Module. This routine is onvoked when DIS
 *                 election fails
 * Input(s)      : pCktEntry - Pointer to Circuit Entry
 *                 pAdjEntry - Pointer to Adjacency Entry 
 *                 u1Reason  - Reason for Not Electing DIS 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjEnqueueDISNotElectEvt (tIsisCktEntry * pCktEntry,
                              tIsisAdjEntry * pAdjEntry, UINT1 u1Reason)
{
    tIsisEvtNoDISElect *pDISEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEnqueueDISNotElectEvt ()\n"));

    pDISEvt = (tIsisEvtNoDISElect *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                     sizeof
                                                     (tIsisEvtNoDISElect));
    if (pDISEvt != NULL)
    {
        pDISEvt->u1EvtID = ISIS_EVT_DIS_NOT_ELECTED;
        pDISEvt->u1Reason = u1Reason;
        pDISEvt->u4CktIdx = pCktEntry->u4CktIdx;

        MEMCPY (pDISEvt->au1SNPA, pAdjEntry->au1AdjNbrSNPA, ISIS_SNPA_ADDR_LEN);

        IsisUtlSendEvent (pCktEntry->pContext, (UINT1 *) pDISEvt,
                          sizeof (tIsisEvtNoDISElect));
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : DIS Not Elected Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEnqueueDISNotElectEvt ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjEnqueueDISElectEvt ()
 * Description   : This routine forms the DIS Elect Event and posts this
 *                 event to Control Module. This routine is invoked when any
 *                 adjacency is deleted
 * Input(s)      : pCktEntry - Pointer to Circuit Entry
 *                 pAdjEntry - Pointer to Adjacency Entry
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
******************************************************************************/

PUBLIC VOID
IsisAdjEnqueueDISElectEvt (tIsisCktEntry * pCktRec, tIsisAdjEntry * pAdjRec)
{
    tIsisEvtDISElect   *pDISEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEnqueueDISElectEvt ()\n"));

    pDISEvt = (tIsisEvtDISElect *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                   sizeof (tIsisEvtDISElect));

    if (pDISEvt != NULL)
    {
        pDISEvt->u1EvtID = ISIS_EVT_ELECT_DIS;
        pDISEvt->u4CktIdx = pCktRec->u4CktIdx;
        pDISEvt->u1CktLevel = pAdjRec->u1AdjUsage;
        IsisUtlSendEvent (pCktRec->pContext, (UINT1 *) pDISEvt,
                          sizeof (tIsisEvtDISElect));
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : DIS Elect Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEnqueueDISElectEvt ()\n"));
}

/*******************************************************************************
 * Function    : IsisAdjGetCktRec ()
 * Description : This routines fetches the Circuit Record for the given Index
 * Input(s)    : pContext  - Pointer to System Context
 *               u4CktIdx  - Index of the circuit to be fetched
 * Output(s)   : pCktRec   - Pointer to Circuit Record matching the index  
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if a matching Circuit Record is found
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjGetCktRec (tIsisSysContext * pContext, UINT4 u4CktIdx,
                  tIsisCktEntry ** pCktRec)
{
    tIsisCktEntry      *pTravCkt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetCktRec ()\n"));

    pTravCkt = pContext->CktTable.pCktRec;
    while (pTravCkt != NULL)
    {
        if (pTravCkt->u4CktIdx == u4CktIdx)
        {
            *pCktRec = pTravCkt;
            ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetCktRec ()\n"));
            return ISIS_SUCCESS;
        }
        pTravCkt = pTravCkt->pNext;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetCktRec ()\n"));
    return ISIS_FAILURE;
}

/******************************************************************************
 * Function Name : IsisAdjDelCktAdjs ()
 * Description   : This routine deletes all the ajacencies pertaining to a
 *                 circuit
 * Input(s)      : pContext  - Pointer to System Context
 *                 pCktRec   - Pointer to Circuit Record
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjDelCktAdjs (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                   UINT1 u1Level)
{
    UINT4               u4DirIdx = 0;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisAdjEntry      *pNextAdj = NULL;
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pNextRbNode = NULL;
    tIsisSPTNode       *pNextSptNode = NULL;
    tIsisSPTNode       *pTempSptNode = NULL;
    tIsisSPTNode       *pPrevSptNode = NULL;
    UINT4               u4RetValue = RB_FAILURE;
    tRBTree             RBTree;
    UINT1               u1TmrIdx = 0;
    UINT1               u1MtIndex = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDelCktAdjs ()\n"));

    pAdjRec = pCktRec->pAdjEntry;
    if (pAdjRec == NULL)
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : No Active Adjacency On Circuit - Index [ %u ]\n",
                 pCktRec->u4CktIdx));
    }
    else
    {
        ISIS_FORM_IPV4_ADDR (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1IPv4Addr,
                             ISIS_MAX_IPV4_ADDR_LEN);
        RTP_PT ((ISIS_LGST,
                 "RTM <T> : Deleting Routes - Circuit Index [%u], "
                 "Level [%s] - Learnt via IPv4 [%s] / IPv6 [%s]\n",
                 pCktRec->u4CktIdx, ISIS_GET_SYS_TYPE_STR (pCktRec->u1CktLevel),
                 au1IPv4Addr,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *)
                               &(pAdjRec->AdjNbrIpV6Addr.au1IpAddr))));
    }
    while (pAdjRec != NULL)
    {
        /* 'u1TmrIdx' is used for stopping Holding Timer */

        u1TmrIdx = pAdjRec->u1TmrIdx;
        pNextAdj = pAdjRec->pNext;
        if ((u1Level == ISIS_LEVEL12) || (pAdjRec->u1AdjUsage == u1Level))
        {
            IsisAdjDelAdj (pContext, pCktRec, pAdjRec, ISIS_FALSE, &u4DirIdx);
            IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING, u4DirIdx, u1TmrIdx);

            for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
            {
                RBTree = (pAdjRec->u1AdjUsage == ISIS_LEVEL1) ?
                    (pContext->SysL1Info.PrevShortPath[u1MtIndex]) :
                    (pContext->SysL2Info.PrevShortPath[u1MtIndex]);

                pTrav = RBTreeGetFirst (RBTree);

                while (pTrav != NULL)
                {
                    pNextSptNode = pTrav->pNext;
                    pNextRbNode = RBTreeGetNext (RBTree, pTrav, NULL);

                    if (pTrav->au4DirIdx[0] == u4DirIdx)
                    {
                        IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_DELETE,
                                              pTrav, 0, pAdjRec->u1AdjUsage,
                                              u1MtIndex);
                        RBTreeRemove (RBTree, pTrav);
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTrav);
                        if (pNextSptNode != NULL)
                        {
                            /* In Case of ECMP If the first SPT Node direction Index is same,
                             * After Removing the first node, Made the next node as the head of the RBTree*/
                            u4RetValue = RBTreeAdd (RBTree, pNextSptNode);
                        }
                    }
                    else
                    {
                        pPrevSptNode = pTrav;
                        while (pNextSptNode != NULL)
                        {
                            pTempSptNode = pNextSptNode->pNext;
                            if (pNextSptNode->au4DirIdx[0] == u4DirIdx)
                            {
                                IsisDecFormRouteInfo (pContext,
                                                      ISIS_SPT_ROUTE_DELETE,
                                                      pNextSptNode, 0,
                                                      pAdjRec->u1AdjUsage,
                                                      u1MtIndex);
                                ISIS_MEM_FREE (ISIS_BUF_SPTN,
                                               (UINT1 *) pNextSptNode);
                                pPrevSptNode->pNext = pTempSptNode;
                                break;
                            }
                            pPrevSptNode = pNextSptNode;
                            pNextSptNode = pTempSptNode;
                        }
                    }
                    pTrav = pNextRbNode;
                }
                if (pContext->u1IsisMTSupport == ISIS_FALSE)
                {
                    break;
                }
            }
        }
        pAdjRec = pNextAdj;
    }
    if (pCktRec->pL1CktInfo != NULL)
    {
        pCktRec->pL1CktInfo->bIsDIS = ISIS_FALSE;
    }
    if (pCktRec->pL2CktInfo != NULL)
    {
        pCktRec->pL2CktInfo->bIsDIS = ISIS_FALSE;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDelCktAdjs ()\n"));
    UNUSED_PARAM (u4RetValue);
}

/******************************************************************************
 * Function Name : IsisAdjDelCkt ()
 * Description   : This routine deletes the specified circuit. It also deletes
 *                 all the resources pertaining to the circuit viz. IPRA
 *                 information, Adjacencies and Circuit Level information. 
 * Input(s)      : pContext  - Pointer to System Context
 *                 u4CktIdx  - Circuit Index of the circuit to be deleted
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if circuit successfully Deleted
 *                 ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjDelCkt (tIsisSysContext * pContext, UINT4 u4CktIdx)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktEntry      *pDelCkt = NULL;    /* Points to the deleted circuit */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDelCkt ()\n"));

    pCktRec = pContext->CktTable.pCktRec;

    if (pCktRec == NULL)
    {
        /* No circuit records are available in the Context
         */

        ADP_PT ((ISIS_LGST, "ADJ <T> : No Active Circuits - Context [ %u ]\n",
                 pContext->u4SysInstIdx));
        return (i4RetVal);
    }

    while (pCktRec != NULL)
    {
        if (pCktRec->u4CktIdx == u4CktIdx)
        {
            /* Matching entry which can be deleted is found
             */

            if (pDelCkt == NULL)
            {
                /* This is the very first entry in the chain of circuits. Hence
                 * advance the head
                 */

                pContext->CktTable.pCktRec = pCktRec->pNext;
                pDelCkt = pCktRec;
            }
            else
            {
                pDelCkt->pNext = pCktRec->pNext;
                pDelCkt = pCktRec;
            }

            pDelCkt->pNext = NULL;
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        pDelCkt = pCktRec;
        pCktRec = pCktRec->pNext;
    }

    if (i4RetVal == ISIS_SUCCESS)
    {

        /* Delete all the adjacencies pertaining to the circuit
         */

        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Deleting Circuit Record and All Adjacencies Associated with Circuit [ %u ]\n",
                 u4CktIdx));
        IsisAdjDelCktAdjs (pContext, pDelCkt, ISIS_LEVEL12);

        /* A Matching circuit which has to be deleted is found. De-allocate
         * all the Circuit Level records associated with the circuit
         */

        if (pDelCkt->u1CktLevel != ISIS_LEVEL2)
        {
            IsisAdjDelCktLvlRec (pContext, pDelCkt, ISIS_LEVEL1);
        }
        if (pDelCkt->u1CktLevel != ISIS_LEVEL1)
        {
            IsisAdjDelCktLvlRec (pContext, pDelCkt, ISIS_LEVEL2);
        }

        (pContext->CktTable.u4NumEntries)--;
        if (pDelCkt->u1OperState == ISIS_UP)
        {
            pContext->CktTable.u4NumActCkts--;
        }
        ISIS_MEM_FREE (ISIS_BUF_CKTS, (UINT1 *) pDelCkt);
        IsisAdjGetNextCktIdx (pContext, &(pContext->CktTable.u4NextCktIndex));
    }
    else
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : No Matching Circuit Records Found - Index [ %u ]\n",
                 u4CktIdx));
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDelCkt ()\n"));
    return (ISIS_SUCCESS);
}

/*******************************************************************************
 * Function      : IsisAdjGetSelfAdjs ()
 * Description   : This routines fetches all the adjacencies matching the 
 *                 given Level and Metric. It constructs a list of all self IS
 *                 adjacencies and IPRA information and returns the list in
 *                 'pAdjList'
 * Input(s)      : pContext     - Pointer to System Context
 *                 u1Level      - Level of the circuit (Level1 or Level2)
 *                 u1MetIdx     - Index for Metric Type (default, error, 
 *                                delay or expense)
 * Output(s)     : pAdjList     - Pointer to the list of matching adjacencies 
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, If Adjacency list is fetched
 *                 ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjGetSelfAdjs (tIsisSysContext * pContext, UINT1 u1Level, UINT1 u1MetIdx,
                    tIsisSPTNode ** pAdjList, UINT1 u1MtIndex)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2Metric = 0;
    UINT4               u4Metric = 0;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisSPTNode       *pNode = NULL;
    UINT1               u1AdjMtId = 0;
    UINT1               u1IfMtId = 0;
    UINT1               u1LSPFlags = 0;
    UINT1               u1Loc = 0;
    UINT1               u1TlvLen = 0;
    UINT2               u2Offset = 0;
    tIsisLSPEntry      *pLSPEntry = NULL;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisLSPTxEntry    *pLSPTxEntry = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisHashBucket    *pDbHashBkt = NULL;
    tIsisHashBucket    *pTxHashBkt = NULL;
    UINT1              *pu1LSPBuf = NULL;
    UINT1               u1DbFlag = ISIS_LSP_NOT_EQUAL;
    UINT1               u1TxFlag = ISIS_LSP_NOT_EQUAL;
    UINT2               u2MtId = 0;
    UINT1               au1NullIP[ISIS_MAX_IP_ADDR_LEN];
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetSelfAdjs ()\n"));

    if (u1MetIdx >= ISIS_NUM_METRICS)
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Invalid Metric Type Input to Get Adjacency Information\n"));
        return ISIS_FAILURE;
    }

    pDirEntry = pContext->AdjDirTable.pDirEntry;

    /* Travel through all the adjacencies via the Direction Entries and update
     * the 'pAdjList' with the matching ones as required
     */
    DEP_PT ((ISIS_LGST,
             "DEC <T> : IS Adjacencies considered for MT [%d] SPF: \n",
             u1MtIndex));
    while ((pDirEntry != NULL) && (pDirEntry->pAdjEntry != NULL))
    {
        pAdjEntry = pDirEntry->pAdjEntry;

        u1IfMtId = pAdjEntry->pCktRec->u1IfMTId;
        u1AdjMtId = pAdjEntry->u1AdjMTId;

        /* If MT is supported, check for the MtIndex
         * MtIndex = 0  =====> AdjMTId & IfMTId should be ISIS_MT0_BITMAP,
         * MtIndex = 2  =====> AdjMTId & IfMTId should be ISIS_MT2_BITMAP
         * Thus, blocking adjacencies which do not belong to this MT
         * In case of single topology, AdjMTId need not be
         * considered */

        if ((pContext->u1IsisMTSupport == ISIS_FALSE) ||
            ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
             (((u1MtIndex == ISIS_MT0_INDEX) &&
               (u1IfMtId & ISIS_MT0_BITMAP) &&
               (u1AdjMtId & ISIS_MT0_BITMAP) &&
               (MEMCMP
                (pAdjEntry->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
                 ISIS_MAX_IPV4_ADDR_LEN) != ISIS_ZERO))
              || ((u1MtIndex == ISIS_MT2_INDEX) && (u1IfMtId & ISIS_MT2_BITMAP)
                  && (u1AdjMtId & ISIS_MT2_BITMAP)))))
        {
            ISIS_FORM_IPV4_ADDR (pAdjEntry->pCktRec->au1IPV4Addr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);
            DEP_PT ((ISIS_LGST,
                     "DEC <E> : Parsing via circuit :: ID [%d] IFIndex [%d] IPv4 [%s]"
                     " IPv6LL [%s]...\n", pAdjEntry->pCktRec->u4CktIdx,
                     pAdjEntry->pCktRec->u4CktIfIdx, au1IPv4Addr,
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pAdjEntry->pCktRec->au1IPV6Addr))));

            /*Blocking suppressed adjacencies from the SPF */
            if (pAdjEntry->u1IsisAdjGRState != ISIS_ADJ_SUPPRESSED)
            {
                if ((pAdjEntry->u1AdjUsage == u1Level)
                    || (pAdjEntry->u1AdjUsage == ISIS_L12ADJ))
                {
                    if (u1Level == ISIS_LEVEL1)
                    {
                        pCktLevel = pAdjEntry->pCktRec->pL1CktInfo;
                    }
                    else if (u1Level == ISIS_LEVEL2)
                    {
                        pCktLevel = pAdjEntry->pCktRec->pL2CktInfo;
                    }
                    else
                    {
                        DEP_PT ((ISIS_LGST,
                                 "DEC <T> : Invalid Level to Get Adjacency Information [%d]\n",
                                 u1Level));
                        return ISIS_FAILURE;    /*klocwork */
                    }

                    /* Metric Index 'u1MetIdx' refers to the appropriate metric 
                     * depending upon the number of metrics configured. For e.g., 
                     * consider the following:
                     *
                     * Num Metrics = 4 (ALL)
                     *
                     * u1MetIdx = 0, Metric Type = DEFAULT
                     * u1MetIdx = 1, Metric Type = DELAY
                     * u1MetIdx = 2, Metric Type = EXPENSE
                     * u1MetIdx = 3, Metric Type = ERROR
                     *
                     * Num Metrics = 3 (Only Def, Del, and Err)
                     *
                     * u1MetIdx = 0, Metric Type = DEFAULT
                     * u1MetIdx = 1, Metric Type = DELAY
                     * u1MetIdx = 2, Metric Type = ERROR
                     * 
                     * Num Metrics = 3 (Only Def, Del, and Exp)
                     *
                     * u1MetIdx = 0, Metric Type = DEFAULT
                     * u1MetIdx = 1, Metric Type = DELAY
                     * u1MetIdx = 2, Metric Type = EXPENSE
                     *
                     * Num Metrics = 3 (Only Def, Err, and Exp)
                     *
                     * u1MetIdx = 0, Metric Type = DEFAULT
                     * u1MetIdx = 1, Metric Type = EXPENSE
                     * u1MetIdx = 2, Metric Type = ERROR
                     * 
                     * Similarly for Num Metrics = 2, the combinations will 
                     * be (Def, Del), (Def, Exp) or (Def, Err)
                     *
                     * Note: Default is always supported.
                     */

                    /* Retrieving the Metric Value based on Metric Index
                     */

                    if (pCktLevel->Metric[u1MetIdx] & ISIS_METRIC_TYPE_MASK)
                    {
                        u2Metric &= ~ISIS_METRIC_TYPE_MASK;
                        u2Metric |= ISIS_SPT_EXT_MET_FLAG;
                    }

                    u4Metric = ISIS_GET_METRIC (pContext, pCktLevel, u1MetIdx);
                    /*   ISIS_COPY_METRIC (pContext, u4Metric, pCktLevel); */

                    /* Alloc memory for nodes to hold the Self-Adjacency information
                     * which will be returned to the calling function through 
                     * the SPT nodes
                     */

                    pNode = (tIsisSPTNode *) ISIS_MEM_ALLOC (ISIS_BUF_SPTN,
                                                             sizeof
                                                             (tIsisSPTNode));
                    if (pNode == NULL)
                    {
                        PANIC ((ISIS_LGST,
                                ISIS_MEM_ALLOC_FAIL " : SPT Node\n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        /* Post a Resource Failure Event
                         */

                        i4RetVal = ISIS_FAILURE;
                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_SPTN);
                        DEP_EE ((ISIS_LGST,
                                 "ADJ <X> : Exiting IsisAdjGetSelfAdjs ()\n"));
                        return (i4RetVal);
                    }
                    else
                    {
                        /* Metric Value takes 10 bits. The remaining bits are 
                         * used for passing additional information about the 
                         * adjacencies to the decision module. Refer to SPT node 
                         * type definition for more details about the additional 
                         * bits usage. Here we are setting the System Type Mask
                         */

                        pNode->pNext = NULL;

                        MEMCPY (pNode->au1DestID, pAdjEntry->au1AdjNbrSysID,
                                ISIS_SYS_ID_LEN);

                        /* Updating additional bits to indicate that the adjacency 
                         * is an IS adjacency and not an IP address
                         */
                        /*jdhp - fetch the LSP to update the u2Metric with att flag */
                        pLSPEntry =
                            IsisUpdGetLSPFromDB (pContext, pNode->au1DestID,
                                                 u1Level, &pPrevDbRec,
                                                 &pDbHashBkt, &u1Loc,
                                                 &u1DbFlag);
                        pLSPTxEntry =
                            IsisUpdGetLSPFromTxQ (pContext, pNode->au1DestID,
                                                  u1Level, &pPrevTxRec,
                                                  &pTxHashBkt, &u1Loc,
                                                  &u1TxFlag);
                        if (u1DbFlag == 0)
                        {
                            pu1LSPBuf = pLSPEntry->pu1LSP;
                        }
                        else if (u1TxFlag == 0)
                        {
                            pu1LSPBuf = pLSPTxEntry->pLSPRec->pu1LSP;
                        }
                        if (pu1LSPBuf != NULL)
                        {
                            if ((pContext->u1IsisMTSupport == TRUE)
                                && (u1MtIndex != 0))
                            {
                                /* For MT-TLVs Overload bit and Attach bit will be set in the first 2 bits of MT TLV. */
                                if (IsisUtlGetNextTlvOffset
                                    (pu1LSPBuf, ISIS_MT_TLV, &u2Offset,
                                     &u1TlvLen) != ISIS_FAILURE)
                                {
                                    while (u1TlvLen)
                                    {
                                        MEMCPY (&u2MtId, (pu1LSPBuf + u2Offset),
                                                ISIS_MT_ID_LEN);
                                        u2MtId = OSIX_HTONS (u2MtId);
                                        u2MtId = (u2MtId & ISIS_MTID_MASK);

                                        if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                                        {
                                            ISIS_GET_1_BYTE (pu1LSPBuf,
                                                             u2Offset,
                                                             u1LSPFlags);
                                            u1LSPFlags =
                                                (u1LSPFlags &
                                                 ISIS_MT_LSP_FLAG_MASK);
                                            break;
                                        }
                                        u1TlvLen -= ISIS_MT_ID_LEN;
                                        u2Offset += ISIS_MT_ID_LEN;
                                    }
                                }
                                /* The given system is announcing that it is attached via
                                 * Default Metric. Hence we can set the Attached bit for the
                                 * metric in the SPT node information bits
                                 */

                                if (u1LSPFlags & ISIS_MT_LSP_ATT_MASK)
                                {
                                    u2Metric |= ISIS_SPT_ATT_FLAG;
                                }

                            }
                            else
                            {
                                ISIS_GET_1_BYTE (pu1LSPBuf,
                                                 (UINT2) (ISIS_OFFSET_CHKSUM +
                                                          2), u1LSPFlags);
                                if (u1LSPFlags & ISIS_FLG_ATT_DEF)
                                {
                                    u2Metric |= ISIS_SPT_ATT_FLAG;
                                }
                            }
                            ISIS_GET_1_BYTE (pu1LSPBuf,
                                             (ISIS_OFFSET_CHKSUM + 2),
                                             u1LSPFlags);
                            u1LSPFlags = u1LSPFlags & ISIS_ISTYPE_MASK;
                            u2Metric |= ISIS_GET_SYS_TYPE_MASK (u1LSPFlags);
                        }
                        else
                        {
                            /*LSP Not present. So don't update the node */
                            pDirEntry = pDirEntry->pNext;
                            u2Offset = 0;
                            pu1LSPBuf = NULL;
                            u1LSPFlags = 0;
                            u1DbFlag = ISIS_LSP_NOT_EQUAL;
                            u1TxFlag = ISIS_LSP_NOT_EQUAL;
                            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pNode);
                            continue;
                        }

                        pNode->au2Metric[u1MetIdx] |= (UINT2)
                            (u2Metric | ISIS_SPT_IS_FLAG);
                        pNode->au4DirIdx[u1MetIdx] = pDirEntry->u4DirIdx;
                        pNode->au1MaxPaths[u1MetIdx] = 0;
                        ISIS_DEC_PRINT_ID (pNode->au1DestID,
                                           (UINT1) ISIS_SYS_ID_LEN, "\t\t\t",
                                           ISIS_OCTET_STRING);
                        if (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPV4)
                        {
                            pNode->u1AddrType = ISIS_ADDR_IPV4;
                        }
                        else if (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPV6)
                        {
                            pNode->u1AddrType = ISIS_ADDR_IPV6;
                        }
                        pNode->u4MetricVal = u4Metric;

                        ISIS_DBG_PRINT_ID (pNode->au1DestID,
                                           (UINT1) ISIS_SYS_ID_LEN,
                                           "ADJ <T> : Copied Destination ID\t",
                                           ISIS_OCTET_STRING);

                        /* Inserting the node at the head */

                        pNode->pNext = (*pAdjList);
                        *pAdjList = pNode;
                    }
                }
            }
        }
        pDirEntry = pDirEntry->pNext;
        u2Offset = 0;
        pu1LSPBuf = NULL;
        u1LSPFlags = 0;
        u1DbFlag = ISIS_LSP_NOT_EQUAL;
        u1TxFlag = ISIS_LSP_NOT_EQUAL;
    }
    if ((*pAdjList) == NULL)
    {
        DEP_PT ((ISIS_LGST, "DEC <E> : No Neighbor Found\n"));
    }
    /* Add all the IPRA entries to the List */
    DEP_PT ((ISIS_LGST, "ADJ <T> : IS Adjacencies considered for MT [%d] SPF: ",
             u1MtIndex));
    pNode = (*pAdjList);
    if (pNode == NULL)
    {
        DEP_PT ((ISIS_LGST, "\n\t\t\t None\n"));
    }
    while (pNode != NULL)
    {
        ISIS_DEC_PRINT_ID (pNode->au1DestID, (UINT1) ISIS_SYS_ID_LEN,
                           "\n\t\t\t", ISIS_OCTET_STRING);
        pNode = pNode->pNext;
    }

    i4RetVal = IsisAdjGetIPRAAdjs (pContext, u1Level, u1MetIdx, pAdjList,
                                   u1MtIndex);
    if (i4RetVal == ISIS_FAILURE)
    {
        WARNING ((ISIS_LGST, "ADJ <E> : Failed To Get IPRA Adjacencies\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }

    DEP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetSelfAdjs ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjGetIPRAAdjs ()
 * Description   : This routine fetches all the IP Reachable entries from
 *                 the given Circuit Record for the given Metric Type and Level.
 * Input(s)      : pContext - Pointer to System Context
 *                 pCktRec  - Pointer to Circuit Record
 *                 u1Level  - IS Level
 *                 u1MetIdx - Index for Metric Type (default, error, delay or 
 *                            expense)
 * Output(s)     : pAdjList - Pointer to the List of IP Reachable Addresses.
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if Updation of IPRAs is successful
 *                 ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisAdjGetIPRAAdjs (tIsisSysContext * pContext, UINT1 u1Level, UINT1 u1MetIdx,
                    tIsisSPTNode ** pAdjList, UINT1 u1MtIndex)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Idx = 0;
    UINT1               au1IPMask[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1AddrLen;
    UINT4               u4Metric = 0;
    UINT2               u2Metric = 0;
    tIsisSPTNode       *pNode = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevelRec = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetIPRAAdjs ()\n"));

    /* Travel through all the IPRA entries for the given circuit and update
     * the 'pAdjList' with the matching ones as required
     */

    pIPRARec = pContext->IPRAInfo.pIPRAEntry;

    while (pIPRARec != NULL)
    {
        /* Metric Index 'u1MetIdx' refers to the appropriate metric depending 
         * upon the number of metrics configured. For e.g., consider the 
         * following;
         *
         * Num Metrics = 4 (ALL)
         *
         * u1MetIdx = 0, Metric Type = DEFAULT
         * u1MetIdx = 1, Metric Type = DELAY
         * u1MetIdx = 2, Metric Type = EXPENSE
         * u1MetIdx = 3, Metric Type = ERROR
         *
         * Num Metrics = 3 (Only Def, Del, and Err)
         *
         * u1MetIdx = 0, Metric Type = DEFAULT
         * u1MetIdx = 1, Metric Type = DELAY
         * u1MetIdx = 2, Metric Type = ERROR
         *
         * Num Metrics = 3 (Only Def, Del, and Exp)
         *
         * u1MetIdx = 0, Metric Type = DEFAULT
         * u1MetIdx = 1, Metric Type = DELAY
         * u1MetIdx = 2, Metric Type = EXPENSE
         *
         * Num Metrics = 3 (Only Def, Exp and Err)
         *
         * u1MetIdx = 0, Metric Type = DEFAULT
         * u1MetIdx = 1, Metric Type = EXPENSE
         * u1MetIdx = 2, Metric Type = ERROR
         * 
         * Similarly for Num Metrics = 2, the combinations will be (Def, Del),
         * (Def, Exp) or (Def, Err)
         *
         * Note: Default is always supported.
         */
        /* Retrieve the Metric Value based on the Metric Index passed
         */

        if ((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON)
            && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
        {
            u4Metric = ISIS_GET_METRIC (pContext, pIPRARec, u1MetIdx);
            /* ISIS_COPY_METRIC (pContext, u4Metric, pIPRARec); */

            if (pIPRARec->Metric[0] & ISIS_METRIC_TYPE_MASK)
            {
                /* The metric is an External Metric as indicated by the I/E bit
                 */

                u2Metric &= ~ISIS_METRIC_TYPE_MASK;
                u2Metric |= ISIS_SPT_EXT_MET_FLAG;
            }

            /* Obtain the IP Mask for the given Prefix Length
             */

            IsisUtlGetIPMask (pIPRARec->u1PrefixLen, au1IPMask);

            pNode = (tIsisSPTNode *) ISIS_MEM_ALLOC (ISIS_BUF_SPTN,
                                                     sizeof (tIsisSPTNode));
            if (pNode == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SPT Node\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_SPTN);

                i4RetVal = ISIS_FAILURE;

                ADP_EE ((ISIS_LGST,
                         "ADJ <X> : Exiting IsisAdjGetIPRAAdjs ()\n"));
                return (i4RetVal);
            }
            else
            {
                /* Metric Value takes 10 bits. The remaining bits are used for
                 * passing additional information about the adjacencies to the
                 * decision module. Refer to SPT node typedefinition for more
                 * details about the additional bits usage. Here we are 
                 * setting the System Type Mask
                 */

                u2Metric |= ISIS_GET_SYS_TYPE_MASK (u1Level);

                pNode->pNext = NULL;

                /* pNode->au1DestID is an 2 * ISIS_MAX_IP_ADDR_LEN size array 
                 * which is used for holding both IP Address and Mask together.
                 * For OSI Systems this will hold System ID and the pseudonode 
                 * ID. Hence copy IPRA Destination IP Address first and then 
                 * the IP Address Mask into the same field one after the other
                 */

                /* IP Address first */

                /* In case of Single-topology:
                 * IPRA can be added based on IPRADestType.
                 *
                 * In case of Multi-topology:
                 * v4 IPRA should be added during SPF of MT#0 
                 * v6 IPRA should be added during SPF of MT#2 */
                if ((((pContext->u1IsisMTSupport == ISIS_TRUE) &&
                      (u1MtIndex == ISIS_MT0_INDEX)) ||
                     (pContext->u1IsisMTSupport == ISIS_FALSE)) &&
                    (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4))
                {
                    u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
                    pNode->u1AddrType = ISIS_ADDR_IPV4;
                    MEMCPY (pNode->au1DestID, pIPRARec->au1IPRADest, u1AddrLen);
                    for (u1Idx = 0; u1Idx < u1AddrLen; u1Idx++)
                    {
                        pNode->au1DestID[u1Idx] &= au1IPMask[u1Idx];
                    }
                    u2Metric |= ISIS_IPV4_ADDR_TYPE;
                }
                else if ((((pContext->u1IsisMTSupport == ISIS_TRUE) &&
                           (u1MtIndex == ISIS_MT2_INDEX)) ||
                          (pContext->u1IsisMTSupport == ISIS_FALSE)) &&
                         (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV6))
                {
                    u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
                    pNode->u1AddrType = ISIS_ADDR_IPV6;
                    MEMCPY (pNode->au1DestID, pIPRARec->au1IPRADest, u1AddrLen);

                    for (u1Idx = 0; u1Idx < u1AddrLen; u1Idx++)
                    {
                        pNode->au1DestID[u1Idx] &= au1IPMask[u1Idx];
                    }
                    if (u1Level == ISIS_LEVEL1)
                    {
                        SPT_SET_UPDOWN_FLAG (pNode, ISIS_RL_L1_UP);
                    }
                    else
                    {
                        SPT_SET_UPDOWN_FLAG (pNode, ISIS_RL_L2_UP);
                    }
                    u2Metric |= ISIS_IPV6_ADDR_TYPE;
                }
                else
                {
                    pIPRARec = pIPRARec->pNext;
                    ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pNode);
                    pNode = NULL;
                    continue;
                }

                u2Metric |= ISIS_SELF_IPRA_ADJ;
                /* IP Address Mask calculated based on the Prefix Length 
                 */

                MEMCPY (pNode->au1DestID + u1AddrLen, au1IPMask, u1AddrLen);

                /* Additional bit indicating the type of node (IS adjacency 
                 * or IP * Address) is not updated since if the corresponding 
                 * bit is '0' * then the node is supposed to hold IP address 
                 * information 
                 */

                pNode->au2Metric[u1MetIdx] = (UINT2) (u2Metric);
                pNode->u4MetricVal = u4Metric;

                /* For Self IPRA's which are manually configured, set the 
                   DirIdx as the configured next hop ip address. */
                if (pIPRARec->u1IPRAType == ISIS_LL_MANUAL)
                {
                    if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
                    {
                        MEMCPY (&(pNode->au4DirIdx[u1MetIdx]),
                                pIPRARec->au1IPRANextHop,
                                ISIS_MAX_IPV4_ADDR_LEN);
                    }
                    else
                    {
                        MEMCPY (&(pNode->au1Ipv6DirIdx),
                                pIPRARec->au1IPRANextHop,
                                ISIS_MAX_IPV6_ADDR_LEN);
                        pNode->u1PrefixLen = pIPRARec->u1PrefixLen;

                    }

                }
                else
                {
                    if (IsisAdjGetCktRecWithIfIdx (pIPRARec->u4IfIndex,
                                                   pIPRARec->u4IfSubIndex,
                                                   &pCktRec) != ISIS_FAILURE)
                    {
                        pCktLevelRec =
                            (u1Level ==
                             ISIS_LEVEL1) ? pCktRec->pL1CktInfo : pCktRec->
                            pL2CktInfo;
                        if (pCktLevelRec != NULL)
                        {
                            u4Metric =
                                ISIS_GET_METRIC (pContext, pCktLevelRec,
                                                 ISIS_DEF_MET);
                            pNode->u4MetricVal = u4Metric;
                        }
                    }
                    pNode->au4DirIdx[u1MetIdx] = 0;
                }

                pNode->au1MaxPaths[u1MetIdx] = 0;

                ISIS_DBG_PRINT_ADDR (pNode->au1DestID,
                                     (UINT1) (2 * u1AddrLen),
                                     "ADJ <T> : Copied IP Address & Mask\t",
                                     ISIS_OCTET_STRING);

                /* Inserting the node at the head
                 */

                pNode->pNext = *pAdjList;
                *pAdjList = pNode;
            }
        }
        else
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <T> : IPRA Record not Active - State [ %u ]\n",
                     pIPRARec->u1IPRAAdminState));
        }
        pIPRARec = pIPRARec->pNext;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetIPRAAdjs ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function      : IsisAdjGetAdjRecFromDirIdx ()
 * Description   : This routine fetches Adjacency Record for a given
 *                 Direction Index
 * Input(s)      : pContext - Pointer to System Context
 *                 u4DirIdx - Direction Index for which adjacency record 
 *                            is to be fetched
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : pAdjRec, Pointer to the requested record if found
 *                 NULL, Otherwise
 ******************************************************************************/

PUBLIC tIsisAdjEntry *
IsisAdjGetAdjRecFromDirIdx (tIsisSysContext * pContext, UINT4 u4DirIdx)
{
    tIsisAdjDirEntry   *pDirEntry = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetAdjRecFromDirIdx ()\n"));

    pDirEntry = pContext->AdjDirTable.pDirEntry;

    while (pDirEntry != NULL)
    {
        if (pDirEntry->u4DirIdx == u4DirIdx)
        {
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjGetAdjRecFromDirIdx ()\n"));
            return (pDirEntry->pAdjEntry);
        }
        else
        {
            pDirEntry = pDirEntry->pNext;
        }
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetAdjRecFromDirIdx ()\n"));
    return (NULL);
}

/******************************************************************************
 * Function Name : IsisAdjAddCkt ()
 * Description   : This routine Inserts the given Circuit Record to the Circuit
 *                 Database. The record will be inserted into the list of
 *                 existing records in a sorted order based on Circuit Index
 * Input(s)      : pContext - Pointer to System Context
 *                 pCktRec  - Pointer to Circuit Record 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjAddCkt (tIsisSysContext * pContext, tIsisCktEntry * pCktRec)
{
    UINT4               u4NextIdx = 0;
    tIsisCktEntry      *pTravCkt = NULL;
    tIsisCktEntry      *pPrevCkt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjAddCkt ()\n"));

    pTravCkt = pContext->CktTable.pCktRec;

    if (pTravCkt == NULL)
    {
        /* Very first record being added to the table
         */

        pContext->CktTable.pCktRec = pCktRec;
        pCktRec->pNext = NULL;
    }
    else if (pCktRec->u4CktIdx < pTravCkt->u4CktIdx)
    {
        /* New record has index which is less than the first record. Insert at
         * the head
         */

        pCktRec->pNext = pTravCkt;
        pContext->CktTable.pCktRec = pCktRec;
    }
    else
    {
        /* Travel through the list and get to the correct position
         */

        pPrevCkt = pTravCkt;
        pTravCkt = pTravCkt->pNext;    /* Already done with the first node */

        while (pTravCkt != NULL)
        {
            if (pCktRec->u4CktIdx < pTravCkt->u4CktIdx)
            {
                /* Got to the correct location. Insert the node
                 */

                pCktRec->pNext = pPrevCkt->pNext;
                pPrevCkt->pNext = pCktRec;
                break;
            }
            pPrevCkt = pTravCkt;
            pTravCkt = pTravCkt->pNext;
        }

        if (pTravCkt == NULL)
        {
            /* This means that the new node is to be inserted at the last
             */

            pPrevCkt->pNext = pCktRec;
            pCktRec->pNext = NULL;
        }
    }

    (pContext->CktTable.u4NumEntries)++;

    IsisAdjGetNextCktIdx (pContext, &u4NextIdx);

    pContext->CktTable.u4NextCktIndex = u4NextIdx;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddCkt ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjUpdtCktRec ()
 * Description   : This routine updates an existing circuit record with values
 *                 provided through an updated record
 * Input(s)      : pExistRec - Pointer to the existing Circuit Record 
 *                 pNewRec   - Pointer to the new Record that has the 
 *                             changed values of the circuit record
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjUpdtCktRec (tIsisCktEntry * pExistRec, tIsisCktEntry * pNewRec)
{
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjUpdtCktRec ()\n"));

    /* Only certain parameters can get modified after the circuit is created and
     * it has becom Operationally UP. Those parameters alone get updated and the
     * rest remain the same
     */

    if (pExistRec->u1CktExistState != ISIS_ACTIVE)
    {
        pExistRec->pContext->CktTable.u4NumActCkts++;
    }

    pExistRec->u4CktIfIdx = pNewRec->u4CktIfIdx;
    pExistRec->u4CktIfSubIdx = pNewRec->u4CktIfSubIdx;
    pExistRec->u1CktLocalID = pNewRec->u1CktLocalID;
    pExistRec->bCktAdminState = pNewRec->bCktAdminState;
    pExistRec->u1CktExistState = pNewRec->u1CktExistState;

    pExistRec->u1CktType = pNewRec->u1CktType;
    pExistRec->bCktExtDomain = pNewRec->bCktExtDomain;
    pExistRec->u1CktLevel = pNewRec->u1CktLevel;
    pExistRec->u1CktMCAddr = pNewRec->u1CktMCAddr;
    pExistRec->bCktPassiveCkt = pNewRec->bCktPassiveCkt;

    pExistRec->u1CktMeshGrpEnabled = pNewRec->u1CktMeshGrpEnabled;

    pExistRec->bCktSmallHello = pNewRec->bCktSmallHello;
    pExistRec->u2CktMeshGrps = pNewRec->u2CktMeshGrps;
    pExistRec->u1CktIfStatus = pNewRec->u1CktIfStatus;
    pExistRec->bTxEnable = pNewRec->bTxEnable;
    pExistRec->bRxEnable = pNewRec->bRxEnable;
    pExistRec->u1QFlag = pNewRec->u1QFlag;
    pExistRec->u4CktUpTime = pNewRec->u4CktUpTime;

    MEMCPY (pExistRec->au1P2PCktID, pNewRec->au1P2PCktID, ISIS_SYS_ID_LEN +
            ISIS_PNODE_ID_LEN);
    MEMCPY (pExistRec->au1SNPA, pNewRec->au1SNPA, ISIS_SNPA_ADDR_LEN);

    pExistRec->u1ISHTxCnt = pNewRec->u1ISHTxCnt;
    pExistRec->u1NodeID = pNewRec->u1NodeID;
    pExistRec->u1IfMTId = pNewRec->u1IfMTId;
    pExistRec->u1OperState = pNewRec->u1OperState;
    pExistRec->u1IsisBfdStatus = pNewRec->u1IsisBfdStatus;

    pExistRec->u1P2PDynHshkMachanism = pNewRec->u1P2PDynHshkMachanism;
    pExistRec->u4ExtLocalCircID = pNewRec->u4ExtLocalCircID;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjUpdtCktRec ()\n"));
}

/*******************************************************************************
 * Function    : IsisAdjAddCktLvlRec ()
 * Description : This routine Inserts the given Circuit Level Record into the 
 *               specified circuit. 
 * Input(s)    : pCktRec  - Pointer to Circuit Record 
 *               pCktLvl  - Pointer to Circuit Level Record
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjAddCktLvlRec (tIsisCktEntry * pCktRec, tIsisCktLevel * pCktLvl)
{
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjAddCktLvlRec ()\n"));

    if (pCktLvl->u1CktLvlIdx == ISIS_LEVEL1)
    {
        if (pCktRec->pL1CktInfo == NULL)
        {
            pCktRec->pL1CktInfo = pCktLvl;
        }
    }
    else if (pCktLvl->u1CktLvlIdx == ISIS_LEVEL2)
    {
        if (pCktRec->pL2CktInfo == NULL)
        {
            pCktRec->pL2CktInfo = pCktLvl;
        }
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddCktLvlRec ()\n"));
}

/*******************************************************************************
 * Function    : IsisAdjDelCktLvlRec ()
 * Description : This routine deletes the Circuit Level Record identified by
 *               'u1CktLvlIdx' from the given Circuit. 
 * Input(s)    : pContext - Pointer to System Context
 *               pCktRec  - Pointer to Circuit Record 
 *               pCktLvl  - Pointer to Circuit Level Record
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjDelCktLvlRec (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                     UINT1 u1CktLvlIdx)
{
    tIsisLETLV         *pTlv = NULL;
    tIsisLETLV         *pNxtTlv = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDelCktLvlRec ()\n"));

    if (u1CktLvlIdx == ISIS_LEVEL1)
    {
        if (pCktRec->pL1CktInfo != NULL)
        {
            ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_DELETE,
                                   pCktRec, pCktRec->pL1CktInfo, ISIS_ACTIVE);

            /* Delete all the PSNPs pending on the circuit at this level
             */

            pTlv = pCktRec->pL1CktInfo->pPSNP;

            while (pTlv != NULL)
            {
                pNxtTlv = pTlv->pNext;
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTlv);
                pTlv = pNxtTlv;
            }

            if (pCktRec->u1CktType == ISIS_P2P_CKT)
            {
                IsisTmrStopECTimer (pContext, ISIS_ECT_P2P_HELLO,
                                    pCktRec->u4CktIdx,
                                    pCktRec->pL1CktInfo->u1HelloTmrIdx);
            }
            else
            {
                IsisTmrStopECTimer (pContext, ISIS_ECT_L1LANHELLO,
                                    pCktRec->u4CktIdx,
                                    pCktRec->pL1CktInfo->u1HelloTmrIdx);
            }

            ISIS_MEM_FREE (ISIS_BUF_CKTL, (UINT1 *) (pCktRec->pL1CktInfo));
            pCktRec->pL1CktInfo = NULL;
        }
    }
    else if (u1CktLvlIdx == ISIS_LEVEL2)
    {
        if (pCktRec->pL2CktInfo != NULL)
        {
            ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_DELETE,
                                   pCktRec, pCktRec->pL2CktInfo, ISIS_ACTIVE);
            if (pCktRec->u1CktType == ISIS_BC_CKT)
            {
                IsisTmrStopECTimer (pContext, ISIS_ECT_L2LANHELLO,
                                    pCktRec->u4CktIdx,
                                    pCktRec->pL2CktInfo->u1HelloTmrIdx);
            }

            /* Delete all the PSNPs pending on the circuit at this level
             */

            pTlv = pCktRec->pL2CktInfo->pPSNP;

            while (pTlv != NULL)
            {
                pNxtTlv = pTlv->pNext;
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTlv);
                pTlv = pNxtTlv;
            }

            ISIS_MEM_FREE (ISIS_BUF_CKTL, (UINT1 *) (pCktRec->pL2CktInfo));
            pCktRec->pL2CktInfo = NULL;
        }
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDelCktLvlRec ()\n"));
}

/******************************************************************************
 * Function    : IsisAdjUpdtCktLvlRec ()
 * Description : This routine updates an existing circuit level record with 
 *               values provided through an updated record
 * Input(s)    : pExistLvl - Pointer to the Existing circuit level record
 *               pNewLvl   - Pointer to the new circuit level record that has
 *                           the updated values
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjUpdtCktLvlRec (tIsisCktLevel * pExistLvl, tIsisCktLevel * pNewLvl)
{
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjUpdtCktLvlRec ()\n"));

    /* Only certain parameters can get modified after the circuit is created and
     * it has becom Operationally UP. Those parameters alone get updated and the
     * rest remain the same
     */

    pExistLvl->u1ISPriority = pNewLvl->u1ISPriority;
    pExistLvl->u2HelloMultiplier = pNewLvl->u2HelloMultiplier;
    pExistLvl->bIsDIS = pNewLvl->bIsDIS;

    pExistLvl->LspThrottleInt = pNewLvl->LspThrottleInt;
    pExistLvl->DRHelloTimeInt = pNewLvl->DRHelloTimeInt;

    pExistLvl->HelloTimeInt = pNewLvl->HelloTimeInt;
    pExistLvl->PSNPInterval = pNewLvl->PSNPInterval;
    pExistLvl->CSNPInterval = pNewLvl->CSNPInterval;

    pExistLvl->u4MinLspReTxInt = pNewLvl->u4MinLspReTxInt;

    MEMCPY (pExistLvl->au1CktLanDISID, pNewLvl->au1CktLanDISID,
            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

    MEMCPY (pExistLvl->Metric, pNewLvl->Metric, (sizeof (tIsisMetric)));
    pExistLvl->u4FullMetric = pNewLvl->u4FullMetric;

    MEMCPY (&pExistLvl->CktTxPasswd, &pNewLvl->CktTxPasswd,
            sizeof (tIsisPasswd));

    MEMCPY (&pExistLvl->aCktRxPasswd, &pNewLvl->aCktRxPasswd,
            (sizeof (tIsisPasswd) * ISIS_MAX_RX_PASSWDS));

    /* The new Cicruit Level record is no more needed, hence it can be freed
     */

    ISIS_MEM_FREE (ISIS_BUF_CKTL, (UINT1 *) (pNewLvl));
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjUpdtCktLvlRec ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjUpdtAdjRec ()
 * Description   : This routine updates an existing adjacency record with 
 *                 values provided through an updated record
 * Input(s)      : pExistAdj - Pointer to the Existing Adjacency Record
 *                 pNewAdj   - Pointer to the new Adjacecny record whose
 *                             values are to be updated in the database 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjUpdtAdjRec (tIsisAdjEntry * pExistAdj, tIsisAdjEntry * pNewAdj)
{

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjUpdtAdjRec ()\n"));

    /* Only certain parameters can get modified after the Adjacency is created 
     * and it has become Operationally UP. Those parameters alone get updated 
     * and the rest remain the same
     */

    pExistAdj->u1AdjState = pNewAdj->u1AdjState;
    pExistAdj->u2HoldTime = pNewAdj->u2HoldTime;
    pExistAdj->u1AdjNbrPriority = pNewAdj->u1AdjNbrPriority;
    pExistAdj->u1AdjIpAddrType = pNewAdj->u1AdjIpAddrType;
    pExistAdj->u1AdjMTId = pNewAdj->u1AdjMTId;
    pExistAdj->u1AdjUsage = pNewAdj->u1AdjUsage;
    if (pNewAdj->u1AdjIpAddrType == ISIS_ADDR_IPV4)
    {
        MEMCPY (&pExistAdj->AdjNbrIpV4Addr.au1IpAddr,
                &pNewAdj->AdjNbrIpV4Addr.au1IpAddr, ISIS_MAX_IPV4_ADDR_LEN);
    }
    else if (pNewAdj->u1AdjIpAddrType == ISIS_ADDR_IPV6)
    {
        MEMCPY (&pExistAdj->AdjNbrIpV6Addr.au1IpAddr,
                &pNewAdj->AdjNbrIpV6Addr.au1IpAddr, ISIS_MAX_IPV6_ADDR_LEN);
    }
    else
    {
        MEMCPY (&pExistAdj->AdjNbrIpV4Addr.au1IpAddr,
                &pNewAdj->AdjNbrIpV4Addr.au1IpAddr, ISIS_MAX_IPV4_ADDR_LEN);
        MEMCPY (&pExistAdj->AdjNbrIpV6Addr.au1IpAddr,
                &pNewAdj->AdjNbrIpV6Addr.au1IpAddr, ISIS_MAX_IPV6_ADDR_LEN);
    }
    MEMCPY (pExistAdj->au1AdjProtSupp, pNewAdj->au1AdjProtSupp,
            ISIS_MAX_PROTS_SUPP);

    pExistAdj->u1IsisMT0BfdEnabled = pNewAdj->u1IsisMT0BfdEnabled;
    pExistAdj->u1IsisMT2BfdEnabled = pNewAdj->u1IsisMT2BfdEnabled;
    pExistAdj->u1IsisBfdRequired = pNewAdj->u1IsisBfdRequired;
    pExistAdj->u1IsisMT0BfdState = pNewAdj->u1IsisMT0BfdState;
    pExistAdj->u1IsisMT2BfdState = pNewAdj->u1IsisMT2BfdState;
    pExistAdj->u1IsisBfdNeighborUseable = pNewAdj->u1IsisBfdNeighborUseable;
    pExistAdj->u1DoNotUpdHoldTmr = pNewAdj->u1DoNotUpdHoldTmr;
    pExistAdj->u1Mt0BfdRegd = pNewAdj->u1Mt0BfdRegd;
    pExistAdj->u1Mt2BfdRegd = pNewAdj->u1Mt2BfdRegd;

    pExistAdj->u4NeighExtCircuitID = pNewAdj->u4NeighExtCircuitID;
    pExistAdj->u4AdjPeerNbrExtCircuitID = pNewAdj->u4AdjPeerNbrExtCircuitID;
    pExistAdj->u1P2PThreewayState = pNewAdj->u1P2PThreewayState;
    pExistAdj->u1P2PPeerThreewayState = pNewAdj->u1P2PPeerThreewayState;
    pExistAdj->u1ThreeWayHndShkVersion = pNewAdj->u1ThreeWayHndShkVersion;

    MEMCPY (&pExistAdj->au1AdjPeerNbrSysID,
            &pNewAdj->au1AdjPeerNbrSysID, ISIS_SYS_ID_LEN);

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjUpdtAdjRec ()\n"));
}

/*******************************************************************************
 * Function    : IsisAdjGetAdjRec ()
 * Description : This routine fetches an Adjacency Record based on the given 
 *               Index
 * Input(s)    : pCktRec      - Pointer to Circuit Record
 *               u4AdjIdx     - Adjacency Index 
 * Output(s)   : pAdjRec      - Pointer to Adjacency Record which matched the
 *                              given index
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if a matching Adjacency Record is found
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjGetAdjRec (tIsisCktEntry * pCktRec, UINT4 u4AdjIdx,
                  tIsisAdjEntry ** pAdjRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisAdjEntry      *pTravAdj = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetAdjRec ()\n"));

    /* Travel through the entire list and fetch the matching record
     */

    /* If cache-pointer is not NULL, check entry pointed by 
     * it, maybe we hit first time and do not need scan list */
    if (NULL != pCktRec->pLastAdjEntry)
    {
        if (u4AdjIdx == pCktRec->pLastAdjEntry->u4AdjIdx)
        {
            *pAdjRec = pCktRec->pLastAdjEntry;
            i4RetVal = ISIS_SUCCESS;
            return i4RetVal;

        }
    }

    pTravAdj = pCktRec->pAdjEntry;

    while (pTravAdj != NULL)
    {
        if (pTravAdj->u4AdjIdx == u4AdjIdx)
        {
            *pAdjRec = pTravAdj;
            i4RetVal = ISIS_SUCCESS;
            break;
        }
        pTravAdj = pTravAdj->pNext;
    }

    if (i4RetVal == ISIS_FAILURE)
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : No Matching Adjacency Record Found - Circuit [%u]\n",
                 pCktRec->u4CktIdx));
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetAdjRec ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjDelAdjAA ()
 * Description   : This routine deletes an Adjacency Area Address Record from 
 *                 Adjacency Record. 
 * Input(s)      : pAdjRec       - Pointer to Adjacency Record
 *                 pau1AreaAddr  - Pointer to Adjacency Area Address 
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjDelAdjAA (tIsisAdjEntry * pAdjRec, UINT1 *pau1AreaAddr)
{
    tIsisAdjAAEntry    *pDelAA = NULL;
    tIsisAdjAAEntry    *pPrevAA = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDelAdjAA ()\n"));

    ISIS_DBG_PRINT_ADDR (pau1AreaAddr, (UINT1) ISIS_AREA_ADDR_LEN,
                         "ADJ <T> : Deleting Area Address\t",
                         ISIS_OCTET_STRING);

    pDelAA = pAdjRec->pAdjAreaAddr;

    while (pDelAA != NULL)
    {
        if (MEMCMP (pDelAA->ISAdjAreaAddress.au1AreaAddr,
                    pau1AreaAddr, ISIS_AREA_ADDR_LEN) == 0)
        {
            ADP_PT ((ISIS_LGST, "ADJ <T> : Found A Matching Record\n"));

            if (pPrevAA != NULL)
            {
                pPrevAA->pNext = pDelAA->pNext;
            }
            else
            {
                pAdjRec->pAdjAreaAddr = pDelAA->pNext;
            }

            pDelAA->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_ADAA, (UINT1 *) pDelAA);
            break;
        }
        pPrevAA = pDelAA;
        pDelAA = pDelAA->pNext;
    }

    if (pDelAA == NULL)
    {
        ADP_PT ((ISIS_LGST, "ADJ <T> : Requested Record Not Found\n"));
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDelAdjAA ()\n"));
}

/*******************************************************************************
 * Function Name : IsisAdjGetCktRecWithIfIdx ()
 * Description   : This routine fetches the Circuit Record based on the given 
 *                 Circuit Index and Circuit Sub Index
 * Input(s)      : u4CktIfIdx    - Circuit Interface Index 
 *                 u4CktIfSubIdx - Circuit Sub-IfIndex
 * Output(s)     : pCktRec       - Pointer to Circuit Record 
 * Globals       : gIsisCktHashTable Referred but not Modified
 * Returns       : ISIS_SUCCESS - If the Circuit Record is fetched
 *                 ISIS_FAILURE - Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjGetCktRecWithIfIdx (UINT4 u4CktIfIdx, UINT4 u4CktIfSubIdx,
                           tIsisCktEntry ** pCktRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisCktEntry      *pTravCkt = NULL;
    tIsisSysContext    *pContext = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetCktRecWithIfIdx ()\n"));

    for (pContext = gpIsisInstances; pContext != NULL;
         pContext = pContext->pNext)
    {
        pTravCkt = pContext->CktTable.pCktRec;

        while (pTravCkt != NULL)
        {
            if ((pTravCkt->u4CktIfIdx == u4CktIfIdx)
                && (pTravCkt->u4CktIfSubIdx == u4CktIfSubIdx))
            {
                *pCktRec = pTravCkt;
                i4RetVal = ISIS_SUCCESS;
                return (i4RetVal);

            }
            else
            {
                pTravCkt = pTravCkt->pNext;
            }

        }
    }                            /* End of for Loop */

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetCktRecWithIfIdx ()\n"));

    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjGetNextCktIdx ()
 * Description   : This routines fetches the next minimum available Circuit 
 *                 Index from the Circuit table.
 * Input(s)      : pContext     - Pointer to System Context 
 * Output(s)     : pu4NextIdx   - Next Available Minimum value of Circuit
 *                                Index
 * Globals       : gIsisMemActuals Referred but not Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjGetNextCktIdx (tIsisSysContext * pContext, UINT4 *pu4NextIdx)
{
    UINT4               u4TempIdx = 1;
    tIsisCktEntry      *pTravCkt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetNextCktIdx ()\n"));

    /* ALGO: Start from '1' for 'u4TempIdx' and proceed comparing the value with
     * the values in the records. Since Circuit Records are sorted, we can
     * return 'u4TempIdx' if it is less than the Value encountered in the
     * Circuit Record. If all circuit records are exhausted, then the value of
     * 'u4TempIdx' at that instance can be returned only if it is less than the
     * max configured value
     */

    pTravCkt = pContext->CktTable.pCktRec;

    while (pTravCkt != NULL)
    {
        if (u4TempIdx < pTravCkt->u4CktIdx)
        {
            *pu4NextIdx = u4TempIdx;
            break;
        }
        u4TempIdx++;
        pTravCkt = pTravCkt->pNext;
    }

    if (pTravCkt == NULL)
    {
        /* We have reached the end of the list. This means all the indices 
         * starting from '1' till the current value of 'u4TempIdx' are already 
         * assigned. Now we can assign 'u4TempIdx' only if this is less than 
         * the max configured value.
         */

        *pu4NextIdx = u4TempIdx;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetNextCktIdx ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjGetNextAdjIdx ()
 * Description   : This routines fetches the next minimum available Adjacency 
 *                 Index from the Adjacency table.
 * Input(s)      : pCktRec      - Pointer to Circuit Record 
 * Output(s)     : pu4NextIdx   - Next Available Minimum value of Adjacency
 *                                Index
 * Globals       : gIsisMemActuals Referred but not Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjGetNextAdjIdx (tIsisCktEntry * pCktRec, UINT4 *pu4NextIdx)
{
    UINT4               u4TempIdx = 1;
    tIsisAdjEntry      *pTravAdj = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetNextAdjIdx ()\n"));

    /* ALGO: Start from '1' for 'u4TempIdx' and proceed comparing the value with
     * the values in the records. Since Adjacency Records are sorted, we can
     * return 'u4TempIdx' if it is less than the Value encountered in the
     * Adjacency Record. If all adjacency records are exhausted, then the value
     * of 'u4TempIdx' at that instance can be returned only if it is less than 
     * the max configured value
     */

    pTravAdj = pCktRec->pAdjEntry;

    while (pTravAdj != NULL)
    {
        if (u4TempIdx < pTravAdj->u4AdjIdx)
        {
            *pu4NextIdx = u4TempIdx;
            break;
        }
        u4TempIdx++;
        pTravAdj = pTravAdj->pNext;
    }

    if (pTravAdj == NULL)
    {
        /* We have reached the end of the list. This means all the indices 
         * starting from '1' till the current value of 'u4TempIdx' are already 
         * assigned. Now we can assign 'u4TempIdx' only if this is less than 
         * the max configured value.
         */

        *pu4NextIdx = u4TempIdx;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetNextAdjIdx ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjGetNextDirIdx ()
 * Description   : This routines fetches the next minimum available Direction 
 *                 Index from the Direction table.
 * Input(s)      : pContext     - Pointer to System Context 
 * Output(s)     : pu4NextIdx   - Next Available Minimum value of Direction 
 *                                Index
 * Globals       : gIsisMemActuals Referred but not Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjGetNextDirIdx (tIsisSysContext * pContext, UINT4 *pu4NextIdx)
{
    UINT4               u4TempIdx = 1;
    tIsisAdjDirEntry   *pTravDir = NULL;
    tIsisAdjDelDirEntry *pDelDirEntry = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetNextDirIdx ()\n"));

    /* ALGO: Start from '1' for 'u4TempIdx' and proceed comparing the value with
     * the values in the records. Since Direction Records are sorted, we can
     * return 'u4TempIdx' if it is less than the Value encountered in the
     * Direction Record. If all direction records are exhausted, then the value
     * of 'u4TempIdx' at that instance can be returned only if it is less than 
     * the max configured value
     */

    pTravDir = pContext->AdjDirTable.pDirEntry;
    pDelDirEntry = pContext->pDelDirEntry;

    while (pTravDir != NULL)
    {
        if (u4TempIdx < pTravDir->u4DirIdx)
        {
            if (pDelDirEntry == NULL)
            {
                *pu4NextIdx = u4TempIdx;
                return;
            }
            while (pDelDirEntry != NULL)
            {
                if (pDelDirEntry->u4DirIdx == u4TempIdx)
                {
                    u4TempIdx++;
                    pDelDirEntry = pDelDirEntry->pNext;
                    break;
                }
                else if (pDelDirEntry->u4DirIdx > u4TempIdx)
                {
                    *pu4NextIdx = u4TempIdx;
                    return;
                }
            }
        }
        else
        {
            pTravDir = pTravDir->pNext;
            u4TempIdx++;
        }
    }
    while (pDelDirEntry != NULL)
    {
        if (pDelDirEntry->u4DirIdx > u4TempIdx)
        {
            *pu4NextIdx = u4TempIdx;
            return;
        }
        else
        {
            pDelDirEntry = pDelDirEntry->pNext;
            u4TempIdx++;
        }
    }

    *pu4NextIdx = u4TempIdx;
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetNextDirIdx ()\n"));
    return;
}

/******************************************************************************
 * Function Name : IsisAdjGetCktAdjs ()
 * Description   : This routines fetches all the adjacencies from a given
 *                 Circuit for a given level. 
 * Input(s)      : pContext     - Pointer to System Context
 *                 pCktRec      - Pointer to Circuit Record
 *                 u1Level      - Level of the circuit (Level1 or Level2)
 * Output(s)     : pAdjList     - Pointer to the list of matching adjacencies 
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if matching Adjacencies found
 *                 ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjGetCktAdjs (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                   UINT1 u1Level, tIsisSPTNode ** pAdjList)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Cnt = 0;
    tIsisAdjEntry      *pAdjRec = NULL;
    tIsisSPTNode       *pNode = NULL;
    tIsisCktLevel      *pCktLevel = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetCktAdjs ()\n"));

    pAdjRec = pCktRec->pAdjEntry;

    /* For P2P circuits irrespective of the Level of circuit, the adjacency 
     * information will be retrieved only from Level1 records. Only statistics
     * will be maintained on per level basis
     */

    if ((pCktRec->u1CktType == ISIS_P2P_CKT)
        || ((pCktRec->u1CktType == ISIS_BC_CKT) && (u1Level == ISIS_LEVEL1)))
    {
        pCktLevel = pCktRec->pL1CktInfo;
    }
    else if ((pCktRec->u1CktType == ISIS_BC_CKT) && (u1Level == ISIS_LEVEL2))
    {
        pCktLevel = pCktRec->pL2CktInfo;
    }
    else
    {
        return ISIS_FAILURE;    /*klocwork */
    }

    while (pAdjRec != NULL)
    {
        if (pAdjRec->u1AdjUsage == u1Level)
        {
            pNode = (tIsisSPTNode *) ISIS_MEM_ALLOC (ISIS_BUF_SPTN,
                                                     sizeof (tIsisSPTNode));
            if (pNode == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SPT Node\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                i4RetVal = ISIS_FAILURE;
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_SPTN);
                ADP_EE ((ISIS_LGST,
                         "ADJ <X> : Exiting IsisAdjGetCktAdjs ()\n"));
                return (i4RetVal);
            }
            else
            {
                pNode->pNext = NULL;

                /* Destination ID includes System ID and Pseudonode ID */

                /* NOTE: The following information is retrieved for Update 
                 * module which uses the adjacency list returned to build a 
                 * pseudonode LSP. It requires only the Destination ID and the
                 * Metric and does not require the DirIdx and MaxPaths.
                 */

                MEMCPY (pNode->au1DestID, pAdjRec->au1AdjNbrSysID,
                        ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                for (u1Cnt = 0; u1Cnt < ISIS_NUM_METRICS; u1Cnt++)
                {
                    pNode->au4DirIdx[u1Cnt] = 0;
                    pNode->au1MaxPaths[u1Cnt] = 0;
                    pNode->u4MetricVal =
                        ISIS_GET_METRIC (pContext, pCktLevel, ISIS_DEF_MET);
                    /*ISIS_COPY_METRIC (pContext, pNode->u4MetricVal, pCktLevel); */

                }

                /* Inserting the node at the head
                 */

                pNode->pNext = (*pAdjList);
                *pAdjList = pNode;
            }
        }
        pAdjRec = pAdjRec->pNext;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetCktAdjs ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function Name : IsisAdjAddAuthToPDU ()
 * Description   : This routines encodes the Authentication Information into 
 *                 the PDU. If the length of the PDU is not sufficient to encode
 *                 the Authentication information, then the routine returns an 
 *                 Error code and the calling routine must ensure that the PDU 
 *                 is freed
 * Input(s)      : pCktLevel    - Pointer to Circuit Level Record
 *                 pu1PDU       - Pointer to PDU to be updated  with
 *                                Authentication information
 *                 u2BufSize    - Maximum size of the Buffer               
 * Output(s)     : pu2Offset    - Last written byte. Subsequent encoding into
 *                                the PDU must start from here 
 *                 pu1PDU       - PDU updated with the Authentication TLVs
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if encoding is successful
 *                 ISIS_FAILURE, otherwise
 *****************************************************************************/

PUBLIC INT4
IsisAdjAddAuthToPDU (tIsisCktLevel * pCktLevel, UINT1 *pu1PDU, UINT2 u2BufSize,
                     UINT2 *pu2Offset)
{
    UINT1               u1PasswdLen = 0;
    UINT1              *pPasswd = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAddAuthToPDU ()\n"));

    u1PasswdLen = pCktLevel->CktTxPasswd.u1Len;

    if ((u2BufSize - *pu2Offset) < (u1PasswdLen + 3))
    {
        WARNING ((ISIS_LGST,
                  "ADJ <W> : Insufficient Buffer To Encode Authentication - Buf Size [ %u ], Password Len [ %u ]",
                  *pu2Offset, u1PasswdLen));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAddAuthToPDU ()\n"));
        return ISIS_FAILURE;
    }

    pPasswd = pCktLevel->CktTxPasswd.au1Password;
    if ((u1PasswdLen == 0) || (pPasswd == NULL))    /*KLOC Work */
    {
        ADP_PT ((ISIS_LGST, "ADJ <T> : Invalid Password [%u] or Length [%u]\n",
                 *pPasswd, u1PasswdLen));
        ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAddAuthToPDU ()\n"));
        return ISIS_FAILURE;
    }
    ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, ISIS_AUTH_INFO_TLV);
    *pu2Offset += 1;
    ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, (u1PasswdLen + 1));
    *pu2Offset += 1;
    ISIS_ASSIGN_1_BYTE (pu1PDU, *pu2Offset, ISIS_CLR_TXT_PASS);
    *pu2Offset += 1;
    MEMCPY ((pu1PDU + *pu2Offset), pPasswd, u1PasswdLen);
    *pu2Offset = (UINT2) (*pu2Offset + u1PasswdLen);

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAddAuthToPDU ()\n"));
    return ISIS_SUCCESS;

}

/******************************************************************************
 * Function Name : IsisAdjGetPNodes ()
 * Description   : This routine fetches all the Pseudonode IDs, along with the
 *                 associated Metrics, available on all the Broadcast Circuits 
 *                 for the given Level. 
 * Input(s)      : pContext     - Pointer to System Context
 *                 u1Level      - Level of the circuit (Level1 or Level2)
 * Output(s)     : pPNList      - Pointer to the list of Pseudonode IDs along
 *                                with the Metrics
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, If Pseudonode IDs are retrieved successfully
 *                 ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjGetPNodes (tIsisSysContext * pContext, UINT1 u1Level,
                  tIsisSPTNode ** pPNList)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Cnt = 0;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisSPTNode       *pNode = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetPNodes ()\n"));

    /* This routine will output a list of SPT nodes, which will have Pseudonode
     * IDs and their associated metrics for each of the Broadcast Circuits. This
     * is used for inserting adjacencies learnt via the Pseudonode LSPs during
     * transient conditions
     */

    /* Peek at each of the circuit, retrieve the LAN Designated ID from the
     * circuit Level along with the Metric value, if the circuit happens to be 
     * a Broadcast circuit
     */

    pCktRec = pContext->CktTable.pCktRec;

    while (pCktRec != NULL)
    {
        if (pCktRec->u1CktType == ISIS_BC_CKT)
        {
            pCktLevel = ((u1Level == ISIS_LEVEL1) ? pCktRec->pL1CktInfo
                         : pCktRec->pL2CktInfo);

            if ((pCktLevel == NULL) || (pCktLevel->u4NumAdjs == 0))
            {
                pCktRec = pCktRec->pNext;
                continue;
            }

            pNode = (tIsisSPTNode *) ISIS_MEM_ALLOC (ISIS_BUF_SPTN,
                                                     sizeof (tIsisSPTNode));
            if (pNode == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SPT Node\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_SPTN);
                ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetPnodes ()\n"));
                return (ISIS_FAILURE);
            }
            else
            {
                for (u1Cnt = 0; u1Cnt < ISIS_NUM_METRICS; u1Cnt++)
                {
                    pNode->au4DirIdx[u1Cnt] = pCktLevel->u4DISDirIdx;
                    pNode->au1MaxPaths[u1Cnt] = 1;
                    pNode->u4MetricVal =
                        ISIS_GET_METRIC (pContext, pCktLevel, ISIS_DEF_MET);
                    /*ISIS_COPY_METRIC (pContext, pNode->u4MetricVal, pCktLevel); */

                }

                MEMCPY (pNode->au1DestID, pCktLevel->au1CktLanDISID,
                        ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                pAdjEntry = IsisAdjGetAdjRecFromDirIdx (pContext,
                                                        pNode->au4DirIdx[0]);
                if (pAdjEntry != NULL)
                {
                    if (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPV4)
                    {
                        pNode->u1AddrType = ISIS_ADDR_IPV4;
                    }
                    else if (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPV6)
                    {
                        pNode->u1AddrType = ISIS_ADDR_IPV6;
                    }
                    else
                    {
                        pNode->u1AddrType = ISIS_ADDR_IPV4;
                    }
                }

                ISIS_DBG_PRINT_ID (pCktLevel->au1CktLanDISID,
                                   (UINT1) (ISIS_SYS_ID_LEN +
                                            ISIS_PNODE_ID_LEN),
                                   "ADJ <T> : Retrieved DIS ID\t",
                                   ISIS_OCTET_STRING);

                /* Inserting the node at the head of the list
                 */

                pNode->pNext = *pPNList;
                *pPNList = pNode;
                i4RetVal = ISIS_SUCCESS;
            }
        }

        pCktRec = pCktRec->pNext;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetPNodes ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisAdjDelPSNPNode ()
 * Description : This function deletes the specified LSP information from the
 *               PSNP held in the specified circuit level record
 * Input (s)   : pCktLvlRec - Pointer to the circuit level record
 *               pu1LSPId   - Pointer to the LSP Identifier
 * Output (s)  : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisAdjDelPSNPNode (tIsisCktLevel * pCktLvlRec, UINT1 *pu1LSPId)
{
    tIsisLETLV         *pTravPSNP = NULL;
    tIsisLETLV         *pPrevPSNP = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <E> : Entered IsisAdjDelPSNPNode () \n"));

    /* PSNPs are maintained as TLVs containing LETLV information. Hence Deleting
     * information pertaining to the given LSPID reduces to finding the
     * appropriate LETLV which matches the given LSPID.
     */

    pTravPSNP = pCktLvlRec->pPSNP;

    /* Travel the PSNP List, and if the PSNP Matching the given
     * LSPID is found, delete the TLV information from the list
     */

    while (pTravPSNP != NULL)
    {
        if (MEMCMP (pTravPSNP->au1LSPId, pu1LSPId, ISIS_LSPID_LEN) == 0)
        {
            /* Found a matching entry. Delete it
             */

            ISIS_DBG_PRINT_ID (pTravPSNP->au1LSPId, (UINT1) ISIS_LSPID_LEN,
                               "ADJ <T> : LSP ID\t", ISIS_OCTET_STRING);

            if (pPrevPSNP == NULL)
            {
                /* Matching entry is the very first in the PSNP list. Deleting
                 * it would cause the head of the list to change
                 */

                pCktLvlRec->pPSNP = pTravPSNP->pNext;
            }
            else
            {
                pPrevPSNP->pNext = pTravPSNP->pNext;
            }

            pTravPSNP->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravPSNP);
            break;
        }
        else
        {
            pPrevPSNP = pTravPSNP;
            pTravPSNP = pTravPSNP->pNext;
        }
    }

    ADP_EE ((ISIS_LGST, "ADJ <E> : Exiting IsisAdjDelPSNPNode ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjAddPSNP ()
 * Description   : This routine Modifies the PSNP Entry if already exist, or
 *                 Adds PSNP Entry.
 * Input(s)      : pCktRec - Pointer to the Circuit Record
 *                 pPSNP   - Pointer to the PSNP Entry
 *                 u1Level - Level of PSNP Entry
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjAddPSNP (tIsisCktEntry * pCktRec, tIsisLETLV * pPSNP, UINT1 u1Level)
{
    INT4                i4RetVal = 0;
    tIsisLETLV         *pTravPSNP = NULL;
    tIsisCktLevel      *pCktLvlRec = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <E> : Entered IsisAdjAddPSNP () \n"));

    pCktLvlRec = (u1Level == ISIS_LEVEL1) ? pCktRec->pL1CktInfo
        : pCktRec->pL2CktInfo;
    pTravPSNP = pCktLvlRec->pPSNP;

    while (pTravPSNP != NULL)
    {
        /* Check whether the given LSP already exists in the PSNP TLV list. If
         * it exists update the Seq. No, RLT, Checksum etc. 
         */

        i4RetVal = MEMCMP (pTravPSNP->au1LSPId, pPSNP->au1LSPId,
                           ISIS_LSPID_LEN);
        if (i4RetVal == 0)
        {
            pTravPSNP->u2RLTime = pPSNP->u2RLTime;
            pTravPSNP->u4SeqNum = pPSNP->u4SeqNum;
            pTravPSNP->u2CheckSum = pPSNP->u2CheckSum;

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Circuit [%u] - Updating LSP Information In PSNP - RLT [%u], Seq. No [%u], Checksum [%u]\n",
                     pCktRec->u4CktIdx, pPSNP->u2RLTime, pPSNP->u4SeqNum,
                     pPSNP->u2CheckSum));

            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pPSNP);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisAdjAddPSNP ()\n"));
            return;
        }
        pTravPSNP = pTravPSNP->pNext;
    }

    /* Information relating to the new received LSP not found. 
     * add New PSNP in the Head of PSNP TLV list
     */

    pPSNP->pNext = pCktLvlRec->pPSNP;
    pCktLvlRec->pPSNP = pPSNP;

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisAdjAddPSNP ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjDelCktLvlPSNPs ()
 * Description   : This routine deletes all the PSNP TLVs present in the
 *                 given Circuit Level Record 
 * Input(s)      : pCktLvlRec - Pointer to the Circuit Level Record
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjDelCktLvlPSNPs (tIsisCktLevel * pCktLvlRec)
{
    tIsisLETLV         *pPSNP = NULL;
    tIsisLETLV         *pNextPSNP = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDelCktLvlPSNPs ()\n"));

    /* Travel through the entire list freeing each one at a time
     */

    pPSNP = pCktLvlRec->pPSNP;

    while (pPSNP != NULL)
    {
        ISIS_DBG_PRINT_ID (pPSNP->au1LSPId, (UINT1) ISIS_LSPID_LEN,
                           "ADJ <T> : LSP ID\t", ISIS_OCTET_STRING);

        pNextPSNP = pPSNP->pNext;
        pPSNP->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pPSNP);
        pPSNP = pNextPSNP;
    }
    pCktLvlRec->pPSNP = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDelCktLvlPSNPs ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjCopyCktDefVal () 
 * Description   : This routine updates the Circuit record with the default
 *                 values where ever applicable. It allocates memory for both
 *                 the Circuit Level1 and Level2 records and initialises them
 *                 with the default values since by default the System Type is 
 *                 taken to be L12 
 * Input(s)      : pContext - Pointer to System Context
 *                 pCktRec  - Pointer to the Circuit Record
 * Output(s)     : None
 * Returns       : VOID
******************************************************************************/

PUBLIC INT4
IsisAdjCopyCktDefVal (tIsisSysContext * pContext, tIsisCktEntry * pCktRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjCopyCktDefVal ()\n"));

    /* Only fields that can take default values are updated. Other fields must
     * be configured by the manager
     */

    pCktRec->pContext = pContext;

    pCktRec->bCktExtDomain = ISIS_FALSE;
    pCktRec->u1CktLevel = ISIS_LEVEL12;
    pCktRec->u1CktMCAddr = ISIS_LL_GROUP;

    pCktRec->bCktPassiveCkt = ISIS_FALSE;
    pCktRec->u1CktMeshGrpEnabled = ISIS_LL_MESH_INACTIVE;
    pCktRec->u2CktMeshGrps = 1;

    pCktRec->bCktAdminState = ISIS_STATE_OFF;
    pCktRec->bCktSmallHello = ISIS_STATE_OFF;
    pCktRec->bTxEnable = ISIS_TRUE;
    pCktRec->bRxEnable = ISIS_TRUE;

    if (pContext->u1IsisBfdAllIfStatus == ISIS_BFD_ENABLE)
    {
        pCktRec->u1IsisBfdStatus = ISIS_BFD_ENABLE;
    }
    else
    {
        pCktRec->u1IsisBfdStatus = ISIS_BFD_DISABLE;
    }

    i4RetVal = IsisAdjInitCktLvl (pContext, pCktRec, ISIS_LEVEL1);
    if (i4RetVal == ISIS_FAILURE)
    {
        return (ISIS_FAILURE);
    }
    i4RetVal = IsisAdjInitCktLvl (pContext, pCktRec, ISIS_LEVEL2);
    if (i4RetVal == ISIS_FAILURE)
    {
        return (ISIS_FAILURE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjCopyCktDefVal ()\n"));
    return (ISIS_SUCCESS);
}

/*******************************************************************************
 * Function Name : IsisAdjInitHelloTmInt
 * Description   : This routine updates the record with default values for 
 *                 Hello Timer and Hello Multiplier for specified circuit type.
 * Input(s)      : pCktRec  - Pointer to Circuit Record
 * Output(s)     : None
 * Returns       : VOID
******************************************************************************/

PUBLIC VOID
IsisAdjInitHelloTmInt (tIsisCktEntry * pCktRec)
{
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjInitHelloTmInt ()\n"));

    if ((pCktRec->u1CktType == ISIS_BC_CKT)
        || (pCktRec->u1CktType == ISIS_P2P_CKT))
    {
        if (pCktRec->pL1CktInfo != NULL)
        {
            pCktRec->pL1CktInfo->u2HelloMultiplier = ISIS_HELLO_MULT;
            pCktRec->pL1CktInfo->HelloTimeInt = ISIS_HELLO_INT;
        }
        if (pCktRec->pL2CktInfo != NULL)
        {
            pCktRec->pL2CktInfo->u2HelloMultiplier = ISIS_HELLO_MULT;
            pCktRec->pL2CktInfo->HelloTimeInt = ISIS_HELLO_INT;
        }
    }
    else
    {
        ADP_PT ((ISIS_LGST, "ADJ <E> : Invalid Circuit Type \n"));
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjInitHelloTmInt ()\n"));
}

/*******************************************************************************
 * Function Name : IsisAdjInitCktLvl () 
 * Description   : This routine allocates the memory for the specified level
 *                 and updates the record with default values. 
 * Input(s)      : pContext - Pointer to System Context
 *                 pCktRec  - Pointer to Circuit Record
 *                 u1CktLvl - Level of the circuit 
 * Output(s)     : None
 * Returns       : VOID
******************************************************************************/

PUBLIC INT4
IsisAdjInitCktLvl (tIsisSysContext * pContext, tIsisCktEntry * pCktRec,
                   UINT1 u1CktLvl)
{
    tIsisCktLevel      *pCktLvl = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjInitCktLvl ()\n"));

    pCktLvl = (tIsisCktLevel *)
        ISIS_MEM_ALLOC (ISIS_BUF_CKTL, sizeof (tIsisCktLevel));

    if (pCktLvl != NULL)
    {
        pCktLvl->u1CktLvlIdx = u1CktLvl;
        pCktLvl->bIsDIS = ISIS_FALSE;
        pCktLvl->u1ISPriority = ISIS_IS_PRIORITY;
        pCktLvl->u2HelloMultiplier = ISIS_HELLO_MULT;
        pCktLvl->HelloTimeInt = ISIS_HELLO_INT;
        pCktLvl->DRHelloTimeInt = ISIS_DR_HELLO_INT;
        pCktLvl->LspThrottleInt = ISIS_LSP_THROTTLE;
        pCktLvl->CSNPInterval = ISIS_CSNP_INT;
        pCktLvl->PSNPInterval = ISIS_PSNP_INT;
        pCktLvl->u4MinLspReTxInt = ISIS_LSP_RETXN_INT;
        pCktLvl->u1CircLevelAuthType = ISIS_AUTH_PASSWD;
        pCktLvl->pPSNP = NULL;
        pCktLvl->pMarkTxLSP = NULL;
        pCktLvl->u1ECTId = ISIS_ZERO;
        pCktLvl->u1CSNPSent = ISIS_ZERO;
        pCktLvl->u1SetSRM = ISIS_TRUE;

        /* Update the default values for all the Metrics in the Circuit LEvel
         * Information Record
         */

        IsisUtlFillMetDefVal (pContext, pCktLvl, ISIS_CKT_LEVEL_REC);

        if (u1CktLvl == ISIS_LEVEL1)
        {
            pCktRec->pL1CktInfo = pCktLvl;
        }
        else
        {
            pCktRec->pL2CktInfo = pCktLvl;
        }

        /*Initialization */
        pCktRec->u1IsisGRRestartHelloTxCount = 0;
        pCktRec->u1IsisGRRestartL1State = 0;
        ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec, pCktLvl,
                               pCktRec->u1CktExistState);
        return (ISIS_SUCCESS);
    }
    else
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_CKTL);
        ADP_PT ((ISIS_LGST,
                 "ADJ <E> : Memory Allocation Failed For Circuit Level\n"));
        if ((u1CktLvl == ISIS_LEVEL1) && (pCktRec->pL1CktInfo == NULL))
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Circuit Level L1-IS !!! \n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
        else if ((u1CktLvl == ISIS_LEVEL2) && (pCktRec->pL2CktInfo == NULL))
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Circuit Level L2-IS !!! \n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
        else if ((u1CktLvl == ISIS_LEVEL12) && (pCktRec->pL2CktInfo == NULL)
                 && (pCktRec->pL1CktInfo == NULL))
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : Circuit Level L1L2-IS !!! \n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
        return (ISIS_FAILURE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjInitCktLvl ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjActivateCktLvl ()
 * Description   : This routine initialises memory for the Circuit Level records
 *                 based on the Circuit Level. The following actions are
 *                 performed:
 *                 -- If Circuit Type is Level1, Level1 circuit information
 *                    record is initialised if it does not exist and if Level2 
 *                    information record exists it is freed
 *                 -- If Circuit Type is Level2, Level2 circuit information
 *                    record is initialised if it does not exist and if Level2 
 *                    information record exists it is freed
 *                 -- If Circuit Type is Level12, both Level1 and Level2
 *                    information records are initialised if they don't exist   
 * Input (s)     : pContext   - Pointer to System Context
 *                 pCktRec    - Pointer to Circuit record
 * Output (s)    : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
******************************************************************************/

PUBLIC INT4
IsisAdjActivateCktLvl (tIsisSysContext * pContext, tIsisCktEntry * pCktRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjActivateCktLvl ()\n"));

    if (pCktRec->u1CktLevel == ISIS_LEVEL1)
    {
        if (pCktRec->pL1CktInfo == NULL)
        {
            i4RetVal = IsisAdjInitCktLvl (pContext, pCktRec, ISIS_LEVEL1);
            if (i4RetVal == ISIS_FAILURE)
            {
                return (ISIS_FAILURE);
            }
        }
        ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec,
                               pCktRec->pL1CktInfo, pCktRec->u1CktExistState);

    }
    else if (pCktRec->u1CktLevel == ISIS_LEVEL2)
    {
        if (pCktRec->pL2CktInfo == NULL)
        {
            i4RetVal = IsisAdjInitCktLvl (pContext, pCktRec, ISIS_LEVEL2);
            if (i4RetVal == ISIS_FAILURE)
            {
                return (ISIS_FAILURE);
            }
        }
        ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec,
                               pCktRec->pL1CktInfo, pCktRec->u1CktExistState);
    }
    else if (pCktRec->u1CktLevel == ISIS_LEVEL12)
    {
        if (pCktRec->pL1CktInfo == NULL)
        {
            i4RetVal = IsisAdjInitCktLvl (pContext, pCktRec, ISIS_LEVEL1);
            if (i4RetVal == ISIS_FAILURE)
            {
                return (ISIS_FAILURE);
            }
        }
        ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec,
                               pCktRec->pL1CktInfo, pCktRec->u1CktExistState);

        if (pCktRec->pL2CktInfo == NULL)
        {
            i4RetVal = IsisAdjInitCktLvl (pContext, pCktRec, ISIS_LEVEL2);
            if (i4RetVal == ISIS_FAILURE)
            {
                return (ISIS_FAILURE);
            }
        }
        ISIS_FLTR_CKT_LVL_LSU (pContext, ISIS_CMD_ADD, pCktRec,
                               pCktRec->pL1CktInfo, pCktRec->u1CktExistState);
    }
    else
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Erroneous Circuit - Index [ %u ], Level [ %u ]\n",
                 pCktRec->u4CktIdx, pCktRec->u1CktLevel));
        return (ISIS_FAILURE);
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjActivateCktLvl ()\n"));
    return (ISIS_SUCCESS);
}

/******************************************************************************
 * Function Name : IsisAdjGetDirEntryWithAdjIdx ()
 * Description   : This function scans the Direction Table and returns Direction
 *                 Index for the given adjacency.
 * Input(s)      : pContext  - Pointer to system Context
 *                 u4AdjIdx  - Adjacency Index
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : Direction Index if a matching entry is found
 *                 0, otherwise
 ******************************************************************************/

PUBLIC UINT4
IsisAdjGetDirEntryWithAdjIdx (tIsisSysContext * pContext,
                              tIsisAdjEntry * pAdjRec)
{
    tIsisAdjDirEntry   *pDirEntry = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetDirEntryWithAdjIdx ()\n"));

    pDirEntry = pContext->AdjDirTable.pDirEntry;
    while (pDirEntry != NULL)
    {
        if (pDirEntry->pAdjEntry == pAdjRec)
        {
            ADP_EE ((ISIS_LGST,
                     "ADJ <X> : Exiting IsisAdjGetDirEntryWithAdjIdx ()\n"));
            return pDirEntry->u4DirIdx;
        }
        pDirEntry = pDirEntry->pNext;
    }

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetDirEntryWithAdjIdx ()\n"));
    return 0;
}

/******************************************************************************
 * Function Name : IsisAdjAddDirEntry ()
 * Description   : This routine adds a given Direction Entry to the database.
 *                 Direction Table entries maintain Adjacency pointers which are
 *                 used for building TENT and PATH
 * Input(s)      : pContext  - Pointer to system Context
 *                 pAdjRec   - Pointer to Adjacency Record
 *                 pDirEntry - Pointer to Direction Entry
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjAddDirEntry (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec,
                    tIsisAdjDirEntry * pDirEntry)
{
    tIsisAdjDirEntry   *pTravDir = NULL;
    tIsisAdjDirEntry   *pPrevDir = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjAddDirEntry ()\n"));

    /* Get the next available Direction Index. The only restriction with the
     * Direction Indices is that it should be a value in the interval [1,
     * MAX_DIRECTIONS]. Hence the following routine is expected to return unused
     * Indices if available i.e. if  [1, 2, 3, 5, 16, 78] is a list of Direction
     * Indices in use then the next available index must be '4'.
     */

    IsisAdjGetNextDirIdx (pContext, &(pDirEntry->u4DirIdx));

    pDirEntry->pAdjEntry = pAdjRec;
    pDirEntry->pNext = NULL;

    /* Inserting the record in sorted order of Direction Index 
     */

    pTravDir = pContext->AdjDirTable.pDirEntry;

    if (pTravDir == NULL)
    {
        /* This is the first entry being inserted - Insert at the head directly
         */

        pContext->AdjDirTable.pDirEntry = pDirEntry;
    }
    else if (pDirEntry->u4DirIdx < pTravDir->u4DirIdx)
    {
        /* Entries already exist and the current entry has the minimum
         * most index. Insert at the head
         */

        pDirEntry->pNext = pTravDir;
        pContext->AdjDirTable.pDirEntry = pDirEntry;
    }
    else
    {
        /* Travel the list and insert at appropriate position
         */

        pPrevDir = pTravDir;
        pTravDir = pTravDir->pNext;    /* Already done with the first element */
        while (pTravDir != NULL)
        {
            if (pDirEntry->u4DirIdx < pTravDir->u4DirIdx)
            {
                pDirEntry->pNext = pTravDir;
                pPrevDir->pNext = pDirEntry;
                break;
            }
            pPrevDir = pTravDir;
            pTravDir = pTravDir->pNext;
        }
        if (pTravDir == NULL)
        {
            /* Current entry is the last entry */

            pPrevDir->pNext = pDirEntry;
        }
    }

    ISIS_FORM_IPV4_ADDR (pAdjRec->pCktRec->au1IPV4Addr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);
    ADP_PT ((ISIS_LGST,
             "ADJ <T> : Adjacency - Added to AddDirEntry[%u] - Circuit [%u], Level [%s], IPv4 [%s], IPv6 [%s]\n",
             pDirEntry->u4DirIdx, pAdjRec->pCktRec->u4CktIdx,
             ISIS_GET_ADJ_USAGE_STR (pAdjRec->u1AdjUsage), au1IPv4Addr,
             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                           &(pAdjRec->pCktRec->au1IPV6Addr))));
    pContext->AdjDirTable.u4NumEntries++;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjAddDirEntry ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjDelDirEntry ()
 * Description   : This routine deletes Direction Entry and delinks the
 *                 entry from Adjacency Direction Table
 * Input(s)      : pContext  - Pointer to system Context
 *                 pAdjRec   - Pointer to Adjacency Record
 * Outputs(s)    : pu4DirIdx - Direction Index to Stop the ECT Timer
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisAdjDelDirEntry (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec,
                    UINT4 *pu4DirIdx)
{
    tIsisAdjDirEntry   *pDelDirEntry = NULL;
    tIsisAdjDirEntry   *pPrevDirEntry = NULL;
    tIsisAdjDelDirEntry *pAdjDelDirEntry = NULL;
    tIsisAdjDelDirEntry *pTravDir = NULL;
    tIsisAdjDelDirEntry *pPrevDir = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjDelDirEntry ()\n"));

    /* NOTE: If an adjacency exists then a Direction Entry MUST exist. Hence
     *       this routine always succeeds
     */

    pDelDirEntry = (pContext->AdjDirTable.pDirEntry);

    if ((pDelDirEntry) && (pDelDirEntry->pAdjEntry == pAdjRec))
    {
        /* Deleting the First Entry
         */

        pContext->AdjDirTable.pDirEntry = pDelDirEntry->pNext;
        pDelDirEntry->pNext = NULL;
        pContext->AdjDirTable.u4NumEntries--;
    }
    else if (pDelDirEntry)
    {
        pPrevDirEntry = pDelDirEntry;
        pDelDirEntry = pDelDirEntry->pNext;
        while (pDelDirEntry != NULL)
        {
            if (pDelDirEntry->pAdjEntry == pAdjRec)
            {
                /* Deleting the Dir Entry
                 */

                pPrevDirEntry->pNext = pDelDirEntry->pNext;
                pDelDirEntry->pNext = NULL;
                pDelDirEntry->pAdjEntry = NULL;
                (pContext->AdjDirTable.u4NumEntries)--;
                break;
            }
            pPrevDirEntry = pDelDirEntry;
            pDelDirEntry = pDelDirEntry->pNext;
        }
    }
    if (pDelDirEntry == NULL)
    {
        return;
    }

    /* Calling routine expects the Index of the Direction Entry that
     * is deleted
     */

    *pu4DirIdx = pDelDirEntry->u4DirIdx;
    /* Here we need to store the adjacency record details till the 
     * decision computes the SPT. If any route is added to the IP
     * Path on this adjacency that has to be deleted since the adjacency 
     * is lost. This happens only when the decision runs next time.
     * So we need to store this adjacecny details till decision runs
     */
    pAdjDelDirEntry = (tIsisAdjDelDirEntry *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                              sizeof
                                                              (tIsisAdjDelDirEntry));
    if (pAdjDelDirEntry != NULL)
    {
        MEMSET (pAdjDelDirEntry, 0x00, sizeof (tIsisAdjDelDirEntry));
        pAdjDelDirEntry->u4DirIdx = pDelDirEntry->u4DirIdx;
        pAdjDelDirEntry->u1Status = ISIS_FALSE;
        pAdjDelDirEntry->u1Count = 0;
        pAdjDelDirEntry->u1Level = pAdjRec->u1AdjUsage;
        if (pAdjRec->u1AdjIpAddrType == ISIS_ADDR_IPV4)
        {
            MEMCPY (&pAdjDelDirEntry->au1AdjNbrIpV4Addr,
                    &pAdjRec->AdjNbrIpV4Addr.au1IpAddr, ISIS_MAX_IPV4_ADDR_LEN);
        }
        else if (pAdjRec->u1AdjIpAddrType == ISIS_ADDR_IPV6)
        {
            MEMCPY (&pAdjDelDirEntry->au1AdjNbrIpV6Addr,
                    &pAdjRec->AdjNbrIpV6Addr.au1IpAddr, ISIS_MAX_IPV6_ADDR_LEN);
        }
        else
        {
            MEMCPY (&pAdjDelDirEntry->au1AdjNbrIpV4Addr,
                    &pAdjRec->AdjNbrIpV4Addr.au1IpAddr, ISIS_MAX_IPV4_ADDR_LEN);
            MEMCPY (&pAdjDelDirEntry->au1AdjNbrIpV6Addr,
                    &pAdjRec->AdjNbrIpV6Addr.au1IpAddr, ISIS_MAX_IPV6_ADDR_LEN);
        }
        pPrevDir = NULL;
        pTravDir = pContext->pDelDirEntry;

        if (pTravDir == NULL)
        {
            /* This is the first entry being inserted - Insert at the head directly
             */

            pContext->pDelDirEntry = pAdjDelDirEntry;;
        }
        else if (pAdjDelDirEntry->u4DirIdx < pTravDir->u4DirIdx)
        {
            /* Entries already exist and the current entry has the minimum
             * most index. Insert at the head
             */

            pAdjDelDirEntry->pNext = pTravDir;
            pContext->pDelDirEntry = pAdjDelDirEntry;
        }
        else
        {
            /* Travel the list and insert at appropriate position
             */

            pPrevDir = pTravDir;
            pTravDir = pTravDir->pNext;    /* Already done with the first element */
            while (pTravDir != NULL)
            {
                if (pAdjDelDirEntry->u4DirIdx < pTravDir->u4DirIdx)
                {
                    pAdjDelDirEntry->pNext = pTravDir;
                    pPrevDir->pNext = pAdjDelDirEntry;
                    break;
                }
                pPrevDir = pTravDir;
                pTravDir = pTravDir->pNext;
            }
            if (pTravDir == NULL)
            {
                /* Current entry is the last entry */

                pPrevDir->pNext = pAdjDelDirEntry;
            }
        }
        ISIS_FORM_IPV4_ADDR (pAdjRec->pCktRec->au1IPV4Addr, au1IPv4Addr,
                             ISIS_MAX_IPV4_ADDR_LEN);
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Adjacency - Added to DelDirEntry[%u] - Circuit [%u], Level [%s], IPv4 [%s], IPv6 [%s]\n",
                 *pu4DirIdx, pAdjRec->pCktRec->u4CktIdx,
                 ISIS_GET_ADJ_USAGE_STR (pAdjRec->u1AdjUsage), au1IPv4Addr,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *)
                               &(pAdjRec->pCktRec->au1IPV6Addr))));
    }                            /*klocwork */
    ISIS_MEM_FREE (ISIS_BUF_ADIR, (UINT1 *) pDelDirEntry);
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjDelDirEntry ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjEnqueueIncompCktEvt ()
 * Description   : This routines builds a Incompatible Circuit Event and posts
 *                 it to the Control Module 
 * Input(s)      : pContext - Pointer to System Context
 *                 pCktRec  - Pointer to Circuit Record
 * Output(s)     : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisAdjEnqueueIncompCktEvt (tIsisSysContext * pContext, tIsisCktEntry * pCktRec)
{
    tIsisEvtIncompCkt  *pIncompCktEvt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjEnqueueIncompCktEvt ()\n"));

    pIncompCktEvt
        = (tIsisEvtIncompCkt *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtIncompCkt));

    /* Filling the associated variables in the structure 
     * tIsisEvtIncompCkt
     */

    if (pIncompCktEvt != NULL)
    {
        pIncompCktEvt->u1EvtID = ISIS_EVT_INCOMP_CKT;
        pIncompCktEvt->u1CktLevel = pCktRec->u1CktLevel;
        pIncompCktEvt->u4CktIdx = pCktRec->u4CktIdx;
        pIncompCktEvt->u4CktIfIdx = pCktRec->u4CktIfIdx;
        IsisUtlSendEvent (pContext, (UINT1 *) pIncompCktEvt,
                          sizeof (tIsisEvtIncompCkt));
    }
    else
    {
        PANIC ((ISIS_LGST,
                ISIS_MEM_ALLOC_FAIL " : Incompatible Circuit Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjEnqueueIncompCktEvt ()\n"));
}

/******************************************************************************
 * Function Name : IsisAdjIsValidAdj ()
 * Description   : This routines verifies whether a valid adjacency exist on the *                 given circuit.
 * Input(s)      : pCktRec  - Pointer to Circuit Record
 *                 pu1SNPA  - The SNPA Address of received packet
 *                 u1Level  - The Level
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC INT4
IsisAdjIsValidAdj (tIsisCktEntry * pCktRec, UINT1 *pu1SNPA, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisAdjEntry      *pAdjRec = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjIsValidAdj ()\n"));

    if (pCktRec->u1CktType == ISIS_BC_CKT)
    {
        i4RetVal = IsisAdjCheckLANAdjExist (pCktRec, pu1SNPA, u1Level,
                                            &pAdjRec);
        if ((i4RetVal == ISIS_FAILURE) || (pAdjRec->u1AdjState != ISIS_ADJ_UP))
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <E> : No Adjacency Exists - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));
            i4RetVal = ISIS_FAILURE;
        }
    }
    else if (pCktRec->u1CktType == ISIS_P2P_CKT)
    {
        if ((pCktRec->pAdjEntry == NULL)
            || (pCktRec->pAdjEntry->u1AdjState != ISIS_ADJ_UP))
        {
            ADP_PT ((ISIS_LGST,
                     "ADJ <E> : No Adjacency Exists - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));
            i4RetVal = ISIS_FAILURE;
        }
    }
    else
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <E> : Adjacency Validity Failed - Circuit Index [ %u ]\n",
                 pCktRec->u4CktIdx));
        i4RetVal = ISIS_FAILURE;
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjIsValidAdj ()\n"));
    return (i4RetVal);
}

/*******************************************************************************
 * Function Name : IsisAdjGetCktRecWithLLHandle()
 * Description   : This routine fetches the Circuit Record based on the given 
 *                 Circuit handle which is given by the Lower Layer while
 *                 receiving the packets  
 * Input(s)      : u2LLHandle    - Lower Layer Handle 
 *                                 This is CktIfIndex in the case of ethernet
 * Output(s)     : pCktRec       - Pointer to Circuit Record 
 * Globals       : gpIsisInstances Referred but not Modified
 * Returns       : ISIS_SUCCESS - If the Circuit Record is fetched
 *                 ISIS_FAILURE - Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisAdjGetCktRecWithLLHandle (UINT2 u2LLHandle, tIsisCktEntry ** pCktRec)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4HashIdx = 0;
    tIsisCktEntry      *pTravCkt = NULL;

    ADP_EE ((ISIS_LGST, "ADJ <X> : Entered IsisAdjGetCktRecWithLLHandle()\n"));

    /* The circuit record has to be searched from the global hash table
     * since only active ckt records present in this global hash table
     * it is a way to check whether any packets received on this circuit
     * can be processed or not
     */
    for (u4HashIdx = 0; u4HashIdx < ISIS_MAX_BUCKETS; u4HashIdx++)
    {
        pTravCkt = (tIsisCktEntry *) gIsisCktHashTable.apHashBkts[u4HashIdx];
        while (pTravCkt != NULL)
        {
            if (pTravCkt->u4LLHandle == (UINT4) u2LLHandle)
            {
                *pCktRec = pTravCkt;
                i4RetVal = ISIS_SUCCESS;
                return (i4RetVal);

            }
            else
            {
                pTravCkt = pTravCkt->pNextHashEntry;
            }
        }
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisAdjGetCktRecWithLLHandle()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Funtion Name        :  IsisDotNetToArray
 * Description         :  This routine is to convert the input NET into UNIT1
 *                        value.
 * Input(s)            :  pu1NetId : Pointer to Octet String.
 *                        pu1Val   : Pointer to the Converted Value.
 * Output(s)           :  None.
 * Returns             :  None.
 ******************************************************************************/
VOID
IsisDotNetToArray (UINT1 *pu1NetId, UINT1 *pu1Val)
{
    UINT1               u1Index = 0;
    UINT1              *pu1Temp = pu1NetId;
    UINT1               u1NetIndex = 0;
    UINT4               u4Value = 0;
    UINT1               u1Char = 0;

    if (!(pu1NetId))
    {
        return;
    }

    for (u1Index = 0; u1Index < IsisNetOctetLen (pu1NetId); u1Index++)
    {
        for (u1NetIndex = 0; (u1NetIndex < 2 &&
                              (*pu1Temp != '\0')); u1NetIndex++)
        {
            if (ISXDIGIT (*pu1Temp))
            {
                if (!ISDIGIT (*pu1Temp))
                {
                    u1Char = (UINT1) (10 + ((*pu1Temp) - 'a'));
                }
                else
                {
                    u1Char = *pu1Temp;
                }
                u4Value = (u4Value * 16) + (0x0f & u1Char);
            }
            if (*pu1Temp == '.')
            {
                u1NetIndex--;
            }
            (pu1Temp)++;
        }

        (pu1Val)[u1Index] = (UINT1) u4Value;

        if (*pu1Temp == '.' || *pu1Temp == '-')
        {
            (pu1Temp)++;
        }
    }
}

/******************************************************************************
 * Funtion Name        :  IsisNetOctetLen
 * Description         :  This routine is to find the octet length of given 
 *                        NetId.
 * Input(s)            :  pu1DotStr:NetId.
 * Output(s)           :  Octet Length of NetId.
 * Returns             :  u4Count
 ******************************************************************************/
UINT4
IsisNetOctetLen (UINT1 *pu1DotStr)
{
    UINT1              *pu1Temp = pu1DotStr;
    UINT4               u4Index = 0;
    UINT4               u4Count = 0;

    if (pu1DotStr)
    {
        for (u4Index = 0; u4Index < STRLEN (pu1DotStr); u4Index++)
        {
            if ((*pu1Temp != '.') && (*pu1Temp != '\0'))
            {
                ++u4Count;
            }
            pu1Temp++;
        }
        u4Count = (u4Count / 2);
    }
    return u4Count;
}

/******************************************************************************
 * Funtion Name        :  IsisFormDotStr
 * Description         :  This routine is to form the Dot compliant Netid.
 * Input(s)            :  u4StrLen
 *                        pu1Src
 *                        pu1Dest
 * Output(s)           :  pu1Dest (Dot compliant net id.)
 * Returns             :  None.
 ******************************************************************************/

VOID
IsisFormDotStr (UINT4 u4StrLen, UINT1 *pu1Src, CHAR * pcDest)
{
    UINT1               u1Cnt = 0;
    UINT1               u1Pos = 0;
    UINT1               u1DestSize = (UINT1) ((3 * ISIS_SYS_ID_LEN) + 1);

    if (!(pu1Src))
    {
        return;
    }

    for (u1Cnt = 0, u1Pos = 0;
         (((u1Cnt < (u4StrLen - 1))
           && (ISIS_DOT1_COUNT (u1Cnt, u1Pos) < u1DestSize)));
         u1Cnt++, u1Pos = ISIS_DOT2_COUNT (u1Cnt, u1Pos))
    {
        if ((u1Cnt % 2) == 0)
        {
            SPRINTF (pcDest + u1Pos, "%02x", pu1Src[u1Cnt]);
        }
        else
        {
            SPRINTF (pcDest + u1Pos, "%02x.", pu1Src[u1Cnt]);
        }
    }
    if (u4StrLen == ISIS_LSPID_LEN)
    {
        SPRINTF ((char *) (pcDest + u1Pos), "-%02x", pu1Src[u1Cnt]);
    }
    else
    {
        SPRINTF (pcDest + u1Pos, "%02x", pu1Src[u1Cnt]);
    }
}

/******************************************************************************
 * Funtion Name        :  IsisDisplayAreaInDot
 * Description         :  This routine is to form the Dot compliant Area ID.
 * Input(s)            :  u4StrLen
 *                        pu1Src
 *                        pu1Dest
 * Output(s)           :  pu1Dest (Dot compliant Area id.)
 * Returns             :  None.
 ******************************************************************************/

VOID
IsisDisplayAreaInDot (UINT4 u4StrLen, UINT1 *pu1Src, CHAR * pcDest)
{
    UINT1               u1Cnt = 0;
    UINT1               u1Pos = 0;
    UINT1               u1DestSize = (UINT1) (2 * ((3 * ISIS_SYS_ID_LEN) + 1));
    if (!(pu1Src))
    {
        return;
    }
    for (u1Cnt = 0, u1Pos = 0;
         (((u1Cnt <= (u4StrLen - 1))
           && (ISIS_DOT1_COUNT (u1Cnt, u1Pos) < u1DestSize)));
         u1Pos = ISIS_DOT2_COUNT (u1Cnt, u1Pos), u1Cnt++)
    {
        if ((u4StrLen - 1) == u1Cnt)
        {
            SPRINTF (pcDest + u1Pos, "%02x", pu1Src[u1Cnt]);
        }
        else
        {
            if ((u1Cnt % 2) == 0)
            {
                SPRINTF (pcDest + u1Pos, "%02x.", pu1Src[u1Cnt]);
            }
            else
            {
                SPRINTF (pcDest + u1Pos, "%02x", pu1Src[u1Cnt]);
            }
        }
    }
}
