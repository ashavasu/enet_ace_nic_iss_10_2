/******************************************************************************
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isdecn.c,v 1.34 2017/09/11 13:44:08 siva Exp $
 *
 *   Description: This file contains the Routines related to the 
 *                processing involved in the implementation of 
 *                Shortest Path Algorithm (SPF).
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

static UINT4        gu4NodeCnt;
static UINT4        gu4TestCnt;

/* Prototypes
 */

PRIVATE VOID        IsisDecValAndInsNodeInTent (tIsisSysContext *,
                                                tIsisSPTNode **, tIsisSPTNode *,
                                                UINT1, UINT1, UINT1);

PRIVATE INT4        IsisDecSPTCreate (tIsisSysContext *, UINT1, UINT1, UINT1,
                                      UINT1);

PRIVATE VOID        IsisDecInsertAdjInTent (tIsisSysContext *, tIsisSPTNode *,
                                            UINT1, UINT1, UINT1);

PRIVATE VOID        IsisDecInsertRLAdjInTent (tIsisSysContext *, tIsisSPTNode *,
                                              UINT1, UINT1, UINT1);

PRIVATE VOID        IsisDecAddNodeToTent (tIsisSysContext *, tIsisSPTNode *,
                                          tIsisSPTNode *, UINT1, UINT1);

PRIVATE INT4        IsisDecAddNodeToPath (tIsisSysContext *, tIsisSPTNode *,
                                          tIsisSPTNode *, UINT1, UINT1, UINT1,
                                          UINT1, UINT1);

PRIVATE VOID        IsisDecRemNodeFromTent (tIsisSysContext *, tIsisSPTNode *,
                                            tIsisSPTNode *, UINT1);

PRIVATE VOID        IsisDecGetMCNodeFromTent (tIsisSysContext *, UINT1,
                                              tIsisSPTNode **, UINT1 *);

PRIVATE UINT1       IsisDecIsNodeInPATH (tIsisSysContext *, tIsisSPTNode *,
                                         UINT1, UINT1, tIsisSPTNode **,
                                         UINT1, UINT1);

PRIVATE UINT1       IsisDecIsNodeInHashTbl (tIsisSysContext *, tIsisSPTNode *,
                                            UINT1, UINT1, tIsisSPTNode **,
                                            UINT1 *, UINT1);

PRIVATE VOID        IsisDecUpdateRoute (tIsisSysContext *, UINT1, UINT1, UINT1);

PRIVATE VOID        IsisDecUpdateNearestL2 (tIsisSysContext *, tIsisSPTNode *,
                                            UINT1, UINT1, UINT1 *, UINT1);

PRIVATE VOID        IsisDecSPTComplete (tIsisSysContext *, UINT1, UINT1);

PRIVATE INT4        IsisDecAddSelfAdjs (tIsisSysContext *, UINT1, UINT1, UINT1);

PRIVATE tIsisSPTNode *IsisDecRemExcessPaths (tIsisSysContext *, tIsisSPTNode *,
                                             tIsisSPTNode *, UINT1);

PRIVATE INT4        IsisDecUpdateDefRoute (tIsisSysContext *, UINT1,
                                           tIsisSPTNode *, UINT1, UINT1);
PRIVATE VOID        IsisDecDeleteNearestL2 (tIsisSysContext *, UINT1, UINT1);

/******************************************************************************
 * Function    : IsisDecProcSPFSchTimeOut () 
 * Description : This function, based on the local system type, triggers the
 *               scheduling of Shortest Path Algorithm for Level1 or Level2 or
 *               both
 * Input (s)   : pContext  - Pointer to System Context
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisDecProcSPFSchTimeOut (tIsisSysContext * pContext)
{
    tIsisAdjDelDirEntry *pDelDirEntry = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    INT4                i4RetVal1 = ISIS_SUCCESS;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecProcSPFSchTimeOut ()\n"));

    switch (pContext->SysActuals.u1SysType)
    {
        case ISIS_LEVEL12:

            i4RetVal = IsisDecSchedSPF (pContext, ISIS_LEVEL2);

            /* If the previous Decision got relinquished then don't schedule
             * this SPF run
             */
            if (i4RetVal != ISIS_DECN_RELQ)
            {
                i4RetVal1 = IsisDecSchedSPF (pContext, ISIS_LEVEL1);
            }

            break;

        case ISIS_LEVEL2:

            i4RetVal = IsisDecSchedSPF (pContext, ISIS_LEVEL2);
            break;

        case ISIS_LEVEL1:

            i4RetVal = IsisDecSchedSPF (pContext, ISIS_LEVEL1);
            break;

        default:

            WARNING ((ISIS_LGST,
                      "DEC <W> : SPF Timeout Occured For Erroneous SysType [ %u ]\n",
                      pContext->SysActuals.u1SysType));
            break;
    }

    /* Activate the SPF scheduling timer again
     */

    if ((i4RetVal != ISIS_DECN_RELQ) || (i4RetVal1 != ISIS_DECN_RELQ))
    {
        pContext->SysTimers.SysSPFSchTimer.pContext = pContext;
        IsisTmrStartTimer (&(pContext->SysTimers.SysSPFSchTimer),
                           ISIS_SPF_SCHDL_TIMER,
                           ((UINT4) (pContext->SysActuals.u2MinSPFSchTime)));
    }

    /* since the decision process is over all pContext->pDelDirEntries can be
     * deleted. 
     */
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12) &&
        (i4RetVal == ISIS_SPT_DONE) && (i4RetVal1 == ISIS_SPT_DONE))
    {
        pDelDirEntry = pContext->pDelDirEntry;

        while (pDelDirEntry != NULL)
        {
            pContext->pDelDirEntry = pDelDirEntry->pNext;
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pDelDirEntry);
            pDelDirEntry = pContext->pDelDirEntry;
        }
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecProcSPFSchTimeOut ()\n"));
}

/******************************************************************************
 * Function    : IsisDecSchedSPF () 
 * Description : This function verifies the SPF trigger count and if the current
 *               value of the trigger count is the same as Max SPF Interval,
 *               then the SPF algorithm is scheduled unconditionally. If not,
 *               the routine checks whether the LSP database change count is
 *               within the configured threshold. If so the SPF is scheduled
 * Input (s)   : pContext - Pointer to System Context
 *               u1Level  - Level of the database for which SPF is to be
 *                          scheduled
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 *****************************************************************************/

PUBLIC INT4
IsisDecSchedSPF (tIsisSysContext * pContext, UINT1 u1Level)
{
    tIsisAdjDelDirEntry *pDelDirEntry = NULL;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    UINT1               u1SchSPF = ISIS_FALSE;
    UINT1               u1MetType = ISIS_DEF_MET;
    UINT1               u1SpfTrgCount = 0;
    UINT2               u2Max = 0;
    UINT2               u2CurrTrgCount = 0;
    UINT1               u1MtIndex = 0;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecSchedSPF ()\n"));

    if (pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER)
    {
        if ((gu1IsisGrState == ISIS_GR_SPF_WAIT) ||
            (gu1IsisGrState == ISIS_GR_SPF_L1COMPLETE) ||
            (gu1IsisGrState == ISIS_GR_SPF_L2COMPLETE))
        {
            u1SchSPF = ISIS_TRUE;
        }
        else
        {
            DEP_PT ((ISIS_LGST,
                     "DEC <T> : SPF Schedule Aborted On gu1IsisGrState Value [ %d ]\n",
                     gu1IsisGrState));
            return ISIS_SUCCESS;
        }
    }

    /* u2CurrTrgCount gives the number of changes affected in the database
     * in the last Min SPF interval and u1SPfTrgCount gives the number of
     * times the Min SPF timer fired
     */

    pContext->u4MaxNodes = 0;
    if (u1Level == ISIS_LEVEL1)
    {
        u2CurrTrgCount = pContext->SysL1Info.u2TrgCount;
        u1SpfTrgCount = pContext->SysL1Info.u1SpfTrgCount;
    }
    else
    {
        u2CurrTrgCount = pContext->SysL2Info.u2TrgCount;
        u1SpfTrgCount = pContext->SysL2Info.u1SpfTrgCount;
    }

    /* SPF algorithm is triggered at u2MaxSPFSchTime irrespective of any other
     * conditions. At every u2MinSPFSchTime, a decision is made. whether to
     * schedule the SPF algorithm or not. If the number of Database changes is
     * sufficient then the algorithm is triggered
     *
     * Example: Max SPF Time is 100 secs, and Min SPF time is 10 secs. Every 10
     * secs. we decide whether to run SPF or not. But when Min SPF fires for 10
     * times which is actually 100 secs (Max SPF time), we trigger the SPF
     * algorithn unconditionally
     */

    u2Max = (UINT2) ((pContext->SysActuals.u2MaxSPFSchTime) /
                     (pContext->SysActuals.u2MinSPFSchTime));

    if (u1SpfTrgCount == u2Max)
    {
        /* Max SPF Time has elapsed and hence we can schedule SPF
         * unconditionally
         */
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : SPF Scheduled - Reason [ PERIODIC ], Interval [ %d ]\n",
                 (u2Max * pContext->SysActuals.u2MinSPFSchTime)));

        u1SchSPF = ISIS_TRUE;
    }
    else if ((u2CurrTrgCount > pContext->SysActuals.u2MinLSPMark)
             && (u2CurrTrgCount < pContext->SysActuals.u2MaxLSPMark))
    {
        /* We do not schedule SPF if the Database changes affected in the
         * previous interval is less than the minum or or more than the maximum
         * threshold. Here we are in the threshold region, hence we can schedule
         * SPF
         */

        DEP_PT ((ISIS_LGST,
                 "DEC <T> : SPF Scheduled - Reason [ LSDB CHANGE DETECTED ], Triggered Value [ %d ]\n",
                 (u2CurrTrgCount)));
        u1SchSPF = ISIS_TRUE;
        if (u1Level == ISIS_LEVEL1)
        {
            pContext->SysL1Info.u1IsisDbFullState = ISIS_DB_NOT_SYNC;
        }
        else
        {
            pContext->SysL2Info.u1IsisDbFullState = ISIS_DB_NOT_SYNC;
        }
    }

    /* Run the SPF algorithm for of the supported metric in each topology
     * (either for single topology alone or for MT#0 and MT#2 seperately).
     *
     * NOTE: We do not have separate timers for scheduling SPF for each 
     * topology & each metric.
     */

    if (u1SchSPF == ISIS_TRUE)
    {
        for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
        {
            for (u1MetIdx = 0; u1MetIdx < ISIS_NUM_METRICS; u1MetIdx++)
            {
                u1MetType = IsisUtlGetMetType (pContext, u1MetIdx);

                if (u1MetType != ISIS_MET_INVALID)
                {
                    i4RetVal = IsisDecSPTCreate (pContext, u1Level, u1MetIdx,
                                                 u1MetType, u1MtIndex);
                }
                else
                {
                    continue;
                }

                if (i4RetVal == ISIS_FAILURE)
                {
                    DEP_PT ((ISIS_LGST, "DEC <T> : SPF Returned FAILURE!\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    IsisDecDeAllocSptDB (pContext->Tent, pContext, u1Level,
                                         ISIS_FALSE, u1MtIndex);
                    IsisDecDeAllocSptDB (pContext->OSIPath, pContext, u1Level,
                                         ISIS_FALSE, u1MtIndex);

                    if (u1Level == ISIS_LEVEL1)
                    {
                        IsisDecDeAllocSptDB (pContext->SysL1Info.
                                             ShortPath[u1MtIndex], pContext,
                                             u1Level, ISIS_FALSE, u1MtIndex);
                    }
                    else
                    {
                        IsisDecDeAllocSptDB (pContext->SysL2Info.
                                             ShortPath[u1MtIndex], pContext,
                                             u1Level, ISIS_FALSE, u1MtIndex);
                    }

                    DEP_EE ((ISIS_LGST,
                             "DEC <X> : Exiting IsisDecSchedSPF ()\n"));
                    return ISIS_FAILURE;
                }
                else if (i4RetVal == ISIS_DECN_RELQ)
                {
                    return (ISIS_DECN_RELQ);
                }
            }

            IsisDecSPTComplete (pContext, u1Level, u1MtIndex);

            if (u1Level == ISIS_LEVEL1)
            {
                pContext->SysL1Info.u1SpfTrgCount = 0;
                pContext->SysL1Info.u2TrgCount = 0;
                pContext->SysL1Info.u1IsisDbFullState = ISIS_DB_SYNC;
            }
            else
            {
                pContext->SysL2Info.u1SpfTrgCount = 0;
                pContext->SysL2Info.u2TrgCount = 0;
                pContext->SysL2Info.u1IsisDbFullState = ISIS_DB_SYNC;
            }

            /* If multi-topology is not supported, it is
             * sufficient for run SPF for single-topology alone */
            if (pContext->u1IsisMTSupport == ISIS_FALSE)
            {
                break;
            }
        }

#ifdef ISIS_FT_ENABLED
        ISIS_FLTR_SCH_SPF_LSU (pContext, u1Level);
#endif
        if (pContext->SysActuals.u1SysType != ISIS_LEVEL12)
        {
            pDelDirEntry = pContext->pDelDirEntry;

            while (pDelDirEntry != NULL)
            {
                pContext->pDelDirEntry = pDelDirEntry->pNext;
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pDelDirEntry);
                pDelDirEntry = pContext->pDelDirEntry;
            }
        }

        return ISIS_SPT_DONE;
    }
    else
    {
        /* SPF is not scheduled for whatever reasons. Reset the u2Trgcount since
         * this count is valid only for one interval. Update the u1SpfTrgCount
         * since this must be maintained cummulatively till Max SPF interval.
         */

        if (u1Level == ISIS_LEVEL1)
        {
            pContext->SysL1Info.u1SpfTrgCount += 1;
            pContext->SysL1Info.u2TrgCount = 0;
        }
        else
        {
            pContext->SysL2Info.u1SpfTrgCount += 1;
            pContext->SysL2Info.u2TrgCount = 0;
        }
    }
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecSchedSPF ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisDecAddSelfAdjs () 
 * Description : This function adds Self Adjacencies as well as Adjacencies
 *               that are listed in PseudoNode LSPs, but which does not exist in
 *               Local System's Adjacency database, into TENT
 * Input (s)   : pContext  - Pointer to System Context
 *               u1Level   - Level for which the SPF runs. 
 *               u1MetIdx  - Metric Index which specifies the index that 
 *                           holds values that are to be to updated in the 
 *                           arrays used in the SPT nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, If addition of Self Adjacencies is successful 
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PRIVATE INT4
IsisDecAddSelfAdjs (tIsisSysContext * pContext, UINT1 u1Level, UINT1 u1MetIdx,
                    UINT1 u1MtIndex)
{
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSPTNode       *pSptNode = NULL;
    tIsisSPTNode       *pSptList = NULL;
    tIsisSPTNode       *pNextNode = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecAddSelfAdjs ()\n"));

    if (u1MetIdx >= ISIS_NUM_METRICS)    /*klocwork */
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FAILURE;
    }

    i4RetVal = IsisAdjGetSelfAdjs (pContext, u1Level, u1MetIdx,
                                   &pSptList, u1MtIndex);

    if (i4RetVal == ISIS_FAILURE)
    {
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : Failed To Get Self Adjacencies - Level [%s]\n",
                 ISIS_GET_SYS_TYPE_STR (u1Level)));
        DETAIL_FAIL (ISIS_CR_MODULE);

        while (pSptList != NULL)
        {
            /* Free the Nodes added to pSptList if any
             */

            pNextNode = pSptList->pNext;
            pSptList->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptList);
            pSptList = pNextNode;
        }

        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecAddSelfAdjs ()\n"));
        return i4RetVal;
    }

    /* IsisDecInsertAdjInTent () does the removal of excess adjacencies 
     * and forming the Equal Cost Multi-Paths, if required. 
     * Refer C.1.4 3), 4), 5), 6), 7) of step 0 of the SPF algorithm in  
     * RFC 1195 
     */

    while (pSptList != NULL)
    {
        /* Delink Node from the AdjList
         */

        pSptNode = pSptList;
        pSptList = pSptList->pNext;
        pSptNode->pNext = NULL;

        /* Add the Node to the TENT
         */
        if ((pContext->SysActuals.bSysL2ToL1Leak == ISIS_TRUE) &&
            (ISIS_ADJ_SYS_TYPE (pSptNode->au2Metric[u1MetIdx]) ==
             ISIS_IP_END_SYSTEM))
        {
            IsisDecInsertRLAdjInTent (pContext, pSptNode, u1MetIdx, u1Level,
                                      u1MtIndex);
        }
        else
        {
            IsisDecInsertAdjInTent (pContext, pSptNode, u1MetIdx, u1Level,
                                    u1MtIndex);
        }
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecAddSelfAdjs ()\n"));

    /* Adding Pseudonode LSPs is not mandatory, hence even if we fail to
     * get the Pseudonode LSPs we can proceed with the decision process
     */

    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisDecUpdateNearestL2 ()
 * Description : This routine computes the nearest L2 system ID for the local 
 *               system
 * Input (s)   : pContext      - Pointer to System Context
 *               pSptNode      - Pointer to the node whose system ID is taken as
 *                               the Nearest L2 System if the System is attached
 *                               to the other area
 *               u1MetIdx      - Metric Index which specifies the index that 
 *                               holds values that are to be to updated in the 
 *                               arrays used in the SPT nodes
 *               u1MetType     - Metric Type whose corresponding database is
 *                               being manipulated
 *               pu1NearL2Flag - A flag indicating whether Nearest L2
 *                               computation is necessary
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisDecUpdateNearestL2 (tIsisSysContext * pContext, tIsisSPTNode * pSptNode,
                        UINT1 u1MetIdx, UINT1 u1MetType,
                        UINT1 *pu1NearL2Flag, UINT1 u1MtIndex)
{
    UINT1               u1LSPFlags = 0;
    UINT1               u1L12Flag = ISIS_FALSE;
    UINT2               u2Metric = 0;
    tIsisSPTNode       *pNewSPTNode = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    CHAR                acOldLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UNUSED_PARAM (pu1NearL2Flag);
    UNUSED_PARAM (u1MetType);

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecUpdateNearestL2Sys ()\n"));

    /* During Level1 Decision process, we have to get the Nearest Attached
     * L2 system. If the system is already computed indictaed by 'pu1NearL2Flag'
     * being ISIS_TRUE, then we can skip this step. Otherwise we have to
     * compute the Nearest L2
     */
    if (u1MetIdx != ISIS_DEF_MET)    /*klocwork */
    {
        return;
    }

    if (pContext->SysActuals.u1SysType == ISIS_LEVEL12)
    {
        if (u1MtIndex != 0)
        {
            u1LSPFlags = pContext->SelfLSP.au1L1MTLspFlag[u1MtIndex];
            if ((u1LSPFlags & ISIS_MT_LSP_ATT_MASK) == ISIS_MT_LSP_ATT_MASK)
            {
                u1L12Flag = ISIS_TRUE;
                DETAIL_INFO (ISIS_DEC_MODULE, (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                               "DEC <T> : Self IS(L12) is attached, MT[ %d ]\n",
                                               u1MtIndex));
            }
        }
        else
        {
            u1LSPFlags = pContext->SelfLSP.u1L1LSPFlags;

            /* The local system is an L12 system and if it is attached for
             * a particular metric then the local system becomes the Nearest L2 for
             * that particular Metric. Whether the local system is attached or not
             * is decided during the Level2 run of the decision process
             */

            if ((u1LSPFlags & ISIS_FLG_ATT_DEF) == ISIS_FLG_ATT_DEF)
            {
                /* Local system is the Nearest L2 for Default Metric. Update
                 * the Local System ID as the Nearest L2 for the Default
                 * Metric
                 */

                DETAIL_INFO (ISIS_DEC_MODULE, (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                               "DEC <T> : Self IS (L12) is attached, MT[ %d ]\n",
                                               u1MtIndex));
                u1L12Flag = ISIS_TRUE;

            }
        }
    }

    if (u1L12Flag != ISIS_TRUE)
    {
        /* Check for the System type to be L12 and set the NearestL2
         * SysID in pContext
         */

        u2Metric = pSptNode->au2Metric[u1MetIdx];

        if (((u2Metric & ISIS_SPT_L1_SYS_MASK) == ISIS_SPT_L1_SYS_MASK)
            && (u2Metric & ISIS_SPT_ATT_FLAG))
        {
            /*att bit set - so check the AddNL2Nodes  list for the specific MT */

            ISIS_FORM_STR (ISIS_LSPID_LEN, pSptNode->au1DestID, acLSPId);
            DEP_PT ((ISIS_LGST,
                     "DEC <T> : Processing ATT bit in LSP ID [ %s ] MT [ %d ]\n",
                     acLSPId, u1MtIndex));

            pNewSPTNode =
                (tIsisSPTNode *) ISIS_MEM_ALLOC (ISIS_BUF_SPTN,
                                                 sizeof (tIsisSPTNode));
            if (pNewSPTNode == NULL)
            {
                PANIC ((ISIS_LGST, "DEC <P> : No Memory For SPT Node\n"));
                DEP_EE ((ISIS_LGST,
                         "DEC <X> : Exiting IsisDecUpdateNearestL2Sys ()\n"));
                return;
            }
            else
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : New SPT Node\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
            MEMCPY (pNewSPTNode, pSptNode, sizeof (tIsisSPTNode));
            pNewSPTNode->pNext = NULL;
            pNewSPTNode->u1DefAddFlag = ISIS_TRUE;
            if ((pContext->pAddNL2Nodes[u1MtIndex] != NULL)
                && (pSptNode->u4MetricVal >
                    pContext->pAddNL2Nodes[u1MtIndex]->u4MetricVal))
            {
                ISIS_FORM_STR (ISIS_LSPID_LEN,
                               pContext->pAddNL2Nodes[u1MtIndex]->au1DestID,
                               acOldLSPId);
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : Ignoring ATT bit in LSP ID [ %s ] MT [ %d ] Metric [%d ]"
                         " as a better exit point"
                         " is available via [ %s ] Metric [ %d ]\n", acLSPId,
                         u1MtIndex, pSptNode->u4MetricVal, acOldLSPId,
                         pContext->pAddNL2Nodes[u1MtIndex]->u4MetricVal));
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pNewSPTNode);
                DEP_EE ((ISIS_LGST,
                         "DEC <X> : Exiting IsisDecUpdateNearestL2Sys ()\n"));
                return;
            }

            if (pContext->pAddNL2Nodes[u1MtIndex] != NULL)
            {
                pNewSPTNode->pNext = pContext->pAddNL2Nodes[u1MtIndex];
            }
            pContext->pAddNL2Nodes[u1MtIndex] = pNewSPTNode;
        }
    }
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecUpdateNearestL2Sys ()\n"));
}

/*******************************************************************************
 * Function    : IsisDecDeleteNearestL2 ()
 * Description : This routine computes the nearest L2 system ID for the local 
 *               system
 * Input (s)   : pContext      - Pointer to System Context
 *               u1MetIdx      - Metric Index which specifies the index that 
 *                               holds values that are to be to updated in the 
 *                               arrays used in the SPT nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisDecDeleteNearestL2 (tIsisSysContext * pContext, UINT1 u1MetIdx,
                        UINT1 u1MtIndex)
{
    tIsisSPTNode       *pSPTNode = NULL;
    tIsisSPTNode       *pNextNode = NULL;
    tIsisSPTNode       *pCmpSPTNode = NULL;
    UINT1               u1DelFlag = ISIS_TRUE;
    INT4                i4RetVal = ISIS_FAILURE;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecDeleteNearestL2 ()\n"));

    if (u1MetIdx != ISIS_DEF_MET)    /*klocwork */
    {
        return;
    }

    /*Delete the Previous iteration's Default routes */
    pSPTNode = pContext->pDelNL2Nodes[u1MtIndex];

    while (pSPTNode != NULL)
    {
        pNextNode = pSPTNode->pNext;
        pCmpSPTNode = pContext->pAddNL2Nodes[u1MtIndex];
        while (pCmpSPTNode != NULL)
        {
            if ((pCmpSPTNode->u4MetricVal == pSPTNode->u4MetricVal)
                && (pCmpSPTNode->au4DirIdx[0] == pSPTNode->au4DirIdx[0]))
            {
                if (ISIS_DEF_RE_ADD != pCmpSPTNode->u1DefAddFlag)
                {
                    pCmpSPTNode->u1DefAddFlag = ISIS_FALSE;
                }
                else
                {
                    pCmpSPTNode->u1DefAddFlag = ISIS_TRUE;
                }
                u1DelFlag = ISIS_FALSE;
                break;

            }
            pCmpSPTNode = pCmpSPTNode->pNext;
        }
        if (u1DelFlag == ISIS_TRUE)
        {
            IsisDecUpdateDefRoute (pContext, ISIS_SPT_ROUTE_DELETE, pSPTNode,
                                   u1MetIdx, u1MtIndex);
            DEP_PT ((ISIS_LGST,
                     "DEC <T> : Default Route Update(DELETE) Relayed To RTM\n"));
        }

        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSPTNode);
        pSPTNode = pNextNode;
        u1DelFlag = ISIS_TRUE;
    }

    /*Add the Current iteration's Default routes */
    pSPTNode = pContext->pAddNL2Nodes[u1MtIndex];

    DEP_PT ((ISIS_LGST,
             "DEC <T> : Area Exit Points Identified For MT [%d] SPF:",
             u1MtIndex));
    if (pSPTNode == NULL)
    {
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : No exit points found during route calculation\n"));
    }
    while (pSPTNode != NULL)
    {
        ISIS_DEC_PRINT_ID (pSPTNode->au1DestID, (UINT1) ISIS_SYS_ID_LEN,
                           "\n\t\t\t", ISIS_OCTET_STRING);
        pNextNode = pSPTNode->pNext;
        if (pSPTNode->u1DefAddFlag == ISIS_TRUE)
        {
            DEP_PT ((ISIS_LGST,
                     "\t\t\t(Default Route Addition Relayed To RTM)\n"));
            i4RetVal =
                IsisDecUpdateDefRoute (pContext, ISIS_SPT_ROUTE_ADD, pSPTNode,
                                       u1MetIdx, u1MtIndex);
            if (i4RetVal == ISIS_FAILURE)
            {
                pSPTNode->u1DefAddFlag = ISIS_DEF_RE_ADD;
            }
        }
        pSPTNode = pNextNode;
    }
    pContext->pDelNL2Nodes[u1MtIndex] = pContext->pAddNL2Nodes[u1MtIndex];
    pContext->pAddNL2Nodes[u1MtIndex] = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecUpdateNearestL2Sys ()\n"));
}

/******************************************************************************
 * Function    : IsisDecUpdateDefRoute ()
 * Description : This function forms the RouteInfo structure to update the
 *               Routing Table
 * Input (s)   : pContext  - Pointer to System Context
 *               u1BitMask - Type of the Route update
 *                             - ISIS_SPT_ROUTE_ADD 
 *                                     New Route to be added
 *                             - ISIS_SPT_ROUTE_DELETE 
 *                                     Existing Route to be deleted
 *                             - ISIS_SPT_ROUTE_MET_CHG
 *                                     Metric Change in the Existing Route
 *                             - ISIS_SPT_ROUTE_IFIDX_CHG
 *                                     Interface Idx Change in the Existing
 *                                     Route
 *               pNode     - Pointer to the SPTNode whose Routing 
 *                           information is to be updated
 *               u1MetIdx  - Metric Index which specifies the index that 
 *                           holds values that are to be to updated in the 
 *                           arrays used in the SPT nodes
 * Output (s)  : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *****************************************************************************/

PRIVATE INT4
IsisDecUpdateDefRoute (tIsisSysContext * pContext, UINT1 u1BitMask,
                       tIsisSPTNode * pNode, UINT1 u1MetIdx, UINT1 u1MtIndex)
{
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisRouteInfo      RouteInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4RetValue = NETIPV4_FAILURE;
    UINT4               u4CktIfIndex = 0;
    UINT1               u1MtSupport = ISIS_FALSE;
    UINT1               u1AdjMtId = 0;
    UINT1               u1IfMtId = 0;
    tIsisAdjDelDirEntry *pDelDirEntry = NULL;
    UINT1               au1NullAddr[ISIS_MAX_IPV6_ADDR_LEN];
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    UINT1               au1IPv6Addr[ISIS_MAX_IPV6_ADDR_LEN];
    UINT1               au1NullIP[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1NullAddr, 0, ISIS_MAX_IPV6_ADDR_LEN);
    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (au1IPv6Addr, 0, sizeof (au1IPv6Addr));
    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecUpdateDefRoute () \n"));

    MEMSET (&RouteInfo, 0, sizeof (tIsisRouteInfo));
    if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER)
        && (gu1IsisGrState != ISIS_GR_SPF_WAIT))
    {
        return ISIS_FAILURE;
    }

    if (u1MetIdx >= ISIS_NUM_METRICS)
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FAILURE;
    }

    u1MtSupport = pContext->u1IsisMTSupport;

    RouteInfo.DestIpAddr.u1PrefixLen = 0;

    RouteInfo.u1MetricType = (UINT1) IsisUtlGetMetType (pContext, u1MetIdx);
    RouteInfo.u1BitMask = u1BitMask;
    pAdjEntry = IsisAdjGetAdjRecFromDirIdx (pContext,
                                            pNode->au4DirIdx[u1MetIdx]);

    if (u1BitMask == ISIS_SPT_ROUTE_ADD)
    {
        if (pAdjEntry == NULL)
        {
            DEP_PT ((ISIS_LGST,
                     "DEC <T> : Adjacency Record Does Not Exist - default route add failed!"
                     " Direction Index [ %u ]\n", pNode->au4DirIdx[u1MetIdx]));
            DETAIL_FAIL (ISIS_CR_MODULE);
            return ISIS_FAILURE;
        }
        u1AdjMtId = pAdjEntry->u1AdjMTId;
        u1IfMtId = pAdjEntry->pCktRec->u1IfMTId;

        /* Installation of a common default route (for both IPv4 and IPv6) in RTM
         * is done only during Single topology SPF.
         * In case of multi-topology,
         * the corresponding RTMs are updated. */

        if (((u1MtSupport == ISIS_FALSE) &&
             (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPVX)))
        {
            /*IPv4 Route Updation */
            MEMSET (RouteInfo.DestIpAddr.au1IpAddr, 0, ISIS_MAX_IPV4_ADDR_LEN);
            RouteInfo.DestIpAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
            RouteInfo.DestIpAddr.u1AddrType = ISIS_ADDR_IPV4;
            MEMCPY (RouteInfo.au1IPAddr,
                    pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
            i4RetValue = NetIpv4GetPortFromIfIndex (u4CktIfIndex,
                                                    &RouteInfo.u4CktIfIndex);

            RouteInfo.u4MetricVal = pNode->u4MetricVal;
            RouteInfo.u1Level = ISIS_LEVEL1;
            i4RetVal = IsisRtmiRtmTxRtUpdate (pContext, &RouteInfo);

            if (i4RetVal == ISIS_FAILURE)
            {
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : Failed To Add IPv4 Default Route MT [%d]\n",
                         u1MtIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
            else
            {
                if ((MEMCMP
                     (pAdjEntry->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
                      ISIS_MAX_IPV4_ADDR_LEN)) != 0)
                {
                    ISIS_FORM_IPV4_ADDR (pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                                         au1IPv4Addr, ISIS_MAX_IPV4_ADDR_LEN);
                    DETAIL_INFO (ISIS_DEC_MODULE,
                                 (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                  "DEC <T> : IPv4 Default Route Added MT [%d]"
                                  " - Next Hop [ %s ]\n", u1MtIndex,
                                  au1IPv4Addr));
                }
            }

            /*IPv6 Route Updation */
            MEMCPY (RouteInfo.au1IPAddr,
                    pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                    ISIS_MAX_IPV6_ADDR_LEN);
            RouteInfo.DestIpAddr.u1AddrType = ISIS_ADDR_IPV6;
            RouteInfo.u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
            MEMSET (RouteInfo.DestIpAddr.au1IpAddr, 0, ISIS_MAX_IPV6_ADDR_LEN);
            RouteInfo.DestIpAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
            NetIpv6GetIfInfo (pAdjEntry->pCktRec->u4CktIfIdx, &NetIpv6IfInfo);
            RouteInfo.u4CktIfIndex = NetIpv6IfInfo.u4IpPort;
            RouteInfo.u4MetricVal = (pNode->u4MetricVal);
            i4RetVal = IsisRtmiRtmTxRtUpdate (pContext, &RouteInfo);
            if (i4RetVal == ISIS_FAILURE)
            {
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : Failed To Add IPv6 Default Route MT [%d]\n",
                         u1MtIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
            else
            {
                if ((MEMCMP
                     (au1IPv6Addr, NetIpv6IfInfo.Ip6Addr.u1_addr,
                      ISIS_MAX_IPV6_ADDR_LEN)) != 0)
                {
                    DETAIL_INFO (ISIS_DEC_MODULE,
                                 (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                  "DEC <T> : IPv6 Default Route Added MT [%d]"
                                  " - Next Hop [ %s ]\n", u1MtIndex,
                                  Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                                &(NetIpv6IfInfo.Ip6Addr.
                                                  u1_addr))));
                }
            }
        }
        else if (((u1MtSupport == ISIS_FALSE) &&
                  (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPV4)) ||
                 ((u1MtSupport == ISIS_TRUE) &&
                  (u1MtIndex == ISIS_MT0_INDEX) &&
                  (u1IfMtId & ISIS_MT0_BITMAP) &&
                  (u1AdjMtId & ISIS_MT0_BITMAP)))
        {
            /*IPv4 Route Updation */
            MEMSET (RouteInfo.DestIpAddr.au1IpAddr, 0, ISIS_MAX_IPV4_ADDR_LEN);
            RouteInfo.DestIpAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
            RouteInfo.DestIpAddr.u1AddrType = ISIS_ADDR_IPV4;
            MEMCPY (RouteInfo.au1IPAddr,
                    pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
            i4RetValue = NetIpv4GetPortFromIfIndex (u4CktIfIndex,
                                                    &RouteInfo.u4CktIfIndex);
            RouteInfo.u4MetricVal = pNode->u4MetricVal;
            RouteInfo.u1Level = ISIS_LEVEL1;
            i4RetVal = IsisRtmiRtmTxRtUpdate (pContext, &RouteInfo);
            if (i4RetVal == ISIS_FAILURE)
            {
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : Failed To Add IPv4 Default Route MT [%d]\n",
                         u1MtIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
            else
            {
                if ((MEMCMP
                     (pAdjEntry->AdjNbrIpV4Addr.au1IpAddr, au1NullIP,
                      ISIS_MAX_IPV4_ADDR_LEN)) != 0)
                {
                    ISIS_FORM_IPV4_ADDR (pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                                         au1IPv4Addr, ISIS_MAX_IPV4_ADDR_LEN);
                    DETAIL_INFO (ISIS_DEC_MODULE,
                                 (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                  "DEC <T> : IPv4 Default Route Added MT [%d]"
                                  " - Next Hop [ %s ]\n", u1MtIndex,
                                  au1IPv4Addr));
                }
            }
        }
        else if (((u1MtSupport == ISIS_FALSE) &&
                  (pAdjEntry->u1AdjIpAddrType == ISIS_ADDR_IPV6)) ||
                 ((u1MtSupport == ISIS_TRUE) &&
                  (u1MtIndex == ISIS_MT2_INDEX) &&
                  (u1IfMtId & ISIS_MT2_BITMAP) &&
                  (u1AdjMtId & ISIS_MT2_BITMAP)))
        {
            if (MEMCMP
                (au1NullAddr, pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                 ISIS_MAX_IPV6_ADDR_LEN) == 0)
            {
                return ISIS_FAILURE;
            }

            /*IPv6 Route Updation */
            MEMCPY (RouteInfo.au1IPAddr,
                    pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                    ISIS_MAX_IPV6_ADDR_LEN);
            MEMSET (RouteInfo.DestIpAddr.au1IpAddr, 0, ISIS_MAX_IPV6_ADDR_LEN);
            RouteInfo.DestIpAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
            RouteInfo.DestIpAddr.u1AddrType = ISIS_ADDR_IPV6;
            NetIpv6GetIfInfo (pAdjEntry->pCktRec->u4CktIfIdx, &NetIpv6IfInfo);

            RouteInfo.u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
            RouteInfo.u4MetricVal = (pNode->u4MetricVal);
            RouteInfo.u1Level = ISIS_LEVEL1;
            i4RetVal = IsisRtmiRtmTxRtUpdate (pContext, &RouteInfo);
            if (i4RetVal == ISIS_FAILURE)
            {
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : Failed To Add IPv6 Default Route MT [%d]\n",
                         u1MtIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
            else
            {
                if ((MEMCMP
                     (au1IPv6Addr, NetIpv6IfInfo.Ip6Addr.u1_addr,
                      ISIS_MAX_IPV6_ADDR_LEN)) != 0)
                {
                    DETAIL_INFO (ISIS_DEC_MODULE,
                                 (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                  "DEC <T> : IPv6 Default Route Added MT [%d]"
                                  " - Next Hop [ %s ]\n", u1MtIndex,
                                  Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                                &(NetIpv6IfInfo.Ip6Addr.
                                                  u1_addr))));
                }
            }
        }
    }
    else if (u1BitMask == ISIS_SPT_ROUTE_DELETE)
    {
        if ((u1MtSupport == ISIS_FALSE) ||
            ((u1MtSupport == ISIS_TRUE) && (u1MtIndex == ISIS_MT0_INDEX)))
        {
            if (pAdjEntry != NULL)
            {
                MEMCPY (RouteInfo.au1IPAddr,
                        pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                        ISIS_MAX_IPV4_ADDR_LEN);
                u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
                i4RetValue = NetIpv4GetPortFromIfIndex (u4CktIfIndex,
                                                        &RouteInfo.
                                                        u4CktIfIndex);
            }
            else
            {
                pDelDirEntry = pContext->pDelDirEntry;

                while (pDelDirEntry != NULL)
                {
                    if (pDelDirEntry->u4DirIdx == pNode->au4DirIdx[u1MetIdx])
                    {
                        MEMCPY (RouteInfo.au1IPAddr,
                                pDelDirEntry->au1AdjNbrIpV4Addr,
                                ISIS_MAX_IPV4_ADDR_LEN);
                        break;
                    }
                    pDelDirEntry = pDelDirEntry->pNext;
                }
            }

            MEMSET (RouteInfo.DestIpAddr.au1IpAddr, 0, ISIS_MAX_IPV4_ADDR_LEN);
            RouteInfo.DestIpAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
            RouteInfo.DestIpAddr.u1AddrType = ISIS_ADDR_IPV4;
            RouteInfo.DestIpAddr.u1PrefixLen = 0;
            RouteInfo.u4MetricVal = pNode->u4MetricVal;
            RouteInfo.u1Level = ISIS_LEVEL1;
            i4RetVal = IsisRtmiRtmTxRtUpdate (pContext, &RouteInfo);
            if (i4RetVal == ISIS_FAILURE)
            {
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : Failed To Delete IPv4 Default Route MT [%d]\n",
                         u1MtIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
            else
            {
                DETAIL_INFO (ISIS_DEC_MODULE,
                             (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                              "DEC <T> : "
                              "IPv4 Default Route Deleted - MT[%d]\n",
                              u1MtIndex));
            }
        }
        if ((u1MtSupport == ISIS_FALSE) ||
            ((u1MtSupport == ISIS_TRUE) && (u1MtIndex == ISIS_MT2_INDEX)))
        {

            if (pAdjEntry != NULL)
            {
                MEMCPY (RouteInfo.au1IPAddr,
                        pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                        ISIS_MAX_IPV6_ADDR_LEN);
                RouteInfo.u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
            }
            else
            {
                pDelDirEntry = pContext->pDelDirEntry;

                while (pDelDirEntry != NULL)
                {
                    if (pDelDirEntry->u4DirIdx == pNode->au4DirIdx[u1MetIdx])
                    {
                        MEMCPY (RouteInfo.au1IPAddr,
                                pDelDirEntry->au1AdjNbrIpV6Addr,
                                ISIS_MAX_IPV6_ADDR_LEN);
                        break;
                    }
                    pDelDirEntry = pDelDirEntry->pNext;
                }
            }

            MEMSET (RouteInfo.DestIpAddr.au1IpAddr, 0, ISIS_MAX_IPV6_ADDR_LEN);
            RouteInfo.DestIpAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
            RouteInfo.DestIpAddr.u1AddrType = ISIS_ADDR_IPV6;
            RouteInfo.DestIpAddr.u1PrefixLen = 0;
            RouteInfo.u4MetricVal = (pNode->u4MetricVal);
            RouteInfo.u1Level = ISIS_LEVEL1;
            i4RetVal = IsisRtmiRtmTxRtUpdate (pContext, &RouteInfo);
            if (i4RetVal == ISIS_FAILURE)
            {
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : Failed To Delete IPv6 Default Route MT [%d]\n",
                         u1MtIndex));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }
            else
            {
                DETAIL_INFO (ISIS_DEC_MODULE,
                             (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                              "DEC <T> : "
                              "IPv6 Default Route Deleted - MT[%d]\n",
                              u1MtIndex));
            }
        }
    }
    else
    {
        /* The computed route is no more valid
         * so don't update in the routing table
         */
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : Route Update Is Not Valid (bitmask [%d]) - RTM Not Updated\n",
                 u1BitMask));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FAILURE;
    }
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecUpdateDefRoute () \n"));
    UNUSED_PARAM (i4RetValue);
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisDecValAndInsNodeInTent () 
 * Description : This routine validates the given list of SPT nodes for
 *                  -- Metric Value computed exceeding Max Path Metric, in which
 *                     case the Final Metric Value is adjusted to be the same as
 *                     ISIS_MAX_PATH_MET
 *                  -- 2 way connectivity i.e. each reported adjacent system in
 *                     the SPT list is verified for the Minimum Cost node's
 *                     system ID inclusion in the respective LSPs
 *               and Inserts the Nodes from SPT list into the TENT. This routine
 *               also computes the nearest L2 system ID for the local system if
 *               necessary. 
 * Input (s)   : pContext      - Pointer to System Context
 *               pSptList      - Pointer to SPT List including Adjacencies which
 *                               are supposed to be inserted into TENT
 *               pMCNode       - Pointer to Minimum Cost Node whose adjacencies
 *                               are being processed
 *               u1Lvl         - Level of the database being manipulated
 *               u1MetIdx      - Metric Index which specifies the index that 
 *                               holds values that are to be to updated in the 
 *                               arrays used in the SPT nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisDecValAndInsNodeInTent (tIsisSysContext * pContext,
                            tIsisSPTNode ** pSptList, tIsisSPTNode * pMCNode,
                            UINT1 u1Lvl, UINT1 u1MetIdx, UINT1 u1MtIndex)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Result = ISIS_SUCCESS;
    UINT4               u4MetVal = 0;
    UINT4               u4SptNodeMetric = 0;
    UINT4               u4MCNodeMetric = 0;
    tIsisSPTNode       *pSptNode = NULL;

    if (u1MetIdx >= ISIS_NUM_METRICS)    /*klocwork */
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return;
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecValAndInsNodeInTent ()\n"));

    /* Scan through the SPT list and retrieve all the adjacencies reported,
     * adjust the Path Metric Value appropriately along with the directions.
     */

    while ((*pSptList) != NULL)
    {
        pSptNode = *pSptList;
        *pSptList = (*pSptList)->pNext;
        pSptNode->pNext = NULL;

        /* Metric listed in the SPT node must be the PATH metric. since all
         * these adjacencies are reachable from MC node, we may have to add the
         * Metric Value from the MC node to the current link metrics present in
         * the SPT node
         */
        u4MCNodeMetric = pMCNode->u4MetricVal;
        u4SptNodeMetric = pSptNode->u4MetricVal;
        u4MetVal = u4SptNodeMetric + u4MCNodeMetric;
        if (pSptNode->u1AddrType == ISIS_ADDR_IPV4)
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                if (u4MetVal >= ISIS_MAX_METRIC_MT)
                {
                    u4MetVal = ISIS_MAX_METRIC_MT;
                }
            }
            else
            {
                if (u4MetVal >= ISIS_MAX_PATH_IPV4_MET)
                {
                    u4MetVal = ISIS_MAX_PATH_IPV4_MET;
                }
            }
        }
        else
        {
            if (u4MetVal >= ISIS_MAX_PATH_IPV6_MET)
            {
                u4MetVal = ISIS_MAX_PATH_IPV6_MET;
            }
        }

        pSptNode->u4MetricVal = u4MetVal;

        /* The direction to reach the system should be the same as the MC node
         * since we have got this adjacency information from MC nodes LSPs.
         */

        pSptNode->au4DirIdx[u1MetIdx] = pMCNode->au4DirIdx[u1MetIdx];

        u1Result = (UINT1) (ISIS_ADJ_SYS_TYPE (pSptNode->au2Metric[u1MetIdx]));

        if (u1Result != ISIS_IP_END_SYSTEM)
        {
            /* For OSI systems, we have to make sure that all the adjacent
             * systems reported by MC nodes in LSPs, must list the MC node as
             * adjacent. Only then we have 2-way connectivity and only such
             * systems LSPs can be considered.
             */

            i4RetVal = IsisUpdIsTwoWayConnected (pContext, u1Lvl,
                                                 pSptNode->au1DestID,
                                                 pMCNode->au1DestID, u1MtIndex);
            if (i4RetVal != ISIS_SUCCESS)
            {
                /* We can ignore the systems which are not 2-way connected
                 */
                ISIS_DBG_PRINT_ID (pSptNode->au1DestID, (UINT1) ISIS_SYS_ID_LEN,
                                   "\tSPT Node SysID\t", ISIS_OCTET_STRING);
                ISIS_DBG_PRINT_ID (pMCNode->au1DestID, (UINT1) ISIS_SYS_ID_LEN,
                                   "\tMC Node SysID\t", ISIS_OCTET_STRING);
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : 2-Way Connectivity Failed During MT [%d] SPF Computation\n",
                         u1MtIndex));
                ISIS_DEC_PRINT_ID (pMCNode->au1DestID,
                                   (UINT1) (ISIS_SYS_ID_LEN + 1),
                                   "\tMC Node:\t", ISIS_OCTET_STRING);
                ISIS_DEC_PRINT_ID (pSptNode->au1DestID,
                                   (UINT1) (ISIS_SYS_ID_LEN + 1),
                                   "\tMC Node Not Present In The LSP Of SPT Node:\t",
                                   ISIS_OCTET_STRING);
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
                continue;
            }
        }

        /* Since two way connectivity exists between the Minimum cost node and
         * the 'pSptNode', this node can be inserted into the Tent.
         * Everything is fine, we can now insert the SPT node in TENT
         */
        if ((ISIS_ADJ_SYS_TYPE (pSptNode->au2Metric[u1MetIdx]) ==
             ISIS_IP_END_SYSTEM)
            && (pContext->SysActuals.bSysL2ToL1Leak == ISIS_TRUE))
        {
            IsisDecInsertRLAdjInTent (pContext, pSptNode, u1MetIdx, u1Lvl,
                                      u1MtIndex);
        }
        else
        {
            IsisDecInsertAdjInTent (pContext, pSptNode, u1MetIdx, u1Lvl,
                                    u1MtIndex);
        }
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecValAndInsNodeInTent ()\n"));
}

/******************************************************************************
 * Function     : IsisDecSPTCreate () 
 * Description  : This routine runs the SPF Algorithm and calculates
 *                the Shortest Paths to all destinations, for a given Metric
 *                and a Level.
 * Input (s)    : pContext  - Pointer to System Context
 *                u1Level   - Level for which the SPF runs. 
 *                u1MetIdx  - Metric Index which specifies the index that 
 *                            holds values that are to be to updated in the 
 *                            arrays used in the SPT nodes
 *                u1MetType - Metric Type for which the Decision process is run
 * Output (s)   : None
 * Globals      : Not Referred or Modified
 * Returns      : ISIS_SUCCESS, If SPF is successfully run
 *                ISIS_FAILURE, Otherwise
 *****************************************************************************/

PRIVATE INT4
IsisDecSPTCreate (tIsisSysContext * pContext, UINT1 u1Level, UINT1 u1MetIdx,
                  UINT1 u1MetType, UINT1 u1MtIndex)
{
    tIsisSPTNode       *pMCNode = NULL;
    tIsisSPTNode       *pSptList = NULL;
    tIsisSPTNode       *pTentNode = NULL;
    tIsisSPTNode       *pSelfNode = NULL;
    tIsisSPTNode       *pPrevNode = NULL;
    tIsisSPTNode       *pNextNode = NULL;
    tIsisSPTNode       *pNextTentNode = NULL;
    tIsisEvtDecnRelq   *pEvtDecnRelq = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1HashIdx = 0;
    UINT1               u1AttStt = 0;
    UINT1               u1Result = 0;
    UINT1               u1Count = 0;
    UINT1               u1AttStatChg = ISIS_FALSE;
    UINT1               u1Loc = ISIS_SPT_LOC_NONE;
    UINT1               u1Flag = ISIS_FALSE;
    UINT1               u1NearL2Flag = ISIS_FALSE;
    UINT1               u1GetATTFlag = ISIS_FALSE;
    UINT1               u1AttFlag = ISIS_NOT_SET;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecSPTCreate ()\n"));

    /* If Level of the Decision Process being run is L2, then get that attached
     * status of the system while getting the Adjacencies from the Update Module
     */

    u1GetATTFlag = ((u1Level == ISIS_LEVEL2) ? ISIS_TRUE : ISIS_FALSE);

    /* Add the SELF Node to the Path 
     * Refer Step 0 (Point 1) of the SPF Algorithm in RFC 1195.
     */

    /* Allocate an SPT node which will hold the Self information as follows:
     * 1. Self Node with the local System ID 
     * 2. Pseudonode ID as Zero
     * 3. Direction as '0' which means SELF. 
     * 4. Initialised MaxPaths array, 
     */

    pSelfNode = (tIsisSPTNode *) ISIS_MEM_ALLOC (ISIS_BUF_SPTN,
                                                 sizeof (tIsisSPTNode));
    if (pSelfNode == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SPT Node\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_SPTN);
        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecSPTCreate ()\n"));
        return (ISIS_FAILURE);
    }

    MEMCPY (pSelfNode->au1DestID, pContext->SysActuals.au1SysID,
            ISIS_SYS_ID_LEN);

    pSelfNode->au1DestID[ISIS_SYS_ID_LEN] = 0;
    pSelfNode->au4DirIdx[u1MetIdx] = 0;
    pSelfNode->au1MaxPaths[u1MetIdx] = 0;
    pSelfNode->pNext = NULL;

    /* Mark all the Metrics with Invalid values. The corresponding Metric
     * value will be filled while the node is being added to TENT. since
     * this is a Self Node the metric is updated as '0' later
     */

    pSelfNode->u4MetricVal = ISIS_SPT_RL_INV_METRIC;

    for (u1Count = 0; u1Count < ISIS_NUM_METRICS; u1Count++)
    {
        pSelfNode->au2Metric[u1Count] = ISIS_SPT_INV_METRIC;
    }

    /* Self System is reachable by a metric value of '0'
     */

    pSelfNode->au2Metric[u1MetIdx] = 0;
    pSelfNode->u4MetricVal = 0;

    /* Check whether the Self Node already exist in PATH. If it exists then
     * update the Self Node information for the current metric in the same Node.
     * If not add the new node to OSI PATH. IsisDecIsNodeInPATH () will return 
     * the previous node pointer (if any) which will be used for updating the 
     * new node information in the PATH
     */

    u1Loc = IsisDecIsNodeInPATH (pContext, pSelfNode, u1Level, u1MetIdx,
                                 &pPrevNode, u1HashIdx, u1MtIndex);

    IsisDecAddNodeToPath (pContext, pPrevNode, pSelfNode, u1Loc, u1Level,
                          u1MetIdx, ISIS_OSI_PATH, u1MtIndex);

    if ((u1Loc == ISIS_SPT_LOC_PATH_DIFF)
        || (u1Loc == ISIS_SPT_LOC_PATH_DIFF_FIRST))
    {
        /* Self Node is already found in the PATH either for the same Metric or
         * a different metric. So free the allocated Self node as the values
         * from the Self node have been updated in the already existing node in
         * the OSI Path
         */

        pSelfNode->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSelfNode);
    }

    /* Add all the Self Adjacencies to the TENT
     *
     * Refer C.1.4 Step 0, 2) of RFC 1195
     */

    i4RetVal = IsisDecAddSelfAdjs (pContext, u1Level, u1MetIdx, u1MtIndex);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* Do not continue, since there is no point in proceeding further if
         * Self Adjacencies themselves cannot be added
         */

        WARNING ((ISIS_LGST,
                  "DEC <W> : Failed To Add Self Adjacencies - Level [%s]\n",
                  ISIS_GET_SYS_TYPE_STR (u1Level)));
        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecSPTCreate ()\n"));
        return (i4RetVal);
    }

    while (u1Flag == ISIS_FALSE)
    {
        /* Keep removing the Minimum cost nodes from the TENT. Based on the
         * System Type (ISIS_IP_END_SYSTEM or ISIS_OSI_SYSTEM) add the node to
         * either OSI or IP Path. We may have to handle multiple nodes also
         * in the case of Equal Cost Multi-Path (ECMP).
         */

        IsisDecGetMCNodeFromTent (pContext, u1MetIdx, &pTentNode, &u1HashIdx);

        if (pTentNode != NULL)
        {
            /* We still have nodes to be considered from TENT. Note down the
             * MC node retrieved which will be of use in case the retrieved
             * nodes is the first node in the ECMP list. As mentioned before
             * only the first TENT node in the ECMP list will have a proper
             * value for Max Paths. We may have to update each of the ECMP nodes
             * with the correct value of Max Paths before the node is moved into
             * PATH
             */

            pMCNode = pTentNode;

            ISIS_DBG_PRINT_ID (pTentNode->au1DestID,
                               (UINT1) (ISIS_SYS_ID_LEN + 1),
                               "\tMinimum Cost Node\t", ISIS_OCTET_STRING);

            /* The Type of Adjacency must be the same for all the Nodes in an
             * ECMP list i.e. if {N1, D1, 1}, {N1, D5, 1} and {N1, D3, 1} are
             * the ECMP nodes in the ECMP list for {N1}, then the Metric bits
             * in the SPT node MUST indicate that {N1} is either OSI or IP 
             * system. Each node in the list cannot depict a different system 
             * type for {N1}. Hence compute the Adjacent System Type from the 
             * very first node in the ECMP list
             */

            u1Result = (UINT1) (ISIS_ADJ_SYS_TYPE (pTentNode->
                                                   au2Metric[u1MetIdx]));

            /* NOTE: The while loop is for handling the ECMP case
             */

            while (pTentNode != NULL)
            {
                pPrevNode = NULL;

                /* Check whether a Node which mathces the pTentNode, exists in
                 * the PATH already. The return value 'u1Loc' indicates various
                 * scenarios. IsisDecAddNodeToPath () routine will handle these
                 * scenarios differently
                 */

                u1Loc = IsisDecIsNodeInPATH (pContext, pTentNode, u1Level,
                                             u1MetIdx, &pPrevNode, u1HashIdx,
                                             u1MtIndex);

                if (u1Loc == ISIS_SPT_LOC_PATH_SAME_DIRIDX)
                {
                    /* This case is Node exists in the path for the same 
                     * destination, same metric type, with the same metric and 
                     * with the same nexthop address index. So
                     * don't process this node. Just free it
                     */

                    pNextTentNode = pTentNode->pNext;
                    pTentNode->pNext = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTentNode);
                    pTentNode = pNextTentNode;
                    continue;
                }

                pNextTentNode = pTentNode->pNext;

                /* Make the MPS value in all the Minimum Cost Nodes to be
                 * the same as the Maximum Paths available for this ECMP
                 * before inserting these minimum cost nodes in the PATH
                 *
                 * NOTE: ECMP Nodes in TENT will have MaxPaths set in decreasing
                 *       order i.e. the first node in the ECMP will have
                 *       MaxPaths, the second MaxPaths - 1 and so on. pMCNode is
                 *       now pointing to the first ECMP node which has the
                 *       correct value of MaxPaths. While inserting in PATH we
                 *       will ensure that all nodes have the same MaxPaths value
                 */

                pTentNode->au1MaxPaths[u1MetIdx] =
                    pMCNode->au1MaxPaths[u1MetIdx];
                (pMCNode->au1MaxPaths[u1MetIdx])--;

                pTentNode->pNext = NULL;

                /* NOTE: FutureISIS implements two different PATHS, one for OSI
                 *       and the other for IP, and the current implementation
                 *       supports IP environments only
                 */

                if (u1Result == ISIS_IP_END_SYSTEM)
                {
                    /* The MC node contains information regarding IPRA's and
                     * hence should be added to the IP PATH
                     */

                    i4RetVal = IsisDecAddNodeToPath (pContext, pPrevNode,
                                                     pTentNode, u1Loc, u1Level,
                                                     u1MetIdx,
                                                     ISIS_CURR_IP_PATH,
                                                     u1MtIndex);

                    if ((i4RetVal == ISIS_FAILURE)
                        || (u1Loc == ISIS_SPT_LOC_PATH_DIFF)
                        || (u1Loc == ISIS_SPT_LOC_PATH_DIFF_FIRST))
                    {
                        /* 'pNode' is already found in the PATH either for 
                         * the same Metric or a different metric. So free 
                         * the incoming node as the values from the incoming 
                         * node have been updated in the already existing 
                         * node in the Path
                         */

                        pTentNode->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTentNode);
                        i4RetVal = ISIS_SUCCESS;
                    }

                    if (pContext->u4MaxNodes < ISIS_MAX_SPF_NODES)
                    {
                        pContext->u4MaxNodes++;
                    }
                    else if (pNextTentNode != NULL)
                    {
                        DEP_PT ((ISIS_LGST,
                                 "DEC <T> : Max nodes processed in SPF, the last node being processed is"
                                 " ECMP, So delay relinquish. Nodes Processed [%d]\n",
                                 pContext->u4MaxNodes));
                    }
                    else
                    {
                        gu4NodeCnt++;
                        OsixDelayTask (5);
                        /* ISIS is delaying itself to give control to other
                         * tasks to run. This is to support the OS that are
                         * non-premtive
                         */

                        DEP_PT ((ISIS_LGST,
                                 "DEC <T> : Relinquish SPF (SPTCreate)- Number of Relinquishes made since SPF schedule [%d]\n",
                                 gu4NodeCnt));
                        pEvtDecnRelq =
                            (tIsisEvtDecnRelq *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                                 sizeof
                                                                 (tIsisEvtDecnRelq));

                        if (pEvtDecnRelq != NULL)
                        {
                            pEvtDecnRelq->u1EvtID = ISIS_EVT_DECN_RELQ;
                            pEvtDecnRelq->u1MetIdx = u1MetIdx;
                            pEvtDecnRelq->u1Level = u1Level;
                            pEvtDecnRelq->u1AttFlag = u1GetATTFlag;
                            pEvtDecnRelq->u1NearL2Flag = u1NearL2Flag;
                            pEvtDecnRelq->u1AttStatChg = u1AttStatChg;
                            pEvtDecnRelq->u1MTIndex = u1MtIndex;

                            IsisUtlSendEvent (pContext,
                                              (UINT1 *) pEvtDecnRelq,
                                              sizeof (tIsisEvtDecnRelq));
                            gu1RelqFlag = ISIS_DECN_WAIT;
                            pContext->u4MaxNodes = 0;

                            return (ISIS_DECN_RELQ);
                        }
                        else
                        {
                            PANIC ((ISIS_LGST,
                                    ISIS_MEM_ALLOC_FAIL
                                    " : Decision Relinquish Event\n"));
                            DETAIL_FAIL (ISIS_CR_MODULE);
                        }
                    }

                    /* IPRA information is treated like End System Adjacencies.
                     * Once loaded into TENT we can proceed further and try to
                     * retrieve other MC nodes. These IPRAs move from TENT to
                     * PATH, when they happen to be the MC nodes
                     */
                    pTentNode = pNextTentNode;

                    continue;
                }
                else
                {
                    /* The MC node contains information regarding IS Adjacencies
                     * and hence should be added to the OSI PATH
                     */

                    if (u1Level == ISIS_LEVEL1)
                    {
                        IsisDecUpdateNearestL2 (pContext, pTentNode, u1MetIdx,
                                                u1MetType, &u1NearL2Flag,
                                                u1MtIndex);
                    }
                    IsisDecAddNodeToPath (pContext, pPrevNode, pTentNode, u1Loc,
                                          u1Level, u1MetIdx, ISIS_OSI_PATH,
                                          u1MtIndex);
                    if ((u1Loc == ISIS_SPT_LOC_PATH_DIFF)
                        || (u1Loc == ISIS_SPT_LOC_PATH_DIFF_FIRST))
                    {
                        /* 'pNode' is already found in the PATH either for 
                         * the same Metric or a different metric. So free 
                         * the incoming node as the values from the incoming 
                         * node have been updated in the already existing 
                         * node in the Path
                         */

                        pTentNode->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTentNode);
                    }
                }

                /* This loop covers the Step 1 of the SPF algorithm of RFC 1195.
                 * Now that the MC node is moved to PATH from TENT, we will
                 * retrieve all the adjacencies reported by the MC node
                 */

                i4RetVal =
                    IsisUpdGetAdjsWithSysId (pContext, pTentNode->au1DestID,
                                             u1MetIdx, u1Level, u1GetATTFlag,
                                             &pSptList, &u1AttStt, u1MtIndex);

                if (i4RetVal == ISIS_SUCCESS)
                {
                    /* We have valid adjacencies to be considered for the MC
                     * node. For each of the adjacency we have to adjust the
                     * Path Metric, verify that the nodes have 2-way
                     * connectivity with the MC node and also update the
                     * Nearest L2 information if not already updated
                     */

                    IsisDecValAndInsNodeInTent (pContext, &pSptList, pTentNode,
                                                u1Level, u1MetIdx, u1MtIndex);

                    if ((u1Level == ISIS_LEVEL2) && (u1GetATTFlag == ISIS_TRUE))
                    {
                        /* We are running a L2 decision process and
                         * 'u1GetATTFlag' indictaes that we have to extract the
                         * Attached status of the MC node whose adjacencies
                         * we have extracted. The flag 'u1AttStt' indicates 
                         * whether the MC node belongs to the same area or some
                         * other area.
                         */

                        if (u1AttStt == ISIS_OTHER_AREA)
                        {
                            /* The local system has connectivity with a node
                             * (in this case the MC node), which belongs to a
                             * different AREA than the one to which the local
                             * system belongs. This means the local system can
                             * route packets to a diferent AREA by which we can
                             * infer that the local system is Attached
                             *
                             * NOTE: A system is not attached if it is
                             * connected to only one AREA
                             */

                            /* Once we know we are attached, we need not
                             * verify Area Addresses of all the other systems,
                             * since if a system is connected to more than one
                             * Area, then the system is attached. This is
                             * already deduced and hence requires no more
                             * verifications. Set the  'u1GetATTFlag' as
                             * ISIS_FALSE and mark the local system as ATTACHED
                             */

                            DETAIL_INFO (ISIS_DEC_MODULE,
                                         (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                          "DEC <T> : Attached Bit Set\n"));
                            u1GetATTFlag = ISIS_FALSE;

                            /* Note down that system is attached 
                             */

                            u1AttStatChg = ISIS_TRUE;

                            u1AttFlag = ISIS_NOT_SET;
                            if (u1MtIndex != 0)
                            {
                                if (pContext->SelfLSP.
                                    au1L1MTLspFlag[u1MtIndex] &
                                    ISIS_MT_LSP_ATT_MASK)
                                {
                                    u1AttFlag = ISIS_SET;
                                }
                            }
                            else if (ISIS_GET_ATT_BIT (pContext, u1MetType))
                            {
                                u1AttFlag = ISIS_SET;
                            }
                            if ((u1AttFlag == ISIS_NOT_SET)
                                && (pContext->SysActuals.u1SysType ==
                                    ISIS_LEVEL12))
                            {
                                if (u1MtIndex != 0)
                                {
                                    pContext->SelfLSP.
                                        au1L1MTLspFlag[u1MtIndex] |=
                                        ISIS_MT_LSP_ATT_MASK;
                                }
                                else
                                {
                                    ISIS_SET_ATT_BIT (pContext, u1MetType);
                                }

                                if ((pContext->SysActuals.u1SysType ==
                                     ISIS_LEVEL12)
                                    && (pContext->SelfLSP.pL1NSNLSP != NULL)
                                    && (pContext->SelfLSP.pL1NSNLSP->pZeroLSP !=
                                        NULL))
                                {
                                    pContext->SelfLSP.pL1NSNLSP->pZeroLSP->
                                        u1DirtyFlag = ISIS_MODIFIED;

                                }
                            }
                        }
                    }
                }
                else
                {
                    ISIS_DBG_PRINT_ID (pTentNode->au1DestID,
                                       (UINT1) (ISIS_SYS_ID_LEN + 1),
                                       "DEC <T> : Ignoring LSPs Of System\t",
                                       ISIS_OCTET_STRING);

                    while (pSptList != NULL)
                    {
                        pNextNode = pSptList->pNext;
                        pSptList->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptList);
                        pSptList = pNextNode;
                    }
                }

                pTentNode = pNextTentNode;
            }                    /* End of ECMP while loop */
        }
        else
        {
            /* TENT is NULL which means all nodes have been processed and the
             * PATH is ready
             */

            u1Flag = ISIS_TRUE;
        }
    }

    /* If the Tent is Empty, Update the Routing Database and quit the 
     * calculation of SPF Database.
     */

    IsisDecUpdateRoute (pContext, u1MetIdx, u1Level, u1MtIndex);

    u1AttFlag = ISIS_NOT_SET;
    if (u1MtIndex != 0)
    {
        if (pContext->SelfLSP.au1L1MTLspFlag[u1MtIndex] & ISIS_MT_LSP_ATT_MASK)
        {
            u1AttFlag = ISIS_SET;
        }
    }
    else if (ISIS_GET_ATT_BIT (pContext, u1MetType))
    {
        u1AttFlag = ISIS_SET;
    }

    if ((u1AttStatChg == ISIS_FALSE) && (u1Level == ISIS_LEVEL2)
        && (u1AttFlag != ISIS_NOT_SET))
    {
        /* The System become unattached. So change the SysL1Flags 
         * from attached to unattached and post an event to Ctrl module
         */

        if (u1MtIndex != 0)
        {
            pContext->SelfLSP.au1L1MTLspFlag[u1MtIndex] &=
                ~ISIS_MT_LSP_ATT_MASK;
        }
        else
        {
            ISIS_RESET_ATT_BIT (pContext, u1MetType);
        }
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12) &&
            (pContext->SelfLSP.pL1NSNLSP != NULL) &&
            (pContext->SelfLSP.pL1NSNLSP->pZeroLSP != NULL))
        {
            pContext->SelfLSP.pL1NSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

        }
    }

    if (u1Level == ISIS_LEVEL1)
    {
        IsisDecDeleteNearestL2 (pContext, u1MetIdx, u1MtIndex);
    }
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecSPTCreate ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisDecInsertAdjInTent () 
 * Description : This routine inserts the given adjacency information into the
 *               TENT, if the given information is not already present in both
 *               PATH and TENT. If an adjacency with the same Destination ID
 *               and same Metric already exist, then based on the Max Path 
 *               Splits, excess paths are pruned. If the adjacency information
 *               exists in PATH, the given pSptNode is ignored
 * Input (s)   : pContext - Pointer to System Context
 *               pSptNode - The Adjacency node to be added ot the Tent.
 *               u1MetIdx - Metric Index which specifies the index that 
 *                          holds values that are to be to updated in the 
 *                          arrays used in the SPT nodes
 *               u1Level  - Level for which the SPF runs. 
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisDecInsertAdjInTent (tIsisSysContext * pContext, tIsisSPTNode * pSptNode,
                        UINT1 u1MetIdx, UINT1 u1Level, UINT1 u1MtIndex)
{
    UINT1               u1Loc = 0;
    UINT1               u1MaxPaths = 0;
    UINT1               u1HashIdx = 0;
    UINT4               u4NodeMetric = 0;
    UINT4               u4SptNodeMetric = 0;
    tIsisSPTNode       *pNode = NULL;
    tIsisSPTNode       *pPrev = NULL;
    tIsisSPTNode       *pRemNode = NULL;
    tIsisSPTNode       *pPrevNode = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecInsertAdjInTent ()\n"));

    /* First check whether the given information already exist in either TENT or
     * PATH for the given Metric Index
     */

    u1Loc = (UINT1) IsisDecIsNodeInHashTbl (pContext, pSptNode, u1Level,
                                            u1MetIdx, &pPrevNode, &u1HashIdx,
                                            u1MtIndex);

    if ((u1HashIdx >= ISIS_MAX_BUCKETS) || (u1MetIdx >= ISIS_NUM_METRICS))    /*klocwork */
    {
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : Hash Index Does Not Exist [%u] or Invalid Metric Type [%u]\n",
                 u1HashIdx, u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return;
    }

    switch (u1Loc)
    {
        case ISIS_SPT_LOC_NONE:
        case ISIS_SPT_LOC_PATH_DIFF:
        case ISIS_SPT_LOC_PATH_DIFF_FIRST:

            /* No such node exists in TENT but a node with the same destination
             * ID exists in PATH but for a different metric. Insert the node 
             * in the TENT in a sorted order so that the minimum cost node is at
             * the head
             */

            /* `au1MaxPaths` indicate how many equal cost paths exist for the
             * given Destination ID. Since this is the first node being
             * added,initialise it to '1'
             */

            pSptNode->au1MaxPaths[u1MetIdx] = 1;

            ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                               (UINT1) (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN),
                               "DEC <T> : Inserting Node Into TENT\t",
                               ISIS_OCTET_STRING);
            IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode, u1HashIdx,
                                  u1MetIdx);
            break;

        case ISIS_SPT_LOC_TENT:

            /* Node exists in the TENT. Verify whether this is an equal cost
             * multi-path node or a new node that can replace the existing one
             * or a node to be ignored 
             */

            pNode = RBTreeGet (pContext->Tent, pSptNode);

            /* Required for ECMP */
            if (pNode == NULL)
            {
                /* klocwork fix. pNode cannot be null as the node 
                 * is already verified to be present in the tent database */
                break;
            }
            u1MaxPaths = pNode->au1MaxPaths[u1MetIdx];
            u4NodeMetric = pNode->u4MetricVal;
            u4SptNodeMetric = pSptNode->u4MetricVal;
            pAdjEntry = (tIsisAdjEntry *)
                IsisAdjGetAdjRecFromDirIdx (pContext,
                                            pNode->au4DirIdx[u1MetIdx]);
            if (pAdjEntry == NULL)
            {
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : No Adjacency Record Found - Direction [%u]\n",
                         pNode->au4DirIdx[u1MetIdx]));
                DETAIL_FAIL (ISIS_CR_MODULE);
                DEP_EE ((ISIS_LGST,
                         "DEC <X> : Exiting IsisDecRemExcessPaths ()\n"));
                return;
            }

            if (u4NodeMetric == u4SptNodeMetric)
            {
                DEP_PT ((ISIS_LGST,
                         "DEC <T> : [ %u ] Equal Cost Path Exists - Circuit Index [ %u ]\n",
                         u1MaxPaths, pAdjEntry->pCktRec->u4CktIdx));

                if (++u1MaxPaths > pContext->SysActuals.u1SysMPS)
                {
                    /* Maximum Paths already exist. Now we have to prune the
                     * exces paths from the list
                     */

                    pRemNode = pSptNode;
                    pPrev = pPrevNode;
                    while (pNode != NULL)
                    {
                        /* Function IsisDecRemExcessPaths () checks the 
                         * two SPT Nodes and returns the node which is 
                         * to be removed
                         */

                        pRemNode = IsisDecRemExcessPaths (pContext, pNode,
                                                          pRemNode, u1MetIdx);
                        if (pRemNode == pNode)
                        {
                            pPrevNode = pPrev;
                        }
                        pPrev = pNode;
                        pNode = pNode->pNext;
                    }

                    if (pRemNode != pSptNode)
                    {
                        /* The removed node is not the new node that we tried to
                         * add. So remove the pRemNode from the TENT and add the
                         * new node. Max Paths will still remain the same since
                         * one is deleted and one is added
                         */

                        ISIS_DBG_PRINT_ID (pRemNode->au1DestID,
                                           (UINT1) (ISIS_SYS_ID_LEN + 1),
                                           "DEC <T> : Pruning Node From TENT\t",
                                           ISIS_OCTET_STRING);
                        IsisDecRemNodeFromTent (pContext, pPrevNode, pRemNode,
                                                u1HashIdx);

                        ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                           (UINT1) (ISIS_SYS_ID_LEN + 1),
                                           "DEC <T> : Added New Node To TENT\t",
                                           ISIS_OCTET_STRING);
                        pSptNode->au1MaxPaths[u1MetIdx] = u1MaxPaths;
                        IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode,
                                              u1HashIdx, u1MetIdx);
                    }
                    else
                    {
                        /* The new node is to be freed.
                         */

                        pSptNode->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
                    }
                }
                else
                {
                    /* Still Max Paths limit has not reached. Add the adjacency
                     * information as an ECMP to TENT
                     * Increment the Equal Cost Paths value for the new 
                     * node to be added
                     */

                    ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                       (UINT1) (ISIS_SYS_ID_LEN + 1),
                                       "DEC <T> : Adding New Node To TENT\t",
                                       ISIS_OCTET_STRING);

                    pSptNode->au1MaxPaths[u1MetIdx] = u1MaxPaths;
                    IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode,
                                          u1HashIdx, u1MetIdx);
                }
            }
            else if (u4NodeMetric > u4SptNodeMetric)
            {
                if (u1MaxPaths > 1)
                {
                    /* Found a node with lesser metric when compared to the
                     * already existing ECMP. So the existing ECMP is to nbe
                     * removed and the new node is to be added 
                     */

                    for (; u1MaxPaths > 0; u1MaxPaths--)
                    {
                        pRemNode = pNode->pNext;
                        IsisDecRemNodeFromTent (pContext, pPrevNode, pNode,
                                                u1HashIdx);
                        pNode = pRemNode;
                    }
                }

                /* The new adjacency node has Metric which is lower than the
                 * metric of the node that is in the TENT. Remove the TENT node
                 * and add the new node to TENT
                 */

                else
                {
                    IsisDecRemNodeFromTent (pContext, pPrevNode, pNode,
                                            u1HashIdx);
                }

                IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode, u1HashIdx,
                                      u1MetIdx);
            }
            else
            {
                /* The node that is already inserted into TENT has a lower
                 * Metric than the new node. Ignore the new information
                 */

                ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                   (UINT1) (ISIS_SYS_ID_LEN + 1),
                                   "DEC <T> : Ignoring Node\t",
                                   ISIS_OCTET_STRING);
                pSptNode->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
            }
            break;

        case ISIS_SPT_LOC_PATH_SAME:
        case ISIS_SPT_LOC_PATH_SAME_FIRST:

            /* Information already exist in PATH. Ignore the new adjacency node
             */

            ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                               (UINT1) (ISIS_SYS_ID_LEN + 1),
                               "DEC <T> : Ignoring Node\t", ISIS_OCTET_STRING);
            pSptNode->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
            break;

        default:

            /* It can never happen. Free the new information
             */

            pSptNode->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
            break;
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecInsertAdjInTent ()\n"));
}

/*******************************************************************************
 * Function    : IsisDecInsertRLAdjInTent () 
 * Description : This routine inserts the given adjacency information into the
 *               TENT, if the given information is not already present in both
 *               PATH and TENT. If an adjacency with the same Destination ID
 *               and same Metric already exist, then based on the Max Path 
 *               Splits, excess paths are pruned. If the adjacency information
 *               exists in PATH, the given pSptNode is ignored
 * Input (s)   : pContext - Pointer to System Context
 *               pSptNode - The Adjacency node to be added ot the Tent.
 *               u1MetIdx - Metric Index which specifies the index that 
 *                          holds values that are to be to updated in the 
 *                          arrays used in the SPT nodes
 *               u1Level  - Level for which the SPF runs. 
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisDecInsertRLAdjInTent (tIsisSysContext * pContext, tIsisSPTNode * pSptNode,
                          UINT1 u1MetIdx, UINT1 u1Level, UINT1 u1MtIndex)
{

    UINT1               u1Loc = 0;
    UINT1               u1MaxPaths = 0;
    UINT1               u1HashIdx = 0;
    UINT1               u1SptUPDownFlag = 0;
    UINT1               u1NodeUPDownFlag = 0;
    UINT4               u4NodeMetric = 0;
    UINT4               u4SptNodeMetric = 0;
    tIsisSPTNode       *pNode = NULL;
    tIsisSPTNode       *pPrev = NULL;
    tIsisSPTNode       *pRemNode = NULL;
    tIsisSPTNode       *pPrevNode = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecInsertRLAdjInTent ()\n"));
    /* First check whether the given information already exist in either TENT or
     * PATH for the given Metric Index
     */
    u1Loc = (UINT1) IsisDecIsNodeInHashTbl (pContext, pSptNode, u1Level,
                                            u1MetIdx, &pPrevNode, &u1HashIdx,
                                            u1MtIndex);

    if ((u1HashIdx >= ISIS_MAX_BUCKETS) || (u1MetIdx >= ISIS_NUM_METRICS))    /*klocwork */
    {
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : Hash Index Does Not Exist [%u] or Invalid Metric Type [%u]\n",
                 u1HashIdx, u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return;
    }

    switch (u1Loc)
    {
        case ISIS_SPT_LOC_NONE:
        case ISIS_SPT_LOC_PATH_DIFF:
        case ISIS_SPT_LOC_PATH_DIFF_FIRST:

            /* No such node exists in TENT but a node with the same destination
             * ID exists in PATH but for a different metric. Insert the node 
             * in the TENT in a sorted order so that the minimum cost node is at
             * the head
             */

            /* `au1MaxPaths` indicate how many equal cost paths exist for the
             * given Destination ID. Since this is the first node being
             * added,initialise it to '1'
             */

            pSptNode->au1MaxPaths[u1MetIdx] = 1;

            ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                               (UINT1) (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN),
                               "DEC <T> : Inserting Node Into TENT\t",
                               ISIS_OCTET_STRING);
            IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode, u1HashIdx,
                                  u1MetIdx);
            break;
        case ISIS_SPT_LOC_TENT:
            /* Node exists in the TENT. Verify whether this is an equal cost
             * multi-path node or a new node that can replace the existing one
             * or a node to be ignored 
             */

            pNode = RBTreeGet (pContext->Tent, pSptNode);
            if (pNode == NULL)
            {
                /* klocwork fix. pNode cannot be null as the node 
                 * is already verified to be present in the tent database */
                break;
            }
            u1MaxPaths = pNode->au1MaxPaths[u1MetIdx];
            u1NodeUPDownFlag = SPT_GET_UPDOWN_FLAG (pNode);
            u1SptUPDownFlag = SPT_GET_UPDOWN_FLAG (pSptNode);
            if (u1SptUPDownFlag > u1NodeUPDownFlag)
            {
                /* This node is certainly better based on order of
                 * preference hence we need to set this route as the
                 * as the best route
                 */

                if (u1MaxPaths > 1)
                {
                    /* Found a node with lesser metric when compared to the
                     * already existing ECMP. So the existing ECMP is to nbe
                     * removed and the new node is to be added 
                     */

                    for (; u1MaxPaths > 0; u1MaxPaths--)
                    {
                        pRemNode = pNode->pNext;
                        IsisDecRemNodeFromTent (pContext, pPrevNode, pNode,
                                                u1HashIdx);
                        pNode = pRemNode;
                    }
                }

                /* The new adjacency node has Metric which is lower than the
                 * metric of the node that is in the TENT. Remove the TENT node
                 * and add the new node to TENT
                 */

                else
                {
                    ISIS_DBG_PRINT_ID (pNode->au1DestID,
                                       (UINT1) (ISIS_SYS_ID_LEN + 1),
                                       "DEC <T> : Removing Node From TENT\t",
                                       ISIS_OCTET_STRING);

                    IsisDecRemNodeFromTent (pContext, pPrevNode, pNode,
                                            u1HashIdx);
                }

                ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                   (UINT1) (ISIS_SYS_ID_LEN + 1),
                                   "DEC <T> : Adding Node To TENT\t",
                                   ISIS_OCTET_STRING);
                IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode, u1HashIdx,
                                      u1MetIdx);
            }                    /* end if (flag comparison) */
            else if (u1SptUPDownFlag < u1NodeUPDownFlag)
            {
                /* The node already inserted into TENT is a better node
                 * it need not be tampered with and new node can be ignored
                 */
                ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                   (UINT1) (ISIS_SYS_ID_LEN + 1),
                                   "DEC <T> : Ignoring Node\t",
                                   ISIS_OCTET_STRING);
                pSptNode->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
            }
            else if (u1SptUPDownFlag == u1NodeUPDownFlag)
            {
                u4NodeMetric = pNode->u4MetricVal;

                u4SptNodeMetric = pSptNode->u4MetricVal;

                if (u4NodeMetric == u4SptNodeMetric)
                {
                    if (++u1MaxPaths > pContext->SysActuals.u1SysMPS)
                    {
                        /* Maximum Paths already exist. Now we have to prune the
                         * exces paths from the list
                         */

                        pRemNode = pSptNode;
                        pPrev = pPrevNode;
                        while (pNode != NULL)
                        {
                            /* Function IsisDecRemExcessPaths () checks  
                             * the two SPT Nodes and returns the node  
                             * which is to be removed
                             */

                            pRemNode =
                                IsisDecRemExcessPaths (pContext, pNode,
                                                       pRemNode, u1MetIdx);
                            if (pRemNode == pNode)
                            {
                                pPrevNode = pPrev;
                            }
                            pPrev = pNode;
                            pNode = pNode->pNext;
                        }

                        if (pRemNode != pSptNode)
                        {
                            /* The removed node is not the new node that we tried to
                             * add. So remove the pRemNode from the TENT and add the
                             * new node. Max Paths will still remain the same since
                             * one is deleted and one is added
                             */

                            ISIS_DBG_PRINT_ID (pRemNode->au1DestID,
                                               (UINT1) (ISIS_SYS_ID_LEN + 1),
                                               "DEC <T> : Pruning Node From TENT\t",
                                               ISIS_OCTET_STRING);
                            IsisDecRemNodeFromTent (pContext, pPrevNode,
                                                    pRemNode, u1HashIdx);

                            ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                               (UINT1) (ISIS_SYS_ID_LEN + 1),
                                               "DEC <T> : Added New Node To TENT\t",
                                               ISIS_OCTET_STRING);
                            pSptNode->au1MaxPaths[u1MetIdx] = u1MaxPaths;
                            IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode,
                                                  u1HashIdx, u1MetIdx);
                        }
                        else
                        {
                            /* The new node is to be freed.
                             */

                            ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                               (UINT1) (ISIS_SYS_ID_LEN + 1),
                                               "DEC <T> : New Node Ignored\t",
                                               ISIS_OCTET_STRING);
                            pSptNode->pNext = NULL;
                            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
                        }
                    }
                    else
                    {
                        /* Still Max Paths limit has not reached. Add the adjacency
                         * information as an ECMP to TENT
                         * Increment the Equal Cost Paths value for the new 
                         * node to be added
                         */

                        ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                           (UINT1) (ISIS_SYS_ID_LEN + 1),
                                           "DEC <T> : Adding New Node To TENT\t",
                                           ISIS_OCTET_STRING);

                        pSptNode->au1MaxPaths[u1MetIdx] = u1MaxPaths;
                        IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode,
                                              u1HashIdx, u1MetIdx);
                    }
                }
                else if (u4NodeMetric > u4SptNodeMetric)
                {
                    if (u1MaxPaths > 1)
                    {
                        /* Found a node with lesser metric when compared to the
                         * already existing ECMP. So the existing ECMP is to nbe
                         * removed and the new node is to be added 
                         */

                        for (; u1MaxPaths > 0; u1MaxPaths--)
                        {
                            pRemNode = pNode->pNext;
                            IsisDecRemNodeFromTent (pContext, pPrevNode, pNode,
                                                    u1HashIdx);
                            pNode = pRemNode;
                        }
                    }

                    /* The new adjacency node has Metric which is lower than the
                     * metric of the node that is in the TENT. Remove the TENT node
                     * and add the new node to TENT
                     */

                    else
                    {

                        ISIS_DBG_PRINT_ID (pNode->au1DestID,
                                           (UINT1) (ISIS_SYS_ID_LEN + 1),
                                           "DEC <T> : Removing Node From TENT\t",
                                           ISIS_OCTET_STRING);

                        IsisDecRemNodeFromTent (pContext, pPrevNode, pNode,
                                                u1HashIdx);
                    }

                    ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                       (UINT1) (ISIS_SYS_ID_LEN + 1),
                                       "DEC <T> : Adding Node To TENT\t",
                                       ISIS_OCTET_STRING);
                    IsisDecAddNodeToTent (pContext, pPrevNode, pSptNode,
                                          u1HashIdx, u1MetIdx);
                }
                else
                {
                    /* The node that is already inserted into TENT has a lower
                     * Metric than the new node. Ignore the new information
                     */

                    ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                                       (UINT1) (ISIS_SYS_ID_LEN + 1),
                                       "DEC <T> : Ignoring Node\t",
                                       ISIS_OCTET_STRING);
                    pSptNode->pNext = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
                }
            }                    /* end if (UP down flag equal check ) */
            break;
        case ISIS_SPT_LOC_PATH_SAME:
        case ISIS_SPT_LOC_PATH_SAME_FIRST:

            /* Information already exist in PATH. Ignore the new adjacency node
             */

            ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                               (UINT1) (ISIS_SYS_ID_LEN + 1),
                               "DEC <T> : Ignoring Node\t", ISIS_OCTET_STRING);
            pSptNode->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
            break;

        default:

            /* It can never happen. Free the new information
             */

            pSptNode->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
            break;
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecInsertRLAdjInTent ()\n"));
}

/******************************************************************************
 * Function    : IsisDecAddNodeToTent () 
 * Description : This function adds the Node to the TENT. If pPrevNode is NULL
 *               the pNewNode is added to the head of the hash bucket indexed by
 *               u1HashIndex, otherwise, the node is added after the pPrevNode
 * Input (s)   : pContext   - Pointer to the system context
 *               pPrevNode  - Pointer to the Node, after which the existing node
 *                            is to be added. May be NULL which means the
 *                            pNewNode is to be added at the head of the hash
 *                            bucket
 *               pNewNode   - Pointer to the adjacency Node to be added to the 
 *                            TENT
 *               u1HashIdx  - Index of the hash bucket where the new node is to
 *                            be added
 *               u1MetIdx   - Metric Index which specifies the index that 
 *                            holds values that are to be to updated in the 
 *                            arrays used in the SPT nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 *****************************************************************************/

PRIVATE VOID
IsisDecAddNodeToTent (tIsisSysContext * pContext, tIsisSPTNode * pPrevNode,
                      tIsisSPTNode * pNewNode, UINT1 u1HashIdx, UINT1 u1MetIdx)
{
    UINT4               u4RetValue = RB_FAILURE;
    UINT1               u1Count = 0;
    tIsisSPTNode       *pExistNode = NULL;

    UNUSED_PARAM (pPrevNode);
    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecAddNodeToTent ()\n"));

    if (u1HashIdx >= ISIS_MAX_BUCKETS)    /*klocwork */
    {
        return;
    }

    if (pNewNode->u1AddrType == ISIS_ADDR_IPV6)
    {
        u1MetIdx = 0;
    }
    for (u1Count = 0; u1Count < ISIS_NUM_METRICS; u1Count++)
    {
        /* Since a single node is used for holding information pertaining to a
         * destination in PATH database, we may have to initialise the metrics 
         * with invalid values in the beginning. These values get filled up when
         * the same destination is added to PATH for the corresponding metric
         */

        if (u1Count != u1MetIdx)
        {
            /* The current metric is already filled. Do not disturb that
             */
            pNewNode->au2Metric[u1Count] = ISIS_SPT_INV_METRIC;
        }
    }

    if ((pExistNode = RBTreeGet (pContext->Tent, pNewNode)) == NULL)
    {
        u4RetValue = RBTreeAdd (pContext->Tent, pNewNode);

    }
    else
    {
        while (pExistNode->pNext != NULL)
        {
            pExistNode = pExistNode->pNext;
        }
        pExistNode->pNext = pNewNode;
    }
    pNewNode->pNext = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecAddNodeToTent ()\n"));
    UNUSED_PARAM (u4RetValue);

}

/******************************************************************************
 * Function    : IsisDecAddNodeToPath () 
 * Description : This function adds the given SPT Node to the Path specified by
 *               'u1PathName;.
 * Input (s)   : pContext   - Pointer to System Context
 *               pPrevNode  - Pointer to the node whose next node must be
 *                            'pNode'
 *               pNode      - Pointer to the SPT Node that has to be added 
 *                            to the Path
 *               u1Loc      - Location of the SPT Node in the Path if it already
 *                            exists. If it has a value of
 *                            ISIS_SPT_LOC_PATH_SAME_FIRST or
 *                            ISIS_SPT_LOC_PATH_DIFF_FIRST, then 'pNode' is the
 *                            first element of the Hash Bucket
 *               u1Level    - Level of the database where the node is to be
 *                            added
 *               u1MetIdx   - Metric Index which specifies the location in the
 *                            metric array where the information from pNode is
 *                            to be updated (if required node already exists in
 *                            PATH)
 *               u1PathName - Specifies either OSI or IP path
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 *****************************************************************************/

PRIVATE INT4
IsisDecAddNodeToPath (tIsisSysContext * pContext, tIsisSPTNode * pPrevNode,
                      tIsisSPTNode * pNode, UINT1 u1Loc, UINT1 u1Level,
                      UINT1 u1MetIdx, UINT1 u1PathName, UINT1 u1MtIndex)
{
    tIsisSPTNode       *pExistNode = NULL;
    tIsisSPTNode       *pTempNode = NULL;
    tRBTree             RBTree;
    INT4                i4Retval = ISIS_SUCCESS;    /*klocwork */
    UINT4               u4RetValue = RB_FAILURE;

    if (u1MetIdx >= ISIS_NUM_METRICS)
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FAILURE;
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecAddNodeToPath ()\n"));

    switch (u1PathName)
    {
        case ISIS_OSI_PATH:

            RBTree = pContext->OSIPath;
            break;

        case ISIS_CURR_IP_PATH:

            if (u1Level == ISIS_LEVEL1)
            {
                RBTree = pContext->SysL1Info.ShortPath[u1MtIndex];
            }
            else
            {
                RBTree = pContext->SysL2Info.ShortPath[u1MtIndex];
            }
            if ((pNode->u1AddrType != ISIS_ADDR_IPV4)
                && (pNode->u1AddrType != ISIS_ADDR_IPV6))
            {
                return ISIS_FAILURE;
            }

            break;

        case ISIS_PREV_IP_PATH:

            if (u1Level == ISIS_LEVEL1)
            {
                RBTree = pContext->SysL1Info.PrevShortPath[u1MtIndex];
            }
            else
            {
                RBTree = pContext->SysL2Info.PrevShortPath[u1MtIndex];
            }

            break;

        default:

            DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Path [ %u ]\n", u1PathName));
            DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecAddNodeToPath ()\n"));
            return ISIS_SUCCESS;
    }

    ISIS_DBG_PRINT_ID (pNode->au1DestID, (UINT1) (ISIS_SYS_ID_LEN + 1),
                       "DEC <T> : Added SysID To PATH\t", ISIS_OCTET_STRING);

    switch (u1Loc)
    {
        case ISIS_SPT_LOC_PATH_DIFF_FIRST:

            /* A node already exist in the PATH as a first element, that matches
             * the new node which is being inserted, but for a different Metric.
             * Since a single node is used for all metrics pertaining to a
             * particular destination, we must update the information in the
             * existing node with the values retrieved from the new node
             *
             * NOTE: Since the existing element is the first element, pPrevNode
             *       will be NULL. Hence update the first element directly
             */

            pExistNode = RBTreeGet (RBTree, pNode);
            if (pExistNode == NULL)
            {
                /* klocwork fix. pExistNode cannot be null as the node 
                 * is already verified to be present in the tree */
                break;
            }
            pExistNode->au2Metric[u1MetIdx] = pNode->au2Metric[u1MetIdx];
            pExistNode->u4MetricVal = pNode->u4MetricVal;
            pExistNode->au4DirIdx[u1MetIdx] = pNode->au4DirIdx[u1MetIdx];
            pExistNode->au1MaxPaths[u1MetIdx] = pNode->au1MaxPaths[u1MetIdx];

            DEP_PT ((ISIS_LGST, "DEC <T> : Direction Index Updated - [ %u ]\n",
                     pExistNode->au4DirIdx[u1MetIdx]));
            break;

        case ISIS_SPT_LOC_PATH_DIFF:

            /* A node already exist in the PATH, that matches the new node which
             * is being inserted, but for a different metric. Since a single 
             * node is used for all metrics pertaining to a particular 
             * destination, we must update the information in the existing node
             * with the values retrieved from the new node. 
             *
             * NOTE: Only Metric, MaxPaths and direction will have to be updated
             */

            /* pPrevNode is always returned by the function 
             * IsisDecIsNodeInHashTbl () which points to the previous node
             * of the requested node. Hence pPrevNode->pNext is the actual
             * node we are interested in
             */
            if (pPrevNode != NULL)
            {
                pExistNode = pPrevNode->pNext;
                pExistNode->au2Metric[u1MetIdx] = pNode->au2Metric[u1MetIdx];
                pExistNode->u4MetricVal = pNode->u4MetricVal;
                pExistNode->au4DirIdx[u1MetIdx] = pNode->au4DirIdx[u1MetIdx];
                pExistNode->au1MaxPaths[u1MetIdx] =
                    pNode->au1MaxPaths[u1MetIdx];
            }
            else
            {
                pExistNode = RBTreeGet (RBTree, pNode);
                if (pExistNode != NULL)
                {
                    pExistNode->au2Metric[u1MetIdx] =
                        pNode->au2Metric[u1MetIdx];
                    pExistNode->u4MetricVal = pNode->u4MetricVal;
                    pExistNode->au4DirIdx[u1MetIdx] =
                        pNode->au4DirIdx[u1MetIdx];
                    pExistNode->au1MaxPaths[u1MetIdx] =
                        pNode->au1MaxPaths[u1MetIdx];
                }
            }
            DEP_PT ((ISIS_LGST, "DEC <T> : Direction Index Updated - [ %u ]\n",
                     pNode->au4DirIdx[u1MetIdx]));
            break;

        case ISIS_SPT_LOC_NONE:
        case ISIS_SPT_LOC_PATH_SAME:
        case ISIS_SPT_LOC_PATH_SAME_FIRST:

            /* There is no node in PATH, which matches the new node being
             * inserted, for any of the Metrics. 
             *                       (OR) 
             * A Node already exists for the given destination and Metric Type,
             * in the PATH, either at the head of the list or somewhere in the
             * List. This is the case of ECMP
             * Hence add it to the head of the list
             */

            pExistNode = RBTreeGet (RBTree, pNode);
            if (pExistNode == NULL)
            {
                u4RetValue = RBTreeAdd (RBTree, pNode);

            }
            else
            {
                if (pNode->u4MetricVal < pExistNode->u4MetricVal)
                {
                    RBTreeRemove (RBTree, pExistNode);
                    while (pExistNode != NULL)
                    {
                        pTempNode = pExistNode->pNext;
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pExistNode);
                        pExistNode = pTempNode;
                    }
                    u4RetValue = RBTreeAdd (RBTree, pNode);
                }

                else
                {
                    while (pExistNode->pNext != NULL)
                    {
                        pExistNode = pExistNode->pNext;
                    }
                    pExistNode->pNext = pNode;
                }
            }
            pNode->pNext = NULL;

            break;

        default:

            DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Location [ %u ]\n", u1Loc));
            break;
    }
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecAddNodeToPath ()\n"));
    UNUSED_PARAM (u4RetValue);
    return i4Retval;            /*klocwork */
}

/******************************************************************************
 * Function    : IsisDecRemNodeFromTent () 
 * Description : This function removes the given SPT Node from the TENT.
 * Input (s)   : pContext  - Pointer to System Context
 *               pPrevNode - The node which is previous to the node to be
 *                           removed.
 *               pTentNode - The Node in the Tent that is to be removed 
 *               u1HashIdx - Index into the hash table
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 *****************************************************************************/

PRIVATE VOID
IsisDecRemNodeFromTent (tIsisSysContext * pContext, tIsisSPTNode * pPrevNode,
                        tIsisSPTNode * pTentNode, UINT1 u1HashIdx)
{
    UINT4               u4RetValue = RB_FAILURE;
    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecRemNodeFromTent ()\n"));

    UNUSED_PARAM (pPrevNode);
    UNUSED_PARAM (u1HashIdx);

    RBTreeRemove (pContext->Tent, pTentNode);
    if (pTentNode->pNext != NULL)
    {
        u4RetValue = RBTreeAdd (pContext->Tent, pTentNode->pNext);
    }

    pTentNode->pNext = NULL;
    ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTentNode);

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecRemNodeFromTent ()\n"));
    UNUSED_PARAM (u4RetValue);

}

/******************************************************************************
 * Function    : IsisDecRemExcessPaths () 
 * Description : Based on the criteria 'Removal of Excess PATHs' as mentioned
 *               in section 7.2.7 of the ISO 10589 specification, this function
 *               processes the given two Nodes 'pNewNode' and 'pTentNode'
 *               and returns the node that has to pruned.
 * Input (s)   : pContext  - Pointer to System Context
 *               pTentNode - Pointer to a Node which already exist in TENT
 *               pNewNode  - Pointer to the New Node to be added to TENT 
 *               u1Level   - Level of the Database to be verified
 *               u1MetIdx  - Metric Index which specifies the index that 
 *                           holds values that are to be to updated in the 
 *                           arrays used in the SPT nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified.
 * Returns     : Pointer to the SPTNode that is to be pruned
 *****************************************************************************/

PRIVATE tIsisSPTNode *
IsisDecRemExcessPaths (tIsisSysContext * pContext, tIsisSPTNode * pTentNode,
                       tIsisSPTNode * pNewNode, UINT1 u1MetIdx)
{
    INT4                i4RetVal = 0;
    UINT1               u1TSOffset = 0;
    UINT1               u1NSOffset = 0;
    UINT1               au1TS[(2 * ISIS_SYS_ID_LEN) + ISIS_PNODE_ID_LEN +
                              ISIS_SNPA_ADDR_LEN];
    UINT1               au1NS[(2 * ISIS_SYS_ID_LEN) + ISIS_PNODE_ID_LEN +
                              ISIS_SNPA_ADDR_LEN];
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisAdjEntry      *pTentAdjEntry = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecRemExcessPaths ()\n"));

    /* Refer Sec. 7.2.7 of ISO 10589 */

    /* LOGIC: We have to prune excess paths based on the following:
     *        -- Adjacency Type
     *        -- Neighbour Id
     *        -- P2P CktId / LAN Ckt ID
     *        -- SNPA Address
     *
     *        To make the comparison logic simple, we form two strings from
     *        the above mentiomed information obtained from the two nodes. Then
     *        we do a Memory Compare of the two strings and prune which ever is
     *        lexicographically greater.
     */

    /* Get the corresponding Adjacency Records corresponding to the given nodes
     */
    if (u1MetIdx >= ISIS_NUM_METRICS)    /*klocwork */
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecRemExcessPaths ()\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return (pNewNode);
    }

    pAdjEntry = (tIsisAdjEntry *)
        IsisAdjGetAdjRecFromDirIdx (pContext, pNewNode->au4DirIdx[u1MetIdx]);
    if (pAdjEntry == NULL)
    {
        /* The new node is to be pruned compared to TENT node
         * as the Adjacency Record does not exist for 'pNewNode'
         */
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : No Adjacency Record Found - Direction Index [ %u ]\n",
                 pNewNode->au4DirIdx[u1MetIdx]));
        DETAIL_FAIL (ISIS_CR_MODULE);
        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecRemExcessPaths ()\n"));
        return (pNewNode);
    }

    pTentAdjEntry
        = (tIsisAdjEntry *)
        IsisAdjGetAdjRecFromDirIdx (pContext, pTentNode->au4DirIdx[u1MetIdx]);

    if (pTentAdjEntry == NULL)
    {
        /* The TENT node is to be pruned compared to new node
         * as the Adjacency Record does not exist for 'pTentNode'
         */
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : No Adjacency Record Found - Direction Index [ %u ]\n",
                 pTentNode->au4DirIdx[u1MetIdx]));
        DETAIL_FAIL (ISIS_CR_MODULE);
        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecRemExcessPaths ()\n"));
        return (pTentNode);
    }

    /* The order in which the strings are filled is the same order as psecified
     * in specs viz. AdjType, Neighbour ID, P2P or Local Circuit ID and SNPA
     * (for BC circuits)
     */

    /* Adding Adjacency Type */

    au1NS[0] = (UINT1) (ISIS_ADJ_SYS_TYPE (pNewNode->au2Metric[u1MetIdx]));
    u1NSOffset += 1;
    au1TS[0] = (UINT1) (ISIS_ADJ_SYS_TYPE (pTentNode->au2Metric[u1MetIdx]));
    u1TSOffset += 1;

    /* Update the Neighbour ID */

    MEMCPY ((au1NS + u1NSOffset), pAdjEntry->au1AdjNbrSysID, ISIS_SYS_ID_LEN);
    u1NSOffset += ISIS_SYS_ID_LEN;
    MEMCPY ((au1TS + u1NSOffset), pTentAdjEntry->au1AdjNbrSysID,
            ISIS_SYS_ID_LEN);
    u1TSOffset += ISIS_SYS_ID_LEN;

    /* Updating the New Node information
     */

    if (pAdjEntry->pCktRec->u1CktType == ISIS_BC_CKT)
    {
        /* For BC circuits we update Local Circuit ID and SNPA 
         */

        au1NS[u1NSOffset] = pAdjEntry->pCktRec->u1CktLocalID;
        u1NSOffset++;
        MEMCPY ((au1NS + u1NSOffset), pAdjEntry->au1AdjNbrSNPA,
                ISIS_SNPA_ADDR_LEN);
        u1NSOffset = (UINT1) (u1NSOffset + ISIS_SNPA_ADDR_LEN);
    }
    else
    {
        /* For P2P circuits we update P2P circuit ID and no SNPA
         */

        MEMCPY ((au1NS + u1NSOffset), pAdjEntry->pCktRec->au1P2PCktID,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
        u1NSOffset = (UINT1) (u1NSOffset + ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
    }

    /* Updating the TENT Node information
     */

    if (pTentAdjEntry->pCktRec->u1CktType == ISIS_BC_CKT)
    {
        /* For BC circuits we updtae Local Circuit ID and SNPA 
         */

        au1TS[u1TSOffset] = pTentAdjEntry->pCktRec->u1CktLocalID;
        u1TSOffset++;
        MEMCPY ((au1TS + u1TSOffset), pTentAdjEntry->au1AdjNbrSNPA,
                ISIS_SNPA_ADDR_LEN);
        u1TSOffset += (UINT1) (u1TSOffset + ISIS_SNPA_ADDR_LEN);
    }
    else
    {
        /* For P2P circuits we update P2P circuit ID and no SNPA
         */

        MEMCPY ((au1TS + u1TSOffset), pTentAdjEntry->pCktRec->au1P2PCktID,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
        u1TSOffset = (UINT1) (u1TSOffset + ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
    }
    u1TSOffset = MEM_MAX_BYTES (u1TSOffset,
                                ((2 * ISIS_SYS_ID_LEN) + ISIS_PNODE_ID_LEN +
                                 ISIS_SNPA_ADDR_LEN));
    i4RetVal = MEMCMP (au1NS, au1TS, u1TSOffset);
    ISIS_DBG_PRINT_ID (au1NS, (UINT1) u1NSOffset, "DEC <T> : New Node Info\t",
                       ISIS_OCTET_STRING);
    ISIS_DBG_PRINT_ID (au1TS, (UINT1) u1TSOffset, "DEC <T> : TENT Node Info\t",
                       ISIS_OCTET_STRING);
    if (i4RetVal < 0)
    {
        /* The new node is to be pruned compared to TENT node
         */

        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecRemExcessPaths ()\n"));
        return (pNewNode);
    }
    else
    {
        /* The TENT node is to be pruned compared to new node
         */

        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecRemExcessPaths ()\n"));
        return (pTentNode);
    }
}

/******************************************************************************
 * Function    : IsisDecGetMCNodeFromTent () 
 * Description : This function gets the minimum cost Node from the Tent
 *               Database. If more than one node exists as a minimum cost 
 *               node, then all the nodes are returned as a list of SPT 
 *               Nodes. The minimum cost node is the first node in
 *               any hash bucket of the hash table
 * Input (s)   : pContext     - Pointer to System Context
 *               u1MetIdx     - Metric Index which specifies the index that 
 *                              holds values that are to be to updated in the 
 *                              arrays used in the SPT nodes
 * Output (s)  : pMinTentNode - Pointer to the Minimum cost node returned
 *               pu1MinBktIdx - Index of the Hash Bucket from where the
 *                              Minimum Cost node is retrieved
 * Globals     : Not referred or modified
 * Returns     : VOID
 *****************************************************************************/

PRIVATE VOID
IsisDecGetMCNodeFromTent (tIsisSysContext * pContext, UINT1 u1MetIdx,
                          tIsisSPTNode ** pMinTentNode, UINT1 *pu1MinBktIdx)
{
    tIsisSPTNode       *pRetNode = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecGetMCNodeFromTent ()\n"));

    UNUSED_PARAM (u1MetIdx);
    UNUSED_PARAM (pu1MinBktIdx);

    pRetNode = (tIsisSPTNode *) RBTreeGetFirst (pContext->Tent);

    *pMinTentNode = pRetNode;

    if (pRetNode != NULL)
    {
        ISIS_DBG_PRINT_ID (pRetNode->au1DestID, (ISIS_SYS_ID_LEN + 1),
                           "DEC <T> : Minimum Cost Node Retrieved\t",
                           ISIS_OCTET_STRING);

        RBTreeRemove (pContext->Tent, pRetNode);
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecGetMCNodeFromTent ()\n"));
}

/*******************************************************************************
 * Function    : IsisDecIsNodeInPATH () 
 * Description : This routine verifies the presence of the given Node either in
 *               OSI or IP PATH database. If the given node is found, then the 
 *               routine outputs a pointer, in pRetNode, which points to a node
 *               which is previous to the requested node i.e. pRetNode->pNext 
 *               will be the requested node. 
 * Input (s)   : pContext   - Pointer to the System context
 *               pSptNode   - Pointer to the SPT Node whose existence 
 *                            is to be verified
 *               u1Level    - Level of the database to be verified 
 *               u1MetIdx   - Metric Index which specifies the index that 
 *                            holds values that are to be to updated in the 
 *                            arrays used in the SPT nodes
 *               u1HashIdx  - Index of the Hash Table
 * Output (s)  : pRetNode   - Pointer to the previous Node if it exists in PATH
 * Globals     : Not Referred or Modified
 * Returns     : It returns the location of the Node as
 *               -- ISIS_SPT_LOC_NONE
 *                          - if the requested node is not found in PATH for
 *                            the given metric
 *               -- ISIS_SPT_LOC_PATH_SAME
 *                          - if the requested node is found either in OSI or
 *                            IP PATH for the given metric
 *               -- ISIS_SPT_LOC_PATH_SAME_FIRST
 *                          - if the requested node is found as the first node
 *                            in any bucket either in the OSI or IP PATH for
 *                            the given metric
 *               -- ISIS_SPT_LOC_PATH_DIFF
 *                          - if the requested node is found either in OSI or
 *                            IP PATH for the Different metric
 *               -- ISIS_SPT_LOC_PATH_DIFF_FIRST
 *                          - if the requested node is found as the first node
 *                            in any bucket either in the OSI or IP PATH for
 *                            the Different metric
 ******************************************************************************/

PRIVATE UINT1
IsisDecIsNodeInPATH (tIsisSysContext * pContext, tIsisSPTNode * pSptNode,
                     UINT1 u1Level, UINT1 u1MetIdx, tIsisSPTNode ** pRetNode,
                     UINT1 u1HashIdx, UINT1 u1MtIndex)
{
    UINT1               u1Loc = ISIS_SPT_LOC_NONE;
    UINT1               u1Found = ISIS_FALSE;
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pChkTrav = NULL;
    tRBTree             RBTree;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecIsNodeInPATH ()\n"));

    UNUSED_PARAM (u1HashIdx);
    /* Verify the existence for the 'pSptNode' in the IP Path
     */
    /*klockwork */
    if (u1MetIdx >= ISIS_NUM_METRICS)
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_SPT_LOC_NONE;
    }

    RBTree =
        (u1Level ==
         ISIS_LEVEL1) ? (pContext->SysL1Info.ShortPath[u1MtIndex]) : (pContext->
                                                                      SysL2Info.
                                                                      ShortPath
                                                                      [u1MtIndex]);

    pTrav = RBTreeGet (RBTree, pSptNode);

    if (pTrav != NULL)
    {
        ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                           (UINT1) (ISIS_SYS_ID_LEN + 1),
                           "DEC <T> : Given Node Found In IP PATH\t",
                           ISIS_OCTET_STRING);

        /* We can update the pRetNode with the previous pointer
         */

        /* Checking the destination exists for the same next hop 
         * address index
         */
        if (pTrav->au4DirIdx[u1MetIdx] == pSptNode->au4DirIdx[u1MetIdx])

        {
            u1Loc = ISIS_SPT_LOC_PATH_SAME_DIRIDX;
            return u1Loc;
        }
        else
        {
            pChkTrav = pTrav;
            while (pChkTrav->pNext != NULL)
            {
                if (MEMCMP (pChkTrav->au1DestID, pChkTrav->pNext->au1DestID,
                            (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0)
                {
                    if (pChkTrav->pNext->au4DirIdx[u1MetIdx] ==
                        pSptNode->au4DirIdx[u1MetIdx])
                    {
                        u1Loc = ISIS_SPT_LOC_PATH_SAME_DIRIDX;
                        return u1Loc;
                    }
                }
                pChkTrav = pChkTrav->pNext;
            }

        }

        if (pSptNode->u4MetricVal <= pTrav->u4MetricVal)
        {
            u1Loc = ISIS_SPT_LOC_NONE;
            return u1Loc;
        }

        u1Loc = ((pTrav->au2Metric[u1MetIdx] !=
                  ISIS_SPT_INV_METRIC) ?
                 ISIS_SPT_LOC_PATH_SAME : ISIS_SPT_LOC_PATH_DIFF);
        u1Found = ISIS_TRUE;
    }

    if (u1Found != ISIS_TRUE)
    {
        *pRetNode = NULL;

        /* Given node not in IP PATH, verify OSI PATH also
         */

        pTrav = RBTreeGet (pContext->OSIPath, pSptNode);

        if (pTrav != NULL)
        {
            ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                               (UINT1) (ISIS_SYS_ID_LEN + 1),
                               "DEC <T> : Given Node Found In OSI PATH\t",
                               ISIS_OCTET_STRING);

            if (pTrav->au4DirIdx[u1MetIdx] == pSptNode->au4DirIdx[u1MetIdx])

            {
                u1Loc = ISIS_SPT_LOC_PATH_SAME_DIRIDX;
                return u1Loc;
            }

            if (pSptNode->u4MetricVal <= pTrav->u4MetricVal)
            {
                u1Loc = ISIS_SPT_LOC_NONE;
                return u1Loc;
            }

            /* We can update the pRetNode with the previous pointer
             */
            u1Loc = ((pTrav->au2Metric[u1MetIdx] !=
                      ISIS_SPT_INV_METRIC) ?
                     ISIS_SPT_LOC_PATH_SAME : ISIS_SPT_LOC_PATH_DIFF);
            u1Found = ISIS_TRUE;
        }
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecIsNodeInPATH ()\n"));
    return u1Loc;
}

/*******************************************************************************
 * Function    : IsisDecIsNodeInHashTbl () 
 * Description : This routine verifies the presence of the given Node either in
 *               TENT. If the node is not found in TENT, then,
 *               'IsisDecIsNodeInPATH ()' function is called to verify the
 *               presence of the Node in the PATH database
 *               (both OSI Path and IP Path).
 *               If the given node is found, then the routine outputs a
 *               pointer, in 'pRetNode', which points to a node which is
 *               previous to the requested node i.e. pRetNode->pNext will be
 *               the requested node.
 *               If the the given node is not found in TENT as well as in PATH,
 *               then the routine returns a pointer in 'pRetNode', which must
 *               be the previous node to the given node in TENT.
 *               If the given node is inserted in TENT subsequently, i.e. it
 *               reduces the job of the calling function by finding the
 *               appropriate position where the given node can be inserted.
 *               Otherwise the calling function, if it intends to insert the
 *               given node in TENT, must do insertion sort with the Metric,
 *               since TENT is sorted on Metrics
 * Input (s)   : pContext   - Pointer to the System context
 *               pSptNode   - Pointer to the SPT Node whose existence 
 *                            is to be verified
 *               u1Level    - Level of the database to be verified 
 *               u1MetIdx   - Metric Index which specifies the index that 
 *                            holds values that are to be to updated in the 
 *                            arrays used in the SPT nodes
 *               u1HashIdx  - Index of the Hash Table
 * Output (s)  : pRetNode   - Pointer to the Node if it exists in the TENT or 
 *                            PATH
 *               pu1HashIdx - Pointer to the Index of the Hash Table
 * Globals     : Not Referred or Modified
 * Returns     : It returns the location of the Node as
 *               -- ISIS_SPT_LOC_NONE
 *                          - if the requested node is not found in PATH for
 *                            the given metric
 *               -- ISIS_SPT_LOC_TENT
 *                          - if the Node is present in TENT
 *               -- ISIS_SPT_LOC_PATH_SAME
 *                          - if the requested node is found either in OSI or
 *                            IP PATH for the given metric
 *               -- ISIS_SPT_LOC_PATH_SAME_FIRST
 *                          - if the requested node is found as the first node
 *                            in any bucket either in the OSI or IP PATH for
 *                            the given metric
 *               -- ISIS_SPT_LOC_PATH_DIFF
 *                          - if the requested node is found either in OSI or
 *                            IP PATH for the Different metric
 *               -- ISIS_SPT_LOC_PATH_DIFF_FIRST
 *                          - if the requested node is found as the first node
 *                            in any bucket either in the OSI or IP PATH for
 *                            the Different metric
 ******************************************************************************/

PRIVATE UINT1
IsisDecIsNodeInHashTbl (tIsisSysContext * pContext, tIsisSPTNode * pSptNode,
                        UINT1 u1Level, UINT1 u1MetIdx, tIsisSPTNode ** pRetNode,
                        UINT1 *pu1HashIdx, UINT1 u1MtIndex)
{
    UINT1               u1Loc = ISIS_SPT_LOC_NONE;
    UINT1               u1Found = ISIS_FALSE;
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pPrev = NULL;
    tIsisSPTNode       *pChkTrav = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecIsNodeInHashTbl ()\n"));
    UNUSED_PARAM (pRetNode);
    UNUSED_PARAM (pu1HashIdx);

    if (u1MetIdx >= ISIS_NUM_METRICS)    /*klocwork */
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return u1Loc;
    }
    /* Search the TENT database. If requested node is found, pRetNode will be
     * updated with the pointer of the previous node, to facilitate deleting the
     * matched node later. The other scenario where this previous pointer will
     * help is when inserting the Node for the first time. If the node is not
     * found we have to return ISIS_SPT_LOC_NONE. The calling routine must then
     * insert the new node at appropriate position by insertion sort. To avoid
     * that, the current routine updates pRetNode with appropriate pointer so
     * that the calling routine can use this pointer to insert the new node
     * without any insertion sort
     *
     * NOTE: Nodes in TENT are sorted on Metric
     */

    pTrav = RBTreeGet (pContext->Tent, pSptNode);
    if (pTrav != NULL)
    {
        /* Node is found. Update pRetNode with the pPrev
         */
        if ((pTrav->au4DirIdx[u1MetIdx] == pSptNode->au4DirIdx[u1MetIdx])
            && (pTrav->au2Metric[u1MetIdx] <= pSptNode->au2Metric[u1MetIdx])
            && (pTrav->u4MetricVal <= pSptNode->u4MetricVal))
        {
            u1Loc = ISIS_SPT_LOC_PATH_SAME_DIRIDX;
            return (u1Loc);
        }
        else
        {
            pChkTrav = pTrav;
            while (pChkTrav->pNext != NULL)
            {
                if (MEMCMP (pChkTrav->au1DestID, pChkTrav->pNext->au1DestID,
                            (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0)
                {
                    if ((pChkTrav->pNext->au4DirIdx[u1MetIdx] ==
                         pSptNode->au4DirIdx[u1MetIdx])
                        && (pTrav->au2Metric[u1MetIdx] <=
                            pSptNode->au2Metric[u1MetIdx])
                        && (pTrav->u4MetricVal <= pSptNode->u4MetricVal))

                    {
                        u1Loc = ISIS_SPT_LOC_PATH_SAME_DIRIDX;
                        return u1Loc;
                    }
                }
                pChkTrav = pChkTrav->pNext;
            }
        }

        ISIS_DBG_PRINT_ID (pSptNode->au1DestID,
                           (UINT1) (ISIS_SYS_ID_LEN + 1),
                           "DEC <T> : Given Node Found In TENT\t",
                           ISIS_OCTET_STRING);
        u1Loc = ISIS_SPT_LOC_TENT;
        u1Found = ISIS_TRUE;
    }

    /* If we have already found a node in TENT, we need not worry about
     * PATH, since it will not be in PATH
     */

    /* Verify both OSI and IP PATHs
     */

    if (u1Found != ISIS_TRUE)
    {

        u1Loc = IsisDecIsNodeInPATH (pContext, pSptNode, u1Level, u1MetIdx,
                                     &pPrev, *pu1HashIdx, u1MtIndex);
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecIsNodeInHashTbl ()\n"));
    return u1Loc;
}

/******************************************************************************
 * Function    : IsisDecUpdateRoute () 
 * Description : This function compares the Current Path with the Previous Path
 *               and the Route is updated in the Previous Path as 
 *                - if the Previous Path is empty, then all the nodes in the
 *                  Current Path are added to the Previous Path. The newly
 *                  added nodes are marked to indicate that the nodes have been
 *                  visited so that these nodes are not processed after SPT
 *                  Completion.
 *                - if ECMP (Equal Cost Multi Path) exists in the Previous Path
 *                  and Current Path, the ECMP is collected from both the Paths
 *                  and then compared
 * Input (s)   : pContext - Pointer to System Context
 *               u1MetIdx - Metric Index which specifies the Metric Type for
 *                          which the Route updation is to be performed
 *               u1Level  - Level of the database being manipulated
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 *****************************************************************************/

PRIVATE VOID
IsisDecUpdateRoute (tIsisSysContext * pContext, UINT1 u1MetIdx,
                    UINT1 u1Level, UINT1 u1MtIndex)
{
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pPrev = NULL;
    tIsisSPTNode       *pCurrList = NULL;
    tIsisSPTNode       *pPrevList = NULL;
    tRBTree             PrevRBTree;
    tRBTree             CurrRBTree;
    UINT4               u4RetValue;
    UINT1               u1IsEcmp = ISIS_FALSE;
    tIsisSPTNode       *pNext = NULL;

#ifdef ROUTEMAP_WANTED
    tIsisRouteInfo      RouteInfo;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    UINT4               u4CktIfIndex = 0;
    UINT1               u1MetricType = 0;
    UINT1               u1PrefLen = 0;

    MEMSET (&RouteInfo, 0, sizeof (tIsisRouteInfo));
#endif
    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecUpdateRoute ()\n"));

    if (u1MetIdx >= ISIS_NUM_METRICS)    /*klocwork */
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return;
    }

    PrevRBTree =
        (u1Level ==
         ISIS_LEVEL1) ? pContext->SysL1Info.
        PrevShortPath[u1MtIndex] : pContext->SysL2Info.PrevShortPath[u1MtIndex];
    CurrRBTree =
        (u1Level ==
         ISIS_LEVEL1) ? pContext->SysL1Info.ShortPath[u1MtIndex] : pContext->
        SysL2Info.ShortPath[u1MtIndex];

    UINT4               u4RIBCount = 0;
    UINT4               u4SPTCount = 0;
    UINT4               u4NodeCount = 0;
    RBTreeCount (PrevRBTree, &u4RIBCount);
    RBTreeCount (CurrRBTree, &u4SPTCount);
    RBTreeCount (pContext->OSIPath, &u4NodeCount);

    DEP_PT ((ISIS_LGST, "DEC <T> : Route Count in IS-IS RIB MT[%d]: %d;"
             " Route Count in current iteration of SPF: %d \n", u1MtIndex,
             u4RIBCount, u4SPTCount));

    DEP_PT ((ISIS_LGST, "DEC <T> : Number of nodes in the SPF topology: %d"
             " (Including Pseudonode Systems and Self) \n", u4NodeCount));

    pTrav = RBTreeGetFirst (CurrRBTree);

    while (pTrav != NULL)
    {
        if (u1IsEcmp == ISIS_FALSE)
        {
            RBTreeRemove (CurrRBTree, pTrav);
        }

        if (pTrav->pNext != NULL)
        {
            u1IsEcmp = ISIS_TRUE;
        }
        else
        {
            u1IsEcmp = ISIS_FALSE;
        }

#ifdef ROUTEMAP_WANTED
        if (pTrav->au4DirIdx[u1MetIdx] != 0)
        {
            pAdjEntry = IsisAdjGetAdjRecFromDirIdx (pContext,
                                                    pTrav->au4DirIdx[u1MetIdx]);
        }
        if (pTrav->u1AddrType == ISIS_ADDR_IPV4)
        {
            IsisUtlComputePrefixLen (&pTrav->au1DestID[ISIS_MAX_IPV4_ADDR_LEN],
                                     ISIS_MAX_IPV4_ADDR_LEN, &u1PrefLen);
            RouteInfo.DestIpAddr.u1PrefixLen = u1PrefLen;
            MEMCPY (RouteInfo.DestIpAddr.au1IpAddr, pTrav->au1DestID,
                    ISIS_MAX_IPV4_ADDR_LEN);

            if (pAdjEntry != NULL)
            {
                MEMCPY (RouteInfo.au1IPAddr,
                        pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                        ISIS_MAX_IPV4_ADDR_LEN);
                u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
                if (NetIpv4GetPortFromIfIndex
                    (u4CktIfIndex, &RouteInfo.u4CktIfIndex) == ISIS_FAILURE)
                {
                    DEP_PT ((ISIS_LGST,
                             "DEC <T> : Failed to update Route Info () \n"));
                }
            }
            RouteInfo.u4MetricVal = pTrav->u4MetricVal;
            RouteInfo.u1Level = u1Level;

        }
        else if (pTrav->u1AddrType == ISIS_ADDR_IPV6)
        {
            IsisUtlComputePrefixLen (&pTrav->au1DestID
                                     [ISIS_MAX_IPV6_ADDR_LEN],
                                     ISIS_MAX_IPV6_ADDR_LEN,
                                     &(RouteInfo.DestIpAddr.u1PrefixLen));

            MEMCPY (RouteInfo.DestIpAddr.au1IpAddr, pTrav->au1DestID,
                    ISIS_MAX_IPV6_ADDR_LEN);
            if (pAdjEntry != NULL)
            {
                MEMCPY (RouteInfo.au1IPAddr,
                        pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                        ISIS_MAX_IPV6_ADDR_LEN);
                RouteInfo.u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
                NetIpv6GetIfInfo (pAdjEntry->pCktRec->u4CktIfIdx,
                                  &NetIpv6IfInfo);
                RouteInfo.u4CktIfIndex = NetIpv6IfInfo.u4IpPort;

            }
            RouteInfo.u4MetricVal = pTrav->u4MetricVal;
            RouteInfo.u1Level = u1Level;

        }
        RouteInfo.DestIpAddr.u1AddrType = pTrav->u1AddrType;

        if ((pTrav->au2Metric[0] & ISIS_SPT_EXT_MET_FLAG)
            == ISIS_SPT_EXT_MET_FLAG)
        {
            /* Metric type: Supported only External type1. */
            u1MetricType = RMAP_METRIC_TYPE_TYPE1EXTERNAL;
        }
        else if (u1Level == ISIS_LEVEL1)
        {
            u1MetricType = RMAP_METRIC_TYPE_INTRA_AREA;
        }
        else if (u1Level == ISIS_LEVEL2)
        {
            u1MetricType = RMAP_METRIC_TYPE_INTER_AREA;
        }
        if (pTrav->au4DirIdx[u1MetIdx] != 0)
        {
            if (ISIS_FAILURE ==
                IsisApplyInFilter (pContext->pDistributeInFilterRMap,
                                   &RouteInfo, u1MetricType))
            {
                /* Stop processing this route and continue with next route */
                if (u1IsEcmp == ISIS_TRUE)
                {
                    pNext = pTrav->pNext;
                }
                else
                {
                    pNext = RBTreeGetFirst (CurrRBTree);
                }
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTrav);
                pTrav = pNext;
                continue;
            }

        }
        pTrav->u4MetricVal = RouteInfo.u4MetricVal;
#endif

        pCurrList = pTrav;
        pPrevList = (tIsisSPTNode *) RBTreeGetFirst (PrevRBTree);

        if (pPrevList == NULL)
        {

            /* There is nothing in the Hash bucket where the Previous list
             * should have existed. This means the pCurrList points to a set
             * of nodes which are entirely new. so add them to the hash
             * bucket unconditionally
             */

            /* Mark these nodes so that these will get updated into the
             * routing table once the verification process is complete
             *
             * As these nodes are new nodes added for this Destination,
             * send these to the Routing Table as New Routes to the
             * Destination ID
             */

            ISIS_MARK_PATH (pCurrList, u1MetIdx, ISIS_SPT_MARK);
            ISIS_DECN_MARK_CHG (pCurrList, ISIS_SPT_NEW_FLAG);

            pCurrList->pNext = NULL;
            u4RetValue = RBTreeAdd (PrevRBTree, pCurrList);

            IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_ADD,
                                  pCurrList, u1MetIdx, u1Level, u1MtIndex);
            if (u1IsEcmp == ISIS_TRUE)
            {
                pNext = pTrav->pNext;
            }

        }
        else
        {
            /* The previous path has some nodes in the hash bucket. Try to
             * match the pCurrList with the existing nodes in the Previous
             * path. Get the pointer to the first matching node from the
             * previous path or the last node from the previous path if none
             * of the nodes match
             */

            if (u1IsEcmp == ISIS_TRUE)
            {
                pNext = pTrav->pNext;
            }
            pPrev = RBTreeGet (PrevRBTree, pCurrList);
            u4RetValue = IsisDecUpdatePrevPath (pContext, pCurrList, pPrev,
                                                pPrevList, u1MetIdx, u1Level,
                                                u1MtIndex);
            pPrev = NULL;
        }
        if (u1IsEcmp == ISIS_TRUE)
        {
            if (u4RetValue == ISIS_FALSE)
            {
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTrav);
            }
            pTrav = pNext;
        }
        else
        {
            if (u4RetValue == ISIS_FALSE)
            {
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTrav);
            }
            pTrav = RBTreeGetFirst (CurrRBTree);
        }

    }

    UNUSED_PARAM (u4RetValue);
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecUpdateRoute ()\n"));
}

/******************************************************************************
 * Function    : IsisDecUpdatePrevPath () 
 * Description : This function compares calculated Short Path Database 
 *               with the Previous Path Database and updates the changed 
 *               ones alone. The updated nodes are marked as visited nodes
 * Input (s)   : pContext  - Pointer to System Context
 *               pCurrList - Pointer to the Calculated shortest Path
 *                           Database Node
 *               pPrev     - Pointer to the Previous pointer of the pPrevList
 *               pPrevList - Pointer to the Previous shortest path 
 *                           Database Node
 *               u1MetIdx  - Metric Index which specifies the index that 
 *                           holds values that are to be to updated in the 
 *                           arrays used in the SPT nodes
 *               u1Level   - Level of the database for which the Previous Path
 *                           is updated
 * Output (s)  : None
 * Globals     : None is referred or modified
 * Returns     : VOID
 *****************************************************************************/

PUBLIC UINT4
IsisDecUpdatePrevPath (tIsisSysContext * pContext, tIsisSPTNode * pCurrList,
                       tIsisSPTNode * pPrev, tIsisSPTNode * pPrevList,
                       UINT1 u1MetIdx, UINT1 u1Level, UINT1 u1MtIndex)
{
    UINT1               u1Loc = ISIS_SPT_LOC_NONE;
    UINT1               u1ChgFlag = 0;
    UINT4               u4TravPPMetric = 0;
    UINT4               u4TravMetric = 0;
    UINT1               u1IsSameRoute = ISIS_FALSE;
    tIsisAdjEntry      *pCurrAdjEntry = NULL;
    tIsisAdjDelDirEntry *pDelDirEntry = NULL;
    tIsisSPTNode       *pTrav = NULL;
    tIsisSPTNode       *pPrevNode = NULL;
    tIsisSPTNode       *pTravPPNode = NULL;
    tIsisSPTNode       *pTempNode = NULL;
    tIsisSPTNode       *pNextNode = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tRBTree             PrevRBTree;
    UINT4               u4RetVal;
    UINT1               u1IsEcmp = ISIS_FALSE;
    /* Compare the Set of Nodes from Path to Previous Path database
     * And Mark Previous Path Database if any Matching Nodes are found
     */

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecUpdatePrevPath ()\n"));
    UNUSED_PARAM (pPrevList);

    pPrevNode = NULL;
    pTrav = pCurrList;

    if (u1MetIdx >= ISIS_NUM_METRICS)    /*klocwork */
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid Metric Type [%u]\n", u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FALSE;
    }

    /* Scanning the Current List */
    PrevRBTree = (u1Level == ISIS_LEVEL1) ?
        pContext->SysL1Info.PrevShortPath[u1MtIndex] :
        pContext->SysL2Info.PrevShortPath[u1MtIndex];

    pCurrAdjEntry = (tIsisAdjEntry *)
        IsisAdjGetAdjRecFromDirIdx (pContext, pTrav->au4DirIdx[u1MetIdx]);

    pTravPPNode = pPrev;
    if ((pTravPPNode) && (pTravPPNode->pNext))
    {
        u1IsEcmp = ISIS_TRUE;
    }
    while (pTravPPNode != NULL)
    {
        u1IsSameRoute = ISIS_FALSE;
        if (pTrav->u4MetricVal == pTravPPNode->u4MetricVal)
        {
            if (pTrav->au4DirIdx[u1MetIdx] == pTravPPNode->au4DirIdx[u1MetIdx])
            {
                u1IsSameRoute = ISIS_TRUE;
            }
            else
            {
                pDelDirEntry = pContext->pDelDirEntry;

                while (pDelDirEntry != NULL)
                {
                    if ((pDelDirEntry->u4DirIdx ==
                         pTravPPNode->au4DirIdx[u1MetIdx])
                        && (pCurrAdjEntry != NULL))
                    {
                        if (pTrav->u1AddrType == ISIS_ADDR_IPV4)
                        {
                            if (MEMCMP (pDelDirEntry->au1AdjNbrIpV4Addr,
                                        pCurrAdjEntry->AdjNbrIpV4Addr.
                                        au1IpAddr, ISIS_MAX_IPV4_ADDR_LEN) == 0)
                            {
                                u1IsSameRoute = ISIS_TRUE;
                            }
                        }
                        else
                        {
                            if (MEMCMP (pDelDirEntry->au1AdjNbrIpV6Addr,
                                        pCurrAdjEntry->AdjNbrIpV6Addr.
                                        au1IpAddr, ISIS_MAX_IPV6_ADDR_LEN) == 0)
                            {
                                u1IsSameRoute = ISIS_TRUE;
                            }

                        }
                        break;
                    }
                    pDelDirEntry = pDelDirEntry->pNext;
                }
            }
            /* same node exists in the previous path so No need to
             * Update the IP Routing table  for this one
             */
            if (u1IsSameRoute == ISIS_TRUE)
            {
                ISIS_MARK_PATH (pTravPPNode, u1MetIdx, ISIS_SPT_MARK);
                ISIS_MARK_PATH (pTrav, u1MetIdx, ISIS_SPT_MARK);

                pTravPPNode->au1MaxPaths[u1MetIdx] =
                    pTrav->au1MaxPaths[u1MetIdx];
                break;
            }
        }
        else if (pTravPPNode->au2Metric[u1MetIdx] == ISIS_SPT_INV_METRIC)
        {
            u1Loc = ISIS_SPT_LOC_PATH_DIFF;
            IsisDecAddNodeToPath (pContext, pPrevNode, pTrav, u1Loc,
                                  u1Level, u1MetIdx,
                                  ISIS_PREV_IP_PATH, u1MtIndex);
            ISIS_MARK_PATH (pTrav, u1MetIdx, ISIS_SPT_MARK);
            ISIS_MARK_PATH (pTravPPNode, u1MetIdx, ISIS_SPT_MARK);

            if (ISIS_DECN_GET_MARK (pTravPPNode) == ISIS_SPT_UNCHG_FLAG)
            {
                ISIS_DECN_MARK_CHG (pTravPPNode, ISIS_SPT_MOD_FLAG);
            }
            break;
        }
        else if (((pTravPPNode->au2Metric[u1MetIdx] & ISIS_SPT_MARK) !=
                  ISIS_SPT_MARK) && (u1IsEcmp != ISIS_TRUE))
        {
            /* Only Metric Change has to be given the 
             * route update status else do explicit 
             * deletion and addition
             */
            u4TravMetric = pTrav->u4MetricVal;
            u4TravPPMetric = pTravPPNode->u4MetricVal;
            if ((u4TravMetric != u4TravPPMetric)
                && (pTrav->au4DirIdx[u1MetIdx] == pTravPPNode->
                    au4DirIdx[u1MetIdx]))
            {
                pTravPPNode->au2Metric[u1MetIdx] = pTrav->au2Metric[u1MetIdx];
                pTravPPNode->u4MetricVal = pTrav->u4MetricVal;
                u1ChgFlag |= ISIS_SPT_ROUTE_MET_CHG;
                ISIS_MARK_PATH (pTravPPNode, u1MetIdx, ISIS_SPT_MARK);
                ISIS_MARK_PATH (pTrav, u1MetIdx, ISIS_SPT_MARK);
                pTravPPNode->au1MaxPaths[u1MetIdx] =
                    pTrav->au1MaxPaths[u1MetIdx];
                if (ISIS_DECN_GET_MARK (pTravPPNode) == ISIS_SPT_UNCHG_FLAG)
                {
                    ISIS_DECN_MARK_CHG (pTravPPNode, ISIS_SPT_MOD_FLAG);
                }

                /* Send the changed Route Information for this
                 * Destination
                 */
                /* Ideally i would have relinquished here also if it takes too
                 * much time to update my previous path 
                 * but idon't want to become too complex so better i
                 * send hellos mean while to retain my adjacencies 
                 */
                gu4TestCnt++;

                if (gu4TestCnt == 5000)
                {
                    pCktRec = pContext->CktTable.pCktRec;
                    while (pCktRec != NULL)
                    {

                        if ((pCktRec->u1CktLevel == ISIS_LEVEL1) ||
                            (pCktRec->u1CktLevel == ISIS_LEVEL12))
                        {
                            IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1,
                                            ISIS_FALSE);
                        }

                        if ((pCktRec->u1CktLevel == ISIS_LEVEL2) ||
                            (pCktRec->u1CktLevel == ISIS_LEVEL12))
                        {
                            IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2,
                                            ISIS_FALSE);
                        }

                        pCktRec = pCktRec->pNext;
                    }
                    gu4TestCnt = 0;
                }
                IsisDecFormRouteInfo (pContext, u1ChgFlag, pTravPPNode,
                                      u1MetIdx, u1Level, u1MtIndex);
                u1ChgFlag = 0;
                break;
            }
            else
            {
                RBTreeRemove (PrevRBTree, pTravPPNode);
                IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_DELETE,
                                      pTravPPNode, u1MetIdx, u1Level,
                                      u1MtIndex);
                ISIS_DECN_MARK_CHG (pTrav, ISIS_SPT_MOD_FLAG);
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTravPPNode);
                break;
            }
        }
        else if ((u1IsEcmp == ISIS_TRUE) &&
                 (pTravPPNode->u4MetricVal != pTrav->u4MetricVal))
        {
            /* Blindly delete the prev ECMP list incase of metric mismatch */
            pTempNode = pTravPPNode;
            RBTreeRemove (PrevRBTree, pTravPPNode);
            while (pTempNode != NULL)
            {
                pNextNode = pTempNode->pNext;
                IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_DELETE,
                                      pTempNode, u1MetIdx, u1Level, u1MtIndex);
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTempNode);
                pTempNode = pNextNode;
            }
            ISIS_DECN_MARK_CHG (pTrav, ISIS_SPT_MOD_FLAG);
            break;
        }
        pPrevNode = pTravPPNode;
        pTravPPNode = pTravPPNode->pNext;
    }

    /* Add the New Nodes into Previous Path which are either New or
     * different from the existing one 
     */

    pTrav = pCurrList;

    if ((pTrav->au2Metric[u1MetIdx] & ISIS_SPT_MARK) != ISIS_SPT_MARK)
    {
        /* This is a new node computed in the current decision 
         * so mark that node so that we can update that to
         * ip routing table and add this node to Prev Path
         */
        ISIS_DECN_MARK_CHG (pTrav, ISIS_SPT_NEW_FLAG);
        ISIS_MARK_PATH (pTrav, u1MetIdx, ISIS_SPT_MARK);
        if ((pPrevNode = RBTreeGet (PrevRBTree, pTrav)) == NULL)
        {
            u4RetVal = RBTreeAdd (PrevRBTree, pTrav);

        }
        else
        {
            while (pPrevNode->pNext != NULL)
            {
                pPrevNode = pPrevNode->pNext;
            }
            pPrevNode->pNext = pTrav;
        }
        pTrav->pNext = NULL;

        IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_ADD,
                              pTrav, u1MetIdx, u1Level, u1MtIndex);

    }
    else
    {
        return ISIS_FALSE;
    }

    UNUSED_PARAM (u4RetVal);
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecUpdatePrevPath ()\n"));
    return ISIS_TRUE;
}

/******************************************************************************
 * Function    : IsisDecFormRouteInfo ()
 * Description : This function forms the RouteInfo structure to update the
 *               Routing Table
 * Input (s)   : pContext  - Pointer to System Context
 *               u1BitMask - Type of the Route update
 *                             - ISIS_SPT_ROUTE_ADD 
 *                                     New Route to be added
 *                             - ISIS_SPT_ROUTE_DELETE 
 *                                     Existing Route to be deleted
 *                             - ISIS_SPT_ROUTE_MET_CHG
 *                                     Metric Change in the Existing Route
 *                             - ISIS_SPT_ROUTE_IFIDX_CHG
 *                                     Interface Idx Change in the Existing
 *                                     Route
 *               pNode     - Pointer to the SPTNode whose Routing 
 *                           information is to be updated
 *               u1MetIdx  - Metric Index which specifies the index that 
 *                           holds values that are to be to updated in the 
 *                           arrays used in the SPT nodes
 * Output (s)  : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisDecFormRouteInfo (tIsisSysContext * pContext, UINT1 u1BitMask,
                      tIsisSPTNode * pNode, UINT1 u1MetIdx,
                      UINT1 u1Level, UINT1 u1MtIndex)
{
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisRouteInfo      RouteInfo;
    tIsisAdjDelDirEntry *pDelDirEntry = NULL;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv6RtInfo      NetIp6RtInfo;
    tNetIpv6RtInfoQueryMsg RtIPv6Query;
    tIsisSPTNode       *pHeadNode = NULL;
    tIsisSPTNode       *pTempNode = NULL;
    tIsisSPTNode       *pPrevNode = NULL;
    tRBTree             PrevRBTree;
    UINT1               au1IpV6Addr[ISIS_MAX_IPV6_ADDR_LEN];
    UINT4               u4CktIfIndex;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT2               u2Metric = 0;
    UINT1               u1PrefLen = 0;
    UINT1               u1MtSupport = ISIS_FALSE;
    UINT1               u1AdjMtId = 0;
    UINT1               u1IfMtId = 0;
    UINT1               au1TempIPAddr[ISIS_MAX_IPV6_ADDR_LEN];

    MEMSET (au1TempIPAddr, 0, ISIS_MAX_IPV6_ADDR_LEN);
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&NetIp6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtIPv6Query, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (au1IpV6Addr, 0x00, ISIS_MAX_IPV6_ADDR_LEN);
    MEMSET (RouteInfo.DestIpAddr.au1IpAddr, 0, ISIS_MAX_IP_ADDR_LEN);
    MEMSET (&RouteInfo, 0, sizeof (tIsisRouteInfo));

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecFormRouteInfo () \n"));

    if ((pContext == NULL) || (u1MetIdx >= ISIS_NUM_METRICS))    /*klocwork */
    {
        DEP_PT ((ISIS_LGST,
                 "DEC <T> : Context Does Not Exist or Invalid Metric Type [%u]\n",
                 u1MetIdx));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return;
    }

    if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER)
        && (gu1IsisGrState != ISIS_GR_SPF_WAIT))
    {
        return;
    }

    PrevRBTree =
        (u1Level ==
         ISIS_LEVEL1) ? pContext->SysL1Info.
        PrevShortPath[u1MtIndex] : pContext->SysL2Info.PrevShortPath[u1MtIndex];

    u1MtSupport = pContext->u1IsisMTSupport;

    /* If MT support is enabled, the IPV4 and IPV6 routes have to
       be updated in respective Routing tables, only when the
       SPF is run for respective MTs */

    if ((((u1MtSupport == ISIS_TRUE) && (u1MtIndex == ISIS_MT0_INDEX)) ||
         (u1MtSupport == ISIS_FALSE)) && (pNode->u1AddrType == ISIS_ADDR_IPV4))
    {
        IsisUtlComputePrefixLen (&pNode->au1DestID[ISIS_MAX_IPV4_ADDR_LEN],
                                 ISIS_MAX_IPV4_ADDR_LEN, &u1PrefLen);
        RouteInfo.DestIpAddr.u1PrefixLen = u1PrefLen;
        RouteInfo.u4MetricVal = pNode->u4MetricVal;
        MEMCPY (RouteInfo.DestIpAddr.au1IpAddr, pNode->au1DestID,
                ISIS_MAX_IPV4_ADDR_LEN);
    }
    else if ((((u1MtSupport == ISIS_TRUE) && (u1MtIndex == ISIS_MT2_INDEX)) ||
              (u1MtSupport == ISIS_FALSE)) &&
             (pNode->u1AddrType == ISIS_ADDR_IPV6))
    {
        RouteInfo.DestIpAddr.u1PrefixLen = pNode->u1PrefixLen;
        RouteInfo.u4MetricVal = (pNode->u4MetricVal);
        MEMCPY (RouteInfo.DestIpAddr.au1IpAddr, pNode->au1DestID,
                ISIS_MAX_IPV6_ADDR_LEN);
    }
    RouteInfo.DestIpAddr.u1AddrType = pNode->u1AddrType;
    u2Metric = pNode->au2Metric[u1MetIdx];
    u2Metric &= ISIS_SPT_L12_SYS_MASK;
    RouteInfo.u1MetricType = (UINT1) IsisUtlGetMetType (pContext, u1MetIdx);
    RouteInfo.u1BitMask = u1BitMask;

    if ((u2Metric & ISIS_SPT_SELF_IPRA_MASK) == ISIS_SELF_IPRA_ADJ)
    {
        if ((((u1MtSupport == ISIS_TRUE) && (u1MtIndex == ISIS_MT0_INDEX)) ||
             (u1MtSupport == ISIS_FALSE)) &&
            (pNode->u1AddrType == ISIS_ADDR_IPV4))
        {
            /* Interface IPRA need not be added to rtm. Only manually configured IPRA
             *          * needs to be added to rtm.*/
            if (pNode->au4DirIdx[u1MetIdx] == 0)
            {
                return;
            }

            MEMCPY (RouteInfo.au1IPAddr, &(pNode->au4DirIdx[u1MetIdx]),
                    ISIS_MAX_IPV4_ADDR_LEN);
            RouteInfo.DestIpAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;

            /* Get the interface index through which the route will be reachable. */

            MEMCPY (&(RtQuery.u4DestinationIpAddress), RouteInfo.au1IPAddr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            RtQuery.u4DestinationIpAddress =
                IP_NTOHL (RtQuery.u4DestinationIpAddress);
            RtQuery.u4DestinationSubnetMask = IP_NTOHL (0xFFFFFFFF);
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
            {
                return;
            }
            RouteInfo.u4CktIfIndex = NetIpRtInfo.u4RtIfIndx;
        }
        else if ((((u1MtSupport == ISIS_TRUE) && (u1MtIndex == ISIS_MT2_INDEX))
                  || (u1MtSupport == ISIS_FALSE))
                 && (pNode->u1AddrType == ISIS_ADDR_IPV6))
        {
            if (MEMCMP
                (pNode->au1Ipv6DirIdx, au1IpV6Addr,
                 ISIS_MAX_IPV6_ADDR_LEN) == 0)
            {
                return;
            }
            MEMCPY (RouteInfo.au1IPAddr, &(pNode->au1Ipv6DirIdx),
                    ISIS_MAX_IPV6_ADDR_LEN);
            RouteInfo.DestIpAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
            MEMCPY (NetIp6RtInfo.Ip6Dst.u1_addr, RouteInfo.au1IPAddr,
                    ISIS_MAX_IPV6_ADDR_LEN);
            NetIp6RtInfo.u1Prefixlen = (UINT1)
                (RouteInfo.DestIpAddr.u1Length *
                 ISIS_IPV6RA_PREFIX_LEN_ROUNDOFF);
            RtIPv6Query.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
            if (NetIpv6GetRoute (&RtIPv6Query, &NetIp6RtInfo) ==
                NETIPV6_FAILURE)
            {
                return;
            }
            RouteInfo.u4CktIfIndex = NetIp6RtInfo.u4Index;
        }
    }
    else if ((u2Metric == ISIS_NBR_IPRA_ADJ)
             || ((u2Metric & ISIS_SPT_SELF_IPRA_MASK) == ISIS_NBR_IPRA_ADJ))
    {
        pAdjEntry = IsisAdjGetAdjRecFromDirIdx (pContext,
                                                pNode->au4DirIdx[u1MetIdx]);
        if (pAdjEntry != NULL)
        {
            u1AdjMtId = pAdjEntry->u1AdjMTId;
            u1IfMtId = pAdjEntry->pCktRec->u1IfMTId;

            RouteInfo.u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
            if (((u1MtSupport == ISIS_FALSE) ||
                 ((u1MtSupport == ISIS_TRUE) &&
                  ((u1MtIndex == ISIS_MT0_INDEX) &&
                   (u1IfMtId & ISIS_MT0_BITMAP) &&
                   (u1AdjMtId & ISIS_MT0_BITMAP)))) &&
                (pNode->u1AddrType == ISIS_ADDR_IPV4))
            {
                u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
                if (NetIpv4GetPortFromIfIndex
                    (u4CktIfIndex, &RouteInfo.u4CktIfIndex) == NETIPV4_FAILURE)
                {
                    return;
                }
                MEMCPY (RouteInfo.au1IPAddr,
                        pAdjEntry->AdjNbrIpV4Addr.au1IpAddr,
                        ISIS_MAX_IPV4_ADDR_LEN);
                if ((u1MtSupport == ISIS_FALSE) &&
                    (MEMCMP (au1TempIPAddr, RouteInfo.au1IPAddr,
                             ISIS_MAX_IPV4_ADDR_LEN) == 0))
                {
                    if ((pHeadNode = RBTreeGet (PrevRBTree, pNode)) != NULL)
                    {
                        RBTreeRemove (PrevRBTree, pHeadNode);
                        if (pNode == pHeadNode)
                        {
                            pTempNode = pHeadNode;
                            pHeadNode = pHeadNode->pNext;
                            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTempNode);
                        }
                        else
                        {
                            pTempNode = pHeadNode;
                            while ((pTempNode != NULL) && (pTempNode != pNode))
                            {
                                pPrevNode = pTempNode;
                                pTempNode = pTempNode->pNext;
                            }
                            pPrevNode->pNext = pTempNode->pNext;
                            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTempNode);
                        }
                        if (pHeadNode != NULL)
                        {
                            RBTreeAdd (PrevRBTree, pHeadNode);
                        }
                    }
                    return;
                }
            }
            else if (((u1MtSupport == ISIS_FALSE) ||
                      ((u1MtSupport == ISIS_TRUE) &&
                       ((u1MtIndex == ISIS_MT2_INDEX) &&
                        (u1IfMtId & ISIS_MT2_BITMAP) &&
                        (u1AdjMtId & ISIS_MT2_BITMAP)))) &&
                     (pNode->u1AddrType == ISIS_ADDR_IPV6))
            {
                RouteInfo.u4CktIfIndex = pAdjEntry->pCktRec->u4CktIfIdx;
                MEMCPY (RouteInfo.au1IPAddr,
                        pAdjEntry->AdjNbrIpV6Addr.au1IpAddr,
                        ISIS_MAX_IPV6_ADDR_LEN);
                if ((u1MtSupport == ISIS_FALSE) &&
                    (MEMCMP (au1TempIPAddr, RouteInfo.au1IPAddr,
                             ISIS_MAX_IPV6_ADDR_LEN) == 0))
                {
                    if ((pHeadNode = RBTreeGet (PrevRBTree, pNode)) != NULL)
                    {
                        RBTreeRemove (PrevRBTree, pHeadNode);
                        if (pNode == pHeadNode)
                        {
                            pTempNode = pHeadNode;
                            pHeadNode = pHeadNode->pNext;
                            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTempNode);
                        }
                        else
                        {
                            pTempNode = pHeadNode;
                            while ((pTempNode != NULL) && (pTempNode != pNode))
                            {
                                pPrevNode = pTempNode;
                                pTempNode = pTempNode->pNext;
                            }
                            pPrevNode->pNext = pTempNode->pNext;
                            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTempNode);
                        }
                        if (pHeadNode != NULL)
                        {
                            RBTreeAdd (PrevRBTree, pHeadNode);
                        }
                    }

                    return;
                }
            }
        }
        else if (u1BitMask == ISIS_SPT_ROUTE_DELETE)
        {
            /* Decn has found a route to be deleted because of adjaceny
             * change. So search in the pContext->pDelDirEntry list
             * to get the nexthop address information pertaining to the
             * route entry is to be deleted 
             */

            pDelDirEntry = pContext->pDelDirEntry;

            while (pDelDirEntry != NULL)
            {
                if (pDelDirEntry->u4DirIdx == pNode->au4DirIdx[u1MetIdx])
                {
                    if ((((u1MtSupport == ISIS_TRUE)
                          && (u1MtIndex == ISIS_MT0_INDEX))
                         || (u1MtSupport == ISIS_FALSE))
                        && (pNode->u1AddrType == ISIS_ADDR_IPV4))
                    {
                        MEMCPY (RouteInfo.au1IPAddr,
                                pDelDirEntry->au1AdjNbrIpV4Addr,
                                ISIS_MAX_IPV4_ADDR_LEN);
                    }
                    else if ((((u1MtSupport == ISIS_TRUE)
                               && (u1MtIndex == ISIS_MT2_INDEX))
                              || (u1MtSupport == ISIS_FALSE))
                             && (pNode->u1AddrType == ISIS_ADDR_IPV6))
                    {
                        MEMCPY (RouteInfo.au1IPAddr,
                                pDelDirEntry->au1AdjNbrIpV6Addr,
                                ISIS_MAX_IPV6_ADDR_LEN);
                    }
                    pDelDirEntry->u1Status = ISIS_TRUE;
                    pDelDirEntry->u1Count = (UINT1) (pDelDirEntry->u1Count + 1);
                    break;
                }
                pDelDirEntry = pDelDirEntry->pNext;
            }
            if (pDelDirEntry == NULL)
            {
                DEP_PT ((ISIS_LGST, "DEC <T> : DelDirRecord Does Not Exist - "
                         " Direction Index [ %u ], Level [ %u ]\n",
                         pNode->au4DirIdx[u1MetIdx], u1Level));
                DEP_EE ((ISIS_LGST,
                         "DEC <X> : Exiting IsisDecFormRouteInfo () \n"));
                return;
            }
        }
        else
        {
            /* The computed route is no more valid
             * so don't update in the routing table
             */
            return;
        }
    }
    else
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Invalid type of IPRA Adjacency"));
        DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecFormRouteInfo () \n"));
        return;
    }

    if (SPT_GET_UPDOWN_FLAG (pNode) == ISIS_RL_L1_DOWN)
    {
        RouteInfo.u1Level = ISIS_INTER_AREA;
    }
    else
    {
        RouteInfo.u1Level = u1Level;
    }
    i4RetVal = IsisRtmiRtmTxRtUpdate (pContext, &RouteInfo);

    if (i4RetVal == ISIS_FAILURE)
    {
        DEP_PT ((ISIS_LGST, "DEC <T> : Failed to update Route Info\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecFormRouteInfo () \n"));
}

/******************************************************************************
 * Function    : IsisDecSPTComplete ()
 * Description : This function traverses the Previous Path and sends Route
 *               Delete Information for the nodes that do not have the visited
 *               mark, If the System is L12, after the L1 Decision Process
 *               Completion for all the Metrics, the 'pDelNodes' in the System 
 *               Context is updated with the nodes that are not visited in  
 *               the comparison of the PrevPath and Short Path.
 *               For the nodes that have the visited mark, the mark is reset. 
 *               After the traversal of the PrevPath, this function
 *               generates the Decision Complete Event.
 * Input (s)   : pContext - Pointer to System Context
 *               u1Level  - Level of the Decision Process run
 * Output (s)  : None
 * Globals     : Not referred or modified
 * Returns     : VOID
 *****************************************************************************/

PRIVATE VOID
IsisDecSPTComplete (tIsisSysContext * pContext, UINT1 u1Level, UINT1 u1MtIndex)
{
    tGrSemInfo          GrSemInfo;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1               u1MetIdx = 0;

    /* Flag to find whether the non visited nodes in the Previous 
     * Path are to be freed 
     */

    UINT1               u1Free = ISIS_TRUE;

    /* Flag indicating the nodes to be delinked from the PrevPath and
     * linked to pContext->pL1DelNodes
     */

    UINT1               u1DelFlag = ISIS_TRUE;
    tIsisSPTNode       *pTrav = NULL;
    tIsisEvtDecnComplete *pEvtDecnComplete = NULL;
    tIsisSPTNode       *pTravNode = NULL;
    /* pTravPrNode points to the previous node in the list of nodes 
     * pointed to by a rbnode */
    tIsisSPTNode       *pTravPrNode = NULL;
    tRBTree             PrevPath;
    tRBTree             CurrPath;
    UINT1               u1UpdateL2Flag = ISIS_FALSE;
    tIsisSPTNode       *pL2Route = NULL;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecSPTComplete () \n"));

    /* Reset the Relq.count */

    gu4NodeCnt = 0;
    gu4TestCnt = 0;

    /* After the Completion of Decision Process for all the Supported Metrics,
     * Check for the ISIS_SPT_MARK in all the nodes in the pPrev Path.
     * The nodes which have not been visited in all the four Metrics 
     * Supported, while comparing the Short Path and PrevPath are to be
     * deleted from the PrevPath. If the System is a L12 System, then after
     * the L1 Decision Process, such nodes are intimated to the Update Process
     * for the updation in the SelfLSPs.
     */

    PrevPath =
        (u1Level ==
         ISIS_LEVEL1) ? pContext->SysL1Info.
        PrevShortPath[u1MtIndex] : pContext->SysL2Info.PrevShortPath[u1MtIndex];

    /* If the System is not a Level 12 System or the Decision Process completed
     * is not a L1 process, then the memmory occupied by the not visted nodes, 
     * during the comparison of the PrevPath and ShortPath,
     * (i.e., the nodes in the PrevPath that are to be deleted) are freed.
     * u1Free Flag is ISIS_TRUE by default, meaning the nodes after the L2
     * Decision Process for a L2 / L12 System and after L1 Decision Process for
     * a L1 System should be freed.
     */

    if (pContext->SysActuals.bSysL2ToL1Leak == ISIS_TRUE &&
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        /* If Route leaking support exists, then in a L12 system the memory occupied
         * by the non visited nodes is not freed */
        u1Free = ISIS_FALSE;
    }
    else
    {
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
            && (u1Level == ISIS_LEVEL1))
        {
            u1Free = ISIS_FALSE;
        }
    }

    /* Traverse the entire PrevPath Table to find the nodes
     */

    pTrav = RBTreeGetFirst (PrevPath);

    while (pTrav != NULL)
    {
        /* Checking the flag for all the supported metrics
         */

        u1DelFlag = ISIS_TRUE;

        /* Check for each metric index whether the node is marked or not
         * if the node is not marked for any metric index we can move that
         * out from the prev path and send a route delete to the IP RTM
         */

        for (u1MetIdx = 0; u1MetIdx < ISIS_NUM_METRICS; u1MetIdx++)
        {
            if ((pTrav->au2Metric[u1MetIdx] & ISIS_SPT_MARK) == ISIS_SPT_MARK)
            {
                /* If the Node has been visited in any one of the 
                 * Metric Type, then the ISIS_SPT_MARK flag would have 
                 * been set. If so, then that node should not be deleted.
                 * The flag is now reset for all the metrics
                 */

                if (u1DelFlag == ISIS_TRUE)
                {
                    u1DelFlag = ISIS_FALSE;
                }

                /* reset the mark for this node for this metric index 
                 */

                if (pTrav->au2Metric[u1MetIdx] != ISIS_SPT_INV_METRIC)
                {
                    pTrav->au2Metric[u1MetIdx] &= ~ISIS_SPT_MARK;
                }
            }
            else if (pTrav->au2Metric[u1MetIdx] != ISIS_SPT_INV_METRIC)
            {
                /* Send the Route Update for these nodes as these 
                 * routes are to be deleted form the Routing Table
                 */

                u1UpdateL2Flag = ISIS_FALSE;
                if (u1Level == ISIS_LEVEL1)
                {
                    pL2Route =
                        RBTreeGet (pContext->SysL2Info.PrevShortPath[u1MtIndex],
                                   pTrav);
                    if (pL2Route != NULL)
                    {
                        u1UpdateL2Flag = ISIS_TRUE;
                    }
                }

                IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_DELETE,
                                      pTrav, u1MetIdx, u1Level, u1MtIndex);

                if (u1UpdateL2Flag == ISIS_TRUE)
                {
                    do
                    {
                        IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_ADD,
                                              pL2Route, u1MetIdx, ISIS_LEVEL2,
                                              u1MtIndex);
                        pL2Route = pL2Route->pNext;
                    }
                    while (pL2Route != NULL);
                }
            }
        }

        /* Ideally i would have relinquished here also if it takes too
         * much time to update my previous path 
         * but idon't want to become too complex so better i
         * send hellos mean while to retain my adjacencies 
         */
        gu4TestCnt++;

        if (gu4TestCnt == 5000)
        {
            pCktRec = pContext->CktTable.pCktRec;
            while (pCktRec != NULL)
            {

                if ((pCktRec->u1CktLevel == ISIS_LEVEL1) ||
                    (pCktRec->u1CktLevel == ISIS_LEVEL12))
                {
                    IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
                }

                if ((pCktRec->u1CktLevel == ISIS_LEVEL2) ||
                    (pCktRec->u1CktLevel == ISIS_LEVEL12))
                {
                    IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
                }

                pCktRec = pCktRec->pNext;
            }
            gu4TestCnt = 0;
        }

        /* If the System is L12 and after L1 Decision Completion the 
         * nodes are not to be freed. Hence these nodes are linked to 
         * the head of the pContext->pL1DelNodes. Otherwise, the nodes 
         * are freed
         */

        pTravNode = pTrav->pNext;
        if (pTravNode == NULL)
        {
            pTravNode = RBTreeGetNext (PrevPath, pTrav, NULL);
        }

        if (u1DelFlag == ISIS_TRUE)
        {
            /* When pTravPrNode is null, the node is the first node in the
             * list of ecmp nodes and is present in the RbTree. Hence, remove
             * the node from the tree and add the next node to the tree. 
             * Else assign the next node as pNext of my previous node */
            if (pTravPrNode == NULL)
            {
                RBTreeRemove (PrevPath, pTrav);
                if (pTrav->pNext != NULL)
                {
                    if (RBTreeAdd (PrevPath, pTrav->pNext) == RB_FAILURE)
                    {
                        pTravNode = RBTreeGet (PrevPath, pTrav->pNext);
                        assert (0);
                    }
                }
            }
            else
            {
                pTravPrNode->pNext = pTrav->pNext;
            }

            /* my next is null, so i have to move to the next node in the rbtree
             * hence reset the pTravPrNode to Null */
            if (pTrav->pNext == NULL)
            {
                pTravPrNode = NULL;
            }
            if (u1Free == ISIS_FALSE)
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    pTrav->pNext = pContext->pL1DelNodes;
                    pContext->pL1DelNodes = pTrav;
                }
                else if (u1Level == ISIS_LEVEL2)
                {
                    pTrav->pNext = pContext->pL2DelNodes;
                    pContext->pL2DelNodes = pTrav;
                }
            }
            else
            {
                pTrav->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTrav);
            }
        }
        else
        {
            /* When the node is not deleted from the tree, store the node
             * to store the links of next node */
            if (pTrav->pNext == NULL)
            {
                pTravPrNode = NULL;
            }
            else
            {
                pTravPrNode = pTrav;
            }
        }
        pTrav = pTravNode;
    }

    /* Form a Decision Process Complete Event and post it to the Control
     * queue for the Update module to update the Self LSPs
     */

    if (u1Level == ISIS_LEVEL1)
    {
        pEvtDecnComplete = (tIsisEvtDecnComplete *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDecnComplete));
        if (pEvtDecnComplete != NULL)
        {
            pEvtDecnComplete->u1EvtID = ISIS_EVT_L1_DECN_COMPLETE;
            IsisUtlSendEvent (pContext, (UINT1 *) pEvtDecnComplete,
                              sizeof (tIsisEvtDecnComplete));
        }
        else
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL
                    " : Decision Process Complete Event\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
    }
    else if ((pContext->SysActuals.bSysL2ToL1Leak == ISIS_TRUE) &&
             ((pContext->SysActuals.u1SysType == ISIS_LEVEL12) &&
              (u1Level == ISIS_LEVEL2)))
    {
        pEvtDecnComplete = (tIsisEvtDecnComplete *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDecnComplete));
        if (pEvtDecnComplete != NULL)
        {
            pEvtDecnComplete->u1EvtID = ISIS_EVT_L2_DECN_COMPLETE;
            IsisUtlSendEvent (pContext, (UINT1 *) pEvtDecnComplete,
                              sizeof (tIsisEvtDecnComplete));
        }
        else
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL
                    " : Decision Process Complete Event\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
        }
    }
    IsisDecDeAllocSptDB (pContext->OSIPath, pContext, u1Level, ISIS_FALSE,
                         u1MtIndex);

    /* Reset the PATH Hash Table
     */

    CurrPath =
        (u1Level ==
         ISIS_LEVEL1) ? pContext->SysL1Info.ShortPath[u1MtIndex] : pContext->
        SysL2Info.ShortPath[u1MtIndex];
    IsisDecDeAllocSptDB (CurrPath, pContext, u1Level, ISIS_FALSE, u1MtIndex);

    if (pContext->u1IsisGRRestartStatus == ISIS_RESTARTING_ROUTER)
    {
        if (pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        {
            if (gu1IsisGrState == ISIS_GR_SPF_L1COMPLETE
                || gu1IsisGrState == ISIS_GR_SPF_L2COMPLETE)
            {
                /*Trigger SPF & Goto SPF_WAIT */
                gu1IsisGrState = ISIS_GR_SPF_DONE;
                MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
                GrSemInfo.pContext = pContext;

                if (((TMO_SLL_Count (&(pContext->SysL1Info.AckLst)) == 0) &&
                     (TMO_SLL_Count (&(pContext->SysL2Info.AckLst)) == 0) &&
                     (TMO_SLL_Count (&(pContext->SysL1Info.CSNPLst)) == 0) &&
                     (TMO_SLL_Count (&(pContext->SysL2Info.CSNPLst)) == 0)) &&
                    (RBTreeGetFirst (pContext->SysL1Info.pGrCSNPTbl) == NULL) &&
                    (RBTreeGetFirst (pContext->SysL2Info.pGrCSNPTbl) == NULL))
                {
                    IsisGrRunRestartSem (ISIS_GR_EVT_ALL_SPF_DONE,
                                         ISIS_GR_SPF_WAIT, &GrSemInfo);
                }

            }

            if (u1Level == ISIS_LEVEL1)
            {
                gu1IsisGrState = ISIS_GR_SPF_L1COMPLETE;
            }
            else if (u1Level == ISIS_LEVEL2)
            {
                gu1IsisGrState = ISIS_GR_SPF_L2COMPLETE;
            }
        }
        else
        {
            gu1IsisGrState = ISIS_GR_SPF_DONE;

            MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
            GrSemInfo.pContext = pContext;

            if (u1Level == ISIS_LEVEL1)
            {
                if (((TMO_SLL_Count (&(pContext->SysL1Info.AckLst)) == 0) &&
                     (TMO_SLL_Count (&(pContext->SysL1Info.CSNPLst)) == 0)) &&
                    (RBTreeGetFirst (pContext->SysL1Info.pGrCSNPTbl) == NULL))
                {
                    IsisGrRunRestartSem (ISIS_GR_EVT_ALL_SPF_DONE,
                                         ISIS_GR_SPF_WAIT, &GrSemInfo);
                }

            }
            else if (u1Level == ISIS_LEVEL2)
            {
                if (((TMO_SLL_Count (&(pContext->SysL2Info.AckLst)) == 0) &&
                     (TMO_SLL_Count (&(pContext->SysL2Info.CSNPLst)) == 0)) &&
                    (RBTreeGetFirst (pContext->SysL2Info.pGrCSNPTbl) == NULL))
                {
                    IsisGrRunRestartSem (ISIS_GR_EVT_ALL_SPF_DONE,
                                         ISIS_GR_SPF_WAIT, &GrSemInfo);
                }

            }
        }
    }

    if (pContext->u1IsisGRRestartMode == ISIS_GR_HELPER_DOWN)
    {
        pContext->u1IsisGRRestartMode = ISIS_GR_NONE;
    }

    DEP_PT ((ISIS_LGST, "DEC <T> : Decision Complete For [ %s ] MT [%d] \n\n",
             ISIS_GET_LEVEL_STR (u1Level), u1MtIndex));
    gu4TestCnt = 0;
    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecSPTComplete ()\n"));
}

/******************************************************************************
 * Function     : IsisDecProcISDownEvt ()
 * Description  : This function Deletes all the SPT nodes from 
 *                    -- Previous Shortest Path Database
 *                    -- Current Shortest Path Database
 *                    -- TENT Database
 * Input (s)    : pContext - Pointer to System Context
 * Output (s)   : None
 * Globals      : Not referred or modified
 * Returns      : VOID
 *****************************************************************************/

PUBLIC VOID
IsisDecProcISDownEvt (tIsisSysContext * pContext)
{
    UINT1               u1MtIndex = 0;
    UINT1               au1TempNearestL2[ISIS_SYS_ID_LEN];

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecProcISDownEvt ()\n"));

    MEMSET (au1TempNearestL2, 0, ISIS_SYS_ID_LEN);
    DEP_PI ((ISIS_LGST,
             "DEC <I> : Deleting Databases: Previous and Current Path, TENT, RRD, Host Name List, Default Route\n"));

    for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
    {

        if (pContext->SysL1Info.PrevShortPath[u1MtIndex] != NULL)
        {
            /* Clear all the SPT nodes from Previous Shortest Path Database
             */

            IsisDecDeAllocSptDB (pContext->SysL1Info.PrevShortPath[u1MtIndex],
                                 pContext, ISIS_LEVEL1, ISIS_TRUE, u1MtIndex);
        }

        if (pContext->SysL1Info.ShortPath[u1MtIndex] != NULL)
        {
            /* Clear all the SPT nodes from Current Shortest Path Database
             */

            IsisDecDeAllocSptDB (pContext->SysL1Info.ShortPath[u1MtIndex],
                                 pContext, ISIS_LEVEL1, ISIS_FALSE, u1MtIndex);
        }

        if (pContext->SysL2Info.PrevShortPath[u1MtIndex] != NULL)
        {
            /* Clear all the SPT nodes from Previous Shortest Path Database
             */

            IsisDecDeAllocSptDB (pContext->SysL2Info.PrevShortPath[u1MtIndex],
                                 pContext, ISIS_LEVEL2, ISIS_TRUE, u1MtIndex);
        }
        if (pContext->SysL2Info.ShortPath[u1MtIndex] != NULL)
        {
            /* Clear all the SPT nodes from Current Shortest Path Database
             */

            IsisDecDeAllocSptDB (pContext->SysL2Info.ShortPath[u1MtIndex],
                                 pContext, ISIS_LEVEL2, ISIS_FALSE, u1MtIndex);
        }

        if (pContext->u1IsisMTSupport == ISIS_FALSE)
        {
            break;
        }

    }

    /* Clear all the SPT nodes from the Tent Path 
     */

    IsisDecDeAllocSptDB (pContext->Tent, pContext,
                         pContext->SysActuals.u1SysType, ISIS_FALSE, u1MtIndex);
    IsisDecDeAllocSptDB (pContext->OSIPath, pContext,
                         pContext->SysActuals.u1SysType, ISIS_FALSE, u1MtIndex);

    if (pContext->pDelNL2Nodes[ISIS_MT0_INDEX] != NULL)
    {
        IsisDecDeleteNearestL2 (pContext, ISIS_DEF_MET, ISIS_MT0_INDEX);
    }

    if ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
        (pContext->pDelNL2Nodes[ISIS_MT2_INDEX] != 0))
    {
        IsisDecDeleteNearestL2 (pContext, ISIS_DEF_MET, ISIS_MT2_INDEX);
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecProcISDownEvt ()\n"));
}

/******************************************************************************
 * Function     : IsisDecDeAllocSptDB ()
 * Description  : This routine frees all the SPT nodes from the given database,
 *                but the database itself is not released
 * Inputs       : pContext  - Pointer to System Context
 *                pDatabase - Pointer to the Database
 * Outputs      : None 
 * Globals      : None 
 * Returns      : VOID 
 *****************************************************************************/
PUBLIC VOID
IsisDecDeAllocSptDB (tRBTree RBTree, tIsisSysContext * pContext, UINT1 u1Level,
                     UINT1 u1Flag, UINT1 u1MtIndex)
{
    tIsisSPTNode       *pNextSptNode = NULL;
    tIsisSPTNode       *pSptNode = NULL;
    tIsisSPTNode       *pSptNodeIn = NULL;
    UINT1               u1MetIdx = 0;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecDeAllocSptDB ()\n"));

    pSptNode = RBTreeGetFirst (RBTree);
    while (pSptNode != NULL)
    {
        if (u1Flag != ISIS_FALSE)
        {
            IsisDecFormRouteInfo (pContext, ISIS_SPT_ROUTE_DELETE, pSptNode,
                                  u1MetIdx, u1Level, u1MtIndex);
        }
        RBTreeRemove (RBTree, pSptNode);
        pNextSptNode = RBTreeGetNext (RBTree, pSptNode, NULL);
        while (pSptNode != NULL)
        {
            pSptNodeIn = pSptNode->pNext;
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptNode);
            pSptNode = pSptNodeIn;
        }
        pSptNode = pNextSptNode;
    }

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecDeAllocSptDB ()\n"));
}

/******************************************************************************
 * Function     : IsisDecRevSPF ()
 * Description  : This routine revives the decision process which was
 *                relinquished before and continues till the end 
 * Inputs       : pContext  - Pointer to System Context
 *                u1MetIdx  - Metric Index
 *                u1Level   - ISIS_LEVEL2 or ISIS_LEVEL1
 *                u1GetATTFlag - Flag indictaing whether to check the system is 
 *                               attached or not 
 *                u1NearL2Flag - Flag indiacting wther to update the Nearest L2
 *                               system or not
 *                u1AttStatChg - Flag indicating whether the systems attached
 *                status is modified from the previous run to now
 * Outputs      : None 
 * Globals      : None 
 * Returns      : VOID 
 *****************************************************************************/

PUBLIC INT4
IsisDecRevSPF (tIsisSysContext * pContext, UINT1 u1MetIdx, UINT1 u1Level,
               UINT1 u1GetATTFlag, UINT1 u1NearL2Flag, UINT1 u1AttStatChg,
               UINT1 u1MtIndex)
{
    tIsisSPTNode       *pMCNode = NULL;
    tIsisSPTNode       *pSptList = NULL;
    tIsisSPTNode       *pTentNode = NULL;
    tIsisSPTNode       *pPrevNode = NULL;
    tIsisSPTNode       *pNextNode = NULL;
    tIsisSPTNode       *pNextTentNode = NULL;
    tIsisEvtDecnRelq   *pEvtDecnRelq = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1HashIdx = 0;
    UINT1               u1AttStt = 0;
    UINT1               u1Result = 0;
    UINT1               u1Loc = ISIS_SPT_LOC_NONE;
    UINT1               u1Flag = ISIS_FALSE;
    UINT1               u1MetType = ISIS_FALSE;
    UINT1               u1AttFlag = ISIS_NOT_SET;

    DEP_EE ((ISIS_LGST, "DEC <X> : Entered IsisDecRevSPF()\n"));

    DEP_PT ((ISIS_LGST, "DEC <T> : SPF Continuing... Level [%s], MT [%d]\n",
             ISIS_GET_LEVEL_STR (u1Level), u1MtIndex));

    u1MetType = IsisUtlGetMetType (pContext, u1MetIdx);
    while (u1Flag == ISIS_FALSE)
    {
        /* Keep removing the Minimum cost nodes from the TENT. Based on the
         * System Type (ISIS_IP_END_SYSTEM or ISIS_OSI_SYSTEM) add the node to
         * either OSI or IP Path. We may have to handle multiple nodes also
         * in the case of Equal Cost Multi-Path (ECMP).
         */

        IsisDecGetMCNodeFromTent (pContext, u1MetIdx, &pTentNode, &u1HashIdx);

        if (pTentNode != NULL)
        {
            /* We still have nodes to be considered from TENT. Note down the
             * MC node retrieved which will be of use in case the retrieved
             * nodes is the first node in the ECMP list. As mentioned before
             * only the first TENT node in the ECMP list will have a proper
             * value for Max Paths. We may have to update each of the ECMP nodes
             * with the correct value of Max Paths before the node is moved into
             * PATH
             */

            pMCNode = pTentNode;

            ISIS_DBG_PRINT_ID (pTentNode->au1DestID,
                               (UINT1) (ISIS_SYS_ID_LEN + 1),
                               "\tMinimum Cost Node\t", ISIS_OCTET_STRING);

            /* The Type of Adjacency must be the same for all the Nodes in an
             * ECMP list i.e. if {N1, D1, 1}, {N1, D5, 1} and {N1, D3, 1} are
             * the ECMP nodes in the ECMP list for {N1}, then the Metric bits
             * in the SPT node MUST indicate that {N1} is either OSI or IP 
             * system. Each node in the list cannot depict a different system 
             * type for {N1}. Hence compute the Adjacent System Type from the 
             * very first node in the ECMP list
             */

            u1Result = (UINT1) (ISIS_ADJ_SYS_TYPE (pTentNode->
                                                   au2Metric[u1MetIdx]));

            /* NOTE: The while loop is for handling the ECMP case
             */

            while (pTentNode != NULL)
            {
                pPrevNode = NULL;

                /* Check whether a Node which mathces the pTentNode, exists in
                 * the PATH already. The return value 'u1Loc' indicates various
                 * scenarios. IsisDecAddNodeToPath () routine will handle these
                 * scenarios differently
                 */

                u1Loc = IsisDecIsNodeInPATH (pContext, pTentNode, u1Level,
                                             u1MetIdx, &pPrevNode, u1HashIdx,
                                             u1MtIndex);

                pNextTentNode = pTentNode->pNext;

                /* Make the MPS value in all the Minimum Cost Nodes to be
                 * the same as the Maximum Paths available for this ECMP
                 * before inserting these minimum cost nodes in the PATH
                 *
                 * NOTE: ECMP Nodes in TENT will have MaxPaths set in decreasing
                 *       order i.e. the first node in the ECMP will have
                 *       MaxPaths, the second MaxPaths - 1 and so on. pMCNode is
                 *       now pointing to the first ECMP node which has the
                 *       correct value of MaxPaths. While inserting in PATH we
                 *       will ensure that all nodes have the same MaxPaths value
                 */

                pTentNode->au1MaxPaths[u1MetIdx] =
                    pMCNode->au1MaxPaths[u1MetIdx];
                (pMCNode->au1MaxPaths[u1MetIdx])--;

                pTentNode->pNext = NULL;

                /* NOTE: FutureISIS implements two different PATHS, one for OSI
                 *       and the other for IP, and the current implementation
                 *       supports IP environments only
                 */

                if (u1Result == ISIS_IP_END_SYSTEM)
                {
                    /* The MC node contains information regarding IPRA's and
                     * hence should be added to the IP PATH
                     */

                    i4RetVal = IsisDecAddNodeToPath (pContext, pPrevNode,
                                                     pTentNode, u1Loc,
                                                     u1Level, u1MetIdx,
                                                     ISIS_CURR_IP_PATH,
                                                     u1MtIndex);

                    if ((u1Loc == ISIS_SPT_LOC_PATH_DIFF)
                        || (u1Loc == ISIS_SPT_LOC_PATH_DIFF_FIRST))
                    {
                        /* 'pNode' is already found in the PATH either for 
                         * the same Metric or a different metric. So free 
                         * the incoming node as the values from the incoming 
                         * node have been updated in the already existing 
                         * node in the Path
                         */

                        pTentNode->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTentNode);
                    }

                    if (pContext->u4MaxNodes < ISIS_MAX_SPF_NODES)
                    {
                        pContext->u4MaxNodes++;
                    }
                    else if (pNextTentNode != NULL)
                    {
                        DEP_PT ((ISIS_LGST,
                                 "DEC <T> : Max nodes processed in SPF, the last node being processed is"
                                 " ECMP, So delay relinquish. Nodes Processed [%d]\n",
                                 pContext->u4MaxNodes));
                    }
                    else
                    {
                        gu4NodeCnt++;
                        OsixDelayTask (5);
                        /* ISIS is delaying itself to give control to other
                         * tasks to run. This is to support the OS that are
                         * non-premtive
                         */

                        DEP_PT ((ISIS_LGST,
                                 "DEC <T> : Relinquish SPF (RevSPF)- Number of Relinquishes made since SPF schedule [%d]\n",
                                 gu4NodeCnt));
                        pEvtDecnRelq =
                            (tIsisEvtDecnRelq *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                                 sizeof
                                                                 (tIsisEvtDecnRelq));

                        if (pEvtDecnRelq != NULL)
                        {
                            pEvtDecnRelq->u1EvtID = ISIS_EVT_DECN_RELQ;
                            pEvtDecnRelq->u1MetIdx = u1MetIdx;
                            pEvtDecnRelq->u1Level = u1Level;
                            pEvtDecnRelq->u1AttFlag = u1GetATTFlag;
                            pEvtDecnRelq->u1NearL2Flag = u1NearL2Flag;
                            pEvtDecnRelq->u1AttStatChg = u1AttStatChg;
                            pEvtDecnRelq->u1MTIndex = u1MtIndex;

                            IsisUtlSendEvent (pContext,
                                              (UINT1 *) pEvtDecnRelq,
                                              sizeof (tIsisEvtDecnRelq));
                            gu1RelqFlag = ISIS_DECN_WAIT;
                            pContext->u4MaxNodes = 0;
                            return (ISIS_DECN_RELQ);
                        }
                        else
                        {
                            PANIC ((ISIS_LGST,
                                    ISIS_MEM_ALLOC_FAIL
                                    " : Decision Relinquish Event\n"));
                            DETAIL_FAIL (ISIS_CR_MODULE);
                        }
                    }

                    /* IPRA information is treated like End System Adjacencies.
                     * Once loaded into TENT we can proceed further and try to
                     * retrieve other MC nodes. These IPRAs move from TENT to
                     * PATH, when they happen to be the MC nodes
                     */
                }
                else
                {
                    /* The MC node contains information regarding IS Adjacencies
                     * and hence should be added to the OSI PATH
                     */

                    if ((u1Level == ISIS_LEVEL1)
                        && (u1NearL2Flag == ISIS_FALSE))
                    {
                        IsisDecUpdateNearestL2 (pContext, pTentNode, u1MetIdx,
                                                u1MetType, &u1NearL2Flag,
                                                u1MtIndex);
                    }

                    IsisDecAddNodeToPath (pContext, pPrevNode, pTentNode, u1Loc,
                                          u1Level, u1MetIdx,
                                          ISIS_OSI_PATH, u1MtIndex);

                    if ((u1Loc == ISIS_SPT_LOC_PATH_DIFF)
                        || (u1Loc == ISIS_SPT_LOC_PATH_DIFF_FIRST))
                    {
                        /* 'pNode' is already found in the PATH either for 
                         * the same Metric or a different metric. So free 
                         * the incoming node as the values from the incoming 
                         * node have been updated in the already existing 
                         * node in the Path
                         */

                        pTentNode->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTentNode);
                    }
                }

                /* This loop covers the Step 1 of the SPF algorithm of RFC 1195.
                 * Now that the MC node is moved to PATH from TENT, we will
                 * retrieve all the adjacencies reported by the MC node
                 */

                i4RetVal =
                    IsisUpdGetAdjsWithSysId (pContext, pTentNode->au1DestID,
                                             u1MetIdx, u1Level, u1GetATTFlag,
                                             &pSptList, &u1AttStt, u1MtIndex);

                if (i4RetVal == ISIS_SUCCESS)
                {
                    IsisDecValAndInsNodeInTent (pContext, &pSptList, pTentNode,
                                                u1Level, u1MetIdx, u1MtIndex);

                    if ((u1Level == ISIS_LEVEL2) && (u1GetATTFlag == ISIS_TRUE))
                    {
                        if (u1AttStt == ISIS_OTHER_AREA)
                        {
                            DETAIL_INFO (ISIS_DEC_MODULE,
                                         (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                          "DEC <T> : Attached Bit Set\n"));
                            u1GetATTFlag = ISIS_FALSE;

                            u1AttStatChg = ISIS_TRUE;

                            u1AttFlag = ISIS_NOT_SET;
                            if (u1MtIndex != 0)
                            {
                                if (pContext->SelfLSP.
                                    au1L1MTLspFlag[u1MtIndex] &
                                    ISIS_MT_LSP_ATT_MASK)
                                {
                                    u1AttFlag = ISIS_SET;
                                }
                            }
                            else if (ISIS_GET_ATT_BIT (pContext, u1MetType))
                            {
                                u1AttFlag = ISIS_SET;
                            }

                            if (u1AttFlag == ISIS_NOT_SET)
                            {
                                if (u1MtIndex != 0)
                                {
                                    pContext->SelfLSP.
                                        au1L1MTLspFlag[u1MtIndex] |=
                                        ISIS_MT_LSP_ATT_MASK;
                                }
                                else
                                {
                                    ISIS_SET_ATT_BIT (pContext, u1MetType);
                                }

                                if ((pContext->SysActuals.u1SysType ==
                                     ISIS_LEVEL12)
                                    && (pContext->SelfLSP.pL1NSNLSP != NULL)
                                    && (pContext->SelfLSP.pL1NSNLSP->pZeroLSP !=
                                        NULL))
                                {
                                    pContext->SelfLSP.pL1NSNLSP->pZeroLSP->
                                        u1DirtyFlag = ISIS_MODIFIED;
                                }
                            }    /* End of if ISIS_GET_ATT_BIT */
                        }        /* End of if (u1AttStt == ISIS_OTHER_AREA) */
                    }
                }
                else
                {
                    ISIS_DBG_PRINT_ID (pTentNode->au1DestID,
                                       (UINT1) (ISIS_SYS_ID_LEN + 1),
                                       "DEC <T> : Ignoring LSPs Of System\t",
                                       ISIS_OCTET_STRING);

                    while (pSptList != NULL)
                    {
                        pNextNode = pSptList->pNext;
                        pSptList->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pSptList);
                        pSptList = pNextNode;
                    }
                }

                if ((u1Loc == ISIS_SPT_LOC_PATH_DIFF)
                    || (u1Loc == ISIS_SPT_LOC_PATH_DIFF_FIRST))
                {
                    /* 'pNode' is already found in the PATH either for the same
                     * Metric or a different metric.  free the incoming node
                     * as the values from the incoming node have been updated
                     * in the already existing node in the Path 
                     */

                    pTentNode->pNext = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pTentNode);
                }
                pTentNode = pNextTentNode;
            }
        }
        else
        {
            /* TENT is NULL which means all nodes have been processed and the
             * PATH is ready
             */

            u1Flag = ISIS_TRUE;
        }
    }

    IsisDecDeAllocSptDB (pContext->Tent, pContext, u1Level, ISIS_FALSE,
                         u1MtIndex);

    /* If the Tent is Empty, Update the Routing Database and quit the 
     * calculation of SPF Database.
     */

    IsisDecUpdateRoute (pContext, u1MetIdx, u1Level, u1MtIndex);

    u1AttFlag = ISIS_NOT_SET;
    if (u1MtIndex != 0)
    {
        if (pContext->SelfLSP.au1L1MTLspFlag[u1MtIndex] & ISIS_MT_LSP_ATT_MASK)
        {
            u1AttFlag = ISIS_SET;
        }
    }
    else if (ISIS_GET_ATT_BIT (pContext, u1MetType))
    {
        u1AttFlag = ISIS_SET;
    }

    if ((u1AttStatChg == ISIS_FALSE) && (u1Level == ISIS_LEVEL2)
        && (u1AttFlag != ISIS_NOT_SET))
    {
        /* The System become unattached. So change the SysL1Flags 
         * from attached to unattached and post an event to Ctrl module
         */

        if (u1MtIndex != 0)
        {
            pContext->SelfLSP.au1L1MTLspFlag[u1MtIndex] &=
                ~ISIS_MT_LSP_ATT_MASK;
        }
        else
        {
            ISIS_RESET_ATT_BIT (pContext, u1MetType);
        }
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12) &&
            (pContext->SelfLSP.pL1NSNLSP != NULL) &&
            (pContext->SelfLSP.pL1NSNLSP->pZeroLSP != NULL))
        {
            pContext->SelfLSP.pL1NSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        }

    }

    if (u1Level == ISIS_LEVEL1)
    {
        IsisDecDeleteNearestL2 (pContext, u1MetIdx, u1MtIndex);
    }

    u1MetIdx++;
    u1Flag = ISIS_FALSE;

    while (u1Flag == ISIS_FALSE)
    {
        while (u1MtIndex < ISIS_MAX_MT)
        {
            while (u1MetIdx < (ISIS_NUM_METRICS))
            {
                u1MetType = IsisUtlGetMetType (pContext, u1MetIdx);

                if (u1MetType != ISIS_MET_INVALID)
                {
                    DEP_PT ((ISIS_LGST,
                             "DEC <T> : Triggering SPF from Relinquish flow for Level [%s] MT [%d]\n",
                             ISIS_GET_LEVEL_STR (u1Level), u1MtIndex));

                    i4RetVal =
                        IsisDecSPTCreate (pContext, u1Level, u1MetIdx,
                                          u1MetType, u1MtIndex);
                    u1MetIdx++;
                }
                else
                {
                    u1MetIdx++;
                    continue;
                }

                if (i4RetVal == ISIS_FAILURE)
                {
                    IsisDecDeAllocSptDB (pContext->Tent, pContext, u1Level,
                                         ISIS_FALSE, u1MtIndex);
                    IsisDecDeAllocSptDB (pContext->OSIPath, pContext, u1Level,
                                         ISIS_FALSE, u1MtIndex);

                    if (u1Level == ISIS_LEVEL1)
                    {
                        IsisDecDeAllocSptDB (pContext->SysL1Info.
                                             ShortPath[u1MtIndex], pContext,
                                             u1Level, ISIS_FALSE, u1MtIndex);
                    }
                    else
                    {
                        IsisDecDeAllocSptDB (pContext->SysL2Info.
                                             ShortPath[u1MtIndex], pContext,
                                             u1Level, ISIS_FALSE, u1MtIndex);
                    }

                    DEP_EE ((ISIS_LGST,
                             "DEC <X> : Exiting IsisDecSchedSPF ()\n"));
                    return ISIS_FAILURE;
                }
                else if (i4RetVal == ISIS_DECN_RELQ)
                {
                    return (ISIS_DECN_RELQ);
                }
            }

            IsisDecSPTComplete (pContext, u1Level, u1MtIndex);

            if (u1Level == ISIS_LEVEL1)
            {
                pContext->SysL1Info.u1SpfTrgCount = 0;
                pContext->SysL1Info.u2TrgCount = 0;
            }
            else
            {
                pContext->SysL2Info.u1SpfTrgCount = 0;
                pContext->SysL2Info.u2TrgCount = 0;
            }

            /* For the SPF of next topology, start from metric index 0 */
            u1MtIndex++;
            u1MetIdx = 0;
            if ((u1MtIndex > 0) && (pContext->u1IsisMTSupport == ISIS_FALSE))
            {
                break;
            }
        }
        if ((u1Level == ISIS_LEVEL2) &&
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            /* For the next level SPF, start from topology index 0 and metric index 0 */
            u1MetIdx = 0;
            u1MtIndex = 0;
            u1Level = ISIS_LEVEL1;
        }
        else
        {
            u1Flag = ISIS_TRUE;
        }
    }

    pContext->SysTimers.SysSPFSchTimer.pContext = pContext;
    IsisTmrStartTimer (&(pContext->SysTimers.SysSPFSchTimer),
                       ISIS_SPF_SCHDL_TIMER,
                       ((UINT4) (pContext->SysActuals.u2MinSPFSchTime)));
    gu1RelqFlag = ISIS_DECN_COMP;

    DEP_EE ((ISIS_LGST, "DEC <X> : Exiting IsisDecRevSPF()\n"));
    return ISIS_SUCCESS;

}
