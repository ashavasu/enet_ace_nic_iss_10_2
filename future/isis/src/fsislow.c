/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: fsislow.c,v 1.43 2016/07/21 07:39:29 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "iscli.h"
# include  "lr.h"
# include  "fssnmp.h"
#include "isincl.h"
#include "isextn.h"

#include "rmap.h"
/* Low Level GET Routine for All Objects  */
PRIVATE INT1 IsisCheckforMetricSupport ARG_LIST ((UINT4, UINT4 *, UINT4));

/****************************************************************************
 Function    :  nmhGetFsIsisMaxInstances
 Input       :  The Indices

                The Object 
                retValFsIsisMaxInstances
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxInstances (INT4 *pi4RetValFsIsisMaxInstances)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxInstances () \n"));

    *pi4RetValFsIsisMaxInstances = ISIS_ZERO;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxInstances ()  \n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxCircuits
 Input       :  The Indices

                The Object 
                retValFsIsisMaxCircuits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxCircuits (INT4 *pi4RetValFsIsisMaxCircuits)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxCircuits () \n"));

    *pi4RetValFsIsisMaxCircuits = ISIS_ZERO;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxCircuits ()  \n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxAreaAddrs
 Input       :  The Indices

                The Object 
                retValFsIsisMaxAreaAddrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxAreaAddrs (INT4 *pi4RetValFsIsisMaxAreaAddrs)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxAreaAddrs () \n"));

    *pi4RetValFsIsisMaxAreaAddrs = (INT4) gIsisMemActuals.u4MaxAreaAddr;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxAreaAddrs ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxAdjs
 Input       :  The Indices

                The Object 
                retValFsIsisMaxAdjs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxAdjs (INT4 *pi4RetValFsIsisMaxAdjs)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxAdjs () \n"));

    *pi4RetValFsIsisMaxAdjs = ISIS_ZERO;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxAdjs ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxIPRAs
 Input       :  The Indices

                The Object 
                retValFsIsisMaxIPRAs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxIPRAs (INT4 *pi4RetValFsIsisMaxIPRAs)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxIPRAs () \n"));

    *pi4RetValFsIsisMaxIPRAs = (INT4) gIsisMemActuals.u4MaxIPRA;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxIPRAs ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxEvents
 Input       :  The Indices

                The Object 
                retValFsIsisMaxEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxEvents (INT4 *pi4RetValFsIsisMaxEvents)
{

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxEvents () \n"));

    *pi4RetValFsIsisMaxEvents = (INT4) gIsisMemActuals.u4MaxEvents;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxEvents ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxSummAddr
 Input       :  The Indices

                The Object 
                retValFsIsisMaxSummAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxSummAddr (INT4 *pi4RetValFsIsisMaxSummAddr)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxSummAddr () \n"));

    *pi4RetValFsIsisMaxSummAddr = (INT4) gIsisMemActuals.u4MaxSA;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxSummAddr ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisStatus
 Input       :  The Indices

                The Object 
                retValFsIsisStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisStatus (INT4 *pi4RetValFsIsisStatus)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisStatus () \n"));

    *pi4RetValFsIsisStatus = gu1IsisStatus;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisStatus ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxLSPEntries
 Input       :  The Indices

                The Object 
                retValFsIsisMaxLSPEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxLSPEntries (INT4 *pi4RetValFsIsisMaxLSPEntries)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxLSPEntries () \n"));

    *pi4RetValFsIsisMaxLSPEntries = (INT4) gIsisMemActuals.u4MaxLSP;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxLSPEntries ()  \n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxMAA
 Input       :  The Indices

                The Object 
                retValFsIsisMaxMAA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxMAA (INT4 *pi4RetValFsIsisMaxMAA)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxAreaAddrs () \n"));

    *pi4RetValFsIsisMaxMAA = ISIS_ZERO;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxAreaAddrs ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisFTStatus
 Input       :  The Indices

                The Object 
                retValFsIsisFTStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisFTStatus (INT4 *pi4RetValFsIsisFTStatus)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisFTStatus () \n"));
#ifdef ISIS_FT_ENABLED

    *pi4RetValFsIsisFTStatus = ISIS_EXT_IS_FT_STATUS ();

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisFTStatus ()  \n"));

    return SNMP_SUCCESS;
#else
    /*UNUSED_PARAM (pi4RetValFsIsisFTStatus); */
    /*silvercreek */
    *pi4RetValFsIsisFTStatus = ISIS_FT_SUPP_DISABLE;
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisFTStatus ()  \n"));

    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIsisFTState
 Input       :  The Indices

                The Object 
                retValFsIsisFTState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisFTState (INT4 *pi4RetValFsIsisFTState)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisFTState () \n"));

#ifdef ISIS_FT_ENABLED

    *pi4RetValFsIsisFTState = ISIS_EXT_IS_FT_STATE ();

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisFTState ()  \n"));

    return SNMP_SUCCESS;

#else

    /*UNUSED_PARAM (pi4RetValFsIsisFTState); */
    /*silvercreek */
    *pi4RetValFsIsisFTState = ISIS_FT_SUPP_DISABLE;
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisFTState ()  \n"));

    return SNMP_SUCCESS;

#endif

}

/****************************************************************************
 Function    :  nmhGetFsIsisFactor
 Input       :  The Indices

                The Object 
                retValFsIsisFactor
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisFactor (INT4 *pi4RetValFsIsisFactor)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisFactor () \n"));

    *pi4RetValFsIsisFactor = (INT4) gIsisMemActuals.u4Factor;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisFactor ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisMaxRoutes
 Input       :  The Indices

                The Object 
                retValFsIsisMaxRoutes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMaxRoutes (INT4 *pi4RetValFsIsisMaxRoutes)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMaxRoutes () \n"));

    *pi4RetValFsIsisMaxRoutes = ISIS_ZERO;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMaxRoutes ()  \n"));

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisMaxInstances
 Input       :  The Indices

                The Object 
                setValFsIsisMaxInstances
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxInstances (INT4 i4SetValFsIsisMaxInstances)
{
    UNUSED_PARAM (i4SetValFsIsisMaxInstances);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxCircuits
 Input       :  The Indices

                The Object 
                setValFsIsisMaxCircuits
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxCircuits (INT4 i4SetValFsIsisMaxCircuits)
{
    UNUSED_PARAM (i4SetValFsIsisMaxCircuits);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxAreaAddrs
 Input       :  The Indices

                The Object 
                setValFsIsisMaxAreaAddrs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxAreaAddrs (INT4 i4SetValFsIsisMaxAreaAddrs)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisMaxAreaAddrs () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    gIsisMemConfigs.u4MaxAreaAddr = (UINT4) i4SetValFsIsisMaxAreaAddrs;
    gIsisMemActuals.u4MaxAreaAddr = gIsisMemConfigs.u4MaxAreaAddr;
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisMaxAreaAddrs ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxAdjs
 Input       :  The Indices

                The Object 
                setValFsIsisMaxAdjs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxAdjs (INT4 i4SetValFsIsisMaxAdjs)
{
    UNUSED_PARAM (i4SetValFsIsisMaxAdjs);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxIPRAs
 Input       :  The Indices

                The Object 
                setValFsIsisMaxIPRAs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxIPRAs (INT4 i4SetValFsIsisMaxIPRAs)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisMaxIPRAs () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    gIsisMemConfigs.u4MaxIPRA = (UINT4) i4SetValFsIsisMaxIPRAs;
    gIsisMemActuals.u4MaxIPRA = gIsisMemConfigs.u4MaxIPRA;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisMaxIPRAs ()  \n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxEvents
 Input       :  The Indices

                The Object 
                setValFsIsisMaxEvents
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxEvents (INT4 i4SetValFsIsisMaxEvents)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisMaxEvents () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    gIsisMemConfigs.u4MaxEvents = (UINT4) i4SetValFsIsisMaxEvents;
    gIsisMemActuals.u4MaxEvents = gIsisMemConfigs.u4MaxEvents;
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisMaxEvents ()  \n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxSummAddr
 Input       :  The Indices

                The Object 
                setValFsIsisMaxSummAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxSummAddr (INT4 i4SetValFsIsisMaxSummAddr)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisMaxSummAddr () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    gIsisMemConfigs.u4MaxSA = (UINT4) i4SetValFsIsisMaxSummAddr;
    gIsisMemActuals.u4MaxSA = gIsisMemConfigs.u4MaxSA;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisMaxSummAddr ()  \n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIsisStatus
 Input       :  The Indices

                The Object 
                setValFsIsisStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisStatus (INT4 i4SetValFsIsisStatus)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    tIsisEvtShutdown   *pEvtShutdown = NULL;
    tIsisEvtResetAll   *pEvtResetAll = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisStatus () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisStatus ()  \n"));
        return i1ErrCode;
    }
    gu1IsisStatus = (UINT1) i4SetValFsIsisStatus;
    switch (i4SetValFsIsisStatus)
    {
        case ISIS_RESET_ALL:

            pEvtResetAll = (tIsisEvtResetAll *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtResetAll));
            if (pEvtResetAll != NULL)
            {
                i1ErrCode = SNMP_SUCCESS;
                pEvtResetAll->u1EvtID = ISIS_EVT_RESETALL;
                NMP_PT ((ISIS_LGST, "NMP <T> : Posting ResetAll Event\n"));
                IsisUtlSendEvent (NULL, (UINT1 *) pEvtResetAll,
                                  sizeof (tIsisEvtResetAll));
            }
            break;

        case ISIS_SHUTDOWN:

            pEvtShutdown = (tIsisEvtShutdown *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtShutdown));
            if (pEvtShutdown != NULL)
            {
                i1ErrCode = SNMP_SUCCESS;
                pEvtShutdown->u1EvtID = ISIS_EVT_SHUTDOWN;

                NMP_PT ((ISIS_LGST, "NMP <T> : Posting Shutdown Event\n"));

                IsisUtlSendEvent (NULL, (UINT1 *) pEvtShutdown,
                                  sizeof (tIsisEvtShutdown));
            }
            break;

        case ISIS_IS_UP:        /*silvercreek */
            i1ErrCode = SNMP_SUCCESS;
            break;

        default:
            break;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisStatus ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxLSPEntries
 Input       :  The Indices

                The Object 
                setValFsIsisMaxLSPEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxLSPEntries (INT4 i4SetValFsIsisMaxLSPEntries)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisMaxLSPEntries () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    gIsisMemConfigs.u4MaxLSP = (UINT4) i4SetValFsIsisMaxLSPEntries;
    gIsisMemActuals.u4MaxLSP = gIsisMemConfigs.u4MaxLSP;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisMaxLSPEntries ()  \n"));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxMAA
 Input       :  The Indices

                The Object 
                setValFsIsisMaxMAA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxMAA (INT4 i4SetValFsIsisMaxMAA)
{
    UNUSED_PARAM (i4SetValFsIsisMaxMAA);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsIsisFTStatus
 Input       :  The Indices

                The Object 
                setValFsIsisFTStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisFTStatus (INT4 i4SetValFsIsisFTStatus)
{
#ifdef ISIS_FT_ENABLED
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisFTStatus () \n"));

    ISIS_EXT_SET_FT_STATUS ((UINT1) i4SetValFsIsisFTStatus);

    /* Register with Redundancy Manager if Fault tolerance 
     * is supported
     */

    if (ISIS_EXT_IS_FT_STATUS () != ISIS_FT_SUPP_DISABLE)
    {
        /* Register with the Redundancy Manager
         */

        IsisFltiRegWithRM ();
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisFTStatus ()  \n"));

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFsIsisFTStatus);
    return SNMP_SUCCESS;        /*silvercreek */
#endif
}

/****************************************************************************
 Function    :  nmhSetFsIsisFactor
 Input       :  The Indices

                The Object 
                setValFsIsisFactor
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisFactor (INT4 i4SetValFsIsisFactor)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisFactor () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    gIsisMemConfigs.u4Factor = (UINT4) i4SetValFsIsisFactor;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisFactor ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIsisMaxRoutes
 Input       :  The Indices

                The Object 
                setValFsIsisMaxRoutes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMaxRoutes (INT4 i4SetValFsIsisMaxRoutes)
{
    UNUSED_PARAM (i4SetValFsIsisMaxRoutes);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxInstances
 Input       :  The Indices

                The Object 
                testValFsIsisMaxInstances
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxInstances (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsIsisMaxInstances)
{
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (i4TestValFsIsisMaxInstances);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxCircuits
 Input       :  The Indices

                The Object 
                testValFsIsisMaxCircuits
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxCircuits (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsIsisMaxCircuits)
{
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (i4TestValFsIsisMaxCircuits);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxAreaAddrs
 Input       :  The Indices

                The Object 
                testValFsIsisMaxAreaAddrs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxAreaAddrs (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsIsisMaxAreaAddrs)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisMaxAreaAddrs () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisMaxAreaAddrs <= 0)
        || (i4TestValFsIsisMaxAreaAddrs > ISIS_MAX_AREA_ADDR_SUPP))
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Area Addr \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

    }
    else
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisMaxAreaAddrs ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxAdjs
 Input       :  The Indices

                The Object 
                testValFsIsisMaxAdjs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxAdjs (UINT4 *pu4ErrorCode, INT4 i4TestValFsIsisMaxAdjs)
{
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (i4TestValFsIsisMaxAdjs);  
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxIPRAs
 Input       :  The Indices

                The Object 
                testValFsIsisMaxIPRAs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxIPRAs (UINT4 *pu4ErrorCode, INT4 i4TestValFsIsisMaxIPRAs)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered " "nmhTestv2FsIsisMaxIPRAs () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisMaxIPRAs <= 0)
        || (i4TestValFsIsisMaxIPRAs > ISIS_MAX_IPRA_SUPP))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Max IPRA \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisMaxIPRAs ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxEvents
 Input       :  The Indices

                The Object 
                testValFsIsisMaxEvents
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxEvents (UINT4 *pu4ErrorCode, INT4 i4TestValFsIsisMaxEvents)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered " "nmhTestv2FsIsisMaxEvents () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisMaxEvents <= 0) ||
        (i4TestValFsIsisMaxEvents > ISIS_MAX_EVTS_SUPP))
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Event \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

    }
    else
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting " "nmhTestv2FsIsisMaxEvents ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxSummAddr
 Input       :  The Indices

                The Object 
                testValFsIsisMaxSummAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxSummAddr (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsIsisMaxSummAddr)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisMaxSummAddr () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisMaxSummAddr <= 0) ||
        i4TestValFsIsisMaxSummAddr > ISIS_MAX_SA_SUPP)
    {

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of Max Summary Address \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

    }
    else
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisMaxSummAddr ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisStatus
 Input       :  The Indices

                The Object 
                testValFsIsisStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsIsisStatus)
{

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2FsIsisStatus () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisStatus <= 0)
        || (i4TestValFsIsisStatus > ISIS_RESET_ALL))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Status  \n"));

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisStatus ()  \n"));

        return SNMP_FAILURE;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisStatus ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxLSPEntries
 Input       :  The Indices

                The Object 
               testValFsIsisMaxLSPEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxLSPEntries (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsIsisMaxLSPEntries)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisMaxLSPEntries () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisMaxLSPEntries <= 0) ||
        (i4TestValFsIsisMaxLSPEntries > ISIS_MAX_LSPS_SUPP))
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of  LSP Entry \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

    }
    else
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisMaxLSPEntries ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxMAA
 Input       :  The Indices

                The Object 
                testValFsIsisMaxMAA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxMAA (UINT4 *pu4ErrorCode, INT4 i4TestValFsIsisMaxMAA)
{
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (i4TestValFsIsisMaxMAA);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisFTStatus
 Input       :  The Indices

                The Object 
                testValFsIsisFTStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisFTStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsIsisFTStatus)
{
/*silvercreek*/
/*#ifdef ISIS_FT_ENABLED*/
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2FsIsisFTStatus () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisFTStatus != ISIS_FT_SUPP_ENABLE)
        && (i4TestValFsIsisFTStatus != ISIS_FT_SUPP_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisFTStatus ()  \n"));

    return i1ErrCode;
/*
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsIsisFTStatus);
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2FsIsisFTStatus () \n"));
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisFTStatus ()  \n"));
    return SNMP_SUCCESS;
#endif
*/

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisFactor
 Input       :  The Indices

                The Object 
                testValFsIsisFactor
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisFactor (UINT4 *pu4ErrorCode, INT4 i4TestValFsIsisFactor)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered " "nmhTestv2FsIsisFactor () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisFactor <= 0)
        || (i4TestValFsIsisFactor > ISIS_MAX_FACTOR))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Instance \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisFactor ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMaxRoutes
 Input       :  The Indices

                The Object 
                testValFsIsisMaxRoutes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMaxRoutes (UINT4 *pu4ErrorCode, INT4 i4TestValFsIsisMaxRoutes)
{
    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    UNUSED_PARAM (i4TestValFsIsisMaxRoutes);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsIsisExtSysTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtSysTable
 Input       :  The Indices
                FsIsisExtSysInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtSysTable (INT4 i4FsIsisExtSysInstance)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtSysTable ()\n"));

    if (i4FsIsisExtSysInstance < 0)
    {
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtSysTable () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtSysTable
 Input       :  The Indices
                FsIsisExtSysInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtSysTable (INT4 *pi4FsIsisExtSysInstance)
{

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtSysTable ()\n"));

    if (UtilIsisGetFirstCxtId ((UINT4 *) pi4FsIsisExtSysInstance) ==
        ISIS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtSysTable \n"));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtSysTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtSysTable (INT4 i4FsIsisExtSysInstance,
                                  INT4 *pi4NextFsIsisExtSysInstance)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexFsIsisExtSysTable ()\n"));

    i1ErrCode = nmhGetNextIndexIsisSysTable (i4FsIsisExtSysInstance,
                                             pi4NextFsIsisExtSysInstance);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSysTable \n"));
    return i1ErrCode;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysAuthSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysAuthSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysAuthSupp (INT4 i4FsIsisExtSysInstance,
                            INT4 *pi4RetValFsIsisExtSysAuthSupp)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtSysAuthSupp ()\n"));

    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysAuthSupp = pContext->SysActuals.u1SysAuthSupp;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtSysAuthSupp () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysAreaAuthType
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysAreaAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysAreaAuthType (INT4 i4FsIsisExtSysInstance,
                                INT4 *pi4RetValFsIsisExtSysAreaAuthType)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysAreaAuthType ()\n"));

    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysAreaAuthType =
            pContext->SysActuals.u1SysAreaAuthType;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysAreaAuthType () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysDomainAuthType
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysDomainAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysDomainAuthType (INT4 i4FsIsisExtSysInstance,
                                  INT4 *pi4RetValFsIsisExtSysDomainAuthType)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysDomainAuthType ()\n"));

    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysDomainAuthType =
            pContext->SysActuals.u1SysDomainAuthType;
        i1ErrCode = SNMP_SUCCESS;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysDomainAuthType () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysAreaTxPasswd
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysAreaTxPasswd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysAreaTxPasswd (INT4 i4FsIsisExtSysInstance,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsIsisExtSysAreaTxPasswd)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysAreaTxPasswd ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (pContext->SysActuals.SysAreaTxPasswd.u1Len <= ISIS_MAX_PASSWORD_LEN)
        {
            pRetValFsIsisExtSysAreaTxPasswd->i4_Length
                = pContext->SysActuals.SysAreaTxPasswd.u1Len;

            MEMCPY (pRetValFsIsisExtSysAreaTxPasswd->pu1_OctetList,
                    pContext->SysActuals.SysAreaTxPasswd.au1Password,
                    pContext->SysActuals.SysAreaTxPasswd.u1Len);

            i1ErrCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysAreaTxPasswd () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysDomainTxPasswd
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysDomainTxPasswd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysDomainTxPasswd (INT4 i4FsIsisExtSysInstance,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsIsisExtSysDomainTxPasswd)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysDomainTxPasswd ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (pContext->SysActuals.SysDomainTxPasswd.u1Len <=
            ISIS_MAX_PASSWORD_LEN)
        {
            pRetValFsIsisExtSysDomainTxPasswd->i4_Length
                = pContext->SysActuals.SysDomainTxPasswd.u1Len;
            MEMCPY (pRetValFsIsisExtSysDomainTxPasswd->pu1_OctetList,
                    pContext->SysActuals.SysDomainTxPasswd.au1Password,
                    pContext->SysActuals.SysDomainTxPasswd.u1Len);
            i1ErrCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysDomainTxPasswd () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysMinSPFSchTime
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysMinSPFSchTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysMinSPFSchTime (INT4 i4FsIsisExtSysInstance,
                                 INT4 *pi4RetValFsIsisExtSysMinSPFSchTime)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysMinSPFSchTime ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysMinSPFSchTime =
            (INT4) pContext->SysActuals.u2MinSPFSchTime;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysMinSPFSchTime () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysMaxSPFSchTime
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysMaxSPFSchTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysMaxSPFSchTime (INT4 i4FsIsisExtSysInstance,
                                 INT4 *pi4RetValFsIsisExtSysMaxSPFSchTime)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysMaxSPFSchTime ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysMaxSPFSchTime =
            (INT4) pContext->SysActuals.u2MaxSPFSchTime;
        i1ErrCode = SNMP_SUCCESS;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysMaxSPFSchTime () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysMinLSPMark
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysMinLSPMark
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysMinLSPMark (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtSysMinLSPMark)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysMinLSPMark ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysMinLSPMark =
            (INT4) pContext->SysActuals.u2MinLSPMark;
        i1ErrCode = SNMP_SUCCESS;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysMinLSPMark () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysMaxLSPMark
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysMaxLSPMark
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysMaxLSPMark (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtSysMaxLSPMark)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysMaxLSPMark ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysMaxLSPMark =
            (INT4) pContext->SysActuals.u2MaxLSPMark;
        i1ErrCode = SNMP_SUCCESS;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysMaxLSPMark () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysDelMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysDelMetSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysDelMetSupp (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtSysDelMetSupp)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysDelMetSupp ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysDelMetSupp =
            (INT4) (((pContext->SysConfigs.u1SysMetricSupp &
                      ISIS_DEL_MET_SUPP_FLAG) != 0) ? ISIS_TRUE : ISIS_FALSE);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysDelMetSupp () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysErrMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysErrMetSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysErrMetSupp (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtSysErrMetSupp)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysErrMetSupp ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysErrMetSupp =
            (INT4) (((pContext->SysConfigs.u1SysMetricSupp &
                      ISIS_ERR_MET_SUPP_FLAG) != 0) ? ISIS_TRUE : ISIS_FALSE);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysErrMetSupp () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysExpMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysExpMetSupp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysExpMetSupp (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtSysExpMetSupp)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysExpMetSupp ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysExpMetSupp =
            (INT4) (((pContext->SysConfigs.u1SysMetricSupp &
                      ISIS_EXP_MET_SUPP_FLAG) != 0) ? ISIS_TRUE : ISIS_FALSE);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysExpMetSupp () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysActSysType
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysActSysType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysActSysType (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtSysActSysType)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysActSysType ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysActSysType =
            (INT4) (pContext->SysActuals.u1SysType);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysActSysType () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysActMPS
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysActMPS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysActMPS (INT4 i4FsIsisExtSysInstance,
                          INT4 *pi4RetValFsIsisExtSysActMPS)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered " "nmhGetFsIsisExtSysActMPS ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysActMPS = (INT4) (pContext->SysActuals.u1SysMPS);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting " "nmhGetFsIsisExtSysActMPS () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysActMaxAA
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysActMaxAA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysActMaxAA (INT4 i4FsIsisExtSysInstance,
                            INT4 *pi4RetValFsIsisExtSysActMaxAA)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysActMaxAA ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysActMaxAA =
            (INT4) (pContext->SysActuals.u1SysMaxAA);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysActMaxAA () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysActSysIDLen
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysActSysIDLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysActSysIDLen (INT4 i4FsIsisExtSysInstance,
                               INT4 *pi4RetValFsIsisExtSysActSysIDLen)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysActSysIDLen ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysActSysIDLen =
            (INT4) (pContext->SysActuals.u1SysIDLen);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysActSysIDLen () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysActSysID
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysActSysID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysActSysID (INT4 i4FsIsisExtSysInstance,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsIsisExtSysActSysID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysActSysID ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        pRetValFsIsisExtSysActSysID->i4_Length = ISIS_SYS_ID_LEN;
        MEMCPY (pRetValFsIsisExtSysActSysID->pu1_OctetList,
                pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysActSysID () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysDroppedPDUs
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysDroppedPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysDroppedPDUs (INT4 i4FsIsisExtSysInstance,
                               INT4 *pi4RetValFsIsisExtSysDroppedPDUs)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysDroppedPDUs () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysDroppedPDUs = (INT4) pContext->SysStats.u4SysDroppedPDUs;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtSysDroppedPDUs () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysActOrigL1LSPBufSize
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysActOrigL1LSPBufSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysActOrigL1LSPBufSize (INT4 i4FsIsisExtSysInstance,
                                       INT4
                                       *pi4RetValFsIsisExtSysActOrigL1LSPBufSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysActOrigL1LSPBufSize ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysActOrigL1LSPBufSize =
            (INT4) (pContext->SysActuals.u4SysOrigL1LSPBufSize);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysActOrigL1LSPBufSize () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysActOrigL2LSPBufSize
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysActOrigL2LSPBufSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysActOrigL2LSPBufSize (INT4 i4FsIsisExtSysInstance,
                                       INT4
                                       *pi4RetValFsIsisExtSysActOrigL2LSPBufSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysActOrigL2LSPBufSize ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysActOrigL2LSPBufSize =
            (INT4) (pContext->SysActuals.u4SysOrigL2LSPBufSize);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysActOrigL2LSPBufSize () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysRouterID
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysRouterID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysRouterID (INT4 i4FsIsisExtSysInstance,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsIsisExtSysRouterID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtSysRouterID ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (pContext->SysRouterID.u1Length < ISIS_MAX_IP_ADDR_LEN)
        {                        /*Klockwork */
            MEMCPY (pRetValFsIsisExtSysRouterID->pu1_OctetList,
                    pContext->SysRouterID.au1IpAddr,
                    pContext->SysRouterID.u1Length);
            pRetValFsIsisExtSysRouterID->i4_Length =
                pContext->SysRouterID.u1Length;
            i1ErrCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtSysRouterID () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysCkts
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysCkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysCkts (INT4 i4FsIsisExtSysInstance,
                        INT4 *pi4RetValFsIsisExtSysCkts)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtSysCkts () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysCkts = (INT4) pContext->CktTable.u4NumEntries;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtSysCkts ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysActiveCkts
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysActiveCkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysActiveCkts (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtSysActiveCkts)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtSysActiveCkts () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysActiveCkts = (INT4) pContext->CktTable.u4NumActCkts;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtSysActiveCkts ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysAdjs
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysAdjs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysAdjs (INT4 i4FsIsisExtSysInstance,
                        INT4 *pi4RetValFsIsisExtSysAdjs)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtSysAdjs () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysAdjs = (INT4) (pContext->CktTable.u4NumL1Adjs +
            pContext->CktTable.u4NumL2Adjs);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtSysAdjs ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysOperState
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysOperState (INT4 i4FsIsisExtSysInstance,
                             INT4 *pi4RetValFsIsisExtSysOperState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtSysOperState () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysOperState = pContext->u1OperState;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtSysCkts ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtRestartSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtRestartSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtRestartSupport (INT4 i4FsIsisExtSysInstance,
                               INT4 *pi4RetValFsIsisExtRestartSupport)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtRestartSupport () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtRestartSupport =
            (INT4) pContext->u1IsisGRRestartSupport;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtRestartSupport ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtGRRestartTimeInterval
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtGRRestartTimeInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtGRRestartTimeInterval (INT4 i4FsIsisExtSysInstance,
                                      INT4
                                      *pi4RetValFsIsisExtGRRestartTimeInterval)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtGRRestartTimeInterval() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtGRRestartTimeInterval =
            (INT4) pContext->u2IsisGRT3TimerInterval;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtGRRestartTimeInterval ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtGRT2TimeIntervalLevel1
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtGRT2TimeIntervalLevel1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtGRT2TimeIntervalLevel1 (INT4 i4FsIsisExtSysInstance,
                                       INT4
                                       *pi4RetValFsIsisExtGRT2TimeIntervalLevel1)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtGRT2TimeIntervalLevel1() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtGRT2TimeIntervalLevel1 =
            (INT4) pContext->u2IsisGRT2TimerL1Interval;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtGRT2TimeIntervalLevel1()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtGRT2TimeIntervalLevel2
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtGRT2TimeIntervalLevel2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtGRT2TimeIntervalLevel2 (INT4 i4FsIsisExtSysInstance,
                                       INT4
                                       *pi4RetValFsIsisExtGRT2TimeIntervalLevel2)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtGRT2TimeIntervalLevel2() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtGRT2TimeIntervalLevel2 =
            (INT4) pContext->u2IsisGRT2TimerL2Interval;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtGRT2TimeIntervalLevel2()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtGRT1TimeInterval
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtGRT1TimeInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtGRT1TimeInterval (INT4 i4FsIsisExtSysInstance,
                                 INT4 *pi4RetValFsIsisExtGRT1TimeInterval)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtGRT1TimeInterval () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtGRT1TimeInterval =
            (INT4) pContext->u1IsisGRT1TimerInterval;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtGRT1TimeInterval ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtGRT1RetryCount
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtGRT1RetryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtGRT1RetryCount (INT4 i4FsIsisExtSysInstance,
                               INT4 *pi4RetValFsIsisExtGRT1RetryCount)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtGRT1RetryCount() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtGRT1RetryCount =
            (INT4) pContext->u1IsisGRMaxT1RetryCount;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtGRT1RetryCount()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtGRMode
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtGRMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtGRMode (INT4 i4FsIsisExtSysInstance,
                       INT4 *pi4RetValFsIsisExtGRMode)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtGRMode() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtGRMode = (INT4) pContext->u1IsisGRRestartMode;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtGRMode()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtRestartStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtRestartStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtRestartStatus (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtRestartStatus)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtRestartStatus() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtRestartStatus =
            (INT4) pContext->u1IsisGRRestartStatus;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtRestartStatus()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtRestartExitReason
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtRestartExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtRestartExitReason (INT4 i4FsIsisExtSysInstance,
                                  INT4 *pi4RetValFsIsisExtRestartExitReason)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtRestartExitReason() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtRestartExitReason =
            (INT4) pContext->u1IsisGRRestartExitReason;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtRestartExitReason()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtRestartReason
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtRestartReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtRestartReason (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtRestartReason)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtRestartReason () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtRestartReason =
            (INT4) pContext->u1IsisGRRestartReason;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtRestartReason ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtHelperSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtHelperSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtHelperSupport (INT4 i4FsIsisExtSysInstance,
                              INT4 *pi4RetValFsIsisExtHelperSupport)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtHelperSupport () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtHelperSupport =
            (INT4) pContext->u1IsisGRHelperSupport;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtHelperSupport ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtHelperGraceTimeLimit
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtHelperGraceTimeLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtHelperGraceTimeLimit (INT4 i4FsIsisExtSysInstance,
                                     INT4
                                     *pi4RetValFsIsisExtHelperGraceTimeLimit)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtHelperGraceTimeLimit () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtHelperGraceTimeLimit =
            (INT4) pContext->u2IsisGRHelperTimeLimit;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtHelperGraceTimeLimit ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysBfdSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object
                retValFsIsisExtSysBfdSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIsisExtSysBfdSupport (INT4 i4FsIsisExtSysInstance, 
                              INT4 *pi4RetValFsIsisExtSysBfdSupport)
{
    INT1                i1Ret = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtSysBfdSupport () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysBfdSupport =
            (INT4) pContext->u1IsisBfdSupport;
        i1Ret = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtSysBfdSupport ()\n"));

    return i1Ret;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysBfdAllIfStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object
                retValFsIsisExtSysBfdAllIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIsisExtSysBfdAllIfStatus (INT4 i4FsIsisExtSysInstance, 
                                  INT4 *pi4RetValFsIsisExtSysBfdAllIfStatus)
{
    INT1                i1Ret = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtSysBfdAllIfStatus () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysBfdAllIfStatus =
            (INT4) pContext->u1IsisBfdAllIfStatus;
        i1Ret = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtSysBfdAllIfStatus ()\n"));

    return i1Ret;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysAuthSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysAuthSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysAuthSupp (INT4 i4FsIsisExtSysInstance,
                            INT4 i4SetValFsIsisExtSysAuthSupp)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysAuthSupp ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i1ErrCode = SNMP_SUCCESS;
        pContext->SysActuals.u1SysAuthSupp =
            (UINT1) i4SetValFsIsisExtSysAuthSupp;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtSysAuthSupp () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysAreaAuthType
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysAreaAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysAreaAuthType (INT4 i4FsIsisExtSysInstance,
                                INT4 i4SetValFsIsisExtSysAreaAuthType)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysAreaAuthType ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        pContext->SysActuals.u1SysAreaAuthType =
            (UINT1) i4SetValFsIsisExtSysAreaAuthType;

        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysAreaAuthType () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysDomainAuthType
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysDomainAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysDomainAuthType (INT4 i4FsIsisExtSysInstance,
                                  INT4 i4SetValFsIsisExtSysDomainAuthType)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysDomainAuthType ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        pContext->SysActuals.u1SysDomainAuthType =
            (UINT1) i4SetValFsIsisExtSysDomainAuthType;

        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysDomainAuthType () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysAreaTxPasswd
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysAreaTxPasswd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysAreaTxPasswd (INT4 i4FsIsisExtSysInstance,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsIsisExtSysAreaTxPasswd)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisLSPInfo       *pLSPInfo = NULL;
    tIsisEvtDISChg     *pDISEvt = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysAreaTxPasswd ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (pSetValFsIsisExtSysAreaTxPasswd->i4_Length == 0)
        {
            MEMSET (pContext->SysActuals.SysAreaTxPasswd.au1Password, 0x00,
                    ISIS_MAX_PASSWORD_LEN);
        }
        else
        {
            MEMCPY (pContext->SysActuals.SysAreaTxPasswd.au1Password,
                    pSetValFsIsisExtSysAreaTxPasswd->pu1_OctetList,
                    pSetValFsIsisExtSysAreaTxPasswd->i4_Length);
        }
        pContext->SysActuals.SysAreaTxPasswd.u1Len
            = (UINT1) pSetValFsIsisExtSysAreaTxPasswd->i4_Length;

        if ((pContext->SelfLSP.pL1NSNLSP != NULL)
            && (pContext->SelfLSP.pL1NSNLSP->pZeroLSP != NULL))
        {
            pContext->SelfLSP.pL1NSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        }
        if ((pContext->SelfLSP.pL1SNLSP != NULL)
            && (pContext->SelfLSP.pL1SNLSP->pZeroLSP != NULL))
        {
            pContext->SelfLSP.pL1SNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        }
		
                if (pSetValFsIsisExtSysAreaTxPasswd->i4_Length != 0)
                {
		/*Flush the L1 Database since all the previously learnt LSPs are invalid*/
		IsisUpdPurgeLSPs (pContext, ISIS_LEVEL1);
                }
		
		/*setting Dirty flag for Non-Zero LSPs*/
        /*Non-PseudoNodes */
        if (pContext->SelfLSP.pL1NSNLSP != NULL)
        {
            pLSPInfo = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
            while (pLSPInfo != NULL)
            {
                pLSPInfo->u1DirtyFlag = ISIS_MODIFIED;
                pLSPInfo = pLSPInfo->pNext;
            }
        }
        /*PseudoNodes */
        if (pContext->SelfLSP.pL1SNLSP != NULL)
        {
            pLSPInfo = pContext->SelfLSP.pL1SNLSP->pNonZeroLSP;
            while (pLSPInfo != NULL)
            {
                pLSPInfo->u1DirtyFlag = ISIS_MODIFIED;
                pLSPInfo = pLSPInfo->pNext;
            }
        }

        /* Delete and Regenerate the Pseudonode LSPs. IS_ADJ_TLV will be added 
         * only during the DIS election. Hence posting a DIS change event,
         * so that existing pseudonode LSPs are deleted and new pseudonode LSPs are
         * generated. */

if(pSetValFsIsisExtSysAreaTxPasswd->i4_Length != 0)
{
        pCktRec = pContext->CktTable.pCktRec;

        while (pCktRec != NULL)
        {
            if ((pCktRec->pL1CktInfo != NULL) &&
                (pCktRec->bCktAdminState == ISIS_STATE_ON) &&
                (pCktRec->u1CktIfStatus == ISIS_STATE_ON) &&
                (pCktRec->u1CktExistState == ISIS_ACTIVE))
            {
                pDISEvt = (tIsisEvtDISChg *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDISChg));
                if (pDISEvt != NULL)
                {
                    pDISEvt->u1EvtID = ISIS_EVT_DIS_CHANGE;
                    pDISEvt->u1CktLevel = ISIS_LEVEL1;
                    pDISEvt->u4CktIdx = pCktRec->u4CktIdx;
                    MEMCPY (pDISEvt->au1PrevDIS, 
                            pCktRec->pL1CktInfo->au1CktLanDISID,
                            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                    MEMCPY (pDISEvt->au1CurrDIS,
                            pCktRec->pL1CktInfo->au1CktLanDISID,
                            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                    IsisUpdProcDISChgEvt (pContext, pDISEvt);
                }
            }
            if ((pCktRec->pL1CktInfo != NULL) &&
                (pCktRec->pL1CktInfo->bIsDIS == ISIS_TRUE))
            {
                IsisUpdProcDISStatChgEvt (pContext, pCktRec, ISIS_LEVEL1,
                        ISIS_DIS_ELECTED);
            }
            pCktRec = pCktRec->pNext;
        }
}

		i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysAreaTxPasswd () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysDomainTxPasswd
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysDomainTxPasswd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysDomainTxPasswd (INT4 i4FsIsisExtSysInstance,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsIsisExtSysDomainTxPasswd)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisLSPInfo       *pLSPInfo = NULL;
    tIsisEvtDISChg     *pDISEvt = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysDomainTxPasswd ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (pSetValFsIsisExtSysDomainTxPasswd->i4_Length == 0)
        {
            MEMSET (pContext->SysActuals.SysDomainTxPasswd.au1Password, 0x00,
                    ISIS_MAX_PASSWORD_LEN);
        }
        else
        {
            MEMCPY (pContext->SysActuals.SysDomainTxPasswd.au1Password,
                    pSetValFsIsisExtSysDomainTxPasswd->pu1_OctetList,
                    pSetValFsIsisExtSysDomainTxPasswd->i4_Length);
        }
        pContext->SysActuals.SysDomainTxPasswd.u1Len
            = (UINT1) pSetValFsIsisExtSysDomainTxPasswd->i4_Length;

        if ((pContext->SelfLSP.pL2NSNLSP != NULL)
            && (pContext->SelfLSP.pL2NSNLSP->pZeroLSP != NULL))
        {
            pContext->SelfLSP.pL2NSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        }
        if ((pContext->SelfLSP.pL2SNLSP != NULL)
            && (pContext->SelfLSP.pL2SNLSP->pZeroLSP != NULL))
        {
            pContext->SelfLSP.pL2SNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        }
      
       if(pSetValFsIsisExtSysDomainTxPasswd->i4_Length != 0)
       {
		/*Flush the L2 Database since all the previously learnt LSPs are invalid*/
		IsisUpdPurgeLSPs (pContext, ISIS_LEVEL2);
       }
        
		/*setting Dirty flag for Non-Zero LSPs*/
        /*Non-PseudoNodes */
        if (pContext->SelfLSP.pL2NSNLSP != NULL)
        {
            pLSPInfo = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
            while (pLSPInfo != NULL)
            {
                pLSPInfo->u1DirtyFlag = ISIS_MODIFIED;
                pLSPInfo = pLSPInfo->pNext;
            }
        }
        /*PseudoNodes */
        if (pContext->SelfLSP.pL2SNLSP != NULL)
        {
            pLSPInfo = pContext->SelfLSP.pL2SNLSP->pNonZeroLSP;
            while (pLSPInfo != NULL)
            {
                pLSPInfo->u1DirtyFlag = ISIS_MODIFIED;
                pLSPInfo = pLSPInfo->pNext;
            }
        }

        /* Delete and Regenerate the Pseudonode LSPs. IS_ADJ_TLV will be added 
         * only during the DIS election. Hence posting a DIS change event,
         * so that existing pseudonode LSPs are deleted and new pseudonode LSPs are
         * generated. */
        if(pSetValFsIsisExtSysDomainTxPasswd->i4_Length != 0)
        {
        pCktRec = pContext->CktTable.pCktRec;

        while (pCktRec != NULL)
        {
            if ((pCktRec->pL2CktInfo != NULL) &&
                (pCktRec->bCktAdminState == ISIS_STATE_ON) &&
                (pCktRec->u1CktIfStatus == ISIS_STATE_ON) &&
                (pCktRec->u1CktExistState == ISIS_ACTIVE))
            {
                pDISEvt = (tIsisEvtDISChg *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDISChg));
                if (pDISEvt != NULL)
                {
                    pDISEvt->u1EvtID = ISIS_EVT_DIS_CHANGE;
                    pDISEvt->u1CktLevel = ISIS_LEVEL2;
                    pDISEvt->u4CktIdx = pCktRec->u4CktIdx;
                    MEMCPY (pDISEvt->au1PrevDIS, 
                            pCktRec->pL2CktInfo->au1CktLanDISID,
                            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                    MEMCPY (pDISEvt->au1CurrDIS,
                            pCktRec->pL2CktInfo->au1CktLanDISID,
                            ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

                    IsisUpdProcDISChgEvt (pContext, pDISEvt);
                }
            }
            if ((pCktRec->pL2CktInfo != NULL) &&
                (pCktRec->pL2CktInfo->bIsDIS == ISIS_TRUE))
            {
                IsisUpdProcDISStatChgEvt (pContext, pCktRec, ISIS_LEVEL2,
                        ISIS_DIS_ELECTED);
            }
            pCktRec = pCktRec->pNext;
        }
         }
		i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysDomainTxPasswd () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysMinSPFSchTime
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysMinSPFSchTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysMinSPFSchTime (INT4 i4FsIsisExtSysInstance,
                                 INT4 i4SetValFsIsisExtSysMinSPFSchTime)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysMinSPFSchTime ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->SysActuals.u2MinSPFSchTime =
            (UINT2) i4SetValFsIsisExtSysMinSPFSchTime;

        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysMinSPFSchTime () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysMaxSPFSchTime
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysMaxSPFSchTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysMaxSPFSchTime (INT4 i4FsIsisExtSysInstance,
                                 INT4 i4SetValFsIsisExtSysMaxSPFSchTime)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysMaxSPFSchTime ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->SysActuals.u2MaxSPFSchTime =
            (UINT2) i4SetValFsIsisExtSysMaxSPFSchTime;

        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysMaxSPFSchTime () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysMinLSPMark
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysMinLSPMark
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysMinLSPMark (INT4 i4FsIsisExtSysInstance,
                              INT4 i4SetValFsIsisExtSysMinLSPMark)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysMinLSPMark ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        pContext->SysActuals.u2MinLSPMark =
            (UINT2) i4SetValFsIsisExtSysMinLSPMark;

        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysMinLSPMark () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysMaxLSPMark
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysMaxLSPMark
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysMaxLSPMark (INT4 i4FsIsisExtSysInstance,
                              INT4 i4SetValFsIsisExtSysMaxLSPMark)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysMaxLSPMark ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        pContext->SysActuals.u2MaxLSPMark =
            (UINT2) i4SetValFsIsisExtSysMaxLSPMark;

        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysMaxLSPMark () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysDelMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysDelMetSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysDelMetSupp (INT4 i4FsIsisExtSysInstance,
                              INT4 i4SetValFsIsisExtSysDelMetSupp)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysDelMetSupp ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (i4SetValFsIsisExtSysDelMetSupp == ISIS_TRUE)
        {
            pContext->SysConfigs.u1SysMetricSupp |= ISIS_DEL_MET_SUPP_FLAG;
        }
        else if (i4SetValFsIsisExtSysDelMetSupp == ISIS_FALSE)
        {
            pContext->SysConfigs.u1SysMetricSupp &= ~ISIS_DEL_MET_SUPP_FLAG;
        }
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysDelMetSupp () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysErrMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysErrMetSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysErrMetSupp (INT4 i4FsIsisExtSysInstance,
                              INT4 i4SetValFsIsisExtSysErrMetSupp)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysErrMetSupp ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (i4SetValFsIsisExtSysErrMetSupp == ISIS_TRUE)
        {
            pContext->SysConfigs.u1SysMetricSupp |= ISIS_ERR_MET_SUPP_FLAG;
        }
        else if (i4SetValFsIsisExtSysErrMetSupp == ISIS_FALSE)
        {
            pContext->SysConfigs.u1SysMetricSupp &= ~ISIS_ERR_MET_SUPP_FLAG;
        }
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysErrMetSupp () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysExpMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysExpMetSupp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysExpMetSupp (INT4 i4FsIsisExtSysInstance,
                              INT4 i4SetValFsIsisExtSysExpMetSupp)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysExpMetSupp ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (i4SetValFsIsisExtSysExpMetSupp == ISIS_TRUE)
        {
            pContext->SysConfigs.u1SysMetricSupp |= ISIS_EXP_MET_SUPP_FLAG;
        }
        else if (i4SetValFsIsisExtSysExpMetSupp == ISIS_FALSE)
        {
            pContext->SysConfigs.u1SysMetricSupp &= ~ISIS_EXP_MET_SUPP_FLAG;
        }
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysExpMetSupp () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysRouterID
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysRouterID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysRouterID (INT4 i4FsIsisExtSysInstance,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsIsisExtSysRouterID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisExtSysRouterID ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        MEMCPY (pContext->SysRouterID.au1IpAddr,
                pSetValFsIsisExtSysRouterID->pu1_OctetList,
                pSetValFsIsisExtSysRouterID->i4_Length);
        pContext->SysRouterID.u1Length =
            (UINT1) pSetValFsIsisExtSysRouterID->i4_Length;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtSysRouterID () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtRestartSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtRestartSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtRestartSupport (INT4 i4FsIsisExtSysInstance,
                               INT4 i4SetValFsIsisExtRestartSupport)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered"
             "nmhSetFsIsisExtRestartSupport ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u1IsisGRRestartSupport =
            (UINT1) i4SetValFsIsisExtRestartSupport;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtRestartSupport () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtGRRestartTimeInterval
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtGRRestartTimeInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtGRRestartTimeInterval (INT4 i4FsIsisExtSysInstance,
                                      INT4
                                      i4SetValFsIsisExtGRRestartTimeInterval)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered"
             "nmhSetFsIsisExtGRRestartTimeInterval ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u2IsisGRT3TimerInterval =
            (UINT2) i4SetValFsIsisExtGRRestartTimeInterval;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtGRRestartTimeInterval () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtGRT2TimeIntervalLevel1
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtGRT2TimeIntervalLevel1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtGRT2TimeIntervalLevel1 (INT4 i4FsIsisExtSysInstance,
                                       INT4
                                       i4SetValFsIsisExtGRT2TimeIntervalLevel1)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered"
             "nmhSetFsIsisExtGRT2TimeIntervalLevel1 ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u2IsisGRT2TimerL1Interval = (UINT2)
            i4SetValFsIsisExtGRT2TimeIntervalLevel1;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtGRT2TimeIntervalLevel1 () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtGRT2TimeIntervalLevel2
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtGRT2TimeIntervalLevel2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtGRT2TimeIntervalLevel2 (INT4 i4FsIsisExtSysInstance,
                                       INT4
                                       i4SetValFsIsisExtGRT2TimeIntervalLevel2)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered"
             "nmhSetFsIsisExtGRT2TimeIntervalLevel2 ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u2IsisGRT2TimerL2Interval = (UINT2)
            i4SetValFsIsisExtGRT2TimeIntervalLevel2;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtGRT2TimeIntervalLevel2 () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtGRT1TimeInterval
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtGRT1TimeInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtGRT1TimeInterval (INT4 i4FsIsisExtSysInstance,
                                 INT4 i4SetValFsIsisExtGRT1TimeInterval)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered"
             "nmhSetFsIsisExtGRT1TimeInterval ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u1IsisGRT1TimerInterval =
            (UINT1) i4SetValFsIsisExtGRT1TimeInterval;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtGRT1TimeInterval () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtGRT1RetryCount
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtGRT1RetryCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtGRT1RetryCount (INT4 i4FsIsisExtSysInstance,
                               INT4 i4SetValFsIsisExtGRT1RetryCount)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered"
             "nmhSetFsIsisExtGRT1RetryCount ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u1IsisGRMaxT1RetryCount =
            (UINT1) i4SetValFsIsisExtGRT1RetryCount;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtGRT1RetryCount () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtRestartReason
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtRestartReason
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtRestartReason (INT4 i4FsIsisExtSysInstance,
                              INT4 i4SetValFsIsisExtRestartReason)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered"
             "nmhSetFsIsisExtRestartReason ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u1IsisGRRestartReason =
            (UINT1) i4SetValFsIsisExtRestartReason;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtRestartReason () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtHelperSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtHelperSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtHelperSupport (INT4 i4FsIsisExtSysInstance,
                              INT4 i4SetValFsIsisExtHelperSupport)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered"
             "nmhSetFsIsisExtHelperSupport ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u1IsisGRHelperSupport =
            (UINT1) i4SetValFsIsisExtHelperSupport;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtHelperSupport () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtHelperGraceTimeLimit
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtHelperGraceTimeLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtHelperGraceTimeLimit (INT4 i4FsIsisExtSysInstance,
                                     INT4 i4SetValFsIsisExtHelperGraceTimeLimit)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtHelperGraceTimeLimit ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        pContext->u2IsisGRHelperTimeLimit = (UINT2)
            i4SetValFsIsisExtHelperGraceTimeLimit;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtHelperGraceTimeLimit () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysBfdSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object
                setValFsIsisExtSysBfdSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsIsisExtSysBfdSupport (INT4 i4FsIsisExtSysInstance, 
                              INT4 i4SetValFsIsisExtSysBfdSupport)
{
#ifdef BFD_WANTED 
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pTravCktRec = NULL;
    tIsisAdjEntry      *pTravAdj = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysBfdSupport ()\n"));

#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif

    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        if ((UINT1) i4SetValFsIsisExtSysBfdSupport ==
             pContext->u1IsisBfdSupport)
        {
            return SNMP_SUCCESS;
        }

        pContext->u1IsisBfdSupport = (UINT1) i4SetValFsIsisExtSysBfdSupport;
        i1ErrCode = SNMP_SUCCESS;

        if (i4SetValFsIsisExtSysBfdSupport == ISIS_BFD_DISABLE)
        {
            /* Deregister from BFD if registered previously */
            pTravCktRec = pContext->CktTable.pCktRec;
            while (pTravCktRec != NULL)
            {
                if (pTravCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE)
                {
                    pTravAdj = pTravCktRec->pAdjEntry;
                    while (pTravAdj != NULL)
                    {
                        ISIS_BFD_DEREGISTER(pContext, pTravAdj, pTravCktRec, ISIS_TRUE);
                        pTravAdj = pTravAdj->pNext;
                    }
                    pTravCktRec->u1IsisBfdStatus = ISIS_BFD_DISABLE;
                }
                pTravCktRec = pTravCktRec->pNext;
            }
            pContext->u1IsisBfdAllIfStatus = ISIS_BFD_DISABLE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysBfdSupport () \n"));

    return i1ErrCode;
#else
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4SetValFsIsisExtSysBfdSupport);
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysBfdAllIfStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object
                setValFsIsisExtSysBfdAllIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsIsisExtSysBfdAllIfStatus (INT4 i4FsIsisExtSysInstance, 
                                  INT4 i4SetValFsIsisExtSysBfdAllIfStatus)
{
#ifdef BFD_WANTED
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pTravCktRec = NULL;
    tIsisAdjEntry      *pTravAdj = NULL;
    UINT4               u4IfIndex = 0;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysBfdAllIfStatus ()\n"));

#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif

    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) == ISIS_SUCCESS)
    {
        if ((UINT1) i4SetValFsIsisExtSysBfdAllIfStatus ==
            pContext->u1IsisBfdAllIfStatus)
        {
            return SNMP_SUCCESS;
        }

        /* BFD support for ISIS should be enabled 
           to enable BFD support at interface level*/
        if (pContext->u1IsisBfdSupport == ISIS_TRUE)

        {
            pContext->u1IsisBfdAllIfStatus = (UINT1)
                i4SetValFsIsisExtSysBfdAllIfStatus;

            /* While enabling/disabling BFD on all interfaces, do
             * the following:
             * While enabling (bfd all-interfaces):
             * 1. Enable BFD on all circuit records.
             * 2. If single-topology ISIS is enabled, check for all the
             *    Adjacencies in the circuit record. If the Adjacency
             *    state is UP, do BFD Registration.
             *    In case of multi-topology ISIS, BFD Registration
             *    will be done on the go while computing BFD_REQUIRED
             *    variable.
             * While disabling (no bfd all-interfaces):
             * 1. Disable BFD on all circuit records.
             * 2. Deregister from BFD for all the Adjacencies (both
             *    single topology and multi-topology adjs). */

            pTravCktRec = pContext->CktTable.pCktRec;
            while (pTravCktRec != NULL)
            {
                u4IfIndex = pTravCktRec->u4CktIfIdx;
		/*Enabling BFD only if the interface is not a loopback*/
                if (CfaIsLoopBackIntf (u4IfIndex) == FALSE)
                {
                pTravAdj = pTravCktRec->pAdjEntry;
                while (pTravAdj != NULL)
                {
                    if (i4SetValFsIsisExtSysBfdAllIfStatus == ISIS_BFD_DISABLE)
                    {
                        /* Deregister from BFD if registered previously */
                        ISIS_BFD_DEREGISTER(pContext, pTravAdj, pTravCktRec, ISIS_TRUE);
                    }
                    else
                    {
                        if ((pContext->u1IsisMTSupport == ISIS_FALSE) &&
                            (pTravAdj->u1AdjState == ISIS_ADJ_UP))
                        {
                            ISIS_BFD_REGISTER (pContext, pTravAdj, pTravCktRec);
                        }
                    }
                    pTravAdj = pTravAdj->pNext;
                }
                pTravCktRec->u1IsisBfdStatus =
                    (UINT1) i4SetValFsIsisExtSysBfdAllIfStatus;
                }
                pTravCktRec = pTravCktRec->pNext;
            }

            i1ErrCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysBfdAllIfStatus () \n"));

    return i1ErrCode;
#else
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4SetValFsIsisExtSysBfdAllIfStatus);
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysAuthSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysAuthSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysAuthSupp (UINT4 *pu4ErrorCode, INT4 i4FsIsisExtSysInstance,
                               INT4 i4TestValFsIsisExtSysAuthSupp)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysAuthSupp ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysAuthSupp () \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtSysAuthSupp < ISIS_AUTH_BOTH_DISABLED) ||
        (i4TestValFsIsisExtSysAuthSupp > ISIS_AUTH_BOTH_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysAuthSupp () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysAreaAuthType
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysAreaAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysAreaAuthType (UINT4 *pu4ErrorCode,
                                   INT4 i4FsIsisExtSysInstance,
                                   INT4 i4TestValFsIsisExtSysAreaAuthType)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysAreaAuthType ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysAreaAuthType () \n"));
        return i1ErrCode;
    }
    if ((i4TestValFsIsisExtSysAreaAuthType != ISIS_AUTH_PASSWD) &&
       (i4TestValFsIsisExtSysAreaAuthType != ISIS_AUTH_MD5))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysAreaAuthType () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysDomainAuthType
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysDomainAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysDomainAuthType (UINT4 *pu4ErrorCode,
                                     INT4 i4FsIsisExtSysInstance,
                                     INT4 i4TestValFsIsisExtSysDomainAuthType)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysDomainAuthType ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysDomainAuthType () \n"));
        return i1ErrCode;
    }
    if ((i4TestValFsIsisExtSysDomainAuthType != ISIS_AUTH_PASSWD) &&
        (i4TestValFsIsisExtSysDomainAuthType != ISIS_AUTH_MD5))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysDomainAuthType () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysAreaTxPasswd
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysAreaTxPasswd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysAreaTxPasswd (UINT4 *pu4ErrorCode,
                                   INT4 i4FsIsisExtSysInstance,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsIsisExtSysAreaTxPasswd)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysAreaTxPasswd ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysAreaTxPasswd () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }
    if ((pTestValFsIsisExtSysAreaTxPasswd->i4_Length <= 0) || 
	    (pTestValFsIsisExtSysAreaTxPasswd->i4_Length > ISIS_MAX_PASSWORD_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;    /*silvercreek */
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_LEN);
    }

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (pContext->SysActuals.SysAreaTxPasswd.au1Password[0] != '\0')
        {
            *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
            i1ErrCode = SNMP_FAILURE;
            CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysAreaTxPasswd () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysDomainTxPasswd
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysDomainTxPasswd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysDomainTxPasswd (UINT4 *pu4ErrorCode,
                                     INT4 i4FsIsisExtSysInstance,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsIsisExtSysDomainTxPasswd)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysDomainTxPasswd ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysDomainTxPasswd ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }

    if ((pTestValFsIsisExtSysDomainTxPasswd->i4_Length <= 0) ||	
	    (pTestValFsIsisExtSysDomainTxPasswd->i4_Length > ISIS_MAX_PASSWORD_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;    /*silvercreek */
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_LEN);
    }
    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if (pContext->SysActuals.SysDomainTxPasswd.au1Password[0] != '\0')
        {
            *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
            i1ErrCode = SNMP_FAILURE;
            CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysDomainTxPasswd () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysMinSPFSchTime
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysMinSPFSchTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysMinSPFSchTime (UINT4 *pu4ErrorCode,
                                    INT4 i4FsIsisExtSysInstance,
                                    INT4 i4TestValFsIsisExtSysMinSPFSchTime)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysMinSPFSchTime () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysMinSPFSchTime () \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtSysMinSPFSchTime < ISIS_LL_LOW_MIN_SPF_SCH_TIME)
        || (i4TestValFsIsisExtSysMinSPFSchTime > ISIS_LL_HIGH_MIN_SPF_SCH_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysMinSPFSchTime () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysMaxSPFSchTime
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysMaxSPFSchTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysMaxSPFSchTime (UINT4 *pu4ErrorCode,
                                    INT4 i4FsIsisExtSysInstance,
                                    INT4 i4TestValFsIsisExtSysMaxSPFSchTime)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysMaxSPFSchTime ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysMaxSPFSchTime () \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtSysMaxSPFSchTime < ISIS_LL_LOW_MAX_SPF_SCH_TIME)
        || (i4TestValFsIsisExtSysMaxSPFSchTime > ISIS_LL_HIGH_MAX_SPF_SCH_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysMaxSPFSchTime () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysMinLSPMark
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysMinLSPMark
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysMinLSPMark (UINT4 *pu4ErrorCode,
                                 INT4 i4FsIsisExtSysInstance,
                                 INT4 i4TestValFsIsisExtSysMinLSPMark)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysMinLSPMark ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysMinLSPMark () \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtSysMinLSPMark < ISIS_LL_LOW_MIN_LSP_CHGS)
        || (i4TestValFsIsisExtSysMinLSPMark > ISIS_LL_HIGH_MIN_LSP_CHGS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysMinLSPMark () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysMaxLSPMark
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysMaxLSPMark
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysMaxLSPMark (UINT4 *pu4ErrorCode,
                                 INT4 i4FsIsisExtSysInstance,
                                 INT4 i4TestValFsIsisExtSysMaxLSPMark)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysMaxLSPMark ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysMaxLSPMark () \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtSysMaxLSPMark < ISIS_LL_LOW_MAX_LSP_CHGS)
        || (i4TestValFsIsisExtSysMaxLSPMark > ISIS_LL_HIGH_MAX_LSP_CHGS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysMaxLSPMark () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysDelMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysDelMetSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysDelMetSupp (UINT4 *pu4ErrorCode,
                                 INT4 i4FsIsisExtSysInstance,
                                 INT4 i4TestValFsIsisExtSysDelMetSupp)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysDelMetSupp ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysDelMetSupp () \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtSysDelMetSupp != ISIS_TRUE)
        && (i4TestValFsIsisExtSysDelMetSupp != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysDelMetSupp () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysErrMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysErrMetSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysErrMetSupp (UINT4 *pu4ErrorCode,
                                 INT4 i4FsIsisExtSysInstance,
                                 INT4 i4TestValFsIsisExtSysErrMetSupp)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysErrMetSupp ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysErrMetSupp () \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtSysErrMetSupp != ISIS_TRUE)
        && (i4TestValFsIsisExtSysErrMetSupp != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysErrMetSupp () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysExpMetSupp
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysExpMetSupp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysExpMetSupp (UINT4 *pu4ErrorCode,
                                 INT4 i4FsIsisExtSysInstance,
                                 INT4 i4TestValFsIsisExtSysExpMetSupp)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysExpMetSupp ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysExpMetSupp () \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtSysExpMetSupp != ISIS_TRUE)
        && (i4TestValFsIsisExtSysExpMetSupp != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysExpMetSupp () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysRouterID
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysRouterID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysRouterID (UINT4 *pu4ErrorCode, INT4 i4FsIsisExtSysInstance,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsIsisExtSysRouterID)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysRouterID ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i1ErrCode = IsisNmhValSysTable (pu4ErrorCode, i4FsIsisExtSysInstance);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSysRouterID () \n"));
        return i1ErrCode;
    }

    if ((pTestValFsIsisExtSysRouterID->i4_Length != ISIS_MAX_IPV4_ADDR_LEN)
        && (pTestValFsIsisExtSysRouterID->i4_Length != ISIS_MAX_IPV6_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysRouterID () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtRestartSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtRestartSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtRestartSupport (UINT4 *pu4ErrorCode,
                                  INT4 i4FsIsisExtSysInstance,
                                  INT4 i4TestValFsIsisExtRestartSupport)
{

    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtRestartSupport ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhTestv2FsIsisExtRestartSupport ()  \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtRestartSupport < ISIS_GR_RESTART_NONE) ||
        (i4TestValFsIsisExtRestartSupport > ISIS_GR_RESTART_BOTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
        && (IssGetCsrRestoresFlag (ISIS_MODULE_ID) != ISIS_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtRestartSupport () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtGRRestartTimeInterval
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtGRRestartTimeInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtGRRestartTimeInterval (UINT4 *pu4ErrorCode,
                                         INT4 i4FsIsisExtSysInstance,
                                         INT4
                                         i4TestValFsIsisExtGRRestartTimeInterval)
{

    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtGRRestartTimeInterval ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhTestv2FsIsisExtGRRestartTimeInterval ()  \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtGRRestartTimeInterval < ISIS_GR_MIN_T3_INTERVAL) ||
        (i4TestValFsIsisExtGRRestartTimeInterval > ISIS_GR_MAX_T3_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
        && (IssGetCsrRestoresFlag (ISIS_MODULE_ID) != ISIS_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtGRRestartTimeInterval () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtGRT2TimeIntervalLevel1
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtGRT2TimeIntervalLevel1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtGRT2TimeIntervalLevel1 (UINT4 *pu4ErrorCode,
                                          INT4 i4FsIsisExtSysInstance,
                                          INT4
                                          i4TestValFsIsisExtGRT2TimeIntervalLevel1)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtGRT2TimeIntervalLevel1 ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhTestv2FsIsisExtGRT2TimeIntervalLevel1 ()  \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtGRT2TimeIntervalLevel1 < ISIS_GR_MIN_T2_INTERVAL) ||
        (i4TestValFsIsisExtGRT2TimeIntervalLevel1 > ISIS_GR_MAX_T2_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
        && (IssGetCsrRestoresFlag (ISIS_MODULE_ID) != ISIS_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtGRT2TimeIntervalLevel1 () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtGRT2TimeIntervalLevel2
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtGRT2TimeIntervalLevel2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtGRT2TimeIntervalLevel2 (UINT4 *pu4ErrorCode,
                                          INT4 i4FsIsisExtSysInstance,
                                          INT4
                                          i4TestValFsIsisExtGRT2TimeIntervalLevel2)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtGRT2TimeIntervalLevel2 ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhTestv2FsIsisExtGRT2TimeIntervalLevel2 ()  \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtGRT2TimeIntervalLevel2 < ISIS_GR_MIN_T2_INTERVAL) ||
        (i4TestValFsIsisExtGRT2TimeIntervalLevel2 > ISIS_GR_MAX_T2_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
        && (IssGetCsrRestoresFlag (ISIS_MODULE_ID) != ISIS_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtGRT2TimeIntervalLevel2 () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtGRT1TimeInterval
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtGRT1TimeInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtGRT1TimeInterval (UINT4 *pu4ErrorCode,
                                    INT4 i4FsIsisExtSysInstance,
                                    INT4 i4TestValFsIsisExtGRT1TimeInterval)
{

    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtGRT1TimeInterval ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhTestv2FsIsisExtGRT1TimeInterval ()  \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtGRT1TimeInterval < ISIS_GR_MIN_T1_INTERVAL) ||
        (i4TestValFsIsisExtGRT1TimeInterval > ISIS_GR_MAX_T1_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
        && (IssGetCsrRestoresFlag (ISIS_MODULE_ID) != ISIS_TRUE))

    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtGRT1TimeInterval () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtGRT1RetryCount
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtGRT1RetryCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtGRT1RetryCount (UINT4 *pu4ErrorCode,
                                  INT4 i4FsIsisExtSysInstance,
                                  INT4 i4TestValFsIsisExtGRT1RetryCount)
{

    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtGRT1RetryCount ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhTestv2FsIsisExtGRT1RetryCount ()  \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtGRT1RetryCount < ISIS_GR_MIN_RETRY_COUNT) ||
        (i4TestValFsIsisExtGRT1RetryCount > ISIS_GR_MAX_RETRY_COUNT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
        && (IssGetCsrRestoresFlag (ISIS_MODULE_ID) != ISIS_TRUE))
    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtGRT1RetryCount () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtRestartReason
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtRestartReason
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtRestartReason (UINT4 *pu4ErrorCode,
                                 INT4 i4FsIsisExtSysInstance,
                                 INT4 i4TestValFsIsisExtRestartReason)
{

    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtRestartReason ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhTestv2FsIsisExtRestartReason ()  \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtRestartReason < ISIS_GR_UNKNOWN) ||
        (i4TestValFsIsisExtRestartReason > ISIS_GR_SW_RED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
        && (IssGetCsrRestoresFlag (ISIS_MODULE_ID) != ISIS_TRUE))

    {
        /* Restart is in progress */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtRestartReason () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtHelperSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtHelperSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtHelperSupport (UINT4 *pu4ErrorCode,
                                 INT4 i4FsIsisExtSysInstance,
                                 INT4 i4TestValFsIsisExtHelperSupport)
{

    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtHelperSupport ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisExtHelperSupport"
                 " ()  \n"));
        return i1ErrCode;
    }

    if ((i4TestValFsIsisExtHelperSupport != ISIS_GR_HELPER_NONE) &&
        (i4TestValFsIsisExtHelperSupport != ISIS_GR_HELPER_BOTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtHelperSupport () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtHelperGraceTimeLimit
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtHelperGraceTimeLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtHelperGraceTimeLimit (UINT4 *pu4ErrorCode,
                                        INT4 i4FsIsisExtSysInstance,
                                        INT4
                                        i4TestValFsIsisExtHelperGraceTimeLimit)
{

    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtHelperGraceTimeLimit ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtHelperGraceTimeLimit"
                 " ()  \n"));
        return i1ErrCode;
    }
    if ((i4TestValFsIsisExtHelperGraceTimeLimit != ISIS_GR_DEF_HELPER_TIME) &&
        ((i4TestValFsIsisExtHelperGraceTimeLimit < ISIS_GR_MIN_HELPER_TIME) ||
         (i4TestValFsIsisExtHelperGraceTimeLimit > ISIS_GR_MAX_HELPER_TIME)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtHelperGraceTimeLimit () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysBfdSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object
                testValFsIsisExtSysBfdSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsIsisExtSysBfdSupport (UINT4 *pu4ErrorCode, 
                                 INT4 i4FsIsisExtSysInstance, 
                                 INT4 i4TestValFsIsisExtSysBfdSupport)
{
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisExtSysBfdSupport () \n"));

    if ((IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext))
        != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisExtSysBfdSupport != ISIS_TRUE) &&
        (i4TestValFsIsisExtSysBfdSupport != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2FsIsisExtSysBfdSupport () \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysBfdAllIfStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object
                testValFsIsisExtSysBfdAllIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsIsisExtSysBfdAllIfStatus (UINT4 *pu4ErrorCode, 
                                     INT4 i4FsIsisExtSysInstance, 
                                     INT4 i4TestValFsIsisExtSysBfdAllIfStatus)
{
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisExtSysBfdAllIfStatus () \n"));

    if ((IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext))
        != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisExtSysBfdAllIfStatus != ISIS_TRUE) &&
        (i4TestValFsIsisExtSysBfdAllIfStatus != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return SNMP_FAILURE;
    }

    /* BFD support for ISIS should be enabled to enable 
       BFD at interface level */
    if (pContext->u1IsisBfdSupport != ISIS_TRUE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : BFD support for M-ISIS is not "
                 "enabled for the system instance,  %d\n", 
                 i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISIS_BFD_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2FsIsisExtSysBfdAllIfStatus () \n"));

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIsisExtSysAreaRxPasswdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtSysAreaRxPasswdTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysAreaRxPasswd
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtSysAreaRxPasswdTable (INT4
                                                       i4FsIsisExtSysInstance,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pFsIsisExtSysAreaRxPasswd)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtSysAreaRxPasswdTable ()\n"));

    if ((i4FsIsisExtSysInstance < 0)
        || (pFsIsisExtSysAreaRxPasswd->i4_Length > ISIS_MAX_PASSWORD_LEN))
    {
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtSysAreaRxPasswdTable () \n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtSysAreaRxPasswdTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysAreaRxPasswd
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtSysAreaRxPasswdTable (INT4 *pi4FsIsisExtSysInstance,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsIsisExtSysAreaRxPasswd)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               au1OctetList[ISIS_MAX_PASSWORD_LEN];
    tSNMP_OCTET_STRING_TYPE RxPasswd;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtSysAreaRxPasswdTable () \n"));

    MEMSET (au1OctetList, 0x00, ISIS_MAX_PASSWORD_LEN);
    RxPasswd.pu1_OctetList = au1OctetList;
    RxPasswd.i4_Length = ISIS_MAX_PASSWORD_LEN;

    i1ErrCode =
        nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable (0,
                                                      pi4FsIsisExtSysInstance,
                                                      &RxPasswd,
                                                      pFsIsisExtSysAreaRxPasswd);
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtSysAreaRxPasswdTable ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtSysAreaRxPasswd
                nextFsIsisExtSysAreaRxPasswd
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable (INT4 i4FsIsisExtSysInstance,
                                              INT4 *pi4NextFsIsisExtSysInstance,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsIsisExtSysAreaRxPasswd,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextFsIsisExtSysAreaRxPasswd)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4SysInstance = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tSNMP_OCTET_STRING_TYPE *pAreaRxPasswd = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextAreaRxPasswd = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable ()\n"));

    pAreaRxPasswd = pFsIsisExtSysAreaRxPasswd;
    pNextAreaRxPasswd = pNextFsIsisExtSysAreaRxPasswd;

    if (IsisCtrlGetSysContext (u4SysInstance, &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance,
                                            &u4SysInstance) == ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                     "with the given Indices \n"));

            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting "
                     "nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable ()\n"));

            return (i1ErrCode);
        }
        else
        {
            MEMSET (pAreaRxPasswd->pu1_OctetList, 0, ISIS_MAX_PASSWORD_LEN);
            if (IsisCtrlGetSysContext (u4SysInstance, &pContext)
                 == ISIS_FAILURE)
            {
               NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                                    "the corresponding index \n"));
            }

        }

    }
    while (pContext != NULL)
    {
        if (nmhUtlGetNextAreaRxPasswd (pContext, pAreaRxPasswd,
                                       pNextAreaRxPasswd) == ISIS_FAILURE)
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance, &u4SysInstance)
                == ISIS_FAILURE)
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                         "with the given Indices \n"));
                NMP_EE ((ISIS_LGST,
                         "NMP <X> : Exiting "
                         "nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable ()\n"));

                return (i1ErrCode);
            }
            else
            {
                MEMSET (pAreaRxPasswd->pu1_OctetList, 0, ISIS_AREA_ADDR_LEN);
                if (IsisCtrlGetSysContext (u4SysInstance, &pContext)
                    == ISIS_FAILURE)
                {
                  NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                                      "the corresponding index \n"));
                }
                continue;
            }
        }
        else
        {
            *pi4NextFsIsisExtSysInstance = (INT4) u4SysInstance;
            MEMCPY (pNextFsIsisExtSysAreaRxPasswd->pu1_OctetList,
                    pNextAreaRxPasswd->pu1_OctetList,
                    pNextAreaRxPasswd->i4_Length);

            pNextFsIsisExtSysAreaRxPasswd->i4_Length =
                pNextAreaRxPasswd->i4_Length;

            i1ErrCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable ()\n"));

    return (i1ErrCode);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysAreaRxPasswdExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysAreaRxPasswd

                The Object 
                retValFsIsisExtSysAreaRxPasswdExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysAreaRxPasswdExistState (INT4 i4FsIsisExtSysInstance,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsIsisExtSysAreaRxPasswd,
                                          INT4
                                          *pi4RetValFsIsisExtSysAreaRxPasswdExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               u1PasswdCount = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysAreaRxPasswdExistState () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhGetFsIsisExtSysAreaRxPasswdExistState ()  \n"));
        return i1ErrCode;
    }

    for (u1PasswdCount = 0; u1PasswdCount < ISIS_MAX_RX_PASSWDS;
         u1PasswdCount++)
    {
        /*Klockwork */
        if (pFsIsisExtSysAreaRxPasswd->i4_Length <= ISIS_MAX_PASSWORD_LEN)
        {
            if ((pFsIsisExtSysAreaRxPasswd->i4_Length ==
                 pContext->SysActuals.aSysAreaRxPasswd[u1PasswdCount].u1Len)
                && (MEMCMP (pFsIsisExtSysAreaRxPasswd->pu1_OctetList,
                            pContext->SysActuals.
                            aSysAreaRxPasswd[u1PasswdCount].au1Password,
                            pFsIsisExtSysAreaRxPasswd->i4_Length) == 0))
            {
                *pi4RetValFsIsisExtSysAreaRxPasswdExistState =
                    pContext->SysActuals.aSysAreaRxPasswd[u1PasswdCount].
                    u1ExistState;
                i1ErrCode = SNMP_SUCCESS;
                break;
            }
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysAreaRxPasswdExistState ()  \n"));
    return i1ErrCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysAreaRxPasswdExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysAreaRxPasswd

                The Object 
                setValFsIsisExtSysAreaRxPasswdExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysAreaRxPasswdExistState (INT4 i4FsIsisExtSysInstance,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsIsisExtSysAreaRxPasswd,
                                          INT4
                                          i4SetValFsIsisExtSysAreaRxPasswdExistState)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1FreeIdx = 0;
    UINT1               u1PasswdCount = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysAreaRxPasswdExistState () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext)) !=
        ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhSetFsIsisExtSysAreaRxPasswdExistState ()  \n"));

        i1RetCode = SNMP_FAILURE;
        return i1RetCode;
    }

    switch (i4SetValFsIsisExtSysAreaRxPasswdExistState)
    {
        case ISIS_CR_GO:

            for (u1PasswdCount = 0; u1PasswdCount < ISIS_MAX_RX_PASSWDS;
                 u1PasswdCount++)
            {
                /*Klockwork */
                if (pFsIsisExtSysAreaRxPasswd->i4_Length <=
                    ISIS_MAX_PASSWORD_LEN)
                {

                    if ((pFsIsisExtSysAreaRxPasswd->i4_Length ==
                         pContext->SysActuals.aSysAreaRxPasswd[u1PasswdCount].
                         u1Len)
                        &&
                        (MEMCMP
                         (pFsIsisExtSysAreaRxPasswd->pu1_OctetList,
                          pContext->SysActuals.aSysAreaRxPasswd[u1PasswdCount].
                          au1Password,
                          pFsIsisExtSysAreaRxPasswd->i4_Length) == 0))
                    {
                        i1RetCode = SNMP_FAILURE;
                        break;
                    }
                    if (pContext->SysActuals.
                        aSysAreaRxPasswd[u1PasswdCount].u1ExistState !=
                        ISIS_ACTIVE)
                    {
                        u1FreeIdx = u1PasswdCount;
                        i1RetCode = SNMP_SUCCESS;
                        break;
                    }
                }
            }

            if (i1RetCode != SNMP_FAILURE)
            {
                pContext->SysActuals.aSysAreaRxPasswd[u1FreeIdx].u1ExistState
                    = ISIS_ACTIVE;
                MEMCPY (pContext->SysActuals.
                        aSysAreaRxPasswd[u1FreeIdx].au1Password,
                        pFsIsisExtSysAreaRxPasswd->pu1_OctetList,
                        pFsIsisExtSysAreaRxPasswd->i4_Length);
                pContext->SysActuals.aSysAreaRxPasswd[u1FreeIdx].u1Len =
                    (UINT1) pFsIsisExtSysAreaRxPasswd->i4_Length;

            }

            break;

        case ISIS_DESTROY:

            for (u1PasswdCount = 0; u1PasswdCount < ISIS_MAX_RX_PASSWDS;
                 u1PasswdCount++)
            {
                /*Klockwork */
                if (pFsIsisExtSysAreaRxPasswd->i4_Length <=
                    ISIS_MAX_PASSWORD_LEN)
                {

                    if ((pFsIsisExtSysAreaRxPasswd->i4_Length ==
                         pContext->SysActuals.aSysAreaRxPasswd[u1PasswdCount].
                         u1Len)
                        &&
                        (MEMCMP
                         (pFsIsisExtSysAreaRxPasswd->pu1_OctetList,
                          pContext->SysActuals.aSysAreaRxPasswd[u1PasswdCount].
                          au1Password,
                          pFsIsisExtSysAreaRxPasswd->i4_Length) == 0))
                    {
                        pContext->SysActuals.aSysAreaRxPasswd[u1PasswdCount].
                            u1ExistState = ISIS_DESTROY;
                        MEMSET (pContext->SysActuals.
                                aSysAreaRxPasswd[u1PasswdCount].au1Password,
                                0x00, ISIS_MAX_PASSWORD_LEN);
                        pContext->SysActuals.
                            aSysAreaRxPasswd[u1PasswdCount].u1Len = 0;
                        i1RetCode = SNMP_SUCCESS;

                        break;
                    }
                }
            }
            break;

        default:

            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Exist State!! \n"));
            i1RetCode = SNMP_FAILURE;
            break;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysAreaRxPasswdExistState ()  \n"));

    return i1RetCode;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysAreaRxPasswdExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysAreaRxPasswd

                The Object 
                testValFsIsisExtSysAreaRxPasswdExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysAreaRxPasswdExistState (UINT4 *pu4ErrorCode,
                                             INT4 i4FsIsisExtSysInstance,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsIsisExtSysAreaRxPasswd,
                                             INT4
                                             i4TestValFsIsisExtSysAreaRxPasswdExistState)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysAreaRxPasswdExistState () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (pFsIsisExtSysAreaRxPasswd);

    if ((i4TestValFsIsisExtSysAreaRxPasswdExistState != ISIS_CR_GO)
        && (i4TestValFsIsisExtSysAreaRxPasswdExistState != ISIS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysAreaRxPasswdExistState ()  \n"));

    return i1RetCode;

}

/* LOW LEVEL Routines for Table : FsIsisExtSysDomainRxPasswdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtSysDomainRxPasswdTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysDomainRxPassword
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtSysDomainRxPasswdTable (INT4
                                                         i4FsIsisExtSysInstance,
                                                         tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFsIsisExtSysDomainRxPassword)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtSysDomainRxPasswdTable ()\n"));

    if ((i4FsIsisExtSysInstance < 0)
        || (pFsIsisExtSysDomainRxPassword->i4_Length > ISIS_MAX_PASSWORD_LEN))
    {
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtSysDomainRxPasswdTable () \n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtSysDomainRxPasswdTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysDomainRxPassword
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtSysDomainRxPasswdTable (INT4 *pi4FsIsisExtSysInstance,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsIsisExtSysDomainRxPassword)
{
    INT1                i1RetCode = SNMP_FAILURE;
    UINT1               au1OctetList[ISIS_MAX_PASSWORD_LEN];
    tSNMP_OCTET_STRING_TYPE RxPasswd;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtSysDomainRxPasswdTable () \n"));

    MEMSET (au1OctetList, 0x00, ISIS_MAX_PASSWORD_LEN);
    RxPasswd.pu1_OctetList = au1OctetList;
    RxPasswd.i4_Length = ISIS_MAX_PASSWORD_LEN;

    i1RetCode =
        nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable (0,
                                                        pi4FsIsisExtSysInstance,
                                                        &RxPasswd,
                                                        pFsIsisExtSysDomainRxPassword);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtSysDomainRxPasswdTable ()  \n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtSysDomainRxPassword
                nextFsIsisExtSysDomainRxPassword
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable (INT4 i4FsIsisExtSysInstance,
                                                INT4
                                                *pi4NextFsIsisExtSysInstance,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsIsisExtSysDomainRxPassword,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextFsIsisExtSysDomainRxPassword)
{
    INT1                i1RetCode = SNMP_FAILURE;
    UINT4               u4SysInstance = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tSNMP_OCTET_STRING_TYPE *pDomainRxPasswd = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextDomainRxPasswd = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable ()\n"));

    pDomainRxPasswd = pFsIsisExtSysDomainRxPassword;
    pNextDomainRxPasswd = pNextFsIsisExtSysDomainRxPassword;

    if (IsisCtrlGetSysContext (u4SysInstance, &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance,
                                            &u4SysInstance) == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                     "with the given Indices \n"));

            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting "
                     "nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable ()\n"));

            return (i1RetCode);
        }
        else
        {
            if (IsisCtrlGetSysContext
                (u4SysInstance, &pContext) == ISIS_FAILURE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                         "with the given Indices \n"));
                NMP_EE ((ISIS_LGST,
                         "NMP <X> : Exiting "
                         "nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable ()\n"));
                return (i1RetCode);
            }
            MEMSET (pDomainRxPasswd->pu1_OctetList, 0, ISIS_MAX_PASSWORD_LEN);
        }

    }
    while (pContext != NULL)
    {
        if (nmhUtlGetNextDomainRxPasswd
            (pContext, pDomainRxPasswd, pNextDomainRxPasswd) == ISIS_FAILURE)
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance, &u4SysInstance)
                == ISIS_FAILURE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                         "with the given Indices \n"));

                NMP_EE ((ISIS_LGST,
                         "NMP <X> : Exiting "
                         "nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable "
                         "()\n"));

                return (i1RetCode);
            }
            else
            {
                if (IsisCtrlGetSysContext
                    (u4SysInstance, &pContext) == ISIS_FAILURE)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Could Not Find the Next Index "
                             "with the given Indices \n"));
                    NMP_EE ((ISIS_LGST,
                             "NMP <X> : Exiting "
                             "nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable "
                             "()\n"));
                    return (i1RetCode);
                }
                MEMSET (pDomainRxPasswd->pu1_OctetList, 0,
                        ISIS_MAX_PASSWORD_LEN);
                continue;
            }
        }
        else
        {
            *pi4NextFsIsisExtSysInstance = (INT4) u4SysInstance;
            MEMCPY (pNextFsIsisExtSysDomainRxPassword->pu1_OctetList,
                    pNextDomainRxPasswd->pu1_OctetList, ISIS_MAX_PASSWORD_LEN);

            pNextFsIsisExtSysDomainRxPassword->i4_Length =
                pNextDomainRxPasswd->i4_Length;
            i1RetCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable ()\n"));
    return (i1RetCode);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysDomainRxPasswdExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysDomainRxPassword

                The Object 
                retValFsIsisExtSysDomainRxPasswdExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysDomainRxPasswdExistState (INT4 i4FsIsisExtSysInstance,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsIsisExtSysDomainRxPassword,
                                            INT4
                                            *pi4RetValFsIsisExtSysDomainRxPasswdExistState)
{
    INT1                i1RetCode = SNMP_FAILURE;
    UINT1               u1PasswdCount = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSysDomainRxPasswdExistState () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));
        i1RetCode = SNMP_FAILURE;
    }
    else
    {
        for (u1PasswdCount = 0; u1PasswdCount < ISIS_MAX_RX_PASSWDS;
             u1PasswdCount++)
        {

            if (pFsIsisExtSysDomainRxPassword->i4_Length <=
                ISIS_MAX_PASSWORD_LEN)
            {
                if ((pFsIsisExtSysDomainRxPassword->i4_Length ==
                     pContext->SysActuals.aSysDomainRxPasswd[u1PasswdCount].
                     u1Len)
                    &&
                    (MEMCMP
                     (pFsIsisExtSysDomainRxPassword->pu1_OctetList,
                      pContext->SysActuals.aSysDomainRxPasswd[u1PasswdCount].
                      au1Password,
                      pFsIsisExtSysDomainRxPassword->i4_Length) == 0))
                {
                    *pi4RetValFsIsisExtSysDomainRxPasswdExistState =
                        pContext->SysActuals.
                        aSysDomainRxPasswd[u1PasswdCount].u1ExistState;

                    i1RetCode = SNMP_SUCCESS;
                    break;
                }
            }
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtSysDomainRxPasswdExistState ()  \n"));
    return (i1RetCode);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysDomainRxPasswdExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysDomainRxPassword

                The Object 
                setValFsIsisExtSysDomainRxPasswdExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysDomainRxPasswdExistState (INT4 i4FsIsisExtSysInstance,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsIsisExtSysDomainRxPassword,
                                            INT4
                                            i4SetValFsIsisExtSysDomainRxPasswdExistState)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1PasswdCount = 0;
    UINT1               u1FreeIdx = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSysDomainRxPasswdExistState () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext)) !=
        ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhSetFsIsisExtSysDomainRxPasswdExistState ()  \n"));
        i1RetCode = SNMP_FAILURE;
        return i1RetCode;
    }

    switch (i4SetValFsIsisExtSysDomainRxPasswdExistState)
    {
        case ISIS_CR_GO:

            for (u1PasswdCount = 0; u1PasswdCount < ISIS_MAX_RX_PASSWDS;
                 u1PasswdCount++)
            {                    /*klocwork */
                if (pFsIsisExtSysDomainRxPassword->i4_Length <=
                    ISIS_MAX_PASSWORD_LEN)
                {
                    if ((pFsIsisExtSysDomainRxPassword->i4_Length ==
                         pContext->SysActuals.aSysDomainRxPasswd[u1PasswdCount].
                         u1Len)
                        &&
                        (MEMCMP
                         (pFsIsisExtSysDomainRxPassword->pu1_OctetList,
                          pContext->SysActuals.
                          aSysDomainRxPasswd[u1PasswdCount].au1Password,
                          pFsIsisExtSysDomainRxPassword->i4_Length) == 0))
                    {
                        i1RetCode = SNMP_FAILURE;
                        break;
                    }
                    if (pContext->SysActuals.
                        aSysDomainRxPasswd[u1PasswdCount].u1ExistState !=
                        ISIS_ACTIVE)
                    {
                        u1FreeIdx = u1PasswdCount;
                        i1RetCode = SNMP_SUCCESS;
                        break;
                    }
                }
            }

            if (i1RetCode != SNMP_FAILURE)
            {
                pContext->SysActuals.aSysDomainRxPasswd[u1FreeIdx].u1ExistState
                    = ISIS_ACTIVE;
                MEMCPY (pContext->SysActuals.
                        aSysDomainRxPasswd[u1FreeIdx].au1Password,
                        pFsIsisExtSysDomainRxPassword->pu1_OctetList,
                        pFsIsisExtSysDomainRxPassword->i4_Length);
                pContext->SysActuals.aSysDomainRxPasswd[u1FreeIdx].u1Len
                    = (UINT1) pFsIsisExtSysDomainRxPassword->i4_Length;
            }
            break;

        case ISIS_DESTROY:

            for (u1PasswdCount = 0; u1PasswdCount < ISIS_MAX_RX_PASSWDS;
                 u1PasswdCount++)
            {                    /*Klocwork */
                if (pFsIsisExtSysDomainRxPassword->i4_Length <=
                    ISIS_MAX_PASSWORD_LEN)
                {
                    if ((pFsIsisExtSysDomainRxPassword->i4_Length ==
                         pContext->SysActuals.aSysDomainRxPasswd[u1PasswdCount].
                         u1Len)
                        &&
                        (MEMCMP
                         (pFsIsisExtSysDomainRxPassword->pu1_OctetList,
                          pContext->SysActuals.
                          aSysDomainRxPasswd[u1PasswdCount].au1Password,
                          pFsIsisExtSysDomainRxPassword->i4_Length) == 0))
                    {
                        pContext->SysActuals.aSysDomainRxPasswd[u1PasswdCount].
                            u1ExistState = ISIS_DESTROY;
                        MEMSET (pContext->SysActuals.
                                aSysDomainRxPasswd[u1PasswdCount].au1Password,
                                0x00, ISIS_MAX_PASSWORD_LEN);
                        pContext->SysActuals.
                            aSysDomainRxPasswd[u1PasswdCount].u1Len = 0;
                        i1RetCode = SNMP_SUCCESS;
                        break;
                    }
                }
            }

            break;

        default:
            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Exist State!! \n"));
            i1RetCode = SNMP_FAILURE;
            break;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtSysDomainRxPasswdExistState ()  \n"));
    return i1RetCode;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysDomainRxPasswdExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysDomainRxPassword

                The Object 
                testValFsIsisExtSysDomainRxPasswdExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysDomainRxPasswdExistState (UINT4 *pu4ErrorCode,
                                               INT4 i4FsIsisExtSysInstance,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsIsisExtSysDomainRxPassword,
                                               INT4
                                               i4TestValFsIsisExtSysDomainRxPasswdExistState)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtSysDomainRxPasswdExistState () \n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (pFsIsisExtSysDomainRxPassword);

    if ((i4TestValFsIsisExtSysDomainRxPasswdExistState != ISIS_CR_GO)
        && (i4TestValFsIsisExtSysDomainRxPasswdExistState != ISIS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtSysDomainRxPasswdExistState ()  \n"));
    return i1RetCode;

}

/* LOW LEVEL Routines for Table : FsIsisExtSummAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtSummAddrTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtSummAddrTable (INT4 i4FsIsisExtSysInstance,
                                                INT4 i4FsIsisExtSummAddressType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsIsisExtSummAddress,
                                                UINT4
                                                u4FsIsisExtSummAddrPrefixLen)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtSummAddrTable () \n"));

    i1RetCode =
        nmhValidateIndexInstanceIsisSummAddrTable (i4FsIsisExtSysInstance,
                                                   i4FsIsisExtSummAddressType,
                                                   pFsIsisExtSummAddress,
                                                   u4FsIsisExtSummAddrPrefixLen);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtSummAddrTable ()  \n"));
    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtSummAddrTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtSummAddrTable (INT4 *pi4FsIsisExtSysInstance,
                                        INT4 *pi4FsIsisExtSummAddressType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsIsisExtSummAddress,
                                        UINT4 *pu4FsIsisExtSummAddrPrefixLen)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtSummAddrTable () \n"));

    i1ErrCode =
        nmhGetFirstIndexIsisSummAddrTable (pi4FsIsisExtSysInstance,
                                           pi4FsIsisExtSummAddressType,
                                           pFsIsisExtSummAddress,
                                           pu4FsIsisExtSummAddrPrefixLen);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSummAddrTable ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtSummAddrTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtSummAddressType
                nextFsIsisExtSummAddressType
                FsIsisExtSummAddress
                nextFsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen
                nextFsIsisExtSummAddrPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtSummAddrTable (INT4 i4FsIsisExtSysInstance,
                                       INT4 *pi4NextFsIsisExtSysInstance,
                                       INT4 i4FsIsisExtSummAddressType,
                                       INT4 *pi4NextFsIsisExtSummAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsIsisExtSummAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsIsisExtSummAddress,
                                       UINT4 u4FsIsisExtSummAddrPrefixLen,
                                       UINT4 *pu4NextFsIsisExtSummAddrPrefixLen)
{
    INT1                i1RetCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexFsIsisExtSummAddrTable ()  \n"));

    i1RetCode = nmhGetNextIndexIsisSummAddrTable (i4FsIsisExtSysInstance,
                                                  pi4NextFsIsisExtSysInstance,
                                                  i4FsIsisExtSummAddressType,
                                                  pi4NextFsIsisExtSummAddressType,
                                                  pFsIsisExtSummAddress,
                                                  pNextFsIsisExtSummAddress,
                                                  u4FsIsisExtSummAddrPrefixLen,
                                                  pu4NextFsIsisExtSummAddrPrefixLen);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSummAddrTable ()  \n"));

    return i1RetCode;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtSummAddrDelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                retValFsIsisExtSummAddrDelayMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSummAddrDelayMetric (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtSummAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsIsisExtSummAddress,
                                    UINT4 u4FsIsisExtSummAddrPrefixLen,
                                    INT4 *pi4RetValFsIsisExtSummAddrDelayMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               u1PrefixLen = (UINT1) u4FsIsisExtSummAddrPrefixLen;
    UINT1               u1MetricIdx = 0;
    UINT1               u1SAType = (UINT1) i4FsIsisExtSummAddressType;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSummAddrDelayMetric () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Instance does not exist !!! \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSummAddrDelayMetric ()\n"));
        return i1ErrCode;
    }

    u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);

    if ((IsisCtrlGetSA (pContext, pFsIsisExtSummAddress->pu1_OctetList,
                        u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
    {
        if ((pContext->SysActuals.u1SysMetricSupp & ISIS_DEL_MET_SUPP_FLAG))
        {
            u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);
            *pi4RetValFsIsisExtSummAddrDelayMetric =
                (INT4) (pSAEntry->Metric[u1MetricIdx]);
        }
        else
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Delay Metric is not supported"
                     "for the Current Configuration!!!\n"));
        }
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index !!!\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSummAddrDelayMetric ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSummAddrErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                retValFsIsisExtSummAddrErrorMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSummAddrErrorMetric (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtSummAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsIsisExtSummAddress,
                                    UINT4 u4FsIsisExtSummAddrPrefixLen,
                                    INT4 *pi4RetValFsIsisExtSummAddrErrorMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               u1SAType = (UINT1) i4FsIsisExtSummAddressType;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT1               u1PrefixLen = (UINT1) u4FsIsisExtSummAddrPrefixLen;
    UINT1               u1MetricIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSummAddrErrorMetric () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Instance does not exist !!! \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSummAddrErrorMetric ()\n"));

        return i1ErrCode;
    }

    if ((IsisCtrlGetSA (pContext, pFsIsisExtSummAddress->pu1_OctetList,
                        u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
    {
        if ((pContext->SysActuals.u1SysMetricSupp & ISIS_ERR_MET_SUPP_FLAG))
        {
            u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);
            *pi4RetValFsIsisExtSummAddrErrorMetric =
                (INT4) (pSAEntry->Metric[u1MetricIdx]);
        }
        else
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Error Metric is not supported"
                     "for the Current Configuration!!!\n"));
        }
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index !!!\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSummAddrErrorMetric ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSummAddrExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                retValFsIsisExtSummAddrExpenseMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSummAddrExpenseMetric (INT4 i4FsIsisExtSysInstance,
                                      INT4 i4FsIsisExtSummAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsIsisExtSummAddress,
                                      UINT4 u4FsIsisExtSummAddrPrefixLen,
                                      INT4
                                      *pi4RetValFsIsisExtSummAddrExpenseMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT1               u1PrefixLen = (UINT1) u4FsIsisExtSummAddrPrefixLen;
    UINT1               u1MetricIdx = 0;
    UINT1               u1SAType = (UINT1) i4FsIsisExtSummAddressType;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSummAddrExpenseMetric () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Instance does not exist !!! \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSummAddrExpenseMetric ()\n"));

        return i1ErrCode;
    }

    if ((IsisCtrlGetSA (pContext, pFsIsisExtSummAddress->pu1_OctetList,
                        u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
    {
        if ((pContext->SysActuals.u1SysMetricSupp & ISIS_EXP_MET_SUPP_FLAG))
        {
            u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);
            *pi4RetValFsIsisExtSummAddrExpenseMetric =
                (INT4) (pSAEntry->Metric[u1MetricIdx]);
        }
        else
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Expense Metric is not supported"
                     "for the Current Configuration!!!\n"));
        }
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index !!!\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSummAddrExpenseMetric ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSummAddrFullMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object
                retValFsIsisExtSummAddrFullMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisExtSummAddrFullMetric (INT4 i4FsIsisExtSysInstance, 
                                        INT4 i4FsIsisExtSummAddressType, 
                                        tSNMP_OCTET_STRING_TYPE 
                                                  *pFsIsisExtSummAddress, 
                                        UINT4 u4FsIsisExtSummAddrPrefixLen, 
                                        UINT4 
                                        *pu4RetValFsIsisExtSummAddrFullMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               u1SAType = (UINT1) i4FsIsisExtSummAddressType;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT1               u1PrefixLen = (UINT1) u4FsIsisExtSummAddrPrefixLen;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtSummAddrFullMetric () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Instance does not exist !!! \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSummAddrFullMetric ()\n"));

        return i1ErrCode;
    }

    if ((IsisCtrlGetSA (pContext, pFsIsisExtSummAddress->pu1_OctetList,
                        u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
    {
        if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
        {
            *pu4RetValFsIsisExtSummAddrFullMetric = (pSAEntry->u4FullMetric);
        }
        else
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Full Metric is not supported"
                     "for the Current Configuration!!!\n"));
			 *pu4RetValFsIsisExtSummAddrFullMetric = 0;
        }
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index !!!\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSummAddrFullMetric ()\n"));

    return i1ErrCode;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtSummAddrDelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                setValFsIsisExtSummAddrDelayMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSummAddrDelayMetric (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtSummAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsIsisExtSummAddress,
                                    UINT4 u4FsIsisExtSummAddrPrefixLen,
                                    INT4 i4SetValFsIsisExtSummAddrDelayMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               u1SAType = (UINT1) i4FsIsisExtSummAddressType;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT1               u1PrefixLen = (UINT1) u4FsIsisExtSummAddrPrefixLen;
    UINT1               u1MetricIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisEvtSummAddrChg *pSAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSummAddrDelayMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Instance does not exist !!! \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhSetIsisSummAddrDelayMetric ()\n"));
        return i1ErrCode;
    }

    if ((IsisCtrlGetSA (pContext, pFsIsisExtSummAddress->pu1_OctetList,
                        u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
    {
        u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);

        pSAEntry->Metric[u1MetricIdx] =
            (UINT1) i4SetValFsIsisExtSummAddrDelayMetric;

        if (((pContext->u1OperState == ISIS_UP)
             && (pSAEntry->u1AdminState != ISIS_SUMM_ADMIN_OFF))
            && (pSAEntry->u1ExistState == ISIS_ACTIVE))
        {
            pSAEvt = (tIsisEvtSummAddrChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSummAddrChg));
            if (pSAEvt != NULL)
            {
                pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                pSAEvt->u1Status = ISIS_SUMM_ADDR_ADD;
                pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                pSAEvt->u1CurrAdminState = pSAEntry->u1AdminState;
                MEMCPY (pSAEvt->Metric, pSAEntry->Metric,
                        (sizeof (tIsisMetric)));
                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV4_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                }
                else
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV6_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                }
                IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                  sizeof (tIsisEvtSummAddrChg));
            }
        }
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index !!!\n"));
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSummAddrDelayMetric ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSummAddrErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                setValFsIsisExtSummAddrErrorMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSummAddrErrorMetric (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtSummAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsIsisExtSummAddress,
                                    UINT4 u4FsIsisExtSummAddrPrefixLen,
                                    INT4 i4SetValFsIsisExtSummAddrErrorMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               u1SAType = (UINT1) i4FsIsisExtSummAddressType;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT1               u1PrefixLen = (UINT1) u4FsIsisExtSummAddrPrefixLen;
    UINT1               u1MetricIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisEvtSummAddrChg *pSAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSummAddrErrorMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Instance does not exist !!! \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhSetIsisSummAddrErrorMetric ()\n"));
        return i1ErrCode;
    }

    if ((IsisCtrlGetSA (pContext, pFsIsisExtSummAddress->pu1_OctetList,
                        u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
    {
        u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);
        pSAEntry->Metric[u1MetricIdx] =
            (UINT1) i4SetValFsIsisExtSummAddrErrorMetric;

        if (((pContext->u1OperState == ISIS_UP)
             && (pSAEntry->u1AdminState != ISIS_SUMM_ADMIN_OFF))
            && (pSAEntry->u1ExistState == ISIS_ACTIVE))
        {
            pSAEvt = (tIsisEvtSummAddrChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSummAddrChg));
            if (pSAEvt != NULL)
            {
                pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                pSAEvt->u1Status = ISIS_SUMM_ADDR_ADD;
                pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                pSAEvt->u1CurrAdminState = pSAEntry->u1AdminState;
                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV4_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                }
                else
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV6_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                }
                MEMCPY (pSAEvt->Metric, pSAEntry->Metric,
                        (sizeof (tIsisMetric)));
                IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                  sizeof (tIsisEvtSummAddrChg));
            }
        }

        i1ErrCode = SNMP_SUCCESS;
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index !!!\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSummAddrErrorMetric ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSummAddrExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                setValFsIsisExtSummAddrExpenseMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSummAddrExpenseMetric (INT4 i4FsIsisExtSysInstance,
                                      INT4 i4FsIsisExtSummAddressType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsIsisExtSummAddress,
                                      UINT4 u4FsIsisExtSummAddrPrefixLen,
                                      INT4
                                      i4SetValFsIsisExtSummAddrExpenseMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               u1SAType = (UINT1) i4FsIsisExtSummAddressType;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT1               u1PrefixLen = (UINT1) u4FsIsisExtSummAddrPrefixLen;
    UINT1               u1MetricIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisEvtSummAddrChg *pSAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSummAddrExpenseMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Instance does not exist !!! \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhSetIsisSummAddrExpenseMetric ()\n"));
        return i1ErrCode;
    }

    if ((IsisCtrlGetSA (pContext, pFsIsisExtSummAddress->pu1_OctetList,
                        u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
    {
        u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);

        pSAEntry->Metric[u1MetricIdx] =
            (UINT1) i4SetValFsIsisExtSummAddrExpenseMetric;

        if (((pContext->u1OperState == ISIS_UP)
             && (pSAEntry->u1AdminState != ISIS_SUMM_ADMIN_OFF))
            && (pSAEntry->u1ExistState == ISIS_ACTIVE))
        {
            pSAEvt = (tIsisEvtSummAddrChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSummAddrChg));
            if (pSAEvt != NULL)
            {
                pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                pSAEvt->u1Status = ISIS_SUMM_ADDR_ADD;
                pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                pSAEvt->u1CurrAdminState = pSAEntry->u1AdminState;
                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV4_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                }
                else
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV6_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                }
                MEMCPY (pSAEvt->Metric, pSAEntry->Metric,
                        (sizeof (tIsisMetric)));
                IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                  sizeof (tIsisEvtSummAddrChg));
            }
        }
        i1ErrCode = SNMP_SUCCESS;
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index !!!\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSummAddrExpenseMetric ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSummAddrFullMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object
                setValFsIsisExtSummAddrFullMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT1 nmhSetFsIsisExtSummAddrFullMetric (INT4 i4FsIsisExtSysInstance, 
                                        INT4 i4FsIsisExtSummAddressType,
                                        tSNMP_OCTET_STRING_TYPE 
                                                 *pFsIsisExtSummAddress, 
                                        UINT4 u4FsIsisExtSummAddrPrefixLen, 
                                        UINT4 u4SetValFsIsisExtSummAddrFullMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               u1SAType = (UINT1) i4FsIsisExtSummAddressType;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT1               u1PrefixLen = (UINT1) u4FsIsisExtSummAddrPrefixLen;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisEvtSummAddrChg *pSAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtSummAddrFullMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : Instance does not exist !!! \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhSetIsisSummAddrFullMetric ()\n"));
        return i1ErrCode;
    }

    if ((IsisCtrlGetSA (pContext, pFsIsisExtSummAddress->pu1_OctetList,
                        u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
    {
        pSAEntry->u4FullMetric = u4SetValFsIsisExtSummAddrFullMetric;

        if (((pContext->u1OperState == ISIS_UP)
             && (pSAEntry->u1AdminState != ISIS_SUMM_ADMIN_OFF))
             && (pSAEntry->u1ExistState == ISIS_ACTIVE))
        {
            pSAEvt = (tIsisEvtSummAddrChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSummAddrChg));
            if (pSAEvt != NULL)
            {
                pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                pSAEvt->u1Status = ISIS_SUMM_ADDR_METRIC_CHG;
                pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                pSAEvt->u1CurrAdminState = pSAEntry->u1AdminState;
                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV4_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                }
                else
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV6_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                }
                pSAEvt->u4FullMetric = pSAEntry->u4FullMetric;
                IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                  sizeof (tIsisEvtSummAddrChg));
            }
        }
        i1ErrCode = SNMP_SUCCESS;
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index !!!\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSummAddrFullMetric ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 *  Function    :  IsisCheckforMetricSupport
 *  
 *  Description :  This funciton is to check the perticular mertic (Delay,Expense,Error)
 *                 Supports or not for an existing instance. 
 *  
 *  Input       :  i4FsIsisExtSysInstance - instance id 
 *                 u1MetricSupport - the type of metric which need to check whether 
 *                                   Supports or not for the instance
 *  
 *  Output      :  pu4ErrorCode - Not support means fills the error code.
 *
 *  Returns     :  SNMP_SUCCUSS or SNMP_FAILURE
 *   
 ****************************************************************************/
PRIVATE INT1
IsisCheckforMetricSupport (UINT4 i4FsIsisExtSysInstance, UINT4 *pu4ErrorCode,
                           UINT4 u1MetricSupport)
{
    tIsisSysContext    *pContext = NULL;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext) ==
        ISIS_SUCCESS)
    {
        if (!(pContext->SysActuals.u1SysMetricSupp & u1MetricSupport))
        {
            if (u1MetricSupport == ISIS_DEL_MET_SUPP_FLAG)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Delay Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            else if (u1MetricSupport == ISIS_EXP_MET_SUPP_FLAG)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Expense Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            else if (u1MetricSupport == ISIS_ERR_MET_SUPP_FLAG)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Error Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSummAddrDelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                testValFsIsisExtSummAddrDelayMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSummAddrDelayMetric (UINT4 *pu4ErrorCode,
                                       INT4 i4FsIsisExtSysInstance,
                                       INT4 i4FsIsisExtSummAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsIsisExtSummAddress,
                                       UINT4 u4FsIsisExtSummAddrPrefixLen,
                                       INT4
                                       i4TestValFsIsisExtSummAddrDelayMetric)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSummAddrDelayMetric ()\n"));

    i1RetCode =
        IsisNmhValSATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                           i4FsIsisExtSummAddressType, pFsIsisExtSummAddress,
                           (INT4)u4FsIsisExtSummAddrPrefixLen, ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSummAddrDelayMetric ()\n"));
        return i1RetCode;
    }

    /* Delay metric is not supported for Multi-topology ISIS. Hence
     * verifying if MT support is enabled */
    if ((IsisCtrlGetSysContext ((UINT4)i4FsIsisExtSysInstance, &pContext) ==
        ISIS_SUCCESS) && (pContext->u1IsisMTSupport == ISIS_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Delay metric not supported "
                "for multi-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_MET_NOT_SUPP_MT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisExtSummAddrDelayMetric < ISIS_LL_MIN_METRIC) ||
        (i4TestValFsIsisExtSummAddrDelayMetric > ISIS_LL_CKTL_MAX_METRIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "i4TestValIsisSummAddrDelayMetric\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
        return i1RetCode;
    }
    i1RetCode =
        IsisCheckforMetricSupport ((UINT4) i4FsIsisExtSysInstance, pu4ErrorCode,
                                   ISIS_DEL_MET_SUPP_FLAG);
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting " "nmhTestv2IsisSummAddrDelayMetric ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSummAddrErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                testValFsIsisExtSummAddrErrorMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSummAddrErrorMetric (UINT4 *pu4ErrorCode,
                                       INT4 i4FsIsisExtSysInstance,
                                       INT4 i4FsIsisExtSummAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsIsisExtSummAddress,
                                       UINT4 u4FsIsisExtSummAddrPrefixLen,
                                       INT4
                                       i4TestValFsIsisExtSummAddrErrorMetric)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSummAddrErrorMetric ()\n"));

    i1RetCode =
        IsisNmhValSATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                           i4FsIsisExtSummAddressType, pFsIsisExtSummAddress,
                           (INT4)u4FsIsisExtSummAddrPrefixLen, ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSummAddrErrorMetric ()\n"));
        return i1RetCode;
    }

    /* Error metric is not supported for Multi-topology ISIS. Hence
     * verifying if MT support is enabled */
    if ((IsisCtrlGetSysContext ((UINT4)i4FsIsisExtSysInstance, &pContext) ==
        ISIS_SUCCESS) && (pContext->u1IsisMTSupport == ISIS_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Error metric not supported "
                "for multi-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_MET_NOT_SUPP_MT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisExtSummAddrErrorMetric < ISIS_LL_MIN_METRIC) ||
        (i4TestValFsIsisExtSummAddrErrorMetric > ISIS_LL_CKTL_MAX_METRIC))

    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "i4TestValIsisSummAddrErrorMetric\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
        return i1RetCode;
    }
    i1RetCode =
        IsisCheckforMetricSupport ((UINT4)i4FsIsisExtSysInstance, pu4ErrorCode,
                                   ISIS_ERR_MET_SUPP_FLAG);
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting " "nmhTestv2IsisSummAddrErrorMetric ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSummAddrExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object 
                testValFsIsisExtSummAddrExpenseMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSummAddrExpenseMetric (UINT4 *pu4ErrorCode,
                                         INT4 i4FsIsisExtSysInstance,
                                         INT4 i4FsIsisExtSummAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsIsisExtSummAddress,
                                         UINT4 u4FsIsisExtSummAddrPrefixLen,
                                         INT4
                                         i4TestValFsIsisExtSummAddrExpenseMetric)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSummAddrExpenseMetric ()\n"));

    i1RetCode =
        IsisNmhValSATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                           i4FsIsisExtSummAddressType, pFsIsisExtSummAddress,
                           (INT4)u4FsIsisExtSummAddrPrefixLen, ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSummAddrExpenseMetric ()\n"));
        return i1RetCode;
    }

    /* Expense metric is not supported for Multi-topology ISIS. Hence
     * verifying if MT support is enabled */
    if ((IsisCtrlGetSysContext ((UINT4)i4FsIsisExtSysInstance, &pContext) ==
        ISIS_SUCCESS) && (pContext->u1IsisMTSupport == ISIS_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Expense metric not supported "
                "for multi-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_MET_NOT_SUPP_MT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisExtSummAddrExpenseMetric < ISIS_LL_MIN_METRIC) ||
        (i4TestValFsIsisExtSummAddrExpenseMetric > ISIS_LL_CKTL_MAX_METRIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "i4TestValIsisSummAddrExpenseMetric\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
        return i1RetCode;
    }
    i1RetCode =
        IsisCheckforMetricSupport ((UINT4)i4FsIsisExtSysInstance, pu4ErrorCode,
                                   ISIS_EXP_MET_SUPP_FLAG);
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting " "nmhTestv2IsisSummAddrExpenseMetric ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSummAddrFullMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen

                The Object
                testValFsIsisExtSummAddrFullMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)                                                             SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)                                                       SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsIsisExtSummAddrFullMetric (UINT4 *pu4ErrorCode, 
                                           INT4 i4FsIsisExtSysInstance, 
                                           INT4 i4FsIsisExtSummAddressType, 
                                           tSNMP_OCTET_STRING_TYPE 
                                                *pFsIsisExtSummAddress, 
                                           UINT4 u4FsIsisExtSummAddrPrefixLen, 
                                           UINT4 
                                           u4TestValFsIsisExtSummAddrFullMetric)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSummAddrFullMetric ()\n"));

    i1RetCode =
        IsisNmhValSATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                           i4FsIsisExtSummAddressType, pFsIsisExtSummAddress,
                           (INT4)u4FsIsisExtSummAddrPrefixLen, ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtSummAddrFullMetric ()\n"));
        return i1RetCode;
    }

    /* Full metric is not supported for Single-topology ISIS. Hence
     * verifying if MT support is enabled or not*/
    if ((IsisCtrlGetSysContext ((UINT4)i4FsIsisExtSysInstance, &pContext) ==
        ISIS_SUCCESS) && (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Full metric not supported "
                "for single-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_MET_NOT_SUPP_ST);
        return SNMP_FAILURE;
    }

    if ((u4TestValFsIsisExtSummAddrFullMetric < ISIS_MIN_METRIC_MT) ||
        (u4TestValFsIsisExtSummAddrFullMetric > ISIS_MAX_METRIC_MT))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "u4TestValIsisSummAddrFullMetric\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting " "nmhTestv2IsisSummAddrFullMetric ()\n"));

    return i1RetCode;
}

/* LOW LEVEL Routines for Table : FsIsisExtSysEventTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtSysEventTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysEventIdx
                FsIsisExtSysEvent
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtSysEventTable (INT4 i4FsIsisExtSysInstance,
                                                INT4 i4FsIsisExtSysEventIdx,
                                                INT4 i4FsIsisExtSysEvent)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtSysEventTable () \n"));

    if ((i4FsIsisExtSysInstance < 0)
        || (i4FsIsisExtSysInstance > (INT4) ISIS_MAX_INSTS))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Instance Index. The Index is %d\n",
                 i4FsIsisExtSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    if ((i4FsIsisExtSysEvent < 0) || (i4FsIsisExtSysEvent > ISIS_LL_MAX_EVENT))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Event. The Event is %d\n",
                 i4FsIsisExtSysEvent));
        i1ErrCode = SNMP_FAILURE;
    }

    if ((i4FsIsisExtSysEventIdx < 0)
        || (i4FsIsisExtSysEventIdx > ISIS_LL_MAX_EVTIDX))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Event Index. The Index is %d\n",
                 i4FsIsisExtSysEventIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtSysEventTable ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtSysEventTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysEventIdx
                FsIsisExtSysEvent
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtSysEventTable (INT4 *pi4FsIsisExtSysInstance,
                                        INT4 *pi4FsIsisExtSysEventIdx,
                                        INT4 *pi4FsIsisExtSysEvent)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtSysEventTable () \n"));

    i1ErrCode =
        nmhGetNextIndexFsIsisExtSysEventTable (0, pi4FsIsisExtSysInstance,
                                               0, pi4FsIsisExtSysEventIdx,
                                               0, pi4FsIsisExtSysEvent);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtSysEventTable ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtSysEventTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtSysEventIdx
                nextFsIsisExtSysEventIdx
                FsIsisExtSysEvent
                nextFsIsisExtSysEvent
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtSysEventTable (INT4 i4FsIsisExtSysInstance,
                                       INT4 *pi4NextFsIsisExtSysInstance,
                                       INT4 i4FsIsisExtSysEventIdx,
                                       INT4 *pi4NextFsIsisExtSysEventIdx,
                                       INT4 i4FsIsisExtSysEvent,
                                       INT4 *pi4NextFsIsisExtSysEvent)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisEvent         *pEvent = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexFsIsisExtSysEventTable () \n"));

    UNUSED_PARAM (i4FsIsisExtSysEvent);

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_FAILURE))
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                            &u4InstIdx) == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                     "Index with the given Indices \n"));
        }
        else
        {
           if(IsisCtrlGetSysContext (u4InstIdx, &pContext)
                 == ISIS_FAILURE)
           {
                 NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                                     "the corresponding index \n"));
           }
           i4FsIsisExtSysEventIdx = 0;
        }
    }
    while (pContext != NULL)
    {
        pEvent = pContext->EventTable.pEvent;

        if ((pEvent != NULL) && (i4FsIsisExtSysEventIdx == 0))
        {
            *pi4NextFsIsisExtSysInstance = (INT4) u4InstIdx;
            *pi4NextFsIsisExtSysEvent = (INT4) pEvent->u1Event;
            *pi4NextFsIsisExtSysEventIdx = (INT4) pEvent->u1EventIdx;
            i1ErrCode = SNMP_SUCCESS;

            NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                     "nmhGetNextIndexFsIsisExtSysEventTable () \n"));
            return i1ErrCode;
        }
        while (pEvent != NULL)
        {
            if (pEvent->u1EventIdx == i4FsIsisExtSysEventIdx)
            {
                if (pEvent->pNext != NULL)
                {
                    *pi4NextFsIsisExtSysInstance = (INT4) u4InstIdx;
                    *pi4NextFsIsisExtSysEvent = (INT4) pEvent->pNext->u1Event;
                    *pi4NextFsIsisExtSysEventIdx =
                        (INT4) pEvent->pNext->u1EventIdx;
                    i1ErrCode = SNMP_SUCCESS;
                }
                else
                {
                    i1ErrCode = SNMP_FAILURE;
                }
                break;
            }
            pEvent = pEvent->pNext;
        }
        if (i1ErrCode == SNMP_FAILURE)
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                                &u4InstIdx) == ISIS_FAILURE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                         "Index with the given Indices \n"));
                break;
            }
            else
            {
                if (IsisCtrlGetSysContext (u4InstIdx, &pContext)
                       == ISIS_FAILURE)
                {
                      NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                                          "the corresponding index \n"));
                }
                i4FsIsisExtSysEventIdx = 0;
            }
        }
        else
        {
            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSysEventTable ()  \n"));

    return i1ErrCode;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysEventStr
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysEventIdx
                FsIsisExtSysEvent

                The Object 
                retValFsIsisExtSysEventStr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysEventStr (INT4 i4FsIsisExtSysInstance,
                            INT4 i4FsIsisExtSysEventIdx,
                            INT4 i4FsIsisExtSysEvent,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsIsisExtSysEventStr)
{
    INT1                i1RetCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisEvent         *pEvent = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtSysEventStr () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_FAILURE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));
    }
    else
    {
        pEvent = pContext->EventTable.pEvent;

        while (pEvent != NULL)
        {
            if ((pEvent->u1Event == i4FsIsisExtSysEvent)
                && (pEvent->u1EventIdx == i4FsIsisExtSysEventIdx))
            {
                STRCPY (pRetValFsIsisExtSysEventStr->pu1_OctetList,
                        pEvent->au1EventData);
                pRetValFsIsisExtSysEventStr->i4_Length =
                    STRLEN (pRetValFsIsisExtSysEventStr->pu1_OctetList);
                i1RetCode = SNMP_SUCCESS;
                break;
            }
            pEvent = pEvent->pNext;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtSysEventStr ()  \n"));

    return (i1RetCode);
}

/* LOW LEVEL Routines for Table : FsIsisExtCircTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtCircTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtCircTable (INT4 i4FsIsisExtSysInstance,
                                            INT4 i4FsIsisExtCircIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtCircTable () \n"));

    if (i4FsIsisExtSysInstance < 0)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Ext Instance Index !!! The Index is %d\n",
                 i4FsIsisExtSysInstance));

        i1ErrCode = SNMP_FAILURE;
    }

    else if ((i4FsIsisExtCircIndex < 0)
             || (i4FsIsisExtCircIndex > (INT4) ISIS_MAX_CKTS))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Circuit Index !!! The Index is %d\n",
                 i4FsIsisExtCircIndex));

        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtCircTable ()  \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtCircTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtCircTable (INT4 *pi4FsIsisExtSysInstance,
                                    INT4 *pi4FsIsisExtCircIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtCircTable () \n"));

    i1ErrCode = nmhGetNextIndexFsIsisExtCircTable (0,
                                                   pi4FsIsisExtSysInstance,
                                                   0, pi4FsIsisExtCircIndex);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtCircTable ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtCircTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtCircIndex
                nextFsIsisExtCircIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtCircTable (INT4 i4FsIsisExtSysInstance,
                                   INT4 *pi4NextFsIsisExtSysInstance,
                                   INT4 i4FsIsisExtCircIndex,
                                   INT4 *pi4NextFsIsisExtCircIndex)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexFsIsisExtCircTable () \n"));

    i1ErrCode = nmhGetNextIndexIsisCircTable (i4FsIsisExtSysInstance,
                                              pi4NextFsIsisExtSysInstance,
                                              i4FsIsisExtCircIndex,
                                              pi4NextFsIsisExtCircIndex);
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtCircTable ()  \n"));

    return i1ErrCode;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircIfStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                retValFsIsisExtCircIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircIfStatus (INT4 i4FsIsisExtSysInstance,
                             INT4 i4FsIsisExtCircIndex,
                             INT4 *pi4RetValFsIsisExtCircIfStatus)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtCircIfStatus () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValFsIsisExtCircIfStatus = (INT4) pCktEntry->u1CktIfStatus;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index, %u\n", u4CktIdx));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtCircIfStatus ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircTxEnable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                retValFsIsisExtCircTxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircTxEnable (INT4 i4FsIsisExtSysInstance,
                             INT4 i4FsIsisExtCircIndex,
                             INT4 *pi4RetValFsIsisExtCircTxEnable)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtCircTxEnable () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtCircTxEnable = (INT4) pCktEntry->bTxEnable;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtCircTxEnable ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircRxEnable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                retValFsIsisExtCircRxEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircRxEnable (INT4 i4FsIsisExtSysInstance,
                             INT4 i4FsIsisExtCircIndex,
                             INT4 *pi4RetValFsIsisExtCircRxEnable)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtCircRxEnable () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtCircRxEnable = (INT4) pCktEntry->bRxEnable;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtCircRxEnable ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircTxISHs
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                retValFsIsisExtCircTxISHs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircTxISHs (INT4 i4FsIsisExtSysInstance,
                           INT4 i4FsIsisExtCircIndex,
                           INT4 *pi4RetValFsIsisExtCircTxISHs)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtCircTxISHs () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtCircTxISHs = (INT4) pCktEntry->CktStats.u4TxISHPDUs;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtCircTxISHs ()\n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircRxISHs
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                retValFsIsisExtCircRxISHs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircRxISHs (INT4 i4FsIsisExtSysInstance,
                           INT4 i4FsIsisExtCircIndex,
                           INT4 *pi4RetValFsIsisExtCircRxISHs)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtCircTxISHs () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtCircRxISHs = (INT4) pCktEntry->CktStats.u4RxISHPDUs;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtCircTxISHs ()\n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircMTID
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                retValFsIsisExtCircMTID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircMTID (INT4 i4FsIsisExtSysInstance,
                         INT4 i4FsIsisExtCircIndex,
                         INT4 *pi4RetValFsIsisExtCircMTID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtCircMTID () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                               &pContext) == ISIS_SUCCESS)
    {
        if (IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                              &pCktEntry) == ISIS_SUCCESS)
        {
            *pi4RetValFsIsisExtCircMTID = pCktEntry->u1IfMTId;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index, %u\n", i4FsIsisExtCircIndex));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtCircMTID ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircSNPA
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                retValFsIsisExtCircSNPA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircSNPA (INT4 i4FsIsisExtSysInstance, INT4 i4FsIsisExtCircIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsIsisExtCircSNPA)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered " "nmhGetFsIsisExtCircSNPA () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        MEMCPY (pRetValFsIsisExtCircSNPA->pu1_OctetList,
                pCktEntry->au1SNPA, ISIS_SNPA_ADDR_LEN);
        pRetValFsIsisExtCircSNPA->i4_Length = ISIS_SNPA_ADDR_LEN;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting " "nmhGetFsIsisExtCircSNPA ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircBfdStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object
                retValFsIsisExtCircBfdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhGetFsIsisExtCircBfdStatus (INT4 i4FsIsisExtSysInstance, 
                              INT4 i4FsIsisExtCircIndex, 
                              INT4 *pi4RetValFsIsisExtCircBfdStatus)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtCircBfdStatus () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtCircBfdStatus = (INT4) pCktEntry->u1IsisBfdStatus;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtCircBfdStatus ()\n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircExtendedCircID
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                retValFsIsisExtCircExtendedCircID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisExtCircExtendedCircID(INT4 i4FsIsisExtSysInstance , 
										INT4 i4FsIsisExtCircIndex , 
										UINT4 *pu4RetValFsIsisExtCircExtendedCircID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;


    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }
    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        *pu4RetValFsIsisExtCircExtendedCircID = pCktEntry->u4ExtLocalCircID;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }
	return (i1ErrCode);
}



/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircIfStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                setValFsIsisExtCircIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircIfStatus (INT4 i4FsIsisExtSysInstance,
                             INT4 i4FsIsisExtCircIndex,
                             INT4 i4SetValFsIsisExtCircIfStatus)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Status = ISIS_DOWN;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisExtCktIfStatus () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }

    else if (pCktEntry->u1CktIfStatus == (UINT1) i4SetValFsIsisExtCircIfStatus)
    {
        i1ErrCode = SNMP_SUCCESS;
        NMP_PT ((ISIS_LGST, "NMP <E> : CktIf Status is already set to the"
                 "desired value exist for the Index, %u\n", u4CktIdx));
    }
    else
    {
        pCktEntry->u1CktIfStatus = (UINT1) i4SetValFsIsisExtCircIfStatus;

        if (((pCktEntry->u1CktExistState == ISIS_ACTIVE)
             && (pCktEntry->bCktAdminState == ISIS_STATE_ON))
            && (pContext->u1OperState == ISIS_UP))
        {
            if (pCktEntry->u1CktIfStatus == ISIS_STATE_ON)
            {
                pCktEntry->u1OperState = (UINT1) ISIS_UP;
                u1Status = (UINT1) ISIS_CKT_UP;
            }
            else
            {
                pCktEntry->u1OperState = (UINT1) ISIS_DOWN;
                u1Status = (UINT1) ISIS_CKT_DOWN;
            }

            pCktEvt = (tIsisEvtCktChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));

            if (pCktEvt != NULL)
            {
                pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                pCktEvt->u1Status = u1Status;
                pCktEvt->u1CktType = pCktEntry->u1CktType;
                pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;

                IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                  sizeof (tIsisEvtCktChange));
            }

        }

        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtCktIfStatus ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircTxEnable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                setValFsIsisExtCircTxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircTxEnable (INT4 i4FsIsisExtSysInstance,
                             INT4 i4FsIsisExtCircIndex,
                             INT4 i4SetValFsIsisExtCircTxEnable)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisExtCircTxEnable () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not"
                 "exist for the Index, %u\n", u4CktIdx));
    }
    else
    {
        pCktEntry->bTxEnable = (UINT1) i4SetValFsIsisExtCircTxEnable;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtCircTxEnable () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircRxEnable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                setValFsIsisExtCircRxEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircRxEnable (INT4 i4FsIsisExtSysInstance,
                             INT4 i4FsIsisExtCircIndex,
                             INT4 i4SetValFsIsisExtCircRxEnable)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisExtCircRxEnable () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }
    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }
    else
    {
        pCktEntry->bRxEnable = (UINT1) i4SetValFsIsisExtCircRxEnable;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtCircRxEnable () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircSNPA
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                setValFsIsisExtCircSNPA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircSNPA (INT4 i4FsIsisExtSysInstance, INT4 i4FsIsisExtCircIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsIsisExtCircSNPA)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisExtCircSNPA () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }
    else
    {
        /* Check for the Status of the RowStatus in the Circuit Table
         */

        if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could not Set SNPA. Circuit "
                     "RowStatus was Active\n"));
        }

        /* Set the Value only if the RowStatus is not in Active State
         */

        else
        {
            MEMCPY (pCktEntry->au1SNPA, pSetValFsIsisExtCircSNPA->pu1_OctetList,
                    ISIS_SNPA_ADDR_LEN);
            i1ErrCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtCircSNPA ()  \n"));

    return i1ErrCode;

}

/****************************************************************************                              
 Function    :  nmhSetFsIsisExtCircBfdStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object
                setValFsIsisExtCircBfdStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhSetFsIsisExtCircBfdStatus (INT4 i4FsIsisExtSysInstance, 
                              INT4 i4FsIsisExtCircIndex, 
                              INT4 i4SetValFsIsisExtCircBfdStatus)
{
#ifdef BFD_WANTED
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pTravAdj = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisExtCircBfdStatus () \n"));

#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }
    else
    {
        if ((UINT1) i4SetValFsIsisExtCircBfdStatus ==
            pCktEntry->u1IsisBfdStatus)
        {
            return SNMP_SUCCESS;
        }

        if (pContext->u1IsisBfdSupport == ISIS_TRUE)
        {
            pCktEntry->u1IsisBfdStatus = 
                (UINT1) i4SetValFsIsisExtCircBfdStatus;
            i1ErrCode = SNMP_SUCCESS;

            /* While enabling/disabling BFD on a interfac e, do
             * the following:
             * While enabling (isis bfd):
             * 1. If single-topology ISIS is enabled, check for all the
             *    Adjacencies in the circuit record. If the Adjacency
             *    state is UP, do BFD Registration.
             *    In case of multi-topology ISIS, BFD Registration
             *    will be done on the go while computing BFD_REQUIRED
             *    variable.
             * While disabling (isis bfd disable):
             * 1. Deregister from BFD for all the Adjacencies (both
             *    single topology and multi-topology adjs). */

            pTravAdj = pCktEntry->pAdjEntry;

            while (pTravAdj != NULL)
            {
                if (i4SetValFsIsisExtCircBfdStatus == ISIS_BFD_DISABLE)
                {
                    ISIS_BFD_DEREGISTER(pContext, pTravAdj, pCktEntry, ISIS_TRUE);
                }
                else
                {
                    if ((pContext->u1IsisMTSupport == ISIS_FALSE) &&
                        (pTravAdj->u1AdjState == ISIS_ADJ_UP))
                    {
                        ISIS_BFD_REGISTER (pContext, pTravAdj, pCktEntry);
                    }
                }
                pTravAdj = pTravAdj->pNext;
            }
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtCircBfdStatus () \n"));

    return i1ErrCode;
#else
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4FsIsisExtCircIndex);
    UNUSED_PARAM (i4SetValFsIsisExtCircBfdStatus);
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircIfStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                testValFsIsisExtCircIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircIfStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtCircIndex,
                                INT4 i4TestValFsIsisExtCircIfStatus)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisExtCircIfStatus ()\n"));

    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4FsIsisExtCircIndex);
    if ((i4TestValFsIsisExtCircIfStatus != ISIS_STATE_OFF) &&
        (i4TestValFsIsisExtCircIfStatus != ISIS_STATE_ON))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of Circuit Interface Status !!! \n"));
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2FsIsisExtCircIfStatus ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircTxEnable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                testValFsIsisExtCircTxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircTxEnable (UINT4 *pu4ErrorCode,
                                INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtCircIndex,
                                INT4 i4TestValFsIsisExtCircTxEnable)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtCircTxEnable () \n"));

    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4FsIsisExtCircIndex);

    if ((i4TestValFsIsisExtCircTxEnable != ISIS_TRUE) &&
        (i4TestValFsIsisExtCircTxEnable != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of Circuit Tx Enable !!! \n"));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtCircTxEnable ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircRxEnable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                testValFsIsisExtCircRxEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircRxEnable (UINT4 *pu4ErrorCode,
                                INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtCircIndex,
                                INT4 i4TestValFsIsisExtCircRxEnable)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtCircRxEnable () \n"));

    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4FsIsisExtCircIndex);

    if ((i4TestValFsIsisExtCircRxEnable != ISIS_TRUE) &&
        (i4TestValFsIsisExtCircRxEnable != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of Circuit Rx Enable !!! \n"));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtCircRxEnable ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircSNPA
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object 
                testValFsIsisExtCircSNPA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircSNPA (UINT4 *pu4ErrorCode, INT4 i4FsIsisExtSysInstance,
                            INT4 i4FsIsisExtCircIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsIsisExtCircSNPA)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtCircSNPA () \n"));

    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4FsIsisExtCircIndex);

    if (pTestValFsIsisExtCircSNPA->i4_Length != ISIS_SNPA_ADDR_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;    /*silvercreek jul22 */
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Length of Circuit SNPA Address !!!\n"));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisExtCircSNPA ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircBfdStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex

                The Object
                testValFsIsisExtCircBfdStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 
nmhTestv2FsIsisExtCircBfdStatus (UINT4 *pu4ErrorCode, 
                                 INT4 i4FsIsisExtSysInstance, 
                                 INT4 i4FsIsisExtCircIndex, 
                                 INT4 i4TestValFsIsisExtCircBfdStatus)
{
    tIsisSysContext    *pContext = NULL;
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtCircBfdStatus () \n"));

    UNUSED_PARAM (i4FsIsisExtCircIndex);

    if ((i4TestValFsIsisExtCircBfdStatus != ISIS_TRUE) &&
        (i4TestValFsIsisExtCircBfdStatus != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of Circuit BFD status!!! \n"));
    }

    /* Getting the system context */
    if ((IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext))
        != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return SNMP_FAILURE;
    }

    /* BFD support for ISIS should be enabled to enable
       BFD at interface level */
    if (pContext->u1IsisBfdSupport != ISIS_TRUE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : BFD support for M-ISIS is not "
                 "enabled for the system instance,  %d\n",
                 i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISIS_BFD_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtCircBfdStatus ()  \n"));

    return i1ErrCode;
}

/* LOW LEVEL Routines for Table : FsIsisExtCircLevelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtCircLevelTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtCircLevelTable (INT4 i4FsIsisExtSysInstance,
                                                 INT4 i4FsIsisExtCircIndex,
                                                 INT4 i4FsIsisExtCircLevelIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtCircLevelTable () \n"));

    if (i4FsIsisExtSysInstance < 0)
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Instance Index. "
                 "The Index is %d\n", i4FsIsisExtSysInstance));
    }
    if ((i4FsIsisExtCircIndex < 0)
        || (i4FsIsisExtCircIndex > (INT4) ISIS_MAX_CKTS))
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Index. "
                 "The Index is %d\n", i4FsIsisExtCircIndex));
    }

    if ((u1CktLvlIdx != ISIS_LEVEL1) &&
        (u1CktLvlIdx != ISIS_LEVEL2) && (u1CktLvlIdx != ISIS_LEVEL12))
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                 "The Index is %u\n", u1CktLvlIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtCircLevelTable ()  \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtCircLevelTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtCircLevelTable (INT4 *pi4FsIsisExtSysInstance,
                                         INT4 *pi4FsIsisExtCircIndex,
                                         INT4 *pi4FsIsisExtCircLevelIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtCircLevelTable () \n"));

    i1ErrCode = nmhGetFirstIndexIsisCircLevelTable (pi4FsIsisExtSysInstance,
                                                    pi4FsIsisExtCircIndex,
                                                    pi4FsIsisExtCircLevelIndex);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtCircLevelTable ()  \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtCircLevelTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtCircIndex
                nextFsIsisExtCircIndex
                FsIsisExtCircLevelIndex
                nextFsIsisExtCircLevelIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtCircLevelTable (INT4 i4FsIsisExtSysInstance,
                                        INT4 *pi4NextFsIsisExtSysInstance,
                                        INT4 i4FsIsisExtCircIndex,
                                        INT4 *pi4NextFsIsisExtCircIndex,
                                        INT4 i4FsIsisExtCircLevelIndex,
                                        INT4 *pi4NextFsIsisExtCircLevelIndex)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexFsIsisExtCircLevelTable () \n"));

    i1ErrCode = nmhGetNextIndexIsisCircLevelTable (i4FsIsisExtSysInstance,
                                                   pi4NextFsIsisExtSysInstance,
                                                   i4FsIsisExtCircIndex,
                                                   pi4NextFsIsisExtCircIndex,
                                                   i4FsIsisExtCircLevelIndex,
                                                   pi4NextFsIsisExtCircLevelIndex);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtCircLevelTable  \n"));

    return i1ErrCode;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircLevelDelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                retValFsIsisExtCircLevelDelayMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircLevelDelayMetric (INT4 i4FsIsisExtSysInstance,
                                     INT4 i4FsIsisExtCircIndex,
                                     INT4 i4FsIsisExtCircLevelIndex,
                                     INT4
                                     *pi4RetValFsIsisExtCircLevelDelayMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetricIdx = 0;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelDelayMetric () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            if (!
                (pContext->SysActuals.u1SysMetricSupp & ISIS_DEL_MET_SUPP_FLAG))
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Error Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            else
            {
                pCktLvlEntry = (u1CktLvlIdx == ISIS_LEVEL1) ?
                    (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);
                u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);
                *pi4RetValFsIsisExtCircLevelDelayMetric =
                    (INT4) ((pCktLvlEntry->Metric[u1MetricIdx])
                            & (ISIS_METRIC_VAL_MASK));

            }

            i1ErrCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelDelayMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircLevelErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                retValFsIsisExtCircLevelErrorMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircLevelErrorMetric (INT4 i4FsIsisExtSysInstance,
                                     INT4 i4FsIsisExtCircIndex,
                                     INT4 i4FsIsisExtCircLevelIndex,
                                     INT4
                                     *pi4RetValFsIsisExtCircLevelErrorMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetricIdx = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelErrorMetric () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            pCktLvlEntry = (u1CktLvlIdx == ISIS_LEVEL1) ?
                (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);

            if ((pContext->SysActuals.u1SysMetricSupp & ISIS_ERR_MET_SUPP_FLAG))
            {
                u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);
                *pi4RetValFsIsisExtCircLevelErrorMetric =
                    (INT4) (pCktLvlEntry->Metric[u1MetricIdx] &
                            ISIS_METRIC_VAL_MASK);
            }
            else
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Error Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            i1ErrCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelErrorMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircLevelExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                retValFsIsisExtCircLevelExpenseMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircLevelExpenseMetric (INT4 i4FsIsisExtSysInstance,
                                       INT4 i4FsIsisExtCircIndex,
                                       INT4 i4FsIsisExtCircLevelIndex,
                                       INT4
                                       *pi4RetValFsIsisExtCircLevelExpenseMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetricIdx = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelExpenseMetric () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index !!! \n"));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            if ((pContext->SysActuals.u1SysMetricSupp & ISIS_EXP_MET_SUPP_FLAG))
            {
                pCktLvlEntry = (u1CktLvlIdx == ISIS_LEVEL1) ?
                    (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);
                u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);
                *pi4RetValFsIsisExtCircLevelExpenseMetric =
                    (INT4) (pCktLvlEntry->Metric[u1MetricIdx]
                            & ISIS_METRIC_VAL_MASK);
            }
            else
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Expense  Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            i1ErrCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelExpenseMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircLevelTxPassword
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                retValFsIsisExtCircLevelTxPassword
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircLevelTxPassword (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtCircIndex,
                                    INT4 i4FsIsisExtCircLevelIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsIsisExtCircLevelTxPassword)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtCircLevelTxPassword () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circ does not "
                 "exist for the Index, %d\n", i4FsIsisExtCircIndex));
    }
    else
    {
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            pCktLevel = (u1CktLvlIdx == ISIS_LEVEL1) ? (pCktEntry->pL1CktInfo)
                : (pCktEntry->pL2CktInfo);

            if (pCktLevel == NULL)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index, %u\n", u1CktLvlIdx));

                i1ErrCode = SNMP_FAILURE;
            }
            else
            {
                pRetValFsIsisExtCircLevelTxPassword->i4_Length =
                    pCktLevel->CktTxPasswd.u1Len;
                MEMCPY (pRetValFsIsisExtCircLevelTxPassword->pu1_OctetList,
                        pCktLevel->CktTxPasswd.au1Password,
                        ISIS_MAX_PASSWORD_LEN);

                i1ErrCode = SNMP_SUCCESS;
            }
        }

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtCircLevelTxPassword ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircLevelWideMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object
                retValFsIsisExtCircLevelWideMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisExtCircLevelWideMetric(INT4 i4FsIsisExtSysInstance, 
                                        INT4 i4FsIsisExtCircIndex, 
                                        INT4 i4FsIsisExtCircLevelIndex, 
                                        UINT4 *pu4RetValFsIsisExtCircLevelWideMetric)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelWideMetric () \n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index !!! \n"));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            pCktLvlEntry = (u1CktLvlIdx == ISIS_LEVEL1) ?
                 (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                *pu4RetValFsIsisExtCircLevelWideMetric = 
                                          pCktLvlEntry->u4FullMetric;
            }
            else
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Wide Metric is not supported"
                         "for the Current Configuration!!!\n"));
				*pu4RetValFsIsisExtCircLevelWideMetric = 0;
            }
            i1ErrCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelWideMetric () \n"));

    return i1ErrCode;
}

/****************************************************************************
    Function    :  nmhGetFsIsisExtCircLevelAuthType
    Input       :  The Indices
                   FsIsisExtSysInstance
                   FsIsisExtCircIndex
                   FsIsisExtCircLevelIndex
                   
                   The Object
                   retValFsIsisExtCircLevelAuthType
    Output      :  The Get Low Lev Routine Take the Indices &
		   store the Value requested in the Return val.
     Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisExtCircLevelAuthType(INT4 i4FsIsisExtSysInstance , INT4 i4FsIsisExtCircIndex , INT4 i4FsIsisExtCircLevelIndex , INT4 *pi4RetValFsIsisExtCircLevelAuthType)
{


	INT1                i1ErrCode = SNMP_FAILURE;
	INT4                i4RetVal = ISIS_SUCCESS;
	UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
	UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
	UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
	tIsisCktLevel      *pCktLevel = NULL;
	tIsisSysContext    *pContext = NULL;
	tIsisCktEntry      *pCktEntry = NULL;

	NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
				"nmhGetFsIsisExtCircLevelAuthType () \n"));

	if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
	{
		NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
					"exist for the Index, %d\n", i4FsIsisExtSysInstance));
	}

	else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
					&pCktEntry)) != ISIS_SUCCESS)
	{
		NMP_PT ((ISIS_LGST, "NMP <E> : Circ does not "
					"exist for the Index, %d\n", i4FsIsisExtCircIndex));
	}
	else
	{
		pCktLevel = (u1CktLvlIdx == ISIS_LEVEL1) ? (pCktEntry->pL1CktInfo)
			: (pCktEntry->pL2CktInfo);

		if (pCktLevel == NULL)
		{
			NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
						"exist for the Index, %u\n", u1CktLvlIdx));
			i1ErrCode = SNMP_FAILURE;
		}
		else
		{
			*pi4RetValFsIsisExtCircLevelAuthType = pCktLevel->u1CircLevelAuthType;
			i1ErrCode = SNMP_SUCCESS;
		}
	}

	NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
				"nmhGetFsIsisExtCircLevelAuthType () \n"));

	return i1ErrCode;
}



/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircLevelDelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                setValFsIsisExtCircLevelDelayMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircLevelDelayMetric (INT4 i4FsIsisExtSysInstance,
                                     INT4 i4FsIsisExtCircIndex,
                                     INT4 i4FsIsisExtCircLevelIndex,
                                     INT4 i4SetValFsIsisExtCircLevelDelayMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetricIdx = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelDelayMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_EE ((ISIS_LGST, "NMP <E> : The Instance does not exist !!! \n"));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        if (pContext->u1IsisMTSupport == ISIS_TRUE)
        {
            return SNMP_FAILURE;
        }
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            pCktLvlEntry = (u1CktLvlIdx == ISIS_LEVEL1) ?
                (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);
            if (pCktLvlEntry != NULL)
            {
                u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);
                pCktLvlEntry->Metric[u1MetricIdx] =
                    (UINT1) i4SetValFsIsisExtCircLevelDelayMetric;
                pCktLvlEntry->Metric[u1MetricIdx] &= ~ISIS_METRIC_SUPP_MASK;
                i1ErrCode = SNMP_SUCCESS;

                pCktEvt = (tIsisEvtCktChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));

                if (pCktEvt != NULL)
                {
                    pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                    pCktEvt->u1Status = ISIS_CKT_MODIFY;
                    pCktEvt->u1CktType = pCktEntry->u1CktType;
                    pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                    pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;
                    MEMCPY (pCktEvt->Metric, pCktLvlEntry->Metric,
                            (sizeof (tIsisMetric)));

                    IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                      sizeof (tIsisEvtCktChange));
                }
            }
            else
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Info does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));
            }

        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelDelayMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircLevelErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                setValFsIsisExtCircLevelErrorMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircLevelErrorMetric (INT4 i4FsIsisExtSysInstance,
                                     INT4 i4FsIsisExtCircIndex,
                                     INT4 i4FsIsisExtCircLevelIndex,
                                     INT4 i4SetValFsIsisExtCircLevelErrorMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetricIdx = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelErrorMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_EE ((ISIS_LGST, "NMP <E> : The Instance does not exist !!! \n"));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        if (pContext->u1IsisMTSupport == ISIS_TRUE)
        {
            return SNMP_FAILURE;
        }
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            pCktLvlEntry = (u1CktLvlIdx == ISIS_LEVEL1) ?
                (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);

            u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);
            if ((pCktLvlEntry == NULL) || (u1MetricIdx >= ISIS_NUM_METRICS))
            {
                return SNMP_FAILURE;
            }
            pCktLvlEntry->Metric[u1MetricIdx] =
                (UINT1) i4SetValFsIsisExtCircLevelErrorMetric;
            pCktLvlEntry->Metric[u1MetricIdx] &= ~ISIS_METRIC_SUPP_MASK;
            i1ErrCode = SNMP_SUCCESS;

            pCktEvt = (tIsisEvtCktChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));

            if (pCktEvt != NULL)
            {
                pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                pCktEvt->u1Status = ISIS_CKT_MODIFY;
                pCktEvt->u1CktType = pCktEntry->u1CktType;
                pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;
                MEMCPY (pCktEvt->Metric, pCktLvlEntry->Metric,
                        (sizeof (tIsisMetric)));

                IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                  sizeof (tIsisEvtCktChange));
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelErrorMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircLevelExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                setValFsIsisExtCircLevelExpenseMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircLevelExpenseMetric (INT4 i4FsIsisExtSysInstance,
                                       INT4 i4FsIsisExtCircIndex,
                                       INT4 i4FsIsisExtCircLevelIndex,
                                       INT4
                                       i4SetValFsIsisExtCircLevelExpenseMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetricIdx = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelExpenseMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_EE ((ISIS_LGST, "NMP <E> : The Instance does not exist !!! \n"));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        if (pContext->u1IsisMTSupport == ISIS_TRUE)
        {
            return SNMP_FAILURE;
        }
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            pCktLvlEntry = (u1CktLvlIdx == ISIS_LEVEL1) ?
                (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);

            u1MetricIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);

            if ((pCktLvlEntry == NULL) || (u1MetricIdx >= ISIS_NUM_METRICS))
            {
                return SNMP_FAILURE;
            }

            pCktLvlEntry->Metric[u1MetricIdx] =
                (UINT1) i4SetValFsIsisExtCircLevelExpenseMetric;
            pCktLvlEntry->Metric[u1MetricIdx] &= ~ISIS_METRIC_SUPP_MASK;
            i1ErrCode = SNMP_SUCCESS;

            pCktEvt = (tIsisEvtCktChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));

            if (pCktEvt != NULL)
            {
                pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                pCktEvt->u1Status = ISIS_CKT_MODIFY;
                pCktEvt->u1CktType = pCktEntry->u1CktType;
                pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;
                MEMCPY (pCktEvt->Metric, pCktLvlEntry->Metric,
                        (sizeof (tIsisMetric)));

                IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                  sizeof (tIsisEvtCktChange));
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelExpenseMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircLevelTxPassword
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                setValFsIsisExtCircLevelTxPassword
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircLevelTxPassword (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtCircIndex,
                                    INT4 i4FsIsisExtCircLevelIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsIsisExtCircLevelTxPassword)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtCircLevelTxPassword () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circ does not "
                 "exist for the Index, %d\n", i4FsIsisExtCircIndex));
    }
    else
    {
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", i4FsIsisExtCircLevelIndex));
        }
        else
        {
            pCktLevel = (u1CktLvlIdx == ISIS_LEVEL1) ? (pCktEntry->pL1CktInfo)
                : (pCktEntry->pL2CktInfo);

            if (pCktLevel != NULL)
            {
                pCktLevel->CktTxPasswd.u1ExistState = ISIS_ACTIVE;
                if (pSetValFsIsisExtCircLevelTxPassword->i4_Length == 0)
                {
                    MEMSET (pCktLevel->CktTxPasswd.au1Password, 0,
                            ISIS_MAX_PASSWORD_LEN);
                }
                else
                {
                    MEMCPY (pCktLevel->CktTxPasswd.au1Password,
                            pSetValFsIsisExtCircLevelTxPassword->pu1_OctetList,
                            pSetValFsIsisExtCircLevelTxPassword->i4_Length);
                }
                pCktLevel->CktTxPasswd.u1Len =
                    (UINT1) pSetValFsIsisExtCircLevelTxPassword->i4_Length;
                i1ErrCode = SNMP_SUCCESS;

            }
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtCircLevelTxPassword ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircLevelWideMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object
                setValFsIsisExtCircLevelWideMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsIsisExtCircLevelWideMetric (INT4 i4FsIsisExtSysInstance, 
                                         INT4 i4FsIsisExtCircIndex, 
                                         INT4 i4FsIsisExtCircLevelIndex, 
                                         UINT4 u4SetValFsIsisExtCircLevelWideMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetricIdx = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelWideMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_EE ((ISIS_LGST, "NMP <E> : The Instance does not exist !!! \n"));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) == ISIS_SUCCESS)
    {
        if (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC)
        {
            return SNMP_FAILURE;
        }
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            pCktLvlEntry = (u1CktLvlIdx == ISIS_LEVEL1) ?
                (pCktEntry->pL1CktInfo) : (pCktEntry->pL2CktInfo);

            if ((pCktLvlEntry == NULL) || (u1MetricIdx >= ISIS_NUM_METRICS))
            {
                return SNMP_FAILURE;
            }

            pCktLvlEntry->u4FullMetric =
                u4SetValFsIsisExtCircLevelWideMetric;
            i1ErrCode = SNMP_SUCCESS;

            pCktEvt = (tIsisEvtCktChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));

            if (pCktEvt != NULL)
            {
                pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                pCktEvt->u1Status = ISIS_CKT_MODIFY;
                pCktEvt->u1CktType = pCktEntry->u1CktType;
                pCktEvt->u1CktLevel = u1CktLvlIdx;
                pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;
                pCktEvt->u4FullMetric = pCktLvlEntry->u4FullMetric;

                IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                  sizeof (tIsisEvtCktChange));
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelWideMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
    Function    :  nmhSetFsIsisExtCircLevelAuthType
    Input       :  The Indices
                   FsIsisExtSysInstance
                   FsIsisExtCircIndex
                   FsIsisExtCircLevelIndex
              
                   The Object
                   setValFsIsisExtCircLevelAuthType
    Output      :  The Set Low Lev Routine Take the Indices &
		   Sets the Value accordingly.
    Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsIsisExtCircLevelAuthType(INT4 i4FsIsisExtSysInstance , INT4 i4FsIsisExtCircIndex , INT4 i4FsIsisExtCircLevelIndex , INT4 i4SetValFsIsisExtCircLevelAuthType)
{
	INT4                i4RetVal = ISIS_SUCCESS;
	UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
	UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
	UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
	tIsisCktLevel      *pCktLevel = NULL;
	tIsisCktEntry      *pCktEntry = NULL;
	tIsisSysContext    *pContext = NULL;

	NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
				"nmhSetFsIsisExtCircLevelAuthType () \n"));
#ifdef ISIS_FT_ENABLED
	if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
	{
		return SNMP_FAILURE;
	}
#endif
	if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
	{
		NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
					"exist for the Index, %d\n", i4FsIsisExtSysInstance));
	}

	else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
					&pCktEntry)) != ISIS_SUCCESS)
	{
		NMP_PT ((ISIS_LGST, "NMP <E> : Circ does not "
					"exist for the Index, %d\n", i4FsIsisExtCircIndex));
	}
	else
	{
		if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
		{
			NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
						"exist for the Index, %u\n", u1CktLvlIdx));
		}
		else
		{
			pCktLevel = (u1CktLvlIdx == ISIS_LEVEL1) ? (pCktEntry->pL1CktInfo)
				: (pCktEntry->pL2CktInfo);

			if (pCktLevel != NULL)
			{
				pCktLevel->u1CircLevelAuthType = (UINT1)i4SetValFsIsisExtCircLevelAuthType;
			}
		}
	}
	NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
				"nmhSetFsIsisExtCircLevelAuthType () \n"));
	return SNMP_SUCCESS;
}



/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircLevelDelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                testValFsIsisExtCircLevelDelayMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircLevelDelayMetric (UINT4 *pu4ErrorCode,
                                        INT4 i4FsIsisExtSysInstance,
                                        INT4 i4FsIsisExtCircIndex,
                                        INT4 i4FsIsisExtCircLevelIndex,
                                        INT4
                                        i4TestValFsIsisExtCircLevelDelayMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UNUSED_PARAM (i4FsIsisExtCircIndex);
    UNUSED_PARAM (i4FsIsisExtCircLevelIndex);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4TestValFsIsisExtCircLevelDelayMetric);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircLevelErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                testValFsIsisExtCircLevelErrorMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircLevelErrorMetric (UINT4 *pu4ErrorCode,
                                        INT4 i4FsIsisExtSysInstance,
                                        INT4 i4FsIsisExtCircIndex,
                                        INT4 i4FsIsisExtCircLevelIndex,
                                        INT4
                                        i4TestValFsIsisExtCircLevelErrorMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    UNUSED_PARAM (i4FsIsisExtCircIndex);
    UNUSED_PARAM (i4FsIsisExtCircLevelIndex);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4TestValFsIsisExtCircLevelErrorMetric);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircLevelExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                testValFsIsisExtCircLevelExpenseMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircLevelExpenseMetric (UINT4 *pu4ErrorCode,
                                          INT4 i4FsIsisExtSysInstance,
                                          INT4 i4FsIsisExtCircIndex,
                                          INT4 i4FsIsisExtCircLevelIndex,
                                          INT4
                                          i4TestValFsIsisExtCircLevelExpenseMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    UNUSED_PARAM (i4FsIsisExtCircIndex);
    UNUSED_PARAM (i4FsIsisExtCircLevelIndex);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4TestValFsIsisExtCircLevelExpenseMetric);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircLevelTxPassword
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object 
                testValFsIsisExtCircLevelTxPassword
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircLevelTxPassword (UINT4 *pu4ErrorCode,
                                       INT4 i4FsIsisExtSysInstance,
                                       INT4 i4FsIsisExtCircIndex,
                                       INT4 i4FsIsisExtCircLevelIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFsIsisExtCircLevelTxPassword)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtCircLevelTxPassword () \n"));
    
#ifdef ISIS_FT_ENABLED
   if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
   {
       return SNMP_FAILURE;
   }
   #endif
   if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
   {
       i1ErrCode = SNMP_FAILURE;
       NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                "exist for the Index, %d\n", i4FsIsisExtSysInstance));
       CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
   }
   else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
   {
       i1ErrCode = SNMP_FAILURE;
       NMP_PT ((ISIS_LGST, "NMP <E> : Circ does not "
                "exist for the Index, %d\n", i4FsIsisExtCircIndex));
       CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
   }  
   else
   {
       if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
       {
           i1ErrCode = SNMP_FAILURE; 
           NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", i4FsIsisExtCircLevelIndex));
           CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
       }
       else
       {
 	    pCktLevel = (u1CktLvlIdx == ISIS_LEVEL1) ? (pCktEntry->pL1CktInfo)
                : (pCktEntry->pL2CktInfo);

    	    if (pCktLevel != NULL)
    	    {
	    	 if (pCktLevel->CktTxPasswd.au1Password[0] != '\0')
             	 {
   		 	*pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
                  	i1ErrCode = SNMP_FAILURE;
                  	CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
              	 }
             }
        } 
   }   

   if ((pTestValFsIsisExtCircLevelTxPassword->i4_Length <= 0)
        || (pTestValFsIsisExtCircLevelTxPassword->i4_Length >
            ISIS_MAX_PASSWORD_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;    /*silvercreek jul22 */
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of Password Length !! \n"));
        CLI_SET_ERR (CLI_ISIS_INV_LEN);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtCircLevelTxPassword ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircLevelWideMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex

                The Object
                testValFsIsisExtCircLevelWideMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsIsisExtCircLevelWideMetric (UINT4 *pu4ErrorCode,
                                            INT4 i4FsIsisExtSysInstance,
                                            INT4 i4FsIsisExtCircIndex,
                                            INT4 i4FsIsisExtCircLevelIndex, 
                                            UINT4 u4TestValFsIsisExtCircLevelWideMetric)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelWideMetric () \n"));

    UNUSED_PARAM (i4FsIsisExtCircIndex);
    UNUSED_PARAM (i4FsIsisExtCircLevelIndex);

    /* Full metric is not supported for Single-topology ISIS. Hence
     * verifying if MT support is enabled or not*/
    if ((IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext) ==
        ISIS_SUCCESS) && (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Wide metric not supported "
                "for single-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_MET_NOT_SUPP_ST);
        return SNMP_FAILURE;
    }

    if ((u4TestValFsIsisExtCircLevelWideMetric < ISIS_LL_CKTL_MIN_METRIC_MT) ||
        (u4TestValFsIsisExtCircLevelWideMetric > ISIS_LL_CKTL_MAX_METRIC_MT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_DEF_MET);
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircLevelWideMetric !!! \n"));
        return i1ErrCode;
    }
    
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting " "nmhTestv2IsisCircLevelWideMetric () \n"));

    return i1ErrCode;
}


/****************************************************************************
    Function    :  nmhTestv2FsIsisExtCircLevelAuthType
    Input       :  The Indices
                   FsIsisExtSysInstance
                   FsIsisExtCircIndex
                   FsIsisExtCircLevelIndex
 
                   The Object
                   testValFsIsisExtCircLevelAuthType
    Output      :  The Test Low Lev Routine Take the Indices &
                   Test whether that Value is Valid Input for Set.
                   Stores the value of error code in the Return val
    Error Codes :  The following error codes are to be returned
                   SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                   SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                   SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                   SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                   SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
    Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
 INT1 nmhTestv2FsIsisExtCircLevelAuthType(UINT4 *pu4ErrorCode , INT4 i4FsIsisExtSysInstance , INT4 i4FsIsisExtCircIndex , INT4 i4FsIsisExtCircLevelIndex , INT4 i4TestValFsIsisExtCircLevelAuthType)
{

	INT1                i1ErrCode = SNMP_SUCCESS;
	INT4                i4RetVal = ISIS_SUCCESS;
	UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
	UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
	UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
	tIsisCktEntry      *pCktEntry = NULL;
	tIsisSysContext    *pContext = NULL;

	NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
				"nmhTestv2FsIsisExtCircLevelAuthType () \n"));

#ifdef ISIS_FT_ENABLED
	if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
	{
		return SNMP_FAILURE;
	}
#endif
	if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
	{
		i1ErrCode = SNMP_FAILURE;
		NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
					"exist for the Index, %d\n", i4FsIsisExtSysInstance));
		CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
	}
	else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
					&pCktEntry)) != ISIS_SUCCESS)
	{
		i1ErrCode = SNMP_FAILURE;
		NMP_PT ((ISIS_LGST, "NMP <E> : Circ does not "
					"exist for the Index, %d\n", i4FsIsisExtCircIndex));
		CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
	}
	else
	{
		if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
		{
			i1ErrCode = SNMP_FAILURE;
			NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
						"exist for the Index,  %d\n", i4FsIsisExtCircLevelIndex));
			CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
		}
		else
		{
			if ((i4TestValFsIsisExtCircLevelAuthType != ISIS_AUTH_PASSWD) &&
					(i4TestValFsIsisExtCircLevelAuthType != ISIS_AUTH_MD5))
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				i1ErrCode = SNMP_FAILURE;
			}

			NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
						"nmhTestv2FsIsisExtCircLevelAuthType () \n"));
		}
	}
	return i1ErrCode;


}




/* LOW LEVEL Routines for Table : FsIsisExtIPRATable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtIPRATable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtIPRATable (INT4 i4FsIsisExtSysInstance,
                                            INT4 i4FsIsisExtIPRAType,
                                            INT4 i4FsIsisExtIPRAIndex)
{
    return (nmhValidateIndexInstanceIsisIPRATable (i4FsIsisExtSysInstance,
                                                   i4FsIsisExtIPRAType,
                                                   i4FsIsisExtIPRAIndex));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtIPRATable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtIPRATable (INT4 *pi4FsIsisExtSysInstance,
                                    INT4 *pi4FsIsisExtIPRAType,
                                    INT4 *pi4FsIsisExtIPRAIndex)
{
    return (nmhGetFirstIndexIsisIPRATable (pi4FsIsisExtSysInstance,
                                           pi4FsIsisExtIPRAType,
                                           pi4FsIsisExtIPRAIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtIPRATable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtIPRAType
                nextFsIsisExtIPRAType
                FsIsisExtIPRAIndex
                nextFsIsisExtIPRAIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtIPRATable (INT4 i4FsIsisExtSysInstance,
                                   INT4 *pi4NextFsIsisExtSysInstance,
                                   INT4 i4FsIsisExtIPRAType,
                                   INT4 *pi4NextFsIsisExtIPRAType,
                                   INT4 i4FsIsisExtIPRAIndex,
                                   INT4 *pi4NextFsIsisExtIPRAIndex)
{
    return (nmhGetNextIndexIsisIPRATable (i4FsIsisExtSysInstance,
                                          pi4NextFsIsisExtSysInstance,
                                          i4FsIsisExtIPRAType,
                                          pi4NextFsIsisExtIPRAType,
                                          i4FsIsisExtIPRAIndex,
                                          pi4NextFsIsisExtIPRAIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRADelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                retValFsIsisExtIPRADelayMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPRADelayMetric (INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtIPRAType,
                                INT4 i4FsIsisExtIPRAIndex,
                                INT4 *pi4RetValFsIsisExtIPRADelayMetric)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisIPRADelMetric ()\n"));

    if ((i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                           &pContext)) == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            if ((pContext->SysActuals.u1SysMetricSupp & ISIS_DEL_MET_SUPP_FLAG))
            {
                u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);
                *pi4RetValFsIsisExtIPRADelayMetric =
                    (INT4) (pIPRARec->Metric[u1MetIdx]);
            }
            else
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Delay Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisIPRADelMetric ()\n"));
    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRAErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                retValFsIsisExtIPRAErrorMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPRAErrorMetric (INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtIPRAType,
                                INT4 i4FsIsisExtIPRAIndex,
                                INT4 *pi4RetValFsIsisExtIPRAErrorMetric)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtIPRAErrorMetric ()\n"));

    if ((i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                           &pContext)) == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);
        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            if ((pContext->SysActuals.u1SysMetricSupp & ISIS_ERR_MET_SUPP_FLAG))
            {
                u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);
                *pi4RetValFsIsisExtIPRAErrorMetric =
                    (INT4) (pIPRARec->Metric[u1MetIdx]);
            }
            else
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Error Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtIPRAErrorMetric ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRAExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                retValFsIsisExtIPRAExpenseMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPRAExpenseMetric (INT4 i4FsIsisExtSysInstance,
                                  INT4 i4FsIsisExtIPRAType,
                                  INT4 i4FsIsisExtIPRAIndex,
                                  INT4 *pi4RetValFsIsisExtIPRAExpenseMetric)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtIPRAExpenseMetric ()\n"));

    if ((i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                           &pContext)) == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);
        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            if ((pContext->SysActuals.u1SysMetricSupp & ISIS_EXP_MET_SUPP_FLAG))
            {
                u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);
                *pi4RetValFsIsisExtIPRAExpenseMetric =
                    (INT4) (pIPRARec->Metric[u1MetIdx]);
            }
            else
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Exp Metric is not supported"
                         "for the Current Configuration!!!\n"));
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtIPRAExpenseMetric ()\n"));
    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRADelayMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                retValFsIsisExtIPRADelayMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPRADelayMetricType (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtIPRAType,
                                    INT4 i4FsIsisExtIPRAIndex,
                                    INT4 *pi4RetValFsIsisExtIPRADelayMetricType)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtIPRAExpenseMetric ()\n"));

    if ((i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                           &pContext)) == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);
        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);
            if (pIPRARec->Metric[u1MetIdx] & ISIS_METRIC_TYPE_MASK)
            {
                *pi4RetValFsIsisExtIPRADelayMetricType = ISIS_EXTERNAL;
            }
            else
            {
                *pi4RetValFsIsisExtIPRADelayMetricType = ISIS_INTERNAL;
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtIPRAExpenseMetric ()\n"));
    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRAErrorMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                retValFsIsisExtIPRAErrorMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPRAErrorMetricType (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtIPRAType,
                                    INT4 i4FsIsisExtIPRAIndex,
                                    INT4 *pi4RetValFsIsisExtIPRAErrorMetricType)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtIPRAExpenseMetric ()\n"));

    if ((i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                           &pContext)) == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);

            if (pIPRARec->Metric[u1MetIdx] & ISIS_METRIC_TYPE_MASK)
            {
                *pi4RetValFsIsisExtIPRAErrorMetricType = ISIS_EXTERNAL;
            }
            else
            {
                *pi4RetValFsIsisExtIPRAErrorMetricType = ISIS_INTERNAL;
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtIPRAExpenseMetric ()\n"));
    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRAExpenseMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                retValFsIsisExtIPRAExpenseMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPRAExpenseMetricType (INT4 i4FsIsisExtSysInstance,
                                      INT4 i4FsIsisExtIPRAType,
                                      INT4 i4FsIsisExtIPRAIndex,
                                      INT4
                                      *pi4RetValFsIsisExtIPRAExpenseMetricType)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtIPRAExpenseMetricType ()\n"));

    if ((i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                           &pContext)) == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);
        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);

            if (pIPRARec->Metric[u1MetIdx] & ISIS_METRIC_TYPE_MASK)
            {
                *pi4RetValFsIsisExtIPRAExpenseMetricType = ISIS_EXTERNAL;
            }
            else
            {
                *pi4RetValFsIsisExtIPRAExpenseMetricType = ISIS_INTERNAL;
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }

    NMP_EE
        ((ISIS_LGST,
          "NMP <X> : Exiting nmhGetFsIsisExtIPRAExpenseMetricType ()\n"));
    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRANextHopType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object
                retValFsIsisExtIPRANextHopType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPRANextHopType (INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtIPRAType,
                                INT4 i4FsIsisExtIPRAIndex,
                                INT4 *pi4RetValFsIsisExtIPRANextHopType)
{

    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entering nmhGetFsIsisExtIPRANextHopType ()\n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                               &pContext) == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));
        return SNMP_FAILURE;
    }

    /* Get the matching IPRA entry */

    if (IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                            (UINT1) i4FsIsisExtIPRAType, &pIPRARec)
        == ISIS_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : No Entry Exists for the index \nSysInstance :"
                 "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                 i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                 i4FsIsisExtIPRAIndex));
        return SNMP_FAILURE;
    }
    *pi4RetValFsIsisExtIPRANextHopType = (INT4) (pIPRARec->u1IPRANextHopType);

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtIPRANextHopType ()\n"));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRANextHop
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                retValFsIsisExtIPRANextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPRANextHop (INT4 i4FsIsisExtSysInstance,
                            INT4 i4FsIsisExtIPRAType, INT4 i4FsIsisExtIPRAIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsIsisExtIPRANextHop)
{
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entering nmhGetFsIsisExtIPRANextHop ()\n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                               &pContext) == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));
        return SNMP_FAILURE;
    }

    /* Get the matching IPRA entry */

    if (IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                            (UINT1) i4FsIsisExtIPRAType, &pIPRARec)
        == ISIS_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : No Entry Exists for the index \nSysInstance :"
                 "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                 i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                 i4FsIsisExtIPRAIndex));
        return SNMP_FAILURE;
    }
    if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
    {
        MEMCPY (pRetValFsIsisExtIPRANextHop->pu1_OctetList,
                pIPRARec->au1IPRANextHop, ISIS_MAX_IPV4_ADDR_LEN);
        pRetValFsIsisExtIPRANextHop->i4_Length = ISIS_MAX_IPV4_ADDR_LEN;
    }
    else
    {
        MEMCPY (pRetValFsIsisExtIPRANextHop->pu1_OctetList,
                pIPRARec->au1IPRANextHop, ISIS_MAX_IPV6_ADDR_LEN);
        pRetValFsIsisExtIPRANextHop->i4_Length = ISIS_MAX_IPV6_ADDR_LEN;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtIPRANextHop ()\n"));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPRAFullMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object
                retValFsIsisExtIPRAFullMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisExtIPRAFullMetric (INT4  i4FsIsisExtSysInstance, 
                                    INT4  i4FsIsisExtIPRAType, 
                                    INT4  i4FsIsisExtIPRAIndex, 
                                    UINT4 *pu4RetValFsIsisExtIPRAFullMetric)
{

    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtIPRAFullMetric ()\n"));

    if ((i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                           &pContext)) == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);
        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                *pu4RetValFsIsisExtIPRAFullMetric = pIPRARec->u4FullMetric;
            }
            else
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Full Metric is not supported"
                         "for the Current Configuration!!!\n"));
				*pu4RetValFsIsisExtIPRAFullMetric = 0;
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtIPRAFullMetric ()\n"));
    return (i1RetCode);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRADelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                setValFsIsisExtIPRADelayMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPRADelayMetric (INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtIPRAType,
                                INT4 i4FsIsisExtIPRAIndex,
                                INT4 i4SetValFsIsisExtIPRADelayMetric)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisExtIPRADelayMetric ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                      &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                       (UINT1) i4FsIsisExtIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);
            pIPRARec->Metric[u1MetIdx]
                = (UINT1) i4SetValFsIsisExtIPRADelayMetric;
            pIPRARec->Metric[u1MetIdx] &= ~ISIS_METRIC_SUPP_MASK;

            pContext->SysActuals.u1SysMetricSupp =
                pContext->SysActuals.u1SysMetricSupp | ISIS_DEL_MET_SUPP_FLAG;

            if (((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4FsIsisExtIPRAType;
                    pIPRAEvt->u1MetricType = ISIS_DEL_MET;
                    MEMCPY (pIPRAEvt->Metric,
                            pIPRARec->Metric, ISIS_NUM_METRICS);
                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : System Context doesn't exist\n"));
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisExtIPRADelayMetric ()\n"));
    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRAErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                setValFsIsisExtIPRAErrorMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPRAErrorMetric (INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtIPRAType,
                                INT4 i4FsIsisExtIPRAIndex,
                                INT4 i4SetValFsIsisExtIPRAErrorMetric)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisExtIPRAErrorMetric ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                      &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);
        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);
            pIPRARec->Metric[u1MetIdx]
                = (UINT1) i4SetValFsIsisExtIPRAErrorMetric;
            pIPRARec->Metric[u1MetIdx] &= ~ISIS_METRIC_SUPP_MASK;
            i1RetCode = SNMP_SUCCESS;

            pContext->SysActuals.u1SysMetricSupp =
                pContext->SysActuals.u1SysMetricSupp | ISIS_ERR_MET_SUPP_FLAG;

            if (((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4FsIsisExtIPRAType;
                    pIPRAEvt->u1MetricType = ISIS_ERR_MET;
                    MEMCPY (pIPRAEvt->Metric,
                            pIPRARec->Metric, ISIS_NUM_METRICS);
                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
        }
    }
    else
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : System Context doesn't exist\n"));
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisExtIPRAErrorMetric ()\n"));
    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRAExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                setValFsIsisExtIPRAExpenseMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPRAExpenseMetric (INT4 i4FsIsisExtSysInstance,
                                  INT4 i4FsIsisExtIPRAType,
                                  INT4 i4FsIsisExtIPRAIndex,
                                  INT4 i4SetValFsIsisExtIPRAExpenseMetric)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisExtIPRAExpenseMetric ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                      &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                       (UINT1) i4FsIsisExtIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);

            pIPRARec->Metric[u1MetIdx]
                = (UINT1) i4SetValFsIsisExtIPRAExpenseMetric;
            pIPRARec->Metric[u1MetIdx] &= ~ISIS_METRIC_SUPP_MASK;
            i1RetCode = SNMP_SUCCESS;

            pContext->SysActuals.u1SysMetricSupp =
                pContext->SysActuals.u1SysMetricSupp | ISIS_EXP_MET_SUPP_FLAG;

            if (((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4FsIsisExtIPRAType;
                    pIPRAEvt->u1MetricType = ISIS_EXP_MET;
                    MEMCPY (pIPRAEvt->Metric,
                            pIPRARec->Metric, ISIS_NUM_METRICS);

                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
        }
    }
    else
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : System Context doesn't exist\n"));
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisExtIPRAExpenseMetric ()\n"));

    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRADelayMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                setValFsIsisExtIPRADelayMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPRADelayMetricType (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtIPRAType,
                                    INT4 i4FsIsisExtIPRAIndex,
                                    INT4 i4SetValFsIsisExtIPRADelayMetricType)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisIPRADelayMetricType\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                      &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_DEL_MET);

            if (i4SetValFsIsisExtIPRADelayMetricType == ISIS_EXTERNAL)
            {
                pIPRARec->Metric[u1MetIdx] |= ISIS_METRIC_TYPE_MASK;
            }
            else if (i4SetValFsIsisExtIPRADelayMetricType == ISIS_INTERNAL)
            {
                pIPRARec->Metric[u1MetIdx] &= ~ISIS_METRIC_TYPE_MASK;
            }
            i1RetCode = SNMP_SUCCESS;

            if (((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4FsIsisExtIPRAType;
                    pIPRAEvt->u1MetricType = ISIS_EXP_MET;
                    MEMCPY (pIPRAEvt->Metric,
                            pIPRARec->Metric, ISIS_NUM_METRICS);

                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
        }
    }
    else
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : System Context doesn't exist\n"));
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisIPRADelayMetricType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRAErrorMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                setValFsIsisExtIPRAErrorMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPRAErrorMetricType (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtIPRAType,
                                    INT4 i4FsIsisExtIPRAIndex,
                                    INT4 i4SetValFsIsisExtIPRAErrorMetricType)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisExtIPRAErrorMetricType ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                      &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_ERR_MET);

            if (i4SetValFsIsisExtIPRAErrorMetricType == ISIS_EXTERNAL)
            {
                pIPRARec->Metric[u1MetIdx] |= ISIS_METRIC_TYPE_MASK;
            }
            else if (i4SetValFsIsisExtIPRAErrorMetricType == ISIS_INTERNAL)
            {
                pIPRARec->Metric[u1MetIdx] &= ~ISIS_METRIC_TYPE_MASK;
            }
            i1RetCode = SNMP_SUCCESS;

            if (((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4FsIsisExtIPRAType;
                    pIPRAEvt->u1MetricType = ISIS_EXP_MET;
                    MEMCPY (pIPRAEvt->Metric,
                            pIPRARec->Metric, ISIS_NUM_METRICS);

                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
        }
    }
    else
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : System Context doesn't exist\n"));
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisExtIPRAErrorMetricType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRAExpenseMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                setValFsIsisExtIPRAExpenseMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPRAExpenseMetricType (INT4 i4FsIsisExtSysInstance,
                                      INT4 i4FsIsisExtIPRAType,
                                      INT4 i4FsIsisExtIPRAIndex,
                                      INT4
                                      i4SetValFsIsisExtIPRAExpenseMetricType)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1MetIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisExtIPRAExpenseMetricType ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                      &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal =
            IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                (UINT1) i4FsIsisExtIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            u1MetIdx = IsisUtlGetMetIndex (pContext, ISIS_EXP_MET);

            if (i4SetValFsIsisExtIPRAExpenseMetricType == ISIS_EXTERNAL)
            {
                pIPRARec->Metric[u1MetIdx] |= ISIS_METRIC_TYPE_MASK;
            }
            else if (i4SetValFsIsisExtIPRAExpenseMetricType == ISIS_INTERNAL)
            {
                pIPRARec->Metric[u1MetIdx] &= ~ISIS_METRIC_TYPE_MASK;
            }
            i1RetCode = SNMP_SUCCESS;

            if (((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4FsIsisExtIPRAType;
                    pIPRAEvt->u1MetricType = ISIS_EXP_MET;
                    MEMCPY (pIPRAEvt->Metric,
                            pIPRARec->Metric, ISIS_NUM_METRICS);

                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
        }
    }
    else
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : System Context doesn't exist\n"));
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisExtIPRAExpenseMetricType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRANextHopType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object
                setValFsIsisExtIPRANextHopType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPRANextHopType (INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtIPRAType,
                                INT4 i4FsIsisExtIPRAIndex,
                                INT4 i4SetValFsIsisExtIPRANextHopType)
{
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisExtIPRANextHopType()\n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));
        return SNMP_FAILURE;
    }

    /* Get the matching IP RA entry */

    if (IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                            (UINT1) i4FsIsisExtIPRAType, &pIPRARec)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : IPRAEntry not existed for the indices "
                 "sysInstance : %d\n IPRAType : %d\n"
                 "IPRAIndex :%d\n", i4FsIsisExtSysInstance,
                 i4FsIsisExtIPRAType, i4FsIsisExtIPRAIndex));
        return SNMP_FAILURE;
    }

    if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Cannot set the variable as Exist"
                 "State is active\n"));
        return SNMP_FAILURE;
    }
    pIPRARec->u1IPRANextHopType = (UINT1) i4SetValFsIsisExtIPRANextHopType;
    pIPRARec->u1QFlag |= ISIS_IPRANEXTHOP_FLAG;

    if (pIPRARec->u1QFlag == ISIS_IPRAALLSET_FLAG)
    {
        pIPRARec->u1IPRAExistState = ISIS_NOT_IN_SER;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisExtIPRANextHopType ()\n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRANextHop
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                setValFsIsisExtIPRANextHop
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPRANextHop (INT4 i4FsIsisExtSysInstance,
                            INT4 i4FsIsisExtIPRAType, INT4 i4FsIsisExtIPRAIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsIsisExtIPRANextHop)
{
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisExtIPRANextHop()\n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));
        return SNMP_FAILURE;
    }

    /* Get the matching IP RA entry */

    if (IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                            (UINT1) i4FsIsisExtIPRAType, &pIPRARec)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : IPRAEntry not existed for the indices "
                 "sysInstance : %d\n IPRAType : %d\n"
                 "IPRAIndex :%d\n", i4FsIsisExtSysInstance,
                 i4FsIsisExtIPRAType, i4FsIsisExtIPRAIndex));
        return SNMP_FAILURE;
    }

    if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Cannot set the variable as Exist"
                 "State is active\n"));
        return SNMP_FAILURE;
    }

    pIPRARec->u1QFlag |= ISIS_IPRANEXTHOP_FLAG;

    if (pIPRARec->u1QFlag == ISIS_IPRAALLSET_FLAG)
    {
        pIPRARec->u1IPRAExistState = ISIS_NOT_IN_SER;
    }
    if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
    {
        MEMCPY (pIPRARec->au1IPRANextHop,
                pSetValFsIsisExtIPRANextHop->pu1_OctetList,
                ISIS_MAX_IPV4_ADDR_LEN);
        pSetValFsIsisExtIPRANextHop->i4_Length = ISIS_MAX_IPV4_ADDR_LEN;
    }
    else
    {
        MEMCPY (pIPRARec->au1IPRANextHop,
                pSetValFsIsisExtIPRANextHop->pu1_OctetList,
                ISIS_MAX_IPV6_ADDR_LEN);
        pSetValFsIsisExtIPRANextHop->i4_Length = ISIS_MAX_IPV6_ADDR_LEN;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtIPRANextHop ()\n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPRAFullMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object
                setValFsIsisExtIPRAFullMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.                                                                        Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsIsisExtIPRAFullMetric(INT4 i4FsIsisExtSysInstance,
                                   INT4 i4FsIsisExtIPRAType, 
                                   INT4 i4FsIsisExtIPRAIndex, 
                                   UINT4 u4SetValFsIsisExtIPRAFullMetric)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisExtIPRAFullMetric ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                                      &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4FsIsisExtIPRAIndex,
                                       (UINT1) i4FsIsisExtIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : No Entry Exists for the index \nSysInstance :"
                     "%d\n IPRAType : %d\nIPRAIndex : %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtIPRAType,
                     i4FsIsisExtIPRAIndex));
        }
        else
        {
            pIPRARec->u4FullMetric
                = u4SetValFsIsisExtIPRAFullMetric;
            i1RetCode = SNMP_SUCCESS;

            if (((pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));
                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4FsIsisExtIPRAType;
                    pIPRAEvt->u1MetricType = ISIS_DEF_MET;
                    pIPRAEvt->u4FullMetric = pIPRARec->u4FullMetric;
                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
        }
    }
    else
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : System Context doesn't exist\n"));
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisExtIPRAFullMetric ()\n"));

    return (i1RetCode);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRADelayMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                testValFsIsisExtIPRADelayMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPRADelayMetric (UINT4 *pu4ErrorCode,
                                   INT4 i4FsIsisExtSysInstance,
                                   INT4 i4FsIsisExtIPRAType,
                                   INT4 i4FsIsisExtIPRAIndex,
                                   INT4 i4TestValFsIsisExtIPRADelayMetric)
{
	UNUSED_PARAM (i4FsIsisExtSysInstance);
	UNUSED_PARAM (i4FsIsisExtIPRAType);
	UNUSED_PARAM (i4FsIsisExtIPRAIndex);
	UNUSED_PARAM (i4TestValFsIsisExtIPRADelayMetric);
	*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRAErrorMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                testValFsIsisExtIPRAErrorMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPRAErrorMetric (UINT4 *pu4ErrorCode,
                                   INT4 i4FsIsisExtSysInstance,
                                   INT4 i4FsIsisExtIPRAType,
                                   INT4 i4FsIsisExtIPRAIndex,
                                   INT4 i4TestValFsIsisExtIPRAErrorMetric)
{
	UNUSED_PARAM (i4FsIsisExtSysInstance);
	UNUSED_PARAM (i4FsIsisExtIPRAType);
	UNUSED_PARAM (i4FsIsisExtIPRAIndex);
	UNUSED_PARAM (i4TestValFsIsisExtIPRAErrorMetric);
	*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRAExpenseMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                testValFsIsisExtIPRAExpenseMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPRAExpenseMetric (UINT4 *pu4ErrorCode,
                                     INT4 i4FsIsisExtSysInstance,
                                     INT4 i4FsIsisExtIPRAType,
                                     INT4 i4FsIsisExtIPRAIndex,
                                     INT4 i4TestValFsIsisExtIPRAExpenseMetric)
{
	UNUSED_PARAM (i4FsIsisExtSysInstance);
	UNUSED_PARAM (i4FsIsisExtIPRAType);
	UNUSED_PARAM (i4FsIsisExtIPRAIndex);
	UNUSED_PARAM (i4TestValFsIsisExtIPRAExpenseMetric);
	*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRADelayMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                testValFsIsisExtIPRADelayMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPRADelayMetricType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsIsisExtSysInstance,
                                       INT4 i4FsIsisExtIPRAType,
                                       INT4 i4FsIsisExtIPRAIndex,
                                       INT4
                                       i4TestValFsIsisExtIPRADelayMetricType)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered function nmhTestv2IsisIPRA"
             "DelayMetricType ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                                     i4FsIsisExtIPRAType, i4FsIsisExtIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtIPRADelayMetricType ()\n"));
        return (i1RetCode);
    }

    if ((i4TestValFsIsisExtIPRADelayMetricType != ISIS_INTERNAL)
        && (i4TestValFsIsisExtIPRADelayMetricType != ISIS_EXTERNAL))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid IPRA DelayMetricType: %d\n",
                 i4TestValFsIsisExtIPRADelayMetricType));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisIPRADelayMetricType ()\n"));
    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRAErrorMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                testValFsIsisExtIPRAErrorMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPRAErrorMetricType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsIsisExtSysInstance,
                                       INT4 i4FsIsisExtIPRAType,
                                       INT4 i4FsIsisExtIPRAIndex,
                                       INT4
                                       i4TestValFsIsisExtIPRAErrorMetricType)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisIPRAErrorMetricType ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                                     i4FsIsisExtIPRAType, i4FsIsisExtIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtIPRAErrorMetricType ()\n"));
        return (i1RetCode);
    }

    if ((i4TestValFsIsisExtIPRAErrorMetricType != ISIS_INTERNAL)
        && (i4TestValFsIsisExtIPRAErrorMetricType != ISIS_EXTERNAL))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid IPRA ErrorMetricType: %d\n",
                 i4TestValFsIsisExtIPRAErrorMetricType));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisIPRAErrorMetricType ()\n"));
    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRAExpenseMetricType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                testValFsIsisExtIPRAExpenseMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPRAExpenseMetricType (UINT4 *pu4ErrorCode,
                                         INT4 i4FsIsisExtSysInstance,
                                         INT4 i4FsIsisExtIPRAType,
                                         INT4 i4FsIsisExtIPRAIndex,
                                         INT4
                                         i4TestValFsIsisExtIPRAExpenseMetricType)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered function "
             "nmhTestv2IsisIPRAExpenseMetricType ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                                     i4FsIsisExtIPRAType, i4FsIsisExtIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtIPRAExpenseMetricType ()\n"));
        return (i1RetCode);
    }

    if ((i4TestValFsIsisExtIPRAExpenseMetricType != ISIS_INTERNAL)
        && (i4TestValFsIsisExtIPRAExpenseMetricType != ISIS_EXTERNAL))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid IPRA ExpenseMetricType: %d\n",
                 i4TestValFsIsisExtIPRAExpenseMetricType));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisIPRAExpenseMetricType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRANextHopType
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object
                testValFsIsisExtIPRANextHopType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPRANextHopType (UINT4 *pu4ErrorCode,
                                   INT4 i4FsIsisExtSysInstance,
                                   INT4 i4FsIsisExtIPRAType,
                                   INT4 i4FsIsisExtIPRAIndex,
                                   INT4 i4TestValFsIsisExtIPRANextHopType)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisExtIPRANextHopType ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                                     i4FsIsisExtIPRAType, i4FsIsisExtIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtIPRANextHopType ()\n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisExtIPRANextHopType != ISIS_IPV4)
        && (i4TestValFsIsisExtIPRANextHopType != ISIS_IPV6))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid i4TestValFsIsisExtIPRANextHopType : %d\n",
                 i4TestValFsIsisExtIPRANextHopType));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;    /*silvercreek jul22 */
        i1RetCode = SNMP_FAILURE;

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2FsIsisExtIPRANextHopType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRANextHop
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object 
                testValFsIsisExtIPRANextHop
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPRANextHop (UINT4 *pu4ErrorCode, INT4 i4FsIsisExtSysInstance,
                               INT4 i4FsIsisExtIPRAType,
                               INT4 i4FsIsisExtIPRAIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsIsisExtIPRANextHop)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    UINT4               u4NextHop = 0;
    UINT4               u4IfIndex = 0;
    tIp6Addr            IPAddr;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisExtIPRANextHop ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                                     i4FsIsisExtIPRAType, i4FsIsisExtIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtIPRANextHop ()\n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return SNMP_FAILURE;
    }
    if ((pTestValFsIsisExtIPRANextHop->i4_Length != ISIS_MAX_IPV4_ADDR_LEN) &&
        (pTestValFsIsisExtIPRANextHop->i4_Length != ISIS_MAX_IPV6_ADDR_LEN))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid pTestValFsIsisExtIPRANextHop : %d\n",
                 pTestValFsIsisExtIPRANextHop->i4_Length));

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;    /*silvercreek jul22 */
        CLI_SET_ERR (CLI_ISIS_INV_LEN);
        return SNMP_FAILURE;
    }
    /* Next hop IP should not be same as our own IP */
    if (pTestValFsIsisExtIPRANextHop->i4_Length == ISIS_MAX_IPV4_ADDR_LEN)
    {
        MEMCPY ((UINT1 *) &u4NextHop,
                pTestValFsIsisExtIPRANextHop->pu1_OctetList,
                pTestValFsIsisExtIPRANextHop->i4_Length);
        u4NextHop = OSIX_NTOHL (u4NextHop);
        if (NetIpv4IfIsOurAddress (u4NextHop) == NETIPV4_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        MEMCPY (&IPAddr.u1_addr[0], pTestValFsIsisExtIPRANextHop->pu1_OctetList,
                pTestValFsIsisExtIPRANextHop->i4_Length);
        if (NetIpv6IsOurAddress (&IPAddr, &u4IfIndex) == NETIPV6_SUCCESS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisExtIPRANextHop()\n"));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPRAFullMetric
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex

                The Object
                testValFsIsisExtIPRAFullMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsIsisExtIPRAFullMetric(UINT4 *pu4ErrorCode,
                                      INT4 i4FsIsisExtSysInstance, 
                                      INT4 i4FsIsisExtIPRAType, 
                                      INT4 i4FsIsisExtIPRAIndex, 
                                      UINT4 u4TestValFsIsisExtIPRAFullMetric)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisIPRAFullMetric ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4FsIsisExtSysInstance,
                                     i4FsIsisExtIPRAType, i4FsIsisExtIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisExtIPRAFullMetric ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return (i1RetCode);
    }

    /* Full metric is not supported for Single-topology ISIS. Hence
     * verifying if MT support is enabled or not*/
    if ((IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext) ==
        ISIS_SUCCESS) && (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Full metric not supported "
                "for single-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_INV_DEF_MET);
        return SNMP_FAILURE;
    }

    if ((u4TestValFsIsisExtIPRAFullMetric < ISIS_MIN_METRIC_MT) ||
        (u4TestValFsIsisExtIPRAFullMetric > ISIS_MAX_METRIC_MT))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid i4TestValFsIsisExtIPRAFullMetric: "
                 "%d\n", u4TestValFsIsisExtIPRAFullMetric));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;    /*silvercreek jul22 */
        i1RetCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_DEF_MET);
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisIPRAFullMetric ()\n"));

    return (i1RetCode);
}

/* LOW LEVEL Routines for Table : FsIsisExtCircLevelRxPasswordTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtCircLevelRxPasswordTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
                FsIsisExtCircLevelRxPassword
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtCircLevelRxPasswordTable (INT4
                                                           i4FsIsisExtSysInstance,
                                                           INT4
                                                           i4FsIsisExtCircIndex,
                                                           INT4
                                                           i4FsIsisExtCircLevelIndex,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pFsIsisExtCircLevelRxPassword)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtCircLevel"
             "RxPasswordTable () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext) !=
        ISIS_SUCCESS)
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
    }

    else if ((i4RetVal =
              IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                                &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index, %d\n", i4FsIsisExtCircIndex));
        i1ErrCode = SNMP_FAILURE;
    }

    else if ((i4FsIsisExtCircLevelIndex != ISIS_LEVEL1)
             && (i4FsIsisExtCircLevelIndex != ISIS_LEVEL2))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                 "exist for the Index,  %d\n", i4FsIsisExtCircLevelIndex));
        i1ErrCode = SNMP_FAILURE;
    }

    else if (pFsIsisExtCircLevelRxPassword->i4_Length <= 0)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of Password Length !! \n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtCircLevel"
             "RxPasswordTable ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtCircLevelRxPasswordTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
                FsIsisExtCircLevelRxPassword
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtCircLevelRxPasswordTable (INT4
                                                   *pi4FsIsisExtSysInstance,
                                                   INT4 *pi4FsIsisExtCircIndex,
                                                   INT4
                                                   *pi4FsIsisExtCircLevelIndex,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsIsisExtCircLevelRxPassword)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT1               au1OctetList[ISIS_MAX_PASSWORD_LEN];
    tSNMP_OCTET_STRING_TYPE RxPasswd;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtCircLevelRxPasswordTable () \n"));

    MEMSET (au1OctetList, 0x00, ISIS_MAX_PASSWORD_LEN);

    RxPasswd.pu1_OctetList = au1OctetList;
    RxPasswd.i4_Length = ISIS_MAX_PASSWORD_LEN;

    i1ErrCode =
        nmhGetNextIndexFsIsisExtCircLevelRxPasswordTable (0,
                                                          pi4FsIsisExtSysInstance,
                                                          0,
                                                          pi4FsIsisExtCircIndex,
                                                          0,
                                                          pi4FsIsisExtCircLevelIndex,
                                                          &RxPasswd,
                                                          pFsIsisExtCircLevelRxPassword);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtCircLevelRxPasswordTable ()  \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtCircLevelRxPasswordTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtCircIndex
                nextFsIsisExtCircIndex
                FsIsisExtCircLevelIndex
                nextFsIsisExtCircLevelIndex
                FsIsisExtCircLevelRxPassword
                nextFsIsisExtCircLevelRxPassword
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtCircLevelRxPasswordTable (INT4 i4FsIsisExtSysInstance,
                                                  INT4
                                                  *pi4NextFsIsisExtSysInstance,
                                                  INT4 i4FsIsisExtCircIndex,
                                                  INT4
                                                  *pi4NextFsIsisExtCircIndex,
                                                  INT4
                                                  i4FsIsisExtCircLevelIndex,
                                                  INT4
                                                  *pi4NextFsIsisExtCircLevelIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsIsisExtCircLevelRxPassword,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pNextFsIsisExtCircLevelRxPassword)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    UINT4               u4NextInstIdx = 0;
    UINT4               u4NextCircIdx = 0;
    UINT1               u1NextCircLvlIdx = 0;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexFsIsisExtCircLevelRxPasswordTable () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                           &pContext) == ISIS_FAILURE))
    {
        if ((i4RetVal = nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                                        &u4NextInstIdx) ==
             ISIS_FAILURE))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                     "Index with the given Indices \n"));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            u4CktIdx = 0;
            u1CktLvlIdx = 0;
            u4InstIdx = u4NextInstIdx;
            if (IsisCtrlGetSysContext (u4InstIdx, &pContext)
                 == ISIS_FAILURE)
            {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                                         "the corresponding index \n"));
            }
            MEMSET (pFsIsisExtCircLevelRxPassword->pu1_OctetList, 0,
                    pFsIsisExtCircLevelRxPassword->i4_Length);
        }

    }
    while (pContext != NULL)
    {
        if (nmhUtlGetNextCktLvlRxPasswd (pContext, u4CktIdx, &u1CktLvlIdx,
                                         pFsIsisExtCircLevelRxPassword,
                                         pNextFsIsisExtCircLevelRxPassword) ==
            ISIS_FAILURE)
        {
            if ((i4RetVal =
                 nmhUtlGetNextIndexIsisCircLevelTable (u4InstIdx,
                                                       u4CktIdx, u1CktLvlIdx,
                                                       &u1NextCircLvlIdx)
                 == ISIS_FAILURE))
            {
                if ((i4RetVal =
                     nmhUtlGetNextIndexIsisCircTable (u4InstIdx,
                                                      u4CktIdx,
                                                      &u4NextCircIdx)
                     == ISIS_FAILURE))
                {

                    if ((i4RetVal =
                         nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                                         &u4NextInstIdx)
                         == ISIS_FAILURE))
                    {
                        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                                 "Index with the given Indices \n"));

                        i1ErrCode = SNMP_FAILURE;
                        break;
                    }
                    else
                    {
                        u4CktIdx = 0;
                        u1CktLvlIdx = 0;
                        u4InstIdx = u4NextInstIdx;
			if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_FAILURE)
                        {
                            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                                                "the corresponding index \n"));
                        }
                        MEMSET (pFsIsisExtCircLevelRxPassword->pu1_OctetList, 0,
                                pFsIsisExtCircLevelRxPassword->i4_Length);
                        continue;
                    }
                }
                else
                {
                    u4CktIdx = u4NextCircIdx;
                    u1CktLvlIdx = 0;
                    MEMSET (pFsIsisExtCircLevelRxPassword->pu1_OctetList, 0,
                            pFsIsisExtCircLevelRxPassword->i4_Length);
                }
            }
            else
            {
                u1CktLvlIdx = u1NextCircLvlIdx;
                MEMSET (pFsIsisExtCircLevelRxPassword->pu1_OctetList, 0,
                        pFsIsisExtCircLevelRxPassword->i4_Length);
            }
        }
        else
        {
            *pi4NextFsIsisExtSysInstance = u4InstIdx;
            *pi4NextFsIsisExtCircIndex = u4CktIdx;
            *pi4NextFsIsisExtCircLevelIndex = u1CktLvlIdx;
            i1ErrCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtCircLevelRxPasswordTable  \n"));

    return i1ErrCode;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtCircLevelRxPasswordExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
                FsIsisExtCircLevelRxPassword

                The Object 
                retValFsIsisExtCircLevelRxPasswordExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtCircLevelRxPasswordExistState (INT4 i4FsIsisExtSysInstance,
                                              INT4 i4FsIsisExtCircIndex,
                                              INT4 i4FsIsisExtCircLevelIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsIsisExtCircLevelRxPassword,
                                              INT4
                                              *pi4RetValFsIsisExtCircLevelRxPasswordExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    UINT1               u1Idx = 0;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtCircLevelRxPasswordExistState () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %u\n", u4InstIdx));
    }

    else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                           &pCktEntry)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circ does not "
                 "exist for the Index, %u\n", u4CktIdx));
    }
    else
    {
        if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index, %u\n", u1CktLvlIdx));
        }
        else
        {
            pCktLevel = (u1CktLvlIdx == ISIS_LEVEL1) ? (pCktEntry->pL1CktInfo)
                : (pCktEntry->pL2CktInfo);
            for (u1Idx = 0; u1Idx < ISIS_MAX_RX_PASSWDS; u1Idx++)
            {
                /*klocwork */
                if (pFsIsisExtCircLevelRxPassword->i4_Length <=
                    ISIS_MAX_PASSWORD_LEN)
                {
                    if ((pCktLevel->aCktRxPasswd[u1Idx].u1Len ==
                         pFsIsisExtCircLevelRxPassword->i4_Length)
                        && (MEMCMP (pCktLevel->aCktRxPasswd[u1Idx].au1Password,
                                    pFsIsisExtCircLevelRxPassword->
                                    pu1_OctetList,
                                    pFsIsisExtCircLevelRxPassword->i4_Length) ==
                            0))
                    {
                        *pi4RetValFsIsisExtCircLevelRxPasswordExistState
                            = pCktLevel->aCktRxPasswd[u1Idx].u1ExistState;
                        i1ErrCode = SNMP_SUCCESS;
                        break;
                    }
                }
            }
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisExtCircLevelRxPasswordExistState ()  \n"));
    return i1ErrCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtCircLevelRxPasswordExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
                FsIsisExtCircLevelRxPassword

                The Object 
                setValFsIsisExtCircLevelRxPasswordExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtCircLevelRxPasswordExistState (INT4 i4FsIsisExtSysInstance,
                                              INT4 i4FsIsisExtCircIndex,
                                              INT4 i4FsIsisExtCircLevelIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsIsisExtCircLevelRxPassword,
                                              INT4
                                              i4SetValFsIsisExtCircLevelRxPasswordExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1PasswdIdx = 0;
    UINT1               u1FreeIdx = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4CktIdx = (UINT4) i4FsIsisExtCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4FsIsisExtCircLevelIndex;
    tIsisCktLevel      *pCktLevel = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtCircLevelRxPasswordExistState () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    switch (i4SetValFsIsisExtCircLevelRxPasswordExistState)
    {
        case ISIS_CR_GO:

            if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
            {
                i1ErrCode = SNMP_FAILURE;
                NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                         "exist for the Index, %u\n", u4InstIdx));
            }

            else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                   &pCktEntry)) != ISIS_SUCCESS)
            {
                i1ErrCode = SNMP_FAILURE;
                NMP_PT ((ISIS_LGST, "NMP <E> : Circ does not "
                         "exist for the Index, %u\n", u4CktIdx));
            }

            else
            {
                if ((u1CktLvlIdx != ISIS_LEVEL1)
                    && (u1CktLvlIdx != ISIS_LEVEL2))
                {
                    i1ErrCode = SNMP_FAILURE;
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Circuit Level Entry does not "
                             "exist for the Index, %u\n", u1CktLvlIdx));
                }
                else
                {
                    pCktLevel =
                        (u1CktLvlIdx == ISIS_LEVEL1) ? (pCktEntry->pL1CktInfo)
                        : (pCktEntry->pL2CktInfo);
                    if (pCktLevel == NULL)
                    {
                        i1ErrCode = SNMP_FAILURE;
                        break;
                    }
                    for (u1PasswdIdx = 0; u1PasswdIdx < ISIS_MAX_RX_PASSWDS;
                         u1PasswdIdx++)
                    {
                        /*klocwork */
                        if (pFsIsisExtCircLevelRxPassword->i4_Length <=
                            ISIS_MAX_PASSWORD_LEN)
                        {

                            if ((pFsIsisExtCircLevelRxPassword->i4_Length ==
                                 pCktLevel->aCktRxPasswd[u1PasswdIdx].u1Len)
                                && (MEMCMP (pFsIsisExtCircLevelRxPassword->
                                            pu1_OctetList,
                                            pCktLevel->
                                            aCktRxPasswd[u1PasswdIdx].
                                            au1Password,
                                            pFsIsisExtCircLevelRxPassword->
                                            i4_Length) == 0))
                            {
                                i1ErrCode = SNMP_FAILURE;
                                NMP_PT ((ISIS_LGST,
                                         "NMP <E> : The given Password Entry "
                                         "already exists !! \n"));
                                break;
                            }
                            if (pCktLevel->aCktRxPasswd[u1PasswdIdx].
                                u1ExistState != ISIS_ACTIVE)
                            {
                                u1FreeIdx = u1PasswdIdx;
                                i1ErrCode = SNMP_SUCCESS;
                                break;
                            }
                        }
                    }

                    if (i1ErrCode != SNMP_FAILURE)
                    {
                        MEMCPY (pCktLevel->aCktRxPasswd[u1FreeIdx].au1Password,
                                pFsIsisExtCircLevelRxPassword->pu1_OctetList,
                                pFsIsisExtCircLevelRxPassword->i4_Length);
                        pCktLevel->aCktRxPasswd[u1FreeIdx].u1Len
                            = (UINT1) pFsIsisExtCircLevelRxPassword->i4_Length;

                        pCktLevel->aCktRxPasswd[u1FreeIdx].u1ExistState
                            = ISIS_ACTIVE;

                    }
                }
            }
            break;

        case ISIS_DESTROY:

            if (IsisCtrlGetSysContext (u4InstIdx, &pContext) != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                         "exist for the Index, %d\n", i4FsIsisExtSysInstance));
            }

            else if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                   &pCktEntry)) != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit does not "
                         "exist for the Index, %d\n", i4FsIsisExtCircIndex));
            }

            else
            {
                if ((u1CktLvlIdx != ISIS_LEVEL1)
                    && (u1CktLvlIdx != ISIS_LEVEL2))
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does "
                             "not exist for the Index, %u\n", u1CktLvlIdx));
                }
                else
                {
                    pCktLevel =
                        (u1CktLvlIdx == ISIS_LEVEL1) ? (pCktEntry->pL1CktInfo)
                        : (pCktEntry->pL2CktInfo);

                    while (u1PasswdIdx < ISIS_MAX_RX_PASSWDS)
                    {
                        /*klocwork */
                        if (pFsIsisExtCircLevelRxPassword->i4_Length <=
                            ISIS_MAX_PASSWORD_LEN)
                        {

                            if ((pCktLevel->aCktRxPasswd[u1PasswdIdx].u1Len
                                 == pFsIsisExtCircLevelRxPassword->i4_Length)
                                && (MEMCMP (pCktLevel->
                                            aCktRxPasswd[u1PasswdIdx].
                                            au1Password,
                                            pFsIsisExtCircLevelRxPassword->
                                            pu1_OctetList,
                                            pFsIsisExtCircLevelRxPassword->
                                            i4_Length) == 0))
                            {
                                MEMSET (pCktLevel->
                                        aCktRxPasswd[u1PasswdIdx].au1Password,
                                        0x00, ISIS_MAX_PASSWORD_LEN);
                                pCktLevel->aCktRxPasswd[u1PasswdIdx].u1Len = 0;

                                pCktLevel->aCktRxPasswd[u1PasswdIdx].
                                    u1ExistState = ISIS_DESTROY;
                                i1ErrCode = SNMP_SUCCESS;
                                break;
                            }
                            u1PasswdIdx++;
                        }
                    }
                }
            }
            break;

        default:
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Invalid Value of Exist State !!!\n"));
            i1ErrCode = SNMP_FAILURE;
            break;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisExtCircLevelRxPasswordExistState ()  \n"));

    return i1ErrCode;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtCircLevelRxPasswordExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
                FsIsisExtCircLevelRxPassword

                The Object 
                testValFsIsisExtCircLevelRxPasswordExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtCircLevelRxPasswordExistState (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsIsisExtSysInstance,
                                                 INT4 i4FsIsisExtCircIndex,
                                                 INT4 i4FsIsisExtCircLevelIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsIsisExtCircLevelRxPassword,
                                                 INT4
                                                 i4TestValFsIsisExtCircLevelRxPasswordExistState)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtCircLevelRxPasswordExistState ()  \n"));

    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4FsIsisExtCircIndex);
    UNUSED_PARAM (i4FsIsisExtCircLevelIndex);
    UNUSED_PARAM (pFsIsisExtCircLevelRxPassword);

    if ((i4TestValFsIsisExtCircLevelRxPasswordExistState != ISIS_CR_GO)
        && (i4TestValFsIsisExtCircLevelRxPasswordExistState != ISIS_DESTROY))
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Exist State!! \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtCircLevelRxPasswordExistState ()  \n"));
    return i1ErrCode;

}

/* LOW LEVEL Routines for Table : FsIsisExtIPIfAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtIPIfAddrTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPIfIndex
                FsIsisExtIPIfSubIndex
                FsIsisExtIPIfAddrType
                FsIsisExtIPIfAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtIPIfAddrTable (INT4 i4FsIsisExtSysInstance,
                                                INT4 i4FsIsisExtIPIfIndex,
                                                INT4 i4FsIsisExtIPIfSubIndex,
                                                INT4 i4FsIsisExtIPIfAddrType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsIsisExtIPIfAddr)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT1               u1IPIfType = (UINT1) i4FsIsisExtIPIfAddrType;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtIPIfAddrTable () \n"));

    if (i4FsIsisExtSysInstance < 0)
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Instance Index. "
                 "The Index is %d\n", i4FsIsisExtSysInstance));
    }
    else if (i4FsIsisExtIPIfSubIndex < 0)
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid IPIf Sub Index. "
                 "The Index is %d\n", i4FsIsisExtIPIfSubIndex));
    }
    else if (i4FsIsisExtIPIfIndex < 0)
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid IPIf Index. "
                 "The Index is %d\n", i4FsIsisExtIPIfIndex));
    }
    else if (u1IPIfType > 2)
    {
        i1ErrCode = SNMP_FAILURE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid IPIfType. "
                 "The Index is %u\n", u1IPIfType));
    }
    else if (pFsIsisExtIPIfAddr->i4_Length <= 0)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of IP_IF Address Length \n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtIPIfAddrTable ()  \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtIPIfAddrTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPIfIndex
                FsIsisExtIPIfSubIndex
                FsIsisExtIPIfAddrType
                FsIsisExtIPIfAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtIPIfAddrTable (INT4 *pi4FsIsisExtSysInstance,
                                        INT4 *pi4FsIsisExtIPIfIndex,
                                        INT4 *pi4FsIsisExtIPIfSubIndex,
                                        INT4 *pi4FsIsisExtIPIfAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsIsisExtIPIfAddr)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT1               au1OctetList[ISIS_MAX_IP_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE IPIfAddr;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtIPIfAddrTable () \n"));

    MEMSET (au1OctetList, 0x00, ISIS_MAX_IP_ADDR_LEN);
    IPIfAddr.pu1_OctetList = au1OctetList;
    IPIfAddr.i4_Length = ISIS_MAX_IP_ADDR_LEN;

    i1ErrCode = nmhGetNextIndexFsIsisExtIPIfAddrTable (0,
                                                       pi4FsIsisExtSysInstance,
                                                       0,
                                                       pi4FsIsisExtIPIfIndex,
                                                       0,
                                                       pi4FsIsisExtIPIfSubIndex,
                                                       0,
                                                       pi4FsIsisExtIPIfAddrType,
                                                       &IPIfAddr,
                                                       pFsIsisExtIPIfAddr);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtIPIfAddrTable ()  \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtIPIfAddrTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtIPIfIndex
                nextFsIsisExtIPIfIndex
                FsIsisExtIPIfSubIndex
                nextFsIsisExtIPIfSubIndex
                FsIsisExtIPIfAddrType
                nextFsIsisExtIPIfAddrType
                FsIsisExtIPIfAddr
                nextFsIsisExtIPIfAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtIPIfAddrTable (INT4 i4FsIsisExtSysInstance,
                                       INT4 *pi4NextFsIsisExtSysInstance,
                                       INT4 i4FsIsisExtIPIfIndex,
                                       INT4 *pi4NextFsIsisExtIPIfIndex,
                                       INT4 i4FsIsisExtIPIfSubIndex,
                                       INT4 *pi4NextFsIsisExtIPIfSubIndex,
                                       INT4 i4FsIsisExtIPIfAddrType,
                                       INT4 *pi4NextFsIsisExtIPIfAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsIsisExtIPIfAddr,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsIsisExtIPIfAddr)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4SysInstance = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexFsIsisExtIPIfAddrTable ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4SysInstance, &pContext);

    if (i4RetVal == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance,
                                            &u4SysInstance) == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                     "Index with the given Indices \n"));
        }
        else
        {
            if (IsisCtrlGetSysContext (u4SysInstance, &pContext)
                            != ISIS_SUCCESS)
            {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                                        "the corresponding index \n"))
            }
            MEMSET (pFsIsisExtIPIfAddr->pu1_OctetList, 0, ISIS_MAX_IP_ADDR_LEN);
            i4FsIsisExtIPIfIndex = 0;
            i4FsIsisExtIPIfSubIndex = 0;
            i4FsIsisExtIPIfAddrType = 0;
        }
    }

    while (pContext != NULL)
    {
        i4RetVal = nmhUtlGNIfAddr (pContext, i4FsIsisExtIPIfIndex,
                                   i4FsIsisExtIPIfSubIndex,
                                   i4FsIsisExtIPIfAddrType,
                                   pFsIsisExtIPIfAddr, pNextFsIsisExtIPIfAddr);

        if (i4RetVal != ISIS_SUCCESS)
        {

            i4RetVal = nmhUtlGNIfAddrType (pContext, i4FsIsisExtIPIfIndex,
                                           i4FsIsisExtIPIfSubIndex,
                                           i4FsIsisExtIPIfAddrType,
                                           pi4NextFsIsisExtIPIfAddrType);

            if (i4RetVal != ISIS_SUCCESS)
            {

                i4RetVal = nmhUtlGNIfSubIdx (pContext, i4FsIsisExtIPIfIndex,
                                             i4FsIsisExtIPIfSubIndex,
                                             pi4NextFsIsisExtIPIfSubIndex);
                if (i4RetVal != ISIS_SUCCESS)
                {
                    i4RetVal = nmhUtlGNIfIdx (pContext, i4FsIsisExtIPIfIndex,
                                              pi4NextFsIsisExtIPIfIndex);

                    if (i4RetVal != ISIS_SUCCESS)
                    {
                        i4RetVal =
                            nmhUtlGetNextIndexIsisSysTable (u4SysInstance,
                                                            &u4SysInstance);
                        if (i4RetVal != ISIS_SUCCESS)
                        {
                            NMP_EE ((ISIS_LGST, "NMP <E> : Indices out of "
                                     "Bound!!!\n"));
                            break;
                        }
                        else
                        {
                           if (IsisCtrlGetSysContext (u4SysInstance, &pContext)
                                          != ISIS_SUCCESS)
                           {
                                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                                                    "the corresponding index \n"));
                           }
                            MEMSET (pFsIsisExtIPIfAddr->pu1_OctetList, 0,
                                    ISIS_MAX_IP_ADDR_LEN);
                            i4FsIsisExtIPIfAddrType = 0;
                            i4FsIsisExtIPIfIndex = 0;
                            i4FsIsisExtIPIfSubIndex = 0;
                        }
                    }
                    else
                    {
                        i4FsIsisExtIPIfIndex = *pi4NextFsIsisExtIPIfIndex;
                        i4FsIsisExtIPIfSubIndex = 0;
                        i4FsIsisExtIPIfAddrType = 0;
                        MEMSET (pFsIsisExtIPIfAddr->pu1_OctetList, 0,
                                ISIS_MAX_IP_ADDR_LEN);
                        continue;
                    }
                }
                else
                {
                    i4FsIsisExtIPIfSubIndex = *pi4NextFsIsisExtIPIfSubIndex;
                    i4FsIsisExtIPIfAddrType = 0;
                    MEMSET (pFsIsisExtIPIfAddr->pu1_OctetList, 0,
                            ISIS_MAX_IP_ADDR_LEN);
                    continue;
                }
            }
            else
            {
                i4FsIsisExtIPIfAddrType = *pi4NextFsIsisExtIPIfAddrType;
                MEMSET (pFsIsisExtIPIfAddr->pu1_OctetList, 0,
                        ISIS_MAX_IP_ADDR_LEN);
            }
        }
        else
        {
            *pi4NextFsIsisExtIPIfSubIndex = i4FsIsisExtIPIfSubIndex;
            *pi4NextFsIsisExtIPIfIndex = i4FsIsisExtIPIfIndex;
            *pi4NextFsIsisExtSysInstance = u4SysInstance;

            /* Currently FutureISIS is supporting only IPv4, hence Next Address
             * Type is always maintained as IPV4. Later, if IPV6 is also
             * supported, the following assignment should be relooked into
             */

            *pi4NextFsIsisExtIPIfAddrType = i4FsIsisExtIPIfAddrType;
            i1RetCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtIPIfAddrTable ()\n"));
    return (i1RetCode);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtIPIfExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPIfIndex
                FsIsisExtIPIfSubIndex
                FsIsisExtIPIfAddrType
                FsIsisExtIPIfAddr

                The Object 
                retValFsIsisExtIPIfExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtIPIfExistState (INT4 i4FsIsisExtSysInstance,
                               INT4 i4FsIsisExtIPIfIndex,
                               INT4 i4FsIsisExtIPIfSubIndex,
                               INT4 i4FsIsisExtIPIfAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsIsisExtIPIfAddr,
                               INT4 *pi4RetValFsIsisExtIPIfExistState)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisExtIPIfExistState ()\n"));

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Instance Index !!!\n"));
    }
    else
    {
        i4RetVal = IsisCtrlGetIPIf (pContext, (UINT4) i4FsIsisExtIPIfIndex,
                                    (UINT4) i4FsIsisExtIPIfSubIndex,
                                    pFsIsisExtIPIfAddr->pu1_OctetList,
                                    (UINT1) i4FsIsisExtIPIfAddrType, &pIPIfRec);
        if (i4RetVal == ISIS_SUCCESS)
        {
            *pi4RetValFsIsisExtIPIfExistState = (INT4) pIPIfRec->u1ExistState;
            i1RetCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtIPIfExistState ()\n"));
    return i1RetCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtIPIfExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPIfIndex
                FsIsisExtIPIfSubIndex
                FsIsisExtIPIfAddrType
                FsIsisExtIPIfAddr

                The Object 
                setValFsIsisExtIPIfExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtIPIfExistState (INT4 i4FsIsisExtSysInstance,
                               INT4 i4FsIsisExtIPIfIndex,
                               INT4 i4FsIsisExtIPIfSubIndex,
                               INT4 i4FsIsisExtIPIfAddrType,
                               tSNMP_OCTET_STRING_TYPE * pFsIsisExtIPIfAddr,
                               INT4 i4SetValFsIsisExtIPIfExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetCode = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4FullMetric = 0;
    tIsisMetric         Metric;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    tIsisEvtIPIFChange *pIPIfEvt = NULL;
    UINT4               u4Port = 0;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    tNetIpv6AddrInfo    NetIpv6AddrInfo;
    UINT4               u4IpAddr = 0;
    UINT1               u1PrefLen = 0;
    INT4                i4TempRetCode = ISIS_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisExtIPIfExistState ()\n"));
    if(NULL == pFsIsisExtIPIfAddr)
    {
          return SNMP_FAILURE;
    }

    MEMSET (&NetIpv4IfInfo, 0, sizeof (NetIpv4IfInfo));
    MEMSET (&NetIpv6AddrInfo, 0, sizeof (NetIpv6AddrInfo));
    MEMSET (&Metric, 0, sizeof (tIsisMetric));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    switch (i4SetValFsIsisExtIPIfExistState)
    {
        case ISIS_CR_GO:

            if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Instance Index !!!\n"));
            }

            else
            {
                i4RetCode = IsisCtrlGetIPIf (pContext,
                                             (UINT4) i4FsIsisExtIPIfIndex,
                                             (UINT4) i4FsIsisExtIPIfSubIndex,
                                             pFsIsisExtIPIfAddr->pu1_OctetList,
                                             (UINT1) i4FsIsisExtIPIfAddrType,
                                             &pIPIfRec);
                if (i4RetCode == ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : IF Entry Already Exists!!!\n"));
                }
                else
                {
                    pIPIfRec = (tIsisIPIfAddr *)
                        ISIS_MEM_ALLOC (ISIS_BUF_IPIF, sizeof (tIsisIPIfAddr));
                    if (pIPIfRec == NULL)
                    {
                        NMP_PT ((ISIS_LGST, "NMP  <E> : No Memory for IPIf "
                                 "Entry !!!\n"));
                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
                        break;
                    }

                    MEMCPY (pIPIfRec->au1IPAddr,
                            pFsIsisExtIPIfAddr->pu1_OctetList,
                            pFsIsisExtIPIfAddr->i4_Length);

                    pIPIfRec->u4CktIfIdx = (UINT4) i4FsIsisExtIPIfIndex;
                    pIPIfRec->u1AddrType = (UINT1) i4FsIsisExtIPIfAddrType;
                    pIPIfRec->u4CktIfSubIdx = (UINT4) i4FsIsisExtIPIfSubIndex;
                    pIPIfRec->u1ExistState = ISIS_ACTIVE;

                    if (i4FsIsisExtIPIfAddrType == ISIS_ADDR_IPV4)
                    {
                        MEMCPY(&u4IpAddr, pIPIfRec->au1IPAddr, ISIS_MAX_IPV4_ADDR_LEN);
                        u4IpAddr =  OSIX_HTONL (u4IpAddr);
                        if (CfaIpIfIsSecondaryAddress  ((UINT4) i4FsIsisExtIPIfIndex,
                                                        u4IpAddr)== CFA_SUCCESS)
                        {
                             pIPIfRec->u1SecondaryFlag = ISIS_TRUE;

                        }
                    }

                    i1ErrCode = SNMP_SUCCESS;
                    IsisCtrlAddIPIf (pContext, pIPIfRec);

                    i4RetCode =
                        IsisAdjGetCktRecWithIfIdx (pIPIfRec->u4CktIfIdx,
                                                   pIPIfRec->u4CktIfSubIdx,
                                                   &pCktRec);
 		    if(ISIS_SUCCESS != i4RetCode)
                    {
                       break;
                    }

                    if ((pCktRec != NULL) && (pCktRec->u1OperState == ISIS_UP))
                    {
                        pIPIfEvt = (tIsisEvtIPIFChange *)
                            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                            sizeof (tIsisEvtIPIFChange));

                        if (pIPIfEvt == NULL)
                        {
                            NMP_PT ((ISIS_LGST, "NMP <E> : No Memory for "
                                     "Posting IP Interface change event\n"));
                            break;
                        }
                        pIPIfEvt->u1EvtID = ISIS_EVT_IP_IF_ADDR_CHANGE;
                        pIPIfEvt->u1Status = ISIS_IP_IF_ADDR_ADD;
                        pIPIfEvt->u4CktIfIdx = (UINT4) i4FsIsisExtIPIfIndex;
                        pIPIfEvt->u4CktIfSubIdx =
                            (UINT4) i4FsIsisExtIPIfSubIndex;
			pIPIfEvt->u1AddrType = (UINT1) i4FsIsisExtIPIfAddrType;
			if (pIPIfEvt->u1AddrType == ISIS_ADDR_IPV4)
			{
				if (NetIpv4GetPortFromIfIndex (i4FsIsisExtIPIfIndex, &u4Port) == NETIPV4_SUCCESS)
				{
				    if (NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) == NETIPV4_SUCCESS)
                    {
				        NetIpv4IfInfo.u4NetMask = OSIX_HTONL (NetIpv4IfInfo.u4NetMask);
				        i4RetCode = IsisUtlComputePrefixLen (((UINT1 *) &(NetIpv4IfInfo.u4NetMask)), 
                                                 ISIS_MAX_IPV4_ADDR_LEN, &u1PrefLen);
                    }
				}
				MEMCPY (pIPIfEvt->au1IPAddr, pIPIfRec->au1IPAddr,
						ISIS_MAX_IPV4_ADDR_LEN);
                if (u1PrefLen == 0)
                {
				   pIPIfEvt->u1PrefixLen = ISIS_IPV4_MAX_PREFIX_LEN;
                }
                else
                {
				   pIPIfEvt->u1PrefixLen = u1PrefLen;
                }
			}
			else
			{
				 MEMCPY (pIPIfEvt->au1IPAddr, pIPIfRec->au1IPAddr,
                                                         ISIS_MAX_IPV6_ADDR_LEN);
				i4TempRetCode = NetIpv6GetFirstIfAddr (i4FsIsisExtIPIfIndex, &NetIpv6AddrInfo);
			        UNUSED_PARAM(i4TempRetCode); /*Coverity fix*/	
        	        if (NetIpv6AddrInfo.u4PrefixLength == 0)
            	    {
					   pIPIfEvt->u1PrefixLen = ISIS_IPV6_MAX_PREFIX_LEN;
	                }
    	            else
        	        {
					   pIPIfEvt->u1PrefixLen = NetIpv6AddrInfo.u4PrefixLength;
	                }
			 
			}
                        IsisUtlSendEvent (pContext, (UINT1 *) pIPIfEvt,
                                          sizeof (tIsisEvtIPIFChange));
                        /* IPRA Change event should also be triggered */
                        if (pCktRec->u1CktLevel != ISIS_LEVEL2)
                        {
                                u4FullMetric = pCktRec->pL1CktInfo->u4FullMetric; 
                                MEMCPY (Metric, pCktRec->pL1CktInfo->Metric,
                                        (sizeof (tIsisMetric)));
                        }
                        else
                        {
                                u4FullMetric = pCktRec->pL2CktInfo->u4FullMetric;
                                MEMCPY (Metric, pCktRec->pL2CktInfo->Metric,
                                        (sizeof (tIsisMetric)));
                        }
                        IsisCtrlAddAutoIPRA (pContext, pCktRec->u4CktIfIdx,
                                             pCktRec->u4CktIfSubIdx,
                                             pIPIfEvt->au1IPAddr,
                                             pIPIfEvt->u1PrefixLen, Metric,
                                             u4FullMetric,
                                             pIPIfEvt->u1AddrType);
                    }
                }
            }
            break;

        case ISIS_DESTROY:

            if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Instance Index !!!\n"));
            }
            else
            {
                if (IsisCtrlGetIPIf (pContext, (UINT4) i4FsIsisExtIPIfIndex,
                                     (UINT4) i4FsIsisExtIPIfSubIndex,
                                     pFsIsisExtIPIfAddr->pu1_OctetList,
                                     (UINT1) i4FsIsisExtIPIfAddrType,
                                     &pIPIfRec) == ISIS_SUCCESS)
                {
                    i4RetCode
                        = IsisCtrlDelIPIf (pContext,
                                           (UINT4) i4FsIsisExtIPIfIndex,
                                           (UINT4) i4FsIsisExtIPIfSubIndex,
                                           pFsIsisExtIPIfAddr->pu1_OctetList,
                                           (UINT1) i4FsIsisExtIPIfAddrType);
                    if (i4RetCode != ISIS_SUCCESS)
                    {
                        NMP_PT ((ISIS_LGST,
                                 "NMP <E> : Entry Does not Exist!!!\n"));
                    }
                    else
                    {
                        i1ErrCode = SNMP_SUCCESS;
                        i4RetCode = IsisAdjGetCktRecWithIfIdx
                            ((UINT4) i4FsIsisExtIPIfIndex,
                             (UINT4) i4FsIsisExtIPIfSubIndex, &pCktRec);
			 if(ISIS_SUCCESS != i4RetCode)
                         {
                              break;
                         }

                        if ((pCktRec != NULL)
                            && (pCktRec->u1OperState == ISIS_UP))
                        {
                            pIPIfEvt = (tIsisEvtIPIFChange *)
                                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtIPIFChange));

                            if (pIPIfEvt == NULL)
                            {
                                NMP_PT ((ISIS_LGST, "NMP <E> : No Memory for "
                                         "Posting IP Interface change event !!!\n"));
                                break;
                            }
                            pIPIfEvt->u1EvtID = ISIS_EVT_IP_IF_ADDR_CHANGE;
                            pIPIfEvt->u1Status = ISIS_IP_IF_ADDR_DEL;
                            pIPIfEvt->u4CktIfIdx = (UINT4) i4FsIsisExtIPIfIndex;
                            pIPIfEvt->u4CktIfSubIdx =
                                (UINT4) i4FsIsisExtIPIfSubIndex;
                            MEMCPY (pIPIfEvt->au1IPAddr,
                                    pFsIsisExtIPIfAddr->pu1_OctetList,
                                    pFsIsisExtIPIfAddr->i4_Length);
                            IsisUtlSendEvent (pContext, (UINT1 *) pIPIfEvt,
                                              sizeof (tIsisEvtIPIFChange));
                        }
                    }
                }
            }
            break;

        default:
            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of ExistState!!!\n"));
            break;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisExtIPIfExistState ()\n"));

    return i1ErrCode;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtIPIfExistState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPIfIndex
                FsIsisExtIPIfSubIndex
                FsIsisExtIPIfAddrType
                FsIsisExtIPIfAddr

                The Object 
                testValFsIsisExtIPIfExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtIPIfExistState (UINT4 *pu4ErrorCode,
                                  INT4 i4FsIsisExtSysInstance,
                                  INT4 i4FsIsisExtIPIfIndex,
                                  INT4 i4FsIsisExtIPIfSubIndex,
                                  INT4 i4FsIsisExtIPIfAddrType,
                                  tSNMP_OCTET_STRING_TYPE * pFsIsisExtIPIfAddr,
                                  INT4 i4TestValFsIsisExtIPIfExistState)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisExtIPIfExistState ()\n"));

    UNUSED_PARAM (i4FsIsisExtSysInstance);
    UNUSED_PARAM (i4FsIsisExtIPIfIndex);
    UNUSED_PARAM (i4FsIsisExtIPIfSubIndex);
    UNUSED_PARAM (pFsIsisExtIPIfAddr);

    if ((IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)) !=
        ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Instance Index !!!\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1ErrCode = SNMP_FAILURE;

    }
    if ((i4FsIsisExtIPIfAddrType != ISIS_IPV4)
        && (i4FsIsisExtIPIfAddrType != ISIS_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }
    if ((i4TestValFsIsisExtIPIfExistState != ISIS_CR_GO)
        && (i4TestValFsIsisExtIPIfExistState != ISIS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2FsIsisExtIPIfExistState ()\n"));
    return (i1ErrCode);
}

/* LOW LEVEL Routines for Table : FsIsisExtLogTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtLogTable
 Input       :  The Indices
                FsIsisExtLogModId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtLogTable (INT4 i4FsIsisExtLogModId)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT1               u1LogModId = (UINT1) i4FsIsisExtLogModId;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtLogTable ()\n"));

    UNUSED_PARAM (i4FsIsisExtLogModId);
    if ((u1LogModId > ISIS_MAX_MODULES))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Module ID. The ID is %u\n", u1LogModId));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtLogTable () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtLogTable
 Input       :  The Indices
                FsIsisExtLogModId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtLogTable (INT4 *pi4FsIsisExtLogModId)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisExtLogTable ()\n"));

    *pi4FsIsisExtLogModId = 0;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisExtLogTable ()  \n"));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtLogTable
 Input       :  The Indices
                FsIsisExtLogModId
                nextFsIsisExtLogModId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtLogTable (INT4 i4FsIsisExtLogModId,
                                  INT4 *pi4NextFsIsisExtLogModId)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexFsIsisExtLogTable () \n"));

    if ((i4FsIsisExtLogModId >= 0)
        && (i4FsIsisExtLogModId < (ISIS_MAX_MODULES - 1)))
    {
        *pi4NextFsIsisExtLogModId = ++i4FsIsisExtLogModId;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtLogTable ()  \n"));
    return i1ErrCode;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtLogLevel
 Input       :  The Indices
                FsIsisExtLogModId

                The Object 
                retValFsIsisExtLogLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtLogLevel (INT4 i4FsIsisExtLogModId,
                         INT4 *pi4RetValFsIsisExtLogLevel)
{
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtLogLevel () \n"));

    if ((i4FsIsisExtLogModId < ISIS_MAX_MODULES) && (i4FsIsisExtLogModId >= 0))
    {

        *pi4RetValFsIsisExtLogLevel =
            (INT4) gau1IsisModLevelMask[i4FsIsisExtLogModId];

        i1ErrCode = SNMP_SUCCESS;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtLogLevel ()  \n"));

    return i1ErrCode;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisExtLogLevel
 Input       :  The Indices
                FsIsisExtLogModId

                The Object 
                setValFsIsisExtLogLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtLogLevel (INT4 i4FsIsisExtLogModId,
                         INT4 i4SetValFsIsisExtLogLevel)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisExtLogLevel () \n"));

    if ((i4FsIsisExtLogModId >= ISIS_MAX_MODULES) || (i4FsIsisExtLogModId < 0))
    {
        i1ErrCode = SNMP_FAILURE;
    }
    else if (i4SetValFsIsisExtLogLevel != 0)
    {
        gau1IsisModLevelMask[i4FsIsisExtLogModId] |=
            (UINT1) i4SetValFsIsisExtLogLevel;
    }
    else
    {
        gau1IsisModLevelMask[i4FsIsisExtLogModId] =
            (UINT1) i4SetValFsIsisExtLogLevel;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisExtLogLevel ()  \n"));

    return i1ErrCode;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtLogLevel
 Input       :  The Indices
                FsIsisExtLogModId

                The Object 
                testValFsIsisExtLogLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtLogLevel (UINT4 *pu4ErrorCode, INT4 i4FsIsisExtLogModId,
                            INT4 i4TestValFsIsisExtLogLevel)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisExtLogLevel () \n"));

    UNUSED_PARAM (i4FsIsisExtLogModId);

    if ((i4TestValFsIsisExtLogLevel > ISIS_LL_MAX_LOGLEVEL) ||
        (i4TestValFsIsisExtLogLevel < 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of LogModId  %d\n",
                 i4TestValFsIsisExtLogLevel));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisExtLogLevel ()  \n"));
    return i1ErrCode;

}

/* LOW LEVEL Routines for Table : FsIsisExtAdjTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtAdjTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtAdjTable (INT4 i4FsIsisExtSysInstance,
                                           INT4 i4FsIsisExtCircIndex,
                                           INT4 i4FsIsisExtAdjIndex)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtAdjTable ()\n"));

    if ((i4FsIsisExtSysInstance < 0) ||
        (i4FsIsisExtAdjIndex <= 0) || (i4FsIsisExtCircIndex <= 0))
    {

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid indices. \n"
                 "The indices are SysInstance = %d"
                 "CircIndex = %d, AdjIndex = %d\n",
                 i4FsIsisExtSysInstance, i4FsIsisExtCircIndex,
                 i4FsIsisExtAdjIndex));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting "
                 "nmhValidateIndexInstanceFsIsisExtAdjTable ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                                     &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            i4RetVal = IsisAdjGetAdjRec (pCktEntry, (UINT4) i4FsIsisExtAdjIndex,
                                         &pAdjEntry);
            if (i4RetVal == ISIS_SUCCESS)
            {
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Invalid indices.The indices are "
                         "SysInstance = %d\n" " CircIndex = %d\n, "
                         "AdjIndex = %d\n", i4FsIsisExtSysInstance,
                         i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Invalid indices.The indices are "
                     "SysInstance = %d\n" " CircIndex = %d\n, "
                     "AdjIndex = %d\n", i4FsIsisExtSysInstance,
                     i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid indices.The indices are "
                 "SysInstance = %d\n" " CircIndex = %d\n, "
                 "AdjIndex = %d\n", i4FsIsisExtSysInstance,
                 i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtAdjTable ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtAdjTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisExtAdjTable (INT4 *pi4FsIsisExtSysInstance,
                                   INT4 *pi4FsIsisExtCircIndex,
                                   INT4 *pi4FsIsisExtAdjIndex)
{
    return (nmhGetNextIndexFsIsisExtAdjTable (0, pi4FsIsisExtSysInstance, 0,
                                              pi4FsIsisExtCircIndex, 0,
                                              pi4FsIsisExtAdjIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtAdjTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtCircIndex
                nextFsIsisExtCircIndex
                FsIsisExtAdjIndex
                nextFsIsisExtAdjIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtAdjTable (INT4 i4FsIsisExtSysInstance,
                                  INT4 *pi4NextFsIsisExtSysInstance,
                                  INT4 i4FsIsisExtCircIndex,
                                  INT4 *pi4NextFsIsisExtCircIndex,
                                  INT4 i4FsIsisExtAdjIndex,
                                  INT4 *pi4NextFsIsisExtAdjIndex)
{

    INT1                i1RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetNextIndexFsIsisExtAdjTable ()\n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4FsIsisExtSysInstance,
                                            (UINT4 *) &i4FsIsisExtSysInstance)
            == ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : NO Further valid index available.\n"
                     "The Indices are SysInstance: %u CircIndex: %d "
                     "AdjIndex :%d\n", i4FsIsisExtSysInstance,
                     i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));

            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting nmhGetNextIndexFsIsisExtAdjTable ()\n"));

            return (SNMP_FAILURE);
        }
        else
        {
            i4FsIsisExtCircIndex = 0;
            i4FsIsisExtAdjIndex = 0;
        }
    }

    if (pContext == NULL)
    {
       if  (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext) == ISIS_FAILURE)
       {
          NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                               "the corresponding index \n"));
       }

    }
    while (pContext != NULL)
    {
        if (nmhUtlGetNextIndexIsisISAdjTable
            (i4FsIsisExtSysInstance, i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex,
             &i4FsIsisExtAdjIndex) == ISIS_FAILURE)
        {
            if (nmhUtlGetNextIndexIsisCircTable ((UINT4) i4FsIsisExtSysInstance,
                                                 (UINT4) i4FsIsisExtCircIndex,
                                                 (UINT4 *)
                                                 &i4FsIsisExtCircIndex) ==
                ISIS_FAILURE)
            {
                if (nmhUtlGetNextIndexIsisSysTable
                    ((UINT4) i4FsIsisExtSysInstance,
                     (UINT4 *) &i4FsIsisExtSysInstance) == ISIS_FAILURE)
                {

                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : NO Further valid index available.\n"
                             "The Indices are sys instance: %d "
                             "CircIndex: %d AdjIndex :%d\n",
                             i4FsIsisExtSysInstance, i4FsIsisExtCircIndex,
                             i4FsIsisExtAdjIndex));
                    NMP_EE ((ISIS_LGST,
                             "NMP <X> : Exiting "
                             "nmhGetNextIndexFsIsisExtAdjTable ()\n"));

                    return (SNMP_FAILURE);
                }
                else
                {
                    i4FsIsisExtCircIndex = 0;
                    i4FsIsisExtAdjIndex = 0;
                    continue;
                }
            }
            else
            {
                i4FsIsisExtAdjIndex = 0;
                continue;
            }
        }
        else
        {
            *pi4NextFsIsisExtSysInstance = i4FsIsisExtSysInstance;
            *pi4NextFsIsisExtCircIndex = i4FsIsisExtCircIndex;
            *pi4NextFsIsisExtAdjIndex = i4FsIsisExtAdjIndex;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> :The next Indices are sys instance:"
                     " %d CircIndex: %d AdjIndex :%d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtCircIndex,
                     i4FsIsisExtAdjIndex));

            i1RetVal = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetNextIndexFsIsisExtAdjTable ()\n"));
    return i1RetVal;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtAdjNeighSysID
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex

                The Object 
                retValFsIsisExtAdjNeighSysID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtAdjNeighSysID (INT4 i4FsIsisExtSysInstance,
                              INT4 i4FsIsisExtCircIndex,
                              INT4 i4FsIsisExtAdjIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsIsisExtAdjNeighSysID)
{

    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtAdjNeighSysID ()\n"));

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal =
            IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                              &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4FsIsisExtAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {
                MEMCPY (pRetValFsIsisExtAdjNeighSysID->pu1_OctetList,
                        pAdjEntry->au1AdjNbrSysID, ISIS_SYS_ID_LEN);

                pRetValFsIsisExtAdjNeighSysID->i4_Length = ISIS_SYS_ID_LEN;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4FsIsisExtSysInstance, i4FsIsisExtCircIndex,
                         i4FsIsisExtAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4FsIsisExtSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisISAdjsysId ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtAdjHelperStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex

                The Object 
                retValFsIsisExtAdjHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtAdjHelperStatus (INT4 i4FsIsisExtSysInstance,
                                INT4 i4FsIsisExtCircIndex,
                                INT4 i4FsIsisExtAdjIndex,
                                INT4 *pi4RetValFsIsisExtAdjHelperStatus)
{

    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtAdjHelperStatus ()\n"));

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4FsIsisExtAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {

                *pi4RetValFsIsisExtAdjHelperStatus =
                    pAdjEntry->u1IsisGRHelperStatus;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d "
                         "AdjIndex :%d\n", i4FsIsisExtSysInstance,
                         i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }

        else
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4FsIsisExtSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtAdjHelperStatus ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetFsIsisExtAdjMTID
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex

                The Object 
                retValFsIsisExtAdjMTID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtAdjMTID (INT4 i4FsIsisExtSysInstance,
                        INT4 i4FsIsisExtCircIndex,
                        INT4 i4FsIsisExtAdjIndex,
                        INT4 *pi4RetValFsIsisExtAdjMTID)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtAdjMTID ()\n"));

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4FsIsisExtAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {

                *pi4RetValFsIsisExtAdjMTID = pAdjEntry->u1AdjMTId;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         "sys instance: %d CircIndex: %d "
                         "AdjIndex :%d\n", i4FsIsisExtSysInstance,
                         i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }

        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtCircIndex));
            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 "sys instance: %d\n", i4FsIsisExtSysInstance));
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtAdjMTID ()\n"));

    return (i1RetCode);
}
/****************************************************************************
 Function    :  nmhGetFsIsisExtISAdj3WayState
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex

                The Object
                retValFsIsisExtISAdj3WayState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisExtISAdj3WayState(INT4 i4FsIsisExtSysInstance , 
								   INT4 i4FsIsisExtCircIndex , 
								   INT4 i4FsIsisExtAdjIndex , 
								   INT4 *pi4RetValFsIsisExtISAdj3WayState)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtAdjMTID ()\n"));

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4FsIsisExtAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {

                *pi4RetValFsIsisExtISAdj3WayState = (INT4) pAdjEntry->u1P2PThreewayState;
                i1RetCode = SNMP_SUCCESS;
            }
            else
			{
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         "sys instance: %d CircIndex: %d "
                         "AdjIndex :%d\n", i4FsIsisExtSysInstance,
                         i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));
			}
		}
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtCircIndex));
            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 "sys instance: %d\n", i4FsIsisExtSysInstance));
        i1RetCode = SNMP_FAILURE;
    }
	return (i1RetCode);	
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtISAdjNbrExtendedCircID
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex

                The Object
                retValFsIsisExtISAdjNbrExtendedCircID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisExtISAdjNbrExtendedCircID(INT4 i4FsIsisExtSysInstance , 
										   INT4 i4FsIsisExtCircIndex , 
										   INT4 i4FsIsisExtAdjIndex , 
										   UINT4 *pu4RetValFsIsisExtISAdjNbrExtendedCircID)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtAdjMTID ()\n"));

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4FsIsisExtAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {

                *pu4RetValFsIsisExtISAdjNbrExtendedCircID = pAdjEntry->u4NeighExtCircuitID;
                i1RetCode = SNMP_SUCCESS;
            }
            else
			{
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         "sys instance: %d CircIndex: %d "
                         "AdjIndex :%d\n", i4FsIsisExtSysInstance,
                         i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));
			}
		}
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtCircIndex));
            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 "sys instance: %d\n", i4FsIsisExtSysInstance));
        i1RetCode = SNMP_FAILURE;
    }
	return (i1RetCode);	
}
/****************************************************************************
 Function    :  nmhGetFsIsisExtAdjHostName
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex

                The Object 
                retValFsIsisExtAdjHostName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtAdjHostName (INT4 i4FsIsisExtSysInstance,
                            INT4 i4FsIsisExtCircIndex, INT4 i4FsIsisExtAdjIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsIsisExtAdjHostName)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               au1OctetList[ISIS_SYS_ID_LEN];
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tSNMP_OCTET_STRING_TYPE IsisExtSysHostNameSysID;

    MEMSET (au1OctetList, 0x00, ISIS_SYS_ID_LEN);
    IsisExtSysHostNameSysID.pu1_OctetList = au1OctetList;
    IsisExtSysHostNameSysID.i4_Length = ISIS_SYS_ID_LEN;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisExtAdjHostName ()\n"));

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);
    if ((i4RetVal == ISIS_SUCCESS)
        && (pContext->u1IsisDynHostNmeSupport != ISIS_DYNHOSTNME_DISABLE))
    {
        i4RetVal =
            IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                              &pCktEntry);
        if (i4RetVal == ISIS_SUCCESS)
        {
            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4FsIsisExtAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {
                MEMCPY (IsisExtSysHostNameSysID.pu1_OctetList,
                        pAdjEntry->au1AdjNbrSysID, ISIS_SYS_ID_LEN);
                IsisExtSysHostNameSysID.i4_Length = ISIS_SYS_ID_LEN;

                i1RetCode =
                    nmhGetFsIsisExtSysRouterHostName (i4FsIsisExtSysInstance,
                                                      &IsisExtSysHostNameSysID,
                                                      pRetValFsIsisExtAdjHostName);
                if (i1RetCode != SNMP_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> :Hostname doesn't exist with the indices."
                             " sys instance: %d CircIndex: %d AdjIndex "
                             ":%d\n", i4FsIsisExtSysInstance,
                             i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));
                }
            }
            else
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d AdjIndex "
                         ":%d\n", i4FsIsisExtSysInstance, i4FsIsisExtCircIndex,
                         i4FsIsisExtAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4FsIsisExtSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisExtAdjHostName ()\n"));

    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtAdjHelperExitReason
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtAdjIndex

                The Object 
                retValFsIsisExtAdjHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtAdjHelperExitReason (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4FsIsisExtCircIndex,
                                    INT4 i4FsIsisExtAdjIndex,
                                    INT4 *pi4RetValFsIsisExtAdjHelperExitReason)
{

    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisExtAdjHelperStatus ()\n"));

    i4RetVal =
        IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, (UINT4) i4FsIsisExtCircIndex,
                                     &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            if (IsisAdjGetAdjRec (pCktEntry, (UINT4) i4FsIsisExtAdjIndex,
                                  &pAdjEntry) == ISIS_SUCCESS)
            {

                *pi4RetValFsIsisExtAdjHelperExitReason =
                    pAdjEntry->u1IsisGRHelperExitReason;
                i1RetCode = SNMP_SUCCESS;
            }
            else
            {

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : AdjEntry doesn't exist with the indices."
                         " sys instance: %d CircIndex: %d "
                         "AdjIndex :%d\n", i4FsIsisExtSysInstance,
                         i4FsIsisExtCircIndex, i4FsIsisExtAdjIndex));

                i1RetCode = SNMP_FAILURE;
            }
        }

        else
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : CktEntry doesn't exist with the indices."
                     " sys instance: %d CircIndex: %d\n",
                     i4FsIsisExtSysInstance, i4FsIsisExtCircIndex));

            i1RetCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry doesn't exist with the indices."
                 " sys instance: %d\n", i4FsIsisExtSysInstance));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisExtAdjHelperStatus ()\n"));

    return (i1RetCode);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxInstances
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxInstances (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxCircuits
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxCircuits (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxAreaAddrs
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxAreaAddrs (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxAdjs
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxAdjs (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxIPRAs
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxIPRAs (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxEvents
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxEvents (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxSummAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxSummAddr (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxLSPEntries
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxLSPEntries (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxMAA
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxMAA (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisFTStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisFTStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisFactor
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisFactor (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisMaxRoutes
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisMaxRoutes (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtSysTable
 Input       :  The Indices
                FsIsisExtSysInstance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtSysTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtSysAreaRxPasswdTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysAreaRxPasswd
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtSysAreaRxPasswdTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtSysDomainRxPasswdTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysDomainRxPassword
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtSysDomainRxPasswdTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtSummAddrTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSummAddressType
                FsIsisExtSummAddress
                FsIsisExtSummAddrPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtSummAddrTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtCircLevelTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtCircLevelTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtIPRATable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPRAType
                FsIsisExtIPRAIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtIPRATable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtCircLevelRxPasswordTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
                FsIsisExtCircLevelIndex
                FsIsisExtCircLevelRxPassword
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtCircLevelRxPasswordTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtIPIfAddrTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtIPIfIndex
                FsIsisExtIPIfSubIndex
                FsIsisExtIPIfAddrType
                FsIsisExtIPIfAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtIPIfAddrTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtCircTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtCircIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtCircTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIsisExtLogTable
 Input       :  The Indices
                FsIsisExtLogModId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisExtLogTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIsisDistInOutRouteMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisDistInOutRouteMapTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisDistInOutRouteMapTable (INT4
                                                      i4FsIsisExtSysInstance,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsIsisDistInOutRouteMapName,
                                                      INT4
                                                      i4FsIsisDistInOutRouteMapType)
{
#ifdef ROUTEMAP_WANTED
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisDistInOutRouteMapTable"
             "FsIsisDistInOutRouteMapTable () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext) !=
        ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if (pContext == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFsIsisDistInOutRouteMapName == NULL ||
        pFsIsisDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName (pContext->pDistanceFilterRMap,
                               pFsIsisDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName (pContext->pDistributeInFilterRMap,
                               pFsIsisDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisDistInOutRouteMapTable"
             "FsIsisDistInOutRouteMapTable () \n"));

    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (i4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisDistInOutRouteMapTable"
             "FsIsisDistInOutRouteMapTable () \n"));
    return SNMP_SUCCESS;
#endif /* ROUTEMAP_WANTED */
}

#ifdef ROUTEMAP_WANTED
/*Filtering nmh processing utils*/
PRIVATE VOID        FilteringRMapSortByIndexIsis (tIsisSysContext *
                                                  pIsisSysContext,
                                                  tFilteringRMap * ppFiltr[]);
PRIVATE INT4        GetFlterTypeByFlterPtrIsis (tIsisSysContext *
                                                pIsisSysContext,
                                                tFilteringRMap * pFltr1);
PRIVATE INT4        FilteringRMapCompareIsis (tIsisSysContext * pIsisSysContext,
                                              tFilteringRMap * pFltr1,
                                              tFilteringRMap * pFltr2);

#define MAX_FILTER_AMOUNT 2

/****************************************************************************
 Function    :  GetFlterTypeByFlterPtrIsis
 Input       :  pFltr1 filter to search
 Output      :  none
 Returns     :  positive fisrt bigger, negative second one bigger
****************************************************************************/
PRIVATE INT4
GetFlterTypeByFlterPtrIsis (tIsisSysContext * pIsisSysContext,
                            tFilteringRMap * pFltr1)
{
    if (pIsisSysContext->pDistanceFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTANCE;
    }
    if (pIsisSysContext->pDistributeInFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_IN;
    }
    return 0;
}

/****************************************************************************
 Function    :  FilteringRMapCompareIsis
 Input       :  compare filters by OID names
 Output      :  none
 Returns     :  positive fisrt bigger, negative second one bigger
****************************************************************************/
PRIVATE INT4
FilteringRMapCompareIsis (tIsisSysContext * pIsisSysContext,
                          tFilteringRMap * pFltr1, tFilteringRMap * pFltr2)
{
#define MAX_VALUE 256
#define EQ_VALUE 0

    INT4                i4CmpResult = EQ_VALUE;

    if ((NULL == pFltr1) && (NULL == pFltr2))
    {
        return EQ_VALUE;
    }
    if (NULL == pFltr1)
    {
        return MAX_VALUE;
    }
    if (NULL == pFltr2)
    {
        return -MAX_VALUE;
    }
    i4CmpResult = STRCMP (pFltr1->au1DistInOutFilterRMapName,
                          pFltr2->au1DistInOutFilterRMapName);
    if (i4CmpResult != 0)
    {
        return i4CmpResult;
    }

    if (pIsisSysContext->pDistributeInFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_IN;
    }
    if (pIsisSysContext->pDistributeInFilterRMap == pFltr2)
    {
        return -FILTERING_TYPE_DISTRIB_IN;
    }

    if (pIsisSysContext->pDistanceFilterRMap == pFltr1)
    {
        return -FILTERING_TYPE_DISTANCE;
    }

    return i4CmpResult;
}

/****************************************************************************
 Function    :  FilteringRMapSortByIndexIsis
 Input       :  tFilteringRMap pointer to array of 3 tFilteringRMap elements
 Output      :  sorted array
 Returns     :  none
****************************************************************************/
PRIVATE VOID
FilteringRMapSortByIndexIsis (tIsisSysContext * pIsisSysContext,
                              tFilteringRMap * ppFiltr[])
{
    BOOL1               b1UpdatedArray = FALSE;
    tFilteringRMap     *pExFilteringRMap = NULL;
    INT4                i4Idx;

    ppFiltr[0] = pIsisSysContext->pDistanceFilterRMap;
    ppFiltr[1] = pIsisSysContext->pDistributeInFilterRMap;

    do
    {
        b1UpdatedArray = FALSE;
        for (i4Idx = 0; i4Idx < MAX_FILTER_AMOUNT - 1; i4Idx++)
            if (FilteringRMapCompareIsis (pIsisSysContext,
                                          ppFiltr[i4Idx],
                                          ppFiltr[i4Idx + 1]) > 0)
            {
                pExFilteringRMap = ppFiltr[i4Idx + 1];
                ppFiltr[i4Idx + 1] = ppFiltr[i4Idx];
                ppFiltr[i4Idx] = pExFilteringRMap;
                b1UpdatedArray = TRUE;
            }
    }
    while (TRUE == b1UpdatedArray);
}

#endif /* ROUTEMAP_WANTED */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisDistInOutRouteMapTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisDistInOutRouteMapTable (INT4 *pi4FsIsisExtSysInstance,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsIsisDistInOutRouteMapName,
                                              INT4
                                              *pi4FsIsisDistInOutRouteMapType)
{
#ifdef ROUTEMAP_WANTED
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisDistInOutRouteMapTable () \n"));

    *pi4FsIsisExtSysInstance = 0;

    if (IsisCtrlGetSysContext ((UINT4) *pi4FsIsisExtSysInstance,
                               &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable ((UINT4) *pi4FsIsisExtSysInstance,
                                            (UINT4 *) pi4FsIsisExtSysInstance)
            == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                     "Index with the given Indices \n"));

            return SNMP_FAILURE;
        }
        else if (IsisCtrlGetSysContext (*pi4FsIsisExtSysInstance, &pContext)
                 == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                     "exist for the Index, %d\n", *pi4FsIsisExtSysInstance));
            return SNMP_FAILURE;
        }
    }

    if (pContext != NULL)
    {
        tFilteringRMap     *FilteringRMapArray[] = { 0, 0, 0 };
        FilteringRMapSortByIndexIsis (pContext, FilteringRMapArray);

        if (NULL != FilteringRMapArray[0])
        {
            *pi4FsIsisDistInOutRouteMapType =
                GetFlterTypeByFlterPtrIsis (pContext, FilteringRMapArray[0]);

            pFsIsisDistInOutRouteMapName->i4_Length =
                STRLEN (FilteringRMapArray[0]->au1DistInOutFilterRMapName);
            MEMCPY (pFsIsisDistInOutRouteMapName->pu1_OctetList,
                    FilteringRMapArray[0]->au1DistInOutFilterRMapName,
                    pFsIsisDistInOutRouteMapName->i4_Length);

            return SNMP_SUCCESS;
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisDistInOutRouteMapTable () \n"));
    return SNMP_FAILURE;

#else
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (pi4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (pi4FsIsisExtSysInstance);
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisDistInOutRouteMapTable () \n"));
    return ISIS_SUCCESS;
#endif /* ROUTEMAP_WANTED */
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisDistInOutRouteMapTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                nextFsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType
                nextFsIsisDistInOutRouteMapType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisDistInOutRouteMapTable (INT4 i4FsIsisExtSysInstance,
                                             INT4 *pi4NextFsIsisExtSysInstance,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsIsisDistInOutRouteMapName,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pNextFsIsisDistInOutRouteMapName,
                                             INT4 i4FsIsisDistInOutRouteMapType,
                                             INT4
                                             *pi4NextFsIsisDistInOutRouteMapType)
{
#ifdef ROUTEMAP_WANTED
    tIsisSysContext    *pContext;
    INT4                i4Idx;

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                               &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4FsIsisExtSysInstance,
                                            (UINT4 *)
                                            pi4NextFsIsisExtSysInstance) ==
            ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                     "Index with the given Indices \n"));

            return SNMP_FAILURE;
        }
        else if (IsisCtrlGetSysContext (*pi4NextFsIsisExtSysInstance, &pContext)
                 == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                     "exist for the Index, %d\n",
                     *pi4NextFsIsisExtSysInstance));
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pi4NextFsIsisExtSysInstance = i4FsIsisExtSysInstance;
    }

    if (pFsIsisDistInOutRouteMapName == NULL ||
        pFsIsisDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    while (pContext != NULL)
    {

        tFilteringRMap     *FilteringRMapArray[] = { 0, 0, 0 };
        FilteringRMapSortByIndexIsis (pContext, FilteringRMapArray);

        for (i4Idx = 0; i4Idx < MAX_FILTER_AMOUNT - 1; i4Idx++)
        {
            /*Klocwork */
            if ((pFsIsisDistInOutRouteMapName->i4_Length) <
                (RMAP_MAX_NAME_LEN + 4))
            {
                if ((NULL != FilteringRMapArray[i4Idx])
                    && (NULL != FilteringRMapArray[i4Idx + 1])
                    && (0 ==
                        MEMCMP (pFsIsisDistInOutRouteMapName->pu1_OctetList,
                                FilteringRMapArray[i4Idx]->
                                au1DistInOutFilterRMapName,
                                pFsIsisDistInOutRouteMapName->i4_Length))
                    && (i4FsIsisDistInOutRouteMapType ==
                        GetFlterTypeByFlterPtrIsis (pContext,
                                                    FilteringRMapArray[i4Idx])))
                {
                    pNextFsIsisDistInOutRouteMapName->i4_Length =
                        STRLEN (FilteringRMapArray[i4Idx + 1]->
                                au1DistInOutFilterRMapName);
                    MEMCPY (pNextFsIsisDistInOutRouteMapName->pu1_OctetList,
                            FilteringRMapArray[i4Idx +
                                               1]->au1DistInOutFilterRMapName,
                            pNextFsIsisDistInOutRouteMapName->i4_Length);
                    *pi4NextFsIsisDistInOutRouteMapType =
                        GetFlterTypeByFlterPtrIsis (pContext,
                                                    FilteringRMapArray[i4Idx +
                                                                       1]);

                    return SNMP_SUCCESS;
                }
            }
        }
        if (nmhUtlGetNextIndexIsisSysTable ((UINT4) pContext->u4SysInstIdx,
                                            (UINT4 *)
                                            pi4NextFsIsisExtSysInstance) ==
            ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                     "Index with the given Indices \n"));

            return SNMP_FAILURE;
        }
        else if (IsisCtrlGetSysContext (*pi4NextFsIsisExtSysInstance, &pContext)
                 == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                     "exist for the Index, %d\n",
                     *pi4NextFsIsisExtSysInstance));
            return SNMP_FAILURE;
        }
        else
        {
            FilteringRMapSortByIndexIsis (pContext, FilteringRMapArray);

            if (NULL != FilteringRMapArray[0])
            {
                *pi4NextFsIsisDistInOutRouteMapType =
                    GetFlterTypeByFlterPtrIsis (pContext,
                                                FilteringRMapArray[0]);

                pNextFsIsisDistInOutRouteMapName->i4_Length =
                    STRLEN (FilteringRMapArray[0]->au1DistInOutFilterRMapName);
                MEMCPY (pNextFsIsisDistInOutRouteMapName->pu1_OctetList,
                        FilteringRMapArray[0]->au1DistInOutFilterRMapName,
                        pNextFsIsisDistInOutRouteMapName->i4_Length);

                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (pNextFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (i4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (pi4NextFsIsisDistInOutRouteMapType);
    UNUSED_PARAM (pi4NextFsIsisExtSysInstance);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    return SNMP_SUCCESS;
#endif /* ROUTEMAP_WANTED */
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisDistInOutRouteMapValue
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType

                The Object 
                retValFsIsisDistInOutRouteMapValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisDistInOutRouteMapValue (INT4 i4FsIsisExtSysInstance,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsIsisDistInOutRouteMapName,
                                    INT4 i4FsIsisDistInOutRouteMapType,
                                    INT4 *pi4RetValFsIsisDistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    tIsisSysContext    *pContext;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if (pFsIsisDistInOutRouteMapName == NULL
        || pFsIsisDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTANCE
        && CmpFilterRMapName (pContext->pDistanceFilterRMap,
                              pFsIsisDistInOutRouteMapName) == 0)
    {
        *pi4RetValFsIsisDistInOutRouteMapValue =
            pContext->pDistanceFilterRMap->u1RMapDistance;
    }
    else
    {
        *pi4RetValFsIsisDistInOutRouteMapValue = 0;
    }
#else
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (i4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (pi4RetValFsIsisDistInOutRouteMapValue);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
#endif /* ROUTEMAP_WANTED */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType

                The Object 
                retValFsIsisDistInOutRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisDistInOutRouteMapRowStatus (INT4 i4FsIsisExtSysInstance,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsIsisDistInOutRouteMapName,
                                        INT4 i4FsIsisDistInOutRouteMapType,
                                        INT4
                                        *pi4RetValFsIsisDistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    tIsisSysContext    *pContext;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if (pFsIsisDistInOutRouteMapName == NULL
        || pFsIsisDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName
            (pContext->pDistanceFilterRMap, pFsIsisDistInOutRouteMapName) == 0)
        {
            *pi4RetValFsIsisDistInOutRouteMapRowStatus =
                pContext->pDistanceFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName
            (pContext->pDistributeInFilterRMap,
             pFsIsisDistInOutRouteMapName) == 0)
        {
            *pi4RetValFsIsisDistInOutRouteMapRowStatus =
                pContext->pDistributeInFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (i4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (pi4RetValFsIsisDistInOutRouteMapRowStatus);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    return SNMP_SUCCESS;
#endif /* ROUTEMAP_WANTED */
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisDistInOutRouteMapValue
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType

                The Object 
                setValFsIsisDistInOutRouteMapValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisDistInOutRouteMapValue (INT4 i4FsIsisExtSysInstance,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsIsisDistInOutRouteMapName,
                                    INT4 i4FsIsisDistInOutRouteMapType,
                                    INT4 i4SetValFsIsisDistInOutRouteMapValue)
{

#ifdef ROUTEMAP_WANTED

    UINT4               u4ErrCode = 0;
    UINT1               u1OldDistance = 0;
    tIsisEvtDistChange  *pDistanceChange = NULL;
    tIsisSysContext    *pContext = NULL;

    if (pFsIsisDistInOutRouteMapName == NULL
        || pFsIsisDistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }
    if (nmhTestv2FsIsisDistInOutRouteMapValue (&u4ErrCode,
                                               i4FsIsisExtSysInstance,
                                               pFsIsisDistInOutRouteMapName,
                                               i4FsIsisDistInOutRouteMapType,
                                               i4SetValFsIsisDistInOutRouteMapValue)
        == SNMP_SUCCESS)
    {
        if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
        {

            if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
                == ISIS_FAILURE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                         "exist for the Index, %d\n", i4FsIsisExtSysInstance));
                return SNMP_FAILURE;
            }
            u1OldDistance = pContext->pDistanceFilterRMap->u1RMapDistance;
            pContext->pDistanceFilterRMap->u1RMapDistance =
                (UINT1) i4SetValFsIsisDistInOutRouteMapValue;
        }
        if (u1OldDistance != (UINT1) i4SetValFsIsisDistInOutRouteMapValue)
        {
            pDistanceChange = (tIsisEvtDistChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDistChange));

            if (pDistanceChange != NULL)
            {
                pDistanceChange->u1EvtID = ISIS_EVT_DISTANCE_CHANGE;

                IsisUtlSendEvent (pContext, (UINT1 *) pDistanceChange,
                        sizeof (tIsisEvtDistChange));
            }
            else
            {
                PANIC ((ISIS_LGST, "NMP <P> : Could not send "
                            "ISIS_EVT_DISTANCE_CHANGE !!!\n"));
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (i4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (i4SetValFsIsisDistInOutRouteMapValue);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
#endif /* ROUTEMAP_WANTED */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIsisDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType

                The Object 
                setValFsIsisDistInOutRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisDistInOutRouteMapRowStatus (INT4 i4FsIsisExtSysInstance,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsIsisDistInOutRouteMapName,
                                        INT4 i4FsIsisDistInOutRouteMapType,
                                        INT4
                                        i4SetValFsIsisDistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    UINT4               u4ErrCode = 0;
    if (nmhTestv2FsIsisDistInOutRouteMapRowStatus (&u4ErrCode,
                                                   i4FsIsisExtSysInstance,
                                                   pFsIsisDistInOutRouteMapName,
                                                   i4FsIsisDistInOutRouteMapType,
                                                   i4SetValFsIsisDistInOutRouteMapRowStatus)
        == SNMP_SUCCESS)
    {
        tIsisSysContext    *pContext;

        if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
            == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                     "exist for the Index, %d\n", i4FsIsisExtSysInstance));
            return SNMP_FAILURE;
        }

        switch (i4SetValFsIsisDistInOutRouteMapRowStatus)
        {
            case CREATE_AND_WAIT:
            {
                tFilteringRMap     *pFilter = NULL;
                UINT4               u4Status = FILTERNIG_STAT_DEFAULT;

                if ((pFilter =
                     MemAllocMemBlk (gIsisMemPoolId.u4RmapFilterPid)) != NULL)
                {
                    MEMSET (pFilter, 0, sizeof (tFilteringRMap));
                    pFilter->u1RowStatus =
                        (UINT1) i4SetValFsIsisDistInOutRouteMapRowStatus;
                    MEMCPY (pFilter->au1DistInOutFilterRMapName,
                            pFsIsisDistInOutRouteMapName->pu1_OctetList,
                            pFsIsisDistInOutRouteMapName->i4_Length);

                    u4Status =
                        RMapGetInitialMapStatus (pFilter->
                                                 au1DistInOutFilterRMapName);
                    if (u4Status != 0)
                    {
                        pFilter->u1Status = FILTERNIG_STAT_ENABLE;
                    }
                    else
                    {
                        pFilter->u1Status = FILTERNIG_STAT_DISABLE;
                    }

                    if (i4FsIsisDistInOutRouteMapType ==
                        FILTERING_TYPE_DISTANCE)
                    {
                        pContext->pDistanceFilterRMap = pFilter;
                    }
                    else if (i4FsIsisDistInOutRouteMapType ==
                             FILTERING_TYPE_DISTRIB_IN)
                    {
                        pContext->pDistributeInFilterRMap = pFilter;
                    }
                }
                else
                {
                    return SNMP_FAILURE;
                }
            }
                break;
            case DESTROY:
                if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                {
                    MemReleaseMemBlock (gIsisMemPoolId.u4RmapFilterPid,
                                        (UINT1 *) pContext->
                                        pDistanceFilterRMap);
                    pContext->pDistanceFilterRMap = NULL;
                }
                else if (i4FsIsisDistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_IN)
                {
                    MemReleaseMemBlock (gIsisMemPoolId.u4RmapFilterPid,
                                        (UINT1 *) pContext->
                                        pDistributeInFilterRMap);
                    pContext->pDistributeInFilterRMap = NULL;
                }
                break;
            default:
                if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                {
                    pContext->pDistanceFilterRMap->u1RowStatus =
                        (UINT1) i4SetValFsIsisDistInOutRouteMapRowStatus;
                }
                else if (i4FsIsisDistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_IN)
                {
                    pContext->pDistributeInFilterRMap->u1RowStatus =
                        (UINT1) i4SetValFsIsisDistInOutRouteMapRowStatus;
                }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (i4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (i4SetValFsIsisDistInOutRouteMapRowStatus);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
#endif /* ROUTEMAP_WANTED */
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisDistInOutRouteMapValue
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType

                The Object 
                testValFsIsisDistInOutRouteMapValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisDistInOutRouteMapValue (UINT4 *pu4ErrorCode,
                                       INT4 i4FsIsisExtSysInstance,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsIsisDistInOutRouteMapName,
                                       INT4 i4FsIsisDistInOutRouteMapType,
                                       INT4
                                       i4TestValFsIsisDistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    tIsisSysContext    *pContext;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if (pContext == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pFsIsisDistInOutRouteMapName == NULL
        || pFsIsisDistInOutRouteMapName->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }

    if (i4TestValFsIsisDistInOutRouteMapValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName
            (pContext->pDistanceFilterRMap, pFsIsisDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName
            (pContext->pDistributeInFilterRMap,
             pFsIsisDistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (i4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFsIsisDistInOutRouteMapValue);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    return SNMP_SUCCESS;
#endif /* ROUTEMAP_WANTED */
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisDistInOutRouteMapRowStatus
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType

                The Object 
                testValFsIsisDistInOutRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisDistInOutRouteMapRowStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4FsIsisExtSysInstance,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsIsisDistInOutRouteMapName,
                                           INT4 i4FsIsisDistInOutRouteMapType,
                                           INT4
                                           i4TestValFsIsisDistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    tIsisSysContext    *pContext;
    INT1                i1Exists = 0;
    INT1                i1Match = 0;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if (pContext == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pFsIsisDistInOutRouteMapName == NULL
        || pFsIsisDistInOutRouteMapName->i4_Length <= 0
        || pFsIsisDistInOutRouteMapName->i4_Length > RMAP_MAX_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTANCE)

    {
        if (pContext->pDistanceFilterRMap != NULL)
        {
            i1Exists = 1;
            if (CmpFilterRMapName
                (pContext->pDistanceFilterRMap,
                 pFsIsisDistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else if (i4FsIsisDistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (pContext->pDistributeInFilterRMap != NULL)
        {
            i1Exists = 1;
            if (CmpFilterRMapName
                (pContext->pDistributeInFilterRMap,
                 pFsIsisDistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsIsisDistInOutRouteMapRowStatus)
    {
        case ACTIVE:
        case DESTROY:
            if (i1Match)
            {
                return SNMP_SUCCESS;
            }
            else if (i1Exists)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            }
            break;
        case CREATE_AND_WAIT:
            if (!i1Exists)
            {
                return SNMP_SUCCESS;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;
    }

    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsIsisDistInOutRouteMapName);
    UNUSED_PARAM (i4FsIsisDistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFsIsisDistInOutRouteMapRowStatus);
    UNUSED_PARAM (i4FsIsisExtSysInstance);
    return SNMP_SUCCESS;
#endif /* ROUTEMAP_WANTED */
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIsisDistInOutRouteMapTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisDistInOutRouteMapName
                FsIsisDistInOutRouteMapType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisDistInOutRouteMapTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIsisPreferenceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisPreferenceTable
 Input       :  The Indices
                FsIsisExtSysInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisPreferenceTable (INT4 i4FsIsisExtSysInstance)
{
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisPreferenceTable"
             "FsIsisPreferenceTable () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance,
                               &pContext) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisPreferenceTable"
             "FsIsisPreferenceTable () \n"));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisPreferenceTable
 Input       :  The Indices
                FsIsisExtSysInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisPreferenceTable (INT4 *pi4FsIsisExtSysInstance)
{
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexFsIsisPreferenceTable () \n"));

    *pi4FsIsisExtSysInstance = 0;

    if (IsisCtrlGetSysContext (*pi4FsIsisExtSysInstance,
                               &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable ((UINT4) *pi4FsIsisExtSysInstance,
                                            (UINT4 *) pi4FsIsisExtSysInstance)
            == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                     "Index with the given Indices \n"));

            return SNMP_FAILURE;
        }
        else if (IsisCtrlGetSysContext (*pi4FsIsisExtSysInstance, &pContext)
                 == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                     "exist for the Index, %d\n", *pi4FsIsisExtSysInstance));
            return SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexFsIsisPreferenceTable () \n"));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisPreferenceTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisPreferenceTable (INT4 i4FsIsisExtSysInstance,
                                      INT4 *pi4NextFsIsisExtSysInstance)
{
    tIsisSysContext    *pContext;

    if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4FsIsisExtSysInstance,
                                        (UINT4 *) pi4NextFsIsisExtSysInstance)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next"
                 "Index with the given Indices \n"));

        return SNMP_FAILURE;
    }
    else if (IsisCtrlGetSysContext (*pi4NextFsIsisExtSysInstance, &pContext)
             == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", *pi4NextFsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisPreferenceValue
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisPreferenceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisPreferenceValue (INT4 i4FsIsisExtSysInstance,
                             INT4 *pi4RetValFsIsisPreferenceValue)
{
    tIsisSysContext    *pContext;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    *pi4RetValFsIsisPreferenceValue = pContext->u1Distance;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisPreferenceRowStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisPreferenceRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisPreferenceRowStatus (INT4 i4FsIsisExtSysInstance,
                                 INT4 *pi4RetValFsIsisPreferenceRowStatus)
{
    tIsisSysContext    *pContext;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsIsisPreferenceRowStatus = ACTIVE;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisPreferenceValue
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisPreferenceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisPreferenceValue (INT4 i4FsIsisExtSysInstance,
                             INT4 i4SetValFsIsisPreferenceValue)
{
    tIsisSysContext    *pContext = NULL;
    UINT1               u1OldDistance = 0;
    tIsisEvtDistChange  *pDistanceChange = NULL;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }
    u1OldDistance = pContext->u1Distance;

    if (i4SetValFsIsisPreferenceValue == 0)
    {
        pContext->u1Distance = ISIS_DEFAULT_PREFERENCE;
    }
    else
    {
        pContext->u1Distance = (UINT1) i4SetValFsIsisPreferenceValue;
    }
    if (u1OldDistance != (UINT1) i4SetValFsIsisPreferenceValue)
    {
        pDistanceChange = (tIsisEvtDistChange *)ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtDistChange));
        
        if (pDistanceChange != NULL)
        {
            pDistanceChange->u1EvtID = ISIS_EVT_DISTANCE_CHANGE;
            IsisUtlSendEvent (pContext, (UINT1 *) pDistanceChange,sizeof (tIsisEvtDistChange));
        }
        else
        {
            PANIC ((ISIS_LGST, "NMP <P> : Could not send ""ISIS_EVT_DISTANCE_CHANGE !!!\n"));
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsIsisPreferenceRowStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisPreferenceRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisPreferenceRowStatus (INT4 i4FsIsisExtSysInstance,
                                 INT4 i4SetValFsIsisPreferenceRowStatus)
{
    tIsisSysContext    *pContext;
    UNUSED_PARAM (i4SetValFsIsisPreferenceRowStatus);

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisPreferenceValue
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisPreferenceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisPreferenceValue (UINT4 *pu4ErrorCode,
                                INT4 i4FsIsisExtSysInstance,
                                INT4 i4TestValFsIsisPreferenceValue)
{
    tIsisSysContext    *pContext;

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if (i4TestValFsIsisPreferenceValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisPreferenceRowStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object
                testValFsIsisPreferenceRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisPreferenceRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsIsisExtSysInstance,
                                    INT4 i4TestValFsIsisPreferenceRowStatus)
{
    tIsisSysContext    *pContext;
    if ((i4TestValFsIsisPreferenceRowStatus < ISIS_ACTIVE) ||
        (i4TestValFsIsisPreferenceRowStatus > ISIS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;    /*silvercreek */
        return SNMP_FAILURE;

    }

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIsisPreferenceTable
 Input       :  The Indices
                FsIsisExtSysInstance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisPreferenceTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisRestartState
 Input       :  The Indices

                The Object
                retValFsIsisRestartState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisRestartState (INT4 *pi4RetValFsIsisRestartState)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisRestartState () \n"));

    *pi4RetValFsIsisRestartState = gu1IsisGrState;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisRestartState ()  \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisRRDAdminStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisRRDAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisRRDAdminStatus (INT4 i4FsIsisExtSysInstance,
                            INT4 *pi4RetValFsIsisRRDAdminStatus)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisRRDAdminStatus () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisRRDAdminStatus = (INT4) pContext->RRDInfo.u1RRDStatus;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisRRDAdminStatus ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisRRDProtoMaskForEnable
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisRRDProtoMaskForEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisRRDProtoMaskForEnable (INT4 i4FsIsisExtSysInstance,
                                   INT4 *pi4RetValFsIsisRRDProtoMaskForEnable)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisRRDProtoMaskForEnable () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisRRDProtoMaskForEnable =
            (INT4) pContext->RRDInfo.u4RRDSrcProtoMask;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisRRDProtoMaskForEnable ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisRRDProtoMaskForDisable
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisRRDProtoMaskForDisable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisRRDProtoMaskForDisable (INT4 i4FsIsisExtSysInstance,
                                    INT4 *pi4RetValFsIsisRRDProtoMaskForDisable)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisRRDProtoMaskForDisable () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisRRDProtoMaskForDisable =
            (INT4) (ISIS_RRD_DISABLE_MASK_VALUE (pContext));
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetFsIsisRRDProtoMaskForDisable ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFsIsisRRDRouteMapName
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisRRDRouteMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisRRDRouteMapName (INT4 i4FsIsisExtSysInstance,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsIsisRRDRouteMapName)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisRRDRouteMapName() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        pRetValFsIsisRRDRouteMapName->i4_Length =
            (INT4) STRLEN (pContext->RRDInfo.au1RRDRMapName);
        MEMCPY (pRetValFsIsisRRDRouteMapName->pu1_OctetList,
                pContext->RRDInfo.au1RRDRMapName,
                pRetValFsIsisRRDRouteMapName->i4_Length);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisRRDRouteMapName()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisRRDImportLevel
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisRRDImportLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisRRDImportLevel (INT4 i4FsIsisExtSysInstance,
                            INT4 *pi4RetValFsIsisRRDImportLevel)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisRRDImportLevel = pContext->RRDInfo.u1RRDImportType;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysDynHostNameSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisExtSysDynHostNameSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysDynHostNameSupport (INT4 i4FsIsisExtSysInstance,
                                      INT4
                                      *pi4RetValFsIsisExtSysDynHostNameSupport)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisExtSysDynHostNameSupport =
            pContext->u1IsisDynHostNmeSupport;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisRRDAdminStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisRRDAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisRRDAdminStatus (INT4 i4FsIsisExtSysInstance,
                            INT4 i4SetValFsIsisRRDAdminStatus)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        pContext->RRDInfo.u1RRDStatus = (UINT1) i4SetValFsIsisRRDAdminStatus;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisRRDProtoMaskForEnable
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisRRDProtoMaskForEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisRRDProtoMaskForEnable (INT4 i4FsIsisExtSysInstance,
                                   INT4 i4SetValFsIsisRRDProtoMaskForEnable)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisRRDProtoMaskForEnable() \n"));

    NMP_PT ((ISIS_LGST, "NMP <E> : Processing RRD Proto Enable\n"));
    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        pContext->RRDInfo.u4RRDSrcProtoMask |=
            (UINT4) i4SetValFsIsisRRDProtoMaskForEnable;
        IsisSendingMessageToRRDQueue (pContext,
                                      (UINT4)
                                      i4SetValFsIsisRRDProtoMaskForEnable,
                                      (UINT1) RTM_REDISTRIBUTE_ENABLE_MESSAGE);

        IsisSendingMessageToRRDQueue6 (pContext,
                                       (UINT4)
                                       i4SetValFsIsisRRDProtoMaskForEnable,
                                       (UINT1) RTM_REDISTRIBUTE_ENABLE_MESSAGE);
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisRRDProtoMaskForEnable()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisRRDProtoMaskForDisable
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisRRDProtoMaskForDisable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisRRDProtoMaskForDisable (INT4 i4FsIsisExtSysInstance,
                                    INT4 i4SetValFsIsisRRDProtoMaskForDisable)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtRRDProtoDisable *pRRDProtoDisable = NULL;
    INT4                i4TempMask = 0;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisRRDProtoMaskForDisable() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        if (i4SetValFsIsisRRDProtoMaskForDisable != ISIS_IMPORT_ALL)
        {
            i4TempMask = (pContext->RRDInfo.u4RRDSrcProtoMask &
                          i4SetValFsIsisRRDProtoMaskForDisable);
            if (i4TempMask == 0)
            {
                return SNMP_SUCCESS;
            }
            i4SetValFsIsisRRDProtoMaskForDisable = i4TempMask;
        }
        pContext->RRDInfo.u4RRDSrcProtoMask &=
            (~(UINT4) i4SetValFsIsisRRDProtoMaskForDisable);
        IsisSendingMessageToRRDQueue (pContext,
                                      (UINT4)
                                      i4SetValFsIsisRRDProtoMaskForDisable,
                                      RTM_REDISTRIBUTE_DISABLE_MESSAGE);

        IsisSendingMessageToRRDQueue6 (pContext,
                                       (UINT4)
                                       i4SetValFsIsisRRDProtoMaskForDisable,
                                       RTM_REDISTRIBUTE_DISABLE_MESSAGE);
        pRRDProtoDisable =
            (tIsisEvtRRDProtoDisable *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                        sizeof
                                                        (tIsisEvtRRDProtoDisable));

        if (pRRDProtoDisable != NULL)
        {
            pRRDProtoDisable->u1EvtID = ISIS_EVT_RRD_PROTO_DISABLE;
            pRRDProtoDisable->u2ProtoMask =
                (UINT2) i4SetValFsIsisRRDProtoMaskForDisable;

            pRRDProtoDisable->u1Level = pContext->RRDInfo.u1RRDImportType;

            IsisUtlSendEvent (pContext, (UINT1 *) pRRDProtoDisable,
                              sizeof (tIsisEvtRRDProtoDisable));
        }
        else
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Failed to post event for no redistribution  \n"
                     "NMP <X> : Exiting nmhSetFsIsisRRDProtoMaskForDisable()\n"));
            return SNMP_FAILURE;
        }
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisRRDProtoMaskForDisable()\n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisRRDRouteMapName
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisRRDRouteMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisRRDRouteMapName (INT4 i4FsIsisExtSysInstance,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsIsisRRDRouteMapName)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4Len = 0;
    UINT4               u4RMapLen = 0;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisRRDRouteMapName() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        if (pSetValFsIsisRRDRouteMapName->i4_Length != 0)
        {
            u4RMapLen = (UINT4) pSetValFsIsisRRDRouteMapName->i4_Length;
            u4Len = (u4RMapLen < sizeof (pContext->RRDInfo.au1RRDRMapName) ?
                     u4RMapLen : sizeof (pContext->RRDInfo.au1RRDRMapName) - 1);
            STRNCPY (pContext->RRDInfo.au1RRDRMapName,
                     pSetValFsIsisRRDRouteMapName->pu1_OctetList, u4Len);
            pContext->RRDInfo.au1RRDRMapName[u4Len] = '\0';
        }
        else
        {
            MEMSET (pContext->RRDInfo.au1RRDRMapName, 0,
                    sizeof (pContext->RRDInfo.au1RRDRMapName));
        }
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisRRDRouteMapName()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetFsIsisRRDImportLevel
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisRRDImportLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisRRDImportLevel (INT4 i4FsIsisExtSysInstance,
                            INT4 i4SetValFsIsisRRDImportLevel)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtImportLevelChange *pRRDLevelChg = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisRRDImportLevel() \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        if ((pContext->RRDInfo.u1RRDImportType != 0) &&
            (pContext->RRDInfo.u1RRDImportType != i4SetValFsIsisRRDImportLevel)
            && (i4SetValFsIsisRRDImportLevel != 0))
        {
            pRRDLevelChg = (tIsisEvtImportLevelChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                sizeof (tIsisEvtImportLevelChange));

            if (pRRDLevelChg != NULL)
            {
                pRRDLevelChg->u1EvtID = ISIS_EVT_IMPORT_LEVEL_CHG;
                pRRDLevelChg->u1PrevImportLevel =
                    pContext->RRDInfo.u1RRDImportType;
                pRRDLevelChg->u1ImportLevel =
                    (UINT1) i4SetValFsIsisRRDImportLevel;

                IsisUtlSendEvent (pContext, (UINT1 *) pRRDLevelChg,
                                  sizeof (tIsisEvtImportLevelChange));
            }
            else
            {
                NMP_EE ((ISIS_LGST,
                         "NMP <X> : Failed to post event Import Level Change \n"
                         "NMP <X> : Exiting nmhSetFsIsisRRDImportLevel()\n"));
                return SNMP_FAILURE;
            }
        }
        pContext->RRDInfo.u1RRDImportType =
            (UINT1) i4SetValFsIsisRRDImportLevel;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisRRDImportLevel()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisExtSysDynHostNameSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisExtSysDynHostNameSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisExtSysDynHostNameSupport (INT4 i4FsIsisExtSysInstance,
                                      INT4
                                      i4SetValFsIsisExtSysDynHostNameSupport)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4Status = 0;
    UINT4               u4Size = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisMsg            IsisMsg;
    tIsisQBuf          *pBuf = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetFsIsisExtSysDynHostNameSupport() \n"));
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_SUCCESS)
    {
        pContext->u1IsisDynHostNmeSupport =
            i4SetValFsIsisExtSysDynHostNameSupport;
        i1ErrCode = SNMP_SUCCESS;
        u4Size = sizeof (u4Status);
        u4Status =
            (i4SetValFsIsisExtSysDynHostNameSupport ==
             ISIS_DYNHOSTNME_ENABLE) ? ISIS_CMD_ADD : ISIS_CMD_DELETE;
        pBuf = (tIsisQBuf *) ISIS_MEM_ALLOC (ISIS_BUF_CHNS, u4Size);
        if (pBuf == NULL)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Failed to allocate buffer for dynamic hostname post event \n"
                     "NMP <X> : Exiting nmhSetFsIsisExtSysDynHostNameSupport()\n"));
            return SNMP_FAILURE;
        }

    /*** copy status to offset 0 ***/
        ISIS_COPY_TO_CHAIN_BUF (pBuf, (UINT1 *) &u4Status, 0,
                                sizeof (u4Status));

        IsisMsg.u4CxtOrIfindex = pContext->u4SysInstIdx;
        IsisMsg.u4Length = u4Size;
        IsisMsg.pu1Msg = (UINT1 *) pBuf;
        IsisMsg.u1MsgType = ISIS_EVT_HOST_NME_SUPPORT;

        if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                gIsisCtrlQId, ISIS_SYS_INT_EVT) == ISIS_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Failed to post event for dynamic hostname  \n"
                     "NMP <X> : Exiting nmhSetFsIsisExtSysDynHostNameSupport()\n"));
        return SNMP_FAILURE;
    }    
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisRRDRouteMapName()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisRRDAdminStatus
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisRRDAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisRRDAdminStatus (UINT4 *pu4ErrorCode,
                               INT4 i4FsIsisExtSysInstance,
                               INT4 i4TestValFsIsisRRDAdminStatus)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisRRDAdminStatus ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisRRDAdminStatus" " ()  \n"));
        return i1ErrCode;
    }
    if ((i4TestValFsIsisRRDAdminStatus != ISIS_RRD_ENABLE) &&
        (i4TestValFsIsisRRDAdminStatus != ISIS_RRD_DISABLE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Redistribution Status"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisRRDAdminStatus () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisRRDProtoMaskForEnable
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisRRDProtoMaskForEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisRRDProtoMaskForEnable (UINT4 *pu4ErrorCode,
                                      INT4 i4FsIsisExtSysInstance,
                                      INT4 i4TestValFsIsisRRDProtoMaskForEnable)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4Range = ISIS_ALL_PROTO_MASK;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisRRDProtoMaskForEnable ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisRRDProtoMaskForEnable"
                 " ()  \n"));
        return i1ErrCode;
    }
    if ((UINT4) i4TestValFsIsisRRDProtoMaskForEnable & (~u4Range))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Redistribution ProtoMask"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisRRDProtoMaskForEnable () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhTestv2FsIsisRRDProtoMaskForDisable
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisRRDProtoMaskForDisable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisRRDProtoMaskForDisable (UINT4 *pu4ErrorCode,
                                       INT4 i4FsIsisExtSysInstance,
                                       INT4
                                       i4TestValFsIsisRRDProtoMaskForDisable)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4Range = ISIS_ALL_PROTO_MASK;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisRRDProtoMaskForDisable ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisRRDProtoMaskForDisable"
                 " ()  \n"));
        return i1ErrCode;
    }
    if ((UINT4) i4TestValFsIsisRRDProtoMaskForDisable & (~u4Range))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Redistribution ProtoMask"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisRRDProtoMaskForDisable () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisRRDRouteMapName
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisRRDRouteMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisRRDRouteMapName (UINT4 *pu4ErrorCode,
                                INT4 i4FsIsisExtSysInstance,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsIsisRRDRouteMapName)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisRRDRouteMapName()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisRRDRouteMapName" " ()  \n"));
        return i1ErrCode;
    }
    if (pTestValFsIsisRRDRouteMapName->i4_Length > RMAP_MAX_NAME_LEN + 1)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisRRDRouteMapName" " ()  \n"));
        return SNMP_FAILURE;
    }
    if ((pTestValFsIsisRRDRouteMapName->i4_Length != 0) &&
        (STRLEN (pContext->RRDInfo.au1RRDRMapName) != 0))
    {
        /* Check for existence of Route Map with different Name.
         *          * If So, return failure, */
        if ((UINT4) pTestValFsIsisRRDRouteMapName->i4_Length !=
            STRLEN (pContext->RRDInfo.au1RRDRMapName))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting nmhTestv2FsIsisRRDRouteMapName"
                     " ()  \n"));
            return SNMP_FAILURE;
        }
        if (STRCMP (pContext->RRDInfo.au1RRDRMapName,
                    pTestValFsIsisRRDRouteMapName->pu1_OctetList) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting nmhTestv2FsIsisRRDRouteMapName"
                     " ()  \n"));
            return SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisRRDRouteMapName() \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisRRDImportLevel
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisRRDImportLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisRRDImportLevel (UINT4 *pu4ErrorCode,
                               INT4 i4FsIsisExtSysInstance,
                               INT4 i4TestValFsIsisRRDImportLevel)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisRRDImportLevel()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {

        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Context for "
                 "given Index \n"));

        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisRRDImportLevel () \n"));
        return i1ErrCode;
    }
    if (i4TestValFsIsisRRDImportLevel & (~ISIS_LEVEL12))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2FsIsisRRDImportLevel()  \n"));
        return SNMP_FAILURE;
    }
    if ((pContext->SysConfigs.u1SysType == ISIS_LEVEL1) &&
        i4TestValFsIsisRRDImportLevel != ISIS_LEVEL1)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ISIS_LEVEL_NOT_SUPPORT);
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisRRDImportLevel() \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisExtSysDynHostNameSupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisExtSysDynHostNameSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisExtSysDynHostNameSupport (UINT4 *pu4ErrorCode,
                                         INT4 i4FsIsisExtSysInstance,
                                         INT4
                                         i4TestValFsIsisExtSysDynHostNameSupport)
{
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisMultiTopologySupport () \n"));

    if ((IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext))
        != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsIsisExtSysDynHostNameSupport != ISIS_DYNHOSTNME_ENABLE) &&
        (i4TestValFsIsisExtSysDynHostNameSupport != ISIS_DYNHOSTNME_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2FsIsisExtSysDynHostNameSupport () \n"));

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIsisExtSysHostNameTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisExtSysIdToSysNameMappingTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysHostNameSysID
                FsIsisExtSysLevelIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisExtSysIdToSysNameMappingTable (INT4
                                                             i4FsIsisExtSysInstance,
                                                             tSNMP_OCTET_STRING_TYPE
                                                             *
                                                             pFsIsisExtSysHostNameSysID)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceFsIsisExtSysTable ()\n"));

    if (i4FsIsisExtSysInstance < 0)
    {
        i1ErrCode = SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsIsisExtSysHostNameSysID);
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceFsIsisExtSysTable () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisExtSysIdToSysNameMappingTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysHostNameSysID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsIsisExtSysIdToSysNameMappingTable (INT4
                                                     *pi4FsIsisExtSysInstance,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsIsisExtSysHostNameSysID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4SysInstance = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSysIdToSysNameMappingTable ()\n"));

    if (IsisCtrlGetSysContext (u4SysInstance, &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance,
                                            &u4SysInstance) == ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                     "with the given Indices \n"));
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting "
                     "nmhGetNextIndexFsIsisExtSysIdToSysNameMappingTable ()\n"));

            return (i1ErrCode);
        }
        else
        {
            if (IsisCtrlGetSysContext (u4SysInstance, &pContext)
                == ISIS_FAILURE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                         "the corresponding index \n"));
            }
        }
    }

    while (pContext != NULL)
    {
        if (((pIsisHostNmeNodeInfo =
              (tIsisHostNmeNodeInfo *) RBTreeGetFirst (pContext->
                                                       HostNmeList)) == NULL)
            || (pContext->u1IsisDynHostNmeSupport == ISIS_DYNHOSTNME_DISABLE))
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance, &u4SysInstance)
                == ISIS_FAILURE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                         "with the given Indices \n"));

                NMP_EE ((ISIS_LGST,
                         "NMP <X> : Exiting "
                         "nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable "
                         "()\n"));

                return (i1ErrCode);
            }
            else
            {
                if (IsisCtrlGetSysContext
                    (u4SysInstance, &pContext) == ISIS_FAILURE)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Could Not Find the Next Index "
                             "with the given Indices \n"));
                    NMP_EE ((ISIS_LGST,
                             "NMP <X> : Exiting "
                             "nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable "
                             "()\n"));
                    return (i1ErrCode);
                }

                continue;
            }
        }
        else
        {

            MEMSET (pFsIsisExtSysHostNameSysID->pu1_OctetList, 0,
                    ISIS_SYS_ID_LEN);
            MEMCPY (pFsIsisExtSysHostNameSysID->pu1_OctetList,
                    pIsisHostNmeNodeInfo->au1SysID,
                    ISIS_SYS_ID_LEN);
            pFsIsisExtSysHostNameSysID->i4_Length =
                (INT4) ISIS_SYS_ID_LEN;
            *pi4FsIsisExtSysInstance = u4SysInstance;
            i1ErrCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Eixting nmhGetFirstIndexFsIsisExtSysIdToSysNameMappingTable"
             " () \n"));
    return (i1ErrCode);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisExtSysIdToSysNameMappingTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisExtSysHostNameSysID
                nextFsIsisExtSysHostNameSysID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisExtSysIdToSysNameMappingTable (INT4 i4FsIsisExtSysInstance,
                                                    INT4
                                                    *pi4NextFsIsisExtSysInstance,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFsIsisExtSysHostNameSysID,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pNextFsIsisExtSysHostNameSysID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4SysInstance = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisHostNmeNodeInfo IsisHostNmeNodeInfo;
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo = NULL;
    tIsisHostNmeNodeInfo *pIsisNxtHostNmeNodeInfo = NULL;

    MEMSET (&IsisHostNmeNodeInfo, 0, sizeof (IsisHostNmeNodeInfo));

    if (IsisCtrlGetSysContext (u4SysInstance, &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance,
                                            &u4SysInstance) == ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                     "with the given Indices \n"));
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting "
                     "nmhGetNextIndexFsIsisExtSysIdToSysNameMappingTable ()\n"));

            return (i1ErrCode);
        }
        else
        {
            if (IsisCtrlGetSysContext (u4SysInstance, &pContext)
                == ISIS_FAILURE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                         "the corresponding index \n"));
            }
        }
    }

    MEMCPY (IsisHostNmeNodeInfo.au1SysID,
            (pFsIsisExtSysHostNameSysID->pu1_OctetList),
             pFsIsisExtSysHostNameSysID->i4_Length);

    pIsisHostNmeNodeInfo =
        (tIsisHostNmeNodeInfo *) RBTreeGet (pContext->HostNmeList,
                                            &IsisHostNmeNodeInfo);

    if (pIsisHostNmeNodeInfo == NULL)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                 "the corresponding index \n"));

        return SNMP_FAILURE;
    }

    while (pContext != NULL)
    {
        pIsisNxtHostNmeNodeInfo =
            (tIsisHostNmeNodeInfo *) RBTreeGetNext (pContext->HostNmeList,
                                                    pIsisHostNmeNodeInfo, NULL);

        if (pIsisNxtHostNmeNodeInfo == NULL)
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4SysInstance, &u4SysInstance)
                == ISIS_FAILURE)
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                         "with the given Indices \n"));
                NMP_EE ((ISIS_LGST,
                         "NMP <X> : Exiting "
                         "nmhGetNextIndexFsIsisExtSysIdToSysNameMappingTable ()\n"));

                return (i1ErrCode);
            }
            else
            {
                if (IsisCtrlGetSysContext (u4SysInstance, &pContext)
                    == ISIS_FAILURE)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                             "the corresponding index \n"));
                }
                else
                {
                    pIsisNxtHostNmeNodeInfo =
                        (tIsisHostNmeNodeInfo *) RBTreeGetFirst (pContext->
                                                                 HostNmeList);
                    if (pIsisNxtHostNmeNodeInfo != NULL)
                    {
                        *pi4NextFsIsisExtSysInstance = (INT4) u4SysInstance;
                        MEMSET (pNextFsIsisExtSysHostNameSysID->pu1_OctetList,
                                0, ISIS_SYS_ID_LEN);
                        MEMCPY (pNextFsIsisExtSysHostNameSysID->pu1_OctetList,
                                pIsisNxtHostNmeNodeInfo->au1SysID, ISIS_SYS_ID_LEN);
                        pNextFsIsisExtSysHostNameSysID->i4_Length =
                            (INT4) ISIS_SYS_ID_LEN;
                        i1ErrCode = SNMP_SUCCESS;
                        break;
                    }
                }
                continue;
            }
        }
        else
        {
            *pi4NextFsIsisExtSysInstance = (INT4) u4SysInstance;
            MEMSET (pNextFsIsisExtSysHostNameSysID->pu1_OctetList, 0,
                    ISIS_SYS_ID_LEN);
            MEMCPY (pNextFsIsisExtSysHostNameSysID->pu1_OctetList,
                    pIsisNxtHostNmeNodeInfo->au1SysID, ISIS_SYS_ID_LEN);
            pNextFsIsisExtSysHostNameSysID->i4_Length =
                (INT4) ISIS_SYS_ID_LEN;
            i1ErrCode = SNMP_SUCCESS;
            break;

        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting "
             "nmhGetNextIndexFsIsisExtSysIdToSysNameMappingTable ()\n"));

    return (i1ErrCode);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisExtSysRouterHostName
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisExtSysHostNameSysID

                The Object 
                retValFsIsisExtSysHostName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisExtSysRouterHostName (INT4 i4FsIsisExtSysInstance,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsIsisExtSysHostNameSysID,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsIsisExtSysHostName)
{
    tIsisHostNmeNodeInfo IsisHostNmeNodeInfo;
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo = NULL;
    tIsisSysContext    *pContext = NULL;

    MEMSET (&IsisHostNmeNodeInfo, 0, sizeof (IsisHostNmeNodeInfo));

    if (IsisCtrlGetSysContext (i4FsIsisExtSysInstance, &pContext) ==
        ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find instance for"
                 "the corresponding index \n"));
    }

    MEMCPY (IsisHostNmeNodeInfo.au1SysID,
            (pFsIsisExtSysHostNameSysID->pu1_OctetList),
            pFsIsisExtSysHostNameSysID->i4_Length);

    pIsisHostNmeNodeInfo =
        (tIsisHostNmeNodeInfo *) RBTreeGet (pContext->HostNmeList,
                                            &IsisHostNmeNodeInfo);

    if (pIsisHostNmeNodeInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pRetValFsIsisExtSysHostName->pu1_OctetList, 0, ISIS_HSTNME_MAX_LEN);
    MEMCPY (pRetValFsIsisExtSysHostName->pu1_OctetList,
            pIsisHostNmeNodeInfo->au1HostNme,
            STRLEN (pIsisHostNmeNodeInfo->au1HostNme));
    pRetValFsIsisExtSysHostName->i4_Length =
        (INT4) STRLEN (pIsisHostNmeNodeInfo->au1HostNme);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsIsisRRDMetricTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsIsisRRDMetricTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisRRDMetricProtocolId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsIsisRRDMetricTable (INT4 i4FsIsisExtSysInstance,
                                              INT4 i4FsIsisRRDMetricProtocolId)
{
    tIsisSysContext    *pContext;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhValidateIndexInstanceFsIsisRRDMetricTable"
             " () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }
    if ((i4FsIsisRRDMetricProtocolId > ISIS_MAX_METRIC_INDEX) ||
        (i4FsIsisRRDMetricProtocolId < ISIS_METRIC_INDEX_STATIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : wrong ProtocolIndex "));
        return SNMP_FAILURE;

    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhValidateIndexInstanceFsIsisRRDMetricTable"
             " () \n"));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsIsisRRDMetricTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisRRDMetricProtocolId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsIsisRRDMetricTable (INT4 *pi4FsIsisExtSysInstance,
                                      INT4 *pi4FsIsisRRDMetricProtocolId)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFirstIndexFsIsisRRDMetricTable"
             " () \n"));

    if (UtilIsisGetFirstCxtId ((UINT4 *) pi4FsIsisExtSysInstance) ==
        ISIS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4FsIsisRRDMetricProtocolId = 0;
    NMP_EE ((ISIS_LGST, "NMP <X> : Eixting nmhGetFirstIndexFsIsisRRDMetricTable"
             " () \n"));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsIsisRRDMetricTable
 Input       :  The Indices
                FsIsisExtSysInstance
                nextFsIsisExtSysInstance
                FsIsisRRDMetricProtocolId
                nextFsIsisRRDMetricProtocolId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsIsisRRDMetricTable (INT4 i4FsIsisExtSysInstance,
                                     INT4 *pi4NextFsIsisExtSysInstance,
                                     INT4 i4FsIsisRRDMetricProtocolId,
                                     INT4 *pi4NextFsIsisRRDMetricProtocolId)
{
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetNextIndexFsIsisRRDMetricTable"
             " () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }
    if (i4FsIsisRRDMetricProtocolId == ISIS_MAX_METRIC_INDEX)
    {
        if (UtilIsisGetNextCxtId ((UINT4) i4FsIsisExtSysInstance,
                                  (UINT4 *) pi4NextFsIsisExtSysInstance) ==
            ISIS_FAILURE)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4NextFsIsisRRDMetricProtocolId = 0;
            return SNMP_SUCCESS;
        }
    }
    *pi4NextFsIsisRRDMetricProtocolId = i4FsIsisRRDMetricProtocolId + 1;
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetNextIndexFsIsisRRDMetricTable"
             " () \n"));
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsIsisRRDMetricValue
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisRRDMetricProtocolId

                The Object 
                retValFsIsisRRDMetricValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisRRDMetricValue (INT4   i4FsIsisExtSysInstance,
                            INT4   i4FsIsisRRDMetricProtocolId,
                            UINT4 *pu4RetValFsIsisRRDMetricValue)
{
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisRRDMetricValue"
             " () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }
    if (i4FsIsisRRDMetricProtocolId > ISIS_MAX_METRIC_INDEX)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsIsisRRDMetricValue =
        pContext->RRDInfo.au4RRDMetric[i4FsIsisRRDMetricProtocolId];
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisRRDMetricValue"
             " () \n"));
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsIsisRRDMetricValue
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisRRDMetricProtocolId

                The Object 
                setValFsIsisRRDMetricValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisRRDMetricValue (INT4  i4FsIsisExtSysInstance,
                            INT4  i4FsIsisRRDMetricProtocolId,
                            UINT4 u4SetValFsIsisRRDMetricValue)
{
    tIsisSysContext    *pContext = NULL;
    tIsisEvtMetricChange *pRRDMetricChg = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisRRDMetricValue"
             " () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }
    if (i4FsIsisRRDMetricProtocolId > ISIS_MAX_METRIC_INDEX)
    {
        return SNMP_FAILURE;
    }
    if ((u4SetValFsIsisRRDMetricValue != 0)  && 
        (u4SetValFsIsisRRDMetricValue != 
         pContext->RRDInfo.au4RRDMetric[i4FsIsisRRDMetricProtocolId]))
    {
        pRRDMetricChg = (tIsisEvtMetricChange *)
             ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                        sizeof (tIsisEvtMetricChange));
    
        if (pRRDMetricChg != NULL)
        {
            pRRDMetricChg->u1EvtID = ISIS_EVT_RRD_METRIC_CHG;
            pRRDMetricChg->u1MetricIndex =
                (UINT1) i4FsIsisRRDMetricProtocolId;
            pRRDMetricChg->u4Metric = u4SetValFsIsisRRDMetricValue;
            IsisUtlSendEvent (pContext, (UINT1 *) pRRDMetricChg,
                              sizeof (tIsisEvtMetricChange));
        }
        else
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Failed to post event RRD Metric Change \n"
                     "NMP <X> : Exiting nmhSetFsIsisRRDMetricValue \n"));
            return SNMP_FAILURE;
        }
    }

    pContext->RRDInfo.au4RRDMetric[i4FsIsisRRDMetricProtocolId] =
              u4SetValFsIsisRRDMetricValue;
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisRRDMetricValue"
             " () \n"));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsIsisRRDMetricValue
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisRRDMetricProtocolId

                The Object 
                testValFsIsisRRDMetricValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisRRDMetricValue (UINT4 *pu4ErrorCode,
                               INT4   i4FsIsisExtSysInstance,
                               INT4   i4FsIsisRRDMetricProtocolId,
                               UINT4  u4TestValFsIsisRRDMetricValue)
{
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2FsIsisRRDMetricValue"
             " () \n"));

    if (IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext)
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }
    if ((i4FsIsisRRDMetricProtocolId > ISIS_MAX_METRIC_INDEX) ||
        (i4FsIsisRRDMetricProtocolId < ISIS_METRIC_INDEX_STATIC))
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisRRDMetricValue"
                 " () \n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* If Multi-topology is enabled the range is 1 to 4261412864,
     * else the range is 1 to 63 */ 
    if (((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) && 
          (u4TestValFsIsisRRDMetricValue > ISIS_MAX_METRIC_MT)) || 
         ((pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC) && 
          (u4TestValFsIsisRRDMetricValue > ISIS_MAX_METRIC_ST)))

    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisRRDMetricValue"
                 " () \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2FsIsisRRDMetricValue"
             " () \n"));
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsIsisRRDMetricTable
 Input       :  The Indices
                FsIsisExtSysInstance
                FsIsisRRDMetricProtocolId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIsisRRDMetricTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
/****************************************************************************
* Function    :  nmhSetFsIsisDotCompliance
* Input       :  The Indices
*                FsIsisExtSysInstance
*
*                The Object
*                setValFsIsisDotCompliance
* Output      :  The Set Low Lev Routine Take the Indices &
*                Sets the Value accordingly.
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsIsisDotCompliance(INT4 i4FsIsisExtSysInstance 
                                         , INT4 i4SetValFsIsisDotCompliance)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetFsIsisDotCompliance ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif    
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        if(pContext->bNetIdDotCompliance == (tBool) i4SetValFsIsisDotCompliance)
        {
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            pContext->bNetIdDotCompliance = (tBool) i4SetValFsIsisDotCompliance;
            i1ErrCode = SNMP_SUCCESS;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetFsIsisDotCompliance() \n"));
    return i1ErrCode;

}

/****************************************************************************
 *  Function    :  nmhSetFsIsisMetricStyle
 *  Input       :  The Indices
 *                 FsIsisExtSysInstance
 *  
 *                 The Object
 *                 setValFsIsisMetricStyle
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT1 nmhSetFsIsisMetricStyle(INT4 i4FsIsisExtSysInstance , INT4 i4SetValFsIsisMetricStyle)
{
    tIsisSysContext    *pContext = NULL;
    INT4                i4ExistState = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT4               u4Error = 0;
    UINT1               u1PrevValue = 0;


    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetFsIsisMetricStyle ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((IsisCtrlGetSysContext (u4InstIdx, &pContext)) 
         != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if (pContext->u1MetricStyle == (UINT1) i4SetValFsIsisMetricStyle)
    {
        return SNMP_SUCCESS;
    }

    if (nmhGetIsisSysExistState (i4FsIsisExtSysInstance, &i4ExistState) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;

    }
    if (i4ExistState == ISIS_ACTIVE)

    {
        /* Made the Exist state down if its active */
        if (nmhTestv2IsisSysExistState
            (&u4Error, i4FsIsisExtSysInstance, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetIsisSysExistState (i4FsIsisExtSysInstance, NOT_IN_SERVICE) !=
            SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    u1PrevValue = pContext->u1MetricStyle;
    pContext->u1MetricStyle =
        (UINT1) i4SetValFsIsisMetricStyle;

    IsisUtlUpdateMetricStyle (pContext);
    IsisUtlUpdateMetrics (pContext);

    if (i4ExistState == ISIS_ACTIVE)
    {
        /* Move the admin state to Active only if it made down previously */
        if (nmhTestv2IsisSysExistState
            (&u4Error, i4FsIsisExtSysInstance, ISIS_ACTIVE) == SNMP_FAILURE)
        {
            pContext->u1MetricStyle = u1PrevValue;
            IsisUtlUpdateMetricStyle (pContext);
            IsisUtlUpdateMetrics (pContext);
            return SNMP_FAILURE;
        }

        if (nmhSetIsisSysExistState (i4FsIsisExtSysInstance, ISIS_ACTIVE) !=
            SNMP_SUCCESS)
        {
            pContext->u1MetricStyle = u1PrevValue;
            IsisUtlUpdateMetricStyle (pContext);
            IsisUtlUpdateMetrics (pContext);
            return SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetFsIsisMetricStyle ()\n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
* Function    :  nmhTestv2FsIsisDotCompliance
* Input       :  The Indices
*                FsIsisExtSysInstance
*
*                The Object
*                testValFsIsisDotCompliance
* Output      :  The Test Low Lev Routine Take the Indices &
*                Test whether that Value is Valid Input for Set.
*                Stores the value of error code in the Return val
* Error Codes :  The following error codes are to be returned
*                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
*                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
*                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
*                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
*                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1 nmhTestv2FsIsisDotCompliance(UINT4 *pu4ErrorCode 
            , INT4 i4FsIsisExtSysInstance , INT4 i4TestValFsIsisDotCompliance)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = 0;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    UINT1               au1Blankspace[ISIS_SYS_ID_LEN];
    tIsisSysContext    *pContext = NULL;

    MEMSET (au1Blankspace,0 , ISIS_SYS_ID_LEN);

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2FsIsisDotCompliance ()\n"));
    if (gu1IsisStatus != ISIS_IS_UP)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System NOT in ISIS_IS_UP Status !!!\n"));
        return SNMP_FAILURE;
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Context does not "
                 "exist for the Index, %d\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisDotCompliance != ISIS_DOT_TRUE)
        && (i4TestValFsIsisDotCompliance != ISIS_DOT_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
    }

    if (MEMCMP (pContext->SysConfigs.au1SysID, au1Blankspace,
                ISIS_SYS_ID_LEN) != 0)
    {
        if(pContext->bNetIdDotCompliance != i4TestValFsIsisDotCompliance)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Different Compliance format exist for the Index\n"));
            return SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2FsIsisDotCompliance () \n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMetricStyle
 Input       :  The Indices
                FsIsisExtSysInstance
 
                The Object
                testValFsIsisMetricStyle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsIsisMetricStyle(UINT4 *pu4ErrorCode , INT4 i4FsIsisExtSysInstance , INT4 i4TestValFsIsisMetricStyle)
{
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2FsIsisMetricStyle ()\n"));

    if ((IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext))
        != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE) ;
        NMP_EE ((ISIS_LGST, "NMP <X>: Exiting nmhTestv2FsIsisMetricStyle ()\n"));
        return SNMP_FAILURE;
    }


    if ((pContext->u1IsisMTSupport == ISIS_TRUE) &&
        (i4TestValFsIsisMetricStyle != ISIS_STYLE_WIDE_METRIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Wide metric style alone is supported"
                "in multi-topology for the Index,  %u\n", i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        NMP_EE ((ISIS_LGST, "NMP <X>: Exiting nmhTestv2FsIsisMetricStyle ()\n"));
        return SNMP_FAILURE;

    }

    if ((i4TestValFsIsisMetricStyle != ISIS_STYLE_NARROW_METRIC)
        && (i4TestValFsIsisMetricStyle != ISIS_STYLE_WIDE_METRIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of MetricStyle \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        NMP_EE ((ISIS_LGST, "NMP <X>: Exiting nmhTestv2FsIsisMetricStyle ()\n"));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
} 

/****************************************************************************
* Function    :  nmhGetFsIsisDotCompliance
* Input       :  The Indices
*                FsIsisExtSysInstance
*
*                The Object
*                retValFsIsisDotCompliance
* Output      :  The Get Low Lev Routine Take the Indices &
*                store the Value requested in the Return val.
* Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisDotCompliance(INT4 i4FsIsisExtSysInstance 
                                        , INT4 *pi4RetValFsIsisDotCompliance)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFsIsisDotCompliance()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisDotCompliance = pContext-> bNetIdDotCompliance;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFsIsisDotCompliance () \n"));
    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFsIsisMetricStyle
 Input       :  The Indices
                FsIsisExtSysInstance
 
                The Object
                retValFsIsisMetricStyle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsIsisMetricStyle(INT4 i4FsIsisExtSysInstance , INT4 *pi4RetValFsIsisMetricStyle)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4FsIsisExtSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetFsIsisMetricStyle ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValFsIsisMetricStyle = (INT4)
            pContext->u1MetricStyle;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4FsIsisExtSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetFsIsisMetricStyle ()\n"));

    return i1ErrCode;

}
