/*******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isupdutl.c,v 1.26 2017/09/11 13:44:09 siva Exp $
 *
 * Description: This file contains the utility routines for update module
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

PRIVATE VOID        IsisUpdUpdateAuthInPDU (tIsisSysContext *, UINT1 *,
                                            tIsisLSPInfo *, UINT2 *, UINT1);
PRIVATE VOID        IsisUpdPruneAA (tIsisSysContext *, UINT1 *, UINT1);

/*******************************************************************************
 * Function    : IsisUpdChkAndTxSelfLSP ()
 * Description : This function 
 * Input (s)   : pContext - Pointer to System Context
 *             : pLSP     - Pointer to the Self LSP information which includes
 *                          various TLVs containing information pertaining to
 *                          the Local System
 *             : u1Level  - Level of LSP
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdChkAndTxSelfLSP (tIsisSysContext * pContext, tIsisLSPInfo * pLSP,
                        UINT1 u1Level)
{
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1               u1LSPFlag = ISIS_FALSE;
    UINT1               u1GenCnt = 0;
    UINT1              *pu1LSP = NULL;
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    UINT2               u2Offset = 0;
    UINT2               u2Len = 0;
    INT4                i4Ret = ISIS_FAILURE;
    tIsisLSPEntry      *pLSPRec = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisLSPEntry      *pFltLSPRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT1               u1AuthType = 0;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdChkAndTxSelfLSP ()\n"));

    if (pLSP == NULL)
    {
        WARNING ((ISIS_LGST, "UPD <W> : LSP To Be Transmitted Is NULL\n"));
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdChkAndTxSelfLSP ()\n"));
        return;
    }
    if ((pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER) &&
        (gu1IsisGrState != ISIS_GR_OVERLOAD))
    {
        return;
    }

    MEMSET (au1LSPId, '\0', ISIS_LSPID_LEN);
    MEMCPY (au1LSPId, pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
    au1LSPId[ISIS_SYS_ID_LEN] = pLSP->u1SNId;
    au1LSPId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN] = pLSP->u1LSPNum;

    /* Retrieve the LSP from either the Database or Transmission Queue
     */

    pLSPRec = IsisUpdGetLSP (pContext, au1LSPId, u1Level, &pPrevRec,
                             &pHashBkt, &u1Loc);
    u1GenCnt = (u1Level == ISIS_LEVEL1) ? pContext->SysL1Info.u1LSPGenCount
        : pContext->SysL2Info.u1LSPGenCount;

    /* Every Min LSP Generation Intreval, we check to see if the LSPs dirty flag
     * is set. If it is set (ISIS_MODIFIED), then we will delete the Self LSP
     * buffers from the Database, Construct the buffers afresh from the SelfLSP
     * Information, maintained as TLVs, and then insert them into the
     * transmission queue for transmission.
     */
    if (pLSP->u1DirtyFlag == ISIS_MODIFIED)
    {
        /* Construct the LSP buffer from scratch with the information available
         * in the SelfLSP information
         */
        ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Dirty Flag Modified For LSP ID [ %s ]\n", acLSPId));
        pLSP->u4SeqNum++;
        pLSP->u1DirtyFlag = ISIS_NOT_MODIFIED;
        pLSP->u2AuthStatus = ISIS_FALSE;

        /* If sequence number exceeds maximum seqence number, then disable 
         * the generation of Self LSPs by MaxAge + ZeroAgeLifeTime
         *
         * Refer Sec. 7.3.16.1
         */

        if (pLSP->u4SeqNum == ISIS_MAX_SEQNO)
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Max Seq. Number Reached For SelfLSP [ %u ]\n",
                     pLSP->u4SeqNum));
            IsisUpdProcMSNExceeded (pContext, u1Level, au1LSPId);
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdChkAndTxSelfLSP ()\n"));
            return;
        }

        pu1LSP = IsisUpdConstructSelfLSP (pContext, pLSP, u1Level, &u2Offset);

        if (pu1LSP != NULL)
        {
            if (((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST)) &&
                (pLSPRec != NULL))
            {
                /* We already have the old SelfLSP buffer in the Transmission
                 * queue. We have to delete the old information and add the
                 * newly constructed information to the TxQ
                 */

                ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE,
                                   ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec);

                ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *)
                               (((tIsisLSPTxEntry *) pLSPRec)->pLSPRec->
                                pu1LSP));

                ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec->pu1LSP = pu1LSP;

                /* LSU for the Standby node is sent as the Authentication
                 * information is updated in the newly constructed Self LSP
                 * Buffer. 
                 * IsisFltrLspLSU () function always expects a LSP Entry for the
                 * LSU to send. Hence the pFltLSPRec points to the LSP Entry of
                 * the pLSPRec->pLSPRec if the LSP is found in the TxQ.
                 */

                pFltLSPRec = ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec;
            }
            else if (((u1Loc == ISIS_LOC_DB) || (u1Loc == ISIS_LOC_DB_FIRST)) &&
                     (pLSPRec != NULL))
            {
                /* We already have the old SelfLSP buffer in the Database. We 
                 * have to delete the old information and add the
                 * newly constructed information to the Database and schedule
                 * the same for transmission
                 */

                ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE, pLSPRec);

                ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) (pLSPRec->pu1LSP));
                pLSPRec->pu1LSP = pu1LSP;
                pFltLSPRec = pLSPRec;
            }
            else                /* u1Loc is None */
            {
                /* This is a new information being constructed. This can happen
                 * since manager can configure new information any time which 
                 * may result in a new LSP buffer. In this case we allocate a
                 * new TxQ header and place the new SelfLSP buffer into the TxQ.
                 * We modify 'u1Loc' to ISIS_LOC_SELF to indicate the Scheduling
                 * routine that it must trigger a Lock Step Update since this 
                 * is a new SelfLSP buffer being added to the TxQ for the first
                 * time
                 */

                u1Loc = ISIS_LOC_SELF;
                pLSPRec = (tIsisLSPEntry *)
                    ISIS_MEM_ALLOC (ISIS_BUF_LDBE, sizeof (tIsisLSPEntry));
                if (pLSPRec == NULL)
                {
                    PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Record\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdChkAndTxSelfLSP ()\n"));
                    ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);
                    return;
                }
                MEMSET (pLSPRec, 0x00, sizeof (tIsisLSPEntry));
                pLSPRec->pu1LSP = pu1LSP;
                pFltLSPRec = pLSPRec;
                u1LSPFlag = ISIS_TRUE;
            }
        }
        else
        {
            WARNING ((ISIS_LGST, "UPD <W> : Unable To Construct SelfLSPs\n"));
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdChkAndTxSelfLSP ()\n"));
            return;
        }

        /* Authentication information is either added or deleted based on the 
         * 'u2AuthStatus' value in pLSPRec
         */

        IsisUpdUpdateAuthInPDU (pContext, pu1LSP, pLSP, &u2Offset, u1Level);
        IsisUpdAddCS (pu1LSP, u2Offset);

        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_ADD, pFltLSPRec);
#ifndef ISIS_FT_ENABLED
        UNUSED_PARAM (pFltLSPRec);
#endif
        if ((gu1IsisGrOverLoad == OVERLOAD)
            || (gu1IsisGrOverLoad == CLEAR_OVERLOAD)
            || (gu1IsisGrOverLoad == SELF_LSP))
        {
            if ((gu1IsisGrOverLoad == SELF_LSP) &&
                (pContext->u1IsisGRRestartStatus == ISIS_STARTING_ROUTER))
            {
                IsisUpdAddToDb (pContext, pLSPRec, u1Level);
            }
            else
            {
                if (u1LSPFlag == ISIS_TRUE)
                {
                    ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pLSPRec);
                }
            }
            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSP, u2Len);
            for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
                 pCktRec = pCktRec->pNext)
            {

                IsisTrfrTxData (pCktRec, pu1LSP, u2Len, u1Level, ISIS_HOLD_BUF,
                                ISIS_FALSE);
            }
            gu1IsisGrOverLoad = 0;
            return;
        }
        i4Ret =
            IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec, (VOID *) pLSPRec,
                                NULL, u1Loc, pHashBkt, ISIS_ALL_CKT);
        if (i4Ret == ISIS_MEM_FAILURE)
        {
            IsisUpdDeleteLSP (pContext, u1Level, au1LSPId);
        }
    }
    else if (u1GenCnt == ISIS_MAX_GEN_COUNT (pContext, u1Level))
    {
        /* We are at the Max LSP Generation Interval. We have to transmit
         * SelfLSPs irrespective of the Dirty Flags.
         */

        if (((u1Loc == ISIS_LOC_TX) || (u1Loc == ISIS_LOC_TX_FIRST)) &&
            (pLSPRec != NULL))
        {
            pu1LSP = ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec->pu1LSP;
        }
        else if (((u1Loc == ISIS_LOC_DB) || (u1Loc == ISIS_LOC_DB_FIRST)) &&
                 (pLSPRec != NULL))
        {
            pu1LSP = pLSPRec->pu1LSP;
        }
        else
        {
            /* This should never happen. Even if a new LSP buffer is added to
             * the SelfLSP information just before the timer fired, its Dirty
             * Flag would have been set and hence the 'if' condition above would
             * have taken care of the new buffer. If not the buffer must be
             * present either in TxQ or Database
             */
            if (pContext->u1IsisDynHostNmeSupport == ISIS_DYNHOSTNME_ENABLE)
            {
                MEMSET (au1LSPId, 0, sizeof (au1LSPId));
                if ((ISIS_LSPID_LEN >= STRLEN (gau1IsisHstNme))
                    && (ISIS_LSPID_LEN >= STRLEN (&pLSP->u1SNId))
                    && (ISIS_LSPID_LEN >= STRLEN (&pLSP->u1LSPNum)))
                {
                    STRNCPY (au1LSPId, gau1IsisHstNme, STRLEN (gau1IsisHstNme));
                    STRNCAT (au1LSPId, &(pLSP->u1SNId), STRLEN (&pLSP->u1SNId));
                    STRNCAT (au1LSPId, &(pLSP->u1LSPNum),
                             STRLEN (&pLSP->u1LSPNum));
                }
            }

            ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) ISIS_LSPID_LEN,
                               "UPD <W> : LSP Not Present In TxQ and DB\t",
                               ISIS_OCTET_STRING);
            return;
        }

        pLSP->u4SeqNum++;

        /* If sequence number exceeds maximum seqence number, then disable 
         * the generation of Self LSPs by MaxAge + ZeroAgeLifeTime
         *
         * Refer Sec. 7.3.16.1
         */

        if (pLSP->u4SeqNum == ISIS_MAX_SEQNO)
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Max Seq. Number Reached For SelfLSP [ %u ]\n",
                     pLSP->u4SeqNum));
            IsisUpdProcMSNExceeded (pContext, u1Level, au1LSPId);
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdChkAndTxSelfLSP ()\n"));
            return;
        }

        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSP, u2Offset);

        /* Authentication information is either added or deleted based on the 
         * 'u2AuthStatus' value in pLSPRec
         */
        u1AuthType = (UINT1) (((u1Level == ISIS_LEVEL1) ?
                               pContext->SysActuals.u1SysAreaAuthType :
                               pContext->SysActuals.u1SysDomainAuthType));
        if (u1AuthType == ISIS_AUTH_MD5)
        {
            if (((u1Level != ISIS_LEVEL2)
                 && (pContext->SysActuals.SysAreaTxPasswd.u1Len != 0))
                || ((u1Level != ISIS_LEVEL1)
                    && (pContext->SysActuals.SysDomainTxPasswd.u1Len != 0)))
            {
                if (pLSP->u2AuthStatus == ISIS_TRUE)
                {
                    ISIS_SET_LSP_SEQNO (pu1LSP, pLSP->u4SeqNum);
                    IsisUpdRemoveAuthFromLSP (pu1LSP, &u2Offset);
                    IsisUpdAddAuthToPDU (pContext, pu1LSP, &u2Offset, u1Level);
                }
            }
        }
        else
        {

            IsisUpdUpdateAuthInPDU (pContext, pu1LSP, pLSP, &u2Offset, u1Level);
        }
        IsisUpdModifyLSP (pContext, pLSPRec, ISIS_LSP_MAXAGE, pLSP->u4SeqNum,
                          u1Loc);
        i4Ret =
            IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec, (VOID *) pLSPRec,
                                NULL, u1Loc, pHashBkt, ISIS_ALL_CKT);
        if (i4Ret == ISIS_MEM_FAILURE)
        {
            IsisUpdDeleteLSP (pContext, u1Level, au1LSPId);
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdChkAndTxSelfLSP ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdConstructAndTxPSNP ()
 * Description : This function constructs and transmits PSNP on the specified
 *               circuit 
 * Input(s)    : pContext - Pointer to System Context
 *               ppPSNP   - Pointer to the List of TLVs
 *               pCktRec  - Pointer to the circuit record 
 *               u1Level  - The level of the PSNP
 * Output(s)   : ppPSNP   - Due to throttle restrictions we may not be able to
 *                          retrieve all TLVs and construct PSNPs. This routine
 *                          updates the pointer to point to the next of the TLV
 *                          that was last included in the PSNP buffer. Next PSNP
 *                          encoding will start from this LETLV onwards
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdConstructAndTxPSNP (tIsisSysContext * pContext, tIsisLETLV ** pPSNP,
                           tIsisCktEntry * pCktRec, UINT1 u1Level)
{
    UINT1               u1TLVLen = 0;
    UINT1              *pu1PSNP = NULL;
    UINT2               u2Len = 0;
    UINT2               u2Offset = 0;
    UINT2               u2BufSize = 0;
    tIsisLETLV         *pTLV = NULL;
    tIsisLETLV         *pNextTLV = NULL;
    tIsisCktLevel      *pCktLvlRec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdConstructAndTxPSNP ()\n"));

    pTLV = *pPSNP;

    /* Calculate the length of the TLVs present in the Circuit Level record 
     * Each TLV occupies the size of ISIS_LSP_ENTRY_TLV_LEN viz., 
     * RLT( 2 bytes), LSP ID (LSP ID Len), LSP Seq. Num (4 bytes), and 
     * LSP Checksum (2 bytes)
     */

    while (pTLV != NULL)
    {
        u2Len += ISIS_LSP_ENTRY_TLV_LEN;
        pTLV = pTLV->pNext;
    }

    /* Allocate memory for the entire PSNP Buffer, including the Common PDU 
     * Header, PSNP Specific Header
     */

    /* Reinitialise the pTLV pointer since we now start adding the TLVs into the
     * PSNP buffer
     */

    pTLV = *pPSNP;

    if (u1Level == ISIS_LEVEL1)
    {
        u2BufSize = (UINT2) pContext->SysActuals.u4SysOrigL1LSPBufSize;
        pCktLvlRec = pCktRec->pL1CktInfo;
    }
    else
    {
        u2BufSize = (UINT2) pContext->SysActuals.u4SysOrigL2LSPBufSize;
        pCktLvlRec = pCktRec->pL2CktInfo;
    }

    /* We have to reserve enough space for the authentication information
     * to be added. Hence adjust the Buffer size accordingly
     * Subtract Code (1 byte), Length (1byte), Auth Type (1 byte) and
     * finally the password length
     */

    if ((u1Level == ISIS_LEVEL1)
        && (pContext->SysActuals.SysAreaTxPasswd.u1Len != 0))
    {
        u2BufSize = (UINT2) (u2BufSize -
                             (3 + pContext->SysActuals.SysAreaTxPasswd.u1Len));
    }
    else if (pContext->SysActuals.SysDomainTxPasswd.u1Len != 0)
    {
        u2BufSize = (UINT2) (u2BufSize -
                             (3 +
                              pContext->SysActuals.SysDomainTxPasswd.u1Len));
    }

    /* Add the LETLVs one by one into the PSNP buffer
     */

    while (u2Len > 0)
    {
        pu1PSNP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_PSNP, u2BufSize);

        if (pu1PSNP == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : PSNP Buffer\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_PSNP);
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdConstructAndTxPSNP ()\n"));
            return;
        }

        /* Encode the Common header first
         */

        if (u1Level == ISIS_LEVEL1)
        {
            IsisUtlFillComHdr (pContext, pu1PSNP, (UINT1) ISIS_L1PSNP_PDU);
        }
        else
        {
            IsisUtlFillComHdr (pContext, pu1PSNP, (UINT1) ISIS_L2PSNP_PDU);
        }

        u2Offset = sizeof (tIsisComHdr);

        /* Skip the PDU length field. We will fill this once the encoding is
         * complete
         */

        u2Offset = (UINT2) (u2Offset + 2);

        ISIS_ASSIGN_STRING (pu1PSNP, u2Offset,
                            pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
        u2Offset = (UINT2) (u2Offset + ISIS_SYS_ID_LEN);

        /* Assign a zero byte value as the Circuit ID/Pseudo Node ID as per 
         * Sec 9.12 & 9.13 of ISO - 10589 - 1992Ver
         */

        ISIS_ASSIGN_1_BYTE (pu1PSNP, u2Offset, 0);

        u2Offset = (UINT2) (u2Offset + ISIS_PNODE_ID_LEN);

        /* Continue encoding TLVs into the PSNP buffer
         */

        while ((pTLV != NULL) && (u2Offset < u2BufSize))
        {
            if (((u1TLVLen + ISIS_LSP_ENTRY_TLV_LEN) > 255) || (u1TLVLen == 0))
            {
                /* A TLV can hold only upto 255 bytes. If u1TLVLen is '0' or 
                 * if 'u1TLVLen + ISIS_LSP_ENTRY_TLV_LEN' is greatr than 255, 
                 * then we are supposed to start with a new TLV.
                 */

                if ((u2Offset + ISIS_LSP_ENTRY_TLV_LEN + 2) > u2BufSize)
                {
                    /* Exceeding BufferSize, we may have to construct a new PSNP
                     * buffer.
                     */

                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Adding LETLV To PSNP - Max Buffer Size Reached - Max Size [ %u ], Current Size [ %u ]\n",
                             u2BufSize,
                             (u2Offset + ISIS_LSP_ENTRY_TLV_LEN + 2)));
                    break;
                }
                if (u1TLVLen != 0)
                {
                    /* Now that the previous TLV is completely filled, we shall
                     * fill the TLV length. TLV length must be filled at an 
                     * offset which is specified by u1TlvLenOffset.
                     */

                    *(pu1PSNP + u2Offset - u1TLVLen - 1) = u1TLVLen;
                }

                /* We can add a new TLV to the current PSNP buffer
                 * Fill the Code, Length of the TLV will be filled once the TLV
                 * is full or PSNP encoding is complete which ever happens first
                 */

                /* Resetting u1TLVLen here since a new TLV is being encoded
                 */

                u1TLVLen = 0;

                ISIS_ASSIGN_1_BYTE (pu1PSNP, u2Offset, ISIS_LSP_ENTRY_TLV);

                /* Skipping Length field
                 */

                u2Offset = (UINT2) (u2Offset + 2);
            }
            else if ((u2Offset + ISIS_LSP_ENTRY_TLV_LEN) > u2BufSize)
            {
                /* We have space in the current TLV, but addiion of an LETLV 
                 * might cause the PSNP buffer to overflow. We may have to 
                 * construct a new PSNP buffer to include the remaining 
                 * information
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Adding LETLV To PSNP - Max Buffer Size Reached - Max Size [ %u ], Current Size [ %u ]\n",
                         u2BufSize, (u2Offset + ISIS_LSP_ENTRY_TLV_LEN)));
                break;
            }

            /* Encode the LETLV values
             */

            ISIS_ASSIGN_2_BYTES (pu1PSNP, u2Offset, pTLV->u2RLTime);
            u2Offset = (UINT2) (u2Offset + 2);

            ISIS_ASSIGN_STRING (pu1PSNP, u2Offset, pTLV->au1LSPId,
                                ISIS_LSPID_LEN);
            u2Offset = (UINT2) (u2Offset + ISIS_LSPID_LEN);

            ISIS_ASSIGN_4_BYTES (pu1PSNP, u2Offset, pTLV->u4SeqNum);
            u2Offset = (UINT2) (u2Offset + 4);

            ISIS_ASSIGN_2_BYTES (pu1PSNP, u2Offset, pTLV->u2CheckSum);
            u2Offset = (UINT2) (u2Offset + 2);

            /* Decrement the u2Len and u2AdjLen for the LETLV entry added
             */

            u2Len -= ISIS_LSP_ENTRY_TLV_LEN;

            u1TLVLen = (UINT1) (u1TLVLen + ISIS_LSP_ENTRY_TLV_LEN);
            pNextTLV = pTLV->pNext;
            IsisAdjDelPSNPNode (pCktLvlRec, pTLV->au1LSPId);
            pTLV = pNextTLV;
        }

        /* Filling the Length field of the Last TLV added in the buffer
         */

        *(pu1PSNP + u2Offset - u1TLVLen - 1) = u1TLVLen;

        IsisUpdAddAuthToPDU (pContext, pu1PSNP, &u2Offset, u1Level);
        ISIS_SET_CTRL_PDU_LEN (pu1PSNP, u2Offset);

        IsisTrfrTxData (pCktRec, pu1PSNP, u2Offset, u1Level, ISIS_FREE_BUF,
                        ISIS_FALSE);
        ISIS_INCR_SENT_PSNP (pCktRec, u1Level);
        ISIS_INCR_OUT_CTRL_PDU_STAT (pCktRec);

        /* We are about to start a new PSNP Buffer and hence Reset the 
         * TLV Length
         */

        u1TLVLen = 0;
    }
    *pPSNP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdConstructAndTxPSNP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdSetCktBitPat ()
 * Description : This function Marks LSPs for transmission by setting SRM flags
 *               on given circuits. If SRM flags already exist then it verifies
 *               whether the given Circuit Id can fit into the length of the SRM
 *               Flags. If yes, it sets the bit corresponding to the new ID that
 *               is to be set. If not, it allocates enough memory that is
 *               sufficient to hold the new ID, copies the old information to
 *               the new Flags, sets the bit corresponding to the new Id and
 *               releases the memory held by the old SRM Flags. The procedure is
 *               the same for ALL_CIRCUITS or ALL_BUT_SPECIFIED_CIRCUIT
 * Input (s)   : pContext   -   Pointer to the System Context
 *               pu1SRM     -   Address of the Pointer to SRM Flags
 *               pu1CktMask -   Pointer to Circuit Mask used in case bits
 *                              corresponding to all circuits are to be set
 *               pu1SRMLen  -   Pointer to SRM Mask Length. The value may get
 *                              updated if the current length is not sufficient
 *                              to hold the new ID
 *               u4CktId    -   Circuit index used in the case of setting a
 *                              specific bit corresponding to the circuit or in
 *                              the case of setting all bits associated with all
 *                              the circuits except the specified one
 *               u4NumCkts  -   the number of active circuits
 *               u1CktFlag  -   Circuit Flag specifying whether SRM bits are
 *                              to be set for
 *                              ALL_CIRCUITS
 *                              ALL_BUT_SPECIFIED_CIRCUIT
 *                              SPECIFIED_CIRCUIT
 * Output (s)  : None
 * Globals     : Not Referred or Modified             
 * Returns     : ISIS_SUCCESS, if Bit pattern is updated successfully
 *               ISIS_FAILURE, Otherwise 
 *****************************************************************************/

PUBLIC INT4
IsisUpdSetCktBitPat (tIsisSysContext * pContext, UINT1 **pu1SRM,
                     UINT1 *pu1CktMask, UINT1 *pu1SRMLen, UINT4 u4CktId,
                     UINT1 u1CktFlag)
{
    UINT1               u1Mask = 0;
    UINT1              *pu1NewSRM = NULL;
    UINT4               u4CktMskLen = 0;

    UPP_EE ((ISIS_LGST, "UPD <E> : Entered IsisUpdSetCktBitPat () \n"));

    /* Retrieve the Circuit Mask Length which indicates the number of bytes that
     * will be required to set all the bits associated with the active circuits.
     *
     * NOTE: We always allocate maximum number of bytes for the SRM flags, since
     * ALL_CIRCUITS, ALL_BUT_SPECIFIED_CIRCUIT are the most frequent operations
     * and this will require maximum number of bytes to hold all the bits of the
     * active circuits. The third operation SPECIFIED_CIRCUIT is very rare and
     * is performed only in the case of local system receiving a PSNP requesting
     * for LSPs.
     */

    ISIS_GET_4_BYTES (pu1CktMask, 0, u4CktMskLen);

    /* Since the value includes the length fields which holds 
     * the number of bytes included in the 'pu1CktMask'
     */

    u4CktMskLen -= 4;

    if (*pu1SRM == NULL)
    {
        /* SRM Flags are still not allocated yet */

        pu1NewSRM = ISIS_MEM_ALLOC (ISIS_BUF_SRMF, u4CktMskLen);
        if (pu1NewSRM == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SRM Flags\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_SRMF);
            UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdSetCktBitPat () \n"));
            return ISIS_MEM_FAILURE;
        }
        *pu1SRMLen = (UINT1) u4CktMskLen;
        MEMSET (pu1NewSRM, 0x00, u4CktMskLen);
        *pu1SRM = pu1NewSRM;
    }
    else if (*pu1SRMLen < u4CktMskLen)
    {
        /* SRM Flags already exist, but the number of bytes in the SRM Flags is
         * not sufficient to hold the new bit corresponding to the New ID. Hence
         * free the old SRM Flags and allocate sufficient length to hold bits
         * corresponding to all active circuits as indicated by the u4CktMaskLen
         */

        UPP_PT ((ISIS_LGST, "UPD <T> : Insufficient SRM Flag Length [ %u ]\n",
                 *pu1SRMLen));

        ISIS_DBG_PRINT_ID (*pu1SRM, (UINT1) (*pu1SRMLen),
                           "UPD <T> : SRM Flags Before Updation\t",
                           ISIS_OCTET_STRING);

        pu1NewSRM = ISIS_MEM_ALLOC (ISIS_BUF_SRMF, u4CktMskLen);

        if (pu1NewSRM != NULL)
        {
            MEMSET (pu1NewSRM, 0x00, u4CktMskLen);

            /* Copy the information held by the old SRM flags to the new block
             * allocated 
             */

            MEMCPY (pu1NewSRM, *pu1SRM, *pu1SRMLen);
            ISIS_MEM_FREE (ISIS_BUF_SRMF, (UINT1 *) *pu1SRM);

            /* Update the new block length and the pointer to the SRM flags
             */

            *pu1SRMLen = (UINT1) u4CktMskLen;
            *pu1SRM = pu1NewSRM;
            ISIS_DBG_PRINT_ID (*pu1SRM, (UINT1) (*pu1SRMLen),
                               "UPD <T> : SRM Flags After Updation\t",
                               ISIS_OCTET_STRING);
        }
        else
        {
            /* No memory to update SRM Flags */

            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SRM Flags\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_SRMF);
            UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdSetCktBitPat () \n"));
            return ISIS_MEM_FAILURE;
        }
    }

    switch (u1CktFlag)
    {
        case ISIS_ALL_CKT:

            /* Set all the bits for all the active circuits
             */

            MEMCPY (*pu1SRM, pu1CktMask + 4, u4CktMskLen);
            break;

        case ISIS_ALL_BUT_SPEC_CKT:

            /* Set all the bits on all the active circuits except the 
             * specified circuit
             */

            MEMCPY (*pu1SRM, pu1CktMask + 4, u4CktMskLen);
            ISIS_BIT_MASK (u4CktId, u1Mask);
            *(*pu1SRM + ISIS_BYTE_POS (u4CktId)) &= ~u1Mask;
            break;

        case ISIS_SPEC_CKT:

            /* Set the Flag on the Specified circuit
             */

            ISIS_BIT_MASK (u4CktId, u1Mask);
            *(*pu1SRM + ISIS_BYTE_POS (u4CktId)) |= u1Mask;
            break;

        default:

            /* Erroneous Circuit Specification */

            UPP_PT ((ISIS_LGST, "UPD <T> : Invalid Circuit Flag [ %u ]\n",
                     u1CktFlag));
            UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdSetCktBitPat () \n"));
            return ISIS_FAILURE;
    }

    UPP_EE ((ISIS_LGST, "UPD <E> : Exiting IsisUpdSetCktBitPat () \n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdResetSeqNums () 
 * Description : This function Resets the Sequence Numbers of all the Self LSPs
 * Input (s)   : pContext - Pointer to System Context
 *             : u1Level  - Level where the Sequnece numbers are to be reset
 * Output (s)  : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisUpdResetSeqNums (tIsisSysContext * pContext)
{
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisLSPInfo       *pNZLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdResetSeqNums ()\n"));

    /* Level 1 Non-Pseudonode */

    if (pContext->SelfLSP.pL1NSNLSP != NULL)
    {
        /* Reset Sequence Number for Zero LSP
         */

        pContext->SelfLSP.pL1NSNLSP->pZeroLSP->u4SeqNum = 0;
        pContext->SelfLSP.pL1NSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

        /* Reset Sequence Number for Non-Zero LSPs
         */

        pNZLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;

        while (pNZLSP != NULL)
        {
            pNZLSP->u4SeqNum = 0;
            pNZLSP->u1DirtyFlag = ISIS_MODIFIED;
            pNZLSP = pNZLSP->pNext;
        }
    }

    /* Level 1 Pseudonode */

    if (pContext->SelfLSP.pL1SNLSP != NULL)
    {
        pSNLSP = pContext->SelfLSP.pL1SNLSP;

        while (pSNLSP != NULL)
        {
            /* Reset Sequence Number for Zero LSP
             */

            pSNLSP->pZeroLSP->u4SeqNum = 0;
            pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

            /* Reset Sequence Number for Non-Zero LSPs
             */

            pNZLSP = pSNLSP->pNonZeroLSP;

            while (pNZLSP != NULL)
            {
                pNZLSP->u4SeqNum = 0;
                pNZLSP->u1DirtyFlag = ISIS_MODIFIED;
                pNZLSP = pNZLSP->pNext;
            }
            pSNLSP = pSNLSP->pNext;
        }
    }

    /* Level 2 Non-Pseudonode */

    if (pContext->SelfLSP.pL2NSNLSP != NULL)
    {
        /* Reset Sequence Number for Zero LSP
         */

        pContext->SelfLSP.pL2NSNLSP->pZeroLSP->u4SeqNum = 0;
        pContext->SelfLSP.pL2NSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

        /* Reset Sequence Number for Non-Zero LSPs
         */

        pNZLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;

        while (pNZLSP != NULL)
        {
            pNZLSP->u4SeqNum = 0;
            pNZLSP->u1DirtyFlag = ISIS_MODIFIED;
            pNZLSP = pNZLSP->pNext;
        }
    }

    /* Level 2 Pseudonode */

    if (pContext->SelfLSP.pL2SNLSP != NULL)
    {
        pSNLSP = pContext->SelfLSP.pL2SNLSP;

        while (pSNLSP != NULL)
        {
            /* Reset Sequence Number for Zero LSP
             */

            pSNLSP->pZeroLSP->u4SeqNum = 0;
            pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

            /* Reset Sequence Number for Non-Zero LSPs
             */

            pNZLSP = pSNLSP->pNonZeroLSP;

            while (pNZLSP != NULL)
            {
                pNZLSP->u4SeqNum = 0;
                pNZLSP->u1DirtyFlag = ISIS_MODIFIED;
                pNZLSP = pNZLSP->pNext;
            }
            pSNLSP = pSNLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdResetSeqNums ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdFillLSPHdr ()
 * Description : This function updates the LSP Header information into the 
 *               given LSP
 * Input(s)    : pContext  - Pointer to System Context
 *               pLSP      - Pointer to the Self LSP
 *               u1Level   - Level of the LSP
 *               pu2Offset - Offset from where the LSP Header information is to
 *                           be updated into the PDU
 *               pu1LSP    - Pointer to LSP Buffer which is to be updated
 * Output(s)   : pu2Offset - Updated offset value after the LSP header is
 *                           filled. Subsequent encoding starts from the updated
 *                           offset
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisUpdFillLSPHdr (tIsisSysContext * pContext, tIsisLSPInfo * pLSP,
                   UINT1 u1Level, UINT2 *pu2Offset, UINT1 *pu1LSP)
{
    UINT1               au1LSPId[ISIS_LSPID_LEN] = { 0 };
    UINT1               u1Flags = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdFillLSPHdr ()\n"));

    *pu2Offset = (UINT2) (*pu2Offset + 2);

    ISIS_ASSIGN_2_BYTES (pu1LSP, *pu2Offset, ISIS_LSP_MAXAGE);
    *pu2Offset = (UINT2) (*pu2Offset + 2);

    MEMCPY (au1LSPId, pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
    au1LSPId[ISIS_SYS_ID_LEN] = pLSP->u1SNId;
    au1LSPId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN] = pLSP->u1LSPNum;

    MEMCPY ((pu1LSP + *pu2Offset), au1LSPId, ISIS_LSPID_LEN);
    *pu2Offset = (UINT2) (*pu2Offset + ISIS_LSPID_LEN);

    ISIS_ASSIGN_4_BYTES (pu1LSP, *pu2Offset, pLSP->u4SeqNum);
    *pu2Offset = (UINT2) (*pu2Offset + 4);

    /* Increment the Offset to skip the Checksum which will be filled later
     */

    *pu2Offset = (UINT2) (*pu2Offset + 2);

    if (u1Level == ISIS_LEVEL1)
    {
        /* u1L1LSPFlags carries the SYSTEM TYPE (either L1 (1), L2 (2), or L12 (3).
         * But the IS Type in LSP Header should carry either L1 (1) or L2 (3) as per standard
         * So resetting the corresponding bits and assigning 1 or 3 (L1 or L2) 
         * depends on LSP Type*/
        u1Flags = pContext->SelfLSP.u1L1LSPFlags;
        u1Flags &= (~ISIS_LEVEL12);
        u1Flags |= IS_TYPE_LEVEL1;
        if ((pLSP->u1SNId == 0) && (pLSP->u1LSPNum == 0))
        {
            ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, u1Flags);
        }
        else
        {
            u1Flags &= (~ISIS_FLG_ATT_DEF);
            ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, u1Flags);
        }
    }
    else
    {
        /* u1L2LSPFlags carries the SYSTEM TYPE (either L1 (1), L2 (2), or L12 (3).
         * But the IS Type in LSP Header should carry either L1 (1) or L2 (3) as per standard
         * So resetting the corresponding bits and assigning 1 or 3 (L1 or L2) 
         * depends on LSP Type*/
        u1Flags = pContext->SelfLSP.u1L2LSPFlags;
        u1Flags &= (~ISIS_LEVEL12);
        u1Flags |= IS_TYPE_LEVEL2;
        ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, u1Flags);
    }
    *pu2Offset = (UINT2) (*pu2Offset + 1);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdFillLSPHdr ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdCompareLSPs ()
 * Description : This function compares the received LSP with the LSP fetched 
 *               from the database. It determines whether the received LSP is 
 *               the same as the one in the Database (except for Sequence 
 *               Number) or is it entirely new. Based on the comparison it 
 *               returns status codes which will be used for further processing
 *               of the LSP 
 * Input(s)    : pu1RcvdLSP - Pointer to the Received LSP
 *               pu1LSP     - Pointer to LSP fetched from the database
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SAME_CS_GRTR_SEQNO - If the received LSP has Greater
 *                                         Sequence Number and the received LSP
 *                                         Checksum is not different from the 
 *                                         checksum of the LSP in the Database
 *               ISIS_SAME_CS_LESS_SEQNO - If the received LSP has Less
 *                                         Sequence Number
 *               ISIS_SAME_CS_SAME_SEQNO - If the received LSP has Same
 *                                         Sequence Number and the received LSP 
 *                                         is not different from the Database
 *                                         LSP
 *               ISIS_DIFF_CS_GRTR_SEQNO - If the received LSP has Greater
 *                                         Sequence Number and if the contents
 *                                         of the received LSP is different from
 *                                         the Database LSP
 *               ISIS_DIFF_CS_LESS_SEQNO - If the received LSP has Less
 *                                         Sequence Number and if the contents
 *                                         of the received LSP is different from
 *                                         the Database LSP
 *               ISIS_DIFF_CS_SAME_SEQNO - If the received LSP has Same
 *                                         Sequence Number and if the contents
 *                                         of the received LSP is different from
 *                                         the Database LSP
 ******************************************************************************/

PUBLIC INT4
IsisUpdCompareLSPs (UINT1 *pu1RcvdLSP, UINT1 *pu1LSP)
{
    INT4                i4RetVal = 0;
    UINT2               u2CS = 0;
    UINT2               u2Len = 0;
    UINT2               u2RcvdLen = 0;
    UINT2               u2AdjCS = 0;
    UINT2               u2RcvdCS = 0;
    UINT4               u4Seqno = 0;
    UINT4               u4RcvdSeqno = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdCompareLSPs ()\n"));

    ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1RcvdLSP, u4RcvdSeqno);
    ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1LSP, u4Seqno);

    ISIS_EXTRACT_CS_FROM_LSP (pu1RcvdLSP, u2RcvdCS);
    ISIS_EXTRACT_CS_FROM_LSP (pu1LSP, u2CS);

    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1RcvdLSP, u2RcvdLen);
    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSP, u2Len);

    /* Before the LSPs are compared, the checksum of the LSP, taken from the
     * database, is adjusted with the Sequence Number from the received LSP.
     *
     * NOTE: Checksum Adjustments will be made only with Sequence Numbers. If
     * Sequence Numbers does not change then Adjusted Checksum is the same as
     * the Checksum taken from the database LSP
     */

    /* NOTE: ISIS_DIFF_CS_... means that either the Checksum has actually
     * changed or the contents of the received LSP is different from the
     * contents of the database LSP 
     */

    if (u4RcvdSeqno != u4Seqno)
    {
        /* Sequence Numbers are different. Check whether the contents of the
         * received LSP and the LSP fetched from the database are the same
         */

        if (u2RcvdLen == u2Len)
        {
            /* Since the Lengths of the both LSPs are same, 
             * compare octet-by-octet
             */

            i4RetVal = MEMCMP ((pu1LSP + ISIS_LSP_HDR_LEN),
                               (pu1RcvdLSP + ISIS_LSP_HDR_LEN),
                               (u2Len - ISIS_LSP_HDR_LEN));
            if (i4RetVal == 0)
            {
                u2AdjCS = u2CS;
            }
        }

    }
    else
    {
        /* No change in sequence numbers and hence the adjusted checksum must be
         * taken as the value of the checksum from the database LSP 
         */

        u2AdjCS = u2CS;
    }

    if (u4Seqno < u4RcvdSeqno)
    {
        if (u2AdjCS == u2RcvdCS)
        {
            i4RetVal = ISIS_SAME_CS_GRTR_SEQNO;
        }
        else
        {
            /* Reach this point either if Checksums are different or the
             * lengths are different
             */

            i4RetVal = ISIS_DIFF_CS_GRTR_SEQNO;
        }
    }
    else if (u4Seqno > u4RcvdSeqno)
    {
        /* Sequence number of the Received LSP is Less than the Sequence Number
         * of the LSP in the Database. We need not bother to check the contents 
         * octet by octet since the Database LSP will anyway be scheduled for
         * transmission to update the peers who have older copies
         */

        if (u2AdjCS == u2RcvdCS)
        {
            i4RetVal = ISIS_SAME_CS_LESS_SEQNO;
        }
        else
        {
            /* Reach this point either if Checksums are different or the
             * lengths are different
             */

            i4RetVal = ISIS_DIFF_CS_LESS_SEQNO;
        }
    }
    else
    {
        if (u2AdjCS == u2RcvdCS)
        {
            /* Assume Checksums are the same at this point
             */

            i4RetVal = ISIS_SAME_CS_SAME_SEQNO;

        }
        else
        {
            /* Reach this point either if Checksums are different or the
             * lengths are different. This should not happen
             */

            i4RetVal = ISIS_DIFF_CS_SAME_SEQNO;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdCompareLSPs ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdCompLSPHdr ()
 * Description : This function compares the LSP headers
 * Globals     : Not Referred               
 * Input(s)    : pu1LSP   - Pointer to LSP
 *               pu1PSNP  - Pointer to PSNP 
 * Output(s)   : None
 * RETURN(s)   : ISIS_SUCCESS - On success
 *               ISIS_FAILURE  - On failure
 *****************************************************************************/

PUBLIC INT4
IsisUpdCompLSPHdr (UINT1 *pu1LSP, UINT1 *pu1PSNP)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT2               u2CS = 0;
    UINT2               u2RcvdCS = 0;
    UINT4               u4Seqno = 0;
    UINT4               u4RcvdSeqno = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdCompLSPHdr ()\n"));

    ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1LSP, u4Seqno);
    ISIS_GET_4_BYTES (pu1PSNP, ISIS_LSPID_LEN + 2, u4RcvdSeqno);

    ISIS_EXTRACT_CS_FROM_LSP (pu1LSP, u2CS);
    ISIS_GET_2_BYTES (pu1PSNP, ISIS_LSPID_LEN + 6, u2RcvdCS);

    if (u4Seqno < u4RcvdSeqno)
    {
        if (u2CS != u2RcvdCS)
        {
            i4RetVal = ISIS_DIFF_CS_GRTR_SEQNO;
        }
        else
        {
            i4RetVal = ISIS_SAME_CS_GRTR_SEQNO;
        }
    }
    else if (u4Seqno > u4RcvdSeqno)
    {
        if (u2CS != u2RcvdCS)
        {
            i4RetVal = ISIS_DIFF_CS_LESS_SEQNO;
        }
        else
        {
            i4RetVal = ISIS_SAME_CS_LESS_SEQNO;
        }
    }
    else
    {
        if (u2CS != u2RcvdCS)
        {
            i4RetVal = ISIS_DIFF_CS_SAME_SEQNO;
        }
        else
        {
            i4RetVal = ISIS_SAME_CS_SAME_SEQNO;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdCompLSPHdr ()\n"));

    return (i4RetVal);
}

/*******************************************************************************
 * Function    : IsisUpdAddCS () 
 * Description : This function calculates the Checksum value for the given PDU
 *               and updates the PDU with the calculated value
 * Input (s)   : pu1LSP  - Pointer to the LSP
 *               u2Len   - Length of the LSP
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdAddCS (UINT1 *pu1LSP, UINT2 u2Len)
{
    UINT2               u2CS = 0;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    UINT4               u4RcvdSeqNum = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddCS ()\n"));

    /* Reset the Checksum bytes in the LSP
     */

    ISIS_SET_LSP_CS (pu1LSP, 0);

    /* NOTE: Checksum calculation includes octets starting from the LSPID
     * onwards. Hence we pass 'pu1LSP + ISIS_OFFSET_LSPID' to the
     * checksum calculation routine
     */

    u2CS = IsisUtlCalcChkSum ((pu1LSP + ISIS_OFFSET_LSPID),
                              (UINT2) (u2Len - ISIS_OFFSET_LSPID),
                              (pu1LSP + ISIS_OFFSET_CHKSUM));

    ISIS_EXTRACT_SEQNUM_FROM_LSP (pu1LSP, u4RcvdSeqNum);
    ISIS_EXTRACT_LSPID_FROM_LSP (pu1LSP, au1LSPId);
    ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
    DETAIL_INFO (ISIS_UPD_MODULE,
                 (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                  "UPD <T> : Updated Checksum [ %x ] - LSP-ID [ %s ], Seq. No [ %u ]\n",
                  u2CS, acLSPId, u4RcvdSeqNum));

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddCS ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdValidateCS ()
 * Description : This function validates the checksum of the given LSP 
 * Input(s)    : pContext - Pointer to System Context
 *               pu1LSP   - Pointer to the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, if Checksum is correct
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisUpdValidateCS (tIsisSysContext * pContext, UINT1 *pu1LSP)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2RcvdCS = 0;
    UINT2               u2Len = 0;
    tIsisEvtInvalidLSP *pEvtInvLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdValidateCS ()\n"));

    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSP, u2Len);
    ISIS_EXTRACT_CS_FROM_LSP (pu1LSP, u2RcvdCS);

    u2RcvdCS = IsisUtlCalcChkSum ((pu1LSP + ISIS_OFFSET_LSPID),
                                  (UINT2) (u2Len - ISIS_OFFSET_LSPID), NULL);

    /* We compare against ISIS_CORRECT_CS, which is 0, because if checksum
     * is calculated including the Checksum bytes, the result will be 0x0000 in
     * the 2 bytes of Checksum. 
     */

    if (u2RcvdCS != ISIS_CORRECT_CS)
    {
        ISIS_DBG_PRINT_ID ((pu1LSP + ISIS_OFFSET_LSPID), (UINT1) ISIS_LSPID_LEN,
                           "UPD <T> : Invalid Checksum For LSP\t",
                           ISIS_OCTET_STRING);
        UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdValidateCS ()\n"));
        i4RetVal = ISIS_FAILURE;
    }

    if (i4RetVal == ISIS_FAILURE)
    {
        pEvtInvLSP = (tIsisEvtInvalidLSP *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtInvalidLSP));
        if (pEvtInvLSP != NULL)
        {
            pEvtInvLSP->u1EvtID = ISIS_EVT_INV_LSP;
            MEMCPY (pEvtInvLSP->au1LSPId, (pu1LSP + ISIS_OFFSET_LSPID),
                    ISIS_LSPID_LEN);

            IsisUtlSendEvent (pContext, (UINT1 *) pEvtInvLSP,
                              sizeof (tIsisEvtInvalidLSP));
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdValidateCS ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdUpdateAA ()
 * Description : This function reads the area addresses included in the received
 *               LSP and updates the local area address table based on the
 *               command
 * Input(s)    : pContext  - Pointer to System Context
 *               pu1LSP    - Pointer to the LSP
 *               u1Cmd     - Command indicating either Add or Delete
 * Output(s)   : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisUpdUpdateAA (tIsisSysContext * pContext, UINT1 *pu1LSP, UINT1 u1Cmd)
{
    UINT1               u1Len = 0;
    UINT1               u1Code = 0;
    UINT1               u1AALen = 0;
    UINT1               au1AA[ISIS_AREA_ADDR_LEN];
    UINT1               u1ProcLen = 0;
    UINT1               u1Level = 0;
    UINT1               u1LSPNum = 0;
    UINT2               u2LSPLen = 0;
    UINT2               u2Offset = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdUpdateAA ()\n"));

    u1Level = (UINT1) ((ISIS_EXTRACT_PDU_TYPE (pu1LSP) == ISIS_L1LSP_PDU) ?
                       ISIS_LEVEL1 : ISIS_LEVEL2);

    /* LSP Number is the last byte of the LSPID which is the field just before
     * Sequence umber. Hence 'ISIS_OFFSET_SEQNO - 1' refers to the LSP number
     * offset in the PDU
     */

    u1LSPNum = *(pu1LSP + ISIS_OFFSET_SEQNO - 1);

    if ((u1Level == ISIS_LEVEL1) && (u1LSPNum == 0))
    {
        /* Area Addresses are to be considered only from LSPs with LSP number
         * '0'
         */

        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSP, u2LSPLen);

        /* Move the Offset to Variable Length fields. We start decoding TLVs and
         * pick the Area Address TLVs and process them
         */

        u2Offset = (UINT2) (((tIsisComHdr *) pu1LSP)->u1HdrLen);

        while (u2Offset < u2LSPLen)
        {
            ISIS_GET_1_BYTE (pu1LSP, u2Offset, u1Code);
            ISIS_GET_1_BYTE (pu1LSP, u2Offset + 1, u1Len);
            u2Offset = (UINT2) (u2Offset + 2);    /* Skip Code and Length */

            if (u1Code == ISIS_AREA_ADDR_TLV)
            {
                /* Scan through the entire TLV, since more than one area address
                 * can be included in a single TLV
                 */

                /* There may be more than one Area Address TLV and hence
                 * 'u1ProcLen' must be re-initialised every time we need to
                 * process an Area Address TLV
                 */

                u1ProcLen = 0;
                while (u1ProcLen < u1Len)
                {
                    /* Get the Area Address Length */

                    ISIS_GET_1_BYTE (pu1LSP, u2Offset, u1AALen);

                    u2Offset = (UINT2) (u2Offset + 1);
                    u1ProcLen = (UINT1) (u1ProcLen + 1);

                    MEMCPY (au1AA, pu1LSP + u2Offset, u1AALen);
                    if (u1Cmd == ISIS_CMD_ADD)
                    {
                        /* Try to add the Area Address extracted, to the local
                         * Area Address table
                         */

                        IsisUpdAddAA (pContext, au1AA, u1AALen);
                    }
                    else
                    {
                        /* Try to delete the Area Address extracted, from the 
                         * local Area Address table
                         */

                        IsisUpdDelAA (pContext, au1AA, u1AALen);
                    }
                    u2Offset = (UINT2) (u2Offset + u1AALen);
                    u1ProcLen = (UINT1) (u1ProcLen + u1AALen);
                }
            }
            else
            {
                /* The TLV is not an Area Address TLV. Skip and continue
                 */

                u2Offset = (UINT2) (u2Offset + u1Len);
            }
        }
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdUpdateAA ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdAddAA ()
 * Description : This function verifies whether the specified Area Address
 *               already exists in the Area Address Table. If an entry exists it
 *               increments the usage count since more than one LSP could have
 *               included this Area Address. If not it adds the entry to the
 *               table if enough memory available. It will prune the database if
 *               memory for holding the new address is not available
 * Input(s)    : pContext - Pointer to System Context
 *               pu1AA    - Pointer to Area Address
 *               u1AALen  - Area Address Length
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisUpdAddAA (tIsisSysContext * pContext, UINT1 *pu1AA, UINT1 u1AALen)
{
    tIsisAAEntry       *pNextAARec = NULL;
    tIsisAAEntry       *pAARec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddAA ()\n"));

    ISIS_DBG_PRINT_ADDR (pu1AA, (UINT1) u1AALen,
                         "UPD <T> : Adding Area Address\t", ISIS_OCTET_STRING);

    pAARec = pContext->AreaAddrTable.pAARec;

    if (u1AALen > ISIS_AREA_ADDR_LEN)
    {
        return;
    }

    /* Scan through all the entries and verify whether an address already exits
     */

    while (pAARec != NULL)
    {
        if ((pAARec->AreaAddr.u1Length == u1AALen)
            && (MEMCMP (pAARec->AreaAddr.au1AreaAddr, pu1AA, u1AALen) == 0))
        {
            /* Address Found. Increment Usage Count
             */

            pAARec->u2UsgCnt++;
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddAA ()\n"));
            return;
        }
        pAARec = pAARec->pNext;
    }

    if (pContext->AreaAddrTable.u2NumEntries == pContext->SysActuals.u1SysMaxAA)
    {
        /* Area Address not found in the table, and there is not enough memory
         * in the table to hold the new address. Try to prune existing entries
         * so that we can accomodate the new entry
         */

        UPP_PT ((ISIS_LGST, "UPD <P> : Max Area Address Limit Reached [ %u ]\n",
                 pContext->AreaAddrTable.u2NumEntries));
        IsisUpdPruneAA (pContext, pu1AA, u1AALen);
    }
    else
    {
        /* Address not found in the table and we have enough memory to hold the
         * new entry
         */

        pNextAARec = (tIsisAAEntry *) ISIS_MEM_ALLOC (ISIS_BUF_ARAD,
                                                      sizeof (tIsisAAEntry));

        if (pNextAARec != NULL)
        {
            pNextAARec->AreaAddr.u1Length = u1AALen;
            MEMCPY (pNextAARec->AreaAddr.au1AreaAddr, pu1AA, u1AALen);
            pNextAARec->u2UsgCnt = 1;

            /* Place the new entry at the head as we don't have any restrictions
             * on the order of entries
             */

            pNextAARec->pNext = pContext->AreaAddrTable.pAARec;
            pContext->AreaAddrTable.pAARec = pNextAARec;
            pContext->AreaAddrTable.u2NumEntries++;
        }
        else
        {
            /* Should not happen, since Max Area Address limit is not reached.
             * We should be able to get a new block for the new address. If
             * it hapens it is really a PANIC. But still try to prune existing
             * entries and accomodate the new address if possible
             */

            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Area Address\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUpdPruneAA (pContext, pu1AA, u1AALen);
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddAA ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdPruneAA ()
 * Description : This function compares each of the area addresses held in the
 *               Area Address database with the new address and selects one
 *               entry which is to be pruned. If no entry is selected from the
 *               database (means new address is to be pruned), then this routine
 *               returns without affecting any changes to the database. If an
 *               entry gets selected from the database, then the selected entry
 *               is replaced by the new address entry. If the selected entry
 *               happens to be one of the manual area addresses configured, then
 *               a manual area address dropped event is generated
 * Input(s)    : pContext  - Pointer to System Context
 *               pu1AA     - Pointer to the area address
 *               u1AALen   - Area address length
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 *****************************************************************************/

PRIVATE VOID
IsisUpdPruneAA (tIsisSysContext * pContext, UINT1 *pu1AA, UINT1 u1AALen)
{
    INT4                i4RetVal = 0;
    UINT1               u1DelAALen = 0;
    UINT1              *pDelAA = NULL;
    tIsisMAAEntry      *pTravMAA = NULL;
    tIsisAAEntry       *pDelRec = NULL;
    tIsisAAEntry       *pTravAA = NULL;
    tIsisEvtManAADropped *pMAADropEvt = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdPruneAA ()\n"));

    /* We first assume that the new address is the one to be pruned and if it
     * finally turns out to be true, we need not do anything, else we will copy
     * the new address to the pruned record
     */

    pDelAA = pu1AA;
    u1DelAALen = u1AALen;

    pTravAA = pContext->AreaAddrTable.pAARec;

    if ((pTravAA != NULL) && (pTravAA->AreaAddr.u1Length > ISIS_AREA_ADDR_LEN))
    {
        return;
    }

    while (pTravAA != NULL)
    {
        /* Area Address lengths should not differ. Even if they differ, we
         * compare the addresses only for the minimum of the lengths of
         * addresses. If the addresses are same in the bytes compared, then we
         * select the address which ever has greater length
         *
         * For e.g., i
         *
         * 1] A1 = 1.2.3.4 and A2 = 1.2.3. A1 is greater than A2.
         * 2] A1 = 1.2.3.0 and A2 = 5,6,7, A2 is greater
         */

        i4RetVal = MEMCMP (pTravAA->AreaAddr.au1AreaAddr, pDelAA,
                           ISIS_MIN (u1DelAALen, pTravAA->AreaAddr.u1Length));

        if (i4RetVal > 0)
        {
            /* pTrav contains an area address which is greater than the already
             * marked address. Mark the address included by 'pTrav'
             */

            pDelAA = pTravAA->AreaAddr.au1AreaAddr;
            u1DelAALen = pTravAA->AreaAddr.u1Length;
            pDelRec = pTravAA;
        }
        else if (i4RetVal == 0)
        {
            /* Area Addresses compared for minimum of both lengths appear to be
             * the same. We have to mark the address which ever is of greater
             * length
             */

            if (pTravAA->AreaAddr.u1Length > u1DelAALen)
            {
                pDelAA = pTravAA->AreaAddr.au1AreaAddr;
                u1DelAALen = pTravAA->AreaAddr.u1Length;
                pDelRec = pTravAA;
            }

            /* Lengths cannot be equal at this point, because if lengths were
             * equal and addresses were also same, we would have found the
             * address in the Area Address table and hence the usage count would
             * have been incremented without any need for pruning addresses
             */
        }
        pTravAA = pTravAA->pNext;
    }

    if (pDelAA != pu1AA)
    {
        /* The marked entry is different from the new entry. We have to update
         * the existing entry with the new address information
         */

        ISIS_DBG_PRINT_ADDR (pDelRec->AreaAddr.au1AreaAddr, (UINT1) u1DelAALen,
                             "UPD <T> : Pruning Address\t", ISIS_OCTET_STRING);
        MEMCPY (pDelRec->AreaAddr.au1AreaAddr, pu1AA, u1AALen);
        pDelRec->AreaAddr.u1Length = u1AALen;
        pDelRec->u2UsgCnt = 1;

        /* Check whether the dropped address is one of the Manual Area Addresses
         * configured. If so generate the Manual Area Address dropped event
         */

        pTravMAA = pContext->ManAddrTable.pMAARec;
        while (pTravMAA != NULL)
        {
            i4RetVal
                = MEMCMP (pTravMAA->ManAreaAddr.au1AreaAddr, pDelAA,
                          ISIS_MIN (u1DelAALen,
                                    pTravMAA->ManAreaAddr.u1Length));

            if (i4RetVal == 0)
            {
                /* Matched one of the configured anual Area Address
                 */

                ISIS_INCR_SYS_MAA_DROPPED (pContext);
                pMAADropEvt = (tIsisEvtManAADropped *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                    sizeof (tIsisEvtManAADropped));
                if (pMAADropEvt != NULL)
                {
                    pMAADropEvt->u1EvtID = ISIS_EVT_MAN_ADDR_DROPPED;
                    pMAADropEvt->u1MAAExistState = pTravMAA->u1ExistState;
                    pMAADropEvt->u1Length = pTravMAA->ManAreaAddr.u1Length;
                    MEMCPY (pMAADropEvt->au1AreaAddr,
                            pTravMAA->ManAreaAddr.au1AreaAddr,
                            pTravMAA->ManAreaAddr.u1Length);
                    IsisUtlSendEvent (pContext, (UINT1 *) pMAADropEvt,
                                      sizeof (tIsisEvtManAADropped));
                }
                else
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <E> : No Memory For MAA Dropped Event\n"));
                }
                break;
            }
            pTravMAA = pTravMAA->pNext;
        }
    }
    else
    {
        /* The new address itself is pruned. Hence there is nothing to be done
         */

        UPP_PT ((ISIS_LGST, "UPD <T> : New Address Is Pruned\n"));
        ISIS_DBG_PRINT_ADDR (pu1AA, (UINT1) u1DelAALen,
                             "UPD <T> : Pruned Address\t", ISIS_OCTET_STRING);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdPruneAA ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdDelAA ()
 * Description : This function Deletes the given Area Address Entry from Area
 *               Address Table
 * Input(s)    : pContext - Pointer to System Context
 *               pu1AA    - Pointer to Area Address to be removed
 *               u1AALen  - Area Address length
 * Output(s)   : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdDelAA (tIsisSysContext * pContext, UINT1 *pu1AA, UINT1 u1AALen)
{
    tIsisAAEntry       *pPrevAARec = NULL;
    tIsisAAEntry       *pAARec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelAA ()\n"));

    pAARec = pContext->AreaAddrTable.pAARec;

    /* Scan through all the entries and mark the matching entry only if both
     * length and area address are the same. If usage count becomes zero then
     * delete the marked entry. Otherwise do not modify the database
     */
    if (u1AALen > ISIS_AREA_ADDR_LEN)
    {
        return;
    }

    while (pAARec != NULL)
    {
        if ((pAARec->AreaAddr.u1Length == u1AALen)
            && (MEMCMP (pAARec->AreaAddr.au1AreaAddr, pu1AA, u1AALen) == 0))
        {
            /* Matching entry found */

            ISIS_DBG_PRINT_ADDR (pAARec->AreaAddr.au1AreaAddr, (UINT1) u1AALen,
                                 "UPD <T> : Marked Address For Deletion\n",
                                 ISIS_OCTET_STRING);
            pAARec->u2UsgCnt--;

            if (pAARec->u2UsgCnt == 0)
            {
                /* This entry is not being referred by any LSPs. Hence we can
                 * delete the entry
                 */

                ISIS_DBG_PRINT_ADDR (pAARec->AreaAddr.au1AreaAddr,
                                     (UINT1) u1AALen,
                                     "UPD <T> : Deleting Address\n",
                                     ISIS_OCTET_STRING);
                if (pPrevAARec != NULL)
                {
                    /* Already advanced through the list. Hence use the
                     * pPrevAARec to delete the marked node
                     */

                    pPrevAARec->pNext = pAARec->pNext;
                }
                else
                {
                    /* Very first entry being deleted. Advance the head
                     */

                    pContext->AreaAddrTable.pAARec = pAARec->pNext;
                }

                ISIS_MEM_FREE (ISIS_BUF_ARAD, (UINT1 *) pAARec);
                pContext->AreaAddrTable.u2NumEntries--;
            }
            break;
        }
        pPrevAARec = pAARec;
        pAARec = pAARec->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelAA ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdUpdateSelfLSPSeqNum ()
 * Description : This routine updates the Sequence Number of the LSP matching
 *               the 'pu1LSPId', to u4RcvdSeqNum. The routine searches the
 *               appropriate Pseudonode or Non-Pseudonode list of LSPs based on
 *               the 'pu1LSPId' and 'u1Level'
 * Input(s)    : pContext     - Pointer to System Context
 *               pu1LSPId     - Pointer to the LSPId of the received LSP
 *               u4RcvdSeqNum - Received Sequence Number
 *               u1Level      - Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisUpdUpdateSelfLSPSeqNum (tIsisSysContext * pContext, UINT1 *pu1LSPId,
                            UINT4 u4RcvdSeqNum, UINT1 u1Level)
{
    UINT1               u1Found = ISIS_FALSE;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNZLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdUpdateSelfLSPSeqNum ()\n"));

    ISIS_DBG_PRINT_ID (pu1LSPId, (UINT1) (ISIS_SYS_ID_LEN + 2),
                       "UPD <T> : Updating Self LSPs For\t", ISIS_OCTET_STRING);

    /* First check whether the given LSP, identified by the 'pu1LSPId' is a
     * Pseudonode or Non-Pseudonode LSP. Non-Pseudonode LSPs will have '0' in
     * the Pseudonode ID field 
     */

    if (*(pu1LSPId + ISIS_SYS_ID_LEN) == 0)
    {
        /* Non-Pseudonode case */

        if (u1Level == ISIS_LEVEL1)
        {
            pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        }
        else
        {
            pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        }

        if (pNSNLSP != NULL)
        {
            /* Check for the LSP numbers alone since all Self LSPs will have the
             * Local system Id for first ISIS_SYS_ID_LEN bytes and '0' for
             * pseudonode ID field
             */

            /* Only one Zero LSP is allowed */

            if (pNSNLSP->pZeroLSP->u1LSPNum ==
                pu1LSPId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN])
            {
                pNSNLSP->pZeroLSP->u4SeqNum = u4RcvdSeqNum;;
            }
            else
            {
                /* Travel through a list of Non-Zero LSPs */

                pNZLSP = pNSNLSP->pNonZeroLSP;
                while (pNZLSP != NULL)
                {
                    /* Check for the LSP numbers alone since all Self LSPs will
                     * have Local system Id for first ISIS_SYS_ID_LEN bytes 
                     * and '0' for pseudonode ID field
                     */

                    if (pNZLSP->u1LSPNum ==
                        pu1LSPId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN])
                    {
                        pNZLSP->u4SeqNum = u4RcvdSeqNum;
                        break;
                    }
                    pNZLSP = pNZLSP->pNext;
                }
            }
        }
    }
    else
    {
        /* Pseudonode case */

        if (u1Level == ISIS_LEVEL1)
        {
            pSNLSP = pContext->SelfLSP.pL1SNLSP;
        }
        else
        {
            pSNLSP = pContext->SelfLSP.pL2SNLSP;
        }

        while (pSNLSP != NULL)
        {
            /* Check for the LSP numbers alone since all Self LSPs will have the
             * Local system Id for first ISIS_SYS_ID_LEN bytes and '0' for
             * pseudonode ID field
             */

            /* Only one Zero LSP is allowed */

            if ((pSNLSP->pZeroLSP->u1SNId == pu1LSPId[ISIS_SYS_ID_LEN])
                && (pSNLSP->pZeroLSP->u1LSPNum ==
                    pu1LSPId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN]))
            {
                pSNLSP->pZeroLSP->u4SeqNum = u4RcvdSeqNum;;
                break;
            }
            else
            {
                /* Travel through a list of Non-Zero LSPs */

                pNZLSP = pSNLSP->pNonZeroLSP;
                while (pNZLSP != NULL)
                {
                    /* Check for the LSP numbers alone since all Self LSPs will
                     * Local system Id for first ISIS_SYS_ID_LEN bytes and '0' 
                     * for pseudonode ID field
                     */

                    if ((pNZLSP->u1SNId == pu1LSPId[ISIS_SYS_ID_LEN])
                        && (pNZLSP->u1LSPNum ==
                            pu1LSPId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN]))
                    {
                        pNZLSP->u4SeqNum = u4RcvdSeqNum;
                        u1Found = ISIS_TRUE;
                        break;
                    }
                    pNZLSP = pNZLSP->pNext;
                }
            }
            if (u1Found == ISIS_TRUE)
            {
                break;
            }
            pSNLSP = pSNLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdUpdateSelfLSPSeqNum ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdUpdateRLTime ()  
 * Description : This function decrements the Remaining Life Time of the given
 *               LSP by the amount of time the LSP is in memory. Each of the LSP
 *               lists in the EC timer blocks has a header which holds the time
 *               stamp value, which indicates the time at which the list was
 *               created. We travel the timer links backward, to get to the head
 *               of the list to extract this time stamp. Difference between the
 *               current time and the time stamp will give the amount of time
 *               the LSP had been in the Database which is decremented from the
 *               RLT in the LSP
 * Input(s)    : pContext - Pointer to System Context
 *               pLSPRec  - Pointer to the LSP Buffer
 * Output(s)   : None
 * Globals     : Not Referred or Modified             
 * Returns     : VOID
 *****************************************************************************/

PUBLIC VOID
IsisUpdUpdateRLTime (tIsisSysContext * pContext, tIsisLSPEntry * pLSPRec)
{
    UINT2               u2RLT = 0;
    UINT2               u2ETime = 0;
    tIsisLSPEntry      *pLSPHdr = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdUpdateRLTime ()\n"));

    /* For Self LSPs, RLTime Need not be updated, Zero Age and Max Age Timers
     * are not Started, and 'pPrevTimerLink' will be NULL
     */

    if (pLSPRec->pPrevTimerLink == NULL)
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdUpdateRLTime ()\n"));
        return;
    }

    pLSPHdr = pLSPRec;

    /* Travel backward till the header is reached
     */

    while (((tIsisTmrHdr *) pLSPHdr)->pPrevTimerLink != NULL)
    {
        pLSPHdr = pLSPHdr->pPrevTimerLink;
    }

    ISIS_EXTRACT_RLT_FROM_LSP (pLSPRec->pu1LSP, u2RLT);

    /* The time difference must be atleast '1'
     */

    u2ETime = (UINT2)
        ISIS_MAX ((ISIS_TIME_DIFF (pContext->SysTimers.TmrECInfo.u4TCount,
                                   ((tIsisTmrHdr *) pLSPHdr)->u4TStamp)), 1);
    if (u2ETime > u2RLT)
    {
        /* This case arises only if the RLT is about to elapse and hence we can
         * mark the RLT as '1'. This means the LSP will be processed shortly for
         * Remaining Life Time Expiry during which time the proper processing of
         * the LSP will take place
         *
         * NOTE: Should never happen in normal execution flow
         */

        u2RLT = 1;
    }
    else
    {
        u2RLT = (UINT2) (u2RLT - u2ETime);
    }

    ISIS_SET_LSP_RLT (pLSPRec->pu1LSP, u2RLT);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdUpdateRLTime ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdAddNbrInfo ()
 * Description : This function updates Neighbour information, included in the
 *               IS Adjacency TLV or IPRA TLV in the LSP buffer, into the SPT 
 *               Node if the Adjacency supports the given Metric
 * Input(s)    : pContext     - Pointer to System Context
 *               pu1LSPBuf    - Pointer pointing to IS ADjacency TLV in the LSP
 *                              Buffer
 *               u1MetricIdx  - Metric Index
 *               u1LSPFlag    - Flag indicating the Attached status of the
 *                              system for the given Metric
 *               u1NbrType    - Specifies whether the information being added to
 *                              the SPT node is IS or IPRA information
 *               bIsPseudoLSP - To identify whether the Processing LSP is
 *                              Pseudonode LSP or not. 
 *                              ISIS_TRUE  - Pseudonode LSP
 *                              ISIS_FALSE - Non-Pseudonode LSP
 * Output(s)   : pNode        - Pointer to SPT Node updated with IS Adjacency
 *                              information from the LSP buffer
 * Globals     : Not Referred or Modified              
 * Returns     : ISIS_SUCCESS
 *               ISIS_FAILURE
 *****************************************************************************/

PUBLIC INT4
IsisUpdAddNbrInfo (tIsisSysContext * pContext, UINT1 *pu1LSPBuf,
                   UINT1 u1MetricIdx, UINT1 u1LSPFlag, UINT1 u1NbrType,
                   tIsisSPTNode ** pNode, UINT1 u1Code, UINT1 u1Level,
                   BOOL1 bIsPseudoLSP)
{
    tIsisLSPEntry      *pLSPEntry = NULL;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisLSPTxEntry    *pLSPTxEntry = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisHashBucket    *pDbHashBkt = NULL;
    tIsisHashBucket    *pTxHashBkt = NULL;
    UINT1               au1LSPId[ISIS_LSPID_LEN] = { 0 };
    UINT1               au1RecdMetric[4];
    UINT1               au1IPMask[ISIS_MAX_IP_ADDR_LEN];
    UINT4               u4Metric = 0;
    UINT2               u2Metric = 0;
    UINT1               u1MSFlag = ISIS_TRUE;
    UINT1               u1MetricType = ISIS_DEF_MET;
    UINT1               u1CtrlOctet;
    UINT1               u1Prefixlen;
    UINT1               u1TmpLspFlag = 0;
    UINT1               u1DbFlag = ISIS_LSP_NOT_EQUAL;
    UINT1               u1TxFlag = ISIS_LSP_NOT_EQUAL;
    UINT1               u1Loc = ISIS_LOC_NONE;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddNbrInfo ()\n"));

    /* pu1LSPBuf points to the Value field of the TLV (skipping the Virtual
     * Flag). In IS Neighbour TLV the first 4 bytes will be the Metric values
     * (we have already skipped the Virtual Flag), and hence copy the 4 bytes of
     * Metrics directly
     */

    MEMCPY (au1RecdMetric, pu1LSPBuf, 4);

    COPY_U4_FROM_BUF (u4Metric, au1RecdMetric);

    ISIS_DBG_PRINT_ID (au1RecdMetric, 4, "UPD <T> : Extracted Metric\t",
                       ISIS_OCTET_STRING);

    if ((u1Code == ISIS_IPV6_RA_TLV)
        && (pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC)
        && (u4Metric > ISIS_LL_CKTL_MAX_METRIC))
    {
        /*Ignoring the IPv6 RA TLV */
        *pNode = NULL;
        return ISIS_FAILURE;
    }

    if (u1Code != ISIS_IPV6_RA_TLV)
    {
        u1MetricType = IsisUtlGetMetType (pContext, u1MetricIdx);

        if (u1MetricType >= ISIS_NUM_METRICS)
        {
            return ISIS_FAILURE;
        }
        switch (u1MetricType)
        {
            case ISIS_DEF_MET:

                /* The Default Metric should be always supported hence extract the 
                 * Metric Value directly
                 */

                u4Metric = (UINT4) au1RecdMetric[0];
                if (u1LSPFlag & ISIS_FLG_ATT_DEF)
                {
                    /* The given system is announcing that it is attached via
                     * Default Metric. Hence we can set the Attached bit for the
                     * metric in the SPT node information bits
                     */

                    u2Metric |= ISIS_SPT_ATT_FLAG;
                }
                break;

            case ISIS_DEL_MET:

                if ((au1RecdMetric[ISIS_DEL_MET] & ISIS_METRIC_SUPP_MASK) != 0)
                {
                    /* The given system does not support the Delay Metric. Note it
                     * down, we need not do any further processing if the Metric
                     * itself is not supported
                     */

                    u1MSFlag = ISIS_FALSE;
                }
                else
                {
                    u4Metric = (UINT4) (au1RecdMetric[ISIS_DEL_MET] &
                                        ISIS_METRIC_VAL_MASK);
                    if (u1LSPFlag & ISIS_FLG_ATT_DEL)
                    {
                        /* The given system is announcing that it is attached via
                         * Delay Metric. Hence we can set the Attached bit for the
                         * metric in the SPT node information bits
                         */

                        u2Metric |= ISIS_SPT_ATT_FLAG;
                    }
                }
                break;

            case ISIS_ERR_MET:

                if ((au1RecdMetric[ISIS_ERR_MET] & ISIS_METRIC_SUPP_MASK) != 0)
                {
                    /* The given system does not support the Error Metric. Note it
                     * down, we need not do any further processing if the Metric
                     * itself is not supported
                     */

                    u1MSFlag = ISIS_FALSE;
                }
                else
                {
                    u4Metric = (UINT4) (au1RecdMetric[ISIS_ERR_MET] &
                                        ISIS_METRIC_VAL_MASK);
                    if (u1LSPFlag & ISIS_FLG_ATT_ERR)
                    {
                        /* The given system is announcing that it is attached via
                         * Error Metric. Hence we can set the Attached bit for the
                         * metric in the SPT node information bits
                         */

                        u2Metric |= ISIS_SPT_ATT_FLAG;
                    }
                }
                break;

            case ISIS_EXP_MET:

                if ((au1RecdMetric[ISIS_EXP_MET] & ISIS_METRIC_SUPP_MASK) != 0)
                {
                    /* The given system does not support the Expense Metric. Note it
                     * down, we need not do any further processing if the Metric
                     * itself is not supported
                     */

                    u1MSFlag = ISIS_FALSE;
                }
                else
                {
                    u4Metric = (UINT4) (au1RecdMetric[ISIS_EXP_MET] &
                                        ISIS_METRIC_VAL_MASK);
                    if (u1LSPFlag & ISIS_FLG_ATT_EXP)
                    {
                        /* The given system is announcing that it is attached via
                         * Expense Metric. Hence we can set the Attached bit for the
                         * metric in the SPT node information bits
                         */

                        u2Metric |= ISIS_SPT_ATT_FLAG;
                    }
                }
                break;
        }

        if (u1MSFlag == ISIS_FALSE)
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : IS Adjacency Does Not Support The Metric [ %s ]\n",
                     ISIS_GET_METRIC_TYPE_STR (pContext, u1MetricType)));
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddNbrInfo ()\n"));
            return ISIS_SUCCESS;

        }
    }
    /* The given system supports the given metric for this adjacency. We can
     * add this information into the SPT node
     */

    *pNode = (tIsisSPTNode *) ISIS_MEM_ALLOC (ISIS_BUF_SPTN,
                                              sizeof (tIsisSPTNode));
    if (*pNode == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SPT Node\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_SPTN);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddNbrInfo ()\n"));
        return ISIS_FAILURE;
    }

    /* Set the EXTERNAL Bit in u2Metric of SPT node if the Destination is 
     * reachable by external metric
     */
    if (u1Code != ISIS_IPV6_RA_TLV)
    {
        if (au1RecdMetric[0] & ISIS_METRIC_TYPE_MASK)
        {
            /* External Metric Bit is set. The Metric is External 
             */

            u2Metric = (UINT2) (u2Metric | ISIS_SPT_EXT_MET_FLAG);
        }
    }
    else
    {
        MEMCPY (&u1CtrlOctet, (pu1LSPBuf + 4), ISIS_CTRL_OCTET_LEN);
        if (u1CtrlOctet & ISIS_METRIC_TYPE_MASK)
        {

            /* External Metric Bit is set. The Metric is External 
             */
            u2Metric = (UINT2) (u2Metric | ISIS_SPT_EXT_MET_FLAG);
        }

    }
    if (u1NbrType == ISIS_OSI_SYSTEM)
    {
        /*  Set the System Type of the Destination which is used for finding 
         *  nearest L2 by decision process
         */

        /* IS Type in LSP holds the following values
         * 0 - Unused value
         * 1 - (i.e. bit 1 set) Level 1 Intermediate system
         * 2 - Unused value
         * 3 - (i.e. bits 1 and 2 set) Level 2 Intermediate system.
         *
         * It will never carry the value for L12 system as defined in standard*/
        if ((u1LSPFlag & ISIS_ISTYPE_MASK) == IS_TYPE_LEVEL1)
        {
            u2Metric |= ISIS_SPT_L1_SYS_MASK;
        }
        else if ((u1LSPFlag & ISIS_ISTYPE_MASK) == IS_TYPE_LEVEL2)
        {
            u2Metric |= ISIS_SPT_L2_SYS_MASK;
        }
        /* The given adjacency is an IS adjacency, mark it in the Metric bits
         */

        u2Metric |= ISIS_SPT_IS_FLAG;

        /* Update the other information into the SPT node viz. Destination ID,
         * Metric Value, Max Paths etc.
         */
        MEMCPY ((*pNode)->au1DestID, (pu1LSPBuf + 4),
                (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));

        if (bIsPseudoLSP == ISIS_TRUE)
        {
            MEMCPY (au1LSPId, (*pNode)->au1DestID, ISIS_LSPID_LEN);

            pLSPEntry =
                IsisUpdGetLSPFromDB (pContext, au1LSPId, u1Level, &pPrevDbRec,
                                     &pDbHashBkt, &u1Loc, &u1DbFlag);
            pLSPTxEntry =
                IsisUpdGetLSPFromTxQ (pContext, au1LSPId, u1Level, &pPrevTxRec,
                                      &pTxHashBkt, &u1Loc, &u1TxFlag);

            if (u1DbFlag != ISIS_LSP_EQUAL)
            {
                /* LSP Zero for the given system is not found in the Database. Try it
                 * out in Transmission Queue
                 */

                if ((u1TxFlag == ISIS_LSP_EQUAL) && (pLSPTxEntry != NULL))    /*klocwork */
                {
                    ISIS_GET_1_BYTE (pLSPTxEntry->pLSPRec->pu1LSP,
                                     (UINT2) (ISIS_OFFSET_CHKSUM + 2),
                                     u1TmpLspFlag);
                }
                else
                {
                    /* LSP Zero is not present in Transmission Queue also, hence
                     * ignore all the LSPs of this particular system
                     */

                    ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) ISIS_LSPID_LEN,
                                       "UPD <T> : LSP Zero Not Found For\t",
                                       ISIS_OCTET_STRING);

                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdAddNbrInfo ()\n"));
                    ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) *pNode);
                    return ISIS_FAILURE;
                }
            }
            else
            {
                if (pLSPEntry != NULL)    /*klocwork */
                {
                    ISIS_GET_1_BYTE (pLSPEntry->pu1LSP,
                                     (UINT2) (ISIS_OFFSET_CHKSUM + 2),
                                     u1TmpLspFlag);
                }
                else
                {
                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdAddNbrInfo ()\n"));
                    ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) *pNode);
                    return ISIS_FAILURE;
                }
            }
        }                        /* End of bIsPseudoLSP */

        if (u1TmpLspFlag & ISIS_FLG_ATT_DEF)
        {
            u2Metric = (UINT2) (u2Metric | ISIS_SPT_ATT_FLAG);
        }
    }
    else if (u1NbrType == ISIS_IP_END_SYSTEM)
    {
        /* The given adjacency is an IPRA adjacency, mark it in the Metric bits
         */

        u2Metric &= ~ISIS_SPT_IS_FLAG;
        u2Metric |= ISIS_NBR_IPRA_ADJ;

        if (u1Code == ISIS_IPV6_RA_TLV)
        {
            /* Extract the control octet information for the UP/DOWN info
             */
            MEMCPY (&u1CtrlOctet, (pu1LSPBuf + 4), ISIS_CTRL_OCTET_LEN);

            if (u1CtrlOctet & ISIS_FLG_DOWN_SET)
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    SPT_SET_UPDOWN_FLAG ((*pNode), ISIS_RL_L1_DOWN);
                }
                else
                {
                    SPT_SET_UPDOWN_FLAG ((*pNode), ISIS_RL_L2_DOWN);
                }
            }
            else
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    SPT_SET_UPDOWN_FLAG ((*pNode), ISIS_RL_L1_UP);
                }
                else
                {
                    SPT_SET_UPDOWN_FLAG ((*pNode), ISIS_RL_L2_UP);
                }
            }

            u1Prefixlen = *(pu1LSPBuf + ISIS_IPV6RA_PREFIXLEN_OFFSET);
            IsisUtlGetIPMask (u1Prefixlen, au1IPMask);
            (*pNode)->u1PrefixLen = u1Prefixlen;
            u1Prefixlen = (UINT1) ((u1Prefixlen -
                                    1) / ISIS_IPV6RA_PREFIX_LEN_ROUNDOFF + 1);
            MEMCPY (((*pNode)->au1DestID),
                    (pu1LSPBuf + ISIS_IPV6RA_PREFIX_OFFSET), u1Prefixlen);
            MEMCPY (((*pNode)->au1DestID) + ISIS_MAX_IPV6_ADDR_LEN, au1IPMask,
                    ISIS_MAX_IPV6_ADDR_LEN);
            u2Metric |= ISIS_IPV6_ADDR_TYPE;
            (*pNode)->u1AddrType = ISIS_ADDR_IPV6;
        }
        else
        {
            /* IPV4 case */
            /* Update the other information into the SPT node viz. Destination ID,
             * Metric Value, Max Paths etc.
             */
            if (au1RecdMetric[0] & ISIS_FLG_DOWN_SET)
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    SPT_SET_UPDOWN_FLAG ((*pNode), ISIS_RL_L1_DOWN);
                }
                else
                {
                    SPT_SET_UPDOWN_FLAG ((*pNode), ISIS_RL_L2_DOWN);
                }
            }
            else
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    SPT_SET_UPDOWN_FLAG ((*pNode), ISIS_RL_L1_UP);
                }
                else
                {
                    SPT_SET_UPDOWN_FLAG ((*pNode), ISIS_RL_L2_UP);
                }
            }
            MEMCPY (((*pNode)->au1DestID),
                    (pu1LSPBuf + ISIS_IPV4RA_PREFIX_OFFSET),
                    2 * ISIS_MAX_IPV4_ADDR_LEN);
            u2Metric |= ISIS_IPV4_ADDR_TYPE;
            (*pNode)->u1AddrType = ISIS_ADDR_IPV4;
        }

    }

    (*pNode)->au2Metric[u1MetricType] = u2Metric;
    (*pNode)->u4MetricVal = u4Metric;
    (*pNode)->au1MaxPaths[u1MetricType] = 1;
    (*pNode)->pNext = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddNbrInfo ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdValidatePDU ()
 * Description : This function Validates the Received LSPs, PSNPs and CSNPs. It
 *               verifies the origination of PDU (whether internal to the domain
 *               or external), checks whether the peer system has a valid
 *               adjacency with the local system (in the case of Broadcast
 *               circuits), and also authenticates the received PDU.
 * Input(s)    : pContext  - Pointer to System Context
 *               pu1RcvdPDU- Pointer to the Rcvd PDU
 *               pCktRec   - Pointer to the CktEntry on which the LSP is
 *                           received 
 *               pu1SNPA   - Pointer to the SNPA address of the System which
 *                           generates this  LSP  
 *               u1Level   - Level of the Circuit over which the PDU 
 *                           is received.  
 * Output(s)   : None
 * Globals     : Not Referred or Modified             
 * Returns     : ISIS_SUCCESS, if Validation successful
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisUpdValidatePDU (tIsisSysContext * pContext, UINT1 *pu1RcvdPDU,
                    tIsisCktEntry * pCktRec, UINT1 *pu1SNPA, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdValidatePDU ()\n"));

    if ((pCktRec->u1CktLevel != u1Level)
        && (pCktRec->u1CktLevel != ISIS_LEVEL12))
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <E> : PDU Validation - Incompatible Level - Self [ %u ], Peer [ %u ]\n",
                 pCktRec->u1CktLevel, u1Level));
        return (ISIS_FAILURE);
    }

    i4RetVal = IsisUtlValidatePDU (pContext, pCktRec, pu1RcvdPDU);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* The received packet is not from external domain and the ID lenghts
         * matched
         */

        i4RetVal = IsisAdjIsValidAdj (pCktRec, pu1SNPA, u1Level);

        if (i4RetVal != ISIS_SUCCESS)
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <E> : PDU Validation - Adjacency Does Not Exist - Circuit [ %u ]\n",
                     pCktRec->u4CktIdx));
            return i4RetVal;
        }

        i4RetVal = IsisUtlAuthenticatePDU (pContext, NULL, pu1RcvdPDU);

        if (i4RetVal == ISIS_FAILURE)
        {
            WARNING ((ISIS_LGST,
                      "UPD <T> : Authentication Failed for the received PDUs\n"));
            return (i4RetVal);
        }

        if (u1Level == ISIS_LEVEL1)
        {
            i4RetVal = IsisUtlMaxAreaAddrCheck (pContext, pCktRec, pu1RcvdPDU);
            if (i4RetVal == ISIS_FAILURE)
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <E> : PDU Validation Failed - Max Area Address Check - Circuit [ %u ]\n",
                         pCktRec->u4CktIdx));
            }
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdValidatePDU ()\n"));
    return (i4RetVal);
}

/*******************************************************************************
 * Function    : IsisUpdUpdateAuthInPDU ()
 * Description : This function does the following:
 *               -- if authentication is enabled in the Tx direction, and if
 *                  authentication information is not available in the given 
 *                  PDU, then it triggers the addition of authentication 
 *                  information
 *               -- if authentication is disabled in the Tx direction, and if
 *                  authentication information is available in the given 
 *                  PDU, then it triggers the deletion of authentication 
 *                  information
 * Input(s)    : pContext  - Pointer to System Context
 *               pu1LSP    - Pointer to LSP 
 *               pLSP      - Pointer to SelfLSP information, which has the
 *                           Authentication Status in the PDU
 *               pu2Offset - Offset into the LSP Buffer where the Authntiation
 *                           information is to be added
 *               u1Level   - Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdUpdateAuthInPDU (tIsisSysContext * pContext, UINT1 *pu1LSP,
                        tIsisLSPInfo * pLSP, UINT2 *pu2Offset, UINT1 u1Level)
{
    if (pLSP->u2AuthStatus == ISIS_TRUE)
    {
        if (((u1Level != ISIS_LEVEL2)
             && (pContext->SysActuals.SysAreaTxPasswd.u1Len == 0))
            || ((u1Level != ISIS_LEVEL1)
                && (pContext->SysActuals.SysDomainTxPasswd.u1Len == 0)))
        {
            IsisUpdRemoveAuthFromLSP (pu1LSP, pu2Offset);
            pLSP->u2AuthStatus = ISIS_FALSE;
        }
    }
    else
    {
        if (((u1Level != ISIS_LEVEL2)
             && (pContext->SysActuals.SysAreaTxPasswd.u1Len != 0))
            || ((u1Level != ISIS_LEVEL1)
                && (pContext->SysActuals.SysDomainTxPasswd.u1Len != 0)))
        {
            IsisUpdAddAuthToPDU (pContext, pu1LSP, pu2Offset, u1Level);
            pLSP->u2AuthStatus = ISIS_TRUE;
        }
    }
    ISIS_SET_CTRL_PDU_LEN (pu1LSP, *pu2Offset);
}

/*******************************************************************************
 * Function    : IsisUpdAddAuthToPDU ()
 * Description : This function adds the authentication information to the given
 *               PDU
 * Input(s)    : pContext  - Pointer to System Context
 *               pu1LSP    - Pointer to LSP 
 *               pu2Offset - Offset into the LSP Buffer where the Authntiation
 *                           information is to be added
 *               u1Level   - Level of the LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * RETURN(s)   : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdAddAuthToPDU (tIsisSysContext * pContext, UINT1 *pu1LSP,
                     UINT2 *pu2Offset, UINT1 u1Level)
{
    UINT1               u1PasswdLen = 0;
    UINT1               u1AuthType = 0;
    UINT1               u1PduType = 0;
    UINT2               u2MD5DigestOffset = 0;
    UINT1               au1CalcDigest[16];
    UINT2               u2Rlt = 0;
    UINT2               u2Cs = 0;
    unUtilAlgo          UtilAlgo;

    UINT1              *pPasswd = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisAddAuthToPDU ()\n"));
    MEMSET (au1CalcDigest, 0, sizeof (au1CalcDigest));
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    u1PduType = ISIS_EXTRACT_PDU_TYPE (pu1LSP);
    u1PduType &= ISIS_PDU_TYPE_MASK;

    u1AuthType = (UINT1) (((u1Level == ISIS_LEVEL1) ?
                           pContext->SysActuals.u1SysAreaAuthType :
                           pContext->SysActuals.u1SysDomainAuthType));

    u1PasswdLen = (UINT1) (((u1Level == ISIS_LEVEL1) ?
                            pContext->SysActuals.SysAreaTxPasswd.u1Len :
                            pContext->SysActuals.SysDomainTxPasswd.u1Len));
    pPasswd = ((u1Level == ISIS_LEVEL1) ?
               pContext->SysActuals.SysAreaTxPasswd.au1Password :
               pContext->SysActuals.SysDomainTxPasswd.au1Password);

    if (u1AuthType == ISIS_AUTH_PASSWD)
    {

        if ((u1PasswdLen != 0))
        {
            /* Construct the TLV with Code, LEngth, authentication Type and the
             * Password
             */

            ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, ISIS_AUTH_INFO_TLV);
            *pu2Offset = (UINT2) (*pu2Offset + 1);

            ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, (u1PasswdLen + 1));
            *pu2Offset = (UINT2) (*pu2Offset + 1);

            ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, ISIS_CLR_TXT_PASS);
            *pu2Offset = (UINT2) (*pu2Offset + 1);
            MEMCPY ((pu1LSP + *pu2Offset), pPasswd, u1PasswdLen);
            *pu2Offset = (UINT2) (*pu2Offset + u1PasswdLen);
            ISIS_SET_CTRL_PDU_LEN (pu1LSP, *pu2Offset);
        }
    }
    else if (u1AuthType == ISIS_AUTH_MD5)
    {
        if (u1PasswdLen != 0)
        {
            if ((u1PduType == ISIS_L1LSP_PDU) || (u1PduType == ISIS_L2LSP_PDU))
            {

                ISIS_EXTRACT_RLT_FROM_LSP (pu1LSP, u2Rlt);
                ISIS_EXTRACT_CS_FROM_LSP (pu1LSP, u2Cs);
                ISIS_ASSIGN_2_BYTES (pu1LSP, ISIS_OFFSET_CHKSUM, 0);
                ISIS_ASSIGN_2_BYTES (pu1LSP, ISIS_OFFSET_RLT, 0);
            }
            ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, ISIS_AUTH_INFO_TLV);
            *pu2Offset = (UINT2) (*pu2Offset + 1);
            ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, ISIS_MD5_DIGEST_LEN + 1);
            *pu2Offset = (UINT2) (*pu2Offset + 1);
            ISIS_ASSIGN_1_BYTE (pu1LSP, *pu2Offset, ISIS_MD5_PASS);
            *pu2Offset = (UINT2) (*pu2Offset + 1);
            u2MD5DigestOffset = *pu2Offset;

            MEMSET ((pu1LSP + *pu2Offset), 0, ISIS_MD5_DIGEST_LEN);
            *pu2Offset = (UINT2) (*pu2Offset + ISIS_MD5_DIGEST_LEN);

            ISIS_SET_CTRL_PDU_LEN (pu1LSP, *pu2Offset);
            UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = (unsigned char *) pu1LSP;
            UtilAlgo.UtilHmacAlgo.i4HmacPktLength = *pu2Offset;
            UtilAlgo.UtilHmacAlgo.pu1HmacKey = pPasswd;
            UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = u1PasswdLen;
            UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest =
                (unsigned char *) &au1CalcDigest;

            UtilHash (ISS_UTIL_ALGO_HMAC_MD5, &UtilAlgo);
            MEMCPY ((pu1LSP + u2MD5DigestOffset), au1CalcDigest,
                    ISIS_MD5_DIGEST_LEN);

            if ((u1PduType == ISIS_L1LSP_PDU) || (u1PduType == ISIS_L2LSP_PDU))
            {
                ISIS_ASSIGN_2_BYTES (pu1LSP, ISIS_OFFSET_CHKSUM, u2Cs);
                ISIS_ASSIGN_2_BYTES (pu1LSP, ISIS_OFFSET_RLT, u2Rlt);
            }
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisAddAuthToPDU ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdCompAAFromBuf ()
 * Description : This function compares the Area addresses of the given LSP and
 *               the Area Addresses of the local system and decides whether the
 *               given system belongs to the same area as the local system or it
 *               belongs to some other area
 * Input(s)    : pContext  - Pointer to System Context
 *               pu1LSPBuf - Pointer pointing to the Area Address TLV
 *               u1Len     - Length of Area Address TLV
 * Output(s)   : None
 * Globals     : Not Referred or Modified             
 * Returns     : ISIS_SAME_AREA, if the one or more of LSPs Area Addresses
 *                               matches one or more Area Addresses of the 
 *                               local system
 *               ISIS_OTHER_AREA, Otherwise
 ******************************************************************************/

PUBLIC UINT1
IsisUpdCompAAFromBuf (tIsisSysContext * pContext, UINT1 *pu1LSPBuf, UINT1 u1Len)
{
    UINT1               u1RetVal = ISIS_OTHER_AREA;
    UINT1               u1Offset = 0;
    UINT1               u1AALen = 0;
    tIsisAAEntry       *pAARec = NULL;
    tIsisMAAEntry      *pMAARec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdCompAAFromBuf ()\n"));

    /* We have to match the Area Addresses from the LSPs with both the Manual
     * Area Addresses configured by the manager, and the Area Addresses computed
     * from all the received L1 LSPs. This is because different systems in an 
     * Area may have different Area Addresses (some addresses being common). If
     * any system is to belong to this Area then atleast one of its addresses 
     * must match one of the addresses of the Area (Area Addresses of all
     * systems put together). Otherwise the system belongs to a different Area
     */

    while (u1Len > 0)
    {
        ISIS_GET_1_BYTE (pu1LSPBuf, u1Offset, u1AALen);
        u1Offset++;
        u1Len--;

        for (pAARec = pContext->AreaAddrTable.pAARec; pAARec != NULL;
             pAARec = pAARec->pNext)
        {
            /* Both the length of the Area Address and the Address itself must
             * be the same for a match
             */

            if ((pAARec->AreaAddr.u1Length == u1AALen)
                &&
                (MEMCMP
                 (pAARec->AreaAddr.au1AreaAddr, (pu1LSPBuf + u1Offset),
                  u1AALen)) == 0)

            {
                /* Even if one Area Address matches, the system belongs to the
                 * same area as the local system
                 */

                u1RetVal = ISIS_SAME_AREA;
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdCompAAFromBuf ()\n"));
                return (u1RetVal);
            }
        }

        for (pMAARec = pContext->ManAddrTable.pMAARec; pMAARec != NULL;
             pMAARec = pMAARec->pNext)
        {
            /* Both the length of the Area Address and the Address itself must
             * be the same for a match
             */

            if ((pMAARec->ManAreaAddr.u1Length == u1AALen)
                && (MEMCMP (pMAARec->ManAreaAddr.au1AreaAddr,
                            (pu1LSPBuf + u1Offset), u1AALen)) == 0)

            {
                /* Even if one Area Address matches, the system belongs to the
                 * same area as the local system
                 */

                u1RetVal = ISIS_SAME_AREA;
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdCompAAFromBuf ()\n"));
                return (u1RetVal);
            }
        }

        u1Offset = (UINT1) (u1Offset + u1AALen);
        u1Len = (UINT1) (u1Len - u1AALen);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdCompAAFromBuf ()\n"));
    return (u1RetVal);
}

/*******************************************************************************
 * Function    : IsisUpdConstructCSNP ()
 * Description : This function constructs CSNP buffer from the LSPs present in
 *               transmission queue and database. It starts with the LSP marks
 *               from the TxQ and Database and constructs the required PDU upto
 *               a maximum buffer size. It checks for Authentication information
 *               and adds it to the PDU if required. It ensures a sorted order
 *               of LSPs in the CSNP buffer
 * Input(s)    : pContext    - Pointer to System Context
 *               pLSPTxQMark - Pointer to LSP in Transmission Queue from where
 *                             the LSPs are supposed to be taken for CSNP
 *                             constuction
 *               pLSPDBMark  - Pointer to LSP in Database from where the LSPs
 *                             are supposed to be taken for CSNP construction
 *               pu1CSNP     - Pointer to CSNP
 *               u1Level     - Level of the CSNP
 * Output(s)   : pLSPTxQMark and pLSPDBMark updated after CSNP is constructed
 *               and transmitted
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC INT4
IsisUpdConstructCSNP (tIsisSysContext * pContext,
                      tIsisLSPTxEntry ** pLSPTxQMark,
                      tIsisLSPEntry ** pLSPDBMark, UINT1 **pu1CSNP,
                      UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1TLVLen = 0;
    UINT1               u1NewTLV = 0;
    UINT2               u2Offset = 0;
    UINT2               u2CSNPLen = 0;
    UINT2               u2OffsetMark = 0;
    UINT2               u2Rlt = 0;
    tIsisLSPEntry      *pEndMark = NULL;
    tIsisLSPEntry      *pBegMark = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdConstructCSNP ()\n"));

    *pu1CSNP = (UINT1 *) ISIS_MEM_ALLOC (ISIS_BUF_CSNP,
                                         ISIS_GET_MAX_BUF_LEN (pContext,
                                                               u1Level));
    if (*pu1CSNP == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Constructing CSNP\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_CSNP);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdConstructCSNP ()\n"));
        return ISIS_FAILURE;
    }

    if (u1Level == ISIS_LEVEL1)
    {
        IsisUtlFillComHdr (pContext, *pu1CSNP, (UINT1) ISIS_L1CSNP_PDU);
    }
    else if (u1Level == ISIS_LEVEL2)
    {
        IsisUtlFillComHdr (pContext, *pu1CSNP, (UINT1) ISIS_L2CSNP_PDU);
    }

    /* Skip the PDU length field, which will be filled at the end
     */

    u2Offset = (sizeof (tIsisComHdr) + 2);

    /* Fill the SourceID
     */

    ISIS_ASSIGN_STRING (*pu1CSNP, u2Offset, pContext->SysActuals.au1SysID,
                        ISIS_SYS_ID_LEN);
    u2Offset = (UINT2) (u2Offset + ISIS_SYS_ID_LEN);

    /* Pseudonode ID must be filled as ZERO. Refer Sec. 9.10 of ISO 10589
     */

    ISIS_ASSIGN_1_BYTE (*pu1CSNP, u2Offset, 0);
    u2Offset++;

    /* We now have to fill Start LSPID and End LSPID here. We will mark the
     * Offset and fill these values later after all the LETLVs have been filled
     * in the PDU. 
     * Move the PDU Offset by the length of (Start LSP ID and the End LSP Id) 
     */

    u2OffsetMark = u2Offset;
    u2Offset = (UINT2) (u2Offset + (2 * ISIS_LSPID_LEN));

    u2CSNPLen = (UINT2) ISIS_GET_MAX_BUF_LEN (pContext, u1Level);

    /* If Authentication is enabled we may have to add the Authentication
     * information in the PDU. So leave enough space for the Authentication
     * information in the PDU
     */

    u2CSNPLen -= (UINT1) (((u1Level == ISIS_LEVEL1) ?
                           pContext->SysActuals.SysAreaTxPasswd.u1Len :
                           pContext->SysActuals.SysDomainTxPasswd.u1Len));

    /* LOGIC: We have two lists, one is the TxQ and the other is the Database.
     * Specification restricts us to add LSPID in a sorted order into the CSNP
     * buffer. The algorithm proceeds as follows:
     *
     * STEP1: If TxQ Mark is NULL, then we directly add LSPs from the Database
     * STEP2: If Database Mark is NULL, then we directly ad LSPs from the TxQ
     * STEP3: If both TxQ and Database are non-NULL, then we compare the LSPIds
     *        of LSPs in TxQ and Database. We add the LSPID which is 
     *        lexicographically samller and advance the pointer.
     * We repeat the above steps till both TxQ and Database Marks become NULL.
     *
     * While adding LSPIDs we have to construct TLVs which can hold upto a
     * maximum of 255 bytes. If we exceed this limit we have to add a new TLV, 
     * if we have enough buffer space. Otherwise we may have to stop at that 
     * point and come back for constructing a new CSNP buffer. The same check
     * for Buffer space is made even while adding LSPIDs in TLVs. 
     */

    while ((u2Offset < u2CSNPLen)
           && ((*pLSPTxQMark != NULL) || (*pLSPDBMark != NULL)))
    {
        u1NewTLV = FALSE;
        if ((u1TLVLen == 0) || ((u1TLVLen + ISIS_LSP_ENTRY_TLV_LEN) > 255))
        {
            /* A TLV can hold only upto 255 bytes. If u1TLVLen is '0' or if
             * 'u1TLVLen + ISIS_LSP_ENTRY_TLV_LEN' is greatr than 255, then we
             * are supposed to start with a new TLV.
             */

            if ((u2Offset + ISIS_LSP_ENTRY_TLV_LEN + 2) > u2CSNPLen)
            {
                /* Exceeding BufferSize, we may have to construct a new CSNP
                 * buffer.
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Adding A New LETLV Into CSNP\n"));
                UPP_PT ((ISIS_LGST, "UPD <T> : MaxBufSize Reached\n"));
                break;
            }
            if (u1TLVLen != 0)
            {
                /* Now that the previous TLV is completely filled, we shall fill
                 * the TLV length. TLV length must be filled at an offset which
                 * is (Current Offset - Length of the TLV - 1).
                 */

                ISIS_ASSIGN_1_BYTE (*pu1CSNP, (u2Offset - u1TLVLen - 1),
                                    u1TLVLen);
            }

            /* We can add a new TLV to the current CSNP buffer
             * Fill the Code, Length of the TLV will be filled once the TLV is
             * full or CSNP encoding is complete which ever happens first
             */

            ISIS_ASSIGN_1_BYTE (*pu1CSNP, u2Offset, ISIS_LSP_ENTRY_TLV);
            u1NewTLV = TRUE;
            /* Resetting u1TLVLen here since a new TLV is being encoded
             */

            u1TLVLen = 0;

            /* Skipping Code and Length fields, to fill in the Value field with
             * LETLVs
             */

            u2Offset = (UINT2) (u2Offset + 2);
        }
        else if ((u2Offset + ISIS_LSP_ENTRY_TLV_LEN) > u2CSNPLen)
        {
            /* We have space in the current TLV, but addiion of an LETLV might
             * cause the CSNP buffer to overflow. We may have to construct a new
             * CSNP buffer to include the remaining information
             */

            UPP_PT ((ISIS_LGST, "UPD <T> : Adding LETLV Entry Into CSNP\n"));
            UPP_PT ((ISIS_LGST, "UPD <T> : MaxBufSize Reached\n"));
            break;
        }

        /* At this point, we have verified that we have enough space in the
         * buffer to add the LETLV information. Proceed and construct CSNPs with
         * LSPs taken from TxQ and Dataase
         */

        if (*pLSPTxQMark == NULL)
        {
            /* We have exhausted the Transmit Queue, hence take LSPs only from
             * the Database
             */

            /* Copy the RLT
             */

            ISIS_EXTRACT_RLT_FROM_LSP (((*pLSPDBMark)->pu1LSP), u2Rlt);
            if (u2Rlt == 0)
            {
                pEndMark = *pLSPDBMark;
                *pLSPDBMark = (*pLSPDBMark)->pNext;
                if (u1NewTLV == TRUE)
                {
                    u2Offset = (u2Offset - 2);
                }
                continue;
            }

            MEMCPY ((*pu1CSNP + u2Offset),
                    ((*pLSPDBMark)->pu1LSP + ISIS_OFFSET_RLT),
                    ISIS_LSP_ENTRY_TLV_LEN);

            if ((u1TLVLen == 0) && (pBegMark == NULL))
            {
                /* We may need mark this to fill the Start LSPID in the LSP
                 * header
                 */

                pBegMark = *pLSPDBMark;
            }

            /* Keep updating the current LSP added to the CSNP as the End LSP.
             * This is work because LSPIDs are added in a sorted order to the
             * CSNP
             */

            pEndMark = *pLSPDBMark;
            *pLSPDBMark = (*pLSPDBMark)->pNext;
            u1TLVLen = (UINT1) (u1TLVLen + ISIS_LSP_ENTRY_TLV_LEN);
            u2Offset = (UINT2) (u2Offset + ISIS_LSP_ENTRY_TLV_LEN);
            continue;
        }
        else if (*pLSPDBMark == NULL)
        {
            /* We have exhausted the Database, hence take LSPs only from
             * the Transmit Queue
             */

            /* Copy the RLT
             */
            ISIS_EXTRACT_RLT_FROM_LSP (((*pLSPTxQMark)->pLSPRec->pu1LSP),
                                       u2Rlt);
            if (u2Rlt == 0)
            {
                pEndMark = (*pLSPTxQMark)->pLSPRec;
                *pLSPTxQMark =
                    (tIsisLSPTxEntry *) ((*pLSPTxQMark)->pLSPRec->pNext);
                if (u1NewTLV == TRUE)
                {
                    u2Offset = (u2Offset - 2);
                }
                continue;
            }

            MEMCPY ((*pu1CSNP + u2Offset),
                    ((*pLSPTxQMark)->pLSPRec->pu1LSP + ISIS_OFFSET_RLT),
                    ISIS_LSP_ENTRY_TLV_LEN);

            if ((u1TLVLen == 0) && (pBegMark == NULL))
            {
                /* We may need mark this to fill the Start LSPID in the LSP
                 * header
                 */

                pBegMark = (*pLSPTxQMark)->pLSPRec;
            }

            /* Keep updating the current LSP added to the CSNP as the End LSP.
             * This is work because LSPIDs are added in a sorted order to the
             * CSNP
             */

            pEndMark = (*pLSPTxQMark)->pLSPRec;
            *pLSPTxQMark = (tIsisLSPTxEntry *) ((*pLSPTxQMark)->pLSPRec->pNext);
            u1TLVLen = (UINT1) (u1TLVLen + ISIS_LSP_ENTRY_TLV_LEN);
            u2Offset = (UINT2) (u2Offset + ISIS_LSP_ENTRY_TLV_LEN);
            continue;
        }

        /* If both LSP TxQ and LSP DB have valid LSPs compare the two LSP Ids 
         * and add the lesser one as the first entry in the TLV
         */

        i4RetVal =
            MEMCMP (((*pLSPTxQMark)->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                    ((*pLSPDBMark)->pu1LSP + ISIS_OFFSET_LSPID),
                    ISIS_LSPID_LEN);

        if (i4RetVal < 0)
        {
            /* Transmit Queue LSPID is lexicographically smaller than the
             * Database LSP
             */

            ISIS_EXTRACT_RLT_FROM_LSP (((*pLSPTxQMark)->pLSPRec->pu1LSP),
                                       u2Rlt);

            if (u2Rlt == 0)
            {
                pEndMark = (*pLSPTxQMark)->pLSPRec;
                *pLSPTxQMark =
                    (tIsisLSPTxEntry *) ((*pLSPTxQMark)->pLSPRec->pNext);
                if (u1NewTLV == TRUE)
                {
                    u2Offset = (u2Offset - 2);
                }
                continue;
            }

            ISIS_ASSIGN_STRING (*pu1CSNP, u2Offset,
                                ((*pLSPTxQMark)->pLSPRec->pu1LSP +
                                 ISIS_OFFSET_RLT), ISIS_LSP_ENTRY_TLV_LEN);

            if ((u1TLVLen == 0) && (pBegMark == NULL))
            {
                /* We may need mark this to fill the Start LSPID in the LSP
                 * header
                 */

                pBegMark = (*pLSPTxQMark)->pLSPRec;
            }

            /* Keep updating the current LSP added to the CSNP as the End LSP.
             * This is work because LSPIDs are added in a sorted order to the
             * CSNP
             */

            pEndMark = (*pLSPTxQMark)->pLSPRec;
            *pLSPTxQMark = (tIsisLSPTxEntry *) ((*pLSPTxQMark)->pLSPRec->pNext);
        }
        else if (i4RetVal > 0)
        {
            ISIS_ASSIGN_STRING (*pu1CSNP, u2Offset,
                                ((*pLSPDBMark)->pu1LSP + ISIS_OFFSET_RLT),
                                ISIS_LSP_ENTRY_TLV_LEN);
            ISIS_EXTRACT_RLT_FROM_LSP (((*pLSPDBMark)->pu1LSP), u2Rlt);
            if (u2Rlt == 0)
            {
                pEndMark = *pLSPDBMark;
                *pLSPDBMark = (*pLSPDBMark)->pNext;
                if (u1NewTLV == TRUE)
                {
                    u2Offset = (u2Offset - 2);
                }
                continue;
            }

            if ((u1TLVLen == 0) && (pBegMark == NULL))
            {
                /* We may need mark this to fill the Start LSPID in the LSP
                 * header
                 */

                pBegMark = *pLSPDBMark;
            }

            /* Keep updating the current LSP added to the CSNP as the End LSP.
             * This is work because LSPIDs are added in a sorted order to the
             * CSNP
             */

            pEndMark = *pLSPDBMark;
            *pLSPDBMark = (*pLSPDBMark)->pNext;
        }
        else
        {
            /* This should never happen. An LSP can never be in TxQ and at the
             * same time in DB
             */

            WARNING ((ISIS_LGST,
                      "UPD <E> : LSP Present In Both TxQ Queue And DB\n"));
            ISIS_EXTRACT_RLT_FROM_LSP (((*pLSPDBMark)->pu1LSP), u2Rlt);

            if (u2Rlt == 0)
            {
                pEndMark = *pLSPDBMark;
                *pLSPDBMark = (*pLSPDBMark)->pNext;
                *pLSPTxQMark =
                    (tIsisLSPTxEntry *) ((*pLSPTxQMark)->pLSPRec->pNext);
                if (u1NewTLV == TRUE)
                {
                    u2Offset = (u2Offset - 2);
                }
                continue;
            }

            ISIS_ASSIGN_STRING (*pu1CSNP, u2Offset,
                                ((*pLSPDBMark)->pu1LSP + ISIS_OFFSET_RLT),
                                ISIS_LSP_ENTRY_TLV_LEN);

            if ((u1TLVLen == 0) && (pBegMark == NULL))
            {
                /* We may need mark this to fill the Start LSPID in the LSP
                 * header
                 */

                pBegMark = *pLSPDBMark;
            }

            /* Keep updating the current LSP added to the CSNP as the End LSP.
             * This is work because LSPIDs are added in a sorted order to the
             * CSNP
             */

            pEndMark = *pLSPDBMark;
            *pLSPDBMark = (*pLSPDBMark)->pNext;
            *pLSPTxQMark = (tIsisLSPTxEntry *) ((*pLSPTxQMark)->pLSPRec->pNext);
        }

        u1TLVLen = (UINT1) (u1TLVLen + ISIS_LSP_ENTRY_TLV_LEN);
        u2Offset = (UINT2) (u2Offset + ISIS_LSP_ENTRY_TLV_LEN);
    }

    /* Fill the Length of the Last TLV
     */

    ISIS_ASSIGN_1_BYTE (*pu1CSNP, (UINT2) (u2Offset - u1TLVLen - 1), u1TLVLen);

    /* Fill up the Start LSP Id and the End LSP Id in the CSNP Header
     */
    if (pBegMark == NULL)        /*kloc Work */
    {

        ISIS_MEM_FREE (ISIS_BUF_CSNP, (UINT1 *) *pu1CSNP);
        return ISIS_FAILURE;
    }
    ISIS_ASSIGN_STRING (*pu1CSNP, u2OffsetMark,
                        (pBegMark->pu1LSP + ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);

    u2OffsetMark += ISIS_LSPID_LEN;
    if (pEndMark == NULL)
    {
        ISIS_MEM_FREE (ISIS_BUF_CSNP, (UINT1 *) *pu1CSNP);
        return ISIS_FAILURE;
    }
    ISIS_ASSIGN_STRING (*pu1CSNP, u2OffsetMark,
                        (pEndMark->pu1LSP + ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);

    /* The following routine fill the authentication information in the
     * CSNP PDU if authentication is supported.  It also fills the PDU Length.
     */

    IsisUpdAddAuthToPDU (pContext, *pu1CSNP, &u2Offset, u1Level);
    ISIS_SET_CTRL_PDU_LEN (*pu1CSNP, u2Offset);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdConstructCSNP ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdDelSelfLSPInfo ()
 * Description : This function deletes all the self LSPs maintained in the
 *               specified context
 * Input(s)    : pContext - Pointer to System Context
 *               u1Level  - Level of LSP
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisUpdDelSelfLSPInfo (tIsisSysContext * pContext, UINT1 u1Level)
{
    tIsisSNLSP         *pNextSNLSP = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pLSPInfo = NULL;
    tIsisLSPInfo       *pNextLSPInfo = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelSelfLSPInfo ()\n"));
    UPP_PT ((ISIS_LGST, "UPD <T> : Deleting Self LSP Info for [%s]\n",
             ISIS_GET_LEVEL_STR (u1Level)));
    if (u1Level == ISIS_LEVEL1)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        pContext->SelfLSP.pL1NSNLSP = NULL;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
        pContext->SelfLSP.pL1SNLSP = NULL;
    }
    else
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        pContext->SelfLSP.pL2NSNLSP = NULL;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
        pContext->SelfLSP.pL2SNLSP = NULL;
    }

    if (pNSNLSP != NULL)
    {
        /* Delete all the TLVs maintained in the ZeroLSP
         */

        UPP_PT ((ISIS_LGST, "UPD <T> : Deleting TLVs From Zero NSNLSP\n"));
        pLSPInfo = pNSNLSP->pZeroLSP;
        IsisUpdDelTlvs (pLSPInfo);

        ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pLSPInfo);

        /* Scan through each of the NonZero LSPs and remove the TLV chains
         */

        UPP_PT ((ISIS_LGST, "UPD <T> : Deleting TLVs From NonZero NSNLSP\n"));

        pLSPInfo = pNSNLSP->pNonZeroLSP;
        while (pLSPInfo != NULL)
        {
            pNextLSPInfo = pLSPInfo->pNext;
            IsisUpdDelTlvs (pLSPInfo);
            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pLSPInfo);
            pLSPInfo = pNextLSPInfo;
        }
        ISIS_MEM_FREE (ISIS_BUF_NSLI, (UINT1 *) pNSNLSP);
    }

    /* Delete all the TLVs from each of the Pseudonode LSPs
     */

    while (pSNLSP != NULL)
    {
        pNextSNLSP = pSNLSP->pNext;

        UPP_PT ((ISIS_LGST, "UPD <T> : Deleting TLVs From Zero SNLSP\n"));
        pLSPInfo = pSNLSP->pZeroLSP;
        IsisUpdDelTlvs (pLSPInfo);
        ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pLSPInfo);

        /* Scan through each of the NonZero LSPs and delet the TLV chains
         */

        UPP_PT ((ISIS_LGST, "UPD <T> : Deleting TLVs From NonZero SNLSP\n"));

        pLSPInfo = pSNLSP->pNonZeroLSP;
        while (pLSPInfo != NULL)
        {
            pNextLSPInfo = pLSPInfo->pNext;
            IsisUpdDelTlvs (pLSPInfo);
            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pLSPInfo);
            pLSPInfo = pNextLSPInfo;
        }
        ISIS_MEM_FREE (ISIS_BUF_SNLI, (UINT1 *) pSNLSP);
        pSNLSP = pNextSNLSP;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelSelfLSPInfo ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdAllocLSPHdr ()
 * Description : This routine allocates memory for the LSP Header which will be
 *               used to hold information regarding all the LSPs of the local
 *               system. It also allocates memory for Zero and Non-Zero LSPs
 *               and initialises these structures with initial values
 *               appropriately
 * Input (s)   : pContext  - Pointer to System Context
 *               u1BufType - Specifies the type of LSP header being requested. 
 *                           It can take the following values:
 *                           ISIS_BUF_SNLI : Pseudo Node LSP Header
 *                           ISIS_BUF_NSLI : Non Pseudo Node LSP Header
 * Output (s)  : pLspHdr   - Pointer to the Allocated LSP Header 
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On Successful Memory allocation 
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdAllocLSPHdr (tIsisSysContext * pContext, tIsisNSNLSP ** pLspHdr,
                    UINT1 u1BufType)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSNLSP         *pSNLSP = NULL;

    /* tIsisNSNLSP and tIsisSNLSP structures are almost similar except that the
     * later includes pointer to the circuit record and a pNext field, since 
     * Pseudonode LSPs are maintained per circuit. This routine assumes the type
     * of pLSPHdr to be tIsisNSNLSP for both Pseudonodes and Non-Pseudonodes and
     * accesses the fields appropriately based 'u1BufType' argument
     */

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAllocLSPHdr () \n"));

    if (*pLspHdr == NULL)
    {
        if (u1BufType == ISIS_BUF_SNLI)
        {
            /* Request for a Pseudonode LSP Header 
             */

            pSNLSP = (tIsisSNLSP *) ISIS_MEM_ALLOC (ISIS_BUF_SNLI,
                                                    sizeof (tIsisSNLSP));
            if (pSNLSP == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : PseudoNode LSP Header\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_SNLI);
                return ISIS_FAILURE;
            }

            /* Initializing LSP Number that will be used by the local system for
             * generating Self-LSPs
             */

            pSNLSP->u1CurrLSPNum = 1;
            pSNLSP->pCktEntry = NULL;
            pSNLSP->pNext = NULL;
            *pLspHdr = (tIsisNSNLSP *) pSNLSP;
        }
        else if (u1BufType == ISIS_BUF_NSLI)
        {
            *pLspHdr = (tIsisNSNLSP *) ISIS_MEM_ALLOC (ISIS_BUF_NSLI,
                                                       sizeof (tIsisNSNLSP));
            if (*pLspHdr == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Non-Pseudonode LSP Header\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_NSLI);
                return ISIS_FAILURE;
            }

            /* Initializing LSP Number that will be used by the local system for
             * generating Self-LSPs
             */

            (*pLspHdr)->u1CurrLSPNum = 1;
        }
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAllocLSPHdr ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdDelLspTimerNode ()
 * Description : This Routine de-links the LSP Record from the Timer Lists.
 *               Timer Lists are maintained as doubly linked lists linking the
 *               LSPs.
 * Input (s)   : pLSPRec   - Pointer to LSP Record
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdDelLspTimerNode (tIsisLSPEntry * pLSPRec)
{
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelLspTimerNode () \n"));

    if ((pLSPRec->pPrevTimerLink != NULL) && (pLSPRec->pNxtTimerLink != NULL))
    {
        pLSPRec->pPrevTimerLink->pNxtTimerLink = pLSPRec->pNxtTimerLink;
        pLSPRec->pNxtTimerLink->pPrevTimerLink = pLSPRec->pPrevTimerLink;
        pLSPRec->pNxtTimerLink = NULL;
        pLSPRec->pPrevTimerLink = NULL;
    }
    else if (pLSPRec->pPrevTimerLink != NULL)
    {
        pLSPRec->pPrevTimerLink->pNxtTimerLink = NULL;
        pLSPRec->pPrevTimerLink = NULL;
    }
    else
    {
        /* Prev record pointer should never be NULL since LSP timer lists always
         * starts with a Head block and the actual LSPs are always linked to the
         * head. If Prev record is NULL then we are trying to delete the head
         * which is not possible in any circumstances
         */

    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelLspTimerNode () \n"));
}

/******************************************************************************
 * Function    : IsisUpdRemoveAuthFromLSP ()
 * Description : This Routine removes the authentication TLV from the PDU and
 *               updates the length of the PDU to reflect the changed length
 * Input (s)   : pu1LSP    - Pointer to LSP 
 * Output (s)  : pu2Offset - Offset which must be updated after deleting the 
 *                           authentication information. After deletion
 *                           pu2Offset will have the exact PDU length  
 * Globals     : Not Referred or Modified
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdRemoveAuthFromLSP (UINT1 *pu1LSP, UINT2 *pu2Offset)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1AuthTlvLen = 0;
    UINT2               u2Offset = *pu2Offset;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdRemoveAuthFromLSP () \n"));

    *pu2Offset = 0;

    /* Get the Offset where the authentication information is present in the PDU
     */
    *pu2Offset = ((tIsisComHdr *) pu1LSP)->u1HdrLen;
    i4RetVal = IsisUtlGetNextTlvOffset (pu1LSP, ISIS_AUTH_INFO_TLV,
                                        pu2Offset, &u1AuthTlvLen);
    if (i4RetVal == ISIS_SUCCESS)
    {
        /* IsisUtlGetNextTlvOffset () would return the pointer to the Value
         * field in the Auth Info TLV. Since Auth info TLV is the last TLV in
         * any SelfLSP buffer, it is enough if we reduce the PDU length by the
         * size of the authentication information TLV. Since pu2Offset is
         * already at the Value field, we reduce pu2Offset by '2' to remove Code
         * and Length fields also. After this pu2Offset contains the total
         * length of the PDU. We update the same into the PDU
         */

        *pu2Offset = (UINT2) (*pu2Offset - 2);
    }
    else
    {
        *pu2Offset = u2Offset;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdRemoveAuthFromLSP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdAddNbrInfoFromMTEx ()
 * Description : This function updates Neighbour information, included in the
 *               Extented or MT IS TLV and Extented or MT IPRA TLV in the 
 *                 LSP buffer, into the SPT Node if the Adjacency supports 
 *                 the given Metric
 * Input(s)    : pContext     - Pointer to System Context
 *               pu1LSPBuf    - Pointer pointing to IS ADjacency TLV in the LSP
 *                              Buffer
 *               u1LSPFlag    - Flag indicating the Attached status of the
 *                              system for the given Metric
 *               ppNode       - Double pointer to the SPT Node
 *               u1Code       - TLV code
 *               u1Level      - Level1/Level2
 *               u1MtIndex    - MT Index (0 for MT-0 or 1 for MT-2)
 *               bIsPseudoLSP - To identify whether the Processing LSP is
 *                              Pseudonode LSP or not. 
 *                              ISIS_TRUE  - Pseudonode LSP
 *                              ISIS_FALSE - Non-Pseudonode LSP
 * Output(s)   : pu1SubTlvLen - Sub TLV length
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS/ISIS_FAILURE
 *****************************************************************************/

PUBLIC INT4
IsisUpdAddNbrInfoFromMTEx (tIsisSysContext * pContext, UINT1 *pu1LSPBuf,
                           UINT1 u1LSPFlag, tIsisSPTNode ** ppNode,
                           UINT1 u1Code, UINT1 u1Level, UINT1 *pu1SubTlvLen,
                           UINT1 u1MtIndex, BOOL1 bIsPseudoLSP)
{
    tIsisLSPEntry      *pLSPEntry = NULL;
    tIsisLSPEntry      *pPrevDbRec = NULL;
    tIsisLSPTxEntry    *pLSPTxEntry = NULL;
    tIsisLSPTxEntry    *pPrevTxRec = NULL;
    tIsisHashBucket    *pDbHashBkt = NULL;
    tIsisHashBucket    *pTxHashBkt = NULL;
    UINT1               au1IPMask[ISIS_MAX_IP_ADDR_LEN] = { 0 };
    UINT1               au1LSPId[ISIS_LSPID_LEN] = { 0 };
    UINT4               u4Metric = 0;
    UINT2               u2Metric = 0;
    UINT2               u2Offset = 0;
    UINT2               u2LspOffset = 0;
    UINT2               u2MtId = 0;
    UINT1               u1CtrlOctet = 0;
    UINT1               u1Prefixlen = 0;
    UINT1               u1SubTlv = 0;
    UINT1               u1MetricType = ISIS_DEF_MET;
    UINT1               u1UnSupTlv = ISIS_FALSE;
    UINT1               u1TmpFlag = 0;
    UINT1               u1TmpLspFlag = 0;
    UINT1               u1TlvLen = 0;
    UINT1               u1DbFlag = ISIS_LSP_NOT_EQUAL;
    UINT1               u1TxFlag = ISIS_LSP_NOT_EQUAL;
    UINT1               u1Loc = ISIS_LOC_NONE;
    UINT1              *pu1Ptr = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddNbrInfoFromMTEx ()\n"));

    /*First extract the Neighbor ID followed by Default Metric */

    *ppNode = (tIsisSPTNode *) ISIS_MEM_ALLOC (ISIS_BUF_SPTN,
                                               sizeof (tIsisSPTNode));
    if (*ppNode == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SPT Node\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_SPTN);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddNbrInfo ()\n"));
        return ISIS_FAILURE;
    }

    switch (u1Code)
    {
        case ISIS_MT_IS_REACH_TLV:
        case ISIS_EXT_IS_REACH_TLV:
            MEMCPY ((*ppNode)->au1DestID, pu1LSPBuf + u2Offset,
                    (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN));

            /*adding system-id+pseudonode id */
            u2Offset = (UINT2) (u2Offset + ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

            pu1Ptr = (UINT1 *) &u4Metric;
            MEMCPY ((pu1Ptr + 1), pu1LSPBuf + u2Offset, ISIS_IS_DEF_METRIC_LEN);

            u4Metric = OSIX_HTONL (u4Metric);

            if (u4Metric > ISIS_LL_CKTL_MAX_METRIC_MT)
            {
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Unable to process Metric out of range [%u]\n",
                         u4Metric));
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) *ppNode);
                *ppNode = NULL;
                return ISIS_FAILURE;
            }
            u2Offset = (UINT2) (u2Offset + ISIS_IS_DEF_METRIC_LEN);

            u1SubTlv = (UINT1) *(pu1LSPBuf + u2Offset);

            /* IS Type in LSP holds the following values
             * 0 - Unused value
             * 1 - (i.e. bit 1 set) Level 1 Intermediate system
             * 2 - Unused value
             * 3 - (i.e. bits 1 and 2 set) Level 2 Intermediate system.
             *
             * It will never carry the value for L12 system as defined in standard*/

            if ((u1LSPFlag & ISIS_ISTYPE_MASK) == IS_TYPE_LEVEL1)
            {
                u2Metric = (UINT2) (u2Metric | ISIS_SPT_L1_SYS_MASK);
            }
            else if ((u1LSPFlag & ISIS_ISTYPE_MASK) == IS_TYPE_LEVEL2)
            {
                u2Metric = (UINT2) (u2Metric | ISIS_SPT_L2_SYS_MASK);
            }
            /* The given adjacency is an IS adjacency, mark it in the Metric bits
             */

            u2Metric = (UINT2) (u2Metric | ISIS_SPT_IS_FLAG);

            if (bIsPseudoLSP == ISIS_TRUE)
            {
                MEMCPY (au1LSPId, (*ppNode)->au1DestID, ISIS_LSPID_LEN);
                ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
                pLSPEntry =
                    IsisUpdGetLSPFromDB (pContext, au1LSPId, u1Level,
                                         &pPrevDbRec, &pDbHashBkt, &u1Loc,
                                         &u1DbFlag);
                pLSPTxEntry =
                    IsisUpdGetLSPFromTxQ (pContext, au1LSPId, u1Level,
                                          &pPrevTxRec, &pTxHashBkt, &u1Loc,
                                          &u1TxFlag);

                if (u1DbFlag != ISIS_LSP_EQUAL)
                {
                    /* LSP Zero for the given system is not found in the Database. Try it
                     * out in Transmission Queue
                     */

                    if ((u1TxFlag == ISIS_LSP_EQUAL) && (pLSPTxEntry != NULL))    /*klocwork */
                    {
                        if ((pContext->u1IsisMTSupport == ISIS_TRUE)
                            && (u1MtIndex != 0))
                        {
                            /* For MT-TLVs Attach bit will be set in the first 2 bits of MT TLV. */
                            if (IsisUtlGetNextTlvOffset
                                (pLSPTxEntry->pLSPRec->pu1LSP, ISIS_MT_TLV,
                                 &u2LspOffset, &u1TlvLen) != ISIS_FAILURE)
                            {
                                while (u1TlvLen)
                                {
                                    MEMCPY (&u2MtId,
                                            (pLSPTxEntry->pLSPRec->pu1LSP +
                                             u2LspOffset), ISIS_MT_ID_LEN);
                                    u2MtId = OSIX_HTONS (u2MtId);
                                    u2MtId = (u2MtId & ISIS_MTID_MASK);

                                    if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                                    {
                                        ISIS_GET_1_BYTE (pLSPTxEntry->pLSPRec->
                                                         pu1LSP, u2LspOffset,
                                                         u1TmpLspFlag);
                                        u1TmpLspFlag =
                                            (u1TmpLspFlag &
                                             ISIS_MT_LSP_FLAG_MASK);
                                        break;
                                    }
                                    u1TlvLen -= ISIS_MT_ID_LEN;
                                    u2LspOffset += ISIS_MT_ID_LEN;
                                }
                            }
                            /* System type flag L1/L2 will be set in the LSp header */
                            ISIS_GET_1_BYTE (pLSPTxEntry->pLSPRec->pu1LSP,
                                             (UINT2) (ISIS_OFFSET_CHKSUM + 2),
                                             u1TmpFlag);
                            u1TmpLspFlag |=
                                (UINT1) (u1TmpFlag & ISIS_ISTYPE_MASK);

                        }
                        else
                        {
                            ISIS_GET_1_BYTE (pLSPTxEntry->pLSPRec->pu1LSP,
                                             (UINT2) (ISIS_OFFSET_CHKSUM + 2),
                                             u1TmpLspFlag);
                        }
                    }
                    else
                    {
                        /* LSP Zero is not present in Transmission Queue also, hence
                         * ignore all the LSPs of this particular system
                         */

                        ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) ISIS_LSPID_LEN,
                                           "UPD <T> : LSP Zero Not Found For\t",
                                           ISIS_OCTET_STRING);

                        UPP_PT ((ISIS_LGST,
                                 "UPD <T> : Unable To Find Zeroth fragment of LSP [%s][%s] MT [%d] while processing pseudonode LSP\n",
                                 acLSPId, ISIS_GET_LEVEL_STR (u1Level),
                                 u1MtIndex));
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddNbrInfoFromMTEx ()\n"));
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) *ppNode);
                        return ISIS_FAILURE;
                    }
                }
                else
                {
                    if (pLSPEntry != NULL)    /*klocwork */
                    {
                        if ((pContext->u1IsisMTSupport == TRUE)
                            && (u1MtIndex != 0))
                        {
                            /* For MT-TLVs Attach bit will be set in the first 2 bits of MT TLV. */
                            if (IsisUtlGetNextTlvOffset
                                (pLSPEntry->pu1LSP, ISIS_MT_TLV, &u2LspOffset,
                                 &u1TlvLen) != ISIS_FAILURE)
                            {
                                while (u1TlvLen)
                                {
                                    MEMCPY (&u2MtId,
                                            (pLSPEntry->pu1LSP + u2LspOffset),
                                            ISIS_MT_ID_LEN);
                                    u2MtId = OSIX_HTONS (u2MtId);
                                    u2MtId = (u2MtId & ISIS_MTID_MASK);

                                    if (u2MtId == ISIS_IPV6_UNICAST_MT_ID)
                                    {
                                        ISIS_GET_1_BYTE (pLSPEntry->pu1LSP,
                                                         u2LspOffset,
                                                         u1TmpLspFlag);
                                        u1TmpLspFlag =
                                            (u1TmpLspFlag &
                                             ISIS_MT_LSP_FLAG_MASK);

                                        break;
                                    }
                                    u2LspOffset += ISIS_MT_ID_LEN;
                                    u1TlvLen -= ISIS_MT_ID_LEN;

                                }

                            }
                            /* System type flag L1/L2 will be set in the LSp header */
                            ISIS_GET_1_BYTE (pLSPEntry->pu1LSP,
                                             (UINT2) (ISIS_OFFSET_CHKSUM + 2),
                                             u1TmpFlag);
                            u1TmpLspFlag |=
                                (UINT1) (u1TmpFlag & ISIS_ISTYPE_MASK);

                        }
                        else
                        {
                            ISIS_GET_1_BYTE (pLSPEntry->pu1LSP,
                                             (UINT2) (ISIS_OFFSET_CHKSUM + 2),
                                             u1TmpLspFlag);
                        }
                    }
                    else
                    {
                        UPP_PT ((ISIS_LGST,
                                 "UPD <T> : Unable To Find Zeroth fragment of LSP [%s][%s] MT [%d] while processing pseudonode LSP\n",
                                 acLSPId, ISIS_GET_LEVEL_STR (u1Level),
                                 u1MtIndex));
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddNbrInfoFromMTEx ()\n"));
                        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) *ppNode);
                        return ISIS_FAILURE;
                    }
                }
            }                    /* End of bIsPseudoLSP */

            if (u1MtIndex != 0)
            {
                /* The given system is announcing that it is attached via
                 * Delay Metric. Hence we can set the Attached bit for the
                 * metric in the SPT node information bits
                 */

                if (u1TmpLspFlag & ISIS_MT_LSP_ATT_MASK)
                {
                    u2Metric = (UINT2) (u2Metric | ISIS_SPT_ATT_FLAG);
                }
            }
            else if (u1TmpLspFlag & ISIS_FLG_ATT_DEF)
            {
                u2Metric = (UINT2) (u2Metric | ISIS_SPT_ATT_FLAG);
            }

            break;

        case ISIS_MT_IPV6_REACH_TLV:
        case ISIS_EXT_IP_REACH_TLV:
            MEMCPY (&u4Metric, pu1LSPBuf + u2Offset, ISIS_EXT_DEF_METRIC_LEN);
            u4Metric = OSIX_HTONL (u4Metric);
            if (u4Metric > ISIS_MAX_METRIC_MT)
            {
                ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) *ppNode);
                *ppNode = NULL;
                return ISIS_SUCCESS;
            }

            u2Offset = (UINT2) (u2Offset + ISIS_EXT_DEF_METRIC_LEN);
            u1CtrlOctet = (UINT1) *(pu1LSPBuf + u2Offset);
            if (u1CtrlOctet & ISIS_FLG_DOWN_SET)
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    SPT_SET_UPDOWN_FLAG ((*ppNode), ISIS_RL_L1_DOWN);
                }
                else
                {
                    SPT_SET_UPDOWN_FLAG ((*ppNode), ISIS_RL_L2_DOWN);
                }
            }
            else
            {
                if (u1Level == ISIS_LEVEL1)
                {
                    SPT_SET_UPDOWN_FLAG ((*ppNode), ISIS_RL_L1_UP);
                }
                else
                {
                    SPT_SET_UPDOWN_FLAG ((*ppNode), ISIS_RL_L2_UP);
                }
            }
            if (u1Code == ISIS_MT_IPV6_REACH_TLV)
            {
                if (u1CtrlOctet & ISIS_METRIC_TYPE_MASK)
                {
                    /* External Metric Bit is set. The Metric is External
                     */
                    u2Metric = (UINT2) (u2Metric | ISIS_SPT_EXT_MET_FLAG);
                }
                u1SubTlv = (u1CtrlOctet & ISIS_MT_IPV6_SUBTLV_MASK);
                u2Offset = (UINT2) (u2Offset + 1);
                u1Prefixlen = (UINT1) *(pu1LSPBuf + u2Offset);
                u2Metric = (UINT2) (u2Metric | ISIS_IPV6_ADDR_TYPE);
                (*ppNode)->u1AddrType = ISIS_ADDR_IPV6;

            }
            else
            {
                u1SubTlv = (u1CtrlOctet & ISIS_EXTN_SUBTLV_MASK);
                u1Prefixlen = (u1CtrlOctet & ISIS_PREFIX_MASK);
                u2Metric = (UINT2) (u2Metric | ISIS_IPV4_ADDR_TYPE);
                (*ppNode)->u1AddrType = ISIS_ADDR_IPV4;
            }
            u2Offset = (UINT2) (u2Offset + 1);
            IsisUtlGetIPMask (u1Prefixlen, au1IPMask);
            (*ppNode)->u1PrefixLen = u1Prefixlen;
            u1Prefixlen = (UINT1) ISIS_ROUNDOFF_PREFIX_LEN (u1Prefixlen);
            MEMCPY (((*ppNode)->au1DestID), (pu1LSPBuf + u2Offset),
                    u1Prefixlen);

            if (u1Code == ISIS_MT_IPV6_REACH_TLV)
            {
                MEMCPY (((*ppNode)->au1DestID) + ISIS_MAX_IPV6_ADDR_LEN,
                        au1IPMask, ISIS_MAX_IPV6_ADDR_LEN);
            }
            else
            {
                MEMCPY (((*ppNode)->au1DestID) + ISIS_MAX_IPV4_ADDR_LEN,
                        au1IPMask, ISIS_MAX_IPV4_ADDR_LEN);
            }

            u2Offset = (UINT2) (u2Offset + u1Prefixlen);
            break;
        default:
            u1UnSupTlv = ISIS_TRUE;
            ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) *ppNode);
            break;
    }
    if (u1UnSupTlv == ISIS_FALSE)
    {
        (*ppNode)->au2Metric[u1MetricType] = u2Metric;
        (*ppNode)->u4MetricVal = u4Metric;
        (*ppNode)->au1MaxPaths[u1MetricType] = 1;
        (*ppNode)->pNext = NULL;

        if (u1SubTlv != 0)
        {
            *pu1SubTlvLen = (UINT1) *(pu1LSPBuf + u2Offset);
            if ((u1Code == ISIS_MT_IPV6_REACH_TLV)
                || (u1Code == ISIS_EXT_IP_REACH_TLV))
            {
                *pu1SubTlvLen += 1;
            }
        }
        else
        {
            *pu1SubTlvLen = 0;
        }
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddNbrInfoFromMTEx ()\n"));
    return ISIS_SUCCESS;
}

INT4
IsisUtilRBTreeIpraEntryFree (tRBElem * pIpraEntryNode, UINT4 u4Arg)
{
    tIsisIPRATLV       *pIpraTLV = NULL;
    pIpraTLV = (tIsisIPRATLV *) pIpraEntryNode;

    UNUSED_PARAM (u4Arg);
    MEMSET (pIpraTLV, 0, sizeof (tIsisIPRATLV));

    if (pIpraEntryNode != NULL)
    {
        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pIpraEntryNode);
    }
    return ISIS_SUCCESS;
}

VOID
IsisUpdEnqueueP2PCktChgEvt (tIsisSysContext * pContext, UINT1 u1State,
                            UINT4 u4CktId)
{
    tIsisEvtP2PCktState *pP2PCktInfo = NULL;

    if ((u1State != ISIS_CKT_UP) && (u1State != ISIS_CKT_DOWN))
    {
        /*invalid state */
        return;
    }

    pP2PCktInfo
        = (tIsisEvtP2PCktState *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                  sizeof (tIsisEvtP2PCktState));
    /* Filling the associated variables in the structure
     * tIsisEvtP2PCktState
     */

    if (pP2PCktInfo != NULL)
    {
        pP2PCktInfo->u1EvtID = ISIS_EVT_P2P_CKT_CHG;
        pP2PCktInfo->u1State = u1State;
        pP2PCktInfo->u4CktIndex = u4CktId;

        IsisUtlSendEvent (pContext, (UINT1 *) pP2PCktInfo,
                          sizeof (tIsisEvtP2PCktState));
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : P2P Circuit Event\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
    }
    ADP_EE ((ISIS_LGST, "ADJ <X> : Exiting IsisUpdEnqueueP2PCktChgEvt ()\n"));
}

/******************************************************************************
 * Function     : IsisUtlGetHostnmefromLSP
 * Description  : This function is to get the hostname received in the LSP packet 
 * Inputs       : pContext - Context identifier
 *                pu1LSP   - LSP buffer
 *                
 * Outputs      : None
 * Returns      : None
 *****************************************************************************/
PUBLIC VOID
IsisUtlGetHostnmefromLSP (tIsisSysContext * pContext, UINT1 *pu1LSP,
                          UINT1 u1Cmd)
{
    UINT1               u1TlvLen = 0;
    UINT2               u2Offset = 0;
    UINT1               au1TempHostNme[ISIS_HSTNME_MAX_LEN + 1] = { 0 };
    UINT1               au1SysID[ISIS_SYS_ID_LEN] = { 0 };
    UINT1               u1HostNmeTlvFound = ISIS_FALSE;
    UINT1               u1LSPID = 0;

    if (pContext->u1IsisDynHostNmeSupport == ISIS_DYNHOSTNME_DISABLE)
    {
        return;
    }
    MEMCPY (au1SysID, (pu1LSP + ((UINT2) ISIS_OFFSET_LSPID)), ISIS_SYS_ID_LEN);
    u1LSPID = (UINT1) (*(pu1LSP + ISIS_OFFSET_LSPNUM));
    /*check only in non pseudonode LSP */
    if (((UINT1) *(pu1LSP + ISIS_OFFSET_PNODEID)) == 0)
    {
        /*checkin ZEROLSP */
        while (IsisUtlGetNextTlvOffset (pu1LSP, ISIS_DYN_HOSTNME_TLV, &u2Offset,
                                        &u1TlvLen) != ISIS_FAILURE)
        {
            u1HostNmeTlvFound = ISIS_TRUE;
            MEMCPY (au1TempHostNme, (pu1LSP + u2Offset), u1TlvLen);
            au1TempHostNme[u1TlvLen] = '\0';
        }
    }
    else
    {
        return;
    }
    if (u1Cmd == ISIS_CMD_MODIFY)
    {
        if (u1HostNmeTlvFound == ISIS_FALSE)
        {
            IsisUpdHostNmeTable (pContext, au1SysID, au1TempHostNme,
                                 ISIS_CMD_DELETE, u1LSPID);
        }
        else
        {
            IsisUpdHostNmeTable (pContext, au1SysID, au1TempHostNme,
                                 ISIS_CMD_MODIFY, u1LSPID);
        }
    }
    else if (u1Cmd == ISIS_CMD_DELETE)
    {
        IsisUpdHostNmeTable (pContext, au1SysID, au1TempHostNme,
                             ISIS_CMD_DELETE, u1LSPID);
    }
    else
    {
        if (u1HostNmeTlvFound == ISIS_TRUE)
        {
            IsisUpdHostNmeTable (pContext, au1SysID, au1TempHostNme,
                                 ISIS_CMD_ADD, u1LSPID);
        }
    }
    return;
}

/******************************************************************************
 * Function     : IsisUpdHostNmeTable
 * Description  : This function is to update the hostname received in the LSP packet 
 * Inputs       : pContext - Context identifier
 *                pu1SysID - System ID
 *                pu1HostNme - Host name
 *                u1cmd - action to perform
 * Outputs      : None
 * Returns      : None
 *****************************************************************************/
PUBLIC VOID
IsisUpdHostNmeTable (tIsisSysContext * pContext, UINT1 *pu1SysID,
                     UINT1 *pu1HostNme, UINT1 u1cmd, UINT1 u1LSPID)
{
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo = NULL;
    tIsisHostNmeNodeInfo IsisHostNmeNodeInfo;
    tIsisHostNmeNodeInfo *p1IsisHostNmeNodeInfo = NULL;
    MEMSET (&IsisHostNmeNodeInfo, 0, sizeof (tIsisHostNmeNodeInfo));

    MEMCPY (IsisHostNmeNodeInfo.au1SysID, pu1SysID,
            MEM_MAX_BYTES (STRLEN (pu1SysID), ISIS_SYS_ID_LEN));

    STRNCPY (IsisHostNmeNodeInfo.au1HostNme, pu1HostNme,
             MEM_MAX_BYTES (STRLEN (pu1HostNme), (ISIS_HSTNME_MAX_LEN + 1)));

    pIsisHostNmeNodeInfo =
        RBTreeGet (pContext->HostNmeList, (tRBElem *) & IsisHostNmeNodeInfo);
    if (pIsisHostNmeNodeInfo != NULL)
    {
        if (pIsisHostNmeNodeInfo->u1FragType == u1LSPID)
        {

            if (u1cmd == ISIS_CMD_DELETE)
            {
                RBTreeRemove (pContext->HostNmeList,
                              (tRBElem *) pIsisHostNmeNodeInfo);
                ISIS_MEM_FREE (ISIS_BUF_HSTNME, (UINT1 *) pIsisHostNmeNodeInfo);
                pContext->u4HostNmeTotEntries--;
            }
            else
            {
                if (MEMCMP (IsisHostNmeNodeInfo.au1HostNme,
                            pIsisHostNmeNodeInfo->au1HostNme,
                            ISIS_HSTNME_MAX_LEN) != 0)
                {
                    RBTreeRemove (pContext->HostNmeList,
                                  (tRBElem *) pIsisHostNmeNodeInfo);
                    ISIS_MEM_FREE (ISIS_BUF_HSTNME,
                                   (UINT1 *) pIsisHostNmeNodeInfo);
                    pContext->u4HostNmeTotEntries--;
                    IsisHostNmeNodeInfo.u1FragType = u1LSPID;
                    p1IsisHostNmeNodeInfo = &IsisHostNmeNodeInfo;
                    IsisUpdAddToHostNmeTable (pContext, p1IsisHostNmeNodeInfo);
                }
            }
        }
    }
    else
    {
        if ((u1cmd == ISIS_CMD_ADD) || (u1cmd == ISIS_CMD_MODIFY))
        {
            IsisHostNmeNodeInfo.u1FragType = u1LSPID;
            p1IsisHostNmeNodeInfo = &IsisHostNmeNodeInfo;
            IsisUpdAddToHostNmeTable (pContext, p1IsisHostNmeNodeInfo);
        }
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IsisUpdAddToHostNmeTable
 *
 *    DESCRIPTION      : Addition of record to HostName RB Tree
 *
 *    INPUT            : IsisHostNmeNodeInfo - RB node 
 *
 *    OUTPUT           : None
 *
 *
 ****************************************************************************/

PUBLIC INT4
IsisUpdAddToHostNmeTable (tIsisSysContext * pContext,
                          tIsisHostNmeNodeInfo * p1IsisHostNmeNodeInfo)
{
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo = NULL;

    if (pContext->u4HostNmeTotEntries == MAX_HOST_NME_ENTRIES)
    {
        return ISIS_FAILURE;
    }

    pIsisHostNmeNodeInfo =
        (tIsisHostNmeNodeInfo *) ISIS_MEM_ALLOC (ISIS_BUF_HSTNME,
                                                 sizeof (tIsisHostNmeNodeInfo));
    if (pIsisHostNmeNodeInfo == NULL)
    {

        return ISIS_FAILURE;
    }

    MEMSET (pIsisHostNmeNodeInfo, 0, sizeof (tIsisHostNmeNodeInfo));

    MEMCPY (pIsisHostNmeNodeInfo->au1HostNme, p1IsisHostNmeNodeInfo->au1HostNme,
            STRLEN (p1IsisHostNmeNodeInfo->au1HostNme));
    MEMCPY (pIsisHostNmeNodeInfo->au1SysID, p1IsisHostNmeNodeInfo->au1SysID,
            ISIS_SYS_ID_LEN);
    if (RBTreeAdd (pContext->HostNmeList, (tRBElem *) pIsisHostNmeNodeInfo) ==
        RB_FAILURE)
    {

        return ISIS_FAILURE;
    }
    pContext->u4HostNmeTotEntries++;
    return ISIS_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IsisHostNmeListCmp
 *
 *    DESCRIPTION      : HostName RB Tree Comparison function.
 *
 *    INPUT            : pRBElem1, pRBElem2 - RB node elements
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : -1,0,1
 *
 ****************************************************************************/
PUBLIC INT4
IsisHostNmeListCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo1 =
        (tIsisHostNmeNodeInfo *) pRBElem1;
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo2 =
        (tIsisHostNmeNodeInfo *) pRBElem2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pIsisHostNmeNodeInfo1->au1SysID,
                       pIsisHostNmeNodeInfo2->au1SysID, ISIS_SYS_ID_LEN);
    return i4RetVal;
}

/******************************************************************************
 * Function     : IsisGetHostNme
 * Description  : This function is to update the hostname received in the LSP packet
 * Inputs       : pContext - Context identifier
 *                pu1SysID - System ID
 *                pu1HostNme - Host name
 *                u1cmd - action to perform
 * Outputs      : None
 * Returns      : None
 *****************************************************************************/
PUBLIC VOID
IsisGetHostNme (tIsisSysContext * pContext, UINT1 *pu1SysID, UINT1 *pu1HostNme)
{

    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo = NULL;
    tIsisHostNmeNodeInfo IsisHostNmeNodeInfo;
    MEMSET (pu1HostNme, 0, ISIS_HSTNME_MAX_LEN);
    MEMSET (&IsisHostNmeNodeInfo, 0, sizeof (tIsisHostNmeNodeInfo));
    MEMCPY (IsisHostNmeNodeInfo.au1SysID, pu1SysID, ISIS_SYS_ID_LEN);

    pIsisHostNmeNodeInfo =
        RBTreeGet (pContext->HostNmeList, (tRBElem *) & IsisHostNmeNodeInfo);
    if (pIsisHostNmeNodeInfo != NULL)
    {
        MEMCPY (pu1HostNme, pIsisHostNmeNodeInfo->au1HostNme,
                STRLEN (pIsisHostNmeNodeInfo->au1HostNme));
    }
    return;
}

/******************************************************************************
 * Function     : IsisHostNmeMDT
 * Description  : This function is to update the hostname received in the LSP packet
 * Inputs       : pContext - Context identifier
 *                pu1SysID - System ID
 *                pu1HostNme - Host name
 *                u1cmd - action to perform
 * Outputs      : None
 * Returns      : None
 *****************************************************************************/
PUBLIC VOID
IsisHostNmeMDT (tIsisSysContext * pContext, tIsisMDT * pMDT, UINT1 u1Cmd)
{
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisHostNmeMDT ()\n"));
    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));
    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : No Memory For MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        return;
    }

    if (ISIS_HSTNME_TLV_LEN != 0)
    {
        ISIS_GET_HSTNME (pMDT->au1IsisHstNme);
        pMDT->u1TLVType = ISIS_DYN_HOSTNME_TLV;
        pMDT->u1Cmd = u1Cmd;
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
            IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
        }
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
        }
    }
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisHostNmeMDT ()\n"));
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisDelAllHstNmeInCxt                                      */
/*                                                                           */
/* Description  : This routine deletes all the external LSAs fronm the       */
/*                database.                                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisDelAllHstNmeInCxt (tIsisSysContext * pContext)
{
    tIsisHostNmeNodeInfo *pIsisHostNmeNodeInfo = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisDelAllHstNmeInCxt ()\n"));
    while ((pIsisHostNmeNodeInfo = (tIsisHostNmeNodeInfo *)
            RBTreeGetFirst (pContext->HostNmeList)) != NULL)
    {
        RBTreeRemove (pContext->HostNmeList, (tRBElem *) pIsisHostNmeNodeInfo);
        ISIS_MEM_FREE (ISIS_BUF_HSTNME, (UINT1 *) pIsisHostNmeNodeInfo);
        pContext->u4HostNmeTotEntries--;
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisDelAllHstNmeInCxt ()\n"));
}

/****************************************************************************
 * Function    : IsisUpdReFillHstNmeTable ()
 * Description : The function is fetches hostname from LSP records in DB/TxQ.
 * Input       : None
 * Output      : None
 * Globals     : None
 * Returns     :
 ****************************************************************************/

PUBLIC VOID
IsisUpdReFillHstNmeTable (tIsisSysContext * pContext, UINT1 u1Level)
{
    tIsisSortedHashTable *pDbHashTbl = NULL;
    tIsisSortedHashTable *pTxQHashTbl = NULL;
    tIsisHashBucket    *pHashBkt;
    tIsisLSPEntry      *pCurrentDbEntry = NULL;
    tIsisLSPEntry      *pNextDbEntry = NULL;
    tIsisLSPTxEntry    *pCurrentTxEntry = NULL;
    tIsisLSPTxEntry    *pNextTxEntry = NULL;

    if (NULL != pContext)
    {
        if (u1Level == ISIS_LEVEL1)
        {
            pDbHashTbl = &(pContext->SysL1Info.LspDB);
            pTxQHashTbl = &(pContext->SysL1Info.LspTxQ);
        }
        else if (u1Level == ISIS_LEVEL2)
        {
            pDbHashTbl = &(pContext->SysL2Info.LspDB);
            pTxQHashTbl = &(pContext->SysL2Info.LspTxQ);
        }
        if ((pDbHashTbl != NULL) && (pDbHashTbl->pHashBkts != NULL))
        {
            /* LSP records found in the given Hash Bucket */
            pHashBkt = pDbHashTbl->pHashBkts;
            if (NULL != pHashBkt)
            {
                pCurrentDbEntry = (tIsisLSPEntry *) (pHashBkt->pFirstElem);

                while (pCurrentDbEntry != NULL)
                {
                    IsisUtlGetHostnmefromLSP (pContext, pCurrentDbEntry->pu1LSP,
                                              ISIS_CMD_ADD);
                    pNextDbEntry =
                        IsisUpdGetNextDbRec (pCurrentDbEntry, &pHashBkt);
                    pCurrentDbEntry = pNextDbEntry;
                }
            }
        }
        if ((pTxQHashTbl != NULL) && (pTxQHashTbl->pHashBkts != NULL))
        {
            /* LSP records found in the given Hash Bucket */
            pHashBkt = pTxQHashTbl->pHashBkts;
            if (NULL != pHashBkt)
            {
                pCurrentTxEntry = (tIsisLSPTxEntry *) (pHashBkt->pFirstElem);

                while (pCurrentTxEntry != NULL)
                {
                    IsisUtlGetHostnmefromLSP (pContext,
                                              pCurrentTxEntry->pLSPRec->pu1LSP,
                                              ISIS_CMD_ADD);
                    pNextTxEntry =
                        IsisUpdGetNextTxRec (pCurrentTxEntry, &pHashBkt);
                    pCurrentTxEntry = pNextTxEntry;
                }
            }
        }
    }
    return;
}
PUBLIC UINT1
IsUpdAddAuthTlvPrgLSP (tIsisSysContext * pContext, UINT1 *pu1LSP,
                       UINT1 u1Level, UINT4 u4RcvdSeqNum)
{
    UINT2               u2Len = 0;
    UINT2               u2AuthStatus = 0;
    UINT1               u1AuthType = 0;

    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1LSP, u2Len);
    u1AuthType = (UINT1) (((u1Level == ISIS_LEVEL1) ?
                           pContext->SysActuals.u1SysAreaAuthType :
                           pContext->SysActuals.u1SysDomainAuthType));

    u2AuthStatus = (UINT2) (((u1Level == ISIS_LEVEL1) ?
                             pContext->SelfLSP.pL1NSNLSP->pZeroLSP->
                             u2AuthStatus : pContext->SelfLSP.pL2NSNLSP->
                             pZeroLSP->u2AuthStatus));
    if (u1AuthType == ISIS_AUTH_MD5)
    {
        if (((u1Level != ISIS_LEVEL2)
             && (pContext->SysActuals.SysAreaTxPasswd.u1Len != 0))
            || ((u1Level != ISIS_LEVEL1)
                && (pContext->SysActuals.SysDomainTxPasswd.u1Len != 0)))
        {
            if (u2AuthStatus == ISIS_TRUE)
            {
                ISIS_SET_LSP_SEQNO (pu1LSP, u4RcvdSeqNum);
                MEMSET (pu1LSP + ISIS_LSP_HDR_LEN, 0, u2Len - ISIS_LSP_HDR_LEN);
                u2Len = ISIS_LSP_HDR_LEN;
                IsisUpdAddAuthToPDU (pContext, pu1LSP, &u2Len, u1Level);
            }
        }
    }
    return u1AuthType;
}
