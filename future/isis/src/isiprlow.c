/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: isiprlow.c,v 1.17 2016/03/26 09:54:43 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "iscli.h"
# include  "lr.h"
# include  "fssnmp.h"
#include "isincl.h"
#include "isextn.h"
/* LOW LEVEL Routines for Table : IsisIPRATable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisIPRATable
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisIPRATable (INT4 i4IsisSysInstance,
                                       INT4 i4IsisIPRAType,
                                       INT4 i4IsisIPRAIndex)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhValidateIndexInstanceIsisIPRATable ()\n"));

    if (((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
         || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE)) ||
        ((i4IsisIPRAType != ISIS_MANUAL_TYPE)
         && (i4IsisIPRAType != ISIS_AUTO_TYPE)) || (i4IsisIPRAIndex <= 0))
    {

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Inavalid Indices. The indices are SysInstance:"
                 "%d\n IpRaType: %d\n IpRaIndex: %d\n", i4IsisSysInstance,
                 i4IsisIPRAType, i4IsisIPRAIndex));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhValidateIndexInstanceIsisIPRATable()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisIPRATable
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisIPRATable (INT4 *pi4IsisSysInstance, INT4 *pi4IsisIPRAType,
                               INT4 *pi4IsisIPRAIndex)
{

    return (nmhGetNextIndexIsisIPRATable (0, pi4IsisSysInstance,
                                          0, pi4IsisIPRAType,
                                          0, pi4IsisIPRAIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisIPRATable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisIPRAType
                nextIsisIPRAType
                IsisIPRAIndex
                nextIsisIPRAIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisIPRATable (INT4 i4IsisSysInstance,
                              INT4 *pi4NextIsisSysInstance, INT4 i4IsisIPRAType,
                              INT4 *pi4NextIsisIPRAType, INT4 i4IsisIPRAIndex,
                              INT4 *pi4NextIsisIPRAIndex)
{
    tIsisSysContext    *pContext = NULL;
    INT4                i4TempRetVal = ISIS_FAILURE;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetNextIndexIsisIPRATable\n"));

    if (IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext) ==
        ISIS_FAILURE)
    {

        if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4IsisSysInstance,
                                            (UINT4 *) &i4IsisSysInstance) ==
            ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : No Further Valid index available:"));
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting nmhGetNextIndexIsisIPRATable ()\n"));

            return (SNMP_FAILURE);
        }

        i4IsisIPRAType = 0;
        i4IsisIPRAIndex = 0;

    }

    if (pContext == NULL)
    {
        i4TempRetVal =
            IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    }

    while (pContext != NULL)
    {

        if (nmhUtlGetNextIndexIsisIPRATable (i4IsisSysInstance,
                                             i4IsisIPRAType, &i4IsisIPRAType,
                                             i4IsisIPRAIndex, &i4IsisIPRAIndex)
            == ISIS_FAILURE)
        {

            if (nmhUtlGetNextIndexIsisSysTable ((UINT4) i4IsisSysInstance,
                                                (UINT4 *)
                                                &i4IsisSysInstance) ==
                ISIS_FAILURE)
            {

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : No Further Valid index available:"));
                NMP_EE ((ISIS_LGST,
                         "NMP <X> :Exiting"
                         "nmhGetNextIndexIsisIPRATable()\n"));

                return (SNMP_FAILURE);
            }
            else
            {
                i4IsisIPRAType = 0;
                i4IsisIPRAIndex = 0;
                continue;
            }

        }

        else
        {
            *pi4NextIsisSysInstance = i4IsisSysInstance;
            *pi4NextIsisIPRAType = i4IsisIPRAType;
            *pi4NextIsisIPRAIndex = i4IsisIPRAIndex;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : The Next Indices are SysInstance : %d\n"
                     "IPRAType : %d\n IPRAIndex : %d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));

            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> :Exiting nmhGetNextIndexIsisIPRATable()\n"));
    UNUSED_PARAM (i4TempRetVal);
    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IsisIPRATable
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IsisIPRATable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisIPRADestType
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                retValIsisIPRADestType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisIPRADestType (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                        INT4 i4IsisIPRAIndex, INT4 *pi4RetValIsisIPRADestType)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered function nmhGetIsisIPRADestType ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType : %d\n"
                     "IPRAIndex :%d\n", i4IsisSysInstance, i4IsisIPRAType,
                     i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            *pi4RetValIsisIPRADestType = (INT4) (pIPRARec->u1IPRADestType);
            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting function nmhGetIsisIPRADestType ()\n"));

    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhGetIsisIPRADest
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                retValIsisIPRADest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisIPRADest (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                    INT4 i4IsisIPRAIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValIsisIPRADest)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisIPRADest() \n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType : %d\n"
                     "IPRAIndex :%d\n", i4IsisSysInstance, i4IsisIPRAType,
                     i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }

        else
        {
            if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
            {
                MEMCPY (pRetValIsisIPRADest->pu1_OctetList,
                        pIPRARec->au1IPRADest, ISIS_MAX_IPV4_ADDR_LEN);
                pRetValIsisIPRADest->i4_Length = ISIS_MAX_IPV4_ADDR_LEN;
            }
            else
            {
                MEMCPY (pRetValIsisIPRADest->pu1_OctetList,
                        pIPRARec->au1IPRADest, ISIS_MAX_IPV6_ADDR_LEN);
                pRetValIsisIPRADest->i4_Length = ISIS_MAX_IPV6_ADDR_LEN;
            }
            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting function nmhGetIsisIPRADest()\n"));
    return (i1RetCode);
}

/****************************************************************************
 Function    :  nmhGetIsisIPRADestPrefixLen
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                retValIsisIPRADestPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisIPRADestPrefixLen (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                             INT4 i4IsisIPRAIndex,
                             UINT4 *pu4RetValIsisIPRADestPrefixLen)
{
    INT4                i4RetVal = 0;
    INT1                i1RetCode = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered function nmhGetIsisIPRADestPrefixLen ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType : %d\n"
                     "IPRAIndex :%d\n", i4IsisSysInstance, i4IsisIPRAType,
                     i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            *pu4RetValIsisIPRADestPrefixLen = (INT4) (pIPRARec->u1PrefixLen);
            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting function nmhGetIsisIPRADestPrefixLen ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisIPRAExistState
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                retValIsisIPRAExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisIPRAExistState (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                          INT4 i4IsisIPRAIndex,
                          INT4 *pi4RetValIsisIPRAExistState)
{
    INT4                i4RetVal = 0;
    INT1                i1RetCode = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisIPRAExiststate ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType : %d\n"
                     "IPRAIndex :%d\n", i4IsisSysInstance, i4IsisIPRAType,
                     i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else
        {

            *pi4RetValIsisIPRAExistState = (INT4) pIPRARec->u1IPRAExistState;
            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisIPRAExiststate ()\n"));
    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisIPRAAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                retValIsisIPRAAdminState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisIPRAAdminState (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                          INT4 i4IsisIPRAIndex,
                          INT4 *pi4RetValIsisIPRAAdminState)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisIPRAAdminState ()\n"));

    /* Get the matching IP RA entry */

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType : %d\n IPRAIndex :%d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            *pi4RetValIsisIPRAAdminState = (INT4) pIPRARec->u1IPRAAdminState;
            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisIPRAAdminState ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisIPRAMetric
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                retValIsisIPRAMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisIPRAMetric (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                      INT4 i4IsisIPRAIndex, INT4 *pi4RetValIsisIPRAMetric)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisIPRADefMetric ()"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType : %d\n IPRAIndex :%d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else
        {

            *pi4RetValIsisIPRAMetric =
                (INT4) (ISIS_METRIC_VAL_MASK & pIPRARec->Metric[0]);
            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisIPRAMetric ()"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisIPRAMetricType
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                retValIsisIPRAMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisIPRAMetricType (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                          INT4 i4IsisIPRAIndex,
                          INT4 *pi4RetValIsisIPRAMetricType)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisIPRAMetricType ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType : %d\n IPRAIndex :%d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            *pi4RetValIsisIPRAMetricType =
                (INT4) ISIS_GET_MET_TYPE (pIPRARec->Metric[0]);

            /* Here we need to increment the value since the 
             * mib defines the internal type as 1 and External type as 2
             */
            *pi4RetValIsisIPRAMetricType = *pi4RetValIsisIPRAMetricType + 1;

            i1RetCode = SNMP_SUCCESS;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisIPRADefMetricType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhGetIsisIPRASNPAAddress
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                retValIsisIPRASNPAAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisIPRASNPAAddress (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                           INT4 i4IsisIPRAIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValIsisIPRASNPAAddress)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered function nmhGetIsisIPRASNPAAddress()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType "
                     ": %d\n IPRAIndex :%d\n", i4IsisSysInstance,
                     i4IsisIPRAType, i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else
        {

            MEMCPY (pRetValIsisIPRASNPAAddress->pu1_OctetList,
                    pIPRARec->au1IPRASNPA, ISIS_SNPA_ADDR_LEN);

            pRetValIsisIPRASNPAAddress->i4_Length = ISIS_SNPA_ADDR_LEN;
            i1RetCode = SNMP_SUCCESS;
        }

    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting function nmhGetIsisIPRASNPAAddress()\n"));
    return (i1RetCode);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIsisIPRADestType
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                setValIsisIPRADestType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisIPRADestType (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                        INT4 i4IsisIPRAIndex, INT4 i4SetValIsisIPRADestType)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisIPRADestType ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices"
                     "sysInstance : %d\nIPRAType : %d\nIPRAIndex :%d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Cannot set the variable as Exist"
                     "State is active\n"));
            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            pIPRARec->u1IPRADestType = (UINT1) i4SetValIsisIPRADestType;
            pIPRARec->u1QFlag |= ISIS_IPRADESTTYPE_FLAG;

            if (pIPRARec->u1QFlag == ISIS_IPRAALLSET_FLAG)
            {
                pIPRARec->u1IPRAExistState = ISIS_NOT_IN_SER;
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));
        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisIPRADestType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetIsisIPRADest
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                setValIsisIPRADest
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisIPRADest (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                    INT4 i4IsisIPRAIndex,
                    tSNMP_OCTET_STRING_TYPE * pSetValIsisIPRADest)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisIPRADest()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : IPRAEntry not existed for the indices "
                     "sysInstance : %d\n IPRAType : %d\n"
                     "IPRAIndex :%d\n", i4IsisSysInstance, i4IsisIPRAType,
                     i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Cannot set the variable as Exist"
                     "State is active\n"));
            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            pIPRARec->u1QFlag |= ISIS_IPRADEST_FLAG;

            if (pIPRARec->u1QFlag == ISIS_IPRAALLSET_FLAG)
            {
                pIPRARec->u1IPRAExistState = ISIS_NOT_IN_SER;
            }

            MEMCPY (pIPRARec->au1IPRADest,
                    pSetValIsisIPRADest->pu1_OctetList,
                    pSetValIsisIPRADest->i4_Length);
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisIPRADest ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetIsisIPRADestPrefixLen
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                setValIsisIPRADestPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisIPRADestPrefixLen (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                             INT4 i4IsisIPRAIndex,
                             UINT4 u4SetValIsisIPRADestPrefixLen)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisIPRADestPrefixLen ()\n"));

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <T> : No IPRA Entry Exists For The Indices "
                     "SysInstance %d\n  IPRAType %d\n"
                     "IPRAIndex %d\n", i4IsisSysInstance, i4IsisIPRAType,
                     i4IsisIPRAIndex));

            return SNMP_FAILURE;
        }
        else if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Cannot set the variable as Exist"
                     "State is active\n"));
            return SNMP_FAILURE;
        }

        pIPRARec->u1PrefixLen = (UINT1) u4SetValIsisIPRADestPrefixLen;

        pIPRARec->u1QFlag |= ISIS_IPRAPREFIXLEN_FLAG;

        if (pIPRARec->u1QFlag == ISIS_IPRAALLSET_FLAG)
        {
            pIPRARec->u1IPRAExistState = ISIS_NOT_IN_SER;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisIPRADestPrefixLen ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetIsisIPRAExistState
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                setValIsisIPRAExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisIPRAExistState (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                          INT4 i4IsisIPRAIndex, INT4 i4SetValIsisIPRAExistState)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisIPRAExistState()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        switch (i4SetValIsisIPRAExistState)
        {
            case ISIS_CR_WT:

                if (i4RetVal == ISIS_SUCCESS)
                {
                    i1RetCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST,
                             "NMP <T> : Create_And_Wait Failed as the "
                             "Entry Already Existed, The Indices are "
                             "sysInstance : %d\n IPRAType : %d\n "
                             "IPRAIndex : %d\n",
                             i4IsisSysInstance, i4IsisIPRAType,
                             i4IsisIPRAIndex));
                }
                else
                {
                    pIPRARec = (tIsisIPRAEntry *)
                        ISIS_MEM_ALLOC (ISIS_BUF_IPRA, sizeof (tIsisIPRAEntry));

                    if (pIPRARec == NULL)
                    {
                        CLI_SET_ERR (CLI_ISIS_MAX_ENTRY);
                        /* Forming Memory Resource Failure Event and
                         * sending to control queue
                         */

                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPRA);
                        i1RetCode = SNMP_FAILURE;
                    }
                    else
                    {
                        pIPRARec->u4IPRAIdx = (UINT4) i4IsisIPRAIndex;
                        pIPRARec->u1IPRAType = (UINT1) i4IsisIPRAType;
                        pIPRARec->u1IPRAExistState = ISIS_NOT_READY;

                        IsisCtrlCopyIPRADefVal (pContext, pIPRARec);

                        /* Checking whether all the mandatory 
                         * parameters are set
                         */

                        if (pIPRARec->u1QFlag == ISIS_IPRAALLSET_FLAG)
                        {
                            pIPRARec->u1IPRAExistState = ISIS_NOT_IN_SER;
                        }
                        else
                        {
                            pIPRARec->u1IPRAExistState = ISIS_NOT_READY;
                        }
                        IsisCtrlAddIPRA (pContext, pIPRARec);

                        i1RetCode = SNMP_SUCCESS;
                    }
                }

                break;

            case ISIS_ACTIVE:

                if (i4RetVal != ISIS_SUCCESS)
                {
                    i1RetCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST,
                             "NMP <T> : Setting IPRA Active Failed "
                             "The Indices are "
                             "sysInstance : %d\n"
                             "IPRAType : %d\n IPRAIndex : %d\n",
                             i4IsisSysInstance, i4IsisIPRAType,
                             i4IsisIPRAIndex));
                }
                else
                {
                    if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
                    {
                        i1RetCode = SNMP_SUCCESS;
                        break;
                    }

                    if (pIPRARec->u1IPRAExistState == ISIS_NOT_IN_SER)
                    {
                        pIPRARec->u1IPRAExistState =
                            (UINT1) i4SetValIsisIPRAExistState;

                        if ((pContext->u1OperState == ISIS_UP)
                            && (pIPRARec->u1IPRAAdminState == ISIS_STATE_ON))
                        {
                            pIPRAEvt = (tIsisEvtIPRAChange *)
                                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtIPRAChange));

                            if (pIPRAEvt != NULL)
                            {
                                pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                                pIPRAEvt->u1Status = ISIS_IPRA_UP;
                                pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                                pIPRAEvt->u1IPRAType = (UINT1) i4IsisIPRAType;
                                IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                                  sizeof (tIsisEvtIPRAChange));
                            }
                        }
                    }
                    else
                    {
                        i1RetCode = SNMP_FAILURE;
                    }
                }

                break;

            case ISIS_NOT_IN_SER:

                if (i4RetVal != ISIS_SUCCESS)
                {
                    i1RetCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST,
                             "NMP <T> : Create_And_Wait Failed as the "
                             "Entry Already Existed, The Indices are "
                             "sysInstance : %d\n "
                             "IPRAType : %d\n IPRAIndex : %d\n",
                             i4IsisSysInstance, i4IsisIPRAType,
                             i4IsisIPRAIndex));
                }
                else
                {
                    if ((pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
                    {
                        pIPRARec->u1IPRAExistState =
                            (UINT1) i4SetValIsisIPRAExistState;
                        i1RetCode = SNMP_SUCCESS;

                        if (pContext->u1OperState == ISIS_UP)
                        {
                            pIPRAEvt = (tIsisEvtIPRAChange *)
                                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtIPRAChange));

                            /* Post an IPRA_UP event to Control
                             */

                            if (pIPRAEvt != NULL)
                            {
                                pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                                pIPRAEvt->u1Status = ISIS_IPRA_DOWN;
                                pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                                pIPRAEvt->u1IPRAType = (UINT1) i4IsisIPRAType;
                                if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
                                {
                                    MEMCPY (pIPRAEvt->au1IPRADest,
                                            pIPRARec->au1IPRADest,
                                            ISIS_MAX_IPV4_ADDR_LEN);
                                }
                                else
                                {
                                    MEMCPY (pIPRAEvt->au1IPRADest,
                                            pIPRARec->au1IPRADest,
                                            ISIS_MAX_IPV6_ADDR_LEN);
                                }
                                ISIS_SET_METRIC (pContext, pIPRAEvt, pIPRARec);
                                IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                                  sizeof (tIsisEvtIPRAChange));
                            }
                        }
                    }
                    else if ((pIPRARec->u1IPRAExistState == ISIS_NOT_READY)
                             && (pIPRARec->u1QFlag == ISIS_IPRAALLSET_FLAG))
                    {
                        pIPRARec->u1IPRAExistState =
                            (UINT1) i4SetValIsisIPRAExistState;
                    }
                    else
                    {
                        if (pIPRARec->u1IPRAExistState != ISIS_NOT_IN_SER)
                        {
                            i1RetCode = SNMP_FAILURE;
                        }
                    }
                }

                break;

            case ISIS_DESTROY:

                if (i4RetVal != ISIS_SUCCESS)
                {
                    i1RetCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST,
                             "NMP <T> : Create_And_Wait Failed as the "
                             "Entry Already Existed, The Indices are "
                             "sysInstance : %d\n "
                             "IPRAType : %d\n IPRAIndex : %d\n",
                             i4IsisSysInstance, i4IsisIPRAType,
                             i4IsisIPRAIndex));
                }
                else
                {
                    if ((pContext->u1OperState == ISIS_UP)
                        && (pIPRARec->u1IPRAAdminState == ISIS_STATE_ON)
                        && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
                    {
                        pIPRAEvt = (tIsisEvtIPRAChange *)
                            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                            sizeof (tIsisEvtIPRAChange));

                        /* Post an IPRA_UP event to Control
                         */

                        if (pIPRAEvt != NULL)
                        {
                            pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                            pIPRAEvt->u1Status = ISIS_IPRA_DOWN;
                            pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                            pIPRAEvt->u1IPRAType = (UINT1) i4IsisIPRAType;
                            pIPRAEvt->u1PrefixLen = pIPRARec->u1PrefixLen;
                            pIPRAEvt->u1IPRADestType = pIPRARec->u1IPRADestType;
                            if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
                            {
                                MEMCPY (pIPRAEvt->au1IPRADest,
                                        pIPRARec->au1IPRADest,
                                        ISIS_MAX_IPV4_ADDR_LEN);
                            }
                            else
                            {
                                MEMCPY (pIPRAEvt->au1IPRADest,
                                        pIPRARec->au1IPRADest,
                                        ISIS_MAX_IPV6_ADDR_LEN);
                            }
                            ISIS_SET_METRIC (pContext, pIPRAEvt, pIPRARec);

                            IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                              sizeof (tIsisEvtIPRAChange));
                        }
                    }
                    IsisCtrlDelIPRA (pContext, (UINT4) i4IsisIPRAIndex,
                                     (UINT1) i4IsisIPRAType);
                }

                break;

            default:

                i1RetCode = SNMP_FAILURE;

                NMP_EE ((ISIS_LGST,
                         "NMP <T> : Invalid i4SetValIsisIPRAExistState "));
                break;
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisIPRAExistState()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetIsisIPRAAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                setValIsisIPRAAdminState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisIPRAAdminState (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                          INT4 i4IsisIPRAIndex, INT4 i4SetValIsisIPRAAdminState)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisIPRAAdminState()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <T> : No IPRA Entry exists for the indices"
                     "SysInstance %d\n IPRAType %d\n IPRAIndex %d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));

            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            pIPRARec->u1IPRAAdminState = (UINT1) i4SetValIsisIPRAAdminState;

            if ((pContext->u1OperState == ISIS_UP)
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));

                /* Post an IPRA_UP event to Control
                 */

                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;

                    if (pIPRARec->u1IPRAAdminState == ISIS_STATE_OFF)
                    {
                        pIPRAEvt->u1Status = ISIS_IPRA_DOWN;
                    }
                    else
                    {
                        pIPRAEvt->u1Status = ISIS_IPRA_UP;
                    }

                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4IsisIPRAType;
                    if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
                    {
                        MEMCPY (pIPRAEvt->au1IPRADest,
                                pIPRARec->au1IPRADest, ISIS_MAX_IPV4_ADDR_LEN);
                    }
                    else
                    {
                        MEMCPY (pIPRAEvt->au1IPRADest,
                                pIPRARec->au1IPRADest, ISIS_MAX_IPV6_ADDR_LEN);
                    }
                    ISIS_SET_METRIC (pContext, pIPRAEvt, pIPRARec);

                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisIPRAAdminState()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetIsisIPRAMetric
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                setValIsisIPRAMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisIPRAMetric (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                      INT4 i4IsisIPRAIndex, INT4 i4SetValIsisIPRAMetric)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisEvtIPRAChange *pIPRAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisIPRADefMetric ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_EE ((ISIS_LGST,
                     "NMP <T> : No IPRA Entry exists for the indices "
                     "SysInstance %d\n IPRAType %d\n IPRAIndex %d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));
            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            pIPRARec->Metric[0] =
                (UINT1) (i4SetValIsisIPRAMetric & ISIS_METRIC_VAL_MASK);

            if ((pContext->u1OperState == ISIS_UP)
                && (pIPRARec->u1IPRAAdminState == ISIS_STATE_ON)
                && (pIPRARec->u1IPRAExistState == ISIS_ACTIVE))
            {
                pIPRAEvt = (tIsisEvtIPRAChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPRAChange));

                if (pIPRAEvt != NULL)
                {
                    pIPRAEvt->u1EvtID = ISIS_EVT_IPRA_CHANGE;
                    pIPRAEvt->u1Status = ISIS_IPRA_CHG;
                    pIPRAEvt->u4IPRAIdx = pIPRARec->u4IPRAIdx;
                    pIPRAEvt->u1IPRAType = (UINT1) i4IsisIPRAType;
                    pIPRAEvt->u1MetricType = ISIS_DEF_MET;
                    MEMCPY (pIPRAEvt->Metric, pIPRARec->Metric,
                            ISIS_NUM_METRICS);
                    IsisUtlSendEvent (pContext, (UINT1 *) pIPRAEvt,
                                      sizeof (tIsisEvtIPRAChange));
                }
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisIPRAAdminState()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetIsisIPRAMetricType
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                setValIsisIPRAMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisIPRAMetricType (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                          INT4 i4IsisIPRAIndex, INT4 i4SetValIsisIPRAMetricType)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisIPRADefMetricType\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <T> : No IPRA Entry exists for the indices"
                     "SysInstance %d\n IPRAType %d\n"
                     "IPRAIndex %d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));
        }
        else if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Cannot set the variable as Exist"
                     "State is active\n"));
            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            if (i4SetValIsisIPRAMetricType == ISIS_EXTERNAL)
            {
                pIPRARec->Metric[0] = (UINT1)
                    (pIPRARec->Metric[0] | ISIS_METRIC_TYPE_MASK);
            }
            else
            {
                pIPRARec->Metric[0] = (UINT1)
                    (pIPRARec->Metric[0] & ISIS_METRIC_VAL_MASK);
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisIPRADefMetricType()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhSetIsisIPRASNPAAddress
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                setValIsisIPRASNPAAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisIPRASNPAAddress (INT4 i4IsisSysInstance, INT4 i4IsisIPRAType,
                           INT4 i4IsisIPRAIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValIsisIPRASNPAAddress)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered function nmhSetIsisIPRASNPAAddress ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        /* Get the matching IP RA entry */

        i4RetVal = IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                       (UINT1) i4IsisIPRAType, &pIPRARec);

        if (i4RetVal == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <T> : No IPRA Entry exists for the indices"
                     "SysInstance %d\n  IPRAType %d\n"
                     "IPRAIndex %d\n",
                     i4IsisSysInstance, i4IsisIPRAType, i4IsisIPRAIndex));

            return (SNMP_FAILURE);
        }
        else if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Cannot set the variable as Exist"
                     "State is active\n"));
            i1RetCode = SNMP_FAILURE;
        }
        else
        {
            MEMCPY (pIPRARec->au1IPRASNPA,
                    pSetValIsisIPRASNPAAddress->pu1_OctetList,
                    ISIS_SNPA_ADDR_LEN);
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));

        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisIPRAAdminState()\n"));

    return (i1RetCode);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IsisIPRADestType
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                testValIsisIPRADestType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisIPRADestType (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                           INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                           INT4 i4TestValIsisIPRADestType)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> :Entered nmhTestv2IsisIPRADestType ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4IsisSysInstance,
                                     i4IsisIPRAType, i4IsisIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisIPRADestType ()\n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return (i1RetCode);
    }

    if ((i4TestValIsisIPRADestType != ISIS_IPV4)
        && (i4TestValIsisIPRADestType != ISIS_IPV6))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid i4TestValIsisIPRADestType : %d\n",
                 i4TestValIsisIPRADestType));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisIPRADestType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2IsisIPRADest
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                testValIsisIPRADest
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisIPRADest (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                       INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                       tSNMP_OCTET_STRING_TYPE * pTestValIsisIPRADest)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisIPRADest ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4IsisSysInstance,
                                     i4IsisIPRAType, i4IsisIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisIPRADest ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return (i1RetCode);
    }

    if ((pTestValIsisIPRADest->i4_Length != ISIS_MAX_IPV4_ADDR_LEN) &&
        (pTestValIsisIPRADest->i4_Length != ISIS_MAX_IPV6_ADDR_LEN))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid pTestValIsisIPRADest : %d\n",
                 pTestValIsisIPRADest->i4_Length));

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        i1RetCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisIPRADest()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2IsisIPRADestPrefixLen
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                testValIsisIPRADestPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisIPRADestPrefixLen (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                                UINT4 u4TestValIsisIPRADestPrefixLen)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisIPRADestPrefixLen ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4IsisSysInstance,
                                     i4IsisIPRAType, i4IsisIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisIPRADestPrefixLen ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return (i1RetCode);
    }

    if (u4TestValIsisIPRADestPrefixLen > ISIS_MAX_IPV6_PREFLEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid IPRAPrefixLength : %d\n",
                 u4TestValIsisIPRADestPrefixLen));

        i1RetCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisIPRADestPrefixLen ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2IsisIPRAExistState
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                testValIsisIPRAExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisIPRAExistState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                             INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                             INT4 i4TestValIsisIPRAExistState)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisIPRAExistState ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4IsisSysInstance,
                                     i4IsisIPRAType, i4IsisIPRAIndex,
                                     ISIS_TRUE);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisIPRADestType ()\n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return (i1RetCode);
    }

    switch (i4TestValIsisIPRAExistState)
    {
        case ISIS_CR_GO:

            *pu4ErrorCode = SNMP_ERR_NO_CREATION;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Invalid i4TestValIsisIPRAExistState: %d\n",
                     i4TestValIsisIPRAExistState));

            i1RetCode = SNMP_FAILURE;

            CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
            break;

        case ISIS_NOT_IN_SER:

            /* check whether the entry exists or not if doesn't exist return
             * SNMP_FAILURE 
             */

            i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance,
                                              &pContext);
            if (i4RetVal == ISIS_SUCCESS)
            {
                i4RetVal =
                    IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                        (UINT1) i4IsisIPRAType, &pIPRARec);

                if (i4RetVal == ISIS_FAILURE)
                {
                    NMP_EE ((ISIS_LGST,
                             "NMP <X> : No IPRA Entry exists for the "
                             "indices SysInstance %d\n IPRAType %d\n"
                             "IPRAIndex %d\n",
                             i4IsisSysInstance, i4IsisIPRAType,
                             i4IsisIPRAIndex));

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1RetCode = SNMP_FAILURE;

                    CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
                }
                else if (pIPRARec->u1IPRAExistState == ISIS_NOT_READY)
                {
                    if (pIPRARec->u1QFlag != ISIS_IPRAALLSET_FLAG)
                    {
                        NMP_EE ((ISIS_LGST,
                                 "NMP <X> :  IPRA Entry Mandatory "
                                 "parameters are not been set "));

                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        i1RetCode = SNMP_FAILURE;

                        CLI_SET_ERR (CLI_ISIS_PARAM_NOT_INIT);
                    }
                    else
                    {
                        i1RetCode = SNMP_SUCCESS;
                    }
                }
                else if (pIPRARec->u1IPRAExistState == ISIS_NOT_IN_SER)
                {
                    i1RetCode = SNMP_FAILURE;

                    CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
                }
                else if (pIPRARec->u1IPRAExistState == ISIS_ACTIVE)
                {
                    i1RetCode = SNMP_SUCCESS;
                    CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
                }
            }
            else
            {
                NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));
                CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
                i1RetCode = SNMP_FAILURE;
            }

            break;

        case ISIS_CR_WT:

            /* Check whether the entry  is already existing or  not */

            i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance,
                                              &pContext);
            if (i4RetVal == ISIS_SUCCESS)
            {
                i4RetVal =
                    IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                        (UINT1) i4IsisIPRAType, &pIPRARec);

                if (i4RetVal == ISIS_SUCCESS)
                {
                    NMP_EE ((ISIS_LGST,
                             "NMP <X> : IPRA Entry already exists for "
                             "the indices SysInstance %d\n IPRAType %d\n"
                             "IPRAIndex %d\n",
                             i4IsisSysInstance, i4IsisIPRAType,
                             i4IsisIPRAIndex));
                    CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1RetCode = SNMP_FAILURE;
                }
                else
                {
                    i1RetCode = SNMP_SUCCESS;
                }
            }
            else
            {
                NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));
                CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
                i1RetCode = SNMP_FAILURE;
            }

            break;

        case ISIS_ACTIVE:
        case ISIS_DESTROY:

            /* Check whether the entry is already existing or not */

            i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance,
                                              &pContext);

            if (i4RetVal == ISIS_SUCCESS)
            {
                i4RetVal =
                    IsisCtrlGetIPRARec (pContext, (UINT4) i4IsisIPRAIndex,
                                        (UINT1) i4IsisIPRAType, &pIPRARec);

                if (i4RetVal == ISIS_SUCCESS)
                {
                    if (i4TestValIsisIPRAExistState == ISIS_ACTIVE)
                    {
                        /* Check whether all the mandatory parameters are 
                         * set or not 
                         */

                        if (pIPRARec->u1QFlag != ISIS_IPRAALLSET_FLAG)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            i1RetCode = SNMP_FAILURE;
                        }
                    }
                    break;
                }

                /* IPRA Entry doesn't exist so only ISIS_DESTROY 
                 * is valid values 
                 */

                else
                {
                    if (i4TestValIsisIPRAExistState == ISIS_ACTIVE)
                    {
                        NMP_PT ((ISIS_LGST,
                                 "NMP <E> : Invalid "
                                 "i4TestValIsisIPRAExistState:"
                                 "%d\n", i4TestValIsisIPRAExistState));
                        CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        i1RetCode = SNMP_FAILURE;
                        break;
                    }
                }
            }
            NMP_PT ((ISIS_LGST, "NMP <T> : No such Instance Index \n"));
            CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
            i1RetCode = SNMP_FAILURE;
            break;

        case ISIS_NOT_READY:

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Invalid i4TestValIsisIPRAExistState: %d\n",
                     i4TestValIsisIPRAExistState));
            CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
            i1RetCode = SNMP_FAILURE;
            break;

        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetCode = SNMP_FAILURE;
            CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Invalid i4TestValIsisIPRAExistState: %d\n",
                     i4TestValIsisIPRAExistState));
            break;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisIPRAExistState()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2IsisIPRAAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                testValIsisIPRAAdminState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisIPRAAdminState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                             INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                             INT4 i4TestValIsisIPRAAdminState)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisIPRAAdminState ()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4IsisSysInstance,
                                     i4IsisIPRAType, i4IsisIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisIPRAAdminState ()\n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return (i1RetCode);
    }

    if ((i4TestValIsisIPRAAdminState == ISIS_STATE_ON)
        || (i4TestValIsisIPRAAdminState == ISIS_STATE_OFF))
    {
        i1RetCode = SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid i4TestValIsisIPRAAdminState: %d\n",
                 i4TestValIsisIPRAAdminState));
        i1RetCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisIPRAAdminState ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2IsisIPRAMetric
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                testValIsisIPRAMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisIPRAMetric (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                         INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                         INT4 i4TestValIsisIPRAMetric)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext     *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisIPRADefMetric()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4IsisSysInstance,
                                     i4IsisIPRAType, i4IsisIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisIPRADefMetric ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return (i1RetCode);
    }

    /* Default metric is not supported for Multi-topology ISIS. Hence
     * verifying if MT support is enabled or not*/
    if ((IsisCtrlGetSysContext ((UINT4)i4IsisSysInstance, &pContext) ==
        ISIS_SUCCESS) && (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Default metric not supported "
                "for Multi-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_INV_DEF_MET);
        return SNMP_FAILURE;
    }

    if ((i4TestValIsisIPRAMetric < ISIS_LL_CKTL_MIN_METRIC)
        || (i4TestValIsisIPRAMetric > ISIS_LL_CKTL_MAX_METRIC))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid i4TestValIsisIPRADefMetric: %d\n",
                 i4TestValIsisIPRAMetric));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_DEF_MET);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisIPRADefMetric ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2IsisIPRAMetricType
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                testValIsisIPRAMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisIPRAMetricType (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                             INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                             INT4 i4TestValIsisIPRAMetricType)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered function nmhTestv2IsisIPRADefMetricType()\n"));

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4IsisSysInstance,
                                     i4IsisIPRAType, i4IsisIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisIPRADefMetricType ()\n"));
        return (i1RetCode);
    }

    if ((i4TestValIsisIPRAMetricType != ISIS_INTERNAL)
        && (i4TestValIsisIPRAMetricType != ISIS_EXTERNAL))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid i4TestValIsisIPRADefMetricType: %d\n",
                 i4TestValIsisIPRAMetricType));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisIPRADefMetricType ()\n"));

    return (i1RetCode);

}

/****************************************************************************
 Function    :  nmhTestv2IsisIPRASNPAAddress
 Input       :  The Indices
                IsisSysInstance
                IsisIPRAType
                IsisIPRAIndex

                The Object 
                testValIsisIPRASNPAAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisIPRASNPAAddress (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                              INT4 i4IsisIPRAType, INT4 i4IsisIPRAIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValIsisIPRASNPAAddress)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    UINT1               au1IpRASnpaAddr[ISIS_SNPA_ADDR_LEN];

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisIPRASNPAAddress()\n"));

    MEMSET (au1IpRASnpaAddr, 0, ISIS_SNPA_ADDR_LEN);

    i1RetCode = IsisNmhValIPRATable (pu4ErrorCode, i4IsisSysInstance,
                                     i4IsisIPRAType, i4IsisIPRAIndex,
                                     ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisIPRASNPAAddress ()\n"));
        return (i1RetCode);
    }

    if ((MEMCMP (pTestValIsisIPRASNPAAddress->pu1_OctetList, au1IpRASnpaAddr,
                 ISIS_SNPA_ADDR_LEN) <= 0)
        || (pTestValIsisIPRASNPAAddress->i4_Length != ISIS_SNPA_ADDR_LEN))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid pTestValIsisIPRASNPAAddress or SNPA "
                 "Address Length : %s\n %d ",
                 pTestValIsisIPRASNPAAddress->pu1_OctetList,
                 pTestValIsisIPRASNPAAddress->i4_Length));

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisIPRASNPAAddress ()\n"));

    return (i1RetCode);

}
