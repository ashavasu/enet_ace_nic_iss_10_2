/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: iscktlow.c,v 1.24 2017/09/11 13:44:07 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include    "iscli.h"
# include  "lr.h"
# include  "fssnmp.h"
#include "isincl.h"
#include "isextn.h"
/* LOW LEVEL Routines for Table : IsisCircTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisCircTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisCircTable (INT4 i4IsisSysInstance,
                                       INT4 i4IsisCircIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisCircTable () \n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Instance Index. The Index is %d\n",
                 i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    else if ((i4IsisCircIndex <= 0) || (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Circuit Index. The Index is %d\n",
                 i4IsisCircIndex));

        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisCircTable () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisCircTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisCircTable (INT4 *pi4IsisSysInstance, INT4 *pi4IsisCircIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexIsisCircTable () \n"));

    i1ErrCode = nmhGetNextIndexIsisCircTable (0, pi4IsisSysInstance,
                                              0, pi4IsisCircIndex);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexIsisCircTable () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisCircTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisCircIndex
                nextIsisCircIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisCircTable (INT4 i4IsisSysInstance,
                              INT4 *pi4NextIsisSysInstance,
                              INT4 i4IsisCircIndex, INT4 *pi4NextIsisCircIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisSysContext    *pContext = NULL;
    INT4                i4RetValue = ISIS_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexIsisCircTable () \n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                            &u4InstIdx) == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next "
                     "Index with the given Indices \n"));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            u4CktIdx = 0;
            i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);

        }
    }

    while ((pContext != NULL) && (i1ErrCode == SNMP_SUCCESS))
    {
        if ((nmhUtlGetNextIndexIsisCircTable (u4InstIdx, u4CktIdx,
                                              &u4CktIdx)) == ISIS_FAILURE)
        {
            if ((nmhUtlGetNextIndexIsisSysTable (u4InstIdx, &u4InstIdx)
                 == ISIS_FAILURE))
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next "
                         "Index with the given Indices \n"));

                i1ErrCode = SNMP_FAILURE;
                break;
            }
            else
            {
                u4CktIdx = 0;
                i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = (INT4) u4InstIdx;
            *pi4NextIsisCircIndex = (INT4) u4CktIdx;
            i1ErrCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetNextIndexIsisCircTable () \n"));

    UNUSED_PARAM (i4RetValue);
    return i1ErrCode;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IsisCircTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IsisCircTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisCircIfIndex
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircIfIndex (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                       INT4 *pi4RetValIsisCircIfIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircIfIndex ()\n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircIfIndex = (INT4) pCktEntry->u4CktIfIdx;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircIfIndex ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircIfSubIndex
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircIfSubIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircIfSubIndex (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 *pi4RetValIsisCircIfSubIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircIfSubIndex () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircIfSubIndex = (INT4) pCktEntry->u4CktIfSubIdx;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircIfSubIndex () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLocalID
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircLocalID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLocalID (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                       INT4 *pi4RetValIsisCircLocalID)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircLocalID () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircLocalID = (INT4) pCktEntry->u1CktLocalID;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircLocalID () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircAdminState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircAdminState (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 *pi4RetValIsisCircAdminState)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircAdminState () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircAdminState = (INT4) pCktEntry->bCktAdminState;
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircAdminState () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircExistState
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircExistState (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 *pi4RetValIsisCircExistState)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircExistState () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircExistState = (INT4) pCktEntry->u1CktExistState;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircExistState () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircType
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircType (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                    INT4 *pi4RetValIsisCircType)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircType () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircType = (INT4) pCktEntry->u1CktType;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircType () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircExtDomain
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircExtDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircExtDomain (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                         INT4 *pi4RetValIsisCircExtDomain)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircExtDomain () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircExtDomain = (INT4) pCktEntry->bCktExtDomain;
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircExtDomain () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircAdjChanges
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircAdjChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircAdjChanges (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          UINT4 *pu4RetValIsisCircAdjChanges)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircAdjChanges () \n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting  nmhGetIsisCircAdjChanges()\n"));
        return (SNMP_FAILURE);
    }

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pu4RetValIsisCircAdjChanges = (pCktEntry->CktStats.u4CktAdjChgs);
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircAdjChanges () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircInitFails
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircInitFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircInitFails (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                         UINT4 *pu4RetValIsisCircInitFails)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircInitFails () \n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting  nmhGetIsisCircInitFails()\n"));
        return (SNMP_FAILURE);
    }

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pu4RetValIsisCircInitFails = (pCktEntry->CktStats.u4CktInitFails);
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit  Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircInitFails () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircRejAdjs
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircRejAdjs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircRejAdjs (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                       UINT4 *pu4RetValIsisCircRejAdjs)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircRejAdjs () \n"));
/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting  nmhGetIsisCircRejAdjs()\n"));
        return (SNMP_FAILURE);
    }

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pu4RetValIsisCircRejAdjs = pCktEntry->CktStats.u4CktRejAdj;
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircRejAdjs () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircOutCtrlPDUs
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircOutCtrlPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircOutCtrlPDUs (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                           UINT4 *pu4RetValIsisCircOutCtrlPDUs)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircOutCtrlPDUs () \n"));
/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting  nmhGetIsisCircOutCtrlPDUs()\n"));
        return (SNMP_FAILURE);
    }

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pu4RetValIsisCircOutCtrlPDUs =
                pCktEntry->CktStats.u4CktOutCtrlPDUs;
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircOutCtrlPDUs () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircInCtrlPDUs
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircInCtrlPDUs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircInCtrlPDUs (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          UINT4 *pu4RetValIsisCircInCtrlPDUs)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircInCtrlPDUs () \n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircInCtrlPDUs()\n"));
        return (SNMP_FAILURE);
    }

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pu4RetValIsisCircInCtrlPDUs = pCktEntry->CktStats.u4CktInCtrlPDUs;
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircInCtrlPDUs () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircIDFieldLenMismatches
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircIDFieldLenMismatches
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircIDFieldLenMismatches (INT4 i4IsisSysInstance,
                                    INT4 i4IsisCircIndex,
                                    UINT4
                                    *pu4RetValIsisCircIDFieldLenMismatches)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircIDFieldLenMismatches () \n"));
/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting  nmhGetIsisCircIDFieldLenMismatches()\n"));
        return (SNMP_FAILURE);
    }

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pu4RetValIsisCircIDFieldLenMismatches =
                pCktEntry->CktStats.u4CktIdLenMsmtchs;
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircIDFieldLenMismatches () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevel
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevel (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                     INT4 *pi4RetValIsisCircLevel)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircLevel () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircLevel = (INT4) pCktEntry->u1CktLevel;
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircLevel () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircMCAddr
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircMCAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircMCAddr (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                      INT4 *pi4RetValIsisCircMCAddr)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircMCAddr () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircMCAddr = (INT4) pCktEntry->u1CktMCAddr;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircMCAddr () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircPtToPtCircID
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircPtToPtCircID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircPtToPtCircID (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValIsisCircPtToPtCircID)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircPtToPtCircID () \n"));
/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting  nmhGetIsisCircPtToPtCircID()\n"));
        return (SNMP_FAILURE);
    }

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            pRetValIsisCircPtToPtCircID->i4_Length =
                ISIS_SYS_ID_LEN + ISIS_CIRC_ID_LEN;

            MEMCPY (pRetValIsisCircPtToPtCircID->pu1_OctetList,
                    pCktEntry->au1P2PCktID, ISIS_SYS_ID_LEN + ISIS_CIRC_ID_LEN);

            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircPtToPtCircID () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircPassiveCircuit
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircPassiveCircuit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircPassiveCircuit (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                              INT4 *pi4RetValIsisCircPassiveCircuit)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircPassiveCircuit () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircPassiveCircuit = (INT4) pCktEntry->bCktPassiveCkt;
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircPassiveCircuit () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircMeshGroupEnabled
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircMeshGroupEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircMeshGroupEnabled (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                INT4 *pi4RetValIsisCircMeshGroupEnabled)
{
    /* Parameters are assigned to itself as it is not used in the 
     * current release of the product 
     */

    i4IsisSysInstance = i4IsisSysInstance;
    i4IsisCircIndex = i4IsisCircIndex;
    pi4RetValIsisCircMeshGroupEnabled = pi4RetValIsisCircMeshGroupEnabled;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircMeshGroupEnabled () \n"));
    NMP_PT ((ISIS_LGST, "NMP <E> : This Version does not support this\n"));
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircMeshGroupEnabled () \n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIsisCircMeshGroup
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircMeshGroup
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircMeshGroup (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                         INT4 *pi4RetValIsisCircMeshGroup)
{
    i4IsisSysInstance = i4IsisSysInstance;
    i4IsisCircIndex = i4IsisCircIndex;
    pi4RetValIsisCircMeshGroup = pi4RetValIsisCircMeshGroup;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircMeshGroup () \n"));
    NMP_PT ((ISIS_LGST, "NMP <E> : This Version does not support this\n"));
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircMeshGroup () \n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIsisCircSmallHellos
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircSmallHellos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircSmallHellos (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                           INT4 *pi4RetValIsisCircSmallHellos)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircSmallHellos () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircSmallHellos = (INT4) pCktEntry->bCktSmallHello;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircSmallHellos () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircUpTime
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                retValIsisCircUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircUpTime (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                      INT4 *pi4RetValIsisCircUpTime)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircUpTime () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisCircUpTime = (INT4)
                ISIS_TIME_DIFF (pContext->SysTimers.TmrECInfo.u4TCount,
                                pCktEntry->u4CktUpTime);
            i1ErrCode = SNMP_SUCCESS;

        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircUpTime () \n"));

    return i1ErrCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIsisCircIfIndex
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircIfIndex (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                       INT4 i4SetValIsisCircIfIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircIfIndex () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */

            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could not Set CktIfIndex. "
                         "as RowStatus is ACTIVE\n"));
                CLI_SET_ERR (CLI_ISIS_ROW_ACTIVE);
                i1ErrCode = SNMP_FAILURE;
            }

            /* Set the Value only if the RowStatus is not in Active State 
             */

            else
            {
                pCktEntry->u4CktIfIdx = (UINT4) i4SetValIsisCircIfIndex;
                pCktEntry->u4LLHandle = (UINT4) i4SetValIsisCircIfIndex;
                pCktEntry->u1QFlag |= ISIS_CIRCIFINDEX_FLAG;
                if (pCktEntry->u1QFlag == ISIS_CIRCALLSET_FLAG)
                {
                    pCktEntry->u1CktExistState = ISIS_NOT_IN_SER;
                }
                i1ErrCode = SNMP_SUCCESS;
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircIfIndex () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircIfSubIndex
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircIfSubIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircIfSubIndex (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 i4SetValIsisCircIfSubIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircIfSubIndex () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */

            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Could not Set CircIfSubIndex "
                         "as RowStatus is ACTIVE\n"));

                i1ErrCode = SNMP_FAILURE;
            }
            /* Set the Value only if the RowStatus is not in Active State 
             */
            else
            {
                pCktEntry->u4CktIfSubIdx = (UINT4) i4SetValIsisCircIfSubIndex;
                pCktEntry->u1QFlag |= ISIS_CIRCIFSUBINDEX_FLAG;
                if (pCktEntry->u1QFlag == ISIS_CIRCALLSET_FLAG)
                {
                    pCktEntry->u1CktExistState = ISIS_NOT_IN_SER;
                }
                i1ErrCode = SNMP_SUCCESS;

            }

        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircIfSubIndex () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLocalID
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircLocalID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLocalID (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                       INT4 i4SetValIsisCircLocalID)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircLocalID () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */

            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could not Set CircLocalID "
                         "as RowStatus = ACTIVE\n"));

                i1ErrCode = SNMP_FAILURE;
            }

            /* Set the Value only if the RowStatus is not in Active State 
             */

            else
            {
                if (pCktEntry->u1CktType == ISIS_P2P_CKT)
                {
                    pCktEntry->u1CktLocalID = ISIS_ZERO;
                }
                else
                {
                    pCktEntry->u1CktLocalID = (UINT1) i4SetValIsisCircLocalID;
                }
                pCktEntry->u1QFlag |= ISIS_CIRCLOCALID_FLAG;
                if (pCktEntry->u1QFlag == ISIS_CIRCALLSET_FLAG)
                {
                    pCktEntry->u1CktExistState = ISIS_NOT_IN_SER;
                }
                i1ErrCode = SNMP_SUCCESS;

            }

        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircLocalID () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircAdminState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircAdminState (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 i4SetValIsisCircAdminState)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Status = 0;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircAdminState () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else if (pCktEntry->bCktAdminState ==
                 (UINT1) i4SetValIsisCircAdminState)
        {
            i1ErrCode = SNMP_SUCCESS;
        }

        else
        {
            pCktEntry->bCktAdminState = (UINT1) i4SetValIsisCircAdminState;
            i1ErrCode = SNMP_SUCCESS;

            if (((pCktEntry->u1CktExistState == ISIS_ACTIVE)
                 && (pCktEntry->u1CktIfStatus == ISIS_STATE_ON))
                && (pContext->u1OperState == ISIS_UP))
            {
                if (pCktEntry->bCktAdminState == ISIS_STATE_ON)
                {
                    pCktEntry->u1OperState = (UINT1) ISIS_UP;
                    u1Status = (UINT1) ISIS_CKT_UP;
                }
                else
                {
                    pCktEntry->u1OperState = (UINT1) ISIS_DOWN;
                    u1Status = (UINT1) ISIS_CKT_DOWN;
                }

                pCktEvt = (tIsisEvtCktChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));

                if (pCktEvt != NULL)
                {
                    pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                    pCktEvt->u1Status = u1Status;
                    pCktEvt->u1CktType = pCktEntry->u1CktType;
                    pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                    pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;

                    IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                      sizeof (tIsisEvtCktChange));
                }
            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircAdminState () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircExistState
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircExistState (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 i4SetValIsisCircExistState)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT1               au1Hash[8];
    UINT1               u1HashIdx;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRecord = NULL;
    tIsisAdjEntry      *pAdjRecord = NULL;
    UINT4               u4IfIndex = 0;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircExistState () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        PANIC ((ISIS_LGST, "NMP <W> : ISIS System Not Active !!! \n"));
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        switch (i4SetValIsisCircExistState)
        {
            case ISIS_CR_WT:

                pCktEntry = (tIsisCktEntry *)
                    ISIS_MEM_ALLOC (ISIS_BUF_CKTS, sizeof (tIsisCktEntry));

                if (pCktEntry == NULL)
                {

                    PANIC ((ISIS_LGST,
                            ISIS_MEM_ALLOC_FAIL " : Circuit !!! \n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    /* Forming Memory Resource Failure Event and sending to 
                     * control queue
                     */

                    IsisUtlFormResFailEvt (pContext, ISIS_BUF_CKTS);

                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    pCktEntry->u4CktIdx = u4CktIdx;
                    pCktEntry->u1CktExistState = ISIS_NOT_READY;
                    i4RetVal = IsisAdjCopyCktDefVal (pContext, pCktEntry);
                    if (i4RetVal == ISIS_FAILURE)
                    {
                        MEMSET (pCktEntry, 0, sizeof (tIsisCktEntry));
                        ISIS_MEM_FREE (ISIS_BUF_CKTS, (UINT1 *) pCktEntry);
                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_CKTS);
                        i1ErrCode = SNMP_FAILURE;
                    }
                    else
                    {
                        IsisAdjAddCkt (pContext, pCktEntry);
                    }
                }
                break;

            case ISIS_ACTIVE:

                if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                  &pCktEntry)) != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                             "exist for the Index,  %u\n", u4CktIdx));

                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    if (pCktEntry->u1CktExistState == ISIS_NOT_IN_SER)
                    {
                        pCktEntry->u1CktExistState = ISIS_ACTIVE;
                        u4IfIndex = pCktEntry->u4CktIfIdx;
                        if (CfaIsLoopBackIntf (u4IfIndex) == TRUE)
                        {
                            pCktEntry->u1IsisBfdStatus = ISIS_BFD_DISABLE;
                        }

                        /*If new circuit is coming up and the router is in helping mode,
                         * router is supposed to quit the helping mode in all the circuit*/
                        if (pContext->u1IsisGRRestartMode == ISIS_GR_HELPER)
                        {
                            /*Scanning all the circuits */
                            for (pCktRecord = pContext->CktTable.pCktRec;
                                 pCktRecord != NULL;
                                 pCktRecord = pCktRecord->pNext)
                            {
                                pAdjRecord = pCktRecord->pAdjEntry;
                                /*To find ,to whom should we stop helping */
                                while (pAdjRecord != NULL)
                                {
                                    if (pAdjRecord->u1IsisGRHelperStatus ==
                                        ISIS_GR_HELPING)
                                    {
                                        IsisGrExitHelper (pContext, pAdjRecord,
                                                          ISIS_GR_HELPER_TOP_CHG);
                                    }
                                    pAdjRecord = pAdjRecord->pNext;
                                }
                            }
                        }

                        /* Add the Circuit in the Global Hash Table
                         */

                        MEMCPY (au1Hash, (UINT1 *) &(pCktEntry->u4CktIfIdx),
                                sizeof (UINT4));
                        MEMCPY (&au1Hash[4],
                                (UINT1 *) &(pCktEntry->u4CktIfSubIdx),
                                sizeof (UINT4));
                        u1HashIdx =
                            IsisUtlGetHashIdx ((UINT1 *) au1Hash,
                                               2 * sizeof (UINT4),
                                               gIsisCktHashTable.u1HashSize);

                        UNUSED_PARAM (u1HashIdx);
                        /* Invoking the TRFR Routine to bind the Circuit to
                         * Data Link Layer
                         */

                        IsisTrfrIfStatusInd (pContext, pCktEntry, ISIS_CKT_UP);

                    }
                    else
                    {
                        NMP_PT ((ISIS_LGST,
                                 "NMP <E> : Could not Set CktExistState to "
                                 "Active since RowStatus != NOT_IN_SERVICE "
                                 "state\n"));

                        i1ErrCode = ISIS_FAILURE;
                    }
                }
                break;

            case ISIS_DESTROY:

                if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                  &pCktEntry)) != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                             "exist for the Index,  %u\n", u4CktIdx));

                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    /*If an circuit is coming down and the router is in helping mode,
                     * router is supposed to quit the helping mode in all the circuit*/
                    if (pContext->u1IsisGRRestartMode == ISIS_GR_HELPER)
                    {
                        /*Scanning all the circuits */
                        for (pCktRecord = pContext->CktTable.pCktRec;
                             pCktRecord != NULL; pCktRecord = pCktRecord->pNext)
                        {
                            pAdjRecord = pCktRecord->pAdjEntry;
                            /*To find ,to whom should we stop helping */
                            while (pAdjRecord != NULL)
                            {
                                if (pAdjRecord->u1IsisGRHelperStatus ==
                                    ISIS_GR_HELPING)
                                {
                                    IsisGrExitHelper (pContext, pAdjRecord,
                                                      ISIS_GR_HELPER_TOP_CHG);
                                }
                                pAdjRecord = pAdjRecord->pNext;
                            }
                        }
                    }

                    /* Invoking the TRFR routine to unbind this circuit
                     * from Data Link Layer
                     */

                    IsisTrfrIfStatusInd (pContext, pCktEntry, ISIS_CKT_DOWN);

                    /* Generate a Circuit DOWN Event to the Control Q
                     */

                    pCktEvt = (tIsisEvtCktChange *)
                        ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                        sizeof (tIsisEvtCktChange));
                    if (pCktEvt != NULL)
                    {
                        pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                        pCktEvt->u1Status = ISIS_CKT_DESTROY;
                        pCktEvt->u1CktType = pCktEntry->u1CktType;
                        pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                        pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;

                        IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                          sizeof (tIsisEvtCktChange));
                    }

                }
                break;

            case ISIS_NOT_IN_SER:

                if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                  &pCktEntry)) != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                             "exist for the Index,  %u\n", u4CktIdx));

                    i1ErrCode = SNMP_FAILURE;
                }
                else if (pCktEntry->u1CktExistState == ISIS_NOT_IN_SER)
                {
                    i1ErrCode = SNMP_SUCCESS;
                }
                else
                {
                    pCktEntry->u1CktExistState = ISIS_NOT_IN_SER;

                    /* Invoking the TRFR routine to unbind this circuit
                     * from Data Link Layer
                     */

                    IsisTrfrIfStatusInd (pContext, pCktEntry, ISIS_CKT_DOWN);

                    /* Generate a Circuit DOWN Event to the Control Q
                     */

                    if (pCktEntry->u1OperState == ISIS_UP)
                    {
                        pCktEntry->u1OperState = ISIS_DOWN;

                        pCktEvt = (tIsisEvtCktChange *)
                            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                            sizeof (tIsisEvtCktChange));
                        if (pCktEvt != NULL)
                        {
                            pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                            pCktEvt->u1Status = ISIS_CKT_DOWN;
                            pCktEvt->u1CktType = pCktEntry->u1CktType;
                            pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                            pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;

                            IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                              sizeof (tIsisEvtCktChange));
                        }
                    }

                }
                break;

            default:

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid value for Circuit "
                         "RowStatus !!! \n"));

                i1ErrCode = SNMP_FAILURE;
                break;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));

        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircExistState () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircType
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircType (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                    INT4 i4SetValIsisCircType)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircType () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */
            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could not Set CircType as "
                         "RowStatus is  ACTIVE\n"));

                i1ErrCode = SNMP_FAILURE;
            }

            /* Set the Value only if the RowStatus is not in Active State 
             */

            else
            {
                pCktEntry->u1CktType = (UINT1) i4SetValIsisCircType;
                if (pCktEntry->u1CktType == ISIS_P2P_CKT)
                {
                    pCktEntry->u4ExtLocalCircID = pCktEntry->u4CktIfIdx;
                    pCktEntry->u1IsP2PThreeWayEnabled = ISIS_TRUE;
                    pCktEntry->u1CktLocalID = ISIS_ZERO;
                }
                if (pCktEntry->u1CktExistState == ISIS_NOT_IN_SER)
                {
                    IsisAdjInitHelloTmInt (pCktEntry);
                }
                /* Initialising the Circuit Level Record according to the 
                 * level of the Circuit configured and 
                 * copying the Default Values for the Circuit Level
                 */

                pCktEntry->u1QFlag |= ISIS_CIRCTYPE_FLAG;

                if (pCktEntry->u1QFlag == ISIS_CIRCALLSET_FLAG)
                {
                    pCktEntry->u1CktExistState = ISIS_NOT_IN_SER;
                }
                i1ErrCode = SNMP_SUCCESS;

            }

        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircType () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircExtDomain
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircExtDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircExtDomain (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                         INT4 i4SetValIsisCircExtDomain)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircExtDomain () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */
            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Could not Set CircExtDomain. "
                         "As RowStatus = ACTIVE\n"));

                i1ErrCode = SNMP_FAILURE;
            }

            /* Set the Value only if the RowStatus is not in Active State 
             */

            else
            {
                pCktEntry->bCktExtDomain = (UINT1) i4SetValIsisCircExtDomain;
                i1ErrCode = SNMP_SUCCESS;

            }

        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircExtDomain () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLevel
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevel (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                     INT4 i4SetValIsisCircLevel)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircLevel () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */

            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could not Set CircLevel as "
                         "RowStatus is ACTIVE\n"));

                i1ErrCode = SNMP_FAILURE;
            }

            /* Set the Value only if the RowStatus is not in Active State 
             */

            else
            {
                pCktEntry->u1CktLevel = (UINT1) i4SetValIsisCircLevel;

                /* Initialising the Circuit Level Record according to the 
                 * level of the Circuit configured and 
                 * copying the Default Values for the Circuit Level
                 */

                i4RetVal = IsisAdjActivateCktLvl (pContext, pCktEntry);
                if (i4RetVal == ISIS_FAILURE)
                {
                    i1ErrCode = SNMP_FAILURE;
                    return i1ErrCode;
                }
                i1ErrCode = SNMP_SUCCESS;

            }

        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircLevel () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircMCAddr
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircMCAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircMCAddr (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                      INT4 i4SetValIsisCircMCAddr)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircMCAddr () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */

            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could not Set CircMCAddr as "
                         "RowStatus is ACTIVE\n"));

                i1ErrCode = SNMP_FAILURE;
            }

            /* Set the Value only if the RowStatus is not in Active State 
             */

            else
            {
                pCktEntry->u1CktMCAddr = (UINT1) i4SetValIsisCircMCAddr;
                i1ErrCode = SNMP_SUCCESS;

            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircMCAddr () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircPassiveCircuit
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircPassiveCircuit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircPassiveCircuit (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                              INT4 i4SetValIsisCircPassiveCircuit)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
#ifdef BFD_WANTED
    tIsisAdjEntry      *pTravAdj = NULL;
#endif
    INT4                i4IfIndex = 0;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircPassiveCircuit () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    nmhGetIsisCircIfIndex (i4IsisSysInstance, i4IsisCircIndex, &i4IfIndex);

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */

            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Could not Set CircPassiveCircuit "
                         "as RowStatus is ACTIVE\n"));

                i1ErrCode = SNMP_FAILURE;
            }

            /* Set the Value only if the RowStatus is not in Active State 
             */

            else
            {
                /* Loopback Interfaces should always be Passive */
                if (CfaIsLoopBackIntf ((UINT4) i4IfIndex) == TRUE)
                {
                    pCktEntry->bCktPassiveCkt = (UINT1) ISIS_TRUE;
                    i1ErrCode = SNMP_SUCCESS;
                }
                else
                {
                    pCktEntry->bCktPassiveCkt =
                        (UINT1) i4SetValIsisCircPassiveCircuit;
                    i1ErrCode = SNMP_SUCCESS;
                }
                if (pCktEntry->bCktPassiveCkt == (UINT1) ISIS_TRUE)
                {
                    /* If the circuit is changed to be a passive circuit,
                     * check if BFD is enabled on that circuit. If enabled,
                     * deregister from BFD */
#ifdef BFD_WANTED
                    pTravAdj = pCktEntry->pAdjEntry;
                    while (pTravAdj != NULL)
                    {
                        if (pCktEntry->u1IsisBfdStatus == ISIS_BFD_ENABLE)
                        {
                            ISIS_BFD_DEREGISTER (pContext, pTravAdj, pCktEntry,
                                                 ISIS_TRUE);
                        }
                        pTravAdj = pTravAdj->pNext;
                    }
#endif
                }

            }
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircPassiveCircuit () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircMeshGroupEnabled
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircMeshGroupEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircMeshGroupEnabled (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                INT4 i4SetValIsisCircMeshGroupEnabled)
{
    i4IsisSysInstance = i4IsisSysInstance;
    i4IsisCircIndex = i4IsisCircIndex;
    i4SetValIsisCircMeshGroupEnabled = i4SetValIsisCircMeshGroupEnabled;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircMeshGroupEnabled () \n"));
    NMP_PT ((ISIS_LGST, "NMP <E> : This Version does not support this\n"));
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircMeshGroupEnabled () \n"));

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIsisCircMeshGroup
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircMeshGroup
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircMeshGroup (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                         INT4 i4SetValIsisCircMeshGroup)
{

    i4IsisSysInstance = i4IsisSysInstance;
    i4IsisCircIndex = i4IsisCircIndex;
    i4SetValIsisCircMeshGroup = i4SetValIsisCircMeshGroup;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircMeshGroup () \n"));
    NMP_PT ((ISIS_LGST, "NMP <E> : This Version does not support this\n"));
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircMeshGroup () \n"));

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIsisCircSmallHellos
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                setValIsisCircSmallHellos
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircSmallHellos (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                           INT4 i4SetValIsisCircSmallHellos)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisCircSmallHellos () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            /* Check for the Status of the RowStatus in the Circuit Table
             */

            if (pCktEntry->u1CktExistState == ISIS_ACTIVE)
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Could not Set CircSmallHellos "
                         "as RowStatus is ACTIVE\n"));

                i1ErrCode = SNMP_FAILURE;
            }

            /* Set the Value only if the RowStatus is not in Active State 
             */

            else
            {
                pCktEntry->bCktSmallHello = (UINT1) i4SetValIsisCircSmallHellos;
                i1ErrCode = SNMP_SUCCESS;

            }

        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisCircSmallHellos () \n"));

    return i1ErrCode;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IsisCircIfIndex
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircIfIndex (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                          INT4 i4IsisCircIndex, INT4 i4TestValIsisCircIfIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircIfIndex () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircIfIndex () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return i1ErrCode;
    }

    if (i4TestValIsisCircIfIndex <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircIfIndex %d \n",
                 i4TestValIsisCircIfIndex));
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircIfIndex () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircIfSubIndex
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircIfSubIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircIfSubIndex (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                             INT4 i4IsisCircIndex,
                             INT4 i4TestValIsisCircIfSubIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircIfSubIndex () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircIfSubIndex () \n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return i1ErrCode;
    }

    if (i4TestValIsisCircIfSubIndex < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircIfSubIndex %d\n",
                 i4TestValIsisCircIfSubIndex));

        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircIfSubIndex () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLocalID
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircLocalID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLocalID (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                          INT4 i4IsisCircIndex, INT4 i4TestValIsisCircLocalID)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircLocalID () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLocalID () \n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return i1ErrCode;
    }

    if ((i4TestValIsisCircLocalID > ISIS_LL_MAX_CIRCUIT_LOCAL_ID)
        || (i4TestValIsisCircLocalID < ISIS_LL_MIN_CIRCUIT_LOCAL_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircLocalID %d\n",
                 i4TestValIsisCircLocalID));

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircLocalID () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircAdminState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircAdminState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                             INT4 i4IsisCircIndex,
                             INT4 i4TestValIsisCircAdminState)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircAdminState () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     * Since the Object to be set is AdminState, there is no need to check for
     * the Active State of the RowStatus of the Table
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_FALSE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircAdminState () \n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return i1ErrCode;
    }

    if ((i4TestValIsisCircAdminState != ISIS_STATE_ON)
        && (i4TestValIsisCircAdminState != ISIS_STATE_OFF))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircAdminState %d\n",
                 i4TestValIsisCircAdminState));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircAdminState () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircExistState
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircExistState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                             INT4 i4IsisCircIndex,
                             INT4 i4TestValIsisCircExistState)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircExistState () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * As the object to be Tested is ExistState, itself, we need not check the
     * existence of the Entry in the following routine. hence the third
     * parameter is ISIS_TRUE indicating that the object is a RowStatus Object
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_TRUE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircExistState () \n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return i1ErrCode;
    }

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        switch (i4TestValIsisCircExistState)
        {
            case ISIS_CR_WT:

                /* Checks whether the entry already exists
                 */

                if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                  &pCktEntry)) == ISIS_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Circuit Entry already exists "
                             "for the Index,  %u\n", u4CktIdx));

                    CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
                }
                else
                {
                    i1ErrCode = SNMP_SUCCESS;
                }
                break;

            case ISIS_CR_GO:
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Invalid Value of CircExistState %d\n",
                         i4TestValIsisCircExistState));

                CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
                break;

            case ISIS_NOT_IN_SER:

                /* if the entry doesn't exist, return failure 
                 */

                if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                  &pCktEntry)) != ISIS_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Circuit Entry does not exist "
                             "for the Index,  %u\n", u4CktIdx));

                    CLI_SET_ERR (CLI_ISIS_NO_ENTRY);
                }
                else if (pCktEntry->u1CktExistState == ISIS_NOT_READY)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                             "CircExistState %d\n",
                             i4TestValIsisCircExistState));
                    CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
                }
                else
                {
                    i1ErrCode = SNMP_SUCCESS;
                }
                break;

            case ISIS_NOT_READY:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Invalid Value of CircExistState %d\n",
                         i4TestValIsisCircExistState));
                CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);

                break;

            case ISIS_ACTIVE:
                if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                  &pCktEntry)) != ISIS_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Circuit Entry does not exist "
                             "for the Index,  %u\n", u4CktIdx));
                    CLI_SET_ERR (CLI_ISIS_NO_ENTRY);
                }
                else
                {
                    /* Checking for all the mandatory variables */

                    if ((pCktEntry->u1QFlag & ISIS_CIRCALLSET_FLAG) !=
                        ISIS_CIRCALLSET_FLAG)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        i1ErrCode = SNMP_FAILURE;

                        NMP_PT ((ISIS_LGST,
                                 "NMP <E> : Mandatory Params Not "
                                 "Initialised \n"));
                        CLI_SET_ERR (CLI_ISIS_PARAM_NOT_INIT);
                    }
                    else
                    {
                        i1ErrCode = SNMP_SUCCESS;
                    }
                }
                break;

            case ISIS_DESTROY:
                if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                                  &pCktEntry)) != ISIS_SUCCESS)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                             "CircExistState %d\n",
                             i4TestValIsisCircExistState));
                    CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
                }
                else
                {
                    i1ErrCode = SNMP_SUCCESS;
                }
                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Invalid Value of CircExistState %d\n",
                         i4TestValIsisCircExistState));
                CLI_SET_ERR (CLI_ISIS_INVALID_EXIST_STATE);
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        CLI_SET_ERR (CLI_ISIS_NO_ENTRY);
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircExistState () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircType
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircType (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                       INT4 i4IsisCircIndex, INT4 i4TestValIsisCircType)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircType () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircType () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return i1ErrCode;
    }

    if ((i4TestValIsisCircType != ISIS_BC_CKT)
        && (i4TestValIsisCircType != ISIS_P2P_CKT)
        && (i4TestValIsisCircType != ISIS_STATIC_IN)
        && (i4TestValIsisCircType != ISIS_STATIC_OUT)
        && (i4TestValIsisCircType != ISIS_DYNAMICALLY_ASSIGNED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircType %d\n",
                 i4TestValIsisCircType));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircType () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircExtDomain
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircExtDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircExtDomain (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                            INT4 i4IsisCircIndex,
                            INT4 i4TestValIsisCircExtDomain)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircExtDomain () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */
/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting  nmhTestv2IsisCircExtDomain()\n"));
        return (SNMP_FAILURE);
    }

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircExtDomain () \n"));
        return i1ErrCode;
    }

    if ((i4TestValIsisCircExtDomain != ISIS_TRUE)
        && (i4TestValIsisCircExtDomain != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircExtDomain %d\n",
                 i4TestValIsisCircExtDomain));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircExtDomain () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevel
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevel (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                        INT4 i4IsisCircIndex, INT4 i4TestValIsisCircLevel)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4IfIndex = 0;
    INT4                i4RetVal = 0;
    tIsisCktEntry      *pCktEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircLevel () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircLevel () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return i1ErrCode;
    }

    i1ErrCode = nmhGetIsisCircIfIndex (i4IsisSysInstance, i4IsisCircIndex,
                                       &i4IfIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISIS_RETRIEVE_RECORD_ERR);
        return SNMP_FAILURE;
    }

    i4RetVal = IsisAdjGetCktRecWithIfIdx (i4IfIndex, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {
        CLI_SET_ERR (CLI_ISIS_INS_NOT_EN_INTER);
        return SNMP_FAILURE;
    }

    if ((i4TestValIsisCircLevel != ISIS_LEVEL1)
        && (i4TestValIsisCircLevel != ISIS_LEVEL2)
        && (i4TestValIsisCircLevel != ISIS_LEVEL12))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircLevel %d\n",
                 i4TestValIsisCircLevel));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircLevel () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircMCAddr
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircMCAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircMCAddr (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                         INT4 i4IsisCircIndex, INT4 i4TestValIsisCircMCAddr)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisCircMCAddr () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircMCAddr () \n"));
        return i1ErrCode;
    }

    if ((i4TestValIsisCircMCAddr != ISIS_LL_GROUP)
        && (i4TestValIsisCircMCAddr != ISIS_LL_FUNCTIONAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircMCAddr %d\n",
                 i4TestValIsisCircMCAddr));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisCircMCAddr () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircPassiveCircuit
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircPassiveCircuit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircPassiveCircuit (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                 INT4 i4IsisCircIndex,
                                 INT4 i4TestValIsisCircPassiveCircuit)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircPassiveCircuit () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircPassiveCircuit () \n"));
        return i1ErrCode;
    }

    if ((i4TestValIsisCircPassiveCircuit != ISIS_TRUE)
        && (i4TestValIsisCircPassiveCircuit != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircPassiveCircuit %d\n", i4TestValIsisCircPassiveCircuit));

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircPassiveCircuit () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircMeshGroupEnabled
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircMeshGroupEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircMeshGroupEnabled (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                   INT4 i4IsisCircIndex,
                                   INT4 i4TestValIsisCircMeshGroupEnabled)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircMeshGroupEnabled () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircMeshGroupEnabled () \n"));
        return i1ErrCode;
    }

    if ((i4TestValIsisCircMeshGroupEnabled != ISIS_LL_MESH_INACTIVE)
        && (i4TestValIsisCircMeshGroupEnabled != ISIS_LL_MESH_BLOCKED)
        && (i4TestValIsisCircMeshGroupEnabled != ISIS_LL_MESH_SET))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircMeshGroupEnabled %d\n",
                 i4TestValIsisCircMeshGroupEnabled));

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircMeshGroupEnabled () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircMeshGroup
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircMeshGroup
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircMeshGroup (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                            INT4 i4IsisCircIndex,
                            INT4 i4TestValIsisCircMeshGroup)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircMeshGroup () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircMeshGroup () \n"));
        return i1ErrCode;
    }

    if ((i4TestValIsisCircMeshGroup > ISIS_LL_MAX_MESH_GROUP)
        || (i4TestValIsisCircMeshGroup < ISIS_LL_MIN_MESH_GROUP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircMeshGroup %d\n",
                 i4TestValIsisCircMeshGroup));

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircMeshGroup () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircSmallHellos
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex

                The Object 
                testValIsisCircSmallHellos
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircSmallHellos (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                              INT4 i4IsisCircIndex,
                              INT4 i4TestValIsisCircSmallHellos)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisCircSmallHellos () \n"));

    /* The following function validates the indices of the Circuit Table
     * If the indices are right,
     * if the third parameter is ISIS_FALSE, then it checks for the existence of
     * the Entry
     * if the fourth parameter is ISIS_TRUE, then it checks for the Active State
     * of the RowStatus
     */

    i1ErrCode =
        IsisNmhValCircTable (pu4ErrorCode, i4IsisSysInstance,
                             i4IsisCircIndex, ISIS_FALSE, ISIS_TRUE);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircSmallHellos () \n"));
        return i1ErrCode;
    }

    if ((i4TestValIsisCircSmallHellos != ISIS_STATE_OFF)
        && (i4TestValIsisCircSmallHellos != ISIS_STATE_ON))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircSmallHellos %d\n",
                 i4TestValIsisCircSmallHellos));
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisCircSmallHellos () \n"));

    return i1ErrCode;

}

/* LOW LEVEL Routines for Table : IsisCircLevelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisCircLevelTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisCircLevelTable (INT4 i4IsisSysInstance,
                                            INT4 i4IsisCircIndex,
                                            INT4 i4IsisCircLevelIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisCircLevelTable () \n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Instance Index. "
                 "The Index is %d\n", i4IsisSysInstance));
    }

    else if ((i4IsisCircIndex <= 0) || (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS))
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Index. "
                 "The Index is %d\n", i4IsisCircIndex));
    }
    else if ((i4IsisCircLevelIndex != ISIS_LEVEL1)
             && (i4IsisCircLevelIndex != ISIS_LEVEL2)
             && (i4IsisCircLevelIndex != ISIS_LEVEL12))
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                 "The Index is %d\n", i4IsisCircLevelIndex));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisCircLevelTable () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisCircLevelTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisCircLevelTable (INT4 *pi4IsisSysInstance,
                                    INT4 *pi4IsisCircIndex,
                                    INT4 *pi4IsisCircLevelIndex)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexIsisCircLevelTable () \n"));

    i1ErrCode = nmhGetNextIndexIsisCircLevelTable (0, pi4IsisSysInstance,
                                                   0, pi4IsisCircIndex,
                                                   0, pi4IsisCircLevelIndex);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexIsisCircLevelTable () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisCircLevelTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisCircIndex
                nextIsisCircIndex
                IsisCircLevelIndex
                nextIsisCircLevelIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisCircLevelTable (INT4 i4IsisSysInstance,
                                   INT4 *pi4NextIsisSysInstance,
                                   INT4 i4IsisCircIndex,
                                   INT4 *pi4NextIsisCircIndex,
                                   INT4 i4IsisCircLevelIndex,
                                   INT4 *pi4NextIsisCircLevelIndex)
{
    INT4                i4RetValue = ISIS_SUCCESS;
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexIsisCircLevelTable () \n"));

    if (u4CktIdx > ISIS_MAX_CKTS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Index. "
                 "The Index is %d\n", i4IsisCircIndex));

        i1ErrCode = SNMP_FAILURE;
    }

    else if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                                &pContext)) == ISIS_FAILURE)
    {
        if ((i4RetVal = nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                                        &u4InstIdx)) ==
            ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next "
                     "Index with the given Indices \n"));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            u4CktIdx = 0;
            u1CktLvlIdx = 0;
            i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);
            i1ErrCode = SNMP_SUCCESS;
        }
    }

    while ((pContext != NULL) && (i1ErrCode == SNMP_SUCCESS))
    {
        if ((i4RetVal =
             nmhUtlGetNextIndexIsisCircLevelTable (u4InstIdx, u4CktIdx,
                                                   u1CktLvlIdx,
                                                   &u1CktLvlIdx)) ==
            ISIS_FAILURE)
        {
            if ((i4RetVal =
                 nmhUtlGetNextIndexIsisCircTable (u4InstIdx, u4CktIdx,
                                                  &u4CktIdx)) == ISIS_FAILURE)
            {
                if ((i4RetVal = nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                                                &u4InstIdx)) ==
                    ISIS_FAILURE)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next "
                             "Index with the given Indices \n"));

                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    u4CktIdx = 0;
                    u1CktLvlIdx = 0;
                    i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);
                }
            }
            else
            {
                u1CktLvlIdx = 0;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = (INT4) u4InstIdx;
            *pi4NextIsisCircIndex = (INT4) u4CktIdx;
            *pi4NextIsisCircLevelIndex = u1CktLvlIdx;
            i1ErrCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexIsisCircLevelTable () \n"));
    UNUSED_PARAM (i4RetValue);
    return i1ErrCode;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IsisCircLevelTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IsisCircLevelTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisCircLevelMetric
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelMetric (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                           INT4 i4IsisCircLevelIndex,
                           INT4 *pi4RetValIsisCircLevelMetric)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelMetric () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelMetric =
                    (INT4) (pCktLvlEntry->Metric[0] & ISIS_METRIC_VAL_MASK);
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelISPriority
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelISPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelISPriority (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                               INT4 i4IsisCircLevelIndex,
                               INT4 *pi4RetValIsisCircLevelISPriority)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelISPriority () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry);

        if (i4RetVal == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelISPriority =
                    (INT4) pCktLvlEntry->u1ISPriority;
                i1ErrCode = SNMP_SUCCESS;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %d\n", i4IsisCircIndex));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelISPriority () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelDesIS
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelDesIS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelDesIS (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 i4IsisCircLevelIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValIsisCircLevelDesIS)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisCircLevelDesIS () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                pRetValIsisCircLevelDesIS->i4_Length = ISIS_SYS_ID_LEN +
                    ISIS_PNODE_ID_LEN;
                MEMCPY (pRetValIsisCircLevelDesIS->pu1_OctetList,
                        pCktLvlEntry->au1CktLanDISID,
                        ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircLevelDesIS () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelLANDesISChanges
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelLANDesISChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelLANDesISChanges (INT4 i4IsisSysInstance,
                                    INT4 i4IsisCircIndex,
                                    INT4 i4IsisCircLevelIndex,
                                    UINT4
                                    *pu4RetValIsisCircLevelLANDesISChanges)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelLANDesISChanges () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pu4RetValIsisCircLevelLANDesISChanges =
                    pCktLvlEntry->u4CktLanDISChgs;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelLANDesISChanges () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelHelloMultiplier
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelHelloMultiplier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelHelloMultiplier (INT4 i4IsisSysInstance,
                                    INT4 i4IsisCircIndex,
                                    INT4 i4IsisCircLevelIndex,
                                    INT4 *pi4RetValIsisCircLevelHelloMultiplier)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelHelloMultiplier () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelHelloMultiplier =
                    (INT4) pCktLvlEntry->u2HelloMultiplier;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelHelloMultiplier () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelHelloTimer
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelHelloTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelHelloTimer (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                               INT4 i4IsisCircLevelIndex,
                               INT4 *pi4RetValIsisCircLevelHelloTimer)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelHelloTimer () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelHelloTimer =
                    (INT4) pCktLvlEntry->HelloTimeInt;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelHelloTimer () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelDRHelloTimer
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelDRHelloTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelDRHelloTimer (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                 INT4 i4IsisCircLevelIndex,
                                 INT4 *pi4RetValIsisCircLevelDRHelloTimer)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelDRHelloTimer () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelDRHelloTimer =
                    (INT4) pCktLvlEntry->DRHelloTimeInt;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelDRHelloTimer () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelLSPThrottle
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelLSPThrottle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelLSPThrottle (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                INT4 i4IsisCircLevelIndex,
                                INT4 *pi4RetValIsisCircLevelLSPThrottle)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelLSPThrottle () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelLSPThrottle =
                    (INT4) pCktLvlEntry->LspThrottleInt;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelLSPThrottle () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelMinLSPRetransInt
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelMinLSPRetransInt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelMinLSPRetransInt (INT4 i4IsisSysInstance,
                                     INT4 i4IsisCircIndex,
                                     INT4 i4IsisCircLevelIndex,
                                     INT4
                                     *pi4RetValIsisCircLevelMinLSPRetransInt)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelMinLSPRetransInt () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelMinLSPRetransInt =
                    (INT4) pCktLvlEntry->u4MinLspReTxInt;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelMinLSPRetransInt () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelCSNPInterval
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelCSNPInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelCSNPInterval (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                 INT4 i4IsisCircLevelIndex,
                                 INT4 *pi4RetValIsisCircLevelCSNPInterval)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelCSNPInterval () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelCSNPInterval =
                    (INT4) pCktLvlEntry->CSNPInterval;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelCSNPInterval () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisCircLevelPartSNPInterval
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                retValIsisCircLevelPartSNPInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisCircLevelPartSNPInterval (INT4 i4IsisSysInstance,
                                    INT4 i4IsisCircIndex,
                                    INT4 i4IsisCircLevelIndex,
                                    INT4 *pi4RetValIsisCircLevelPartSNPInterval)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetIsisCircLevelPartSNPInterval () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry != NULL)
            {
                *pi4RetValIsisCircLevelPartSNPInterval =
                    (INT4) pCktLvlEntry->PSNPInterval;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));

        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetIsisCircLevelPartSNPInterval () \n"));

    return i1ErrCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIsisCircLevelMetric
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelMetric (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                           INT4 i4IsisCircLevelIndex,
                           INT4 i4SetValIsisCircLevelMetric)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelMetric () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {
            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));
            }
            else
            {
                pCktLvlEntry->Metric[0] = (UINT1) i4SetValIsisCircLevelMetric;

                /* ISIS_METRIC_SUPP_MASK is used for extracting the metric
                 * and hence will have the corresponding bit set to 1.
                 * A bitwise & with ~ISIS_METRIC_SUPP_MASK will reset 
                 * the bit which indicate the metric is supported
                 */

                pCktLvlEntry->Metric[0] &= ~ISIS_METRIC_SUPP_MASK;

                i1ErrCode = SNMP_SUCCESS;
                /* Generate Event for Metric Change as Circuit Change Event
                 */

                if (pCktEntry->u1OperState == ISIS_UP)
                {
                    pCktEvt = (tIsisEvtCktChange *)
                        ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                        sizeof (tIsisEvtCktChange));

                    if (pCktEvt != NULL)
                    {
                        pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                        pCktEvt->u1Status = ISIS_CKT_MODIFY;
                        pCktEvt->u1CktType = pCktEntry->u1CktType;
                        pCktEvt->u1CktLevel = u1CktLvlIdx;
                        pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;
                        MEMCPY (pCktEvt->Metric, pCktLvlEntry->Metric,
                                (sizeof (tIsisMetric)));

                        IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                          sizeof (tIsisEvtCktChange));
                    }
                }
            }
        }

        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuitt Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLevelISPriority
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelISPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelISPriority (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                               INT4 i4IsisCircLevelIndex,
                               INT4 i4SetValIsisCircLevelISPriority)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisEvtPriorityChange *pPrioEvt = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelISPriority () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {
            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));
            }
            else
            {
                if (pCktLvlEntry->u1ISPriority ==
                    (UINT1) i4SetValIsisCircLevelISPriority)
                {
                    return SNMP_SUCCESS;
                }

                pCktLvlEntry->u1ISPriority =
                    (UINT1) i4SetValIsisCircLevelISPriority;
                i1ErrCode = SNMP_SUCCESS;

                /* Generate Event for Priority Change if the RowStatus is ACTIVE
                 */

                if (pCktEntry->u1OperState == ISIS_UP)
                {
                    pPrioEvt = (tIsisEvtPriorityChange *)
                        ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                        sizeof (tIsisEvtPriorityChange));
                    if (pPrioEvt != NULL)
                    {
                        pPrioEvt->u1EvtID = ISIS_EVT_PRIORITY_CHANGE;
                        pPrioEvt->u1Priority =
                            (UINT1) i4SetValIsisCircLevelISPriority;
                        pPrioEvt->u1CktLevel = u1CktLvlIdx;
                        pPrioEvt->u4CktIdx = pCktEntry->u4CktIdx;

                        IsisUtlSendEvent (pContext, (UINT1 *) pPrioEvt,
                                          sizeof (tIsisEvtPriorityChange));
                    }
                }
            }
        }

        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelISPriority () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLevelHelloMultiplier
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelHelloMultiplier
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelHelloMultiplier (INT4 i4IsisSysInstance,
                                    INT4 i4IsisCircIndex,
                                    INT4 i4IsisCircLevelIndex,
                                    INT4 i4SetValIsisCircLevelHelloMultiplier)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelHelloMultiplier () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));
            }
            else
            {
                pCktLvlEntry->u2HelloMultiplier =
                    (UINT2) i4SetValIsisCircLevelHelloMultiplier;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelHelloMultiplier () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLevelHelloTimer
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelHelloTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelHelloTimer (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                               INT4 i4IsisCircLevelIndex,
                               INT4 i4SetValIsisCircLevelHelloTimer)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelHelloTimer () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));
            }
            else
            {
                pCktLvlEntry->HelloTimeInt =
                    (UINT4) i4SetValIsisCircLevelHelloTimer;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelHelloTimer () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLevelDRHelloTimer
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelDRHelloTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelDRHelloTimer (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                 INT4 i4IsisCircLevelIndex,
                                 INT4 i4SetValIsisCircLevelDRHelloTimer)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelDRHelloTimer () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));

            }
            else
            {
                pCktLvlEntry->DRHelloTimeInt =
                    (UINT2) i4SetValIsisCircLevelDRHelloTimer;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelDRHelloTimer () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLevelLSPThrottle
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelLSPThrottle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelLSPThrottle (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                INT4 i4IsisCircLevelIndex,
                                INT4 i4SetValIsisCircLevelLSPThrottle)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered mhSetIsisCircLevelLSPThrottle () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {
            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));
            }
            else
            {
                pCktLvlEntry->LspThrottleInt =
                    (UINT2) i4SetValIsisCircLevelLSPThrottle;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                     "exist for the Index,  %u\n", u4InstIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                 "exist for the Index,  %u\n", u4CktIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelLSPThrottle () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLevelMinLSPRetransInt
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelMinLSPRetransInt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelMinLSPRetransInt (INT4 i4IsisSysInstance,
                                     INT4 i4IsisCircIndex,
                                     INT4 i4IsisCircLevelIndex,
                                     INT4 i4SetValIsisCircLevelMinLSPRetransInt)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelMinLSPRetransInt () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));

            }
            else
            {
                pCktLvlEntry->u4MinLspReTxInt =
                    (UINT4) i4SetValIsisCircLevelMinLSPRetransInt;
                i1ErrCode = SNMP_SUCCESS;

            }
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelMinLSPRetransInt () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2FsIsisMultiTopologySupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                testValFsIsisMultiTopologySupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIsisMultiTopologySupport (UINT4 *pu4ErrorCode,
                                     INT4 i4FsIsisExtSysInstance,
                                     INT4 i4TestValFsIsisMultiTopologySupport)
{
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2FsIsisMultiTopologySupport () \n"));

    if ((IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext))
        != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", i4FsIsisExtSysInstance));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsIsisMultiTopologySupport != ISIS_TRUE) &&
        (i4TestValFsIsisMultiTopologySupport != ISIS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2FsIsisMultiTopologySupport () \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsIsisMultiTopologySupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                retValFsIsisMultiTopologySupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIsisMultiTopologySupport (INT4 i4FsIsisExtSysInstance,
                                  INT4 *pi4RetValFsIsisMultiTopologySupport)
{
    tIsisSysContext    *pContext = NULL;
    INT1                i1ErrCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetFsIsisMultiTopologySupport () \n"));

    if ((IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext))
        != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", i4FsIsisExtSysInstance));
    }
    else
    {
        *pi4RetValFsIsisMultiTopologySupport = (INT4) pContext->u1IsisMTSupport;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisCircAdminState () \n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetFsIsisMultiTopologySupport
 Input       :  The Indices
                FsIsisExtSysInstance

                The Object 
                setValFsIsisMultiTopologySupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIsisMultiTopologySupport (INT4 i4FsIsisExtSysInstance,
                                  INT4 i4SetValFsIsisMultiTopologySupport)
{
    tIsisSysContext    *pContext = NULL;
#ifdef BFD_WANTED
    tIsisCktEntry      *pTravCktRec = NULL;
    tIsisAdjEntry      *pTravAdj = NULL;
#endif
    INT4                i4ExistState = 0;
    UINT4               u4Error = 0;
    UINT1               u1PrevValue = 0;
    UINT1               u1PrevMetricStyle = 0;

    if ((IsisCtrlGetSysContext ((UINT4) i4FsIsisExtSysInstance, &pContext))
        != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", i4FsIsisExtSysInstance));
        return SNMP_FAILURE;
    }

    if (pContext->u1IsisMTSupport == (UINT1) i4SetValFsIsisMultiTopologySupport)
    {
        return SNMP_SUCCESS;
    }

#ifdef BFD_WANTED
    if ((i4SetValFsIsisMultiTopologySupport == ISIS_MT_DISABLE) &&
        (pContext->u1IsisBfdSupport == ISIS_BFD_ENABLE))
    {
        /* Deregister from BFD if registered previously */
        pTravCktRec = pContext->CktTable.pCktRec;
        while (pTravCktRec != NULL)
        {
            if (pTravCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE)
            {
                pTravAdj = pTravCktRec->pAdjEntry;
                while (pTravAdj != NULL)
                {
                    ISIS_BFD_DEREGISTER (pContext, pTravAdj, pTravCktRec,
                                         ISIS_FALSE);
                    pTravAdj = pTravAdj->pNext;
                }
            }
            pTravCktRec = pTravCktRec->pNext;
        }
    }
#endif

    if (nmhGetIsisSysExistState (i4FsIsisExtSysInstance, &i4ExistState) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4ExistState == ISIS_ACTIVE)
    {
        /* Made the Exist state down if its active */
        if (nmhTestv2IsisSysExistState
            (&u4Error, i4FsIsisExtSysInstance, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetIsisSysExistState (i4FsIsisExtSysInstance, NOT_IN_SERVICE) !=
            SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }

    u1PrevValue = pContext->u1IsisMTSupport;
    u1PrevMetricStyle = pContext->u1MetricStyle;
    pContext->u1IsisMTSupport = (UINT1) i4SetValFsIsisMultiTopologySupport;

    if ((pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC)
        || (i4SetValFsIsisMultiTopologySupport == ISIS_FALSE))
    {
        if (i4SetValFsIsisMultiTopologySupport == ISIS_FALSE)
        {
            pContext->u1MetricStyle = ISIS_STYLE_NARROW_METRIC;
        }
        else
        {
            pContext->u1MetricStyle = ISIS_STYLE_WIDE_METRIC;
        }
        IsisUtlUpdateMetricStyle (pContext);
        IsisUtlUpdateMetrics (pContext);
    }

    if (i4ExistState == ISIS_ACTIVE)
    {
        /* Move the admin state to Active only if it made down previously */
        if (nmhTestv2IsisSysExistState
            (&u4Error, i4FsIsisExtSysInstance, ISIS_ACTIVE) == SNMP_FAILURE)
        {
            pContext->u1IsisMTSupport = u1PrevValue;
            pContext->u1MetricStyle = u1PrevMetricStyle;
            IsisUtlUpdateMetricStyle (pContext);
            IsisUtlUpdateMetrics (pContext);
            return SNMP_FAILURE;
        }

        if (nmhSetIsisSysExistState (i4FsIsisExtSysInstance, ISIS_ACTIVE) !=
            SNMP_SUCCESS)
        {
            pContext->u1IsisMTSupport = u1PrevValue;
            pContext->u1MetricStyle = u1PrevMetricStyle;
            IsisUtlUpdateMetricStyle (pContext);
            IsisUtlUpdateMetrics (pContext);
            return SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetFsIsisMultiTopologySupport () \n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIsisCircLevelCSNPInterval
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelCSNPInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelCSNPInterval (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                                 INT4 i4IsisCircLevelIndex,
                                 INT4 i4SetValIsisCircLevelCSNPInterval)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelCSNPInterval () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));

            }
            else
            {
                pCktLvlEntry->CSNPInterval =
                    (UINT2) i4SetValIsisCircLevelCSNPInterval;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelCSNPInterval () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisCircLevelPartSNPInterval
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                setValIsisCircLevelPartSNPInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisCircLevelPartSNPInterval (INT4 i4IsisSysInstance,
                                    INT4 i4IsisCircIndex,
                                    INT4 i4IsisCircLevelIndex,
                                    INT4 i4SetValIsisCircLevelPartSNPInterval)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisCircLevelIndex;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhSetIsisCircLevelPartSNPInterval () \n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktEntry))
            == ISIS_SUCCESS)
        {

            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisCircLevelIndex));
            }
            if (pCktLvlEntry == NULL)
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                         "exist for the Index,  %d\n", u1CktLvlIdx));
            }
            else
            {
                pCktLvlEntry->PSNPInterval =
                    (UINT2) i4SetValIsisCircLevelPartSNPInterval;
                i1ErrCode = SNMP_SUCCESS;
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Entry does not "
                     "exist for the Index,  %u\n", u4CktIdx));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhSetIsisCircLevelPartSNPInterval () \n"));

    return i1ErrCode;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelMetric
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelMetric (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                              INT4 i4IsisCircIndex, INT4 i4IsisCircLevelIndex,
                              INT4 i4TestValIsisCircLevelMetric)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelMetric () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelMetric () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }
    /* Default metric is not supported for Multi-topology ISIS. Hence
     * verifying if MT support is enabled or not*/
    if ((IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext) ==
         ISIS_SUCCESS) && (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Default metric not supported "
                 "for Multi-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_MET_NOT_SUPP_MT);
        return SNMP_FAILURE;
    }

    if ((i4TestValIsisCircLevelMetric > ISIS_LL_CKTL_MAX_METRIC)
        || (i4TestValIsisCircLevelMetric < ISIS_LL_CKTL_MIN_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_DEF_MET);
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircLevelMetric \n"));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelMetric () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelISPriority
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelISPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelISPriority (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                  INT4 i4IsisCircIndex,
                                  INT4 i4IsisCircLevelIndex,
                                  INT4 i4TestValIsisCircLevelISPriority)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelISPriority () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelISPriority () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }

    if ((i4TestValIsisCircLevelISPriority > ISIS_LL_CKTL_MAX_PRIORITY)
        || (i4TestValIsisCircLevelISPriority < ISIS_LL_CKTL_MIN_PRIORITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_PRIO);
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of CircLevelISPriority "
                 "%d\n", i4TestValIsisCircLevelISPriority));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelISPriority () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelHelloMultiplier
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelHelloMultiplier
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelHelloMultiplier (UINT4 *pu4ErrorCode,
                                       INT4 i4IsisSysInstance,
                                       INT4 i4IsisCircIndex,
                                       INT4 i4IsisCircLevelIndex,
                                       INT4
                                       i4TestValIsisCircLevelHelloMultiplier)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelHelloMultiplier () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelHelloMultiplier () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }

    if ((i4TestValIsisCircLevelHelloMultiplier > ISIS_LL_CKTL_MAX_HELLO_MULT)
        || (i4TestValIsisCircLevelHelloMultiplier <
            ISIS_LL_CKTL_MIN_HELLO_MULT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircLevelHelloMultiplier %d\n",
                 i4TestValIsisCircLevelHelloMultiplier));
        CLI_SET_ERR (CLI_ISIS_INV_HELLO_MUL);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelHelloMultiplier () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelHelloTimer
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelHelloTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelHelloTimer (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                  INT4 i4IsisCircIndex,
                                  INT4 i4IsisCircLevelIndex,
                                  INT4 i4TestValIsisCircLevelHelloTimer)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelHelloTimer () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelHelloTimer () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }

    if ((i4TestValIsisCircLevelHelloTimer > ISIS_LL_CKTL_MAX_HELLO_TIMER)
        || (i4TestValIsisCircLevelHelloTimer < ISIS_LL_CKTL_MIN_HELLO_TIMER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_HELLO_INT);
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of CircLevelHelloTimer \n"));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelHelloTimer () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelDRHelloTimer
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelDRHelloTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelDRHelloTimer (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                    INT4 i4IsisCircIndex,
                                    INT4 i4IsisCircLevelIndex,
                                    INT4 i4TestValIsisCircLevelDRHelloTimer)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelDRHelloTimer () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelDRHelloTimer () \n"));
        return i1ErrCode;
    }

    if ((i4TestValIsisCircLevelDRHelloTimer > ISIS_LL_CKTL_MAX_DR_HELLO_TIMER)
        || (i4TestValIsisCircLevelDRHelloTimer <
            ISIS_LL_CKTL_MIN_DR_HELLO_TIMER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircLevelDRHelloTimer %d\n",
                 i4TestValIsisCircLevelDRHelloTimer));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelDRHelloTimer () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelLSPThrottle
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelLSPThrottle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelLSPThrottle (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                   INT4 i4IsisCircIndex,
                                   INT4 i4IsisCircLevelIndex,
                                   INT4 i4TestValIsisCircLevelLSPThrottle)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelLSPThrottle () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelLSPThrottle () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }

    if ((i4TestValIsisCircLevelLSPThrottle > ISIS_LL_CKTL_MAX_LSP_THROTTLE)
        || (i4TestValIsisCircLevelLSPThrottle < ISIS_LL_CKTL_MIN_LSP_THROTTLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_LSP);
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircLevelLSPThrottle %d\n",
                 i4TestValIsisCircLevelLSPThrottle));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelLSPThrottle () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelMinLSPRetransInt
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelMinLSPRetransInt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelMinLSPRetransInt (UINT4 *pu4ErrorCode,
                                        INT4 i4IsisSysInstance,
                                        INT4 i4IsisCircIndex,
                                        INT4 i4IsisCircLevelIndex,
                                        INT4
                                        i4TestValIsisCircLevelMinLSPRetransInt)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelMinLSPRetransInt () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelMinLSPRetransInt () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }

    if ((i4TestValIsisCircLevelMinLSPRetransInt >
         ISIS_LL_CKTL_MAX_MINLSP_RETRANS_INT)
        || (i4TestValIsisCircLevelMinLSPRetransInt <
            ISIS_LL_CKTL_MIN_MINLSP_RETRANS_INT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INV_LSP);
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircLevelMinLSPRetransInt %d\n",
                 i4TestValIsisCircLevelMinLSPRetransInt));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelMinLSPRetransInt () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelCSNPInterval
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelCSNPInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelCSNPInterval (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                    INT4 i4IsisCircIndex,
                                    INT4 i4IsisCircLevelIndex,
                                    INT4 i4TestValIsisCircLevelCSNPInterval)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelCSNPInterval () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelCSNPInterval () \n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1ErrCode;
    }

    if ((IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext) ==
         ISIS_SUCCESS)
        && (IsisAdjGetCktRec (pContext, (UINT4) i4IsisCircIndex, &pCktEntry) ==
            ISIS_SUCCESS))
    {
        if (pCktEntry->u1CktType == ISIS_P2P_CKT)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1ErrCode = SNMP_FAILURE;
            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting nmhTestv2IsisCircLevelCSNPInterval () \n"));
            CLI_SET_ERR (CLI_ISIS_INVALID_FOR_P2P);
            return i1ErrCode;
        }
    }

    if ((i4TestValIsisCircLevelCSNPInterval > ISIS_LL_CKTL_MAX_CSNP_INT)
        || (i4TestValIsisCircLevelCSNPInterval < ISIS_LL_CKTL_MIN_CSNP_INT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircLevelCSNPInterval %d\n",
                 i4TestValIsisCircLevelCSNPInterval));
        CLI_SET_ERR (CLI_ISIS_INV_CSNP_INT);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelCSNPInterval () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisCircLevelPartSNPInterval
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisCircLevelIndex

                The Object 
                testValIsisCircLevelPartSNPInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisCircLevelPartSNPInterval (UINT4 *pu4ErrorCode,
                                       INT4 i4IsisSysInstance,
                                       INT4 i4IsisCircIndex,
                                       INT4 i4IsisCircLevelIndex,
                                       INT4
                                       i4TestValIsisCircLevelPartSNPInterval)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhTestv2IsisCircLevelPartSNPInterval () \n"));

    i1ErrCode =
        IsisNmhValCircLevelTable (pu4ErrorCode, i4IsisSysInstance,
                                  i4IsisCircIndex, i4IsisCircLevelIndex);

    if (i1ErrCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisCircLevelPartSNPInterval () \n"));
        return i1ErrCode;
    }

    if ((i4TestValIsisCircLevelPartSNPInterval > ISIS_LL_CKTL_MAX_PSNP_INT)
        || (i4TestValIsisCircLevelPartSNPInterval < ISIS_LL_CKTL_MIN_PSNP_INT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "CircLevelPartSNPInterval %d\n",
                 i4TestValIsisCircLevelPartSNPInterval));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisCircLevelPartSNPInterval () \n"));

    return i1ErrCode;

}

/* LOW LEVEL Routines for Table : IsisPacketCountTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisPacketCountTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisPacketCountLevel
                IsisPacketCountDirection
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisPacketCountTable (INT4 i4IsisSysInstance,
                                              INT4 i4IsisCircIndex,
                                              INT4 i4IsisPacketCountLevel,
                                              INT4 i4IsisPacketCountDirection)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisPacketCountLevel;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisPacketCountTable () \n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Instance Index. "
                 "The Index is %d\n", i4IsisSysInstance));
    }

    else if ((i4IsisCircIndex <= 0) || (i4IsisCircIndex > (INT4) ISIS_MAX_CKTS))
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Index Level."));
    }

    else if ((u1CktLvlIdx != ISIS_LEVEL1) && (u1CktLvlIdx != ISIS_LEVEL2))
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of PacketCountTable"
                 "Level Index \n"));
    }
    else if ((i4IsisPacketCountDirection != ISIS_LL_SENDING)
             && (i4IsisPacketCountDirection != ISIS_LL_RECEIVING))
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of PacketCountTable"
                 "Direction \n"));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisPacketCountTable () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisPacketCountTable
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisPacketCountLevel
                IsisPacketCountDirection
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisPacketCountTable (INT4 *pi4IsisSysInstance,
                                      INT4 *pi4IsisCircIndex,
                                      INT4 *pi4IsisPacketCountLevel,
                                      INT4 *pi4IsisPacketCountDirection)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexIsisPacketCountTable () \n"));

    i1ErrCode = nmhGetNextIndexIsisPacketCountTable (0, pi4IsisSysInstance,
                                                     0, pi4IsisCircIndex, 0,
                                                     pi4IsisPacketCountLevel,
                                                     0,
                                                     pi4IsisPacketCountDirection);

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexIsisPacketCountTable () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisPacketCountTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisCircIndex
                nextIsisCircIndex
                IsisPacketCountLevel
                nextIsisPacketCountLevel
                IsisPacketCountDirection
                nextIsisPacketCountDirection
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisPacketCountTable (INT4 i4IsisSysInstance,
                                     INT4 *pi4NextIsisSysInstance,
                                     INT4 i4IsisCircIndex,
                                     INT4 *pi4NextIsisCircIndex,
                                     INT4 i4IsisPacketCountLevel,
                                     INT4 *pi4NextIsisPacketCountLevel,
                                     INT4 i4IsisPacketCountDirection,
                                     INT4 *pi4NextIsisPacketCountDirection)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisPacketCountLevel;
    UINT1               u1PktCntDir = (UINT1) i4IsisPacketCountDirection;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexIsisPacketCountTable () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx,
                                           &pContext)) == ISIS_FAILURE)
    {
        if ((i4RetVal = nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                                        &u4InstIdx)) ==
            ISIS_FAILURE)
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next "
                     "Index with the given Indices \n"));
        }
        else
        {
            u4CktIdx = 0;
            u1CktLvlIdx = 0;
            i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);
            i1ErrCode = SNMP_SUCCESS;
        }

    }

    while ((pContext != NULL) && (i1ErrCode == SNMP_SUCCESS))
    {
        i4RetVal = nmhUtlGetNextPktCntTable (u4InstIdx, u4CktIdx, u1CktLvlIdx,
                                             u1PktCntDir, &u1PktCntDir);
        if (i4RetVal == ISIS_FAILURE)
        {
            i4RetVal = nmhUtlGetNextIndexIsisCircLevelTable (u4InstIdx,
                                                             u4CktIdx,
                                                             u1CktLvlIdx,
                                                             &u1CktLvlIdx);
            if (i4RetVal == ISIS_FAILURE)
            {
                i4RetVal = nmhUtlGetNextIndexIsisCircTable (u4InstIdx, u4CktIdx,
                                                            &u4CktIdx);
                if (i4RetVal == ISIS_FAILURE)
                {
                    i4RetVal = nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                                               &u4InstIdx);
                    if (i4RetVal == ISIS_FAILURE)
                    {
                        i1ErrCode = SNMP_FAILURE;

                        NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next "
                                 "Index with the given Indices \n"));
                    }
                    else
                    {
                        u4CktIdx = 0;
                        u1CktLvlIdx = 0;
                        i4RetValue =
                            IsisCtrlGetSysContext (u4InstIdx, &pContext);
                    }
                }
                else
                {
                    u1PktCntDir = 0;
                    u1CktLvlIdx = 0;
                }
            }
            else
            {
                u1PktCntDir = 0;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = (INT4) u4InstIdx;
            *pi4NextIsisCircIndex = (INT4) u4CktIdx;
            *pi4NextIsisPacketCountLevel = (INT4) u1CktLvlIdx;
            *pi4NextIsisPacketCountDirection = (INT4) u1PktCntDir;
            i1ErrCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexIsisPacketCountTable () \n"));

    UNUSED_PARAM (i4RetValue);
    return i1ErrCode;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisPacketCountHello
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisPacketCountLevel
                IsisPacketCountDirection

                The Object 
                retValIsisPacketCountHello
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisPacketCountHello (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                            INT4 i4IsisPacketCountLevel,
                            INT4 i4IsisPacketCountDirection,
                            UINT4 *pu4RetValIsisPacketCountHello)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisPacketCountLevel;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisPacketCountHello () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {

        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisPacketCountLevel));
            }
            if (pCktLvlEntry != NULL)
            {
                if (i4IsisPacketCountDirection == ISIS_LL_SENDING)
                {
                    *pu4RetValIsisPacketCountHello =
                        pCktLvlEntry->SentPktStats.u4HelloPDUs;
                }
                else if (i4IsisPacketCountDirection == ISIS_LL_RECEIVING)
                {
                    *pu4RetValIsisPacketCountHello =
                        pCktLvlEntry->RcvdPktStats.u4HelloPDUs;
                }
                else
                {
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Packet Count "
                             "Direction. The Index is %d\n",
                             i4IsisPacketCountDirection));
                }
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }

    }

    else
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisPacketCountHello () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisPacketCountLSP
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisPacketCountLevel
                IsisPacketCountDirection

                The Object 
                retValIsisPacketCountLSP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisPacketCountLSP (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                          INT4 i4IsisPacketCountLevel,
                          INT4 i4IsisPacketCountDirection,
                          UINT4 *pu4RetValIsisPacketCountLSP)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisPacketCountLevel;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisPacketCountLSP () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisPacketCountLevel));
            }
            if (pCktLvlEntry != NULL)
            {
                if (i4IsisPacketCountDirection == ISIS_LL_SENDING)
                {
                    *pu4RetValIsisPacketCountLSP =
                        pCktLvlEntry->SentPktStats.u4LinkStatePDUs;
                }
                else if (i4IsisPacketCountDirection == ISIS_LL_RECEIVING)
                {
                    *pu4RetValIsisPacketCountLSP =
                        pCktLvlEntry->RcvdPktStats.u4LinkStatePDUs;
                }
                else
                {
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Packet Count "
                             "Direction. The Index is %d\n",
                             i4IsisPacketCountDirection));
                }
            }
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
            i1ErrCode = SNMP_FAILURE;
        }

    }
    else
    {
        i1ErrCode = SNMP_FAILURE;

        NMP_PT ((ISIS_LGST, "NMP <E> :  System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisPacketCountLSP () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisPacketCountCSNP
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisPacketCountLevel
                IsisPacketCountDirection

                The Object 
                retValIsisPacketCountCSNP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisPacketCountCSNP (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                           INT4 i4IsisPacketCountLevel,
                           INT4 i4IsisPacketCountDirection,
                           UINT4 *pu4RetValIsisPacketCountCSNP)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisPacketCountLevel;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisPacketCountCSNP () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisPacketCountLevel));
            }
            if (pCktLvlEntry != NULL)
            {
                if (i4IsisPacketCountDirection == ISIS_LL_SENDING)
                {
                    *pu4RetValIsisPacketCountCSNP =
                        pCktLvlEntry->SentPktStats.u4CSNPDUs;
                }

                else if (i4IsisPacketCountDirection == ISIS_LL_RECEIVING)
                {
                    *pu4RetValIsisPacketCountCSNP =
                        pCktLvlEntry->RcvdPktStats.u4CSNPDUs;
                }
                else
                {
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Packet Count "
                             "Direction. The Index is %d\n",
                             i4IsisPacketCountDirection));
                }
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisPacketCountCSNP () \n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisPacketCountPSNP
 Input       :  The Indices
                IsisSysInstance
                IsisCircIndex
                IsisPacketCountLevel
                IsisPacketCountDirection

                The Object 
                retValIsisPacketCountPSNP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisPacketCountPSNP (INT4 i4IsisSysInstance, INT4 i4IsisCircIndex,
                           INT4 i4IsisPacketCountLevel,
                           INT4 i4IsisPacketCountDirection,
                           UINT4 *pu4RetValIsisPacketCountPSNP)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4CktIdx = (UINT4) i4IsisCircIndex;
    UINT1               u1CktLvlIdx = (UINT1) i4IsisPacketCountLevel;
    tIsisCktLevel      *pCktLvlEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisPacketCountPSNP () \n"));

    if ((i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext))
        == ISIS_SUCCESS)
    {
        if ((i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx,
                                          &pCktEntry)) == ISIS_SUCCESS)
        {
            if (u1CktLvlIdx == ISIS_LEVEL1)
            {
                pCktLvlEntry = pCktEntry->pL1CktInfo;
            }
            else if (u1CktLvlIdx == ISIS_LEVEL2)
            {
                pCktLvlEntry = pCktEntry->pL2CktInfo;
            }
            else
            {
                i1ErrCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Circuit Level Index. "
                         "The Index is %d\n", i4IsisPacketCountLevel));
            }
            if (pCktLvlEntry != NULL)
            {
                if (i4IsisPacketCountDirection == ISIS_LL_SENDING)
                {
                    *pu4RetValIsisPacketCountPSNP =
                        pCktLvlEntry->SentPktStats.u4PSNPDUs;
                }

                else if (i4IsisPacketCountDirection == ISIS_LL_RECEIVING)
                {
                    *pu4RetValIsisPacketCountPSNP =
                        pCktLvlEntry->RcvdPktStats.u4PSNPDUs;
                }
                else
                {
                    i1ErrCode = SNMP_FAILURE;

                    NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Packet Count "
                             "Direction. The Index is %d\n",
                             i4IsisPacketCountDirection));
                }
            }
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;

            NMP_PT ((ISIS_LGST, "NMP <E> : Circuit Level Entry does not "
                     "exist for the Index,  %d\n", u1CktLvlIdx));
        }
    }

    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not "
                 "exist for the Index,  %u\n", u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisPacketCountPSNP () \n"));

    return i1ErrCode;

}
