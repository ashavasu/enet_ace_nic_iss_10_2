#include "isincl.h"

#ifndef IP6_WANTED
INT4
NetIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6IfInfo);
    return IP6_FAILURE;
}

INT4
NetIpv6GetRoute (tNetIpv6RtInfoQueryMsg * pNetIpv6RtQuery,
                 tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    UNUSED_PARAM (pNetIpv6RtQuery);
    UNUSED_PARAM (pNetIpv6RtInfo);
    return IP6_FAILURE;
}

INT4
NetIpv6GetFirstIfAddr (UINT4 u4IfIndex, tNetIpv6AddrInfo * pNetIpv6AddrInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6AddrInfo);
    return IP6_FAILURE;
}

INT4
NetIpv6GetNextIfAddr (UINT4 u4IfIndex,
                      tNetIpv6AddrInfo * pNetIpv6AddrInfo,
                      tNetIpv6AddrInfo * pNetIpv6NextAddrInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6AddrInfo);
    UNUSED_PARAM (pNetIpv6NextAddrInfo);
    return IP6_FAILURE;
}

INT4
NetIpv6RegisterHigherLayerProtocolInCxt (UINT4 u4ContextId, UINT4 u4Proto,
                                         UINT4 u4Mask, VOID *pFp)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Proto);
    UNUSED_PARAM (u4Mask);
    UNUSED_PARAM (pFp);
    return IP6_SUCCESS;
}

INT4
NetIpv6DeRegisterHigherLayerProtocolInCxt (UINT4 u4ContextId, UINT4 u4Proto)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Proto);
    return IP6_FAILURE;
}

INT4
NetIpv6LeakRoute (UINT1 u1CmdType, tNetIpv6RtInfo * pNetIpv6RtInfo)
{
    UNUSED_PARAM (u1CmdType);
    UNUSED_PARAM (pNetIpv6RtInfo);
    return IP6_FAILURE;
}

INT4
NetIpv6IsOurAddress (tIp6Addr * pAddr, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (pAddr);
    UNUSED_PARAM (pu4IfIndex);
    return IP6_FAILURE;
}
#endif

#ifndef RRD6_WANTED
VOID
Rtm6GRStaleRtMark (tRtm6RegnId * pRegnId)
{
    UNUSED_PARAM (pRegnId);
    return;
}

VOID
Rtm6ProcessGRRouteCleanUp (tRtm6RegnId * pRegnId, INT1 i1GRCompFlag)
{

    UNUSED_PARAM (pRegnId);
    UNUSED_PARAM (i1GRCompFlag);
    return;
}

INT4
Rtm6IsForwPlanPreserved (tRtm6RegnId * pRegnId)
{
    UNUSED_PARAM (pRegnId);
    return IP6_FAILURE;
}

INT4
Rtm6Register (tRtm6RegnId * pRegnId,
              UINT1 u1BitMask,
              INT4 (*SendToApplication) (tRtm6RespInfo * pRespInfo,
                                         tRtm6MsgHdr * Rtm6Header))
{
    UNUSED_PARAM (pRegnId);
    UNUSED_PARAM (u1BitMask);
    UNUSED_PARAM (SendToApplication);
    return RTM6_SUCCESS;
}

INT4
Rtm6Deregister (tRtm6RegnId * pRegnId)
{
    UNUSED_PARAM (pRegnId);
    return RTM6_SUCCESS;
}

INT1
RpsEnqueuePktToRtm6 (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UNUSED_PARAM (pBuf);
    return RTM6_FAILURE;
}
#endif
