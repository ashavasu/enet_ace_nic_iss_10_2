/******************************************************************************
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isupddbs.c,v 1.12 2017/09/11 13:44:09 siva Exp $
 *
 *   Description: This file contains the database access routines for 
 *                Update Module. 
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/* Prototypes
 */

PRIVATE VOID        IsisUpdNormalize (tIsisSortedHashTable *, UINT1);

/******************************************************************************
 * Function Name : IsisUpdAddLSP ()
 * Description   : This function adds the New LSP either into Database or
 *                 Transmission Queue based on the argument 'u1Loc'.
 * Input (s)     : pContext  - Pointer to system context
 *                 pu1LSP    - Pointer to LSP
 *                 u1Loc     - Flag indicating the place of insertion
 *                             (Database or Transmission Queue)
 *                 u1Level   - Level of the LSP
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Return (s)    : ISIS_SUCESS, On Successful Addition of LSP
 *                 ISIS_FAILURE, Otherwise 
 *****************************************************************************/

PUBLIC INT4
IsisUpdAddLSP (tIsisSysContext * pContext, UINT1 *pu1LSP, UINT1 u1Loc,
               UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2RcvdRemLT = 0;
    tIsisLSPEntry      *pLSPRec = NULL;
    tIsisLSPTxEntry    *pLSPTxRec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddLSP ()\n"));

    /* LSP Information is held in LSP Entries. Allocate an LSP Entry and hold
     * the received LSP in the structure
     */

    pLSPRec = (tIsisLSPEntry *)
        ISIS_MEM_ALLOC (ISIS_BUF_LDBE, sizeof (tIsisLSPEntry));

    if (pLSPRec == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : LSP Entry Failed\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        i4RetVal = ISIS_FAILURE;
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_LDBE);
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddLSP ()\n"));
        return i4RetVal;
    }

    ISIS_EXTRACT_RLT_FROM_LSP (pu1LSP, u2RcvdRemLT);
    pLSPRec->pu1LSP = pu1LSP;
    pLSPRec->pNext = NULL;

    if (u1Loc == ISIS_LOC_TX)
    {
        /* Since the LSP is to be placed in the Transmission Queue, allocate a
         * LSP TxQ structure since all entries in the transmission queue are
         * held in such structures
         */

        pLSPTxRec = (tIsisLSPTxEntry *)
            ISIS_MEM_ALLOC (ISIS_BUF_LTXQ, sizeof (tIsisLSPTxEntry));

        if (pLSPTxRec == NULL)
        {
            PANIC ((ISIS_LGST,
                    ISIS_MEM_ALLOC_FAIL " : LSP TxQ Entry Failed\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            i4RetVal = ISIS_FAILURE;
            ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pLSPRec);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_LTXQ);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddLSP ()\n"));
            return i4RetVal;
        }

        MEMSET (pLSPTxRec, 0x0, sizeof (tIsisLSPTxEntry));
        pLSPTxRec->pLSPRec = pLSPRec;
        IsisUpdAddToTxQ (pContext, pLSPTxRec, u1Level);
        if (MEMCMP ((pu1LSP + ISIS_OFFSET_LSPID),
                    pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN) != 0)
        {

            /* Start the Remaining Life Time timer with the value extracted
             * from the received LSP 
             */

            IsisUpdStartLSPTimer (pContext, pLSPRec, ISIS_ECT_MAX_AGE,
                                  u2RcvdRemLT);
        }
    }
    else
    {
        /* The LSP is to be inserted into the Database
         */

        IsisUpdAddToDb (pContext, pLSPRec, u1Level);
        if (MEMCMP ((pu1LSP + ISIS_OFFSET_LSPID),
                    pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN) != 0)
        {

            /* Start the Remaining Life Time timer with the value extracted
             * from the received LSP 
             */

            IsisUpdStartLSPTimer (pContext, pLSPRec, ISIS_ECT_MAX_AGE,
                                  u2RcvdRemLT);
        }
    }

    ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_ADD, pLSPRec);

    /* Area Address updation must be handled for every valid LSP received since
     * current LSP may or may not include certain Area Addresses included in the
     * previous transmission. If any addresses are included in the current LSP,
     * then the Area Address table has to be updated afresh. If some of the
     * addresses included in the previous transmission are excluded now, we may
     * have to delete those addresses based on the usage count
     */

    IsisUpdUpdateAA (pContext, pu1LSP, ISIS_CMD_ADD);
    IsisUtlGetHostnmefromLSP (pContext, pu1LSP, ISIS_CMD_ADD);
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddLSP ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function Name : IsisUpdReplaceLSP ()
 * Description   : This function frees the existing LSP included in pLSPRec
 *                 (either in TxQ or Database) and updates it with the received
 *                 LSP
 * Input (s)     : pContext   - Pointer to the system context
 *                 pu1RcvdLSP - Pointer to the received LSP which replaces the
 *                              existing LSP
 *                 pLSPRec    - Pointer to the LSP Record whose LSP is to be
 *                              replaced by pu1RcvdLSP 
 *                 u1Loc      - Location of LSP Record
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 *****************************************************************************/

PUBLIC VOID
IsisUpdReplaceLSP (tIsisSysContext * pContext, UINT1 *pu1RcvdLSP,
                   tIsisLSPEntry * pLSPRec, UINT1 u1Loc)
{
    UINT2               u2RcvdRemLT = 0;
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdReplaceLSP ()\n"));

    if ((u1Loc == ISIS_LOC_TX_FIRST) || (u1Loc == ISIS_LOC_TX))
    {
        ISIS_DEL_LSP_TIMER_NODE (((tIsisLSPTxEntry *) pLSPRec)->pLSPRec);

        /* Start the Remaining Life Time timer with the value extracted
         * from the received LSP 
         */

        ISIS_EXTRACT_RLT_FROM_LSP (pu1RcvdLSP, u2RcvdRemLT);
        IsisUpdStartLSPTimer (pContext, ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec,
                              ISIS_ECT_MAX_AGE, u2RcvdRemLT);

        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE,
                           ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec);

        ISIS_DBG_PRINT_ID ((((tIsisLSPTxEntry *)
                             (pLSPRec))->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                           (UINT1) ISIS_LSPID_LEN,
                           "UPD <T> : Replacing LSP - LSPID\t",
                           ISIS_OCTET_STRING);

        ISIS_MEM_FREE (ISIS_BUF_LSPI,
                       (UINT1 *) ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec->
                       pu1LSP);

        ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec->pu1LSP = pu1RcvdLSP;

        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_ADD,
                           ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec);
        IsisUtlSetMTIdfromLSP (pContext, pu1RcvdLSP,
                               ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec);
        IsisUtlGetHostnmefromLSP (pContext, pu1RcvdLSP, ISIS_CMD_MODIFY);
    }
    else if ((u1Loc == ISIS_LOC_DB_FIRST) || (u1Loc == ISIS_LOC_DB))
    {
        ISIS_DEL_LSP_TIMER_NODE (pLSPRec);

        /* Start the Remaining Life Time timer with the value extracted
         * from the received LSP 
         */

        ISIS_EXTRACT_RLT_FROM_LSP (pu1RcvdLSP, u2RcvdRemLT);
        IsisUpdStartLSPTimer (pContext, pLSPRec, ISIS_ECT_MAX_AGE, u2RcvdRemLT);
        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE, pLSPRec);

        ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pLSPRec->pu1LSP);
        pLSPRec->pu1LSP = pu1RcvdLSP;
        ISIS_DBG_PRINT_ID (pLSPRec->pu1LSP, (UINT1) ISIS_LSPID_LEN,
                           "UPD <T> : Replacing LSP - LSPID\t",
                           ISIS_OCTET_STRING);

        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_ADD, pLSPRec);
        IsisUtlSetMTIdfromLSP (pContext, pu1RcvdLSP, pLSPRec);
        IsisUtlGetHostnmefromLSP (pContext, pu1RcvdLSP, ISIS_CMD_MODIFY);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdReplaceLSP ()\n"));
}

/******************************************************************************
 * Function Name : IsisUpdModifyLSP ()
 * Description   : This routine updates the Remaining Life Time and the 
 *                 Sequence Number of the LSP in the 'pLspRec' present either 
 *                 in the LSP DB or the LSP TxQ. The checksum is adjusted
 *                 based on the new sequence number extracted from the
 *                 received LSP.
 * Input (s)     : pContext     - Pointer to system context
 *                 pLSPRec      - Pointer to the LSP Rec
 *                 u2RemLT      - Remaining Life Time of the LSP that has to be
 *                                updated if non-zero
 *                 u4CurrSeqNo  - Sequence number of the LSP that has to be
 *                                updated if non-zero
 *                 u1Loc        - Flag indicating  the Location of the LSP 
 *                                (Database or Transmission Queue)
 * Output (s)     : None
 * Globals        : Not Referred or Modified
 * Returns        : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisUpdModifyLSP (tIsisSysContext * pContext, tIsisLSPEntry * pLSPRec,
                  UINT2 u2RemLT, UINT4 u4CurrSeqNo, UINT1 u1Loc)
{
    UINT1              *pu1Lsp = NULL;
    UINT2               u2AdjCS = 0;
    UINT2               u2PduLen = 0;
    tIsisLSPEntry      *pLSPEntry = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT1               au1LSPId[ISIS_LSPID_LEN];

#ifndef ISIS_FT_ENABLED
    UNUSED_PARAM (pContext);
#endif

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdModifyLSP ()\n"));

    if ((u1Loc == ISIS_LOC_TX_FIRST) || (u1Loc == ISIS_LOC_TX))
    {
        pLSPEntry = ((tIsisLSPTxEntry *) pLSPRec)->pLSPRec;
    }
    else if ((u1Loc == ISIS_LOC_DB_FIRST) || (u1Loc == ISIS_LOC_DB))
    {
        pLSPEntry = pLSPRec;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdModifyLSP ()\n"));
        return;
    }

    pu1Lsp = pLSPEntry->pu1LSP;

    ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1Lsp, u2PduLen);

    /* Update the Remaining Lifetime and the Seq Number along with the
     * checksum into the LSP
     */

    ISIS_SET_LSP_RLT (pu1Lsp, u2RemLT);
    ISIS_SET_LSP_SEQNO (pu1Lsp, u4CurrSeqNo);

    /* Calculate the adjusted checksum based on the new sequence number to be
     * updated
     */

    u2AdjCS = IsisUtlCalcChkSum ((pu1Lsp + ISIS_OFFSET_LSPID),
                                 (UINT2) (u2PduLen - ISIS_OFFSET_LSPID),
                                 (pu1Lsp + ISIS_OFFSET_CHKSUM));

    ISIS_EXTRACT_LSPID_FROM_LSP (pu1Lsp, au1LSPId);
    ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
    DETAIL_INFO (ISIS_UPD_MODULE, (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                   "UPD <T> : LSP ID [ %s ], Updating Adjusted CheckSum [ %u ], "
                                   "Updating RLT [ %u ], Updating Seq. No [ %u ]\n",
                                   acLSPId, u2AdjCS, u2RemLT, u4CurrSeqNo));

    IsisUtlSetMTIdfromLSP (pContext, pu1Lsp, pLSPEntry);
#ifdef ISIS_FT_ENABLED

    /* Send a Lock Step Update to the standby node
     */

    if (u2RemLT == 0)
    {
        /* Zero RLT LSPs need not be tranferred to Standby nodes since
         * these LSPs will anyway be removed from the database after ZERO
         * AGE timer expiry
         */

        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE, pLSPEntry);
    }
    else
    {
        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_MODIFY, pLSPEntry);
    }

#endif

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdModifyLSP ()\n"));
}

/*******************************************************************************
 * Function Name : IsisUpdAddToTxQ ()
 * Description   : This function adds the given LSP Entry to the LSP 
 *                 Transmission Queue.
 * Input (s)     : pContext  - Pointer to system context
 *                 pLSPTxRec - Pointer to the the LSP Record to be removed
 *                 u1Level   - Level of the LSP. Level1 and Level2 LSPs are held
 *                             in separate queues
 * Output (s)    : None
 * Globals       : Not referred or modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdAddToTxQ (tIsisSysContext * pContext, tIsisLSPTxEntry * pLSPTxRec,
                 UINT1 u1Level)
{
    INT4                i4Result = 0;
    INT4                i4Last = 0;
    tIsisLSPTxEntry    *pTrav = NULL;
    tIsisLSPTxEntry    *pLast = NULL;
    tIsisLSPTxEntry    *pPrev = NULL;
    tIsisLSPTxEntry    *pNextLSP = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisSortedHashTable *pHashTbl = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT1               au1LSPId[ISIS_LSPID_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddToTxQ ()\n"));

    if (pLSPTxRec->pLSPRec->pu1LSP != NULL)
    {
        ISIS_EXTRACT_LSPID_FROM_LSP (pLSPTxRec->pLSPRec->pu1LSP, au1LSPId);
        ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
        DETAIL_INFO (ISIS_UPD_MODULE, (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                       "UPD <T> : Adding An LSP To TxQ - LSP ID [ %s ]\n",
                                       acLSPId));
    }

    if (pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER)
    {
        return;
    }
    if (u1Level == ISIS_LEVEL1)
    {
        pHashTbl = &(pContext->SysL1Info.LspTxQ);
    }
    else
    {
        pHashTbl = &(pContext->SysL2Info.LspTxQ);
    }
    IsisUtlSetMTIdfromLSP (pContext, pLSPTxRec->pLSPRec->pu1LSP,
                           pLSPTxRec->pLSPRec);
    if (pHashTbl->pHashBkts == NULL)
    {
        /* First entry to be inserted into the table i.e. there are no hash
         * buckets allocated till this point. Allocate a new hash bucket and
         * insert the node into the hash table
         */

        pHashTbl->pHashBkts = (tIsisHashBucket *)
            ISIS_MEM_ALLOC (ISIS_BUF_MISC, sizeof (tIsisHashBucket));
        if (pHashTbl->pHashBkts == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Hash Bkts in TxQ\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddToTxQ ()\n"));
            return;
        }
        pHashTbl->pHashBkts->pNext = NULL;
        pHashTbl->pHashBkts->pFirstElem = NULL;
        pHashTbl->pHashBkts->pLastElem = NULL;
        pHashTbl->u2HashSize++;
    }

    /* Hash buckets are available. Travel the list of buckets, comparing the
     * LSPIDs of the first and last elementas, in each of the buckets, with the
     * LSPID of the LSP to be inserted till an appropriate bucket is found
     */

    pHashBkt = pHashTbl->pHashBkts;

    while (pHashBkt != NULL)
    {
        pTrav = (tIsisLSPTxEntry *) (pHashBkt->pFirstElem);
        pLast = (tIsisLSPTxEntry *) (pHashBkt->pLastElem);

        if (pTrav == NULL)
        {
            /* This is a new hash bucket just created. Insert the LSP into the
             * hash bucket directly at the head. Mark the inserted LSP as the
             * First and Last element since this is the only record present in
             * the bucket
             */

            pHashBkt->pFirstElem = pLSPTxRec;
            pHashBkt->pLastElem = pLSPTxRec;
            pLSPTxRec->pLSPRec->pNext = NULL;
            pHashTbl->u4TotalMbrCount++;
            pHashBkt->u2MbrCount++;
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddToTxQ ()\n"));
            return;
        }

        /* ALGO: Compare the LSPID of the Last element with the LSPID of the LSP
         *       to be inserted. If the Last Element's LSPID is greater than the
         *       new LSP's LSPID then the new LSP has to be inserted into the 
         *       current bucket somewhere before the last element. 
         *       If not, compare the new LSPs LSPID with the LSPID of the first
         *       element (if any) of the next hash bucket. If the new LSP's
         *       LSPID is less than the LSPID of the first element (if any) of
         *       the next hash bucket, then the new LSP fits between the Last
         *       LSP of the current bucket and the first LSP of the next bucket.
         *       We insert the new LSP as the last element of the current bucket
         *       and leave the job of normalising the member count to Split
         *       Bucket routine that is invoked at the end of this routine. This
         *       process continues till a bucket where the LSP fits is found
         */

        /* Compare the new LSPs LSPID with the Last element
         */

        i4Last = MEMCMP (((pLast->pLSPRec->pu1LSP) + ISIS_OFFSET_LSPID),
                         ((pLSPTxRec->pLSPRec->pu1LSP) +
                          ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);
        if (i4Last < 0)
        {
            /* Last LSP's LSPID in the current bucket is less than the new LSPs
             * LSPID
             */

            if (pLast->pLSPRec->pNext != NULL)
            {
                /* Hash bucket next to the current one has valid entries.
                 * Compare the LSPID of the new LSP with the very first element
                 * of the next hash bucket. 
                 */

                pNextLSP = (tIsisLSPTxEntry *) (pLast->pLSPRec->pNext);

                i4Result = MEMCMP ((pNextLSP->pLSPRec->pu1LSP
                                    + ISIS_OFFSET_LSPID),
                                   (pLSPTxRec->pLSPRec->pu1LSP
                                    + ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);
                if (i4Result < 0)
                {
                    /* LSPID of the LSP held in the first element of the next 
                     * hash bucket is less than the LSPID of the new LSP,
                     * proceed down the hash bucket list. So the new LSP cannot
                     * be either the last element of the current bucket or the
                     * first element of the next hash bucket
                     */

                    pHashBkt = pHashBkt->pNext;
                    continue;
                }
            }

            /* The Last LSP of the current hash bucket is the last LSP in the
             * entire hash table. Hence we can insert the new LSP as the last
             * entry of the current bucket since the new LSPs LSPID is greater
             * than the LSPID of the Last entry
             */

            pLSPTxRec->pLSPRec->pNext =
                ((tIsisLSPTxEntry *) pHashBkt->pLastElem)->pLSPRec->pNext;
            pLast->pLSPRec->pNext = (tIsisLSPEntry *) pLSPTxRec;

            /* The Last Element of the current hash bucket is updated to 
             * point to the new LSP Record
             */

            pHashBkt->pLastElem = pLSPTxRec;
            break;
        }
        else
        {
            /* The new LSPs LSPID is less than the Last LSPs LSPID in the
             * current hash bucket. Hence the new LSP should be placed somewhere
             * before the last element of current bucket as per the LSPID 
             * sorting order
             */

            /* pTrav points to the first element of the hash bucket */

            while (pTrav != (tIsisLSPTxEntry *) pLast->pLSPRec->pNext)
            {
                i4Result
                    = MEMCMP ((pTrav->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                              (pLSPTxRec->pLSPRec->pu1LSP +
                               ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);
                if (i4Result > 0)
                {
                    /* The LSPID of the LSP in the hash bucket is greater than
                     * the LSPID of the new LSP. 
                     */

                    if (pPrev == NULL)
                    {
                        /* New LSP fits the very first slot in the hash bucket
                         * chain. Hence modifying the head pointer to point to
                         * the new LSP
                         */

                        pLSPTxRec->pLSPRec->pNext = (tIsisLSPEntry *) pTrav;
                        pHashBkt->pFirstElem = pLSPTxRec;
                    }
                    else
                    {
                        /* Linking the new LSP record into the chain between
                         * pTrav and pPrev
                         */

                        pLSPTxRec->pLSPRec->pNext = (tIsisLSPEntry *) pTrav;
                        pPrev->pLSPRec->pNext = (tIsisLSPEntry *) pLSPTxRec;
                    }
                    break;
                }
                else
                {
                    /* Travel as long as the LSPID in the hash bucket records is
                     * less than the LSPID of the new LSP
                     */

                    pPrev = pTrav;
                    pTrav = (tIsisLSPTxEntry *) pTrav->pLSPRec->pNext;
                }
            }

            /* The new LSP would have definitely been inserted into the hash
             * bucket. Break at this point to update the statistics and try to
             * normalise the hash table
             */

            break;
        }
    }

    if (pHashBkt != NULL)
    {
        pHashTbl->u4TotalMbrCount++;
        pHashBkt->u2MbrCount++;
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddToTxQ ()\n"));
}

/*******************************************************************************
 * Function Name : IsisUpdAddToDb ()
 * Description   : This function adds the given LSP Entry to the LSP Database.
 * Input (s)     : pContext  - Pointer to system context
 *                 pLSPTxRec - Pointer to the the LSP Record to be added
 *                 u1Level   - Level of the LSP. Level1 and Level2 LSPs are held
 *                             in separate queues
 * Output (s)    : None
 * Globals       : Not referred or modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdAddToDb (tIsisSysContext * pContext, tIsisLSPEntry * pLSPRec,
                UINT1 u1Level)
{
    INT4                i4Result = 0;
    INT4                i4Last = 0;
    tIsisLSPEntry      *pTrav = NULL;
    tIsisLSPEntry      *pLast = NULL;
    tIsisLSPEntry      *pPrev = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisSortedHashTable *pHashTbl = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddToDb ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        pHashTbl = &(pContext->SysL1Info.LspDB);
    }
    else
    {
        pHashTbl = &(pContext->SysL2Info.LspDB);
    }

    IsisUtlSetMTIdfromLSP (pContext, pLSPRec->pu1LSP, pLSPRec);
    if (pHashTbl->pHashBkts == NULL)
    {
        /* First entry to be inserted into the table i.e. there are no hash
         * buckets allocated till this point. Allocate a new hash bucket and
         * insert the node into the hash table
         */

        pHashTbl->pHashBkts = (tIsisHashBucket *)
            ISIS_MEM_ALLOC (ISIS_BUF_MISC, sizeof (tIsisHashBucket));
        if (pHashTbl->pHashBkts == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Hash Bkts in TxQ\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddToDb ()\n"));
            return;
        }
        pHashTbl->pHashBkts->pNext = NULL;
        pHashBkt = pHashTbl->pHashBkts;
        pHashTbl->pHashBkts->pFirstElem = NULL;
        pHashTbl->pHashBkts->pLastElem = NULL;
        pHashTbl->u2HashSize++;
    }

    /* Hash buckets are available. Travel the list of buckets, comparing the
     * LSPIDs of the first and last elementas, in each of the buckets, with the
     * LSPID of the LSP to be inserted till an appropriate bucket is found
     */

    pHashBkt = pHashTbl->pHashBkts;

    while (pHashBkt != NULL)
    {
        pTrav = (tIsisLSPEntry *) (pHashBkt->pFirstElem);
        pLast = (tIsisLSPEntry *) (pHashBkt->pLastElem);

        if (pTrav == NULL)
        {
            /* This is a new hash bucket just created. Insert the LSP into the
             * hash bucket directly at the head. Mark the inserted LSP as the
             * First and Last element since this is the only record present in
             * the bucket
             */

            pHashBkt->pFirstElem = pLSPRec;
            pHashBkt->pLastElem = pLSPRec;
            pLSPRec->pNext = NULL;
            pHashTbl->u4TotalMbrCount++;
            pHashBkt->u2MbrCount++;

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddToDb ()\n"));
            return;
        }

        /* ALGO: Compare the LSPID of the Last element with the LSPID of the LSP
         *       to be inserted. If the Last Element's LSPID is greater than the
         *       new LSP's LSPID then the new LSP has to be inserted into the 
         *       current bucket somewhere before the last element. 
         *       If not, compare the new LSPs LSPID with the LSPID of the first
         *       element (if any) of the next hash bucket. If the new LSP's
         *       LSPID is less than the LSPID of the first element (if any) of
         *       the next hash bucket, then the new LSP fits between the Last
         *       LSP of the current bucket and the first LSP of the next bucket.
         *       We insert the new LSP as the last element of the current bucket
         *       and leave the job of normalising the member count to Split
         *       Bucket routine that is invoked at the end of this routine. This
         *       process continues till a bucket where the LSP fits is found
         */

        /* Compare the new LSPs LSPID with the Last element
         */

        i4Last = MEMCMP (((pLast->pu1LSP) + ISIS_OFFSET_LSPID),
                         ((pLSPRec->pu1LSP) + ISIS_OFFSET_LSPID),
                         ISIS_LSPID_LEN);
        if (i4Last < 0)
        {
            /* Last LSP's LSPID in the current bucket is less than the new LSPs
             * LSPID
             */

            if (pLast->pNext != NULL)
            {
                /* Hash bucket next to the current one has valid entries.
                 * Compare the LSPID of the new LSP with the very first element
                 * of the next hash bucket. 
                 */

                i4Result =
                    MEMCMP ((pLast->pNext->pu1LSP + ISIS_OFFSET_LSPID),
                            (pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                            ISIS_LSPID_LEN);
                if (i4Result < 0)
                {
                    /* LSPID of the LSP held in the first element of the next 
                     * hash bucket is less than the LSPID of the new LSP,
                     * proceed down the hash bucket list. So the new LSP cannot
                     * be either the last element of the current bucket or the
                     * first element of the next hash bucket
                     */
                    pHashBkt = pHashBkt->pNext;
                    continue;
                }
            }

            /* The Last LSP of the current hash bucket is the last LSP in the
             * entire hash table. Hence we can insert the new LSP as the last
             * entry of the current bucket since the new LSPs LSPID is greater
             * than the LSPID of the Last entry
             */

            pLSPRec->pNext = ((tIsisLSPEntry *) pHashBkt->pLastElem)->pNext;
            pLast->pNext = pLSPRec;

            /* The Last Element of the current hash bucket is updated to 
             * point to the new LSP Record
             */

            pHashBkt->pLastElem = pLSPRec;
            break;
        }

        else
        {
            /* The new LSPs LSPID is less than the Last LSPs LSPID in the
             * current hash bucket. Hence the new LSP should be placed somewhere
             * before the last element of current bucket as per the LSPID 
             * sorting order
             */

            /* pTrav points to the first element of the hash bucket */

            while (pTrav != pLast->pNext)
            {
                i4Result = MEMCMP ((pTrav->pu1LSP + ISIS_OFFSET_LSPID),
                                   (pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                                   ISIS_LSPID_LEN);
                if (i4Result > 0)
                {
                    /* The LSPID of the LSP in the hash bucket is greater than
                     * the LSPID of the new LSP. 
                     */

                    if (pPrev == NULL)
                    {
                        /* New LSP fits the very first slot in the hash bucket
                         * chain. Hence modifying the head pointer to point to
                         * the new LSP
                         */

                        pLSPRec->pNext = pTrav;
                        pHashBkt->pFirstElem = pLSPRec;
                    }
                    else
                    {
                        /* Linking the new LSP record into the chain between
                         * pTrav and pPrev
                         */

                        pLSPRec->pNext = pTrav;
                        pPrev->pNext = pLSPRec;
                    }

                    break;
                }
                else
                {
                    /* Travel as long as the LSPID in the hash bucket records is
                     * less than the LSPID of the new LSP
                     */

                    pPrev = pTrav;
                    pTrav = pTrav->pNext;
                }
            }

            /* The new LSP would have definitely been inserted into the hash
             * bucket. Break at this point to update the statistics and try to
             * normalise the hash table
             */

            break;
        }
    }
    if (pHashBkt != NULL)
    {
        pHashTbl->u4TotalMbrCount++;
        pHashBkt->u2MbrCount++;
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddToDb ()\n"));
}

/*******************************************************************************
 * Function    : IsisUpdDeleteLSP ()
 * Description : This routine Deletes the LSP specified by 'pu1LSPId' from the 
 *               database
 * Input(s)    : pContext  - Pointer to system context
 *               u1Level   - LSP Level
 *               pu1LSPId  - LSP Id of the LSP record to be deleted 
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdDeleteLSP (tIsisSysContext * pContext, UINT1 u1Level, UINT1 *pu1LSPId)
{
    UINT1               u1Loc = 0;
    tIsisLSPEntry      *pDelRec = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDeleteLSP ()\n"));

    pDelRec = IsisUpdGetLSP (pContext, pu1LSPId, u1Level, &pPrevRec, &pHashBkt,
                             &u1Loc);

    ISIS_FORM_STR (ISIS_LSPID_LEN, pu1LSPId, acLSPId);
    if (pDelRec != NULL)
    {
        UPP_PT ((ISIS_LGST, "UPD <T> : Deleting LSP [%s] Level [%s]\n", acLSPId,
                 ISIS_GET_LEVEL_STR (u1Level)));

        if ((u1Loc == ISIS_LOC_TX_FIRST) || (u1Loc == ISIS_LOC_TX))
        {
            /* Update Area Address
             */

            IsisUpdUpdateAA (pContext,
                             ((tIsisLSPTxEntry *) pDelRec)->pLSPRec->pu1LSP,
                             ISIS_CMD_DELETE);
            IsisUtlGetHostnmefromLSP (pContext,
                                      ((tIsisLSPTxEntry *) pDelRec)->pLSPRec->
                                      pu1LSP, ISIS_CMD_DELETE);
            /* Remove the LSP Record from the TxQ
             */

            IsisUpdRemLSPFromTxQ (pContext, (tIsisLSPTxEntry *) pPrevRec,
                                  (tIsisLSPTxEntry *) pDelRec, pHashBkt,
                                  u1Level, u1Loc);

            /* Send a Lock Step Update to the standby node
             */

            ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE,
                               ((tIsisLSPTxEntry *) pDelRec)->pLSPRec);

            /* Removing this node from the Timer List
             */

            ISIS_DEL_LSP_TIMER_NODE (((tIsisLSPTxEntry *) pDelRec)->pLSPRec);

            /* Freeing the LSP associated information
             */

            ISIS_MEM_FREE (ISIS_BUF_LSPI,
                           (UINT1 *) ((tIsisLSPTxEntry *) pDelRec)->pLSPRec->
                           pu1LSP);
            ISIS_MEM_FREE (ISIS_BUF_LDBE,
                           (UINT1 *) ((tIsisLSPTxEntry *) pDelRec)->pLSPRec);
            ISIS_MEM_FREE (ISIS_BUF_SRMF,
                           (UINT1 *) ((tIsisLSPTxEntry *) pDelRec)->pu1SRM);
            ISIS_MEM_FREE (ISIS_BUF_LTXQ,
                           (UINT1 *) ((tIsisLSPTxEntry *) pDelRec));
        }
        else if ((u1Loc == ISIS_LOC_DB) || (u1Loc == ISIS_LOC_DB_FIRST))
        {
            /* Update Area Address
             */

            IsisUpdUpdateAA (pContext, pDelRec->pu1LSP, ISIS_CMD_DELETE);

            /* Remove the LSP Record from the Database
             */
            IsisUtlGetHostnmefromLSP (pContext, pDelRec->pu1LSP,
                                      ISIS_CMD_DELETE);

            IsisUpdRemLSPFromDB (pContext, pPrevRec, pDelRec, pHashBkt,
                                 u1Level, u1Loc);

            /* Send a Lock Step Update to the standby node
             */

            ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE, pDelRec);

            /* Removing the Timer Links pointers for the LSP Record that 
             * has been removed from the LSP Database. The previous and 
             * next timer links of this LSP Record is updated.
             */

            ISIS_DEL_LSP_TIMER_NODE (pDelRec);
            ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pDelRec->pu1LSP);
            ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pDelRec);
        }
    }
    else
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : [Deleting LSP] Required LSP [ %s ] Not found in Tx/Db\n",
                 acLSPId));
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDeleteLSP ()\n"));
}

/******************************************************************************
 * Function Name : IsisUpdRemLSPFromDB ()
 * Description   : This function removes the LSP Entry from the LSP Database
 *                 and returns the node to the calling function for further
 *                 processing. It does not free the node that is removed from
 *                 the Database and the onus of freeing the removed node is on
 *                 the calling function
 * Input (s)     : pContext - Pointer to system context
 *                 pPrevRec - Pointer to the previous record
 *                 pDbRec   - Pointer to the the LSP Record to be removed
 *                 pHashBkt - Pointer to the Hash Bucket where the LSP
 *                            record is present
 *                 u1Level  - Level of the LSP
 *                 u1Loc    - Location of the LSP (Database or Transmission Q)
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisUpdRemLSPFromDB (tIsisSysContext * pContext, tIsisLSPEntry * pPrevRec,
                     tIsisLSPEntry * pDbRec, tIsisHashBucket * pHashBkt,
                     UINT1 u1Level, UINT1 u1Loc)
{
    UINT1               u1Flag = ISIS_LSP_EQUAL;
    tIsisHashBucket    *pPrevBkt = NULL;
    tIsisSortedHashTable *pHashTbl = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT1               au1LSPId[ISIS_LSPID_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdRemLSPFromDB ()\n"));

    /* ALGO: The LSP hash table is in fact not a hash table but a sorted list
     * indexed by a table based on LSPIDs. The Index table holds pointers to
     * first and last elements of the list. For e.g., if the table has 10
     * indices and there are 100 LSPs, 
     *
     * Table [0]->FirstElement = LSP One
     * Table [0]->LastElement  = LSP Ten
     *
     * Table [1]->FirstElement = LSP Eleven 
     * Table [1]->LastElement  = LSP Twenty
     *             .
     *             .
     *             .
     * Table [10]->FirstElement = LSP Ninety One
     * Table [10]->FirstElement = LSP Hundred
     *
     * As can be seen the above table depicts an ideal position and in the
     * normal cases there may not be the same number of LSPs in each bucket. To
     * delete an LSP first the appropriate Hash Bucket i.e. index into the table
     * is found. Then the list is traversed and the appropriate record removed
     * using normal linked list techniques. One important issue to remember here
     * is that all the LSPs, One to Hundred, are independently linked as a
     * singly linked list in a sorted order and hence removing an LSP means it
     * has to be de-linked from the complete chain. and also update FirstElement
     * or LastElement pointers in individual hash buckets. Refer to Design
     * Document for more details regarding the organisation of the sorted hash
     * table
     */

    if (u1Level == ISIS_LEVEL1)
    {
        pHashTbl = &(pContext->SysL1Info.LspDB);
    }
    else
    {
        pHashTbl = &(pContext->SysL2Info.LspDB);
    }

    /* If pHashBkt is NULL then traverse the entire Hash table to find out the 
     * previous record and the Bucket, to delink the LSP Record form the LSP 
     * Database
     */

    if (pHashBkt == NULL)
    {
        /* Eventhough we have the LSP record, to remove the LSP record from 
         * database, we need both the Hash bucket pointer and the previous node
         * pointer. Retrieve that
         */

        pDbRec = IsisUpdGetLSPFromDB (pContext,
                                      (pDbRec->pu1LSP + ISIS_OFFSET_LSPID),
                                      u1Level, &pPrevRec, &pHashBkt, &u1Loc,
                                      &u1Flag);

        if ((u1Flag != ISIS_LSP_EQUAL) || (pDbRec == NULL))
        {
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdRemLSPFromDB ()\n"));
            return;
        }
    }

    if (pDbRec->pu1LSP != NULL)
    {
        ISIS_EXTRACT_LSPID_FROM_LSP (pDbRec->pu1LSP, au1LSPId);
        ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
        DETAIL_INFO (ISIS_UPD_MODULE, (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                       "UPD <T> : LSP Not Found - Removing LSP From DB - LSP ID [ %s ]\n",
                                       acLSPId));
    }

    if (u1Loc == ISIS_LOC_DB_FIRST)
    {
        /* This is the first LSP in the hash table
         */

        pHashBkt->pFirstElem = pDbRec->pNext;
        pHashTbl->u4TotalMbrCount--;
        pHashBkt->u2MbrCount--;

        if (pHashBkt->u2MbrCount == 0)
        {
            /* There are no members left after removing the LSP. Hash Bucket is
             * NULL and hence delink the hash bucket from the chain of buckets
             */

            pHashTbl->pHashBkts = pHashBkt->pNext;
            pHashBkt->pFirstElem = NULL;
            pHashBkt->pLastElem = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pHashBkt);
            pHashTbl->u2HashSize--;
        }
    }
    else if ((u1Loc == ISIS_LOC_DB) && (pPrevRec != NULL))
    {
        /* The LSP being removed is not the first LSP. Check whether this LSP is
         * the first or last element in the bucket
         */

        if (((tIsisLSPEntry *) pHashBkt->pLastElem) == pDbRec)
        {

            /* Last element, and hence modify the last element pointer to point
             * to the pPrevRec
             */

            pHashBkt->pLastElem = pPrevRec;
        }
        if (((tIsisLSPEntry *) pHashBkt->pFirstElem) == pDbRec)
        {
            /* First element, and hence modify the first element pointer to 
             * point to the element which is next to the removed LSP
             */

            pHashBkt->pFirstElem = pDbRec->pNext;
        }

        /* Now delink the record from the complete chain since all the nodes in
         * the hash table are linked in a singly linked list
         */

        pPrevRec->pNext = pDbRec->pNext;
        pHashBkt->u2MbrCount--;

        if (pHashBkt->u2MbrCount == 0)
        {
            /* No more members in the hash bucket
             */

            pHashBkt->pFirstElem = NULL;
            pHashBkt->pLastElem = NULL;

            /* Travel through the Hash bucket chain and delink the hash bucket
             * itself
             */

            pPrevBkt = pHashTbl->pHashBkts;
            while (pPrevBkt->pNext != pHashBkt)
            {
                pPrevBkt = pPrevBkt->pNext;
            }
            pPrevBkt->pNext = pHashBkt->pNext;
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pHashBkt);
            pHashTbl->u2HashSize--;
        }
        pHashTbl->u4TotalMbrCount--;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdRemLSPFromDB ()\n"));
}

/******************************************************************************
 * Function Name : IsisUpdRemLSPFromTxQ ()
 * Description   : This function removes the LSP Entry from the LSP TxQ
 *                 and returns the node to the calling function for further
 *                 processing. It does not free the node that is removed from
 *                 the TxQ and the onus of freeing the removed node is on
 *                 the calling function
 * Input (s)     : pContext   - Pointer to system context
 *                 pPrevTxRec - Pointer to the previous record
 *                 pTxRec     - Pointer to the the LSP Record to be removed
 *                 pHashBkt   - Pointer to the Hash Bucket where the LSP
 *                            record is present
 *                 u1Level    - Level of the LSP
 *                 u1Loc      - Location of the LSP (Database or Transmission Q)
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Return (s)    : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisUpdRemLSPFromTxQ (tIsisSysContext * pContext, tIsisLSPTxEntry * pPrevTxRec,
                      tIsisLSPTxEntry * pTxRec, tIsisHashBucket * pHashBkt,
                      UINT1 u1Level, UINT1 u1Loc)
{
    UINT1               u1Flag = ISIS_LSP_NOT_EQUAL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisHashBucket    *pPrevBkt = NULL;
    tIsisSortedHashTable *pHashTbl = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT1               au1LSPId[ISIS_LSPID_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdRemLSPFromTxQ ()\n"));

    /* ALGO: The LSP hash table is in fact not a hash table but a sorted list
     * indexed by a table based on LSPIDs. The Index table holds pointers to
     * first and last elements of the list. For e.g., if the table has 10
     * indices and there are 100 LSPs, 
     *
     * Table [0]->FirstElement = LSP One
     * Table [0]->LastElement  = LSP Ten
     *
     * Table [1]->FirstElement = LSP Eleven 
     * Table [1]->LastElement  = LSP Twenty
     *             .
     *             .
     *             .
     * Table [10]->FirstElement = LSP Ninety One
     * Table [10]->FirstElement = LSP Hundred
     *
     * As can be seen the above table depicts an ideal position and in the
     * normal cases there may not be the same number of LSPs in each bucket. To
     * delete an LSP first the appropriate Hash Bucket i.e. index into the table
     * is found. Then the list is traversed and the appropriate record removed
     * using normal linked list techniques. One important issue to remember here
     * is that all the LSPs, One to Hundred, are independently linked as a
     * singly linked list in a sorted order and hence removing an LSP means it
     * has to be de-linked from the complete chain. and also update FirstElement
     * or LastElement pointers in individual hash buckets. Refer to Design
     * Document for more details regarding the organisation of the sorted hash
     * table
     */

    if (u1Level == ISIS_LEVEL1)
    {
        pHashTbl = &(pContext->SysL1Info.LspTxQ);

        /* Update the LSP Transmission mark in the Circuit Level so that
         * in the next transmission interval the Next LSP should be transmitted
         * as this LSP is moving out of transmission queue
         */

        pCktRec = pContext->CktTable.pCktRec;
        while (pCktRec != NULL)
        {
            if ((pCktRec->pL1CktInfo != NULL)
                && (pCktRec->pL1CktInfo->pMarkTxLSP == pTxRec))
            {
                pCktRec->pL1CktInfo->pMarkTxLSP =
                    (tIsisLSPTxEntry *) pTxRec->pLSPRec->pNext;
            }
            pCktRec = pCktRec->pNext;
        }
    }
    else
    {
        pHashTbl = &(pContext->SysL2Info.LspTxQ);
        pCktRec = pContext->CktTable.pCktRec;
        while (pCktRec != NULL)
        {
            if ((pCktRec->pL2CktInfo != NULL)
                && (pCktRec->pL2CktInfo->pMarkTxLSP == pTxRec))
            {
                pCktRec->pL2CktInfo->pMarkTxLSP =
                    (tIsisLSPTxEntry *) pTxRec->pLSPRec->pNext;
            }
            pCktRec = pCktRec->pNext;
        }
    }

    /* If pHashBkt is NULL then traverse the entire Hash table to find out the 
     * previous record and the Bucket, to delink the LSP Record form the LSP 
     * Database
     */

    if (pHashBkt == NULL)
    {
        /* Eventhough we have the LSP record, to remove the LSP record from 
         * database, we need both the Hash bucket pointer and the previous node
         * pointer. Retrieve that
         */

        pTxRec = IsisUpdGetLSPFromTxQ (pContext,
                                       (pTxRec->pLSPRec->pu1LSP +
                                        ISIS_OFFSET_LSPID), u1Level,
                                       &pPrevTxRec, &pHashBkt, &u1Loc, &u1Flag);

        if ((u1Flag != ISIS_LSP_EQUAL) || (pTxRec == NULL))
        {
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdRemLSPFromTxQ ()\n"));
            return;
        }
    }

    if (pTxRec->pLSPRec->pu1LSP != NULL)
    {
        ISIS_EXTRACT_LSPID_FROM_LSP (pTxRec->pLSPRec->pu1LSP, au1LSPId);
        ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
        DETAIL_INFO (ISIS_UPD_MODULE, (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                       "UPD <T> : [Removing LSP From TxQ] LSP Not Found in TxQ - LSP ID [ %s ]\n",
                                       acLSPId));
    }

    if (u1Loc == ISIS_LOC_TX_FIRST)
    {
        /* This is the first LSP in the hash table
         */

        pHashBkt->pFirstElem = pTxRec->pLSPRec->pNext;
        pTxRec->pLSPRec->pNext = NULL;
        pHashTbl->u4TotalMbrCount--;
        pHashBkt->u2MbrCount--;
        if (pHashBkt->u2MbrCount == 0)
        {
            /* There are no members left after removing the LSP. Hash Bucket is
             * NULL and hence delink the hash bucket from the chain of buckets
             */

            pHashTbl->pHashBkts = pHashBkt->pNext;
            pHashBkt->pNext = NULL;
            pHashBkt->pFirstElem = NULL;
            pHashBkt->pLastElem = NULL;
            pHashTbl->u2HashSize--;
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pHashBkt);
        }
    }
    else if ((u1Loc == ISIS_LOC_TX) && (pPrevTxRec != NULL))
    {
        /* The LSP being removed is not the first LSP. Check whether this LSP is
         * the first or last element in the bucket
         */

        if (((tIsisLSPTxEntry *) pHashBkt->pLastElem)->pLSPRec
            == pTxRec->pLSPRec)
        {

            /* Last element, and hence modify the last element pointer to point
             * to the pPrevRec
             */

            pHashBkt->pLastElem = pPrevTxRec;
        }
        if (((tIsisLSPTxEntry *) pHashBkt->pFirstElem)->pLSPRec
            == pTxRec->pLSPRec)
        {
            /* First element, and hence modify the first element pointer to 
             * point to the element which is next to the removed LSP
             */

            pHashBkt->pFirstElem = pTxRec->pLSPRec->pNext;
        }

        /* Now delink the record from the complete chain since all the nodes in
         * the hash table are linked in a singly linked list
         */

        pPrevTxRec->pLSPRec->pNext = pTxRec->pLSPRec->pNext;
        pTxRec->pLSPRec->pNext = NULL;
        pHashBkt->u2MbrCount--;
        if (pHashBkt->u2MbrCount == 0)
        {
            /* No more members in the hash bucket
             */

            pHashBkt->pFirstElem = NULL;
            pHashBkt->pLastElem = NULL;

            /* Travel through the Hash bucket chain and delink the hash bucket
             * itself
             */

            if (pHashTbl->pHashBkts != NULL)
            {
                pPrevBkt = pHashTbl->pHashBkts;
                while (pPrevBkt->pNext != pHashBkt)
                {
                    pPrevBkt = pPrevBkt->pNext;
                }
                pPrevBkt->pNext = pHashBkt->pNext;
                pHashTbl->u2HashSize--;
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pHashBkt);
            }
        }
        pHashTbl->u4TotalMbrCount--;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdRemLSPFromTxQ ()\n"));
}

/*******************************************************************************
 * Function Name : IsisUpdGetLSP ()
 * Description   : This function returns the requested LSP record either from
 *                 the transmission queue or the Database. In this process it
 *                 also outputs, a LSP record which is previous to the requested
 *                 LSP record, pointer to the first entry in the hash bucket
 *                 where the reuested record was found, and the location (TxQ or
 *                 Database) where the requested LSP record was found. The
 *                 outputs are used by the calling routine for further
 *                 processing of the LSP.
 * Input (s)     : pContext - Pointer to system context
 *                 au1LSPId - LSP Id of the LSP Record to be fetched
 *                 u1Level  - Level of the LSP
 * Output (s)    : pPrevRec - Pointer to the Record which is previous to the
 *                            requested LSP
 *                 pHashBkt - Pointer to the Hash Bucket where the fetched LSP
 *                            record is present
 *                 pu1Loc   - Pointer to the Location of the LSP Record 
 *                            - Transmit Queue
 *                            - Database
 *                            - None
 * Globals       : Not Referred or Modified
 * Returns       : Pointer to the LSP record if found
 *                 NULL, otherwise
 ******************************************************************************/

PUBLIC tIsisLSPEntry *
IsisUpdGetLSP (tIsisSysContext * pContext, UINT1 *au1LSPId, UINT1 u1Level,
               tIsisLSPEntry ** pPrevRec, tIsisHashBucket ** pHashBkt,
               UINT1 *pu1Loc)
{
    UINT1               u1Flag = ISIS_LSP_NOT_EQUAL;
    tIsisLSPEntry      *pRetRec = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdGetLSP ()\n"));

    ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) (ISIS_LSPID_LEN + 1),
                       "UPD <T> : Trying To Retrieve LSP\t", ISIS_OCTET_STRING);

    /* The functions IsisUpdGetLSPFromTxQ () and IsisUpdGetLSPFromDB () returns
     * pointer to an LSP which is either greater than (lexicographically) or
     * equal to the requested LSP. This function returns the pointer to LSP only
     * if the returned LSP is equal to the requested one indicated by
     * ISIS_LSP_EQUAL in the 'u1Flag'.
     */

    pRetRec = (tIsisLSPEntry *)
        IsisUpdGetLSPFromTxQ (pContext, au1LSPId, u1Level,
                              (tIsisLSPTxEntry **) pPrevRec, pHashBkt,
                              pu1Loc, &u1Flag);
    if (u1Flag != ISIS_LSP_EQUAL)
    {
        /* Not found in Transmision Queue. Try to retrieve from Database
         */

        pRetRec = (tIsisLSPEntry *) IsisUpdGetLSPFromDB (pContext,
                                                         au1LSPId, u1Level,
                                                         pPrevRec, pHashBkt,
                                                         pu1Loc, &u1Flag);
        if (u1Flag != ISIS_LSP_EQUAL)
        {
            /* Not even found in the database 
             */
            if (au1LSPId != NULL)
            {
                ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
                DETAIL_INFO (ISIS_UPD_MODULE, (ISIS_LGST, ISIS_MAX_LOG_STR_SIZE,
                                               "UPD <T> : [LSP Record Requested] LSP Not Found in Database - LSP ID [ %s ]\n",
                                               acLSPId));
            }
            *pu1Loc = ISIS_LOC_NONE;
            pRetRec = NULL;
            *pPrevRec = NULL;
            *pHashBkt = NULL;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSP ()\n"));
    return pRetRec;
}

/******************************************************************************
 * Function Name : IsisUpdGetLSPFromDB ()
 * Description   : This function returns the requested LSP along with the
 *                 Previous record pointer, the hash bucket where the LSP is
 *                 located, Location as either NONE or DB based on whether the
 *                 LSP is missing or found, If the requested LSP is not found
 *                 then an LSP which is lexicographically greater than the
 *                 requested one and which is the minimum amongst the rest of
 *                 the LSPs is returned. In case of matching LSP the u1Flag is
 *                 set to ISIS_LSP_EQUAL and if not found the u1Flag is set to
 *                 either ISIS_LSP_NOT_EQUAL or ISIS_LSP_GREATER depending upon
 *                 whether a lexicographically greater LSP is found
 * Input (s)     : pContext - Pointer to system context
 *                 au1LSPId - Pointer to the LSP Id whose LSP Record has to be
 *                            fetched
 *                 u1Level  - Level of the LSP
 * Output (s)    : pPrevRec - Pointer to the Previous Record of the LSP Record
 *                            fetched
 *                 pHashBkt - Pointer to the Hash Bucket where the fetched LSP
 *                            record is present
 *                 pu1Loc   - Pointer to the Location of the LSP Record in the
 *                            database 
 *                 pu1Flag  - Flag indicating the result of the fetch 
 * Globals       : Not Referred or Modified
 * Return        : Pointer to an LSP record if possible
 *                 NULL, Otherwise
 *****************************************************************************/

PUBLIC tIsisLSPEntry *
IsisUpdGetLSPFromDB (tIsisSysContext * pContext, UINT1 *au1LSPId, UINT1 u1Level,
                     tIsisLSPEntry ** pPrevRec, tIsisHashBucket ** pHashBkt,
                     UINT1 *pu1Loc, UINT1 *pu1Flag)
{
    INT4                i4First = 0;
    INT4                i4Last = 0;
    INT4                i4Result = 0;
    tIsisLSPEntry      *pRetRec = NULL;
    tIsisLSPEntry      *pStartEntry = NULL;
    tIsisLSPEntry      *pLastEntry = NULL;
    tIsisSortedHashTable *pDbHashTbl = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdGetLSPFromDB ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        pDbHashTbl = &(pContext->SysL1Info.LspDB);
    }
    else
    {
        pDbHashTbl = &(pContext->SysL2Info.LspDB);
    }
    if (pDbHashTbl->pHashBkts == NULL)
    {
        /* No LSP records found in the given Hash Bucket
         */

        *pu1Loc = ISIS_LOC_NONE;
        *pu1Flag = ISIS_LSP_NOT_EQUAL;
        *pPrevRec = NULL;
        *pHashBkt = NULL;

        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSPFromDB ()\n"));
        return pRetRec;
    }
    *pHashBkt = pDbHashTbl->pHashBkts;

    for (; (*pHashBkt) != NULL; (*pHashBkt) = (*pHashBkt)->pNext)
    {
        /* Start comparing the given LSPID with the LSPIDs present in first and
         * last entry of records held in the hash buckets. If the given LSPID
         * falls between these two records, then continue searching through the
         * list of records held in the bucket till a matching record is found.
         */
        if ((*pHashBkt)->pFirstElem == NULL)
        {
            continue;
        }

        pStartEntry = (tIsisLSPEntry *) ((*pHashBkt)->pFirstElem);
        i4First =
            MEMCMP ((pStartEntry->pu1LSP + ISIS_OFFSET_LSPID),
                    au1LSPId, ISIS_LSPID_LEN);

        /* If the first element of any bucket has a LSP Id equal or greater to 
         * that of the given one then return that LSP Record. 
         */

        if (i4First == 0)
        {
            /* Found an exact match which is the very first entry of the list
             * held in the hash bucket. ISIS_LOC_DB_FIRST is returned in pu1Loc
             * only if the requested LSP is found as the very first entry of the
             * very first hash bucket in the hash table. An exact match is
             * indicated through pu1Flag
             */

            *pu1Loc = (*pHashBkt == pDbHashTbl->pHashBkts) ?
                ISIS_LOC_DB_FIRST : ISIS_LOC_DB;
            *pu1Flag = ISIS_LSP_EQUAL;

            /* In case we have moved down the hash bucket chain, pLastEntry
             * will be pointing to the Last Node in the previous bucket which
             * will be the required Previous Node.
             */

            *pPrevRec = pLastEntry;
            pRetRec = pStartEntry;

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSPFromDB ()\n"));
            return pRetRec;
        }

        else if (i4First > 0)
        {
            /* The very first LSPID in the Hash bucket is greater than the given
             * LSPID. Since LSPs are sorted based on LSPIDs then there is no
             * chance of finding an exact match. Return the greater LSP
             * indicating the same in pu1Flag
             */

            *pu1Loc = (*pHashBkt == pDbHashTbl->pHashBkts) ?
                ISIS_LOC_DB_FIRST : ISIS_LOC_DB;
            *pu1Flag = ISIS_LSP_GREATER;
            *pPrevRec = pLastEntry;
            pRetRec = pStartEntry;

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSPFromDB ()\n"));
            return pRetRec;
        }

        /* At this point it is clear that the first LSPID found in the hash
         * bucket is neither equal to nor greater than the requested LSPID. We
         * may have to verify whether the requested LSP is indeed in this list
         * by comparing against the Last entry also
         */

        pLastEntry = (tIsisLSPEntry *) ((*pHashBkt)->pLastElem);

        i4Last =
            MEMCMP ((pLastEntry->pu1LSP + ISIS_OFFSET_LSPID),
                    au1LSPId, ISIS_LSPID_LEN);

        if (i4Last < 0)
        {
            /* The Last Entry is less than the given LSPID, hence no point in
             * continuing in this list since both the start and last are less
             * than the requested one
             */

            continue;
        }
        else
        {
            /* The Last Entry has a greater LSP Id than that of the given 
             * LSP Id, hence the LSP MUST be in the current bucket
             */

            /* We have already finished processing the First Entry, hence
             * start from the Next Entry
             */

            *pPrevRec = pStartEntry;
            pStartEntry = pStartEntry->pNext;

            while (pStartEntry != pLastEntry->pNext)
            {
                i4Result = MEMCMP ((pStartEntry->pu1LSP +
                                    ISIS_OFFSET_LSPID),
                                   au1LSPId, ISIS_LSPID_LEN);
                if (i4Result > 0)
                {
                    /* Found an entry with LSPID greater than the given LSPID.
                     * Need not continue beyond this since LSPs are sorted on
                     * LSPIDs
                     */

                    pRetRec = pStartEntry;
                    *pu1Loc = ISIS_LOC_DB;
                    *pu1Flag = ISIS_LSP_GREATER;

                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdGetLSPFromDB ()\n"));
                    return pRetRec;
                }
                else if (i4Result == 0)
                {
                    /* Found an exact match. 
                     */

                    pRetRec = pStartEntry;
                    *pu1Loc = ISIS_LOC_DB;
                    *pu1Flag = ISIS_LSP_EQUAL;

                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdGetLSPFromDB ()\n"));
                    return pRetRec;
                }
                *pPrevRec = pStartEntry;
                pStartEntry = pStartEntry->pNext;
            }
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSPFromDB ()\n"));

    return pRetRec;
}

/*******************************************************************************
 * Function Name : IsisUpdGetLSPFromTxQ ()
 * Description   : This function returns the requested LSP along with the
 *                 Previous record pointer, the hash bucket where the LSP is
 *                 located, Location as either NONE or TXQ based on whether the
 *                 LSP is missing or found, If the requested LSP is not found
 *                 then an LSP which is lexicographically greater than the
 *                 requested one and which is the minimum amongst the rest of
 *                 the LSPs is returned. In case of matching LSP the u1Flag is
 *                 set to ISIS_LSP_EQUAL and if not found the u1Flag is set to
 *                 either ISIS_LSP_NOT_EQUAL or ISIS_LSP_GREATER depending upon
 *                 whether a lexicographically greater LSP is found
 * Input (s)     : pContext - Pointer to system context
 *                 au1LSPId - Pointer to the LSP Id whose LSP Record has to be
 *                            fetched
 *                 u1Level  - Level of the LSP
 * Output (s)    : pPrevRec - Pointer to the Previous Record of the LSP Record
 *                            fetched
 *                 pHashBkt - Pointer to the Hash Bucket where the fetched LSP
 *                            record is present
 *                 pu1Loc   - Pointer to the Location of the LSP Record in the
 *                            database
 *                 pu1Flag  - Flag indicating the result of the fetch 
 * Globals       : Not Referred or Modified
 * Return        : Pointer to an LSP record if possible
 *                 NULL, Otherwise
 ******************************************************************************/

PUBLIC tIsisLSPTxEntry *
IsisUpdGetLSPFromTxQ (tIsisSysContext * pContext, UINT1 *au1LSPId,
                      UINT1 u1Level, tIsisLSPTxEntry ** pPrevRec,
                      tIsisHashBucket ** pHashBkt, UINT1 *pu1Loc,
                      UINT1 *pu1Flag)
{
    INT4                i4First = 0;
    INT4                i4Last = 0;
    INT4                i4Result = 0;
    tIsisLSPTxEntry    *pRetRec = NULL;
    tIsisLSPTxEntry    *pStartEntry = NULL;
    tIsisLSPTxEntry    *pLastEntry = NULL;
    tIsisSortedHashTable *pTxHashTbl = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdGetLSPFromTxQ ()\n"));

    if (u1Level == ISIS_LEVEL1)
    {
        pTxHashTbl = &(pContext->SysL1Info.LspTxQ);
    }
    else
    {
        pTxHashTbl = &(pContext->SysL2Info.LspTxQ);
    }

    if (pTxHashTbl->pHashBkts == NULL)
    {
        /* No LSP records found in the given Hash Bucket
         */

        *pu1Loc = ISIS_LOC_NONE;
        *pu1Flag = ISIS_LSP_NOT_EQUAL;
        *pPrevRec = NULL;
        *pHashBkt = NULL;

        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSPFromTxQ ()\n"));
        return pRetRec;
    }

    *pHashBkt = pTxHashTbl->pHashBkts;

    for (; (*pHashBkt) != NULL; (*pHashBkt) = (*pHashBkt)->pNext)
    {
        /* Start comparing the given LSPID with the LSPIDs present in first and
         * last entry of records held in the hash buckets. If the given LSPID
         * falls between these two records, then continue searching through the
         * list of records held in the bucket till a matching record is found.
         */

        pStartEntry = (tIsisLSPTxEntry *) ((*pHashBkt)->pFirstElem);
        i4First =
            MEMCMP ((pStartEntry->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                    au1LSPId, ISIS_LSPID_LEN);

        /* If the first element of any bucket has a LSP Id equal or greater to 
         * that of the given one then return that LSP Record. 
         */

        if (i4First == 0)
        {
            /* Found an exact match which is the very first entry of the list
             * held in the hash bucket. ISIS_LOC_TX_FIRST is returned in pu1Loc
             * only if the requested LSP is found as the very first entry of the
             * very first hash bucket in the hash table. An exact match is
             * indicated through pu1Flag
             */

            *pu1Loc = (*pHashBkt == pTxHashTbl->pHashBkts) ?
                ISIS_LOC_TX_FIRST : ISIS_LOC_TX;
            *pu1Flag = ISIS_LSP_EQUAL;

            /* In case we have moved down the hash bucket chain, pLastEntry
             * will be pointing to the Last Node in the previous bucket which
             * will be the required Previous Node.
             */

            *pPrevRec = pLastEntry;
            pRetRec = pStartEntry;

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSPFromTxQ ()\n"));
            return pRetRec;
        }
        else if (i4First > 0)
        {
            /* The very first LSPID in the Hash bucket is greater than the given
             * LSPID. Since LSPs are sorted based on LSPIDs then there is no
             * chance of finding an exact match. Return the greater LSP
             * indicating the same in pu1Flag
             */

            *pu1Loc = (*pHashBkt == pTxHashTbl->pHashBkts) ?
                ISIS_LOC_TX_FIRST : ISIS_LOC_TX;
            *pu1Flag = ISIS_LSP_GREATER;
            *pPrevRec = pLastEntry;
            pRetRec = pStartEntry;

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSPFromTxQ ()\n"));
            return pRetRec;
        }

        /* At this point it is clear that the first LSPID found in the hash
         * bucket is neither equal to nor greater than the requested LSPID. We
         * may have to verify whether the requested LSP is indeed in this list
         * by comparing against the Last entry also
         */

        pLastEntry = (tIsisLSPTxEntry *) ((*pHashBkt)->pLastElem);

        i4Last =
            MEMCMP ((pLastEntry->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                    au1LSPId, ISIS_LSPID_LEN);
        if (i4Last < 0)
        {
            /* The Last Entry is less than the given LSPID, hence no point in
             * continuing in this list since both the start and last are less
             * than the requested one
             */

            continue;
        }
        else
        {
            /* The Last Entry has a greater LSP Id than that of the given 
             * LSP Id, hence the LSP MUST be in the current bucket
             */

            /* We have already finished processing the First Entry, hence
             * start from the Next Entry
             */

            *pPrevRec = pStartEntry;
            pStartEntry = (tIsisLSPTxEntry *) pStartEntry->pLSPRec->pNext;

            while (pStartEntry !=
                   (tIsisLSPTxEntry *) pLastEntry->pLSPRec->pNext)
            {
                i4Result = MEMCMP ((pStartEntry->pLSPRec->pu1LSP +
                                    ISIS_OFFSET_LSPID),
                                   au1LSPId, ISIS_LSPID_LEN);
                if (i4Result > 0)
                {
                    /* Found an entry with LSPID greater than the given LSPID.
                     * Need not continue beyond this since LSPs are sorted on
                     * LSPIDs
                     */

                    pRetRec = pStartEntry;
                    *pu1Loc = ISIS_LOC_TX;
                    *pu1Flag = ISIS_LSP_GREATER;

                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdGetLSPFromTxQ ()\n"));
                    return pRetRec;
                }
                else if (i4Result == 0)
                {
                    /* Found an exact match. 
                     */

                    pRetRec = pStartEntry;
                    *pu1Loc = ISIS_LOC_TX;
                    *pu1Flag = ISIS_LSP_EQUAL;

                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdGetLSPFromTxQ ()\n"));
                    return pRetRec;
                }
                *pPrevRec = pStartEntry;
                pStartEntry = (tIsisLSPTxEntry *) pStartEntry->pLSPRec->pNext;
            }
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetLSPFromTxQ ()\n"));
    return pRetRec;
}

/******************************************************************************
 * Function Name : IsisUpdSplitBucket ()
 * Description   : This function normalises the member count in the all the 
 *                 buckets. If the total member count in the bucket
 *                 exceeds limit, then this routine allocates a new bucket,
 *                 links it after the current hash bucket, and distributes the
 *                 existing elements across both the hash buckets equally.
 *                 For e.g., if the current bucket 'B' has 'N' elements, and the
 *                 limit is 'n', then the current routine will move 'N'/2  
 *                 elements to the new hash bucket 'B`'leaving 'N'/2 elements 
 *                 in the bucket 'B'.
 * Input (s)     : pContext  - Pointer to system context
 *                 pHashBkt  - Pointer to the Hash bucket to be splited
 *                 u1Loc     - Location of the LSP Record, either ISIS_LOC_DB 
 *                             or ISIS_LOC_TX
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisUpdSplitBucket (tIsisSysContext * pContext, tIsisSortedHashTable * pHashTbl,
                    UINT1 u1Loc)
{
    UINT2               u2MbrCnt = 1;
    tIsisLSPEntry      *pRec = NULL;
    tIsisLSPTxEntry    *pTxRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    tIsisHashBucket    *pNewHashBkt = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdSplitBucket ()\n"));

    pHashBkt = pHashTbl->pHashBkts;
    while (pHashBkt != NULL)
    {
        if (pHashBkt->u2MbrCount > pHashTbl->u2MaxMbrPerBucket)
        {
            /* Allocate a new hash bucket */

            pNewHashBkt = (tIsisHashBucket *)
                ISIS_MEM_ALLOC (ISIS_BUF_MISC, sizeof (tIsisHashBucket));

            if (pNewHashBkt == NULL)
            {
                /* We don't try to split the hash bucket now. Try again later
                 */

                IsisUtlFormResFailEvt (pContext, ISIS_BUF_MISC);
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdSplitBucket ()\n"));
                return;
            }

            /* Linking the new hash bucket after the current bucket 
             */

            pNewHashBkt->pNext = pHashBkt->pNext;
            pHashBkt->pNext = pNewHashBkt;
            pHashTbl->u2HashSize++;

            pNewHashBkt->u2MbrCount = (UINT2) (pHashBkt->u2MbrCount -
                                               pHashTbl->u2MaxMbrPerBucket);
            pHashBkt->u2MbrCount = pHashTbl->u2MaxMbrPerBucket;

            if (u1Loc == ISIS_LOC_DB)
            {
                pRec = (tIsisLSPEntry *) (pHashBkt->pFirstElem);
            }
            else if (u1Loc == ISIS_LOC_TX)
            {
                pTxRec = (tIsisLSPTxEntry *) (pHashBkt->pFirstElem);
            }

            u2MbrCnt = 1;

            while (u2MbrCnt != pHashBkt->u2MbrCount)
            {
                /* The original hash bucket chain has members more than that 
                 * it should hold. But the member count has been updated. 
                 * Hence travel through the list till 'u2MbrCnt' reaches 
                 * 'pHashBkt->u2MbrCount'. At this point the pRec pointer 
                 * points to the last element that should remain in the
                 * current hash bucket. Break at this point and move all the 
                 * elements after 'pRec' to the new Hash Bucket
                 */

                if (u1Loc == ISIS_LOC_DB)
                {
                    pRec = pRec->pNext;
                }
                else if (u1Loc == ISIS_LOC_TX)
                {
                    pTxRec = (tIsisLSPTxEntry *) pTxRec->pLSPRec->pNext;
                }
                u2MbrCnt++;
            }

            /* The Last element of the current bucket will now become the 
             * last element of the new hash bucket since the last few 
             * elements of the curent has bucket are moved to the new bucket
             */

            pNewHashBkt->pLastElem = pHashBkt->pLastElem;

            if (u1Loc == ISIS_LOC_DB)
            {
                /* As mentioned 'pRec' is now pointing to the last element of 
                 * the current hash bucket. Hence pRec->pNext will be the 
                 * very first element of the new hash bucket
                 */

                pNewHashBkt->pFirstElem = pRec->pNext;
                pHashBkt->pLastElem = pRec;
            }
            else if (u1Loc == ISIS_LOC_TX)
            {
                /* As mentioned 'pTxRec' is now pointing to the last element
                 * of the current hash bucket. Hence pTxRec->pNext will be 
                 * the very first element of the new hash bucket
                 */

                pNewHashBkt->pFirstElem = pTxRec->pLSPRec->pNext;
                pHashBkt->pLastElem = pTxRec;
            }
        }
        pHashBkt = pHashBkt->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdSplitBucket ()\n"));
}

/*******************************************************************************
 * Function      : IsisUpdProcSHNTimeOut ()
 * Description   : This function validates the total size of the hash table
 *                 (vertical size) and triggers normalization only if the size
 *                 exceeds ISIS_MAX_BUCKETS. If the system is an L12 system both
 *                 L1 and L2 Database and Transmit Queues are normalised 
 * Input (s)     : pContext  - Pointer to system context
 * Output (s)    : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdProcSHNTimeOut (tIsisSysContext * pContext)
{
    tIsisSortedHashTable *pLSPTxHash = NULL;
    tIsisSortedHashTable *pLSPDbHash = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdProcSHNormTimeOut ()\n"));

    /* if the gu1RelqFlag is set then don't process this timer just restart it
     * because the LSP Database should not be disturbed while i am still in the
     * process of computing the SPT
     */
    if (gu1RelqFlag != ISIS_DECN_WAIT)
    {
        if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
        {
            pLSPDbHash = &(pContext->SysL1Info.LspDB);
            pLSPTxHash = &(pContext->SysL1Info.LspTxQ);

            IsisUpdSplitBucket (pContext, pLSPTxHash, ISIS_LOC_TX);
            IsisUpdSplitBucket (pContext, pLSPDbHash, ISIS_LOC_DB);

            if (pLSPDbHash->u2HashSize > ISIS_MAX_SH_BKTS)
            {
                /* Normal size of a hash table is restricted to 0xFF. If the size
                 * exceeds the Max Buckets, we have to normalize the entire table
                 * and try to bring it back to the normal size
                 */

                IsisUpdNormalize (pLSPDbHash, ISIS_LOC_DB);
            }
            if (pLSPTxHash->u2HashSize > ISIS_MAX_SH_BKTS)
            {
                /* Normal size of a hash table is restricted to 0xFF. If the size
                 * exceeds the Max Buckets, we have to normalize the entire table
                 * and try to bring it back to the normal size
                 */

                IsisUpdNormalize (pLSPTxHash, ISIS_LOC_TX);
            }
        }

        if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
        {
            pLSPDbHash = &(pContext->SysL2Info.LspDB);
            pLSPTxHash = &(pContext->SysL2Info.LspTxQ);

            IsisUpdSplitBucket (pContext, pLSPTxHash, ISIS_LOC_TX);
            IsisUpdSplitBucket (pContext, pLSPDbHash, ISIS_LOC_DB);

            if (pLSPDbHash->u2HashSize > ISIS_MAX_SH_BKTS)
            {
                IsisUpdNormalize (pLSPDbHash, ISIS_LOC_DB);
            }
            if (pLSPTxHash->u2HashSize > ISIS_MAX_SH_BKTS)
            {
                IsisUpdNormalize (pLSPTxHash, ISIS_LOC_TX);
            }
        }
    }

    pContext->SysTimers.SysDbaseNormTimer.pContext = pContext;
    IsisTmrStartTimer (&(pContext->SysTimers.SysDbaseNormTimer),
                       ISIS_DBS_NORMN_TIMER, ISIS_DB_NORM_TIME);

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdProcSHNormTimeOut ()\n"));
}

/*******************************************************************************
 * Function      : IsisUpdNormalize ()
 * Description   : This function tries to reduce the size of the hash table. If
 *                 the number of Hash buckets is greater than ISIS_MAX_BUCKETS,
 *                 then it re-distributes the nodes in the hash table so that
 *                 only a minimum number of hash buckets remain. In this process
 *                 certain hash buckets will be removed, the Member Count, First
 *                 and Last elements of certain hash buckets get modified. The
 *                 result of the normalisation is that every hash bucket will
 *                 have ISIS_MAX_HASH_MBRS, except the last bucket which may
 *                 have lesser members.
 * Input (s)     : pHash       - Pointer to the Hash table that has to be 
 *                               normalised
 *                 u1LspDbName - Specifies whether Database or Transmit Queue
 * Output (s)    : None 
 * Globals       : Not Referred or Modified
 * Returns       : VOID 
 ******************************************************************************/

PRIVATE VOID
IsisUpdNormalize (tIsisSortedHashTable * pHash, UINT1 u1LspDbName)
{
    UINT2               u2ECnt = 0;
    UINT2               u2MemberCnt = 0;
    UINT2               u2MaxMbrCnt = 0;
    VOID               *pTravNode = NULL;
    VOID               *pPrevNode = NULL;
    tIsisHashBucket    *pDelBkt = NULL;
    tIsisHashBucket    *pTravBkt = NULL;
    tIsisHashBucket    *pPrevBkt = NULL;
    tIsisHashBucket    *pHashBkts = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdNormalize ()\n"));

    pHashBkts = pHash->pHashBkts;

    /* LOGIC: We will try to combine bucket by bucket till the combination of
     * buckets has ISIS_MAX_HASH_MBRS. Once the combination is found, a single
     * bucket in the combination is updated for Member Count, First element and
     * the Last element and the remaining freed. The same process continues on
     * the remaining hash buckets till all the hash buckets are exhausted
     */

    pPrevBkt = pHashBkts;
    pTravBkt = pHashBkts->pNext;

    /* Since we are starting from the second bucket, we count the number of
     * elements in the first bucket
     */

    u2MaxMbrCnt = pHash->u2MaxMbrPerBucket;
    u2MemberCnt = pPrevBkt->u2MbrCount;

    /* Scan through the entire hash bucket chain and try combining them if
     * possible
     */

    while (pTravBkt != NULL)
    {
        if (u2MemberCnt == u2MaxMbrCnt)
        {
            /* The Previous Bucket contains Maximum Number of Nodes, and Hence
             * cannot be adjusted, proceed to Next Bucket
             */

            u2MemberCnt = pTravBkt->u2MbrCount;
            pPrevBkt = pTravBkt;
            pTravBkt = pTravBkt->pNext;
        }
        else if ((u2MemberCnt + pTravBkt->u2MbrCount) > u2MaxMbrCnt)
        {
            /* We have reached a point where the member count in a combination
             * of nodes (at least 2) has exceeded the maximum limit prescribed
             * for a bucket. We adjust the count in the current bucket to
             * reflect the actual number that will remain after moving certain
             * nodes to the previous bucket
             */

            pTravBkt->u2MbrCount =
                (UINT2) (pTravBkt->u2MbrCount - (u2MaxMbrCnt - u2MemberCnt));

            /* Now that we have removed certain nodes from the current bucket
             * to the previous bucket we have to move the First Element pointer
             * in the current bucket to an appropriate position
             */

            pTravNode = pTravBkt->pFirstElem;
            pPrevNode = pTravNode;

            for (u2ECnt = 0; u2ECnt < (u2MaxMbrCnt - u2MemberCnt); u2ECnt++)
            {
                pPrevNode = pTravNode;
                pTravNode
                    = ((u1LspDbName == ISIS_LOC_DB)
                       ? (void *) ((tIsisLSPEntry *) (pTravNode))->pNext
                       : (void *) ((tIsisLSPTxEntry *) (pTravNode))->pLSPRec->
                       pNext);
            }

            pTravBkt->pFirstElem
                = ((u1LspDbName == ISIS_LOC_DB)
                   ? (void *) ((tIsisLSPEntry *) (pTravNode))
                   : (void *) ((tIsisLSPTxEntry *) (pTravNode)));

            /* The pPrevNode points to a node which is just before the First
             * Element of the current bucket and hence must be the Last Element
             * of the previous bucket
             */

            pPrevBkt->pLastElem
                = ((u1LspDbName == ISIS_LOC_DB)
                   ? (void *) ((tIsisLSPEntry *) (pPrevNode))
                   : (void *) ((tIsisLSPTxEntry *) (pPrevNode)));

            /* In the next iteration we will start from the current bucket and
             * hence we have to take the member count as the members of the
             * current bucket
             */

            u2MemberCnt = pTravBkt->u2MbrCount;

            /* Now that we have moved required number of elements to the
             * previous bucket, we can set the count as Maximum
             */

            pPrevBkt->u2MbrCount = u2MaxMbrCnt;

            /* Repeat the algorithm with the next set of nodes
             */

            pPrevBkt = pTravBkt;
            pTravBkt = pTravBkt->pNext;
        }
        else
        {
            /* The toal number of nodes is not greater than the maximum
             * prescribed limit. So we can move all the nodes from the current
             * bucket to the previous bucket and free the current node
             */

            pPrevBkt->u2MbrCount =
                (UINT2) (pPrevBkt->u2MbrCount + pTravBkt->u2MbrCount);

            /* Note down the total number of nodes combined till now
             */

            u2MemberCnt = (UINT2) (u2MemberCnt + pTravBkt->u2MbrCount);

            /* Since all the nodes have moved to the previous bucket, the last
             * element of the current bucket becomes the last element of the
             * previous bucket
             */

            pPrevBkt->pLastElem = pTravBkt->pLastElem;

            /* Initialise the current bucket and free it since it is no more
             * required
             */

            pTravBkt->u2MbrCount = 0;
            pTravBkt->pFirstElem = NULL;
            pTravBkt->pLastElem = NULL;

            pDelBkt = pTravBkt;
            pPrevBkt->pNext = pTravBkt->pNext;

            /* Previous bucket is still the same, we advance the pTravBkt alone
             */

            pTravBkt = pTravBkt->pNext;
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pDelBkt);
            pHash->u2HashSize--;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdNormalize ()\n"));
}

/******************************************************************************
 * Function Name : IsisUpdValLSPDataBase () 
 * Description   : This function scans both the LSP Database and the
 *                 Transmission queue and computes the Checksum for each of the
 *                 LSPs. If the checksum fails it purges the entire LSP Database
 * Input (s)     : pContext - Pointer to the system context
 *               : u1Level  - Level of the LSP Database to be verified. 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ****************************************************************************/

PUBLIC VOID
IsisUpdValLSPDataBase (tIsisSysContext * pContext, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1PrgFlag = ISIS_FALSE;
    UINT1               u1Loc = ISIS_LOC_TX;
    UINT2               u2RLT = 0;
    tIsisLSPTxEntry    *pTxRec = NULL;
    tIsisLSPEntry      *pDbRec = NULL;
    tIsisEvtCorrLsp    *pEventCorrLsp = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };
    UINT1               au1LSPId[ISIS_LSPID_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdValLSPDataBase ()\n"));

    pTxRec = ISIS_UPD_FIRST_LSP_TX_ENTRY (pContext, u1Level);
    pDbRec = ISIS_UPD_FIRST_LSP_DB_ENTRY (pContext, u1Level);

    while (pTxRec != NULL)
    {
        ISIS_EXTRACT_RLT_FROM_LSP ((pTxRec->pLSPRec->pu1LSP), u2RLT);

        /* Only LSPs with Non-Zero Remaining Life Time would be validated
         *
         * Refer Sec 7.3.18 a) of ISO 10589
         */
        ;
        if (u2RLT != 0)
        {
            i4RetVal = IsisUpdValidateCS (pContext, pTxRec->pLSPRec->pu1LSP);

            if (i4RetVal != ISIS_SUCCESS)
            {
                /* We can purge the complete Database
                 */

                u1PrgFlag = ISIS_TRUE;
                u1Loc = ISIS_LOC_TX;
                break;
            }
        }
        pTxRec = (tIsisLSPTxEntry *) (pTxRec->pLSPRec->pNext);
    }

    /* If 'u1PrgFlag' is ISIS_TRUE we need not worry about Database LSPs. We can
     * directly start purging all LSPs
     */

    if (u1PrgFlag != ISIS_TRUE)
    {
        /* All Transmission Queue LSPs are fine, verify the Database LSPs
         */

        while (pDbRec != NULL)
        {
            ISIS_EXTRACT_RLT_FROM_LSP ((pDbRec->pu1LSP), u2RLT);

            /* Only LSPs with Non-Zero Remaining Life Time would be validated
             *
             * Refer Sec 7.3.18 a) of ISO 10589
             */

            if (u2RLT != 0)
            {
                i4RetVal = IsisUpdValidateCS (pContext, pDbRec->pu1LSP);

                if (i4RetVal != ISIS_SUCCESS)
                {
                    /* We can purge the complete LSP database
                     */

                    u1PrgFlag = ISIS_TRUE;
                    u1Loc = ISIS_LOC_DB;
                    break;
                }
            }
            pDbRec = pDbRec->pNext;
        }
    }

    if (u1PrgFlag == ISIS_TRUE)
    {
        /* Post the Corrupted LSP Detected event
         */

        if ((pTxRec->pLSPRec->pu1LSP != NULL) && (u1Loc == ISIS_LOC_TX))
        {
            ISIS_EXTRACT_LSPID_FROM_LSP (pTxRec->pLSPRec->pu1LSP, au1LSPId);
            ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
        }

        else if ((pDbRec->pu1LSP != NULL) && (u1Loc == ISIS_LOC_DB))
        {
            ISIS_EXTRACT_LSPID_FROM_LSP (pDbRec->pu1LSP, au1LSPId);
            ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
        }

        WARNING ((ISIS_LGST,
                  "UPD <W> : [Checksum Error] Corrupted LSP in TxQ - [%s] Purging LSP\n",
                  acLSPId));

        if (u1Level == ISIS_LEVEL1)
        {
            pContext->SysL1Stats.u4SysCorrLSPs++;
        }
        else
        {
            pContext->SysL2Stats.u4SysCorrLSPs++;
        }
        pEventCorrLsp = (tIsisEvtCorrLsp *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCorrLsp));
        if (pEventCorrLsp != NULL)
        {
            pEventCorrLsp->u1EvtID = ISIS_EVT_CORR_LSP_DET;
            pEventCorrLsp->u1Level = u1Level;
            if (u1Loc == ISIS_LOC_TX)
            {
                MEMCPY (pEventCorrLsp->au1TrapLspID,
                        (pTxRec->pLSPRec->pu1LSP + ISIS_OFFSET_LSPID),
                        ISIS_LSPID_LEN);
            }
            else if (u1Loc == ISIS_LOC_DB)
            {
                MEMCPY (pEventCorrLsp->au1TrapLspID,
                        (pDbRec->pu1LSP + ISIS_OFFSET_LSPID), ISIS_LSPID_LEN);
            }

            IsisUtlSendEvent (pContext, (UINT1 *) pEventCorrLsp,
                              sizeof (tIsisEvtCorrLsp));
        }

        /* If Purge Flag is set then Purge the Entire LSP Database 
         *
         * Refer Sec. 7.3.18 of ISO 10589
         */

        IsisUpdPurgeLSPs (pContext, u1Level);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdValLSPDataBase ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdPurgeLSPs () 
 * Description : This routine purges the LSPs from LSP Database as well as
 *               LSP TxQ. The routine does not explicitly send any purges but
 *               just frees all the memory associated with the Database back to
 *               the free pools
 * Input (s)   : pContext - Pointer to the system context
 *             : u1Level  - Level of the LSP Database to be purged 
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Return (s)  : VOID
 ****************************************************************************/

PUBLIC VOID
IsisUpdPurgeLSPs (tIsisSysContext * pContext, UINT1 u1Level)
{
    tIsisLSPEntry      *pNextDbRec = NULL;
    tIsisLSPEntry      *pDbRec = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisLSPTxEntry    *pNextTxRec = NULL;
    tIsisLSPTxEntry    *pTxRec = NULL;
    tIsisHashBucket    *pTravBkt = NULL;
    tIsisHashBucket    *pNextBkt = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdPurgeLSPs ()\n"));

    /* Make all pMarkTxLSP in the Circuit Level Records NULL,
     * as entire LSP Database is being purged
     */
    UPP_PT ((ISIS_LGST, "UPD <T> : Purging [%s] LSPs from DB and TxQ\n",
             ISIS_GET_LEVEL_STR (u1Level)));
    pCktRec = pContext->CktTable.pCktRec;
    while (pCktRec != NULL)
    {
        if ((u1Level == ISIS_LEVEL1) && (pCktRec->pL1CktInfo != NULL))
        {
            pCktRec->pL1CktInfo->pMarkTxLSP = NULL;

            if ((pCktRec->u1CktType == ISIS_P2P_CKT)
                || (pCktRec->pL1CktInfo->bIsDIS != ISIS_TRUE))
            {
                IsisTmrStopECTimer (pContext, ISIS_ECT_L1_PSNP,
                                    pCktRec->u4CktIdx,
                                    pCktRec->pL1CktInfo->u1ECTId);
                pCktRec->pL1CktInfo->u1ECTId = 0;
            }
            else
            {
                IsisUpdStopCSNPTimer (pContext, pCktRec, u1Level);
            }
        }
        if ((u1Level == ISIS_LEVEL2) && (pCktRec->pL2CktInfo != NULL))
        {
            pCktRec->pL2CktInfo->pMarkTxLSP = NULL;

            if ((pCktRec->u1CktType == ISIS_P2P_CKT)
                || (pCktRec->pL2CktInfo->bIsDIS != ISIS_TRUE))
            {
                IsisTmrStopECTimer (pContext, ISIS_ECT_L2_PSNP,
                                    pCktRec->u4CktIdx,
                                    pCktRec->pL2CktInfo->u1ECTId);
                pCktRec->pL2CktInfo->u1ECTId = 0;
            }
            else
            {
                IsisUpdStopCSNPTimer (pContext, pCktRec, u1Level);
            }
        }
        pCktRec = pCktRec->pNext;
    }

    /* Delete LSPs from the Transmission Queue
     */

    if (u1Level == ISIS_LEVEL1)
    {
        /* Make the number of elments in the L1 Transmission Q to Zero
         */

        pTravBkt = pContext->SysL1Info.LspTxQ.pHashBkts;
        pContext->SysL1Info.LspTxQ.u2HashSize = 0;
        pContext->SysL1Info.LspTxQ.u4TotalMbrCount = 0;

        /* Since All LSPs and Hash Buckets are Removed, Mark the Hash Buckets
         * as NULL
         */

        pContext->SysL1Info.LspTxQ.pHashBkts = NULL;
    }
    else
    {
        /* Make the number of elments in the L2 Transmission Q to Zero
         */

        pTravBkt = pContext->SysL2Info.LspTxQ.pHashBkts;
        pContext->SysL2Info.LspTxQ.u2HashSize = 0;
        pContext->SysL2Info.LspTxQ.u4TotalMbrCount = 0;
        pContext->SysL2Info.LspTxQ.pHashBkts = NULL;
    }

    while (pTravBkt != NULL)
    {
        pTxRec = (tIsisLSPTxEntry *) (pTravBkt->pFirstElem);

        while (pTxRec != pTravBkt->pLastElem)
        {
            /* Remove all the elements in the TravBkt
             */

            pNextTxRec = (tIsisLSPTxEntry *) pTxRec->pLSPRec->pNext;

            /* Delink from the Timer chain 
             */

            ISIS_DEL_LSP_TIMER_NODE (pTxRec->pLSPRec);

            /* Free the pu1LSP in the LSPEntry
             */

            ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pTxRec->pLSPRec->pu1LSP);

            /* Free the pLSPRec in the LSPTxEntry
             */

            ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pTxRec->pLSPRec);

            /* Free SRM Flags
             */

            ISIS_MEM_FREE (ISIS_BUF_SRMF, (UINT1 *) pTxRec->pu1SRM);

            /* Free the pLSPTxRec 
             */

            ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pTxRec);
            pTxRec = pNextTxRec;
        }

        if (pTxRec != NULL)
        {
            if (pTxRec->pLSPRec != NULL)
            {
                ISIS_DEL_LSP_TIMER_NODE (pTxRec->pLSPRec);
                ISIS_MEM_FREE (ISIS_BUF_LSPI,
                               (UINT1 *) pTxRec->pLSPRec->pu1LSP);
                ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pTxRec->pLSPRec);
            }
            ISIS_MEM_FREE (ISIS_BUF_SRMF, (UINT1 *) pTxRec->pu1SRM);
            ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pTxRec);
        }

        pTravBkt->pLastElem = NULL;
        pTravBkt->pFirstElem = NULL;
        pNextBkt = pTravBkt->pNext;
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pTravBkt);
        pTravBkt = pNextBkt;
    }

    /* Delete all the LSPs from the LSP DB
     */

    if (u1Level == ISIS_LEVEL1)
    {
        /* Make the number of elments in the L1 DB to Zero
         */

        pTravBkt = pContext->SysL1Info.LspDB.pHashBkts;
        pContext->SysL1Info.LspDB.u2HashSize = 0;
        pContext->SysL1Info.LspDB.u4TotalMbrCount = 0;
        pContext->SysL1Info.LspDB.pHashBkts = NULL;
    }
    else
    {
        /* Make the number of elments in the L2 DB to Zero
         */

        pTravBkt = pContext->SysL2Info.LspDB.pHashBkts;
        pContext->SysL2Info.LspDB.u2HashSize = 0;
        pContext->SysL2Info.LspDB.u4TotalMbrCount = 0;
        pContext->SysL2Info.LspDB.pHashBkts = NULL;
    }

    while (pTravBkt != NULL)
    {
        pDbRec = (tIsisLSPEntry *) pTravBkt->pFirstElem;
        while (pDbRec != pTravBkt->pLastElem)
        {
            pNextDbRec = pDbRec->pNext;

            /* Delink from the Timer chain
             */

            ISIS_DEL_LSP_TIMER_NODE (pDbRec);

            /* Free the pu1LSP in the LSPEntry
             */

            ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pDbRec->pu1LSP);

            /* Free the Database Record
             */

            ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pDbRec);
            pDbRec = pNextDbRec;
        }
        if (pDbRec != NULL)
        {
            ISIS_DEL_LSP_TIMER_NODE (pDbRec);
            if (pDbRec->pu1LSP != NULL)
            {
                ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pDbRec->pu1LSP);
            }
            ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pDbRec);
        }

        pTravBkt->pLastElem = NULL;
        pTravBkt->pFirstElem = NULL;
        pNextBkt = pTravBkt->pNext;
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pTravBkt);
        pTravBkt = pNextBkt;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdPurgeLSPs ()\n"));
}

/******************************************************************************
 * Function Name : IsisUpdGetNextDbRec ()
 * Description   : This function retrieves an LSP record with LSPID which is
 *                 lexicographically greater than the given LSPID. Since LSPs
 *                 are sorted on LSPIDs, the procesing reduces to returning the
 *                 next record linked to the given record. This routine also 
 *                 returns the pointer to the hash bucket where the next record
 *                 is found. 
 * Input (s)     : pDbRec    - Pointer to the system Record whose next LSP
 *                             record is to be fetched
 * Output (s)    : pHashBkt  - Pointer to the Hash Bucket where the fetched LSP
 *                             record is present
 * Globals       : Not Referred or Modified
 * Returns       : Pointer to the retrieved LSP record, if available
 *                 NULL, otherwise
 *****************************************************************************/

PUBLIC tIsisLSPEntry *
IsisUpdGetNextDbRec (tIsisLSPEntry * pDbRec, tIsisHashBucket ** pHashBkt)
{
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdGetNextDbRec ()\n"));

    if (pDbRec == (tIsisLSPEntry *) ((*pHashBkt)->pLastElem))
    {
        /* The given record is the last element in the bucket. Hence advance the
         * Hash Bucket pointer down the list since the next record will be
         * available in the next hash bucket
         */

        *pHashBkt = (*pHashBkt)->pNext;
        if (*pHashBkt != NULL)
        {
            return ((tIsisLSPEntry *) ((*pHashBkt)->pFirstElem));
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetNextDbRec ()\n"));
    return (pDbRec->pNext);
}

/******************************************************************************
 * Function Name : IsisUpdGetNextTxRec ()
 * Description   : This function retrieves an LSP record with LSPID which is
 *                 lexicographically greater than the given LSPID. Since LSPs
 *                 are sorted on LSPIDs, the procesing reduces to returning the
 *                 next record linked to the given record. This routine also 
 *                 returns the pointer to the hash bucket where the next record
 *                 is found. 
 * Input (s)     : pTxRec    - Pointer to the system Record whose next LSP
 *                             record is to be fetched
 * Output (s)    : pHashBkt  - Pointer to the Hash Bucket where the fetched LSP
 *                             record is present
 * Globals       : Not Referred or Modified
 * Returns       : Pointer to the retrieved LSP record, if available
 *                 NULL, otherwise
 *****************************************************************************/

PUBLIC tIsisLSPTxEntry *
IsisUpdGetNextTxRec (tIsisLSPTxEntry * pTxRec, tIsisHashBucket ** pHashBkt)
{
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdGetNextTxRec ()\n"));

    if (pTxRec == (tIsisLSPTxEntry *) ((*pHashBkt)->pLastElem))
    {
        /* The given record is the last element in the bucket. Hence advance the
         * Hash Bucket pointer down the list since the next record will be
         * available in the next hash bucket
         */

        *pHashBkt = (*pHashBkt)->pNext;
        if (*pHashBkt != NULL)
        {
            return ((tIsisLSPTxEntry *) (*pHashBkt)->pFirstElem);
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdGetNextTxRec ()\n"));

    /* Return the next record in the chain */

    return ((tIsisLSPTxEntry *) pTxRec->pLSPRec->pNext);
}
