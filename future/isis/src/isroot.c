/********************************************************************
 * Copyright (C) Future Software Limited,  2001
 *
 * $Id: isroot.c,v 1.2 2009/06/03 06:39:03 premap-iss Exp $
 *
 * Description: This file contains the routines which will initialize
 *              environment for ISIS
 *******************************************************************/

#include "isincl.h"
#include "isextn.h"

#ifdef FUTURE_SNMP_WANTED
#include "include.h"
#include "fsisimdb.h"
#include "stdismdb.h"

INT4                RegisterISISwithFutureSNMP (void);
extern INT4 SNMP_AGT_RegisterMib ARG_LIST ((tSNMP_GroupOIDType *,
                                            tSNMP_BaseOIDType *,
                                            tSNMP_MIBObjectDescrType *,
                                            tSNMP_GLOBAL_STRUCT, INT4));
#endif
extern tOsixSemId   gIsisSemId;

/******************************************************************************
 *  Function Name    : RegisterISISwithFutureSNMP ()
 *  Description      : This function registers ISIS with the FutureSNMP
 *  Input (s)        : None
 *  Outputs (s)      : None
 *  Globals          : None referred or modified
 *  Returns          : ISIS_SUCCESS on succesful registration with FutureSNMP
 *                     ISIS_FAILURE
 ******************************************************************************/
INT4
RegisterISISwithFutureSNMP ()
{
#ifdef FUTURE_SNMP_WANTED
    if (SNMP_AGT_RegisterMib ((tSNMP_GroupOIDType *)
                              & fsisis_FMAS_GroupOIDTable,
                              (tSNMP_BaseOIDType *)
                              & fsisis_FMAS_BaseOIDTable,
                              (tSNMP_MIBObjectDescrType *)
                              & fsisis_FMAS_MIBObjectTable,
                              fsisis_FMAS_Global_data,
                              (INT4) fsisis_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return ISIS_FAILURE;
    }

    if (SNMP_AGT_RegisterMib ((tSNMP_GroupOIDType *)
                              & stdisis_FMAS_GroupOIDTable,
                              (tSNMP_BaseOIDType *)
                              & stdisis_FMAS_BaseOIDTable,
                              (tSNMP_MIBObjectDescrType *)
                              & stdisis_FMAS_MIBObjectTable,
                              stdisis_FMAS_Global_data,
                              (INT4) stdisis_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return ISIS_FAILURE;
    }
#endif

    return ISIS_SUCCESS;
}

/* -------------------------------------------------------------------+
 *  Function           : IsisProtocolLock
 *
 *  Input(s)           : None.
 *
 *  Output(s)          : None.
 *
 *  Returns            : None.
 *
 *  Action             : Takes a Protocol Sempaphore
 * +-------------------------------------------------------------------*/

INT4
IsisProtocolLock (VOID)
{
    if (OsixSemTake (gIsisSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------------+
 *  Function           : IsisProtocolUnlock
 *
 *  Input(s)           : None.
 *
 *  Output(s)          : None.
 *
 *  Returns            : None.
 *
 *  Action             : Releases a Protocol Sempaphore
 * +-------------------------------------------------------------------*/

INT4
IsisProtocolUnlock (VOID)
{

    if (OsixSemGive (gIsisSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
