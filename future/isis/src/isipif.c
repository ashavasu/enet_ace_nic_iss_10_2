/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isipif.c,v 1.36 2017/09/11 13:44:08 siva Exp $
 *
 * Description: This file contains the portable routines
 *              Routines.
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/* Private Function Prototypes*/
PRIVATE VOID        IsisRtmiProcRTMRegAck (tRtmMsgHdr *);
PRIVATE VOID        IsisRtmiProcRTM6RegAck (tRtm6MsgHdr *);

/*******************************************************************************
 * Function Name : IsisProcRtmPkt ()
 * Description   : This is the entry point of RTMI. This routine decodes the
 *                 message and processes it appropriately based on the message
 *                 type. 
 * Input (s)     : pIsisMsg     - Pointer to the Received RTM Message 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisProcRtmPkt (tIsisMsg * pIsisMsg)
{
    tRtmMsgHdr         *pRtmMsgHdr = NULL;

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisProcRtmPkt ()\n"));

    pRtmMsgHdr = (tRtmMsgHdr *) (VOID *) (pIsisMsg->pu1Msg);

    switch (pRtmMsgHdr->u1MessageType)
    {
        case RTM_REGISTRATION_ACK_MESSAGE:

            RTP_PT ((ISIS_LGST, "RTM <T> : Processing RTM Reg-Ack Message\n"));
            IsisRtmiProcRTMRegAck (pRtmMsgHdr);
            break;

        default:

            RTP_PT ((ISIS_LGST, "RTM <T> : Erroneous RTM Message Type [ %u ]\n",
                     pRtmMsgHdr->u1MessageType));
            break;
    }
    MEMSET (&gIsisRtmHdr, 0, sizeof (tRtmMsgHdr));
    pIsisMsg->pu1Msg = NULL;
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisProcRtmPkt ()\n"));
}

/*******************************************************************************
 * Function Name : IsisProcRtm6Pkt ()
 * Description   : This is the entry point of RTMI. This routine decodes the
 *                 message and processes it appropriately based on the message
 *                 type. 
 * Input (s)     : pIsisMsg     - Pointer to the Received RTM Message 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisProcRtm6Pkt (tIsisMsg * pIsisMsg)
{
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisProcRtm6Pkt ()\n"));

    pRtm6MsgHdr = (tRtm6MsgHdr *) (VOID *) (pIsisMsg->pu1Msg);

    switch (pRtm6MsgHdr->u1MessageType)
    {
        case RTM6_REGISTRATION_ACK_MESSAGE:

            RTP_PT ((ISIS_LGST, "RTM <T> : Processing RTM Reg-Ack Message\n"));
            IsisRtmiProcRTM6RegAck (pRtm6MsgHdr);
            break;

        default:

            RTP_PT ((ISIS_LGST, "RTM <T> : Erroneous RTM Message Type [ %u ]\n",
                     pRtm6MsgHdr->u1MessageType));
            break;
    }
    MEMSET (&gIsisRtm6Hdr, 0, sizeof (tRtm6MsgHdr));
    pIsisMsg->pu1Msg = NULL;
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisProcRtmPkt ()\n"));
}

/*****************************************************************************/
/* Function        : IsisRecvMsgFromRtm                                      */
/* Description     : This is a callback function that fills the RTM message  */
/*                   required tIsisQMsg and posts it in the ISIS INPUT Q     */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/

PUBLIC INT4
IsisRecvMsgFromRtm (tRtmRespInfo * pRespInfo, tRtmMsgHdr * pRtmHdr)
{
    INT4                i4ReturnStatus = ISIS_FAILURE;
    tIsisMsg            IsisMsg;
    tRtmRouteDataInfo  *pRtmRouteData = NULL;

    MEMSET ((UINT1 *) &IsisMsg, 0, sizeof (IsisMsg));

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRecvMsgFromRtm ()\n"));

    if ((pRtmHdr == NULL) || (pRespInfo == NULL))
    {
        RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRecvMsgFromRtm ()\n"));
        return RTM_FAILURE;
    }
    switch (pRtmHdr->u1MessageType)
    {
        case RTM_REGISTRATION_ACK_MESSAGE:
            i4ReturnStatus =
                IsisEnqueueMessage ((UINT1 *) pRtmHdr, sizeof (tRtmMsgHdr),
                                    gIsisRtmQId, ISIS_SYS_RT_DATA_EVT);
            break;
        case RTM_ROUTE_UPDATE_MESSAGE:
        case RTM_ROUTE_CHANGE_NOTIFY_MESSAGE:

            pRtmRouteData = (tRtmRouteDataInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                                  sizeof
                                                                  (tRtmRouteDataInfo));
            if (pRtmRouteData == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : RTM message\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                return RTM_FAILURE;
            }
            MEMCPY (&(pRtmRouteData->unRtInfo.RtInfo), pRespInfo->pRtInfo,
                    sizeof (tNetIpv4RtInfo));
            pRtmRouteData->u2ProtoId = pRtmHdr->RegnId.u2ProtoId;

            if (pRtmHdr->u1MessageType == RTM_ROUTE_UPDATE_MESSAGE)
            {
                IsisMsg.u1MsgType = ISIS_SYS_ROUTE_UPDATE;
            }
            else
            {
                IsisMsg.u1MsgType = ISIS_SYS_ROUTE_CHANGE;
            }
            IsisMsg.pu1Msg = (UINT1 *) pRtmRouteData;

            i4ReturnStatus =
                IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                    gIsisRtmQId, ISIS_SYS_ROUTE_DATA_EVT);

            if (i4ReturnStatus == ISIS_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, IsisMsg.pu1Msg);
                return RTM_FAILURE;
            }

            break;

        default:
            break;
    }
    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRecvMsgFromRtm ()\n"));
    if (i4ReturnStatus == ISIS_SUCCESS)
    {
        return RTM_SUCCESS;
    }
    else
    {
        return RTM_FAILURE;
    }
}

/*****************************************************************************/
/* Function        : IsisRecvMsgFromRtm6                                      */
/* Description     : This is a callback function that fills the RTM message  */
/*                   required tIsisQMsg and posts it in the ISIS INPUT Q     */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
PUBLIC INT4
IsisRecvMsgFromRtm6 (tRtm6RespInfo * pRespInfo, tRtm6MsgHdr * pRtm6Header)
{
    INT4                i4ReturnStatus = ISIS_FAILURE;
    tIsisMsg            IsisMsg;
    tRtmRouteDataInfo  *pRtmRouteData = NULL;

    MEMSET ((UINT1 *) &IsisMsg, 0, sizeof (tIsisMsg));

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRecvMsgFromRtm6 ()\n"));
    if ((pRtm6Header == NULL) || (pRespInfo == NULL))
    {
        return RTM6_FAILURE;
    }
    switch (pRtm6Header->u1MessageType)
    {
        case RTM6_REGISTRATION_ACK_MESSAGE:
            i4ReturnStatus =
                IsisEnqueueMessage ((UINT1 *) pRtm6Header, sizeof (tRtm6MsgHdr),
                                    gIsisRtmQId, ISIS_SYS_RT6_DATA_EVT);
            break;
        case RTM6_ROUTE_UPDATE_MESSAGE:
        case RTM6_ROUTE_CHANGE_NOTIFY_MESSAGE:

            pRtmRouteData = (tRtmRouteDataInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                                  sizeof
                                                                  (tRtmRouteDataInfo));

            if (pRtmRouteData == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : RTM message\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                return RTM6_FAILURE;
            }

            /* inside ISIS all protocols are enumerated as in RTM4 */
            IsisProtoIdFromRtm6 (&(pRtm6Header->RegnId.u2ProtoId));

            MEMCPY (&(pRtmRouteData->unRtInfo.Rt6Info), pRespInfo->pRtInfo,
                    sizeof (tNetIpv6RtInfo));
            pRtmRouteData->u2ProtoId = pRtm6Header->RegnId.u2ProtoId;

            if (pRtm6Header->u1MessageType == RTM6_ROUTE_UPDATE_MESSAGE)
            {
                IsisMsg.u1MsgType = ISIS_SYS_ROUTE6_UPDATE;
            }
            else
            {
                IsisMsg.u1MsgType = ISIS_SYS_ROUTE6_CHANGE;
            }
            IsisMsg.pu1Msg = (UINT1 *) pRtmRouteData;

            i4ReturnStatus =
                IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                    gIsisRtmQId, ISIS_SYS_ROUTE_DATA_EVT);

            if (i4ReturnStatus == ISIS_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MISC, IsisMsg.pu1Msg);
                return RTM6_FAILURE;
            }
            break;

        default:
            break;
    }
    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRecvMsgFromRtm6 ()\n"));
    if (i4ReturnStatus == ISIS_SUCCESS)
    {
        return RTM6_SUCCESS;
    }
    else
    {
        return RTM6_FAILURE;
    }
}

/*******************************************************************************
 * Function Name : IsisRtmiProcRTMRegAck ()
 * Description   : This routine processes the Registration Acknowledge message
 *                 recieved from the RTM. 
 * Input (s)     : pContext - Pointer to the System Context 
 *                 pMsg     - Pointer to the Registration Ack Message 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisRtmiProcRTMRegAck (tRtmMsgHdr * pRtmMsgHdr)
{
    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRtmiProcRTMRegAck ()\n"));

    /* Note down the Registration ID present in the Header of the
     * Acknowledgement Message as returned by the RTM.
     */

    if (pRtmMsgHdr != NULL)
    {
        RTP_PT ((ISIS_LGST, "RTM <T> : RTM Registration Id [ %u ]\n",
                 pRtmMsgHdr->RegnId.u2ProtoId));
        gIsisExtInfo.u2RtmRegID = pRtmMsgHdr->RegnId.u2ProtoId;
    }
    else
    {
        RTP_PT ((ISIS_LGST, "RTM <E> : Buffer Copy Failed\n"));
    }

    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRtmiProcRTMRegAck ()\n"));
}

/*******************************************************************************
 * Function Name : IsisRtmiProcRTM6RegAck ()
 * Description   : This routine processes the Registration Acknowledge message
 *                 recieved from the RTM. 
 * Input (s)     : pContext - Pointer to the System Context 
 *                 pMsg     - Pointer to the Registration Ack Message 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PRIVATE VOID
IsisRtmiProcRTM6RegAck (tRtm6MsgHdr * pRtm6MsgHdr)
{
    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRtmiProcRTM6RegAck ()\n"));

    /* Note down the Registration ID present in the Header of the
     * Acknowledgement Message as returned by the RTM.
     */

    if (pRtm6MsgHdr != NULL)
    {
        RTP_PT ((ISIS_LGST, "RTM <T> : RTM Registration Id [ %u ]\n",
                 pRtm6MsgHdr->RegnId.u2ProtoId));
        gIsisExtInfo.u2Rtm6RegID = pRtm6MsgHdr->RegnId.u2ProtoId;
    }
    else
    {
        RTP_PT ((ISIS_LGST, "RTM <E> : Buffer Copy Failed\n"));
    }

    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRtmiProcRTM6RegAck ()\n"));
}

/******************************************************************************
 * Function Name : IsisRegisterWithIP
 * Description   : This function registers the ISIS Module with the IP 
 * Input (s)     : None 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - On successful Registration of ISIS Module 
 *                                with IP
 *                 ISIS_FAILURE - Otherwise
 ******************************************************************************/
PUBLIC INT4
IsisRegisterWithIP (tIsisSysContext * pContext)
{
    tNetIpRegInfo       NetRegInfo;
    UINT1               u4Mask = 0;

    RTP_EE ((ISIS_LGST, "IP <X> : Entered IsisRegisterWithIP ()\n"));

    MEMSET ((UINT1 *) &NetRegInfo, 0, sizeof (tNetIpRegInfo));
    NetRegInfo.pIfStChng = IsisIfStateChgHdlr;
    NetRegInfo.u2InfoMask |= NETIPV4_IFCHG_REQ;
    NetRegInfo.u1ProtoId = ISIS_ID;
    NetRegInfo.u4ContextId = pContext->u4SysInstIdx;
    NetIpv4RegisterHigherLayerProtocol (&NetRegInfo);
    u4Mask = NETIPV6_INTERFACE_PARAMETER_CHANGE;
    NetIpv6RegisterHigherLayerProtocolInCxt (pContext->u4SysInstIdx, ISIS_ID,
                                             u4Mask,
                                             (void *) IsisV6IfStateChgHdlr);
    /* Register with IP6, for the Address Change Notification. */
    NetIpv6RegisterHigherLayerProtocolInCxt (pContext->u4SysInstIdx, ISIS_ID,
                                             NETIPV6_ADDRESS_CHANGE,
                                             (VOID *) IsisV6IfAddrChgHdlr);

    RTP_EE ((ISIS_LGST, "IP <X> : Exiting IsisRegisterWithIP ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisDeRegisterFromIP
 * Description   : This function de registers the ISIS Module from IP 
 * Input (s)     : None 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - On successful De-Registration of ISIS Module 
 *                                from IP
 *                 ISIS_FAILURE - Otherwise
 ******************************************************************************/
PUBLIC INT4
IsisDeRegisterFromIP (UINT4 u4SysInstIdx)
{
    RTP_EE ((ISIS_LGST, "IP <X> : Entered IsisDeRegisterFromIP ()\n"));
    if (NetIpv4DeRegisterHigherLayerProtocolInCxt
        (u4SysInstIdx, ISIS_ID) == NETIPV4_FAILURE)
    {
        RTP_EE ((ISIS_LGST,
                 "IP <X> : IP Deregistration failed. Exiting IsisDeRegisterFromIP ()\n"));

        return ISIS_FAILURE;
    }
    if (NetIpv6DeRegisterHigherLayerProtocolInCxt
        (u4SysInstIdx, ISIS_ID) == NETIPV6_FAILURE)
    {
        RTP_EE ((ISIS_LGST,
                 "IP <X> : IP Deregistration failed. Exiting IsisDeRegisterFromIP ()\n"));
        return ISIS_FAILURE;
    }

    RTP_EE ((ISIS_LGST, "IP <X> : Exiting IsisDeRegisterFromIP ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisIfStateChgHdlr ()
 * Description   : This procedure is called to inform the change in the
                   operational status of a logical interface and is
                   indicated by the IP module. The value could be
                   IFACE_DELETED or OPER_STATE. Depending on the current
                   status of the iface, the different actions are taken.
 * Input (s)     : pNetIfInfo, the logical interface.
                   u4BitMap, the event that happened.
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/
PUBLIC VOID
IsisIfStateChgHdlr (tNetIpv4IfInfo * pNetIfInfo, UINT4 u4BitMap)
{
    tIsisCktEntry      *pCktRec = NULL;
    tIsisLLInfo        *pLLInfo = NULL;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    tIsisMsg            IsisMsg;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4IfIndex = 0;
    UINT4               u4Addr = 0;
    UINT1               u1Status = ISIS_UP;
    UINT1               u1OperState = ISIS_STATE_ON;
    UINT4               u4NetMask = 0;
    INT1               *pi1InterfaceName = NULL;
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET ((UINT1 *) &NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));

    u4IfIndex = pNetIfInfo->u4CfaIfIndex;

    pi1InterfaceName = &ai1InterfaceName[0];

    if ((u4BitMap & IF_SECIP_CREATED) || (u4BitMap & IF_SECIP_DELETED))
    {
        u4IfIndex = pNetIfInfo->u4IfIndex;
    }

    if (ISIS_GET_CKT_HASH_SIZE () == 0)
    {
        /* Still no circuit records are configured in the ISIS Context so ignore
         * the Inidcation*/
        return;
    }

    /* Get the  Circuit record for the given ifindex */
    i4RetVal = IsisAdjGetCktRecWithIfIdx (u4IfIndex, 0, &pCktRec);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* Ignore the indication as no circuit is configured over the interface */
        return;
    }

    if (u4BitMap & IP_ADDR)
    {
        pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                  sizeof (tIsisLLInfo));
        if (pLLInfo == NULL)
        {
            return;
        }

        IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
        IsisMsg.u4Length = sizeof (tIsisLLInfo);
        pLLInfo->u1AddrType = ISIS_ADDR_IPV4;
        IsisMsg.u1MsgType = ISIS_MSG_IF_ADDR_CHG;
        IsisMsg.u1DataType = 0;
        IsisMsg.pMemFreeRoutine = NULL;

        if (pNetIfInfo->u4Addr != 0)
        {
            IsisMsg.u1LenOrStat = ISIS_IP_IF_ADDR_ADD;
        }
        else
        {
            IsisMsg.u1LenOrStat = ISIS_IP_IF_ADDR_DEL;
        }
        IsisMsg.u4CxtOrIfindex = pNetIfInfo->u4CfaIfIndex;
        i4RetVal = IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                       gIsisCtrlQId, ISIS_SYS_LL_DATA_EVT);
        if (i4RetVal == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "IP <X> : Enqueuing of Message Failed\n"));
        }

        return;
    }

    if (u4BitMap & IF_SECIP_DELETED)
    {
        u1OperState = ISIS_SEC_IP_REMOVED;
    }

    if (u4BitMap & IFACE_DELETED)
    {
        u1Status = ISIS_DOWN;
        u1OperState = ISIS_DOWN;
    }

    if (u4BitMap & OPER_STATE)
    {
        if (pNetIfInfo->u4Oper == IPIF_OPER_ENABLE)
        {
            u1OperState = ISIS_STATE_ON;
            CfaCliGetIfName (pCktRec->u4CktIfIdx, pi1InterfaceName);
            WARNING ((ISIS_LGST,
                      "[ISIS_STATE_ON] - Interface [%s] state changed to [UP]\n",
                      pi1InterfaceName));
        }
        else
        {
            u1OperState = ISIS_STATE_OFF;
            CfaCliGetIfName (pCktRec->u4CktIfIdx, pi1InterfaceName);
            WARNING ((ISIS_LGST,
                      "[ISIS_STATE_OFF] - Interface [%s] state changed to [DOWN]\n",
                      pi1InterfaceName));
        }
    }

    if (u4BitMap & IF_SECIP_CREATED)
    {
        u1OperState = ISIS_SEC_IP_ADDED;
        IsisMsg.u4Length = 1;

        if (u1Status == ISIS_UP)
        {
            pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                      sizeof (tIsisLLInfo));
            if (pLLInfo == NULL)
            {
                return;
            }
            IsisGetIfInfo (pCktRec->u4CktIfIdx, pLLInfo);
            pLLInfo->u1AddrType = ISIS_ADDR_IPV4;
            IsisMsg.u4Length = sizeof (tIsisLLInfo);
            if (pNetIfInfo->u4Addr != 0)
            {
                u4Addr = pNetIfInfo->u4Addr;
                u4Addr = OSIX_HTONL (u4Addr);
                MEMCPY (pLLInfo->au1IpV4Addr, &u4Addr, ISIS_MAX_IPV4_ADDR_LEN);
                u4NetMask = OSIX_HTONL (pNetIfInfo->u4NetMask);
                MEMCPY (pLLInfo->au1IpMask, &u4NetMask, ISIS_MAX_IPV4_ADDR_LEN);

            }
            ISIS_FORM_IPV4_ADDR (pLLInfo->au1IpV4Addr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);
            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Interface State Change [UP] - Secondary IP - Circuit [%u], IPv4 [%s]\n",
                     pCktRec->u4CktIfIdx, au1IPv4Addr));
        }

        IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
        IsisMsg.u1MsgType = ISIS_MSG_SEC_IF_STAT_IND;
        IsisMsg.u1DataType = 0;
        IsisMsg.pMemFreeRoutine = NULL;
        IsisMsg.u1LenOrStat = u1OperState;
        IsisMsg.u4CxtOrIfindex = pCktRec->u4LLHandle;

        if (IsisEnqueueMessage
            ((UINT1 *) &IsisMsg, sizeof (tIsisMsg), gIsisCtrlQId,
             ISIS_SYS_LL_DATA_EVT) == ISIS_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
        }

        return;
    }

    IsisMsg.u4Length = 1;

    if (u1Status == ISIS_UP)
    {
        pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                  sizeof (tIsisLLInfo));
        if (pLLInfo == NULL)
        {
            return;
        }

        IsisGetIfInfo (pCktRec->u4CktIfIdx, pLLInfo);
        pLLInfo->u1AddrType = ISIS_ADDR_IPV4;
        IsisMsg.u4Length = sizeof (tIsisLLInfo);
        if (pNetIfInfo->u4Addr != 0)
        {
            u4Addr = pNetIfInfo->u4Addr;
            u4Addr = OSIX_HTONL (u4Addr);
            MEMCPY (pLLInfo->au1IpV4Addr, &u4Addr, ISIS_MAX_IPV4_ADDR_LEN);
            u4NetMask = OSIX_HTONL (pNetIfInfo->u4NetMask);
            MEMCPY (pLLInfo->au1IpMask, &u4NetMask, ISIS_MAX_IPV4_ADDR_LEN);

        }
        ISIS_FORM_IPV4_ADDR (pLLInfo->au1IpV4Addr, au1IPv4Addr,
                             ISIS_MAX_IPV4_ADDR_LEN);
        RTP_PT ((ISIS_LGST,
                 "RTM <T> : Interface State Change [UP] - Circuit [%u], IPv4 [%s]\n",
                 pCktRec->u4CktIfIdx, au1IPv4Addr));

    }

    IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
    IsisMsg.u1MsgType = ISIS_MSG_IF_STAT_IND;
    IsisMsg.u1DataType = 0;
    IsisMsg.pMemFreeRoutine = NULL;
    IsisMsg.u1LenOrStat = u1OperState;
    IsisMsg.u4CxtOrIfindex = pCktRec->u4LLHandle;

    if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg), gIsisCtrlQId,
                            ISIS_SYS_LL_DATA_EVT) == ISIS_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
    }

    return;
}

/******************************************************************************
 * Function Name : IsisV6IfStateChgHdlr ()
 * Description   : This procedure is called to inform the change in the
 operational status of a logical interface and is
 indicated by the IP module. The value could be
 IFACE_DELETED or OPER_STATE. Depending on the current
 status of the iface, the different actions are taken.
 * Input (s)     : pNetIfInfo, the logical interface.
 u4BitMap, the event that happened.
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/
PUBLIC VOID
IsisV6IfStateChgHdlr (tNetIpv6HliParams * pIp6HliParams)
{
    tIsisCktEntry      *pCktRec = NULL;
    tIsisLLInfo        *pLLInfo = NULL;
    tIsisMsg            IsisMsg;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Status = ISIS_UP;
    UINT1               u1OperState = ISIS_STATE_ON;

    MEMSET ((UINT1 *) &NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    if (ISIS_GET_CKT_HASH_SIZE () == 0)
    {
        /* Still no circuit records are configured in the ISIS Context so ignore
         * the Inidcation*/
        return;
    }

    /* Get the  Circuit record for the given ifindex */
    i4RetVal =
        IsisAdjGetCktRecWithIfIdx (pIp6HliParams->unIpv6HlCmdType.IfStatChange.
                                   u4Index, 0, &pCktRec);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* Ignore the indication as no circuit is configured over the interface */
        return;
    }

    switch (pIp6HliParams->u4Command)
    {
        case NETIPV6_INTERFACE_PARAMETER_CHANGE:

            if (pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4Mask ==
                NETIPV6_INTERFACE_STATUS_CHANGE)
            {
                if ((pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4IfStat) ==
                    NETIPV6_IF_DELETE)
                {
                    u1Status = ISIS_DOWN;
                    u1OperState = ISIS_DOWN;
                }
                else if ((pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4IfStat)
                         == NETIPV6_IF_UP)
                {
                    u1OperState = ISIS_STATE_ON;
                }
                else if ((pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4IfStat)
                         == NETIPV6_IF_DOWN)
                {
                    u1OperState = ISIS_STATE_OFF;
                }
            }
            else
            {
                /* Ignore all other interface related changes like MTU, SPEED */
                return;
            }

            break;
        default:
            return;
    }

    IsisMsg.u4Length = 1;

    if (u1Status == ISIS_UP)
    {
        pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                  sizeof (tIsisLLInfo));
        if (pLLInfo == NULL)
        {
            return;
        }

        IsisGetIfInfo (pCktRec->u4CktIfIdx, pLLInfo);
        IsisMsg.u4Length = sizeof (tIsisLLInfo);
        pLLInfo->u1AddrType = ISIS_ADDR_IPV6;
        RTP_PT ((ISIS_LGST,
                 "RTM <T> : Interface State Change [UP] - Circuit [%u], IPv6 [%s]\n",
                 pCktRec->u4CktIfIdx,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pLLInfo->au1IpV6Addr))));
    }

    IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
    IsisMsg.u1MsgType = ISIS_MSG_IF_STAT_IND;
    IsisMsg.u1DataType = 0;
    IsisMsg.pMemFreeRoutine = NULL;
    IsisMsg.u1LenOrStat = u1OperState;
    IsisMsg.u4CxtOrIfindex = pCktRec->u4LLHandle;

    if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg), gIsisCtrlQId,
                            ISIS_SYS_LL_DATA_EVT) == ISIS_FAILURE)
    {
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
    }

    return;
}

/******************************************************************************
 * Function Name : IsisV6IfAddrChgHdlr  ()
 * Description   : This procedure is called to inform the change in the
                   address change of a logical interface and is
                   indicated by the IP module.
 * Input (s)     : pNetIpv6HlParams  -Interface Details
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None
 ******************************************************************************/

VOID
IsisV6IfAddrChgHdlr (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tIsisCktEntry      *pCktEntry = NULL;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tIsisLLInfo        *pLLInfo = NULL;
    tIsisMsg            IsisMsg;
    INT4                i4RetVal = ISIS_FAILURE;
    INT4                i4TempRetVal = ISIS_FAILURE;
    MEMSET ((UINT1 *) &NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET ((UINT1 *) &IsisMsg, 0, sizeof (tIsisMsg));
    if (ISIS_GET_CKT_HASH_SIZE () == 0)
    {
        /* Still no circuit records are configured in the ISIS Context so ignore
         * the Inidcation*/
        return;
    }

    if ((IS_ADDR_UNSPECIFIED
         (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr))
        || (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type ==
            ADDR6_MULTI))
    {
        /* Address is not valid. */
        return;
    }

    /* Get the  Circuit record for the given ifindex */
    i4RetVal =
        IsisAdjGetCktRecWithIfIdx (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.
                                   u4Index, 0, &pCktEntry);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* Ignore the indication as no circuit is configured over the interface */
        return;
    }
    if (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type ==
        ADDR6_LLOCAL)
    {
        pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                  sizeof (tIsisLLInfo));
        if (pLLInfo == NULL)
        {
            return;
        }

        IsisGetIfInfo (pCktEntry->u4CktIfIdx, pLLInfo);
        IsisMsg.u4Length = sizeof (tIsisLLInfo);
        pLLInfo->u1AddrType = ISIS_ADDR_IPV6;
        IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
        IsisMsg.u1MsgType = ISIS_MSG_IF_STAT_IND;
        IsisMsg.u1DataType = 0;
        IsisMsg.pMemFreeRoutine = NULL;
        IsisMsg.u1LenOrStat = ISIS_STATE_OFF;
        IsisMsg.u4CxtOrIfindex = pCktEntry->u4LLHandle;
        if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                gIsisCtrlQId, ISIS_SYS_LL_DATA_EVT)
            == ISIS_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
        }

        pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                  sizeof (tIsisLLInfo));
        if (pLLInfo == NULL)
        {
            return;
        }

        IsisGetIfInfo (pCktEntry->u4CktIfIdx, pLLInfo);
        IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
        pLLInfo->u1AddrType = ISIS_ADDR_IPV6;
        pLLInfo->u1IpV6PrefixLen =
            (UINT1) pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.
            u4PrefixLength;
        MEMCPY (pLLInfo->au1IpV6Addr,
                pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.
                Ip6Addr.u1_addr, ISIS_MAX_IPV6_ADDR_LEN);
        IsisMsg.u1LenOrStat = ISIS_STATE_ON;
        if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                gIsisCtrlQId, ISIS_SYS_LL_DATA_EVT)
            == ISIS_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLInfo);
        }
        return;
    }

    pLLInfo = (tIsisLLInfo *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                              sizeof (tIsisLLInfo));
    if (pLLInfo == NULL)
    {
        return;
    }

    IsisMsg.pu1Msg = (UINT1 *) pLLInfo;
    IsisMsg.u4Length = sizeof (tIsisLLInfo);
    pLLInfo->u1AddrType = ISIS_ADDR_IPV6;
    pLLInfo->u1IpV6PrefixLen =
        (UINT1) pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.
        u4PrefixLength;
    MEMCPY (pLLInfo->au1IpV6Addr,
            pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr.
            u1_addr, ISIS_MAX_IPV6_ADDR_LEN);
    IsisMsg.u1MsgType = ISIS_MSG_IF_ADDR_CHG;
    IsisMsg.u1DataType = 0;
    IsisMsg.pMemFreeRoutine = NULL;

    if (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Mask ==
        NETIPV6_ADDRESS_ADD)
    {
        IsisMsg.u1LenOrStat = ISIS_IP_IF_ADDR_ADD;
        RTP_PT ((ISIS_LGST,
                 "RTM <T> : IP Interface Address Add - Circuit [%u], IPv6 [%s]\n",
                 pCktEntry->u4CktIfIdx,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pLLInfo->au1IpV6Addr))));
    }
    else
    {
        IsisMsg.u1LenOrStat = ISIS_IP_IF_ADDR_DEL;
        RTP_PT ((ISIS_LGST,
                 "RTM <T> : IP Interface Address Delete - Circuit [%u], IPv6 [%s]\n",
                 pCktEntry->u4CktIfIdx,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pLLInfo->au1IpV6Addr))));
    }
    IsisMsg.u4CxtOrIfindex =
        pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Index;
    i4TempRetVal = IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                       gIsisCtrlQId, ISIS_SYS_LL_DATA_EVT);
    UNUSED_PARAM (i4TempRetVal);
    return;
}

/******************************************************************************
 * Function Name : IsisRegisterWithRtm ()
 * Description   : This function registers the ISIS Module with the FutureRTM 
 *                 module. 
 * Input (s)     : None 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - On successful Registration of ISIS Module 
 *                                with the RTM Module
 *                 ISIS_FAILURE - Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisRegisterWithRtm (tIsisSysContext * pContext)
{
    tRtmRegnId          RegnId;
    tRtm6RegnId         rtm6RegnId;
    MEMSET (&rtm6RegnId, 0, sizeof (tRtm6RegnId));
    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRegisterWithRtm ()\n"));

    RegnId.u2ProtoId = ISIS_MIB_PROT_ID;
    RegnId.u4ContextId = pContext->u4SysInstIdx;

    if (RtmRegister (&RegnId, ISIS_SET_MASK, IsisRecvMsgFromRtm) != RTM_SUCCESS)
    {
        return ISIS_FAILURE;
    }
    rtm6RegnId.u2ProtoId = ISIS_MIB_PROT_ID;
    rtm6RegnId.u4ContextId = pContext->u4SysInstIdx;
    if (Rtm6Register (&rtm6RegnId, RTM6_ACK_REQUIRED,
                      IsisRecvMsgFromRtm6) != RTM6_SUCCESS)
    {
        return ISIS_FAILURE;
    }

    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRegisterWithRtm ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function Name : IsisRtmiRtmTxRtUpdate ()
 * Description   : This routine forms the route update messages in the 
 *                 FutureRTM message format. The formed route update messages 
 *                 are enqueued to the FutureRTM module with the help of the 
 *                 API provided by the RTM Module. The enqueue function is
 *                 portable depending on the target system.
 *                 This routine is called by the Decision module. 
 * Input (s)     : pRouteNode - Pointer to the RTMRouteInfo which has the 
 *                              details of the nearest route, 
 *                              metric cost and the next hop address
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - On successful sending of the route 
 *                                update to the RTM
 *                 ISIS_FAILURE - Otherwise               
 ******************************************************************************/

PUBLIC INT4
IsisRtmiRtmTxRtUpdate (tIsisSysContext * pContext, tIsisRouteInfo * pRouteNode)
{

    tNetIpv4RtInfo      NetRtv4Info;
    tNetIpv6RtInfo      NetRtv6Info;
    UINT1               au1IPMask[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1CommandType = 0;
    CHR1                au1IpAddStr[ISIS_MAX_IP_ADDR_LEN];
    CHR1                au1NxtHopStr[ISIS_MAX_IP_ADDR_LEN];
    CHR1               *pi1IpAddStr = NULL;
    CHR1               *pi1NxtHopStr = NULL;

    MEMSET (au1IpAddStr, 0, ISIS_MAX_IP_ADDR_LEN);
    MEMSET (au1NxtHopStr, 0, ISIS_MAX_IP_ADDR_LEN);

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRtmiRtmTxRtUpdate () \n"));

    MEMSET ((UINT1 *) &NetRtv4Info, 0, sizeof (tNetIpv4RtInfo));
    MEMSET ((UINT1 *) &NetRtv6Info, 0, sizeof (tNetIpv6RtInfo));
    if (pRouteNode->DestIpAddr.u1AddrType == ISIS_ADDR_IPV4)
    {
        MEMCPY (&(NetRtv4Info.u4DestNet), pRouteNode->DestIpAddr.au1IpAddr,
                ISIS_MAX_IPV4_ADDR_LEN);
        NetRtv4Info.u4DestNet = OSIX_NTOHL (NetRtv4Info.u4DestNet);

        IsisUtlGetIPMask (pRouteNode->DestIpAddr.u1PrefixLen, au1IPMask);
        MEMCPY (&(NetRtv4Info.u4DestMask), au1IPMask, ISIS_MAX_IPV4_ADDR_LEN);
        NetRtv4Info.u4DestMask = OSIX_NTOHL (NetRtv4Info.u4DestMask);

        MEMCPY (&(NetRtv4Info.u4NextHop), pRouteNode->au1IPAddr,
                ISIS_MAX_IPV4_ADDR_LEN);
        NetRtv4Info.u4NextHop = OSIX_NTOHL (NetRtv4Info.u4NextHop);
        NetRtv4Info.u4RtIfIndx = pRouteNode->u4CktIfIndex;
        NetRtv4Info.i4Metric1 = (INT4) pRouteNode->u4MetricVal;
        NetRtv4Info.u2RtProto = ISIS_ID;
        NetRtv4Info.u4ContextId = pContext->u4SysInstIdx;
        NetRtv4Info.u2Weight = IP_DEFAULT_WEIGHT;

        switch (pRouteNode->u1BitMask)
        {
            case ISIS_SPT_ROUTE_ADD:
                CLI_CONVERT_IPADDR_TO_STR (pi1IpAddStr, NetRtv4Info.u4DestNet);
                MEMCPY (au1IpAddStr, pi1IpAddStr, ISIS_MAX_IP_ADDR_LEN);
                CLI_CONVERT_IPADDR_TO_STR (pi1NxtHopStr, NetRtv4Info.u4NextHop);
                MEMCPY (au1NxtHopStr, pi1NxtHopStr, ISIS_MAX_IP_ADDR_LEN);
                RTP_PT ((ISIS_LGST,
                         "ISIS->RTM : Added IPV4 Route %s: [%s/%d], Next Hop [%s]\n",
                         ISIS_GET_ROUTE_TAG (pRouteNode->u1Level), au1IpAddStr,
                         pRouteNode->DestIpAddr.u1PrefixLen, au1NxtHopStr));
                NetRtv4Info.u4RowStatus = ISIS_ACTIVE;
                u1CommandType = NETIPV4_ADD_ROUTE;
                break;
            case ISIS_SPT_ROUTE_DELETE:
                CLI_CONVERT_IPADDR_TO_STR (pi1IpAddStr, NetRtv4Info.u4DestNet);
                MEMCPY (au1IpAddStr, pi1IpAddStr, ISIS_MAX_IP_ADDR_LEN);
                CLI_CONVERT_IPADDR_TO_STR (pi1NxtHopStr, NetRtv4Info.u4NextHop);
                MEMCPY (au1NxtHopStr, pi1NxtHopStr, ISIS_MAX_IP_ADDR_LEN);
                RTP_PT ((ISIS_LGST,
                         "ISIS->RTM : Deleted IPV4 Route %s: [%s/%d], Next Hop [%s]\n",
                         ISIS_GET_ROUTE_TAG (pRouteNode->u1Level), au1IpAddStr,
                         pRouteNode->DestIpAddr.u1PrefixLen, au1NxtHopStr));
                NetRtv4Info.u4RowStatus = ISIS_DESTROY;
                u1CommandType = NETIPV4_DELETE_ROUTE;
                break;
            case ISIS_SPT_ROUTE_IFIDX_CHG:
            case ISIS_SPT_ROUTE_MET_CHG:
                CLI_CONVERT_IPADDR_TO_STR (pi1IpAddStr, NetRtv4Info.u4DestNet);
                MEMCPY (au1IpAddStr, pi1IpAddStr, ISIS_MAX_IP_ADDR_LEN);
                CLI_CONVERT_IPADDR_TO_STR (pi1NxtHopStr, NetRtv4Info.u4NextHop);
                MEMCPY (au1NxtHopStr, pi1NxtHopStr, ISIS_MAX_IP_ADDR_LEN);
                RTP_PT ((ISIS_LGST,
                         "ISIS->RTM : Modified IPV4 Route %s: [%s/%d], Next Hop [%s], Metric [%u]\n",
                         ISIS_GET_ROUTE_TAG (pRouteNode->u1Level), au1IpAddStr,
                         pRouteNode->DestIpAddr.u1PrefixLen, au1NxtHopStr,
                         pRouteNode->u4MetricVal));
                NetRtv4Info.u4RowStatus = ISIS_ACTIVE;
                u1CommandType = NETIPV4_MODIFY_ROUTE;
                break;
            default:
                break;
        }
        if (ISIS_LEVEL1 == pRouteNode->u1Level)
        {
            NetRtv4Info.u1MetricType = IP_ISIS_LEVEL1;
        }
        else if (ISIS_LEVEL2 == pRouteNode->u1Level)
        {
            NetRtv4Info.u1MetricType = IP_ISIS_LEVEL2;
        }
        else if (ISIS_INTER_AREA == pRouteNode->u1Level)
        {
            NetRtv4Info.u1MetricType = IP_ISIS_INTER_AREA;
        }

        NetRtv4Info.u1Preference =
            IsisIp4FilterRouteSource (pContext, &NetRtv4Info);

        /* If the administrative distance(AD) is 255,the router does not believe
           the source of that route and does not install the route in the
           routing table.So for UNKNOWN AD(255), send delete action to RTM */

        if (NetRtv4Info.u1Preference == IP_UNKNOWN_AD)
        {
            u1CommandType = NETIPV4_DELETE_ROUTE;
        }

        if (pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
        {
            NetRtv4Info.u2ChgBit |= IP_BIT_GR;
        }

        if (u1CommandType == NETIPV4_MODIFY_ROUTE)
        {
            if (NetIpv4LeakRoute (NETIPV4_DELETE_ROUTE, &NetRtv4Info) ==
                NETIPV4_FAILURE)
            {
                RTP_PT ((ISIS_LGST,
                         " RTM <E> : Error in deleting route to RTM forwarding "
                         "database\n"));

                return ISIS_FAILURE;
            }

            if (NetIpv4LeakRoute (NETIPV4_ADD_ROUTE, &NetRtv4Info) ==
                NETIPV4_FAILURE)
            {
                RTP_PT ((ISIS_LGST,
                         " RTM <E> : Error in adding route to RTM forwarding "
                         "database\n"));
                return ISIS_FAILURE;
            }
        }
        else
        {
            if (NetIpv4LeakRoute (u1CommandType, &NetRtv4Info) ==
                NETIPV4_FAILURE)
            {
                RTP_PT ((ISIS_LGST,
                         " RTM <E> : Error in adding route to RTM forwarding "
                         "database\n"));
                return ISIS_FAILURE;
            }
        }
    }
    else
    {
        MEMCPY (NetRtv6Info.NextHop.u1_addr, pRouteNode->au1IPAddr,
                ISIS_MAX_IPV6_ADDR_LEN);
        NetRtv6Info.u4Index = pRouteNode->u4CktIfIndex;
        MEMCPY (NetRtv6Info.Ip6Dst.u1_addr, pRouteNode->DestIpAddr.au1IpAddr,
                ISIS_MAX_IPV6_ADDR_LEN);
        NetRtv6Info.u1Prefixlen = pRouteNode->DestIpAddr.u1PrefixLen;
        NetRtv6Info.u4Metric = pRouteNode->u4MetricVal;
        NetRtv6Info.u4RowStatus = ACTIVE;
        NetRtv6Info.i1DefRtrFlag = 0;
        NetRtv6Info.i1Proto = ISIS_ID;
        NetRtv6Info.u4ContextId = pContext->u4SysInstIdx;

        switch (pRouteNode->u1BitMask)
        {
            case ISIS_SPT_ROUTE_ADD:
                RTP_PT ((ISIS_LGST,
                         "ISIS->RTM6 : Added IPV6 Route %s: [%s/%d], Next Hop [%s]\n",
                         ISIS_GET_ROUTE_TAG (pRouteNode->u1Level),
                         Ip6PrintAddr ((tIp6Addr *) (VOID *) NetRtv6Info.Ip6Dst.
                                       u1_addr), NetRtv6Info.u1Prefixlen,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *) NetRtv6Info.
                                       NextHop.u1_addr)));
                NetRtv6Info.u4RowStatus = ISIS_ACTIVE;
                u1CommandType = NETIPV6_ADD_ROUTE;
                break;
            case ISIS_SPT_ROUTE_DELETE:
                RTP_PT ((ISIS_LGST,
                         "ISIS->RTM6 : Deleted IPV6 Route %s: [%s/%d], Next Hop [%s]\n",
                         ISIS_GET_ROUTE_TAG (pRouteNode->u1Level),
                         Ip6PrintAddr ((tIp6Addr *) (VOID *) NetRtv6Info.Ip6Dst.
                                       u1_addr), NetRtv6Info.u1Prefixlen,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *) NetRtv6Info.
                                       NextHop.u1_addr)));
                NetRtv6Info.u4RowStatus = ISIS_DESTROY;
                u1CommandType = NETIPV6_DELETE_ROUTE;
                break;
            case ISIS_SPT_ROUTE_IFIDX_CHG:
            case ISIS_SPT_ROUTE_MET_CHG:
                RTP_PT ((ISIS_LGST,
                         "ISIS->RTM6 : Modified IPV6 Route %s: [%s/%d] Next Hop [%s], Metric [%d]\n",
                         ISIS_GET_ROUTE_TAG (pRouteNode->u1Level),
                         Ip6PrintAddr ((tIp6Addr *) (VOID *) NetRtv6Info.Ip6Dst.
                                       u1_addr), NetRtv6Info.u1Prefixlen,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *) NetRtv6Info.
                                       NextHop.u1_addr),
                         pRouteNode->u4MetricVal));
                NetRtv6Info.u4RowStatus = ISIS_ACTIVE;
                u1CommandType = NETIPV6_MODIFY_ROUTE;
                break;
            default:
                break;
        }
        if (ISIS_LEVEL1 == pRouteNode->u1Level)
        {
            NetRtv6Info.u1MetricType = IP_ISIS_LEVEL1;
        }
        else if (ISIS_LEVEL2 == pRouteNode->u1Level)
        {
            NetRtv6Info.u1MetricType = IP_ISIS_LEVEL2;
        }
        else if (ISIS_INTER_AREA == pRouteNode->u1Level)
        {
            NetRtv6Info.u1MetricType = IP_ISIS_INTER_AREA;
        }

        NetRtv6Info.u1Preference =
            IsisIp6FilterRouteSource (pContext, &NetRtv6Info);

        if (u1CommandType == NETIPV6_MODIFY_ROUTE)
        {
            if (NetIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetRtv6Info) ==
                NETIPV6_FAILURE)
            {
                RTP_PT ((ISIS_LGST,
                         " RTM <E> : Error in deleting route to RTM forwarding "
                         "database\n"));
                return ISIS_FAILURE;
            }
            if (NetIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetRtv6Info) ==
                NETIPV6_FAILURE)
            {
                RTP_PT ((ISIS_LGST,
                         " RTM <E> : Error in adding route to RTM forwarding "
                         "database\n"));
                return ISIS_FAILURE;
            }
        }
        else
        {
            if (NetIpv6LeakRoute (u1CommandType, &NetRtv6Info) ==
                NETIPV6_FAILURE)
            {
                RTP_PT ((ISIS_LGST,
                         " RTM <E> : Error in adding route to RTM forwarding "
                         "database\n"));
                return ISIS_FAILURE;
            }
        }
    }
    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRtmiRtmTxRtUpdate () \n"));
    return ISIS_SUCCESS;

}

/******************************************************************************
 * Function Name : IsisDeRegisterFromRtm
 * Description   : This routine deregisters the ISIS Module from the 
 *                 FutureRTM Module
 * Input (s)     : None 
 * Output (s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS - On successful DeRegistration from
 *                                the RTM
 *                 ISIS_FAILURE - Otherwise               
 ******************************************************************************/

PUBLIC INT4
IsisDeRegisterFromRtm (UINT4 u4SysInstIdx)
{
    tRtmRegnId          RegnId;
    tRtm6RegnId         rtm6RegnId;
    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisDeRegisterFromRtm () \n"));

    RegnId.u2ProtoId = ISIS_ID;
    RegnId.u4ContextId = u4SysInstIdx;
    if (RtmDeregister (&RegnId) == RTM_FAILURE)
    {
        RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisDeRegisterFromRtm () \n"));
        return ISIS_FAILURE;
    }
    rtm6RegnId.u2ProtoId = ISIS_ID;
    rtm6RegnId.u4ContextId = u4SysInstIdx;
    Rtm6Deregister (&rtm6RegnId);
    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisDeRegisterFromRtm () \n"));
    return ISIS_SUCCESS;
}

/* Following function is used as  VCM callback functions. */
/*****************************************************************************/
/*                                                                           */
/* Function     : IsisVcmCallbackFn                                          */
/*                                                                           */
/* Description  : Function to Indicate Context/Interface mapping deletion to */
/*                ISIS. This fucntion is registered with VCM module          */
/*                as a call back function                                    */
/*                                                                           */
/* Input        : u4IfIndex   - Ip Interface Index                           */
/*                u4VcmCxtId  - Context Id                                   */
/*                u1BitMap    - Bit Map to identify the change               */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IsisVcmCallbackFn (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1BitMap)
{

    tIsisCktEntry      *pCktEntry;
    tIsisEvtVcmInfo    *pIsisEvtVcmInfo = NULL;
    tIsisSysContext    *pContext = NULL;

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisVcmCallbackFn () \n"));
    /*Internal mapping with ISIS */
    u4VcmCxtId = u4VcmCxtId - 1;

    /*Checking Bitmap is valid or not */
    if ((u1BitMap & (VCM_IF_MAP_CHG_REQ | VCM_CXT_STATUS_CHG_REQ)) == 0)
    {
        DETAIL_FAIL (ISIS_CR_MODULE);
        RTP_EE ((ISIS_LGST, "VCM <X> : Exiting IsisVcmCallbackFn ()\n"));
        return;
    }

    /*Checking whether the specified context is available or not */
    if (IsisCtrlGetSysContext (u4VcmCxtId, &pContext) == ISIS_FAILURE)
    {
        RTP_EE ((ISIS_LGST,
                 "VCM <X> : VCM Call back function Failed.Exiting IsisVcmCallbackFn ()\n"));
        return;
    }

    /*Checking whether the specified circuit is available or not */
    if (u1BitMap == VCM_IF_MAP_CHG_REQ)
    {
        if (IsisAdjGetCktRecWithIfIdx (u4IpIfIndex, 0, &pCktEntry) ==
            ISIS_SUCCESS)
        {
            if (pCktEntry == NULL)
            {
                DETAIL_FAIL (ISIS_CR_MODULE);
                RTP_EE ((ISIS_LGST,
                         "VCM <X> : Exiting IsisVcmCallbackFn ()\n"));
                return;
            }
        }
        else
        {
            RTP_EE ((ISIS_LGST, "VCM <X> : Exiting IsisVcmCallbackFn ()\n"));
            return;
        }
    }
    /*Send Event to ISIS */
    pIsisEvtVcmInfo = (tIsisEvtVcmInfo *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtVcmInfo));
    if (pIsisEvtVcmInfo == NULL)
    {
        return;
    }
    pIsisEvtVcmInfo->u1EvtID = ISIS_EVT_VCM;
    pIsisEvtVcmInfo->u4IpIfIndex = u4IpIfIndex;
    pIsisEvtVcmInfo->u4VcmCxtId = u4VcmCxtId;
    pIsisEvtVcmInfo->u1BitMap = u1BitMap;
    IsisUtlSendEvent (pContext, (UINT1 *) pIsisEvtVcmInfo,
                      sizeof (tIsisEvtVcmInfo));
    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisVcmCallbackFn()\n"));
    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IsisVcmMsgHandler                                   */
/*                                                                         */
/*     Description   : Process Interface tontext Mapping Information       */
/*                     Change Notification and context deletion indication */
/*                     from IP                                             */
/*     Input(s)      : pIsisVcmInfo  -Vcm Info                             */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
IsisVcmMsgHandler (tIsisMsg * pIsisMsg)
{
    tIsisCktEntry      *pCktEntry;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtCktChange  *pCktEvt = NULL;
    tIsisEvtVcmInfo    *pIsisVcmInfo = NULL;
    UINT4               u4VcmCxtId;

    RTP_EE ((ISIS_LGST, "VCM <X> : Entered VcmMsgHandler ()\n"));

    pIsisVcmInfo = (tIsisEvtVcmInfo *) (VOID *) pIsisMsg->pu1Msg;
    /*Internal Mapping with ISIS */

    u4VcmCxtId = (pIsisVcmInfo->u4VcmCxtId - 1);
    if (IsisCtrlGetSysContext (u4VcmCxtId, &pContext) == ISIS_FAILURE)
    {
        return;
    }

    switch (pIsisVcmInfo->u1BitMap)
    {
        case VCM_CXT_STATUS_CHG_REQ:

            /*Context Deleted Chg Notification from VCM */
            IsisCtrlDelSysContext (u4VcmCxtId);
            break;

        case VCM_IF_MAP_CHG_REQ:
            if (IsisAdjGetCktRecWithIfIdx
                (pIsisVcmInfo->u4IpIfIndex, 0, &pCktEntry) == ISIS_FAILURE)
            {
                return;
            }
            /* Invoking the TRFR routine to unbind this circuit from Data Link Layer */
            IsisTrfrIfStatusInd (pContext, pCktEntry, ISIS_CKT_DOWN);

            /* Generate a Circuit DOWN Event to the Control Q */
            pCktEvt = (tIsisEvtCktChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtCktChange));
            if (pCktEvt != NULL)
            {
                pCktEvt->u1EvtID = ISIS_EVT_CKT_CHANGE;
                pCktEvt->u1Status = ISIS_CKT_DESTROY;
                pCktEvt->u1CktType = pCktEntry->u1CktType;
                pCktEvt->u1CktLevel = pCktEntry->u1CktLevel;
                pCktEvt->u4CktIdx = pCktEntry->u4CktIdx;

                IsisUtlSendEvent (pContext, (UINT1 *) pCktEvt,
                                  sizeof (tIsisEvtCktChange));
            }
        default:
            return;

    }
    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting VcmMsgHandler()\n"));
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function        : RtmIsisGrNotifInd                                       */
/*                                                                           */
/* Description     : This routine sends an event to RTM indicating the       */
/*                   Grace period                                            */
/*                                                                           */
/* Input           : pContext         -   pointer to ISIS context            */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : ISIS_SUCCESS / ISIS_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
RtmIsisGrNotifInd (tIsisSysContext * pContext)
{
    tOsixMsg           *pRtmMsg;
    tRtmMsgHdr         *pRtmMsgHdr;

    if ((pRtmMsg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (UINT2) + sizeof (UINT4)), 0)) == NULL)

    {
        PANIC ((ISIS_LGST, "Buffer Allocation Failure\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FAILURE;
    }

    pRtmMsgHdr = (tRtmMsgHdr *) ISIS_RTM_IF_GET_MSG_HDR_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = RTM_GR_NOTIFY_MSG;
    pRtmMsgHdr->RegnId.u2ProtoId = ISIS_ID;
    pRtmMsgHdr->RegnId.u4ContextId = pContext->u4SysInstIdx;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT2) + sizeof (UINT4);

    if (IP_COPY_TO_BUF (pRtmMsg, &(pContext->u2IsisGRT3TimerInterval),
                        0, sizeof (UINT4)) == IP_BUF_FAILURE)
    {
        PANIC ((ISIS_LGST, "Buffer Copy Failure\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IP_RELEASE_BUF (pRtmMsg, FALSE);
        return ISIS_FAILURE;
    }

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pRtmMsg) != IP_SUCCESS)
    {
        PANIC ((ISIS_LGST, "Failure in sending buffer to RTM\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        CRU_BUF_Release_MsgBufChain (pRtmMsg, 0);
        return ISIS_FAILURE;
    }

    return ISIS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : Rtm6IsisGrNotifInd                                       */
/*                                                                           */
/* Description     : This routine sends an event to RTM indicating the       */
/*                   Grace period                                            */
/*                                                                           */
/* Input           : pContext         -   pointer to ISIS context            */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : ISIS_SUCCESS / ISIS_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
Rtm6IsisGrNotifInd (tIsisSysContext * pContext)
{
    tOsixMsg           *pRtm6Msg;
    tRtm6MsgHdr        *pRtm6MsgHdr;

    if ((pRtm6Msg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (UINT2) + sizeof (UINT4)), 0)) == NULL)

    {
        PANIC ((ISIS_LGST, "Buffer Allocation Failure\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FAILURE;
    }

    pRtm6MsgHdr = (tRtm6MsgHdr *) ISIS_RTM_IF_GET_MSG_HDR_PTR (pRtm6Msg);

    /* fill the message header */
    pRtm6MsgHdr->u1MessageType = RTM6_GR_NOTIFY_MSG;
    pRtm6MsgHdr->RegnId.u2ProtoId = ISIS_ID;
    pRtm6MsgHdr->RegnId.u4ContextId = pContext->u4SysInstIdx;
    pRtm6MsgHdr->u2MsgLen = sizeof (UINT2) + sizeof (UINT4);

    if (IP_COPY_TO_BUF (pRtm6Msg, &(pContext->u2IsisGRT3TimerInterval),
                        0, sizeof (UINT4)) == IP_BUF_FAILURE)
    {
        PANIC ((ISIS_LGST, "Buffer Copy Failure\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IP_RELEASE_BUF (pRtm6Msg, FALSE);
        return ISIS_FAILURE;
    }
    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm6 (pRtm6Msg) != IP6_SUCCESS)
    {
        PANIC ((ISIS_LGST, "Failure in sending buffer to RTM\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        CRU_BUF_Release_MsgBufChain (pRtm6Msg, 0);
        return ISIS_FAILURE;
    }

    return ISIS_SUCCESS;
}

/**************************************************************************/
/* Function Name : IsisIp4FilterRouteSource                               */
/*                                                                        */
/* Description   : Apply route map, return distance (or default distance) */
/*                 for the route                                          */
/* Inputs        : 1. pContext - protocol context                         */
/*                 2. u4SrcAddr - source IP address                       */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/
UINT1
IsisIp4FilterRouteSource (tIsisSysContext * pContext,
                          tNetIpv4RtInfo * pNetRtInfo)
{
#ifdef ROUTEMAP_WANTED
    UINT4               u4DestMask = 0;
    UINT1               au1IpMask[ISIS_IPV4_ADDR_LEN];
    UINT1               u1Distance = 0;

    MEMSET (au1IpMask, 0, ISIS_IPV4_ADDR_LEN);
    if (pContext != NULL)
    {
        u1Distance = pContext->u1Distance;
        if (pContext->pDistanceFilterRMap != NULL
            && pContext->pDistanceFilterRMap->u1Status == FILTERNIG_STAT_ENABLE)
        {
            tRtMapInfo          MapInfo;
            MEMSET (&MapInfo, 0, sizeof (MapInfo));
            IPVX_ADDR_INIT_FROMV4 (MapInfo.DstXAddr, pNetRtInfo->u4DestNet);
            u4DestMask = OSIX_HTONL (pNetRtInfo->u4DestMask);
            MEMCPY (au1IpMask, &u4DestMask, ISIS_IPV4_ADDR_LEN);

            IsisUtlComputePrefixLen (au1IpMask, ISIS_IPV4_ADDR_LEN,
                                     (UINT1 *) (&(MapInfo.u2DstPrefixLen)));
            if (RMapApplyRule2
                (&MapInfo,
                 pContext->pDistanceFilterRMap->au1DistInOutFilterRMapName) ==
                RMAP_ENTRY_MATCHED)
            {
                u1Distance = pContext->pDistanceFilterRMap->u1RMapDistance;
            }
        }
    }
    return u1Distance;
#else
    UNUSED_PARAM (pContext);
    UNUSED_PARAM (pNetRtInfo);
    return ISIS_DEFAULT_PREFERENCE;
#endif
}

/**************************************************************************/
/* Function Name : IsisIp6FilterRouteSource                               */
/*                                                                        */
/* Description   : Apply route map, return distance (or default distance) */
/*                 for the route                                          */
/* Inputs        : 1. pContext - protocol context                         */
/*               : 2. pSrcAddr - source IPv6 address                      */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/
UINT1
IsisIp6FilterRouteSource (tIsisSysContext * pContext,
                          tNetIpv6RtInfo * pNetRt6Info)
{
#ifdef ROUTEMAP_WANTED
    UINT1               u1Distance = 0;
    if (pContext != NULL)
    {
        u1Distance = pContext->u1Distance;
        if (pContext->pDistanceFilterRMap != NULL
            && pContext->pDistanceFilterRMap->u1Status == FILTERNIG_STAT_ENABLE)
        {
            tRtMapInfo          MapInfo;
            MEMSET (&MapInfo, 0, sizeof (MapInfo));
            IPVX_ADDR_INIT_FROMV6 (MapInfo.DstXAddr,
                                   pNetRt6Info->Ip6Dst.u1_addr);
            MapInfo.u2DstPrefixLen = (UINT2) pNetRt6Info->u1Prefixlen;
            if (RMapApplyRule2
                (&MapInfo,
                 pContext->pDistanceFilterRMap->au1DistInOutFilterRMapName) ==
                RMAP_ENTRY_MATCHED)
            {
                u1Distance = pContext->pDistanceFilterRMap->u1RMapDistance;
            }
        }
    }
    return u1Distance;
#else
    UNUSED_PARAM (pNetRt6Info);
    UNUSED_PARAM (pContext);
    return ISIS_DEFAULT_PREFERENCE;
#endif
}

/****************************************************************************
 Function    :  IsisSendingMessageToRRDQueue
 Description :  This function is called to send messages to the RTM input Q
 Input       :  -The bit mask correspoding to protocols with which RRD is enabled
                -The type of the message to be sent
                -The route map name
 Output      :  none
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IsisSendingMessageToRRDQueue (tIsisSysContext * pContext,
                              UINT4 u4RRDSrcProtoMaskEnable,
                              UINT1 u1MessageType)
{
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    tOsixMsg           *pBuf = NULL;
    tRtmMsgHdr         *pRtmMsgHdr = NULL;
    UINT2               u2Bitmask = 0;
    UINT4               u4Len = 0;

    /* prepare intermediate buf for route-map, *
     * ensure no garbage after map name        */
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN);
    u4Len = (STRLEN (pContext->RRDInfo.au1RRDRMapName) < sizeof (au1RMapName) ?
             STRLEN (pContext->RRDInfo.au1RRDRMapName) : sizeof (au1RMapName) -
             1);
    STRNCPY (au1RMapName, pContext->RRDInfo.au1RRDRMapName, u4Len);
    au1RMapName[u4Len] = '\0';

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain ((sizeof (tRtmMsgHdr) +
                                        sizeof (UINT2) + RMAP_MAX_NAME_LEN),
                                       0)) == NULL)
    {
        PANIC ((ISIS_LGST, "Buffer Allocation Failure\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return SNMP_FAILURE;
    }
    pRtmMsgHdr = (tRtmMsgHdr *) CRU_BUF_Get_ModuleData (pBuf);
    pRtmMsgHdr->u1MessageType = u1MessageType;
    pRtmMsgHdr->RegnId.u2ProtoId = ISIS_ID;
    pRtmMsgHdr->RegnId.u4ContextId = pContext->u4SysInstIdx;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT4) + RMAP_MAX_NAME_LEN;

    /* Setting the Particular Protocol Bit and Sending the
     * Message to the RTM queue
     */
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_STATIC) == ISIS_IMPORT_STATIC)
    {
        u2Bitmask |= RTM_STATIC_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_DIRECT) == ISIS_IMPORT_DIRECT)
    {
        u2Bitmask |= RTM_DIRECT_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_RIP) == ISIS_IMPORT_RIP)
    {
        u2Bitmask |= RTM_RIP_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_OSPF) == ISIS_IMPORT_OSPF)
    {
        u2Bitmask |= RTM_OSPF_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_BGP) == ISIS_IMPORT_BGP)
    {
        u2Bitmask |= RTM_BGP_MASK;
    }
    /*put protocol-mask in packet on offset 0 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, (UINT1 *) &u2Bitmask, 0, sizeof (u2Bitmask)) == CRU_FAILURE)
    {
        PANIC ((ISIS_LGST, "Copying Reg Msg Failed\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    /*put route-map in packet on offset 2 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, au1RMapName, sizeof (u2Bitmask),
         RMAP_MAX_NAME_LEN) == CRU_FAILURE)
    {
        PANIC ((ISIS_LGST, "Copying Reg Msg Failed\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    RTP_PT ((ISIS_LGST,
             "RTM <T> : %s to RTM for redistribution of [%s] routes \n",
             ISIS_GET_RRD_MSG_TYPE_STR (u1MessageType),
             ISIS_GET_RRD_RTM_IMPORTMASK_STR (u4RRDSrcProtoMaskEnable)));
    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm (pBuf) != RTM_SUCCESS)
    {
        RTP_PT ((ISIS_LGST, "RTM <T> : %s to RTM for redistribution failed \n",
                 ISIS_GET_RRD_MSG_TYPE_STR (u1MessageType)));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IsisSendingMessageToRRDQueue6
 Description :  This function is called to send messages to the RTM input Q
 Input       :  -The bit mask correspoding to protocols with which RRD is enabled
                -The type of the message to be sent
                -The route map name
 Output      :  none
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
IsisSendingMessageToRRDQueue6 (tIsisSysContext * pContext,
                               UINT4 u4RRDSrcProtoMaskEnable,
                               UINT1 u1MessageType)
{
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    tOsixMsg           *pBuf = NULL;
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;
    UINT2               u2Bitmask = 0;
    UINT4               u4Len = 0;

    /* prepare intermediate buf for route-map, *
     * ensure no garbage after map name        */
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN);
    u4Len = (STRLEN (pContext->RRDInfo.au1RRDRMapName) < sizeof (au1RMapName) ?
             STRLEN (pContext->RRDInfo.au1RRDRMapName) : sizeof (au1RMapName) -
             1);
    STRNCPY (au1RMapName, pContext->RRDInfo.au1RRDRMapName, u4Len);
    au1RMapName[u4Len] = '\0';

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain ((sizeof (tRtmMsgHdr) +
                                        sizeof (UINT2) + RMAP_MAX_NAME_LEN),
                                       0)) == NULL)
    {
        PANIC ((ISIS_LGST, "Buffer Allocation Failure\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return SNMP_FAILURE;
    }
    pRtm6MsgHdr = (tRtm6MsgHdr *) IP6_GET_MODULE_DATA_PTR (pBuf);
    pRtm6MsgHdr->u1MessageType = u1MessageType;
    pRtm6MsgHdr->RegnId.u2ProtoId = ISIS_ID;
    pRtm6MsgHdr->RegnId.u4ContextId = pContext->u4SysInstIdx;
    pRtm6MsgHdr->u2MsgLen = sizeof (UINT2);

    /* Setting the Particular Protocol Bit and Sending the
     * Message to the RTM queue
     */
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_STATIC) == ISIS_IMPORT_STATIC)
    {
        u2Bitmask |= RTM6_STATIC_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_DIRECT) == ISIS_IMPORT_DIRECT)
    {
        u2Bitmask |= RTM6_DIRECT_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_RIP) == ISIS_IMPORT_RIP)
    {
        u2Bitmask |= RTM6_RIP_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_OSPF) == ISIS_IMPORT_OSPF)
    {
        u2Bitmask |= RTM6_OSPF_MASK;
    }
    if ((u4RRDSrcProtoMaskEnable & ISIS_IMPORT_BGP) == ISIS_IMPORT_BGP)
    {
        u2Bitmask |= RTM6_BGP_MASK;
    }
    /*put protocol-mask in packet on offset 0 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, (UINT1 *) &u2Bitmask, 0, sizeof (u2Bitmask)) == CRU_FAILURE)
    {
        PANIC ((ISIS_LGST, "Copying Reg Msg Failed\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    /*put route-map in packet on offset 2 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, au1RMapName, sizeof (u2Bitmask),
         RMAP_MAX_NAME_LEN) == CRU_FAILURE)
    {
        PANIC ((ISIS_LGST, "Copying Reg Msg Failed\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }

    RTP_PT ((ISIS_LGST,
             "RTM <T> : %s to RTM6 for redistribution of [%s] routes \n",
             ISIS_GET_RRD_MSG_TYPE_STR (u1MessageType),
             ISIS_GET_RRD_RTM_IMPORTMASK_STR (u4RRDSrcProtoMaskEnable)));
    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm6 (pBuf) != RTM_SUCCESS)
    {
        RTP_PT ((ISIS_LGST, "RTM <T> : %s to RTM6 for redistribution failed \n",
                 ISIS_GET_RRD_MSG_TYPE_STR (u1MessageType)));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IsisProtoIdFromRtm6 
 Description :  Convert protocol-id while redistributing IPv6 routes 
                from RTM6 into ISIS
 Input       :  pu2ProtoId - protocol id enumerated as in RTM6
 Output      :  pu2ProtoId - protocol id enumerated as in BGP (RTM4)
****************************************************************************/
VOID
IsisProtoIdFromRtm6 (UINT2 *pu2ProtoId)
{
    /* inside BGP all protocols are enumerated as in RTM4 */
    switch (*pu2ProtoId)
    {
        case IP6_LOCAL_PROTOID:
            *pu2ProtoId = ISIS_LOCAL_ID;
            break;

        case IP6_NETMGMT_PROTOID:
            *pu2ProtoId = ISIS_STATIC_ID;
            break;

        case IP6_RIP_PROTOID:
            *pu2ProtoId = ISIS_RIP_ID;
            break;

        case IP6_OSPF_PROTOID:
            *pu2ProtoId = ISIS_OSPF_ID;
            break;

        case IP6_BGP_PROTOID:
            *pu2ProtoId = ISIS_BGP_ID;
            break;
        default:
            RTP_EE ((ISIS_LGST, " Wrong ProtoID \n"));

    }
}

/*****************************************************************************/
/* Function        : IsisRtmProcessRtChange                                  */
/* Description     : This procedure scans the Route Change message from RTM  */
/*                   and sends the update message to ISIS task               */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/*                 : pRtmHdr   -   ptr to RTM header                         */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
IsisRtmProcessRtChange (tNetIpv4RtInfo * pNetIpRtInfo, UINT2 u2ProtoId)
{
    tIsisMDT           *pMDT = NULL;
    tIsisMDT           *pMDT1 = NULL;
    UINT4               u4ProtoId = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisRRDRoutes     *pRRDRoute = NULL;
    tIsisRRDRoutes      RRDRoute;
    CHR1                au1NxtHopStr[ISIS_MAX_IP_ADDR_LEN];
    CHR1               *pi1NxtHopStr = NULL;

    if (IsisCtrlGetSysContext (pNetIpRtInfo->u4ContextId, &pContext) ==
        ISIS_FAILURE)
    {
        UPP_PT ((ISIS_LGST, "UPD <T> : Invalid Context ID [%u]\n",
                 pNetIpRtInfo->u4ContextId));
        return ISIS_FAILURE;
    }

    if (u2ProtoId == ISIS_OSPF_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_OSPF;
    }
    else if (u2ProtoId == ISIS_STATIC_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_STATIC;
    }
    else if (u2ProtoId == ISIS_LOCAL_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_DIRECT;
    }
    else if (u2ProtoId == ISIS_RIP_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_RIP;
    }
    else if (u2ProtoId == ISIS_BGP_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_BGP;
    }
    if ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) &&
        ((UINT4) pNetIpRtInfo->i4Metric1 > ISIS_MAX_METRIC_MT))
    {
        pNetIpRtInfo->i4Metric1 = ISIS_MAX_METRIC_MT;
    }
    else if ((pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC) &&
             (pNetIpRtInfo->i4Metric1 > ISIS_LL_CKTL_MAX_METRIC))
    {
        pNetIpRtInfo->i4Metric1 = ISIS_LL_CKTL_MAX_METRIC;
    }
    if (pContext->RRDInfo.au4RRDMetric[u4ProtoId] != 0)
    {
        pNetIpRtInfo->i4Metric1 =
            (INT4) pContext->RRDInfo.au4RRDMetric[u4ProtoId];
    }
    pNetIpRtInfo->u4DestNet = OSIX_NTOHL (pNetIpRtInfo->u4DestNet);
    MEMSET (&RRDRoute.IPAddr, 0, sizeof (tIsisIpAddr));
    RRDRoute.IPAddr.u1AddrType = ISIS_IPV4;
    MEMCPY (RRDRoute.IPAddr.au1IpAddr, &(pNetIpRtInfo->u4DestNet),
            ISIS_MAX_IPV4_ADDR_LEN);
    RRDRoute.IPAddr.u1PrefixLen =
        IsisGetSubnetmasklen (pNetIpRtInfo->u4DestMask);

    pRRDRoute =
        (tIsisRRDRoutes *) RBTreeGet (pContext->RRDRouteRBTree,
                                      (tRBElem *) & RRDRoute);
    if (pRRDRoute == NULL)
    {
        RTP_PT ((ISIS_LGST,
                 "RTM <E> : Unable to update, Route Entry not present in RRD RBTree\n"));
        return (ISIS_FAILURE);
    }
    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        return ISIS_FAILURE;
    }
    if (pNetIpRtInfo->u4RowStatus == RTM_DESTROY)
    {
        pMDT->u1Cmd = ISIS_CMD_DELETE;
        RBTreeRemove (pContext->RRDRouteRBTree, (tRBElem *) pRRDRoute);
        ISIS_MEM_FREE (ISIS_RRD_ROUTE, (UINT1 *) pRRDRoute);
    }
    if (pNetIpRtInfo->u4RowStatus == RTM_ACTIVE)
    {
        pMDT->u1Cmd = ISIS_CMD_MODIFY;
        pRRDRoute->u4Metric = (UINT4) pNetIpRtInfo->i4Metric1;
    }
    MEMCPY (pMDT->IPAddr.au1IpAddr, &(pNetIpRtInfo->u4DestNet),
            ISIS_MAX_IPV4_ADDR_LEN);
    pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
    pMDT->IPAddr.u1PrefixLen = IsisGetSubnetmasklen (pNetIpRtInfo->u4DestMask);

    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
    }
    else
    {
        pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
    }
    pMDT->u1SrcProtoId = (UINT1) u2ProtoId;
    ISIS_COPY_METRIC (pContext, pNetIpRtInfo->i4Metric1, pMDT);
    CLI_CONVERT_IPADDR_TO_STR (pi1NxtHopStr, pNetIpRtInfo->u4NextHop);
    MEMCPY (au1NxtHopStr, pi1NxtHopStr, ISIS_MAX_IPV4_ADDR_LEN);
    RTP_PT ((ISIS_LGST,
             "IPv4 Route Change - [%d.%d.%d.%d] with NextHop [%s], Metric [%d]\n",
             pMDT->IPAddr.au1IpAddr[0], pMDT->IPAddr.au1IpAddr[1],
             pMDT->IPAddr.au1IpAddr[2], pMDT->IPAddr.au1IpAddr[3], au1NxtHopStr,
             pNetIpRtInfo->i4Metric1));
    if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_1) ==
        ISIS_IMPORT_LEVEL_1)
    {
        pMDT1 = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

        if (pMDT1 == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            return ISIS_FAILURE;
        }
        MEMCPY (pMDT1, pMDT, sizeof (tIsisMDT));
        pMDT1->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
        if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT1) == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Failed in updating"
                     "L1 RTM route into SelfLSP\n"));
        }
        else
        {
            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Updated RTM route in L1 Self-LSP\n"));
        }
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
    }
    if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_2) ==
        ISIS_IMPORT_LEVEL_2)
    {
        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
        if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Failed in updating"
                     "L2 RTM route into SelfLSP\n"));
        }
        else
        {
            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Updated RTM route in L2 Self-LSP\n"));
        }
    }
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function        : IsisRtmProcessUpdate                                    */
/* Description     : This procedure scans the Route Update message from RTM  */
/*                   and sends the update message to ISIS task               */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/*                 : pRtmHdr   -   ptr to RTM header                         */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
INT4
IsisRtmProcessUpdate (tNetIpv4RtInfo * pNetIpRtInfo, UINT2 u2ProtoId)
{
    tIsisMDT           *pMDT = NULL;
    tIsisMDT           *pMDT1 = NULL;
    UINT4               u4ProtoId = 0;
    UINT4               u4NetAddr = 0;
    UINT4               u4IfIndex = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisRRDRoutes     *pRRDRoute = NULL;
    CHR1                au1NxtHopStr[ISIS_MAX_IP_ADDR_LEN];
    CHR1               *pi1NxtHopStr = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    UINT1               u1IsisEnabled = 0;

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRtmProcessUpdate ()\n"));

    if (IsisCtrlGetSysContext (pNetIpRtInfo->u4ContextId, &pContext) ==
        ISIS_FAILURE)
    {
        RTP_PT ((ISIS_LGST, "UPD <T> : Invalid Context ID [%u]\n",
                 pNetIpRtInfo->u4ContextId));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FAILURE;
    }
    /*Apply configured Metic value on the route */
    if (u2ProtoId == ISIS_OSPF_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_OSPF;
    }
    else if (u2ProtoId == ISIS_STATIC_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_STATIC;
    }
    else if (u2ProtoId == ISIS_LOCAL_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_DIRECT;
        if (NetIpv4GetCfaIfIndexFromPort
            (pNetIpRtInfo->u4RtIfIndx, &u4IfIndex) == NETIPV4_SUCCESS)
        {
            pIPIfRec = pContext->IPIfTable.pIPIfAddr;
            while (pIPIfRec != NULL)
            {
                if ((pIPIfRec->u1AddrType == ISIS_ADDR_IPV4) &&
                    (pIPIfRec->u4CktIfIdx == u4IfIndex))
                {
                    MEMCPY (&u4NetAddr, pIPIfRec->au1IPAddr,
                            ISIS_MAX_IPV4_ADDR_LEN);
                    u4NetAddr =
                        ((OSIX_NTOHL (u4NetAddr)) & (pNetIpRtInfo->u4DestMask));
                    if (u4NetAddr == pNetIpRtInfo->u4DestNet)
                    {
                        u1IsisEnabled = ISIS_TRUE;
                        break;
                    }
                }
                pIPIfRec = pIPIfRec->pNext;
            }
        }
    }
    else if (u2ProtoId == ISIS_RIP_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_RIP;
    }
    else if (u2ProtoId == ISIS_BGP_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_BGP;
    }
    if ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) &&
        ((UINT4) pNetIpRtInfo->i4Metric1 > ISIS_MAX_METRIC_MT))
    {
        pNetIpRtInfo->i4Metric1 = ISIS_MAX_METRIC_MT;
    }
    else if ((pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC) &&
             (pNetIpRtInfo->i4Metric1 > ISIS_LL_CKTL_MAX_METRIC))
    {
        pNetIpRtInfo->i4Metric1 = ISIS_LL_CKTL_MAX_METRIC;
    }
    if (pContext->RRDInfo.au4RRDMetric[u4ProtoId] != 0)
    {
        pNetIpRtInfo->i4Metric1 =
            (INT4) pContext->RRDInfo.au4RRDMetric[u4ProtoId];
    }

    pRRDRoute = (tIsisRRDRoutes *) ISIS_MEM_ALLOC (ISIS_RRD_ROUTE, 0);
    if (pRRDRoute == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : RRD Route\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_RRD_ROUTE);
        return ISIS_FAILURE;
    }
    pNetIpRtInfo->u4DestNet = OSIX_NTOHL (pNetIpRtInfo->u4DestNet);

    pRRDRoute->IPAddr.u1AddrType = ISIS_IPV4;
    MEMCPY (pRRDRoute->IPAddr.au1IpAddr, &(pNetIpRtInfo->u4DestNet),
            ISIS_MAX_IPV4_ADDR_LEN);
    pRRDRoute->IPAddr.u1PrefixLen =
        IsisGetSubnetmasklen (pNetIpRtInfo->u4DestMask);
    pRRDRoute->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
    pRRDRoute->u4Metric = (UINT4) pNetIpRtInfo->i4Metric1;
    pRRDRoute->u1Protocol = (UINT1) u2ProtoId;
    CLI_CONVERT_IPADDR_TO_STR (pi1NxtHopStr, pNetIpRtInfo->u4NextHop);
    MEMCPY (au1NxtHopStr, pi1NxtHopStr, ISIS_MAX_IPV4_ADDR_LEN);
    RTP_PT ((ISIS_LGST,
             "Redistributing [%s] [%s] Route [%d.%d.%d.%d] to ISIS - NextHop [%s] Metric [%u]\n",
             ISIS_GET_RRD_PROTO_STR (pRRDRoute->u1Protocol),
             ISIS_GET_ADDR_TYPE_STR (pRRDRoute->IPAddr.u1AddrType),
             pRRDRoute->IPAddr.au1IpAddr[0], pRRDRoute->IPAddr.au1IpAddr[1],
             pRRDRoute->IPAddr.au1IpAddr[2], pRRDRoute->IPAddr.au1IpAddr[3],
             au1NxtHopStr, pRRDRoute->u4Metric));

    if (RBTreeAdd (pContext->RRDRouteRBTree, (tRBElem *) pRRDRoute) !=
        RB_SUCCESS)
    {
        RTP_PT ((ISIS_LGST,
                 "RTM <E> : RRD RBTree addition failure - route already present\n"));
        ISIS_MEM_FREE (ISIS_RRD_ROUTE, (UINT1 *) pRRDRoute);
        return (ISIS_FAILURE);
    }
    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        return ISIS_FAILURE;
    }

    pMDT->u1Cmd = ISIS_CMD_ADD;
    MEMCPY (pMDT->IPAddr.au1IpAddr, &(pNetIpRtInfo->u4DestNet),
            ISIS_MAX_IPV4_ADDR_LEN);
    pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
    pMDT->IPAddr.u1PrefixLen = IsisGetSubnetmasklen (pNetIpRtInfo->u4DestMask);;
    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
    }
    else
    {
        pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
    }
    pMDT->u1SrcProtoId = (UINT1) u2ProtoId;
    if (u1IsisEnabled == ISIS_TRUE)
    {
        pMDT->u1IsisEnabled = ISIS_TRUE;
    }
    ISIS_COPY_METRIC (pContext, pNetIpRtInfo->i4Metric1, pMDT);

    if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_1) ==
        ISIS_IMPORT_LEVEL_1)
    {
        pMDT1 = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

        if (pMDT1 == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            return ISIS_FAILURE;
        }
        MEMCPY (pMDT1, pMDT, sizeof (tIsisMDT));
        pMDT1->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
        if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT1) == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update"
                     " L1 RTM Route [%d.%d.%d.%d] in [%s]\n",
                     pMDT1->IPAddr.au1IpAddr[0], pMDT1->IPAddr.au1IpAddr[1],
                     pMDT1->IPAddr.au1IpAddr[2], pMDT1->IPAddr.au1IpAddr[3],
                     ISIS_GET_LSP_STR (pMDT1->u1LSPType)));
        }
        else
        {
            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Updated L1 RTM Route [%d.%d.%d.%d] in [%s]\n",
                     pMDT1->IPAddr.au1IpAddr[0], pMDT1->IPAddr.au1IpAddr[1],
                     pMDT1->IPAddr.au1IpAddr[2], pMDT1->IPAddr.au1IpAddr[3],
                     ISIS_GET_LSP_STR (pMDT1->u1LSPType)));
        }
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
    }
    if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_2) ==
        ISIS_IMPORT_LEVEL_2)
    {
        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
        if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update"
                     " L2 RTM Route [%d.%d.%d.%d] in [%s]\n",
                     pMDT->IPAddr.au1IpAddr[0], pMDT->IPAddr.au1IpAddr[1],
                     pMDT->IPAddr.au1IpAddr[2], pMDT->IPAddr.au1IpAddr[3],
                     ISIS_GET_LSP_STR (pMDT->u1LSPType)));
        }
        else
        {
            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Updated L2 RTM Route [%d.%d.%d.%d] in [%s]\n",
                     pMDT->IPAddr.au1IpAddr[0], pMDT->IPAddr.au1IpAddr[1],
                     pMDT->IPAddr.au1IpAddr[2], pMDT->IPAddr.au1IpAddr[3],
                     ISIS_GET_LSP_STR (pMDT->u1LSPType)));
        }
    }
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRtmProcessUpdate ()\n"));

    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function        : IsisRtm6ProcessUpdate                                    */
/* Description     : This procedure scans the Route Update message from RTM  */
/*                   and sends the update message to ISIS task               */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/*                 : pRtmHdr   -   ptr to RTM header                         */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
INT1
IsisRtm6ProcessUpdate (tNetIpv6RtInfo * pNetIpRtInfo, UINT2 u2ProtoId)
{
    tIsisMDT           *pMDT = NULL;
    tIsisMDT           *pMDT1 = NULL;
    UINT4               u4ProtoId = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisRRDRoutes     *pRRDRoute = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    UINT1               u1IsisEnabled = 0;

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRtm6ProcessUpdate ()\n"));

    if (IsisCtrlGetSysContext (pNetIpRtInfo->u4ContextId, &pContext) ==
        ISIS_FAILURE)
    {
        RTP_PT ((ISIS_LGST, "RTM <T> : Invalid Context ID [%u]\n",
                 pNetIpRtInfo->u4ContextId));
        DETAIL_FAIL (ISIS_CR_MODULE);
        return ISIS_FAILURE;
    }
    /*Apply configured Metic value on the route */
    if (u2ProtoId == ISIS_OSPF_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_OSPF;
    }
    else if (u2ProtoId == ISIS_STATIC_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_STATIC;
    }
    else if (u2ProtoId == ISIS_LOCAL_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_DIRECT;
        pIPIfRec = pContext->IPIfTable.pIPIfAddr;
        while (pIPIfRec != NULL)
        {
            if ((pIPIfRec->u1AddrType == ISIS_ADDR_IPV6) &&
                (pIPIfRec->u4CktIfIdx == pNetIpRtInfo->u4Index))
            {
                if (MEMCMP
                    (pNetIpRtInfo->Ip6Dst.u1_addr, pIPIfRec->au1IPAddr,
                     pNetIpRtInfo->u1Prefixlen))
                {
                    u1IsisEnabled = ISIS_TRUE;
                    break;
                }
            }
            pIPIfRec = pIPIfRec->pNext;
        }
    }
    else if (u2ProtoId == ISIS_RIP_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_RIP;
    }
    else if (u2ProtoId == ISIS_BGP_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_BGP;
    }
    if ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) &&
        (pNetIpRtInfo->u4Metric > ISIS_MAX_METRIC_MT))
    {
        pNetIpRtInfo->u4Metric = ISIS_MAX_METRIC_MT;
    }
    else if ((pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC) &&
             (pNetIpRtInfo->u4Metric > ISIS_LL_CKTL_MAX_METRIC))
    {
        pNetIpRtInfo->u4Metric = ISIS_LL_CKTL_MAX_METRIC;
    }
    if (pContext->RRDInfo.au4RRDMetric[u4ProtoId] != 0)
    {
        pNetIpRtInfo->u4Metric = pContext->RRDInfo.au4RRDMetric[u4ProtoId];
    }
    pRRDRoute = (tIsisRRDRoutes *) ISIS_MEM_ALLOC (ISIS_RRD_ROUTE, 0);
    if (pRRDRoute == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : RRD Route\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_RRD_ROUTE);
        return ISIS_FAILURE;
    }

    pRRDRoute->IPAddr.u1AddrType = ISIS_IPV6;
    MEMCPY (pRRDRoute->IPAddr.au1IpAddr, &(pNetIpRtInfo->Ip6Dst.u1_addr),
            ISIS_MAX_IP_ADDR_LEN);
    pRRDRoute->IPAddr.u1PrefixLen = pNetIpRtInfo->u1Prefixlen;
    pRRDRoute->IPAddr.u1Length = ISIS_MAX_IP_ADDR_LEN;
    pRRDRoute->u4Metric = pNetIpRtInfo->u4Metric;
    pRRDRoute->u1Protocol = (UINT1) u2ProtoId;
    RTP_PT ((ISIS_LGST,
             "Redistributing [%s] [%s] Route [%s] to ISIS - NextHop [%s] Metric [%u]\n",
             ISIS_GET_RRD_PROTO_STR (pRRDRoute->u1Protocol),
             ISIS_GET_ADDR_TYPE_STR (pRRDRoute->IPAddr.u1AddrType),
             Ip6PrintAddr ((tIp6Addr *) (VOID *) (pNetIpRtInfo->Ip6Dst.
                                                  u1_addr)),
             Ip6PrintAddr ((tIp6Addr *) (VOID *) (pNetIpRtInfo->NextHop.
                                                  u1_addr)),
             pRRDRoute->u4Metric));
    if (RBTreeAdd (pContext->RRDRouteRBTree, (tRBElem *) pRRDRoute) !=
        RB_SUCCESS)
    {
        RTP_PT ((ISIS_LGST,
                 "RTM <E> : RRD RBTree addition failure - route already present\n"));
        ISIS_MEM_FREE (ISIS_RRD_ROUTE, (UINT1 *) pRRDRoute);
        return (ISIS_FAILURE);
    }
    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        return ISIS_FAILURE;
    }

    pMDT->u1Cmd = ISIS_CMD_ADD;
    MEMCPY (pMDT->IPAddr.au1IpAddr, &(pNetIpRtInfo->Ip6Dst.u1_addr),
            ISIS_MAX_IP_ADDR_LEN);
    pMDT->IPAddr.u1Length = ISIS_MAX_IP_ADDR_LEN;
    pMDT->IPAddr.u1PrefixLen = pNetIpRtInfo->u1Prefixlen;
    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
    }
    else
    {
        pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
    }
    pMDT->u1SrcProtoId = (UINT1) u2ProtoId;
    if (u1IsisEnabled == ISIS_TRUE)
    {
        pMDT->u1IsisEnabled = ISIS_TRUE;
    }
    ISIS_COPY_METRIC (pContext, pNetIpRtInfo->u4Metric, pMDT);

    if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_1) ==
        ISIS_IMPORT_LEVEL_1)
    {
        pMDT1 = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

        if (pMDT1 == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            return ISIS_FAILURE;
        }
        MEMCPY (pMDT1, pMDT, sizeof (tIsisMDT));
        pMDT1->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
        if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT1) == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update"
                     " L1 RTM6 Route [%s] Into Self-LSP [%s]\n",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *) (pNetIpRtInfo->Ip6Dst.
                                                          u1_addr)),
                     ISIS_GET_LSP_STR (pMDT1->u1LSPType)));
        }
        else
        {
            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Updated RTM6 Route [%s] Into Self-LSP [%s]\n",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *) (pNetIpRtInfo->Ip6Dst.
                                                          u1_addr)),
                     ISIS_GET_LSP_STR (pMDT1->u1LSPType)));
        }
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
    }
    if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_2) ==
        ISIS_IMPORT_LEVEL_2)
    {
        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
        if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update"
                     " L2 RTM6 Route [%s] Into Self-LSP [%s]\n",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *) (pNetIpRtInfo->Ip6Dst.
                                                          u1_addr)),
                     ISIS_GET_LSP_STR (pMDT->u1LSPType)));
        }
        else
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Updated RTM6 Route [%s] in [%s]\n",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *) (pNetIpRtInfo->Ip6Dst.
                                                          u1_addr)),
                     ISIS_GET_LSP_STR (pMDT->u1LSPType)));
        }
    }
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRtm6ProcessUpdate ()\n"));

    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function        : IsisRtm6ProcessRtChange                                 */
/* Description     : This procedure scans the Route Update message from RTM  */
/*                   and sends the update message to ISIS task               */
/* Input           : pRtmMsg   -   ptr to the RTM message buffer             */
/*                 : pRtmHdr   -   ptr to RTM header                         */
/* Output          : None                                                    */
/* Returns         : RTM_SUCCESS, if processing is successful                */
/*                   RTM_FAILURE, otherwise                                  */
/*****************************************************************************/
INT1
IsisRtm6ProcessRtChange (tNetIpv6RtInfo * pNetIpRtInfo, UINT2 u2ProtoId)
{
    tIsisMDT           *pMDT = NULL;
    tIsisMDT           *pMDT1 = NULL;
    UINT4               u4ProtoId = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisRRDRoutes      RRDRoute;
    tIsisRRDRoutes     *pRRDRoute = NULL;

    RTP_EE ((ISIS_LGST, "RTM <X> : Entered IsisRtm6ProcessRtChange ()\n"));

    if (IsisCtrlGetSysContext (pNetIpRtInfo->u4ContextId, &pContext) ==
        ISIS_FAILURE)
    {
        RTP_PT ((ISIS_LGST, "UPD <T> : Invalid Context ID [%u]\n",
                 pNetIpRtInfo->u4ContextId));
        return ISIS_FAILURE;
    }
    /*Apply configured Metic value on the route */
    if (u2ProtoId == ISIS_OSPF_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_OSPF;
    }
    else if (u2ProtoId == ISIS_STATIC_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_STATIC;
    }
    else if (u2ProtoId == ISIS_LOCAL_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_DIRECT;
    }
    else if (u2ProtoId == ISIS_RIP_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_RIP;
    }
    else if (u2ProtoId == ISIS_BGP_ID)
    {
        u4ProtoId = ISIS_METRIC_INDEX_BGP;
    }

    if ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) &&
        (pNetIpRtInfo->u4Metric > ISIS_MAX_METRIC_MT))
    {
        pNetIpRtInfo->u4Metric = ISIS_MAX_METRIC_MT;
    }
    else if ((pContext->u1MetricStyle == ISIS_STYLE_NARROW_METRIC) &&
             (pNetIpRtInfo->u4Metric > ISIS_LL_CKTL_MAX_METRIC))
    {
        pNetIpRtInfo->u4Metric = ISIS_LL_CKTL_MAX_METRIC;
    }
    if (pContext->RRDInfo.au4RRDMetric[u4ProtoId] != 0)
    {
        pNetIpRtInfo->u4Metric = pContext->RRDInfo.au4RRDMetric[u4ProtoId];
    }
    MEMSET (&RRDRoute.IPAddr, 0, sizeof (tIsisIpAddr));
    RRDRoute.IPAddr.u1AddrType = ISIS_IPV6;
    MEMCPY (RRDRoute.IPAddr.au1IpAddr, &(pNetIpRtInfo->Ip6Dst.u1_addr),
            ISIS_MAX_IP_ADDR_LEN);
    RRDRoute.IPAddr.u1PrefixLen = pNetIpRtInfo->u1Prefixlen;
    pRRDRoute =
        (tIsisRRDRoutes *) RBTreeGet (pContext->RRDRouteRBTree,
                                      (tRBElem *) & RRDRoute);

    if ((pRRDRoute == NULL) && (pNetIpRtInfo->u4RowStatus == RTM_DESTROY))
    {
        RTP_PT ((ISIS_LGST,
                 "RTM <E> : Unable to update, Route Entry not present in RRD RBTree\n"));
        return (ISIS_FAILURE);
    }

    if ((pRRDRoute == NULL) && (pNetIpRtInfo->u4RowStatus == RTM_ACTIVE))
    {
        IsisRtm6ProcessUpdate (pNetIpRtInfo, u2ProtoId);
        return ISIS_SUCCESS;
    }

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
        return ISIS_FAILURE;
    }
    if (pNetIpRtInfo->u4RowStatus == RTM_DESTROY)
    {
        pMDT->u1Cmd = ISIS_CMD_DELETE;
        RBTreeRemove (pContext->RRDRouteRBTree, (tRBElem *) pRRDRoute);
        ISIS_MEM_FREE (ISIS_RRD_ROUTE, (UINT1 *) pRRDRoute);

    }
    if (pNetIpRtInfo->u4RowStatus == RTM_ACTIVE)
    {
        pMDT->u1Cmd = ISIS_CMD_MODIFY;
        pRRDRoute->u4Metric = pNetIpRtInfo->u4Metric;
    }
    if (pNetIpRtInfo->u4Metric > ISIS_LL_CKTL_MAX_METRIC)
    {
        pNetIpRtInfo->u4Metric = ISIS_LL_CKTL_MAX_METRIC;
    }
    if (pContext->RRDInfo.au4RRDMetric[u4ProtoId] != 0)
    {
        pNetIpRtInfo->u4Metric = pContext->RRDInfo.au4RRDMetric[u4ProtoId];
    }

    MEMCPY (pMDT->IPAddr.au1IpAddr, &(pNetIpRtInfo->Ip6Dst.u1_addr),
            ISIS_MAX_IP_ADDR_LEN);
    pMDT->IPAddr.u1Length = ISIS_MAX_IP_ADDR_LEN;
    pMDT->IPAddr.u1PrefixLen = pNetIpRtInfo->u1Prefixlen;
    if (pContext->u1IsisMTSupport == ISIS_TRUE)
    {
        pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
    }
    else
    {
        pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
    }
    pMDT->u1SrcProtoId = (UINT1) u2ProtoId;
    ISIS_COPY_METRIC (pContext, pNetIpRtInfo->u4Metric, pMDT);

    RTP_PT ((ISIS_LGST,
             "RTM <T> : (Redistribute) IPv6 Route Change - [%s] with NextHop [%s], Metric [%u]\n",
             Ip6PrintAddr ((tIp6Addr *) (VOID *) (pNetIpRtInfo->Ip6Dst.
                                                  u1_addr)),
             Ip6PrintAddr ((tIp6Addr *) (VOID *) (pNetIpRtInfo->NextHop.
                                                  u1_addr)),
             pNetIpRtInfo->u4Metric));
    if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_1) ==
        ISIS_IMPORT_LEVEL_1)
    {
        pMDT1 = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

        if (pMDT1 == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            return ISIS_FAILURE;
        }
        MEMCPY (pMDT1, pMDT, sizeof (tIsisMDT));
        pMDT1->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
        if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT1) == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update "
                     "L1 RTM route into SelfLSP\n"));
        }
        else
        {
            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Updated RTM6 Route in L1 Self-LSP\n"));
        }

        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
    }
    if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_2) ==
        ISIS_IMPORT_LEVEL_2)
    {
        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
        if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) == ISIS_FAILURE)
        {
            RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update"
                     " L2 RTM route into self LSP\n"));
        }
        else
        {
            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Updated RTM6 Route in L2 Self-LSP\n"));
        }
    }
    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

    RTP_EE ((ISIS_LGST, "RTM <X> : Exiting IsisRtm6ProcessRtChange ()\n"));

    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : IsisGetSubnetmasklen                                      */
/* Description   : Given a destination mask this function gives the          */
/*                 equivalent IP subnet prefix-length.                       */
/* Input(s)      : Destination mask (u4Mask)                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : IP Prefixlength (32 bit)                                  */
/*****************************************************************************/

UINT1
IsisGetSubnetmasklen (UINT4 u4Mask)
{
    UINT1               u1PLen = 0;

    while (u4Mask > 0)
    {
        u4Mask = u4Mask << 1;
        u1PLen++;
    }
    return u1PLen;
}

/******************************************************************************
 * Function    : IsisHandleRRDProtoDisable ()
 * Description : This Routine deletes the redistributed routes.
 *  Input (s)   : pContext     - Pointer to System Context
 *                u2ProtoMask  - Consists Delete routes from which protcol.
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of IPRA TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/
INT1
IsisHandleRRDProtoDisable (tIsisSysContext * pContext, UINT2 u2ProtoMask,
                           UINT1 u1Level)
{
    tIsisIPRATLV       *pTravTlv;
    tIsisIPRATLV       *pNextTlv;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pTravLsp;
    tIsisRRDRoutes     *pRRDRoute = NULL;
    tIsisRRDRoutes     *pRRDTempRoute = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    tIsisRRDRoutes      RRDRoute;
    UINT1               u1Protocol = 0;
    UINT1               u1RemBytes = 0;
    UINT1               u1TlvDelete = ISIS_TRUE;

    if (u1Level == ISIS_LEVEL1)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
    }
    else if (u1Level == ISIS_LEVEL2)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
    }
    if (pNSNLSP == NULL)
    {
        pRRDRoute = RBTreeGetFirst (pContext->RRDRouteRBTree);
        while (pRRDRoute != NULL)
        {
            pRRDTempRoute =
                RBTreeGetNext (pContext->RRDRouteRBTree, pRRDRoute, NULL);
            RBTreeRemove (pContext->RRDRouteRBTree, ((tRBElem *) pRRDRoute));
            ISIS_MEM_FREE (ISIS_RRD_ROUTE, (UINT1 *) pRRDRoute);
            pRRDRoute = pRRDTempRoute;
        }
        return ISIS_FAILURE;
    }
    pTravLsp = pNSNLSP->pNonZeroLSP;

    if (u2ProtoMask & ISIS_IMPORT_STATIC)
    {
        u1Protocol = STATIC_ID;
    }
    else if (u2ProtoMask & ISIS_IMPORT_DIRECT)
    {
        u1Protocol = LOCAL_ID;
    }
    else if (u2ProtoMask & ISIS_IMPORT_BGP)
    {
        u1Protocol = BGP_ID;
    }
    else if (u2ProtoMask & ISIS_IMPORT_RIP)
    {
        u1Protocol = RIP_ID;
    }
    else if (u2ProtoMask & ISIS_IMPORT_OSPF)
    {
        u1Protocol = OSPF_ID;
    }
    else
    {
        return ISIS_FAILURE;
    }
    RTP_PT ((ISIS_LGST, "ISIS <T> : Deleting Redistributed Routes From [%s]\n",
             ISIS_GET_RRD_PROTO_STR (u1Protocol)));
    while (pTravLsp != NULL)
    {
        pTravTlv = (tIsisIPRATLV *) RBTreeGetFirst (pTravLsp->IPRATLV);

        while (pTravTlv != NULL)
        {
            u1TlvDelete = ISIS_TRUE;
            pNextTlv = (tIsisIPRATLV *) RBTreeGetNext
                (pTravLsp->IPRATLV, (tRBElem *) pTravTlv, NULL);

            if ((pTravTlv->u1Protocol != 0) &&
                ((u2ProtoMask == ISIS_IMPORT_ALL) ||
                 (u1Protocol == pTravTlv->u1Protocol)))
            {
                MEMSET (&(RRDRoute.IPAddr), 0, sizeof (tIsisIpAddr));
                MEMCPY (&(RRDRoute.IPAddr), &(pTravTlv->IPAddr),
                        sizeof (tIsisIpAddr));
                if ((pTravTlv->u1Code == ISIS_IP_INTERNAL_RA_TLV)
                    || (pTravTlv->u1Code == ISIS_IP_EXTERNAL_RA_TLV)
                    || (pTravTlv->u1Code == ISIS_EXT_IP_REACH_TLV))
                {
                    RRDRoute.IPAddr.u1AddrType = ISIS_IPV4;
                }
                else if ((pTravTlv->u1Code == ISIS_IPV6_RA_TLV) ||
                         (pTravTlv->u1Code == ISIS_MT_IPV6_REACH_TLV))
                {
                    RRDRoute.IPAddr.u1AddrType = ISIS_IPV6;
                }

                pRRDRoute =
                    RBTreeGet (pContext->RRDRouteRBTree,
                               ((tRBElem *) & RRDRoute));
                if (pRRDRoute != NULL)
                {
                    /* For connected interfaces, check if ISIS is enabled on those interfaces 
                     * If ISIS is enabled, then do not delete those TLVs. Only remove the node
                     * from the RRDRBTree*/
                    if (u1Protocol == LOCAL_ID)
                    {
                        pIPIfRec = pContext->IPIfTable.pIPIfAddr;
                        while (pIPIfRec != NULL)
                        {
                            if (pIPIfRec->u1AddrType ==
                                RRDRoute.IPAddr.u1AddrType)
                            {
                                if (IsisUtlCompIPAddr
                                    (RRDRoute.IPAddr.au1IpAddr,
                                     RRDRoute.IPAddr.u1PrefixLen,
                                     pIPIfRec->au1IPAddr,
                                     RRDRoute.IPAddr.u1PrefixLen) == ISIS_TRUE)
                                {
                                    u1TlvDelete = ISIS_FALSE;
                                    break;
                                }
                            }
                            pIPIfRec = pIPIfRec->pNext;
                        }
                    }
                    RBTreeRemove (pContext->RRDRouteRBTree,
                                  ((tRBElem *) pRRDRoute));
                    ISIS_MEM_FREE (ISIS_RRD_ROUTE, (UINT1 *) pRRDRoute);
                }
                /*cheak remaining bytes logic - handle with subtracting additional 2 bytes(type+length) */
                switch (pTravTlv->u1Code)
                {
                        /*update no. of tlv in the lsp structure */
                    case ISIS_IP_INTERNAL_RA_TLV:
                    case ISIS_IP_EXTERNAL_RA_TLV:
                        if (u1TlvDelete == ISIS_TRUE)
                        {
                            pTravLsp->u1NumIPRATLV--;
                            u1RemBytes =
                                (UINT1) ISIS_GET_REM_BYTES (pTravLsp->
                                                            u1NumIPRATLV,
                                                            ISIS_IPRA_TLV_LEN);
                            if (u1RemBytes < ISIS_IPRA_TLV_LEN)
                            {
                                pTravLsp->u2LSPLen =
                                    (UINT2) (pTravLsp->u2LSPLen - 2);
                            }
                        }
                        RRDRoute.IPAddr.u1AddrType = ISIS_IPV4;
                        break;
                    case ISIS_IPV6_RA_TLV:
                        if (u1TlvDelete == ISIS_TRUE)
                        {
                            pTravLsp->u1NumIPV6RATLV--;
                            u1RemBytes =
                                (UINT1) ISIS_GET_REM_BYTES (pTravLsp->
                                                            u1NumIPV6RATLV,
                                                            ISIS_IP6RA_TLV_LEN);
                            if (u1RemBytes < ISIS_IP6RA_TLV_LEN)
                            {
                                pTravLsp->u2LSPLen =
                                    (UINT2) (pTravLsp->u2LSPLen - 2);
                            }
                        }
                        RRDRoute.IPAddr.u1AddrType = ISIS_IPV6;
                        break;
                    case ISIS_EXT_IP_REACH_TLV:
                        if (u1TlvDelete == ISIS_TRUE)
                        {
                            pTravLsp->u1NumExtIpReachTLV--;
                            u1RemBytes =
                                (UINT1) ISIS_GET_REM_BYTES (pTravLsp->
                                                            u1NumExtIpReachTLV,
                                                            ISIS_EXT_IP_REACH_TLV_LEN);
                            if (u1RemBytes < ISIS_EXT_IP_REACH_TLV_LEN)
                            {
                                pTravLsp->u2LSPLen =
                                    (UINT2) (pTravLsp->u2LSPLen - 2);
                            }
                        }
                        RRDRoute.IPAddr.u1AddrType = ISIS_IPV4;
                        break;
                    case ISIS_MT_IPV6_REACH_TLV:
                        if (u1TlvDelete == ISIS_TRUE)
                        {
                            pTravLsp->u1NumMTIpv6ReachTLV--;
                            u1RemBytes =
                                (UINT1) ISIS_GET_REM_BYTES (pTravLsp->
                                                            u1NumMTIpv6ReachTLV,
                                                            ISIS_MT_IPV6_REACH_TLV_LEN);
                            if (u1RemBytes < ISIS_MT_IPV6_REACH_TLV_LEN)
                            {
                                pTravLsp->u2LSPLen =
                                    (UINT2) (pTravLsp->u2LSPLen - 2);
                            }
                        }
                        RRDRoute.IPAddr.u1AddrType = ISIS_IPV6;
                        break;

                    default:
                        break;
                }
                if (u1TlvDelete == ISIS_TRUE)
                {
                    pTravLsp->u2LSPLen =
                        (UINT2) (pTravLsp->u2LSPLen - pTravTlv->u1Len);
                    RBTreeRemove (pTravLsp->IPRATLV, (tRBElem *) pTravTlv);
                    ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTlv);
                    pTravTlv = NULL;
                    pTravLsp->u1DirtyFlag = ISIS_MODIFIED;
                }
            }
            pTravTlv = pNextTlv;
        }
        pTravLsp = pTravLsp->pNext;
    }
    return ISIS_SUCCESS;
}
