/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *   
 *   $Id: ishelper.c,v 1.10 2016/03/26 09:54:43 siva Exp $
 *     
 *   Description: This file contains the main Routines associated with
 *                   Helper Module.
 *         
 *******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/******************************************************************************
 * Function Name : IsIsisGrHelping ()
 * Description   : This function checks whether the router is ready to help or
 *                 not 
 * 
 * Input(s)      : pContext  --Context
 *                 u1Level   --LEVEL1/LEVEL2
 *                 
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS/ISIS_FAILURE 
 ******************************************************************************/

PUBLIC INT4
IsIsisGrHelping (tIsisSysContext * pContext, UINT1 u1Level)
{
    if ((pContext->u1IsisGRHelperSupport == ISIS_GR_HELPER_NONE) ||
        (pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER) ||
        (pContext->u1IsisGRRestartMode == ISIS_GR_HELPER_DOWN) ||
        ((u1Level == ISIS_LEVEL1)&&(pContext->SysL1Info.u1IsisDbFullState == ISIS_DB_NOT_SYNC)) ||
        ((u1Level == ISIS_LEVEL2)&&(pContext->SysL2Info.u1IsisDbFullState == ISIS_DB_NOT_SYNC)))
    {
        return ISIS_FAILURE;
    }
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function Name : IsisGrExitHelper ()
 * Description   : This function makes the router to come out of the helping mode
 * 
 * Input(s)      : pAdjRec   --Adjacent Entry
 *                 u1IsisGrHelperExitReason -Helper Exit Reason
 *                 
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None 
 ******************************************************************************/

PUBLIC VOID
IsisGrExitHelper (tIsisSysContext * pContext,
                  tIsisAdjEntry * pAdjRec, UINT1 u1IsisGrHelperExitReason)
{
    UINT4               u4DirIdx = 0;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisEvtHelper     *pEvtHelper = NULL;
    tIsisCktLevel      *pCktLvlRec = NULL;
    UINT1               u1ECTId = 0;

    pAdjRec->u1IsisGRHelperExitReason = u1IsisGrHelperExitReason;
    pCktRec = pAdjRec->pCktRec;

    /*If a router is sending CSNP at the time of GR, then it needs to 
     * be stopped while exiting helper mode*/
    if (gu1IsisGrCsnp == ISIS_TRUE)
    {
        if ((pCktRec->u1CktLevel == ISIS_LEVEL1) ||
            (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            IsisUpdStopCSNPTimer (pContext, pCktRec, ISIS_LEVEL1);
            pCktLvlRec = pCktRec->pL1CktInfo;

            if (pCktLvlRec != NULL)
            {
                ISIS_ADJ_SET_ECTID (pCktLvlRec, 0);
                UPP_PT ((ISIS_LGST, "UPD <T> : Starting L1 PSNP Timer\n"));
                IsisTmrStartECTimer (pContext, ISIS_ECT_L1_PSNP,
                                     pCktRec->u4CktIdx,
                                     pCktLvlRec->PSNPInterval, &u1ECTId);

            }

        }

        if ((pCktRec->u1CktLevel == ISIS_LEVEL2) ||
            (pCktRec->u1CktLevel == ISIS_LEVEL12))
        {
            IsisUpdStopCSNPTimer (pContext, pCktRec, ISIS_LEVEL2);
            pCktLvlRec = pCktRec->pL2CktInfo;

            if (pCktLvlRec != NULL)
            {
                ISIS_ADJ_SET_ECTID (pCktLvlRec, 0);
                UPP_PT ((ISIS_LGST, "UPD <T> : Starting L2 PSNP Timer\n"));
                IsisTmrStartECTimer (pContext, ISIS_ECT_L2_PSNP,
                                     pCktRec->u4CktIdx,
                                     pCktLvlRec->PSNPInterval, &u1ECTId);

            }
        }
        UPP_PT ((ISIS_LGST, "UPD <T> : ECTId [ %u ]\n", u1ECTId));
        if (pCktLvlRec != NULL)
        {
            ISIS_ADJ_SET_ECTID (pCktLvlRec, u1ECTId);
        }

    }
    if (u1IsisGrHelperExitReason == ISIS_GR_HELPER_COMPLETED)
    {
        /*Restart Helper Timer with the Normal Hold time */
        u4DirIdx = IsisAdjGetDirEntryWithAdjIdx (pContext, pAdjRec);
        IsisTmrRestartECTimer (pContext, ISIS_ECT_HOLDING,
                               u4DirIdx, pAdjRec->u2HoldTime,
                               &pAdjRec->u1TmrIdx);
    }
    else
    {
        if (u1IsisGrHelperExitReason != ISIS_GR_HELPER_TIMEDOUT)
        {
            /*Make the Adjacecy Down */
            IsisAdjDelAdj (pContext, pAdjRec->pCktRec,
                           pAdjRec, ISIS_TRUE, &u4DirIdx);

            /* Stopping the Hold Timer started for this adjacency */
            IsisTmrStopECTimer (pContext, ISIS_ECT_HOLDING,
                                u4DirIdx, pAdjRec->u1TmrIdx);
        }
    }
    /*As routing table, needs to be updated */
    if (pAdjRec->u1IsisAdjGRState == ISIS_ADJ_SUPPRESSED)
    {
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            IsisDecSchedSPF (pContext, ISIS_LEVEL1);
        }
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            IsisDecSchedSPF (pContext, ISIS_LEVEL2);
        }
    }

    pAdjRec->u1IsisGRHelperStatus = ISIS_GR_NOT_HELPING;
    pAdjRec->u4StartTime = 0;
    
    /*Come out Helper Mode if not helping any of the adjacency */
    for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
         pCktRec = pCktRec->pNext)
    {
        pAdjEntry = pCktRec->pAdjEntry;
        while (pAdjEntry != NULL)
        {
            if (pAdjEntry->u1IsisGRHelperStatus == ISIS_GR_HELPING)
            {
                return;
            }
            pAdjEntry = pAdjEntry->pNext;
        }
    }
    if (u1IsisGrHelperExitReason == ISIS_GR_HELPER_TOP_CHG)
    {
        pContext->u1IsisGRRestartMode = ISIS_GR_HELPER_DOWN;
    }
    else
    {
        pContext->u1IsisGRRestartMode = ISIS_GR_NONE;
    }

    gu1IsisGrCsnp = ISIS_FALSE;

    pEvtHelper = (tIsisEvtHelper *)
        ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtHelper));
    if (pEvtHelper != NULL)
    {
        pEvtHelper->u1EvtID = ISIS_EVT_HELPER;
        MEMCPY (pEvtHelper->au1SysID, pContext->SysActuals.au1SysID,
                ISIS_SYS_ID_LEN);
        MEMCPY (pEvtHelper->au1AdjNbrSysID, pAdjRec->au1AdjNbrSysID,
                ISIS_SYS_ID_LEN);
        pEvtHelper->u1IsisGRHelperStatus = pAdjRec->u1IsisGRHelperStatus;
        pEvtHelper->u2AdjGRTime = pAdjRec->u2AdjGRTime;
        pEvtHelper->u1IsisGRHelperExitReason =
            pAdjRec->u1IsisGRHelperExitReason;
        IsisSnmpIfSendTrap ((UINT1 *) pEvtHelper, pEvtHelper->u1EvtID);
        ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtHelper);
    }
    return;
}

/******************************************************************************
 * Function Name : IsisGrHelperTxLSP  ()
 * Description   : This function transfer the LSA's to the restarter 
 *                 not 
 * 
 * Input(s)      : pContext  --Context
 *                 pCktRec   --Circuit Entry
 *                 pu1CSNP   --CSNP Packet
 *                 u1Level   --Level
 *                 
 * Outputs(s)    : None
 * Globals       : Not Referred or Modified
 * Returns       : None 
 ******************************************************************************/

PUBLIC VOID
IsisGrHelperTxLSP (tIsisSysContext * pContext,
                   tIsisCktEntry * pCktRec, UINT1 *pu1CSNP, UINT1 u1Level)
{

    UINT1               u1Len = 0;
    UINT1               u1Code = 0;
    UINT1               u1Loc = 0;
    UINT2               u2PDULen = 0;
    UINT2               u2Offset = 0;
    UINT2               u2Len = 0;
    INT4                i4Ret = ISIS_FAILURE;
    tIsisLSPEntry      *pRec = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;

    ISIS_GET_2_BYTES (pu1CSNP, (UINT2) sizeof (tIsisComHdr), u2PDULen);

    /* Get to the variable length fields where the LETLVs are included
     */

    u2Offset =
        (sizeof (tIsisComHdr) + ISIS_GR_OFFSET +
         (ISIS_GR_OFFSET * ISIS_LSPID_LEN) + ISIS_SYS_ID_LEN +
         ISIS_PNODE_ID_LEN);

    while (u2Offset < u2PDULen)
    {
        ISIS_GET_1_BYTE (pu1CSNP, u2Offset, u1Code);
        u2Offset++;

        ISIS_GET_1_BYTE (pu1CSNP, u2Offset, u1Len);
        u2Offset++;

        switch (u1Code)
        {
            case ISIS_AUTH_INFO_TLV:

                /* We don't need to consider authentication here
                 */

                UPP_PT ((ISIS_LGST, "UPD <T> : Ignoring Auth In CSNP\n"));
                u2Offset = (UINT2) (u2Offset + u1Len);
                break;
            case ISIS_LSP_ENTRY_TLV:

                while (u1Len > 0)
                {
                    pRec =
                        IsisUpdGetLSP (pContext,
                                       (pu1CSNP + u2Offset + ISIS_GR_OFFSET),
                                       u1Level, &pPrevRec, &pHashBkt, &u1Loc);
                    if (pRec != NULL)
                    {
                        ISIS_EXTRACT_PDU_LEN_FROM_LSP (pRec->pu1LSP, u2Len);
                        i4Ret = IsisUpdSchLSPForTx (pContext, (VOID *) pPrevRec,
                                (VOID *) pRec, pCktRec, u1Loc,
                                pHashBkt, ISIS_SPEC_CKT);
                        if (i4Ret == ISIS_MEM_FAILURE)
                        {
                            IsisUpdDeleteLSP (pContext, u1Level, 
                                             (pu1CSNP + u2Offset + ISIS_GR_OFFSET));
                        }
                        u2Offset =
                            (UINT2) (u2Offset + ISIS_LSPID_LEN +
                                     ISIS_LSPID_OFFSET);
                        u1Len =
                            (UINT1) (u1Len -
                                     (ISIS_LSPID_LEN + ISIS_LSPID_OFFSET));
                    }
                }
                break;
            default:
                break;

        }
    }
    return;
}
