/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isgr.c,v 1.18 2014/12/19 10:27:02 siva Exp $
 *
 *   Description: This file contains the routines associated with
 *                graceful restart. 
 *
 ******************************************************************************/
#include "isincl.h"
#include "isextn.h"

/***************************************************************************/
/*                                                                         */
/*     Function Name : IsisShutDownProcess                                 */
/*                                                                         */
/*     Description   : This routing triggers planned graceful shutdown     */
/*                     This is called from ISS module.                     */
/*                                                                         */
/*     Input(s)      : None                                                */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/

VOID
IsisGrShutDownProcess (VOID)
{
    tIsisSysContext    *pContext = NULL;
#ifdef CLI_WANTED
    UINT1               au1TaskName[] = "ISIS";
    UINT1              *pu1TaskName = au1TaskName;
#endif

    IsisLock ();

    for (pContext = gpIsisInstances;
         pContext != NULL; pContext = pContext->pNext)
    {
        /*Check any one context is having restart support. */
        if (((pContext->u1IsisGRRestartReason != ISIS_GR_UNKNOWN)
             && ((pContext->u1IsisGRRestartSupport == ISIS_GR_RESTART_PLANNED)
                 || (pContext->u1IsisGRRestartSupport ==
                     ISIS_GR_RESTART_BOTH))))
        {
            /*Planned GR */
            IsisGrInitiateRestart (ISIS_RESTARTING_ROUTER, ISIS_GR_SW_RESTART);
            break;
        }
    }

#ifdef CLI_WANTED
    /*Saving all cli commands for Graceful restart */
    IssCsrSaveCli (pu1TaskName);
#endif

    IsisUnlock ();
   if (pContext == NULL)
    {
        /*UnPlanned GR */
        IsisLock ();
        IsisShutDownAll ();
        IsisUnlock ();
        IsisDelSemandQue ();
        IssSetModuleSystemControl (ISIS_MODULE_ID, MODULE_SHUTDOWN);
        OsixDeleteTask (SELF, (const UINT1 *) "ISIS");
    }
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IsisGrInitiateRestart                               */
/*                                                                         */
/*     Description   : This routing initiate the graceful restart          */
/*                     Mark the RTM routes and send GR Restart request.    */
/*                                                                         */
/*     Input(s)      : u1RestartType and u1RestartReason                   */
/*                                                                         */
/*     Output(s)     : ISIS_SUCCESS/ISIS_FAILURE.                          */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/

PUBLIC INT4
IsisGrInitiateRestart (UINT1 u1RestartType, UINT1 u1RestartReason)
{
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisTimer         *pTimer = NULL;
    tIsisGrCheck       *pAckLst = NULL;
    tIsisEvtRestarter  *pEvtRestart = NULL;
    tRtmRegnId          RegnIdv4;
    tRtm6RegnId         RegnIdv6;
    MEMSET (&RegnIdv6, 0, sizeof (tRtm6RegnId));
    RegnIdv6.u2ProtoId = ISIS_ID;
    MEMSET (&RegnIdv4, 0, sizeof (tRtmRegnId));

    RegnIdv4.u2ProtoId = ISIS_ID;

    pContext = ISIS_GET_CONTEXT ();

    gu1IsisGrState = 0;            /*Global Status initialization */

    while (pContext != NULL)
    {
        if ((pContext->u1IsisGRRestartExitReason == ISIS_GR_RESTART_INPROGRESS)
            || (pContext->u1IsisGRRestartSupport == ISIS_GR_RESTART_NONE)
            || ((pContext->u1IsisGRRestartSupport != ISIS_GR_RESTART_BOTH)
                && (u1RestartType == ISIS_STARTING_ROUTER)))
        {
            /* Scan the next context */
            pContext = pContext->pNext;
            continue;
        }

        /*Restart reason is updated */
        pContext->u1IsisGRRestartReason = u1RestartReason;
        pContext->u1IsisGRRestartMode = ISIS_GR_RESTARTER;
        pContext->u1IsisGRRestarterState = ISIS_GR_SHUTDOWN;
        pContext->u1IsisGRRestartStatus = u1RestartType;

        /*Starting T3 Timer */
        pTimer = &(pContext->SysTimers.SysRestartT3Timer);
        pTimer->pContext = pContext;
        IsisTmrStartTimer (pTimer,
                           ISIS_GR_T3_TIMER, pContext->u2IsisGRT3TimerInterval);

        /*Initializing the SLL Ack list */
        /*Level-1 */
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            TMO_SLL_Init (&(pContext->SysL1Info.AckLst));
        }
        /*Level-2 */
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
            (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
        {
            TMO_SLL_Init (&(pContext->SysL2Info.AckLst));
        }

        /*Scanning the circuit for transmitting the restart TLV */
        for (pCktRec = pContext->CktTable.pCktRec; pCktRec != NULL;
             pCktRec = pCktRec->pNext)
        {
            /*Level-1 */
            if (((pCktRec->u1CktLevel == ISIS_LEVEL1) ||
                 (pCktRec->u1CktLevel == ISIS_LEVEL12))
                && (pCktRec->pL1CktInfo->u4NumAdjs != 0))
            {
                /*If circuit has no adjacency then no need to wait for RA */
                if (pCktRec->pAdjEntry != NULL)
                {
                    /*Adding the entry to Ack list */
                    pAckLst =
                        (tIsisGrCheck *) MemAllocMemBlk (gIsisMemPoolId.
                                                         u4AckRxPid);
                    if (pAckLst == NULL)
                    {
                        continue;
                    }

                    pAckLst->u4CktId = pCktRec->u4CktIdx;    /*Circuit ID */
                    TMO_SLL_Add ((&(pContext->SysL1Info.AckLst)),
                                 &pAckLst->next);
                }
                /*Sending the Restart request */
                IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL1, ISIS_FALSE);
            }

            /*Level-2 */
            if (((pCktRec->u1CktLevel == ISIS_LEVEL2) ||
                 (pCktRec->u1CktLevel == ISIS_LEVEL12))
                && (pCktRec->pL2CktInfo->u4NumAdjs != 0))
            {
                /*If circuit has no adjacency then no need to wait for RA */
                if (pCktRec->pAdjEntry != NULL)
                {
                    /*Adding the entry to Ack list */
                    pAckLst =
                        (tIsisGrCheck *) MemAllocMemBlk (gIsisMemPoolId.
                                                         u4AckRxPid);
                    if (pAckLst == NULL)
                    {
                        continue;
                    }

                    pAckLst->u4CktId = pCktRec->u4CktIdx;    /*Circuit ID */
                    TMO_SLL_Add ((&(pContext->SysL2Info.AckLst)),
                                 (&pAckLst->next));
                }
                IsisAdjTxHello (pContext, pCktRec, ISIS_LEVEL2, ISIS_FALSE);
            }

            if (pCktRec->pAdjEntry != NULL)
            {
                if (((((pCktRec->u1CktLevel == ISIS_LEVEL1) ||
                       (pCktRec->u1CktLevel == ISIS_LEVEL12)))
                     && (pCktRec->pL1CktInfo->u4NumAdjs != 0))
                    ||
                    ((((pCktRec->u1CktLevel == ISIS_LEVEL2)
                       || (pCktRec->u1CktLevel == ISIS_LEVEL12)))
                     && (pCktRec->pL2CktInfo->u4NumAdjs != 0)))
                {
                    /*Started  T1 timer for each interface */
                    IsisTmrStartECTimer (pContext, ISIS_GR_ECT_T1,
                                         pCktRec->u4CktIdx,
                                         pContext->u1IsisGRT1TimerInterval,
                                         &(pCktRec->u1IsisGRT1TmrIdx));
                }
            }
        }

        /*Status Change */
        pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_INPROGRESS;
        /*Disable Redistribution*/
        IsisSendingMessageToRRDQueue (pContext, ISIS_IMPORT_ALL, RTM_REDISTRIBUTE_DISABLE_MESSAGE );
        IsisSendingMessageToRRDQueue6 (pContext, ISIS_IMPORT_ALL, RTM_REDISTRIBUTE_DISABLE_MESSAGE );
        /* Indicate the RTM regarding Graceful shutdown */
        RtmIsisGrNotifInd (pContext);

        /*Stale Route marking for IPv4 */
        RtmGRStaleRtMark (&RegnIdv4);
        /* Indicate the RTM regarding Graceful shutdown */
        Rtm6IsisGrNotifInd (pContext);
        /*Stale Route marking for IPv6 */
        Rtm6GRStaleRtMark (&RegnIdv6);

        pEvtRestart = (tIsisEvtRestarter *)
            ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtRestarter));
        if (pEvtRestart != NULL)
        {
            pEvtRestart->u1EvtID = ISIS_EVT_RESTARTER;
            MEMCPY (pEvtRestart->au1SysID, pContext->SysActuals.au1SysID,
                    ISIS_SYS_ID_LEN);
            pEvtRestart->u1IsisGRRestartStatus =
                pContext->u1IsisGRRestartStatus;
            pEvtRestart->u2IsisGRT3TimerInterval =
                pContext->u2IsisGRT3TimerInterval;
            pEvtRestart->u1IsisGRRestartExitReason =
                pContext->u1IsisGRRestartExitReason;
            IsisSnmpIfSendTrap ((UINT1 *) pEvtRestart, pEvtRestart->u1EvtID);
            ISIS_MEM_FREE (ISIS_BUF_EVTS, (UINT1 *) pEvtRestart);
        }
        pContext = pContext->pNext;
    }
    return ISIS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrGetSecondsSinceBase                                  */
/*                                                                           */
/* Description  : This routine calculates the seconds from the obtained      */
/*                time. The seconds is calculated from the BASE year (2000)  */
/*                                                                           */
/* Input        : tUtlTm   -   Time format structure                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : The calculated time in seconds                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
IsisGrGetSecondsSinceBase (tUtlTm utlTm)
{
    UINT4               u4year = utlTm.tm_year;
    UINT4               u4Secs = 0;
    --u4year;
    while (u4year >= TM_BASE_YEAR)
    {
        u4Secs += SECS_IN_YEAR (u4year);
        --u4year;
    }
    u4Secs += (utlTm.tm_yday * SECS_IN_DAY);
    u4Secs += (utlTm.tm_hour * SECS_IN_HOUR);
    u4Secs += (utlTm.tm_min * SECS_IN_MINUTE);
    u4Secs += (utlTm.tm_sec);
    return u4Secs;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IsisGrStoreRestartInfo                              */
/*                                                                         */
/*     Description   : This routing preserve the ISIS information in a file*/
/*                     before restart.                                     */
/*                                                                         */
/*     Input(s)      : None                                                */
/*                                                                         */
/*     Output(s)     : ISIS_SUCCESS/ISIS_FAILURE.                          */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/

INT4
IsisGrStoreRestartInfo (VOID)
{

    tIsisSysContext    *pContext = NULL;
    tIsisGrInfo        *pGrInfo = NULL;
    tIsisGrCxtInfo    **pu1GrCxtinfo = NULL;
    UINT4               u4Cxtcount = 0;
    UINT4               u4SysTime = 0;
    tUtlTm              utlTm;
    UINT4               u4BufSize = 0;
    UINT4               u4Index = 0;
    UINT4               u4RemGracePeriod = 0;
    tIsisTimer         *pTimer = NULL;

    pContext = ISIS_GET_CONTEXT ();

    /* Allocate memory to hold the GR information. */
    u4BufSize = sizeof (tIsisGrInfo);
    pGrInfo = (tIsisGrInfo *) MemAllocMemBlk (gIsisMemPoolId.u4GrInfoPid);

    if (pGrInfo == NULL)
    {
        return ISIS_FAILURE;
    }
    CONTEXT_COUNT (pContext, &u4Cxtcount);
    MEMSET (pGrInfo, 0, u4BufSize);

    /*General Information */
    pGrInfo->u4CxtCount = u4Cxtcount;    /*Context Count */

    pContext = ISIS_GET_CONTEXT ();

    pu1GrCxtinfo = IsisUtlAllocMemGRInfoList ();
    if (pu1GrCxtinfo == NULL)
    {
        MemReleaseMemBlock (gIsisMemPoolId.u4GrInfoPid, (UINT1 *) pGrInfo);
        return ISIS_FAILURE;
    }
    MEMSET (pu1GrCxtinfo, 0, (u4Cxtcount * sizeof (UINT1)));
    /* Allocate memory to hold the GR Cxt information. */
    u4BufSize = sizeof (tIsisGrCxtInfo);

    u4Cxtcount = 0;

    /*Context Information */
    while (pContext != NULL)
    {
        pu1GrCxtinfo[u4Index] = IsisUtlAllocMemGRInfo ();

        if (pu1GrCxtinfo[u4Index] == NULL)
        {
            pContext = pContext->pNext;
            u4Index++;
            continue;
        }

        pTimer = &(pContext->SysTimers.SysRestartT3Timer);

        if (TmrGetRemainingTime
            (gIsisTmrListID, &(pTimer->Timer),
             &u4RemGracePeriod) != TMR_SUCCESS)
        {
            u4RemGracePeriod = 0;
        }
        else
        {
            u4RemGracePeriod = u4RemGracePeriod / NO_OF_TICKS_PER_SEC;
            TmrStopTimer (gIsisTmrListID, &(pTimer->Timer));
        }
        if (u4RemGracePeriod == 0)
        {
            pu1GrCxtinfo[u4Index]->u1RestartStatus = ISIS_GR_NONE;
        }
        else
        {
            pu1GrCxtinfo[u4Index]->u1RestartStatus =
                pContext->u1IsisGRRestartStatus;
        }

        /* Current system time */
        MEMSET (&utlTm, 0, sizeof (tUtlTm));
        UtlGetTime (&utlTm);
        /* Get the seconds since the base year (2000) */
        u4SysTime = IsisGrGetSecondsSinceBase (utlTm);

        /*Information to be stored */
        pu1GrCxtinfo[u4Index]->u4IsisCxtId = pContext->u4SysInstIdx;
        pu1GrCxtinfo[u4Index]->u1RestartReason =
            pContext->u1IsisGRRestartReason;
        /* software restart,switch to redundant... */
        pu1GrCxtinfo[u4Index]->u1ExitReason =
            pContext->u1IsisGRRestartExitReason;
        /* progress.. */
        pu1GrCxtinfo[u4Index]->u4GrEndTime = (u4SysTime + u4RemGracePeriod);
        u4Index++;
        pContext = pContext->pNext;
    }

    pGrInfo->pCxtinfo = pu1GrCxtinfo;

    /*Storing the structure info in  a file */
    IssStoreGraceContent (pGrInfo, ISIS_MODULE);

    /*Deallocation of memory */
    u4Index = 0;

    while (u4Index < pGrInfo->u4CxtCount)
    {
        if (pu1GrCxtinfo[u4Index] != NULL)
        {
            MemReleaseMemBlock (gIsisMemPoolId.u4GrCxtpid,
                                (UINT1 *) pu1GrCxtinfo[u4Index]);
        }
        u4Index++;
    }

    MemReleaseMemBlock (gIsisMemPoolId.u4GrCxtInfoPid, (UINT1 *) pu1GrCxtinfo);
    MemReleaseMemBlock (gIsisMemPoolId.u4GrInfoPid, (UINT1 *) pGrInfo);

    return ISIS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrRestoreRestartInfo                                 */
/*                                                                           */
/* Description  : This routine restores all Global GR specific configuration */
/*                from non-volatile storage.                                 */
/*                                                                           */
/* Input        : pContext                                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsisGrRestoreRestartInfo (tIsisSysContext * pContext)
{
    tUtlTm              utlTm;    /* Used to get the time */
    UINT4               u4IsisCxtId = 0;
    UINT4               u4SysTime = 0;    /* Current system time */
    UINT4               u4GraceEndTime = 0;    /* Absolute time at which GR
                                             * exits */
    UINT4               u4Index = 0;
    tIsisTimer         *pTimer = NULL;

    while (u4Index < gIsisGrinfo->u4CxtCount)
    {
        u4IsisCxtId = gIsisGrinfo->pCxtinfo[u4Index]->u4IsisCxtId;

        if (u4IsisCxtId == pContext->u4SysInstIdx)

        {
            pContext->u1IsisGRRestartReason =
                gIsisGrinfo->pCxtinfo[u4Index]->u1RestartReason;
            pContext->u1IsisGRRestartExitReason =
                gIsisGrinfo->pCxtinfo[u4Index]->u1ExitReason;
            if (gIsisGrinfo->pCxtinfo[u4Index]->u1RestartStatus ==
                ISIS_RESTARTING_ROUTER)
            {
                pContext->u1IsisGRRestartStatus = ISIS_RESTARTING_ROUTER;
                pContext->u1IsisGRRestartExitReason =
                    ISIS_GR_RESTART_INPROGRESS;
                pContext->u1IsisGRRestarterState = ISIS_GR_RESTART;
                pContext->u1IsisGRRestartMode = ISIS_GR_RESTARTER;
            }
            else if (gIsisGrinfo->pCxtinfo[u4Index]->u1RestartStatus ==
                     ISIS_RESTART_NONE)
            {
                pContext->u1IsisGRRestartStatus = ISIS_RESTART_NONE;
                pContext->u1IsisGRRestarterState = ISIS_GR_NONE;
                pContext->u1IsisGRRestartMode = ISIS_GR_NONE;
                u4Index++;
                continue;
            }
            u4GraceEndTime = gIsisGrinfo->pCxtinfo[u4Index]->u4GrEndTime;

            /* Store the 1s timer and current system time */
            MEMSET (&utlTm, 0, sizeof (tUtlTm));
            UtlGetTime (&utlTm);
            /* Get the seconds since the base year (2000) */
            u4SysTime = IsisGrGetSecondsSinceBase (utlTm);

            /* u4SysTime contains current system time
             * u4GraceEndTime contains the graceful restart endtime
             * Start the grace timer with the remaining time */
            if ((u4GraceEndTime <= u4SysTime) &&
                (pContext->u1IsisGRRestartExitReason ==
                 ISIS_GR_RESTART_INPROGRESS))
            {
                pContext->u1IsisGRRestartStatus = ISIS_RESTART_NONE;
                pContext->u1IsisGRRestarterState = ISIS_GR_NONE;
                pContext->u1IsisGRRestartMode = ISIS_GR_NONE;
            }
            else if ((u4GraceEndTime > u4SysTime) &&
                     (pContext->u1IsisGRRestartExitReason ==
                      ISIS_GR_RESTART_INPROGRESS))
            {
                /*Starting T3 Timer */
                pTimer = &(pContext->SysTimers.SysRestartT3Timer);
                pTimer->pContext = pContext;
                IsisTmrStartTimer (pTimer,
                                   ISIS_GR_T3_TIMER,
                                   (u4GraceEndTime - u4SysTime));
            }
        }
        u4Index++;
    }

}

/*****************************************************************************/
/* Function     : RbCompareLsp                                               */
/*                                                                           */
/* Description  : Compare function for the LSP                               */
/*                                                                           */
/* Input        : e1        Pointer to tNeighbor node1                       */
/*                e2        Pointer to tNeighbor node2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. ISIS_EQUAL if keys of both the elements are same.       */
/*                2. ISIS_LESS key of first element is less than second      */
/*                element key.                                               */
/*                3. ISIS_GREATER if key of first element is greater than    */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
RbCompareLsp (tRBElem * e1, tRBElem * e2)
{
    tLspInfo           *pFstLsp = e1;
    tLspInfo           *pScndLsp = e2;
    INT4                i4RetVal = 0;

    i4RetVal =
        MEMCMP ((pFstLsp->au1LSPId), (pScndLsp->au1LSPId), ISIS_LSPID_LEN);

    if (i4RetVal < 0)
    {
        return ISIS_RB_LESS;
    }
    else if (i4RetVal > 0)
    {
        return ISIS_RB_GREATER;
    }

    return ISIS_RB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrCheckAndShutdown                                     */
/*                                                                           */
/* Description  : This routine does the actions required on exiting  GR      */
/*                .                                                  */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsisGrCheckAndShutdown (VOID)
{
    tIsisSysContext    *pContext = NULL;

    pContext = ISIS_GET_CONTEXT ();
    while (pContext != NULL)
    {
        if (!((pContext->u1IsisGRShutState == ISIS_GR_SHUT) ||
              ((pContext->u1IsisGRShutState == ISIS_GR_NO_SHUT)
               && (pContext->u1IsisGRRestartSupport == ISIS_GR_RESTART_NONE))))
        {
            return;
        }
        pContext = pContext->pNext;
    }
    /*Store restart info */
    IsisGrStoreRestartInfo ();

    /*Shutdown all context */
    IsisShutDownAll ();
    /* Release the semaphore before deleting it */
    IsisUnlock ();
    IsisDelSemandQue ();
    /* Delete the ISIS  Task */
    IssSetModuleSystemControl (ISIS_MODULE_ID, MODULE_SHUTDOWN);
    OsixDeleteTask (SELF, (const UINT1 *) "ISIS");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrSelfLspGen                                           */
/*                                                                           */
/* Description  : This routine generates self LSP                            */
/*                .                                                          */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsisGrSelfLspGen (tIsisSysContext * pContext, UINT1 u1Level, UINT1 u1Flag)
{

    switch (u1Flag)
    {
        case OVERLOAD:
            IsisGrSelfOLLspGen (pContext, u1Level);
            break;
        case CLEAR_OVERLOAD:
            IsisGrSelfClearOLLspGen (pContext, u1Level);
            break;
        case SELF_LSP:
            IsisGRSendSelfLsp (pContext, u1Level);
            break;
        default:
            break;
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrSelfOLLspGen                                         */
/*                                                                           */
/* Description  : This routine generates self LSP with Overload flag set     */
/*                .                                                          */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsisGrSelfOLLspGen (tIsisSysContext * pContext, UINT1 u1Level)
{

    UINT1              *pu1LSPFlags = NULL;
    UINT1              *pu1MTLSPFlags = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;

    if (u1Level == ISIS_LEVEL1)
    {
        pu1LSPFlags = &(pContext->SelfLSP.u1L1LSPFlags);
        pu1MTLSPFlags = &(pContext->SelfLSP.au1L1MTLspFlag[ISIS_MT2_INDEX]);
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
    }
    else
    {
        pu1LSPFlags = &(pContext->SelfLSP.u1L2LSPFlags);
        pu1MTLSPFlags = &(pContext->SelfLSP.au1L2MTLspFlag[ISIS_MT2_INDEX]);
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
    }
    /*Overlload LSPshould be senr duwing restat. so differentiate 
     * with normal operation set gu1IsisGrOverLoad,after transfer it will be cleared*/
    gu1IsisGrOverLoad = OVERLOAD;

    /*Sending non Pseudonode Zero  LSP */
    if (pNSNLSP != NULL)
    {
        *pu1LSPFlags |= ISIS_DBOL_MASK;
        *pu1MTLSPFlags |= ISIS_MT_LSP_OFLAG_MASK;
        pNSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        IsisUpdChkAndTxSelfLSP (pContext, pNSNLSP->pZeroLSP, u1Level);
    }

    /*Sending Pseudonode Zero LSP */
    while (pSNLSP != NULL)
    {
        gu1IsisGrOverLoad = OVERLOAD;
        pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        IsisUpdChkAndTxSelfLSP (pContext, pSNLSP->pZeroLSP, u1Level);
        pSNLSP = pSNLSP->pNext;
    }
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrSelfClearOLLspGen                                    */
/*                                                                           */
/* Description  : This routine generates self LSP with Clear Overload flag   */
/*                .                                                          */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsisGrSelfClearOLLspGen (tIsisSysContext * pContext, UINT1 u1Level)
{

    UINT1              *pu1LSPFlags = NULL;
    UINT1              *pu1MTLSPFlags = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;

    if (u1Level == ISIS_LEVEL1)
    {
        pu1LSPFlags = &(pContext->SelfLSP.u1L1LSPFlags);
        pu1MTLSPFlags = &(pContext->SelfLSP.au1L1MTLspFlag[ISIS_MT2_INDEX]);
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
    }
    else
    {
        pu1LSPFlags = &(pContext->SelfLSP.u1L2LSPFlags);
        pu1MTLSPFlags = &(pContext->SelfLSP.au1L2MTLspFlag[ISIS_MT2_INDEX]);
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
    }

    /*Overlload LSPshould be sent during restart. so differentiate 
     * with normal operation set gu1IsisGrOverLoard.After transfer it will be cleared*/
    gu1IsisGrOverLoad = CLEAR_OVERLOAD;

    /*Sending non Pseudonode Zero  LSP */
    *pu1LSPFlags = (UINT1) ((*pu1LSPFlags) & ~ISIS_DBOL_MASK);
    *pu1MTLSPFlags = (UINT1) ((*pu1MTLSPFlags) & ~ISIS_MT_LSP_OFLAG_MASK);
    pNSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
    IsisUpdChkAndTxSelfLSP (pContext, pNSNLSP->pZeroLSP, u1Level);

    /*Sending Pseudonode Zero LSP */
    while (pSNLSP != NULL)
    {
        gu1IsisGrOverLoad = CLEAR_OVERLOAD;
        pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        IsisUpdChkAndTxSelfLSP (pContext, pSNLSP->pZeroLSP, u1Level);
        pSNLSP = pSNLSP->pNext;
    }
    if ((pContext->SysActuals.u1SysType != ISIS_LEVEL1)
        && (u1Level == ISIS_LEVEL2))
    {
        IsisUpdAddIPRAFromPath (pContext, u1Level);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGRSendSelfLsp                                          */
/*                                                                           */
/* Description  : This routine generates self LSP                            */
/*                                                                           */
/* Input        : pContext - pointer to context structure                    */
/*                u1Level  - LEVEL1/LEVEL2                                   */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsisGRSendSelfLsp (tIsisSysContext * pContext, UINT1 u1Level)
{

    tIsisSNLSP         *pSNLSP = NULL;
    tIsisLSPInfo       *pNZLSP = NULL;

    if (u1Level == ISIS_LEVEL1)
    {
        /* Level 1 Non-Pseudonode */
        if (pContext->SelfLSP.pL1NSNLSP != NULL)
        {
            /*Zero LSP */
            pContext->SelfLSP.pL1NSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
            gu1IsisGrOverLoad = SELF_LSP;
            IsisUpdChkAndTxSelfLSP (pContext,
                                    pContext->SelfLSP.pL1NSNLSP->pZeroLSP,
                                    u1Level);
            /*NonZero LSP */
            pNZLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
            while (pNZLSP != NULL)
            {
                pNZLSP->u1DirtyFlag = ISIS_MODIFIED;
                gu1IsisGrOverLoad = SELF_LSP;
                IsisUpdChkAndTxSelfLSP (pContext, pNZLSP, u1Level);
                pNZLSP = pNZLSP->pNext;
            }
        }
        /* Level 1 Pseudonode */
        if (pContext->SelfLSP.pL1SNLSP != NULL)
        {
            pSNLSP = pContext->SelfLSP.pL1SNLSP;
            while (pSNLSP != NULL)
            {
                /* Zero LSP */
                pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
                gu1IsisGrOverLoad = SELF_LSP;
                IsisUpdChkAndTxSelfLSP (pContext, pSNLSP->pZeroLSP, u1Level);
                /* Non-Zero LSPs */
                pNZLSP = pSNLSP->pNonZeroLSP;
                while (pNZLSP != NULL)
                {
                    pNZLSP->u1DirtyFlag = ISIS_MODIFIED;
                    gu1IsisGrOverLoad = SELF_LSP;
                    IsisUpdChkAndTxSelfLSP (pContext, pNZLSP, u1Level);
                    pNZLSP = pNZLSP->pNext;
                }
                pSNLSP = pSNLSP->pNext;
            }
        }
    }
    if (u1Level == ISIS_LEVEL2)
    {
        /* Level 2 Non-Pseudonode */
        if (pContext->SelfLSP.pL2NSNLSP != NULL)
        {
            /* Zero LSP */
            pContext->SelfLSP.pL2NSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
            gu1IsisGrOverLoad = SELF_LSP;
            IsisUpdChkAndTxSelfLSP (pContext,
                                    pContext->SelfLSP.pL2NSNLSP->pZeroLSP,
                                    u1Level);
            /* Non-Zero LSPs */
            pNZLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
            while (pNZLSP != NULL)
            {
                pNZLSP->u1DirtyFlag = ISIS_MODIFIED;
                gu1IsisGrOverLoad = SELF_LSP;
                IsisUpdChkAndTxSelfLSP (pContext, pNZLSP, u1Level);
                pNZLSP = pNZLSP->pNext;
            }
        }
        /* Level 2 Pseudonode */
        if (pContext->SelfLSP.pL2SNLSP != NULL)
        {
            pSNLSP = pContext->SelfLSP.pL2SNLSP;
            while (pSNLSP != NULL)
            {
                /* Zero LSP */
                pSNLSP->pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
                gu1IsisGrOverLoad = SELF_LSP;
                IsisUpdChkAndTxSelfLSP (pContext, pSNLSP->pZeroLSP, u1Level);
                /* Non-Zero LSPs */
                pNZLSP = pSNLSP->pNonZeroLSP;
                while (pNZLSP != NULL)
                {
                    pNZLSP->u1DirtyFlag = ISIS_MODIFIED;
                    gu1IsisGrOverLoad = SELF_LSP;
                    IsisUpdChkAndTxSelfLSP (pContext, pNZLSP, u1Level);
                    pNZLSP = pNZLSP->pNext;
                }
                pSNLSP = pSNLSP->pNext;
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrStartup                                              */
/*                                                                           */
/* Description  : This routine will do LIST initalization                    */
/*                and T2 timer start for GR                                  */
/*                                                                           */
/* Input        : pContext - pointer to context structure                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
IsisGrStartup (tIsisSysContext * pContext)
{
    tIsisTimer         *pTimer = NULL;
    UINT1               au1SemName[OSIX_NAME_LEN + 4];
	
    /*Initializing the SLL Ack and CSNP list - just for optimizing the check */
    /*Level-1 */
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        TMO_SLL_Init (&(pContext->SysL1Info.AckLst));
        TMO_SLL_Init (&(pContext->SysL1Info.CSNPLst));
    }
    /*Level-2 */
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
        TMO_SLL_Init (&(pContext->SysL2Info.AckLst));
        TMO_SLL_Init (&(pContext->SysL2Info.CSNPLst));
    }

    /*Starting T2 Timer based on the Level */
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL1) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
	MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
	IsisGetSemName (pContext, au1SemName);

        pTimer = &(pContext->SysL1Info.SysRestartT2Timer);
        pTimer->pContext = pContext;
        IsisTmrStartTimer (pTimer,
                           ISIS_GR_L1_T2_TIMER,
                           pContext->u2IsisGRT2TimerL1Interval);
        /*Creating the CSNP table for validation */
        pContext->SysL1Info.pGrCSNPTbl =
            RBTreeCreateEmbeddedExtended (ISIS_OFFSET (tLspInfo, RbNode),
                                 (tRBCompareFn) RbCompareLsp, ((UINT1 *) au1SemName));
    }
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL2) ||
        (pContext->SysActuals.u1SysType == ISIS_LEVEL12))
    {
	MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
	IsisGetSemName (pContext, au1SemName);

        pTimer = &(pContext->SysL2Info.SysRestartT2Timer);
        pTimer->pContext = pContext;
        IsisTmrStartTimer (pTimer,
                           ISIS_GR_L2_T2_TIMER,
                           pContext->u2IsisGRT2TimerL2Interval);
        /*Creating the CSNP table for validation */
        pContext->SysL2Info.pGrCSNPTbl =
            RBTreeCreateEmbeddedExtended (ISIS_OFFSET (tLspInfo, RbNode),
                                  (tRBCompareFn) RbCompareLsp, ((UINT1 *) au1SemName));
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IsisGrSendHello                                            */
/*                                                                           */
/* Description  : This routine will process GR Hello                         */
/*                                                                           */
/* Input        : pContext - pointer to context structure                    */
/*                pCktRec  - pointer to circuit structure                    */
/*                u1Level  - LEVEL1/LEVEL2                                   */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IsisGRHelloProcess (tIsisSysContext * pContext,
                    tIsisCktEntry * pCktRec, UINT1 u1Level)
{
    tIsisGrCheck       *pAckLst = NULL;
    tIsisGrCheck       *pCSNPLst = NULL;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisGrCheck       *pLstNode = NULL;

    if (pContext->u1IsisGRRestarterState == ISIS_GR_SHUTDOWN)
    {
        i4RetVal = ISIS_SUCCESS;
    }
    else if (pContext->u1IsisGRRestarterState == ISIS_GR_RESTART)
    {
        if (u1Level == ISIS_LEVEL1)
        {
            TMO_SLL_Scan (&(pContext->SysL1Info.AckLst), pLstNode,
                          tIsisGrCheck *)
            {
                if (pLstNode->u4CktId == pCktRec->u4CktIdx)
                {
                    break;
                }
            }
            if ((pLstNode == NULL)
                && ((pCktRec->u1IsisGRRestartL1State & ISIS_GR_ACK_RCVD) !=
                    ISIS_GR_ACK_RCVD) /*&& (pCktRec->pAdjEntry != NULL) */ )
            {
                pAckLst =
                    (tIsisGrCheck *) MemAllocMemBlk (gIsisMemPoolId.u4AckRxPid);
                if (pAckLst == NULL)
                {
                    return ISIS_FAILURE;
                }
                pCSNPLst =
                    (tIsisGrCheck *) MemAllocMemBlk (gIsisMemPoolId.
                                                     u4CSNPRxPid);
                if (pCSNPLst == NULL)
                {
                    MemReleaseMemBlock (gIsisMemPoolId.u4AckRxPid,
                                        (UINT1 *) pAckLst);
                    return ISIS_FAILURE;
                }
                pAckLst->u4CktId = pCktRec->u4CktIdx;    /*Circuit ID */
                pCSNPLst->u4CktId = pCktRec->u4CktIdx;
                TMO_SLL_Add ((&(pContext->SysL1Info.AckLst)), (&pAckLst->next));
                TMO_SLL_Add ((&(pContext->SysL1Info.CSNPLst)),
                             (&pCSNPLst->next));
                pCktRec->u1IsisGRL1HelloSend = ISIS_TRUE;
            }
        }
        else if (u1Level == ISIS_LEVEL2)
        {
            TMO_SLL_Scan (&(pContext->SysL2Info.AckLst), pLstNode,
                          tIsisGrCheck *)
            {
                if (pLstNode->u4CktId == pCktRec->u4CktIdx)
                {
                    break;
                }
            }
            if ((pLstNode == NULL)
                && ((pCktRec->u1IsisGRRestartL2State & ISIS_GR_ACK_RCVD) !=
                    ISIS_GR_ACK_RCVD) /* && (pCktRec->pAdjEntry != NULL) */ )
            {
                pAckLst =
                    (tIsisGrCheck *) MemAllocMemBlk (gIsisMemPoolId.u4AckRxPid);
                if (pAckLst == NULL)
                {
                    return ISIS_FAILURE;
                }
                pCSNPLst =
                    (tIsisGrCheck *) MemAllocMemBlk (gIsisMemPoolId.
                                                     u4CSNPRxPid);
                if (pCSNPLst == NULL)
                {
                    MemReleaseMemBlock (gIsisMemPoolId.u4AckRxPid,
                                        (UINT1 *) pAckLst);
                    return ISIS_FAILURE;
                }

                pAckLst->u4CktId = pCktRec->u4CktIdx;    /*Circuit ID */
                pCSNPLst->u4CktId = pCktRec->u4CktIdx;
                TMO_SLL_Add ((&(pContext->SysL2Info.AckLst)), (&pAckLst->next));
                TMO_SLL_Add ((&(pContext->SysL2Info.CSNPLst)),
                             (&pCSNPLst->next));
                pCktRec->u1IsisGRL2HelloSend = ISIS_TRUE;

            }
        }
        if ((pLstNode == NULL) && (((pCktRec->u1CktLevel == ISIS_LEVEL1) && (pCktRec->u1IsisGRL1HelloSend == ISIS_TRUE)) || ((pCktRec->u1CktLevel == ISIS_LEVEL2) && (pCktRec->u1IsisGRL2HelloSend == ISIS_TRUE)) || ((pCktRec->u1CktLevel == ISIS_LEVEL12) && ((pCktRec->u1IsisGRL1HelloSend == ISIS_TRUE) && (pCktRec->u1IsisGRL2HelloSend == ISIS_TRUE))))    /* &&
                                                                                                                                                                                                                                                                                                                                                                   (pCktRec->pAdjEntry != NULL) */ )
        {

            /* Increment the Grace LSA transmission count */
            pCktRec->u1IsisGRRestartHelloTxCount++;
            /*Started  T1 timer for each interface */
            IsisTmrStartECTimer (pContext, ISIS_GR_ECT_T1,
                                 pCktRec->u4CktIdx,
                                 pContext->u1IsisGRT1TimerInterval,
                                 &(pCktRec->u1IsisGRT1TmrIdx));
        }

        i4RetVal = ISIS_SUCCESS;
    }

    return i4RetVal;
}
