/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: isupdgen.c,v 1.32 2017/09/11 13:44:09 siva Exp $
 *
 *   Description: This file contains the main Routines associated with
 *                Update Module. 
 *
 ******************************************************************************/

#include "isincl.h"
#include "isextn.h"

/* Function Prototypes
 */

PRIVATE INT4        IsisUpdDelAreaAddrTLV (tIsisSysContext *, tIsisMDT *);
PRIVATE INT4        IsisUpdAddIPIfAddrTLV (tIsisSysContext *, tIsisMDT *);
PRIVATE INT4        IsisUpdDelProtSuppTLV (tIsisSysContext *, tIsisMDT *);
PRIVATE INT4        IsisUpdDelIPIfAddrTLV (tIsisSysContext *, tIsisMDT *);
PRIVATE INT4        IsisUpdGetSelfLSPBuffer (tIsisSysContext *, tIsisLSPInfo **,
                                             UINT1, UINT4, UINT1, UINT1 *);
PRIVATE VOID        IsisUpdInsPSTLVInSelfLSP (tIsisSysContext *, tIsisMDT *,
                                              tIsisLSPInfo **, UINT4);
PRIVATE VOID        IsisUpdDelPSTLVFromSelfLSP (tIsisLSPInfo *, tIsisMDT *);
PRIVATE VOID        IsisUpdDelIPIfTLVFromSelfLSP (tIsisSysContext *, UINT1,
                                                  tIsisLSPInfo *, tIsisMDT *,
                                                  UINT1);
PRIVATE VOID        IsisUpdInsIPIfAddrTLVInSelfLSP (tIsisSysContext *,
                                                    tIsisMDT *, tIsisLSPInfo *,
                                                    UINT4, UINT1, UINT1 *);
PRIVATE VOID        IsisUpdInsIPV6IfAddrTLVInSelfLSP (tIsisSysContext *,
                                                      tIsisMDT *,
                                                      tIsisLSPInfo *, UINT4,
                                                      UINT1, UINT1 *);
PRIVATE VOID        IsisUpdAddSelfLSP (tIsisSysContext *, UINT1, tIsisLSPInfo *,
                                       UINT1 *);
PRIVATE INT4
       IsisUpdAddDynHostNmeTLV (tIsisSysContext * pContext, tIsisMDT * pMDT);
INT4
 
 
 
         IsisUpdDelDynHostNmeTLV (tIsisSysContext * pContext, tIsisMDT * pMDT);

/* Self-LSPs Updation
 * ------------------
 * LOGIC 
 * The LSP buffer holds information regarding all TLVs that are 
 * to be part of the LSP when it is constructed. It also holds the total
 * number of such TLVs that are currently held in the buffer. If the total
 * number of TLVs does not fit into a single buffer, a new buffer is
 * allocated (only for Non-Zero LSP case) and the new TLV added to the new
 * buffer. If not the information to be updated is added to the current
 * buffer. While calculating the buffer size we may have to include the Code
 * and Length bytes for each TLV we add. Since a single TLV can hold only
 * 255 bytes of information, we may have to decide whether the new
 * information being added exceeds this limit. If so the buffer size is
 * incremented by '2', for code and length, along with the information
 * length. Otherwise the buffer length is incremented by the information
 * length and the new information added to the current buffer
 *
 * For e.g., let the information be I whose length is '4' bytes.
 * Max Size of TLV is '0xFF'
 * Max Buffer size is 1492
 *
 * Case I:   No information is present and the first information element 'I'
 *           is being added
 *           Action - Direclty add the element, increment the buffer size by
 *           length of 'I' which is 4 bytes and also add '2' for code and
 *           length
 *
 * Case II:  '5' Information elements already present and a new information
 *           element 'I' is being added
 *           Action - '5' elements occupy '5 * 4 = 20' bytes and add '2' for
 *           code and length, the total is '22' bytes. The new information
 *           element will add another '4' bytes which is less than 1492 and
 *           also less than '0xFF'. Add it and increment the buffer length
 *           by '4'
 *
 * Case III: '63' Information elements already present and a new Information
 *           element 'I' is being added
 *           Action - '63' elements will occupy '63 * 4 = 252' bytes and
 *           along with '2' bytes for code and length, they totally occupy
 *           254 bytes. The new information element is '4' bytes which if
 *           added to the buffer will make the size '258' which is more than
 *           the size that a single TLV can hold. So we may have to add a
 *           new TLV, which means the buffer size must now be incremented by
 *           '4' for information element and '2' for the new TLVs code and
 *           length. Do it
 *
 * Case IV: Addition of new Information element will exceed buffer size
 *          Action - For Zero-LSP ignore the new information element, and
 *          for Non-Zero-LSPs create a new buffer anf repeat steps I, II or
 *          III as applicable
 *
 * How to find out whether the addition of a new information element causes
 * a new TLV to be added:
 *
 * The LSP buffer has a field which holds the number of TLVs that are
 * currently held in the buffer. Let us call this 'N'. It also holds buffer
 * length, call this 'L'. Let the length of the information element be 'l'.
 * Maxsize of a TLV is 0xFF.
 *
 * Formula: if ((255 - ((N * l) % 255)) < l) then a new TLV must be added, 
 * otherwise the new information element can be directly added without adding 
 * the '2' bytes for code and length
 */

/******************************************************************************
 * Function    : IsisUpdGetSelfLSPBuffer ()
 * Description : This Routine goes through the list of NonZero SelfLSP buffers 
 *               and returns a buffer which has enough space to accomodate the 
 *               new TLV information. This routine is invoked while updating 
 *               IS Adjacency, Summary Address, IPRA, IP I/f Address 
 *               Information into the Self LSPs
 * Input (s)   : pContext      - Pointer to the system context
 *               pNonZeroLSP   - Pointer to the list of Non Zero LSPs (may be
 *                               NULL)
 *               u1TlvType     - Type of the TLV information to be added to
 *                               the SelfLSP
 *               u4LSPBufSize  - Maximum buffer size allowed for the LSP
 *               u1LSPType     - Type of LSP, L1 pseudonode / L1 non-pseudonode
 *                               L2 pseudonode / L2 non-pseudonode
 *               pu1CurrLSPNum - LSP number to be assigned in case a new SelfLSP
 *                               buffer is allocated
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, if a SelfLSP buffer is either found or a New
 *                             SelfLSP buffer is allocated
 *               ISIS_FAILURE, Otherwise              
 ******************************************************************************/

PRIVATE INT4
IsisUpdGetSelfLSPBuffer (tIsisSysContext * pContext,
                         tIsisLSPInfo ** pNonZeroLSP, UINT1 u1TlvType,
                         UINT4 u4LSPBufSize, UINT1 u1LSPType,
                         UINT1 *pu1CurrLSPNum)
{
    UINT1               u1RemBytes = 0;
    UINT1               u1NumTlv = 0;
    UINT1               u1TlvLen = 0;
    UINT1               au1SemName[OSIX_NAME_LEN + 4];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdGetSelfLSPBuffer () \n"));

    /* Check If we have a NonZero-LSP which has enough space for the new 
     * TLV information
     */

    u1TlvLen = ISIS_GET_TLV_LEN (u1TlvType);

    if ((pu1CurrLSPNum == NULL) || (u1TlvLen == 0))
    {
        return ISIS_FAILURE;
    }

    while (*pNonZeroLSP != NULL)
    {
        /* Refer to LOGIC of Self-LSP Updation explained @ the beginning of 
         * this file
         */

        u1NumTlv = (UINT1) ISIS_GET_NUM_TLV ((*pNonZeroLSP), u1TlvType);

        u1RemBytes = (UINT1) ISIS_GET_REM_BYTES (u1NumTlv, u1TlvLen);

        if (u1RemBytes < u1TlvLen)
        {
            /* We have to add a new TLV for holding the new information since
             * the remaining bytes are not sufficient to hold the TLV
             * information with the existing TLVs
             */

            if (((*pNonZeroLSP)->u2LSPLen + (u1TlvLen + 2))
                > (UINT2) u4LSPBufSize)
            {
                /* We cannot add the information to the LSP buffer, move further
                 */

                *pNonZeroLSP = (*pNonZeroLSP)->pNext;
            }
            else
            {
                /* The buffer has enough space to add a new TLV. Mark the buffer
                 * since the new information will anyway be added to the buffer
                 */

                (*pNonZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
                (*pNonZeroLSP)->u2LSPLen += (u1TlvLen + 2);
                if (u1TlvType == ISIS_IS_ADJ_TLV)
                {
                    (*pNonZeroLSP)->u2LSPLen += 1;
                }
                break;
            }
        }

        /* We need not add a new TLV, but check whether we have enough space
         * to add information directly into the buffer.
         */

        else if (((*pNonZeroLSP)->u2LSPLen + u1TlvLen) > (UINT2) u4LSPBufSize)
        {
            /* Not enough space in the buffer, Move further
             */

            *pNonZeroLSP = (*pNonZeroLSP)->pNext;
        }
        else
        {
            /* A new TLV need not be added and there is enough space in the 
             * buffer. Mark it
             */

            if (u1NumTlv == 0)
            {
                (*pNonZeroLSP)->u2LSPLen += 2;
                if (u1TlvType == ISIS_IS_ADJ_TLV)
                {
                    (*pNonZeroLSP)->u2LSPLen += 1;
                }
            }
            (*pNonZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
            (*pNonZeroLSP)->u2LSPLen += u1TlvLen;
            break;
        }
    }

    /* Allocation of Buffer for the NonZeroLSP and updating the Contents
     */

    if (*pNonZeroLSP == NULL)
    {
        if ((*pNonZeroLSP =
             (tIsisLSPInfo *) ISIS_MEM_ALLOC (ISIS_BUF_SLSP,
                                              sizeof (tIsisLSPInfo))) != NULL)
        {
            (*pNonZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
            (*pNonZeroLSP)->u1LSPNum = *pu1CurrLSPNum;
            (*pNonZeroLSP)->u4SeqNum = 0;
            (*pNonZeroLSP)->u1NumAATLV = 0;
            (*pNonZeroLSP)->u1NumPSTLV = 0;
            (*pNonZeroLSP)->u1NumISTLV = 0;
            (*pNonZeroLSP)->u1NumIPIfTLV = 0;
            (*pNonZeroLSP)->u1NumIPRATLV = 0;
            (*pNonZeroLSP)->u2LSPLen = (UINT2) (2 + u1TlvLen);
            if (u1TlvType == ISIS_IS_ADJ_TLV)
            {
                /* Reserve Space for Virtual Flag
                 */

                (*pNonZeroLSP)->u2LSPLen += 1;
            }

            /* Create IPRA RBTree only for Non Pseudo Non Zero LSP */
            if ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP) ||
                (u1LSPType == ISIS_L2_NON_PSEUDO_LSP))
            {
                MEMSET (au1SemName, '\0', OSIX_NAME_LEN + 4);
                IsisGetSemName (pContext, au1SemName);

                (*pNonZeroLSP)->IPRATLV =
                    RBTreeCreateEmbeddedExtended (0, IsisUpdRBTreeIPRATLVCmp,
                                                  ((UINT1 *) au1SemName));
            }

            /* Link this New LSP to the SysContext
             * and Update the Current LSP Number
             */

            IsisUpdAddSelfLSP (pContext, u1LSPType, *pNonZeroLSP,
                               pu1CurrLSPNum);

        }
        else
        {
            /* Existing buffers cannot accomodate the new information and we are
             * unable to allocate a new buffer to hold the information. 
             */

            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : SelfLSP Buffer\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdGetSelfLSPBuffer ()\n"));
            return (ISIS_FAILURE);
        }
    }
    return (ISIS_SUCCESS);
}

/******************************************************************************
 * Function    : IsisUpdAddSelfLSP ()
 * Description : This Routine inserts the Self LSP Buffer in the pseudonode /
 *               non-pseudonode LSP. This routine also updates the next
 *               available LSP Number.
 * Input (s)   : pContext      - Pointer to the system context
 *               u1LSPType     - Type of LSP, L1 pseudonode / L1 non-pseudonode

 *                               L2 pseudonode / L2 non-pseudonode
 *               pNonZeroLSP   - Pointer to the Non Zero LSPs which has to
 *                               be inserted
 *               pu1CurrLSPNum - LSP number to be assigned in case a new SelfLSP
 *                               buffer is allocated
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdAddSelfLSP (tIsisSysContext * pContext, UINT1 u1LSPType,
                   tIsisLSPInfo * pNonZeroLSP, UINT1 *pu1CurrLSPNum)
{
    UINT1               u1NextLSPNum = 0;
    UINT1               u1AddFlag = ISIS_FALSE;
    UINT1               u1NumFlag = ISIS_FALSE;
    tIsisLSPInfo       *pNZLSP = NULL;
    tIsisLSPInfo       *pPrev = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddSelfLSP ()\n"));

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNZLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
        if (pNZLSP == NULL)
        {
            /* This is the first non-zero LSP, to be added, hence
             * add the LSP to context, and increment the current
             * LSP number (the initialized value of current sequence
             * number is '1', hence the next available number is the 
             * increment of '1').
             */

            pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;

            /* Update the pNZLSP as this should be always the First Entry
             */

            pNZLSP = pNonZeroLSP;

        }
        else if (pNonZeroLSP->u1LSPNum < pNZLSP->u1LSPNum)
        {
            /* The LSP to be inserted at Head, Insert LSP and set the 
             * 'u1AddFlag' and continue with updating the next available
             * LSP Number.
             */
            pNonZeroLSP->pNext = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
            pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;

            /* Update the pNZLSP as this should be always the First Entry
             */

            pNZLSP = pNonZeroLSP;

        }
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNZLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
        if (pNZLSP == NULL)
        {
            pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;

            /* Update the pNZLSP as this should be always the First Entry
             */

            pNZLSP = pNonZeroLSP;

        }
        else if (pNonZeroLSP->u1LSPNum < pNZLSP->u1LSPNum)
        {
            pNonZeroLSP->pNext = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
            pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;

            /* Update the pNZLSP as this should be always the First Entry
             */

            pNZLSP = pNonZeroLSP;

        }
    }
    else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
    {
        pNZLSP = pContext->SelfLSP.pL1SNLSP->pNonZeroLSP;
        if (pNZLSP == NULL)
        {
            pContext->SelfLSP.pL1SNLSP->pNonZeroLSP = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;

            /* Update the pNZLSP as this should be always the First Entry
             */

            pNZLSP = pNonZeroLSP;

        }
        else if (pNonZeroLSP->u1LSPNum < pNZLSP->u1LSPNum)
        {
            pNonZeroLSP->pNext = pContext->SelfLSP.pL1SNLSP->pNonZeroLSP;
            pContext->SelfLSP.pL1SNLSP->pNonZeroLSP = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;

            /* Update the pNZLSP as this should be always the First Entry
             */

            pNZLSP = pNonZeroLSP;

        }
    }
    else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
    {
        pNZLSP = pContext->SelfLSP.pL2SNLSP->pNonZeroLSP;
        if (pNZLSP == NULL)
        {
            pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;

            /* Update the pNZLSP as this should be always the First Entry
             */

            pNZLSP = pNonZeroLSP;

        }
        else if (pNonZeroLSP->u1LSPNum < pNZLSP->u1LSPNum)
        {
            pNonZeroLSP->pNext = pContext->SelfLSP.pL2SNLSP->pNonZeroLSP;
            pContext->SelfLSP.pL2SNLSP->pNonZeroLSP = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;

            /* Update the pNZLSP as this should be always the First Entry
             */

            pNZLSP = pNonZeroLSP;

        }
    }

    /* Initialize the Next Available number to minimum possible value i.e '1'
     */

    u1NextLSPNum = 1;
    pPrev = NULL;

    while (pNZLSP != NULL)
    {
        if ((u1NumFlag != ISIS_TRUE) && (u1NextLSPNum < pNZLSP->u1LSPNum))
        {
            /* The next available LSP number is obtained, if the LSP is inserted
             * then, come out of the loop.
             */

            *pu1CurrLSPNum = u1NextLSPNum;
            u1NumFlag = ISIS_TRUE;
            if (u1AddFlag == ISIS_TRUE)
            {
                break;
            }
        }
        else
        {
            /* This number is already allocated, hence try the next 
             * number
             */

            u1NextLSPNum++;
        }
        if ((u1AddFlag != ISIS_TRUE)
            && (pNonZeroLSP->u1LSPNum < pNZLSP->u1LSPNum) && (pPrev != NULL))
        {
            /* Found a place where we can insert the LSP, insert the LSP.
             * if next available LSP Number is already updated, then come
             * out of loop.
             */

            pNonZeroLSP->pNext = pNZLSP;
            pPrev->pNext = pNonZeroLSP;
            u1AddFlag = ISIS_TRUE;
            if (u1NumFlag == ISIS_TRUE)
            {
                break;
            }
        }
        pPrev = pNZLSP;
        pNZLSP = pNZLSP->pNext;
    }

    if ((u1AddFlag != ISIS_TRUE) && (pPrev != NULL))
    {
        /* No place found in the existing List, Hence add the LSP at
         * the End of the List.
         *
         * NOTE: pPrev will contain the Last LSP Buffer.
         */

        pPrev->pNext = pNonZeroLSP;
        u1NextLSPNum++;
    }

    if (u1NumFlag != ISIS_TRUE)
    {
        /* The Current LSP Number is not updated since all the LSPs
         * are ordered, hence chose the next number and assign it.
         */

        *pu1CurrLSPNum = u1NextLSPNum;
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddSelfLSP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdUpdateIPRA ()
 * Description : This routine updates the SelfLSP database with the given IPRA
 *               information. 'u1Cmd' specifies whether the action is "ADD",
 *               "MODIFY" or "DELETE". This routine allocates an MDT structure,
 *               u[dates the structure with the given IPRA information and
 *               performs the specified action
 * Input (s)   : pContext  - Pointer to System Context
 *               puIPRARec - Pointer to IPRA Record
 *               u1Cmd     - Command (ADD/DELETE/MODIFY)
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisUpdUpdateIPRA (tIsisSysContext * pContext, tIsisIPRAEntry * pIPRARec,
                   UINT1 u1Cmd)
{
    tIsisCktEntry      *pCktRec = NULL;
    tIsisMDT           *pMDT = NULL;
    tIsisMDT           *pL2MDT = NULL;
    UINT4               u4Status;
    INT4                i4Retval = ISIS_FAILURE;
    UINT1               u1CtrlOctet = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdUpdateIPRA ()\n"));

    i4Retval = IsisAdjGetCktRecWithIfIdx (pIPRARec->u4IfIndex,
                                          pIPRARec->u4IfSubIndex, &pCktRec);

    UNUSED_PARAM (i4Retval);

    /* MDT - Multi Data Type which can hold information regarding
     *  - IPRA
     *  - Protocol Supported
     *  - IP Interface Address
     *  - Area Addresses
     *
     * Currently used for updating IPRA
     */

    pMDT = (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

    if (pMDT == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL ": MDT\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
    }
    else if ((pIPRARec->Metric[0] & ISIS_METRIC_TYPE_MASK)
             == ISIS_METRIC_TYPE_MASK)
    {
        /* RFC 1195 allows only Default Metric for IPRAs. FutureISIS supports
         * all the four metrics for IPRAs through configuration. If Default
         * Metric is of type External, we expect the other 3 metrics also to
         * follow suit. Hence only the Default Metric Type is checked
         */

        if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
        {
            /* External IP reachability information cannot be present in Level1
             * LSPs
             */

            pMDT->u1Cmd = u1Cmd;
            pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
            if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
            {
                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPRARec->au1IPRADest,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;

                if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                {
                    pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
                }
                else
                {
                    pMDT->u1TLVType = ISIS_IP_EXTERNAL_RA_TLV;
                }
            }
            else if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV6)
            {
                ISIS_IN6_IS_ADDR_LINKLOCAL (pIPRARec->au1IPRADest, u4Status);

                /*Link Local Check */
                if (u4Status == ISIS_TRUE)

                {
                    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                    return;
                }

                MEMCPY (pMDT->IPAddr.au1IpAddr, pIPRARec->au1IPRADest,
                        ISIS_MAX_IPV6_ADDR_LEN);
                pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;

                if (pContext->u1IsisMTSupport == ISIS_TRUE)
                {
                    pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
                }
                else
                {
                    pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
                }

                u1CtrlOctet = pMDT->u1CtrlOctet;
                pMDT->u1CtrlOctet =
                    ISIS_SET_MET_TYPE (u1CtrlOctet, ISIS_EXTERNAL_METRIC);

            }
            else
            {
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                return;
            }
            ISIS_SET_METRIC (pContext, pMDT, pIPRARec);
            pMDT->IPAddr.u1PrefixLen = pIPRARec->u1PrefixLen;
            IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
        }
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    }
    else
    {
        /* Configured IPRA is internal 
         */

        /* pMDT holds Level1 IPRA information 
         */

        pMDT->u1Cmd = u1Cmd;
        pMDT->u1LSPType
            = (UINT1) (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
                        || (pContext->SysActuals.u1SysType == ISIS_LEVEL12)) ?
                       ISIS_L1_NON_PSEUDO_LSP : ISIS_L2_NON_PSEUDO_LSP);
        if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV4)
        {
            MEMCPY (pMDT->IPAddr.au1IpAddr, pIPRARec->au1IPRADest,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;

            if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
            {
                pMDT->u1TLVType = ISIS_EXT_IP_REACH_TLV;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IP_INTERNAL_RA_TLV;
            }

        }
        else if (pIPRARec->u1IPRADestType == ISIS_ADDR_IPV6)
        {
            ISIS_IN6_IS_ADDR_LINKLOCAL (pIPRARec->au1IPRADest, u4Status);

            /*Link Local Check */
            if (u4Status == ISIS_TRUE)

            {
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                return;
            }

            MEMCPY (pMDT->IPAddr.au1IpAddr, pIPRARec->au1IPRADest,
                    ISIS_MAX_IPV6_ADDR_LEN);
            pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;

            if (pContext->u1IsisMTSupport == ISIS_TRUE)
            {
                pMDT->u1TLVType = ISIS_MT_IPV6_REACH_TLV;
            }
            else
            {
                pMDT->u1TLVType = ISIS_IPV6_RA_TLV;
            }

            u1CtrlOctet = pMDT->u1CtrlOctet;
            pMDT->u1CtrlOctet =
                ISIS_SET_MET_TYPE (u1CtrlOctet, ISIS_INTERNAL_METRIC);
        }
        if ((pCktRec != NULL) && (pIPRARec->u1IPRAType == ISIS_AUTO_TYPE))
        {
            if (pCktRec->pL1CktInfo != NULL)
            {
                pMDT->u4FullMetric = pCktRec->pL1CktInfo->u4FullMetric;
                MEMCPY (pMDT->Metric, pCktRec->pL1CktInfo->Metric,
                        (sizeof (tIsisMetric)));
            }
            else if (pCktRec->pL2CktInfo != NULL)
            {
                pMDT->u4FullMetric = pCktRec->pL2CktInfo->u4FullMetric;
                MEMCPY (pMDT->Metric, pCktRec->pL2CktInfo->Metric,
                        (sizeof (tIsisMetric)));
            }
        }
        else
        {
            ISIS_SET_METRIC (pContext, pMDT, pIPRARec);
        }
        pMDT->IPAddr.u1PrefixLen = pIPRARec->u1PrefixLen;

        if (pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        {
            /* Update the IPRA information in the Level2 SelfLSP which is held
             * in pL2MDT
             */

            pL2MDT =
                (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP, sizeof (tIsisMDT));

            if (pL2MDT == NULL)
            {
                UPP_PT ((ISIS_LGST, "UPD <T> : No Memory For Level2  MDT\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
            }
            else
            {
                /* Internal IPRAs are supposed to be added to both Level2 and
                 * Level1 if System Type is Level12
                 */

                /* Copy pMDT into pL2MDT, since both the structures must hold
                 * the same information except for the type which is updated
                 * appropriately below
                 */

                MEMCPY (pL2MDT, pMDT, sizeof (tIsisMDT));
                pL2MDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                if ((pCktRec != NULL) && (pCktRec->pL2CktInfo != NULL) &&
                    (pIPRARec->u1IPRAType == ISIS_AUTO_TYPE))
                {
                    pL2MDT->u4FullMetric = pCktRec->pL2CktInfo->u4FullMetric;
                    MEMCPY (pL2MDT->Metric, pCktRec->pL2CktInfo->Metric,
                            (sizeof (tIsisMetric)));
                }
                /* Updating the L2 Self LSP with the reachability information
                 */

                IsisUpdUpdateSelfLSP (pContext, 0, pL2MDT);
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pL2MDT);
            }
        }

        /* Updating the appropriate Level1 Self LSP with the reachability 
         * information
         */

        IsisUpdUpdateSelfLSP (pContext, 0, pMDT);
        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdUpdateIPRA ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdFindAndPurgeSelfLSP ()
 * Description : This Routine first fetches the LSP from either the Database or
 *               the Transmit Queue, and purges it from the entire Network
 * Input (s)   : pContext     - Pointer to System Context
 *               pLSPInfo     - Pointer to the LSP Information to be purged
 *               u1Level      - Level of the LSP
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, if LSP successfully purged
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdFindAndPurgeSelfLSP (tIsisSysContext * pContext, tIsisLSPInfo * pLSPInfo,
                            UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Loc = 0;
    UINT1              *pu1LSP = NULL;
    UINT1               au1LSPId[ISIS_LSPID_LEN];
    tIsisLSPEntry      *pRec = NULL;
    tIsisLSPEntry      *pPrevRec = NULL;
    tIsisHashBucket    *pHashBkt = NULL;
    CHAR                acLSPId[3 * ISIS_LSPID_LEN + 1] = { 0 };

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdFindAndPurgeSelfLSP () \n"));

    /* Deleting all the TLVs from the LSP
     */

    IsisUpdDelTlvs (pLSPInfo);

    /* To purge an LSP we first have to fetch the correct location of the LSP
     * including the Previous Node and the Hash Bucket etc. Hence Get the LSP
     * from either the Database or the Transmit Queue first
     */

    MEMCPY (au1LSPId, pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
    au1LSPId[ISIS_SYS_ID_LEN] = pLSPInfo->u1SNId;
    au1LSPId[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN] = pLSPInfo->u1LSPNum;

    ISIS_DBG_PRINT_ID (au1LSPId, (UINT1) ISIS_LSPID_LEN,
                       "UPD <T> : Purging SelfLSP <ID(6), SNID(1), NUM(1)>\t",
                       ISIS_OCTET_STRING);
    pRec = IsisUpdGetLSP (pContext, au1LSPId, u1Level, &pPrevRec,
                          &pHashBkt, &u1Loc);

    ISIS_FORM_STR (ISIS_LSPID_LEN, au1LSPId, acLSPId);
    UPP_PT ((ISIS_LGST, "UPD <T> : Purging SelfLSP [%s] [%s]\n",
             acLSPId, ISIS_GET_LEVEL_STR (u1Level)));
    if (pRec == NULL)
    {
        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting IsisUpdFindAndPurgeSelfLSP () \n"));
        return ISIS_FAILURE;
    }

    if ((u1Loc == ISIS_LOC_DB_FIRST) || (u1Loc == ISIS_LOC_DB))
    {
        /* Remove the LSP Record from the Database
         */

        IsisUpdRemLSPFromDB (pContext, pPrevRec, pRec, pHashBkt,
                             u1Level, u1Loc);

        ISIS_DEL_LSP_TIMER_NODE (pRec);

        /* Send a Lock Step Update to the standby node
         */

        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE, pRec);

        pu1LSP = pRec->pu1LSP;
        pRec->pu1LSP = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pRec);
    }
    else if ((u1Loc == ISIS_LOC_TX_FIRST) || (u1Loc == ISIS_LOC_TX))
    {
        /* Remove the LSP Record from the TxQ
         */

        IsisUpdRemLSPFromTxQ (pContext, (tIsisLSPTxEntry *) pPrevRec,
                              (tIsisLSPTxEntry *) pRec, pHashBkt,
                              u1Level, u1Loc);

        ISIS_DEL_LSP_TIMER_NODE (((tIsisLSPTxEntry *) pRec)->pLSPRec);

        /* Send a Lock Step Update to the standby node
         */

        ISIS_FLTR_LSP_LSU (pContext, ISIS_CMD_DELETE,
                           ((tIsisLSPTxEntry *) pRec)->pLSPRec);

        pu1LSP = ((tIsisLSPTxEntry *) pRec)->pLSPRec->pu1LSP;

        /* Freeing the LSP associated information
         */

        ISIS_MEM_FREE (ISIS_BUF_LDBE,
                       (UINT1 *) ((tIsisLSPTxEntry *) pRec)->pLSPRec);
        ISIS_MEM_FREE (ISIS_BUF_SRMF,
                       (UINT1 *) ((tIsisLSPTxEntry *) pRec)->pu1SRM);
        ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) ((tIsisLSPTxEntry *) pRec));
    }
    else
    {
        /* Self LSP is not available in the Database or Transmit Queue
         */

        return ISIS_SUCCESS;
    }

    if (ISIS_CONTEXT_ACTIVE (pContext) == ISIS_TRUE)
    {
        IsisUpdNWPurgeLSP (pContext, pu1LSP);
    }
    else
    {
        ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pu1LSP);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdFindAndPurgeSelfLSP () \n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdUpdateSelfLSP ()
 * Description : This routine modifies the specified database based on the
 *               command specified in the MDT structure. It Adds, Deletes or
 *               Modifies the specified database maintained as part of the
 *               SelfLSPs 
 * Input (s)   : pContext - Pointer to System Context
 *               u4CktIdx - Circuit Index 
 *               pMDT     - Pointer to Multi Data Type
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Succesful upation of Self LSP
 *               ISIS_FAILURE, Otherwise 
 ******************************************************************************/

PUBLIC INT4
IsisUpdUpdateSelfLSP (tIsisSysContext * pContext, UINT4 u4CktIdx,
                      tIsisMDT * pMDT)
{
    INT4                i4RetVal = ISIS_FAILURE;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdUpdateSelfLSP ()\n"));

    /* Add/Delete or Modify based on the command included in the MDT structure
     */

    switch (pMDT->u1Cmd)
    {
        case ISIS_CMD_ADD:

            switch (pMDT->u1TLVType)
            {
                case ISIS_IS_ADJ_TLV:

                    i4RetVal = IsisUpdAddISAdjTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_AREA_ADDR_TLV:

                    i4RetVal = IsisUpdAddAreaAddrTLV (pContext, pMDT);
                    break;

                case ISIS_IP_INTERNAL_RA_TLV:

                    i4RetVal = IsisUpdAddIPRATLV (pContext, pMDT, ISIS_TRUE,
                                                  ISIS_TRUE);
                    break;

                case ISIS_IP_EXTERNAL_RA_TLV:

                    i4RetVal = IsisUpdAddIPRATLV (pContext, pMDT, ISIS_FALSE,
                                                  ISIS_TRUE);
                    break;

                case ISIS_IPV6_RA_TLV:

                    i4RetVal = IsisUpdAddIPRATLV (pContext, pMDT, ISIS_TRUE,
                                                  ISIS_TRUE);
                    break;

                case ISIS_PROT_SUPPORT_TLV:

                    IsisUpdAddProtSuppTLV (pContext, pMDT);
                    break;

                case ISIS_IPV6IF_ADDR_TLV:
                case ISIS_IPV4IF_ADDR_TLV:

                    i4RetVal = IsisUpdAddIPIfAddrTLV (pContext, pMDT);
                    break;

                case ISIS_MT_TLV:

                    i4RetVal = IsisUpdAddMTTLV (pContext, pMDT);
                    break;

                case ISIS_EXT_IS_REACH_TLV:

                    i4RetVal =
                        IsisUpdAddExtISReachTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_EXT_IP_REACH_TLV:

                    i4RetVal =
                        IsisUpdAddExtMtIPReachTLV (pContext, pMDT, ISIS_TRUE,
                                                   ISIS_TRUE);
                    break;

                case ISIS_MT_IS_REACH_TLV:

                    i4RetVal =
                        IsisUpdAddMTISReachTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_MT_IPV6_REACH_TLV:

                    i4RetVal =
                        IsisUpdAddExtMtIPReachTLV (pContext, pMDT, ISIS_TRUE,
                                                   ISIS_TRUE);
                    break;
                case ISIS_DYN_HOSTNME_TLV:
                    i4RetVal = IsisUpdAddDynHostNmeTLV (pContext, pMDT);
                    break;
                default:

                    i4RetVal = ISIS_FAILURE;
                    UPP_PT ((ISIS_LGST,
                             "UPD <E> : Invalid TLV Type [%s] To Add To Self-LSP\n",
                             ISIS_GET_TLV_TYPE_STR (pMDT->u1TLVType)));
                    break;
            }
            break;

        case ISIS_CMD_DELETE:

            switch (pMDT->u1TLVType)
            {
                case ISIS_IS_ADJ_TLV:

                    i4RetVal = IsisUpdDelISAdjTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_AREA_ADDR_TLV:

                    i4RetVal = IsisUpdDelAreaAddrTLV (pContext, pMDT);
                    break;

                case ISIS_IP_INTERNAL_RA_TLV:

                    i4RetVal = IsisUpdDelIPRATLV (pContext, pMDT, ISIS_TRUE,
                                                  ISIS_TRUE);
                    break;

                case ISIS_IP_EXTERNAL_RA_TLV:
                    i4RetVal = IsisUpdDelIPRATLV (pContext, pMDT, ISIS_FALSE,
                                                  ISIS_TRUE);
                    break;
                case ISIS_IPV6_RA_TLV:

                    i4RetVal = IsisUpdDelIPRATLV (pContext, pMDT, ISIS_TRUE,
                                                  ISIS_TRUE);
                    break;

                case ISIS_PROT_SUPPORT_TLV:

                    i4RetVal = IsisUpdDelProtSuppTLV (pContext, pMDT);
                    break;

                case ISIS_IPV4IF_ADDR_TLV:
                case ISIS_IPV6IF_ADDR_TLV:

                    i4RetVal = IsisUpdDelIPIfAddrTLV (pContext, pMDT);
                    break;

                case ISIS_MT_TLV:

                    i4RetVal = IsisUpdDelMTTLV (pContext, pMDT);
                    break;

                case ISIS_EXT_IS_REACH_TLV:

                    i4RetVal =
                        IsisUpdDelExtISReachTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_EXT_IP_REACH_TLV:

                    i4RetVal =
                        IsisUpdDelExtMtIPReachTLV (pContext, pMDT, ISIS_TRUE,
                                                   ISIS_TRUE);
                    break;

                case ISIS_MT_IS_REACH_TLV:

                    i4RetVal =
                        IsisUpdDelMTISReachTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_MT_IPV6_REACH_TLV:

                    i4RetVal =
                        IsisUpdDelExtMtIPReachTLV (pContext, pMDT, ISIS_TRUE,
                                                   ISIS_TRUE);
                    break;
                case ISIS_DYN_HOSTNME_TLV:
                    i4RetVal = IsisUpdDelDynHostNmeTLV (pContext, pMDT);
                    break;
                default:

                    i4RetVal = ISIS_FAILURE;
                    UPP_PT ((ISIS_LGST,
                             "UPD <E> : Invalid TLV Type [%s] To Delete From Self-LSP\n",
                             ISIS_GET_TLV_TYPE_STR (pMDT->u1TLVType)));
                    break;
            }
            break;

        case ISIS_CMD_MODIFY:

            switch (pMDT->u1TLVType)
            {
                case ISIS_IS_ADJ_TLV:

                    i4RetVal = IsisUpdModISAdjTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_IP_INTERNAL_RA_TLV:
                case ISIS_IP_EXTERNAL_RA_TLV:
                case ISIS_IPV6_RA_TLV:

                    i4RetVal = IsisUpdModIPRATLV (pContext, pMDT, ISIS_TRUE);
                    break;

                case ISIS_EXT_IS_REACH_TLV:

                    i4RetVal =
                        IsisUpdModExtISReachTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_EXT_IP_REACH_TLV:

                    i4RetVal =
                        IsisUpdModExtMtIPReachTLV (pContext, pMDT, ISIS_TRUE);
                    break;

                case ISIS_MT_IS_REACH_TLV:

                    i4RetVal =
                        IsisUpdModMTISReachTLV (pContext, pMDT, u4CktIdx);
                    break;

                case ISIS_MT_IPV6_REACH_TLV:

                    i4RetVal =
                        IsisUpdModExtMtIPReachTLV (pContext, pMDT, ISIS_TRUE);
                    break;

                case ISIS_DYN_HOSTNME_TLV:

                    i4RetVal = IsisUpdAddDynHostNmeTLV (pContext, pMDT);
                    break;
                default:

                    i4RetVal = ISIS_FAILURE;
                    UPP_PT ((ISIS_LGST,
                             "UPD <E> : Invalid TLV Type [%s] To Modify In Self-LSP\n",
                             ISIS_GET_TLV_TYPE_STR (pMDT->u1TLVType)));
                    break;
            }
            break;

        default:

            UPP_PT ((ISIS_LGST,
                     "UPD <E> : Invalid Command [%u] To Update Self-LSP\n",
                     pMDT->u1Cmd));
            break;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdUpdateSelfLSP ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdAddISAdjTLV ()
 * Description : This routine adds an IS Adjacency TLV to SelfLSP. If IS
 *               adjacencies already exist, then the new adjacency information
 *               is added adjacent to the existing nodes. If not the new
 *               information is added at the head. If the existing LSPs does not
 *               have enough space to hold the new TLV, a new buffer is
 *               allocated and the new TLV information is added to the buffer.
 *               Otherwise the new TLV is added to the existing buffer
 * Input (s)   : pContext - Pointer to System Context
 *             : pMDT     - Pointer to MDT
 *             : u4CktIdx - Circuit Index
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, if addition of IS Adjacency TLV is successful
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdAddISAdjTLV (tIsisSysContext * pContext, tIsisMDT * pMDT, UINT4 u4CktIdx)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1LSPType = 0;
    UINT1               au1TLVMetric[4];
    UINT1              *pu1LSPNum = NULL;
    UINT4               u4LSPBufSize = 0;
    UINT4               u4Count = 0;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pTempNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddISAdjTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pMDT->au1AdjSysID, acNbrSysId);

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pu1LSPNum = &(pNSNLSP->u1CurrLSPNum);
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_LSPID_LEN + 11) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pu1LSPNum = &(pNSNLSP->u1CurrLSPNum);
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;

        /* Pseudonode LSPs are maintained per circuit. Go through the entire
         * list and get the appropriate Pseudonode LSP where the current
         * adjacency is to be added
         */

        while ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL)
               && (pSNLSP->pCktEntry->u4CktIdx != u4CktIdx))
        {
            pSNLSP = pSNLSP->pNext;
        }
        if (pSNLSP == NULL)
        {
            /* There may be cases where DIS status is marked as TRUE
             * for the Local System by the Adjacency module, but the Update
             * module has not processed the DIS Status change event. The Update
             * Module while processing the DIS Status change event will
             * construct pseudonode LSPs appropriately. Hence we need not worry
             * about the current adjacency at this point.
             */

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
            return (ISIS_SUCCESS);
        }
        pNonZeroLSP = pSNLSP->pNonZeroLSP;
        pu1LSPNum = &(pSNLSP->u1CurrLSPNum);
    }
    else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;

        /* Pseudonode LSPs are maintained per circuit. Go through the entire
         * list and get the appropriate Pseudonode LSP where the current
         * adjacency is to be added
         */

        while ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL)
               && (pSNLSP->pCktEntry->u4CktIdx != u4CktIdx))
        {
            pSNLSP = pSNLSP->pNext;
        }
        if (pSNLSP == NULL)
        {
            /* There may be cases where DIS status is marked as TRUE
             * for the Local System by the Adjacency module, but the Update
             * module has not processed the DIS Status change event. The Update
             * Module while processing the DIS Status change event will
             * construct pseudonode LSPs appropriately. Hence we need not worry
             * about the current adjacency at this point.
             */

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
            return (ISIS_SUCCESS);
        }
        pNonZeroLSP = pSNLSP->pNonZeroLSP;
        pu1LSPNum = &(pSNLSP->u1CurrLSPNum);
    }

    /* Check whether the entry exists already
     * to avoid addition of duplicate entries */
    pTravNonZeroLSP = pNonZeroLSP;
    while (pTravNonZeroLSP != NULL)
    {
        pTravTlv = pTravNonZeroLSP->pTLV;
        while (pTravTlv != NULL)
        {
            if (MEMCMP (&pTravTlv->au1Value[4], pMDT->au1AdjSysID,
                        (ISIS_SYS_ID_LEN + 1)) == 0)
            {
                i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

                if (i4RetVal == ISIS_FAILURE)
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Circuit Does Not Exist - Index [ %u ]\n",
                             u4CktIdx));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                    return ISIS_FAILURE;
                }
                /* In case of P2P circuits there is no Pseudo Node Id , So an additional
                 * check for u4CktIdx is needed to ensure no duplicate entries are added
                 */

                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    if (pTravTlv->u4CktIdx == u4CktIdx)
                    {
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                        return (ISIS_SUCCESS);
                    }
                }
                else
                {
                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                    return (ISIS_SUCCESS);
                }
            }
            pTravTlv = pTravTlv->pNext;
        }
        pTravNonZeroLSP = pTravNonZeroLSP->pNext;
    }

    /* The following routine will build the Metric array with Metrics arranged
     * in the order specified by the ISO 10589 specifications, which is required
     * for building the IS Adjacency TLV
     */

    IsisUtlFormMetric (pContext, pMDT->Metric, au1TLVMetric);

    /* Try to get a SelfLSP buffer which has got enough room to add the new IS
     * adjacency information. If not allocate a new buffer and initialise it 
     * and add the new IS Adjacency information to that
     */

    i4RetVal = IsisUpdGetSelfLSPBuffer (pContext, &pNonZeroLSP,
                                        ISIS_IS_ADJ_TLV, u4LSPBufSize,
                                        u1LSPType, pu1LSPNum);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* No LSP Buffers available to hold the new TLV information
         */

        WARNING ((ISIS_LGST, "UPD <W> : [No LSP Buffer]"
                  " Unable To Add IS Adjacency To Self-LSP - Adjacent System ID [ %s ]"
                  ", LSP Type [ %s ]\n", acNbrSysId,
                  ISIS_GET_LSP_STR (u1LSPType)));
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV ()\n"));
        return ISIS_FAILURE;
    }
    if ((u1LSPType == ISIS_L2_PSEUDO_LSP) || (u1LSPType == ISIS_L1_PSEUDO_LSP))
    {
        if (pSNLSP->pCktEntry != NULL)
        {
            pNonZeroLSP->u1SNId = pSNLSP->pCktEntry->u1CktLocalID;
        }
    }

    ISIS_DBG_PRINT_ID (pMDT->au1AdjSysID, (UINT1) (ISIS_SYS_ID_LEN + 1),
                       "UPD <T> : Self-LSP Updated With IS Adjacency\t",
                       ISIS_OCTET_STRING);

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisTLV) +
                                        ISIS_IS_ADJ_TLV_LEN);

    if (pTLV != NULL)
    {
        /* Virtual Flag will be added while the LSP buffer is constructed for
         * generation
         */

        pTLV->u1Code = ISIS_IS_ADJ_TLV;

        /* Since the same peer can be adjacent to the local system over two
         * different circuits, we may have to maintain the circuit information
         * along with the Adjacency
         */

        pTLV->u4CktIdx = u4CktIdx;
        pTLV->u1Len = ISIS_IS_ADJ_TLV_LEN;

        MEMCPY (pTLV->au1Value, au1TLVMetric, 4);
        MEMCPY (&(pTLV->au1Value[4]), pMDT->au1AdjSysID, ISIS_SYS_ID_LEN + 1);
        pNonZeroLSP->u1NumISTLV++;

        if (pNonZeroLSP->pTLV != NULL)
        {
            pTravTlv = pNonZeroLSP->pTLV;

            /* Go through the entire TLV chain and verify whether TLVs with the
             * same code already exist. If they exist add the new TLV adjacent
             * to the existing ones. Otherwise add the new TLV at the head
             */
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adding IS Adjacency To Self-LSP - Adjacent System ID [ %s ]\n"
                     "LSPType [ %s ]", acNbrSysId,
                     ISIS_GET_LSP_STR (u1LSPType)));

            while (pTravTlv != NULL)
            {
                if (pTravTlv->u1Code == pTLV->u1Code)
                {
                    pTLV->pNext = pTravTlv->pNext;
                    pTravTlv->pNext = pTLV;
                    break;
                }
                else
                {
                    pTravTlv = pTravTlv->pNext;
                }
            }

            if (pTravTlv == NULL)
            {
                /* No TLV matches the new TLV's code. Insert the record at the
                 * Head
                 */

                pTLV->pNext = pNonZeroLSP->pTLV;
                pNonZeroLSP->pTLV = pTLV;
            }
        }
        else
        {
            pNonZeroLSP->pTLV = pTLV;
        }
    }
    else
    {
        /* Ignore the new TLV that is to be added, nevertheless generate a PANIC
         * since Self-LSPs are no more correct
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IS Adjacency TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);

        RBTreeCount (pNonZeroLSP->IPRATLV, &u4Count);
        if ((pNonZeroLSP->pTLV == NULL) && (u4Count == 0))
        {
            /* This is a new LSP buffer allocated since the existing buffers did
             * not have enough space to hold the new IS Adjaency information 
             * (only in this case pNonZeroLSP->pTLV will be NULL). Since we are
             * unable to allocate memory for the TLV, we can release the LSP 
             * buffer also
             */
            if (pTempNonZeroLSP == pNonZeroLSP)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL1SNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL2SNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
            }
            else
            {
                while (pTempNonZeroLSP != NULL)
                {
                    if (pTempNonZeroLSP->pNext == pNonZeroLSP)
                    {
                        pTempNonZeroLSP->pNext = pNonZeroLSP->pNext;
                        break;
                    }
                    pTempNonZeroLSP = pTempNonZeroLSP->pNext;
                }
            }
            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pNonZeroLSP);
        }

        i4RetVal = ISIS_FAILURE;
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV ()\n"));
        return (i4RetVal);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdAddAreaAddrTLV ()
 * Description : This Routine adds an Area Address TLV to SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *             : pMDT     - Pointer to MDT structure which contains the 
 *             :            necessary information
 *             : u1Flag   - Flag Indicating whether Dirty Flag is to be set.
 *                          Flag will be ISIS_TRUE, when a Manual Area Address
 *                          is added. For addition of Area Addresses, Dirty
 *                          Flag need not be set
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, if addition of Area Address TLV is successful
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdAddAreaAddrTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1RemBytes = 0;
    UINT1               u1LSPType = 0;
    UINT1               u1AATlvLen = 0;
    UINT4               u4LSPBufSize = 0;
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo      **pZeroLSP = NULL;
    tIsisLSPInfo       *pTravZeroLSP = NULL;
    UINT1               au1AreaAddr[ISIS_AREA_ADDR_LEN];
    CHAR                acAreaAddr[ISIS_AREA_ADDR_LEN];
    UINT1               au1NullIP[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1AreaAddr, 0, ISIS_AREA_ADDR_LEN);
    MEMSET (acAreaAddr, 0, ISIS_AREA_ADDR_LEN);
    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddAreaAddrTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    /* Area Addresses will be added only to the Zero LSP since they are not
     * significant in any other LSP. Area Addresses must not be present in
     * Pseudonode LSPs
     */

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddAreaAddrTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pZeroLSP = &(pNSNLSP->pZeroLSP);
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddAreaAddrTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pZeroLSP = &(pNSNLSP->pZeroLSP);
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddAreaAddrTLV ()\n"));
        return ISIS_FAILURE;
    }

    if (*pZeroLSP == NULL)
    {
        *pZeroLSP = (tIsisLSPInfo *) ISIS_MEM_ALLOC (ISIS_BUF_SLSP,
                                                     sizeof (tIsisLSPInfo));
        if ((*pZeroLSP) == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Zero LSP\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            /* Shutdown the Instance
             */
            return ISIS_FAILURE;
        }
        (*pZeroLSP)->u1SNId = 0;
        (*pZeroLSP)->u1LSPNum = 0;
        (*pZeroLSP)->u2LSPLen = 0;
        (*pZeroLSP)->u2AuthStatus = ISIS_FALSE;
        (*pZeroLSP)->u4SeqNum = 0;
        (*pZeroLSP)->u1NumPSTLV = 0;
        (*pZeroLSP)->u1NumAATLV = 0;
        (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
        (*pZeroLSP)->pNext = NULL;
        (*pZeroLSP)->pTLV = NULL;
    }
    /* Refer to LOGIC of Self-LSP Updation explained @ the beginnig of this file
     */
    u1AATlvLen = (UINT1) (pMDT->AreaAddress.u1Length + 1);

    u1RemBytes = (UINT1) ISIS_GET_REM_BYTES (((*pZeroLSP)->u1NumAATLV),
                                             u1AATlvLen);
/* Duplicate Check */
    if ((*pZeroLSP)->pTLV != NULL)
    {
        pTravZeroLSP = *pZeroLSP;
        while (pTravZeroLSP != NULL)
        {
            pTravTlv = pTravZeroLSP->pTLV;

            while (pTravTlv != NULL)
            {
                if ((pTravTlv->u1Code == ISIS_AREA_ADDR_TLV) &&
                    (MEMCMP
                     ((pTravTlv->au1Value + 1),
                      &(pMDT->AreaAddress.au1AreaAddr),
                      pMDT->AreaAddress.u1Length) == 0))
                {
                    return ISIS_SUCCESS;
                    /* the same area address is already present */
                }
                pTravTlv = pTravTlv->pNext;
            }
            pTravZeroLSP = pTravZeroLSP->pNext;
        }
    }
    if (u1RemBytes < ISIS_AREA_ADDR_TLV_LEN)
    {
        if (((*pZeroLSP)->u2LSPLen + (u1AATlvLen + 2)) > (UINT2) u4LSPBufSize)
        {
            /* We cannot have more than one Zero LSP
             */

            WARNING ((ISIS_LGST,
                      "UPD <W> : Zero LSP Exceeding Configured Buffer Size [%u]\n",
                      u4LSPBufSize));
            return ISIS_FAILURE;
        }
        (*pZeroLSP)->u2LSPLen += 2;    /* '2' for Code and Length */
    }

    ISIS_DBG_PRINT_ADDR (pMDT->AreaAddress.au1AreaAddr,
                         (UINT1) (pMDT->AreaAddress.u1Length),
                         "UPD <T> : Self-LSP Updated With Area Address\t",
                         ISIS_OCTET_STRING);

    if ((*pZeroLSP)->u1NumAATLV == 0)
    {
        (*pZeroLSP)->u2LSPLen += 2;
    }

    (*pZeroLSP)->u2LSPLen += u1AATlvLen;

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisTLV) +
                                        u1AATlvLen);

    if (pTLV != NULL)
    {
        (*pZeroLSP)->u1NumAATLV++;
        (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
        pTLV->u1Code = ISIS_AREA_ADDR_TLV;
        pTLV->u1Len = u1AATlvLen;
        pTLV->au1Value[0] = pMDT->AreaAddress.u1Length;

        if (pMDT->AreaAddress.u1Length > ISIS_AREA_ADDR_LEN)
        {
            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTLV);
            return ISIS_FAILURE;
        }
        MEMCPY ((pTLV->au1Value + 1), &(pMDT->AreaAddress.au1AreaAddr),
                pMDT->AreaAddress.u1Length);

        /* Keep TLVs with the same code together
         */

        if ((*pZeroLSP)->pTLV != NULL)
        {
            pTravTlv = (*pZeroLSP)->pTLV;
            if ((MEMCMP
                 (pMDT->AreaAddress.au1AreaAddr, au1NullIP,
                  ISIS_MAX_IPV4_ADDR_LEN)) != 0)
            {
                MEMCPY (au1AreaAddr, pMDT->AreaAddress.au1AreaAddr,
                        ISIS_AREA_ADDR_LEN);
                ISIS_FORM_STR (ISIS_AREA_ADDR_LEN, au1AreaAddr, acAreaAddr);
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Adding Area Address To Self-LSP [Type: %s] [ %s ]\n",
                         ISIS_GET_SELF_LSP_TYPE (u1LSPType), acAreaAddr));
            }

            while (pTravTlv != NULL)
            {
                if (pTravTlv->u1Code == pTLV->u1Code)
                {
                    pTLV->pNext = pTravTlv->pNext;
                    pTravTlv->pNext = pTLV;
                    break;
                }
                else
                {
                    pTravTlv = pTravTlv->pNext;
                }
            }

            if (pTravTlv == NULL)
            {
                /* No TLV matches the new TLV's code. Insert the record at the
                 * Head
                 */

                pTLV->pNext = (*pZeroLSP)->pTLV;
                (*pZeroLSP)->pTLV = pTLV;
            }
        }
        else
        {
            (*pZeroLSP)->pTLV = pTLV;
        }
    }
    else
    {
        /* Ignore the new TLV, but generate a PANIC since SelfLSPs are no more
         * correct
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : AA TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);
        i4RetVal = ISIS_FAILURE;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddAreaAddrTLV ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdAddProtSuppTLV ()
 * Description : This routine adds a Protocol Support TLV to SelfLSP. In the
 *               case of Pseudonode LSPs the given information is added to each
 *               of the Zero LSPs
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure which contains the
 *                          Protocol Supported Entry that is to be added
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdAddProtSuppTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    UINT1               u1LSPType = 0;
    UINT4               u4LSPBufSize = 0;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddProtSuppTLV ()\n"));

    /* Protocol Support information will be added only to the Zero LSP 
     */

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }

    if (pNSNLSP != NULL)
    {
        /* We need to update only one Non-Pseudonode Zero LSP
         */

        IsisUpdInsPSTLVInSelfLSP (pContext, pMDT, &(pNSNLSP->pZeroLSP),
                                  u4LSPBufSize);
    }
    else
    {
        /* Since we have one Pseudonode LSP per circuit, the Protocol support
         * information must be Added to each of the Zero LSPs present in each of
         * the Pseudonode LSPs
         */

        while (pSNLSP != NULL)
        {
            /* We never expect the following routine to fail since Zero LSPs
             * carry only Area Addresses, apart from Protocol support 
             * Information, which may not exceed 1492 bytes. We are also 
             * restricted by the specification stating that only one Zero LSP 
             * is permitted and hence we must be able to add the PS information
             * to the Zero LSP buffer
             */

            IsisUpdInsPSTLVInSelfLSP (pContext, pMDT, &(pSNLSP->pZeroLSP),
                                      u4LSPBufSize);
            pSNLSP = pSNLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddProtSuppTLV ()\n"));
    return;
}

/******************************************************************************
 * Function    : IsisUpdInsPSTLVInSelfLSP ()
 * Description : This routine inserts the Protocol Support information to 
 *               into the pZeroLSP
 * Input (s)   : pContext     - Pointer to System Context
 *               pMDT         - Pointer to MDT structure which contains the
 *                              Protocol Supported Entry that is to be added
 *               pZeroLSP     - Pointer to Zero LSP where the given information
 *                              is to be added
 *               u4LSPBufSize - Maximum buffer size allowed for the Zero LSP 
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdInsPSTLVInSelfLSP (tIsisSysContext * pContext, tIsisMDT * pMDT,
                          tIsisLSPInfo ** pZeroLSP, UINT4 u4LSPBufSize)
{
    UINT1               u1RemBytes = 0;
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    tIsisTLV           *pProtTlv = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdInsPSTLVInSelfLSP ()\n"));

    if (*pZeroLSP == NULL)
    {
        (*pZeroLSP) = (tIsisLSPInfo *) ISIS_MEM_ALLOC (ISIS_BUF_SLSP,
                                                       sizeof (tIsisLSPInfo));
        if ((*pZeroLSP) == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Zero LSP\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            /* Shutdown the Instance
             */
            return;
        }
        (*pZeroLSP)->u1SNId = 0;
        (*pZeroLSP)->u1LSPNum = 0;
        (*pZeroLSP)->u2LSPLen = 0;
        (*pZeroLSP)->u2AuthStatus = ISIS_FALSE;
        (*pZeroLSP)->u4SeqNum = 0;
        (*pZeroLSP)->u1NumPSTLV = 0;
        (*pZeroLSP)->u1NumAATLV = 0;
        (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
        (*pZeroLSP)->pNext = NULL;
        (*pZeroLSP)->pTLV = NULL;
    }

    if ((*pZeroLSP)->pTLV != NULL)
    {
        /* Go through the entire TLV chain and verify whether TLVs with the
         * same code already exist. If they exist Check whether a TLV with
         * the same Protocol as incoming one already exist. If such a TLV 
         * exist, then ignore the incoming TLV.
         */

        pProtTlv = (*pZeroLSP)->pTLV;

        while (pProtTlv != NULL)
        {
            if ((pProtTlv->u1Code == ISIS_PROT_SUPPORT_TLV)
                && (pProtTlv->au1Value[0] == pMDT->u1ProtSupp))
            {
                /* A Protocol Support TLV with the same protocol as the incoming
                 * one already exist. Ignore the new TLV that we are trying to
                 * add
                 */

                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdInsPSTLVInSelfLSP ()\n"));

                (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
                return;
            }
            else if ((pProtTlv->u1Code == ISIS_PROT_SUPPORT_TLV)
                     && (pTravTlv == NULL))
            {
                /* Note down the TLV in the chain. The incoming protocol support
                 * TLV will be added next to this TLV, since TLVs with the same
                 * code are grouped together
                 */

                pTravTlv = pProtTlv;
            }
            else if ((pTravTlv != NULL)
                     && (pProtTlv->u1Code != ISIS_PROT_SUPPORT_TLV))
            {
                /* This condition occurs when the incoming protocol does not 
                 * exist, and we have seen all the TLVs whose code is same as 
                 * the incoming TLV code. Hence we can break the loop and 
                 * proceed to insert the new TLV in the list
                 */

                break;
            }
            else
            {
                pProtTlv = pProtTlv->pNext;
            }
        }
    }

    /* Refer to LOGIC of Self-LSP Updation explained @ the beginnig of this file
     */

    u1RemBytes = (UINT1) ISIS_GET_REM_BYTES (((*pZeroLSP)->u1NumPSTLV),
                                             ISIS_PROT_SUPP_TLV_LEN);

    if (u1RemBytes < ISIS_PROT_SUPP_TLV_LEN)
    {
        if (((*pZeroLSP)->u2LSPLen + (ISIS_PROT_SUPP_TLV_LEN + 2))
            > (UINT2) u4LSPBufSize)
        {
            WARNING ((ISIS_LGST,
                      "UPD <W> : Zero LSP Exceeding Configured Buffer Size [%u]\n",
                      u4LSPBufSize));
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdInsPSTLVInSelfLSP ()\n"));
            return;
        }
        (*pZeroLSP)->u2LSPLen += 2;
    }

    if ((*pZeroLSP)->u1NumPSTLV == 0)
    {
        (*pZeroLSP)->u2LSPLen += 2;
    }
    (*pZeroLSP)->u2LSPLen += ISIS_PROT_SUPP_TLV_LEN;

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisTLV)
                                        + ISIS_PROT_SUPP_TLV_LEN);
    if (pTLV != NULL)
    {
        pTLV->u1Code = ISIS_PROT_SUPPORT_TLV;
        pTLV->u1Len = ISIS_PROT_SUPP_TLV_LEN;
        MEMCPY (pTLV->au1Value, &pMDT->u1ProtSupp, ISIS_PROT_SUPP_TLV_LEN);

        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Adding Protocol Support Info To Self-LSP [ %s ]\n",
                 ISIS_GET_PROT_SUPP (pMDT->u1ProtSupp)));

        if (pTravTlv == NULL)
        {
            /* No TLV matches the new TLV's code. Insert the record at the
             * Head
             */
            pTLV->pNext = (*pZeroLSP)->pTLV;
            (*pZeroLSP)->pTLV = pTLV;
        }
        else
        {
            pTLV->pNext = pTravTlv->pNext;
            pTravTlv->pNext = pTLV;
        }

        (*pZeroLSP)->u1NumPSTLV++;
        (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
    }
    else
    {
        /* Ignore the Protocol support information. but generate a PANIC since
         * SelfLSPs are no more correct
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : PS TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdInsPSTLVInSelfLSP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdAddIPV6IfAddrTLV ()
 * Description : This Routine adds IP Interface Address TLV to SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure which contains the 
 *                          IP Interface Address to be added 
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of IP IF Address TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisUpdAddIPIfAddrTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    UINT1               u1LSPType = 0;
    UINT4               u4LSPBufSize = 0;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddIPIfAddrTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }

    if (pNSNLSP != NULL)
    {
        /* We need to update only one Non-Pseudonode LSP
         */

        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        if (pMDT->u1TLVType == ISIS_IPV4IF_ADDR_TLV)
        {
            /* When adding the first Ipv4Address TLV, add IPv4 PS TLV also */
            if ((pNonZeroLSP == NULL) || (pNonZeroLSP->u1NumIPIfTLV == 0))
            {
                pMDT->u1ProtSupp = ISIS_IPV4_SUPP;
                IsisUpdAddProtSuppTLV (pContext, pMDT);
            }
            IsisUpdInsIPIfAddrTLVInSelfLSP (pContext, pMDT, pNonZeroLSP,
                                            u4LSPBufSize, u1LSPType,
                                            &(pNSNLSP->u1CurrLSPNum));
        }
        else
        {
            /* When adding the first Ipv6Address TLV, add IPv6 PS TLV also */
            if ((pNonZeroLSP == NULL) || (pNonZeroLSP->u1NumIPV6IfTLV == 0))
            {
                pMDT->u1ProtSupp = ISIS_IPV6_SUPP;
                IsisUpdAddProtSuppTLV (pContext, pMDT);
            }
            IsisUpdInsIPV6IfAddrTLVInSelfLSP (pContext, pMDT, pNonZeroLSP,
                                              u4LSPBufSize, u1LSPType,
                                              &(pNSNLSP->u1CurrLSPNum));
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPIfAddrTLV ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdInsIPIfAddrTLVInSelfLSP ()
 * Description : This routine inserts the IP Interface Address information to 
 *               into the pZeroLSP
 * Input (s)   : pContext     - Pointer to System Context
 *               pMDT         - Pointer to MDT structure which contains the
 *                              IP Interface Address Entry that is to be added
 *               pNonZeroLSP  - Pointre to LSP where the given information
 *                              is to be added
 *               u4LSPBufSize - Maximum buffer size allowed for the LSP 
 *               u1LSPType    - Type of LSP (Pseudonode or Non-Pseudonode)
 *               pu1LSPNum    - Pointer to the LSP number that should be
 *                              assigned to any new LSP buffer created. It holds
 *                              the current value of the LSP number
 * Output (s)  : pu1LSPNum    - The LSP number after incrementing it by '1' if
 *                              a new LSP buffer is allocated
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdInsIPIfAddrTLVInSelfLSP (tIsisSysContext * pContext, tIsisMDT * pMDT,
                                tIsisLSPInfo * pNonZeroLSP, UINT4 u4LSPBufSize,
                                UINT1 u1LSPType, UINT1 *pu1LSPNum)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4Count = 0;
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    UPP_EE ((ISIS_LGST,
             "UPD <X> : Entered IsisUpdInsIPIfAddrTLVInSelfLSP ()\n"));

    ISIS_FORM_IPV4_ADDR (pMDT->IPAddr.au1IpAddr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    /* Get the appropriate NonZero LSP buffer where we can add the new IP
     * Interface Address information
     */

    i4RetVal = IsisUpdGetSelfLSPBuffer (pContext, &pNonZeroLSP,
                                        ISIS_IPV4IF_ADDR_TLV, u4LSPBufSize,
                                        u1LSPType, pu1LSPNum);
    if (i4RetVal == ISIS_FAILURE)
    {
        /* No LSP Buffers available to hold the new TLV information
         */
        WARNING ((ISIS_LGST,
                  "UPD <W> : [No LSP Buffers Available] Unable To Add IPv4 Interface Address To Self-LSP [ %s ], LSP Type [ %s ]\n",
                  au1IPv4Addr, ISIS_GET_SELF_LSP_TYPE (u1LSPType)));
        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting IsisUpdInsIPIfAddrTLVInSelfLSP ()\n"));
        return;
    }

    if (pNonZeroLSP->pTLV != NULL)
    {
        pTravTlv = pNonZeroLSP->pTLV;
        while (pTravTlv != NULL)
        {
            if (MEMCMP
                (pTravTlv->au1Value, pMDT->IPAddr.au1IpAddr,
                 ISIS_MAX_IPV4_ADDR_LEN) == 0)
            {
                return;
            }
            pTravTlv = pTravTlv->pNext;
        }
    }

    UPP_PT ((ISIS_LGST,
             "UPD <T> : Adding IPv4 Interface Address To Self-LSP [ %s ], LSP Type [ %s ]\n",
             au1IPv4Addr, ISIS_GET_LSP_STR (u1LSPType)));

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS,
                                        sizeof (tIsisTLV) +
                                        ISIS_MAX_IPV4_ADDR_LEN);
    if (pTLV != NULL)
    {
        pTLV->u1Code = ISIS_IPV4IF_ADDR_TLV;
        pTLV->u1Len = ISIS_MAX_IPV4_ADDR_LEN;

        MEMCPY (pTLV->au1Value, pMDT->IPAddr.au1IpAddr, ISIS_MAX_IPV4_ADDR_LEN);

        pNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        pNonZeroLSP->u1NumIPIfTLV++;

        if (pNonZeroLSP->pTLV != NULL)
        {
            pTravTlv = pNonZeroLSP->pTLV;

            /* Go through the entire TLV chain and verify whether TLVs with the
             * same code already exist. If they exist add the new TLV adjacent
             * to the existing ones. Otherwise add the new TLV at the head
             */

            while (pTravTlv != NULL)
            {
                if (pTravTlv->u1Code == pTLV->u1Code)
                {
                    pTLV->pNext = pTravTlv->pNext;
                    pTravTlv->pNext = pTLV;
                    break;
                }
                else
                {
                    pTravTlv = pTravTlv->pNext;
                }
            }

            if (pTravTlv == NULL)
            {
                /* No TLV matches the new TLV's code. Insert the record at the
                 * Head
                 */

                pTLV->pNext = pNonZeroLSP->pTLV;
                pNonZeroLSP->pTLV = pTLV;
            }
        }
        else
        {
            pNonZeroLSP->pTLV = pTLV;
        }
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IP I/f Address TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);

        RBTreeCount (pNonZeroLSP->IPRATLV, &u4Count);
        if ((pNonZeroLSP->pTLV == NULL) && (u4Count == 0))
        {
            /* This is a new LSP buffer allocated since the existing buffers did
             * not have enough space to hold the new IP I/f Address information 
             * (only in this case pNonZeroLSP->pTLV will be NULL). Since we are
             * unable to allocate memory for the TLV, we can release the LSP 
             * buffer also
             */

            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pNonZeroLSP);
        }

        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting IsisUpdInsIPIfAddrTLVInSelfLSP ()\n"));
        return;
    }

    UPP_EE ((ISIS_LGST,
             "UPD <X> : Exiting IsisUpdInsIPIfAddrTLVInSelfLSP ()\n"));
    return;
}

/******************************************************************************
 * Function    : IsisUpdInsIPV6IfAddrTLVInSelfLSP ()
 * Description : This routine inserts the IP Interface Address information to 
 *               into the pZeroLSP
 * Input (s)   : pContext     - Pointer to System Context
 *               pMDT         - Pointer to MDT structure which contains the
 *                              IP Interface Address Entry that is to be added
 *               pNonZeroLSP  - Pointre to LSP where the given information
 *                              is to be added
 *               u4LSPBufSize - Maximum buffer size allowed for the LSP 
 *               u1LSPType    - Type of LSP (Pseudonode or Non-Pseudonode)
 *               pu1LSPNum    - Pointer to the LSP number that should be
 *                              assigned to any new LSP buffer created. It holds
 *                              the current value of the LSP number
 * Output (s)  : pu1LSPNum    - The LSP number after incrementing it by '1' if
 *                              a new LSP buffer is allocated
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdInsIPV6IfAddrTLVInSelfLSP (tIsisSysContext * pContext, tIsisMDT * pMDT,
                                  tIsisLSPInfo * pNonZeroLSP,
                                  UINT4 u4LSPBufSize, UINT1 u1LSPType,
                                  UINT1 *pu1LSPNum)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4Count = 0;
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    tIp6Addr            Ip6Addr;
    tIsisLSPInfo       *pTempNonZeroLSP = NULL;

    UPP_EE ((ISIS_LGST,
             "UPD <X> : Entered IsisUpdInsIPV6IfAddrTLVInSelfLSP ()\n"));

    /* Get the appropriate NonZero LSP buffer where we can add the new IP
     * Interface Address information
     */
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ip6Addr, pMDT->IPAddr.au1IpAddr, ISIS_MAX_IPV6_ADDR_LEN);
    if (IS_ADDR_LLOCAL (Ip6Addr))
    {
        return;
    }

    /* Traverse through all the fragments and check whther the interface
       Address TLV is already present or not. If not we will continue and the
       TLV in LSP */
    pTempNonZeroLSP = pNonZeroLSP;
    while ((pTempNonZeroLSP != NULL) && (pTempNonZeroLSP->pTLV != NULL))
    {
        pTravTlv = pTempNonZeroLSP->pTLV;
        while (pTravTlv != NULL)
        {
            if (MEMCMP
                (pTravTlv->au1Value, pMDT->IPAddr.au1IpAddr,
                 ISIS_MAX_IPV6_ADDR_LEN) == 0)
            {
                return;
            }
            pTravTlv = pTravTlv->pNext;
        }
        pTempNonZeroLSP = pTempNonZeroLSP->pNext;
    }

    i4RetVal = IsisUpdGetSelfLSPBuffer (pContext, &pNonZeroLSP,
                                        ISIS_IPV6IF_ADDR_TLV, u4LSPBufSize,
                                        u1LSPType, pu1LSPNum);
    if (i4RetVal == ISIS_FAILURE)
    {
        /* No LSP Buffers available to hold the new TLV information
         */

        WARNING ((ISIS_LGST,
                  "UPD <W> : [No LSP Buffers Available] Unable To Add IPv6 Interface Address To Self-LSP [ %s ], LSP Type [ %s ]\n",
                  Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                &(pMDT->IPAddr.au1IpAddr)),
                  ISIS_GET_SELF_LSP_TYPE (u1LSPType)));
        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting IsisUpdInsIPIfAddrTLVInSelfLSP ()\n"));
        return;
    }

    UPP_PT ((ISIS_LGST,
             "UPD <T> : Adding IPv6 Interface Address To Self-LSP [ %s ], LSP Type [ %s ]\n",
             Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pMDT->IPAddr.au1IpAddr)),
             ISIS_GET_SELF_LSP_TYPE (pMDT->u1LSPType)));

    if (pNonZeroLSP->pTLV != NULL)
    {
        pTravTlv = pNonZeroLSP->pTLV;
        while (pTravTlv != NULL)
        {
            if (MEMCMP
                (pTravTlv->au1Value, pMDT->IPAddr.au1IpAddr,
                 ISIS_MAX_IPV6_ADDR_LEN) == 0)
            {
                return;
            }
            pTravTlv = pTravTlv->pNext;
        }
    }

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS,
                                        sizeof (tIsisTLV) +
                                        ISIS_MAX_IPV6_ADDR_LEN);
    if (pTLV != NULL)
    {
        pTLV->u1Code = ISIS_IPV6IF_ADDR_TLV;
        pTLV->u1Len = ISIS_MAX_IPV6_ADDR_LEN;

        MEMCPY (pTLV->au1Value, pMDT->IPAddr.au1IpAddr, ISIS_MAX_IPV6_ADDR_LEN);

        pNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
        pNonZeroLSP->u1NumIPV6IfTLV++;

        if (pNonZeroLSP->pTLV != NULL)
        {
            pTravTlv = pNonZeroLSP->pTLV;

            /* Go through the entire TLV chain and verify whether TLVs with the
             * same code already exist. If they exist add the new TLV adjacent
             * to the existing ones. Otherwise add the new TLV at the head
             */

            while (pTravTlv != NULL)
            {
                if (pTravTlv->u1Code == pTLV->u1Code)
                {
                    pTLV->pNext = pTravTlv->pNext;
                    pTravTlv->pNext = pTLV;
                    break;
                }
                else
                {
                    pTravTlv = pTravTlv->pNext;
                }
            }

            if (pTravTlv == NULL)
            {
                /* No TLV matches the new TLV's code. Insert the record at the
                 * Head
                 */

                pTLV->pNext = pNonZeroLSP->pTLV;
                pNonZeroLSP->pTLV = pTLV;
            }
        }
        else
        {
            pNonZeroLSP->pTLV = pTLV;
        }
    }
    else
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IP I/f Address TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);

        RBTreeCount (pNonZeroLSP->IPRATLV, &u4Count);
        if ((pNonZeroLSP->pTLV == NULL) && (u4Count == 0))
        {
            /* This is a new LSP buffer allocated since the existing buffers did
             * not have enough space to hold the new IP I/f Address information 
             * (only in this case pNonZeroLSP->pTLV will be NULL). Since we are
             * unable to allocate memory for the TLV, we can release the LSP 
             * buffer also
             */

            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pNonZeroLSP);
        }

        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting IsisUpdInsIPV6IfAddrTLVInSelfLSP ()\n"));
        return;
    }

    UPP_EE ((ISIS_LGST,
             "UPD <X> : Exiting IsisUpdInsIPV6IfAddrTLVInSelfLSP ()\n"));
    return;
}

/******************************************************************************
 * Function    : IsisUpdAddIPRATLV ()
 * Description : This Routine adds IP Reachability Address TLV to SelfLSP
 * Input (s)   : pContext     - Pointer to System Context
 *               pMDT         - Pointer to MDT structure which contains  
 *                              LSPType, IPRA information.
 *               u1SAChk      - Flag indicating whether a matching Summary 
 *                              Address is to be added to Self LSP. In case of
 *                              summary addresses themselves being added viz
 *                              this routine or in the case of SPT PATH Nodes
 *                              being added, this flag will be set to FALSE 
 *               u1SAIncrFlag - Flag Indicating whether the SA usage count 
 *                              is to be updated. The flag will be set to FALSE
 *                              in case of addtions being made for Summary
 *                              Addresses to PATH nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of IPRA TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdAddIPRATLV (tIsisSysContext * pContext, tIsisMDT * pMDT, UINT1 u1SAChk,
                   UINT1 u1SAIncrFlag)
{
    tIsisIPRATLV       *pIPRATLV = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisSAEntry       *pSARec = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pTempNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisIPRATLV        IPRATravTLV;
    UINT4               u4LSPBufSize = 0;
    UINT4               u4MetricVal = 0;
    UINT4               u4Count = 0;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               au1TLVMetric[4];
    UINT1               au1Mask[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1SAState = ISIS_SUMM_ADMIN_OFF;
    UINT1               u1AddFlag = ISIS_TRUE;
    UINT1               u1LSPType = 0;
    UINT1               u1AddrLen = 0;
    UINT1               u1Idx = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddIPRATLV ()\n"));

    ISIS_FORM_IPV4_ADDR (pMDT->IPAddr.au1IpAddr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);
    MEMSET (&IPRATravTLV, 0, sizeof (tIsisIPRATLV));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRATLV ()\n"));
            return (ISIS_FAILURE);
        }
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
        u1SAState = ISIS_SUMM_ADMIN_L1;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRATLV ()\n"));
            return (ISIS_FAILURE);
        }
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
        u1SAState = ISIS_SUMM_ADMIN_L2;
    }

    /* NOTE: 
     *
     * 1. u1SAChk = FALSE implies no summarisation and hence do not worry about
     *    u1SAIncrFlag. This is the case when Summary Address entries themselves
     *    are being added
     *    
     * 2. u1SAChk = TRUE implies summarisation is required. This means that the
     *    information being added is IPRA information. Then
     *
     *    2a. u1SAIncrFlag = TRUE implies that the usage count is to be
     *        incremented since the entry being added is IPRA information
     *        configured by the manager
     *    2b. u1SAIncrFlag = FALSE implies that the usage count need not be
     *        incremented since the entry being added is either IPRA information
     *        retrieved from the PATH nodes or it is Summary Address Information
     */

    if (u1SAChk == ISIS_TRUE)
    {
        /* This routine is invoked to add even the Summary Addresses from the
         * Summary Addrress Table as TLVs in Self-LSPs apart from adding IPRAs.
         * In the former case we need not check the Summary Address Table while
         * in the later case, each IPRA will be checked against the configured
         * Summary Addresses. If a matching entry is found the IPRA will not be
         * added and instead the Summary Address entry is added as a TLV.
         * Otherwise the IPRA entry is added to the Self-LSPs. The 'u1SAChk'
         * flag will be FLASE while Summary Address entries are being added
         */

        pSARec = pContext->SummAddrTable.pSAEntry;
    }

    while (pSARec != NULL)
    {
        /* Try to summarize the given IPRA entry
         */

        if (((pSARec->u1AdminState == u1SAState)
             || (pSARec->u1AdminState == ISIS_SUMM_ADMIN_L12))
            && ((IsisUtlCompIPAddr (pSARec->au1SummAddr,
                                    pSARec->u1PrefixLen,
                                    pMDT->IPAddr.au1IpAddr,
                                    pSARec->u1PrefixLen)) == ISIS_TRUE))
        {
            /* Apart from Summary Addresses and configured IPRA information, 
             * this function is also invoked for adding IPRA information
             * retrieved from the PATH nodes. In case this function is invoked
             * while adding PATH information, the information must be summarised
             * but the usage count need not be incremented. 'u1SAIncrFlag'
             * specifies whether usage count is to be incremented
             */

            if (u1SAIncrFlag == ISIS_TRUE)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pSARec->u2L1UsageCnt++;
                }
                else
                {
                    pSARec->u2L2UsageCnt++;
                }
            }

            if (((pSARec->u2L1UsageCnt == 1)
                 && (u1LSPType == ISIS_L1_NON_PSEUDO_LSP))
                || ((pSARec->u2L2UsageCnt == 1)
                    && (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)))
            {
                /* A matching summary address information record is found. The
                 * usage count is also '1' and hence we can add the Summary
                 * Address information instead of the IPRA information.
                 * Overwrite the MDT record with the Summary Address Information
                 */
                MEMCPY (pMDT->Metric, pSARec->Metric, (sizeof (tIsisMetric)));
                if (pSARec->u1AddrType == ISIS_ADDR_IPV4)
                {
                    u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
                    MEMCPY (pMDT->IPAddr.au1IpAddr, pSARec->au1SummAddr,
                            ISIS_MAX_IPV4_ADDR_LEN);
                }
                else
                {
                    u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
                    MEMCPY (pMDT->IPAddr.au1IpAddr, pSARec->au1SummAddr,
                            ISIS_MAX_IPV6_ADDR_LEN);
                }
                ISIS_DBG_PRINT_ADDR (pSARec->au1SummAddr, u1AddrLen,
                                     "UPD <T> : Summary Address\t",
                                     ISIS_OCTET_STRING);
                ISIS_DBG_PRINT_ID (pSARec->Metric, (UINT1) ISIS_NUM_METRICS,
                                   "UPD <T> : Metric\t", ISIS_OCTET_STRING);
                pMDT->IPAddr.u1PrefixLen = pSARec->u1PrefixLen;
            }
            else
            {
                /* Received IPRA information matched an existing Summary Address
                 * information and the usage count of the Summary Address entry 
                 * is greater than '1' which means that the address information
                 * has already been added. So the new information need not be
                 * added to Self-LSPs
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : IPRA Info Already Exist In Self-LSP"
                         " LSP Type [ %s ]\n", ISIS_GET_LSP_STR (u1LSPType)));
                u1AddFlag = ISIS_FALSE;
            }
            break;
        }
        pSARec = pSARec->pNext;
    }

    if (u1AddFlag == ISIS_FALSE)
    {
        /* No need to add the given IPRA information, it has already been added
         */

        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRATLV ()\n"));
        return ISIS_SUCCESS;
    }

    /* Construct the Metric, in order, as specified in ISO10589 specifications
     */
    if (pMDT->u1TLVType != ISIS_IPV6_RA_TLV)
    {
        IsisUtlFormMetric (pContext, pMDT->Metric, au1TLVMetric);
    }

    pTravNonZeroLSP = pNonZeroLSP;

    IPRATravTLV.u1Code = pMDT->u1TLVType;
    MEMCPY (IPRATravTLV.IPAddr.au1IpAddr,
            pMDT->IPAddr.au1IpAddr,
            (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
    IPRATravTLV.IPAddr.u1PrefixLen = pMDT->IPAddr.u1PrefixLen;

    while (pTravNonZeroLSP != NULL)
    {
        /* SCan the IPRA TLV RBTree to get the matching TLV */

        pIPRATLV = (tIsisIPRATLV *)
            RBTreeGet (pTravNonZeroLSP->IPRATLV, (tRBElem *) & IPRATravTLV);

        if (pIPRATLV != NULL)
        {
            if (pMDT->u1SrcProtoId != pIPRATLV->u1Protocol)
            {
                if (pMDT->u1SrcProtoId != 0)
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Protocol Id [%s] updated in the IPRA entry \n",
                             ISIS_GET_RRD_PROTO_STR (pMDT->u1SrcProtoId)));
                    pIPRATLV->u1Protocol = pMDT->u1SrcProtoId;
                }
                else
                {
                    pIPRATLV->u1IsisEnabled = ISIS_TRUE;
                }
                if (pMDT->u1IsisEnabled == ISIS_TRUE)
                {
                    /* This flag is set, if the interface is conneced interface and
                     * redistribution is enabled on it. */
                    pIPRATLV->u1IsisEnabled = ISIS_TRUE;
                }
                else
                {
                    if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                    {
                        if (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC)
                        {
                            COPY_U4_FROM_BUF (u4MetricVal, pMDT->Metric);
                        }
                        else
                        {
                            u4MetricVal = pMDT->u4FullMetric;
                            u4MetricVal = OSIX_HTONL (u4MetricVal);
                        }
                        MEMCPY (pIPRATLV->au1Value, &u4MetricVal,
                                sizeof (tIsisMetric));
                    }
                    else
                    {
                        MEMCPY (pIPRATLV->au1Value, au1TLVMetric,
                                sizeof (au1TLVMetric));
                    }
                }
            }
            u1AddFlag = ISIS_FALSE;
        }
        if (u1AddFlag == ISIS_FALSE)
        {
            break;
        }
        pTravNonZeroLSP = pTravNonZeroLSP->pNext;
    }

    if (u1AddFlag == ISIS_FALSE)
    {
        /* No need to add the given IPRA information, it has already been added
         */

        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRATLV ()\n"));
        return ISIS_SUCCESS;
    }

    /* Try to get a SelfLSP buffer which has got enough room to add the new IPRA
     * information. If not allocate a new buffer and initialise it and add the
     * new IPRA information to that
     */

    i4RetVal = IsisUpdGetSelfLSPBuffer (pContext, &pNonZeroLSP,
                                        pMDT->u1TLVType, u4LSPBufSize,
                                        u1LSPType, &(pNSNLSP->u1CurrLSPNum));

    if (i4RetVal == ISIS_FAILURE)
    {
        /* No LSP Buffers available to hold the new TLV information
         */
        if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
        {
            WARNING ((ISIS_LGST,
                      "UPD <W> : [No LSP Buffers Available] Unable To Add IPv6 IPRA To Self-LSP [ %s ], LSP Type [ %s ]\n",
                      Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                    &(pMDT->IPAddr.au1IpAddr)),
                      ISIS_GET_SELF_LSP_TYPE (u1LSPType)));
        }
        else
        {
            WARNING ((ISIS_LGST,
                      "UPD <W> : [No LSP Buffers Available] Unable To Add IPRA To Self-LSP [ %s ], LSP Type [ %s ]\n",
                      au1IPv4Addr, ISIS_GET_SELF_LSP_TYPE (u1LSPType)));
        }

        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRATLV ()\n"));
        return ISIS_FAILURE;
    }

    /* Allocate memory for the TLV */
    pIPRATLV =
        (tIsisIPRATLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisIPRATLV));

    if (pIPRATLV != NULL)
    {
        MEMSET (pIPRATLV, 0, sizeof (tIsisIPRATLV));
        pIPRATLV->u1Code = pMDT->u1TLVType;
        pIPRATLV->u1Protocol = pMDT->u1SrcProtoId;
        if (pMDT->u1IsisEnabled == ISIS_TRUE)
        {
            /* This flag is set, if the interface is conneced interface and
             * redistribution is enabled on it. */
            pIPRATLV->u1IsisEnabled = ISIS_TRUE;
        }
        if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
        {
            MEMCPY (pIPRATLV->IPAddr.au1IpAddr,
                    pMDT->IPAddr.au1IpAddr,
                    ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen));
            pIPRATLV->u1Len = (UINT1) (ISIS_IPV6_IPRA_TLV_LEN + (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));    /*diab */
            pNonZeroLSP->u1NumIPV6RATLV++;
            pNonZeroLSP->u2LSPLen -= (ISIS_MAX_IPV6_ADDR_LEN -
                                      (ISIS_ROUNDOFF_PREFIX_LEN
                                       (pMDT->IPAddr.u1PrefixLen)));
        }
        else
        {
            MEMCPY (pIPRATLV->IPAddr.au1IpAddr,
                    pMDT->IPAddr.au1IpAddr,
                    (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
            IsisUtlGetIPMask (pMDT->IPAddr.u1PrefixLen, au1Mask);
            pIPRATLV->u1Len = ISIS_IPRA_TLV_LEN;
            pNonZeroLSP->u1NumIPRATLV++;
        }
        pIPRATLV->IPAddr.u1PrefixLen = pMDT->IPAddr.u1PrefixLen;

        ISIS_DBG_PRINT_ID (pMDT->IPAddr.au1IpAddr, pMDT->IPAddr.u1Length,
                           "UPD <T> : Updating SelfLSP With IP Address\t",
                           ISIS_OCTET_STRING);
        ISIS_DBG_PRINT_ID (au1Mask, (UINT1) pMDT->IPAddr.u1Length,
                           "UPD <T> : Updating SelfLSP With IP Mask\t",
                           ISIS_OCTET_STRING);
        ISIS_DBG_PRINT_ID (au1TLVMetric, (UINT1) sizeof (au1TLVMetric),
                           "UPD <T> : Updating SelfLSP With Metrics\t",
                           ISIS_OCTET_STRING);

        if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adding IPv6 IPRA To Self-LSP [ %s ], LSP Type [ %s ]\n",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pMDT->IPAddr.au1IpAddr)),
                     ISIS_GET_SELF_LSP_TYPE (pMDT->u1LSPType)));
        }
        else
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adding IPv4 IPRA To Self-LSP [ %s ], LSP Type [ %s ]\n",
                     au1IPv4Addr, ISIS_GET_SELF_LSP_TYPE (pMDT->u1LSPType)));
        }
        if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
        {
            if (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC)
            {
                COPY_U4_FROM_BUF (u4MetricVal, pMDT->Metric);
            }
            else
            {
                u4MetricVal = pMDT->u4FullMetric;
                u4MetricVal = OSIX_HTONL (u4MetricVal);
            }

            MEMCPY (pIPRATLV->au1Value, &u4MetricVal, sizeof (tIsisMetric));
            /* The IPV6 IPRA TLV format is as follows:
             * 4 bytes of metric value, 2 byte control field , followed by IP Prefix.
             * Control byte has 1st bit as UP/DOWN bit, 2nd bit as INTERNAL/EXTERNAL
             * bit,3rd bit to indicate presence of Sub-Tlvs, and last 6 bits of the
             * 2nd byte indicating the prefix len
             */
            /* already copied the metric value */
            /* ISIS_ST_FLT_103 while copying u1CtrlOctec addr of the same
             * was not passed
             */
            MEMCPY (&pIPRATLV->au1Value[4], &(pMDT->u1CtrlOctet), 1);
            MEMCPY (&pIPRATLV->au1Value[5], &(pMDT->IPAddr.u1PrefixLen), 1);
            if ((ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)) <=
                ISIS_MAX_IPV6_ADDR_LEN)
            {
                MEMCPY (&pIPRATLV->au1Value[6], pMDT->IPAddr.au1IpAddr,
                        ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen));
            }
            if (ISIS_GET_UPDOWN_TYPE (pMDT->u1CtrlOctet) == ISIS_RL_L1_DOWN)
            {
                pIPRATLV->u1LeakedRoute = ISIS_TRUE;
            }
        }
        else
        {
            if (ISIS_GET_UPDOWN_TYPE (pMDT->u1CtrlOctet) == ISIS_RL_L1_DOWN)
            {
                au1TLVMetric[0] |= ISIS_FLG_DOWN_SET;
            }

            MEMCPY (pIPRATLV->au1Value, au1TLVMetric, sizeof (au1TLVMetric));

            for (u1Idx = 0; u1Idx < ISIS_MAX_IPV4_ADDR_LEN; u1Idx++)
            {
                pMDT->IPAddr.au1IpAddr[u1Idx] &= au1Mask[u1Idx];
            }
            MEMCPY (&pIPRATLV->au1Value[4], pMDT->IPAddr.au1IpAddr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            MEMCPY (&pIPRATLV->au1Value[4 + ISIS_MAX_IPV4_ADDR_LEN], au1Mask,
                    ISIS_MAX_IPV4_ADDR_LEN);
            if (ISIS_GET_UPDOWN_TYPE (pMDT->u1CtrlOctet) == ISIS_RL_L1_DOWN)
            {
                pIPRATLV->u1LeakedRoute = ISIS_TRUE;
            }
        }
        pIPRATLV->u1L1toL2Flag = pMDT->u1L1toL2Flag;
        /* Add the IPRA TLV to the RBtree in LSP */
        if (RBTreeAdd (pNonZeroLSP->IPRATLV, (tRBElem *) pIPRATLV) !=
            RB_SUCCESS)
        {
            i4RetVal = ISIS_FAILURE;
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRATLV ()\n"));
            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pIPRATLV);
            return (i4RetVal);
        }
    }

    else
    {
        /* Ignore the new TLV that is to be added, nevertheless generate a PANIC
         * since Self-LSPs are no more correct
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IPRA TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);

        RBTreeCount (pNonZeroLSP->IPRATLV, &u4Count);
        if ((pNonZeroLSP->pTLV == NULL) && (u4Count == 0))
        {
            /* This is a new LSP buffer allocated since the existing buffers did
             * not have enough space to hold the new IPRA information (only in
             * this case pNonZeroLSP->pTLV will be NULL). Since we are unable to
             * allocate memory for the TLV, we can release the LSP buffer also
             */
            if (pTempNonZeroLSP == pNonZeroLSP)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
            }
            else
            {
                while (pTempNonZeroLSP != NULL)
                {
                    if (pTempNonZeroLSP->pNext == pNonZeroLSP)
                    {
                        pTempNonZeroLSP->pNext = pNonZeroLSP->pNext;
                        break;
                    }
                    pTempNonZeroLSP = pTempNonZeroLSP->pNext;
                }
            }
            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pNonZeroLSP);
        }
        i4RetVal = ISIS_FAILURE;
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRATLV ()\n"));
        return (i4RetVal);
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddIPRATLV ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdAddLSPBufSizeTLV ()
 * Description : This Routine adds OriginatingLSPBufferSize TLV to SelfLSP
 * Input (s)   : pContext     - Pointer to System Context
 *               u1Level      - The Level of the System
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of IPRA TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdAddLSPBufSizeTLV (tIsisSysContext * pContext, UINT1 u1Level)
{
    UINT4               u4LSPBufSize = 0;
    tIsisTLV           *pTLV = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;

    /* When this function is called, the Non-pseudonode LSP 0
     * should be available.
     */

    if (u1Level == ISIS_LEVEL1)
    {
        pZeroLSP = pContext->SelfLSP.pL1NSNLSP->pZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize;
    }
    else
    {
        pZeroLSP = pContext->SelfLSP.pL2NSNLSP->pZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize;
    }

    if (pZeroLSP->u2LSPLen >= (pContext->SysActuals.u4SysOrigL1LSPBufSize -
                               ISIS_LSP_HDR_SIZE - ISIS_MAX_PASSWORD_LEN))
    {
        WARNING ((ISIS_LGST,
                  "UPD <E> : No space to add OriginatingLSPBufSize TLV\n"));
        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting function IsisUpdAddLSPBufSizeTLV()\n"));
        return ISIS_FAILURE;
    }
    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisTLV) +
                                        ISIS_LSP_BUF_SIZE_TLV_LEN);
    if (pTLV != NULL)
    {
        /* Form the OriginatingLSPBufferSizeTLV
         */

        pTLV->u1Code = ISIS_LSP_BUF_SIZE_TLV;
        pTLV->u1Len = ISIS_LSP_BUF_SIZE_TLV_LEN;
        ISIS_ASSIGN_2_BYTES (pTLV->au1Value, 0, (UINT2) u4LSPBufSize);

        /* Insert the TLV at the Head as this TLV is added only once
         */

        pTLV->pNext = pZeroLSP->pTLV;
        pZeroLSP->pTLV = pTLV;
    }
    else
    {
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);
        WARNING ((ISIS_LGST,
                  "UPD <E> : No Memory to add OriginatingLSPBufSize TLV\n"));
        return ISIS_FAILURE;
    }

    UPP_EE ((ISIS_LGST,
             "UPD <X> : Exiting function IsisUpdAddLSPBufSizeTLV()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisUpdDelISAdjTLV ()
 * Description : This Routine deletes the IS Adjacency information, specified 
 *               in pMDT, from the SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *             : pMDT     - Pointer to Multi Data Type structure, that 
 *                          contains the Adjacency to be deleted from 
 *                          Self LSP
 *             : u4CktIdx - Circuit Index, applicable in case of Pseudonode LSPs
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful deletion of IS Adj TLV from
 *                             Self LSP
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdDelISAdjTLV (tIsisSysContext * pContext, tIsisMDT * pMDT, UINT4 u4CktIdx)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4Count = 0;
    UINT1               u1RemBytes = 0;
    UINT1               u1LSPType = 0;
    UINT1               u1Level = 0;
    UINT1               u1PNodeFlag = ISIS_SET;
    UINT1               u1ISAdjFound = ISIS_FALSE;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisSNLSP         *pPrevSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pPrevNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisTLV           *pTravTLV = NULL;
    tIsisTLV           *pPrevTLV = NULL;
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelISAdjTLV ()\n"));

    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pMDT->au1AdjSysID, acNbrSysId);

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        u1PNodeFlag = ISIS_NOT_SET;
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP != NULL)
        {
            pNonZeroLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
        }
        u1Level = ISIS_LEVEL1;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        u1PNodeFlag = ISIS_NOT_SET;
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP != NULL)
        {
            pNonZeroLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
        }
        u1Level = ISIS_LEVEL2;
    }
    else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
    {
        pPrevSNLSP = NULL;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;

        /* Get the appropriate PseudoNode LSP based on the Circuit Index
         *
         * NOTE: Pseudonode LSPs are maintained per circuit basis
         */

        while ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL)
               && (pSNLSP->pCktEntry->u4CktIdx != u4CktIdx))
        {
            pPrevSNLSP = pSNLSP;
            pSNLSP = pSNLSP->pNext;
        }
        if (pSNLSP == NULL)
        {
            /* There may be cases where DIS status is marked as TRUE
             * for the Local System by the Adjacency module, but the Update
             * module has not processed the DIS Status change event. The Update
             * Module while processing the DIS Status change event will
             * construct pseudonode LSPs appropriately. Hence we need not worry
             * about the current adjacency at this point.
             */

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
            return (ISIS_SUCCESS);
        }
        pNonZeroLSP = pSNLSP->pNonZeroLSP;
        u1Level = ISIS_LEVEL1;
    }
    else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
    {
        pPrevSNLSP = NULL;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;

        /* Get the appropriate PseudoNode LSP based on the Circuit Index
         *
         * NOTE: Pseudonode LSPs are maintained per circuit basis
         */

        while ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL)
               && (pSNLSP->pCktEntry->u4CktIdx != u4CktIdx))
        {
            pPrevSNLSP = pSNLSP;
            pSNLSP = pSNLSP->pNext;
        }
        if (pSNLSP == NULL)
        {
            /* There may be cases where DIS status is marked as TRUE
             * for the Local System by the Adjacency module, but the Update
             * module has not processed the DIS Status change event. The Update
             * Module while processing the DIS Status change event will
             * construct pseudonode LSPs appropriately. Hence we need not worry
             * about the current adjacency at this point.
             */

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
            return (ISIS_SUCCESS);
        }
        pNonZeroLSP = pSNLSP->pNonZeroLSP;
        u1Level = ISIS_LEVEL2;
    }

    /* Go through the chain of SelfLSPs till the appropriate LSP buffer which
     * includes information matching the given IS Adjacency is found
     */

    pTravNonZeroLSP = pNonZeroLSP;

    while (pTravNonZeroLSP != NULL)
    {
        /* Since we are starting with a New TLV chain, initialize the pPrevTLV
         * pointer to NULL.
         */

        pPrevTLV = NULL;
        pTravTLV = pTravNonZeroLSP->pTLV;

        /* Scan the TLV List in each LSP buffer
         */

        while (pTravTLV != NULL)
        {
            if (pTravTLV->u1Code == pMDT->u1TLVType)
            {
                /* First four bytes of 'au1Value' holds the four metrics
                 */

                /* NOTE: Same Peer may have multiple adjacencies with the local
                 * system over different circuits. Hence we have to match the
                 * circuit before deleting the adjacency information
                 */

                if ((pTravTLV->u4CktIdx == u4CktIdx)
                    && (((MEMCMP (pTravTLV->au1Value + 4, pMDT->au1AdjSysID,
                                  ISIS_SYS_ID_LEN + 1) == 0)
                         && ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                             || (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)))
                        || ((MEMCMP (pTravTLV->au1Value + 4, pMDT->au1AdjSysID,
                                     ISIS_SYS_ID_LEN) == 0)
                            && ((u1LSPType == ISIS_L1_PSEUDO_LSP)
                                || (u1LSPType == ISIS_L2_PSEUDO_LSP)))))

                {
                    /* A matching TLV is found
                     */

                    ISIS_DBG_PRINT_ID (pMDT->au1AdjSysID,
                                       (UINT1) (ISIS_SYS_ID_LEN + 1),
                                       "UPD <T> : Deleting Adjacency\t",
                                       ISIS_OCTET_STRING);

                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Deleting IS Adjacency From Self-LSP - Adjacent System ID [ %s ], LSP Type [ %s ]\n",
                             acNbrSysId, ISIS_GET_SELF_LSP_TYPE (u1LSPType)));
                    u1ISAdjFound = ISIS_TRUE;
                    i4RetVal = ISIS_SUCCESS;

                    if (pPrevTLV != NULL)
                    {
                        /* Deleting a TLV which is not the first TLV in the
                         * chain
                         */

                        pPrevTLV->pNext = pTravTLV->pNext;
                    }
                    else
                    {
                        /* The TLV to be deleted is the first one in the list of
                         * TLVs maintained in the LSP buffer
                         */

                        pTravNonZeroLSP->pTLV = pTravTLV->pNext;
                    }
                    pTravTLV->pNext = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                    break;
                }
                else
                {
                    pPrevTLV = pTravTLV;
                    pTravTLV = pTravTLV->pNext;
                }
            }
            else
            {
                pPrevTLV = pTravTLV;
                pTravTLV = pTravTLV->pNext;
            }
        }

        if (u1ISAdjFound == ISIS_TRUE)
        {
            /* Verify whether removal of the specified IS Adjacency from the LSP
             * buffer, has left the LSP buffer empty without any TLVs
             */

            RBTreeCount (pTravNonZeroLSP->IPRATLV, &u4Count);
            if ((pTravNonZeroLSP->pTLV == NULL) && (u4Count == 0))
            {
                /* No more TLVs left in the LSP buffer. Now try to remove the
                 * LSP buffer also from the Self LSPs
                 */

                if (pPrevNonZeroLSP == NULL)
                {
                    /* The LSP buffer to be removed is the first LSP buffer in
                     * the list. We have to advance the head
                     */

                    if (u1PNodeFlag == ISIS_SET)
                    {
                        /* The buffer to be removed is one of the Pseudonode LSP
                         * buffers
                         */

                        pSNLSP->pNonZeroLSP = pTravNonZeroLSP->pNext;
                    }
                    else
                    {
                        /* The buffer to be removed is one of the Non-Pseudonode
                         * LSP buffers
                         */

                        pNSNLSP->pNonZeroLSP = pTravNonZeroLSP->pNext;
                    }
                }
                else
                {
                    /* The buffer to be removed is not the first one in the list
                     */

                    pPrevNonZeroLSP->pNext = pTravNonZeroLSP->pNext;
                }

                /* Refer 7.3.4.6 in ISO 10589 of Addendum - 1 which includes
                 * corrections to the base document
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Purging Self-LSP Info - Reason [ IS Adjacency Deleted - LSP Buffer Empty ]\n"));
                IsisUpdFindAndPurgeSelfLSP (pContext, pTravNonZeroLSP, u1Level);
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pTravNonZeroLSP);
                if (((u1LSPType == ISIS_L1_PSEUDO_LSP) ||
                     (u1LSPType == ISIS_L2_PSEUDO_LSP)) &&
                    (pSNLSP->pNonZeroLSP == NULL))
                {
                    pZeroLSP = pSNLSP->pZeroLSP;
                    IsisUpdFindAndPurgeSelfLSP (pContext, pZeroLSP, u1Level);
                    ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pZeroLSP);
                    if (pPrevSNLSP == NULL)
                    {
                        if (u1Level == ISIS_LEVEL1)
                        {
                            pContext->SelfLSP.pL1SNLSP = pSNLSP->pNext;
                        }
                        else
                        {
                            pContext->SelfLSP.pL2SNLSP = pSNLSP->pNext;
                        }
                    }
                    else
                    {
                        pPrevSNLSP->pNext = pSNLSP->pNext;
                    }
                    pSNLSP->pNext = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_SNLI, (UINT1 *) pSNLSP);
                }

            }
            else
            {
                /* Some TLVs are still left in the Buffer from which we have
                 * deleted the given adjacency information. We have to adjust
                 * the length of the Buffer appropriately since the removal of a
                 * TLV will decrease the length of the buffer
                 */

                pTravNonZeroLSP->u1NumISTLV--;

                u1RemBytes =
                    (UINT1) ISIS_GET_REM_BYTES (pTravNonZeroLSP->u1NumISTLV,
                                                ISIS_IS_ADJ_TLV_LEN);

                /* LOGIC: Decrement the number of TLVs. We now have to decide
                 * whether to decrement the buffer length by
                 * "ISIS_IS_ADJ_TLV_LEN" or "ISIS_IS_ADJ_TLV_LEN + 2" (2 bytes
                 * for Code and Length).
                 * Now assume you are going to add back the Adjacency 
                 * information, that is just now deleted, to the SelfLSP buffer.
                 * The number of TLVs will be the number obtained after 
                 * decrementing the total number by '1'. If 'u1RemBytes' 
                 * indicates that we have to add a new TLV, then we can
                 * decrement the length of the buffer by "ISIS_IS_ADJ_TLV_LEN +
                 * 2", else we must decrement the length only by
                 * "ISIS_IS_ADJ_TLV_LEN". This is because, if addition causes
                 * '2' extra bytes to be included, then deletion must remove
                 * those '2' extra bytes
                 */

                if (u1RemBytes < ISIS_IS_ADJ_TLV_LEN)
                {
                    pTravNonZeroLSP->u2LSPLen -= (ISIS_IS_ADJ_TLV_LEN + 2);
                }
                else
                {
                    pTravNonZeroLSP->u2LSPLen -= ISIS_IS_ADJ_TLV_LEN;
                }
                pTravNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
            }
            break;
        }
        else
        {
            /* Moving the Pointer to next LSP when the appropriate TLV is not
             * there in the present TLV
             */

            pPrevNonZeroLSP = pTravNonZeroLSP;
            pTravNonZeroLSP = pTravNonZeroLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelISAdjTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdDelAreaAddrTLV ()
 * Description : This Routine deletes the Area Address information, specified 
 *               in pMDT, from the SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *             : pMDT     - Pointer to Multi Data Type structure, that 
 *                          contains the Area Address to be deleted from 
 *                          Self LSP
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful deletion of Area Address
 *               information from Self LSP
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisUpdDelAreaAddrTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1RemBytes = 0;
    UINT1               u1LSPType = 0;
    tIsisTLV           *pTravTLV = NULL;
    tIsisTLV           *pPrevTLV = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;
    UINT1               au1AreaAddr[ISIS_AREA_ADDR_LEN];
    CHAR                acAreaAddr[(3 * ISIS_AREA_ADDR_LEN) + 1];
    UINT1               au1NullIP[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1AreaAddr, 0, ISIS_AREA_ADDR_LEN);
    MEMSET (acAreaAddr, 0, ((3 * ISIS_AREA_ADDR_LEN) + 1));
    MEMSET (au1NullIP, 0, ISIS_MAX_IP_ADDR_LEN);

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelAreaAddrTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pZeroLSP = pContext->SelfLSP.pL1NSNLSP->pZeroLSP;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pZeroLSP = pContext->SelfLSP.pL2NSNLSP->pZeroLSP;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelAreaAddrTLV ()\n"));
        return i4RetVal;

    }

    /* NOTE: Area Address should not be present in PseudoNode LSPs and Area
     * Addresses included in NonZero LSPs can be ignored
     * Refer Sec 9.8 of ISO 10589 Specifiction
     */

    /* Scan through the entire TLV list and get the matching information record
     */

    if ((MEMCMP
         (pMDT->AreaAddress.au1AreaAddr, au1NullIP,
          ISIS_MAX_IPV4_ADDR_LEN)) != 0)
    {
        MEMCPY (au1AreaAddr, pMDT->AreaAddress.au1AreaAddr, ISIS_AREA_ADDR_LEN);
        ISIS_FORM_STR (ISIS_AREA_ADDR_LEN, au1AreaAddr, acAreaAddr);
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Deleting Area Address From Self-LSP [ %s ], LSP Type [ %s ]\n",
                 acAreaAddr, ISIS_GET_SELF_LSP_TYPE (pMDT->u1LSPType)));
    }

    pTravTLV = pZeroLSP->pTLV;

    while (pTravTLV != NULL)
    {
        if (pTravTLV->u1Code == pMDT->u1TLVType)
        {
            /* Found a matching type of record. Verify whether the Length and
             * contents are same
             */

            if ((((pTravTLV->u1Len) - 1) == pMDT->AreaAddress.u1Length)
                && (pMDT->AreaAddress.u1Length < ISIS_AREA_ADDR_LEN))
            {
                /* Lenghts are same, Verify the contents
                 */

                if ((MEMCMP (&(pTravTLV->au1Value[1]),
                             &(pMDT->AreaAddress.au1AreaAddr),
                             pMDT->AreaAddress.u1Length)) == 0)

                {
                    /* Found an exact match. Remove the TLV from the List of
                     * TLVs from the SelfLSP
                     */

                    ISIS_DBG_PRINT_ADDR (pMDT->AreaAddress.au1AreaAddr,
                                         pMDT->AreaAddress.u1Length,
                                         "UPD <T> : Deleted SelfLSP AreaAddr\t",
                                         ISIS_OCTET_STRING);

                    i4RetVal = ISIS_SUCCESS;

                    if (pPrevTLV != NULL)
                    {
                        /* Not a first TLV in the List */

                        pPrevTLV->pNext = pTravTLV->pNext;
                        pTravTLV->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                        break;
                    }
                    else
                    {
                        /* Very first TLV in the List and hence advance the head
                         */

                        pZeroLSP->pTLV = pTravTLV->pNext;
                        pTravTLV->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                        break;
                    }
                }
            }
        }

        pPrevTLV = pTravTLV;
        pTravTLV = pTravTLV->pNext;
    }

    if (i4RetVal == ISIS_SUCCESS)
    {
        pZeroLSP->u1NumAATLV--;

        u1RemBytes = (UINT1) ISIS_GET_REM_BYTES (pZeroLSP->u1NumAATLV,
                                                 pMDT->AreaAddress.u1Length +
                                                 1);

        /* LOGIC: Decrement the number of TLVs. We now have to decide
         * whether to decrement the buffer length by
         * "ISIS_AREA_ADDR_TLV_LEN" or "ISIS_AREA_ADDR_TLV_LEN + 2" (2 bytes
         * for Code and Length).
         * Now assume you are going to add back the Adjacency 
         * information, that is just now deleted, to the SelfLSP buffer.
         * The number of TLVs will be the number obtained after 
         * decrementing the total number by '1'. If 'u1RemBytes' 
         * indicates that we have to add a new TLV, then we can
         * decrement the length of the buffer by "ISIS_AREA_ADDR_TLV_LEN +
         * 2", else we must decrement the length only by
         * "ISIS_AREA_ADDR_TLV_LEN". This is because, if addition causes
         * '2' extra bytes to be included, then deletion must remove
         * those '2' extra bytes
         */

        if (u1RemBytes < ISIS_AREA_ADDR_TLV_LEN)
        {
            /* Area Address + Area Address Length (1 byte) + (Code and Length)
             * (2 bytes)
             */

            pZeroLSP->u2LSPLen -= (pMDT->AreaAddress.u1Length + 1 + 2);
        }
        else
        {
            /* Area Address + Area Address Length (1 byte) 
             */

            pZeroLSP->u2LSPLen -= (pMDT->AreaAddress.u1Length + 1);
        }
        pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelAreaAddrTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdDelProtSuppTLV ()
 * Description : This routine deletes the Protocol Support TLV from the SelfLSP.
 *               In the case of Pseudonode LSPs the given information is deleted
 *               from each of the Zero LSPs
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to Multi Data Type Structure which
 *                          contains the Protocol Supported Entry to be
 *                          deleted
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Deletion of the Protocol
 *                             Supported TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisUpdDelProtSuppTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    UINT1               u1LSPType = 0;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelProtSuppTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
    }
    else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
    }

    else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
    }

    if (pNSNLSP != NULL)
    {
        pZeroLSP = pNSNLSP->pZeroLSP;
        IsisUpdDelPSTLVFromSelfLSP (pZeroLSP, pMDT);
    }
    else if (pSNLSP != NULL)
    {
        while (pSNLSP != NULL)
        {
            pZeroLSP = pSNLSP->pZeroLSP;
            IsisUpdDelPSTLVFromSelfLSP (pZeroLSP, pMDT);
            pSNLSP = pSNLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelProtSuppTLV ()\n"));
    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisUpdDelPSTLVFromSelfLSP ()
 * Description : This routine deletes the given Protocol Support TLV from
 *               SelfLSP
 * Input (s)   : pZeroLSP - Pointer to Zero LSP
 *               pMDT     - Pointer to Multi Data Type Structure which
 *                          contains the Protocol Supported Entry to be
 *                          deleted
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdDelPSTLVFromSelfLSP (tIsisLSPInfo * pZeroLSP, tIsisMDT * pMDT)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1RemBytes = 0;
    tIsisTLV           *pTravTLV = NULL;
    tIsisTLV           *pPrevTLV = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelPSTLVFromSelfLSP ()\n"));

    pTravTLV = pZeroLSP->pTLV;

    /* Go through the chain of TLVs in the given Zero LSP and delete the
     * matching TLV
     */

    UPP_PT ((ISIS_LGST,
             "UPD <T> : Deleting Protocol Support Info From Self-LSP [ %s ], LSP Type [ %s ]\n",
             ISIS_GET_PROT_SUPP (pMDT->u1ProtSupp),
             ISIS_GET_LSP_STR (pMDT->u1LSPType)));

    while (pTravTLV != NULL)
    {
        if (pTravTLV->u1Code == ISIS_PROT_SUPPORT_TLV)
        {
            if ((MEMCMP (pTravTLV->au1Value, &(pMDT->u1ProtSupp),
                         ISIS_PROT_SUPP_TLV_LEN)) == 0)
            {
                i4RetVal = ISIS_SUCCESS;

                if (pPrevTLV != NULL)
                {
                    /* Deleting the PS TLV which is not the first one in the 
                     * chain
                     */

                    pPrevTLV->pNext = pTravTLV->pNext;
                    ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                    break;
                }
                else
                {
                    /* Deleting the PS TLV which is the first one in the chain
                     */

                    pZeroLSP->pTLV = pTravTLV->pNext;
                    ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                    break;
                }
            }
        }
        pPrevTLV = pTravTLV;
        pTravTLV = pTravTLV->pNext;
    }

    if (i4RetVal == ISIS_SUCCESS)
    {
        pZeroLSP->u1NumPSTLV--;

        u1RemBytes = (UINT1) ISIS_GET_REM_BYTES (pZeroLSP->u1NumPSTLV,
                                                 ISIS_PROT_SUPP_TLV_LEN);

        /* LOGIC: Decrement the number of TLVs. We now have to decide
         * whether to decrement the buffer length by
         * "ISIS_PROT_SUPP_TLV_LEN" or "ISIS_PROT_SUPP_TLV_LEN + 2" (2 bytes
         * for Code and Length).
         * Now assume you are going to add back the Protocol support 
         * information, that is just now deleted, to the SelfLSP buffer.
         * The number of TLVs will be the number obtained after 
         * decrementing the total number by '1'. If 'u1RemBytes' 
         * indicates that we have to add a new TLV, then we can
         * decrement the length of the buffer by "ISIS_PROT_SUPP_TLV_LEN +
         * 2", else we must decrement the length only by
         * "ISIS_PROT_SUPP_TLV_LEN". This is because, if addition causes
         * '2' extra bytes to be included, then deletion must remove
         * those '2' extra bytes
         */

        if (u1RemBytes < ISIS_PROT_SUPP_TLV_LEN)
        {
            pZeroLSP->u2LSPLen -= (ISIS_PROT_SUPP_TLV_LEN + 2);
        }
        else
        {
            pZeroLSP->u2LSPLen -= (ISIS_PROT_SUPP_TLV_LEN);
        }
        pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelPSTLVFromSelfLSP ()\n"));
    return;
}

/******************************************************************************
 * Function    : IsisUpdDelIPIfAddrTLV ()
 * Description : This Routine Deletes the given IP Interface Address TLV from 
 *               SelfLSP. In the case of Pseudonode LSPs the given information 
 *               is deleted from each of the Zero LSPs
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to Multi Data Structure which contains 
 *                          the IP Interface Address to be deleted
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Deletion of the IP Interface
 *                             TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisUpdDelIPIfAddrTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    UINT1               u1LSPType = 0;
    UINT1               u1Level = 0;
    UINT1               u1PNodeFlag = ISIS_SET;
    UINT1               u1Count = 0;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelIPIfAddrTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        u1Level = ISIS_LEVEL1;
        u1PNodeFlag = ISIS_NOT_SET;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        u1Level = ISIS_LEVEL2;
        u1PNodeFlag = ISIS_NOT_SET;
    }
    else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
        u1Level = ISIS_LEVEL1;
    }
    else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
        u1Level = ISIS_LEVEL2;
    }

    if ((pNSNLSP != NULL) && (pNSNLSP->pNonZeroLSP != NULL))
    {
        if (pMDT->u1TLVType == ISIS_IPV6IF_ADDR_TLV)
        {
            pMDT->u1ProtSupp = ISIS_IPV6_SUPP;
            u1Count = pNSNLSP->pNonZeroLSP->u1NumIPV6IfTLV;
        }
        else
        {
            pMDT->u1ProtSupp = ISIS_IPV4_SUPP;
            u1Count = pNSNLSP->pNonZeroLSP->u1NumIPIfTLV;
        }
        /* When Deleting the last Ipv4/Ipv6 address TLV delete the 
         * IPv4/IPv6 Protocol Supported TLV also
         */

        if (u1Count == 1)
        {
            IsisUpdDelProtSuppTLV (pContext, pMDT);
        }
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        IsisUpdDelIPIfTLVFromSelfLSP (pContext, u1PNodeFlag, pNonZeroLSP, pMDT,
                                      u1Level);
    }
    else if (pSNLSP != NULL)
    {
        while (pSNLSP != NULL)
        {
            if (pMDT->u1TLVType == ISIS_IPV6IF_ADDR_TLV)
            {
                pMDT->u1ProtSupp = ISIS_IPV6_SUPP;
            }
            else
            {
                pMDT->u1ProtSupp = ISIS_IPV4_SUPP;
            }
            IsisUpdDelProtSuppTLV (pContext, pMDT);

            pNonZeroLSP = pSNLSP->pNonZeroLSP;
            IsisUpdDelIPIfTLVFromSelfLSP (pContext, u1PNodeFlag,
                                          pNonZeroLSP, pMDT, u1Level);
            pSNLSP = pSNLSP->pNext;
        }
    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelIPIfAddrTLV ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdDelIPIfTLVFromSelfLSP ()
 * Description : This routine Deletes the given IP Interface Address TLV from 
 *               SelfLSP.
 * Input (s)   : pContext    - Pointer to System Context
 *               u1PNodeFlag - Flag indicating the LSP type (either Pseudonode
 *                             or Non-Pseudonode)
 *               pNonZeroLSP - The LSP from which the information is to be
 *                             deleted              
 *               pMDT        - Pointer to Multi Data Structure which contains 
 *                             the IP Interface Address to be deleted
 *               u1Level     - Level of LSP (Level1 or Level2)              
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : VOID
 ******************************************************************************/

PRIVATE VOID
IsisUpdDelIPIfTLVFromSelfLSP (tIsisSysContext * pContext, UINT1 u1PNodeFlag,
                              tIsisLSPInfo * pNonZeroLSP, tIsisMDT * pMDT,
                              UINT1 u1Level)
{
    UINT4               u4Count = 0;
    UINT1               u1RemBytes = 0;
    UINT1               u1IPIfFoundFlag = ISIS_FALSE;
    tIsisTLV           *pPrevTLV = NULL;
    tIsisTLV           *pTravTLV = NULL;
    tIsisLSPInfo       *pPrevNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelIPIfTLVFromSelfLSP ()\n"));

    ISIS_FORM_IPV4_ADDR (pMDT->IPAddr.au1IpAddr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);
    pTravNonZeroLSP = pNonZeroLSP;

    /* Go through the LSP buffer chain and verify whether the given IP I/f
     * Address information is included in the TLV chain
     */
    if (pMDT->u1TLVType == ISIS_IPV6IF_ADDR_TLV)
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Deleting IPv6 Interface Address From Self-LSP [ %s ], LSP Type [ %s ]\n",
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pMDT->IPAddr.au1IpAddr)),
                 ISIS_GET_LSP_STR (pMDT->u1LSPType)));
    }
    if (pMDT->u1TLVType == ISIS_IPV4IF_ADDR_TLV)
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Deleting IPv4 Interface Address From Self-LSP [ %s ], LSP Type [ %s ]\n",
                 au1IPv4Addr, ISIS_GET_LSP_STR (pMDT->u1LSPType)));
    }

    while (pTravNonZeroLSP != NULL)
    {
        /* Since we are starting with a New TLV chain, initialize the pPrevTLV
         * pointer to NULL.
         */

        pPrevTLV = NULL;
        pTravTLV = pTravNonZeroLSP->pTLV;

        /* Verify each of the TLV information for a match
         */

        while (pTravTLV != NULL)
        {
            if (pTravTLV->u1Code == pMDT->u1TLVType)
            {
                if (pMDT->IPAddr.u1Length <= ISIS_MAX_IP_ADDR_LEN)
                {
                    if ((MEMCMP (pTravTLV->au1Value, pMDT->IPAddr.au1IpAddr,
                                 pMDT->IPAddr.u1Length)) == 0)
                    {
                        u1IPIfFoundFlag = ISIS_TRUE;

                        ISIS_DBG_PRINT_ADDR (pMDT->IPAddr.au1IpAddr,
                                             pMDT->IPAddr.u1Length,
                                             "UPD <T> : Deleting IP I/f Addr\t",
                                             ISIS_OCTET_STRING);

                        if (pPrevTLV != NULL)
                        {
                            /* Deleting a TLV which is not the first TLV in the
                             * chain
                             */

                            pPrevTLV->pNext = pTravTLV->pNext;
                            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                            break;
                        }
                        else
                        {
                            /* The TLV to be deleted is the first one in the list of
                             * TLVs maintained in the LSP buffer
                             */

                            pTravNonZeroLSP->pTLV = pTravTLV->pNext;
                            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                            break;
                        }
                    }
                }
            }
            pPrevTLV = pTravTLV;
            pTravTLV = pTravTLV->pNext;
        }

        if (u1IPIfFoundFlag == ISIS_TRUE)
        {
            /* Verify whether removal of the specified IS Adjacency from the LSP
             * buffer, has left the LSP buffer empty without any TLVs
             */
            RBTreeCount (pTravNonZeroLSP->IPRATLV, &u4Count);
            if ((pTravNonZeroLSP->pTLV == NULL) && (u4Count == 0))
            {
                /* No more TLVs left in the LSP buffer. Now try to remove the
                 * LSP buffer also from the Self LSPs
                 */

                if (pPrevNonZeroLSP == NULL)
                {
                    /* The LSP buffer to be removed is the first LSP buffer in
                     * the list. We have to advance the head
                     */

                    if (u1PNodeFlag != ISIS_SET)
                    {
                        /* The buffer to be removed is one of the Non-Pseudonode
                         * LSP buffers
                         */

                        if (u1Level == ISIS_LEVEL1)
                        {
                            pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP
                                = pTravNonZeroLSP->pNext;
                        }
                        else if (u1Level == ISIS_LEVEL2)
                        {
                            pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP
                                = pTravNonZeroLSP->pNext;
                        }
                    }
                    else
                    {
                        /* The buffer to be removed is one of the Pseudonode LSP
                         * buffers
                         */

                        if (u1Level == ISIS_LEVEL1)
                        {
                            pContext->SelfLSP.pL1SNLSP->pNonZeroLSP
                                = pTravNonZeroLSP->pNext;
                        }
                        else if (u1Level == ISIS_LEVEL2)
                        {
                            pContext->SelfLSP.pL2SNLSP->pNonZeroLSP
                                = pTravNonZeroLSP->pNext;
                        }
                    }
                }
                else
                {
                    pPrevNonZeroLSP->pNext = pTravNonZeroLSP->pNext;
                }

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Purging Self-LSP Info - Reason [ IP Interface Address Deleted - LSP Buffer Empty ]\n"));
                IsisUpdFindAndPurgeSelfLSP (pContext, pTravNonZeroLSP, u1Level);
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pTravNonZeroLSP);
            }
            else
            {
                /* Some TLVs are still left in the Buffer from which we have
                 * deleted the given adjacency information. We have to adjust
                 * the length of the Buffer appropriately since the removal of a
                 * TLV will decrease the length of the buffer
                 */
                if (pMDT->u1TLVType == ISIS_IPV6IF_ADDR_TLV)
                {
                    pTravNonZeroLSP->u1NumIPV6IfTLV--;

                    u1RemBytes =
                        (UINT1) ISIS_GET_REM_BYTES (pTravNonZeroLSP->
                                                    u1NumIPV6IfTLV,
                                                    ISIS_MAX_IPV6_ADDR_LEN);
                    if (u1RemBytes < ISIS_MAX_IPV6_ADDR_LEN)
                    {
                        pTravNonZeroLSP->u2LSPLen -= 2;
                    }
                    pTravNonZeroLSP->u2LSPLen -= ISIS_MAX_IPV6_ADDR_LEN;
                }
                else
                {
                    pTravNonZeroLSP->u1NumIPIfTLV--;

                    u1RemBytes =
                        (UINT1) ISIS_GET_REM_BYTES (pTravNonZeroLSP->
                                                    u1NumIPIfTLV,
                                                    ISIS_MAX_IPV4_ADDR_LEN);
                    if (u1RemBytes < ISIS_MAX_IPV4_ADDR_LEN)
                    {
                        pTravNonZeroLSP->u2LSPLen -= 2;
                    }
                    pTravNonZeroLSP->u2LSPLen -= ISIS_MAX_IPV4_ADDR_LEN;

                }

                /* LOGIC: Decrement the number of TLVs. We now have to decide
                 * whether to decrement the buffer length by
                 * "ISIS_IPV4_ADDR_TLV_LEN" or "ISIS_IPV4_ADDR_TLV_LEN + 2" 
                 * (2 bytes for Code and Length).
                 * Now assume you are going to add back the IP I/f Address 
                 * information, that is just now deleted, to the SelfLSP buffer.
                 * The number of TLVs will be the number obtained after 
                 * decrementing the total number by '1'. If 'u1RemBytes' 
                 * indicates that we have to add a new TLV, then we can
                 * decrement the length of the buffer by 
                 * "ISIS_IPV4_ADDR_TLV_LEN + 2", else we must decrement the 
                 * length only by "ISIS_IPV4_ADDR_TLV_LEN". This is because, if
                 * addition causes '2' extra bytes to be included, then deletion
                 * must remove those '2' extra bytes
                 */

                pTravNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
            }
            break;
        }
        else
        {
            pPrevNonZeroLSP = pTravNonZeroLSP;
            pTravNonZeroLSP = pTravNonZeroLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelIPIfTLVFromSelfLSP ()\n"));
}

/******************************************************************************
 * Function    : IsisUpdDelIPRATLV ()
 * Description : This routine deletes the given IPRA Information from SelfLSP
 * Input (s)   : pContext     - Pointer to System Context
 *               pMDT         - Pointer to Multi Data Type structure, that
 *                              contains the IPRA to be deleted
 *               u1SAChk      - Flag indicating whether a matching Summary 
 *                              Address is to be added to Self LSP. In case of
 *                              summary addresses themselves being added viz
 *                              this routine or in the case of SPT PATH Nodes
 *                              being added, this flag will be set to FALSE 
 *               u1SADcrFlag  - Flag Indicating whether the SA usage count 
 *                              is to be updated. The flag will be set to FALSE
 *                              in case of addtions being made for Summary
 *                              Addresses to PATH nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Deletion of the IPRA Info
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdDelIPRATLV (tIsisSysContext * pContext, tIsisMDT * pMDT, UINT1 u1SAChk,
                   UINT1 u1SADcrFlag)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4Count = 0;
    UINT4               u4MetricVal = 0;
    UINT1               u1Level = 0;
    UINT1               u1SAState = ISIS_SUMM_ADMIN_OFF;
    UINT1               u1LSPType = 0;
    UINT1               u1RemBytes = 0;
    UINT1               u1TLVDeleted = ISIS_FALSE;
    UINT1               au1TLVMetric[4];
    UINT1               au1Metric[4];
    tIsisRRDRoutes     *pRRDRoute = NULL;
    tIsisIPRATLV       *pTravTLV = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisSAEntry       *pSARec = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pPrevNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisIPRATLV        IPRATravTLV;
    tIsisRRDRoutes      RRDRoute;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelIPRATLV ()\n"));
    MEMSET (&IPRATravTLV, 0, sizeof (tIsisIPRATLV));
    ISIS_FORM_IPV4_ADDR (pMDT->IPAddr.au1IpAddr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    u1LSPType = pMDT->u1LSPType;
    /* NOTE: IPRAs are present only in the Non-Pseudonode LSPs
     */

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP != NULL)
        {
            pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        }
        u1Level = ISIS_LEVEL1;
        u1SAState = ISIS_SUMM_ADMIN_L1;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP != NULL)
        {
            pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        }
        u1Level = ISIS_LEVEL2;
        u1SAState = ISIS_SUMM_ADMIN_L2;
    }

    /* NOTE: 
     *
     * 1. u1SAChk = FALSE implies no need to bothger about the matching Summary
     *    Address record. This is the case when Summary Address entries 
     *    themselves are being deleted
     *    
     * 2. u1SAChk = TRUE implies Summary Address entries are to be updated for
     *    usage count since information being Deleted is IPRA information. Then
     *
     *    2a. u1SADcrFlag = TRUE implies that the usage count is to be
     *        decremented since the entry being deleted is IPRA information
     *        configured by the manager
     *    2b. u1SADcrFlag = FALSE implies that the usage count need not be
     *        decremented since the entry being deleted is either IPRA 
     *        information retrieved from the PATH nodes or it is Summary 
     *        Address Information
     */

    if (u1SAChk == ISIS_TRUE)
    {
        pSARec = pContext->SummAddrTable.pSAEntry;
    }

    /* Go through the summary address entries and get an entry that matches the
     * given IPRA
     */

    while (pSARec != NULL)
    {
        if (((pSARec->u1AdminState == u1SAState)
             || (pSARec->u1AdminState == ISIS_SUMM_ADMIN_L12))
            && ((IsisUtlCompIPAddr (pSARec->au1SummAddr,
                                    pSARec->u1PrefixLen,
                                    pMDT->IPAddr.au1IpAddr,
                                    pSARec->u1PrefixLen)) == ISIS_TRUE))
        {
            if (u1SADcrFlag == ISIS_TRUE)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pSARec->u2L1UsageCnt--;
                }
                else
                {
                    pSARec->u2L2UsageCnt--;
                }

            }

            /* Check if the given IPRA is the only entry referring to this 
             * Summary Address (u2UsageCount == 0). A Non-Zero Usage count
             * indicates that there are other IPRAs which matched the Summary
             * Address entry and hence the TLV containing this SA entry must be
             * retained in the SelfLSP. If Usage count is '0', then we can
             * search for the TLV containing information regarding the matching
             * SA entry and delete it.
             */

            if (((pSARec->u2L1UsageCnt == 1)
                 && (u1LSPType == ISIS_L1_NON_PSEUDO_LSP))
                || ((pSARec->u2L2UsageCnt == 1)
                    && (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)))
            {
                /* Mark this information, we will delete the SA TLV from
                 * SelfLSP later
                 */

                MEMCPY (pMDT->Metric, pSARec->Metric, (sizeof (tIsisMetric)));
                if (pSARec->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pMDT->IPAddr.au1IpAddr, pSARec->au1SummAddr,
                            ISIS_MAX_IPV4_ADDR_LEN);
                    pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
                    pMDT->IPAddr.u1PrefixLen = pSARec->u1PrefixLen;
                }
                else
                {

                    MEMCPY (pMDT->IPAddr.au1IpAddr, pSARec->au1SummAddr,
                            ISIS_MAX_IPV6_ADDR_LEN);
                    pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
                    pMDT->IPAddr.u1PrefixLen = pSARec->u1PrefixLen;
                }
            }
            else
            {
                /* There are other IPRAs referring to the Summary Address entry,
                 * hence do not delete the TLV containing the information
                 */

                return ISIS_SUCCESS;
            }
            break;
        }

        pSARec = pSARec->pNext;
    }

    /* Form the metric string for the given IPRA */
    if (pMDT->u1TLVType != ISIS_IPV6_RA_TLV)
    {
        IsisUtlFormMetric (pContext, pMDT->Metric, au1TLVMetric);
    }

    pTravNonZeroLSP = pNonZeroLSP;

    /* Go through the chain of SelfLSPs till the appropriate LSP buffer which
     * includes information matching the given IPRA is found
     */
    IPRATravTLV.u1Code = pMDT->u1TLVType;
    MEMCPY (IPRATravTLV.IPAddr.au1IpAddr,
            pMDT->IPAddr.au1IpAddr,
            (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
    IPRATravTLV.IPAddr.u1PrefixLen = pMDT->IPAddr.u1PrefixLen;
    if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Deleting IPv6 IPRA From Self-LSP [ %s ], LSP Type [ %s ]\n",
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pMDT->IPAddr.au1IpAddr)),
                 ISIS_GET_LSP_STR (u1LSPType)));
    }
    if ((pMDT->u1TLVType == ISIS_IP_INTERNAL_RA_TLV)
        || (pMDT->u1TLVType == ISIS_IP_EXTERNAL_RA_TLV))
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Deleting IPv4 IPRA From Self-LSP [ %s ], LSP Type [ %s ]\n",
                 au1IPv4Addr, ISIS_GET_LSP_STR (u1LSPType)));
    }
    while (pTravNonZeroLSP != NULL)
    {
        pTravTLV = (tIsisIPRATLV *)
            RBTreeGet (pTravNonZeroLSP->IPRATLV, (tRBElem *) & IPRATravTLV);

        if ((pTravTLV != NULL) && (pMDT->u1SrcProtoId == pTravTLV->u1Protocol))
        {
            RBTreeRemove (pTravNonZeroLSP->IPRATLV, (tRBElem *) pTravTLV);
            ISIS_DBG_PRINT_ADDR (pMDT->IPAddr.au1IpAddr,
                                 (UINT1) pMDT->IPAddr.u1Length,
                                 "UPD <T> : Deleted IPRA - SelfLSP\t",
                                 ISIS_OCTET_STRING);

            u1TLVDeleted = ISIS_TRUE;
            i4RetVal = ISIS_SUCCESS;
        }
        if ((u1TLVDeleted != ISIS_TRUE) &&
            (pTravTLV != NULL) &&
            (pTravTLV->u1Protocol == ISIS_LOCAL_ID) &&
            (pMDT->u1SrcProtoId == 0))
        {
            pTravTLV->u1IsisEnabled = ISIS_FALSE;
            MEMSET (&RRDRoute.IPAddr, 0, sizeof (tIsisIpAddr));
            MEMCPY (&(RRDRoute.IPAddr), &(pTravTLV->IPAddr),
                    sizeof (tIsisIpAddr));
            if ((pTravTLV->u1Code == ISIS_IP_INTERNAL_RA_TLV)
                || (pTravTLV->u1Code == ISIS_EXT_IP_REACH_TLV))
            {
                RRDRoute.IPAddr.u1AddrType = ISIS_IPV4;
            }
            else
            {
                RRDRoute.IPAddr.u1AddrType = ISIS_IPV6;
            }
            pRRDRoute =
                RBTreeGet (pContext->RRDRouteRBTree, ((tRBElem *) & RRDRoute));
            if (pRRDRoute != NULL)
            {
                au1Metric[0] = (UINT1) pRRDRoute->u4Metric;
                /* update the metric alone */
                if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                {
                    if (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC)
                    {
                        COPY_U4_FROM_BUF (u4MetricVal, au1Metric);
                    }
                    else
                    {
                        u4MetricVal = pRRDRoute->u4Metric;
                        u4MetricVal = OSIX_HTONL (u4MetricVal);
                    }
                    MEMCPY (pTravTLV->au1Value, &u4MetricVal,
                            sizeof (tIsisMetric));
                }
                else
                {
                    IsisUtlFormMetric (pContext, au1Metric, au1TLVMetric);
                    MEMCPY (pTravTLV->au1Value, au1TLVMetric,
                            sizeof (au1TLVMetric));
                }
            }
        }

        if (u1TLVDeleted == ISIS_TRUE)
        {
            /* We have deleted a TLV, and hence we now have to verify whether
             * the Buffer which held the deleted TLV has become empty. If there
             * are no more TLVs in the buffer, then the LSP can be purged.
             */
            RBTreeCount (pTravNonZeroLSP->IPRATLV, &u4Count);
            if ((pTravNonZeroLSP->pTLV == NULL) && (u4Count == 0))
            {
                /* No more LSPs in the Buffer
                 */

                if (pPrevNonZeroLSP == NULL)
                {
                    /* The LSP Buffer is the first in the chain of SelfLSPs
                     */

                    pNSNLSP->pNonZeroLSP = pTravNonZeroLSP->pNext;
                }
                else
                {
                    /* The LSP Buffer is not the first in the chain of SelfLSPs
                     */

                    pPrevNonZeroLSP->pNext = pTravNonZeroLSP->pNext;
                }

                /* Purge the LSP buffer which has become empty due to the
                 * deletion of the TLV
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Purging Self-LSP Info - Reason [ IPRA Info Deleted - LSP Buffer Empty ]\n"));
                IsisUpdFindAndPurgeSelfLSP (pContext, pTravNonZeroLSP, u1Level);
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pTravNonZeroLSP);
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
            }
            else
            {
                /* Some TLVs are still left in the Buffer from which we have
                 * deleted the given IPRA information. We have to adjust
                 * the length of the Buffer appropriately since the removal of a
                 * TLV will decrease the length of the buffer
                 */

                if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
                {
                    pTravNonZeroLSP->u1NumIPV6RATLV--;
                    u1RemBytes =
                        (UINT1) ISIS_GET_REM_BYTES (pTravNonZeroLSP->
                                                    u1NumIPV6RATLV,
                                                    ISIS_IP6RA_TLV_LEN);
                    if (u1RemBytes < ISIS_IP6RA_TLV_LEN)
                    {
                        pTravNonZeroLSP->u2LSPLen =
                            (UINT2) (pTravNonZeroLSP->u2LSPLen - 2);
                    }
                }
                else
                {
                    pTravNonZeroLSP->u1NumIPRATLV--;
                    u1RemBytes =
                        (UINT1) ISIS_GET_REM_BYTES (pTravNonZeroLSP->
                                                    u1NumIPRATLV,
                                                    ISIS_IPRA_TLV_LEN);
                    if (u1RemBytes < ISIS_IPRA_TLV_LEN)
                    {
                        pTravNonZeroLSP->u2LSPLen =
                            (UINT2) (pTravNonZeroLSP->u2LSPLen - 2);
                    }
                }

                /* LOGIC: Decrement the number of TLVs. We now have to decide
                 * whether to decrement the buffer length by
                 * "ISIS_IPRA_TLV_LEN" or "ISIS_IPRA_TLV_LEN + 2" (2 bytes
                 * for Code and Length).  Now assume you are going to add back 
                 * the IPRA information, that is just now deleted, to the 
                 * SelfLSP buffer. The number of TLVs will be the number 
                 * obtained after decrementing the total number by '1'. If 
                 * 'u1RemBytes' indicates that we have to add a new TLV, then 
                 * we can decrement the length of the buffer by 
                 * "ISIS_IPRA_TLV_LEN + 2", else we must decrement the length 
                 * only by "ISIS_IPRA_TLV_LEN". This is because, if addition 
                 * causes '2' extra bytes to be included, then deletion must 
                 * remove those '2' extra bytes
                 */

                pTravNonZeroLSP->u2LSPLen =
                    (UINT2) (pTravNonZeroLSP->u2LSPLen - pTravTLV->u1Len);
                pTravNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
            }
            break;
        }
        else
        {
            pPrevNonZeroLSP = pTravNonZeroLSP;
            pTravNonZeroLSP = pTravNonZeroLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelIPRATLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdModIPRATLV ()
 * Description : This routine modifies the IPRA TLV with the Changed IPRA Metric
 *               whenever manager configures the IPRA entries
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT which holds the modified IPRA
 *                          information
 *               u1SACheck - ISIS_TRUE/ISIS_FALSE
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful updation of IPRA Info
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdModIPRATLV (tIsisSysContext * pContext, tIsisMDT * pMDT, UINT1 u1SACheck)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4MetricVal = 0;
    UINT1               u1LSPType = 0;
    UINT1               u1SAState = ISIS_SUMM_ADMIN_OFF;
    UINT1               au1TLVMetric[4];
    tIsisIPRATLV       *pTravTLV = NULL;
    tIsisIPRATLV        IPRATravTLV;
    tIsisSAEntry       *pSARec = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdModIPRATLV ()\n"));

    MEMSET (&IPRATravTLV, 0, sizeof (tIsisIPRATLV));

    ISIS_FORM_IPV4_ADDR (pMDT->IPAddr.au1IpAddr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP && pContext->SelfLSP.pL1NSNLSP)
    {
        pTravNonZeroLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
        u1SAState = ISIS_SUMM_ADMIN_L1;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP && pContext->SelfLSP.pL2NSNLSP)
    {
        pTravNonZeroLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
        u1SAState = ISIS_SUMM_ADMIN_L2;
    }

    if (u1SACheck == ISIS_TRUE)
    {
        pSARec = pContext->SummAddrTable.pSAEntry;
    }
    if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
    {
        if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Updating IPv6 IPRA [ %s ] In Self-LSP - LSP Type [ %s ], Metric [ %u ]\n",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pMDT->IPAddr.au1IpAddr)),
                     ISIS_GET_LSP_STR (u1LSPType), pMDT->u4FullMetric));
        }
        else
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Updating IPv6 IPRA [ %s ] In Self-LSP - LSP Type [ %s ], Metric [ %u ]\n",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pMDT->IPAddr.au1IpAddr)),
                     ISIS_GET_LSP_STR (u1LSPType), pMDT->Metric[0]));
        }
    }
    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Updating IPv4 IPRA [ %s ] In Self-LSP - LSP Type [ %s ], Metric [ %u ]\n",
                 au1IPv4Addr, ISIS_GET_LSP_STR (u1LSPType),
                 pMDT->u4FullMetric));
    }
    else
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Updating IPv4 IPRA [ %s ]In Self-LSP - LSP Type [ %s ], Metric [ %u ]\n",
                 au1IPv4Addr, ISIS_GET_LSP_STR (u1LSPType), pMDT->Metric[0]));
    }
    while (pSARec != NULL)
    {
        /* Comparing the given IPRA information with the Summary Address
         * entries. If the given IPRA matches a Summary Address entry, then we
         * cannot modify the metric in the LSP buffer since the buffer would now
         * hold the Summary Address and not the IPRA.
         */

        if (((pSARec->u1AdminState == u1SAState)
             || (pSARec->u1AdminState == ISIS_SUMM_ADMIN_L12))
            && ((IsisUtlCompIPAddr (pSARec->au1SummAddr,
                                    pSARec->u1PrefixLen,
                                    pMDT->IPAddr.au1IpAddr,
                                    pSARec->u1PrefixLen)) == ISIS_TRUE))
        {
            return ISIS_SUCCESS;
        }
        pSARec = pSARec->pNext;
    }

    /* Construct the metric in order as specified in ISO 10589
     */

    if (pMDT->u1TLVType != ISIS_IPV6_RA_TLV)
    {
        IsisUtlFormMetric (pContext, pMDT->Metric, au1TLVMetric);
    }
    /* Go through all the LSP buffers and find for a matching entry and 
     * delete the same
     */
    IPRATravTLV.u1Code = pMDT->u1TLVType;
    MEMCPY (IPRATravTLV.IPAddr.au1IpAddr,
            pMDT->IPAddr.au1IpAddr,
            (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
    IPRATravTLV.IPAddr.u1PrefixLen = pMDT->IPAddr.u1PrefixLen;

    while (pTravNonZeroLSP != NULL)
    {
        pTravTLV = (tIsisIPRATLV *)
            RBTreeGet (pTravNonZeroLSP->IPRATLV, (tRBElem *) & IPRATravTLV);

        if ((pTravTLV != NULL)
            && ((pMDT->u1SrcProtoId == pTravTLV->u1Protocol) ||
                (pMDT->u1SrcProtoId == 0))
            && (pMDT->u1IsisEnabled != ISIS_TRUE))
        {
            /* IPRA TLV mathes the given information. Updating the
             * Metric for the matched TLVnnnn
             */
            if ((pMDT->u1L1toL2Flag == ISIS_TRUE)
                && (pTravTLV->u1L1toL2Flag != ISIS_TRUE))
            {
                return ISIS_SUCCESS;
            }

            ISIS_DBG_PRINT_ADDR (pMDT->IPAddr.au1IpAddr,
                                 pMDT->IPAddr.u1Length,
                                 "UPD <T> : Updating IPRA\t",
                                 ISIS_OCTET_STRING);

            if (pMDT->u1TLVType == ISIS_IPV6_RA_TLV)
            {
                if (pContext->u1MetricStyle != ISIS_STYLE_WIDE_METRIC)
                {
                    COPY_U4_FROM_BUF (u4MetricVal, pMDT->Metric);
                }
                else
                {
                    u4MetricVal = pMDT->u4FullMetric;
                    u4MetricVal = OSIX_HTONL (u4MetricVal);
                }
                MEMCPY (pTravTLV->au1Value, &u4MetricVal, sizeof (tIsisMetric));
            }
            else
            {
                MEMCPY (pTravTLV->au1Value, au1TLVMetric,
                        sizeof (au1TLVMetric));
            }
            pTravNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;

            return ISIS_SUCCESS;
        }

        pTravNonZeroLSP = pTravNonZeroLSP->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdModIPRATLV ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisUpdCompIPAddrWithTLV ()
 * Description : This routine compares the given IP addresses after masking both
 *               the addresses upto prefix length, i.e. addresses are compared
 *               upto the first 'u1PrefixLen' bytes
 * Input (s)   : pu1TLVIPAddr - Pointer to IP Address in TLV
 *               pu1IPAddr    - Pointer to given IP Address
 *               u1PrefixLen  - Prefix Length
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_TRUE, If the IP Addresses Match
 *               ISIS_FALSE, Otherwise
 ******************************************************************************/

PUBLIC UINT1
IsisUpdCompIPAddrWithTLV (UINT1 *pu1TLVIPAddr, UINT1 *pu1IPAddr,
                          UINT1 u1AddrLen, UINT1 u1PrefixLen)
{
    UINT1               u1Byte = 0;
    UINT1               u1Found = ISIS_TRUE;
    UINT1               u1IPAddrByte = 0;
    UINT1               au1Mask[ISIS_MAX_IP_ADDR_LEN];
    UINT1               u1MaxByte = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdCompIPAddrWithTLV ()\n"));

    /* Calculate the mask based on the prefix length
     */

    IsisUtlGetIPMask (u1PrefixLen, au1Mask);

    ISIS_DBG_PRINT_ADDR (pu1IPAddr, u1AddrLen,
                         "UPD <T> : IP Address\t", ISIS_OCTET_STRING);
    ISIS_DBG_PRINT_ADDR (au1Mask, u1AddrLen,
                         "UPD <T> : Calculated Mask\t", ISIS_OCTET_STRING);
    if (u1AddrLen == ISIS_MAX_IPV4_ADDR_LEN)
    {
        for (u1Byte = 0; u1Byte < ISIS_MAX_IPV4_ADDR_LEN; u1Byte++)
        {
            if (u1Byte < ISIS_MAX_IP_ADDR_LEN)
            {

                u1IPAddrByte = (UINT1) (pu1TLVIPAddr[u1Byte] &
                                        pu1TLVIPAddr[u1Byte +
                                                     ISIS_MAX_IPV4_ADDR_LEN]);
                if (u1IPAddrByte != (pu1IPAddr[u1Byte] & au1Mask[u1Byte]))
                {
                    u1Found = ISIS_FALSE;
                    break;
                }
            }
        }
    }
    else
    {
        u1MaxByte = (UINT1) ISIS_ROUNDOFF_PREFIX_LEN (u1PrefixLen);
        for (u1Byte = 0; u1Byte < u1MaxByte; u1Byte++)
        {
            if (u1Byte < ISIS_MAX_IP_ADDR_LEN)
            {
                u1IPAddrByte = (UINT1) (pu1TLVIPAddr[u1Byte]);
                if (u1IPAddrByte != (pu1IPAddr[u1Byte] & au1Mask[u1Byte]))
                {
                    u1Found = ISIS_FALSE;
                    break;
                }
            }
        }

    }
    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdCompIPAddrWithTLV ()\n"));
    return (u1Found);
}

/******************************************************************************
 * Function    : IsisUpdModISAdjTLV ()
 * Description : This Routine Modifies IS Adjacency TLV whenever there is any
 *               change in Adjacency Metric 
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to Multi Data Type structure, that 
 *                          contains the modified Metric 
 *               u4CktIdx - Circuit over which the Adjacency has changed
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Updation of IS Adj TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisUpdModISAdjTLV (tIsisSysContext * pContext, tIsisMDT * pMDT, UINT4 u4CktIdx)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1LSPType = 0;
    UINT1               au1TLVMetric[4];
    UINT1               au1AdjSysID[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisTLV           *pTravTLV = NULL;
    CHAR                acNbrSysId[(3 * ISIS_SYS_ID_LEN) + 1];

    MEMSET (acNbrSysId, 0, ((3 * ISIS_SYS_ID_LEN) + 1));

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdModISAdjTLV ()\n"));

    ISIS_FORM_STR (ISIS_SYS_ID_LEN, pMDT->au1AdjSysID, acNbrSysId);

    u1LSPType = pMDT->u1LSPType;

    if ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP) &&
        (pContext->SelfLSP.pL1NSNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
    }
    else if ((u1LSPType == ISIS_L2_NON_PSEUDO_LSP) &&
             (pContext->SelfLSP.pL2NSNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
    }
    else if ((u1LSPType == ISIS_L1_PSEUDO_LSP) &&
             (pContext->SelfLSP.pL1SNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL1SNLSP->pNonZeroLSP;
    }
    else if ((u1LSPType == ISIS_L2_PSEUDO_LSP) &&
             (pContext->SelfLSP.pL2SNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL2SNLSP->pNonZeroLSP;
    }

    while (pNonZeroLSP != NULL)
    {
        pTravTLV = pNonZeroLSP->pTLV;

        IsisUtlFormMetric (pContext, pMDT->Metric, au1TLVMetric);

        MEMCPY (au1AdjSysID, pMDT->au1AdjSysID,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

        while (pTravTLV != NULL)
        {
            /* Finding the Appropriate TLV
             */

            if (pTravTLV->u1Code == pMDT->u1TLVType)
            {
                /* Since the same peer can be adjacent to the local system over
                 * multiple circuits, we have to match the adjacency information
                 * with both SysId and Circuit Index
                 */
                if ((pTravTLV->u4CktIdx == u4CktIdx)
                    && ((MEMCMP (pTravTLV->au1Value + 4, au1AdjSysID,
                                 ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0))
                {
                    /* Only Metric will change. Copy that
                     */

                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Updating IS Adjacency In Self-LSP - Adjacent System ID [ %s ], LSP Type [ %s ], Metric [ %u ]\n",
                             acNbrSysId, ISIS_GET_LSP_STR (u1LSPType),
                             au1TLVMetric[0]));

                    MEMCPY (pTravTLV->au1Value, au1TLVMetric,
                            sizeof (au1TLVMetric));
                    pNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
                    i4RetVal = ISIS_SUCCESS;
                    break;
                }
                else
                {

                    pTravTLV = pTravTLV->pNext;
                }
            }
            else
            {
                pTravTLV = pTravTLV->pNext;
            }
        }
        pNonZeroLSP = pNonZeroLSP->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdModISAdjTLV ()\n"));
    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisUpdDelTlvs ()
 * Description : This routine frees all the TLVs in the given LSP Information
 * Input(s)    : pLSPInfo - Pointer to the LSP Information
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisUpdDelTlvs (tIsisLSPInfo * pLSPInfo)
{
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pNextTLV = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelTlvs () \n"));

    if (pLSPInfo != NULL)
    {
        /* Scan through the TLV chain and free each of the TLV
         */

        pTLV = pLSPInfo->pTLV;
        while (pTLV != NULL)
        {
            pNextTLV = pTLV->pNext;
            pTLV->pNext = NULL;
            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTLV);
            pTLV = pNextTLV;
        }

        if (pLSPInfo->IPRATLV != NULL)
        {
            RBTreeDestroy (pLSPInfo->IPRATLV,
                           (tRBKeyFreeFn) IsisUtilRBTreeIpraEntryFree, 0);
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelTlvs () \n"));
}

/******************************************************************************
 * Function    : IsisUpdAddMTTLV ()
 * Description : This Routine adds MT TLV to SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the MT TLV info
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of MT TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

INT4
IsisUpdAddMTTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo      **pZeroLSP = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4LSPBufSize = 0;
    UINT2               u2MTId = 0;
    UINT1               u1RemBytes = 0;
    UINT1               u1LSPType = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddMTTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    /* MT TLV will be added only to the Zero LSP since they are not
     * significant in any other LSP. MT TLVs must not be present in
     * Pseudonode LSPs
     */

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddMTTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pZeroLSP = &(pNSNLSP->pZeroLSP);
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddMTTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pZeroLSP = &(pNSNLSP->pZeroLSP);
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddMTTLV ()\n"));
        return ISIS_FAILURE;
    }

    /* Scan through the entire TLV list and check whether the same TLV already exists */

    if (*pZeroLSP != NULL)
    {
        pTravTlv = (*pZeroLSP)->pTLV;

        while (pTravTlv != NULL)
        {
            if (pTravTlv->u1Code == pMDT->u1TLVType)
            {
                if (pTravTlv->u1Len == ISIS_MT_ID_LEN)
                {
                    u2MTId = OSIX_HTONS (pMDT->u2MTId);
                    if ((MEMCMP (pTravTlv->au1Value, &u2MTId, ISIS_MT_ID_LEN) ==
                         0))
                    {
                        return ISIS_SUCCESS;
                    }
                }
            }

            pTravTlv = pTravTlv->pNext;
        }
    }

    /* Allocate memory and add the TLV in the list */
    if (*pZeroLSP == NULL)
    {
        *pZeroLSP = (tIsisLSPInfo *) ISIS_MEM_ALLOC (ISIS_BUF_SLSP,
                                                     sizeof (tIsisLSPInfo));
        if ((*pZeroLSP) == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Zero LSP\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            /* Shutdown the Instance
             */
            return ISIS_FAILURE;
        }
        (*pZeroLSP)->u2AuthStatus = ISIS_FALSE;
        (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
    }
    /* Refer to LOGIC of Self-LSP Updation explained @ the beginnig of this file
     */

    u1RemBytes = (UINT1) ISIS_GET_REM_BYTES (((*pZeroLSP)->u1NumMTTLV),
                                             ISIS_MT_ID_LEN);

    if (u1RemBytes < ISIS_MT_ID_LEN)
    {
        if (((*pZeroLSP)->u2LSPLen + (ISIS_MT_ID_LEN + 2)) >
            (UINT2) u4LSPBufSize)
        {
            /* We cannot have more than one Zero LSP
             */

            WARNING ((ISIS_LGST,
                      "UPD <W> : Zero LSP Exceeding Configured Buffer Size [%u]\n",
                      u4LSPBufSize));
            return ISIS_FAILURE;
        }
        (*pZeroLSP)->u2LSPLen = (UINT2) ((*pZeroLSP)->u2LSPLen + 2);    /* '2' for Code and Length */
    }

    if ((*pZeroLSP)->u1NumMTTLV == 0)
    {
        (*pZeroLSP)->u2LSPLen = (UINT2) ((*pZeroLSP)->u2LSPLen + 2);
    }

    (*pZeroLSP)->u2LSPLen = (UINT2) ((*pZeroLSP)->u2LSPLen + ISIS_MT_ID_LEN);

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisTLV));

    if (pTLV != NULL)
    {
        (*pZeroLSP)->u1NumMTTLV++;
        (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;

        pTLV->u1Code = ISIS_MT_TLV;
        pTLV->u1Len = ISIS_MT_ID_LEN;

        u2MTId = OSIX_HTONS (pMDT->u2MTId);
        MEMCPY (pTLV->au1Value, &u2MTId, ISIS_MT_ID_LEN);

        /* Keep TLVs with the same code together
         */

        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Adding MT TLV To Self-LSP [ %u ], LSP Type [ %s ]\n",
                 pMDT->u2MTId, ISIS_GET_LSP_STR (u1LSPType)));

        if ((*pZeroLSP)->pTLV != NULL)
        {
            pTravTlv = (*pZeroLSP)->pTLV;

            while (pTravTlv != NULL)
            {
                if (pTravTlv->u1Code == pTLV->u1Code)
                {
                    pTLV->pNext = pTravTlv->pNext;
                    pTravTlv->pNext = pTLV;
                    break;
                }
                else
                {
                    pTravTlv = pTravTlv->pNext;
                }
            }

            if (pTravTlv == NULL)
            {
                /* No TLV matches the new TLV's code. Insert the record at the
                 * Head
                 */

                pTLV->pNext = (*pZeroLSP)->pTLV;
                (*pZeroLSP)->pTLV = pTLV;
            }
        }
        else
        {
            (*pZeroLSP)->pTLV = pTLV;
        }
    }
    else
    {
        /* Ignore the new TLV, but generate a PANIC since SelfLSPs are no more
         * correct
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MT TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);
        i4RetVal = ISIS_FAILURE;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddMTTLV ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdAddExtISReachTLV ()
 * Description : This Routine adds Extended IS Reachability TLV to SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the Extended IS 
 *                          Reachability information
 *               u4CktIdx  - Circuit Index
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of Extended IS Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/
INT4
IsisUpdAddExtISReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                         UINT4 u4CktIdx)
{
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pTempNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    tIsisTLV           *pTempTlv = NULL;
    UINT1              *pu1LSPNum = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4LSPBufSize = 0;
    UINT4               u4Count = 0;
    UINT4               u4FullMetric = 0;
    UINT1               u1LSPType = 0;
    tIsisCktEntry      *pCktRec = NULL;
    CHAR                acTlvValue[ISIS_TLV_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddExtISReachTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddExtISReachTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pu1LSPNum = &(pNSNLSP->u1CurrLSPNum);
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_LSPID_LEN + 11) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddExtISReachTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pu1LSPNum = &(pNSNLSP->u1CurrLSPNum);
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL1SNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;

        /* Pseudonode LSPs are maintained per circuit. Go through the entire
         * list and get the appropriate Pseudonode LSP where the current
         * adjacency is to be added
         */

        while ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL)
               && (pSNLSP->pCktEntry->u4CktIdx != u4CktIdx))
        {
            pSNLSP = pSNLSP->pNext;
        }
        if (pSNLSP == NULL)
        {
            /* There may be cases where DIS status is marked as TRUE
             * for the Local System by the Adjacency module, but the Update
             * module has not processed the DIS Status change event. The Update
             * Module while processing the DIS Status change event will
             * construct pseudonode LSPs appropriately. Hence we need not worry
             * about the current adjacency at this point.
             */

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
            return (ISIS_SUCCESS);
        }
        pNonZeroLSP = pSNLSP->pNonZeroLSP;
        pu1LSPNum = &(pSNLSP->u1CurrLSPNum);
    }
    else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
    {
        pSNLSP = pContext->SelfLSP.pL2SNLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;

        /* Pseudonode LSPs are maintained per circuit. Go through the entire
         * list and get the appropriate Pseudonode LSP where the current
         * adjacency is to be added
         */

        while ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL)
               && (pSNLSP->pCktEntry->u4CktIdx != u4CktIdx))
        {
            pSNLSP = pSNLSP->pNext;
        }
        if (pSNLSP == NULL)
        {
            /* There may be cases where DIS status is marked as TRUE
             * for the Local System by the Adjacency module, but the Update
             * module has not processed the DIS Status change event. The Update
             * Module while processing the DIS Status change event will
             * construct pseudonode LSPs appropriately. Hence we need not worry
             * about the current adjacency at this point.
             */

            UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
            return (ISIS_SUCCESS);
        }
        pNonZeroLSP = pSNLSP->pNonZeroLSP;
        pu1LSPNum = &(pSNLSP->u1CurrLSPNum);
    }

    /* Check whether the entry exists already
     * to avoid addition of duplicate entries */
    pTravNonZeroLSP = pNonZeroLSP;
    while (pTravNonZeroLSP != NULL)
    {
        pTravTlv = pTravNonZeroLSP->pTLV;
        while (pTravTlv != NULL)
        {
            if (MEMCMP (pTravTlv->au1Value, pMDT->au1AdjSysID,
                        (ISIS_SYS_ID_LEN + 1)) == 0)
            {
                i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

                if ((i4RetVal == ISIS_FAILURE) || (pCktRec == NULL))
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <T> : Circuit Does Not Exist - Index [ %u ]\n",
                             u4CktIdx));
                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                    return ISIS_FAILURE;
                }
                /* In case of P2P circuits there is no Pseudo Node Id , So an additional
                 * check for u4CktIdx is needed to ensure no duplicate entries are added
                 */

                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    if (pTravTlv->u4CktIdx == u4CktIdx)
                    {
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                        return (ISIS_SUCCESS);
                    }
                }
                else
                {
                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                    return (ISIS_SUCCESS);
                }
            }
            pTravTlv = pTravTlv->pNext;
        }
        pTravNonZeroLSP = pTravNonZeroLSP->pNext;
    }

    /* Try to get a SelfLSP buffer which has got enough room to add the new Extended IS
     * Reachability information. If not allocate a new buffer and initialise it 
     * and add the new information to that
     */

    i4RetVal = IsisUpdGetSelfLSPBuffer (pContext, &pNonZeroLSP,
                                        ISIS_EXT_IS_REACH_TLV, u4LSPBufSize,
                                        u1LSPType, pu1LSPNum);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* No LSP Buffers available to hold the new TLV information
         */

        WARNING ((ISIS_LGST,
                  "UPD <W> : [No LSP Buffers Available] Unable To Add Extended IS Reachability To Self-LSP - Circuit Index [ %u ]\n",
                  u4CktIdx));
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddExtISReachTLV ()\n"));
        return ISIS_FAILURE;
    }
    if ((u1LSPType == ISIS_L2_PSEUDO_LSP) || (u1LSPType == ISIS_L1_PSEUDO_LSP))
    {
        if ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL))
        {
            pNonZeroLSP->u1SNId = pSNLSP->pCktEntry->u1CktLocalID;
        }
        else
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Cannot de-reference"
                     "Null pointer\n"));
        }

    }

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisTLV));

    if (pTLV != NULL)
    {
        pTLV->u1Code = ISIS_EXT_IS_REACH_TLV;

        /* Since the same peer can be adjacent to the local system over two
         * different circuits, we may have to maintain the circuit information
         * along with the Adjacency
         */

        pTLV->u4CktIdx = u4CktIdx;
        pTLV->u1Len = ISIS_EXT_IS_REACH_TLV_LEN;

        MEMCPY (pTLV->au1Value, pMDT->au1AdjSysID,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);
        u4FullMetric = (pMDT->u4FullMetric << 8);
        u4FullMetric = OSIX_HTONL (u4FullMetric);
        MEMCPY (&(pTLV->au1Value[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN]),
                &u4FullMetric, ISIS_IS_DEF_METRIC_LEN);
        pNonZeroLSP->u1NumExtISReachTLV++;

        if (pNonZeroLSP->pTLV != NULL)
        {
            pTravTlv = pNonZeroLSP->pTLV;
            pTempTlv = pNonZeroLSP->pTLV;

            /* If the same EXT IS Reachability is already added, dont add again */
            while (pTempTlv != NULL)
            {
                if ((pTLV->u1Code == pTempTlv->u1Code) &&
                    (MEMCMP (pTempTlv->au1Value, pTLV->au1Value, pTLV->u1Len) ==
                     0))
                {
                    if ((i4RetVal == ISIS_FAILURE) || (pCktRec == NULL))
                    {
                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Circuit Does Not Exist - Index [ %u ]\n",
                                 u4CktIdx));
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                        return ISIS_FAILURE;
                    }
                    if (pCktRec->u1CktType == ISIS_P2P_CKT)
                    {
                        if (pTempTlv->u4CktIdx == u4CktIdx)
                        {
                            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTLV);
                            pNonZeroLSP->u2LSPLen =
                                (UINT2) (pNonZeroLSP->u2LSPLen - pTLV->u1Len);
                            pNonZeroLSP->u1NumExtISReachTLV--;

                            return (ISIS_SUCCESS);
                        }
                    }
                    else if (pCktRec->u1CktType == ISIS_BC_CKT)
                    {
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTLV);
                        pNonZeroLSP->u2LSPLen =
                            (UINT2) (pNonZeroLSP->u2LSPLen - pTLV->u1Len);
                        pNonZeroLSP->u1NumExtISReachTLV--;
                        return ISIS_SUCCESS;
                    }
                }

                pTempTlv = pTempTlv->pNext;
            }

            /* Go through the entire TLV chain and verify whether TLVs with the
             * same code already exist. If they exist add the new TLV adjacent
             * to the existing ones. Otherwise add the new TLV at the head
             */

            ISIS_FORM_STR (ISIS_SYS_ID_LEN + 1, pTLV->au1Value, acTlvValue);
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adding Extended IS Reachability [%s] To Self-LSP - Circuit Index [ %u ]"
                     " LSP Type [ %s ]\n",
                     acTlvValue, u4CktIdx, ISIS_GET_LSP_STR (u1LSPType)));

            while (pTravTlv != NULL)
            {
                if (pTravTlv->u1Code == pTLV->u1Code)
                {
                    pTLV->pNext = pTravTlv->pNext;
                    pTravTlv->pNext = pTLV;
                    break;
                }
                else
                {
                    pTravTlv = pTravTlv->pNext;
                }
            }

            if (pTravTlv == NULL)
            {
                /* No TLV matches the new TLV's code. Insert the record at the
                 * Head
                 */

                pTLV->pNext = pNonZeroLSP->pTLV;
                pNonZeroLSP->pTLV = pTLV;
                ISIS_FORM_STR (ISIS_SYS_ID_LEN, pTLV->au1Value, acTlvValue);
                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Adding Extended IS Reachability [%s] To Self-LSP - Circuit Index [ %u ]\n",
                         acTlvValue, u4CktIdx));
            }
        }
        else
        {
            pNonZeroLSP->pTLV = pTLV;
        }
    }
    else
    {
        /* Ignore the new TLV that is to be added, nevertheless generate a PANIC
         * since Self-LSPs are no more correct
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Ext IS Reach TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);

        RBTreeCount (pNonZeroLSP->IPRATLV, &u4Count);
        if ((pNonZeroLSP->pTLV == NULL) && (u4Count == 0))
        {
            /* This is a new LSP buffer allocated since the existing buffers did
             * not have enough space to hold the new Extended IS Reachability information 
             * (only in this case pNonZeroLSP->pTLV will be NULL). Since we are
             * unable to allocate memory for the TLV, we can release the LSP 
             * buffer also
             */
            if (pTempNonZeroLSP == pNonZeroLSP)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL1SNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL2SNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
            }
            else
            {
                while (pTempNonZeroLSP != NULL)
                {
                    if (pTempNonZeroLSP->pNext == pNonZeroLSP)
                    {
                        pTempNonZeroLSP->pNext = pNonZeroLSP->pNext;
                        break;
                    }
                    pTempNonZeroLSP = pTempNonZeroLSP->pNext;
                }
            }
            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pNonZeroLSP);
        }

        i4RetVal = ISIS_FAILURE;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddExtISReachTLV () \n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdAddExtMtIPReachTLV ()
 * Description : This Routine adds Extended IP Reachability TLV to SelfLSP
 * Input (s)   : pContext     - Pointer to System Context
 *               pMDT         - Pointer to MDT structure contains the Extended IP
 *                              Reachability information
 *               u1SAChk      - Flag indicating whether a matching Summary 
 *                              Address is to be added to Self LSP. In case of
 *                              summary addresses themselves being added viz
 *                              this routine or in the case of SPT PATH Nodes
 *                              being added, this flag will be set to FALSE 
 *               u1SAIncrFlag - Flag Indicating whether the SA usage count 
 *                              is to be updated. The flag will be set to FALSE
 *                              in case of additions being made for Summary
 *                              Addresses to PATH nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of Extended IP Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/
INT4
IsisUpdAddExtMtIPReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                           UINT1 u1SAChk, UINT1 u1SAIncrFlag)
{
    tIsisIPRATLV       *pTLV = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisSAEntry       *pSARec = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisLSPInfo       *pTempNonZeroLSP = NULL;
    tIsisIPRATLV        IPRATravTLV;
    UINT4               u4DefMetric = 0;
    UINT4               u4LSPBufSize = 0;
    UINT4               u4Count = 0;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2MtId = 0;
    UINT1               u1SAState = ISIS_SUMM_ADMIN_OFF;
    UINT1               u1LSPType = 0;
    UINT1               u1AddrLen = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddExtMtIPReachTLV ()\n"));
    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (&IPRATravTLV, 0, sizeof (tIsisIPRATLV));
    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddExtMtIPReachTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
        u1SAState = ISIS_SUMM_ADMIN_L1;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddExtMtIPReachTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
        u1SAState = ISIS_SUMM_ADMIN_L2;
    }
    else
    {
        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting IsisUpdAddExtMtIPReachTLV ()\n"));
        return ISIS_FAILURE;
    }

    /* NOTE: 
     *
     * 1. u1SAChk = FALSE implies no summarisation and hence do not worry about
     *    u1SAIncrFlag. This is the case when Summary Address entries themselves
     *    are being added
     *    
     * 2. u1SAChk = TRUE implies summarisation is required. This means that the
     *    information being added is IPRA information. Then
     *
     *    2a. u1SAIncrFlag = TRUE implies that the usage count is to be
     *        incremented since the entry being added is IPRA information
     *        configured by the manager
     *    2b. u1SAIncrFlag = FALSE implies that the usage count need not be
     *        incremented since the entry being added is either IPRA information
     *        retrieved from the PATH nodes or it is Summary Address Information
     */

    if (u1SAChk == ISIS_TRUE)
    {
        /* This routine is invoked to add even the Summary Addresses from the
         * Summary Addrress Table as TLVs in Self-LSPs apart from adding IPRAs.
         * In the former case we need not check the Summary Address Table while
         * in the later case, each IPRA will be checked against the configured
         * Summary Addresses. If a matching entry is found the IPRA will not be
         * added and instead the Summary Address entry is added as a TLV.
         * Otherwise the IPRA entry is added to the Self-LSPs. The 'u1SAChk'
         * flag will be FLASE while Summary Address entries are being added
         */

        pSARec = pContext->SummAddrTable.pSAEntry;
    }

    while (pSARec != NULL)
    {
        /* Try to summarize the given IPRA entry
         */

        if (((pSARec->u1AdminState == u1SAState)
             || (pSARec->u1AdminState == ISIS_SUMM_ADMIN_L12))
            && ((IsisUtlCompIPAddr (pSARec->au1SummAddr,
                                    pSARec->u1PrefixLen,
                                    pMDT->IPAddr.au1IpAddr,
                                    pSARec->u1PrefixLen)) == ISIS_TRUE))
        {

            /* Apart from Summary Addresses and configured IPRA information, 
             * this function is also invoked for adding IPRA information
             * retrieved from the PATH nodes. In case this function is invoked
             * while adding PATH information, the information must be summarised
             * but the usage count need not be incremented. 'u1SAIncrFlag'
             * specifies whether usage count is to be incremented
             */

            if (u1SAIncrFlag == ISIS_TRUE)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pSARec->u2L1UsageCnt++;
                }
                else
                {
                    pSARec->u2L2UsageCnt++;
                }
            }

            if (((pSARec->u2L1UsageCnt == 1)
                 && (u1LSPType == ISIS_L1_NON_PSEUDO_LSP))
                || ((pSARec->u2L2UsageCnt == 1)
                    && (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)))
            {
                /* A matching summary address information record is found. The
                 * usage count is also '1' and hence we can add the Summary
                 * Address information instead of the IPRA information.
                 * Overwrite the MDT record with the Summary Address Information
                 */
                ISIS_SET_METRIC (pContext, pMDT, pSARec);

                if (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV)
                {
                    u1AddrLen = ISIS_MAX_IPV4_ADDR_LEN;
                }
                else
                {
                    u1AddrLen = ISIS_MAX_IPV6_ADDR_LEN;
                }
                MEMCPY (pMDT->IPAddr.au1IpAddr, pSARec->au1SummAddr, u1AddrLen);
                pMDT->IPAddr.u1PrefixLen = pSARec->u1PrefixLen;
            }
            else
            {
                /* Received IPRA information matched an existing Summary Address
                 * information and the usage count of the Summary Address entry 
                 * is greater than '1' which means that the address information
                 * has already been added. So the new information need not be
                 * added to Self-LSPs
                 */
                if (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV)
                {
                    ISIS_FORM_IPV4_ADDR (pMDT->IPAddr.au1IpAddr, au1IPv4Addr,
                                         ISIS_MAX_IPV4_ADDR_LEN);
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Ext IPRA Info [%s] Already Exists In Self-LSP"
                             ", LSP Type [ %s ]\n", au1IPv4Addr,
                             ISIS_GET_LSP_STR (u1LSPType)));
                }
                else
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : MT IPv6 RA Info [%s] Already Exists In Self-LSP"
                             ", LSP Type [ %s ]\n",
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pMDT->IPAddr.au1IpAddr)),
                             ISIS_GET_LSP_STR (u1LSPType)));
                }
                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdAddExtMtIPReachTLV ()\n"));
                return ISIS_SUCCESS;
            }
            break;
        }
        pSARec = pSARec->pNext;
    }

    pTravNonZeroLSP = pNonZeroLSP;

    IPRATravTLV.u1Code = pMDT->u1TLVType;
    MEMCPY (IPRATravTLV.IPAddr.au1IpAddr,
            pMDT->IPAddr.au1IpAddr,
            (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
    IPRATravTLV.IPAddr.u1PrefixLen = pMDT->IPAddr.u1PrefixLen;

    while (pTravNonZeroLSP != NULL)
    {
        /* SCan the IPRA TLV RBTree to get the matching TLV */

        pTLV = (tIsisIPRATLV *)
            RBTreeGet (pTravNonZeroLSP->IPRATLV, (tRBElem *) & IPRATravTLV);

        if (pTLV != NULL)
        {
            if (pMDT->u1SrcProtoId != pTLV->u1Protocol)
            {
                if (pMDT->u1SrcProtoId != 0)
                {
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Protocol Id [%s] updated in the IPRA entry \n",
                             ISIS_GET_RRD_PROTO_STR (pMDT->u1SrcProtoId)));
                    pTLV->u1Protocol = pMDT->u1SrcProtoId;
                }
                if (pMDT->u1IsisEnabled == ISIS_TRUE)
                {
                    /* This flag is set, if the interface is conneced interface and
                     * redistribution is enabled on it. */
                    pTLV->u1IsisEnabled = ISIS_TRUE;
                }

                if (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV)
                {
                    /* The Extended IP Reachabiltiy TLV format is as follows:
                     * 4 bytes of default metric value, 2 bit control field in that 
                     * 1st bit is UP/DiOWN bit, 2nd bit is S Bit(Sub-TLVs)
                     * and next 6 bits is prefix length followed by 0 to 4 bytes of Address prefix
                     */
                    if (pMDT->u1IsisEnabled != ISIS_TRUE)
                    {
                        u4DefMetric = OSIX_HTONL (pMDT->u4FullMetric);
                        MEMCPY (pTLV->au1Value, &u4DefMetric,
                                sizeof (u4DefMetric));
                    }
                    pTLV->au1Value[ISIS_EXT_DEF_METRIC_LEN] = pMDT->u1CtrlOctet;
                    pTLV->au1Value[ISIS_EXT_DEF_METRIC_LEN]
                        |= pMDT->IPAddr.u1PrefixLen;
                    MEMCPY (&(pTLV->au1Value[ISIS_EXT_DEF_METRIC_LEN + 1]),
                            pMDT->IPAddr.au1IpAddr,
                            ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.
                                                      u1PrefixLen));
                }
                else
                {
                    /* The MT IPv6 Reachabiltiy TLV format is as follows:
                     * 2 bytes of MT ID, 4 bytes of default metric value, 3 bits of control field in that 
                     * 1st bit is UP/DOWN bit, 2nd bit is INTERNAL/EXTERNAL bit, 1 bit is S Bit(Sub-TLVs)
                     * and next 5 bits are reserved. 1 byte for prefix length followed by 0 to 16 bytes 
                     * of IPV6 Address prefix
                     */
                    u2MtId = OSIX_HTONS (ISIS_IPV6_UNICAST_MT_ID);
                    MEMCPY (pTLV->au1Value, &u2MtId, ISIS_MT_ID_LEN);
                    if (pMDT->u1IsisEnabled != ISIS_TRUE)
                    {
                        u4DefMetric = OSIX_HTONL (pMDT->u4FullMetric);
                        MEMCPY (&(pTLV->au1Value[ISIS_MT_ID_LEN]),
                                &u4DefMetric, sizeof (u4DefMetric));
                    }
                    pTLV->au1Value[ISIS_MT_ID_LEN + ISIS_EXT_DEF_METRIC_LEN]
                        = pMDT->u1CtrlOctet;
                    pTLV->au1Value[7] = pMDT->IPAddr.u1PrefixLen;
                    MEMCPY (&(pTLV->au1Value[8]), pMDT->IPAddr.au1IpAddr,
                            ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.
                                                      u1PrefixLen));
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : MT IPv6 RA Info [%s] Already Exists In Self-LSP"
                             ", LSP Type [ %s ]\n",
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pMDT->IPAddr.au1IpAddr)),
                             ISIS_GET_LSP_STR (u1LSPType)));
                }
            }
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddExtMtIPReachTLV ()\n"));
            return ISIS_SUCCESS;
        }
        pTravNonZeroLSP = pTravNonZeroLSP->pNext;
    }

    /* Try to get a SelfLSP buffer which has got enough room to add the new Extended
     * IP Reachability information. If not allocate a new buffer and initialise it and add the
     * new IPRA information to that
     */

    i4RetVal = IsisUpdGetSelfLSPBuffer (pContext, &pNonZeroLSP,
                                        pMDT->u1TLVType, u4LSPBufSize,
                                        u1LSPType, &(pNSNLSP->u1CurrLSPNum));

    if (i4RetVal == ISIS_FAILURE)
    {
        /* No LSP Buffers available to hold the new TLV information
         */

        WARNING ((ISIS_LGST,
                  "UPD <W> : [No LSP Buffers Available] Unable To Add Extended IP Reachability To Self-LSP"
                  " - LSP Type [ %s ] \n", ISIS_GET_LSP_STR (u1LSPType)));
        UPP_EE ((ISIS_LGST,
                 "UPD <X> : Exiting IsisUpdAddExtMtIPReachTLV ()\n"));
        return ISIS_FAILURE;
    }

    pTLV =
        (tIsisIPRATLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisIPRATLV));
    if (pTLV != NULL)
    {
        MEMSET (pTLV, 0, sizeof (tIsisIPRATLV));
        pTLV->u1Code = pMDT->u1TLVType;
        pTLV->u1Protocol = pMDT->u1SrcProtoId;
        MEMCPY (pTLV->IPAddr.au1IpAddr,
                pMDT->IPAddr.au1IpAddr,
                ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen));
        pTLV->IPAddr.u1PrefixLen = pMDT->IPAddr.u1PrefixLen;
        if (pMDT->u1IsisEnabled == ISIS_TRUE)
        {
            /* This flag is set, if the interface is conneced interface and
             * redistribution is enabled on it. */
            pTLV->u1IsisEnabled = ISIS_TRUE;
        }
        if (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV)
        {
            /* The Extended IP Reachabiltiy TLV format is as follows:
             * 4 bytes of default metric value, 2 bit control field in that 
             * 1st bit is UP/DiOWN bit, 2nd bit is S Bit(Sub-TLVs)
             * and next 6 bits is prefix length followed by 0 to 4 bytes of Address prefix
             */

            pNonZeroLSP->u1NumExtIpReachTLV++;
            pTLV->u1Len = (UINT1) (ISIS_EXT_DEF_METRIC_LEN + 1 +
                                   (ISIS_ROUNDOFF_PREFIX_LEN
                                    (pMDT->IPAddr.u1PrefixLen)));
            u4DefMetric = (UINT4) (pMDT->u4FullMetric);
            u4DefMetric = OSIX_HTONL (u4DefMetric);
            MEMCPY (pTLV->au1Value, &u4DefMetric, sizeof (u4DefMetric));
            pTLV->au1Value[ISIS_EXT_DEF_METRIC_LEN] = pMDT->u1CtrlOctet;
            pTLV->au1Value[ISIS_EXT_DEF_METRIC_LEN] |= pMDT->IPAddr.u1PrefixLen;
            MEMCPY (&(pTLV->au1Value[ISIS_EXT_DEF_METRIC_LEN + 1]),
                    pMDT->IPAddr.au1IpAddr,
                    ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen));

            pNonZeroLSP->u2LSPLen =
                (UINT2) (pNonZeroLSP->u2LSPLen -
                         (ISIS_MAX_IPV4_ADDR_LEN -
                          ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
            if (ISIS_GET_UPDOWN_TYPE (pMDT->u1CtrlOctet) == ISIS_RL_L1_DOWN)
            {
                pTLV->u1LeakedRoute = ISIS_TRUE;
            }
        }
        else
        {
            /* The MT IPv6 Reachabiltiy TLV format is as follows:
             * 2 bytes of MT ID, 4 bytes of default metric value, 3 bits of control field in that 
             * 1st bit is UP/DOWN bit, 2nd bit is INTERNAL/EXTERNAL bit, 1 bit is S Bit(Sub-TLVs)
             * and next 5 bits are reserved. 1 byte for prefix length followed by 0 to 16 bytes 
             * of IPV6 Address prefix
             */
            pNonZeroLSP->u1NumMTIpv6ReachTLV++;
            pTLV->u1Len =
                (UINT1) (ISIS_MT_ID_LEN + ISIS_EXT_DEF_METRIC_LEN + 1 + 1 +
                         (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));

            u2MtId = OSIX_HTONS (ISIS_IPV6_UNICAST_MT_ID);
            MEMCPY (pTLV->au1Value, &u2MtId, ISIS_MT_ID_LEN);
            u4DefMetric = (UINT4) (pMDT->u4FullMetric);
            u4DefMetric = OSIX_HTONL (u4DefMetric);
            MEMCPY (&(pTLV->au1Value[ISIS_MT_ID_LEN]), &u4DefMetric,
                    sizeof (u4DefMetric));
            pTLV->au1Value[ISIS_MT_ID_LEN + ISIS_EXT_DEF_METRIC_LEN] =
                pMDT->u1CtrlOctet;
            pTLV->au1Value[7] = pMDT->IPAddr.u1PrefixLen;
            MEMCPY (&(pTLV->au1Value[8]), pMDT->IPAddr.au1IpAddr,
                    ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen));

            pNonZeroLSP->u2LSPLen =
                (UINT2) (pNonZeroLSP->u2LSPLen -
                         (ISIS_MAX_IPV6_ADDR_LEN -
                          ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
            if (ISIS_GET_UPDOWN_TYPE (pMDT->u1CtrlOctet) == ISIS_RL_L1_DOWN)
            {
                pTLV->u1LeakedRoute = ISIS_TRUE;
            }
        }
        pTLV->u1L1toL2Flag = pMDT->u1L1toL2Flag;
        /* Add the IPRA TLV to the RBtree in LSP */
        if (RBTreeAdd (pNonZeroLSP->IPRATLV, (tRBElem *) pTLV) != RB_SUCCESS)
        {
            i4RetVal = ISIS_FAILURE;
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddExtMtIPReachTLV ()\n"));
            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTLV);
            return (i4RetVal);
        }
        if (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV)
        {
            ISIS_FORM_IPV4_ADDR (pMDT->IPAddr.au1IpAddr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adding Extended IP Reachability To Self-LSP [%s], LSP Type [ %s ]\n",
                     au1IPv4Addr, ISIS_GET_LSP_STR (u1LSPType)));
        }
        else
        {
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adding MT IPv6 RA Info [%s] To Self-LSP"
                     ", LSP Type [ %s ]\n",
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pMDT->IPAddr.au1IpAddr)),
                     ISIS_GET_LSP_STR (u1LSPType)));
        }
    }

    else
    {
        /* Ignore the new TLV that is to be added, nevertheless generate a PANIC
         * since Self-LSPs are no more correct
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IPReach TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);

        RBTreeCount (pNonZeroLSP->IPRATLV, &u4Count);
        if ((pNonZeroLSP->pTLV == NULL) && (u4Count == 0))
        {
            /* This is a new LSP buffer allocated since the existing buffers did
             * not have enough space to hold the new IP Reachability  information 
             * (only in this case pNonZeroLSP->pTLV will be NULL). Since we are unable to
             * allocate memory for the TLV, we can release the LSP buffer also
             */
            if (pTempNonZeroLSP == pNonZeroLSP)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
            }
            else
            {
                while (pTempNonZeroLSP != NULL)
                {
                    if (pTempNonZeroLSP->pNext == pNonZeroLSP)
                    {
                        pTempNonZeroLSP->pNext = pNonZeroLSP->pNext;
                        break;
                    }
                    pTempNonZeroLSP = pTempNonZeroLSP->pNext;
                }
            }
            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pNonZeroLSP);
        }
        i4RetVal = ISIS_FAILURE;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddExtMtIPReachTLV ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdAddMTISReachTLV ()
 * Description : This Routine adds MT IS Reachability TLV to SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the MT IS
 *                          Reachability information
 *               u4CktIdx  - Circuit Index
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of MT IS Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/
INT4
IsisUpdAddMTISReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                        UINT4 u4CktIdx)
{
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pTempNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    tIsisTLV           *pTempTlv = NULL;
    UINT1              *pu1LSPNum = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4LSPBufSize = 0;
    UINT4               u4Count = 0;
    UINT4               u4WideMetric = 0;
    UINT2               u2MtId = 0;
    UINT1               u1LSPType = 0;
    UINT1               u1Found = ISIS_FALSE;
    tIsisCktEntry      *pCktRec = NULL;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddMTISReachTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

    if (i4RetVal == ISIS_FAILURE)
    {
        ADP_PT ((ISIS_LGST,
                 "ADJ <T> : Circuit Does Not Exist - Index [ %u ]\n",
                 u4CktIdx));
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
        return ISIS_FAILURE;
    }

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddMTISReachTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pu1LSPNum = &(pNSNLSP->u1CurrLSPNum);
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_LSPID_LEN + 11) - ISIS_MAX_PASSWORD_LEN;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddMTISReachTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pu1LSPNum = &(pNSNLSP->u1CurrLSPNum);
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        /* Storing the head ptr temporarily for later reference */
        pTempNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddMTISReachTLV ()\n"));
        return ISIS_FAILURE;
    }

    /* Check whether the entry exists already
     * to avoid addition of duplicate entries */
    pTravNonZeroLSP = pNonZeroLSP;
    while (pTravNonZeroLSP != NULL)
    {
        pTravTlv = pTravNonZeroLSP->pTLV;
        while (pTravTlv != NULL)
        {
            if (MEMCMP (&pTravTlv->au1Value[2], pMDT->au1AdjSysID,
                        (ISIS_SYS_ID_LEN + 1)) == 0)
            {
                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    if (pTravTlv->u4CktIdx == u4CktIdx)
                    {
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                        return (ISIS_SUCCESS);
                    }
                }
                else
                {
                    UPP_EE ((ISIS_LGST,
                             "UPD <X> : Exiting IsisUpdAddMTISReachTLV () \n"));
                    return (ISIS_SUCCESS);
                }
            }
            pTravTlv = pTravTlv->pNext;
        }
        pTravNonZeroLSP = pTravNonZeroLSP->pNext;
    }

    /* Try to get a SelfLSP buffer which has got enough room to add the new MT IS
     * Reachability information. If not allocate a new buffer and initialise it 
     * and add the new information to that
     */

    i4RetVal = IsisUpdGetSelfLSPBuffer (pContext, &pNonZeroLSP,
                                        ISIS_MT_IS_REACH_TLV, u4LSPBufSize,
                                        u1LSPType, pu1LSPNum);

    if (i4RetVal == ISIS_FAILURE)
    {
        /* No LSP Buffers available to hold the new TLV information
         */
        WARNING ((ISIS_LGST,
                  "UPD <W> : [No LSP Buffers Available] Unable To Add MT IS Reachability To SelfLSP -"
                  "  Circuit Index [ %u ], LSP Type [ %s ]\n",
                  u4CktIdx, ISIS_GET_SELF_LSP_TYPE (u1LSPType)));
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddMTISReachTLV ()\n"));
        return ISIS_FAILURE;
    }

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisTLV));

    if (pTLV != NULL)
    {
        pTLV->u1Code = ISIS_MT_IS_REACH_TLV;

        /* Since the same peer can be adjacent to the local system over two
         * different circuits, we may have to maintain the circuit information
         * along with the Adjacency
         */

        pTLV->u4CktIdx = u4CktIdx;
        pTLV->u1Len = ISIS_MT_IS_REACH_TLV_LEN;

        u2MtId = OSIX_HTONS (pMDT->u2MTId);
        MEMCPY (pTLV->au1Value, &u2MtId, ISIS_MT_ID_LEN);
        MEMCPY (&(pTLV->au1Value[2]), pMDT->au1AdjSysID, ISIS_SYS_ID_LEN + 1);
        /* Default metric is a 3 byte field. But since pMDT->Metric[0] which holds the 
         * default metric value is 1 byte, we are skipping 2 bytes and adding in the 3rd byte
         */
        u4WideMetric = (pMDT->u4FullMetric << 8);
        u4WideMetric = OSIX_HTONL (u4WideMetric);
        MEMCPY (&(pTLV->au1Value[9]), &u4WideMetric, ISIS_IS_DEF_METRIC_LEN);
        pNonZeroLSP->u1NumMTISReachTLV++;

        if (pNonZeroLSP->pTLV != NULL)
        {
            pTravTlv = pNonZeroLSP->pTLV;
            pTempTlv = pNonZeroLSP->pTLV;

            /* If the same MT IS Reachability is already added, dont add again */
            while (pTempTlv != NULL)
            {
                if ((pTLV->u1Code == pTempTlv->u1Code) &&
                    (MEMCMP (pTempTlv->au1Value, pTLV->au1Value, pTLV->u1Len) ==
                     0))
                {
                    i4RetVal = IsisAdjGetCktRec (pContext, u4CktIdx, &pCktRec);

                    if (i4RetVal == ISIS_FAILURE)
                    {
                        ADP_PT ((ISIS_LGST,
                                 "ADJ <T> : Circuit Does Not Exist - Index [ %u ]\n",
                                 u4CktIdx));
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTLV);
                        pNonZeroLSP->u2LSPLen =
                            (UINT2) (pNonZeroLSP->u2LSPLen - pTLV->u1Len);
                        pNonZeroLSP->u1NumMTISReachTLV--;
                        return ISIS_FAILURE;
                    }
                    /* In case of P2P circuits there is no Pseudo Node Id , So an additional
                     * check for u4CktIdx is needed to ensure no duplicate entries are added
                     */

                    if (pCktRec->u1CktType == ISIS_P2P_CKT)
                    {
                        if (pTravTlv->u4CktIdx == u4CktIdx)
                        {
                            UPP_EE ((ISIS_LGST,
                                     "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                            u1Found = ISIS_TRUE;
                        }
                    }
                    else
                    {
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddISAdjTLV () \n"));
                        u1Found = ISIS_TRUE;
                    }

                    if (u1Found == ISIS_TRUE)
                    {
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTLV);
                        pNonZeroLSP->u2LSPLen =
                            (UINT2) (pNonZeroLSP->u2LSPLen - pTLV->u1Len);
                        pNonZeroLSP->u1NumMTISReachTLV--;
                        return ISIS_SUCCESS;
                    }
                }

                pTempTlv = pTempTlv->pNext;
            }

            /* Go through the entire TLV chain and verify whether TLVs with the
             * same code already exist. If they exist add the new TLV adjacent
             * to the existing ones. Otherwise add the new TLV at the head
             */
            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adding MT IS Reachability To Self-LSP - Circuit Index [ %u ], LSP Type [ %s ]\n",
                     u4CktIdx, ISIS_GET_SELF_LSP_TYPE (u1LSPType)));
            while (pTravTlv != NULL)
            {
                if (pTravTlv->u1Code == pTLV->u1Code)
                {
                    pTLV->pNext = pTravTlv->pNext;
                    pTravTlv->pNext = pTLV;
                    break;
                }
                else
                {
                    pTravTlv = pTravTlv->pNext;
                }
            }

            if (pTravTlv == NULL)
            {
                /* No TLV matches the new TLV's code. Insert the record at the
                 * Head
                 */

                pTLV->pNext = pNonZeroLSP->pTLV;
                pNonZeroLSP->pTLV = pTLV;
            }
        }
        else
        {
            pNonZeroLSP->pTLV = pTLV;
        }
    }
    else
    {
        /* Ignore the new TLV that is to be added, nevertheless generate a PANIC
         * since Self-LSPs are no more correct
         */

        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MT IS Reach TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);

        RBTreeCount (pNonZeroLSP->IPRATLV, &u4Count);
        if ((pNonZeroLSP->pTLV == NULL) && (u4Count == 0))
        {
            /* This is a new LSP buffer allocated since the existing buffers did
             * not have enough space to hold the new MT IS Reachability information 
             * (only in this case pNonZeroLSP->pTLV will be NULL). Since we are
             * unable to allocate memory for the TLV, we can release the LSP 
             * buffer also
             */
            if (pTempNonZeroLSP == pNonZeroLSP)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
                else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
                {
                    pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP =
                        pNonZeroLSP->pNext;
                }
            }
            else
            {
                while (pTempNonZeroLSP != NULL)
                {
                    if (pTempNonZeroLSP->pNext == pNonZeroLSP)
                    {
                        pTempNonZeroLSP->pNext = pNonZeroLSP->pNext;
                        break;
                    }
                    pTempNonZeroLSP = pTempNonZeroLSP->pNext;
                }
            }
            ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pNonZeroLSP);
        }

        i4RetVal = ISIS_FAILURE;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddMTISReachTLV () \n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function    : IsisUpdDelMTTLV ()
 * Description : This Routine Deleted MT TLV from SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the MT TLV info
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful deletion of MT TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/
INT4
IsisUpdDelMTTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    tIsisTLV           *pTravTLV = NULL;
    tIsisTLV           *pPrevTLV = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT2               u2MTId = 0;
    UINT1               u1RemBytes = 0;
    UINT1               u1LSPType = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelMTTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
        && (pContext->SelfLSP.pL1NSNLSP != NULL))
    {
        pZeroLSP = pContext->SelfLSP.pL1NSNLSP->pZeroLSP;
    }
    else if ((u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
             && (pContext->SelfLSP.pL2NSNLSP != NULL))
    {
        pZeroLSP = pContext->SelfLSP.pL2NSNLSP->pZeroLSP;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelMTTLV ()\n"));
        return ISIS_SUCCESS;
    }

    /* Scan through the entire TLV list and get the matching information record
     */

    pTravTLV = pZeroLSP->pTLV;

    while (pTravTLV != NULL)
    {
        if (pTravTLV->u1Code == pMDT->u1TLVType)
        {
            /* Found a matching type of record. Verify whether the Length and
             * contents are same
             */

            if (pTravTLV->u1Len == ISIS_MT_ID_LEN)
            {
                /* Lenghts are same, Verify the contents
                 */

                u2MTId = OSIX_HTONS (pMDT->u2MTId);
                if ((MEMCMP (pTravTLV->au1Value, &u2MTId, ISIS_MT_ID_LEN) == 0))
                {
                    /* Found an exact match. Remove the TLV from the List of
                     * TLVs from the SelfLSP
                     */
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Deleting MT Info From Self-LSP [ %u ]\n",
                             pMDT->u2MTId));
                    i4RetVal = ISIS_SUCCESS;

                    if (pPrevTLV != NULL)
                    {
                        /* Not a first TLV in the List */

                        pPrevTLV->pNext = pTravTLV->pNext;
                        pTravTLV->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                        break;
                    }
                    else
                    {
                        /* Very first TLV in the List and hence advance the head
                         */

                        pZeroLSP->pTLV = pTravTLV->pNext;
                        pTravTLV->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                        break;
                    }
                }
            }
        }

        pPrevTLV = pTravTLV;
        pTravTLV = pTravTLV->pNext;
    }

    if (i4RetVal == ISIS_SUCCESS)
    {
        pZeroLSP->u1NumMTTLV--;

        u1RemBytes =
            (UINT1) ISIS_GET_REM_BYTES (pZeroLSP->u1NumMTTLV, ISIS_MT_ID_LEN);

        if (u1RemBytes < ISIS_MT_ID_LEN)
        {
            pZeroLSP->u2LSPLen =
                (UINT2) (pZeroLSP->u2LSPLen - (ISIS_MT_ID_LEN + 2));
        }
        else
        {
            pZeroLSP->u2LSPLen = (UINT2) (pZeroLSP->u2LSPLen - ISIS_MT_ID_LEN);
        }
        pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelMTTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdDelExtISReachTLV ()
 * Description : This Routine deletes Extended IS Reachability TLV from SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the Extended IS 
 *                          Reachability information
 *               u4CktIdx  - Circuit Index
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful deletion of Extended IS Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/
INT4
IsisUpdDelExtISReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                         UINT4 u4CktIdx)
{
    tIsisSNLSP         *pSNLSP = NULL;
    tIsisSNLSP         *pPrevSNLSP = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;
    tIsisLSPInfo       *pPrevNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisTLV           *pTravTLV = NULL;
    tIsisTLV           *pPrevTLV = NULL;
    UINT4               u4Count = 0;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1RemBytes = 0;
    UINT1               u1LSPType = 0;
    UINT1               u1Level = 0;
    UINT1               u1PNodeFlag = ISIS_SET;
    UINT1               u1ISAdjFound = ISIS_FALSE;
    CHAR                acTlvValue[ISIS_TLV_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelExtISReachTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        u1PNodeFlag = ISIS_NOT_SET;
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP != NULL)
        {
            pNonZeroLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
        }
        u1Level = ISIS_LEVEL1;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        u1PNodeFlag = ISIS_NOT_SET;
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP != NULL)
        {
            pNonZeroLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
        }
        u1Level = ISIS_LEVEL2;
    }
    else if (u1LSPType == ISIS_L1_PSEUDO_LSP)
    {
        pPrevSNLSP = NULL;
        pSNLSP = pContext->SelfLSP.pL1SNLSP;

        /* Get the appropriate PseudoNode LSP based on the Circuit Index
         *
         * NOTE: Pseudonode LSPs are maintained per circuit basis
         */

        while ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL)
               && (pSNLSP->pCktEntry->u4CktIdx != u4CktIdx))
        {
            pPrevSNLSP = pSNLSP;
            pSNLSP = pSNLSP->pNext;
        }
        if (pSNLSP == NULL)
        {
            /* There may be cases where DIS status is marked as TRUE
             * for the Local System by the Adjacency module, but the Update
             * module has not processed the DIS Status change event. The Update
             * Module while processing the DIS Status change event will
             * construct pseudonode LSPs appropriately. Hence we need not worry
             * about the current adjacency at this point.
             */

            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdDelExtISReachTLV () \n"));
            return (ISIS_SUCCESS);
        }
        pNonZeroLSP = pSNLSP->pNonZeroLSP;
        u1Level = ISIS_LEVEL1;
    }
    else if (u1LSPType == ISIS_L2_PSEUDO_LSP)
    {
        pPrevSNLSP = NULL;
        pSNLSP = pContext->SelfLSP.pL2SNLSP;

        /* Get the appropriate PseudoNode LSP based on the Circuit Index
         *
         * NOTE: Pseudonode LSPs are maintained per circuit basis
         */

        while ((pSNLSP != NULL) && (pSNLSP->pCktEntry != NULL)
               && (pSNLSP->pCktEntry->u4CktIdx != u4CktIdx))
        {
            pPrevSNLSP = pSNLSP;
            pSNLSP = pSNLSP->pNext;
        }
        if (pSNLSP == NULL)
        {
            /* There may be cases where DIS status is marked as TRUE
             * for the Local System by the Adjacency module, but the Update
             * module has not processed the DIS Status change event. The Update
             * Module while processing the DIS Status change event will
             * construct pseudonode LSPs appropriately. Hence we need not worry
             * about the current adjacency at this point.
             */

            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdDelExtISReachTLV () \n"));
            return (ISIS_SUCCESS);
        }
        pNonZeroLSP = pSNLSP->pNonZeroLSP;
        u1Level = ISIS_LEVEL2;
    }

    /* Go through the chain of SelfLSPs till the appropriate LSP buffer which
     * includes information matching the given Extended IS Reachability is found
     */

    pTravNonZeroLSP = pNonZeroLSP;

    while (pTravNonZeroLSP != NULL)
    {
        /* Since we are starting with a New TLV chain, initialize the pPrevTLV
         * pointer to NULL.
         */

        pPrevTLV = NULL;
        pTravTLV = pTravNonZeroLSP->pTLV;

        /* Scan the TLV List in each LSP buffer
         */

        while (pTravTLV != NULL)
        {
            if (pTravTLV->u1Code == pMDT->u1TLVType)
            {
                /* NOTE: Same Peer may have multiple adjacencies with the local
                 * system over different circuits. Hence we have to match the
                 * circuit before deleting the adjacency information
                 */

                if ((pTravTLV->u4CktIdx == u4CktIdx)
                    && (((MEMCMP (pTravTLV->au1Value, pMDT->au1AdjSysID,
                                  ISIS_SYS_ID_LEN + 1) == 0)
                         && ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                             || (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)))
                        || ((MEMCMP (pTravTLV->au1Value, pMDT->au1AdjSysID,
                                     ISIS_SYS_ID_LEN) == 0)
                            && ((u1LSPType == ISIS_L1_PSEUDO_LSP)
                                || (u1LSPType == ISIS_L2_PSEUDO_LSP)))))
                {
                    /* A matching TLV is found
                     */
                    ISIS_FORM_STR (ISIS_TLV_LEN, pTravTLV->au1Value,
                                   acTlvValue);

                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Deleting Extended IS Reachability TLV [%s] From Self-LSP - Circuit Index [ %u ], LSP Type [ %s ]\n",
                             acTlvValue, u4CktIdx,
                             ISIS_GET_LSP_STR (u1LSPType)));

                    u1ISAdjFound = ISIS_TRUE;
                    i4RetVal = ISIS_SUCCESS;

                    if (pPrevTLV != NULL)
                    {
                        /* Deleting a TLV which is not the first TLV in the
                         * chain
                         */

                        pPrevTLV->pNext = pTravTLV->pNext;
                    }
                    else
                    {
                        /* The TLV to be deleted is the first one in the list of
                         * TLVs maintained in the LSP buffer
                         */

                        pTravNonZeroLSP->pTLV = pTravTLV->pNext;
                    }
                    pTravTLV->pNext = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                    break;
                }
                else
                {
                    pPrevTLV = pTravTLV;
                    pTravTLV = pTravTLV->pNext;
                }
            }
            else
            {
                pPrevTLV = pTravTLV;
                pTravTLV = pTravTLV->pNext;
            }
        }

        if (u1ISAdjFound == ISIS_TRUE)
        {
            /* Verify whether removal of the specified Extended IS Reachability from the LSP
             * buffer, has left the LSP buffer empty without any TLVs
             */
            RBTreeCount (pTravNonZeroLSP->IPRATLV, &u4Count);
            if ((pTravNonZeroLSP->pTLV == NULL) && (u4Count == 0))
            {
                /* No more TLVs left in the LSP buffer. Now try to remove the
                 * LSP buffer also from the Self LSPs
                 */

                if (pPrevNonZeroLSP == NULL)
                {
                    /* The LSP buffer to be removed is the first LSP buffer in
                     * the list. We have to advance the head
                     */

                    if (u1PNodeFlag == ISIS_SET)
                    {
                        /* The buffer to be removed is one of the Pseudonode LSP
                         * buffers
                         */

                        pSNLSP->pNonZeroLSP = pTravNonZeroLSP->pNext;
                    }
                    else
                    {
                        /* The buffer to be removed is one of the Non-Pseudonode
                         * LSP buffers
                         */

                        pNSNLSP->pNonZeroLSP = pTravNonZeroLSP->pNext;
                    }
                }
                else
                {
                    /* The buffer to be removed is not the first one in the list
                     */

                    pPrevNonZeroLSP->pNext = pTravNonZeroLSP->pNext;
                }

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Purging Self-LSP Info - Reason [ Extended IS Reachability Deleted - LSP Buffer Empty ]\n"));
                IsisUpdFindAndPurgeSelfLSP (pContext, pTravNonZeroLSP, u1Level);
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pTravNonZeroLSP);
                if (pSNLSP != NULL)
                {
                    if (((u1LSPType == ISIS_L1_PSEUDO_LSP) ||
                         (u1LSPType == ISIS_L2_PSEUDO_LSP)) &&
                        (pSNLSP->pNonZeroLSP == NULL))
                    {
                        pZeroLSP = pSNLSP->pZeroLSP;
                        IsisUpdFindAndPurgeSelfLSP (pContext, pZeroLSP,
                                                    u1Level);
                        ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pZeroLSP);
                        if (pPrevSNLSP == NULL)
                        {
                            if (u1Level == ISIS_LEVEL1)
                            {
                                pContext->SelfLSP.pL1SNLSP = pSNLSP->pNext;
                            }
                            else
                            {
                                pContext->SelfLSP.pL2SNLSP = pSNLSP->pNext;
                            }

                        }
                        else
                        {
                            pPrevSNLSP->pNext = pSNLSP->pNext;
                        }
                        pSNLSP->pNext = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_SNLI, (UINT1 *) pSNLSP);
                    }
                }
                else
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Cannot dereference"
                             "a NULL pointer \n"));
                }
            }

            else
            {
                /* Some TLVs are still left in the Buffer from which we have
                 * deleted the given adjacency information. We have to adjust
                 * the length of the Buffer appropriately since the removal of a
                 * TLV will decrease the length of the buffer
                 */

                pTravNonZeroLSP->u1NumExtISReachTLV--;

                u1RemBytes =
                    (UINT1) ISIS_GET_REM_BYTES (pTravNonZeroLSP->
                                                u1NumExtISReachTLV,
                                                ISIS_EXT_IS_REACH_TLV_LEN);

                /* LOGIC: Decrement the number of TLVs. We now have to decide
                 * whether to decrement the buffer length by
                 * "ISIS_EXT_IS_REACH_TLV_LEN" or "ISIS_EXT_IS_REACH_TLV_LEN + 2" (2 bytes
                 * for Code and Length).
                 * Now assume you are going to add back the Adjacency 
                 * information, that is just now deleted, to the SelfLSP buffer.
                 * The number of TLVs will be the number obtained after 
                 * decrementing the total number by '1'. If 'u1RemBytes' 
                 * indicates that we have to add a new TLV, then we can
                 * decrement the length of the buffer by "ISIS_EXT_IS_REACH_TLV_LEN +
                 * 2", else we must decrement the length only by
                 * "ISIS_EXT_IS_REACH_TLV_LEN". This is because, if addition causes
                 * '2' extra bytes to be included, then deletion must remove
                 * those '2' extra bytes
                 */

                if (u1RemBytes < ISIS_EXT_IS_REACH_TLV_LEN)
                {
                    pTravNonZeroLSP->u2LSPLen =
                        (UINT2) (pTravNonZeroLSP->u2LSPLen -
                                 (ISIS_EXT_IS_REACH_TLV_LEN + 2));
                }
                else
                {
                    pTravNonZeroLSP->u2LSPLen =
                        (UINT2) (pTravNonZeroLSP->u2LSPLen -
                                 ISIS_EXT_IS_REACH_TLV_LEN);
                }
                pTravNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
            }
            break;
        }
        else
        {
            /* Moving the Pointer to next LSP when the appropriate TLV is not
             * there in the present TLV
             */

            pPrevNonZeroLSP = pTravNonZeroLSP;
            pTravNonZeroLSP = pTravNonZeroLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelExtISReachTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdDelExtMtIPReachTLV ()
 * Description : This Routine deletes Extended IP Reachability TLV from SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the Extended IP
 *                          Reachability information
 *               u1SAChk      - Flag indicating whether a matching Summary 
 *                              Address is to be added to Self LSP. In case of
 *                              summary addresses themselves being added viz
 *                              this routine or in the case of SPT PATH Nodes
 *                              being added, this flag will be set to FALSE 
 *               u1SADcrFlag - Flag Indicating whether the SA usage count 
 *                              is to be updated. The flag will be set to FALSE
 *                              in case of addtions being made for Summary
 *                              Addresses to PATH nodes
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful deletion of Extended IP Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/
INT4
IsisUpdDelExtMtIPReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                           UINT1 u1SAChk, UINT1 u1SADcrFlag)
{
    tIsisIPRATLV       *pTravTLV = NULL;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisSAEntry       *pSARec = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pPrevNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisIPRATLV        IPRATravTLV;
    tIsisRRDRoutes     *pRRDRoute = NULL;
    tIsisRRDRoutes      RRDRoute;
    UINT4               u4Count = 0;
    UINT4               u4DefMetric;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Level = 0;
    UINT1               u1Result = 0;
    UINT1               u1SAState = ISIS_SUMM_ADMIN_OFF;
    UINT1               u1LSPType = 0;
    UINT1               u1RemBytes = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelExtMtIPReachTLV ()\n"));
    MEMSET (&IPRATravTLV, 0, sizeof (tIsisIPRATLV));

    u1LSPType = pMDT->u1LSPType;

    if ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
        && (pContext->SelfLSP.pL1NSNLSP != NULL))
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u1Level = ISIS_LEVEL1;
        u1SAState = ISIS_SUMM_ADMIN_L1;
    }
    else if ((u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
             && (pContext->SelfLSP.pL2NSNLSP != NULL))
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        pNonZeroLSP = pNSNLSP->pNonZeroLSP;
        u1Level = ISIS_LEVEL2;
        u1SAState = ISIS_SUMM_ADMIN_L2;
    }
    else
    {
        return ISIS_SUCCESS;
    }

    /* NOTE: 
     *
     * 1. u1SAChk = FALSE implies no need to bothger about the matching Summary
     *    Address record. This is the case when Summary Address entries 
     *    themselves are being deleted
     *    
     * 2. u1SAChk = TRUE implies Summary Address entries are to be updated for
     *    usage count since information being Deleted is IPRA information. Then
     *
     *    2a. u1SADcrFlag = TRUE implies that the usage count is to be
     *        decremented since the entry being deleted is IPRA information
     *        configured by the manager
     *    2b. u1SADcrFlag = FALSE implies that the usage count need not be
     *        decremented since the entry being deleted is either IPRA 
     *        information retrieved from the PATH nodes or it is Summary 
     *        Address Information
     */

    if (u1SAChk == ISIS_TRUE)
    {
        pSARec = pContext->SummAddrTable.pSAEntry;
    }

    /* Go through the summary address entries and get an entry that matches the
     * given IPRA
     */

    while (pSARec != NULL)
    {
        if (((pSARec->u1AdminState == u1SAState)
             || (pSARec->u1AdminState == ISIS_SUMM_ADMIN_L12))
            && ((IsisUtlCompIPAddr (pSARec->au1SummAddr,
                                    pSARec->u1PrefixLen,
                                    pMDT->IPAddr.au1IpAddr,
                                    pSARec->u1PrefixLen)) == ISIS_TRUE))
        {
            if (u1SADcrFlag == ISIS_TRUE)
            {
                if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
                {
                    pSARec->u2L1UsageCnt--;
                }
                else
                {
                    pSARec->u2L2UsageCnt--;
                }

            }

            /* Check if the given IPRA is the only entry referring to this 
             * Summary Address (u2UsageCount == 0). A Non-Zero Usage count
             * indicates that there are other IPRAs which matched the Summary
             * Address entry and hence the TLV containing this SA entry must be
             * retained in the SelfLSP. If Usage count is '0', then we can
             * search for the TLV containing information regarding the matching
             * SA entry and delete it.
             */

            if (((pSARec->u2L1UsageCnt == 1)
                 && (u1LSPType == ISIS_L1_NON_PSEUDO_LSP))
                || ((pSARec->u2L2UsageCnt == 1)
                    && (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)))
            {
                /* Mark this information, we will delete the SA TLV from
                 * SelfLSP later
                 */

                pMDT->u4FullMetric = pSARec->u4FullMetric;
                if (pSARec->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pMDT->IPAddr.au1IpAddr, pSARec->au1SummAddr,
                            ISIS_MAX_IPV4_ADDR_LEN);
                    pMDT->IPAddr.u1Length = ISIS_MAX_IPV4_ADDR_LEN;
                    pMDT->IPAddr.u1PrefixLen = pSARec->u1PrefixLen;
                }
                else
                {

                    MEMCPY (pMDT->IPAddr.au1IpAddr, pSARec->au1SummAddr,
                            ISIS_MAX_IPV6_ADDR_LEN);
                    pMDT->IPAddr.u1Length = ISIS_MAX_IPV6_ADDR_LEN;
                    pMDT->IPAddr.u1PrefixLen = pSARec->u1PrefixLen;
                }
            }
            else
            {
                /* There are other IPRAs referring to the Summary Address entry,
                 * hence do not delete the TLV containing the information
                 */

                return ISIS_SUCCESS;
            }
            break;
        }

        pSARec = pSARec->pNext;
    }

    pTravNonZeroLSP = pNonZeroLSP;

    /* Go through the chain of SelfLSPs till the appropriate LSP buffer which
     * includes information matching the given IPRA is found
     */
    IPRATravTLV.u1Code = pMDT->u1TLVType;
    MEMCPY (IPRATravTLV.IPAddr.au1IpAddr,
            pMDT->IPAddr.au1IpAddr,
            (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
    IPRATravTLV.IPAddr.u1PrefixLen = pMDT->IPAddr.u1PrefixLen;

    if (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV)
    {
        ISIS_FORM_IPV4_ADDR (pMDT->IPAddr.au1IpAddr, au1IPv4Addr,
                             ISIS_MAX_IPV4_ADDR_LEN);
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Deleting Extended IP Reachability From Self-LSP - IP [ %s ], LSP Type [ %s ]\n",
                 au1IPv4Addr, ISIS_GET_LSP_STR (u1LSPType)));
    }
    else
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Deleting MT IPv6 Reachability From Self-LSP - IP [ %s ], LSP Type [ %s ]\n",
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) &(pMDT->IPAddr.au1IpAddr)),
                 ISIS_GET_LSP_STR (u1LSPType)));
    }

    while (pTravNonZeroLSP != NULL)
    {

        pTravTLV = (tIsisIPRATLV *)
            RBTreeGet (pTravNonZeroLSP->IPRATLV, (tRBElem *) & IPRATravTLV);

        if ((pTravTLV != NULL) && (pMDT->u1SrcProtoId == pTravTLV->u1Protocol))
        {
            RBTreeRemove (pTravNonZeroLSP->IPRATLV, (tRBElem *) pTravTLV);
            u1Result = ISIS_TRUE;
            i4RetVal = ISIS_SUCCESS;
        }
        if ((u1Result != ISIS_TRUE) &&
            (pTravTLV != NULL) &&
            (pTravTLV->u1Protocol == ISIS_LOCAL_ID) &&
            (pMDT->u1SrcProtoId == 0))
        {
            MEMSET (&(RRDRoute.IPAddr), 0, sizeof (tIsisIpAddr));
            MEMCPY (&(RRDRoute.IPAddr), &(pTravTLV->IPAddr),
                    sizeof (tIsisIpAddr));
            if ((pTravTLV->u1Code == ISIS_IP_INTERNAL_RA_TLV)
                || (pTravTLV->u1Code == ISIS_EXT_IP_REACH_TLV))
            {
                RRDRoute.IPAddr.u1AddrType = ISIS_IPV4;
            }
            else
            {
                RRDRoute.IPAddr.u1AddrType = ISIS_IPV6;
            }
            pRRDRoute =
                RBTreeGet (pContext->RRDRouteRBTree, ((tRBElem *) & RRDRoute));
            if (pRRDRoute != NULL)
            {
                /* update the metric alone */
                u4DefMetric = OSIX_HTONL (pRRDRoute->u4Metric);
                if (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV)
                {
                    MEMCPY (pTravTLV->au1Value, &u4DefMetric,
                            sizeof (u4DefMetric));
                }
                else
                {
                    MEMCPY (&(pTravTLV->au1Value[ISIS_MT_ID_LEN]),
                            &u4DefMetric, sizeof (u4DefMetric));
                }
            }
            break;
        }
        if (u1Result == ISIS_TRUE)
        {
            /* We have deleted a TLV, and hence we now have to verify whether
             * the Buffer which held the deleted TLV has become empty. If there
             * are no more TLVs in the buffer, then the LSP can be purged.
             */
            RBTreeCount (pTravNonZeroLSP->IPRATLV, &u4Count);
            if ((pTravNonZeroLSP->pTLV == NULL) && (u4Count == 0))
            {
                /* No more LSPs in the Buffer
                 */

                if (pPrevNonZeroLSP == NULL)
                {
                    /* The LSP Buffer is the first in the chain of SelfLSPs
                     */

                    pNSNLSP->pNonZeroLSP = pTravNonZeroLSP->pNext;
                }
                else
                {
                    /* The LSP Buffer is not the first in the chain of SelfLSPs
                     */

                    pPrevNonZeroLSP->pNext = pTravNonZeroLSP->pNext;
                }

                /* Purge the LSP buffer which has become empty due to the
                 * deletion of the TLV
                 */

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Purging Self-LSP Info - Reason [ Extended IP Reachability Deleted - LSP Buffer Empty ]\n"));
                IsisUpdFindAndPurgeSelfLSP (pContext, pTravNonZeroLSP, u1Level);
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pTravNonZeroLSP);
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
            }
            else
            {
                /* Some TLVs are still left in the Buffer from which we have
                 * deleted the given IPRA information. We have to adjust
                 * the length of the Buffer appropriately since the removal of a
                 * TLV will decrease the length of the buffer
                 */
                if (pMDT->u1TLVType == ISIS_EXT_IP_REACH_TLV)
                {
                    pTravNonZeroLSP->u1NumExtIpReachTLV--;
                    u1RemBytes = (UINT1) ISIS_GET_REM_BYTES
                        (pTravNonZeroLSP->u1NumExtIpReachTLV,
                         ISIS_EXT_IP_REACH_TLV_LEN);
                    if (u1RemBytes < ISIS_EXT_IP_REACH_TLV_LEN)
                    {
                        pTravNonZeroLSP->u2LSPLen =
                            (UINT2) (pTravNonZeroLSP->u2LSPLen - 2);
                    }
                }
                else
                {
                    pTravNonZeroLSP->u1NumMTIpv6ReachTLV--;
                    u1RemBytes = (UINT1) ISIS_GET_REM_BYTES
                        (pTravNonZeroLSP->u1NumMTIpv6ReachTLV,
                         ISIS_MT_IPV6_REACH_TLV_LEN);
                    if (u1RemBytes < ISIS_MT_IPV6_REACH_TLV_LEN)
                    {
                        pTravNonZeroLSP->u2LSPLen =
                            (UINT2) (pTravNonZeroLSP->u2LSPLen - 2);
                    }
                }

                /* LOGIC: Decrement the number of TLVs. We now have to decide
                 * whether to decrement the buffer length by
                 * "ISIS_EXT_IP_REACH_TLV_LEN or ISIS_MT_IPV6_REACH_TLV_LEN" or 
                 *  "(ISIS_EXT_IP_REACH_TLV_LEN or ISIS_MT_IPV6_REACH_TLV_LEN) + 2" (2 bytes
                 * for Code and Length).  Now assume you are going to add back 
                 * the IPRA information, that is just now deleted, to the 
                 * SelfLSP buffer. The number of TLVs will be the number 
                 * obtained after decrementing the total number by '1'. If 
                 * 'u1RemBytes' indicates that we have to add a new TLV, then 
                 * we can decrement the length of the buffer by 
                 * "(ISIS_EXT_IP_REACH_TLV_LEN or ISIS_MT_IPV6_REACH_TLV_LEN) + 2", 
                 * else we must decrement the length 
                 * only by "ISIS_EXT_IP_REACH_TLV_LEN or ISIS_MT_IPV6_REACH_TLV_LEN". 
                 * This is because, if addition 
                 * causes '2' extra bytes to be included, then deletion must 
                 * remove those '2' extra bytes
                 */

                pTravNonZeroLSP->u2LSPLen =
                    (UINT2) (pTravNonZeroLSP->u2LSPLen - pTravTLV->u1Len);
                pTravNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
            }
            break;
        }
        else
        {
            pPrevNonZeroLSP = pTravNonZeroLSP;
            pTravNonZeroLSP = pTravNonZeroLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelExtMtIPReachTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdDelMTISReachTLV ()
 * Description : This Routine deletes MT IS Reachability TLV from SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the MT IS
 *                          Reachability information
 *               u4CktIdx  - Circuit Index
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful deletion of MT IS Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

INT4
IsisUpdDelMTISReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                        UINT4 u4CktIdx)
{
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisLSPInfo       *pPrevNonZeroLSP = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisTLV           *pTravTLV = NULL;
    tIsisTLV           *pPrevTLV = NULL;
    UINT4               u4Count = 0;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT2               u2MtId = 0;
    UINT1               u1RemBytes = 0;
    UINT1               u1LSPType = 0;
    UINT1               u1Level = 0;
    UINT1               u1ISAdjFound = ISIS_FALSE;
    CHAR                acTlvValue[ISIS_TLV_LEN];

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelMTISReachTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP != NULL)
        {
            pNonZeroLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
        }
        u1Level = ISIS_LEVEL1;
    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP != NULL)
        {
            pNonZeroLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
        }
        u1Level = ISIS_LEVEL2;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelMTISReachTLV ()\n"));
        return i4RetVal;
    }

    /* Go through the chain of SelfLSPs till the appropriate LSP buffer which
     * includes information matching the given MT IS Reachability is found
     */

    pTravNonZeroLSP = pNonZeroLSP;

    while (pTravNonZeroLSP != NULL)
    {
        /* Since we are starting with a New TLV chain, initialize the pPrevTLV
         * pointer to NULL.
         */

        pPrevTLV = NULL;
        pTravTLV = pTravNonZeroLSP->pTLV;

        /* Scan the TLV List in each LSP buffer
         */

        while (pTravTLV != NULL)
        {
            if (pTravTLV->u1Code == pMDT->u1TLVType)
            {
                /* NOTE: Same Peer may have multiple adjacencies with the local
                 * system over different circuits. Hence we have to match the
                 * circuit before deleting the adjacency information
                 */

                u2MtId = OSIX_HTONS (pMDT->u2MTId);
                if ((pTravTLV->u4CktIdx == u4CktIdx)
                    && ((MEMCMP (pTravTLV->au1Value, &u2MtId, ISIS_MT_ID_LEN))
                        == 0)
                    &&
                    ((MEMCMP
                      (&(pTravTLV->au1Value[2]), pMDT->au1AdjSysID,
                       ISIS_SYS_ID_LEN + 1)) == 0))
                {
                    /* A matching TLV is found
                     */

                    ISIS_FORM_STR (ISIS_TLV_LEN, pTravTLV->au1Value,
                                   acTlvValue);
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Deleting MT IS Reachability TLV [%s] From SelfLSP - Circuit Index [ %u ]"
                             ", LSP Type [ %s ]\n", acTlvValue, u4CktIdx,
                             ISIS_GET_LSP_STR (u1LSPType)));

                    u1ISAdjFound = ISIS_TRUE;
                    i4RetVal = ISIS_SUCCESS;

                    if (pPrevTLV != NULL)
                    {
                        /* Deleting a TLV which is not the first TLV in the
                         * chain
                         */

                        pPrevTLV->pNext = pTravTLV->pNext;
                    }
                    else
                    {
                        /* The TLV to be deleted is the first one in the list of
                         * TLVs maintained in the LSP buffer
                         */

                        pTravNonZeroLSP->pTLV = pTravTLV->pNext;
                    }
                    pTravTLV->pNext = NULL;
                    ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                    break;
                }
                else
                {
                    pPrevTLV = pTravTLV;
                    pTravTLV = pTravTLV->pNext;
                }
            }
            else
            {
                pPrevTLV = pTravTLV;
                pTravTLV = pTravTLV->pNext;
            }
        }

        if (u1ISAdjFound == ISIS_TRUE)
        {
            /* Verify whether removal of the specified MT IS Reachability from the LSP
             * buffer, has left the LSP buffer empty without any TLVs
             */

            RBTreeCount (pTravNonZeroLSP->IPRATLV, &u4Count);
            if ((pTravNonZeroLSP->pTLV == NULL) && (u4Count == 0))
            {
                /* No more TLVs left in the LSP buffer. Now try to remove the
                 * LSP buffer also from the Self LSPs
                 */

                if (pPrevNonZeroLSP == NULL)
                {
                    /* The LSP buffer to be removed is the first LSP buffer in
                     * the list. We have to advance the head
                     */
                    pNSNLSP->pNonZeroLSP = pTravNonZeroLSP->pNext;
                }
                else
                {
                    /* The buffer to be removed is not the first one in the list
                     */

                    pPrevNonZeroLSP->pNext = pTravNonZeroLSP->pNext;
                }

                UPP_PT ((ISIS_LGST,
                         "UPD <T> : Purging Self-LSP Info - Reason [ MT IS Reachability Deleted - LSP Buffer Empty ]\n"));
                IsisUpdFindAndPurgeSelfLSP (pContext, pTravNonZeroLSP, u1Level);
                ISIS_MEM_FREE (ISIS_BUF_SLSP, (UINT1 *) pTravNonZeroLSP);
            }
            else
            {
                /* Some TLVs are still left in the Buffer from which we have
                 * deleted the given adjacency information. We have to adjust
                 * the length of the Buffer appropriately since the removal of a
                 * TLV will decrease the length of the buffer
                 */

                pTravNonZeroLSP->u1NumMTISReachTLV--;

                u1RemBytes =
                    (UINT1) ISIS_GET_REM_BYTES (pTravNonZeroLSP->
                                                u1NumMTISReachTLV,
                                                ISIS_MT_IS_REACH_TLV_LEN);

                if (u1RemBytes < ISIS_MT_IS_REACH_TLV_LEN)
                {
                    pTravNonZeroLSP->u2LSPLen =
                        (UINT2) (pTravNonZeroLSP->u2LSPLen -
                                 (ISIS_MT_IS_REACH_TLV_LEN + 2));
                }
                else
                {
                    pTravNonZeroLSP->u2LSPLen =
                        (UINT2) (pTravNonZeroLSP->u2LSPLen -
                                 ISIS_MT_IS_REACH_TLV_LEN);
                }
                pTravNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
            }
            break;
        }
        else
        {
            /* Moving the Pointer to next LSP when the appropriate TLV is not
             * there in the present TLV
             */

            pPrevNonZeroLSP = pTravNonZeroLSP;
            pTravNonZeroLSP = pTravNonZeroLSP->pNext;
        }
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelMTISReachTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdDelDynHostNmeTLV ()
 * Description : This Routine Deleted MT TLV from SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the hostname TLV info
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On Successful deletion of hostname TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/
INT4
IsisUpdDelDynHostNmeTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    tIsisTLV           *pTravTLV = NULL;
    tIsisTLV           *pPrevTLV = NULL;
    tIsisLSPInfo       *pZeroLSP = NULL;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1HostNmeTlvLen = 0;
    UINT1               u1RemBytes = 0;
    UINT1               u1LSPType = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdDelDynHostNmeTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
        && (pContext->SelfLSP.pL1NSNLSP != NULL))
    {
        pZeroLSP = pContext->SelfLSP.pL1NSNLSP->pZeroLSP;
    }
    else if ((u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
             && (pContext->SelfLSP.pL2NSNLSP != NULL))
    {
        pZeroLSP = pContext->SelfLSP.pL2NSNLSP->pZeroLSP;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelDynHostNmeTLV ()\n"));
        return ISIS_SUCCESS;
    }

    /* Scan through the entire TLV list and get the matching information record
     */
    pTravTLV = pZeroLSP->pTLV;

    while (pTravTLV != NULL)
    {
        if (pTravTLV->u1Code == pMDT->u1TLVType)
        {
            u1HostNmeTlvLen = pTravTLV->u1Len;

            i4RetVal = ISIS_SUCCESS;

            if (pPrevTLV != NULL)
            {
                /* Not a first TLV in the List */

                pPrevTLV->pNext = pTravTLV->pNext;
                pTravTLV->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                break;
            }
            else
            {
                /* Very first TLV in the List and hence advance the head
                 */

                pZeroLSP->pTLV = pTravTLV->pNext;
                pTravTLV->pNext = NULL;
                ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTLV);
                break;
            }
        }

        pPrevTLV = pTravTLV;
        pTravTLV = pTravTLV->pNext;
    }

    if (i4RetVal == ISIS_SUCCESS)
    {
        UPP_PT ((ISIS_LGST,
                 "UPD <T> : Deleting Dynamic hostname TLV To Self-LSP - LSP Type [ %s ]\n",
                 ISIS_GET_LSP_STR (u1LSPType)));
        pZeroLSP->u1NumHostNmeTLV--;

        u1RemBytes =
            (UINT1) ISIS_GET_REM_BYTES (pZeroLSP->u1NumHostNmeTLV,
                                        u1HostNmeTlvLen);

        if (u1RemBytes < u1HostNmeTlvLen)
        {
            pZeroLSP->u2LSPLen =
                (UINT2) (pZeroLSP->u2LSPLen - (u1HostNmeTlvLen + 2));
        }
        else
        {
            pZeroLSP->u2LSPLen = (UINT2) (pZeroLSP->u2LSPLen - u1HostNmeTlvLen);
        }
        pZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdDelDynHostNmeTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdModExtISReachTLV ()
 * Description : This Routine modifies Extended IS Reachability TLV in SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the Extended IS 
 *                          Reachability information
 *               u4CktIdx  - Circuit Index
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful modification of Extended IS Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

INT4
IsisUpdModExtISReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                         UINT4 u4CktIdx)
{
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisTLV           *pTravTLV = NULL;
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT4               u4WideMetric = 0;
    UINT1               au1AdjSysID[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
    UINT1               u1LSPType = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdModExtISReachTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP) &&
        (pContext->SelfLSP.pL1NSNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
    }
    else if ((u1LSPType == ISIS_L2_NON_PSEUDO_LSP) &&
             (pContext->SelfLSP.pL2NSNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
    }
    else if ((u1LSPType == ISIS_L1_PSEUDO_LSP) &&
             (pContext->SelfLSP.pL1SNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL1SNLSP->pNonZeroLSP;
    }
    else if ((u1LSPType == ISIS_L2_PSEUDO_LSP) &&
             (pContext->SelfLSP.pL2SNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL2SNLSP->pNonZeroLSP;
    }

    while (pNonZeroLSP != NULL)
    {
        pTravTLV = pNonZeroLSP->pTLV;

        MEMCPY (au1AdjSysID, pMDT->au1AdjSysID,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

        while (pTravTLV != NULL)
        {
            /* Finding the Appropriate TLV
             */

            if (pTravTLV->u1Code == pMDT->u1TLVType)
            {
                /* Since the same peer can be adjacent to the local system over
                 * multiple circuits, we have to match the adjacency information
                 * with both SysId and Circuit Index
                 */
                if ((pTravTLV->u4CktIdx == u4CktIdx)
                    && ((MEMCMP (pTravTLV->au1Value, au1AdjSysID,
                                 ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0))
                {
                    /* Only Metric will change. Copy that
                     */

                    u4WideMetric = (pMDT->u4FullMetric << 8);
                    u4WideMetric = OSIX_HTONL (u4WideMetric);
                    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)
                    {
                        UPP_PT ((ISIS_LGST,
                                 "UPD <T> : Updating Metric - Extended IS Reachability In Self-LSP - Metric [ %u ]"
                                 ", LSP Type [ %s ]\n",
                                 pMDT->u4FullMetric,
                                 ISIS_GET_LSP_STR (u1LSPType)));
                    }
                    else
                    {
                        UPP_PT ((ISIS_LGST,
                                 "UPD <T> : Updating Metric - Extended IS Reachability In Self-LSP - Metric [ %u ]"
                                 ", LSP Type [ %s ]\n",
                                 pMDT->Metric[0],
                                 ISIS_GET_LSP_STR (u1LSPType)));
                    }
                    MEMCPY (&(pTravTLV->au1Value[7]), &u4WideMetric,
                            ISIS_IS_DEF_METRIC_LEN);
                    pNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
                    i4RetVal = ISIS_SUCCESS;
                    break;
                }
                else
                {

                    pTravTLV = pTravTLV->pNext;
                }
            }
            else
            {
                pTravTLV = pTravTLV->pNext;
            }
        }
        pNonZeroLSP = pNonZeroLSP->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdModExtISReachTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdModExtMtIPReachTLV ()
 * Description : This Routine modifies Extended IP Reachability/MT -IPV6 Reach 
 *               TLV in SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the Extended IP
 *                          Reachability information
 *               u1SACheck - ISIS_TRUE/ISIS_FALSE
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful modification of Extended IP Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

INT4
IsisUpdModExtMtIPReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                           UINT1 u1SACheck)
{
    tIsisIPRATLV       *pTravTLV = NULL;
    tIsisSAEntry       *pSARec = NULL;
    tIsisLSPInfo       *pTravNonZeroLSP = NULL;
    tIsisIPRATLV        IPRATravTLV;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4DefMetric = 0;
    UINT1               u1LSPType = 0;
    UINT1               u1SAState = ISIS_SUMM_ADMIN_OFF;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdModExtMtIPReachTLV ()\n"));
    MEMSET (&IPRATravTLV, 0, sizeof (tIsisIPRATLV));
    u1LSPType = pMDT->u1LSPType;

    if ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP) && (pContext->SelfLSP.pL1NSNLSP))
    {
        pTravNonZeroLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
        u1SAState = ISIS_SUMM_ADMIN_L1;
    }
    else if ((u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
             && (pContext->SelfLSP.pL2NSNLSP))
    {
        pTravNonZeroLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
        u1SAState = ISIS_SUMM_ADMIN_L2;
    }
    else
    {
        return ISIS_SUCCESS;
    }

    if (u1SACheck == ISIS_TRUE)
    {
        pSARec = pContext->SummAddrTable.pSAEntry;
    }

    UPP_PT ((ISIS_LGST,
             "UPD <T> : Updating Extended MT IP / IPv6 Reachability in Self-LSP - LSP Type [ %s ]\n",
             ISIS_GET_LSP_STR (u1LSPType)));

    while (pSARec != NULL)
    {
        /* Comparing the given Extended IP Reachability information with the Summary Address
         * entries. If the given EXT IP REACH entry  matches a Summary Address entry, then we
         * cannot modify the metric in the LSP buffer since the buffer would now
         * hold the Summary Address and not the EXT IP Reachability information.
         */

        if (((pSARec->u1AdminState == u1SAState)
             || (pSARec->u1AdminState == ISIS_SUMM_ADMIN_L12))
            && ((IsisUtlCompIPAddr (pSARec->au1SummAddr,
                                    pSARec->u1PrefixLen,
                                    pMDT->IPAddr.au1IpAddr,
                                    pSARec->u1PrefixLen)) == ISIS_TRUE))
        {
            return ISIS_SUCCESS;
        }
        pSARec = pSARec->pNext;
    }

    IPRATravTLV.u1Code = pMDT->u1TLVType;
    MEMCPY (IPRATravTLV.IPAddr.au1IpAddr,
            pMDT->IPAddr.au1IpAddr,
            (ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen)));
    IPRATravTLV.IPAddr.u1PrefixLen = pMDT->IPAddr.u1PrefixLen;

    while (pTravNonZeroLSP != NULL)
    {

        pTravTLV = (tIsisIPRATLV *)
            RBTreeGet (pTravNonZeroLSP->IPRATLV, (tRBElem *) & IPRATravTLV);
        if ((pTravTLV != NULL) &&
            ((pMDT->u1SrcProtoId == pTravTLV->u1Protocol) ||
             (pMDT->u1SrcProtoId == 0)))
        {
            /* IPRA TLV mathes the given information. Updating the
             * Metric for the matched TLVnnnn
             */
            if ((pMDT->u1L1toL2Flag == ISIS_TRUE)
                && (pTravTLV->u1L1toL2Flag != ISIS_TRUE))
            {
                return ISIS_SUCCESS;
            }
            if (pMDT->u1IsisEnabled != ISIS_TRUE)
            {
                u4DefMetric = OSIX_HTONL (pMDT->u4FullMetric);
                if (pMDT->u1TLVType != ISIS_MT_IPV6_REACH_TLV)
                {
                    MEMCPY (pTravTLV->au1Value, &u4DefMetric,
                            ISIS_EXT_DEF_METRIC_LEN);
                }
                else
                {
                    MEMCPY (&(pTravTLV->au1Value[2]), &u4DefMetric,
                            ISIS_EXT_DEF_METRIC_LEN);
                }
                pTravNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
            }
            return ISIS_SUCCESS;
        }

        pTravNonZeroLSP = pTravNonZeroLSP->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdModExtMtIPReachTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdModMTISReachTLV ()
 * Description : This Routine modifis MT IS Reachability TLV in SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure contains the MT IS
 *                          Reachability information
 *               u4CktIdx  - Circuit Index
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful modification of MT IS Reachability TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

INT4
IsisUpdModMTISReachTLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                        UINT4 u4CktIdx)
{
    tIsisLSPInfo       *pNonZeroLSP = NULL;
    tIsisTLV           *pTravTLV = NULL;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4WideMetric = 0;
    UINT1               au1AdjSysID[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
    UINT2               u2MtId = 0;
    UINT1               u1LSPType = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdModMTISReachTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP) &&
        (pContext->SelfLSP.pL1NSNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;
    }
    else if ((u1LSPType == ISIS_L2_NON_PSEUDO_LSP) &&
             (pContext->SelfLSP.pL2NSNLSP != NULL))
    {
        pNonZeroLSP = pContext->SelfLSP.pL2NSNLSP->pNonZeroLSP;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdModMTISReachTLV ()\n"));
        return ISIS_SUCCESS;
    }

    while (pNonZeroLSP != NULL)
    {
        pTravTLV = pNonZeroLSP->pTLV;

        MEMCPY (au1AdjSysID, pMDT->au1AdjSysID,
                ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN);

        while (pTravTLV != NULL)
        {
            /* Finding the Appropriate TLV
             */

            if (pTravTLV->u1Code == pMDT->u1TLVType)
            {
                /* Since the same peer can be adjacent to the local system over
                 * multiple circuits, we have to match the adjacency information
                 * with both SysId and Circuit Index
                 */
                u2MtId = OSIX_HTONS (pMDT->u2MTId);

                if ((pTravTLV->u4CktIdx == u4CktIdx)
                    && ((MEMCMP (pTravTLV->au1Value, &u2MtId, ISIS_MT_ID_LEN))
                        == 0)
                    &&
                    ((MEMCMP
                      (&(pTravTLV->au1Value[2]), au1AdjSysID,
                       ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) == 0))
                {
                    /* Only Metric will change. Copy that
                     */

                    u4WideMetric = (pMDT->u4FullMetric << 8);
                    u4WideMetric = OSIX_HTONL (u4WideMetric);
                    MEMCPY (&(pTravTLV->au1Value[9]), &u4WideMetric,
                            ISIS_IS_DEF_METRIC_LEN);
                    pNonZeroLSP->u1DirtyFlag = ISIS_MODIFIED;
                    i4RetVal = ISIS_SUCCESS;
                    UPP_PT ((ISIS_LGST,
                             "UPD <T> : Updating Metric - MT IS Reachability In Self-LSP - Metric [ %u ]"
                             ", LSP Type [ %s ]\n", u4WideMetric,
                             ISIS_GET_LSP_STR (u1LSPType)));
                    break;
                }
                else
                {
                    pTravTLV = pTravTLV->pNext;
                }
            }
            else
            {
                pTravTLV = pTravTLV->pNext;
            }
        }
        pNonZeroLSP = pNonZeroLSP->pNext;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdModMTISReachTLV ()\n"));
    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisUpdFillMDTFromIPRATLV ()
 * Description : This routine Fills pMDT from IPRA, IPV6RA TLV
 * Input (s)   : pContext     - Pointer to System Context
 *               u1TLVType    - Type of TLV.
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, 
 *               ISIS_FAILURE, 
 ******************************************************************************/
INT1
IsisUpdFillMDTFromIPRATLV (tIsisSysContext * pContext, tIsisMDT * pMDT,
                           tIsisIPRATLV * pTlv)
{
    UINT1              *pu1Tlv = pTlv->au1Value;
    UINT1               u1CtrlByte = 0;
    UINT1               u1PrefixLen = 0;

    pMDT->u1TLVType = pTlv->u1Code;
    pMDT->u1SrcProtoId = pTlv->u1Protocol;
    switch (pMDT->u1TLVType)
    {
        case ISIS_IP_INTERNAL_RA_TLV:
        case ISIS_IP_EXTERNAL_RA_TLV:
            IsisUtlGetMetric (pContext, pu1Tlv, (UINT1 *) &(pMDT->Metric));
            pu1Tlv = pu1Tlv + sizeof (tIsisMetric);
            MEMCPY (pMDT->IPAddr.au1IpAddr, pu1Tlv, ISIS_MAX_IPV4_ADDR_LEN);
            pu1Tlv += ISIS_MAX_IPV4_ADDR_LEN;
            IsisUtlComputePrefixLen (pu1Tlv, ISIS_IPV4_ADDR_LEN,
                                     &(pMDT->IPAddr.u1PrefixLen));

            break;

        case ISIS_EXT_IP_REACH_TLV:
            MEMCPY (&(pMDT->u4FullMetric), pu1Tlv, ISIS_EXT_DEF_METRIC_LEN);
            pMDT->u4FullMetric = OSIX_HTONL (pMDT->u4FullMetric);
            pu1Tlv = pu1Tlv + sizeof (tIsisMetric);
            MEMCPY (&u1CtrlByte, pu1Tlv, 1);
            u1PrefixLen = (u1CtrlByte & 0x3F);
            u1CtrlByte = (u1CtrlByte & 0xC0);
            MEMCPY (&(pMDT->u1CtrlOctet), &u1CtrlByte, 1);
            pMDT->IPAddr.u1PrefixLen = u1PrefixLen;
            pu1Tlv += 1;
            MEMCPY (pMDT->IPAddr.au1IpAddr, pu1Tlv,
                    ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen));

            break;

        case ISIS_MT_IPV6_REACH_TLV:
            pu1Tlv += ISIS_MT_ID_LEN;
            MEMCPY (&(pMDT->u4FullMetric), pu1Tlv, ISIS_EXT_DEF_METRIC_LEN);
            pMDT->u4FullMetric = OSIX_HTONL (pMDT->u4FullMetric);

            /* Intentional fall of through */
        case ISIS_IPV6_RA_TLV:
            MEMCPY (&(pMDT->Metric), pu1Tlv, sizeof (tIsisMetric));
            pu1Tlv = pu1Tlv + sizeof (tIsisMetric);
            MEMCPY (&(pMDT->u1CtrlOctet), pu1Tlv, 1);
            pu1Tlv += 1;
            MEMCPY (&(pMDT->IPAddr.u1PrefixLen), pu1Tlv, 1);
            pu1Tlv += 1;
            MEMCPY (pMDT->IPAddr.au1IpAddr, pu1Tlv,
                    ISIS_ROUNDOFF_PREFIX_LEN (pMDT->IPAddr.u1PrefixLen));

            break;
        default:
            return ISIS_FAILURE;

    }
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisHandleImportLevelChange ()
 * Description : This Routine duplicates/Move the redistributed SelfLSP IP Reachable TLVs 
 *               into another Level
 * Input (s)   : pContext     - Pointer to System Context
 *               u1SrcLevel   - This is source Level, For example if this is level-1 
 *                              this fuction will copy the L1SelfLsp IP Reachable TLVs 
 *                              to L2SelfLSP.
 *               u1Type       - Duplicate or Move To another level LSP.
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of IPRA TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

INT1
IsisHandleImportLevelChange (tIsisSysContext * pContext, UINT1 u1SrcLevel,
                             UINT1 u1Type)
{
    tIsisNSNLSP        *pSrcNSNLSP = NULL;
    tIsisLSPInfo       *pSrcNonZeroLSP = NULL;
    tIsisIPRATLV       *pTravTlv;
    tIsisIPRATLV       *pNextTlv = NULL;
    tIsisMDT           *pMDT = NULL;
    tIsisMDT           *pMDT1 = NULL;

    CTP_EE ((ISIS_LGST, "CTP <X> : Entered IsisHandleImportLevelChange ()\n"));
    if (u1SrcLevel == ISIS_IMPORT_LEVEL_2)
    {
        /*Duplicate or Move Imported IPRA TLVs from Level2 to Level1 */
        pSrcNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if ((pSrcNSNLSP == NULL))
        {
            return ISIS_FAILURE;
        }
    }
    else
    {
        pSrcNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if ((pSrcNSNLSP == NULL))
        {
            return ISIS_FAILURE;
        }
    }
    pSrcNonZeroLSP = pSrcNSNLSP->pNonZeroLSP;

    /* Scan All IP Reachable TLVs for Imported TLVs, If any Imported IPRA or 
     * IPV6RA TLV exist create a copy/delete/move the TLV information to other level */
    while (pSrcNonZeroLSP != NULL)
    {
        pTravTlv = (tIsisIPRATLV *) RBTreeGetFirst (pSrcNonZeroLSP->IPRATLV);

        while (pTravTlv != NULL)
        {
            pNextTlv = (tIsisIPRATLV *) RBTreeGetNext
                (pSrcNonZeroLSP->IPRATLV, (tRBElem *) pTravTlv, NULL);

            if (((pTravTlv->u1Code == ISIS_IP_INTERNAL_RA_TLV) ||
                 (pTravTlv->u1Code == ISIS_IPV6_RA_TLV) ||
                 (pTravTlv->u1Code == ISIS_MT_IPV6_REACH_TLV) ||
                 (pTravTlv->u1Code == ISIS_EXT_IP_REACH_TLV)) &&
                (pTravTlv->u1Protocol != 0))
            {
                /* Duplicate one level TLVs into Another Level */
                if (u1Type == ISIS_DUPLICATE_IPRA_TLV)
                {
                    pMDT =
                        (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                     sizeof (tIsisMDT));
                    if (pMDT == NULL)
                    {
                        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
                        return ISIS_FAILURE;
                    }
                    IsisUpdFillMDTFromIPRATLV (pContext, pMDT, pTravTlv);
                    if (u1SrcLevel == ISIS_LEVEL2)
                    {
                        pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                    }
                    else
                    {
                        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                    }
                    pMDT->u1Cmd = ISIS_CMD_ADD;
                    if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) ==
                        ISIS_FAILURE)
                    {
                        RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Add "
                                 "RTM Routes For IS-IS Level Change\n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                        return ISIS_FAILURE;
                    }
                    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                }
                else if (u1Type == ISIS_DELETE_IPRA_TLV)
                {
                    pMDT =
                        (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                     sizeof (tIsisMDT));
                    if (pMDT == NULL)
                    {
                        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
                        return ISIS_FAILURE;
                    }
                    IsisUpdFillMDTFromIPRATLV (pContext, pMDT, pTravTlv);
                    if (u1SrcLevel == ISIS_LEVEL1)
                    {
                        pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                    }
                    else
                    {
                        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                    }
                    pMDT->u1Cmd = ISIS_CMD_DELETE;
                    if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) ==
                        ISIS_FAILURE)
                    {
                        RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Delete "
                                 "RTM Routes For IS-IS Level Change\n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                        return ISIS_FAILURE;
                    }
                    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                }
                else
                {                /* Move to Another Level */
                    pMDT =
                        (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                     sizeof (tIsisMDT));
                    if (pMDT == NULL)
                    {
                        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
                        return ISIS_FAILURE;
                    }
                    IsisUpdFillMDTFromIPRATLV (pContext, pMDT, pTravTlv);
                    if (u1SrcLevel == ISIS_LEVEL1)
                    {
                        pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                    }
                    else
                    {
                        pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                    }
                    pMDT->u1Cmd = ISIS_CMD_ADD;
                    pMDT1 =
                        (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                     sizeof (tIsisMDT));
                    if (pMDT1 == NULL)
                    {
                        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
                        return ISIS_FAILURE;
                    }
                    MEMCPY (pMDT1, pMDT, sizeof (tIsisMDT));
                    if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) ==
                        ISIS_FAILURE)
                    {
                        RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update "
                                 "RTM Routes For IS-IS Level Change\n"));
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                        return ISIS_FAILURE;
                    }
                    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);

                    /* Delete from the source Level */
                    if (u1SrcLevel == ISIS_LEVEL1)
                    {
                        pMDT1->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                    }
                    else
                    {
                        pMDT1->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                    }
                    pMDT1->u1Cmd = ISIS_CMD_DELETE;
                    if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT1) ==
                        ISIS_FAILURE)
                    {
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
                        return ISIS_FAILURE;
                    }
                    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
                }
            }
            pTravTlv = pNextTlv;
        }
        pSrcNonZeroLSP = pSrcNonZeroLSP->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTP <X> : Exiting IsisHandleImportLevelChange ()\n"));

    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisUpdRBTreeIPRATLVCmp ()
 * Description : This routine is used to compare the TLV's during RBTree
 *               functionalities.
 * Input(s)    : pIPRATLVNode - Key1
 *               pIPRATLVIn   - key2
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : -1/1 when key elements are less or greater
 *                0   when key elements are equal
 ******************************************************************************/
INT4
IsisUpdRBTreeIPRATLVCmp (tRBElem * pIPRATLVNode, tRBElem * pIPRATLVIn)
{
    tIsisIPRATLV       *pTLVNode = pIPRATLVNode;
    tIsisIPRATLV       *pTLVIn = pIPRATLVIn;

    /* Compare the TLV code */
    if (pTLVNode->u1Code > pTLVIn->u1Code)
        return 1;
    else if (pTLVNode->u1Code < pTLVIn->u1Code)
        return -1;

    /* Compare the IP Address */
    if ((MEMCMP (pTLVNode->IPAddr.au1IpAddr,
                 pTLVIn->IPAddr.au1IpAddr, ISIS_MAX_IP_ADDR_LEN)) > 0)
        return 1;
    else if ((MEMCMP (pTLVNode->IPAddr.au1IpAddr,
                      pTLVIn->IPAddr.au1IpAddr, ISIS_MAX_IP_ADDR_LEN)) < 0)
        return -1;

    /* Compare the Prefix length */
    if (pTLVNode->IPAddr.u1PrefixLen > pTLVIn->IPAddr.u1PrefixLen)
        return 1;
    else if (pTLVNode->IPAddr.u1PrefixLen < pTLVIn->IPAddr.u1PrefixLen)
        return -1;

    return 0;
}

/*******************************************************************************
 * Function    : IsisUpdRRDRoutesRBTreeCmp ()
 * Description : This routine is used to compare the RedistributeRoutes during RBTree
 *               functionalities.
 * Input(s)    : pRRDRouteNode - Key1
 *               pRRDRouteIn   - key2
 * Output(s)   : None
 * Globals     : Not Referred or Modified              
 * Returns     : -1/1 when key elements are less or greater
 *                0   when key elements are equal
 ******************************************************************************/
INT4
IsisUpdRRDRoutesRBTreeCmp (tRBElem * pRRDRouteNode, tRBElem * pRRDRouteIn)
{
    tIsisRRDRoutes     *pRRDRoute = pRRDRouteNode;
    tIsisRRDRoutes     *pInRoute = pRRDRouteIn;

    /* Compare the Address type */
    if (pRRDRoute->IPAddr.u1AddrType > pInRoute->IPAddr.u1AddrType)
        return 1;
    else if (pRRDRoute->IPAddr.u1AddrType < pInRoute->IPAddr.u1AddrType)
        return -1;

    /* Compare the IP Address */
    if ((MEMCMP (pRRDRoute->IPAddr.au1IpAddr,
                 pInRoute->IPAddr.au1IpAddr, ISIS_MAX_IP_ADDR_LEN)) > 0)
        return 1;
    else if ((MEMCMP (pRRDRoute->IPAddr.au1IpAddr,
                      pInRoute->IPAddr.au1IpAddr, ISIS_MAX_IP_ADDR_LEN)) < 0)
        return -1;

    /* Compare the Prefix length */
    if (pRRDRoute->IPAddr.u1PrefixLen > pInRoute->IPAddr.u1PrefixLen)
        return 1;
    else if (pRRDRoute->IPAddr.u1PrefixLen < pInRoute->IPAddr.u1PrefixLen)
        return -1;

    return 0;
}

/******************************************************************************
 * Function    : IsisHandleMetricChange ()
 * Description : This Routine scans the redistributed routes in selfLSP IPRA and  
 *               change the metric value for those routes.
 * Input (s)   : pContext     - Pointer to System Context
 *               u1SrcLevel   - This is source Level, For example if this is level-1 
 *                              this fuction will copy the L1SelfLsp IP Reachable TLVs 
 *                              to L2SelfLSP.
 *               u1Type       - Duplicate or Move To another level LSP.
 * Output (s)  : None
 * Globals     : Not Referred or Modified               
 * Returns     : ISIS_SUCCESS, On Successful Addition of IPRA TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

INT1
IsisHandleMetricChange (tIsisSysContext * pContext, UINT1 u1MetricIndex,
                        UINT4 u4Metric)
{
    tIsisNSNLSP        *pSrcNSNLSP = NULL;
    tIsisLSPInfo       *pSrcNonZeroLSP = NULL;
    tIsisIPRATLV       *pTravTlv = NULL;
    tIsisIPRATLV       *pNextTlv = NULL;
    tIsisMDT           *pMDT = NULL;
    tIsisMDT           *pMDT1 = NULL;
    UINT1               u1ProtoId = 0;
    tIsisRRDRoutes     *pRRDRoute = NULL;

    pRRDRoute = (tIsisRRDRoutes *) RBTreeGetFirst (pContext->RRDRouteRBTree);

    RTP_PT ((ISIS_LGST,
             "RTM <T> : Updating Metric for Redistributed Routes - New Metric Set [ %u ]\n",
             u4Metric));
    while (pRRDRoute != NULL)
    {
        pRRDRoute->u4Metric = u4Metric;
        pRRDRoute =
            RBTreeGetNext (pContext->RRDRouteRBTree, (tRBElem *) pRRDRoute,
                           NULL);
    }

    CTP_EE ((ISIS_LGST, "CTP <X> : Entered IsisHandleMetricChange ()\n"));
    if (pContext->RRDInfo.u1RRDImportType == ISIS_IMPORT_LEVEL_2)
    {
        /*Duplicate or Move Imported IPRA TLVs from Level2 to Level1 */
        pSrcNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if ((pSrcNSNLSP == NULL))
        {
            return ISIS_FAILURE;
        }
    }
    else
    {
        pSrcNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if ((pSrcNSNLSP == NULL))
        {
            return ISIS_FAILURE;
        }
    }
    pSrcNonZeroLSP = pSrcNSNLSP->pNonZeroLSP;
    if (u1MetricIndex == ISIS_METRIC_INDEX_STATIC)
    {
        u1ProtoId = ISIS_STATIC_ID;
    }
    else if (u1MetricIndex == ISIS_METRIC_INDEX_DIRECT)
    {
        u1ProtoId = ISIS_LOCAL_ID;
    }
    else if (u1MetricIndex == ISIS_METRIC_INDEX_OSPF)
    {
        u1ProtoId = ISIS_OSPF_ID;
    }
    else if (u1MetricIndex == ISIS_METRIC_INDEX_RIP)
    {
        u1ProtoId = ISIS_RIP_ID;
    }
    else if (u1MetricIndex == ISIS_METRIC_INDEX_BGP)
    {
        u1ProtoId = ISIS_BGP_ID;
    }
    else
    {
        return ISIS_FAILURE;
    }

    /* Scan All IP Reachable TLVs for Imported TLVs, Change the metric and updateself LSP */
    while (pSrcNonZeroLSP != NULL)
    {
        pTravTlv = (tIsisIPRATLV *) RBTreeGetFirst (pSrcNonZeroLSP->IPRATLV);

        while (pTravTlv != NULL)
        {
            pNextTlv = (tIsisIPRATLV *) RBTreeGetNext
                (pSrcNonZeroLSP->IPRATLV, (tRBElem *) pTravTlv, NULL);

            if (((pTravTlv->u1Code == ISIS_IP_INTERNAL_RA_TLV) ||
                 (pTravTlv->u1Code == ISIS_IPV6_RA_TLV) ||
                 (pTravTlv->u1Code == ISIS_MT_IPV6_REACH_TLV) ||
                 (pTravTlv->u1Code == ISIS_EXT_IP_REACH_TLV)) &&
                (pTravTlv->u1Protocol == u1ProtoId))
            {
                pMDT =
                    (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                 sizeof (tIsisMDT));
                if (pMDT == NULL)
                {
                    PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
                    return ISIS_FAILURE;
                }
                IsisUpdFillMDTFromIPRATLV (pContext, pMDT, pTravTlv);
                MEMSET ((pMDT->Metric), 0, sizeof (UINT4));
                if ((pTravTlv->u1Code == ISIS_EXT_IP_REACH_TLV) ||
                    (pTravTlv->u1Code == ISIS_MT_IPV6_REACH_TLV))
                {
                    pMDT->u4FullMetric = u4Metric;
                }
                else
                {
                    pMDT->Metric[0] = (UINT1) u4Metric;    /*Default Metric only supported */
                }
                pMDT->u1IsisEnabled = pTravTlv->u1IsisEnabled;
                pMDT1 =
                    (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                 sizeof (tIsisMDT));
                if (pMDT1 == NULL)
                {
                    PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    IsisUtlFormResFailEvt (pContext, ISIS_BUF_MDTP);
                    ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                    return ISIS_FAILURE;
                }
                MEMCPY (pMDT1, pMDT, sizeof (tIsisMDT));
                if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_2)
                    == ISIS_IMPORT_LEVEL_2)
                {
                    pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                    pMDT->u1Cmd = ISIS_CMD_MODIFY;
                    if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT) ==
                        ISIS_FAILURE)
                    {
                        RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update "
                                 "RTM Route For IS-IS Metric Change\n"));
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
                        return ISIS_FAILURE;
                    }
                }
                if ((pContext->RRDInfo.u1RRDImportType & ISIS_IMPORT_LEVEL_1)
                    == ISIS_IMPORT_LEVEL_1)
                {
                    pMDT1->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                    pMDT1->u1Cmd = ISIS_CMD_MODIFY;
                    if (IsisUpdUpdateSelfLSP (pContext, 0, pMDT1) ==
                        ISIS_FAILURE)
                    {
                        RTP_PT ((ISIS_LGST, "RTM <T> : Failed To Update "
                                 "RTM Routes For IS-IS Metric Change\n"));
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                        ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
                        return ISIS_FAILURE;
                    }
                }
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT1);
            }
            pTravTlv = pNextTlv;
        }
        pSrcNonZeroLSP = pSrcNonZeroLSP->pNext;
    }

    CTP_EE ((ISIS_LGST, "CTP <X> : Exiting IsisHandleImportLevelChange ()\n"));

    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function    : IsisUpdAddDynHostNmeTLV ()
 * Description : This Routine adds IP Interface Address TLV to SelfLSP
 * Input (s)   : pContext - Pointer to System Context
 *               pMDT     - Pointer to MDT structure which contains the
 *                          IP Interface Address to be added
 * Output (s)  : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On Successful Addition of IP IF Address TLV
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PRIVATE INT4
IsisUpdAddDynHostNmeTLV (tIsisSysContext * pContext, tIsisMDT * pMDT)
{
    UINT1               u1LSPType = 0;
    UINT4               u4LSPBufSize = 0;
    tIsisNSNLSP        *pNSNLSP = NULL;
    tIsisLSPInfo      **pZeroLSP = NULL;
    INT4                i4RetVal = ISIS_FAILURE;
    tIsisTLV           *pTLV = NULL;
    tIsisTLV           *pTravTlv = NULL;
    tIsisTLV           *pHstTlv = NULL;
    UINT1               u1HostNmeTlvLen = 0;
    UINT1               u1RemBytes = 0;

    UPP_EE ((ISIS_LGST, "UPD <X> : Entered IsisUpdAddDynHostNmeTLV ()\n"));

    u1LSPType = pMDT->u1LSPType;

    if (u1LSPType == ISIS_L1_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL1NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddDynHostNmeTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pZeroLSP = &(pNSNLSP->pZeroLSP);
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL1LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;

    }
    else if (u1LSPType == ISIS_L2_NON_PSEUDO_LSP)
    {
        pNSNLSP = pContext->SelfLSP.pL2NSNLSP;
        if (pNSNLSP == NULL)
        {
            UPP_EE ((ISIS_LGST,
                     "UPD <X> : Exiting IsisUpdAddDynHostNmeTLV ()\n"));
            return (ISIS_FAILURE);
        }
        pZeroLSP = &(pNSNLSP->pZeroLSP);
        u4LSPBufSize = pContext->SysActuals.u4SysOrigL2LSPBufSize -
            sizeof (tIsisComHdr) -
            (ISIS_SYS_ID_LEN + 13) - ISIS_MAX_PASSWORD_LEN;
    }
    else
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddDynHostNmeTLV ()\n"));
        return ISIS_FAILURE;
    }
    if (pZeroLSP == NULL)
    {
        UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddDynHostNmeTLV ()\n"));
        return ISIS_FAILURE;
    }

    /* Scan through the entire TLV list and check whether the same TLV already exists */
    u1HostNmeTlvLen = (UINT1) STRLEN (pMDT->au1IsisHstNme);

    if (*pZeroLSP == NULL)
    {
        (*pZeroLSP) = (tIsisLSPInfo *) ISIS_MEM_ALLOC (ISIS_BUF_SLSP,
                                                       sizeof (tIsisLSPInfo));
        if ((*pZeroLSP) == NULL)
        {
            PANIC ((ISIS_LGST, "UPD <P> : No Memory For Zero LSP\n"));
            /* Shutdown the Instance
             */
            return ISIS_FAILURE;
        }
        (*pZeroLSP)->u1SNId = 0;
        (*pZeroLSP)->u1LSPNum = 0;
        (*pZeroLSP)->u2LSPLen = 0;
        (*pZeroLSP)->u2AuthStatus = ISIS_FALSE;
        (*pZeroLSP)->u4SeqNum = 0;
        (*pZeroLSP)->u1NumPSTLV = 0;
        (*pZeroLSP)->u1NumAATLV = 0;
        (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
        (*pZeroLSP)->pNext = NULL;
        (*pZeroLSP)->pTLV = NULL;
    }

    if ((*pZeroLSP)->pTLV != NULL)
    {
        /* Go through the entire TLV chain and verify whether TLVs with the
         * same code already exist. If they exist Check whether a TLV with
         * the same hostname as incoming one already exist. If such a TLV
         * exist, then ignore the incoming TLV.
         */

        pHstTlv = (*pZeroLSP)->pTLV;

        while (pHstTlv != NULL)
        {
            if (pHstTlv->u1Code == ISIS_DYN_HOSTNME_TLV)
            {
                if (pHstTlv->u1Len == u1HostNmeTlvLen)
                {
                    if (STRNCMP (pMDT->au1IsisHstNme,
                                 pHstTlv->au1Value, pHstTlv->u1Len) == 0)
                    {
                        UPP_EE ((ISIS_LGST,
                                 "UPD <X> : Exiting IsisUpdAddDynHostNmeTLV ()\n"));

                        return ISIS_SUCCESS;
                    }
                }
                MEMSET (pHstTlv->au1Value, 0, ISIS_TLV_LEN);

                STRNCPY (pHstTlv->au1Value, pMDT->au1IsisHstNme,
                         MEM_MAX_BYTES (u1HostNmeTlvLen, ISIS_TLV_LEN));

                pHstTlv->u1Len = u1HostNmeTlvLen;
                (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;

                UPP_EE ((ISIS_LGST,
                         "UPD <X> : Exiting IsisUpdAddDynHostNmeTLV ()\n"));

                return ISIS_SUCCESS;
            }
            else if ((pHstTlv->u1Code == ISIS_DYN_HOSTNME_TLV)
                     && (pTravTlv == NULL))
            {
                pTravTlv = pHstTlv;
            }
            else if ((pTravTlv != NULL)
                     && (pHstTlv->u1Code != ISIS_DYN_HOSTNME_TLV))
            {
                break;
            }
            else
            {
                pHstTlv = pHstTlv->pNext;
            }
        }
    }

    /* Refer to LOGIC of Self-LSP Updation explained @ the beginnig of this file
     */
    u1RemBytes = (UINT1) ISIS_GET_REM_BYTES (((*pZeroLSP)->u1NumHostNmeTLV),
                                             u1HostNmeTlvLen);

    if (u1RemBytes < u1HostNmeTlvLen)
    {
        if (((*pZeroLSP)->u2LSPLen + (u1HostNmeTlvLen + 2)) >
            (UINT2) u4LSPBufSize)
        {
            /* We cannot have more than one Zero LSP
             */

            WARNING ((ISIS_LGST,
                      "UPD <W> : Zero LSP Exceeding Configured Buffer Size [%u]\n",
                      u4LSPBufSize));
            return ISIS_FAILURE;
        }
        (*pZeroLSP)->u2LSPLen += LSP_LEN_VALUE_ADD;
    }

    if ((*pZeroLSP)->u1NumHostNmeTLV == 0)
    {
        (*pZeroLSP)->u2LSPLen += LSP_LEN_VALUE_ADD;
    }
    (*pZeroLSP)->u2LSPLen += u1HostNmeTlvLen;

    pTLV = (tIsisTLV *) ISIS_MEM_ALLOC (ISIS_BUF_TLVS, sizeof (tIsisTLV));

    if (pTLV != NULL)
    {

        pTLV->u1Code = (UINT1) ISIS_DYN_HOSTNME_TLV;
        pTLV->u1Len = u1HostNmeTlvLen;

        MEMCPY (pTLV->au1Value, pMDT->au1IsisHstNme,
                MEM_MAX_BYTES (u1HostNmeTlvLen, (sizeof (pTLV->au1Value) - 1)));

        if (pTravTlv == NULL)
        {
            /* No TLV matches the new TLV's code. Insert the record at the
             *              * Head
             *                           */

            pTLV->pNext = (*pZeroLSP)->pTLV;
            (*pZeroLSP)->pTLV = pTLV;
        }
        else
        {

            UPP_PT ((ISIS_LGST,
                     "UPD <T> : Adding Dynamic hostname TLV To Self-LSP - LSP Type [ %s ]\n",
                     ISIS_GET_LSP_STR (u1LSPType)));
            pTLV->pNext = pTravTlv->pNext;
            pTravTlv->pNext = pTLV;
        }

        (*pZeroLSP)->u1NumHostNmeTLV++;
        (*pZeroLSP)->u1DirtyFlag = ISIS_MODIFIED;
    }
    else
    {
        /* Ignore the new TLV, but generate a PANIC since SelfLSPs are no more
         * correct
         */

        PANIC ((ISIS_LGST,
                ISIS_MEM_ALLOC_FAIL " : No Memory For Host Name TLV\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        IsisUtlFormResFailEvt (pContext, ISIS_BUF_TLVS);
        i4RetVal = ISIS_FAILURE;
    }

    UPP_EE ((ISIS_LGST, "UPD <X> : Exiting IsisUpdAddDynHostNmeTLV ()\n"));
    return (i4RetVal);
}
