 /*****************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isfltlsu.c,v 1.7 2017/09/11 13:44:08 siva Exp $
 *
 * Description: This file contains the Fault Tolerance Module Lock Step Update
 *              routines.
 *
 ******************************************************************************/

#ifdef ISIS_FT_ENABLED

#include "isincl.h"
#include "isextn.h"

/*******************************************************************************
 * Function    : IsisFltrAdjLSU () 
 * Description : This routine encodes the given Adjacency entry and transmits a
 *               LSU to the Standby node for backup
 * Input(s)    : pContext - Pointer to System Context
 *               u1Cmd    - Command indicating Addition or Deletion
 *               pAdjRec  - Pointer to adjacency record that is to 
 *                          be updated on the Standby Node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful data sent to FTM
 *               ISIS_FAILURE, Otherwise
*******************************************************************************/

PUBLIC INT4
IsisFltrAdjLSU (tIsisSysContext * pContext, UINT1 u1Cmd,
                tIsisAdjEntry * pAdjRec)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));

    ISIS_FORM_IPV4_ADDR (pAdjRec->AdjNbrIpV4Addr.au1IpAddr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrAdjLSU ()\n"));
    FTP_PT ((ISIS_LGST,
             "FLT <X> : Sending Adjacency Rec. Sync for Neighbor IP[%s] IPv6 [%s] Cmd [%s]\n",
             au1IPv4Addr,
             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                           &(pAdjRec->AdjNbrIpV6Addr.au1IpAddr)),
             ISIS_GET_SYNC_CMD_STR (u1Cmd)));
    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisAdjEntry) + ISIS_INST_IDX_LEN +
                        ISIS_CKTS_IDX_LEN + ISIS_ADJN_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {

                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));
                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_ADJN, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pAdjRec->pCktRec->u4CktIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pAdjRec->u4AdjIdx);

            /* Encode the pointers in the given database as NULL
             *
             * NOTE: Pointers to the parent nodes are filled with the
             *       corresponding indices
             */

            IsisFltrBldAdjRec (pBuf, pAdjRec, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where 
             * we need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            u2MemReq = (ISIS_INST_IDX_LEN + ISIS_CKTS_IDX_LEN +
                        ISIS_ADJN_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {

                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_ADJN, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pAdjRec->pCktRec->u4CktIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pAdjRec->u4AdjIdx);

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrAdjLSU ()\n"));
            return ISIS_FAILURE;
    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Adjacency LSU Failed - Adjacency Index [%u]\n",
                 pAdjRec->u4AdjIdx));
    }
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrAdjLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrManAreaAddrLSU () 
 * Description : This routine encodes the given MAA entry and transmits a
 *               LSU to the Standby node for backup
 * Input(s)    : pContext - Pointer to System Context
 *               u1Cmd    - Command indicating Addition or Deletion
 *               pMAARec  - Pointer to Manual Area Address record 
 *                          that is to updated on the Standby Node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of MAA entry
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrManAreaAddrLSU (tIsisSysContext * pContext,
                        UINT1 u1Cmd, tIsisMAAEntry * pMAARec)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;
    CHAR                acArea[3 * ISIS_AREA_ADDR_LEN + 1] = { 0 };

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrManAreaAddrLSU ()\n"));

    ISIS_FORM_STR (ISIS_AREA_ADDR_LEN, pMAARec->ManAreaAddr.au1AreaAddr,
                   acArea);
    FTP_PT ((ISIS_LGST, "FLT <X> : Sending MAA Sync for Area [%s] Cmd [%s]\n",
             acArea, ISIS_GET_SYNC_CMD_STR (u1Cmd)));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisMAAEntry) + ISIS_INST_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_MAAD, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            IsisFltrBldMAARec (pBuf, pMAARec, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where 
             * we need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);

            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (ISIS_INST_IDX_LEN + ISIS_AREA_ADDR_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_MAAD, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_N_BYTE (pBuf, pMAARec->ManAreaAddr.au1AreaAddr,
                                 u2Offset, ISIS_AREA_ADDR_LEN);

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            break;

    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : MAA LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrManAreaAddrLSU ()\n"));

    return i4RetVal;
}

/******************************************************************************
 * Function    : IsisFltrIPRALSU () 
 * Description : This routine encodes the given IPRA entry and transmits a
 *               LSU to the Standby node for backup
 * Input(s)    : pContext - Pointer to System Context
 *               u1Cmd    - Command indicating Addition or Deletion
 *               pIPRARec - Pointer to IPRA record that is to updated on the
 *                          Standby Node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of IPRA entry
 *               ISIS_FAILURE, Otherwise
******************************************************************************/

PUBLIC INT4
IsisFltrIPRALSU (tIsisSysContext * pContext, UINT1 u1Cmd,
                 tIsisIPRAEntry * pIPRARec)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrIPRALSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisIPRAEntry) + ISIS_INST_IDX_LEN +
                        ISIS_CKTS_IDX_LEN + ISIS_IPRA_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_IPRA, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pIPRARec->u4IPRAIdx);

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, pIPRARec->u1IPRAType);

            /* Encode the pointers in the given database as NULL
             *
             * NOTE: Pointers to the parent nodes are filled with the
             *       corresponding indices
             */

            IsisFltrBldIPRARec (pBuf, pIPRARec, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we
             * need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (ISIS_INST_IDX_LEN + ISIS_CKTS_IDX_LEN +
                        ISIS_IPRA_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_IPRA, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pIPRARec->u4IPRAIdx);

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, pIPRARec->u1IPRAType);

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we 
             * need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;

    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : IPRA LSU Failed - IPRA Index [%u]\n",
                 pIPRARec->u4IPRAIdx));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrIPRALSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrSummAddrLSU () 
 * Description : This routine encodes the given SA entry and transmits a LSU to
 *               the Standby node for backup
 * Input(s)    : pContext - Pointer to System Context
 *               u1Cmd    - Command indicating Addition or Deletion
 *               pSAEntry - Pointer to buffer containing the SA record that
 *                          is to be updated on the Standby node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of SAA entry
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrSummAddrLSU (tIsisSysContext * pContext, UINT1 u1Cmd,
                     tIsisSAEntry * pSAEntry)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrSummAddrLSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisSAEntry) + ISIS_INST_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_SAAD, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            IsisFltrBldSARec (pBuf, pSAEntry, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
            {
                u2MemReq = (ISIS_INST_IDX_LEN + ISIS_SAAT_IDX_LEN +
                            ISIS_MAX_IPV4_ADDR_LEN);
            }
            else
            {

                u2MemReq = (ISIS_INST_IDX_LEN + ISIS_SAAT_IDX_LEN +
                            ISIS_MAX_IPV6_ADDR_LEN);
            }

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_SAAD, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, pSAEntry->u1AddrType);

            if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
            {
                ISIS_RED_PUT_N_BYTE (pBuf, pSAEntry->au1SummAddr, u2Offset,
                                     ISIS_MAX_IPV4_ADDR_LEN);
            }
            else
            {
                ISIS_RED_PUT_N_BYTE (pBuf, pSAEntry->au1SummAddr, u2Offset,
                                     ISIS_MAX_IPV6_ADDR_LEN);
            }
            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, pSAEntry->u1PrefixLen);

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we
             * need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;
    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : SA LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrSummAddrLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrIPIfLSU () 
 * Description : This routine encodes the given SA entry and transmits a LSU to
 *               the Standby node for backup
 * Input(s)    : pContext   - Pointer to System Context
 *               u1Cmd      - Command indicating Addition or Deletion
 *               pIPIfEntry - Pointer to IPIf record that is to updated on 
 *                            the Standby Node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of the IP I/f entry
 *               ISIS_FAILURE, Otherwise
*******************************************************************************/

PUBLIC INT4
IsisFltrIPIfLSU (tIsisSysContext * pContext, UINT1 u1Cmd,
                 tIsisIPIfAddr * pIPIfEntry)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrIPIfLSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisIPIfAddr) + ISIS_INST_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }
            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_IPIF, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            IsisFltrBldIPIfRec (pBuf, pIPIfEntry, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */
            if (pIPIfEntry->u1AddrType == ISIS_ADDR_IPV4)
            {
                u2MemReq = (ISIS_INST_IDX_LEN + ISIS_CKTS_IDX_LEN +
                            ISIS_CKTS_SIDX_LEN + ISIS_MAX_IPV4_ADDR_LEN + 1);
            }
            else
            {

                u2MemReq = (ISIS_INST_IDX_LEN + ISIS_CKTS_IDX_LEN +
                            ISIS_CKTS_SIDX_LEN + ISIS_MAX_IPV6_ADDR_LEN + 1);
            }

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }
            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_IPIF, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pIPIfEntry->u4CktIfIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pIPIfEntry->u4CktIfSubIdx);

            if (pIPIfEntry->u1AddrType == ISIS_ADDR_IPV4)
            {
                ISIS_RED_PUT_N_BYTE (pBuf, pIPIfEntry->au1IPAddr, u2Offset,
                                     ISIS_MAX_IPV4_ADDR_LEN);
            }
            else
            {
                ISIS_RED_PUT_N_BYTE (pBuf, pIPIfEntry->au1IPAddr, u2Offset,
                                     ISIS_MAX_IPV6_ADDR_LEN);

            }
            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, pIPIfEntry->u1AddrType);

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we 
             * need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;
    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : IP I/f LSU Failed - IF Index [%u]\n",
                 pIPIfEntry->u4CktIfIdx));
    }
    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrIPIfLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrAdjAALSU () 
 * Description : This routine encodes the given AdjAA entry and transmits a LSU
 *               to the Standby node for backup
 * Input(s)    : pContext  - Pointer to System Context
 *               u1Cmd     - Command indicating Addition or Deletion
 *               pAdjRec   - Pointer to adjacency Record             
 *               pAdjAARec - Pointer to adjacency area address record that 
 *                           is to updated on the Standby Node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Adjacency Area Address
 *                             record
 *               ISIS_FAILURE, Otherwise
*******************************************************************************/

PUBLIC INT4
IsisFltrAdjAALSU (tIsisSysContext * pContext, UINT1 u1Cmd,
                  tIsisAdjEntry * pAdjRec, tIsisAdjAAEntry * pAdjAARec)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrAdjAALSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisAdjAAEntry) + ISIS_INST_IDX_LEN +
                        ISIS_CKTS_IDX_LEN + ISIS_ADJN_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : CktLvl Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_ADAD, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pAdjRec->pCktRec->u4CktIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pAdjRec->u4AdjIdx);

            IsisFltrBldAdjAARec (pBuf, pAdjAARec, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (ISIS_INST_IDX_LEN + ISIS_CKTS_IDX_LEN +
                        ISIS_ADJN_IDX_LEN + ISIS_AREA_ADDR_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_ADAD, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pAdjRec->pCktRec->u4CktIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pAdjRec->u4AdjIdx);

            ISIS_RED_PUT_N_BYTE (pBuf, pAdjAARec->ISAdjAreaAddress.au1AreaAddr,
                                 u2Offset, ISIS_AREA_ADDR_LEN);

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;
    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : Adj AA LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrAdjAALSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrCktLSU () 
 * Description : This routine encodes the given Circuit entry and transmits a 
 *               LSU to the Standby node for backup
 * Input(s)    : pContext - Pointer to System Context
 *               u1Cmd    - Command indicating Addition or Deletion
 *               pCktRec  - Pointer to circuit record that is to updated on
 *                          the Standby Node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Circuit entry
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrCktLSU (tIsisSysContext * pContext, UINT1 u1Cmd,
                tIsisCktEntry * pCktRec)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrCktLSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisCktEntry) + ISIS_INST_IDX_LEN +
                        ISIS_CKTS_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }
            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_CKTS, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pCktRec->u4CktIdx);

            /* Encode the pointers in the given database as NULL
             *
             * NOTE: Pointers to the parent nodes are filled with the
             *       corresponding indices
             */

            IsisFltrBldCktRec (pBuf, pCktRec, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we 
             * need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (ISIS_INST_IDX_LEN + ISIS_CKTS_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }
            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /*        pBuf = IsisFltrFillPeerNodeHdr (u2MemReq,
               ISIS_LOCK_STEP_UPDATE, &u2Offset);

               if (pBuf == NULL)
               {
               PANIC ((ISIS_LGST, "FLT <E> : No Memory For Circuit LSU\n"));
               PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Circuit LSU\n"));
               FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrCktLSU ()\n"));
               return ISIS_FAILURE;
               } */

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_CKTS, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pCktRec->u4CktIdx);

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;
    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : Circuit LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrCktLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrCktLevelLSU () 
 * Description : This routine encodes the given Circuit Level entry and 
 *               transmits a LSU to the Standby node for backup
 * Input(s)    : pContext     - Pointer to System Context
 *               u1Cmd        - Command indicating Addition or Deletion
 *               pCktRec      - Pointer to Circuit Record
 *               pCktLevelRec - Pointer to Circuit Level record that is to 
 *                              updated on the Standby Node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Circuit Level record
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrCktLevelLSU (tIsisSysContext * pContext, UINT1 u1Cmd,
                     tIsisCktEntry * pCktRec, tIsisCktLevel * pCktLevelRec)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrCktLevelLSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisCktLevel) + ISIS_INST_IDX_LEN +
                        ISIS_CKTS_IDX_LEN + ISIS_CKTL_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_CKTL, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pCktRec->u4CktIdx);

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, pCktLevelRec->u1CktLvlIdx);

            /* Encode the pointers in the given database as NULL
             *
             * NOTE: Pointers to the parent nodes are filled with the
             *       corresponding indices
             */

            IsisFltrBldCktLevelRec (pBuf, pCktLevelRec, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (ISIS_INST_IDX_LEN + ISIS_CKTS_IDX_LEN +
                        ISIS_CKTL_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_CKTL, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pCktRec->u4CktIdx);

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, pCktLevelRec->u1CktLvlIdx);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled u2Offset is decremented by the message 
             * header now added such as the Command and the Type of Data, etc
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we 
             * need to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;
    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST,
                 "FLT <E> : Circuit Level LSU Failed - Circuit [%u]\n",
                 pCktRec->u4CktIdx));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrCktLevelLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrSysContextLSU () 
 * Description : This routine encodes the given Context and transmits a LSU to
 *               the Standby node for backup
 * Input(s)    : pContext - Pointer to System Context which is to be updated
 *               u1Cmd    - Command indicating Addition or Deletion
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of System Context
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrSysContextLSU (tIsisSysContext * pContext, UINT1 u1Cmd)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrSysContextLSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisSysContext) + ISIS_INST_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_CTXT, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            /* Encode the pointers in the given database as NULL
             *
             * NOTE: Pointers to the parent nodes are filled with the
             *       corresponding indices
             */

            IsisFltrBldSysContextRec (pBuf, pContext, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (ISIS_INST_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_CTXT, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;

    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : Context LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrSysContextLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrSysConfLSU () 
 * Description : This routine encodes the given System Configs entry and 
 *               transmits a LSU to the Standby node for backup
 * Input(s)    : pContext - Pointer to System Context
 *               u1Cmd    - Command indicating Addition or Deletion
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation System Configs information
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrSysConfLSU (tIsisSysContext * pContext, UINT1 u1Cmd)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrSysConfLSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisSysConfigs) + ISIS_INST_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_CONF, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            IsisFltrBldSysConfRec (pBuf, pContext, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;

    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : System Configs LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrSysConfLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrSysActLSU () 
 * Description : This routine encodes the given System Actuals entry and 
 *               transmits a LSU to the Standby node for backup
 * Input(s)    : pContext - Pointer to System Context
 *               u1Cmd    - Command indicating Addition or Deletion
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of System Actuals entry
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrSysActLSU (tIsisSysContext * pContext, UINT1 u1Cmd)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrSysActLSU ()\n"));

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (sizeof (tIsisSysActuals) + ISIS_INST_IDX_LEN);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_ACTS, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            IsisFltrBldSysActualsRec (pBuf, pContext, &u2Offset);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;
    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : System Actuals LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrSysActLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrLspLSU () 
 * Description : This routine encodes the given LSP and transmits a LSU to the 
 *               Standby node for backup
 * Input(s)    : pContext - Pointer to System Context
 *               u1Cmd    - Command indicating Addition or Deletion
 *               pLspRec  - Pointer to LSP record that is to updated on the 
 *                          Standby Node
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of LSP
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrLspLSU (tIsisSysContext * pContext, UINT1 u1Cmd,
                tIsisLSPEntry * pLspRec)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1LspType = 0;
    UINT1               u1SNId = 0;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;
    UINT2               u2LspLen = 0;
    UINT2               u2RLT = 0;
    UINT4               u4SeqNum = 0;
    UINT4               u4CktIdx = 0;
    tIsisSNLSP         *pSNLSP = NULL;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrLspLSU ()\n"));

    u1LspType = (ISIS_EXTRACT_PDU_TYPE (pLspRec->pu1LSP) == ISIS_L1LSP_PDU)
        ? ISIS_LEVEL1 : ISIS_LEVEL2;

    switch (u1Cmd)
    {
        case ISIS_CMD_ADD:

            /* Circuit Index needs to be passed to the Standby Node along with
             * the PseudoNode LSPs generated by local IS. In the Standby Node
             * on reception of PseudoNode LSPs of local IS, while constructing`
             * Self LSP Buffers, Circuit Record is retrieved using the passed
             * Circuit Index and linked to PseudoNode LSP Buffer
             */

            i4RetVal = MEMCMP ((pLspRec->pu1LSP + ISIS_OFFSET_LSPID),
                               pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);
            if (i4RetVal == 0)
            {
                u1SNId = *(pLspRec->pu1LSP + ISIS_OFFSET_PNODEID);
                if (u1SNId != 0)
                {
                    pSNLSP = (u1LspType == ISIS_LEVEL1) ?
                        pContext->SelfLSP.pL1SNLSP : pContext->SelfLSP.pL2SNLSP;
                    while (pSNLSP != NULL)
                    {
                        if (u1SNId == pSNLSP->pZeroLSP->u1SNId)
                        {
                            u4CktIdx = pSNLSP->pCktEntry->u4CktIdx;
                            break;
                        }
                        pSNLSP = pSNLSP->pNext;
                    }
                }
            }

            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pLspRec->pu1LSP, u2LspLen);

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (UINT2)
                (ISIS_INST_IDX_LEN + ISIS_CKTS_IDX_LEN + u2LspLen + 3);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_LSPS, 1, &u2Offset);

            /* Copy the Indices required to identify the entry being encoded
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, u4CktIdx);

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, u1LspType);

            ISIS_RED_PUT_2_BYTE (pBuf, u2Offset, u2LspLen);

            ISIS_RED_PUT_N_BYTE (pBuf, pLspRec->pu1LSP, u2Offset, u2LspLen);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_DELETE:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             */

            u2MemReq = (ISIS_INST_IDX_LEN + ISIS_LSPID_LEN + 1);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_LSPS, 1, &u2Offset);

            /* LSP record should be preceeded by the SysInstIdx, LSP Type i.e 
             * L1LSP or L2LSP and LSP ID for deletion and modification
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, u1LspType);

            ISIS_RED_PUT_N_BYTE (pBuf,
                                 (pLspRec->pu1LSP + ISIS_OFFSET_LSPID),
                                 u2Offset, ISIS_LSPID_LEN);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        case ISIS_CMD_MODIFY:

            /* calculate the Memory required for this Database as 'u2MemReq'.
             * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
             * the Message Buffer and fills in the Peer Node Header Information
             * as required.
             *
             * Remaining Life Time and Sequence Number are sent as the LSU
             * information when the LSP is modified.
             * So the memory required is 
             * 2 bytes (Remaining Life Time) + 
             * 4 bytes (Sequence Number) +
             * 1 byte  (LSP Level) +
             */

            u2MemReq = (ISIS_INST_IDX_LEN + ISIS_LSPID_LEN + 7);

            pBuf =
                IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                              (UINT2) sizeof (tIsisFLTHdr) +
                                              u2MemReq));

            if (pBuf == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                FTP_EE ((ISIS_LGST,
                         "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

                return ISIS_FAILURE;
            }

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
            /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
            u2Offset = (UINT2) (u2Offset + 2);

            /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
             * described in the beginning of 'isfltutl.c' file.
             * For LSUs the Entry Count is always 1
             */

            IsisFltrFillHdr (pBuf, u1Cmd, ISIS_DB_LSPS, 1, &u2Offset);

            /* We fill only the indices for sending a LSU for deleting entries
             * on the Standby node
             */

            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

            ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, u1LspType);

            ISIS_RED_PUT_N_BYTE (pBuf,
                                 (pLspRec->pu1LSP + ISIS_OFFSET_LSPID),
                                 u2Offset, ISIS_LSPID_LEN);

            /* Only the Remaining Life Time and Sequence Number can change 
             * in the LSP and hence these two are updated in the Standby Node
             */

            ISIS_EXTRACT_RLT_FROM_LSP (pLspRec->pu1LSP, u2RLT);
            ISIS_RED_PUT_2_BYTE (pBuf, u2Offset, u2RLT);

            ISIS_EXTRACT_SEQNUM_FROM_LSP (pLspRec->pu1LSP, u4SeqNum);
            ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, u4SeqNum);

            /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
             * to fill the value of the Length (i.e., u2Offset) 
             * of the buffer filled. 
             *
             * NOTE: Length of the buffer does not include the Headers and other
             *       fields in the FLT header viz. Command(1), Database Type(1) 
             *       and the Length (2) itself
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                                  (u2Offset - ISIS_MSG_LENGTH_POS));

            /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need
             * to fill the value of the total Length (i.e., u2Offset) 
             * of the buffer including the PeerNodeMsgHdr 
             */

            ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);
            break;

        default:

            FTP_PT ((ISIS_LGST, "FLT <E> : Invalid Command [ %u ]\n", u1Cmd));
            return ISIS_FAILURE;
    }

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : LSP LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrLspLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrProtSuppLSU () 
 * Description : This routine encodes the given PS entry and transmits a LSU to
 *               the Standby node for backup
 * Input(s)    : pContext     - Pointer to System Context
 *               u1ProtSupp   - The Supported Protocol
 *               u1ExistState - The Exist State of the Protocol Support Entry
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of the PS information
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrProtSuppLSU (tIsisSysContext * pContext, UINT1 u1ProtSupp,
                     UINT1 u1ExistState)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrProtSuppLSU ()\n"));

    /* calculate the Memory required for this Database as 'u2MemReq'.
     * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    u2MemReq = (ISIS_INST_IDX_LEN + 2);

    pBuf =
        IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                      (UINT2) sizeof (tIsisFLTHdr) + u2MemReq));

    if (pBuf == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

        return ISIS_FAILURE;
    }
    ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     * For LSUs the Entry Count is always 1
     */

    IsisFltrFillHdr (pBuf, ISIS_CMD_MODIFY, ISIS_DB_PRSU, 1, &u2Offset);

    /* Copy the Indices required to identify the entry being encoded
     */

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

    ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, u1ProtSupp);

    ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, u1ExistState);

    /* ISIS_FT_LENGTH_POS is the position in the pBuf where we need 
     * to fill the value of the Length (i.e., u2Offset) 
     * of the buffer filled. 
     *
     * NOTE: Length of the buffer does not include the Headers and other
     *       fields in the FLT header viz. Command(1), Database Type(1) 
     *       and the Length (2) itself
     */

    ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));

    /* ISIS_PEERHDR_LENGTH_POS is the position in the pBuf where we need 
     * to fill the value of the total Length (i.e., u2Offset) 
     * of the buffer including the PeerNodeMsgHdr 
     */

    ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    if ((i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset)) != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : PS LSU Failed - State [%u]\n",
                 u1ExistState));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrProtSuppLSU ()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrMemCfgLSU() 
 * Description : This routine encodes the given MemConfigs and transmits a 
 *               LSU to the Standby node for backup
 * Input(s)    : None
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Circuit entry
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrMemCfgLSU ()
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrMemCfgLSU ()\n"));

    /* calculate the Memory required for this Database as 'u2MemReq'.
     * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    u2MemReq = (sizeof (tIsisMemConfigs));

    pBuf =
        IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                      (UINT2) sizeof (tIsisFLTHdr) + u2MemReq));

    if (pBuf == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

        return ISIS_FAILURE;
    }
    ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     * For LSUs the Entry Count is always 1
     */

    IsisFltrFillHdr (pBuf, ISIS_CMD_ADD, ISIS_DB_MCFG, 1, &u2Offset);

    /* No Indices required to identify this entry being encoded
     */

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxInsts);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4Factor);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxLSP);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxCkts);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxAreaAddr);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxMAA);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxAdjs);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxRoutes);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxIPRA);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxSA);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxIPIF);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxEvents);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxLspTxQLen);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, gIsisMemConfigs.u4MaxMsgBuffs);

    ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));

    ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset);
    if (i4RetVal != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : MemConfigs LSU Failed\n"));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrMemCfgLSU()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrISStatLSU() 
 * Description : This routine updates the status of IS to standby node
 * Input(s)    : u1Status - The Staus of the IS
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Circuit entry
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrISStatLSU (UINT1 u1Status)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 0;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrISStatLSU ()\n"));

    /* calculate the Memory required for this Database as 'u2MemReq'.
     * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    pBuf =
        IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                      (UINT2) sizeof (tIsisFLTHdr) + u2MemReq));

    if (pBuf == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

        return ISIS_FAILURE;
    }
    ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     * For LSUs the Entry Count is always 1
     */

    IsisFltrFillHdr (pBuf, u1Status, ISIS_IS_STAT, 1, &u2Offset);

    ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));

    ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset);
    if (i4RetVal != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : IS Status LSU Failed - Status [%u]\n",
                 u1Status));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrISStatLSU()\n"));

    return i4RetVal;
}

/*******************************************************************************
 * Function    : IsisFltrSchSPFLSU () 
 * Description : This routine sends a LSU to standby for computing SPF
 * Input(s)    : u1Status - The Staus of the IS
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : ISIS_SUCCESS, On successful updation of Circuit entry
 *               ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisFltrSchSPFLSU (tIsisSysContext * pContext, UINT1 u1Level)
{
    INT4                i4RetVal = ISIS_SUCCESS;
    tRmMsg             *pBuf = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MemReq = 4;

    FTP_EE ((ISIS_LGST, "FLT <X> : Entered IsisFltrSchSPFLSU ()\n"));

    /* calculate the Memory required for this Database as 'u2MemReq'.
     * The Function 'IsisFltrFillPeerNodeHdr ()' allocates memory for
     * the Message Buffer and fills in the Peer Node Header Information
     * as required.
     */

    pBuf =
        IsisRmAllocForRmMsg ((UINT2) (ISIS_MSG_LENGTH_POS +
                                      (UINT2) sizeof (tIsisFLTHdr) + u2MemReq));

    if (pBuf == NULL)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MAA Bulk Update\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrFormMAABulkData ()\n"));

        return ISIS_FAILURE;
    }

    ISIS_RED_PUT_1_BYTE (pBuf, u2Offset, LOCK_STEP_UPDATE_MSG);
    /*Increment offset by 2 bytes for filling up the 2 bytes MsgLength */
    u2Offset = (UINT2) (u2Offset + 2);

    /* The function 'IsisFltrFillHdr ()' fills the ISIS-FLTR Header as
     * described in the beginning of 'isfltutl.c' file.
     * For LSUs the Entry Count is always 1. 
     * NOTE: For this LSP, the command does not matter.
     */

    IsisFltrFillHdr (pBuf, u1Level, ISIS_SD_DECN, 1, &u2Offset);

    ISIS_RED_PUT_4_BYTE (pBuf, u2Offset, pContext->u4SysInstIdx);

    ISIS_RED_COPY_2_BYTE (pBuf, ISIS_FT_LENGTH_POS,
                          (u2Offset - ISIS_MSG_LENGTH_POS));

    ISIS_RED_COPY_2_BYTE (pBuf, ISIS_PEERHDR_LENGTH_POS, u2Offset);

    i4RetVal = IsisRmSendMsgToRm (pBuf, u2Offset);

    if (i4RetVal != ISIS_SUCCESS)
    {
        FTP_PT ((ISIS_LGST, "FLT <E> : Sch SPF LSU Failed - Level [%u]\n",
                 u1Level));
    }

    FTP_EE ((ISIS_LGST, "FLT <X> : Exiting IsisFltrSchSPFLSU()\n"));

    UNUSED_PARAM (pContext);
    UNUSED_PARAM (u1Level);
    return i4RetVal;
}

#endif
