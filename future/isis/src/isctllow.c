/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: isctllow.c,v 1.32 2017/09/11 13:44:08 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

#include "iscli.h"
#include  "fssnmp.h"
#include "isincl.h"
#include "isextn.h"
/* LOW LEVEL Routines for Table : IsisSysTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisSysTable
 Input       :  The Indices
                IsisSysInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisSysTable (INT4 i4IsisSysInstance)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisSysTable ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid SysInstance Index. "
                 "The Index is %d\n", i4IsisSysInstance));
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisSysTable ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisSysTable
 Input       :  The Indices
                IsisSysInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisSysTable (INT4 *pi4IsisSysInstance)
{
    if (UtilIsisGetFirstCxtId ((UINT4 *) pi4IsisSysInstance) == ISIS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisSysTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisSysTable (INT4 i4IsisSysInstance,
                             INT4 *pi4NextIsisSysInstance)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT4               u4CurrIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4NextIdx = ISIS_LL_MAX_IDX;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetNextIndexIsisSysTable ()\n"));

    pContext = ISIS_GET_CONTEXT ();

    if (u4CurrIdx < ISIS_LL_MAX_IDX)
    {
        while (pContext != NULL)
        {
            if ((pContext->u4SysInstIdx > u4CurrIdx)
                && (pContext->u4SysInstIdx < u4NextIdx))
            {
                u4NextIdx = pContext->u4SysInstIdx;
                *pi4NextIsisSysInstance = (INT4) u4NextIdx;
                i1ErrCode = SNMP_SUCCESS;
            }
            pContext = pContext->pNext;
        }
    }

    NMP_PT ((ISIS_LGST, "NMP <T> : Next SysInstance Index is "
             " %d\n", *pi4NextIsisSysInstance));

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetNextIndexIsisSysTable ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhDepv2IsisSysTable
 Input       :  The Indices
                IsisSysInstance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IsisSysTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisSysVersion
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysVersion (INT4 i4IsisSysInstance,
                      tSNMP_OCTET_STRING_TYPE * pRetValIsisSysVersion)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT1                ai1Str[10];
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Len = 0;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysVersion ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        SPRINTF ((CHAR *) ai1Str, "%d", pContext->SysActuals.u1SysVer);
        u1Len = (UINT1) STRLEN (ai1Str);
        MEMCPY (pRetValIsisSysVersion->pu1_OctetList, ai1Str, u1Len);
        pRetValIsisSysVersion->i4_Length = u1Len;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for the Index %u\n",
                 u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysVersion ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysType
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysType (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisSysType)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysType ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysType = (INT4) pContext->SysConfigs.u1SysType;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for the Index %u\n",
                 u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysType ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysID
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysID (INT4 i4IsisSysInstance,
                 tSNMP_OCTET_STRING_TYPE * pRetValIsisSysID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    UINT1               au1BlankSpace[ISIS_SYS_ID_LEN];

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysID ()\n"));

    MEMSET (au1BlankSpace, 0, ISIS_SYS_ID_LEN);

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (MEMCMP (pContext->SysConfigs.au1SysID, au1BlankSpace,
                    ISIS_SYS_ID_LEN) != 0)
        {

            pRetValIsisSysID->i4_Length = ISIS_SYS_ID_LEN;
            MEMCPY (pRetValIsisSysID->pu1_OctetList,
                    pContext->SysConfigs.au1SysID, ISIS_SYS_ID_LEN);
        }
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for the Index %u\n",
                 u4InstIdx));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysID ()\n"));

    return (i1ErrCode);
}

/****************************************************************************
 Function    :  nmhGetIsisSysMaxPathSplits
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysMaxPathSplits
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysMaxPathSplits (INT4 i4IsisSysInstance,
                            INT4 *pi4RetValIsisSysMaxPathSplits)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysMaxPathSplits ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysMaxPathSplits = (INT4) pContext->SysConfigs.u1SysMPS;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));

        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysMaxPathSplits ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetIsisSysMaxLSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysMaxLSPGenInt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysMaxLSPGenInt (INT4 i4IsisSysInstance,
                           INT4 *pi4RetValIsisSysMaxLSPGenInt)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysMaxLSPGenInt ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysMaxLSPGenInt =
            (INT4) pContext->SysActuals.u2SysMaxLSPGenInt;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does "
                 "not exist for the Index, %d\n", i4IsisSysInstance));

        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysMaxLSPGenInt ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetIsisSysOrigL1LSPBuffSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysOrigL1LSPBuffSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysOrigL1LSPBuffSize (INT4 i4IsisSysInstance,
                                INT4 *pi4RetValIsisSysOrigL1LSPBuffSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetIsisSysOrigL1LSPBuffSize ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysOrigL1LSPBuffSize =
            (INT4) pContext->SysConfigs.u4SysOrigL1LSPBufSize;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for the Index, "
                 "%d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSysOrigL1LSPBuffSize ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetIsisSysMaxAreaAddresses
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysMaxAreaAddresses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysMaxAreaAddresses (INT4 i4IsisSysInstance,
                               INT4 *pi4RetValIsisSysMaxAreaAddresses)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetIsisSysMaxAreaAddresses ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysMaxAreaAddresses =
            (INT4) pContext->SysConfigs.u1SysMaxAA;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for the Index, "
                 "%d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSysMaxAreaAddresses ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysMinL1LSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysMinL1LSPGenInt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysMinL1LSPGenInt (INT4 i4IsisSysInstance,
                             INT4 *pi4RetValIsisSysMinL1LSPGenInt)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysMinL1LSPGenInt ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysMinL1LSPGenInt =
            (INT4) pContext->SysActuals.u2SysMinL1LSPGenInt;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysMinL1LSPGenInt ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetIsisSysMinL2LSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysMinL2LSPGenInt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysMinL2LSPGenInt (INT4 i4IsisSysInstance,
                             INT4 *pi4RetValIsisSysMinL2LSPGenInt)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysMinL2LSPGenInt ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysMinL2LSPGenInt =
            (INT4) pContext->SysActuals.u2SysMinL2LSPGenInt;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysMinL2LSPGenInt ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetIsisSysPollESHelloRate
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysPollESHelloRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysPollESHelloRate (INT4 i4IsisSysInstance,
                              INT4 *pi4RetValIsisSysPollESHelloRate)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysPollESHelloRate ()\n"));

    NMP_PT ((ISIS_LGST, "NMP <E> : This version of Future ISIS does not "
             " Support ES Hello Rate \n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysPollESHelloRate =
            (INT4) pContext->SysActuals.u4ESHelloRate;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysPollESHelloRate ()\n"));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetIsisSysWaitTime
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysWaitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysWaitTime (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisSysWaitTime)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysWaitTime ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysWaitTime = (INT4) pContext->SysActuals.u2SysWaitTime;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysWaitTime ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetIsisSysAdminState
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysAdminState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysAdminState (INT4 i4IsisSysInstance,
                         INT4 *pi4RetValIsisSysAdminState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysAdminState ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysAdminState =
            (INT4) pContext->SysActuals.u1SysAdminState;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for the "
                 "Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysAdminState ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysL1State
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysL1State
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysL1State (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisSysL1State)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysL1State ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysL1State = (INT4) pContext->SysActuals.u1SysL1State;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for the "
                 "Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysL1State ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysOrigL2LSPBuffSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysOrigL2LSPBuffSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysOrigL2LSPBuffSize (INT4 i4IsisSysInstance,
                                INT4 *pi4RetValIsisSysOrigL2LSPBuffSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetIsisSysOrigL2LSPBuffSize ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysOrigL2LSPBuffSize =
            (INT4) pContext->SysConfigs.u4SysOrigL2LSPBufSize;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSysOrigL2LSPBuffSize ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysL2State
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysL2State
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysL2State (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisSysL2State)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysL2State ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysL2State = (INT4) pContext->SysActuals.u1SysL2State;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysL2State ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysLogAdjacencyChanges
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysLogAdjacencyChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysLogAdjacencyChanges (INT4 i4IsisSysInstance,
                                  INT4 *pi4RetValIsisSysLogAdjacencyChanges)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetIsisSysLogAdjacencyChanges ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysLogAdjacencyChanges =
            (INT4) pContext->SysActuals.bSysLogAdjChanges;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSysLogAdjacencyChanges ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysMaxAreaCheck
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysMaxAreaCheck
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysMaxAreaCheck (INT4 i4IsisSysInstance,
                           INT4 *pi4RetValIsisSysMaxAreaCheck)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysMaxAreaCheck ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysMaxAreaCheck =
            (INT4) pContext->SysActuals.bSysMaxAACheck;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysMaxAreaCheck ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysNextCircIndex
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysNextCircIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysNextCircIndex (INT4 i4IsisSysInstance,
                            INT4 *pi4RetValIsisSysNextCircIndex)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysNextCircIndex ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysNextCircIndex =
            (INT4) (pContext->CktTable.u4NextCktIndex);

        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));

        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysNextCircIndex ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysExistState
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysExistState (INT4 i4IsisSysInstance,
                         INT4 *pi4RetValIsisSysExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysExistState ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysExistState =
            (INT4) pContext->SysActuals.u1SysExistState;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysExistState ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysL2toL1Leaking
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysL2toL1Leaking
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysL2toL1Leaking (INT4 i4IsisSysInstance,
                            INT4 *pi4RetValIsisSysL2toL1Leaking)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysL2ToL1Leaking ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysL2toL1Leaking =
            (INT4) pContext->SysActuals.bSysL2ToL1Leak;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysL2ToL1Leaking ()\n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysSetOverload
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysSetOverload
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysSetOverload (INT4 i4IsisSysInstance,
                          INT4 *pi4RetValIsisSysSetOverload)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysSetOverload ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysSetOverload = (INT4) pContext->SysActuals.u1SysSetOL;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysSetOverload ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysL1MetricStyle
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysL1MetricStyle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysL1MetricStyle (INT4 i4IsisSysInstance,
                            INT4 *pi4RetValIsisSysL1MetricStyle)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysL1MetricStyle ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysL1MetricStyle = (INT4)
            pContext->SysConfigs.u1SysL1MetricStyle;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysL1MetricStyle ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysL1SPFConsiders
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysL1SPFConsiders
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysL1SPFConsiders (INT4 i4IsisSysInstance,
                             INT4 *pi4RetValIsisSysL1SPFConsiders)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysL1SPFConsiders ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysL1SPFConsiders = (INT4)
            pContext->SysActuals.u1SysL1SPFConsiders;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysL1SPFConsiders ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysL2MetricStyle
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysL2MetricStyle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysL2MetricStyle (INT4 i4IsisSysInstance,
                            INT4 *pi4RetValIsisSysL2MetricStyle)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysL2MetricStyle ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysL2MetricStyle = (INT4)
            pContext->SysConfigs.u1SysL2MetricStyle;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysL2MetricStyle ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysL2SPFConsiders
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysL2SPFConsiders
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysL2SPFConsiders (INT4 i4IsisSysInstance,
                             INT4 *pi4RetValIsisSysL2SPFConsiders)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysL2SPFConsiders ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysL2SPFConsiders = (INT4)
            pContext->SysActuals.u1SysL2SPFConsiders;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysL2SPFConsiders ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysTEEnabled
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysTEEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysTEEnabled (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisSysTEEnabled)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysTEEnabled ()\n"));

    NMP_EE ((ISIS_LGST, "NMP <X> : TE Not Supported \n"));

    UNUSED_PARAM (i4IsisSysInstance);
    *pi4RetValIsisSysTEEnabled = 3;

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysTEEnabled ()\n"));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIsisSysMaxAge
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysMaxAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysMaxAge (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisSysMaxAge)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysMaxAge ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysMaxAge = (INT4) pContext->SysActuals.u4SysMaxAge;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysMaxAge ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysReceiveLSPBufferSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSysReceiveLSPBufferSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysReceiveLSPBufferSize (INT4 i4IsisSysInstance,
                                   INT4 *pi4RetValIsisSysReceiveLSPBufferSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : "
             "Entered nmhGetIsisSysReceiveLSPBufferSize ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSysReceiveLSPBufferSize =
            (INT4) pContext->SysActuals.u4SysRxLSPBufSize;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSysReceiveLSPBufferSize ()\n"));

    return i1ErrCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIsisSysType
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysType (INT4 i4IsisSysInstance, INT4 i4SetValIsisSysType)
{
    INT4                i4ExistState = 0;
    UINT4               u4Error = 0;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysType ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif

    if (IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext) !=
        ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        return SNMP_FAILURE;
    }

    if (pContext->SysConfigs.u1SysType == i4SetValIsisSysType)
    {
        return SNMP_SUCCESS;
    }

    if (nmhGetIsisSysExistState (i4IsisSysInstance, &i4ExistState) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4ExistState == ISIS_ACTIVE)
    {
        /* Made the Exist state down if its active */
        if (nmhTestv2IsisSysExistState
            (&u4Error, i4IsisSysInstance, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetIsisSysExistState (i4IsisSysInstance, NOT_IN_SERVICE) !=
            SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
    }
    pContext->SysConfigs.u1SysType = (UINT1) i4SetValIsisSysType;
    pContext->u1QFlag |= ISIS_SYSTYPE_FLAG;

    if ((pContext->u1QFlag == ISIS_SYSALLSET_FLAG)
        && (pContext->SysActuals.u1SysExistState != ISIS_ACTIVE))
    {
        pContext->SysActuals.u1SysExistState = ISIS_NOT_IN_SER;
    }
    if (i4ExistState == ISIS_ACTIVE)
    {
        /* Move the admin state to Active only if it made down previously */
        if (nmhTestv2IsisSysExistState
            (&u4Error, i4IsisSysInstance, ISIS_ACTIVE) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (nmhSetIsisSysExistState (i4IsisSysInstance, ISIS_ACTIVE) !=
            SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysType ()\n"));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIsisSysID
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysID (INT4 i4IsisSysInstance,
                 tSNMP_OCTET_STRING_TYPE * pSetValIsisSysID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    UINT1               au1TempSysID[ISIS_SYS_ID_LEN];
    tIsisMsg            IsisMsg;
    tIsisQBuf          *pBuf = NULL;
    UINT4               u4Status = 0;
    UINT4               u4Size = 0;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysID ()\n"));
    MEMSET (au1TempSysID, 0, ISIS_SYS_ID_LEN);

#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        if ((MEMCMP (pContext->SysConfigs.au1SysID,
                     pSetValIsisSysID->pu1_OctetList,
                     pSetValIsisSysID->i4_Length) != 0)
            && (pContext->u1IsisDynHostNmeSupport) == ISIS_DYNHOSTNME_ENABLE)
        {
            /*5301 - Event */
            u4Size = (sizeof (u4Status) + ISIS_SYS_ID_LEN);
            u4Status = ISIS_CMD_MODIFY;
            pBuf = (tIsisQBuf *) ISIS_MEM_ALLOC (ISIS_BUF_CHNS, u4Size);
            if (pBuf != NULL)
            {
    /*** copy status to offset 0 ***/
                ISIS_COPY_TO_CHAIN_BUF (pBuf, (UINT1 *) &u4Status, 0,
                                        sizeof (u4Status));
        /*** copy status to offset 4 ***/
                ISIS_COPY_TO_CHAIN_BUF (pBuf, pContext->SysConfigs.au1SysID,
                                        sizeof (u4Status), ISIS_SYS_ID_LEN);

                IsisMsg.u4CxtOrIfindex = pContext->u4SysInstIdx;
                IsisMsg.u4Length = u4Size;
                IsisMsg.pu1Msg = (UINT1 *) pBuf;
                IsisMsg.u1MsgType = ISIS_EVT_HOST_NME_SUPPORT;

                if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                                        gIsisCtrlQId,
                                        ISIS_SYS_INT_EVT) == ISIS_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    NMP_EE ((ISIS_LGST,
                             "NMP <X> : Failed to post event for dynamic hostname  \n"
                             "NMP <X> : Exiting nmhSetFsIsisExtSysDynHostNameSupport()\n"));
                }
            }
        }
        MEMCPY (pContext->SysConfigs.au1SysID, pSetValIsisSysID->pu1_OctetList,
                pSetValIsisSysID->i4_Length);
        if (MEMCMP (pContext->SysConfigs.au1SysID,
                    au1TempSysID, pSetValIsisSysID->i4_Length) == 0)
        {
            MEMCPY (pContext->SysActuals.au1SysID,
                    pSetValIsisSysID->pu1_OctetList,
                    pSetValIsisSysID->i4_Length);

        }
        pContext->u1QFlag |= ISIS_SYSID_FLAG;
        i1ErrCode = SNMP_SUCCESS;

        if ((pContext->u1QFlag == ISIS_SYSALLSET_FLAG)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
        {
            pContext->SysActuals.u1SysExistState = ISIS_NOT_IN_SER;
        }

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysID ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysMaxPathSplits
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysMaxPathSplits
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysMaxPathSplits (INT4 i4IsisSysInstance,
                            INT4 i4SetValIsisSysMaxPathSplits)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysMAxPathSplits ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysConfigs.u1SysMPS = (UINT1) i4SetValIsisSysMaxPathSplits;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysMaxPathSplits ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysMaxLSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysMaxLSPGenInt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysMaxLSPGenInt (INT4 i4IsisSysInstance,
                           INT4 i4SetValIsisSysMaxLSPGenInt)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysMaxLSPGenInt ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u2SysMaxLSPGenInt =
            (UINT2) i4SetValIsisSysMaxLSPGenInt;
        i1ErrCode = SNMP_SUCCESS;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysMaxLSPGenInt ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysOrigL1LSPBuffSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysOrigL1LSPBuffSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysOrigL1LSPBuffSize (INT4 i4IsisSysInstance,
                                INT4 i4SetValIsisSysOrigL1LSPBuffSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetIsisSysOrigL1LSPBuffSize ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysConfigs.u4SysOrigL1LSPBufSize =
            (UINT4) i4SetValIsisSysOrigL1LSPBuffSize;
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSysOrigL1LSPBuffSize ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysMaxAreaAddresses
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysMaxAreaAddresses
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysMaxAreaAddresses (INT4 i4IsisSysInstance,
                               INT4 i4SetValIsisSysMaxAreaAddresses)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetIsisSysMaxAreaAddresses ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysConfigs.u1SysMaxAA =
            (UINT1) i4SetValIsisSysMaxAreaAddresses;
        pContext->u1QFlag |= ISIS_SYSMAA_FLAG;
        i1ErrCode = SNMP_SUCCESS;

        if ((pContext->u1QFlag == ISIS_SYSALLSET_FLAG)
            && (pContext->SysActuals.u1SysExistState == ISIS_NOT_READY))
        {
            pContext->SysActuals.u1SysExistState = ISIS_NOT_IN_SER;
        }

    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSysMaxAreaAddresses ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysMinL1LSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysMinL1LSPGenInt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysMinL1LSPGenInt (INT4 i4IsisSysInstance,
                             INT4 i4SetValIsisSysMinL1LSPGenInt)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysMinL1LSPGenInt ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u2SysMinL1LSPGenInt =
            (UINT2) i4SetValIsisSysMinL1LSPGenInt;
        i1ErrCode = SNMP_SUCCESS;

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysMinL1LSPGenInt ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysMinL2LSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysMinL2LSPGenInt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysMinL2LSPGenInt (INT4 i4IsisSysInstance,
                             INT4 i4SetValIsisSysMinL2LSPGenInt)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysMinL2LSPGenInt ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;

    }
    else
    {
        pContext->SysActuals.u2SysMinL2LSPGenInt =
            (UINT2) i4SetValIsisSysMinL2LSPGenInt;
        i1ErrCode = SNMP_SUCCESS;

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysMinL2LSPGenInt ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysPollESHelloRate
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysPollESHelloRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysPollESHelloRate (INT4 i4IsisSysInstance,
                              INT4 i4SetValIsisSysPollESHelloRate)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysPollESHelloRate ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u4ESHelloRate =
            (UINT2) i4SetValIsisSysPollESHelloRate;
        i1ErrCode = SNMP_SUCCESS;

    }
    NMP_PT ((ISIS_LGST, "NMP <E> : This version of Future ISIS does not "
             " Support ES Hello Rate \n"));
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysPollESHelloRate ()\n"));

    return (i1ErrCode);            /*silvercreek jul22 */
}

/****************************************************************************
 Function    :  nmhSetIsisSysWaitTime
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysWaitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysWaitTime (INT4 i4IsisSysInstance, INT4 i4SetValIsisSysWaitTime)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysWaitTime ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u2SysWaitTime = (UINT2) i4SetValIsisSysWaitTime;
        i1ErrCode = SNMP_SUCCESS;

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysWaitTime ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysAdminState
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysAdminState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysAdminState (INT4 i4IsisSysInstance, INT4 i4SetValIsisSysAdminState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1Event = 0;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtISStatChange *pISEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysAdminState ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else if (pContext->SysActuals.u1SysAdminState ==
             (UINT1) i4SetValIsisSysAdminState)
    {
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        pContext->SysActuals.u1SysAdminState =
            (UINT1) i4SetValIsisSysAdminState;

#ifdef NPAPI_WANTED
        if (IsisHwProgram (pContext->SysActuals.u1SysAdminState) ==
            ISIS_FAILURE)
        {
            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            i1ErrCode = SNMP_SUCCESS;
        }
#else
        i1ErrCode = SNMP_SUCCESS;
#endif

#ifdef ISIS_FT_ENABLED
        if ((pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
            && ((ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_DISABLE)
                || (ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE)))
#else
        if (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
#endif
        {
            if (i4SetValIsisSysAdminState == ISIS_STATE_ON)
            {
                u1Event = ISIS_EVT_IS_UP;
            }
            else
            {
                u1Event = ISIS_EVT_IS_DOWN;
                pContext->u1IsisGRRestartExitReason = ISIS_GR_RESTART_NONE;
            }

            /* Forming and enqueuing IS UP/DOWN Event
             */

            pISEvt = (tIsisEvtISStatChange *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtISStatChange));
            if (pISEvt != NULL)
            {
                pISEvt->u1EvtID = u1Event;
                pISEvt->u1PrevState = ISIS_FT_INIT;
                MEMCPY (pISEvt->au1SysID,
                        pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);

                IsisUtlSendEvent (pContext, (UINT1 *) pISEvt,
                                  sizeof (tIsisEvtISStatChange));
            }
        }

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysAdminState ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysOrigL2LSPBuffSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysOrigL2LSPBuffSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysOrigL2LSPBuffSize (INT4 i4IsisSysInstance,
                                INT4 i4SetValIsisSysOrigL2LSPBuffSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetIsisSysOrigL2LSPBuffSize ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysConfigs.u4SysOrigL2LSPBufSize =
            (UINT4) i4SetValIsisSysOrigL2LSPBuffSize;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSysOrigL2LSPBuffSize ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysLogAdjacencyChanges
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysLogAdjacencyChanges
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysLogAdjacencyChanges (INT4 i4IsisSysInstance,
                                  INT4 i4SetValIsisSysLogAdjacencyChanges)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetIsisSysLogAdjacencyChanges ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.bSysLogAdjChanges =
            (UINT1) i4SetValIsisSysLogAdjacencyChanges;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSysLogAdjacencyChanges ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysMaxAreaCheck
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysMaxAreaCheck
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysMaxAreaCheck (INT4 i4IsisSysInstance,
                           INT4 i4SetValIsisSysMaxAreaCheck)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysMaxAreaCheck ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.bSysMaxAACheck =
            (UINT1) i4SetValIsisSysMaxAreaCheck;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysMaxAreaCheck ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysExistState
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysExistState (INT4 i4IsisSysInstance, INT4 i4SetValIsisSysExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtISStatChange *pISEvt = NULL;
    tGrSemInfo          GrSemInfo;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysExistState ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    switch (i4SetValIsisSysExistState)
    {
        case ISIS_CR_WT:

            i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

            if (i4RetVal == ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : System Entry already exists for"
                         " the Index,  %d\n", i4IsisSysInstance));
                i1ErrCode = SNMP_FAILURE;
            }
            else
            {
                pContext = (tIsisSysContext *)
                    ISIS_MEM_ALLOC (ISIS_BUF_CTXT, sizeof (tIsisSysContext));

                if (pContext == NULL)
                {
                    PANIC ((ISIS_LGST,
                            ISIS_MEM_ALLOC_FAIL " : Context Init\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    /* Initialize the Context with default values, by default
                     * the following routine shall initailise teh System Exist
                     * State as ISIS_NOT_READY
                     */

                    if (IsisCtrlInitSysContext (pContext, u4InstIdx) ==
                        ISIS_FAILURE)
                    {
                        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : \n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        i1ErrCode = SNMP_FAILURE;
                    }
                    else
                    {
                        IsisCtrlAddSysContext (pContext);
                        i1ErrCode = SNMP_SUCCESS;
                    }
                }
            }
            break;

        case ISIS_ACTIVE:

            i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

            if (i4RetVal != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for"
                         " the Index,  %d\n", i4IsisSysInstance));

                i1ErrCode = SNMP_FAILURE;
            }
            else
            {
                if (pContext->SysActuals.u1SysExistState == ISIS_NOT_IN_SER)
                {
                    pContext->SysActuals.u1SysExistState = ISIS_ACTIVE;
                    i1ErrCode = SNMP_SUCCESS;

                    /* Copy the System configs to System actuals and allocate
                     * memory for Shortest Path Table
                     */
                    IsisCtrlCopyConfigsToActuals (pContext);

                    if (((pContext->u1IsisGRRestartStatus ==
                          ISIS_STARTING_ROUTER)
                         || (pContext->u1IsisGRRestartStatus ==
                             ISIS_GR_RESTART_PLANNED))
                        && (pContext->u1IsisGRRestartMode == ISIS_GR_RESTARTER))
                    {

                        MEMSET (&GrSemInfo, 0, sizeof (tGrSemInfo));
                        GrSemInfo.pContext = pContext;
                        /* GR SEM Function is executed now */
                        IsisGrRunRestartSem (ISIS_GR_EVT_RESTART,
                                             ISIS_GR_ADJ_RESTART, &GrSemInfo);
                    }

#ifdef ISIS_FT_ENABLED
                    if ((pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
                        && ((ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_DISABLE)
                            || (ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE)))
#else
                    if (pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON)
#endif
                    {
                        /* Forming and enqueuing IS UP Event
                         */

                        pISEvt = (tIsisEvtISStatChange *)
                            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                            sizeof (tIsisEvtISStatChange));
                        if (pISEvt != NULL)
                        {
                            pISEvt->u1EvtID = ISIS_EVT_IS_UP;
                            pISEvt->u1PrevState = ISIS_FT_INIT;
                            MEMCPY (pISEvt->au1SysID,
                                    pContext->SysActuals.au1SysID,
                                    ISIS_SYS_ID_LEN);

                            IsisUtlSendEvent (pContext, (UINT1 *) pISEvt,
                                              sizeof (tIsisEvtISStatChange));
                        }
                    }
                }
                else
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Could not Set SysExistState to Active"
                             " since RowStatus was not in NOT_IN_SERVICE "
                             "state\n"));

                    i1ErrCode = SNMP_FAILURE;
                }
            }
            break;

        case ISIS_DESTROY:

            i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

            if (i4RetVal != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                         "the Index,  %d\n", i4IsisSysInstance));

                i1ErrCode = SNMP_FAILURE;
            }
            else
            {
                /* Forming and enqueuing IS DESTROY Event
                 */

                pISEvt = (tIsisEvtISStatChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                    sizeof (tIsisEvtISStatChange));

                if (pISEvt != NULL)
                {
                    pISEvt->u1EvtID = ISIS_EVT_IS_DESTROY;
                    MEMCPY (pISEvt->au1SysID,
                            pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);

                    IsisUtlSendEvent (pContext, (UINT1 *) pISEvt,
                                      sizeof (tIsisEvtISStatChange));
                    i1ErrCode = SNMP_SUCCESS;
                }
                else
                {
                    PANIC ((ISIS_LGST, "NMP <P> : Could not send "
                            "IS_DESTROY EVENT !!!\n"));
                    i1ErrCode = SNMP_FAILURE;
                }

            }
            break;

        case ISIS_CR_GO:

            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Exist State Specified"));
            i1ErrCode = SNMP_FAILURE;
            break;

        case ISIS_NOT_IN_SER:

            i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

            if (i4RetVal != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                         " the Index, %d\n", i4IsisSysInstance));
                i1ErrCode = SNMP_FAILURE;
            }
            else if (pContext->SysActuals.u1SysExistState == ISIS_NOT_IN_SER)
            {
                i1ErrCode = SNMP_SUCCESS;
            }
            else
            {
                pContext->SysActuals.u1SysExistState = ISIS_NOT_IN_SER;
                i1ErrCode = SNMP_SUCCESS;

                pISEvt = (tIsisEvtISStatChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                    sizeof (tIsisEvtISStatChange));

                if (pISEvt != NULL)
                {
                    pISEvt->u1EvtID = ISIS_EVT_IS_DOWN;
                    MEMCPY (pISEvt->au1SysID,
                            pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);

                    IsisUtlSendEvent (pContext, (UINT1 *) pISEvt,
                                      sizeof (tIsisEvtISStatChange));
                }
            }
            break;

        default:
            break;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysExistState ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysL2toL1Leaking
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysL2toL1Leaking
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysL2toL1Leaking (INT4 i4IsisSysInstance,
                            INT4 i4SetValIsisSysL2toL1Leaking)
{

    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisMsg            IsisMsg;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysSetOverload ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else if (pContext->SysActuals.bSysL2ToL1Leak ==
             (UINT1) i4SetValIsisSysL2toL1Leaking)
    {
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        pContext->SysActuals.bSysL2ToL1Leak =
            (UINT1) i4SetValIsisSysL2toL1Leaking;
        i1ErrCode = SNMP_SUCCESS;
    }

    /* Event will be posted only during Disabling route leak.
     * Route leak enable processing will be done during L2decisioncomplete event*/
    if (pContext->SysActuals.bSysL2ToL1Leak == ISIS_CLI_ROUTE_LEAK_DISABLE)
    {
        IsisMsg.pu1Msg = NULL;
        IsisMsg.u1MsgType = ISIS_MSG_NO_RLEAK;
        IsisMsg.u1DataType = 0;
        IsisMsg.pMemFreeRoutine = NULL;
        IsisMsg.u4CxtOrIfindex = u4InstIdx;

        if (IsisEnqueueMessage
            ((UINT1 *) &IsisMsg, sizeof (tIsisMsg), gIsisCtrlQId,
             ISIS_SYS_LL_DATA_EVT) == ISIS_FAILURE)
        {
            return ISIS_FAILURE;
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysL2toL1Leaking ()\n"));
    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSysSetOverload
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysSetOverload
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysSetOverload (INT4 i4IsisSysInstance,
                          INT4 i4SetValIsisSysSetOverload)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisEvtLSPDBOL    *pEvtDBOL1 = NULL;
    tIsisEvtLSPDBOL    *pEvtDBOL2 = NULL;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysSetOverload ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else if (pContext->SysActuals.u1SysSetOL ==
             (UINT1) i4SetValIsisSysSetOverload)
    {
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        pContext->SysActuals.u1SysSetOL = (UINT1) i4SetValIsisSysSetOverload;

        if (((i4SetValIsisSysSetOverload == ISIS_LL_SET_L1OVERLOAD)
             || (i4SetValIsisSysSetOverload == ISIS_LL_SET_L12OVERLOAD))
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
            && (pContext->SysActuals.u1SysType != ISIS_LEVEL2))
        {
            pEvtDBOL1 = (tIsisEvtLSPDBOL *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtLSPDBOL));
            if (pEvtDBOL1 != NULL)
            {
                pEvtDBOL1->u1EvtID = ISIS_EVT_LSP_DBOL;
                pEvtDBOL1->u1Level = ISIS_LEVEL1;
                pEvtDBOL1->u1SysL1State = pContext->SysActuals.u1SysL1State;
                pEvtDBOL1->u1SysL2State = pContext->SysActuals.u1SysL2State;
                IsisUtlSendEvent (pContext, (UINT1 *) pEvtDBOL1,
                                  sizeof (tIsisEvtLSPDBOL));
            }
            else
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Posting Events\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_EVTS);

                NMP_EE ((ISIS_LGST,
                         "NMP <X> : Exiting nmhSetIsisSysSetOverload ()\n"));
                return i1ErrCode;
            }

        }

        if (((i4SetValIsisSysSetOverload == ISIS_LL_SET_L2OVERLOAD)
             || (i4SetValIsisSysSetOverload == ISIS_LL_SET_L12OVERLOAD))
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
            && (pContext->SysActuals.u1SysType != ISIS_LEVEL1))
        {
            pEvtDBOL2 = (tIsisEvtLSPDBOL *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtLSPDBOL));
            if (pEvtDBOL2 != NULL)
            {
                pEvtDBOL2->u1EvtID = ISIS_EVT_LSP_DBOL;
                pEvtDBOL2->u1Level = ISIS_LEVEL2;
                pEvtDBOL2->u1SysL1State = pContext->SysActuals.u1SysL1State;
                pEvtDBOL2->u1SysL2State = pContext->SysActuals.u1SysL2State;
                IsisUtlSendEvent (pContext, (UINT1 *) pEvtDBOL2,
                                  sizeof (tIsisEvtLSPDBOL));
            }
            else
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Posting Events\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_EVTS);

                return SNMP_FAILURE;
            }
        }

        if ((i4SetValIsisSysSetOverload == ISIS_LL_CLEAR_OVERLOAD)
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))
        {
            if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
            {
                pEvtDBOL1 = (tIsisEvtLSPDBOL *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtLSPDBOL));
                if (pEvtDBOL1 != NULL)
                {
                    pEvtDBOL1->u1EvtID = ISIS_EVT_LSP_DBOL_RECOV;
                    pEvtDBOL1->u1Level = ISIS_LEVEL1;
                    pEvtDBOL1->u1SysL1State = pContext->SysActuals.u1SysL1State;
                    pEvtDBOL1->u1SysL2State = pContext->SysActuals.u1SysL2State;
                    IsisUtlSendEvent (pContext, (UINT1 *) pEvtDBOL1,
                                      sizeof (tIsisEvtLSPDBOL));
                }
                else
                {
                    PANIC ((ISIS_LGST,
                            ISIS_MEM_ALLOC_FAIL " : Posting Events\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    IsisUtlFormResFailEvt (pContext, ISIS_BUF_EVTS);

                    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                             "nmhSetIsisSysSetOverload ()\n"));
                    return SNMP_FAILURE;
                }
            }

            if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
            {
                pEvtDBOL2 = (tIsisEvtLSPDBOL *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtLSPDBOL));
                if (pEvtDBOL2 != NULL)
                {
                    pEvtDBOL2->u1EvtID = ISIS_EVT_LSP_DBOL_RECOV;
                    pEvtDBOL2->u1Level = ISIS_LEVEL2;
                    pEvtDBOL2->u1SysL1State = pContext->SysActuals.u1SysL1State;
                    pEvtDBOL2->u1SysL2State = pContext->SysActuals.u1SysL2State;
                    IsisUtlSendEvent (pContext, (UINT1 *) pEvtDBOL2,
                                      sizeof (tIsisEvtLSPDBOL));
                }
                else
                {
                    PANIC ((ISIS_LGST,
                            ISIS_MEM_ALLOC_FAIL " : Posting Events\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    IsisUtlFormResFailEvt (pContext, ISIS_BUF_EVTS);

                    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                             "nmhSetIsisSysSetOverload ()\n"));
                    return SNMP_FAILURE;
                }
            }
        }

        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysSetOverload ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetIsisSysL1MetricStyle
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysL1MetricStyle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysL1MetricStyle (INT4 i4IsisSysInstance,
                            INT4 i4SetValIsisSysL1MetricStyle)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysL1MetricStyle ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysConfigs.u1SysL1MetricStyle =
            (UINT1) i4SetValIsisSysL1MetricStyle;
        i1ErrCode = SNMP_SUCCESS;

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysL1MetricStyle ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetIsisSysL1SPFConsiders
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysL1SPFConsiders
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysL1SPFConsiders (INT4 i4IsisSysInstance,
                             INT4 i4SetValIsisSysL1SPFConsiders)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysL1SPFConsiders ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u1SysL1SPFConsiders =
            (UINT1) i4SetValIsisSysL1SPFConsiders;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysL1SPFConsiders ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetIsisSysL2MetricStyle
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysL2MetricStyle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysL2MetricStyle (INT4 i4IsisSysInstance,
                            INT4 i4SetValIsisSysL2MetricStyle)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysL2MetricStyle ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u1SysL2MetricStyle =
            (UINT1) i4SetValIsisSysL2MetricStyle;
        i1ErrCode = SNMP_SUCCESS;

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysL2MetricStyle ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetIsisSysL2SPFConsiders
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysL2SPFConsiders
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysL2SPFConsiders (INT4 i4IsisSysInstance,
                             INT4 i4SetValIsisSysL2SPFConsiders)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysL2SPFConsiders ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u1SysL2SPFConsiders =
            (UINT1) i4SetValIsisSysL2SPFConsiders;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysL2SPFConsiders ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetIsisSysTEEnabled
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysTEEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysTEEnabled (INT4 i4IsisSysInstance, INT4 i4SetValIsisSysTEEnabled)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysTEEnabled ()\n"));

    UNUSED_PARAM (i4IsisSysInstance);
    UNUSED_PARAM (i4SetValIsisSysTEEnabled);
    NMP_EE ((ISIS_LGST, "NMP <X> : TE Not Supported \n"));

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysTEEnabled ()\n"));

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIsisSysMaxAge
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysMaxAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysMaxAge (INT4 i4IsisSysInstance, INT4 i4SetValIsisSysMaxAge)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSysMaxAge ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u4SysMaxAge = (UINT4) i4SetValIsisSysMaxAge;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysMaxAge ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhSetIsisSysReceiveLSPBufferSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                setValIsisSysReceiveLSPBufferSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysReceiveLSPBufferSize (INT4 i4IsisSysInstance,
                                   INT4 i4SetValIsisSysReceiveLSPBufferSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetIsisSysReceiveLSPBufferSize ()\n"));
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pContext->SysActuals.u4SysRxLSPBufSize =
            (UINT4) i4SetValIsisSysReceiveLSPBufferSize;
        i1ErrCode = SNMP_SUCCESS;
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSysMaxAge ()\n"));

    return i1ErrCode;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IsisSysType
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysType (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                      INT4 i4TestValIsisSysType)
{
    INT1                i1RetCode = SNMP_FAILURE;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysType ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysType ()\n"));
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return i1RetCode;
    }

    if ((i4TestValIsisSysType == ISIS_LEVEL1)
        || (i4TestValIsisSysType == ISIS_LEVEL2)
        || (i4TestValIsisSysType == ISIS_LEVEL12))
    {
        i1RetCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysType \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INVALID_SYS_TYPE);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysType ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysID
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysID (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                    tSNMP_OCTET_STRING_TYPE * pTestValIsisSysID)
{
    INT1                i1RetCode = SNMP_FAILURE;
    UINT1               au1SysID[ISIS_SYS_ID_LEN];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1Blankspace[ISIS_SYS_ID_LEN];

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysID ()\n"));

    MEMSET (au1SysID, 0, ISIS_SYS_ID_LEN);
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Blankspace, 0, ISIS_SYS_ID_LEN);

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysID ()\n"));
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return i1RetCode;
    }

    if (MEMCMP (pTestValIsisSysID->pu1_OctetList, au1Blankspace,
                ISIS_SYS_ID_LEN) == 0)
    {
        /* When IsisSysID is deleted , it will be set to NULL .
         *                                         So returning SUCCESS*/
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysID ()\n"));
        return SNMP_SUCCESS;
    }

    OctetStr.pu1_OctetList = au1SysID;
    OctetStr.i4_Length = 0;
    nmhGetIsisSysID (i4IsisSysInstance, &OctetStr);

    if ((OctetStr.i4_Length != 0) &&
        (MEMCMP (OctetStr.pu1_OctetList, pTestValIsisSysID->pu1_OctetList,
                 ISIS_SYS_ID_LEN) != 0))
    {
        CLI_SET_ERR (CLI_ISIS_INV_SYS_ID);
        return SNMP_FAILURE;

    }

    if (pTestValIsisSysID->i4_Length != ISIS_SYS_ID_LEN)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Length of SysID \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        i1RetCode = SNMP_FAILURE;

        CLI_SET_ERR (CLI_ISIS_INV_LEN);
    }
    else
    {
        i1RetCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysID ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysMaxPathSplits
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysMaxPathSplits
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysMaxPathSplits (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                               INT4 i4TestValIsisSysMaxPathSplits)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysMaxPathSplits ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysMaxPathSplits ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysMaxPathSplits < 1)
        || (i4TestValIsisSysMaxPathSplits > ISIS_LL_MAX_PATH_SPLITS))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysMaxPathSplits \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysMaxPathSplits ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysMaxLSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysMaxLSPGenInt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysMaxLSPGenInt (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                              INT4 i4TestValIsisSysMaxLSPGenInt)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysMaxLSPGenInt ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysMaxLSPGenInt ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysMaxLSPGenInt < ISIS_LL_LOW_MAX_LSP_GEN_INT)
        || (i4TestValIsisSysMaxLSPGenInt > ISIS_LL_HIGH_MAX_LSP_GEN_INT))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysMaxLSPGenInt \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysMaxLSPGenInt ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysOrigL1LSPBuffSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysOrigL1LSPBuffSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysOrigL1LSPBuffSize (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                   INT4 i4TestValIsisSysOrigL1LSPBuffSize)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysOrigL1LSPBuffSize ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysOrigL1LSPBuffSize ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysOrigL1LSPBuffSize > ISIS_LL_MAX_PDU_SIZE)
        || (i4TestValIsisSysOrigL1LSPBuffSize < ISIS_LL_MIN_PDU_SIZE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysL1LSPBuffSize \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysOrigL1LSPBuffSize ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysMaxAreaAddresses
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysMaxAreaAddresses
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysMaxAreaAddresses (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                  INT4 i4TestValIsisSysMaxAreaAddresses)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysMaxAreaAddresses ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysMaxAreaAddresses ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysMaxAreaAddresses < ISIS_LL_LOW_MAX_AREA_ADDRESS)
        || (i4TestValIsisSysMaxAreaAddresses > ISIS_LL_HIGH_MAX_AREA_ADDRESS))

    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of SysMaxAreaAddresses \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysMaxAreaAddresses ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysMinL1LSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysMinL1LSPGenInt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysMinL1LSPGenInt (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                INT4 i4TestValIsisSysMinL1LSPGenInt)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysMinL1LSPGenInt ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysMinL1LSPGenInt ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysMinL1LSPGenInt < ISIS_LL_LOW_MIN_L1LSP_GEN_INT)
        || (i4TestValIsisSysMinL1LSPGenInt > ISIS_LL_HIGH_MIN_L1LSP_GEN_INT))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysMinL1LSPGenInt \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysMinL1LSPGenInt ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysMinL2LSPGenInt
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysMinL2LSPGenInt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysMinL2LSPGenInt (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                INT4 i4TestValIsisSysMinL2LSPGenInt)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysMinL2LSPGenInt ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysMinL2LSPGenInt ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysMinL2LSPGenInt < ISIS_LL_LOW_MIN_L2LSP_GEN_INT)
        || (i4TestValIsisSysMinL2LSPGenInt > ISIS_LL_HIGH_MIN_L2LSP_GEN_INT))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysMinL2LSPGenInt \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysMinL2LSPGenInt ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysPollESHelloRate
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysPollESHelloRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysPollESHelloRate (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                 INT4 i4TestValIsisSysPollESHelloRate)
{
    NMP_PT ((ISIS_LGST, "NMP <E> : ES Hello Rate is not supported \n"));
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    UNUSED_PARAM (i4IsisSysInstance);
    UNUSED_PARAM (i4TestValIsisSysPollESHelloRate);
    return (SNMP_FAILURE);
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysWaitTime
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysWaitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysWaitTime (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                          INT4 i4TestValIsisSysWaitTime)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysWaitTime ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysWaitTime ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysWaitTime < ISIS_LL_LOW_SYS_WAIT_TIME)
        || (i4TestValIsisSysWaitTime > ISIS_LL_HIGH_SYS_WAIT_TIME))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysWaitTime \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysWaitTime ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysAdminState
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysAdminState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysAdminState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                            INT4 i4TestValIsisSysAdminState)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysAdminState ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysAdminState ()\n"));

        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return i1RetCode;
    }

    if ((i4TestValIsisSysAdminState != ISIS_STATE_ON)
        && (i4TestValIsisSysAdminState != ISIS_STATE_OFF))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysAdminState \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysAdminState ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysOrigL2LSPBuffSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysOrigL2LSPBuffSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysOrigL2LSPBuffSize (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                   INT4 i4TestValIsisSysOrigL2LSPBuffSize)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysOrigL2LSPBuffSize ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysOrigL2LSPBuffSize ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysOrigL2LSPBuffSize < ISIS_LL_MIN_PDU_SIZE)
        || (i4TestValIsisSysOrigL2LSPBuffSize > ISIS_LL_MAX_PDU_SIZE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysL2LSPBuffSize \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysOrigL2LSPBuffSize ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysLogAdjacencyChanges
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysLogAdjacencyChanges
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysLogAdjacencyChanges (UINT4 *pu4ErrorCode,
                                     INT4 i4IsisSysInstance,
                                     INT4 i4TestValIsisSysLogAdjacencyChanges)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysLogAdjacencyChanges ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysLogAdjacencyChanges ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysLogAdjacencyChanges != ISIS_TRUE)
        && (i4TestValIsisSysLogAdjacencyChanges != ISIS_FALSE))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of SysLogAdjacencyChanges \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysLogAdjacencyChanges ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysMaxAreaCheck
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysMaxAreaCheck
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysMaxAreaCheck (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                              INT4 i4TestValIsisSysMaxAreaCheck)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysMaxAreaCheck ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysMaxAreaCheck ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysMaxAreaCheck != ISIS_TRUE)
        && (i4TestValIsisSysMaxAreaCheck != ISIS_FALSE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysMaxAreaCheck \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysMaxAreaCheck ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysExistState
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysExistState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                            INT4 i4TestValIsisSysExistState)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4NumInst = 0;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysExistState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysType ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
        return (SNMP_FAILURE);
    }

    switch (i4TestValIsisSysExistState)
    {
        case ISIS_CR_WT:

            /* Checks whether the entry already exists */

            i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

            if (i4RetVal == ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : System Entry already exists for "
                         "the Index,  %d\n", i4IsisSysInstance));

                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetCode = SNMP_FAILURE;
                CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
            }

            else
            {
                pContext = ISIS_GET_CONTEXT ();
                while (pContext != NULL)
                {
                    u4NumInst++;
                    pContext = pContext->pNext;
                }
                if (u4NumInst < ISIS_MAX_INSTS)
                {
                    i1RetCode = SNMP_SUCCESS;
                }
                else
                {
                    i1RetCode = SNMP_FAILURE;
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Cannot Create System Entry, since the"
                             " number of existing entries already equals"
                             " Maximum number of Entries Supported !!!!\n"));
                    CLI_SET_ERR (CLI_ISIS_UNABLE_TO_CREATE_ENTRY);
                }
            }
            break;

        case ISIS_CR_GO:

            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysExistState \n"));

            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            i1RetCode = SNMP_FAILURE;

            CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
            break;

        case ISIS_NOT_IN_SER:

            /* if the entry doesn't exist, return failure */

            i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

            if (i4RetVal != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                         "the Index,  %d\n", i4IsisSysInstance));

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetCode = SNMP_FAILURE;

                CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
            }

            else if (pContext->SysActuals.u1SysExistState == ISIS_NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetCode = SNMP_FAILURE;

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Invalid Value of SysExistState \n"));

                CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
            }

            else
            {
                i1RetCode = SNMP_SUCCESS;
            }
            break;

        case ISIS_NOT_READY:

            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysExistState \n"));

            CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1RetCode = SNMP_FAILURE;
            break;

        case ISIS_ACTIVE:

            i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

            if (i4RetVal != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                         "the Index,  %d\n", i4IsisSysInstance));

                CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetCode = SNMP_FAILURE;
            }

            else
            {
                /* Checking for all the mandatory variables 
                 */

                if (((pContext->u1QFlag & ISIS_SYSALLSET_FLAG)
                     != ISIS_SYSALLSET_FLAG))
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Mandatory Params Not Initialised \n"));

                    CLI_SET_ERR (CLI_ISIS_PARAM_NOT_INIT);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1RetCode = SNMP_FAILURE;
                }
                else
                {
                    i1RetCode = SNMP_SUCCESS;
                }
            }
            break;

        case ISIS_DESTROY:

            i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

            if (i4RetVal != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist "
                         "for the Index,  %d\n", i4IsisSysInstance));

                CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetCode = SNMP_FAILURE;
            }

            else
            {
                i1RetCode = SNMP_SUCCESS;
            }
            break;

        default:

            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysExistState \n"));

            CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetCode = SNMP_FAILURE;

    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysExistState ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysL2toL1Leaking
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysL2toL1Leaking
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysL2toL1Leaking (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                               INT4 i4TestValIsisSysL2toL1Leaking)
{

    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysL2toL1Leaking ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysL2toL1Leaking ()\n"));
        return i1RetCode;
    }
    if ((i4TestValIsisSysL2toL1Leaking != ISIS_TRUE)
        && (i4TestValIsisSysL2toL1Leaking != ISIS_FALSE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of route leaking\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysL2toL1Leaking ()\n"));
    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysSetOverload
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysSetOverload
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysSetOverload (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                             INT4 i4TestValIsisSysSetOverload)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysSetOverload ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysSetOverload ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1RetCode;
    }

    if ((i4TestValIsisSysSetOverload != ISIS_LL_SET_L1OVERLOAD)
        && (i4TestValIsisSysSetOverload != ISIS_LL_SET_L2OVERLOAD)
        && (i4TestValIsisSysSetOverload != ISIS_LL_SET_L12OVERLOAD)
        && (i4TestValIsisSysSetOverload != ISIS_LL_CLEAR_OVERLOAD))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysSetOverload \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysSetOverload ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysL1MetricStyle
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysL1MetricStyle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysL1MetricStyle (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                               INT4 i4TestValIsisSysL1MetricStyle)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysL1MetricStyle()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysL1MetricStyle ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysL1MetricStyle != ISIS_STYLE_NARROW_METRIC)
        && (i4TestValIsisSysL1MetricStyle != ISIS_STYLE_WIDE_METRIC)
        && (i4TestValIsisSysL1MetricStyle != ISIS_STYLE_BOTH_METRIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysL1MetricStyle \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X>: Exiting nmhTestv2IsisSysL1MetricStyle ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysL1SPFConsiders
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysL1SPFConsiders
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysL1SPFConsiders (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                INT4 i4TestValIsisSysL1SPFConsiders)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP<X> : Entered nmhTestv2IsisSysL1SPFConsiders()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysL1SPFConsiders ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysL1SPFConsiders != ISIS_STYLE_NARROW_METRIC)
        && (i4TestValIsisSysL1SPFConsiders != ISIS_STYLE_WIDE_METRIC)
        && (i4TestValIsisSysL1SPFConsiders != ISIS_STYLE_BOTH_METRIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysL1SPFConsiders \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP<X>: Exiting nmhTestv2IsisSysL1SPFConsiders ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSysL2MetricStyle
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysL2MetricStyle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysL2MetricStyle (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                               INT4 i4TestValIsisSysL2MetricStyle)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysL2MetricStyle()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysL2MetricStyle ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysL2MetricStyle != ISIS_STYLE_NARROW_METRIC)
        && (i4TestValIsisSysL2MetricStyle != ISIS_STYLE_WIDE_METRIC)
        && (i4TestValIsisSysL2MetricStyle != ISIS_STYLE_BOTH_METRIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysL2MetricStyle \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysL2MetricStyle()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysL2SPFConsiders
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysL2SPFConsiders
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysL2SPFConsiders (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                INT4 i4TestValIsisSysL2SPFConsiders)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP<X> : Entered nmhTestv2IsisSysL2SPFConsiders()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysL2SPFConsiders ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysL2SPFConsiders != ISIS_STYLE_NARROW_METRIC)
        && (i4TestValIsisSysL2SPFConsiders != ISIS_STYLE_WIDE_METRIC)
        && (i4TestValIsisSysL2SPFConsiders != ISIS_STYLE_BOTH_METRIC))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysL2SPFConsiders \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP<X>: Exiting nmhTestv2IsisSysL2SPFConsiders ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysTEEnabled
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysTEEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysTEEnabled (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                           INT4 i4TestValIsisSysTEEnabled)
{
    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSysTEEnabled ()\n"));

    NMP_EE ((ISIS_LGST, "NMP <X> : TE Not Supported \n"));

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysTEEnabled ()\n"));

    UNUSED_PARAM (i4IsisSysInstance);
    UNUSED_PARAM (i4TestValIsisSysTEEnabled);

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysMaxAge
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysMaxAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysMaxAge (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                        INT4 i4TestValIsisSysMaxAge)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP<X> : Entered nmhTestv2IsisSysMaxAge ()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysMaxAge ()\n"));
        return i1RetCode;
    }

    if ((i4TestValIsisSysMaxAge < ISIS_MAX_AGE_MIN_INT)
        || (i4TestValIsisSysMaxAge > ISIS_MAX_AGE_MAX_INT))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of SysMaxAge \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP<X>: Exiting nmhTestv2IsisSysMaxAge ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhTestv2IsisSysReceiveLSPBufferSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                testValIsisSysReceiveLSPBufferSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysReceiveLSPBufferSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IsisSysInstance,
                                      INT4 i4TestValIsisSysReceiveLSPBufferSize)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP<X> : Entered nmhTestv2IsisSysReceiveLSPBufferSize()\n"));

    i1RetCode = IsisNmhValSysTable (pu4ErrorCode, i4IsisSysInstance);

    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysReceiveLSPBufferSize()\n"));
        return i1RetCode;
    }
    if ((i4TestValIsisSysReceiveLSPBufferSize < ISIS_MIN_STD_RX_LSPBUF_SIZE)
        || (i4TestValIsisSysReceiveLSPBufferSize > ISIS_MAX_STD_RX_LSPBUF_SIZE))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of SysReceiveLSPBufferSize \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
        return i1RetCode;
    }

    i1RetCode = (INT1) IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i1RetCode == ISIS_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSysReceiveLSPBufferSize()\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i1RetCode = SNMP_FAILURE;

    }
    else if (i4TestValIsisSysReceiveLSPBufferSize >=
             (INT4) ISIS_MAX (pContext->SysConfigs.u4SysOrigL1LSPBufSize,
                              pContext->SysConfigs.u4SysOrigL2LSPBufSize))
    {
        i1RetCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of SysReceiveLSPBufferSize \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP<X>: Exiting nmhTestv2IsisSysReceiveLSPBufferSize ()\n"));

    return i1RetCode;

}

/* LOW LEVEL Routines for Table : IsisManAreaAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisManAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisManAreaAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisManAreaAddrTable (INT4 i4IsisSysInstance,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pIsisManAreaAddr)
{
    INT1                i1ErrCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisManAreaAddrTable ()\n"));

    /*UNUSED_PARAM (i4IsisSysInstance); */
    /*UNUSED_PARAM (pIsisManAreaAddr); *//*silvercreek */
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhValidateIndexInstanceIsisManAreaAddrTable ()\n"));
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return (SNMP_FAILURE);
    }

    if ((pIsisManAreaAddr->i4_Length > ISIS_AREA_ADDR_LEN)
        || (pIsisManAreaAddr->i4_Length <= 0))
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : Invalid Value of ManAreaAddress Length \n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisManAreaAddrTable ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisManAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisManAreaAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisManAreaAddrTable (INT4 *pi4IsisSysInstance,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pIsisManAreaAddr)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               au1OctetList[ISIS_AREA_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE FirstManArea;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexIsisManAreaAddrTable"));
/*silvercreek*/
    if ((*pi4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (*pi4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetFirstIndexIsisManAreaAddrTable ()\n"));
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return (SNMP_FAILURE);
    }

    MEMSET (au1OctetList, 0x00, ISIS_AREA_ADDR_LEN);
    FirstManArea.pu1_OctetList = au1OctetList;

    FirstManArea.i4_Length = 0;

    if (nmhGetNextIndexIsisManAreaAddrTable (0, pi4IsisSysInstance,
                                             &FirstManArea,
                                             pIsisManAreaAddr) == SNMP_SUCCESS)
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexIsisManAreaAddrTable"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisManAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisManAreaAddr
                nextIsisManAreaAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisManAreaAddrTable (INT4 i4IsisSysInstance,
                                     INT4 *pi4NextIsisSysInstance,
                                     tSNMP_OCTET_STRING_TYPE * pIsisManAreaAddr,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextIsisManAreaAddr)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4CurrIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4NextIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tSNMP_OCTET_STRING_TYPE *pMAAEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetNextIndexIsisManAreaAddrTable ()\n"));

    pMAAEntry = pIsisManAreaAddr;

    i4RetVal = IsisCtrlGetSysContext (u4CurrIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4CurrIdx, &u4NextIdx)
            != ISIS_SUCCESS)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                     "the Index,  %u\n", u4CurrIdx));
            NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                     "nmhGetNextIndexIsisManAreaAddrTable ()\n"));
            return (SNMP_FAILURE);
        }

        else
        {
            u4CurrIdx = u4NextIdx;
            i4RetValue = IsisCtrlGetSysContext (u4CurrIdx, &pContext);
            MEMSET (pMAAEntry->pu1_OctetList, 0, ISIS_AREA_ADDR_LEN);
            pMAAEntry->i4_Length = 0;
        }
    }

    while (pContext != NULL)
    {
        if ((i4RetVal = nmhUtlGetNextMAAddr (pContext, pMAAEntry,
                                             pNextIsisManAreaAddr))
            != ISIS_SUCCESS)
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4CurrIdx, &u4NextIdx)
                != ISIS_SUCCESS)
            {

                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next Index "
                         "with the given Indices \n"));

                NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                         "nmhGetNextIndexIsisManAreaAddrTable ()\n"));

                return (SNMP_FAILURE);
            }

            else
            {
                u4CurrIdx = u4NextIdx;
                i4RetValue = IsisCtrlGetSysContext (u4CurrIdx, &pContext);
                MEMSET (pMAAEntry->pu1_OctetList, 0, ISIS_AREA_ADDR_LEN);
                pMAAEntry->i4_Length = 0;
                continue;
            }
        }

        else
        {
            *pi4NextIsisSysInstance = (INT4) u4CurrIdx;

            NMP_PT ((ISIS_LGST, "NMP <E> : The Next Indices are SysInstance : "
                     "Next Manual Area Address : %d %x %x \n",
                     *pi4NextIsisSysInstance,
                     pNextIsisManAreaAddr->pu1_OctetList[0],
                     pNextIsisManAreaAddr->pu1_OctetList[1]));

            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexIsisManAreaAddrTable ()\n"));
    UNUSED_PARAM (i4RetValue);
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisManAreaAddrExistState
 Input       :  The Indices
                IsisSysInstance
                IsisManAreaAddr

                The Object 
                retValIsisManAreaAddrExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisManAreaAddrExistState (INT4 i4IsisSysInstance,
                                 tSNMP_OCTET_STRING_TYPE * pIsisManAreaAddr,
                                 INT4 *pi4RetValIsisManAreaAddrExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisMAAEntry      *pMAAEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetIsisManAreaAddrExistState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisManAreaAddrExistState()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for "
                 "the given Index\n"));
        i1ErrCode = SNMP_FAILURE;
    }
    else if ((i4RetVal = IsisCtrlGetMAA (pContext,
                                         pIsisManAreaAddr->pu1_OctetList,
                                         (UINT1) pIsisManAreaAddr->i4_Length,
                                         &pMAAEntry)) == ISIS_SUCCESS)
    {
        *pi4RetValIsisManAreaAddrExistState = (INT4) pMAAEntry->u1ExistState;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : ManualAreaAddres Entry does not exist for "
                 "the given Index\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisManAreaAddrExistState ()\n"));

    return i1ErrCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIsisManAreaAddrExistState
 Input       :  The Indices
                IsisSysInstance
                IsisManAreaAddr

                The Object 
                setValIsisManAreaAddrExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisManAreaAddrExistState (INT4 i4IsisSysInstance,
                                 tSNMP_OCTET_STRING_TYPE * pIsisManAreaAddr,
                                 INT4 i4SetValIsisManAreaAddrExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisMAAEntry      *pMAAEntry = NULL;
    tIsisEvtManAAChange *pMAAEvt = NULL;
    tIsisEvtISStatChange *pISEvt = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetIsisManAreaAddrExistState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysType ()\n"));
        return (SNMP_FAILURE);
    }
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        switch (i4SetValIsisManAreaAddrExistState)
        {
            case ISIS_CR_WT:
            case ISIS_ACTIVE:
            case ISIS_NOT_IN_SER:

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Invalid Exist State given !!! "));
                i1ErrCode = SNMP_FAILURE;
                break;

            case ISIS_DESTROY:

                i4RetVal = IsisCtrlGetMAA (pContext,
                                           pIsisManAreaAddr->pu1_OctetList,
                                           (UINT1) pIsisManAreaAddr->i4_Length,
                                           &pMAAEntry);

                if (i4RetVal != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : ManualAreaAddres Entry does "
                             "not exist for the given Index"));
                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    i4RetVal = IsisCtrlDelMAA (pContext,
                                               pMAAEntry->ManAreaAddr.u1Length,
                                               pMAAEntry->ManAreaAddr.
                                               au1AreaAddr);
                    if (i4RetVal != ISIS_SUCCESS)
                    {
                        NMP_PT ((ISIS_LGST,
                                 "NMP <E> : Could not Delete  MAAEntry \n"));
                        i1ErrCode = SNMP_FAILURE;
                    }
                    else
                    {
                        /* Forming Manual Area Address Changed Event and sending
                         * to control queue
                         */

                        if (pContext->ManAddrTable.pMAARec == NULL)
                        {
                            pContext->u1QFlag &= ~ISIS_SYSMAA_FLAG;

                            pContext->SysActuals.u1SysExistState =
                                ISIS_NOT_READY;

#ifdef ISIS_FT_ENABLED
                            if ((pContext->u1OperState == ISIS_UP) ||
                                (ISIS_EXT_IS_FT_STATE () == ISIS_FT_STANDBY))
#else
                            if (pContext->u1OperState == ISIS_UP)
#endif
                            {

                                pISEvt = (tIsisEvtISStatChange *)
                                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                    sizeof
                                                    (tIsisEvtISStatChange));

                                if (pISEvt != NULL)
                                {
                                    pISEvt->u1EvtID = ISIS_EVT_IS_DOWN;
                                    MEMCPY (pISEvt->au1SysID,
                                            pContext->SysActuals.au1SysID,
                                            ISIS_SYS_ID_LEN);

                                    IsisUtlSendEvent (pContext,
                                                      (UINT1 *) pISEvt,
                                                      sizeof
                                                      (tIsisEvtISStatChange));
                                }
                            }
                        }
                        else if (pContext->u1OperState == ISIS_UP)
                        {
                            pMAAEvt =
                                (tIsisEvtManAAChange *)
                                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtManAAChange));
                            if (pMAAEvt != NULL)
                            {
                                pMAAEvt->u1EvtID = ISIS_EVT_MAN_ADDR_CHANGE;
                                pMAAEvt->u1Status = ISIS_MAA_DROPPED;
                                pMAAEvt->u1Length =
                                    (UINT1) pIsisManAreaAddr->i4_Length;
                                MEMCPY (pMAAEvt->au1AreaAddr,
                                        pIsisManAreaAddr->pu1_OctetList,
                                        pIsisManAreaAddr->i4_Length);

                                IsisUtlSendEvent (pContext, (UINT1 *) pMAAEvt,
                                                  sizeof (tIsisEvtManAAChange));
                            }
                        }

                        i1ErrCode = SNMP_SUCCESS;

                    }
                }
                break;

            case ISIS_CR_GO:

                i4RetVal = IsisCtrlGetMAA (pContext,
                                           pIsisManAreaAddr->pu1_OctetList,
                                           (UINT1) pIsisManAreaAddr->i4_Length,
                                           &pMAAEntry);
                if (i4RetVal == ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : ManualAreaAddres Entry already "
                             "exist for the given Index"));
                    i1ErrCode = SNMP_FAILURE;
                }
                else if (pContext->ManAddrTable.u2NumEntries ==
                         pContext->SysConfigs.u1SysMaxAA)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Manual Area Address "
                             "exceeding Maximum Area Address\n"));
                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    pMAAEntry = (tIsisMAAEntry *)
                        ISIS_MEM_ALLOC (ISIS_BUF_MAAT, sizeof (tIsisMAAEntry));

                    if (pMAAEntry == NULL)
                    {
                        CLI_SET_ERR (CLI_ISIS_MAX_ENTRY);
                        /* Forming Memory Resource Failure Event and 
                         * sending to control queue
                         */

                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_MAAT);
                        i1ErrCode = SNMP_FAILURE;
                    }

                    else
                    {
                        pMAAEntry->u1ExistState = ISIS_ACTIVE;

                        /* Copy the manual area address into the record
                         */

                        MEMCPY (pMAAEntry->ManAreaAddr.au1AreaAddr,
                                pIsisManAreaAddr->pu1_OctetList,
                                pIsisManAreaAddr->i4_Length);

                        /*check is added,to validate Area address first byte
                           is not greater than 0x9f value(i.e first digit should
                           not be alphabet). */
                        if (pContext->bNetIdDotCompliance ==
                            (tBool) ISIS_DOT_TRUE)
                        {
                            if (pMAAEntry->ManAreaAddr.au1AreaAddr[0] >
                                ISIS_MAX_FIRST_BYTE_AREA_ID)
                            {
                                CLI_SET_ERR (CLI_ISIS_INVALID_AREA_ID);
                                return SNMP_FAILURE;
                            }
                        }

                        pMAAEntry->ManAreaAddr.u1Length =
                            (UINT1) pIsisManAreaAddr->i4_Length;

                        IsisCtrlAddMAA (pContext, pMAAEntry);

                        /* Forming Manual Area Address Changed Event and 
                         * sending to control queue
                         */

                        if (pContext->u1OperState == ISIS_UP)
                        {
                            pMAAEvt = (tIsisEvtManAAChange *)
                                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtManAAChange));
                            if (pMAAEvt != NULL)
                            {
                                pMAAEvt->u1EvtID = ISIS_EVT_MAN_ADDR_CHANGE;
                                pMAAEvt->u1Status = ISIS_MAA_ADDED;
                                pMAAEvt->u1Length =
                                    (UINT1) pIsisManAreaAddr->i4_Length;
                                MEMCPY (pMAAEvt->au1AreaAddr,
                                        pIsisManAreaAddr->pu1_OctetList,
                                        pIsisManAreaAddr->i4_Length);

                                IsisUtlSendEvent (pContext, (UINT1 *) pMAAEvt,
                                                  sizeof (tIsisEvtManAAChange));

                            }
                        }

                        i1ErrCode = SNMP_SUCCESS;

                        pContext->u1QFlag |= ISIS_SYSMAA_FLAG;

                        if ((pContext->u1QFlag == ISIS_SYSALLSET_FLAG)
                            && (pContext->SysActuals.u1SysExistState
                                != ISIS_ACTIVE))
                        {
                            pContext->SysActuals.u1SysExistState =
                                ISIS_NOT_IN_SER;
                        }
                    }
                }
                break;

            default:
                break;

        }
    }

    else
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : ManualAreaAddres Entry does not exist for "
                 "the given System Index = %d ", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisManAreaAddrExistState ()\n"));

    return i1ErrCode;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IsisManAreaAddrExistState
 Input       :  The Indices
                IsisSysInstance
                IsisManAreaAddr

                The Object 
                testValIsisManAreaAddrExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisManAreaAddrExistState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                    tSNMP_OCTET_STRING_TYPE * pIsisManAreaAddr,
                                    INT4 i4TestValIsisManAreaAddrExistState)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisMAAEntry      *pMAAEntry = NULL;
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisManAreaAddrExistState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysType ()\n"));

        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index,  %d\n", i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
    }
    else
    {
        switch (i4TestValIsisManAreaAddrExistState)
        {
            case ISIS_CR_GO:

                /* Checks whether the entry already exists
                 */
                if ((pIsisManAreaAddr->i4_Length > ISIS_AREA_ADDR_LEN)
                    || (pIsisManAreaAddr->i4_Length <= 0))
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Invalid Value of ManAreaAddress Length \n"));
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    i1RetCode = SNMP_FAILURE;
                    CLI_SET_ERR (CLI_ISIS_INV_LEN);
                }

                if (IsisCtrlGetMAA (pContext, pIsisManAreaAddr->pu1_OctetList,
                                    (UINT1) pIsisManAreaAddr->i4_Length,
                                    &pMAAEntry) == ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Could Not "
                             "Create ManualAreaAddress Entry since the "
                             "Entry already exists "));
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1RetCode = SNMP_FAILURE;
                    CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
                }

                /* Checks whether number of entries already equals
                 * maximum no. of entries */

                else if (pContext->ManAddrTable.u2NumEntries ==
                         pContext->SysConfigs.u1SysMaxAA)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Number of Entries equals "
                             "maximum, cannot create anymore " "Entries \n"));

                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    i1RetCode = SNMP_FAILURE;
                    CLI_SET_ERR (CLI_ISIS_MAX_ENTRY);
                }
                break;

            case ISIS_DESTROY:

                /* if the entry doesn't exist, return failure
                 */

                if (IsisCtrlGetMAA (pContext, pIsisManAreaAddr->pu1_OctetList,
                                    (UINT1) pIsisManAreaAddr->i4_Length,
                                    &pMAAEntry) != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : ManualAreaAddress Entry does "
                             "not  exist for the given Index"));

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1RetCode = SNMP_FAILURE;

                    CLI_SET_ERR (CLI_ISIS_NO_ENTRY);
                }
                break;

            case ISIS_NOT_READY:
            case ISIS_CR_WT:
            case ISIS_ACTIVE:
            case ISIS_NOT_IN_SER:

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                         "TestValIsisManAreaAddrExistState \n"));

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetCode = SNMP_FAILURE;
                CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
                break;

            default:

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                         "TestValIsisManAreaAddrExistState \n"));

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i1RetCode = SNMP_FAILURE;
                CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
                break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisManAreaAddrExistState ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhDepv2IsisManAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisManAreaAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IsisManAreaAddrTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IsisAreaAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisAreaAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisAreaAddrTable (INT4 i4IsisSysInstance,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pIsisAreaAddr)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    INT4                i4RetVal = 0;
    UINT1               au1NullAA[ISIS_AREA_ADDR_LEN];
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisAreaAddrTable ()\n"));

    if (i4IsisSysInstance < 0)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                 "nmhValidateIndexInstanceIsisAreaAddrTable ()\n"));
        return (SNMP_FAILURE);
    }

    if ((pIsisAreaAddr->i4_Length < 0)
        || (pIsisAreaAddr->i4_Length > ISIS_AREA_ADDR_LEN))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Length of AreaAddress \n"));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

        if (i4RetVal != ISIS_SUCCESS)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                     "the Index,  %d\n", i4IsisSysInstance));
            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            MEMSET (au1NullAA, 0x00, ISIS_AREA_ADDR_LEN);
            if (MEMCMP (au1NullAA, pIsisAreaAddr->pu1_OctetList,
                        pIsisAreaAddr->i4_Length) == 0)
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Invalid Value of AreaAddress \n"));
                i1ErrCode = SNMP_FAILURE;
            }
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisAreaAddrTable ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisAreaAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisAreaAddrTable (INT4 *pi4IsisSysInstance,
                                   tSNMP_OCTET_STRING_TYPE * pIsisAreaAddr)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               au1OctetList[ISIS_AREA_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE FirstArea;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexIsisAreaAddrTable"));

    MEMSET (au1OctetList, 0x00, ISIS_AREA_ADDR_LEN);

    FirstArea.pu1_OctetList = au1OctetList;
    FirstArea.i4_Length = 0;
    if (nmhGetNextIndexIsisAreaAddrTable (0, pi4IsisSysInstance, &FirstArea,
                                          pIsisAreaAddr) == SNMP_SUCCESS)
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexIsisAreaAddrTable"));

    return (i1ErrCode);

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisAreaAddrTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisAreaAddr
                nextIsisAreaAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisAreaAddrTable (INT4 i4IsisSysInstance,
                                  INT4 *pi4NextIsisSysInstance,
                                  tSNMP_OCTET_STRING_TYPE * pIsisAreaAddr,
                                  tSNMP_OCTET_STRING_TYPE * pNextIsisAreaAddr)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4CurrIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4NextIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tSNMP_OCTET_STRING_TYPE *pAAEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetNextIndexIsisAreaAddrTable ()\n"));

    pAAEntry = pIsisAreaAddr;

    i4RetVal = IsisCtrlGetSysContext (u4CurrIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4CurrIdx, &u4NextIdx) !=
            ISIS_SUCCESS)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                     "the Index,  %u\n", u4CurrIdx));

            NMP_EE ((ISIS_LGST,
                     "NMP <X> : Exiting nmhGetNextIndexIsisAreaAddrTable"
                     "()\n"));

            return (SNMP_FAILURE);
        }
        else
        {
            u4CurrIdx = u4NextIdx;
            i4RetValue = IsisCtrlGetSysContext (u4CurrIdx, &pContext);
            MEMSET (pAAEntry->pu1_OctetList, 0, ISIS_AREA_ADDR_LEN);
        }
    }

    while (pContext != NULL)
    {
        if ((i4RetVal = nmhUtlGetNextAreaAddr (pContext, pAAEntry,
                                               pNextIsisAreaAddr))
            != ISIS_SUCCESS)
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4CurrIdx, &u4NextIdx)
                != ISIS_SUCCESS)
            {

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Could Not Find the Next Index with "
                         "the given Indices \n"));

                NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                         "nmhGetNextIndexIsisAreaAddrTable ()\n"));

                return (SNMP_FAILURE);
            }
            else
            {
                u4CurrIdx = u4NextIdx;
                i4RetValue = IsisCtrlGetSysContext (u4CurrIdx, &pContext);
                MEMSET (pAAEntry->pu1_OctetList, 0, ISIS_AREA_ADDR_LEN);
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = (INT4) u4CurrIdx;

            NMP_PT ((ISIS_LGST, "NMP <E> : The Next Indices are SysInstance : "
                     "Next Area Address : %d %x %x \n",
                     *pi4NextIsisSysInstance,
                     pNextIsisAreaAddr->pu1_OctetList[0],
                     pNextIsisAreaAddr->pu1_OctetList[1]));

            break;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexIsisAreaAddrTable ()\n"));
    UNUSED_PARAM (i4RetValue);
    return (SNMP_SUCCESS);

}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : IsisSysProtSuppTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisSysProtSuppTable
 Input       :  The Indices
                IsisSysInstance
                IsisSysProtSuppProtocol
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisSysProtSuppTable (INT4 i4IsisSysInstance,
                                              INT4 i4IsisSysProtSuppProtocol)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisSysProtSuppTable ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysType ()\n"));
        return (SNMP_FAILURE);
    }

    if ((i4IsisSysProtSuppProtocol != ISIS_OSI_SUPP)
        && (i4IsisSysProtSuppProtocol != ISIS_IPV4_SUPP)
        && (i4IsisSysProtSuppProtocol != ISIS_IPV6_PROTO_SUPP))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Protocol Supported Index. "
                 "The Index is %u\n", (UINT4) i4IsisSysProtSuppProtocol));
        i1ErrCode = SNMP_FAILURE;
    }

    else
    {
        i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);

        if (pContext == NULL)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                     "the Index,  %d\n", i4IsisSysInstance));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisSysProtSuppTable ()\n"));
    UNUSED_PARAM (i4RetValue);

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisSysProtSuppTable
 Input       :  The Indices
                IsisSysInstance
                IsisSysProtSuppProtocol
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisSysProtSuppTable (INT4 *pi4IsisSysInstance,
                                      INT4 *pi4IsisSysProtSuppProtocol)
{
    return (nmhGetNextIndexIsisSysProtSuppTable (0, pi4IsisSysInstance, 0,
                                                 pi4IsisSysProtSuppProtocol));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisSysProtSuppTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisSysProtSuppProtocol
                nextIsisSysProtSuppProtocol
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisSysProtSuppTable (INT4 i4IsisSysInstance,
                                     INT4 *pi4NextIsisSysInstance,
                                     INT4 i4IsisSysProtSuppProtocol,
                                     INT4 *pi4NextIsisSysProtSuppProtocol)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1ProtSupp = ISIS_IPV4_SUPP;
    UINT1               u1NextProtSupp = 0;
    UINT4               u4CurrIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4NextIdx = 0;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetNextIndexIsisSysProtSuppTable ()\n"));

    u1ProtSupp = (UINT1) i4IsisSysProtSuppProtocol;

    i4RetVal = IsisCtrlGetSysContext (u4CurrIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4CurrIdx, &u4NextIdx) !=
            ISIS_SUCCESS)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                     "the Index,  %u\n", u4CurrIdx));

            NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                     "nmhGetNextIndexIsisSysProtSuppTable ()\n"));

            return (SNMP_FAILURE);
        }

        else
        {
            u4CurrIdx = u4NextIdx;
            i4RetValue = IsisCtrlGetSysContext (u4CurrIdx, &pContext);
            u1ProtSupp = 0;
        }
    }

    while (pContext != NULL)
    {
        if ((i4RetVal = nmhUtlGetNextPSEntry (pContext, u1ProtSupp,
                                              &u1NextProtSupp)) != ISIS_SUCCESS)
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4CurrIdx, &u4NextIdx)
                != ISIS_SUCCESS)
            {

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Could Not Find the Next Index with "
                         "the given Indices \n"));

                NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                         "nmhGetNextIndexIsisSysProtSuppTable ()\n"));

                return (SNMP_FAILURE);

            }

            else
            {
                u1ProtSupp = 0;
                u4CurrIdx = u4NextIdx;
                i4RetValue = IsisCtrlGetSysContext (u4CurrIdx, &pContext);
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = (INT4) u4CurrIdx;
            *pi4NextIsisSysProtSuppProtocol = (INT4) u1NextProtSupp;

            NMP_PT ((ISIS_LGST, "NMP <E> : The Next Indices are SysInstance : "
                     "Next Protocol Supported : %d %d \n",
                     *pi4NextIsisSysInstance, *pi4NextIsisSysProtSuppProtocol));

            NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
                     "nmhGetNextIndexIsisSysProtSuppTable ()\n"));

            return (SNMP_SUCCESS);
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetNextIndexIsisSysProtSuppTable ()\n"));
    UNUSED_PARAM (i4RetValue);
    return (SNMP_FAILURE);

}

/****************************************************************************
 Function    :  nmhDepv2IsisSysProtSuppTable
 Input       :  The Indices
                IsisSysInstance
                IsisSysProtSuppProtocol
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IsisSysProtSuppTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisSysProtSuppExistState
 Input       :  The Indices
                IsisSysInstance
                IsisSysProtSuppProtocol

                The Object 
                retValIsisSysProtSuppExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysProtSuppExistState (INT4 i4IsisSysInstance,
                                 INT4 i4IsisSysProtSuppProtocol,
                                 INT4 *pi4RetValIsisSysProtSuppExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT1               u1PSIdx = (UINT1) i4IsisSysProtSuppProtocol;
    tIsisSysContext    *pContext = NULL;
    tIsisProtSupp      *pPSEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetIsisSysProtSuppExistState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysType ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for "
                 "the given Index\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    else
    {
        if ((i4RetVal =
             IsisCtrlGetPSEntry (pContext, u1PSIdx, &pPSEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisSysProtSuppExistState = (INT4) pPSEntry->u1ExistState;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Protocol Support Entry does not exist "
                     "for the given Index\n"));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetIsisSysProtSuppExistState ()\n"));

    return i1ErrCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIsisSysProtSuppExistState
 Input       :  The Indices
                IsisSysInstance
                IsisSysProtSuppProtocol

                The Object 
                setValIsisSysProtSuppExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSysProtSuppExistState (INT4 i4IsisSysInstance,
                                 INT4 i4IsisSysProtSuppProtocol,
                                 INT4 i4SetValIsisSysProtSuppExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT1               u1ProtSupp = (UINT1) i4IsisSysProtSuppProtocol;
    tIsisSysContext    *pContext = NULL;
    tIsisEvtPSChange   *pPSEvt = NULL;
    tIsisProtSupp      *pPSEntry = NULL;
    INT4                i4Count;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhSetIsisSysProtSuppExistState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhSetIsisSysProtSuppExistState ()\n"));
        return (SNMP_FAILURE);
    }
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist "
                 "for the given Index\n"));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        switch (i4SetValIsisSysProtSuppExistState)
        {
            case ISIS_CR_WT:
                if ((i4RetVal = IsisCtrlGetPSEntry (pContext, u1ProtSupp,
                                                    &pPSEntry)) == ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Protocol Support Entry does not "
                             "exist for the given Index\n"));
                    i1ErrCode = SNMP_FAILURE;
                    return i1ErrCode;
                }
                i4Count = IsisCtrlAddPSEntry (pContext, u1ProtSupp);
                if (i4Count != ISIS_FAILURE)
                    pContext->aProtSupp[i4Count].u1ExistState = ISIS_NOT_IN_SER;
                i1ErrCode = SNMP_SUCCESS;
                break;

            case ISIS_NOT_IN_SER:
                if ((i4RetVal = IsisCtrlGetPSEntry (pContext, u1ProtSupp,
                                                    &pPSEntry)) != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Protocol Support Entry does not "
                             "exist for the given Index\n"));
                    i1ErrCode = SNMP_FAILURE;
                    return i1ErrCode;
                }
                if (pPSEntry->u1ExistState == ISIS_ACTIVE)
                {
                    pPSEntry->u1ExistState = ISIS_NOT_IN_SER;
                    if (pContext->u1OperState == ISIS_UP)
                    {
                        pPSEvt = (tIsisEvtPSChange *)
                            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                            sizeof (tIsisEvtPSChange));
                        if (pPSEvt != NULL)
                        {
                            pPSEvt->u1EvtID = ISIS_EVT_PROT_SUPP_CHANGE;
                            pPSEvt->u1Status = ISIS_PS_DELETED;
                            pPSEvt->u1ProtSupp = u1ProtSupp;
                            IsisUtlSendEvent (pContext, (UINT1 *) pPSEvt,
                                              sizeof (tIsisEvtPSChange));
                        }
                    }
                }
                i1ErrCode = SNMP_SUCCESS;
                break;

            case ISIS_ACTIVE:
                if ((i4RetVal = IsisCtrlGetPSEntry (pContext, u1ProtSupp,
                                                    &pPSEntry)) != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Protocol Support Entry does not "
                             "exist for the given Index\n"));
                    i1ErrCode = SNMP_FAILURE;
                    return i1ErrCode;
                }
                pPSEntry->u1ExistState = ISIS_ACTIVE;
                if (pContext->u1OperState == ISIS_UP)
                {
                    pPSEvt = (tIsisEvtPSChange *)
                        ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                        sizeof (tIsisEvtPSChange));
                    if (pPSEvt != NULL)
                    {
                        pPSEvt->u1EvtID = ISIS_EVT_PROT_SUPP_CHANGE;
                        pPSEvt->u1Status = ISIS_PS_ADDED;
                        pPSEvt->u1ProtSupp = u1ProtSupp;
                        IsisUtlSendEvent (pContext, (UINT1 *) pPSEvt,
                                          sizeof (tIsisEvtPSChange));
                    }
                }
                i1ErrCode = SNMP_SUCCESS;
                break;
            case ISIS_DESTROY:
                if ((i4RetVal = IsisCtrlGetPSEntry (pContext, u1ProtSupp,
                                                    &pPSEntry)) != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Protocol Support Entry does not "
                             "exist for the given Index\n"));
                    i1ErrCode = SNMP_FAILURE;
                }

                else
                {
                    if ((i4RetVal = IsisCtrlDelPSEntry (pContext, u1ProtSupp))
                        != ISIS_SUCCESS)
                    {
                        i1ErrCode = SNMP_FAILURE;
                    }

                    else
                    {
                        i1ErrCode = SNMP_SUCCESS;

                        /* Forming Protocol Support Changed Event and sending
                         * to control queue
                         */

                        if (pContext->u1OperState == ISIS_UP)
                        {
                            pPSEvt = (tIsisEvtPSChange *)
                                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtPSChange));
                            if (pPSEvt != NULL)
                            {
                                pPSEvt->u1EvtID = ISIS_EVT_PROT_SUPP_CHANGE;
                                pPSEvt->u1Status = ISIS_PS_DELETED;
                                pPSEvt->u1ProtSupp = u1ProtSupp;

                                IsisUtlSendEvent (pContext, (UINT1 *) pPSEvt,
                                                  sizeof (tIsisEvtPSChange));
                            }
                        }
                    }

                }
                break;

            case ISIS_CR_GO:

                if ((i4RetVal = IsisCtrlGetPSEntry (pContext, u1ProtSupp,
                                                    &pPSEntry)) == ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Protocol Support Entry already Exists "
                             "for the given Index"));
                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    i4Count = IsisCtrlAddPSEntry (pContext, u1ProtSupp);
                    if (i4Count != ISIS_FAILURE)
                        pContext->aProtSupp[i4Count].u1ExistState = ISIS_ACTIVE;
                    /* Forming Protocol Support Changed Event and sending to
                     * control queue
                     */

                    if (pContext->u1OperState == ISIS_UP)
                    {
                        pPSEvt = (tIsisEvtPSChange *)
                            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                            sizeof (tIsisEvtPSChange));
                        if (pPSEvt != NULL)
                        {
                            pPSEvt->u1EvtID = ISIS_EVT_PROT_SUPP_CHANGE;
                            pPSEvt->u1Status = ISIS_PS_ADDED;
                            pPSEvt->u1ProtSupp = u1ProtSupp;

                            IsisUtlSendEvent (pContext, (UINT1 *) pPSEvt,
                                              sizeof (tIsisEvtPSChange));

                        }
                    }
                    i1ErrCode = SNMP_SUCCESS;
                }
                break;

            default:
                break;

        }
    }
    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhSetIsisSysProtSuppExistState ()\n"));

    return i1ErrCode;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IsisSysProtSuppExistState
 Input       :  The Indices
                IsisSysInstance
                IsisSysProtSuppProtocol

                The Object 
                testValIsisSysProtSuppExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSysProtSuppExistState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                    INT4 i4IsisSysProtSuppProtocol,
                                    INT4 i4TestValIsisSysProtSuppExistState)
{
    INT1                i1RetCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PSIdx = (UINT1) i4IsisSysProtSuppProtocol;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisProtSupp      *pPSEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSysProtSuppExistState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhTestv2IsisSysType ()\n"));
        CLI_SET_ERR (CLI_ISIS_INV_INSTANCE);
        return (SNMP_FAILURE);
    }
    if ((i4IsisSysProtSuppProtocol != ISIS_OSI_SUPP)
        && (i4IsisSysProtSuppProtocol != ISIS_IPV4_SUPP)
        && (i4IsisSysProtSuppProtocol != ISIS_IPV6_PROTO_SUPP))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index,  %u\n", (UINT4) i4IsisSysInstance));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index,  %u\n", (UINT4) i4IsisSysInstance));

        i1RetCode = SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
    }
    else
    {
        switch (i4TestValIsisSysProtSuppExistState)
        {
            case ISIS_CR_WT:
            case ISIS_CR_GO:

                /* Checks whether the entry already exists
                 */

                if (IsisCtrlGetPSEntry (pContext, u1PSIdx, &pPSEntry)
                    == ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Protocol Support Entry already "
                             "exists for the given Index"));
                    CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1RetCode = SNMP_FAILURE;
                }

                else
                {
                    i1RetCode = SNMP_SUCCESS;
                }
                break;
            case ISIS_NOT_IN_SER:
            case ISIS_DESTROY:
            case ISIS_ACTIVE:

                /* if the entry doesn't exist, return failure
                 */

                if (IsisCtrlGetPSEntry (pContext, u1PSIdx, &pPSEntry)
                    != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Protocol Support Entry does not "
                             "exist for the given Index\n"));

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    i1RetCode = SNMP_FAILURE;
                    CLI_SET_ERR (CLI_ISIS_NO_ENTRY);
                }

                else
                {
                    i1RetCode = SNMP_SUCCESS;
                }
                break;

            case ISIS_NOT_READY:

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                         "TestValIsisSysProtSuppExistState \n"));

                CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetCode = SNMP_FAILURE;
                break;

            default:

                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                         "TestValIsisSysProtSuppExistState \n"));
                CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i1RetCode = SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSysProtSuppExistState ()\n"));

    return i1RetCode;
}

/* LOW LEVEL Routines for Table : IsisSummAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisSummAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisSummAddrTable (INT4 i4IsisSysInstance,
                                           INT4 i4IsisSummAddressType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pIsisSummAddress,
                                           UINT4 u4IsisSummAddrPrefixLen)
{
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT1               au1NullSA[ISIS_MAX_IP_ADDR_LEN];

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisSummAddrTable ()\n"));

    UNUSED_PARAM (i4IsisSysInstance);
    if ((pIsisSummAddress->i4_Length != ISIS_MAX_IPV4_ADDR_LEN) &&
        (pIsisSummAddress->i4_Length != ISIS_MAX_IPV6_ADDR_LEN))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Length of Summary Address \n"));
        i1ErrCode = SNMP_FAILURE;
    }

    MEMSET (au1NullSA, 0x00, ISIS_MAX_IP_ADDR_LEN);
    if (MEMCMP (au1NullSA, pIsisSummAddress->pu1_OctetList,
                MEM_MAX_BYTES ((UINT4) pIsisSummAddress->i4_Length,
                               sizeof (au1NullSA))) == 0)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Summary Address \n"));
        i1ErrCode = SNMP_FAILURE;
    }

    if ((i4IsisSummAddressType != ISIS_IPV6)
        && (i4IsisSummAddressType != ISIS_IPV4))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "Summary Address Type\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    if (((i4IsisSummAddressType == ISIS_IPV4)
         && (u4IsisSummAddrPrefixLen > ISIS_MAX_IPV4_PREFLEN))
        || ((i4IsisSummAddressType == ISIS_IPV6)
            && (u4IsisSummAddrPrefixLen > ISIS_MAX_IPV6_PREFLEN)))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of Prefix Length \n"));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisSummAddrTable ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisSummAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisSummAddrTable (INT4 *pi4IsisSysInstance,
                                   INT4 *pi4IsisSummAddressType,
                                   tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                                   UINT4 *pu4IsisSummAddrPrefixLen)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    UINT1               au1OctetList[ISIS_MAX_IP_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE FirstSA;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhGetFirstIndexIsisSummAddrTable ()\n"));

    MEMSET (au1OctetList, 0x00, ISIS_MAX_IP_ADDR_LEN);

    FirstSA.pu1_OctetList = au1OctetList;
    FirstSA.i4_Length = ISIS_MAX_IP_ADDR_LEN;

    if (nmhGetNextIndexIsisSummAddrTable (0, pi4IsisSysInstance, 0,
                                          pi4IsisSummAddressType,
                                          &FirstSA, pIsisSummAddress,
                                          0, pu4IsisSummAddrPrefixLen)
        == SNMP_SUCCESS)
    {
        i1ErrCode = SNMP_SUCCESS;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhGetFirstIndexIsisSummAddrTable ()\n"));

    return (i1ErrCode);
}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisSummAddrTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisSummAddressType
                nextIsisSummAddressType
                IsisSummAddress
                nextIsisSummAddress
                IsisSummAddrPrefixLen
                nextIsisSummAddrPrefixLen
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisSummAddrTable (INT4 i4IsisSysInstance,
                                  INT4 *pi4NextIsisSysInstance,
                                  INT4 i4IsisSummAddressType,
                                  INT4 *pi4NextIsisSummAddressType,
                                  tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextIsisSummAddress,
                                  UINT4 u4IsisSummAddrPrefixLen,
                                  UINT4 *pu4NextIsisSummAddrPrefixLen)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1SAType = (UINT1) i4IsisSummAddressType;
    UINT1              *pu1NextSAType = (UINT1 *) pi4NextIsisSummAddressType;
    UINT1               u1PrefLen = (UINT1) u4IsisSummAddrPrefixLen;
    UINT1               u1NextPrefLen = 0;
    UINT4               u4CurrIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4NextIdx = 0;
    tIsisSysContext    *pContext = NULL;
    tSNMP_OCTET_STRING_TYPE *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetNextIndexIsisSummAddrTable ()\n"));

    pSAEntry = pIsisSummAddress;

    i4RetVal = IsisCtrlGetSysContext (u4CurrIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4CurrIdx, &u4NextIdx) !=
            ISIS_SUCCESS)
        {

            NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                     "the Index,  %u\n", u4CurrIdx));

            NMP_EE ((ISIS_LGST, "NMP <X> :"
                     " Exiting nmhGetNextIndexIsisSummAddrTable ()\n"));

            return (SNMP_FAILURE);
        }
        else
        {
            u4CurrIdx = u4NextIdx;
            i4RetValue = IsisCtrlGetSysContext (u4CurrIdx, &pContext);
            u1SAType = 0;
            MEMSET (pSAEntry->pu1_OctetList, 0, ISIS_MAX_IP_ADDR_LEN);
            u1PrefLen = 0;
        }
    }

    while (pContext != NULL)
    {
        if ((i4RetVal = nmhUtlGetNextSummAddr (pContext,
                                               u1SAType, pu1NextSAType,
                                               pSAEntry, pNextIsisSummAddress,
                                               u1PrefLen,
                                               &u1NextPrefLen)) != ISIS_SUCCESS)
        {
            if (nmhUtlGetNextIndexIsisSysTable (u4CurrIdx, &u4NextIdx)
                != ISIS_SUCCESS)
            {

                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Could Not Find the Next Index with "
                         "the given Indices \n"));

                NMP_EE ((ISIS_LGST, "NMP <X> :"
                         " Exiting nmhGetNextIndexIsisSummAddrTable \n"));

                return (SNMP_FAILURE);
            }
            else
            {
                u4CurrIdx = u4NextIdx;
                i4RetValue = IsisCtrlGetSysContext (u4CurrIdx, &pContext);
                u1SAType = 0;
                MEMSET (pSAEntry->pu1_OctetList, 0, ISIS_MAX_IP_ADDR_LEN);
                u1PrefLen = 0;
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = (INT1) u4CurrIdx;
            *pi4NextIsisSummAddressType = (INT4) *pu1NextSAType;
            *pu4NextIsisSummAddrPrefixLen = u1NextPrefLen;

            NMP_PT ((ISIS_LGST, "NMP <E> : The Next Indices are SysInstance "
                     " : %d  Next SA Type : %d Next Address :%x %x %x %x \n"
                     " Next Prefix Length : %d \n",
                     *pi4NextIsisSysInstance, *pi4NextIsisSummAddressType,
                     pNextIsisSummAddress->pu1_OctetList[0],
                     pNextIsisSummAddress->pu1_OctetList[1],
                     pNextIsisSummAddress->pu1_OctetList[2],
                     pNextIsisSummAddress->pu1_OctetList[3],
                     *pu4NextIsisSummAddrPrefixLen));
            break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetNextIndexIsisSummAddrTable ()\n"));
    UNUSED_PARAM (i4RetValue);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhDepv2IsisSummAddrTable
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IsisSummAddrTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisSummAddrExistState
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                retValIsisSummAddrExistState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSummAddrExistState (INT4 i4IsisSysInstance,
                              INT4 i4IsisSummAddressType,
                              tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                              UINT4 u4IsisSummAddrPrefixLen,
                              INT4 *pi4RetValIsisSummAddrExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1SAType = (UINT1) i4IsisSummAddressType;
    UINT1               u4InstIdx = (UINT1) i4IsisSysInstance;
    UINT1               u1PrefixLen = (UINT1) u4IsisSummAddrPrefixLen;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSummAddrExistState ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> :  System Entry does not exist for "
                 "the given Index %d \n", i4IsisSysInstance));

        i1ErrCode = SNMP_FAILURE;
    }

    else
    {
        if ((i4RetVal =
             IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                            u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisSummAddrExistState = (INT1) (pSAEntry->u1ExistState);
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Summary Address Entry does not exist "
                     "for the given Index\n"));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSummAddrExistState ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSummAddrAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                retValIsisSummAddrAdminState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSummAddrAdminState (INT4 i4IsisSysInstance,
                              INT4 i4IsisSummAddressType,
                              tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                              UINT4 u4IsisSummAddrPrefixLen,
                              INT4 *pi4RetValIsisSummAddrAdminState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PrefixLen = (UINT1) u4IsisSummAddrPrefixLen;
    UINT1               u1SAType = (UINT1) i4IsisSummAddressType;
    UINT1               u4InstIdx = (UINT1) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSummAddrAdminState ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for "
                 "the given Index\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    else
    {
        if ((i4RetVal =
             IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                            u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisSummAddrAdminState = (INT1) pSAEntry->u1AdminState;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Summary Address Entry does not exist "
                     "for the given Index\n"));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSummAddrAdminState ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSummAddrMetric
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                retValIsisSummAddrMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSummAddrMetric (INT4 i4IsisSysInstance, INT4 i4IsisSummAddressType,
                          tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                          UINT4 u4IsisSummAddrPrefixLen,
                          INT4 *pi4RetValIsisSummAddrMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PrefixLen = (UINT1) u4IsisSummAddrPrefixLen;
    UINT1               u1SAType = (UINT1) i4IsisSummAddressType;
    UINT1               u4InstIdx = (UINT1) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSummAddrMetric ()\n"));

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for "
                 "the given Index\n"));
        i1ErrCode = SNMP_FAILURE;
    }

    else
    {
        if ((i4RetVal =
             IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                            u1SAType, u1PrefixLen, &pSAEntry)) == ISIS_SUCCESS)
        {
            *pi4RetValIsisSummAddrMetric = (INT1) (pSAEntry->Metric[0]);
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            NMP_PT ((ISIS_LGST,
                     "NMP <E> : Summary Address Entry does not exist "
                     "for the given Index\n"));
            i1ErrCode = SNMP_FAILURE;
        }
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSummAddrMetric ()\n"));

    return i1ErrCode;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIsisSummAddrExistState
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                setValIsisSummAddrExistState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSummAddrExistState (INT4 i4IsisSysInstance,
                              INT4 i4IsisSummAddressType,
                              tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                              UINT4 u4IsisSummAddrPrefixLen,
                              INT4 i4SetValIsisSummAddrExistState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PrefixLen = (UINT1) u4IsisSummAddrPrefixLen;
    UINT1               u1SAType = (UINT1) i4IsisSummAddressType;
    UINT1               u4InstIdx = (UINT1) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisEvtSummAddrChg *pSAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSummAddrExistState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhSetIsisSummAddrExistState ()\n"));
        return (SNMP_FAILURE);
    }
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST,
                 "NMP <E> : System Entry does not exist for "
                 "the given Index\n"));
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        switch (i4SetValIsisSummAddrExistState)
        {
            case ISIS_CR_GO:
            case ISIS_CR_WT:

                if ((i4RetVal =
                     IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                                    u1SAType, u1PrefixLen, &pSAEntry))
                    == ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Could Not Create Summary Address "
                             "Entry since the Entry already exists "));

                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    pSAEntry = (tIsisSAEntry *) ISIS_MEM_ALLOC (ISIS_BUF_SATE,
                                                                sizeof
                                                                (tIsisSAEntry));
                    if (pSAEntry == NULL)
                    {
                        PANIC ((ISIS_LGST,
                                ISIS_MEM_ALLOC_FAIL " : Summary Address\n"));
                        DETAIL_FAIL (ISIS_CR_MODULE);
                        /* Forming Memory Resource Failure Event and"
                         * sending to control queue
                         */

                        IsisUtlFormResFailEvt (pContext, ISIS_BUF_SATE);

                        i1ErrCode = SNMP_FAILURE;
                    }
                    else
                    {
                        MEMCPY (pSAEntry->au1SummAddr,
                                pIsisSummAddress->pu1_OctetList,
                                pIsisSummAddress->i4_Length);

                        pSAEntry->u1AddrType = u1SAType;

                        pSAEntry->u1PrefixLen = u1PrefixLen;

                        /* Filling default values for all supported Metric
                         * Values
                         */

                        IsisUtlFillMetDefVal (pContext, pSAEntry,
                                              ISIS_SUMMARY_REC);

                        pSAEntry->u1AdminState = ISIS_SUMM_ADMIN_OFF;
                        IsisCtrlAddSA (pContext, pSAEntry);
                        i1ErrCode = SNMP_SUCCESS;

                        if (i4SetValIsisSummAddrExistState == ISIS_CR_WT)
                        {
                            pSAEntry->u1ExistState = ISIS_NOT_IN_SER;
                        }
                        else
                        {
                            pSAEntry->u1ExistState = ISIS_ACTIVE;
                        }
                    }
                }
                break;

            case ISIS_ACTIVE:
                if ((i4RetVal =
                     IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                                    u1SAType, u1PrefixLen,
                                    &pSAEntry)) != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does "
                             "not exist for the given Index \n"));

                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    if (pSAEntry->u1ExistState == ISIS_NOT_IN_SER)
                    {
                        pSAEntry->u1ExistState = ISIS_ACTIVE;
                        i1ErrCode = SNMP_SUCCESS;

                        /* Summary Address Change Event
                         */

                        if ((pContext->u1OperState == ISIS_UP)
                            && (pSAEntry->u1AdminState != ISIS_SUMM_ADMIN_OFF))
                        {
                            pSAEvt = (tIsisEvtSummAddrChg *)
                                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtSummAddrChg));
                            if (pSAEvt != NULL)
                            {
                                pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                                pSAEvt->u1Status = ISIS_SUMM_ADDR_ADD;
                                pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                                pSAEvt->u1CurrAdminState =
                                    pSAEntry->u1AdminState;
                                ISIS_SET_METRIC (pContext, pSAEvt, pSAEntry);
                                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                                {
                                    MEMCPY (pSAEvt->au1IPAddr,
                                            pSAEntry->au1SummAddr,
                                            ISIS_MAX_IPV4_ADDR_LEN);
                                    pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                                }
                                else
                                {
                                    MEMCPY (pSAEvt->au1IPAddr,
                                            pSAEntry->au1SummAddr,
                                            ISIS_MAX_IPV6_ADDR_LEN);
                                    pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                                }
                                IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                                  sizeof (tIsisEvtSummAddrChg));
                            }
                        }
                    }
                    else
                    {
                        NMP_PT ((ISIS_LGST,
                                 "NMP <E> : Could not Set SysExistState to "
                                 "Active since RowStatus was not in "
                                 "NOT_IN_SERVICE state\n"));
                        i1ErrCode = SNMP_FAILURE;
                    }

                }
                break;

            case ISIS_DESTROY:
                if ((i4RetVal =
                     IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                                    u1SAType, u1PrefixLen, &pSAEntry))
                    != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does "
                             "not exist for the given Index"));

                    i1ErrCode = SNMP_FAILURE;
                }
                else
                {
                    i1ErrCode = SNMP_SUCCESS;
                    /* Summary Address Change Event
                     */

                    if ((pSAEntry->u1ExistState == ISIS_ACTIVE) &&
                        (pSAEntry->u1AdminState != ISIS_SUMM_ADMIN_OFF))
                    {

                        pSAEvt = (tIsisEvtSummAddrChg *)
                            ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                            sizeof (tIsisEvtSummAddrChg));
                        if (pSAEvt != NULL)
                        {
                            pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                            pSAEvt->u1Status = ISIS_SUMM_ADDR_DEL;
                            pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                            pSAEvt->u1CurrAdminState = pSAEntry->u1AdminState;
                            if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                            {
                                MEMCPY (pSAEvt->au1IPAddr,
                                        pSAEntry->au1SummAddr,
                                        ISIS_MAX_IPV4_ADDR_LEN);
                                pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                            }
                            else
                            {
                                MEMCPY (pSAEvt->au1IPAddr,
                                        pSAEntry->au1SummAddr,
                                        ISIS_MAX_IPV6_ADDR_LEN);
                                pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                            }
                            IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                              sizeof (tIsisEvtSummAddrChg));
                        }
                    }
                    IsisCtrlDelSA (pContext, pSAEntry->au1SummAddr,
                                   pSAEntry->u1AddrType, pSAEntry->u1PrefixLen);

                }
                break;

            case ISIS_NOT_IN_SER:

                if ((i4RetVal =
                     IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                                    u1SAType, u1PrefixLen, &pSAEntry))
                    != ISIS_SUCCESS)
                {
                    NMP_PT ((ISIS_LGST,
                             "NMP <E> : Could Not set the Exist State "
                             "since the Entry doesnt exist "));
                    return (SNMP_FAILURE);

                }
                else
                {
                    pSAEntry->u1ExistState = ISIS_NOT_IN_SER;
                    i1ErrCode = SNMP_SUCCESS;
                }

                if ((pContext->u1OperState == ISIS_UP)
                    && (pSAEntry->u1AdminState != ISIS_SUMM_ADMIN_OFF))
                {
                    pSAEvt = (tIsisEvtSummAddrChg *)
                        ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                        sizeof (tIsisEvtSummAddrChg));
                    if (pSAEvt != NULL)
                    {
                        pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                        pSAEvt->u1Status = ISIS_SUMM_ADDR_DEL;
                        pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                        if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                        {
                            MEMCPY (pSAEvt->au1IPAddr,
                                    pSAEntry->au1SummAddr,
                                    ISIS_MAX_IPV4_ADDR_LEN);
                            pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                        }
                        else
                        {
                            MEMCPY (pSAEvt->au1IPAddr,
                                    pSAEntry->au1SummAddr,
                                    ISIS_MAX_IPV6_ADDR_LEN);
                            pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                        }
                        IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                          sizeof (tIsisEvtSummAddrChg));
                    }
                }
                break;

            default:
                break;

        }

    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSummAddrExistState ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSummAddrAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                setValIsisSummAddrAdminState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSummAddrAdminState (INT4 i4IsisSysInstance,
                              INT4 i4IsisSummAddressType,
                              tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                              UINT4 u4IsisSummAddrPrefixLen,
                              INT4 i4SetValIsisSummAddrAdminState)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PrevAdminState = ISIS_SUMM_ADMIN_OFF;
    UINT1               u1PrefixLen = (UINT1) u4IsisSummAddrPrefixLen;
    UINT1               u1SAType = (UINT1) i4IsisSummAddressType;
    UINT1               u4InstIdx = (UINT1) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisEvtSummAddrChg *pSAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSummAddrAdminState ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhSetIsisSummAddrAdminState ()\n"));
        return (SNMP_FAILURE);
    }
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index,  %d\n", i4IsisSysInstance));
        return (SNMP_FAILURE);
    }

    if ((i4RetVal = IsisCtrlGetSA (pContext,
                                   pIsisSummAddress->pu1_OctetList,
                                   u1SAType, u1PrefixLen, &pSAEntry))
        == ISIS_FAILURE)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Summary Address Entry does not exist "
                 "for the given Index\n"));
        return (SNMP_FAILURE);
    }
    else
    {
        u1PrevAdminState = pSAEntry->u1AdminState;
        pSAEntry->u1AdminState = (UINT1) i4SetValIsisSummAddrAdminState;
        i1ErrCode = SNMP_SUCCESS;

        /* Summary Address Change Event
         */
#ifdef ISIS_FT_ENABLED
        if (((pContext->u1OperState == ISIS_UP) ||
             (ISIS_EXT_IS_FT_STATE () == ISIS_FT_STANDBY))
            && (pSAEntry->u1ExistState == ISIS_ACTIVE))
#else
        if ((pContext->u1OperState == ISIS_UP)
            && (pSAEntry->u1ExistState == ISIS_ACTIVE))
#endif
        {
            pSAEvt = (tIsisEvtSummAddrChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSummAddrChg));
            if (pSAEvt != NULL)
            {
                pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                pSAEvt->u1Status = ISIS_SUMM_ADDR_ADMIN_CHG;
                pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                pSAEvt->u1PrevAdminState = u1PrevAdminState;
                pSAEvt->u1CurrAdminState = pSAEntry->u1AdminState;
                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV4_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                }
                else
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV6_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                }
                ISIS_SET_METRIC (pContext, pSAEvt, pSAEntry);
                IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                  sizeof (tIsisEvtSummAddrChg));
            }
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSummAddrAdminState ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhSetIsisSummAddrMetric
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                setValIsisSummAddrMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIsisSummAddrMetric (INT4 i4IsisSysInstance, INT4 i4IsisSummAddressType,
                          tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                          UINT4 u4IsisSummAddrPrefixLen,
                          INT4 i4SetValIsisSummAddrMetric)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PrefixLen = (UINT1) u4IsisSummAddrPrefixLen;
    UINT1               u1SAType = (UINT1) i4IsisSummAddressType;
    UINT1               u4InstIdx = (UINT1) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;
    tIsisEvtSummAddrChg *pSAEvt = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhSetIsisSummAddrMetric ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSummAddrMetric ()\n"));
        return (SNMP_FAILURE);
    }
#ifdef ISIS_FT_ENABLED
    if (ISIS_IS_SYSTEM_ACTIVE () != ISIS_TRUE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4RetVal =
         IsisCtrlGetSysContext (u4InstIdx, &pContext)) != ISIS_SUCCESS)
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index,  %d\n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }
    else if ((i4RetVal =
              IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                             u1SAType, u1PrefixLen, &pSAEntry)) != ISIS_SUCCESS)
    {
        i1ErrCode = SNMP_FAILURE;
    }
    else
    {
        pSAEntry->Metric[0] = (UINT1) i4SetValIsisSummAddrMetric;
        i1ErrCode = SNMP_SUCCESS;

        /* Summary Address Change Event
         */

        if (((pContext->u1OperState == ISIS_UP)
             && (pSAEntry->u1ExistState == ISIS_ACTIVE))
            && (pSAEntry->u1AdminState != ISIS_SUMM_ADMIN_OFF))
        {
            pSAEvt = (tIsisEvtSummAddrChg *)
                ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtSummAddrChg));
            if (pSAEvt != NULL)
            {
                pSAEvt->u1EvtID = ISIS_EVT_SUMM_ADDR_CHANGE;
                pSAEvt->u1Status = ISIS_SUMM_ADDR_METRIC_CHG;
                pSAEvt->u1PrefixLen = pSAEntry->u1PrefixLen;
                pSAEvt->u1CurrAdminState = pSAEntry->u1AdminState;
                pSAEvt->Metric[0] = pSAEntry->Metric[0];
                if (pSAEntry->u1AddrType == ISIS_ADDR_IPV4)
                {
                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV4_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV4;
                }
                else
                {

                    MEMCPY (pSAEvt->au1IPAddr,
                            pSAEntry->au1SummAddr, ISIS_MAX_IPV6_ADDR_LEN);
                    pSAEvt->u1AddrType = ISIS_ADDR_IPV6;
                }

                IsisUtlSendEvent (pContext, (UINT1 *) pSAEvt,
                                  sizeof (tIsisEvtSummAddrChg));
            }
        }
    }
    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhSetIsisSummAddrMetric ()\n"));

    return i1ErrCode;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IsisSummAddrExistState
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                testValIsisSummAddrExistState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSummAddrExistState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                 INT4 i4IsisSummAddressType,
                                 tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                                 UINT4 u4IsisSummAddrPrefixLen,
                                 INT4 i4TestValIsisSummAddrExistState)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT1               u1PrefixLen = (UINT1) u4IsisSummAddrPrefixLen;
    UINT1               u1SAType = (UINT1) i4IsisSummAddressType;
    tIsisSysContext    *pContext = NULL;
    tIsisSAEntry       *pSAEntry = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSummAddrExistState ()\n"));

    i1RetCode =
        IsisNmhValSATable (pu4ErrorCode, i4IsisSysInstance,
                           i4IsisSummAddressType, pIsisSummAddress,
                           (INT4) u4IsisSummAddrPrefixLen, ISIS_TRUE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSummAddrExistState ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1RetCode;
    }

    i4RetVal = (INT4) IsisCtrlGetSysContext (i4IsisSysInstance, &pContext);

    if (i4RetVal != ISIS_SUCCESS)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSummAddrExistState ()\n"));

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValIsisSummAddrExistState)
    {
        case ISIS_CR_WT:
        case ISIS_CR_GO:

            /* Checks whether the entry already exists
             */

            if (IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                               u1SAType, u1PrefixLen, &pSAEntry)
                == ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Could Not Create Summary Address"
                         " Entry since the entry already exists "));

                CLI_SET_ERR (CLI_ISIS_ENTRY_EXIST);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                i1RetCode = SNMP_FAILURE;
            }
            else
            {
                i1RetCode = SNMP_SUCCESS;
            }
            break;

        case ISIS_NOT_IN_SER:

            /* if the entry doesn't exist, return failure
             */

            if (IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                               u1SAType, u1PrefixLen, &pSAEntry)
                != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Summary Address Entry does not "
                         "exist for the given Inidex\n"));

                CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetCode = SNMP_FAILURE;
            }
            else if (pSAEntry->u1ExistState == ISIS_NOT_READY)
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                         "TestValIsisSummAddrSuppExistState \n"));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
                i1RetCode = SNMP_FAILURE;
            }
            else
            {
                i1RetCode = SNMP_SUCCESS;
            }
            break;

        case ISIS_NOT_READY:

            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                     "TestValIsisSummAddrSuppExistState \n"));

            CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1RetCode = SNMP_FAILURE;
            break;

        case ISIS_ACTIVE:
        case ISIS_DESTROY:
            if (IsisCtrlGetSA (pContext, pIsisSummAddress->pu1_OctetList,
                               u1SAType, u1PrefixLen, &pSAEntry)
                != ISIS_SUCCESS)
            {
                NMP_PT ((ISIS_LGST,
                         "NMP <E> : Summary Address Entry does not "
                         "exist for the given Index\n"));
                CLI_SET_ERR (CLI_ISIS_INVALID_INDEX);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                i1RetCode = SNMP_FAILURE;
            }
            break;

        default:
            NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                     "TestValIsisSummAddrSuppExistState \n"));
            CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetCode = SNMP_FAILURE;

    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSummAddrExistState ()\n"));

    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSummAddrAdminState
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                testValIsisSummAddrAdminState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSummAddrAdminState (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                                 INT4 i4IsisSummAddressType,
                                 tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                                 UINT4 u4IsisSummAddrPrefixLen,
                                 INT4 i4TestValIsisSummAddrAdminState)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhTestv2IsisSummAddrAdminState ()\n"));

    i1RetCode =
        IsisNmhValSATable (pu4ErrorCode, i4IsisSysInstance,
                           i4IsisSummAddressType, pIsisSummAddress,
                           (INT4) u4IsisSummAddrPrefixLen, ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSummAddrAdminState ()\n"));
        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
        return i1RetCode;
    }

    if ((i4TestValIsisSummAddrAdminState != ISIS_SUMM_ADMIN_L1)
        && (i4TestValIsisSummAddrAdminState != ISIS_SUMM_ADMIN_L2)
        && (i4TestValIsisSummAddrAdminState != ISIS_SUMM_ADMIN_L12)
        && (i4TestValIsisSummAddrAdminState != ISIS_SUMM_ADMIN_OFF))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "TestValIsisSummAddrSuppAdminState \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetCode = SNMP_FAILURE;

        CLI_SET_ERR (CLI_ISIS_INVALID_INPUT);
    }

    i4RetValue = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);

    if (((pContext->SysActuals.u1SysType == ISIS_LEVEL1)
         && (i4TestValIsisSummAddrAdminState == ISIS_SUMM_ADMIN_L2))
        || ((pContext->SysActuals.u1SysType == ISIS_LEVEL2)
            && (i4TestValIsisSummAddrAdminState == ISIS_SUMM_ADMIN_L1)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetCode = SNMP_FAILURE;

        CLI_SET_ERR (CLI_ISIS_INV_LEVEL);
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhTestv2IsisSummAddrAdminState ()\n"));
    UNUSED_PARAM (i4RetValue);
    return i1RetCode;
}

/****************************************************************************
 Function    :  nmhTestv2IsisSummAddrMetric
 Input       :  The Indices
                IsisSysInstance
                IsisSummAddressType
                IsisSummAddress
                IsisSummAddrPrefixLen

                The Object 
                testValIsisSummAddrMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IsisSummAddrMetric (UINT4 *pu4ErrorCode, INT4 i4IsisSysInstance,
                             INT4 i4IsisSummAddressType,
                             tSNMP_OCTET_STRING_TYPE * pIsisSummAddress,
                             UINT4 u4IsisSummAddrPrefixLen,
                             INT4 i4TestValIsisSummAddrMetric)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhTestv2IsisSummAddrMetric ()\n"));

    i1RetCode =
        IsisNmhValSATable (pu4ErrorCode, i4IsisSysInstance,
                           i4IsisSummAddressType, pIsisSummAddress,
                           (INT4) u4IsisSummAddrPrefixLen, ISIS_FALSE);
    if (i1RetCode == SNMP_FAILURE)
    {
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhTestv2IsisSummAddrMetric ()\n"));
        return i1RetCode;
    }

    /* Default metric is not supported for Multi-topology ISIS. Hence
     * verifying if MT support is enabled or not*/
    if ((IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext) ==
         ISIS_SUCCESS) && (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        NMP_PT ((ISIS_LGST, "NMP <E> : Default metric not supported "
                 "for Multi-topology ISIS !!! \n"));
        CLI_SET_ERR (CLI_ISIS_MET_NOT_SUPP_MT);
        return SNMP_FAILURE;
    }

    if ((i4TestValIsisSummAddrMetric >= ISIS_LL_CKTL_MIN_METRIC)
        && (i4TestValIsisSummAddrMetric <= ISIS_LL_CKTL_MAX_METRIC))
    {
        i1RetCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid Value of "
                 "i4TestValIsisSummAddrMetric\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ISIS_INV_DEF_MET);
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhTestv2IsisSummAddrMetric ()\n"));

    return i1RetCode;

}

/* LOW LEVEL Routines for Table : IsisSysStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisSysStatsTable
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisSysStatsTable (INT4 i4IsisSysInstance,
                                           INT4 i4IsisSysStatLevel)
{
    INT1                i1RetCode = SNMP_SUCCESS;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered "
             "nmhValidateIndexInstanceIsisSysStatsTable ()\n"));

    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid SysInstance Index. "
                 "The Index is %d\n", i4IsisSysInstance));
        i1RetCode = SNMP_FAILURE;
    }

    if ((i4IsisSysStatLevel != ISIS_LEVEL1)
        && (i4IsisSysStatLevel != ISIS_LEVEL2))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid SysStatLevel Index. "
                 "The Index is %d\n", i4IsisSysStatLevel));
        i1RetCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting "
             "nmhValidateIndexInstanceIsisSysStatsTable ()\n"));

    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisSysStatsTable
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisSysStatsTable (INT4 *pi4IsisSysInstance,
                                   INT4 *pi4IsisSysStatLevel)
{
    return (nmhGetNextIndexIsisSysStatsTable (0, pi4IsisSysInstance, 0,
                                              pi4IsisSysStatLevel));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisSysStatsTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
                IsisSysStatLevel
                nextIsisSysStatLevel
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisSysStatsTable (INT4 i4IsisSysInstance,
                                  INT4 *pi4NextIsisSysInstance,
                                  INT4 i4IsisSysStatLevel,
                                  INT4 *pi4NextIsisSysStatLevel)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT1                i1ErrCode = SNMP_SUCCESS;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4SysStatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Entered nmhGetNextIndexIsisSysStatsTable ()\n"));

    if (IsisCtrlGetSysContext (u4InstIdx, &pContext) == ISIS_FAILURE)
    {
        if (nmhUtlGetNextIndexIsisSysTable (u4InstIdx,
                                            &u4InstIdx) == ISIS_FAILURE)
        {
            NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next "
                     "Index with the given Indices \n"));

            i1ErrCode = SNMP_FAILURE;
        }
        else
        {
            u4SysStatLevel = 0;
            i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);
        }
    }

    while ((pContext != NULL) && (i1ErrCode == SNMP_SUCCESS))
    {
        if ((nmhUtlGetNextIndexIsisSysStatLevel (u4InstIdx, u4SysStatLevel,
                                                 &u4SysStatLevel)) ==
            ISIS_FAILURE)
        {
            if ((nmhUtlGetNextIndexIsisSysTable (u4InstIdx, &u4InstIdx)
                 == ISIS_FAILURE))
            {
                NMP_PT ((ISIS_LGST, "NMP <E> : Could Not Find the Next "
                         "Index with the given Indices \n"));

                i1ErrCode = SNMP_FAILURE;
                break;
            }
            else
            {
                u4SysStatLevel = 0;
                i4RetValue = IsisCtrlGetSysContext (u4InstIdx, &pContext);
                continue;
            }
        }
        else
        {
            *pi4NextIsisSysInstance = (INT4) u4InstIdx;
            *pi4NextIsisSysStatLevel = (INT4) u4SysStatLevel;
            i1ErrCode = SNMP_SUCCESS;
            break;
        }
    }

    NMP_EE ((ISIS_LGST,
             "NMP <X> : Exiting nmhGetNextIndexIsisSysStatsTable () \n"));
    UNUSED_PARAM (i4RetValue);

    return i1ErrCode;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisSysStatCorrLSPs
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatCorrLSPs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatCorrLSPs (INT4 i4IsisSysInstance, INT4 i4IsisSysStatLevel,
                           UINT4 *pu4RetValIsisSysStatCorrLSPs)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));
/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatCorrLSPs ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatCorrLSPs = pContext->SysL1Stats.u4SysCorrLSPs;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatCorrLSPs = pContext->SysL2Stats.u4SysCorrLSPs;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatAuthTypeFails
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatAuthTypeFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatAuthTypeFails (INT4 i4IsisSysInstance, INT4 i4IsisSysStatLevel,
                                UINT4 *pu4RetValIsisSysStatAuthTypeFails)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatAuthTypeFails =
                pContext->SysL1Stats.u4SysAuthTypeFails;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatAuthTypeFails =
                pContext->SysL2Stats.u4SysAuthTypeFails;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatAuthFails
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatAuthFails
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatAuthFails (INT4 i4IsisSysInstance, INT4 i4IsisSysStatLevel,
                            UINT4 *pu4RetValIsisSysStatAuthFails)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatAuthFails ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatAuthFails =
                pContext->SysL1Stats.u4SysAuthFails;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatAuthFails =
                pContext->SysL2Stats.u4SysAuthFails;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatLSPDbaseOloads
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatLSPDbaseOloads
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatLSPDbaseOloads (INT4 i4IsisSysInstance,
                                 INT4 i4IsisSysStatLevel,
                                 UINT4 *pu4RetValIsisSysStatLSPDbaseOloads)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatLSPDbaseOloads ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatLSPDbaseOloads =
                pContext->SysL1Stats.u4SysLSPDBOLs;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatLSPDbaseOloads =
                pContext->SysL2Stats.u4SysLSPDBOLs;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;
}

/****************************************************************************
 Function    :  nmhGetIsisSysStatManAddrDropFromAreas
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatManAddrDropFromAreas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatManAddrDropFromAreas (INT4 i4IsisSysInstance,
                                       INT4 i4IsisSysStatLevel,
                                       UINT4
                                       *pu4RetValIsisSysStatManAddrDropFromAreas)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

    UNUSED_PARAM (i4IsisSysStatLevel);
/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        *pu4RetValIsisSysStatManAddrDropFromAreas =
            pContext->SysStats.u4SysMAADropFromAreas;
        i1ErrCode = SNMP_SUCCESS;
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d \n", i4IsisSysInstance));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatAttmptToExMaxSeqNums
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatAttmptToExMaxSeqNums
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatAttmptToExMaxSeqNums (INT4 i4IsisSysInstance,
                                       INT4 i4IsisSysStatLevel,
                                       UINT4
                                       *pu4RetValIsisSysStatAttmptToExMaxSeqNums)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatAttmptToExMaxSeqNums ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatAttmptToExMaxSeqNums =
                pContext->SysL1Stats.u4SysAttemptToExcdMaxSeqNo;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatAttmptToExMaxSeqNums =
                pContext->SysL2Stats.u4SysAttemptToExcdMaxSeqNo;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatSeqNumSkips
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatSeqNumSkips
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatSeqNumSkips (INT4 i4IsisSysInstance, INT4 i4IsisSysStatLevel,
                              UINT4 *pu4RetValIsisSysStatSeqNumSkips)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatSeqNumSkips ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatSeqNumSkips =
                pContext->SysL1Stats.u4SysSeqNumSkips;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatSeqNumSkips =
                pContext->SysL2Stats.u4SysSeqNumSkips;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatOwnLSPPurges
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatOwnLSPPurges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatOwnLSPPurges (INT4 i4IsisSysInstance, INT4 i4IsisSysStatLevel,
                               UINT4 *pu4RetValIsisSysStatOwnLSPPurges)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatOwnLSPPurges ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatOwnLSPPurges =
                pContext->SysL1Stats.u4SysOwnLSPPurges;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatOwnLSPPurges =
                pContext->SysL2Stats.u4SysOwnLSPPurges;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatIDFieldLenMismatches
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatIDFieldLenMismatches
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatIDFieldLenMismatches (INT4 i4IsisSysInstance,
                                       INT4 i4IsisSysStatLevel,
                                       UINT4
                                       *pu4RetValIsisSysStatIDFieldLenMismatches)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatIDFieldLenMismatches ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatIDFieldLenMismatches =
                pContext->SysL1Stats.u4SysIDFieldLenMismatches;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatIDFieldLenMismatches =
                pContext->SysL2Stats.u4SysIDFieldLenMismatches;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatMaxAreaAddrMismatches
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatMaxAreaAddrMismatches
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatMaxAreaAddrMismatches (INT4 i4IsisSysInstance,
                                        INT4 i4IsisSysStatLevel,
                                        UINT4
                                        *pu4RetValIsisSysStatMaxAreaAddrMismatches)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatMaxAreaAddrMismatches ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatMaxAreaAddrMismatches =
                pContext->SysStats.u4SysMaxAAMismatches;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatMaxAreaAddrMismatches = 0;

            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSysStatPartChanges
 Input       :  The Indices
                IsisSysInstance
                IsisSysStatLevel

                The Object 
                retValIsisSysStatPartChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSysStatPartChanges (INT4 i4IsisSysInstance, INT4 i4IsisSysStatLevel,
                              UINT4 *pu4RetValIsisSysStatPartChanges)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4InstIdx = (UINT4) i4IsisSysInstance;
    UINT4               u4StatLevel = (UINT4) i4IsisSysStatLevel;
    tIsisSysContext    *pContext = NULL;

    NMP_EE ((ISIS_LGST, "NMP <X> : Entered nmhGetIsisSysLSPIgnoreErrors ()\n"));

/*silvercreek*/
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : Invalid System Index \n"));
        NMP_EE ((ISIS_LGST,
                 "NMP <X> : Exiting nmhGetIsisSysStatPartChanges ()\n"));
        return (SNMP_FAILURE);
    }

    i4RetVal = IsisCtrlGetSysContext (u4InstIdx, &pContext);

    if (i4RetVal == ISIS_SUCCESS)
    {
        if (u4StatLevel == ISIS_LEVEL1)
        {
            *pu4RetValIsisSysStatPartChanges =
                pContext->SysL1Stats.u4SysPartChanges;
            i1ErrCode = SNMP_SUCCESS;
        }
        else if (u4StatLevel == ISIS_LEVEL2)
        {
            *pu4RetValIsisSysStatPartChanges =
                pContext->SysL2Stats.u4SysPartChanges;
            i1ErrCode = SNMP_SUCCESS;
        }
        else
        {
            i1ErrCode = SNMP_FAILURE;
        }
    }
    else
    {
        NMP_PT ((ISIS_LGST, "NMP <E> : System Entry does not exist for "
                 "the Index, %d %d\n", i4IsisSysInstance, i4IsisSysStatLevel));
        i1ErrCode = SNMP_FAILURE;
    }

    NMP_PT ((ISIS_LGST, "NMP <E> : This mib object doesnot support for "
             "the current ISIS implementation \n"));

    NMP_EE ((ISIS_LGST, "NMP <X> : Exiting nmhGetIsisSysLSPIgnoreErrors ()\n"));

    return i1ErrCode;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : IsisNotificationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIsisNotificationTable
 Input       :  The Indices
                IsisSysInstance
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIsisNotificationTable (INT4 i4IsisSysInstance)
{
    INT1                i1RetCode = SNMP_SUCCESS;
    if ((i4IsisSysInstance < ISIS_LL_LOW_INSTANCE)
        || (i4IsisSysInstance > ISIS_LL_HIGH_INSTANCE))
    {
        i1RetCode = SNMP_FAILURE;
    }
    return i1RetCode;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIsisNotificationTable
 Input       :  The Indices
                IsisSysInstance
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIsisNotificationTable (INT4 *pi4IsisSysInstance)
{
    return (nmhGetNextIndexIsisSysTable (0, pi4IsisSysInstance));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIsisNotificationTable
 Input       :  The Indices
                IsisSysInstance
                nextIsisSysInstance
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIsisNotificationTable (INT4 i4IsisSysInstance,
                                      INT4 *pi4NextIsisSysInstance)
{
    return (nmhGetNextIndexIsisSysTable (i4IsisSysInstance,
                                         pi4NextIsisSysInstance));

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIsisTrapLSPID
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisTrapLSPID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisTrapLSPID (INT4 i4IsisSysInstance,
                     tSNMP_OCTET_STRING_TYPE * pRetValIsisTrapLSPID)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext (i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        pRetValIsisTrapLSPID->i4_Length = ISIS_LSPID_LEN;
        MEMCPY (pRetValIsisTrapLSPID->pu1_OctetList,
                pContext->TrapNotifyTable.au1TrapLSPId, ISIS_LSPID_LEN);
        i1ErrCode = SNMP_SUCCESS;

    }

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisSystemLevel
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisSystemLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisSystemLevel (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisSystemLevel)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisSystemLevel = (INT4) pContext->TrapNotifyTable.u1SysLevel;
        i1ErrCode = SNMP_SUCCESS;
    }

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisPDUFragment
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisPDUFragment
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisPDUFragment (INT4 i4IsisSysInstance,
                       tSNMP_OCTET_STRING_TYPE * pRetValIsisPDUFragment)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        pRetValIsisPDUFragment->i4_Length = ISIS_TRAP_PDU_FRAG_SIZE;
        MEMCPY (pRetValIsisPDUFragment->pu1_OctetList,
                pContext->TrapNotifyTable.au1IsisPduFrag,
                ISIS_TRAP_PDU_FRAG_SIZE);
        i1ErrCode = SNMP_SUCCESS;

    }

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisFieldLen
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisFieldLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisFieldLen (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisFieldLen)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisFieldLen = (INT4) pContext->TrapNotifyTable.u1FieldLen;
        i1ErrCode = SNMP_SUCCESS;
    }

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisMaxAreaAddress
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisMaxAreaAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisMaxAreaAddress (INT4 i4IsisSysInstance,
                          INT4 *pi4RetValIsisMaxAreaAddress)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisMaxAreaAddress =
            (INT4) pContext->TrapNotifyTable.u1MaxAreaAddr;
        i1ErrCode = SNMP_SUCCESS;
    }

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisProtocolVersion
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisProtocolVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisProtocolVersion (INT4 i4IsisSysInstance,
                           INT4 *pi4RetValIsisProtocolVersion)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisProtocolVersion =
            (INT4) pContext->TrapNotifyTable.u1ProtVersion;
        i1ErrCode = SNMP_SUCCESS;
    }

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisLSPSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisLSPSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisLSPSize (INT4 i4IsisSysInstance, INT4 *pi4RetValIsisLSPSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisLSPSize = (INT4) pContext->TrapNotifyTable.u2LSPSize;
        i1ErrCode = SNMP_SUCCESS;
    }

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisOriginatingBufferSize
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisOriginatingBufferSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisOriginatingBufferSize (INT4 i4IsisSysInstance,
                                 INT4 *pi4RetValIsisOriginatingBufferSize)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        *pi4RetValIsisOriginatingBufferSize =
            (INT4) pContext->TrapNotifyTable.u2BuffSize;
        i1ErrCode = SNMP_SUCCESS;
    }

    return i1ErrCode;

}

/****************************************************************************
 Function    :  nmhGetIsisProtocolsSupported
 Input       :  The Indices
                IsisSysInstance

                The Object 
                retValIsisProtocolsSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIsisProtocolsSupported (INT4 i4IsisSysInstance,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValIsisProtocolsSupported)
{
    INT1                i1ErrCode = SNMP_FAILURE;
    INT4                i4RetVal = SNMP_SUCCESS;
    tIsisSysContext    *pContext = NULL;

    i4RetVal = IsisCtrlGetSysContext ((UINT4) i4IsisSysInstance, &pContext);
    if (i4RetVal == ISIS_SUCCESS)
    {
        pRetValIsisProtocolsSupported->i4_Length = ISIS_MAX_ADJ_PROT_SUPP;
        MEMCPY (pRetValIsisProtocolsSupported->pu1_OctetList,
                pContext->TrapNotifyTable.au1ProtSupp, ISIS_MAX_ADJ_PROT_SUPP);
        i1ErrCode = SNMP_SUCCESS;

    }

    return i1ErrCode;

}
