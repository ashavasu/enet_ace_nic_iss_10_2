/******************************************************************************
 * 
 *   Copyright (C) Future Software Limited, 2001
 *
 *   $Id: ismain.c,v 1.49 2017/09/11 13:44:08 siva Exp $
 *
 *   Description: This file contains the main Routines associated with
 *                Control Module. 
 *
 ******************************************************************************/

#include "isincl.h"
#include "isglob.h"
/* Private Function prototypes */
PRIVATE tIsisMsg   *IsisRecvMsg (UINT4 u4Events);
PRIVATE INT4        IsisMemPoolInit (VOID);
PRIVATE VOID        IsisDeAllocAreaAddr (tIsisSysContext *);
PRIVATE VOID        IsisDeAllocSummAddr (tIsisSysContext *);
PRIVATE VOID        IsisDeAllocManAreaAddr (tIsisSysContext *);
PRIVATE VOID        IsisDeAllocIPIF (tIsisSysContext *);
PRIVATE VOID        IsisMemDeInit (VOID);
PRIVATE INT4        IsisInitLspTxPool (UINT4);
PRIVATE INT4        IsisFreeLspTxPool (VOID);
PRIVATE VOID        IsisProcessIfStatInd (tIsisMsg * pIsisMsg);

PRIVATE INT4        IsisIsPduValidOnCkt (UINT1 *, tIsisCktEntry *, UINT4);
#ifdef ROUTEMAP_WANTED
PRIVATE VOID        IsisProcessRMapHandler (tIsisMsg *);
#endif /* ROUTEMAP_WANTED */

tOsixSemId          gIsisSemId;
tRtmMsgHdr          gIsisRtmHdr;
tRtm6MsgHdr         gIsisRtm6Hdr;

/******************************************************************************
 * Function      : IsisInit ()
 * Description   : This routine is invoked during system intialization. The
 *                 routine initialises the global variable, all the memory 
 *                 required for the system, initialise timers, create 
 *                 semaphore and queues for ISIS protocol
 * Input(s)      : None
 * Output(s)     : None
 * Globals       : Not Referred or Modified 
 * Returns       : VOID
 *****************************************************************************/

PUBLIC INT4
IsisInit ()
{

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisInit ()\n"));

    /* The following routine initialises all the global variables */
    IsisInitGlobals ();

    /* Initialise all the memory required by the task */
    gIsisExtInfo.u1NodeType |= ISIS_CTRL_NODE_TYPE_MASK;

    if (IsisMemInit () == ISIS_FAILURE)
    {
        PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : Memory Init Failed\n"));
        DETAIL_FAIL (ISIS_CR_MODULE);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInit ()\n"));
        return ISIS_FAILURE;
    }

    /* Initialise the Timer Lists, Create Queues */
    if (IsisInitModule () == ISIS_FAILURE)
    {
        IsisMemDeInit ();
        PANIC ((ISIS_LGST, "CTL <P> : Timer and Queue Failed\n"));
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInit ()\n"));
        return ISIS_FAILURE;
    }

    gi4IsisSysLogId = SYS_LOG_REGISTER ((UINT1 *) ISIS_TASK_NAME,
                                        SYSLOG_CRITICAL_LEVEL);

    return ISIS_SUCCESS;
}

/*******************************************************************************
 * Function    : IsisCtrlInitGlobals
 * Description : This function initialises all the global variables
 * Input(s)    : None
 * Outputs(s)  : None
 * Globals     : All the global variables are initialised
 * Returns     : VOID
 ******************************************************************************/

PUBLIC VOID
IsisInitGlobals ()
{
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlInitGlobals ()\n"));

    gai1IsisLogStr[0] = '\0';
    CTP_PT ((ISIS_LGST, "CTL <T> : ISIS is in IS_UP state\n"));
    gu1IsisStatus = ISIS_IS_UP;
    gu1RelqFlag = ISIS_DECN_COMP;
    gu1IsisGrState = ISIS_GR_NORMAL_ROUTER;
    gu4SemNameVar = 0;

    MEMSET (&gau1IsisModLevelMask, 0x00, ISIS_MAX_MODULES);
    MEMSET (&gIsisMemConfigs, 0x00, sizeof (tIsisMemConfigs));
    MEMSET (&gIsisMemActuals, 0x00, sizeof (tIsisMemConfigs));

    gpIsisInstances = NULL;

    MEMSET (&gIsisMemPoolId, 0x00, sizeof (tIsisMemPoolId));
    MEMSET (&gIsisLspTxPool, 0x00, sizeof (tIsisLSPTxTable));
    MEMSET (&gIsisCktHashTable, 0x00, sizeof (tIsisHashTable));
    MEMSET (&gIsisExtInfo, 0x00, sizeof (tIsisExtInfo));
    MEMSET (&gIsisTmrListID, 0x00, sizeof (tTimerListId));

    /* By Default, the following are set for the Global variable gIsisExtInfo 
     * 1. FT Status is ISIS_FT_SUPP_DISABLE.
     *    This is enabled through the mib object fsIsisFTState
     * 2. The FT State is of the system is ISIS_FT_INIT, (Out Of Service) mode
     * 3. Buffer Size is set by default to the maximum as LSP Buffer Size.
     *    This value is later altered as given by the RM Module when a GO_ACTIVE
     *    message is received.
     */

    ISIS_EXT_SET_FT_STATE (ISIS_FT_INIT);
    ISIS_EXT_SET_FT_STATUS (ISIS_FT_SUPP_DISABLE);
    ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_NOT_STARTED);
    ISIS_EXT_SET_FT_BUF_SIZE (ISIS_DEF_FT_BUF_SIZE);

    ISIS_SET_CKT_HASH_SIZE (ISIS_MAX_BUCKETS);

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInitGlobals ()\n"));
}

/******************************************************************************
 * Function    : IsisMemReInit ()
 * Description : This routine copies the non-zero memory configuration 
 *               parameters from gIsisMemConfigs to gIsisMemActuals. It then
 *               initialises memory for the ISIS based on these new parameters
 *               and brings up ISIS with changed memory requirements. If any of
 *               the values in gIsisMemConfigs is zero, then the original value
 *               in gIsisMemActuals will be retained
 * Input(s)    : None
 * Output(s)   : None
 * Globals     : gIsisMemActuals Modified, gIsisMemConfigs referred
 * Returns     : ISIS_SUCCESS, If memory initialization successful 
 *               ISIS_FAILURE, Otherwise
 *****************************************************************************/

PUBLIC INT4
IsisMemReInit (VOID)
{
    INT1                i1Result = ISIS_FAILURE;
    tIsisMemConfigs    *pIsisMemConfigs = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisMemReInit ()\n"));

    pIsisMemConfigs = &gIsisMemConfigs;

    if (pIsisMemConfigs->u4MaxInsts != 0)
    {
        gIsisMemActuals.u4MaxInsts = pIsisMemConfigs->u4MaxInsts;
    }
    if (pIsisMemConfigs->u4MaxCkts != 0)
    {
        gIsisMemActuals.u4MaxCkts = pIsisMemConfigs->u4MaxCkts;
    }
    if (pIsisMemConfigs->u4MaxAreaAddr != 0)
    {
        gIsisMemActuals.u4MaxAreaAddr = pIsisMemConfigs->u4MaxAreaAddr;
    }
    if (pIsisMemConfigs->u4MaxAdjs != 0)
    {
        gIsisMemActuals.u4MaxAdjs = pIsisMemConfigs->u4MaxAdjs;
    }
    if (pIsisMemConfigs->u4MaxRoutes != 0)
    {
        gIsisMemActuals.u4MaxRoutes = pIsisMemConfigs->u4MaxRoutes;
    }
    if (pIsisMemConfigs->u4MaxIPRA != 0)
    {
        gIsisMemActuals.u4MaxIPRA = pIsisMemConfigs->u4MaxIPRA;
    }
    if (pIsisMemConfigs->u4MaxSA != 0)
    {
        gIsisMemActuals.u4MaxSA = pIsisMemConfigs->u4MaxSA;
    }
    if (pIsisMemConfigs->u4MaxIPIF != 0)
    {
        gIsisMemActuals.u4MaxIPIF = pIsisMemConfigs->u4MaxIPIF;
    }
    if (pIsisMemConfigs->u4MaxEvents != 0)
    {
        gIsisMemActuals.u4MaxEvents = pIsisMemConfigs->u4MaxEvents;
    }
    if (pIsisMemConfigs->u4MaxLSP != 0)
    {
        gIsisMemActuals.u4MaxLSP = pIsisMemConfigs->u4MaxLSP;
    }
    if (pIsisMemConfigs->u4Factor != 0)
    {
        gIsisMemActuals.u4Factor = pIsisMemConfigs->u4Factor;
    }
    if (pIsisMemConfigs->u4MaxLspTxQLen != 0)
    {
        gIsisMemActuals.u4MaxLspTxQLen = pIsisMemConfigs->u4MaxLspTxQLen;
    }
    if (pIsisMemConfigs->u4MaxMsgBuffs != 0)
    {
        gIsisMemActuals.u4MaxMsgBuffs = pIsisMemConfigs->u4MaxMsgBuffs;
    }

    if (IsisMemPoolInit () == ISIS_SUCCESS)
    {
        i1Result = ISIS_SUCCESS;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisMemReInit ()\n"));

    return (i1Result);
}

/******************************************************************************
 * Function       : IsisMemInit ()
 * Description    : This routine  copies the memory configuration paramenters 
 *                  to the Global memory actuals  based on the value of
 *                  u1ConfMode and allocates all the memory 
 *                  required by the Control Task
 * Input(s)       : None
 * Output(s)      : None
 * Globals        : gIsisMemActuals Modified
 * Returns        : ISIS_SUCCESS, if memory initialization successful
 *                  ISIS_FAILURE, otherwise
 *****************************************************************************/

PUBLIC INT4
IsisMemInit ()
{
    INT4                i4RetVal = ISIS_FAILURE;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisMemInit ()\n"));
    gIsisMemActuals.u4MaxInsts = MAX_ISIS_INSTANCES;
    gIsisMemActuals.u4MaxCkts = MAX_ISIS_CIRCUITS;
    gIsisMemActuals.u4MaxAreaAddr = MAX_ISIS_AREA_ADDRESSES;
    gIsisMemActuals.u4MaxMAA = MAX_ISIS_MANUAL_AREA_ADDRESSES;
    gIsisMemActuals.u4MaxAdjs = MAX_ISIS_ADJACENCIES;
    gIsisMemActuals.u4MaxRoutes = MAX_ISIS_SPT_NODES;
    gIsisMemActuals.u4MaxIPRA = MAX_ISIS_IPRAS;
    gIsisMemActuals.u4MaxSA = MAX_ISIS_SUMMARY_ADDRESSES;
    gIsisMemActuals.u4MaxIPIF = MAX_ISIS_IP_IFACES;
    gIsisMemActuals.u4MaxEvents = MAX_ISIS_EVENTS;
    gIsisMemActuals.u4MaxLSP = MAX_ISIS_LSP_ENTRIES;
    gIsisMemActuals.u4Factor = ISIS_DEF_MAX_FACT;
    gIsisMemActuals.u4MaxLspTxQLen = ISIS_DEF_MAX_TXQL;
    gIsisMemActuals.u4MaxMsgBuffs = MAX_ISIS_MESSAGES;
    i4RetVal = IsisMemPoolInit ();

    CTP_EE ((ISIS_LGST, "CTL <X> : Exited IsisMemInit ()\n"));
    return (i4RetVal);
}

/******************************************************************************
 * Function      : IsisMemPoolInit ()
 * Description   : This routine initialises all the memory pools required for
 *                 the protocol operations. 
 * Input(s)      : None 
 * Output(s)     : None
 * Globals       : gIsisMemActuals Referred, Not Modified 
 * Returns       : ISIS_SUCCESS if initialization is successful
 *                 ISIS_FAILURE otherwise 
 *****************************************************************************/

PRIVATE INT4
IsisMemPoolInit ()
{
    INT4                i4RetVal;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisMemPoolInit ()\n"));

    /* Initializing the ISIS System PoolId structure. */
    MEMSET (&gIsisMemPoolId, 0, sizeof (tIsisMemPoolId));

    /* Create mem-pools required by for enabling ISIS */
    IsisSizingMemCreateMemPools ();

    /* Assign pool-identifiers to the existing variables */
    gIsisMemPoolId.u4InstsPid = ISISMemPoolIds[MAX_ISIS_INSTANCES_SIZING_ID];
    gIsisMemPoolId.u4CktsPid = ISISMemPoolIds[MAX_ISIS_CIRCUITS_SIZING_ID];
    gIsisMemPoolId.u4CktLvlPid = ISISMemPoolIds[MAX_ISIS_CKT_LEVELS_SIZING_ID];
    gIsisMemPoolId.u4AAPid = ISISMemPoolIds[MAX_ISIS_AREA_ADDRESSES_SIZING_ID];
    gIsisMemPoolId.u4MAAPid =
        ISISMemPoolIds[MAX_ISIS_MANUAL_AREA_ADDRESSES_SIZING_ID];
    gIsisMemPoolId.u4AdjsPid = ISISMemPoolIds[MAX_ISIS_ADJACENCIES_SIZING_ID];
    gIsisMemPoolId.u4AdjDirPid =
        ISISMemPoolIds[MAX_ISIS_ADJ_DIR_ENTRIES_SIZING_ID];
    gIsisMemPoolId.u4IPRAPid = ISISMemPoolIds[MAX_ISIS_IPRAS_SIZING_ID];
    gIsisMemPoolId.u4IPIFPid = ISISMemPoolIds[MAX_ISIS_IP_IFACES_SIZING_ID];
    gIsisMemPoolId.u4SAPid =
        ISISMemPoolIds[MAX_ISIS_SUMMARY_ADDRESSES_SIZING_ID];
    gIsisMemPoolId.u4EventsPid = ISISMemPoolIds[MAX_ISIS_EVENTS_SIZING_ID];
    gIsisMemPoolId.u4RoutesPid = ISISMemPoolIds[MAX_ISIS_SPT_NODES_SIZING_ID];
    gIsisMemPoolId.u4ECBPid = ISISMemPoolIds[MAX_ISIS_TMR_NODES_SIZING_ID];
    gIsisMemPoolId.u4HTblPid =
        ISISMemPoolIds[MAX_ISIS_HASH_TABLE_NODES_SIZING_ID];
    gIsisMemPoolId.u4LSEPid = ISISMemPoolIds[MAX_ISIS_LSP_ENTRIES_SIZING_ID];
    gIsisMemPoolId.u4MsgBuffPid = ISISMemPoolIds[MAX_ISIS_MESSAGES_SIZING_ID];
    gIsisMemPoolId.u4LspTxQPid = ISISMemPoolIds[MAX_ISIS_LSPS_TXQ_SIZING_ID];
    gIsisMemPoolId.u4AckRxPid = ISISMemPoolIds[MAX_ISIS_ACK_RX_SIZING_ID];
    gIsisMemPoolId.u4CSNPRxPid = ISISMemPoolIds[MAX_ISIS_CSNP_RX_SIZING_ID];
    gIsisMemPoolId.u4RmapFilterPid =
        ISISMemPoolIds[MAX_ISIS_LSPS_RMAP_FILTERS_SIZING_ID];
    gIsisMemPoolId.u4GrInfoPid = ISISMemPoolIds[MAX_ISIS_GR_INFO_SIZING_ID];
    gIsisMemPoolId.u4GrCxtInfoPid = ISISMemPoolIds[MAX_ISIS_GR_CXT_SIZING_ID];
    gIsisMemPoolId.u4GrCxtpid =
        ISISMemPoolIds[MAX_GR_CXT_INFO_BLOCK_SIZE_SIZING_ID];
    gau1IsisBdyId[ISIS_BDY_64_BLK] =
        ISISMemPoolIds[MAX_ISIS_128_BLKS_SIZING_ID];
    gau1IsisBdyId[ISIS_BDY_1500_BLK] =
        ISISMemPoolIds[MAX_ISIS_1500_BLKS_SIZING_ID];
    gIsisMemPoolId.u4RRDRoutesPid =
        ISISMemPoolIds[MAX_ISIS_RRD_ROUTES_SIZING_ID];
    gIsisMemPoolId.u4HostNmePid =
        ISISMemPoolIds[MAX_HOST_NME_ENTRIES_SIZING_ID];

    i4RetVal = IsisInitLspTxPool (gIsisMemActuals.u4MaxLspTxQLen);

    if (i4RetVal != ISIS_SUCCESS)
    {
        PANIC ((ISIS_LGST,
                "CTL <P> : Memory Pool Creation Failed For LspTxPool \n"));

        IsisMemDeInit ();
        return ISIS_FAILURE;
    }
#ifdef CLI_WANTED
    if (CsrMemAllocation (ISIS_MODULE_ID) == CSR_FAILURE)
    {
        IsisMemDeInit ();
        return ISIS_FAILURE;
    }
#endif

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisMemInit ()\n"));

    return (ISIS_SUCCESS);
}

/******************************************************************************
 * Function      : IsisMemDeInit ()
 * Description   : This routine De-allocates all the memory pools allocated
 *                 during initialization.
 * Input(s)      : None 
 * Output(s)     : None
 * Globals       : gIsisMemPoolId Referred, not Modified
 * Returns       : VOID
 *****************************************************************************/

PRIVATE VOID
IsisMemDeInit ()
{

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisMemDeInit ()\n"));

    IsisFreeLspTxPool ();
    IsisSizingMemDeleteMemPools ();

#ifdef CLI_WANTED
    CsrMemDeAllocation (ISIS_MODULE_ID);
#endif

    MEMSET (&gIsisMemPoolId, 0, sizeof (tIsisMemPoolId));

    CTP_EE ((ISIS_LGST, "CTL <X> : Exited IsisMemDeInit ()\n"));
}

/*******************************************************************************
 * Function     : IsisResetAll ()
 * Description  : This routine Resets all the instances of ISIS, releases all
 *                the memory associated with these instances back to the
 *                system. It then starts reinitialising with the new
 *                configurations 
 * Input(s)     : None 
 * Output(s)    : None 
 * Globals      : Not Referred or Modified
 * Returns      : ISIS_SUCCESS, On successful Re-initialization
 *                ISIS_FAILURE, Otherwise
 ******************************************************************************/

PUBLIC INT4
IsisResetAll ()
{
    INT4                i4RetVal = ISIS_SUCCESS;
    UINT1               u1Cnt = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisSysContext    *pNextContext = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisResetAll ()\n"));

    pContext = ISIS_GET_CONTEXT ();

    while (pContext != NULL)
    {
        pNextContext = pContext->pNext;
        pContext->pNext = NULL;

        CTP_PT ((ISIS_LGST, "CTL <T> : Resetting Instance [ %u ]\n",
                 pContext->u4SysInstIdx));
        IsisTmrStopTimer (&(pContext->SysTimers.SysRestartT3Timer));
        IsisResetInst (pContext);
        pContext = pNextContext;
    }

    for (u1Cnt = 0; u1Cnt < gIsisCktHashTable.u1HashSize; u1Cnt++)
    {
        if (u1Cnt < ISIS_MAX_BUCKETS)    /*klocwork */
        {
            gIsisCktHashTable.apHashBkts[u1Cnt] = NULL;
        }
    }

    ISIS_PUT_CONTEXT (NULL);
    ISIS_FLTR_IS_STAT_LSU (ISIS_RESET_ALL);

    /* Free all the memory associated with the instances back to the system, and
     * start with the new configurations
     */

    IsisMemDeInit ();

    CTP_PT ((ISIS_LGST,
             "CTL <T> : ReStarting ISIS With New Memory Configurations\n"));

    i4RetVal = IsisMemReInit ();
    if (i4RetVal == ISIS_SUCCESS)
    {
        CTP_PT ((ISIS_LGST, "CTL <T> : ISIS is in IS_UP state\n"));
        gu1IsisStatus = ISIS_IS_UP;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisResetAll ()\n"));
    return (i4RetVal);
}

/*******************************************************************************
 * Function       : IsisShutDownAll ()
 * Description    : This routine Destroys all instances of the ISIS module and
 *                  frees all the memory associated with these instances back to
 *                  the system
 * Input(s)       : None 
 * Output(s)      : None 
 * Globals        : gpIsisInstances Updated
 * Returns        : VOID 
 ******************************************************************************/

PUBLIC VOID
IsisShutDownAll ()
{
    tIsisSysContext    *pContext = NULL;
    tIsisSysContext    *pNextContext = NULL;
    UINT1               u1IsisGRRestarterState = 0;
    UINT4               u4SysInstIdx = 0;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisShutDownAll ()\n"));

    pContext = ISIS_GET_CONTEXT ();

    /*ISIS status is made shutdown
     * this is verified in event processing 
     * to avoid process events when 
     * ISIS module is shut*/
    gu1IsisStatus = ISIS_SHUTDOWN;

    while (pContext != NULL)
    {
        pNextContext = pContext->pNext;
        pContext->pNext = NULL;
        CTP_PT ((ISIS_LGST, "CTL <T> : Shutting Down Instance [ %u ]\n",
                 pContext->u4SysInstIdx));
        u1IsisGRRestarterState = pContext->u1IsisGRRestarterState;
        u4SysInstIdx = pContext->u4SysInstIdx;
        IsisTmrStopTimer (&(pContext->SysTimers.SysRestartT3Timer));
        IsisResetInst (pContext);
        IsisDeRegFromExtMod (u1IsisGRRestarterState, u4SysInstIdx);
        pContext = pNextContext;
    }
#ifdef ISIS_FT_ENABLED
    /* De-register from RM only if enabled */
    IsisFltiDeRegWithRM ();
#endif

    ISIS_PUT_CONTEXT (NULL);

#ifdef SNMP_2_WANTED
    /* UnRegister SNMP */
    UnRegisterSTDISI ();
    UnRegisterFSISIS ();
#endif
    /* DeRegister from the External Modules, such as RRD and RM
     */
#ifdef ISS_WANTED
    /* DeRegister with iss for hostname update. */
    IsisDeRegWithIss ();
#endif

    /* as the System is Shutting down, the FT Support can be disabled
     * and the FT State can be made as ISIS_FT_INIT
     */

    ISIS_EXT_SET_FT_STATUS (ISIS_FT_SUPP_DISABLE);
    ISIS_EXT_SET_FT_STATE (ISIS_FT_INIT);
    ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_NOT_STARTED);
    IsisDeRegFromVcm ();
    IsisMemDeInit ();
    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisShutDownAll ()\n"));
}

/*******************************************************************************
 * Function       : IsisDelSemandQue ()
 * Description    : This routine Destroys Semaphore and Queues created for ISIS module 
 * Input(s)       : None 
 * Output(s)      : None 
 * Globals        : gpIsisInstances Updated
 * Returns        : VOID 
 ******************************************************************************/
PUBLIC VOID
IsisDelSemandQue ()
{
    /*Queue Delete */
    OsixQueDel (gIsisCtrlQId);
    OsixQueDel (gIsisRtmQId);
#ifdef ISIS_FT_ENABLED
    OsixQueDel (gIsisFltQId);
#endif
    OsixSemDel (gIsisSemId);
}

/******************************************************************************
 * Function       : IsisResetInst ()
 * Description    : This routine frees all the memory associated with the
 *                  specified context and moves it back to free pools.
 * Input(s)       : pContext  - Pointer to System Context
 * Output(s)      : None 
 * Globals        : Not Referred or Modified 
 * Returns        : VOID
 *****************************************************************************/

PUBLIC VOID
IsisResetInst (tIsisSysContext * pContext)
{
    tIsisCktEntry      *pCktRec = NULL;
    tIsisCktEntry      *pNextCktRec = NULL;
    tIsisSPTNode       *pL1DelNodes = NULL;
    tIsisSPTNode       *pNextDelNode = NULL;
    tIsisLSPTxEntry    *pPurgeRec = NULL;
    tIsisLSPTxEntry    *pNxtPurgeRec = NULL;
    tIsisIPRAEntry     *pIPRARec = NULL;
    tIsisIPRAEntry     *pNextIPRARec = NULL;
    tIsisLLData        *pLLDataNode = NULL;
    tIsisLLData        *pNextLLDataNode = NULL;
    tIsisAdjDelDirEntry *pDelDirEntry = NULL;
    tIsisAdjDelDirEntry *pNextDelDirEntry = NULL;
    UINT1               au1Hash[8];
    UINT1               u1HashIdx = 0;
    UINT1               u1MtIndex = 0;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisResetInst ()\n"));

    /* Sending Lock Step Update to Fault Tolerance
     * Module
     */

    ISIS_FLTR_SYS_CTXT_LSU (pContext, ISIS_CMD_DELETE, ISIS_ACTIVE);

    /* Stop all the Active timers 
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Stopping All Timers\n"));
    IsisStopAllTimers (pContext);

    /* Clear all the Area Addresses
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting Area Addresses\n"));
    IsisDeAllocAreaAddr (pContext);

    /* Clear all the Summary Addresses
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting Summary Addresses\n"));
    IsisDeAllocSummAddr (pContext);

    /* Clear all the Manual Area Addresses
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting Manual Area Addresses\n"));
    IsisDeAllocManAreaAddr (pContext);

    /* Clear all the Events 
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting Event Table Entries\n"));
    IsisDeAllocEvent (pContext);

    /* Delete all IPRAs */

    pIPRARec = pContext->IPRAInfo.pIPRAEntry;
    while (pIPRARec != NULL)
    {
        pNextIPRARec = pIPRARec->pNext;
        ISIS_MEM_FREE (ISIS_BUF_IPRA, (UINT1 *) pIPRARec);
        pIPRARec = pNextIPRARec;
    }

    /* Delete all the circuits which in turn handles deletion of 
     * Adjacencies, Adjacency Area Addresses etc.
     */
    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting Circuits\n"));
    pCktRec = pContext->CktTable.pCktRec;
    while (pCktRec != NULL)
    {
        pNextCktRec = pCktRec->pNext;

        IsisTmrStopECTimer (pContext, ISIS_GR_ECT_T1,
                            pCktRec->u4CktIdx, pCktRec->u1IsisGRT1TmrIdx);

        if (pCktRec->u1CktExistState == ISIS_ACTIVE)
        {
            IsisTrfrIfStatusInd (pContext, pCktRec, ISIS_CKT_DOWN);

            /* Form a single Key of 8 bytes IfIndex (4) + IfSubIndex (4) bytes.
             * The hashing function uses all the 8 bytes together and returns
             * the hash key
             */

            MEMCPY (au1Hash, (UINT1 *) &(pCktRec->u4CktIfIdx), sizeof (UINT4));
            MEMCPY (&au1Hash[4], (UINT1 *) &(pCktRec->u4CktIfSubIdx),
                    sizeof (UINT4));
            u1HashIdx = IsisUtlGetHashIdx ((UINT1 *) au1Hash,
                                           2 * sizeof (UINT4),
                                           gIsisCktHashTable.u1HashSize);
            ISIS_DEL_CKT_IN_HASHTBL (pCktRec, u1HashIdx);
        }
        IsisAdjDelCkt (pContext, pCktRec->u4CktIdx);
        pCktRec = pNextCktRec;
    }

    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting EC-1 Timer blocks\n"));
    IsisTmrDeAllocTmrECB (pContext->SysTimers.TmrECInfo.apTmrEC1,
                          ISIS_MAX_EC1_BLKS);

    /* Clear all EC timer blocks in the EC-Block 2 
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting EC-2 Timer blocks\n"));
    IsisTmrDeAllocTmrECB (pContext->SysTimers.TmrECInfo.apTmrEC2,
                          ISIS_MAX_EC2_BLKS);

    /* Moving all EC timer blocks in the EC-Block 3 
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting EC-3 Timer blocks\n"));
    IsisTmrDeAllocTmrECB (pContext->SysTimers.TmrECInfo.apTmrEC3,
                          ISIS_MAX_EC3_BLKS);

    if (pContext->CktTable.pu1L1CktMask != NULL)
    {
        /* Usually This Memory will be freed by Adjacency module when
         * Last Adjacency goes down. When the system is going from
         * active to out-of-service, This memory will not be freed.
         * Hence free it here.
         */

        ISIS_MEM_FREE (ISIS_BUF_ECTI,
                       (UINT1 *) pContext->CktTable.pu1L1CktMask);
        pContext->CktTable.pu1L1CktMask = NULL;
    }

    if (pContext->CktTable.pu1L2CktMask != NULL)
    {
        ISIS_MEM_FREE (ISIS_BUF_ECTI,
                       (UINT1 *) pContext->CktTable.pu1L2CktMask);
        pContext->CktTable.pu1L2CktMask = NULL;
    }

    /* Clear the IPIF Address Table
     */
    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting IP Interface Address Table\n"));
    IsisDeAllocIPIF (pContext);
    IsisDecProcISDownEvt (pContext);
    pLLDataNode = pContext->pLLDataNode;
    while (pLLDataNode != NULL)
    {
        pNextLLDataNode = pLLDataNode->pNext;
        IsisFreeDLLBuf ((tIsisMsg *) (VOID *) (pLLDataNode->pu1Msg));
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) (pLLDataNode->pu1Msg));
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLDataNode);
        pLLDataNode = pNextLLDataNode;
    }

    pDelDirEntry = pContext->pDelDirEntry;
    while (pDelDirEntry != NULL)
    {
        pNextDelDirEntry = pDelDirEntry->pNext;
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pDelDirEntry);
        pDelDirEntry = pNextDelDirEntry;
    }

    /* Purge all LSPs and free the memory associated with the purged LSPs
     */
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL1))
    {

        IsisUpdPurgeLSPs (pContext, ISIS_LEVEL1);
        IsisUpdDelSelfLSPInfo (pContext, ISIS_LEVEL1);

        /* NOTE: IsisDecProcISDownEvt () would have freed all the SPT nodes in 
         *       the Previous Shortest Path Database and Current Shortest Path
         *       Database. Since the current routine is processing the Destroy
         *       Event, we may have to free the Database also
         */
        for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
        {
            if (pContext->SysL1Info.PrevShortPath[u1MtIndex] != NULL)
            {
                RBTreeDestroy (pContext->SysL1Info.PrevShortPath[u1MtIndex],
                               NULL, 0);
                pContext->SysL1Info.PrevShortPath[u1MtIndex] = NULL;
            }

            if (pContext->SysL1Info.ShortPath[u1MtIndex] != NULL)
            {
                RBTreeDestroy (pContext->SysL1Info.ShortPath[u1MtIndex], NULL,
                               0);
                pContext->SysL1Info.ShortPath[u1MtIndex] = NULL;
            }
        }
    }
    if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
        || (pContext->SysActuals.u1SysType == ISIS_LEVEL2))
    {
        CTP_PI ((ISIS_LGST, "CTL <I> : Deleting L2 LSP Database\n"));
        IsisUpdPurgeLSPs (pContext, ISIS_LEVEL2);
        IsisUpdDelSelfLSPInfo (pContext, ISIS_LEVEL2);

        /* NOTE: IsisDecProcISDownEvt () would have freed all the SPT nodes in 
         *       the Previous Shortest Path Database and Current Shortest Path
         *       Database. Since the current routine is processing the Destroy
         *       Event, we may have to free the Database also
         */
        for (u1MtIndex = 0; u1MtIndex < ISIS_MAX_MT; u1MtIndex++)
        {
            if (pContext->SysL2Info.PrevShortPath[u1MtIndex] != NULL)
            {
                RBTreeDestroy (pContext->SysL2Info.PrevShortPath[u1MtIndex],
                               NULL, 0);
                pContext->SysL2Info.PrevShortPath[u1MtIndex] = NULL;
            }

            if (pContext->SysL2Info.ShortPath[u1MtIndex] != NULL)
            {
                RBTreeDestroy (pContext->SysL2Info.ShortPath[u1MtIndex], NULL,
                               0);
                pContext->SysL2Info.ShortPath[u1MtIndex] = NULL;
            }
        }
    }

    /* NOTE: IsisDecProcISDownEvt () would have freed all the SPT nodes in 
     *       the TENT Database. Since the current routine is processing the 
     *       Destroy Event, we may have to free the Database also
     */

    if (pContext->Tent != NULL)
    {
        RBTreeDestroy (pContext->Tent, NULL, 0);
        pContext->Tent = NULL;
    }
    /* NOTE: OSI path is cleared then and there by the decision process. Since
     *       the current routine is processing Destroy Event, we may have to
     *       free the Database
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Deleting OSI PATH Database\n"));

    if (pContext->OSIPath != NULL)
    {
        RBTreeDestroy (pContext->OSIPath, NULL, 0);
        pContext->OSIPath = NULL;
    }
    /* Clear the Deleted nodes which are maintained by the decision module for
     * updating Routing Database
     */

    CTP_PI ((ISIS_LGST, "CTL <I> : Clearing Deleted Nodes List\n"));
    pL1DelNodes = pContext->pL1DelNodes;
    pContext->pL1DelNodes = NULL;
    while (pL1DelNodes != NULL)
    {
        pNextDelNode = pL1DelNodes->pNext;
        ISIS_MEM_FREE (ISIS_BUF_SPTN, (UINT1 *) pL1DelNodes);
        pL1DelNodes = pNextDelNode;
    }
    pPurgeRec = pContext->pL1PurgeQueue;
    while (pPurgeRec != NULL)
    {
        pNxtPurgeRec = (tIsisLSPTxEntry *) pPurgeRec->pLSPRec->pNext;

        ISIS_MEM_FREE (ISIS_BUF_SRMF, pPurgeRec->pu1SRM);
        pPurgeRec->pu1SRM = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pPurgeRec->pLSPRec->pu1LSP);
        pPurgeRec->pLSPRec->pu1LSP = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pPurgeRec->pLSPRec);
        pPurgeRec->pLSPRec = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pPurgeRec);
        pPurgeRec = pNxtPurgeRec;
    }

    pPurgeRec = pContext->pL2PurgeQueue;
    while (pPurgeRec != NULL)
    {
        pNxtPurgeRec = (tIsisLSPTxEntry *) pPurgeRec->pLSPRec->pNext;

        ISIS_MEM_FREE (ISIS_BUF_SRMF, pPurgeRec->pu1SRM);
        pPurgeRec->pu1SRM = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LSPI, (UINT1 *) pPurgeRec->pLSPRec->pu1LSP);
        pPurgeRec->pLSPRec->pu1LSP = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LDBE, (UINT1 *) pPurgeRec->pLSPRec);
        pPurgeRec->pLSPRec = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LTXQ, (UINT1 *) pPurgeRec);
        pPurgeRec = pNxtPurgeRec;
    }
#ifdef ROUTEMAP_WANTED
    if (pContext->pDistanceFilterRMap != NULL)
    {
        MemReleaseMemBlock (gIsisMemPoolId.u4RmapFilterPid,
                            (UINT1 *) pContext->pDistanceFilterRMap);
        pContext->pDistanceFilterRMap = NULL;
    }
    if (pContext->pDistributeInFilterRMap != NULL)
    {
        MemReleaseMemBlock (gIsisMemPoolId.u4RmapFilterPid,
                            (UINT1 *) pContext->pDistributeInFilterRMap);
        pContext->pDistributeInFilterRMap = NULL;
    }
#endif
    CTP_PI ((ISIS_LGST, "CTL <I> : Context With Index [ %u ] Destroyed\n",
             pContext->u4SysInstIdx));

    RBTreeDestroy (pContext->RRDRouteRBTree, NULL, 0);
    pContext->RRDRouteRBTree = NULL;

    RBTreeDestroy (pContext->HostNmeList, NULL, 0);
    pContext->HostNmeList = NULL;
    pContext->u4HostNmeTotEntries = 0;

    ISIS_MEM_FREE (ISIS_BUF_CTXT, (UINT1 *) pContext);
    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisResetInst ()\n"));
}

/******************************************************************************
 * Function    : IsisDeAllocAreaAddr ()
 * Description : This routine frees all the Area Addresses included in the Area
 *               Address Table of the given instance and moves them back to the
 *               free pool 
 * Input(s)    : pContext  - Pointer to System Context
 * Output(s)   : None 
 * Globals     : None 
 * Returns     : VOID 
 *****************************************************************************/

PRIVATE VOID
IsisDeAllocAreaAddr (tIsisSysContext * pContext)
{
    tIsisAAEntry       *pAARec = NULL;
    tIsisAAEntry       *pNextAARec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlDeAllocAreaAddrTable ()\n"));

    /* Scan through the entire list and de-alloc one by one
     */

    pAARec = pContext->AreaAddrTable.pAARec;
    while (pAARec != NULL)
    {
        pNextAARec = pAARec->pNext;
        pAARec->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_ARAD, (UINT1 *) pAARec);
        pAARec = pNextAARec;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlDeAllocAreaAddrTable ()\n"));
}

/******************************************************************************
 * Function    : IsisDeAllocSummAddr ()
 * Description : This routine frees all the Summary Addresses included in the 
 *               Summary Address Table of the given instance and moves them 
 *               back to the free pool 
 * Input(s)    : pContext  - Pointer to System Context
 * Output(s)   : None 
 * Globals     : None 
 * Returns     : VOID 
 *****************************************************************************/

PRIVATE VOID
IsisDeAllocSummAddr (tIsisSysContext * pContext)
{
    tIsisSAEntry       *pSARec = NULL;
    tIsisSAEntry       *pNextSARec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisDeAllocSummAddr ()\n"));

    /* Scan through the entire list and de-alloc one by one
     */

    pSARec = pContext->SummAddrTable.pSAEntry;
    while (pSARec != NULL)
    {
        pNextSARec = pSARec->pNext;
        pSARec->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_SATE, (UINT1 *) pSARec);
        pSARec = pNextSARec;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisDeAllocSummAddr ()\n"));
}

/******************************************************************************
 * Function    : IsisDeAllocManAreaAddr ()
 * Description : This routine frees all the Manual Addresses included in the 
 *               Manual Address Table of the given instance and moves them 
 *               back to the free pool 
 * Input(s)    : pContext  - Pointer to System Context
 * Output(s)   : None 
 * Globals     : None 
 * Returns     : VOID 
 *****************************************************************************/

PRIVATE VOID
IsisDeAllocManAreaAddr (tIsisSysContext * pContext)
{
    tIsisMAAEntry      *pMAARec = NULL;
    tIsisMAAEntry      *pNextMAARec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisDeAllocManAreaAddr ()\n"));

    /* Scan through the entire list and de-alloc one by one
     */

    pMAARec = pContext->ManAddrTable.pMAARec;
    while (pMAARec != NULL)
    {
        pNextMAARec = pMAARec->pNext;
        pMAARec->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_MAAT, (UINT1 *) pMAARec);
        pMAARec = pNextMAARec;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisDeAllocManAreaAddr ()\n"));
}

/******************************************************************************
 * Function    : IsisDeAllocEvent ()
 * Description : This routine frees all the events included in the 
 *               event table of the given instance and moves them 
 *               back to the free pool 
 * Input(s)    : pContext  - Pointer to System Context
 * Output(s)   : None 
 * Globals     : Not Referred or Modified 
 * Returns     : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisDeAllocEvent (tIsisSysContext * pContext)
{
    tIsisEvent         *pEventRec = NULL;
    tIsisEvent         *pNextEventRec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisDeAllocEvent ()\n"));

    /* Scan through the entire list and de-alloc one by one
     */

    pEventRec = pContext->EventTable.pEvent;
    while (pEventRec != NULL)
    {
        pNextEventRec = pEventRec->pNext;
        pEventRec->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_EVTE, (UINT1 *) pEventRec);
        pEventRec = pNextEventRec;
    }
    pContext->EventTable.pEvent = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisDeAllocEvent ()\n"));
}

/******************************************************************************
 * Function    : IsisDeAllocIPIF ()
 * Description : This routine frees all the IP Interface Addresses included in 
 *               the IP Interface Address Table of the given instance and moves
 *               them back to the free pool 
 * Input(s)    : pContext  - Pointer to System Context
 * Output(s)   : None 
 * Globals     : Not Referred or Modified 
 * Returns     : VOID 
 *****************************************************************************/

PRIVATE VOID
IsisDeAllocIPIF (tIsisSysContext * pContext)
{
    tIsisIPIfAddr      *pIPIFRec = NULL;
    tIsisIPIfAddr      *pNextIPIFRec = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisDeAllocIPIF ()\n"));

    /* Scan through the entire list and de-alloc one by one
     */

    pIPIFRec = pContext->IPIfTable.pIPIfAddr;
    while (pIPIFRec != NULL)
    {
        pNextIPIFRec = pIPIFRec->pNext;
        pIPIFRec->pNext = NULL;
        ISIS_MEM_FREE (ISIS_BUF_IPIF, (UINT1 *) pIPIFRec);
        pIPIFRec = pNextIPIFRec;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisDeAllocIPIF ()\n"));
}

/******************************************************************************
 * Function       : IsisStopAllTimers ()
 * Description    : This routine Stops all timers running for the given context.
 * Input(s)       : pContext  - Pointer to the System Context 
 * Output(s)      : None 
 * Globals        : None 
 * Returns        : VOID 
 *****************************************************************************/

PUBLIC VOID
IsisStopAllTimers (tIsisSysContext * pContext)
{
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisStopAllTimers ()\n"));

    IsisTmrStopTimer (&(pContext->SysTimers.SysECTimer));
    IsisTmrStopTimer (&(pContext->SysTimers.SysISSeqWrapTimer));
    IsisTmrStopTimer (&(pContext->SysTimers.SysDbaseNormTimer));
    IsisTmrStopTimer (&(pContext->SysTimers.SysSPFSchTimer));
    IsisTmrStopTimer (&(pContext->SysL1Info.SysLSPGenTimer));
    IsisTmrStopTimer (&(pContext->SysL2Info.SysLSPGenTimer));
    IsisTmrStopTimer (&(pContext->SysL1Info.SysWaitingTimer));
    IsisTmrStopTimer (&(pContext->SysL2Info.SysWaitingTimer));
    /* ISIS GR Timers */

    IsisTmrStopTimer (&(pContext->SysL1Info.SysRestartT2Timer));
    IsisTmrStopTimer (&(pContext->SysL2Info.SysRestartT2Timer));

    CTP_EE ((ISIS_LGST, "CTL <X> : Exited IsisStopAllTimers ()\n"));
}

/******************************************************************************
 * Function       : IsisStartAllTimers ()
 * Description    : This routine starts Equivalence Class Timers, SPF
 *                  Scheduling Timers and the Database Normalization Timers.
 * Input(s)       : pContext - Pointer to the System Context
 * Output(s)      : VOID 
 * Globals        : None 
 * Returns        : VOID
 *****************************************************************************/

PRIVATE VOID
IsisStartAllTimers (tIsisSysContext * pContext)
{
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisStartAllTimers ()\n"));

    pContext->SysTimers.SysECTimer.pContext = pContext;
    IsisTmrStartTimer (&(pContext->SysTimers.SysECTimer),
                       ISIS_EQUV_CLSS_TIMER, 1);

    pContext->SysTimers.SysSPFSchTimer.pContext = pContext;
    IsisTmrStartTimer (&(pContext->SysTimers.SysSPFSchTimer),
                       ISIS_SPF_SCHDL_TIMER,
                       (UINT4) (pContext->SysActuals.u2MinSPFSchTime));

    pContext->SysTimers.SysDbaseNormTimer.pContext = pContext;
    IsisTmrStartTimer (&(pContext->SysTimers.SysDbaseNormTimer),
                       ISIS_DBS_NORMN_TIMER, ISIS_DB_NORM_TIME);

    if (pContext->SysActuals.u1SysType != ISIS_LEVEL2)
    {
        pContext->SysL1Info.SysLSPGenTimer.pContext = pContext;
        IsisTmrStartTimer (&(pContext->SysL1Info.SysLSPGenTimer),
                           ISIS_L1LSP_GEN_TIMER, 1);
    }
    if (pContext->SysActuals.u1SysType != ISIS_LEVEL1)
    {
        pContext->SysL2Info.SysLSPGenTimer.pContext = pContext;
        IsisTmrStartTimer (&(pContext->SysL2Info.SysLSPGenTimer),
                           ISIS_L2LSP_GEN_TIMER, 1);
    }
    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisStartAllTimers ()\n"));
}

#ifdef ISIS_FT_ENABLED
/******************************************************************************
 * Function       : IsisCtrlHandleStandbyUp ()
 * Description    : This routine updates the standby count and clears the local
 *                  variables for bulk updates
 * Input(s)       : pRmMsg   - Pointer to RM Message
 * Output(s)      : VOID 
 * Globals        : None 
 * Returns        : None 
 *****************************************************************************/

PUBLIC VOID
IsisCtrlHandleStandbyUp (tRmMsg * pRmMsg)
{
    tRmNodeInfo        *pData = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlHandleStandbyUp ()\n"));

    pData = (tRmNodeInfo *) pRmMsg;
    gIsisExtInfo.u4PeerCount = pData->u1NumStandby;

    IsisRmRelRmMsgMem ((UINT1 *) pData);

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlHandleStandbyUp ()\n"));
}

/******************************************************************************
 * Function       : IsisCtrlHandleStandbyDown ()
 * Description    : This routine updates the standby count and clears the local
 *                  variables for bulk updates
 * Input(s)       : pRmMsg   - Pointer to RM Message
 * Output(s)      : VOID 
 * Globals        : None 
 * Returns        : None 
 *****************************************************************************/

PUBLIC VOID
IsisCtrlHandleStandbyDown (tRmMsg * pRmMsg)
{
    tRmNodeInfo        *pData = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlHandleStandbyDown ()\n"));

    pData = (tRmNodeInfo *) pRmMsg;
    gIsisExtInfo.u4PeerCount = pData->u1NumStandby;

    IsisRmRelRmMsgMem ((UINT1 *) pData);

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlHandleStandbyDown ()\n"));
}

/******************************************************************************
 * Function       : IsisCtrlConfigRestoreComplete ()
 * Description    : This routine indicates that the configurtation is complete
 *                  The node sends standby event processed to the RM module
 * Input(s)       : pRmMsg   - Pointer to RM Message
 * Output(s)      : VOID 
 * Globals        : None 
 * Returns        : None 
 *****************************************************************************/

PUBLIC VOID
IsisCtrlConfigRestoreComplete ()
{
    tIsisSysContext    *pContext = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlHandleStandbyDown ()\n"));
    if (ISIS_EXT_IS_FT_STATE () == ISIS_FT_INIT &&
        IsisRmGetNodeState () == RM_STANDBY)
    {
        ISIS_EXT_SET_FT_STATE (ISIS_FT_STANDBY);
        ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_NOT_STARTED);

        pContext = ISIS_GET_CONTEXT ();
        /* Scan all the context and stop all the timers from all contexts */

        while (pContext != NULL)
        {
            IsisStopAllTimers (pContext);
            pContext = pContext->pNext;
        }

        if (IsisRmSendEventToRm (RM_STANDBY_EVT_PROCESSED, RM_NONE) ==
            ISIS_FAILURE)
        {
            CTP_PT ((ISIS_LGST,
                     "CTL <X> : RM active processed event send failed\r\n"));
        }
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlHandleStandbyDown ()\n"));
}

/******************************************************************************
 * Function       : IsisCtrlProcessRmMessage ()
 * Description    : This routine process the RM message - Bulk request /
 *                  bulk update / bulk tail messages
 * Input(s)       : pRmMsg   - Pointer to RM Message
 * Output(s)      : VOID 
 * Globals        : None 
 * Returns        : None 
 *****************************************************************************/

PUBLIC VOID
IsisCtrlProcessRmMessage (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    UINT4               u4SeqNum = 0;
    UINT2               u2OffSet = 0;
    UINT1               u1MsgType = 0;

    RM_PKT_GET_SEQNUM (pRmMsg, &u4SeqNum);

    FTP_PT ((ISIS_LGST, "FLT <T> : Seq. No [ %u ] Currently Being Processed\n",
             u4SeqNum));

    /* Strip off the RM header */
    RM_STRIP_OFF_RM_HDR (pRmMsg, u2DataLen);

    /* Handle packet processing */
    /* Get the message type */
    ISIS_RED_GET_1_BYTE (pRmMsg, u2OffSet, u1MsgType);

    switch (u1MsgType)
    {
        case RM_BULK_UPDT_REQ_MSG:

            /* Send the bulk updates. Once the bulk updates transmission is
             * completed, send te bulk tail message to indicate the
             * completion
             */
            if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE)
                && (gIsisExtInfo.u4PeerCount > 0))
            {
                ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_IN_PROGRESS);
                IsisFltrFormBulkData ();
            }
            break;

        case RM_BULK_UPDATE_MSG:
        case LOCK_STEP_UPDATE_MSG:
            if (ISIS_EXT_IS_FT_STATE () == ISIS_FT_STANDBY)
            {
                IsisFltrProcData (pRmMsg);    /* Process the bulk updates */
            }
            break;

        case RM_BULK_UPDT_TAIL_MSG:

            if (ISIS_EXT_IS_FT_STATE () == ISIS_FT_STANDBY)
            {
                FTP_PT ((ISIS_LGST,
                         "FLT <T> : Sending Event Bulk Update Completion\n"));
                ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_COMPLETED);
                IsisRmSendEventToRm (RM_PROTOCOL_BULK_UPDT_COMPLETION, RM_NONE);
            }
            break;

            /* Add cases for dynamic updates processing */

        default:
            break;
    }

    RM_FREE (pRmMsg);
    IsisSendProtoAckToRM (u4SeqNum);
}

/******************************************************************************
 * Function       : IsisCtrlGoStandby ()
 * Description    : This routine stops, remove and frees all the timers running
 *                  for the given context
 * Input(s)       : None
 * Output(s)      : VOID 
 * Globals        : None 
 * Returns        : None 
 *****************************************************************************/

PUBLIC VOID
IsisCtrlGoStandby ()
{
    tIsisSysContext    *pContext = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGoStandby ()\n"));

    /* If the current node status is idle, then no need to process this event
     * The node will become standby after receiving RM_CONFIG_RESTORE_COMPLETE
     * after completing MSR restoration
     */
    if (ISIS_EXT_IS_FT_STATE () != ISIS_FT_ACTIVE)
    {
        return;
    }

    gIsisExtInfo.u4PeerCount = 0;
    ISIS_EXT_SET_FT_STATUS (ISIS_FT_SUPP_DISABLE);
    ISIS_EXT_SET_FT_STATE (ISIS_FT_STANDBY);
    ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_NOT_STARTED);

/* All dynamic info to be removed */
    pContext = ISIS_GET_CONTEXT ();
    while (pContext != NULL)
    {
        /*Remove all LSPs in Db and Tx */

        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL1))
        {
            IsisUpdPurgeLSPs (pContext, ISIS_LEVEL1);
        }
        if ((pContext->SysActuals.u1SysType == ISIS_LEVEL12)
            || (pContext->SysActuals.u1SysType == ISIS_LEVEL2))
        {
            IsisUpdPurgeLSPs (pContext, ISIS_LEVEL2);
        }
        /*Remove all adjacency related info. */
        IsisAdjProcISDownEvt (pContext);
        /* Stop all the timers and Resets all the statistics */
        IsisCtrlProcISDownEvt (pContext);
        /* To clear nodes from shortest path and Tent */
        IsisDecProcISDownEvt (pContext);

        pContext = pContext->pNext;
    }

    if (IsisRmSendEventToRm (RM_STANDBY_EVT_PROCESSED, RM_NONE) == ISIS_FAILURE)
    {
        CTP_PT ((ISIS_LGST,
                 "CTL <X> : RM active processed event send failed\r\n"));
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGoStandby ()\n"));
}

/*******************************************************************************
 * Function    : IsisCtrlGoActive ()
 * Description : This routine activates all the timers associated with the ISIS
 *               module, generates ISUP event and initialises memory for the
 *               Shortest Path Databases viz. Prev PATH, TENT and PATH. It also
 *               indicates the Data Link Layers about the change in status of
 *               all the circuits
 * Input(s)    : None
 * Output(s)   : None 
 * Globals     : Not Referred or Modified 
 * Returns     : ISIS_SUCCESS, if the ISIS module successfully comes up
 *               ISIS_FAILURE, Otherwise 
 ******************************************************************************/

PUBLIC VOID
IsisCtrlGoActive ()
{
    UINT1               u1PrevState = ISIS_FT_INIT;
    UINT4               u4Event = 0;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    tIsisEvtISStatChange *pISEvt = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisCtrlGoActive ()\n"));

    u1PrevState = ISIS_EXT_IS_FT_STATE ();
    ISIS_EXT_SET_BLK_UPD_STATUS (ISIS_BULK_UPDT_NOT_STARTED);

    if ((u1PrevState == ISIS_FT_STANDBY) || (u1PrevState == ISIS_FT_INIT))
    {
        ISIS_EXT_SET_FT_STATE (ISIS_FT_ACTIVE);
        ISIS_EXT_SET_FT_STATUS (ISIS_FT_SUPP_ENABLE);

        pContext = ISIS_GET_CONTEXT ();

        while (pContext != NULL)
        {
            /* Init cache pointer for ipra table */
            pContext->pLastIPRAEntry = NULL;
            /* If the Active node's System Context is already Operationally
             * ISIS_UP, then skip the 'pContext'. In Standby Node the
             * Operational State of each System Context is ISIS_DOWN
             */

            if (pContext->u1OperState == ISIS_UP)
            {
                pContext = pContext->pNext;
                continue;
            }

            if ((pContext->SysActuals.u1SysExistState == ISIS_ACTIVE)
                && (pContext->SysActuals.u1SysAdminState == ISIS_STATE_ON))
            {
                /* Forming and enqueuing IS UP Event
                 */

                pISEvt = (tIsisEvtISStatChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                    sizeof (tIsisEvtISStatChange));
                if (pISEvt != NULL)
                {
                    pISEvt->u1EvtID = ISIS_EVT_IS_UP;
                    pISEvt->u1PrevState = u1PrevState;
                    MEMCPY (pISEvt->au1SysID,
                            pContext->SysActuals.au1SysID, ISIS_SYS_ID_LEN);

                    IsisUtlSendEvent (pContext, (UINT1 *) pISEvt,
                                      sizeof (tIsisEvtISStatChange));
                }

                pCktRec = pContext->CktTable.pCktRec;

                while (pCktRec != NULL)
                {
                    /* Init cache pointer for adjacency table */
                    pCktRec->pLastAdjEntry = NULL;
                    IsisTrfrIfStatusInd (pContext, pCktRec, ISIS_CKT_UP);
                    pCktRec = pCktRec->pNext;
                }
            }

            pContext = pContext->pNext;
        }
    }

    gIsisExtInfo.u4PeerCount = (UINT4) IsisRmGetStandbyNodeCount ();

    if (u1PrevState == ISIS_FT_STANDBY)
    {
        u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    else
    {
        u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }

    if (IsisRmSendEventToRm (u4Event, RM_NONE) == ISIS_FAILURE)
    {
        CTP_PT ((ISIS_LGST,
                 "CTL <X> : RM active processed event send failed\r\n"));
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisCtrlGoActive ()\n"));
}
#endif
/******************************************************************************
 * Function      : IsisInitModule ()
 * Description   : This routine initialises all the queues and semaphores
 *                 required for ISIS protocol operations. It creates 3 queueus
 *                 one each for the Link Layer packets, RTM data and FTM data.
 *                 It also creates a semaphore for ensuring exclusive access for
 *                 ISIS Task and the Management Task, since both of these may be
 *                 accesing the data structures simultaneously.
 * Input(s)      : None
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : ISIS_SUCCESS, if initialization is successful
 *                 ISIS_FAILURE, otherwise
 *****************************************************************************/

PUBLIC INT4
IsisInitModule (VOID)
{
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisInitModule ()\n"));

    if (IsisTmrCreateTimerList () == ISIS_FAILURE)
    {
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInitModule ()\n"));
        return ISIS_FAILURE;
    }

    /* ISIS Module implements 3 different queues one each for Route Table
     * Manager module and Redundancy Manager module and the third queue is
     * implemented for inter-module communication viz. posting of Exceptions as
     * Events, Lower Layer Data, Timer Events etc. If any of the resource
     * allocations fails, the routine de-allocates all the previously allocated
     * resources and returns
     */
    if (OsixQueCrt
        (ISIS_CTRL_QUEUE, OSIX_MAX_Q_MSG_LEN, ISIS_CTRLQ_DEPTH,
         &gIsisCtrlQId) == OSIX_FAILURE)
    {
        PANIC ((ISIS_LGST, "CTL <P> : Control Queue Creation Failed!!!\n"));

        TmrDeleteTimerList (gIsisTmrListID);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInitModule ()\n"));
        return ISIS_FAILURE;
    }

    if (OsixQueCrt
        (ISIS_RTM_QUEUE, OSIX_MAX_Q_MSG_LEN, ISIS_RTMQ_DEPTH,
         &gIsisRtmQId) == OSIX_FAILURE)
    {
        PANIC ((ISIS_LGST, "CTL <P> : RTM Queue Creation Failed!!!\n"));

        TmrDeleteTimerList (gIsisTmrListID);
        OsixQueDel (gIsisCtrlQId);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInitModule ()\n"));
        return ISIS_FAILURE;
    }

#ifdef ISIS_FT_ENABLED

    if (OsixQueCrt
        (ISIS_FLT_QUEUE, OSIX_MAX_Q_MSG_LEN, ISIS_FLTQ_DEPTH,
         &gIsisFltQId) == OSIX_FAILURE)
    {
        PANIC ((ISIS_LGST, "CTL <P> : FLTR Queue Creation Failed!!!\n"));

        TmrDeleteTimerList (gIsisTmrListID);
        OsixQueDel (gIsisCtrlQId);
        OsixQueDel (gIsisRtmQId);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInitModule ()\n"));
        return ISIS_FAILURE;
    }
#endif

    if (OsixSemCrt (ISIS_SYS_SEM_NAME, &gIsisSemId) == OSIX_FAILURE)
    {
        PANIC ((ISIS_LGST, "CTL <P> : Semaphore Creation Failed!!!\n"));

        TmrDeleteTimerList (gIsisTmrListID);
        OsixQueDel (gIsisCtrlQId);
        OsixQueDel (gIsisRtmQId);
#ifdef ISIS_FT_ENABLED
        OsixQueDel (gIsisFltQId);
#endif
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInitModule ()\n"));
        return ISIS_FAILURE;
    }
    OsixSemGive (gIsisSemId);

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisInitModule ()\n"));
    return ISIS_SUCCESS;
}

/******************************************************************************
 * Function      : IsisProcessEvent ()
 * Description   : This routine processes the Events received from other modules
 * Input(s)      : pIsisMsg - Received message
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 *****************************************************************************/

PUBLIC VOID
IsisProcessEvent (tIsisMsg * pIsisMsg)
{
    UINT1               u1PrevState = ISIS_FT_INIT;
#ifdef ISIS_FT_ENABLED
    UINT1               u1Level = 0;
#endif
    INT4                i4RetVal = 0;
    tIsisMsg           *pAdjMsg = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisAdjDirEntry   *pDirEntry = NULL;
    tIsisAdjDirEntry   *pTmpDirEntry = NULL;
    tIsisEvtDecnRelq   *pEvtDecnRelq = NULL;
    tIsisLLData        *pLLData = NULL;
    tIsisLLData        *pNextLLData = NULL;
    UINT4               u4SysInstIdx = 0;
    UINT4               u4StartTime = 0;
    UINT4               u4RemTime = 0;
    tUtlTm              utlTm;
    tIsisEvtImportLevelChange *pRRDLevleChg = NULL;
    tIsisEvtRRDProtoDisable *pRRDProtoDisable = NULL;
    tIsisEvtMetricChange *pRRDMetricChange = NULL;
    tIsisEvtP2PCktState *pP2PCircuitInfo = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisProcessEvent ()\n"));

    switch (*(UINT1 *) (pIsisMsg->pu1Msg))
    {
        case ISIS_EVT_IP_IF_ADDR_CHANGE:
        case ISIS_EVT_MAN_ADDR_CHANGE:
        case ISIS_EVT_PROT_SUPP_CHANGE:

            /* Since both Update and Adjacency modules require these events, 
             * clone the message before invoking the Adjacency or Update 
             * modules. Both Update and Adjacency module will release the 
             * pIsisMsg once the processing is complete
             */

            if ((pAdjMsg = IsisUtlCloneMsg (pIsisMsg)) != NULL)
            {
                IsisUpdProcCtrlPkt (pIsisMsg);
                IsisAdjProcCtrlPkt (pAdjMsg);
            }
            else
            {
                /* Should never happen. If it happened tune the number of 
                 * buffers allocated for Events (ISIS_BUF_EVTS and messages 
                 * (ISIS_BUF_MSGS) properly
                 */

                WARNING ((ISIS_LGST,
                          "CTL <W>: Unable to Process Event [ %u ]\n",
                          *(UINT1 *) (pIsisMsg->pu1Msg)));

                /* Discard the Event */

                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            }
            break;

        case ISIS_EVT_CKT_CHANGE:

            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                /* Discard the Event */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }
#ifdef ISIS_FT_ENABLED
            if ((pContext->u1OperState != ISIS_UP)
                && (ISIS_EXT_IS_FT_STATE () != ISIS_FT_STANDBY))
#else
            if (pContext->u1OperState != ISIS_UP)
#endif
            {
                IsisAdjProcCtrlPkt (pIsisMsg);
            }

            else if ((pAdjMsg = IsisUtlCloneMsg (pIsisMsg)) != NULL)
            {
                IsisUpdProcCtrlPkt (pIsisMsg);
                IsisAdjProcCtrlPkt (pAdjMsg);
            }
            else
            {
                /* Should never happen. If it happened tune the number of 
                 * buffers allocated for Events (ISIS_BUF_EVTS and messages 
                 * (ISIS_BUF_MSGS) properly */

                WARNING ((ISIS_LGST,
                          "CTL <W>: Unable to Process Event [ %u ]\n",
                          *(UINT1 *) (pIsisMsg->pu1Msg)));

                /* Discard the Event */

                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            }
            break;

        case ISIS_EVT_ADJ_CHANGE:
        case ISIS_EVT_IPRA_CHANGE:
        case ISIS_EVT_SUMM_ADDR_CHANGE:
        case ISIS_EVT_DIS_CHANGE:
        case ISIS_EVT_CHG_IN_DIS_STAT:
        case ISIS_EVT_L1_DECN_COMPLETE:
        case ISIS_EVT_L2_DECN_COMPLETE:
        case ISIS_EVT_LSP_DBOL:
        case ISIS_EVT_LSP_DBOL_RECOV:
        case ISIS_EVT_CORR_LSP_DET:
        case ISIS_EVT_DISTANCE_CHANGE:

            IsisUpdProcCtrlPkt (pIsisMsg);
            break;

        case ISIS_EVT_IS_UP:

            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                /* Discard the Event */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }

            IsisCtrlCopyConfigsToActuals (pContext);

            i4RetVal = IsisCtrlAllocShortPath (pContext);

            if (i4RetVal == ISIS_FAILURE)
            {
                PANIC ((ISIS_LGST,
                        ISIS_MEM_ALLOC_FAIL " : Shortest Path Database!!\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
            }

            u1PrevState =
                ((tIsisEvtISStatChange *) pIsisMsg->pu1Msg)->u1PrevState;
#ifdef ISIS_FT_ENABLED
            if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
                (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
            {
#endif
                IsisStartAllTimers (pContext);
#ifdef ISIS_FT_ENABLED
            }
#endif
            IsisCtrlProcISUpEvt (pContext, u1PrevState);
            IsisUpdProcCtrlPkt (pIsisMsg);

            /* Now that the Context has come up, mark its Operational State 
             * as UP */

#ifdef ISIS_FT_ENABLED
            if ((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE) &&
                (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))
            {
#endif
                pContext->u1OperState = ISIS_UP;
#ifdef ISIS_FT_ENABLED
            }
#endif

            /* Starting Holding Timer for all the Adjacencies pertaining to
             * this Instance */

            pDirEntry = pContext->AdjDirTable.pDirEntry;

            while (pDirEntry != NULL)
            {
                pTmpDirEntry = pDirEntry->pNext;
                if (pDirEntry->pAdjEntry->u1IsisGRHelperStatus ==
                    ISIS_GR_HELPING)
                {
                    MEMSET (&utlTm, 0, sizeof (tUtlTm));
                    UtlGetTime (&utlTm);
                    /* Get the seconds since the base year (2000) */
                    u4StartTime = IsisGrGetSecondsSinceBase (utlTm);

                    u4RemTime =
                        (u4StartTime - pDirEntry->pAdjEntry->u4StartTime);
                    if (pDirEntry->pAdjEntry->u2AdjGRTime > u4RemTime)
                    {
                        /*Restarting with the remaining time for Helper */
                        IsisTmrRestartECTimer (pContext, ISIS_ECT_HOLDING,
                                               pDirEntry->u4DirIdx,
                                               (UINT4) (pDirEntry->pAdjEntry->
                                                        u2AdjGRTime -
                                                        (UINT2) u4RemTime),
                                               &(pDirEntry->pAdjEntry->
                                                 u1TmrIdx));
                    }
                    else
                    {
                        /*Even if GR timer has expired, the adjacency is started for
                         *holding time, so as to maintain the routes, and not flush the
                         *adjacency immediately when the restarter is present*/
                        IsisTmrRestartECTimer (pContext, ISIS_ECT_HOLDING,
                                               pDirEntry->u4DirIdx,
                                               pDirEntry->pAdjEntry->u2HoldTime,
                                               &(pDirEntry->pAdjEntry->
                                                 u1TmrIdx));
                    }

                }
                else
                {
                    IsisTmrStartECTimer (pContext, ISIS_ECT_HOLDING,
                                         pDirEntry->u4DirIdx,
                                         (UINT4) pDirEntry->pAdjEntry->
                                         u2HoldTime,
                                         &(pDirEntry->pAdjEntry->u1TmrIdx));
                }
                pDirEntry = pTmpDirEntry;
            }

            break;

        case ISIS_EVT_IS_DESTROY:

            IsisCtrlProcISDestroyEvt (pIsisMsg);
            break;

        case ISIS_EVT_IS_DOWN:

            PANIC ((ISIS_LGST, "CTL <P> : IS DOWN Event received.\n"));
            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                /* Discard the Event */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }

            /* Both Adjacency and Update module will require the event data for
             * further processing. Clone it
             */

            if ((pAdjMsg = IsisUtlCloneMsg (pIsisMsg)) != NULL)
            {
                IsisUpdProcCtrlPkt (pIsisMsg);
                IsisAdjProcCtrlPkt (pAdjMsg);
            }
            else
            {
                /* Should never happen. If it happened tune the number of 
                 * buffers allocated for Events (ISIS_BUF_EVTS and messages 
                 * (ISIS_BUF_MSGS) properly
                 */

                WARNING ((ISIS_LGST,
                          "CTL <W>: Unable to Process Event [ %u ]\n",
                          *(UINT1 *) (pIsisMsg->pu1Msg)));

                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            }
            IsisCtrlProcISDownEvt (pContext);
            IsisDecProcISDownEvt (pContext);

            /* NOTE: Adjacency and Update modules have already processed the 
             * DOWN event and must have deleted all the SelfLSP information. 
             * We can now mark the Operational Status as DOWN
             */

            pContext->u1OperState = ISIS_DOWN;
            /*Resetting attached bit */
            pContext->SelfLSP.u1L1LSPFlags &= ~ISIS_FLG_ATT_DEF;
            break;

        case ISIS_EVT_ELECT_DIS:
        case ISIS_EVT_PRIORITY_CHANGE:

            IsisAdjProcCtrlPkt (pIsisMsg);
            break;

        case ISIS_EVT_SHUTDOWN:

            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

            CTP_PT ((ISIS_LGST, "CTL <T> : Shutting Down All Instances...\n"));
            IsisShutDownAll ();
            break;

        case ISIS_EVT_RESETALL:

            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

            CTP_PT ((ISIS_LGST, "CTL <T> : Resetting All Instances...\n"));
            IsisResetAll ();
            break;

        case ISIS_EVT_DECN_RELQ:

            /* We need to ensure at this point of that timers processing is
             * over. Because this event is posted internally and keep on
             * coming till the decision is over leaving the timers fired
             * unprocessed 
             */
            IsisTmrProcTimeOut ();

            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                /* Discard the Event */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }

            pEvtDecnRelq = (tIsisEvtDecnRelq *) (VOID *) pIsisMsg->pu1Msg;
            i4RetVal = IsisDecRevSPF (pContext,
                                      pEvtDecnRelq->u1MetIdx,
                                      pEvtDecnRelq->u1Level,
                                      pEvtDecnRelq->u1AttFlag,
                                      pEvtDecnRelq->u1NearL2Flag,
                                      pEvtDecnRelq->u1AttStatChg,
                                      pEvtDecnRelq->u1MTIndex);

#ifdef ISIS_FT_ENABLED
            u1Level = pEvtDecnRelq->u1Level;
#endif
            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

            /* If the Decision process is over, start processing of the Updt 
             * packets which were hold till now in context
             */
            if (i4RetVal != ISIS_DECN_RELQ)
            {
#ifdef ISIS_FT_ENABLED
                ISIS_FLTR_SCH_SPF_LSU (pContext, u1Level);
#endif
                pLLData = pContext->pLLDataNode;
                while (pLLData != NULL)
                {
                    IsisUpdProcCtrlPkt ((tIsisMsg *) (VOID *) (pLLData->
                                                               pu1Msg));
                    pNextLLData = pLLData->pNext;
                    ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pLLData);
                    pLLData = pNextLLData;
                }
                pContext->pLLDataNode = NULL;
            }
            break;
        case ISIS_EVT_VCM:

            CTP_PT ((ISIS_LGST, "CTL <T> : Processing VCM Data\n"));
            IsisVcmMsgHandler (pIsisMsg);
            break;

        case ISIS_EVT_IMPORT_LEVEL_CHG:
            CTP_PT ((ISIS_LGST,
                     "CTL <T> : Processing RRD ImportLevel Change\n"));
            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                /* Discard the Event */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }
            pRRDLevleChg =
                (tIsisEvtImportLevelChange *) (VOID *) (pIsisMsg->pu1Msg);
            if (pRRDLevleChg->u1ImportLevel == ISIS_IMPORT_LEVEL_12)
            {
                IsisHandleImportLevelChange (pContext,
                                             pRRDLevleChg->u1PrevImportLevel,
                                             ISIS_DUPLICATE_IPRA_TLV);
            }
            else if (pRRDLevleChg->u1PrevImportLevel == ISIS_IMPORT_LEVEL_12)
            {
                IsisHandleImportLevelChange
                    (pContext,
                     ((UINT1)
                      (pRRDLevleChg->
                       u1PrevImportLevel & (~pRRDLevleChg->u1ImportLevel))),
                     ISIS_DELETE_IPRA_TLV);
            }
            else
            {
                IsisHandleImportLevelChange (pContext,
                                             pRRDLevleChg->u1PrevImportLevel,
                                             ISIS_MOVE_IPRA_TLV);
            }
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;
        case ISIS_EVT_RRD_PROTO_DISABLE:
            CTP_PT ((ISIS_LGST, "CTL <T> : Processing RRD Disable Event\n"));
            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                /* Discard the Event */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }
            pRRDProtoDisable =
                (tIsisEvtRRDProtoDisable *) (VOID *) (pIsisMsg->pu1Msg);
            if ((pRRDProtoDisable->u1Level & ISIS_LEVEL1))
            {
                IsisHandleRRDProtoDisable (pContext,
                                           pRRDProtoDisable->u2ProtoMask,
                                           ISIS_LEVEL1);
            }
            if ((pRRDProtoDisable->u1Level & ISIS_LEVEL2))
            {
                IsisHandleRRDProtoDisable (pContext,
                                           pRRDProtoDisable->u2ProtoMask,
                                           ISIS_LEVEL2);
            }
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            if (pRRDProtoDisable->u2ProtoMask == ISIS_IMPORT_ALL)
            {
                pContext->RRDInfo.u1RRDImportType = 0;
                MEMSET (pContext->RRDInfo.au1RRDRMapName, 0,
                        sizeof (pContext->RRDInfo.au1RRDRMapName));
            }
            break;
        case ISIS_EVT_RRD_METRIC_CHG:
            CTP_PT ((ISIS_LGST,
                     "CTL <T> : Processing RRD Metric Change Event\n"));
            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                /* Discard the Event */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }
            pRRDMetricChange =
                (tIsisEvtMetricChange *) (VOID *) (pIsisMsg->pu1Msg);

            IsisHandleMetricChange (pContext,
                                    pRRDMetricChange->u1MetricIndex,
                                    pRRDMetricChange->u4Metric);
            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

            break;
        case ISIS_EVT_P2P_CKT_CHG:
            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                /* Discard the Event */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }
            pP2PCircuitInfo =
                (tIsisEvtP2PCktState *) (VOID *) (pIsisMsg->pu1Msg);
            CTP_PT ((ISIS_LGST,
                     "CTL <T> : Processing Point-to-point Circuit change Event - State [%s]\n",
                     ISIS_GET_CIRC_STATE_STR (pP2PCircuitInfo->u1State)));

            IsisUpdHandleP2PCircuitChange (pContext,
                                           pP2PCircuitInfo->u1State,
                                           pP2PCircuitInfo->u4CktIndex);
            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        default:

            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessEvent ()\n"));
}

/*******************************************************************************
 * Function      : IsisProcessData ()
 * Description   : This routine processes the data received from other modules
 *                 by appropriately categorising the message type  
 * Input(s)      : pIsisMsg - Received message
 *                 pCktRec  - Pointer to the circuit over the which the message
 *                            is received
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

PUBLIC VOID
IsisProcessData (tIsisMsg * pIsisMsg, tIsisCktEntry * pCktRec)
{
    INT4                i4RetValue = ISIS_FAILURE;
    INT4                i4RetVal = 0;
    tIsisMsg           *pCtlMsg = NULL;
    UINT4               u4SysInstIdx = 0;
    UINT4               u4Metric = 0;
    tIsisSysContext    *pContext = NULL;
    tIp6Addr            Ip6Addr;
    tIsisMetric         Metric;
    tIsisLLInfo        *pLLInfo = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    tIsisIPRATLV       *pTravTlv = NULL;
    tIsisIPRATLV       *pNextTlv = NULL;
    tIsisLSPInfo       *pTravLsp = NULL;
    tIsisMDT           *pMDT = NULL;
    UINT1               u1RemBytes = 0;
    UINT1               u2MTId = 0;
    tIsisEvtIPIFChange *pIPIfEvt = NULL;
    tRtmRouteDataInfo  *pRtmRouteData = NULL;
    tIsisCktEntry      *pTempCktRec = NULL;
#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
    tIp6If             *pIf6 = NULL;
    INT4                i4count = 0;
#endif
#endif
#ifdef BFD_WANTED
    tIsisAdjEntry      *pTravAdj = NULL;
    tIsisAdjEntry      *pAdjEntry = NULL;
    tIsisAdjEntry      *pL1AdjEntry = NULL;
    tIsisAdjEntry      *pL2AdjEntry = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisIpAddr        *pIpAddr = NULL;
    UINT1               u1MtId;
#endif
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisProcessData ()\n"));

    MEMSET (&Ip6Addr, 0, ISIS_MAX_IPV6_ADDR_LEN);
    MEMSET (&Metric, 0, sizeof (Metric));

    switch (pIsisMsg->u1MsgType)
    {
        case ISIS_SYS_ROUTE_UPDATE:

            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Processing RTM ISIS_SYS_ROUTE_UPDATE\n"));
            pRtmRouteData = (tRtmRouteDataInfo *) (void *) pIsisMsg->pu1Msg;
            IsisRtmProcessUpdate (&(pRtmRouteData->unRtInfo.RtInfo),
                                  pRtmRouteData->u2ProtoId);

            /* Free the pIsisMsg and its pu1Msg */
            ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

            break;

        case ISIS_SYS_ROUTE_CHANGE:

            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Processing RTM ISIS_SYS_ROUTE_CHANGE\n"));
            pRtmRouteData = (tRtmRouteDataInfo *) (void *) pIsisMsg->pu1Msg;
            IsisRtmProcessRtChange (&(pRtmRouteData->unRtInfo.RtInfo),
                                    pRtmRouteData->u2ProtoId);

            /* Free the pIsisMsg and its pu1Msg */
            ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_SYS_ROUTE6_UPDATE:

            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Processing RTM ISIS_SYS_ROUTE6_UPDATE\n"));
            pRtmRouteData = (tRtmRouteDataInfo *) (void *) pIsisMsg->pu1Msg;
            IsisRtm6ProcessUpdate (&(pRtmRouteData->unRtInfo.Rt6Info),
                                   pRtmRouteData->u2ProtoId);

            /* Free the pIsisMsg and its pu1Msg */
            ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_SYS_ROUTE6_CHANGE:

            RTP_PT ((ISIS_LGST,
                     "RTM <T> : Processing RTM ISIS_SYS_ROUTE6_CHANGE\n"));
            pRtmRouteData = (tRtmRouteDataInfo *) (void *) pIsisMsg->pu1Msg;
            IsisRtm6ProcessRtChange (&(pRtmRouteData->unRtInfo.Rt6Info),
                                     pRtmRouteData->u2ProtoId);

            /* Free the pIsisMsg and its pu1Msg */
            ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_MSG_LL_DATA:

            /* The System accepts the messages and events 
             * only when the system is 
             * -- Operationally UP
             * -- if Fault Tolerance is supported 
             *       FT State is ACTIVE or LSU_ENABLE
             */

            /* The System Accepts LL Data received from Circuit only
             * when
             * -- Circuit Receive status is enabled
             * -- Circuit Type is either Point to Point or Broadcast
             */

            if (pCktRec != NULL)
            {
                i4RetVal = ISIS_CONTEXT_ACTIVE (pCktRec->pContext);
            }
            else
            {
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }
            if ((pCktRec->u1CktType != ISIS_BC_CKT)
                && (pCktRec->u1CktType != ISIS_P2P_CKT))
            {
                pIsisMsg->u1DataType = (UINT1) ISIS_DATA_NORMAL;
            }
            if ((i4RetVal == ISIS_TRUE)
                && (((pCktRec->u1CktType == ISIS_P2P_CKT)
                     || (pCktRec->u1CktType == ISIS_BC_CKT))
                    && (pCktRec->bRxEnable == ISIS_TRUE)))
            {
                IsisProcLLData (pIsisMsg);
            }
            else
            {
                CTP_PT ((ISIS_LGST, "CTL <T> : LL Data, Context Inactive\n"));
                IsisFreeDLLBuf (pIsisMsg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            }
            break;

#ifdef ISIS_FT_ENABLED
        case ISIS_MSG_FTM_DATA:

            CTP_PT ((ISIS_LGST, "CTL <T> : Processing FTM Data\n"));
            IsisFltrProcCtrlPkt (pIsisMsg);
            break;
#endif

        case ISIS_MSG_RTM_DATA:

            CTP_PT ((ISIS_LGST, "CTL <T> : Processing RTM Data\n"));
            IsisProcRtmPkt (pIsisMsg);
            break;

        case ISIS_MSG_RTM6_DATA:

            CTP_PT ((ISIS_LGST, "CTL <T> : Processing RTM6 Data\n"));
            IsisProcRtm6Pkt (pIsisMsg);
            break;

        case ISIS_MSG_EVENT:

            /* Log the events into the LOG table for debugging 
             * purposes
             */
            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            i4RetValue = IsisCtrlGetSysContext (u4SysInstIdx, &pContext);
            IsisCtrlLogEvent (pContext, pIsisMsg->pu1Msg);
            IsisProcessEvent (pIsisMsg);
            UNUSED_PARAM (i4RetValue);
            break;

        case ISIS_MSG_IF_STAT_IND:

            CTP_PT ((ISIS_LGST,
                     "CTL <T> : Processing I/F Status Indication\n"));
            if ((pCtlMsg = IsisUtlCloneMsg (pIsisMsg)) != NULL)
            {
                IsisProcessIfStatInd (pCtlMsg);
                IsisAdjProcCtrlPkt (pIsisMsg);
            }
            else
            {
                /* Should never happen. If it happened tune the number of
                 * buffers allocated for Events (ISIS_BUF_EVTS and messages
                 * (ISIS_BUF_MSGS) properly
                 */
                WARNING ((ISIS_LGST,
                          "CTL <W>: Unable to Process Event [ %u ]\n",
                          *(UINT1 *) (pIsisMsg->pu1Msg)));

                /* Discard the Event
                 */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            }

            break;

        case ISIS_MSG_SEC_IF_STAT_IND:

            CTP_PT ((ISIS_LGST,
                     "CTL <T> : Processing I/F (Secondary IP) Status Indication\n"));
            if ((pCtlMsg = IsisUtlCloneMsg (pIsisMsg)) != NULL)
            {
                IsisProcessIfStatInd (pCtlMsg);
            }
            else
            {
                /* Should never happen. If it happened tune the number of
                 * buffers allocated for Events (ISIS_BUF_EVTS and messages
                 * (ISIS_BUF_MSGS) properly
                 */
                WARNING ((ISIS_LGST,
                          "CTL <W>: Unable to Process Event [ %u ]\n",
                          *(UINT1 *) (pIsisMsg->pu1Msg)));

                /* Discard the Event
                 */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            }

            break;

#ifdef ROUTEMAP_WANTED
        case ISIS_MSG_ROUTEMAP_UPDATE:
            IsisProcessRMapHandler (pIsisMsg);
            break;
#endif /* ROUTEMAP_WANTED */

        case ISIS_MSG_IF_ADDR_CHG:

            pLLInfo = (tIsisLLInfo *) (VOID *) pIsisMsg->pu1Msg;

            if (pIsisMsg->u1LenOrStat == ISIS_IP_IF_ADDR_ADD)
            {
                if (pCktRec->u1OperState == ISIS_UP)
                {
                    ISIS_SET_MT_ID (pCktRec->u1IfMTId, pLLInfo->u1AddrType);
                    pCktRec->u1ProtoSupp |= pLLInfo->u1AddrType;

#ifdef BFD_WANTED
                    /* In case of single topology ISIS,
                     * Register with BFD for all the adj entries in the 
                     * circuit record, for the new protocol support
                     * being added in the circuit record.*/
                    u4SysInstIdx = pCktRec->pContext->u4SysInstIdx;
                    pTravAdj = pCktRec->pAdjEntry;
                    while (pTravAdj != NULL)
                    {
                        if ((pCktRec->pContext->u1IsisMTSupport == ISIS_FALSE)
                            && (pCktRec->u1IsisBfdStatus == ISIS_BFD_ENABLE))
                        {
                            ISIS_BFD_REGISTER (pCktRec->pContext, pTravAdj,
                                               pCktRec);
                        }
#ifdef ISIS_FT_ENABLED
                        ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_ADD,
                                           pTravAdj);
#endif
                        pTravAdj = pTravAdj->pNext;
                    }
#endif

                    if (pCktRec->pContext->u1IsisMTSupport == ISIS_TRUE)
                    {
                        /* If MT #0 is alone supported don't add MT #0 TLV in LSP,
                         * since it is optional*/

                        if (pCktRec->u1IfMTId != ISIS_MT0_BITMAP)
                        {
                            pMDT =
                                (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                             sizeof (tIsisMDT));
                            if (pMDT == NULL)
                            {
                                PANIC ((ISIS_LGST,
                                        ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                                DETAIL_FAIL (ISIS_CR_MODULE);
                                return;
                            }

                            pMDT->u1Cmd = ISIS_CMD_ADD;
                            pMDT->u1TLVType = ISIS_MT_TLV;
                            for (u2MTId = 0; u2MTId <= ISIS_MAX_MT; u2MTId++)
                            {
                                /* Left shift is done to get the corresponding MT_BIT_MAP values
                                 * ex. for MT #0, (1 << 0) = 1 (ISIS_MT0_BITMAP)
                                 *     for MT #2, (1 << 2) = 4 (ISIS_MT2_BITMAP)
                                 */
                                if (pCktRec->u1IfMTId & (1 << u2MTId))
                                {
                                    pMDT->u2MTId = u2MTId;
                                    if (pCktRec->pContext->SysActuals.
                                        u1SysType != ISIS_LEVEL2)
                                    {
                                        pMDT->u1LSPType =
                                            ISIS_L1_NON_PSEUDO_LSP;
                                        IsisUpdUpdateSelfLSP (pCktRec->pContext,
                                                              0, pMDT);

                                    }

                                    if (pCktRec->pContext->SysActuals.
                                        u1SysType != ISIS_LEVEL1)
                                    {
                                        pMDT->u1LSPType =
                                            ISIS_L2_NON_PSEUDO_LSP;
                                        IsisUpdUpdateSelfLSP (pCktRec->pContext,
                                                              0, pMDT);

                                    }
                                }
                            }
                            if (pCktRec->u1IfMTId == ISIS_MT2_BITMAP)
                            {
                                for (pTempCktRec =
                                     pCktRec->pContext->CktTable.pCktRec;
                                     pTempCktRec != NULL;
                                     pTempCktRec = pTempCktRec->pNext)
                                {
                                    if (pTempCktRec->u1IfMTId & ISIS_MT0_BITMAP)
                                    {
                                        pMDT->u2MTId = 0;
                                        if (pTempCktRec->pContext->SysActuals.
                                            u1SysType != ISIS_LEVEL2)
                                        {
                                            pMDT->u1LSPType =
                                                ISIS_L1_NON_PSEUDO_LSP;
                                            IsisUpdUpdateSelfLSP (pTempCktRec->
                                                                  pContext, 0,
                                                                  pMDT);

                                        }
                                        if (pTempCktRec->pContext->SysActuals.
                                            u1SysType != ISIS_LEVEL1)
                                        {
                                            pMDT->u1LSPType =
                                                ISIS_L2_NON_PSEUDO_LSP;
                                            IsisUpdUpdateSelfLSP (pTempCktRec->
                                                                  pContext, 0,
                                                                  pMDT);
                                        }
                                        break;
                                    }
                                }
                            }
                            ISIS_MEM_FREE (ISIS_BUF_MDTP, (UINT1 *) pMDT);
                        }
                    }

                    if ((pLLInfo->u1AddrType == ISIS_ADDR_IPV6)
                        && (pCktRec->pContext->u1IsisMTSupport == ISIS_TRUE))
                    {

                        /*Updating LSP with MT IS ADJ TLV - as the mtid for the circuit has been updated */
                        pMDT =
                            (tIsisMDT *) ISIS_MEM_ALLOC (ISIS_BUF_MDTP,
                                                         sizeof (tIsisMDT));
                        if (pMDT == NULL)
                        {
                            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : MDT\n"));
                            DETAIL_FAIL (ISIS_CR_MODULE);
                        }
                        else
                        {
                            pMDT->u1Cmd = ISIS_CMD_ADD;
                            pMDT->u2MTId = ISIS_IPV6_UNICAST_MT_ID;
                            pMDT->u1TLVType = ISIS_MT_IS_REACH_TLV;
                            if (((pCktRec->u1CktLevel == ISIS_LEVEL1)
                                 || (pCktRec->u1CktLevel == ISIS_LEVEL12))
                                && (pCktRec->pL1CktInfo != NULL)
                                && (pCktRec->pL1CktInfo->u4NumAdjs != 0))
                            {
                                pMDT->u1LSPType = ISIS_L1_NON_PSEUDO_LSP;
                                ISIS_SET_METRIC (pCktRec->pContext, pMDT,
                                                 pCktRec->pL1CktInfo);
                                if (pCktRec->u1CktType == ISIS_BC_CKT)
                                {
                                    MEMCPY (pMDT->au1AdjSysID,
                                            pCktRec->pL1CktInfo->au1CktLanDISID,
                                            ISIS_SYS_ID_LEN +
                                            ISIS_PNODE_ID_LEN);
                                }
                                else if ((pCktRec->pContext->u1IsisMTSupport ==
                                          ISIS_FALSE)
                                         ||
                                         ((pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                                          && (pCktRec->pAdjEntry)
                                          && (pCktRec->pAdjEntry->
                                              u1AdjMTId & ISIS_MT2_BITMAP)))
                                {
                                    /* This is for point-to-point interface */
                                    MEMCPY (pMDT->au1AdjSysID,
                                            pCktRec->pAdjEntry->au1AdjNbrSysID,
                                            ISIS_SYS_ID_LEN);
                                    pMDT->au1AdjSysID[ISIS_SYS_ID_LEN] = 0;
                                    i4RetVal =
                                        IsisUpdUpdateSelfLSP (pCktRec->pContext,
                                                              pCktRec->u4CktIdx,
                                                              pMDT);
                                    ISIS_MEM_FREE (ISIS_BUF_MDTP,
                                                   (UINT1 *) pMDT);
                                }
                            }
                            if (((pCktRec->u1CktLevel == ISIS_LEVEL2)
                                 || (pCktRec->u1CktLevel == ISIS_LEVEL12))
                                && (pCktRec->pL2CktInfo != NULL)
                                && (pCktRec->pL2CktInfo->u4NumAdjs != 0))
                            {
                                pMDT->u1LSPType = ISIS_L2_NON_PSEUDO_LSP;
                                ISIS_SET_METRIC (pCktRec->pContext, pMDT,
                                                 pCktRec->pL2CktInfo);
                                if (pCktRec->u1CktType == ISIS_BC_CKT)
                                {
                                    MEMCPY (pMDT->au1AdjSysID,
                                            pCktRec->pL2CktInfo->au1CktLanDISID,
                                            ISIS_SYS_ID_LEN +
                                            ISIS_PNODE_ID_LEN);
                                }
                                else if ((pCktRec->pContext->u1IsisMTSupport ==
                                          ISIS_FALSE)
                                         ||
                                         ((pCktRec->u1IfMTId & ISIS_MT2_BITMAP)
                                          && (pCktRec->pAdjEntry)
                                          && (pCktRec->pAdjEntry->
                                              u1AdjMTId & ISIS_MT2_BITMAP)))
                                {
                                    /* This is for point-to-point interface */
                                    MEMCPY (pMDT->au1AdjSysID,
                                            pCktRec->pAdjEntry->au1AdjNbrSysID,
                                            ISIS_SYS_ID_LEN);
                                    pMDT->au1AdjSysID[ISIS_SYS_ID_LEN] = 0;
                                    i4RetVal =
                                        IsisUpdUpdateSelfLSP (pCktRec->pContext,
                                                              pCktRec->u4CktIdx,
                                                              pMDT);
                                    ISIS_MEM_FREE (ISIS_BUF_MDTP,
                                                   (UINT1 *) pMDT);
                                }
                            }
                            if (pCktRec->u1CktType == ISIS_BC_CKT)
                            {
                                if (((pCktRec->pL1CktInfo != NULL)
                                     && (pCktRec->u1CktLevel == ISIS_LEVEL1)
                                     && (pCktRec->pL1CktInfo->u4NumAdjs == 0))
                                    || ((pCktRec->pL2CktInfo != NULL)
                                        && (pCktRec->u1CktLevel == ISIS_LEVEL2)
                                        && (pCktRec->pL2CktInfo->u4NumAdjs ==
                                            0)))

                                {
                                    /* There is only one system on the LAN, and Hence current DIS is NULL.
                                     * No need to Add IS Adj TLV
                                     */
                                    ISIS_MEM_FREE (ISIS_BUF_MDTP,
                                                   (UINT1 *) pMDT);
                                }
                                else
                                {
                                    i4RetVal =
                                        IsisUpdUpdateSelfLSP (pCktRec->pContext,
                                                              pCktRec->u4CktIdx,
                                                              pMDT);
                                    if (i4RetVal != ISIS_SUCCESS)
                                    {
                                        UPP_PT ((ISIS_LGST,
                                                 "UPD <E> : Unable To Add MT IS TLV in LSP\n"));
                                    }
                                }
                            }
                        }
                    }
                }

                /* Add IPRA entry for ipv6 alone. For ipv4 it will be handled in 
                 * ipv4 state change handler
                 */
                if (pLLInfo->u1AddrType == ISIS_ADDR_IPV6)
                {
                    CTP_PT ((ISIS_LGST,
                             "CTL <T> : Processing IPv6 Address Change Indication - UP\n"));

                    if ((pCktRec->u1CktLevel != ISIS_LEVEL2) &&
                        (pCktRec->pL1CktInfo != NULL))
                    {
                        u4Metric = pCktRec->pL1CktInfo->u4FullMetric;
                        MEMCPY (&Metric, pCktRec->pL1CktInfo->Metric,
                                sizeof (tIsisMetric));
                    }
                    else
                    {
                        if (pCktRec->pL2CktInfo != NULL)
                        {
                            u4Metric = pCktRec->pL2CktInfo->u4FullMetric;
                            MEMCPY (&Metric, pCktRec->pL2CktInfo->Metric,
                                    sizeof (tIsisMetric));
                        }
                    }
                    MEMCPY (&Ip6Addr, pLLInfo->au1IpV6Addr,
                            ISIS_MAX_IPV6_ADDR_LEN);
                    if (IS_ADDR_LLOCAL (Ip6Addr) == FALSE)
                    {
                        IsisCtrlAddAutoIPRA (pCktRec->pContext,
                                             pCktRec->u4CktIfIdx,
                                             pCktRec->u4CktIfSubIdx,
                                             pLLInfo->au1IpV6Addr,
                                             pLLInfo->u1IpV6PrefixLen, Metric,
                                             u4Metric, pLLInfo->u1AddrType);

                        i4RetVal =
                            IsisCtrlGetIPIf (pCktRec->pContext,
                                             pCktRec->u4CktIfIdx,
                                             pCktRec->u4CktIfSubIdx,
                                             pLLInfo->au1IpV6Addr,
                                             ISIS_ADDR_IPV6, &pIPIfRec);
                        if (i4RetVal == ISIS_SUCCESS)
                        {
                            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                            pIsisMsg->pu1Msg = NULL;
                            pLLInfo = NULL;
                            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                            return;
                        }

                        pIPIfRec = (tIsisIPIfAddr *)
                            ISIS_MEM_ALLOC (ISIS_BUF_IPIF,
                                            sizeof (tIsisIPIfAddr));
                        if (pIPIfRec == NULL)
                        {
                            PANIC ((ISIS_LGST,
                                    ISIS_MEM_ALLOC_FAIL " : IPIf Entry\n"));
                            DETAIL_FAIL (ISIS_CR_MODULE);
                            IsisUtlFormResFailEvt (pCktRec->pContext,
                                                   ISIS_BUF_IPIF);
                            /* Free the pIsisMsg and its pu1Msg */
                            ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                            pIsisMsg->pu1Msg = NULL;
                            pLLInfo = NULL;
                            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                            return;
                        }
                        MEMCPY (pIPIfRec->au1IPAddr, pLLInfo->au1IpV6Addr,
                                ISIS_MAX_IPV6_ADDR_LEN);
                        pIPIfRec->u4CktIfIdx = pCktRec->u4CktIfIdx;
                        pIPIfRec->u1AddrType = pLLInfo->u1AddrType;
                        pIPIfRec->u4CktIfSubIdx = pCktRec->u4CktIfSubIdx;
                        pIPIfRec->u1ExistState = ISIS_ACTIVE;
                        IsisCtrlAddIPIf (pCktRec->pContext, pIPIfRec);

                    }
                }
                else if (pLLInfo->u1AddrType == ISIS_ADDR_IPV4)
                {
                    CTP_PT ((ISIS_LGST,
                             "CTL <T> : Processing IPv4 Address Change Indication - UP\n"));
                }
            }
            else
            {

                if (pLLInfo->u1AddrType == ISIS_ADDR_IPV6)
                {
                    CTP_PT ((ISIS_LGST,
                             "CTL <T> : Processing IPv6 Address Change Indication - DELETE\n"));
#ifdef BFD_WANTED
                    /* Deregister from BFD, before deleting the address from 
                     * circuit record */
                    u4SysInstIdx = pCktRec->pContext->u4SysInstIdx;
                    pTravAdj = pCktRec->pAdjEntry;
                    while (pTravAdj != NULL)
                    {
                        if (pTravAdj->u1Mt2BfdRegd == ISIS_BFD_TRUE)
                        {
                            IsisDeRegisterWithBfd (u4SysInstIdx, pCktRec,
                                                   pTravAdj,
                                                   ISIS_IPV6_UNICAST_MT_ID,
                                                   ISIS_TRUE);
#ifdef ISIS_FT_ENABLED
                            ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_ADD,
                                               pTravAdj);
#endif
                        }
                        pTravAdj = pTravAdj->pNext;
                    }
#endif

                    pContext = pCktRec->pContext;
                    pIPIfRec = pContext->IPIfTable.pIPIfAddr;

                    while (pIPIfRec != NULL)
                    {
                        if ((pIPIfRec->u4CktIfIdx == pCktRec->u4CktIfIdx)
                            && (pIPIfRec->u4CktIfSubIdx ==
                                pCktRec->u4CktIfSubIdx)
                            && (pIPIfRec->u1AddrType == ISIS_ADDR_IPV6)
                            &&
                            (MEMCMP
                             (pIPIfRec->au1IPAddr, pLLInfo->au1IpV6Addr,
                              ISIS_MAX_IPV6_ADDR_LEN) == 0))
                        {
                            pIPIfEvt = (tIsisEvtIPIFChange *)
                                ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                sizeof (tIsisEvtIPIFChange));

                            if (pIPIfEvt != NULL)
                            {
                                pIPIfEvt->u1EvtID = ISIS_EVT_IP_IF_ADDR_CHANGE;
                                pIPIfEvt->u1Status = ISIS_IP_IF_ADDR_DEL;
                                pIPIfEvt->u4CktIfIdx = pCktRec->u4CktIfIdx;
                                pIPIfEvt->u4CktIfSubIdx =
                                    pCktRec->u4CktIfSubIdx;
                                pIPIfEvt->u1AddrType = ISIS_ADDR_IPV6;
                                MEMCPY (pIPIfEvt->au1IPAddr,
                                        pIPIfRec->au1IPAddr,
                                        ISIS_MAX_IPV6_ADDR_LEN);
                                IsisUtlSendEvent (pContext, (UINT1 *) pIPIfEvt,
                                                  sizeof (tIsisEvtIPIFChange));
                            }
                            else
                            {
                                PANIC ((ISIS_LGST,
                                        ISIS_MEM_ALLOC_FAIL
                                        " : IP Interface Change\n"));
                                DETAIL_FAIL (ISIS_CR_MODULE);
                            }
                            IsisCtrlDelIPIf (pContext, pCktRec->u4CktIfIdx,
                                             pCktRec->u4CktIfSubIdx,
                                             pIPIfRec->au1IPAddr,
                                             ISIS_ADDR_IPV6);
                            break;
                        }
                        pIPIfRec = pIPIfRec->pNext;
                    }
                    IsisCtrlDelAutoSecIPRA (pCktRec->pContext,
                                            pCktRec->u4CktIfIdx,
                                            pCktRec->u4CktIfSubIdx,
                                            pLLInfo->u1AddrType,
                                            pLLInfo->au1IpV6Addr);

#ifdef IP6_WANTED
#ifndef LNXIP6_WANTED
                    /* Check for Valid Interface  */
                    pIf6 = Ip6ifGetEntry (pCktRec->u4CktIfIdx);
                    if (pIf6 == NULL)
                    {
                        /* Invalid Interface */
                        ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
                        pIsisMsg->pu1Msg = NULL;
                        pLLInfo = NULL;
                        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                        CTP_EE ((ISIS_LGST,
                                 "CTL <X> : Exiting IsisProcessData ()\n"));
                        return;
                    }

                    i4count = TMO_SLL_Count (&pIf6->addr6Ilist);

                    if (i4count == 0)
                    {
                        ISIS_RESET_MT_ID (pCktRec->u1IfMTId,
                                          pLLInfo->u1AddrType);
                        pCktRec->u1ProtoSupp &= ~pLLInfo->u1AddrType;
                    }
#else
                    ISIS_RESET_MT_ID (pCktRec->u1IfMTId, pLLInfo->u1AddrType);
                    pCktRec->u1ProtoSupp &= ~pLLInfo->u1AddrType;
#endif
#endif

                }
                else
                {
                    CTP_PT ((ISIS_LGST,
                             "CTL <T> : Processing IPv4 Address Change Indication - DELETE\n"));
#ifdef BFD_WANTED
                    /* Deregister from BFD, before deleting the address from 
                     * circuit record */
                    u4SysInstIdx = pCktRec->pContext->u4SysInstIdx;
                    pTravAdj = pCktRec->pAdjEntry;
                    while (pTravAdj != NULL)
                    {
                        if (pTravAdj->u1Mt0BfdRegd == ISIS_BFD_TRUE)
                        {
                            IsisDeRegisterWithBfd (u4SysInstIdx, pCktRec,
                                                   pTravAdj,
                                                   ISIS_IPV4_UNICAST_MT_ID,
                                                   ISIS_TRUE);
#ifdef ISIS_FT_ENABLED
                            ISIS_FLTR_ADJ_LSU (pCktRec->pContext, ISIS_CMD_ADD,
                                               pTravAdj);
#endif
                        }
                        pTravAdj = pTravAdj->pNext;
                    }
#endif
                    MEMSET (pCktRec->au1IPV4Addr, 0, ISIS_MAX_IPV4_ADDR_LEN);
                }
            }
#ifdef ISIS_FT_ENABLED
            ISIS_FLTR_CKT_LSU (pCktRec->pContext, ISIS_CMD_ADD, pCktRec,
                               pCktRec->u1CktExistState)
#endif
                /* Free the pIsisMsg and its pu1Msg */
                ISIS_MEM_FREE (ISIS_BUF_EVTS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            pLLInfo = NULL;
            ISIS_MEM_FREE ((UINT1) ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        case ISIS_MSG_NO_RLEAK:
            CTP_PT ((ISIS_LGST,
                     "CTL <T> : Processing No Route Leak Indication\n"));
            u4SysInstIdx = pIsisMsg->u4CxtOrIfindex;
            if (IsisCtrlGetSysContext (u4SysInstIdx, &pContext) == ISIS_FAILURE)
            {
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                break;
            }
            if (pContext->SelfLSP.pL1NSNLSP != NULL)
            {
                pTravLsp = pContext->SelfLSP.pL1NSNLSP->pNonZeroLSP;

                while (pTravLsp != NULL)
                {
                    pTravTlv = (tIsisIPRATLV *) RBTreeGetFirst
                        (pTravLsp->IPRATLV);
                    while (pTravTlv != NULL)
                    {
                        pNextTlv = (tIsisIPRATLV *) RBTreeGetNext
                            (pTravLsp->IPRATLV, (tRBElem *) pTravTlv, NULL);

                        if (pTravTlv->u1LeakedRoute == ISIS_TRUE)
                        {
                            switch (pTravTlv->u1Code)
                            {
                                    /*update no. of tlv in the lsp structure */
                                case ISIS_IP_INTERNAL_RA_TLV:
                                case ISIS_IP_EXTERNAL_RA_TLV:
                                    pTravLsp->u1NumIPRATLV--;
                                    u1RemBytes =
                                        (UINT1) ISIS_GET_REM_BYTES (pTravLsp->
                                                                    u1NumIPRATLV,
                                                                    ISIS_IPRA_TLV_LEN);
                                    if (u1RemBytes < ISIS_IPRA_TLV_LEN)
                                    {
                                        pTravLsp->u2LSPLen =
                                            (UINT2) (pTravLsp->u2LSPLen - 2);
                                    }
                                    break;
                                case ISIS_IPV6_RA_TLV:
                                    pTravLsp->u1NumIPV6RATLV--;
                                    u1RemBytes =
                                        (UINT1) ISIS_GET_REM_BYTES (pTravLsp->
                                                                    u1NumIPV6RATLV,
                                                                    ISIS_IP6RA_TLV_LEN);
                                    if (u1RemBytes < ISIS_IP6RA_TLV_LEN)
                                    {
                                        pTravLsp->u2LSPLen =
                                            (UINT2) (pTravLsp->u2LSPLen - 2);
                                    }
                                    break;
                                case ISIS_EXT_IP_REACH_TLV:
                                    pTravLsp->u1NumExtIpReachTLV--;
                                    u1RemBytes =
                                        (UINT1) ISIS_GET_REM_BYTES (pTravLsp->
                                                                    u1NumExtIpReachTLV,
                                                                    ISIS_EXT_IP_REACH_TLV_LEN);
                                    if (u1RemBytes < ISIS_EXT_IP_REACH_TLV_LEN)
                                    {
                                        pTravLsp->u2LSPLen =
                                            (UINT2) (pTravLsp->u2LSPLen - 2);
                                    }
                                    break;
                                case ISIS_MT_IPV6_REACH_TLV:
                                    pTravLsp->u1NumMTIpv6ReachTLV--;
                                    u1RemBytes =
                                        (UINT1) ISIS_GET_REM_BYTES (pTravLsp->
                                                                    u1NumMTIpv6ReachTLV,
                                                                    ISIS_MT_IPV6_REACH_TLV_LEN);
                                    if (u1RemBytes < ISIS_MT_IPV6_REACH_TLV_LEN)
                                    {
                                        pTravLsp->u2LSPLen =
                                            (UINT2) (pTravLsp->u2LSPLen - 2);
                                    }
                                    break;

                                default:
                                    break;
                            }
                            pTravLsp->u2LSPLen =
                                (UINT2) (pTravLsp->u2LSPLen - pTravTlv->u1Len);
                            RBTreeRemove (pTravLsp->IPRATLV,
                                          (tRBElem *) pTravTlv);
                            ISIS_MEM_FREE (ISIS_BUF_TLVS, (UINT1 *) pTravTlv);
                            pTravTlv = NULL;
                            pTravLsp->u1DirtyFlag = ISIS_MODIFIED;
                        }
                        pTravTlv = pNextTlv;
                    }
                    pTravLsp = pTravLsp->pNext;
                }
            }

            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

#ifdef MBSM_WANTED
        case ISIS_SYS_MSG_DATA:
            IsisMbsmUpdateLCStatus ((tCRU_BUF_CHAIN_HEADER *) pIsisMsg->pu1Msg,
                                    CRU_BUF_Get_U2Reserved ((tCRU_BUF_CHAIN_HEADER *) pIsisMsg->pu1Msg));
            ISIS_MEM_FREE (ISIS_BUF_CHNS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

            break;
#endif

#ifdef BFD_WANTED
        case ISIS_MSG_BFD_SESSION_STATE_NOTIFICATION:
        {
            pIpAddr = (tIsisIpAddr *) (VOID *) pIsisMsg->pu1Msg;

            i4RetValue =
                IsisCtrlGetSysContext (pIsisMsg->u4CxtOrIfindex, &pContext);
            if (i4RetValue == ISIS_FAILURE)
            {
                /* System Instance does not exist */
                ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                pIpAddr = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                return;
            }

            /* Fetching the Circuit Record for given IfIndex */
            i4RetValue = IsisUtlGetCktRecWithIfIndex (pContext,
                                                      pIsisMsg->u4IfSubIdx,
                                                      &pCktEntry);
            if (i4RetValue == ISIS_FAILURE)
            {
                /* Circuit Record does not exist */
                ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                pIpAddr = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                return;
            }

            /* Fetching the Adjacency record for given Destination IP */
            i4RetValue = IsisUtlGetAdjRecWithIpAddr (pIpAddr, pCktEntry,
                                                     &pL1AdjEntry,
                                                     &pL2AdjEntry);
            if ((i4RetValue == ISIS_FAILURE)
                || ((pL1AdjEntry == NULL) && (pL2AdjEntry == NULL)))
            {
                /* Adjacent IP Addr & Dest IP Addr does not match */
                ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
                pIsisMsg->pu1Msg = NULL;
                pIpAddr = NULL;
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                return;
            }

            /* MT ID for which notification has been received */
            u1MtId = (pIpAddr->u1AddrType == ISIS_ADDR_IPV4) ?
                ISIS_IPV4_UNICAST_MT_ID : ISIS_IPV6_UNICAST_MT_ID;

            while ((pL1AdjEntry != NULL) || (pL2AdjEntry != NULL))
            {
                if (pL1AdjEntry != NULL)
                {
                    pAdjEntry = pL1AdjEntry;
                    pL1AdjEntry = NULL;
                }
                else
                {
                    pAdjEntry = pL2AdjEntry;
                    pL2AdjEntry = NULL;
                }

                /* Process BFD UP and DOWN notifications */
                if ((pIsisMsg->u1LenOrStat == BFD_SESS_STATE_UP) &&
                    (pContext->u1IsisMTSupport == ISIS_TRUE) &&
                    (pAdjEntry->u1IsisBfdRequired == ISIS_BFD_TRUE))
                {
                    /* BFD session state UP notification has to be 
                     * processed only if BFD is enabled for
                     * multi-topology ISIS neighbors */
                    IsisUtlProcessAdjUpNotification (pContext, pAdjEntry,
                                                     pCktEntry, u1MtId);
                }
                else if ((pIsisMsg->u1LenOrStat == BFD_SESS_STATE_DOWN) &&
                         (((pContext->u1IsisMTSupport == ISIS_TRUE) &&
                           (pAdjEntry->u1IsisBfdRequired == ISIS_BFD_TRUE)) ||
                          (pContext->u1IsisMTSupport == ISIS_FALSE)))
                {
                    /* BFD session state DOWN notification has to be
                     * processed for both single topology & multi topology
                     * ISIS neighbors */
                    IsisUtlProcessAdjDownNotification (pContext, pAdjEntry,
                                                       pCktEntry, u1MtId);
                }
            }
        }
            /* Free the pIsisMsg and its pu1Msg */
            ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            pIpAddr = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;
#endif
        case ISIS_EVT_HOST_NME_CHANGE:
            IsisHostNmeHdlr (pIsisMsg);
            /* Free the pIsisMsg and its pu1Msg */
            ISIS_MEM_FREE (ISIS_BUF_CHNS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;
        case ISIS_EVT_HOST_NME_SUPPORT:
            IsisHostNmeSupportChgEvt (pIsisMsg);
            /* Free the pIsisMsg and its pu1Msg */
            ISIS_MEM_FREE (ISIS_BUF_CHNS, pIsisMsg->pu1Msg);
            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            break;

        default:

            /* Should never happen. If it happens, we do notknow how to free
             * the buffer held in pIsisMsg->pu1Msg, which may lead to memory
             * leaks. If the control reaches this point, make sure that this is
             * fixed before proceeding further
             */

            WARNING ((ISIS_LGST, "CTL <E> : Unknown Message Type [ %u ]\n",
                      pIsisMsg->u1MsgType));

            pIsisMsg->pu1Msg = NULL;
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessData ()\n"));
}

/*******************************************************************************
 * Function      : IsisTaskMain ()
 * Description   : This routine waits for events from other modules and
 *                 schedules the ISIS modules based on the type of Events
 * Input(s)      : None
 * Output(s)     : None
 * Globals       : Not Referred or Modified
 * Returns       : VOID
 ******************************************************************************/

VOID
IsisTaskMain (INT1 *pi1TaskParam)
{
    tIsisMsg           *pIsisMsg = NULL;
    tIsisCktEntry      *pCktRec = NULL;
    UINT4               u4Events = 0;
    UINT4               u4FTSelfNodeID = 0;
    UINT4               u4FTPeerNodeID = 0;
    UINT2               u2ModuleID = 0;
    INT4                i4RetValue = ISIS_FAILURE;
    /*  INT1                i1LoopStatus = ISIS_SUCCESS; *//*diab */

    UINT2               u2LLIfIndex = 0;
    UNUSED_PARAM (pi1TaskParam);
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisTaskMain ()\n"));

    /* The following routine does the protocol initialization */
    if (IsisInit () == ISIS_FAILURE)
    {
        ISIS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixTskIdSelf (&gIsisTaskId) == OSIX_FAILURE)
    {
        ISIS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    ISIS_INIT_COMPLETE (OSIX_SUCCESS);

    if (IsisRegWithVcm () == ISIS_FAILURE)
    {
        IsisMemDeInit ();
        SYS_LOG_DEREGISTER (gi4IsisSysLogId);
        return;
    }
#ifdef ISIS_FT_ENABLED
    if (IsisFltiRegWithRM () == ISIS_FAILURE)
    {
        IsisDeRegFromVcm ();
        IsisMemDeInit ();
        SYS_LOG_DEREGISTER (gi4IsisSysLogId);
        return;
    }
#endif

#ifdef SNMP_2_WANTED
    /* Register with SNMP */
    RegisterSTDISI ();
    RegisterFSISIS ();
#endif

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_ISIS, IsisSendRouteMapUpdateMsg);
#endif /* ROUTEMAP_WANTED */
#ifdef ISS_WANTED
    /* Register with iss for hostname update. */
    IsisRegWithIss ();
#endif
    u2ModuleID = 8;
    u4FTSelfNodeID = 1;
    u4FTPeerNodeID = 2;

    ISIS_EXT_SET_MODULEID (u2ModuleID);
    ISIS_EXT_SET_FT_PEER_NODEID (u4FTPeerNodeID);
    ISIS_EXT_SET_FT_SELF_NODEID (u4FTSelfNodeID);

#ifndef ISIS_FT_ENABLED
    UNUSED_PARAM (u2ModuleID);
    UNUSED_PARAM (u4FTSelfNodeID);
    UNUSED_PARAM (u4FTPeerNodeID);
#endif

    /* Indicate ISS module regarding module start */
    IssSetModuleSystemControl (ISIS_MODULE_ID, MODULE_START);

    /* The following loop is an infinite loop and will never be exited unless
     * ISIS module is forced to shutdown in which case ISIS can be restarted by
     * invoking IsisCtrlInit () function again
     */

    /*Restore GR File Content */
    if (FlashFileExists (ISIS_GR_CONF) == ISS_SUCCESS)
    {
        gIsisGrinfo =
            (tIsisGrInfo *) MemAllocMemBlk (gIsisMemPoolId.u4GrInfoPid);
        /* Global Structure get populated */
        IssRestoreGraceContent (gIsisGrinfo, ISIS_MODULE);
    }
    for (;;)
    {
        OsixEvtRecv (gIsisTaskId, (ISIS_SYS_FT_DATA_EVT |
                                   ISIS_SYS_RT_DATA_EVT | ISIS_SYS_INT_EVT |
                                   ISIS_SYS_LL_DATA_EVT | ISIS_SYS_TMR_EVT |
                                   ISIS_SYS_RT6_DATA_EVT |
                                   ISIS_SYS_ROUTE_DATA_EVT |
                                   ISIS_SYS_BFD_STATE_EVT), (OSIX_WAIT),
                     (UINT4 *) &u4Events);
        CTP_PT ((ISIS_LGST, "CTL <T> : Processing Events [ %s ]\n",
                 ISIS_GET_SYS_EVT_NAME (u4Events)));
        IsisLock ();

        if (gu1IsisStatus == ISIS_SHUTDOWN)
        {
            u4Events = 0;
            IsisUnlock ();
            continue;
        }

        if (u4Events & ISIS_SYS_TMR_EVT)
        {
            IsisTmrProcTimeOut ();

            /* All timers are processed, reset the event mask */
            u4Events &= ~ISIS_SYS_TMR_EVT;
        }

        while (u4Events)
        {
            pIsisMsg = IsisRecvMsg (u4Events);

            if (pIsisMsg != NULL)
            {
                if ((pIsisMsg->u1MsgType == ISIS_MSG_LL_DATA)
                    || (pIsisMsg->u1MsgType == ISIS_MSG_IF_STAT_IND)
                    || (pIsisMsg->u1MsgType == ISIS_MSG_SEC_IF_STAT_IND))
                {
                    u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
                    i4RetValue =
                        IsisAdjGetCktRecWithLLHandle (u2LLIfIndex, &pCktRec);
                }
                else if (pIsisMsg->u1MsgType == ISIS_MSG_IF_ADDR_CHG)
                {
                    i4RetValue =
                        IsisAdjGetCktRecWithIfIdx (pIsisMsg->u4CxtOrIfindex, 0,
                                                   &pCktRec);
                }

                if ((pIsisMsg->u1MsgType == ISIS_MSG_LL_DATA)
                    && (i4RetValue == ISIS_FAILURE))
                {
                    /* Free the memories and continue as we are unable
                     * to retrieve the ckt corresponding
                     * to the LL If Index Ckt Rec is not applicable/used
                     * for IF_STAT_IND and
                     * will not be NULL for IF_ADDR_CHG
                     */
                    IsisFreeDLLBuf (pIsisMsg);
                    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                    continue;
                }

                IsisProcessData (pIsisMsg, pCktRec);
            }
            else
            {
                /* No more data in the queues. Reset the Event Mask and go back
                 * to Wait for Events
                 */

                u4Events = 0;
            }
        }

        UNUSED_PARAM (i4RetValue);
        IsisUnlock ();
    }

    /*klocwork */
    /*CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisTaskMain ()\n")); */
}

/*******************************************************************************
 * Function       : IsisProcLLData ()
 * Description    : This routine processes the data received from lower layers 
 *                  and transfers the control to respective modules for further
 *                  processing.
 * Input(s)       : pMsg - Pointer to received Message
 * Output(s)      : None
 * Globals        : Not Referred or Modified
 * Returns        : VOID
 ******************************************************************************/

PUBLIC VOID
IsisProcLLData (tIsisMsg * pMsg)
{
    INT4                i4RetVal = ISIS_FAILURE;
    INT1               *pi1InterfaceName = NULL;
    UINT1               au1PktHdr[ISIS_MAX_HDR_LEN];
    INT1                ai1InterfaceName[CFA_CLI_MAX_IF_NAME_LEN];
    tIsisCktEntry      *pCktRec = NULL;
    tIsisLLData        *pLLData = NULL;
    UINT2               u2LLIfIndex = 0;
    tIsisComHdr         IsisComHdr;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisProcLLData ()\n"));
    pi1InterfaceName = &ai1InterfaceName[0];
    MEMSET (au1PktHdr, 0, ISIS_MAX_HDR_LEN);
    /* Circuit Record is held in the pMsg->u4Context, Retrieve it
     */

    u2LLIfIndex = (UINT2) pMsg->u4CxtOrIfindex;
    if (IsisAdjGetCktRecWithLLHandle (u2LLIfIndex, &pCktRec) == ISIS_FAILURE)
    {
        return;
    }

    /* Get the common header out, which includes the PDU type. De-multiplex 
     * based on the PDU type to hand over control to either Adjacency or 
     * Update Module
     *
     * Note: IsisCopyFromDLLBuf () is a portable routine whihc takes care of
     * copying out data from DLL buffers which may be any type - chained or
     * linear. This routine hides the details of the DLL buffer.
     */

    IsisCopyFromDLLBuf (pMsg, au1PktHdr, ISIS_MAX_HDR_LEN);

    MEMCPY (&IsisComHdr, au1PktHdr, sizeof (tIsisComHdr));
    CTP_PI ((ISIS_LGST, "CTL <I> : Processing [ %s ] Rcvd On Ckt [ %u ] \n",
             ISIS_GET_PDU_TYPE_STR (IsisComHdr.u1PDUType), pCktRec->u4CktIdx));
    /* Update u1MsgType field in pMsg to reflect the correct PDU type 
     * which is used by the Update and Adjacency  modules for further 
     * procesing. 
     *
     * Note: The PDU types currently differ from the Message Types
     * (defined in isdefs.h). Hence pMsg->u1MsgType can be updated
     * without causing any problems for Update or Adjacency modules for
     * De-multiplexing based on this field. If PDU types clash with
     * Message types then Message types should be updated to avoid any
     * clash.
     *
     * Currently Defined Message Types:
     *
     * ISIS_MSG_LL_DATA               0xF1 
     * ISIS_MSG_EVENT                 0xF2
     * ISIS_MSG_HL_DATA               0xF3
     * ISIS_MSG_IF_STAT_IND           0xF5
     * ISIS_MSG_FTM_DATA              0xF6
     * ISIS_MSG_ROUTE_INFO            0xF7
     * ISIS_MSG_RTM_DATA              0xF8
     * ISIS_MSG_ROUTEMAP_UPDATE       0xF9
     */
    i4RetVal = IsisIsPduValidOnCkt (au1PktHdr, pCktRec, pMsg->u4Length);

    if (i4RetVal != ISIS_SUCCESS)
    {
        ISIS_INCR_SYS_DROPPED_PDUS (pCktRec->pContext);
        IsisFreeDLLBuf (pMsg);
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pMsg);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcLLData ()\n"));
        return;
    }

    if (INT_LOG == ISIS_LOG_ENABLE)
    {
        CfaCliGetIfName ((UINT4) u2LLIfIndex, pi1InterfaceName);
        if (gau1IsisModLevelMask[ISIS_INT_MODULE] & ISIS_PT_MASK)
        {

            PANIC ((ISIS_LGST, "Received %s On Interface:%s\n",
                    ISIS_GET_PDU_TYPE_STR_FOR_TRACE (IsisComHdr.u1PDUType),
                    pi1InterfaceName));
            IsisDbgPrintCommHdr (pCktRec->pContext, &IsisComHdr);

        }
    }

    if (PKT_LOG == ISIS_LOG_ENABLE)
    {
        CfaCliGetIfName ((UINT4) u2LLIfIndex, pi1InterfaceName);
        if (gau1IsisModLevelMask[ISIS_PDMP_MODULE] & ISIS_PT_MASK)
        {
            PANIC ((ISIS_LGST, "Received %s pkt dump On Interface:%s\n",
                    ISIS_GET_PDU_TYPE_STR_FOR_TRACE (IsisComHdr.u1PDUType),
                    pi1InterfaceName));
            IsisDbgPrintHEXDMP (pMsg->u4Length, pMsg->pu1Msg);
        }
    }
    switch (IsisComHdr.u1PDUType)
    {
        case ISIS_L1LSP_PDU:
        case ISIS_L2LSP_PDU:
        case ISIS_L1PSNP_PDU:
        case ISIS_L2PSNP_PDU:
        case ISIS_L1CSNP_PDU:
        case ISIS_L2CSNP_PDU:

            ISIS_INCR_IN_CTRL_PDU_STAT (pCktRec);
            pMsg->u1MsgType = IsisComHdr.u1PDUType;

            if (gu1RelqFlag == ISIS_DECN_WAIT)
            {

                pLLData = (tIsisLLData *) ISIS_MEM_ALLOC (ISIS_BUF_MISC,
                                                          sizeof (tIsisLLData));
                if (pLLData == NULL)
                {
                    return;
                }
                pLLData->pu1Msg = (UINT1 *) pMsg;

                if (pCktRec->pContext->pLLDataNode == NULL)
                {
                    pCktRec->pContext->pLLDataNode = pLLData;
                }
                else
                {
                    pLLData->pNext = pCktRec->pContext->pLLDataNode;
                    pCktRec->pContext->pLLDataNode = pLLData;

                }
                return;

            }

            IsisUpdProcCtrlPkt (pMsg);
            break;

        case ISIS_L1LAN_IIH_PDU:
        case ISIS_L2LAN_IIH_PDU:
        case ISIS_P2P_IIH_PDU:
        case ISIS_ISH_PDU:

            pMsg->u1MsgType = IsisComHdr.u1PDUType;
            IsisAdjProcCtrlPkt (pMsg);
            break;

        default:

            CTP_PT ((ISIS_LGST, "CTL <T> : Invalid PDU Received \n"));

            /* Discard the PDU
             */

            IsisFreeDLLBuf (pMsg);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pMsg);
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcLLData ()\n"));
}

/******************************************************************************
 * Function       : IsisRecvMsg ()
 * Description    : This function dequeues messages from one of the queues and 
 *                  resets the event flag corresponding to that queue when all 
 *                  the messages from the queue have been dequeued. It formats
 *                  the dequeued message to tIsisMsg format as required by the
 *                  Control Module
 * Input(s)       : u4Events - Events
 * Globals        : Not referred or modified
 * Returns        : ISIS_SUCCESS - On success 
 *                  ISIS_FAILURE - Otherwise
 *****************************************************************************/

PRIVATE tIsisMsg   *
IsisRecvMsg (UINT4 u4Events)
{
    tIsisMsg           *pIsisMsg = NULL;

    tCRU_BUF_CHAIN_HEADER *pQMsg;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisRecvMsg ()\n"));

    pIsisMsg = (tIsisMsg *) ISIS_MEM_ALLOC (ISIS_BUF_MSGS, sizeof (tIsisMsg));
    if (pIsisMsg == NULL)
    {
        return NULL;
    }

    /* The Data Receiving Logic: This routine receives data from 3 different
     * modules viz. Lower Layer, RTM and FT. Lower Layers enqueue data in
     * the tIsisMsg format. Hence no extra formatting is required for this case.
     * RTM, Timer and FT are external modules which may enqueue data as chained
     * or linear buffers. Hence this routine is expected to format these 
     * messages in tIsisMsg format before returning to the Control Module.
     * Apart from external modules ISIS internal modules post Events to the 
     * Control Module for Logging and debugging purposes
     */

    if (u4Events & ISIS_SYS_LL_DATA_EVT)
    {
        /* Dequeue a message at a time
         */

        if (OsixQueRecv (gIsisCtrlQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            CRU_BUF_Copy_FromBufChain (pQMsg, (UINT1 *) pIsisMsg, 0,
                                       sizeof (tIsisMsg));

            CRU_BUF_Release_MsgBufChain (pQMsg, TRUE);

        }
        else
        {
            /* No more messages in this queue. Reset the Events flag since this
             * event is no more valid
             */
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            u4Events &= ~ISIS_SYS_LL_DATA_EVT;
            return NULL;
        }
    }

    else if (u4Events & ISIS_SYS_ROUTE_DATA_EVT)
    {
        /* Dequeue a message at a time
         */
        if (OsixQueRecv (gIsisRtmQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            CRU_BUF_Copy_FromBufChain (pQMsg, (UINT1 *) pIsisMsg, 0,
                                       sizeof (tIsisMsg));

            CRU_BUF_Release_MsgBufChain (pQMsg, TRUE);

        }
        else
        {
            /* No more messages in this queue. Reset the Events flag since this
             * event is no more valid
             */
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            u4Events &= (UINT4) (~ISIS_SYS_ROUTE_DATA_EVT);
            return NULL;
        }
    }

    else if (u4Events & ISIS_SYS_INT_EVT)
    {
        /* Dequeue a message at a time
         */
        if (OsixQueRecv (gIsisCtrlQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            CRU_BUF_Copy_FromBufChain (pQMsg, (UINT1 *) pIsisMsg, 0,
                                       sizeof (tIsisMsg));

            CRU_BUF_Release_MsgBufChain (pQMsg, TRUE);

        }
        else
        {
            /* No more messages in this queue. Reset the Events flag since this
             * event is no more valid
             */

            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            u4Events &= ~ISIS_SYS_INT_EVT;
            return NULL;
        }
    }
    else if ((u4Events & ISIS_SYS_RT_DATA_EVT) && (gu1IsisStatus == ISIS_IS_UP))
    {
        if (OsixQueRecv (gIsisRtmQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            MEMSET (&gIsisRtmHdr, 0, sizeof (tRtmMsgHdr));

            CRU_BUF_Copy_FromBufChain (pQMsg, (UINT1 *) &gIsisRtmHdr, 0,
                                       sizeof (tRtmMsgHdr));
            CRU_BUF_Release_MsgBufChain (pQMsg, TRUE);
        }
        else
        {
            /* No more messages in this queue. Reset the Events flag since this
             * event is no more valid
             */
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            u4Events &= ~ISIS_SYS_RT_DATA_EVT;
            return NULL;
        }

        /* Formatting the message as required by the Control Module 
         * since RTM will not send data as required by ISIS
         */

        pIsisMsg->u4CxtOrIfindex = 0;
        /* Not Applicable for RTM
         * Data
         */
        pIsisMsg->pu1Msg = ((UINT1 *) (&gIsisRtmHdr));

        /* Pointer to the RTM Data Buffer
         * in the tQMsg buffer format
         */
        pIsisMsg->pMemFreeRoutine = NULL;
        /* Not Applicable for RTM
         * Data
         */
        pIsisMsg->u1LenOrStat = 0;
        /* Not Applicable for RTM
         * Data
         */
        pIsisMsg->u1MsgType = ISIS_MSG_RTM_DATA;
        /* Specifies RTM Responses
         */
        pIsisMsg->u1DataType = 0;
        /* Not Applicable for RTM
         * Data
         */
    }
    else if ((u4Events & ISIS_SYS_RT6_DATA_EVT)
             && (gu1IsisStatus == ISIS_IS_UP))
    {
        if (OsixQueRecv (gIsisRtmQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            MEMSET (&gIsisRtm6Hdr, 0, sizeof (tRtm6MsgHdr));

            CRU_BUF_Copy_FromBufChain (pQMsg, (UINT1 *) &gIsisRtm6Hdr, 0,
                                       sizeof (tRtm6MsgHdr));
            CRU_BUF_Release_MsgBufChain (pQMsg, TRUE);
        }
        else
        {
            /* No more messages in this queue. Reset the Events flag since this
             * event is no more valid
             */
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            u4Events &= ~ISIS_SYS_RT6_DATA_EVT;
            return NULL;
        }

        /* Formatting the message as required by the Control Module 
         * since RTM will not send data as required by ISIS
         */

        pIsisMsg->u4CxtOrIfindex = 0;
        /* Not Applicable for RTM
         * Data
         */
        pIsisMsg->pu1Msg = ((UINT1 *) (&gIsisRtm6Hdr));

        /* Pointer to the RTM Data Buffer
         * in the tQMsg buffer format
         */
        pIsisMsg->pMemFreeRoutine = NULL;
        /* Not Applicable for RTM
         * Data
         */
        pIsisMsg->u1LenOrStat = 0;
        /* Not Applicable for RTM
         * Data
         */
        pIsisMsg->u1MsgType = ISIS_MSG_RTM6_DATA;
        /* Specifies RTM Responses
         */
        pIsisMsg->u1DataType = 0;
        /* Not Applicable for RTM
         * Data
         */
    }

    else if (u4Events & ISIS_SYS_BFD_STATE_EVT)
    {
        /* Dequeue a message at a time
         */
        if (OsixQueRecv (gIsisCtrlQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            CRU_BUF_Copy_FromBufChain (pQMsg, (UINT1 *) pIsisMsg, 0,
                                       sizeof (tIsisMsg));

            CRU_BUF_Release_MsgBufChain (pQMsg, TRUE);

        }
        else
        {
            /* No more messages in this queue. Reset the Events flag since this
             * event is no more valid
             */

            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            u4Events &= ~ISIS_SYS_BFD_STATE_EVT;
            return NULL;
        }
    }

#ifdef ISIS_FT_ENABLED

    else if ((u4Events & ISIS_SYS_FT_DATA_EVT) && (gu1IsisStatus == ISIS_IS_UP))
    {
        if (OsixQueRecv (gIsisFltQId, (UINT1 *) &pQMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            CRU_BUF_Copy_FromBufChain (pQMsg, (UINT1 *) pIsisMsg, 0,
                                       sizeof (tIsisMsg));

            CRU_BUF_Release_MsgBufChain (pQMsg, TRUE);

        }
        else
        {
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            return NULL;
        }

        /* Formatting the message as required by the Control Module 
         * since FTM or PNI will not send data as required by ISIS
         */

        pIsisMsg->u4CxtOrIfindex = 0;
        /* Not Applicable for FT
         * Data
         */
        pIsisMsg->pMemFreeRoutine = NULL;
        /* Not Applicable for FT
         * Data
         */
        pIsisMsg->u1LenOrStat = 0;
        /* Not Applicable for FT
         * Data
         */
        pIsisMsg->u1MsgType = ISIS_MSG_FTM_DATA;
        /* Specifies FTM data
         */
        pIsisMsg->u1DataType = 0;
        /* Not Applicable for FT
         * Data
         */
    }
#endif
    else
    {
        /* No more messages in this queue. Reset the Events flag since this
         * event is no more valid
         */

        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
        u4Events &= ~ISIS_SYS_FT_DATA_EVT;
        return NULL;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisRecvMsg ()\n"));

    return pIsisMsg;
}

/******************************************************************************
 * 
 * Function       : IsisInitLspTxPool ()
 * Description    : This routine allocates the u2NumElems of LspTxEntries  
 * Input(s)       : u2NumElems - The number of Elements required in Pool
 * Output(s)      : None 
 * Globals        : gLspTxPool 
 * Returns        : ISIS_SUCCESS
 *                  ISIS_FAILURE
 *
 *****************************************************************************/

PRIVATE INT4
IsisInitLspTxPool (UINT4 u4NumElems)
{
    tIsisLSPTxEntry    *pList = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisInitLspTxPool ()\n"));

    gIsisLspTxPool.u2NumEntries = 0;
    gIsisLspTxPool.pLSPTxRec = NULL;

    if (u4NumElems == 0)
    {
        return ISIS_FAILURE;
    }

    while (u4NumElems > 0)
    {
        pList = (tIsisLSPTxEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LSPTX,
                                                    sizeof (tIsisLSPTxEntry));

        /* if malloc fails free the memory pool
         */

        if (pList == NULL)
        {
            IsisFreeLspTxPool ();
            return (ISIS_FAILURE);
        }

        if (gIsisLspTxPool.pLSPTxRec == NULL)
        {
            gIsisLspTxPool.pLSPTxRec = pList;
        }
        else
        {
            /* Temporaily using the pLSPRec field for linking all the Tx pool
             * records. This field will actually hold the LSP database entry 
             * when it is present in the Transmission queue
             */

            pList->pLSPRec = (tIsisLSPEntry *) gIsisLspTxPool.pLSPTxRec;
            gIsisLspTxPool.pLSPTxRec = pList;
        }

        gIsisLspTxPool.u2NumEntries++;
        u4NumElems--;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exited IsisInitLspTxPool ()\n"));

    return (ISIS_SUCCESS);
}

/******************************************************************************
 * Function       : IsisFreeLspTxPool ()
 * Description    : This routine frees the all LSPTxEntries i.e allocated in
 *                  IsisInitLspTxPool() 
 * Input(s)       : None 
 *              
 * Output(s)      : None 
 *                  None 
 * Globals        : gLspTxpool 
 * Returns        : None 
 *****************************************************************************/

PRIVATE INT4
IsisFreeLspTxPool (VOID)
{
    tIsisLSPTxEntry    *pTravLSPBuf = NULL;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisFreeLspTxPool ()\n"));

    while (gIsisLspTxPool.u2NumEntries > 0)
    {
        pTravLSPBuf = gIsisLspTxPool.pLSPTxRec;
        gIsisLspTxPool.pLSPTxRec = (tIsisLSPTxEntry *) pTravLSPBuf->pLSPRec;
        pTravLSPBuf->pLSPRec = NULL;
        ISIS_MEM_FREE (ISIS_BUF_LSPTX, (UINT1 *) pTravLSPBuf);
        gIsisLspTxPool.u2NumEntries--;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exited IsisFreeLspTxPool ()\n"));

    return (ISIS_SUCCESS);

}

/******************************************************************************
 * Function       : IsisAllocLSPTxBlock (VOID)
 * Description    : This routine allocates the IsisLSPTxEntry buffer from the 
 *                  global LSPTxPool
 * Input(s)       : None 
 * Output(s)      : None 
 * Globals        : gLspTxPool 
 * Returns        : pAllocBuff
 *****************************************************************************/

PUBLIC tIsisLSPTxEntry *
IsisAllocLSPTxBlock (VOID)
{
    tIsisLSPTxEntry    *pAllocBuff;

    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisAllocLSPTxBlock ()\n"));

    /* Check there are no entries exist in the LSPTX Pool  then malloc one and
     * give to application
     */

    if (gIsisLspTxPool.u2NumEntries == 0)
    {
        pAllocBuff = (tIsisLSPTxEntry *) ISIS_MEM_ALLOC (ISIS_BUF_LSPTX,
                                                         sizeof
                                                         (tIsisLSPTxEntry));
    }

    /* If there are Entries exist then give one and update u2Numentries
     */
    else
    {
        pAllocBuff = gIsisLspTxPool.pLSPTxRec;
        gIsisLspTxPool.pLSPTxRec = (tIsisLSPTxEntry *) pAllocBuff->pLSPRec;
        pAllocBuff->pLSPRec = NULL;
        gIsisLspTxPool.u2NumEntries--;
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exited IsisAllocLSPTxBlock ()\n"));

    return (pAllocBuff);
}

/******************************************************************************
 * Function       : IsisDeAllocLSPTxBlock (pLspTxBuff)
 * Description    : This routine moves the IsisLSPTxEntry buffer to the Memory
 *                  pool 
 * Input(s)       : pLspTxBuff  - Pointer to the LSP Transmission Buffer
 * Output(s)      : None 
 * Globals        : gLspTxPool 
 * Returns        : None 
 *****************************************************************************/

PUBLIC VOID
IsisDeAllocLSPTxBlock (tIsisLSPTxEntry * pLspTxBuff)
{
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisDeAllocLSPTxBlock ()\n"));

    /* If the no of entries in the Pool equals to the Max entries possble then
     * free the record
     */

    if (gIsisLspTxPool.u2NumEntries == gIsisMemActuals.u4MaxLspTxQLen)
    {
        ISIS_MEM_FREE (ISIS_BUF_LSPTX, (UINT1 *) pLspTxBuff);
    }

    /* else link this record to the pool
     */

    else
    {
        if (gIsisLspTxPool.pLSPTxRec == NULL)
        {
            gIsisLspTxPool.pLSPTxRec = pLspTxBuff;
            pLspTxBuff->pLSPRec = NULL;
        }
        else
        {
            pLspTxBuff->pLSPRec = (tIsisLSPEntry *) gIsisLspTxPool.pLSPTxRec;
            gIsisLspTxPool.pLSPTxRec = pLspTxBuff;
            gIsisLspTxPool.u2NumEntries++;
        }
    }

    CTP_EE ((ISIS_LGST, "CTL <X> : Exited IsisDeAllocLSPTxBlock ()\n"));
}

/******************************************************************************
 * Function       : IsisIsPduValidOnCkt ()
 * Description    : This function Validates the PDU depending on the Ckt
 *                  that it is received.
 * Input(s)       : u1PduType - Type of the PDU received on the Ckt 
 *                  pCktRec   - Pointer to the Circuit Record
 * Output(s)      : None
 * Globals        : None 
 * Returns        : ISIS_SUCCESS - If the PDU is Valid one
 *                  ISIS_FAILURE - If the PDU is not a valid one
 *****************************************************************************/

PRIVATE INT4
IsisIsPduValidOnCkt (UINT1 *pu1Pdu, tIsisCktEntry * pCktRec, UINT4 u4Len)
{
    INT4                i4RetVal = ISIS_FAILURE;
    UINT2               u2PktLen = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));

    ISIS_FORM_IPV4_ADDR (pCktRec->au1IPV4Addr, au1IPv4Addr,
                         ISIS_MAX_IPV4_ADDR_LEN);

    switch (((tIsisComHdr *) pu1Pdu)->u1PDUType)
    {
        case ISIS_L1LAN_IIH_PDU:

            ISIS_EXTRACT_HELLO_PDU_LEN (pu1Pdu, u2PktLen);
            if ((pCktRec->u1CktType == ISIS_P2P_CKT)
                || (u2PktLen > u4Len) || (pCktRec->u1CktLevel == ISIS_LEVEL2))
            {
                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <E> : Packet Validation Failed - Circuit Index [ %u ], Received [ L1 LAN IIH ],"
                             " Configured [ P2P ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             pCktRec->u4CktIdx, au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));
                }

                if (u2PktLen > u4Len)
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <E> : L1 LAN IIH - Packet Length Mismatch - Circuit Index [ %u ],"
                             " Self [ %u ], Peer [ %u ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             pCktRec->u4CktIdx, u4Len, u2PktLen, au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));
                }

                if (pCktRec->u1CktLevel == ISIS_LEVEL2)
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <E> : L1 LAN IIH - Packet Level Mismatch - Circuit Index [ %u ],"
                             " Self [ L%u ], Peer [ L%s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, "1",
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));
                }
                return i4RetVal;
            }
            else
            {
                i4RetVal = ISIS_SUCCESS;
            }
            break;

        case ISIS_L1CSNP_PDU:
        case ISIS_L1PSNP_PDU:
        case ISIS_L1LSP_PDU:

            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1Pdu, u2PktLen);
            if ((pCktRec->u1CktLevel != ISIS_LEVEL2) && (u2PktLen <= u4Len))
            {
                i4RetVal = ISIS_SUCCESS;
            }
            break;

        case ISIS_L2LAN_IIH_PDU:

            ISIS_EXTRACT_HELLO_PDU_LEN (pu1Pdu, u2PktLen);
            if ((pCktRec->u1CktType == ISIS_P2P_CKT)
                || (u2PktLen > u4Len) || (pCktRec->u1CktLevel == ISIS_LEVEL1))
            {
                if (pCktRec->u1CktType == ISIS_P2P_CKT)
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <E> : Packet Validation Failed - Circuit Index [ %u ],"
                             " Received [ L2 LAN IIH ], Configured [ P2P ], IPv4 Address [ %s ],"
                             " IPv6 Address [ %s ]\n", pCktRec->u4CktIdx,
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));
                }

                if (u2PktLen > u4Len)
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <E> : L2 LAN IIH - Packet Length Mismatch - Circuit Index [ %u ],"
                             " Self [ %u ], Peer [ %u ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             pCktRec->u4CktIdx, u4Len, u2PktLen, au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));
                }

                if (pCktRec->u1CktLevel == ISIS_LEVEL1)
                {
                    ADP_PT ((ISIS_LGST,
                             "ADJ <E> : L2 LAN IIH - Packet Level Mismatch - Circuit Index [ %u ],"
                             " Self [ LEVEL_%u ], Peer [ LEVEL_%s ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                             pCktRec->u4CktIdx, pCktRec->u1CktLevel, "2",
                             au1IPv4Addr,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pCktRec->au1IPV6Addr))));
                }

                return i4RetVal;
            }
            else
            {
                i4RetVal = ISIS_SUCCESS;
            }
            break;

        case ISIS_L2CSNP_PDU:
        case ISIS_L2PSNP_PDU:
        case ISIS_L2LSP_PDU:

            ISIS_EXTRACT_PDU_LEN_FROM_LSP (pu1Pdu, u2PktLen);
            if ((pCktRec->u1CktLevel != ISIS_LEVEL1) && (u2PktLen <= u4Len))
            {
                i4RetVal = ISIS_SUCCESS;
            }
            break;

        case ISIS_ISH_PDU:

            u2PktLen = *(pu1Pdu + 1);
            if ((pCktRec->u1CktType == ISIS_P2P_CKT) && (u2PktLen <= u4Len))
            {
                i4RetVal = ISIS_SUCCESS;
            }
            if (pCktRec->u1CktType == ISIS_BC_CKT)
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : Packet Validation Failed - Circuit Index [ %u ], Received [ ISH ],"
                         " Configured [ BC ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         pCktRec->u4CktIdx, au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));
            }

            if (u2PktLen > u4Len)
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : ISH - Packet Length Mismatch - Circuit Index [ %u ],"
                         " Self [ %u ], Peer [ %u ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         pCktRec->u4CktIdx, u4Len, u2PktLen, au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));
            }
            break;

        case ISIS_P2P_IIH_PDU:

            ISIS_EXTRACT_HELLO_PDU_LEN (pu1Pdu, u2PktLen);
            if ((pCktRec->u1CktType == ISIS_P2P_CKT) && (u2PktLen <= u4Len))
            {
                i4RetVal = ISIS_SUCCESS;
            }
            if (pCktRec->u1CktType == ISIS_BC_CKT)
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : Packet Validation Failed - Circuit Index [ %u ], Received [ P2P IIH ],"
                         " Configured [ BC ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         pCktRec->u4CktIdx, au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));
            }

            if (u2PktLen > u4Len)
            {
                ADP_PT ((ISIS_LGST,
                         "ADJ <E> : P2P IIH - Packet Length Mismatch - Circuit Index [ %u ],"
                         " Self [ %u ], Peer [ %u ], IPv4 Address [ %s ], IPv6 Address [ %s ]\n",
                         pCktRec->u4CktIdx, u4Len, u2PktLen, au1IPv4Addr,
                         Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                       &(pCktRec->au1IPV6Addr))));
            }
            break;

        default:
            break;
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------------+
 *  Function           : IsisLock
 *
 *  Input(s)           : None.
 *
 *  Output(s)          : None.
 *
 *  Returns            : None.
 *
 *  Action             : Takes a Protocol Sempaphore
 * +-------------------------------------------------------------------*/

INT4
IsisLock (VOID)
{
    if (OsixSemTake (gIsisSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* -------------------------------------------------------------------+
 *  Function           : IsisUnlock
 *
 *  Input(s)           : None.
 *
 *  Output(s)          : None.
 *
 *  Returns            : None.
 *
 *  Action             : Releases a Protocol Sempaphore
 * +-------------------------------------------------------------------*/

INT4
IsisUnlock (VOID)
{
    if (OsixSemGive (gIsisSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/******************************************************************************
 * Function       : IsisProcessIfStatInd ()
 * Description    : This function adds IPRA and IPIF entries when the Ckt
 *                  is up and deletes the entries when the circuit goes down.
 * Input(s)       : pIsisMsg  - Pointer to the Message
 * Output(s)      : None
 * Globals        : None 
 * Returns        : VOID
 *****************************************************************************/

PRIVATE VOID
IsisProcessIfStatInd (tIsisMsg * pIsisMsg)
{
    UINT4               u4Port = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4TempAddr = 0;
    UINT4               u4NetMask = 0;

    UINT4               u4Offset = 0;
    INT4                i4RetVal = ISIS_FAILURE;
    UINT4               u4Metric = 0;
    tIp6Addr            Ip6Addr;
    tIsisMetric         Metric;
    UINT1               u1Status = ISIS_DOWN;
    UINT1               u1PrefixLen = ISIS_DOWN;
    UINT1               au1IpV6Addr[ISIS_MAX_IPV6_ADDR_LEN];
    UINT1               au1IpV6IPRAAddr[ISIS_MAX_IPV6_ADDR_LEN];
    tIsisLLInfo        *pIfInfo = NULL;
    tIsisIPIfAddr      *pIPIfRec = NULL;
    tIsisIPIfAddr      *pSecIPIfRec = NULL;
    tIsisSysContext    *pContext = NULL;
    tIsisCktEntry      *pCktEntry = NULL;
    tIsisEvtIPIFChange *pIPIfEvt = NULL;
    UINT2               u2LLIfIndex = 0;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    UINT1              *pu1Ipv6Addr = NULL;
    UINT1               u1Idx = 0;
    UINT1               au1IPv4Addr[ISIS_MAX_IP_ADDR_LEN];
    CTP_EE ((ISIS_LGST, "CTL <X> : Entered IsisProcessIfStatInd ()\n"));

    MEMSET (au1IPv4Addr, 0, sizeof (au1IPv4Addr));
    MEMSET (au1IpV6Addr, 0, ISIS_MAX_IPV6_ADDR_LEN);
    MEMSET (au1IpV6IPRAAddr, 0, sizeof (au1IpV6IPRAAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (Metric, 0, sizeof (Metric));

    if ((pIsisMsg->u1MsgType != ISIS_MSG_IF_STAT_IND) &&
        (pIsisMsg->u1MsgType != ISIS_MSG_SEC_IF_STAT_IND))
    {
        /* Do not worry, Ignore the message
         */

        CTP_PT ((ISIS_LGST, "CTL <E> : Unknown Status Indication Message\n"));
        if (pIsisMsg->pu1Msg != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
        }
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
        return;
    }

    /* Interface Status Indication. Retrieve the Circuit and process the
     * indication with respect to that circuit
     */
    u2LLIfIndex = (UINT2) pIsisMsg->u4CxtOrIfindex;
    if (IsisAdjGetCktRecWithIfIdx ((UINT4) u2LLIfIndex, 0, &pCktEntry) ==
        ISIS_FAILURE)
    {
        if (pIsisMsg->pu1Msg != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, pIsisMsg->pu1Msg);
        }
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

        return;
    }

    pContext = pCktEntry->pContext;
    u1Status = pIsisMsg->u1LenOrStat;
    pIfInfo = (tIsisLLInfo *) (VOID *) pIsisMsg->pu1Msg;
    pIsisMsg->pu1Msg = NULL;

    if ((pIfInfo == NULL) || (pIfInfo->u1AddrType == 0))
    {
        if (pIfInfo != NULL)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
        }

        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
        return;
    }
    if ((pCktEntry->u1CktLevel != ISIS_LEVEL2)
        && (pCktEntry->pL1CktInfo != NULL))
    {
        u4Metric = pCktEntry->pL1CktInfo->u4FullMetric;
        MEMCPY (Metric, pCktEntry->pL1CktInfo->Metric, sizeof (tIsisMetric));
    }
    else
    {
        if (pCktEntry->pL2CktInfo != NULL)
        {
            u4Metric = pCktEntry->pL2CktInfo->u4FullMetric;
            MEMCPY (Metric, pCktEntry->pL2CktInfo->Metric,
                    sizeof (tIsisMetric));
        }
    }
    if ((pIfInfo->u1AddrType == ISIS_ADDR_IPV4)
        || (pIfInfo->u1AddrType == ISIS_ADDR_IPVX))
    {
        if (NetIpv4GetPortFromIfIndex (u2LLIfIndex, &u4Port) == NETIPV4_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            return;
        }
        if (NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) == NETIPV4_FAILURE)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            return;
        }
        pIfInfo->u1EncapType = NetIpv4IfInfo.u1EncapType;

        if ((u1Status != ISIS_SEC_IP_REMOVED)
            && (u1Status != ISIS_SEC_IP_ADDED))
        {
            NetIpv4IfInfo.u4Addr = OSIX_HTONL (NetIpv4IfInfo.u4Addr);
            MEMCPY (pIfInfo->au1IpV4Addr, &NetIpv4IfInfo.u4Addr,
                    sizeof (UINT4));
            NetIpv4IfInfo.u4NetMask = OSIX_HTONL (NetIpv4IfInfo.u4NetMask);
            MEMCPY (pIfInfo->au1IpMask, &NetIpv4IfInfo.u4NetMask,
                    sizeof (UINT4));
        }

    }
    if ((pIfInfo->u1AddrType == ISIS_ADDR_IPV6)
        || (pIfInfo->u1AddrType == ISIS_ADDR_IPVX))
    {
        if (NETIPV6_SUCCESS == NetIpv6GetIfInfo (u2LLIfIndex, &NetIpv6IfInfo))
        {
            MEMCPY (pIfInfo->au1IpV6Addr,
                    (UINT1 *) &(NetIpv6IfInfo.Ip6Addr.u1_addr),
                    ISIS_MAX_IPV6_ADDR_LEN);
        }
    }

    if (u1Status == ISIS_SEC_IP_ADDED)
    {
        if (pIfInfo->u1AddrType == ISIS_ADDR_IPV4)
        {
            IsisUtlComputePrefixLen (pIfInfo->au1IpMask, ISIS_MAX_IPV4_ADDR_LEN,
                                     &u1PrefixLen);
            i4RetVal = IsisCtrlGetIPIf (pContext, pCktEntry->u4CktIfIdx,
                                        pCktEntry->u4CktIfSubIdx,
                                        pIfInfo->au1IpV4Addr, ISIS_ADDR_IPV4,
                                        &pIPIfRec);
            /* IPIF Record does not exist, hence create a record and
             * then update the information
             */
            if (i4RetVal != ISIS_SUCCESS)
            {
                pSecIPIfRec = (tIsisIPIfAddr *)
                    ISIS_MEM_ALLOC (ISIS_BUF_IPIF, sizeof (tIsisIPIfAddr));
                if (pSecIPIfRec == NULL)
                {
                    PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IPIF Entry\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                    IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
                    ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
                    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                    CTP_EE ((ISIS_LGST,
                             "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
                    return;
                }
                MEMSET (pSecIPIfRec, 0, sizeof (tIsisIPIfAddr));
                MEMCPY (pSecIPIfRec->au1IPAddr, pIfInfo->au1IpV4Addr,
                        ISIS_MAX_IPV4_ADDR_LEN);
                pSecIPIfRec->u4CktIfIdx = pCktEntry->u4CktIfIdx;
                pSecIPIfRec->u1AddrType = ISIS_ADDR_IPV4;
                pSecIPIfRec->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
                pSecIPIfRec->u1ExistState = ISIS_ACTIVE;
                pSecIPIfRec->u1SecondaryFlag = ISIS_TRUE;
                IsisCtrlAddIPIf (pContext, pSecIPIfRec);
                ISIS_FORM_IPV4_ADDR (pIfInfo->au1IpV4Addr, au1IPv4Addr,
                                     ISIS_MAX_IPV4_ADDR_LEN);
                CTP_PT ((ISIS_LGST,
                         "CTL <E> : Adding Secondary IP IF Entry - IF Index [%u],IPv4 [%s]\n",
                         pCktEntry->u4CktIfIdx, au1IPv4Addr));
                pIPIfEvt =
                    (tIsisEvtIPIFChange *) ISIS_MEM_ALLOC (ISIS_BUF_EVTS,
                                                           sizeof
                                                           (tIsisEvtIPIFChange));

                if (pIPIfEvt != NULL)
                {
                    pIPIfEvt->u1EvtID = ISIS_EVT_IP_IF_ADDR_CHANGE;
                    pIPIfEvt->u1Status = ISIS_IP_IF_ADDR_ADD;
                    pIPIfEvt->u4CktIfIdx = pCktEntry->u4CktIfIdx;
                    pIPIfEvt->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
                    pIPIfEvt->u1AddrType = ISIS_ADDR_IPV4;
                    MEMCPY (pIPIfEvt->au1IPAddr, pSecIPIfRec->au1IPAddr,
                            ISIS_MAX_IPV4_ADDR_LEN);

                    IsisUtlSendEvent (pContext, (UINT1 *) pIPIfEvt,
                                      sizeof (tIsisEvtIPIFChange));
                }
                else
                {
                    PANIC ((ISIS_LGST,
                            ISIS_MEM_ALLOC_FAIL
                            " : IP Interface Change Event\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                }

                IsisCtrlAddSecAutoIPRA (pContext, pCktEntry->u4CktIfIdx,
                                        pCktEntry->u4CktIfSubIdx,
                                        pIfInfo->au1IpV4Addr, u1PrefixLen,
                                        Metric, u4Metric, ISIS_ADDR_IPV4);
            }
        }
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
        return;
    }

    if (u1Status == ISIS_SEC_IP_REMOVED)
    {
        pIPIfRec = pContext->IPIfTable.pIPIfAddr;

        while (pIPIfRec != NULL)
        {
            if ((pIPIfRec->u4CktIfIdx == pCktEntry->u4CktIfIdx)
                && (pIPIfRec->u4CktIfSubIdx == pCktEntry->u4CktIfSubIdx)
                && (pIPIfRec->u1AddrType == pIfInfo->u1AddrType)
                && (pIPIfRec->u1SecondaryFlag == 1)
                &&
                (MEMCMP
                 (pIPIfRec->au1IPAddr, pIfInfo->au1IpV4Addr,
                  ISIS_MAX_IPV4_ADDR_LEN) == 0))
            {
                pIPIfEvt = (tIsisEvtIPIFChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPIFChange));

                if (pIPIfEvt != NULL)
                {
                    pIPIfEvt->u1EvtID = ISIS_EVT_IP_IF_ADDR_CHANGE;
                    pIPIfEvt->u1Status = ISIS_IP_IF_ADDR_DEL;
                    pIPIfEvt->u4CktIfIdx = pCktEntry->u4CktIfIdx;
                    pIPIfEvt->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
                    pIPIfEvt->u1AddrType = pIPIfRec->u1AddrType;
                    if (pIPIfRec->u1AddrType == ISIS_ADDR_IPV4)
                    {
                        MEMCPY (pIPIfEvt->au1IPAddr, pIPIfRec->au1IPAddr,
                                ISIS_MAX_IPV4_ADDR_LEN);
                    }
                    IsisUtlSendEvent (pContext, (UINT1 *) pIPIfEvt,
                                      sizeof (tIsisEvtIPIFChange));
                }
                else
                {
                    CTP_PT ((ISIS_LGST, "CTL <E> : No Memory for "
                             "Posting IP Interface change event\n"));
                }
                ISIS_FORM_IPV4_ADDR (pIPIfRec->au1IPAddr, au1IPv4Addr,
                                     ISIS_MAX_IPV4_ADDR_LEN);
                CTP_PT ((ISIS_LGST,
                         "CTL <E> : Removing Secondary IP IF Entry - IF Index [%u],IPv4 [%s]\n",
                         pCktEntry->u4CktIfIdx, au1IPv4Addr));
                IsisCtrlDelIPIf (pContext, pCktEntry->u4CktIfIdx,
                                 pCktEntry->u4CktIfSubIdx, pIPIfRec->au1IPAddr,
                                 ISIS_ADDR_IPV4);
                break;
            }
            pIPIfRec = pIPIfRec->pNext;
        }

        for (u1Idx = 0; u1Idx < ISIS_MAX_IPV4_ADDR_LEN; u1Idx++)
        {
            pIfInfo->au1IpV4Addr[u1Idx] &= pIfInfo->au1IpMask[u1Idx];
        }

        IsisCtrlDelAutoSecIPRA (pContext, pCktEntry->u4CktIfIdx,
                                pCktEntry->u4CktIfSubIdx, pIfInfo->u1AddrType,
                                pIfInfo->au1IpV4Addr);
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
        return;

    }

    if (u1Status == ISIS_STATE_OFF)
    {
        CTP_PT ((ISIS_LGST,
                 "CTL <E> : Processing ISIS_STATE_OFF for circuit [%d]\n",
                 pCktEntry->u4CktIfIdx));
        pIPIfRec = pContext->IPIfTable.pIPIfAddr;

        while (pIPIfRec != NULL)
        {
            if ((pIPIfRec->u4CktIfIdx == pCktEntry->u4CktIfIdx)
                && (pIPIfRec->u4CktIfSubIdx == pCktEntry->u4CktIfSubIdx)
                && (pIPIfRec->u1AddrType == pIfInfo->u1AddrType))
            {
                pIPIfEvt = (tIsisEvtIPIFChange *)
                    ISIS_MEM_ALLOC (ISIS_BUF_EVTS, sizeof (tIsisEvtIPIFChange));

                if (pIPIfEvt != NULL)
                {
                    pIPIfEvt->u1EvtID = ISIS_EVT_IP_IF_ADDR_CHANGE;
                    pIPIfEvt->u1Status = ISIS_IP_IF_ADDR_DEL;
                    pIPIfEvt->u4CktIfIdx = pCktEntry->u4CktIfIdx;
                    pIPIfEvt->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
                    pIPIfEvt->u1AddrType = pIPIfRec->u1AddrType;
                    if (pIPIfRec->u1AddrType == ISIS_ADDR_IPV4)
                    {
                        MEMCPY (pIPIfEvt->au1IPAddr, pIPIfRec->au1IPAddr,
                                ISIS_MAX_IPV4_ADDR_LEN);
                    }
                    else
                    {
                        MEMCPY (pIPIfEvt->au1IPAddr, pIPIfRec->au1IPAddr,
                                ISIS_MAX_IPV6_ADDR_LEN);
                    }
                    IsisUtlSendEvent (pContext, (UINT1 *) pIPIfEvt,
                                      sizeof (tIsisEvtIPIFChange));
                }
                else
                {
                    PANIC ((ISIS_LGST,
                            ISIS_MEM_ALLOC_FAIL
                            " : IP Interface Change Event\n"));
                    DETAIL_FAIL (ISIS_CR_MODULE);
                }
                if (pIPIfRec->u1AddrType == ISIS_ADDR_IPV4)
                {
                    ISIS_FORM_IPV4_ADDR (pIPIfRec->au1IPAddr, au1IPv4Addr,
                                         ISIS_MAX_IPV4_ADDR_LEN);
                    CTP_PT ((ISIS_LGST,
                             "CTL <E> : Removing IP IF Entry - IF Index [%u], IPv4 [%s]\n",
                             pCktEntry->u4CktIfIdx, au1IPv4Addr));
                    IsisCtrlDelIPIf (pContext, pCktEntry->u4CktIfIdx,
                                     pCktEntry->u4CktIfSubIdx,
                                     pIPIfRec->au1IPAddr, ISIS_ADDR_IPV4);
                }
                else
                {
                    ISIS_FORM_IPV4_ADDR (pIPIfRec->au1IPAddr, au1IPv4Addr,
                                         ISIS_MAX_IPV4_ADDR_LEN);
                    CTP_PT ((ISIS_LGST,
                             "CTL <E> : Removing IPv6 IF Entry - IF Index [%u], IPv6 [%s]\n",
                             pCktEntry->u4CktIfIdx,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                           &(pIPIfRec->au1IPAddr))));
                    IsisCtrlDelIPIf (pContext, pCktEntry->u4CktIfIdx,
                                     pCktEntry->u4CktIfSubIdx,
                                     pIPIfRec->au1IPAddr, ISIS_ADDR_IPV6);
                    break;
                }
            }
            pIPIfRec = pIPIfRec->pNext;
        }
        IsisCtrlDelAutoIPRA (pContext, pCktEntry->u4CktIfIdx,
                             pCktEntry->u4CktIfSubIdx, pIfInfo->u1AddrType);
        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

        CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
        return;
    }
    if ((pCktEntry->u1CktExistState != ISIS_ACTIVE)
        ||
        ((MEMCMP (pIfInfo->au1IpV4Addr, au1IPv4Addr, ISIS_MAX_IPV4_ADDR_LEN) ==
          0)
         && (MEMCMP (pIfInfo->au1IpV6Addr, au1IpV6Addr, ISIS_MAX_IPV6_ADDR_LEN)
             == 0)))
    {
        /* Circuit is still not active or no IP address configured
         * hence do not generate any events
         */

        ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
        ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

        CTP_EE ((ISIS_LGST, "CTP <X> : Exiting IsisProcessIfStatInd ()\n"));
        return;
    }

    CTP_PT ((ISIS_LGST, "CTL <E> : Processing ISIS_STATE_ON for Circuit [%d]\n",
             pCktEntry->u4CktIfIdx));
    if (pIfInfo->u1AddrType == ISIS_ADDR_IPV4)
    {
        IsisUtlComputePrefixLen (pIfInfo->au1IpMask, ISIS_MAX_IPV4_ADDR_LEN,
                                 &u1PrefixLen);
        i4RetVal = IsisCtrlGetIPIf (pContext, pCktEntry->u4CktIfIdx,
                                    pCktEntry->u4CktIfSubIdx,
                                    pIfInfo->au1IpV4Addr, ISIS_ADDR_IPV4,
                                    &pIPIfRec);
        /* IPIF Record does not exist, hence create a record and
         * then update the information
         */
        if (i4RetVal != ISIS_SUCCESS)
        {

            pIPIfRec = (tIsisIPIfAddr *)
                ISIS_MEM_ALLOC (ISIS_BUF_IPIF, sizeof (tIsisIPIfAddr));
            if (pIPIfRec == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IP IF Entry\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                CTP_EE ((ISIS_LGST,
                         "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
                return;
            }
            MEMCPY (pIPIfRec->au1IPAddr, pIfInfo->au1IpV4Addr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pIPIfRec->u4CktIfIdx = pCktEntry->u4CktIfIdx;
            pIPIfRec->u1AddrType = ISIS_ADDR_IPV4;
            pIPIfRec->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
            pIPIfRec->u1ExistState = ISIS_ACTIVE;
            IsisCtrlAddIPIf (pContext, pIPIfRec);
        }

        MEMCPY (&u4IpAddr, pIfInfo->au1IpV4Addr, ISIS_MAX_IPV4_ADDR_LEN);
        while (NetIpv4GetNextSecondaryAddress
               ((UINT2) u4Port, u4IpAddr, &u4IpAddr,
                &u4NetMask) == NETIPV4_SUCCESS)
        {
            pSecIPIfRec = (tIsisIPIfAddr *)
                ISIS_MEM_ALLOC (ISIS_BUF_IPIF, sizeof (tIsisIPIfAddr));
            if (pSecIPIfRec == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IP IF Entry\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                CTP_EE ((ISIS_LGST,
                         "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
                return;
            }
            u4TempAddr = OSIX_HTONL (u4IpAddr);
            MEMSET (pSecIPIfRec, 0, sizeof (tIsisIPIfAddr));
            MEMCPY (pSecIPIfRec->au1IPAddr, &u4TempAddr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pSecIPIfRec->u4CktIfIdx = pCktEntry->u4CktIfIdx;
            pSecIPIfRec->u1AddrType = ISIS_ADDR_IPV4;
            pSecIPIfRec->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
            pSecIPIfRec->u1ExistState = ISIS_ACTIVE;
            pSecIPIfRec->u1SecondaryFlag = ISIS_TRUE;
            i4RetVal = IsisCtrlGetIPIf (pContext, pCktEntry->u4CktIfIdx,
                                        pCktEntry->u4CktIfSubIdx,
                                        pSecIPIfRec->au1IPAddr, ISIS_ADDR_IPV4,
                                        &pIPIfRec);
            /* IPIF Record does not exist, hence create a record and
             * then update the information
             */
            if (i4RetVal == ISIS_SUCCESS)
            {
                ISIS_MEM_FREE (ISIS_BUF_IPIF, (UINT1 *) pSecIPIfRec);
                CTP_EE ((ISIS_LGST,
                         "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
                continue;
            }
            ISIS_FORM_IPV4_ADDR (pSecIPIfRec->au1IPAddr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);
            CTP_PT ((ISIS_LGST,
                     "CTL <E> : Adding IP IF Entry (secondary) - IF Index [%u],IPv4 [%s]\n",
                     pCktEntry->u4CktIfIdx, au1IPv4Addr));
            IsisCtrlAddIPIf (pContext, pSecIPIfRec);

        }
        IsisCtrlAddAutoIPRA (pContext, pCktEntry->u4CktIfIdx,
                             pCktEntry->u4CktIfSubIdx, pIfInfo->au1IpV4Addr,
                             u1PrefixLen, Metric, u4Metric, ISIS_ADDR_IPV4);
    }
    else if (pIfInfo->u1AddrType == ISIS_ADDR_IPV6)
    {
        u1PrefixLen = pIfInfo->u1IpV6PrefixLen;
        i4RetVal = IsisCtrlGetIPIf (pContext, pCktEntry->u4CktIfIdx,
                                    pCktEntry->u4CktIfSubIdx,
                                    pIfInfo->au1IpV6Addr, ISIS_ADDR_IPV6,
                                    &pIPIfRec);
        if (i4RetVal == ISIS_SUCCESS)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
            return;
        }

        /* IPIF Record does not exist, hence create a record and
         * then update the information
         */
        pIPIfRec = (tIsisIPIfAddr *)
            ISIS_MEM_ALLOC (ISIS_BUF_IPIF, sizeof (tIsisIPIfAddr));
        if (pIPIfRec == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IP IF Entry\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
            return;
        }
        MEMCPY (pIPIfRec->au1IPAddr, pIfInfo->au1IpV6Addr,
                ISIS_MAX_IPV6_ADDR_LEN);
        pIPIfRec->u4CktIfIdx = pCktEntry->u4CktIfIdx;
        pIPIfRec->u1AddrType = ISIS_ADDR_IPV6;
        pIPIfRec->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
        pIPIfRec->u1ExistState = ISIS_ACTIVE;
        if (i4RetVal != ISIS_SUCCESS)
        {
            CTP_PT ((ISIS_LGST,
                     "CTL <E> : Adding IP IF Entry - IF Index [%u],IPv6 [%s]\n",
                     pCktEntry->u4CktIfIdx,
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pIPIfRec->au1IPAddr))));
            IsisCtrlAddIPIf (pContext, pIPIfRec);
        }
        pu1Ipv6Addr = pIfInfo->au1IpV6Addr;
        while (u4Offset != (ISIS_MAX_IPV6_ADDR_LEN * ISIS_MAX_IPV6_ADDRESSES))
        {
            if (*pu1Ipv6Addr != 0)
            {
                MEMSET (&Ip6Addr, 0, ISIS_MAX_IPV6_ADDR_LEN);
                MEMCPY (&Ip6Addr, pu1Ipv6Addr, ISIS_MAX_IPV6_ADDR_LEN);
                if (IS_ADDR_LLOCAL (Ip6Addr) == FALSE)
                {
                    CTP_PT ((ISIS_LGST,
                             "CTL <E> : Adding IPv6 Address - IF Index [%u],IPv6 [%s]\n",
                             pCktEntry->u4CktIfIdx,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *) pu1Ipv6Addr)));
                    IsisCtrlAddAutoIPRA (pContext, pCktEntry->u4CktIfIdx,
                                         pCktEntry->u4CktIfSubIdx, pu1Ipv6Addr,
                                         u1PrefixLen, Metric, u4Metric,
                                         ISIS_ADDR_IPV6);
                }
                pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
            }
            u4Offset += ISIS_MAX_IPV6_ADDR_LEN;
        }
    }
    else if (pIfInfo->u1AddrType == ISIS_ADDR_IPVX)
    {
        /*For v4 updation */
        IsisUtlComputePrefixLen (pIfInfo->au1IpMask, ISIS_MAX_IPV4_ADDR_LEN,
                                 &u1PrefixLen);
        i4RetVal = IsisCtrlGetIPIf (pContext, pCktEntry->u4CktIfIdx,
                                    pCktEntry->u4CktIfSubIdx,
                                    pIfInfo->au1IpV4Addr, ISIS_ADDR_IPV4,
                                    &pIPIfRec);
        if (i4RetVal == ISIS_SUCCESS)
        {
            IsisCtrlAddAutoIPRA (pContext, pCktEntry->u4CktIfIdx,
                                 pCktEntry->u4CktIfSubIdx, pIfInfo->au1IpV4Addr,
                                 u1PrefixLen, Metric, u4Metric, ISIS_ADDR_IPV4);
            pu1Ipv6Addr = pIfInfo->au1IpV6Addr;
            u1PrefixLen = pIfInfo->u1IpV6PrefixLen;
            while (u4Offset !=
                   (ISIS_MAX_IPV6_ADDR_LEN * ISIS_MAX_IPV6_ADDRESSES))
            {
                if (*pu1Ipv6Addr != 0)
                {
                    MEMSET (&Ip6Addr, 0, ISIS_MAX_IPV6_ADDR_LEN);
                    MEMCPY (&Ip6Addr, pu1Ipv6Addr, ISIS_MAX_IPV6_ADDR_LEN);
                    if (IS_ADDR_LLOCAL (Ip6Addr) == FALSE)
                    {
                        CTP_PT ((ISIS_LGST,
                                 "CTL <E> : Adding IPv6 Address - IF Index [%u],IPv6 [%s]\n",
                                 pCktEntry->u4CktIfIdx,
                                 Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                               pu1Ipv6Addr)));
                        IsisCtrlAddAutoIPRA (pContext, pCktEntry->u4CktIfIdx,
                                             pCktEntry->u4CktIfSubIdx,
                                             pu1Ipv6Addr, u1PrefixLen, Metric,
                                             u4Metric, ISIS_ADDR_IPV6);
                    }
                    pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
                }
                u4Offset += ISIS_MAX_IPV6_ADDR_LEN;
            }
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
            return;
        }

        /* IPIF Record does not exist, hence create a record and
         * then update the information
         */

        if (i4RetVal != ISIS_SUCCESS)
        {
            pIPIfRec = (tIsisIPIfAddr *)
                ISIS_MEM_ALLOC (ISIS_BUF_IPIF, sizeof (tIsisIPIfAddr));
            if (pIPIfRec == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IP IF Entry\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                CTP_EE ((ISIS_LGST,
                         "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
                return;
            }
            MEMCPY (pIPIfRec->au1IPAddr, pIfInfo->au1IpV4Addr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pIPIfRec->u4CktIfIdx = pCktEntry->u4CktIfIdx;
            pIPIfRec->u1AddrType = ISIS_ADDR_IPV4;
            pIPIfRec->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
            pIPIfRec->u1ExistState = ISIS_ACTIVE;
            ISIS_FORM_IPV4_ADDR (pIfInfo->au1IpV4Addr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);
            CTP_PT ((ISIS_LGST,
                     "CTL <E> : Adding IP IF Entry - IF Index [%u],IPv4 [%s]\n",
                     pCktEntry->u4CktIfIdx, au1IPv4Addr));
            IsisCtrlAddIPIf (pContext, pIPIfRec);
        }
        MEMCPY (&u4IpAddr, pIfInfo->au1IpV4Addr, ISIS_MAX_IPV4_ADDR_LEN);
        while (NetIpv4GetNextSecondaryAddress
               ((UINT2) u4Port, u4IpAddr, &u4IpAddr,
                &u4NetMask) == NETIPV4_SUCCESS)
        {
            pSecIPIfRec = (tIsisIPIfAddr *)
                ISIS_MEM_ALLOC (ISIS_BUF_IPIF, sizeof (tIsisIPIfAddr));
            if (pSecIPIfRec == NULL)
            {
                PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IPIF Entry\n"));
                DETAIL_FAIL (ISIS_CR_MODULE);
                IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
                ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
                ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
                CTP_EE ((ISIS_LGST,
                         "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
                return;
            }
            u4TempAddr = OSIX_HTONL (u4IpAddr);
            MEMCPY (pSecIPIfRec->au1IPAddr, &u4TempAddr,
                    ISIS_MAX_IPV4_ADDR_LEN);
            pSecIPIfRec->u4CktIfIdx = pCktEntry->u4CktIfIdx;
            pSecIPIfRec->u1AddrType = ISIS_ADDR_IPV4;
            pSecIPIfRec->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
            pSecIPIfRec->u1SecondaryFlag = ISIS_TRUE;
            pSecIPIfRec->u1ExistState = ISIS_ACTIVE;
            i4RetVal = IsisCtrlGetIPIf (pContext, pCktEntry->u4CktIfIdx,
                                        pCktEntry->u4CktIfSubIdx,
                                        pSecIPIfRec->au1IPAddr, ISIS_ADDR_IPV4,
                                        &pIPIfRec);
            /* IPIF Record does not exist, hence create a record and
             * then update the information
             */
            if (i4RetVal == ISIS_SUCCESS)
            {
                ISIS_MEM_FREE (ISIS_BUF_IPIF, (UINT1 *) pSecIPIfRec);
                CTP_EE ((ISIS_LGST,
                         "CTP <X> : Exiting IsisProcessIfStatInd ()\n"));
                continue;
            }
            ISIS_FORM_IPV4_ADDR (pSecIPIfRec->au1IPAddr, au1IPv4Addr,
                                 ISIS_MAX_IPV4_ADDR_LEN);
            CTP_PT ((ISIS_LGST,
                     "CTL <E> : Adding IP IF Entry (secondary) - IF Index [%u],IPv4 [%s]\n",
                     pCktEntry->u4CktIfIdx, au1IPv4Addr));
            IsisCtrlAddIPIf (pContext, pSecIPIfRec);

        }

        IsisCtrlAddAutoIPRA (pContext, pCktEntry->u4CktIfIdx,
                             pCktEntry->u4CktIfSubIdx, pIfInfo->au1IpV4Addr,
                             u1PrefixLen, Metric, u4Metric, ISIS_ADDR_IPV4);
        /*For v6 updation */
        u1PrefixLen = pIfInfo->u1IpV6PrefixLen;
        i4RetVal = IsisCtrlGetIPIf (pContext, pCktEntry->u4CktIfIdx,
                                    pCktEntry->u4CktIfSubIdx,
                                    pIfInfo->au1IpV6Addr, ISIS_ADDR_IPV6,
                                    &pIPIfRec);
        if (i4RetVal == ISIS_SUCCESS)
        {
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
            return;
        }

        /* IPIF Record does not exist, hence create a record and
         * then update the information
         */
        pIPIfRec = (tIsisIPIfAddr *)
            ISIS_MEM_ALLOC (ISIS_BUF_IPIF, sizeof (tIsisIPIfAddr));
        if (pIPIfRec == NULL)
        {
            PANIC ((ISIS_LGST, ISIS_MEM_ALLOC_FAIL " : IPIF Entry\n"));
            DETAIL_FAIL (ISIS_CR_MODULE);
            IsisUtlFormResFailEvt (pContext, ISIS_BUF_IPIF);
            ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
            ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);
            CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
            return;
        }
        MEMCPY (pIPIfRec->au1IPAddr, pIfInfo->au1IpV6Addr,
                ISIS_MAX_IPV6_ADDR_LEN);
        pIPIfRec->u4CktIfIdx = pCktEntry->u4CktIfIdx;
        pIPIfRec->u1AddrType = ISIS_ADDR_IPV6;
        pIPIfRec->u4CktIfSubIdx = pCktEntry->u4CktIfSubIdx;
        pIPIfRec->u1ExistState = ISIS_ACTIVE;
        if (i4RetVal != ISIS_SUCCESS)
        {
            CTP_PT ((ISIS_LGST,
                     "CTL <E> : Adding IP IF Entry - IF Index [%u],IPv6 [%s]\n",
                     pCktEntry->u4CktIfIdx,
                     Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                   &(pIPIfRec->au1IPAddr))));
            IsisCtrlAddIPIf (pContext, pIPIfRec);
        }
        pu1Ipv6Addr = pIfInfo->au1IpV6Addr;
        while (u4Offset != (ISIS_MAX_IPV6_ADDR_LEN * ISIS_MAX_IPV6_ADDRESSES))
        {
            if (*pu1Ipv6Addr != 0)
            {
                MEMSET (&Ip6Addr, 0, ISIS_MAX_IPV6_ADDR_LEN);
                MEMCPY (&Ip6Addr, pu1Ipv6Addr, ISIS_MAX_IPV6_ADDR_LEN);
                if (IS_ADDR_LLOCAL (Ip6Addr) == FALSE)
                {
                    CTP_PT ((ISIS_LGST,
                             "CTL <E> : Adding IPv6 Address - IF Index [%u],IPv6 [%s]\n",
                             pCktEntry->u4CktIfIdx,
                             Ip6PrintAddr ((tIp6Addr *) (VOID *) pu1Ipv6Addr)));
                    IsisCtrlAddAutoIPRA (pContext, pCktEntry->u4CktIfIdx,
                                         pCktEntry->u4CktIfSubIdx, pu1Ipv6Addr,
                                         u1PrefixLen, Metric, u4Metric,
                                         ISIS_ADDR_IPV6);
                }
                pu1Ipv6Addr += ISIS_MAX_IPV6_ADDR_LEN;
            }
            u4Offset += ISIS_MAX_IPV6_ADDR_LEN;
        }
    }
    ISIS_MEM_FREE (ISIS_BUF_MISC, (UINT1 *) pIfInfo);
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pIsisMsg);

    CTP_EE ((ISIS_LGST, "CTL <X> : Exiting IsisProcessIfStatInd ()\n"));
}

#ifdef ROUTEMAP_WANTED
/*****************************************************************************/
/* Function     : IsisSendRouteMapUpdateMsg                                  */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module, this function posts an event to ISIS     */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : ISIS_SUCCESS if Route Map update Msg Sent to ISIS          */
/*                successfully                                               */
/*                ISIS_FAILURE otherwise                                     */
/*****************************************************************************/
PUBLIC INT4
IsisSendRouteMapUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
    tIsisMsg            IsisMsg;
    tIsisQBuf          *pBuf;
    UINT4               u4Size;
    UINT1               au1NameBuf[RMAP_MAX_NAME_LEN + 4];
    tIsisSysContext    *pContext;

    u4Size = RMAP_MAX_NAME_LEN + sizeof (u4Status) + 4;    /* message size is
                                                         * RouteMap Name+Status */

    if (pu1RMapName == NULL)
    {
        return ISIS_FAILURE;
    }

    pContext = ISIS_GET_CONTEXT ();
    if (pContext == NULL)
    {
        return ISIS_FAILURE;
    }

    /* enshure there is no garbage behind map name */
    MEMSET (au1NameBuf, 0, sizeof (au1NameBuf));
    STRNCPY (au1NameBuf, pu1RMapName, STRLEN (pu1RMapName));
    pBuf = (tIsisQBuf *) ISIS_MEM_ALLOC (ISIS_BUF_CHNS, u4Size);
    if (pBuf == NULL)
    {
        return ISIS_FAILURE;
    }

    /*** copy status to offset 0 ***/
    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Status, 0,
                                   sizeof (u4Status)) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return ISIS_FAILURE;
    }
    /*** copy map name to offset 4 ***/
    if (CRU_BUF_Copy_OverBufChain (pBuf, au1NameBuf, sizeof (u4Status),
                                   RMAP_MAX_NAME_LEN + 4) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return ISIS_FAILURE;
    }
    IsisMsg.u4Length = u4Size;
    IsisMsg.pu1Msg = (UINT1 *) pBuf;
    IsisMsg.u1MsgType = ISIS_MSG_ROUTEMAP_UPDATE;

    if (IsisEnqueueMessage ((UINT1 *) &IsisMsg, sizeof (tIsisMsg),
                            gIsisCtrlQId, ISIS_SYS_INT_EVT) == ISIS_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return ISIS_FAILURE;
    }

    return ISIS_SUCCESS;
}

/*****************************************************************************/
/* Function     : IsisProcessRMapHandler                                     */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module.                                          */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
IsisProcessRMapHandler (tIsisMsg * pBuf)
{
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Status = 0;
    UINT1               u1Status = FILTERNIG_STAT_DEFAULT;
    tIsisSysContext    *pContext;

    pContext = ISIS_GET_CONTEXT ();

    if ((pContext == NULL) || (pBuf == NULL) || (pBuf->pu1Msg == NULL))
    {
        return;
    }

    /*** copy status to offset 0 ***/
    MEMCPY (&u4Status, pBuf->pu1Msg, sizeof (u4Status));

    /*** copy map name to offset 4 ***/
    MEMSET (au1RMapName, 0, (RMAP_MAX_NAME_LEN + 4));
    MEMCPY (au1RMapName, pBuf->pu1Msg + sizeof (u4Status),
            RMAP_MAX_NAME_LEN + 4);

    if (u4Status != 0)
    {
        u1Status = FILTERNIG_STAT_ENABLE;
    }
    else
    {
        u1Status = FILTERNIG_STAT_DISABLE;
    }

    /* Scan through all comtexts, update status */
    while (pContext != NULL)
    {
        if (pContext->pDistributeInFilterRMap != NULL)
        {
            if (STRCMP
                (pContext->pDistributeInFilterRMap->au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pContext->pDistributeInFilterRMap->u1Status = u1Status;
            }
        }

        if (pContext->pDistanceFilterRMap != NULL)
        {
            if (STRCMP
                (pContext->pDistanceFilterRMap->au1DistInOutFilterRMapName,
                 au1RMapName) == 0)
            {
                pContext->pDistanceFilterRMap->u1Status = u1Status;
            }
        }

        pContext = pContext->pNext;
    }

    ISIS_MEM_FREE (ISIS_BUF_CHNS, pBuf->pu1Msg);
    pBuf->pu1Msg = NULL;
    ISIS_MEM_FREE (ISIS_BUF_MSGS, (UINT1 *) pBuf);
    pBuf = NULL;
}
#endif /* ROUTEMAP_WANTED */
