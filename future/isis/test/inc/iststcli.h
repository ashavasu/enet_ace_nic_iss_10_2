
/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: iststcli.h,v 1.1.1.1 2015/12/29 11:57:48 siva Exp $
 **
 ** Description: ISIS UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"

INT4 cli_process_isis_test_cmd (tCliHandle CliHandle, ...);

INT4
IsisUt(INT4 );


