/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: isistest.h,v 1.1.1.1 2015/12/29 11:57:48 siva Exp $
 **
 ** Description: ISIS UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#ifndef _ISISTEST_H_
#define _ISISTEST_H_
#include "isincl.h"
#include "iscli.h"
#include "isclipt.h"
#include "isextn.h"

VOID IsisExecuteUtCase(UINT4 u4FileNumber, UINT4 u4TestNumber);
VOID IsisExecuteUtAll (VOID);
VOID IsisExecuteUtFile (UINT4 u4File);

typedef struct
{
    UINT1     u1File;
    UINT1     u1Case;
}tTestCase;

#define FILE_COUNT     1
tTestCase gIsisTestCase[] = {
    {1,25}, /* isiscli */
    {2,15}, /* file2 */
    {3,15}, /* file3 */
    {4,15}, /* file4 */
    {5,15}, /* file5 */
    {6,15}, /* file6 */
    {7,15}, /* file7 */
    {8,15}, /* file8 */
    {9,15}, /* file9 */
    {10,15} /* file10 */
};

INT4 IsisUt_1 (VOID);
INT4 IsisUt_2 (VOID);
INT4 IsisUt_3 (VOID);
INT4 IsisUt_4 (VOID);
INT4 IsisUt_5 (VOID);
INT4 IsisUt_6 (VOID);
INT4 IsisUt_7 (VOID);
INT4 IsisUt_8 (VOID);
INT4 IsisUt_9 (VOID);
INT4 IsisUt_10 (VOID);
INT4 IsisUt_11 (VOID);
INT4 IsisUt_12 (VOID);
INT4 IsisUt_13 (VOID);
INT4 IsisUt_14 (VOID);
INT4 IsisUt_15 (VOID);
INT4 IsisUt_16 (VOID);
INT4 IsisUt_17 (VOID);
INT4 IsisUt_18 (VOID);
INT4 IsisUt_19 (VOID);
INT4 IsisUt_20 (VOID);
INT4 IsisUt_21 (VOID);
INT4 IsisUt_22 (VOID);
INT4 IsisUt_23 (VOID);

#endif /* _ISISTEST_H_ */ 
