/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: isistest.c,v 1.1.1.1 2015/12/29 11:57:48 siva Exp $
 **
 ** Description: ISIS UT Test cases file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "isistest.h"

VOID
IsisExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber)
{
    INT4                i4RetVal = ISIS_FALSE;

    switch (u4FileNumber)
    {
        case 1:
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = IsisUt_1 ();
                    break;
                default:
                    printf ("Invalid Test for file isiscli.c \r\n");
                    return;
            }
            if (i4RetVal == ISIS_TRUE)
            {
                printf ("Unit Test Case: %d of isiscli.c has Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == ISIS_FALSE)
            {
                printf ("Unit Test Case: %d of isiscli.c has Failed \r\n",
                        u4TestNumber);
            }
            return;

        default:
            printf ("Invalid File for ISIS\r\n");
            return;
    }
}

VOID
IsisExecuteUtAll (VOID)
{
    UINT1               u1File = 0;

    for (u1File = 1; u1File <= FILE_COUNT; u1File++)
    {
        IsisExecuteUtFile (u1File);
    }
}

VOID
IsisExecuteUtFile (UINT4 u4File)
{
    UINT1               u1Case = 0;

    for (u1Case = 1; u1Case <= gIsisTestCase[u4File - 1].u1Case; u1Case++)
    {
        IsisExecuteUtCase (u4File, u1Case);
    }
}

INT4
IsisUt_1 (VOID)
{

    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("router isis");
    CliExecuteAppCmd ("net 10:10:00:00:00:00:00:00");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("ip router isis");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();

    tIsisCktEntry    *pCktEntry = NULL;
    INT4             i4RetVal = 0;

    i4RetVal = IsisAdjGetCktRecWithIfIdx (33, 0, &pCktEntry);
    if (i4RetVal != ISIS_SUCCESS)
    {
        return ISIS_FALSE;
    }
    
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int vlan 1");
    CliExecuteAppCmd ("no ip router isis");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no router isis");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
    return ISIS_TRUE;
}

