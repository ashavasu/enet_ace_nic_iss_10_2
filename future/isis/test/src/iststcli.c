/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: iststcli.c,v 1.1.1.1 2015/12/29 11:57:48 siva Exp $
 **
 ** Description: ISIS UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"
#include "isincl.h"
#include "iscli.h"
#include "iststcli.h"

#define MAX_ARGS 3

extern VOID         IsisExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber);
extern VOID         IsisExecuteUtAll (VOID);
extern VOID         IsisExecuteUtFile (UINT4 u4File);

INT4
cli_process_isis_test_cmd (tCliHandle CliHandle, ...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT4                i4Inst = 0;
    INT1                argno = 0;

    va_start (ap, CliHandle);
    i4Inst = va_arg (ap, INT4);

    /* Walk through the arguments and store in args array.
     */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    if (args[0] == NULL)
    {
        /* specified case on specified file */
        if (args[2] != NULL)
        {
            IsisExecuteUtCase (*(UINT4 *) (args[1]), *(UINT4 *) (args[2]));
        }
        /* all cases on specified file */
        else
        {
            IsisExecuteUtFile (*(UINT4 *) (args[1]));
        }
    }
    else
    {
        /* all cases in all the files */
        IsisExecuteUtAll ();
    }

    return CLI_SUCCESS;
}
