##########################################################################
# Copyright (C) Future Software Limited,2010			         #
#                                                                        #
# $Id: make.h,v 1.1.1.1 2015/12/29 11:57:48 siva Exp $
#								         #
# Description : Contains information fro creating the make file          #
#		for this isis module      				         #
#								         #
##########################################################################

#include the make.h and make.rule from LR
include ../../LR/make.h
include ../../LR/make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
ISIS_INCL_DIR           = $(BASE_DIR)/isis/inc

ISIS_TEST_BASE_DIR  = ${BASE_DIR}/isis/test
ISIS_TEST_SRC_DIR   = ${ISIS_TEST_BASE_DIR}/src
ISIS_TEST_INC_DIR   = ${ISIS_TEST_BASE_DIR}/inc
ISIS_TEST_OBJ_DIR   = ${ISIS_TEST_BASE_DIR}/obj

ISIS_TEST_INCLUDES  = -I$(ISIS_TEST_INC_DIR) \
                     -I$(ISIS_INCL_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h					 #
##########################################################################
