/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isgr.h,v 1.2 2010/08/03 09:24:26 prabuc Exp $
 *
 * Description: This file contains all the gr variable definitions
 *
 *****************************************************************************/

#ifndef __ISGR_H__
#define __ISGR_H__


/* GR Action routine numbers */
enum {
GR0 = 0, 
GR1,           
GR2,           
GR3,           
GR4,           
GR5,           
GR6,           
GR7,           
GR8,           
GR9,           
GR10,          
GR11,          
MAX_GR_FUNC   
};

#define MAX_GR_EVENT 10 
#define MAX_GR_STATE 4 

/* type of the functions which perform the action in Graceful restart */

typedef  void (*tGRFunc)(tGrSemInfo *);


/*Restarting Router*/

/* the graceful restart state machine transition table */
static UINT1  au1GrTable[MAX_GR_EVENT][MAX_GR_STATE] = {
/*_____________________________________________________________
        Restarting   ADJ_SEEN_RA    ADJ_SEEN_CSNP    SPF_WAIT  
_______________________________________________________________
Router
restarts */{   GR1,     GR0,        GR0,      GR0   },
/*______________________________________________________
RX RA
*/         {   GR2,     GR0,        GR3,      GR0   },
/*______________________________________________________
RX CSNP
set */     {   GR4,     GR5,        GR0,      GR0   },
/*______________________________________________________
RX IIH w/o Restart TLV */  
           {   GR0,     GR0,        GR0,      GR0   },
/*______________________________________________________
T1 Expires
*/         {   GR6,     GR6,        GR6,      GR0   },
/*______________________________________________________
T1 Expires nth time
*/         {   GR7,     GR7,        GR7,      GR0   },
/*______________________________________________________
T2 Expires
*/         {   GR8,     GR8,        GR8,      GR0   },
/*___________ ___________________________________________
T3 Expires
*/         {   GR9,     GR9,        GR9,      GR0   },
/*_______________________________________________________
LSP DB sync
*/         {   GR10,     GR0,        GR0,      GR0   },
/*__________ _____________________________________________
All SPF Done
*/         {   GR0,     GR0,        GR0,      GR11   }
/*_____________ __________________________________________
*/
};



/* array of pointers to functions performing actions during Graceful restart transitions */

tGRFunc aGrFunc[MAX_GR_FUNC] = {
               /* GR0 */         IsisGrInvalid,
               /* GR1 */         IsisGrRestart,
               /* GR2 */         IsisGrSeenRA,
               /* GR3 */         IsisGrAdjustT3,
               /* GR4 */         IsisGrSeenCsnp,
               /* GR5 */         IsisGrT1Cancel,
               /* GR6 */         IsisGrT1Expire,
               /* GR7 */         IsisGrT1ExpireNth,
               /* GR8 */         IsisGrT2Expire,
               /* GR9 */         IsisGrT3Expire,
               /* GR10 */        IsisGrLspDbSync,
               /* GR11 */        IsisGrRunning
               };

/******************************************************************************/

/*Starting router*/

#define  MAX_GR_START_FUNC   12 

#define MAX_GR_START_EVENT 9 
#define MAX_GR_START_STATE 3 


/* the graceful restart state machine transition table */
static UINT1  au1GrStartTable[MAX_GR_START_EVENT][MAX_GR_START_STATE] = {
/*_____________________________________________
        Starting   ADJ_SEEN_RA    ADJ_SEEN_CSNP     
____________________________________________
Router
Starts */{   GR1,     GR0,        GR0 },
/*__________________________________________
RX RA
*/         {   GR2,     GR0,        GR3 },
/*__________________________________________
RX CSNP
set */     {   GR4,     GR3,        GR0 },
/*___________________________________________
RX IIH w/o Restart TLV */  
           {   GR0,     GR0,        GR0 },
/*__________________________________________
T1 Expires
*/         {   GR6,     GR6,        GR6 },
/*__________________________________________
T1 Expires nth time
*/         {   GR7,     GR7,        GR7 },
/*__________________________________________
T2 Expires
*/         {   GR8,     GR8,        GR8 },
/*__________________________________________
ADJ UP
*/         {   GR5,     GR0,        GR0 },
/*__________________________________________
LSP DB sync
*/         {   GR9,     GR9,        GR9 }
};

/* "ADJ UP" Event should be a placed above "T1 Expires",
 * inorder to reduce code complexity(Planned and Unplanned 
 * state machine was tried to sink with each other)
 */

/* array of pointers to functions performing actions during Graceful restart transitions */

/*Starting router*/
tGRFunc aGrStartFunc[MAX_GR_START_FUNC] = {
               /* GR0 */         IsisGrInvalid,
               /* GR1 */         IsisGrRestart,
               /* GR2 */         IsisGrSeenRA,
               /* GR3 */         IsisGrT1Cancel,
               /* GR4 */         IsisGrSeenCsnp,
               /* GR5 */         IsisGrAdjUp,
               /* GR6 */         IsisGrT1Expire,
               /* GR7 */         IsisGrT1ExpireNth,
               /* GR8 */         IsisGrT2Expire,
               /* GR9 */         IsisGrLspDbSync
               };



#endif
