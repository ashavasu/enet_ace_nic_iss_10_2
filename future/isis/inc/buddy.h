/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: buddy.h,v 1.2 2009/06/03 06:39:01 premap-iss Exp $
 *
 * Description: This file contains all the type definitions, macros, constants
 * used by the buddy module
 *
 *****************************************************************************/

#ifndef __BUDDY_H__
#define __BUDDY_H__

/* Buddy Specific */


#define BUDDY_LS      gai1BuddyLogStr
#define BUDDY_LS_LEN  200
#define BUDDY_LOG_ENABLE 1
#define BUDDY_LOG_DISABLE 2

#define BUDDY_DEBUG_PRINT(x)\
        SPRINTF x;\
        UtlTrcPrint (BUDDY_LS);

#define SBP_WARNING(x)\
        BUDDY_DEBUG_PRINT(x)

#define SBP_PANIC(x)\
        BUDDY_DEBUG_PRINT(x)

#if (SBD_LOG == BUDDY_LOG_ENABLE)

#define SBP_PT(x) BUDDY_DEBUG_PRINT(x)
#define SBP_EE(x) BUDDY_DEBUG_PRINT(x) 

#else

#define SBP_PT(x)
#define SBP_EE(x)

#endif

#define BUDDY_ACTIVE             1
#define BUDDY_INACTIVE           0
#define BUDDY_EQUAL              0
#define BUDDY_NOT_EQUAL          1
#define BUDDY_TRUE               0
#define BUDDY_FALSE              1

#define BUDDY_SET                0
#define BUDDY_RESET             -1
#define BUDDY_PAT_MASK           0xC0
#define BUDDY_SET_PAT_END        0x40

#define BUDDY_INCR_BUF(i,buf,pos)  ((gBuddyTable[i].u1CFlag == BUDDY_CONT_BUF) ?\
        (buf + pos) : (buf + 4 + pos))

#define BUDDY_BIT_OFFSET(i,x,y)     \
        (2 *  (((UINT4)(y) - (UINT4)(x) - gBuddyTable[i].u2HdrSize) / \
                              gBuddyTable[i].u4MinBlkSize))
#define BUDDY_START_BYTE(x)\
        ((x)/8)

#define BUDDY_START_BIT(x)\
        ((x) % 8)

#define BUDDY_NUM_AC_BITS(x)\
        ((x) / BUDDY_MIN_BLK_SIZE) * 2 

#define BUDDY_COMPARE_BUFFS(i,x,y) \
        (((UINT4)(y) - (UINT4)(x)) <= gBuddyTable[i].u4MaxBlkSize) \
                 ? BUDDY_EQUAL : BUDDY_NOT_EQUAL

#define BUDDY_ALLOC_BUF MEM_CALLOC
#define BUDDY_FREE_BUF MEM_FREE


#define BUDDY_MIN(x,y) ((x) < (y) ? (x) : (y))



#define BUDDY_GET_MID_SET_MASK()  0xAA


#define BUDDY_GET_FREE_LEFT_BLKS(u1Pat) \
        (((u1Pat & 0x01) == 1) ? 0 : \
        (((u1Pat & 0x04) == 0x04) ? 1 :\
        ((((u1Pat & 0x10) == 0x10) ? 2 : \
        (((u1Pat & 0x40) == 0x40) ? 3 : \
        ((u1Pat == 0) ? 4 : 0))))))

#define BUDDY_GET_FREE_RIGHT_BLKS(u1Pat) \
        (((u1Pat == 1) || (u1Pat == 0)) ? 4 : \
        (((u1Pat & 0x40) == 0x40) ? 1 :\
        ((((u1Pat & 0x10) == 0x10) ? 2 : \
        (((u1Pat & 0x04) == 0x04) ? 3 : 0)))))

typedef struct _BuddyBuf {

   struct _BuddyBuf  *pNext;
   UINT1             au1Data[4];

} tBuddyBuf;

typedef struct _BuddyTable {

   UINT4       u4MaxBlkSize;       
                         /* The maximum Block size that can be allocated
                          * to application
                          */
   UINT4       u4MinBlkSize;
                         /* The minimum Block size that can be allocated
                          * to the application.
                          */ 
   UINT4       u4MemAlloc;                      
   tBuddyBuf   *pBuddyBuf;        
                         /* The pointer to the list of buddy buffers
                          */
   UINT4       *pu4FreeQ;        
                         /* The pointer to free buddy blocks of different
                          * sizes
                          */
   UINT4       u4NumBlks;          
                         /* The number 'u4MaxBlkSize' block of buddy buffers
                          */ 
   UINT2       u2HdrSize;
                         /* The size of the header in the buddy buffer
                          */
   UINT1       u1BuddyStatus; 
                         /* The Status of the Buddy instance
                          */
   UINT1       u1CFlag;
                         /* The Flag Indicating where Buffers allocated are
                          * contiguous or non-contiguous
                          */ 
} tBuddyTable;

PUBLIC VOID BuddyPrintStatistics (UINT1 u1Id);

#endif /* __BUDDY_H__ */
