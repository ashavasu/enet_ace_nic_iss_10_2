/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 * $Id: isfltmac.h,v 1.6 2015/04/09 06:48:19 siva Exp $
 *
 * Description: This file contains  various macros used by the
 *              FLTR Module
 *
 *******************************************************************/

#ifndef __ISFLTRMAC_H__
#define __ISFLTRMAC_H__

#ifdef ISIS_FT_ENABLED

#define ISIS_EXT_SET_FT_STATE(u1State)\
        gIsisExtInfo.u1FTState = u1State

#define ISIS_EXT_SET_FT_STATUS(u1Status)\
        gIsisExtInfo.u1FTStatus = u1Status

#define ISIS_EXT_SET_BLK_UPD_STATUS(u4Status)\
        gIsisExtInfo.u4DynBulkUpdatStatus = u4Status

#define ISIS_EXT_SET_FT_BUF_SIZE(u2BufSize)\
        gIsisExtInfo.u2FTBufSize = u2BufSize

#define ISIS_EXT_SET_FT_PEER_NODEID(u4NodeID)\
        gIsisExtInfo.u4FTPeerNodeID = u4NodeID

#define ISIS_EXT_SET_FT_SELF_NODEID(u4NodeID)\
        gIsisExtInfo.u4FTSelfNodeID = u4NodeID

#define ISIS_EXT_SET_MODULEID(u2ModuleID)\
        gIsisExtInfo.u2ModuleID = u2ModuleID

#define ISIS_EXT_IS_FT_STATE()\
        gIsisExtInfo.u1FTState

#define ISIS_EXT_IS_FT_STATUS()\
        gIsisExtInfo.u1FTStatus

#define ISIS_EXT_IS_BLK_UPD_STATUS()\
        gIsisExtInfo.u4DynBulkUpdatStatus

#define ISIS_EXT_SELF_NODE_ID()\
        gIsisExtInfo.u4SelfNodeID    

#define ISIS_EXT_FT_BUF_SIZE()\
        gIsisExtInfo.u2FTBufSize

#define ISIS_EXT_MOD_ID()\
        gIsisExtInfo.u2ModuleID

#define ISIS_EXT_PROTO_ID()\
        gIsisExtInfo.u2ProtocolID

#define ISIS_EXT_REG_ID()\
        gIsisExtInfo.u1RtmRegID

#define ISIS_CONTEXT_ACTIVE(pContext) \
        ((((ISIS_EXT_IS_FT_STATUS() == ISIS_FT_SUPP_DISABLE)\
          && (pContext->u1OperState == ISIS_UP))\
        || ((ISIS_EXT_IS_FT_STATUS() == ISIS_FT_SUPP_ENABLE)\
           && ((ISIS_EXT_IS_FT_STATE() >= ISIS_FT_ACTIVE)\
           && (pContext->u1OperState == ISIS_UP)))) ? ISIS_TRUE\
                                                    : ISIS_FALSE)

#define ISIS_IS_SYSTEM_ACTIVE() \
        (((ISIS_EXT_IS_FT_STATUS() == ISIS_FT_SUPP_DISABLE)\
         || ((ISIS_EXT_IS_FT_STATUS() == ISIS_FT_SUPP_ENABLE)\
           && ((ISIS_EXT_IS_FT_STATE() >= ISIS_FT_ACTIVE)))) ? ISIS_TRUE\
                                                             : ISIS_FALSE)

                                                       
#define ISIS_IS_FT_LSU_ENABLED() \
        (((ISIS_EXT_IS_FT_STATE () == ISIS_FT_ACTIVE)\
         && (ISIS_EXT_IS_FT_STATUS () == ISIS_FT_SUPP_ENABLE))\
          ? ISIS_TRUE : ISIS_FALSE)

#define ISIS_FLTR_ADJ_LSU(pContext, u1Cmd, pRec) \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        { \
            IsisFltrAdjLSU (pContext, u1Cmd, pRec);\
        }

#define ISIS_FLTR_MAA_LSU(pContext, u1Cmd, pRec) \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        { \
            IsisFltrManAreaAddrLSU (pContext, u1Cmd, pRec);\
        }

#define ISIS_FLTR_IPRA_LSU(pContext, u1Cmd, pIPRARec, u1ExistState) \
        if (((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (u1ExistState == ISIS_ACTIVE)) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        { \
            IsisFltrIPRALSU (pContext, u1Cmd, pIPRARec);\
        }

#define ISIS_FLTR_SA_LSU(pContext, u1Cmd, pRec, u1ExistState) \
        if (((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (u1ExistState == ISIS_ACTIVE)) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        { \
            IsisFltrSummAddrLSU (pContext, u1Cmd, pRec);\
        }

#define ISIS_FLTR_IPIF_LSU(pContext, u1Cmd, pRec) \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        { \
            IsisFltrIPIfLSU (pContext, u1Cmd, pRec);\
        }

#define ISIS_FLTR_ADJ_AA_LSU(pContext, u1Cmd, pAdjRec, pAdjAARec) \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        { \
            IsisFltrAdjAALSU (pContext, u1Cmd, pAdjRec, pAdjAARec);\
        }
#define ISIS_FLTR_CKT_LSU(pContext, u1Cmd, pCktRec, u1ExistState) \
        if (((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (u1ExistState == ISIS_ACTIVE)) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        { \
            IsisFltrCktLSU (pContext, u1Cmd, pCktRec);\
        }
#define ISIS_FLTR_CKT_LVL_LSU(pContext, u1Cmd, pCktRec, pCktLvl, u1ExistState) \
        if (((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (u1ExistState == ISIS_ACTIVE)) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        {\
            IsisFltrCktLevelLSU (pContext, u1Cmd, pCktRec, pCktLvl);\
        }

#define ISIS_FLTR_SYS_CTXT_LSU(pContext, u1Cmd, u1ExistState) \
        if (((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (u1ExistState == ISIS_ACTIVE)) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        {\
            IsisFltrSysContextLSU (pContext, u1Cmd); \
        }

#define ISIS_FLTR_SYS_CFGS_LSU(pContext, u1Cmd) \
        if (((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE)\
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))\
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        {\
            IsisFltrSysConfLSU (pContext, u1Cmd); \
        }

#define ISIS_FLTR_SYS_ACTS_LSU(pContext, u1Cmd) \
        if (((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE)\
            && (pContext->SysActuals.u1SysExistState == ISIS_ACTIVE))\
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        {\
            IsisFltrSysActLSU (pContext, u1Cmd); \
        }

#define ISIS_FLTR_LSP_LSU(pContext, u1Cmd, pRec) \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
     && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        { \
            IsisFltrLspLSU (pContext, u1Cmd, pRec);\
        }

#define ISIS_FLTR_PROT_SUPP_LSU(pContext, u1ProtSupp, u1ExistState) \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE) \
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        {\
            IsisFltrProtSuppLSU (pContext, u1ProtSupp, u1ExistState); \
        }

#define ISIS_FLTR_MEM_CFG_LSU() \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE)\
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        {\
            IsisFltrMemCfgLSU ();\
        }

#define ISIS_FLTR_IS_STAT_LSU(u1Status) \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE)\
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        {\
            IsisFltrISStatLSU (u1Status);\
        }

#define ISIS_FLTR_SCH_SPF_LSU(pContext, u1Level) \
        if ((ISIS_IS_FT_LSU_ENABLED () == ISIS_TRUE)\
            && (ISIS_EXT_IS_BLK_UPD_STATUS() != ISIS_BULK_UPDT_NOT_STARTED))\
        {\
            IsisFltrSchSPFLSU (pContext, u1Level);\
        }

#define ISIS_RED_PUT_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset = (UINT2) (u4Offset + 1); \
}

#define ISIS_RED_PUT_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset = (UINT2) (u4Offset + 2); \
}

#define ISIS_RED_PUT_4_BYTE(pMsg, u4Offset, u4Value) \
{\
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, u4Value); \
    u4Offset = (UINT2)(u4Offset + 4); \
}
#define ISIS_RED_COPY_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1Value); \
}

#define ISIS_RED_COPY_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2Value); \
}

#define ISIS_RED_COPY_4_BYTE(pMsg, u4Offset, u4Value) \
{\
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, u4Value); \
}

#define ISIS_RED_READ_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1Value); \
}

#define ISIS_RED_READ_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2Value); \
}

#define ISIS_RED_READ_4_BYTE(pMsg, u4Offset, u4Value) \
{\
    RM_GET_DATA_4_BYTE (pMsg, u4Offset, u4Value); \
}

#define ISIS_RED_READ_N_BYTE(pMsg, u4Offset, u4Value, u4Size) \
{\
    RM_GET_DATA_N_BYTE (pMsg, u4Offset, u4Value, u4Size); \
}

#define ISIS_RED_PUT_N_BYTE(pdest, psrc, u4Offset, u4Size) \
{\
    RM_COPY_TO_OFFSET(pdest, psrc, u4Offset, u4Size); \
    u4Offset = (UINT2) (u4Offset + u4Size); \
}

#define ISIS_RED_GET_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset = (UINT2) (u4Offset + 1); \
}

#define ISIS_RED_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset = (UINT2) (u4Offset + 2); \
}

#define ISIS_RED_GET_4_BYTE(pMsg, u4Offset, u4Value) \
{\
    RM_GET_DATA_4_BYTE (pMsg, u4Offset, u4Value); \
    u4Offset = (UINT2) (u4Offset + 4); \
}

#define ISIS_RED_GET_N_BYTE(psrc, pdest, u4Offset, u4Size) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, u4Size); \
    u4Offset = (UINT2) (u4Offset + u4Size); \
}

#define ISIS_RED_BUF_MOVE_VALID_OFFSET(pBuf, Len) \
{ \
    CRU_BUF_Move_ValidOffset (pBuf, Len);\
}


#define  ISIS_RED_BULK_UPD_REQ_MSG_LEN  1 /* Msg Type(1) + Msg Length */
#define  ISIS_RED_BULK_UPD_MSG_LEN      3 /* Msg Type(1) + Msg Length */
#define  ISIS_RED_BULK_TAIL_MSG_LEN     1 /* Msg Type(1) + Msg Length */
#define  ISIS_RED_MSG_HDR_SIZE          3 /* Msg Type(1) + Msg Length */
#define  ISIS_RED_BULK_LEN_OFFSET       1 /* Offset in the RM Message to
                                             fill the RM Message length */






#endif
#endif /* __ISFLTRMAC_H__ */
