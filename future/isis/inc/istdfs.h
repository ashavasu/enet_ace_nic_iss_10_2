/******************************************************************************
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 *
 * $Id: istdfs.h,v 1.28 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file contains the typedefs used in the ISIS Module
 *
 *****************************************************************************/

#ifndef __ISTDFS_H__
#define __ISTDFS_H__

#include "istincl.h"

/* NOTE: Make sure any addition (modification) or deletion of elements in the
 *       structures defined does not spoil the alignment. 
 */    


typedef INT4 (*tIsisFptr)(void *);
typedef void (*tIsisProto) (INT1 *);

typedef enum {

   ISIS_DB_LSPS = 0x01,
   ISIS_DB_CKTS,
   ISIS_DB_CKTL,
   ISIS_DB_ADJN,
   ISIS_DB_ADAD,
   ISIS_DB_IPRA,
   ISIS_DB_MAAD,
   ISIS_DB_SAAD,
   ISIS_DB_IPIF,
   ISIS_DB_CTXT,
   ISIS_DB_CONF,
   ISIS_DB_ACTS, 
   ISIS_DB_PRSU,
   ISIS_DB_MCFG,
   ISIS_IS_STAT,
   ISIS_SD_DECN
} eIsisDBType;

/* Types of buffers that may be allocated during normal protocol operations.
 * Each type of buffer may be allocated from a different memory management
 * system. for e.g., ISIS_BUF_MISC will be allocated from HEAP, ISIS_BUF_TLVs
 * will be allocated from Free Pools etc. These Buffer types are passed to
 * memory allocation routines to enable them decide the memory allocation
 * strategy.
 */

typedef enum {

   ISIS_BUF_MISC = 0x01,      /* Miscellaneous buffers 
                               */
   ISIS_BUF_BDDY,             /* Buffers maintained in the Buddy Buffer pools.
                               * type is used only for releasing of the buffers
                               * in the DLLI and cannot be used for allocationg
                               * any buffers
                               */ 
   ISIS_BUF_TLVS,             /* Buffers for constructing TLVs
                               */
   ISIS_BUF_MDTP,             /* Multi-Data Type buffers used
                               * during Self LSP construction
                               */
   ISIS_BUF_LDBE,             /* Buffers used for holding LSP
                               * headers in the LSP Database. These headers
                               * include a pointer to the LSP along with the
                               * next Hash Link and Timer Links.
                               */
   ISIS_BUF_SLSP,             /* Buffer to hold Self LSP information which
                               * includes Pseudonode and Non-Pseudonode (Zero
                               * and Non Zero LSPs) LSP Header information 
                               */
   ISIS_BUF_SNLI,             /* Buffers to hold Pseudonode LSP Header which in
                               * holds LSP Info.
                               */
   ISIS_BUF_NSLI,             /* Buffers to hold Non-Pseudonode LSP Header which
                               * in turn holds LSP Info.
                               */
   ISIS_BUF_LTXQ,             /* Buffer to Hold LSP TxQ Entries
                               */
   ISIS_BUF_LSPI,             /* Buffer to hold the received or constructed
                               * Self LSP buffers
                               */
   ISIS_BUF_SPTN,             /* SPT Nodes for PATH, TENT and
                               * Update process 
                               */
   ISIS_BUF_IPRA,             /* Buffers to hold IP Reachable 
                               * Information
                               */
   ISIS_BUF_ADIR,             /* AdJ Dir Buffers
                               */
   ISIS_BUF_CTXT,             /* Buffers to hold System Context
                               * information
                               */
   ISIS_BUF_FLTR,             /* Buffers used for constructing
                               * packets during Lock Step Update
                               * and Bulk Update process.
                               */ 
   ISIS_BUF_RTMD,             /* Buffers used for updating Route information in
                               * routing table and forwarding cache
                               */ 
   ISIS_BUF_CKTS,             /* Buffers for Circuit Records
                               */
   ISIS_BUF_CKTL,             /* Buffers for Circuit Level Records
                               */
   ISIS_BUF_SRMF,             /* Buffers to hold SRM flags
                               */
   ISIS_BUF_ADJN,             /* Buffers for Adjacency Records
                               */
   ISIS_BUF_ADAA,             /* Buffers for Adjacency Area 
                               * Address Records
                               */ 
   ISIS_BUF_EVTS,             /* Buffers for posting Events
                               */
   ISIS_BUF_EVTE,             /* Buffers for logging events in the Event Table
                               */
   ISIS_BUF_SATE,             /* Buffers for Sumary Address Table
                               */
   ISIS_BUF_MAAT,             /* Buffers for Manual Area Address
                               * Table
                               */
   ISIS_BUF_ARAD,             /* Buffers for Area Address
                               */
   ISIS_BUF_IPIF,             /* Buffers for IP Interface Address
                               * Table
                               */
   ISIS_BUF_ECTS,             /* Buffers for Equivalence Class
                               * of timers
                               */
   ISIS_BUF_ECTI,             /* Buffers for holding the information in the 
                               * Equivalence Class of timers
                               */
   ISIS_BUF_HLPD,             /* Buffers to construct and process
                               * HELLO PDUs
                               */
   ISIS_BUF_CSNP,             /* Buffers to construct and process
                               * CSNP PDUs
                               */
   ISIS_BUF_PSNP,             /* Buffers to construct and process
                               * PSNP PDUs
                               */
   ISIS_BUF_DLLI,             /* Buffers transmitted or received to/from DLL
                               * modules
                               */
   ISIS_BUF_MSGS,             /* Interface Message Buffer
                               */
   ISIS_BUF_CHNS,             /* Chained Buffers used for exchanging messages
                               * across modules
                               */
   ISIS_BUF_LSPTX,

   ISIS_RRD_ROUTE,
   ISIS_BUF_HTBL,
   ISIS_BUF_HSTNME
                             
} eIsisBufTypes ;

/* Module Enumeration
 */

typedef enum {

   ISIS_ADJ_MODULE = 0x00,
   ISIS_CTL_MODULE,
   ISIS_UPD_MODULE,
   ISIS_DEC_MODULE,
   ISIS_TMR_MODULE,
   ISIS_FLT_MODULE,
   ISIS_RTM_MODULE,
   ISIS_DLL_MODULE,
   ISIS_BPC_MODULE,
   ISIS_FWD_MODULE,
   ISIS_TRF_MODULE,
   ISIS_SBD_MODULE,
   ISIS_NMG_MODULE,
   ISIS_DBG_MODULE,
   ISIS_UTL_MODULE,
   ISIS_INT_MODULE,
   ISIS_EE_MODULE,
   ISIS_PDMP_MODULE,
   ISIS_DT_MODULE, 
   ISIS_CR_MODULE
} eIsisModules ;

/* The enumerations are used during LSP Database Fault Tolerance updates. Since
 * LSP buffer sizes are huge, it may not be feasible to send entire LSPs as and
 * when they are received. The only fields that may get updated during a stable
 * state are the Remaining Life Time and the Sequence Numbers. Hence only the
 * modified fileds will be updated on the standby node instead of the entire
 * LSP.
 */

typedef enum {

   ISIS_LSP_RLT = 0x01,       /* Remaining Life Time */
   ISIS_LSP_SEQ               /* Sequence Number */
} eIsisLSPOID ;

/* Timers Enumeration
 */

typedef enum {

   ISIS_EQUV_CLSS_TIMER = 0x00,
                              /* Equivalence Class Timer
                               */ 
   ISIS_L1LSP_GEN_TIMER,      
                              /* L1 LSP Generation Timer
                               */
   ISIS_L2LSP_GEN_TIMER,      
                              /* L2 LSP Generation Timer
                               */
   ISIS_SPF_SCHDL_TIMER,      
                              /* Shortest Path Calculation Scheduling
                               * timer
                               */ 
   ISIS_L1WAITING_TIMER,      
                              /* L1 LSP Database Waiting Timer
                               */
   ISIS_L2WAITING_TIMER,      
                              /* L2 LSP Database Waiting Timer
                               */
    ISIS_DBS_NORMN_TIMER,      
                              /* Database Normalization timer
                               */
    ISIS_GR_T3_TIMER,         /*GR Grace Timer*/
    ISIS_GR_L1_T2_TIMER,      /*GR Level1 T2 Timer*/
    ISIS_GR_L2_T2_TIMER,      /*GR Level2 T2 timer*/

   ISIS_SEQ_REINT_TIMER,      
                              /* Sequence Wrap around timer
                              */
   ISIS_INACTIVE_TIMER        
                              /* Default status will always be Inactive
                               */

} eIsisTimerId ;

typedef UINT2 tTimerInt;

typedef UINT4 tLSPBufSize;

typedef UINT4 tCounter;

typedef UINT1 tRowStatus;

typedef UINT1 tBool;

typedef UINT4 tMilliSec;

/* Structure to hold the metric values supported by the protocol. The size of
 * the Array is restricted to hold only the Number of metrics supported,
 * thereby reducing the overall memory requirement. The protocol must ensure
 * that it accesses the proper metric value from proper index based on other
 * criteria like the types of metrics supported. The following scenarios are to
 * be considered
 *
 * Metrics Supported : DEFAULT ONLY
 * -----------------
 *
 * tIsisMetric[0] = DEFAULT, Indices 1, 2, and 3 are not defined
 *
 * Metrics Supported : DEFAULT, DELAY 
 * -----------------
 *
 * tIsisMetric[0] = DEFAULT
 * tIsisMetric[1] = DELAY, Indices 2, and 3 are not defined
 *
 * Metrics Supported : DEFAULT, ERROR 
 * -----------------
 *
 * tIsisMetric[0] = DEFAULT
 * tIsisMetric[1] = ERROR, Indices 2, and 3 are not defined
 *
 * Metrics Supported : DEFAULT, EXPENSE 
 * -----------------
 *
 * tIsisMetric[0] = DEFAULT
 * tIsisMetric[1] = EXPENSE, Indices 2, and 3 are not defined
 *
 * Metrics Supported : DEFAULT, DELAY, ERROR
 * -----------------
 *
 * tIsisMetric[0] = DEFAULT
 * tIsisMetric[1] = DELAY
 * tIsisMetric[2] = ERROR, Index 3 is not defined
 *
 * Metrics Supported : DEFAULT, DELAY, EXPENSE
 * -----------------
 *
 * tIsisMetric[0] = DEFAULT
 * tIsisMetric[1] = DELAY
 * tIsisMetric[2] = EXPENSE, Index 3 is not defined
 *
 * Metrics Supported : DEFAULT, EXPENSE and ERROR
 * -----------------
 *
 * tIsisMetric[0] = DEFAULT
 * tIsisMetric[1] = EXPENSE
 * tIsisMetric[2] = ERROR, Index 3 is not defined
 *
 * Metrics Supported : DEFAULT, DELAY, ERROR and EXPENSE
 * -----------------
 *
 * tIsisMetric[0] = DEFAULT
 * tIsisMetric[1] = DELAY
 * tIsisMetric[2] = EXPENSE
 * tIsisMetric[3] = ERROR
 *
 * The order of filling the indices is DEFAULT, DELAY, EXPENSE and ERROR 
 */
typedef UINT1 tIsisMetric [ISIS_NUM_METRICS];



/*
 * This structure includes all the memory configuration parameters required for
 * bringing up ISIS. Values for these parameters MUST be filled by the 'root'
 * task which is initialising the system. If some of the values are
 * not initialised, appropriate default values are used based on the
 * Configuration Mode. All the configuration parameters are global and not per
 * instance. For e.g., u4MaxCkts = 400, will configure the ISIS system with a
 * total of 400 circuit records, which can be allocated independently per
 * instance. This means each instance can have a different number of circuit
 * records.
 */

struct IsisMemConfigs {

   UINT4                   u4MaxInsts; 
                              /* Max number of Instances that will be
                               * initialised.
                               */
   UINT4                   u4Factor;
                              /* Buddy memory calculation is based on this 
                               * value. Since buddy memory is used for storing
                               * Self LSPs and LSPs received from peers, this 
                               * value should be appropriately configured. 
                               * Buddy module uses the following formula to 
                               * calculate the maximum number of buddy buffers
                               * of size 1492 to be allocated:
                               *
                               * Buddy Buffers = (u4MaxLSPs * u1Factor) / 100,
                               *
                               * where u1Factor is any value in 
                               * the interval[1,100]. For e.g., if u1Factor =
                               * 30 and u4MaxLSP = 100, then Buddy module
                               * allocates 30 buffers of size 1492.
                               */ 
   UINT4                   u4MaxLSP;   
                              /* Max LSPs the IS is expected to handle. This
                               * will be used for allocating LSP headers of
                               * type tIsisLSPEntry. 
                               */
   UINT4                   u4MaxCkts;     
                              /* Max number of Circuts to be configured.
                               */ 
   UINT4                   u4MaxAreaAddr;
                              /* Max number of Area Address entries to be 
                               * configured. 
                               */
   UINT4                   u4MaxMAA;
                              /* Max number of Manual Area Address entries to be
                               * configured. 
                               */
   UINT4                   u4MaxAdjs;
                              /* Max number of Adjancency entries to be 
                               * configured. 
                               */
   UINT4                   u4MaxRoutes;
                              /* Maximum number of routes the IS is expected to
                               * handle. This parameter will be used for
                               * creating SPT nodes which are used for
                               * constructing PATH and TENT databases. SPT
                               * nodes are used by Decision module to retrieve
                               * adjancency information from the LSP and 
                               * adjacency databases. The value includes both L1
                               * and L2 Routing Databases
                               */
   UINT4                   u4MaxIPRA;
                              /* Max number of IP Reachable addresses 
                               * configured. This includes both External and
                               * Internal reachibility information.
                               */ 
   UINT4                   u4MaxSA;
                              /* Max number of Summary Addresses to be
                               * configured. Applicable for L12 and L2 systems.
                               */ 
   UINT4                   u4MaxIPIF ;   
                              /* Max number of IP interface Address entries to 
                               * be configured. IP Interface addresses are
                               * required for Hello PDUs.
                               */
   UINT4                   u4MaxEvents;
                              /* Max event table entries to be configured.
                               * The Event table is used for logging exceptions
                               * that may arise during protocol operations. The
                               * table is treated like a circularly linked
                               * list, i.e., once the table is full the free
                               * index wraps around to the first entry in the
                               * table. Hence the table displays only the last
                               * 'N' events where 'N' is the size of the table.
                               */
   UINT4                   u4MaxLspTxQLen;
                              /* Maximum LSPs that can be present in the
                               * Transmission Queue at any time. The LSPs in
                               * the Transmission Queue use a different header
                               * which includes SRM flags. This parameter
                               * defines the maximum number of such headers.
                               * If the queue is full, then additional memory
                               * is allocated to accomodate the temporary
                               * internal congestion and the extra memory
                               * allocated freed up subsequently when the
                               * congestion eases.
                               */ 
   UINT4                   u4MaxMsgBuffs;
                              /* Maximum number of message buffers to be
                               * allocated. All messages posted to Control
                               * module are expected to be encapsulated within
                               * this structure. These buffers are used only
                               * for internal communication and are not held
                               * in any databases. Once the required data is
                               * retrieved from this message the message is
                               * freed immediately.
                               */ 
};

struct IsisSysCfg 
{
   tIsisMemConfigs   MemConfigs;
                              /* structure holding all the memory
                               * configuration parameters.
                               */ 
   UINT2             u2Rsvd;                              
                              /* The Rsvd field for Allignment
                               */

   UINT1             u1CtrlTask;   
                              /* Flag Indicating whether to Initialize the 
                               * Control task
                               */ 
   UINT1             u1DefMode;
                              /* The Mode of the configuration. Typical modes
                               * are listed in isdefs.h
                               */ 

};

/* Structure holds all the memory pool identifiers which holds buffers of
 * various sizes.
 */

struct IsisMemPoolId {

   tMemPoolId  u4InstsPid;    /* Free Pool Id for Contexts */
   tMemPoolId  u4CktsPid;     /* Free Pool Id for Circuits */
   tMemPoolId  u4CktLvlPid;   /* Free Pool Id for Circuits Level Tables*/
   tMemPoolId  u4MAAPid;      /* Free Pool Id for Manual Area Addresses */
   tMemPoolId  u4AAPid;       /* Free Pool Id for Area Addresses */
   tMemPoolId  u4AdjsPid;     /* Free Pool Id for Adjancencies */
   tMemPoolId  u4AdjDirPid;   /* Free Pool Id for Adjacency Directions */
   tMemPoolId  u4RoutesPid;   /* Free Pool Id for SPT nodes */
   tMemPoolId  u4IPRAPid;     /* Free Pool Id for IPRA entries */
   tMemPoolId  u4SAPid;       /* Free Pool Id for Summary Addresses */
   tMemPoolId  u4IPIFPid ;    /* Free Pool Id for IP Interface Addresses */
   tMemPoolId  u4EventsPid;   /* Free Pool Id for Events that are posted across
                               * various modules
                               */
   tMemPoolId  u4HTblPid ;    /* Free Pool Id for the Hash Tables
                               */ 
   tMemPoolId  u4LSEPid;      /* Free Pool Id for LSP Headers */
   tMemPoolId  u4ECBPid;      /* Free Pool Id for Equivalence Class Timers */
   tMemPoolId  u4MsgBuffPid;  /* Free Pool Id for Interface messages */
   tMemPoolId  u4EvtTblPid;   /* Free Pool Id for Event Table entries */
   tMemPoolId  u4DllPid   ;   /* Free Pool Id for  DLL entries */ 
   tMemPoolId  u4LspTxQPid;   /* Free Pool Id for LSPs in transmission Queue */ 
   tMemPoolId  u4AckRxPid;   /* Free Pool Id for received acknowledgment messages*/ 
   tMemPoolId  u4CSNPRxPid;   /* Free Pool Id for recieved CSNP messages*/ 
   tMemPoolId  u4RmapFilterPid;   /* Free Pool Id for LSPs routemap filters */ 
   tMemPoolId  u4GrInfoPid;   /* Free Pool Id for GR Info List */
   tMemPoolId  u4GrCxtInfoPid;   /* Free Pool Id for GR Context Info List */
   tMemPoolId  u4GrCxtpid;   /* Free Pool Id to store array of GR Context pointers*/
   tMemPoolId  u4RRDRoutesPid;   /* Free Pool Id to store RRD Routes*/
   tMemPoolId  u4HostNmePid;   /* Free Pool Id to Host name Info List*/
};


/* This structure holds information pertaining to ISIS timers. Timer ID is used
 * for differentiating between various timers.
 * 
 * NOTE: Make sure the size of this structure is aligned to 4 byte boundary
 * since this structure forms a part of _IsisTimers structure so that any
 * elements following this structure will be aligned. If tAppTimer which is
 * FSAP2 specific is either modified or removed due to target specific
 * requirements, then any modifications should ensure that all the fields in
 * the modified structure are aligned and the size is also a multiple of 4.
 */

struct IsisTimer {

   tTmrAppTimer     Timer;
                              /* Application Timer structure as defined 
                               * in FSAP2. If FSAP2 is not used this field may
                               * have to be modified as required by the targets
                               * timer management module.
                               */
   eIsisTimerId     TimerId;
                              /* Timer Identifier. 
                               */ 
   tIsisSysContext  *pContext;
                              /* pointer to System Context 
                               */
};

/* Structure to hold Area Address. The #if conditional statement defines an
 * additional field for alignment based on the ISIS_AREA_ADDR_LEN to make the
 * size of the structure a multiple of 4 so that any element following this
 * structure will be properly aligned. If the size of au1AreaAddr and u1Length
 * adds up to 4 then the alignment field will not get defined.
 */

struct IsisAreaAddrs {

    UINT1    au1AreaAddr [ISIS_AREA_ADDR_LEN];
    UINT1    u1Length;        /* Length of the Area Address */
#if ((ISIS_AREA_ADDR_LEN + 1) % 4)
    UINT1    au1Rsvd[(4 - ((ISIS_AREA_ADDR_LEN + 1) % 4))];
#endif
};

/* The structure defines the IP address and is generic enough to hold either
 * IPV4 or IPV6 addresses.
 */

struct IsisIpAddr {

    UINT1    au1IpAddr [ISIS_MAX_IP_ADDR_LEN];
                              /* Array containing IP address 
                               */
    UINT1    u1Length;
                              /* Length of the IP Address. 
                               */
    UINT1    u1PrefixLen;     
                              /* Field indicating the significant bits in the IP
                               * address. These significant bits will be used 
                               * for comparison purposes. This field provides 
                               * the IP Address Mask functionalities.
                               */
    UINT1    u1AddrType;
    UINT1    u1IpAddrIndex;
};


/* Passwords are used for authentication and can be of any size from 1 to
 * ISIS_MAX_PASSWORD_LEN.
 */

struct IsisPasswd {
  UINT1     au1Password[ISIS_MAX_PASSWORD_LEN];
                              /* The clear text Password 
                               */
   UINT1     u1Len;
                              /* The Length of the Password 
                               */
   UINT1     u1ExistState;    /* The Exist state of the entry
                               */
#if ((ISIS_MAX_PASSWORD_LEN + 2) % 4)    
   UINT1     au1Rsvd[(4 - ((ISIS_MAX_PASSWORD_LEN + 2) % 4))];
                              /* Included for alignment
                               */
#endif
};

/* This structure records all the statistics of various control packets observed
 * on a given circuit level and direction.
 */

struct IsisPktCount {

   tCounter          u4HelloPDUs;
                              /* The no. of Hello PDUs seen in this direction
                               * at this level
                               */
   tCounter          u4LinkStatePDUs;
                              /* The no. of LSP PDUs seen in this direction
                               * at this level
                               */
   tCounter          u4CSNPDUs;
                              /* The no. of CSN PDUs seen in this direction
                               * at this level
                               */
   tCounter          u4PSNPDUs;
                              /* The no. of PSN PDUs seen in this direction
                               * at this level
                               */
};

/*
 * This structure holds information pertaining to the sorted list of LSPs
 * held in the bucket. pFirstElem and pLastElem pointers are used for hashing a 
 * given LSP into appropriate bucket.
 */

struct IsisHashBucket {
   
   VOID                    *pFirstElem;
                              /* pointer to the first element in the bucket
                               */
   VOID                    *pLastElem;
                              /* pointer to the last element in the bucket
                               */
   struct IsisHashBucket  *pNext;
                              /* pointer to the next bucket.
                               */ 
   UINT2                   u2MbrCount;
                              /* The number of elements in the bucket
                               */
   UINT2                   u2Rsvd;
                              /* Included for alignment
                               */
};

/*
 * This structure holds an array of pointers to Hash Buckets which in turn hold
 * sorted chain of LSPs. u2HashSize indicates the size of the hash table (number
 * of hash buckets) which is dependent on the normalization procedure.u2HashSize
 * may change dynamically.
 */

struct IsisSortedHashTable {

   tIsisHashBucket         *pHashBkts;
                              /* Array of Hash Buckets
                               */
   UINT4                   u4TotalMbrCount;
                              /* The total number of nodes present in the 
                               * hash table. 
                               */
   UINT2                   u2HashSize;  
                              /* the size of the Hash table
                               */
   UINT2                   u2MaxMbrPerBucket;
                              /* The maximum number of nodes in any bucket.
                               */
};

/*
 * This structure holds an array of pointers in hash buckets used for maintaning
 * circuit tables, PATH and TENT databases.
 */

struct IsisHashTable {

   VOID                     *apHashBkts [ISIS_MAX_BUCKETS];
                              /* Array of Hash Buckets
                               */

   UINT1                    u1HashSize;
                              /* Size of the hash table
                               */
   UINT1                    au1Rsvd [3];
                              /* Added for alignment
                               */ 
};

/*
 * This structure defines the fixed Common header used in the processing of 
 * Control packets
 */

struct IsisComHdr {

   UINT1               u1IDRPDisc;        
                              /* Intra-Domain Routing Protocol descriminator,
                               * to identify the packet 
                               */
   UINT1               u1HdrLen;
                              /* Length of IS-IS fixed PDU header 
                               */ 
   UINT1               u1VerProtoId;      
                              /* Version (Current defined value 1) 
                               */ 
   UINT1               u1IdLen;           
                              /* Length of ID length field of the NET/NSAP 
                               */  
   UINT1               u1PDUType;         
                              /* Type of IS-IS PDU 
                               */ 
   UINT1               u1Version;         
                              /* IS-IS protocol version,equals to 1 
                               */  
   UINT1               u1Reserved;        
                              /* Reserved field, should be 0 
                               */
   UINT1               u1MaxAreaAddr;     
                              /* No of max area addresses present in the 
                               * area 
                               */
};

/*
 * This structure defines the LSP Entry TLVs. Since PSNPs are transmitted only 
 * on PSNP timeout, PSNP buffers are not constructed immediately. Hence LSP 
 * Entry TLVs are constructed and maintained per circuit which will be used 
 * for constructing PSNP buffers on PSNP timeout. 
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned.
 */

struct IsisLETLV {

  struct IsisLETLV       *pNext;
                              /* Pointer to next TLV
                               */ 
  UINT1                   au1LSPId[ISIS_LSPID_LEN];
                              /* LSP Identifier, which is a concatenation of
                               * System Id, pseudonode Id and LSP Number
                               */ 
  UINT4                   u4SeqNum;
                              /* Sequence Number
                               */ 
  UINT2                   u2RLTime;
                              /* Remaining Life time
                               */
  UINT2                   u2CheckSum;
                              /* Check sum value of the corresponding LSP
                               */ 
#if (ISIS_LSPID_LEN % 4)    
   UINT1     au1Rsvd[(4 - (ISIS_LSPID_LEN % 4))];
                              /* Included for alignment
                               */
#endif    
};

/*
 * This structure defines the TLV type which is used for maintaining Self LSPs.
 * Any change in local information is reflected in these TLVs immediately and
 * the dirty flag is set (Dirty flag is maintained per LSP). Self LSPs are 
 * constructed (from scratch) from these TLVs if Dirty Flag is set. Otherwise 
 * the stored Self LSP, constructed in the previous interval, is transmitted.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned.
 */

struct IsisTLV {

 struct IsisTLV      *pNext;
 /* Pointer to the next TLV
 */
 UINT4                u4CktIdx;
 UINT1                au1Value [ISIS_TLV_LEN];
 /* Pointer to the Value of the TLV
 */
 UINT1                u1Code;
 /* TLV type, possible values area address TLV,
  * IS Neighbour TLV, IPRA TLV, Protocol Supported
  */
 UINT1                u1Len;
 /* Length of the TLV
 */ 
 UINT1                u1Protocol; 
 /*This is used for only IP Reachable TLV . Represents Source Protocol, from which route is redistributed into ISIS.
  */

 /* Circuit Index, used only for IS adjacency 
  * TLVs. Since an IS can have more than one 
  * interface with the same neighbour, Circuit
  * Index will be used for identifying the proper
  * TLVs while inserting and deleting adjacency
  * information
  */
   UINT1     au1Rsvd[3];
                              /* Included for alignment*/
};


struct IsisIPRATLV {
    tRBNodeEmbd     RbNode;
    tIsisIpAddr     IPAddr;  /* IP Reachable Address */
    UINT1           au1Value [ISIS_TLV_LEN];
    /* Pointer to the Value of the TLV
    */
    UINT1           u1Code;
    /* TLV type, possible values area address TLV,
    * IS Neighbour TLV, IPRA TLV, Protocol Supported
    */
    UINT1           u1Len;
    /* Length of the TLV
    */
    UINT1           u1Protocol;
    /*This is used for only IP Reachable TLV . 
     * Represents Source Protocol, from which route is redistributed into ISIS.*/
    UINT1           u1LeakedRoute;
    UINT1           u1IsisEnabled;
    UINT1           u1L1toL2Flag;
};

struct IsisRRDRoutes {
    tRBNodeEmbd     RbNode;
    tIsisIpAddr     IPAddr;  /* RRD route */
    UINT4           u4Metric;
    UINT1           u1Protocol;
    UINT1           u1Pad[3];
};
/*
 * This structure includes all the configured objects that are currently in use.
 * Manager can only modify objects which follow resetting timer behaviour.
 * Objects which follow replaceOnlyWhileDisabled behaviour have read only 
 * access. At boot up all the objects included in this structure gets updated
 * based on either default values or the values configured in IsisSysConfigs
 * structure. 
 */

struct IsisSysActuals {

   tIsisPasswd   SysAreaTxPasswd;
                              /* The authentication key used for transmitting
                               * Level 1 PDUs
                               */
   tIsisPasswd   SysDomainTxPasswd;
                              /* The authentication key used for transmitting
                               * Level 2 PDUs
                               */
   tIsisPasswd   aSysAreaRxPasswd [ISIS_MAX_RX_PASSWDS];
                              /* The list of authentication keys used for 
                               * validation of received Level 1 PDUs
                               */
   tIsisPasswd   aSysDomainRxPasswd [ISIS_MAX_RX_PASSWDS];
                              /* The list of authentication keys used for 
                               * validation of received Level 2 PDUs
                               */
   tLSPBufSize   u4SysOrigL1LSPBufSize;   
                              /* The maximum size of L1 LSPs and SNPs
                               * originated by the IS.
                               */      
   tLSPBufSize   u4SysOrigL2LSPBufSize;   
                              /* The maximum size of L2 LSPs and SNPs
                               * originated by this IS            
                               */
    UINT4         u4SysMaxAge;
                              /* Value of the remaining life time field in 
                               * self LSPs
                               */
   UINT4         u4SysRxLSPBufSize;
                              /* Size of the largest Buffer that can be stored
                               */

   UINT4         u4ESHelloRate;
                              /* Rate at which the ES Hellos need to be sent
                               * when the interface become up
                               */
   UINT1         au1SysID [ISIS_SYS_ID_LEN];       
                              /* An unique System ID of the IS  
                               */

   UINT2         u2MinSPFSchTime;
                              /* The minimum time to schedule SPF algorithm.
                               * When the Min SPF Timer fires, if the rate
                               * of change of LSP database is in a specified 
                               * range, then the SPF is scheduled 
                               */
   UINT2         u2MaxSPFSchTime;
                              /* The Maximum time interval after which SPF is 
                               * scheduled irrespective of the rate of change of
                               * the LSP database
                               */
   UINT2         u2MinLSPMark;
                              /* The minimum number of LSPs that is expected to
                               * change in a SPF schedule interval as defined by
                               * MinSPFSchTime, before SPF is scheduled. If the
                               * number of LSP changes in an interval is below
                               * this mark, the scheduling of SPF algorithm is
                               * deferred. SPF is scheduled if and only if the
                               * number of LSP changes is anywhere between
                               * u2MinLSPMark and u2MaxLSPMark (both inclusive).
                               */
   UINT2         u2MaxLSPMark;
                              /* The maximum number of LSPs that can
                               * change in a SPF schedule interval as defined by
                               * MinSPFSchTime, before SPF is scheduled. If the
                               * number of LSP changes in an interval is above
                               * this mark, the scheduling of SPF algorithm is
                               * deferred. SPF is scheduled if and only if the
                               * number of LSP changes is anywhere between
                               * u2MinLSPMark and u2MaxLSPMark (both inclusive).
                               */
   tTimerInt     u2SysMaxLSPGenInt;
                              /* Maximum interval between LSP Generation
                               * in seconds by this instance. When Max LSP
                               * Generation timer fires, Self LSPs will be
                               * propogated (periodic generation).
                               */
   tTimerInt     u2SysMinL1LSPGenInt;
                              /* The minimum interval between successive
                               * generation of L1 LSPs in secs. Every Min
                               * Generation interval, all Self L1 LSPs are 
                               * scanned and if found modified, are propogated
                               * (event based generation).
                               */
   tTimerInt     u2SysMinL2LSPGenInt;
                              /* The minimum interval between successive
                               * generation of L2 LSPs in secs. Every Min
                               * Generation interval, all Self L2 LSPs are 
                               * scanned and if found modified, are propogated
                               * (event based generation).
                               */
   tTimerInt     u2SysWaitTime;   
                              /* Defines the waiting timer for LSP Database.
                               * This timer gets activated if LSP database 
                               * becomes full and the Is cannot process
                               * anymore LSPs.
                               */


   UINT1         u1SysVer;    /* Version of ISIS protocol.
                               */
   UINT1         u1SysType;   /* System type (L1, L2, L1L2)
                               */
   UINT1         u1SysMPS;    
                              /* Max Path Splits
                               */
   UINT1         u1SysMaxAA;
                              /* Maximum Area Addresses
                               */
   UINT1         u1SysAdminState;
                              /* The administrative state of the IS 
                               */
   UINT1         u1SysIDLen;  /* The Length of SystemID configured for
                               * this area 
                               */
   UINT1         u1SysAuthSupp;
                              /* Flag indicating Authentication Status
                               */   
   UINT1         u1SysAreaAuthType;
                              /* The type of authentication methods supported
                               * by the area. Current implementation supports
                               * only password authentication. Possible values
                               * Bit 0 : if set supports password authenticat
                               * Bit 1 : if set supports MD5
                               * Bit 2-7 reserved for future use
                               */
   UINT1         u1SysDomainAuthType;
                              /* The type of authentication methods supported
                               * by the Domain. Current implementation supports
                               * only password authentication. Possible values
                               * Bit 0 : if set supports password authenticat
                               * Bit 1 : if set supports MD5
                               * Bit 2-7 reserved for future use
                               */
   tBool         bSysLSPIgnoreErr;
                              /* If TRUE, allow the router to ignore
                               * LSPs that are received with internal
                               * checksum errors, rather than purging
                               */
   tBool         bSysLogAdjChanges;   
                              /* If TRUE, causes ISIS to generate
                               * a log message when an ISIS 
                               * changes state(UP or DOWN)
                               */
   UINT1         u1SysSetOL;
                              /* Administratively sets the overload bit
                               * for each level. It takes the following values:
                               * L1OVERLOAD, L2OVERLOAD, L12OVERLOAD, CLEAR
                               */
   UINT1         u1SysL1State;
                              /* The state of L1 database, possible values
                               * OFF, ON and WAIT
                               */
   UINT1         u1SysL2State;
                              /* The state of L2 database, possible values
                               * OFF, ON and WAIT
                               */
   tRowStatus    u1SysExistState;
                              /* The state of the IS-IS Router, follows the
                               * RowStatus behaviour
                               */
   tBool         bSysL2ToL1Leak;
                              /* if TRUE, allow the router to leak L2 routes
                               * into L1
                               */
  tBool         bSysMaxAACheck;
                              /* When ON, enables checking of maximum
                               * area addresses value received in the PDUs
                               */
   UINT1         u1SysMetricSupp;
                              /* The metrics supported by the IS.
                               * The possible values
                               * Bit 0 : default metric , 
                               *         this bit should be always set
                               *         hence ignored when it is not 
                               *         set by manager.
                               * Bit 1 : if set, supports delay metric.
                               * Bit 2 : if set, supports error metric.
                               * Bit 3 : if set, supports expense metric
                               * Bits 4-7 : Reserved
                               */
   UINT1         u1SysL1MetricStyle;
                              /* The System Level 1 Metric Style which
                               * may be narrow, wide or both
                               */
   UINT1         u1SysL2MetricStyle;
                              /* The System Level 2 Metric Style which
                               * may be narrow, wide or both
                               */
   UINT1         u1SysL1SPFConsiders;
                              /* The Style of Metric considered in SPF
                               * computation
                               */
   UINT1         u1SysL2SPFConsiders;
                              /* The Style of Metric considered in SPF
                               * computation
                               */
   UINT1         u1SysTEEnabled;
                              /* Specifies whether Traffic Engineering is
                               * supported. 
                               */
   
#if ((ISIS_SYS_ID_LEN + 3) % 4)
    UINT1        au1SysIdRsvd [(4 - ((ISIS_SYS_ID_LEN + 3) % 4))];
                              /* Included for alignment since ISIS_SYS_ID_LEN
                               * may be any value between 6 and 8 (both
                               * inclusive)
                               */
#endif    
};

/*
 * This structure holds all manager configured objects which follows 
 * replaceOnlyWhileDisabled behaviour. These objects are updated into 
 * tIsisSysAcutals structure when the system is reinitialized.
 */

struct IsisSysConfigs {

   UINT4         u4ESHelloRate;
                              /* Rate at which the ES Hellos need to be sent
                               * when the interface become up
                               */
   tLSPBufSize   u4SysOrigL1LSPBufSize;   
                              /* The maximum size of L1 LSPs and SNPs
                               * originated by the IS.
                               */      
   tLSPBufSize   u4SysOrigL2LSPBufSize;   
                              /* The maximum size of L2 LSPs and SNPs
                               * originated by this IS            
                               */
   UINT1         au1SysID [ISIS_SYS_ID_LEN];       
                              /* An unique System ID of the IS  
                               */

   UINT1         u1SysType;   /* System type (L1, L2, L1L2)
                               */
   UINT1         u1SysMPS;    
                              /* Max Path Splits
                               */
   UINT1         u1SysMaxAA;
                              /* Maximum Area Addresses
                               */
   UINT1         u1SysMetricSupp;
                              /* The metrics supported by the IS.
                               * The possible values
                               * Bit 0 : default metric , 
                               *         this bit should be always set
                               *         hence ignored when it is not 
                               *         set by manager.
                               * Bit 1 : if set, supports delay metric.
                               * Bit 2 : if set, supports error metric.
                               * Bit 3 : if set, supports expense metric
                               * Bits 4-7 : Reserved
                               */
   tBool         bSysL2ToL1Leak;
                              /* if TRUE, allow the router to leak L2 routes
                               * into L1
                               */
   UINT1         u1SysL1MetricStyle;
                              /* The System Level 1 Metric Style which
                               * may be narrow, wide or both
                               */
   UINT1         u1SysL2MetricStyle;
                              /* The System Level 2 Metric Style which
                               * may be narrow, wide or both
                               */
#if ((ISIS_SYS_ID_LEN + 3) % 4)    
    UINT1        au1Rsvd [(4 - ((ISIS_SYS_ID_LEN + 3) % 4))];
                              /* Included for alignment since ISIS_SYS_ID_LEN
                               * may be any value between 6 and 8 (both
                               * inclusive)
                               */
#endif   
};

/*
 * This structure records all the statistics pertaining to a circuit
 */

struct IsisCktStats {

   tCounter              u4CktAdjChgs;
                              /* No. of times Adjacency state changes
                               * have occured in this circuit
                               */
   tCounter              u4CktInitFails;
                              /* No. of times initialization of this circuit
                               * has failed
                               */
   tCounter              u4TxISHPDUs;
                              /* The no. of ISH PDUs sent to peer nodes
                               */
   tCounter              u4RxISHPDUs;
                              /* The no. of ISH PDUs received from peer nodes
                               */
   tCounter              u4CktRejAdj;
                              /* No. of times an Adjacency has been rejected 
                               * on this circuit
                               */
   tCounter              u4CktOutCtrlPDUs;
                              /* No. of ISIS Control PDUs Sent on this circuit
                               */
   tCounter              u4CktInCtrlPDUs;
                              /* No. of ISIS Control PDUs received over this
                               * circuit
                               */
   tCounter              u4CktIdLenMsmtchs;
                              /* No. of times an ISIS Control PDU with an ID 
                               * field length different from local system has
                               * been received
                               */
};


/*
 * This structure defines the IP Interface Address Entry. 
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. IP Interafce Address table is the 
 * only structure referring to this and the reference is through a pointer. 
 */

struct IsisIPIfAddr {
   
   struct IsisIPIfAddr    *pNext;
                              /* Pointer to the next Entry
                               */
   UINT4                   u4CktIfIdx; 
                              /* Identifies the circuit to which this address is
                               * associated.
                               */
   UINT4                   u4CktIfSubIdx;
                              /* Identifies the circuit to which this address is
                               * associated
                               */ 
   UINT1                   au1IPAddr[ISIS_MAX_IP_ADDR_LEN];
                              /* IP address 
                               */
#if ((ISIS_MAX_IP_ADDR_LEN) % 4)    
   UINT1     au1Rsvd[(4 - ((ISIS_MAX_IP_ADDR_LEN) % 4))];
                              /* Included for alignment
                               */
#endif    
   UINT1                   u1AddrType;
                              /* The IP Addres type, possible values
                               * IPV4 or IPV6
                               */
   tRowStatus              u1ExistState;
                              /* Status of the entry. Follows the RowStatus
                               * behaviour
                               */

   UINT1                   u1SecondaryFlag;
                              /* Flag to indicate whether secondary address 
                               * is configured */

   UINT1                   au1Rsvd1 [1];                           
                              /* Included for alignment
                               */
};

/*
 * This structure defines the IP Interface address table which holds all the
 * Interface address entries that are currently configured.
 */

struct IsisIPIfAddrTable {

   tIsisIPIfAddr           *pIPIfAddr;
                              /* Pointer to the first entry in the table
                               */
   UINT2                   u2NumEntries;
                              /* Number of Entries in the table
                               */
   UINT2                   u2Rsvd;
                              /* Included for Alignment
                               */
};

/*
 * This structure defines the header for holding an LSP in the LSP database.
 */

struct  IsisLSPEntry {
   
   UINT1                  *pu1LSP;
                              /* Pointer to LSP Buffer
                               */
   struct IsisLSPEntry   *pNxtTimerLink; 
                              /* Pointer to next timer link. Timer lists are
                               * implemented as doubly linked lists of which
                               * this LSP can be a part. This field points to 
                               * the previous node in the chain.
                               */
   struct IsisLSPEntry   *pPrevTimerLink;    
                              /* Pointer to previous timer link. Timer lists are
                               * implemented as doubly linked lists of which
                               * this LSP can be a part. This field points to 
                               * the next node in the chain.
                               */
   struct IsisLSPEntry   *pNext;    
                              /* Pointer to next LSP link in the database
                               */
   UINT2   u2HoldTime; /*Used only for printing purposes*/ 
   UINT1      u1MtId; /*MISIS*/
                             /* The MT IDs supported by the node that sent the LSP.
                              * for MT 0, 0th bit will be set. (i.e 0x0001)
                              * for MT 2, 2nd bit will be set. (i.e 0x0100)
                              */
   UINT1      au1Rsvd[1]; 
                            
};

/*
 * This structure includes Circuit Level specific information
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Circuit Entry is the 
 * only structure referring to this and the reference is through a pointer. 
 */

struct IsisCktLevel {

   tIsisMetric       Metric;  /* Metric value associated with the circuit at
                               * this level.
                               */

   UINT4             u4FullMetric; 
                              /* The default wide metric value associated with the
                               * circuit at this level. This value is set only
                               * when multi-topology is enabled.
                               */
   tIsisPktCount     RcvdPktStats;                              
                              /* The statistics of IS-IS PDUs received
                               * at this level
                               */
   tIsisPktCount     SentPktStats;                              
                              /* The statistics of IS-IS PDUs sent
                               * at this level
                               */
   tIsisPasswd       CktTxPasswd;
                              /* The authentication key used for transmitting
                               * PDUs
                               */
   tIsisPasswd       aCktRxPasswd [ISIS_MAX_RX_PASSWDS];
                              /* The list of authentication keys used for 
                               * validation of received PDUs
                               */

   tIsisLETLV        *pPSNP;  /* Pointer to PSNP buffer which holds a list of
                               * TLVs that will be used for constructing PNSP 
                               * to be transmitted.. PSNP buffers are
                               * maintained per circuit.
                               */
   tIsisLSPTxEntry   *pMarkTxLSP;
                              /* Since LSP databases are huge, it may not be
                               * possible to transmit all the LSPs in a single
                               * interval. Hence the implementation marks the
                               * last LSP transmitted in the previous interval
                               * so that the transmission can resume from the
                               * LSP next to the marked LSP. This field is used
                               * for marking LSPs in the Tx Q.
                               */

   tCounter          u4CktLanDISChgs;
                              /* No. of times the LAN Designated IS
                               * has changed at this level
                               */

   UINT4             u4DISDirIdx;
                              /* The Direction Index of the DIS in cases of 
                               * Broadcast circuits. This Direction Index
                               * will be used by decision module to add 
                               * those adjacencies that are listed in 
                               * pseudonode LSPs, but do not exist in local
                               * systems adjacency database. 
                               */ 


   UINT4             u4MinLspReTxInt; 
                              /* Minimum Interval in seconds between
                               * the re-transmission of LSPs*/
  UINT4             u4NumAdjs;
                              /* The Adjacencies existing in the circuit at 
                               * this level
                               */ 
#if ((ISIS_NUM_METRICS) % 4)    
    UINT1            au1MetRsvd [(4 - ((ISIS_NUM_METRICS) % 4))];
                              /* Included for alignment since ISIS_NUM_METRICS
                               * may be any value between 1 and 4 (both
                               * inclusive)
                               */
#endif    
   UINT1             au1CLCktID [ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
                              /* Concatenation of System ID and the Pseudonode
                               * ID of either the local system or the peer which
                               * is based on negotiations in the case of
                               * point-to-point circuits. Concatenation of 
                               * system ID and Pseudonode ID of the local system
                               * in the case of broadcast circuits which can be
                               * ignored since au1CktLanDISID is used for BC
                               * circuits.
                               */
   UINT1             u1CircLevelAuthType; 

   UINT1             au1CktLanDISID [ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
                              /* The pseudonode ID of the LAN DIS for this
                               * circuit. This is used in Hello PDUs once the
                               * Designated IS is elected.
                               */
   UINT1     au1Rsvd2[1];
                              /* Included for alignment
                               */

   UINT2             u2HelloMultiplier; 
                              /* This value multiplied by the corresponding 
                               * HelloTimer in  seconds (rounded up) is
                               * used as the Holding time in Hello PDUs
                               * transmitted by this IS.
                               */
/* used on P2P circuits*/
 UINT1             u1CSNPSent;                              
 UINT1             u1SetSRM;                              

   tMilliSec         LspThrottleInt; 
                              /* This interval defines the time in millisecs 
                               * that should elapse between successive PDU 
                               * transmissions on this circuit.
                               */
   tMilliSec         DRHelloTimeInt;
                              /* Maximum period, in milli seconds, between 
                               * IIH PDUs generated by this system on 
                               * Multi-Access networks, if the local system
                               * is a DIS for that network.
                               */
   tMilliSec         HelloTimeInt;
                              /* Maximum period, in milli seconds, between 
                               * IIH PDUs generated by this system.
                               */
    tTimerInt         PSNPInterval;  
                              /* Interval of time, in seconds, between 
                               * transmission of PSNPs
                               */
   tTimerInt         CSNPInterval; 
                              /* Time delay between successive transmission
                               * of CSNPs on Broadcast Circuits.
                               */
   tTimerInt         u2ThrtlCount;
                              /* Number of times the Throttle timer has
                               * fired.
                               */

   UINT1             u1CktLvlIdx;
                              /* Unique identifier - index for the table
                               */
   UINT1             u1ISPriority; 
                              /* The priority for becoming LAN Designated 
                               * Intermediate System on a broadcast Circuit
                               */
  tBool             bIsDIS;
                              /* Flag indicating whether the local system is
                               * the DIS for the circuit at this level.
                               */ 
   UINT1             u1ECTId; /* The Equivalent Class Timer Identifier of 
                               * CSNP/PSNP
                               */
   UINT1             u1HelloTmrIdx;
                              /* Equivalence Class timer Identifier for Hello
                               * Timers
                               */ 
   UINT1             u1LSPTxTmrIdx;
                              /* Equivalence Class timer Identifier for LSP
                               * Transmission Timers
                                */ 
};

/*
 *  This structure holds area addresses of the neighbouring IS as reported in 
 *  received IIH PDUs. These area addresses are computed as specified in 
 *  Sec. 7.2.11 of ISO 10589.
 */

struct IsisAdjAAEntry {

   UINT4                    u4AdjAreaAddrIdx;
                              /* Adj area Address index 
                               * Newly Added in MIB-8
                               */

   tIsisAreaAddr            ISAdjAreaAddress;
                              /* The area address of the IS neighbour 
                               */
   struct IsisAdjAAEntry   *pNext;
                              /* pointer to the next IsAAEntry
                               */
};

/*
 * This structure holds the adjacency information pertaining to a circuit.
 * Adjancencies are maintained as a linked list independently per circuit.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Circuit Entry is the 
 * only structure referring to this and the reference is through a pointer. 
 */

struct IsisAdjEntry {
   tIsisIpAddr          AdjNbrIpV4Addr;

   tIsisIpAddr          AdjNbrIpV6Addr;
 
   tIsisCktEntry         *pCktRec; 
                              /* Pointer to Circuit Entry to which this
                               * Adjacency belongs
                               */ 
   tIsisAdjAAEntry       *pAdjAreaAddr;
                              /* pointer to a list of area addresses of the
                               * neighboruing IS
                               */
   struct IsisAdjEntry  *pNext; 
                              /* Pointer to next adjacency record
                               */

   UINT4                 u4AdjUpTime;
                              /* The amount of time in secs since this adjacency
                               * entered state UP
                               */

   UINT4                 u4AdjIdx;
                              /* Unique value to identify the IS Adjacency
                               * from all other such adjacencies on this
                               * circuit
                               */
   UINT4     u4StartTime;
            /* Starting Time Stamp- as GR Helper*/
   UINT2                 u2HoldTime;
                              /* The value of the Holding time for the adjacency
                               * as reported in Hellos PDUs
                               */

   UINT2                u2AdjGRTime;  /*Adjacent GR time*/

   UINT1                 au1AdjProtSupp[ISIS_MAX_PROTS_SUPP];
                              /* The protocols supported by the neighbouring IS.
                               */
   UINT1                u1AdjMTId; 
                             /* The MT IDs supported by the adjacent node.
                              * for MT 0, 0th bit will be set. (i.e 0x0001)
                              * for MT 2, 2nd bit will be set. (i.e 0x0100)
                              */

   UINT1                 au1AdjNbrSNPA[ISIS_SNPA_ADDR_LEN];
   UINT1       u1P2PThreewayState;
   UINT1       u1P2PPeerThreewayState;
   UINT4     u4NeighExtCircuitID;   
   UINT4     u4AdjPeerNbrExtCircuitID;   
   UINT1                 au1AdjPeerNbrSysID[ISIS_SYS_ID_LEN];
   UINT1       au1Pad[2]; /*added for 8 byte padding*/ 
                              /* The SNPA Address of the neighbouring system
                               */
   UINT1                 au1AdjNbrSysID[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
                              /* The System ID  of the the neighbouring IS
                               */
   
   UINT1       u1ThreeWayHndShkVersion;

   UINT1                 u1AdjState;
                              /* The state of the adjacency. the possible 
                               * values are INIT, UP, FAILED, DOWN
                               */
   UINT1                 u1AdjNbrSysType;
                              /* The value indicates the Neighbouring IS System
                               * type. The possible values are UNDEFINED, L1, L2
                               * and L1L2
                               */
   UINT1                 u1TmrIdx;
                              /* Any started timer will be inserted into a
                               * appropriate entry in the EC Timer array.
                               * This field stores the index into the Timer 
                               * Array, which will be used for stopping 
                               * Holding timers.
                               */ 
   UINT1                 u1AdjUsage;
                              /* The value indicates the adjacency usage with
                               * the neighbouring system. The possible values 
                               * are L1, L2, L1L2 and UNKNOWN in the case of
                               * Point-to-Point and L1, L2 in case of Broadcast.
                               */
   UINT1                 u1AdjNbrPriority;
                              /* The priority of the neighbouring IS used for 
                               * determining the LAN Designated IS
                               */
   UINT1                 u1AdjIpAddrType; 
                              /* Type of IP Address, ipV4 or ipV6, supported by
                               * the neighbouring IS
                               */
  
 
   UINT1                u1IsisAdjFullState;
   UINT1                u1IsisGRHelperStatus;
   UINT1                u1IsisGRHelperExitReason;
   UINT1                u1IsisAdjGRState; /*Suppresses or Running*/
   UINT1                u1HelperAckState;
   UINT1                u1IsGRCapEnabled;

   /* Variables added for supporting BFD for M-ISIS */
   UINT1                u1IsisMT0BfdEnabled; /* This variable holds the value of 
                                                MTID-NLPID pair received in BFD 
                                                Enabled TLV, for MT 0. 
                                                0th Bit set ==> BFD Enabled for IPv4 NLPID 
                                                                & the ckt also has IPv4 & BFD enabled
                                                ISIS_TOPO_BFD_REQUIRED variable for MT 0  
                                                is the logical OR of 0th bit and 1st bit. */

   UINT1                u1IsisMT2BfdEnabled; /* This variable holds the value of 
                                                MTID-NLPID pair received in BFD 
                                                Enabled TLV, for MT 2. 
                                                1st Bit set ==> BFD Enabled for IPv6 NLPID
                                                               & the ckt also has IPv6 & BFD enabled
                                                ISIS_TOPO_BFD_REQUIRED variable for MT 2 
                                                is the logical OR of 0th bit and 1st bit. */

   UINT1                u1IsisBfdRequired;   /* This variable indicates if ISIS is 
                                                required to register with BFD 
                                                for path monitoring. 
                                                This is a logical AND of 
                                                u1IsisMT0BfdEnabled & u1IsisMT2BfdEnabled 
                                                and is the ISIS_BFD_REQUIRED variable. */

   UINT1                u1IsisMT0BfdState;   /* This variable indicates if BFD session 
                                                state is up for the NLPID, 
                                                for which BFD is registered.
                                                0th Bit set ==> BFD session UP for NLPID - IPV4 (0xCC) 
                                                ISIS_TOPO_USEABLE variable for MT 0 
                                                is the logical AND of 0th bit and 1st bit. */

   UINT1                u1IsisMT2BfdState;   /* This variable indicates if BFD session 
                                                state is up for the NLPID, 
                                                for which BFD is registered. 
                                                1st Bit set ==> BFD session UP for NLPID - IPV6 (0x8E)
                                                ISIS_TOPO_USEABLE variable for MT 2 
                                                is the logical AND of 0th bit and 1st bit. */

   UINT1                u1IsisBfdNeighborUseable; /* This variable indicates if the 
                                                     neighbor is useable i.e. if 
                                                     adjacency can be established 
                                                     with the particular neighbor
                                                     or not */
   UINT1                u1DoNotUpdHoldTmr; /* Indicates whether Hold timer can be
                                              restarted or not */
   UINT1                u1Mt0BfdRegd; /* Indicates whether BFD registration is done for 
                                         for this MT ID. In case of BFD over single topology
                                         ISIS, this variable is used to indicate BFD registration
                                         for IPv4 path */
   UINT1                u1Mt2BfdRegd; /* In case of BFD over single topology ISIS, this 
                                         variable is used to indicate BFD registration 
                                         for IPv6 path */
   UINT1                au1Rsvd3 [3];
};

/*
 * This structure holds the List of IP Reachability Entries for a particular
 * circuit
 */

struct IsisIPRAInfo {

   tIsisIPRAEntry  *pIPRAEntry;
                              /* Pointer to the first entry in the table
                               */
   UINT2           u2NumEntries;
                              /* Number of Entries in the table
                               */
   UINT1    au1Pad[2];
   UINT4           u4NxtAutoIdx;
                              /* The next available IPRA Index for 
          * IPRA of type automatic (IPRA learnt from 
          * RTM etc.)
                               */ 
};
struct IsisRRDConfig {
  UINT4     au4RRDMetric[ISIS_MAX_METRIC_INDEX + 1];  
                   /*Metric value for each routing protocol */
  UINT4     u4RRDSrcProtoMask;
  UINT1     au1RRDRMapName[RMAP_MAX_NAME_LEN + 4]; 
  UINT1     u1RRDStatus;
  UINT1     u1RRDImportType; /* used for Import the routes in which level to be flooded*/
  UINT1     au1Pad[2]; 
};

/*
 * This structure holds details regarding a configured circuit
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Circuit table is the 
 * only structure referring to this and the reference is through a pointer. 
 */

struct  IsisCktEntry {
   tIsisCktLevel         *pL1CktInfo;
                              /* Pointer to L1 Circuit Level table 
                               */
   tIsisCktLevel         *pL2CktInfo;
                              /* Pointer to L2 Circuit Level table 
                               */
   tIsisAdjEntry         *pAdjEntry; 
                              /* Pointer to list of adjacency entries  
                               */ 
   tIsisAdjEntry         *pLastAdjEntry;
                              /* Cache-pointer for holding last node accesed by 
                               * GetNext... function(SNMP improvement, table isisAdjTable) 
                               * Structure alignment is not need.
                               */ 
   tIsisSysContext       *pContext;
                              /* Pointer to the system context
                               */
   struct IsisCktEntry  *pNext;
                              /* Pointer to the next circuit record
                               */
   struct IsisCktEntry  *pNextHashEntry;
                              /* Circuit records are maintained in a global hash
                               * table which can be accessed by hashing on 
                               * CircuitIfIndex. This field holds the pointer to
                               * the next global circuit hash entry.
                               */
   struct IsisCktEntry  *pPrevHashEntry;
                              /* Pointer to the previous circuit record in the
                               * global hash table
                               */
   tIsisCktStats         CktStats;
                              /*  Statistics pertaining to the circuit.
                               */

   UINT4                 u4CktIdx;
                              /* The value of the Identifier unique within the 
                               * instance of the protocol
                               */
   UINT4                 u4CktIfIdx;
                              /* The Circuit If Index - same as the IfIndex
                               * configured in the MIBII Interafce table.
                               */
   UINT4                 u4CktIfSubIdx;
                              /* The specifier for the part of the interface
                               * IfIndex to which this Circuit corresponds
                               * such as DLCI or VPI/VCI
                               */
   UINT4                u4LLHandle;    
                              /* Data Link Layer module specific information
                               * which is used for all interactions with the
                               * module
                               */ 
   UINT4                 u4CktUpTime;
                              /* The amount of time in secs since this circuit
                               * entered state UP
                               */
   UINT4                 u4CktMtu;
                              /* The Maximum Data transfer unit in bytes
                               * supported by the circuit
                               */
/* P2P three way handshake*/
 UINT4     u4ExtLocalCircID;
         /* This ID must be uniquie across all circuits in an IS*/
 UINT1    au1Pad[4];
/* P2P three way handshake*/

   UINT1                au1IPV4Addr [ISIS_MAX_IPV4_ADDR_LEN];
                              /* Holds the IPV4 Address of the interface
                               */ 

  UINT1                au1IPV6Addr [ISIS_MAX_IPV6_ADDR_LEN];
                                    /* Holds the IP Address of the IPV6 interface
                                      */ 
  UINT1                 au1P2PCktID[ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
                              /* The ID of the Circuit allocated during 
                               * initialisation. This value must be configured
                               * by the manager for passive circuits since no
                               * negotiations will be held over such circuits.
                               * This value will be used only for passive
                               * circuits
                               */
   UINT1                 u1ProtoSupp;
                              /* Supported protocol. The possible values
                               * are IPV4, IPV6, or Both
                               */
 
   UINT1                 au1SNPA[ISIS_SNPA_ADDR_LEN];
                              /* Source SNPA address for this circuit
                               */
   UINT1                 u1IfMTId;
                             /* MTs supported by the interface
                              * for MT 0, 0th bit will be set. (i.e value 0x0001)
                              * for MT 2, 2nd bit will be set. (i.e value 0x0100)
                              */
   UINT1                 u1TwoWayStateDecision; /* This is used as input for three way state transition*/

   UINT2                 u2CktMeshGrps;
                              /* The circuit in the same mesh group acts as a 
                               * virtual multi-access network which prevents
                               * flooding of LSPs in another circuit in the 
                               * same mesh group
                               */

   UINT1                 u1CktLocalID;
                              /* The identification that can be used in 
                               * protocol packets to identify the Circuits. This
                               * is unique per instance
                               */
   tBool                 bCktAdminState;
                              /* The administrative state of the Circuit 
                               */
   tRowStatus            u1CktExistState;
                              /* The existance state of the Circuit, follows
                               * the row status behaviour
                               */
   UINT1                 u1CktType;
                              /* The type of the Circuit. The possible values
                               * are  P2P , Broadcast, static in, static out, 
                               * DA, nbma etc
                               */
   tBool                 bCktExtDomain;
                              /* Flag indicating the type of link, whether
                               * internal or external. if TRUE (external) 
                               * suppress Tx'ions and Interpretations of ISIS 
                               * PDUs
                               */
   UINT1                 u1CktLevel;
                              /* This indicates the Level of IS. Possible
                               * values L1, L2 Only or L1L2
                               */
   UINT1                 u1CktMCAddr;
                              /* Specifies which type of Multicast address
                               * will be used for sending Hello PDUs on this
                               * Circuit
                               */
   tBool                 bCktPassiveCkt;
                              /* This tells if we should include this interface
                               * in LSPs, even if it is not running the ISIS
                               * protocol
                               */
   UINT1                 u1CktMeshGrpEnabled;
                              /* This indicates the status of the Port in
                               * a Mesh group. Possible values are inactive,
                               * blocked or set
                               */
   tBool                 bCktSmallHello;
                              /* This indicates whether unpadded hellos can
                               * be sent on LAN circuits
                               */
   UINT1                 u1CktIfStatus;
                              /* The Circuit interface status. This is 
                               * set to ON when lower layers are up. This
                               * can even be set by the manager if lower layers
                               * do not explicitly give status indications to
                               * ISIS. Unless this object is set to UP, protocol
                               * operations do not commence on this circuit.
                               */
   tBool                 bTxEnable;
                              /* Is Transmission enabled for this circuit
                               * ( for testing purposes only )
                               */
   tBool                 bRxEnable;
                              /* Is Reception enabled for this circuit
                               * ( for testing purposes only )
                               */
   UINT1                 u1QFlag;
                              /* Quorum Flag used to check whether the mandatory
                               * parameter are set or not 
                               * bit1 is for CircIfIndex
                               * bit2 is for CircIfSubIndex
                               * bit3 is for CircLocalId
                               * bit4 is for CircType
                               */
    UINT1                 u1ISHTxCnt;
                              /* The number of times ISH packets are sent on
                               * the point to point circuit. If it reaches 
                               * Threshold value, the system will stop sending
                               * ISH PDU.
                               * NOTE : This field is valid only for point to 
                               * point circuit
                               */ 
   UINT1                 u1NodeID;
                              /* The Identifier of the Node to which the circuit
                               * belongs. The value is configured by the manager
                               */
   UINT1                 u1OperState;
                              /* The operational State of the Circuit
                               * This is made up only when the Cicruit is
                               * ACTIVE, Interface Status is UP and Admin Status
                               * is ON
                               i*/
   UINT1                u1IsisGRT1TmrIdx; /*T1 Timer ID*/
   UINT1                u1IsisGRRestartHelloTxCount  ;  /*Hello retransmission count*/
   UINT1                u1IsisGRL1HelloSend;
   UINT1                u1IsisGRL2HelloSend;
   UINT1                u1IsisGRRestartL1State; /*L1 Restart Ack/CSNP received*/
   UINT1                u1IsisGRRestartL2State; /*L2 Restart Ack/CSNP received*/

   /* Variables added for BFD_M-ISIS */
   UINT1                u1IsisBfdStatus;
                                                /* Indicates whether BFD is enabled/disabled 
                                                 * on a particular circuit on which 
                                                 * multi-topology is enabled. */
   /* P2P three way handshake*/
   UINT1     u1IsP2PThreeWayEnabled;
   UINT1    u1P2PDynHshkMachanism;
};

/*
 * This structure holds information regarding all the circuits and adjacencies
 * established over this circuit. 
 */

struct IsisCktTable {

   UINT4         u4NumEntries;
                              /* Number of Entries in the table
                               */
   UINT4         u4NumActCkts;
                              /* Number of Circuits in the active state
                               */
   UINT4         u4NumL1Adjs;
                              /* Total number of L1 adjacencies established 
                               * over all the circuits
                               */
   UINT4         u4NumL2Adjs;
                              /* Total number of L2 adjacencies established 
                               * over all the circuits
                               */
   UINT4         u4NextCktIndex;
                              /* A get on this object should return the next
                               * (minimum) free Circuit Table index. One
                               * restriction is the index returned MUST always
                               * be less than ISIS_MAX_CIRCUITS.
                               */
   tIsisCktEntry *pCktRec;
                              /* Pointer to the first circuit entry in the table
                               */
   UINT1         *pu1L1CktMask;
                              /* Field indicating the ALL_CIRCUITS_SET mask
                               * for L1 Circuits
                               */ 
   UINT1         *pu1L2CktMask;
                              /* Field indicating the ALL_CIRCUITS_SET mask
                               * for L1 Circuits
                               */ 
};

/*
 * This structure holds the IP reachability information. Each entry specifies
 * the reachable IP address for all the Metrics supported.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. IP Reachable Address table is the 
 * only structure referring to this and the reference is through a pointer. 
 */

struct IsisIPRAEntry {

   struct IsisIPRAEntry  *pNext;
                              /* Pointer to next IPRA record
                               */

   tIsisMetric            Metric;
                              /* the metric value for reaching
                               * the specified destination over the 
                               * circuit. It also includes the metric
                               * type whether internal or external. 
                               */
   UINT4                  u4FullMetric;
                              /* The default wide metric value for 
                               * this IPRA entry. This value is set
                               * only when multi-topology is enabled */

   UINT4                  u4IfIndex;
                              /* The Interface index on which IP Address is
          * Reachable, usefull when the IPRA Type is 
          * automatic
          */ 
   UINT4                  u4IfSubIndex;
                              /* The Interface sub index on which IP Address is
          * Reachable, usefull when the IPRA Type is 
          * automatic
          */ 
#if (ISIS_NUM_METRICS % 4)    
    UINT1                 au1MetRsvd[(4 - (ISIS_NUM_METRICS % 4))];
                              /* Included for alignment since ISIS_NUM_METRICS
                               * may be any value between 1 and 4 (both
                               * inclusive)
                               */
#endif    

   UINT4                  u4IPRAIdx;
                              /* An unique identifier for the IS-IS IP Reachable
                               * Address (IPRA) entry
                               */
   UINT1                  u1IPRAType;
                              /* The type of IP Reachable address, possible
                               * values are manual and automatic
                               */
   UINT1                  u1IPRADestType;
                              /* Type of IPRA (IPv4 or IPv6)
                               */

    UINT1                  au1IPRASNPA[ISIS_SNPA_ADDR_LEN];
                              /* The SNPA address to which a PDU may
                               * be forwarded to reach a destination
                               * which matches this IPRA
                               */
    UINT1    au1Rsvd[3]; 
   tRowStatus             u1IPRAExistState;
                              /* Status of the entry, follows Rowstatus
                               * behaviour
                               */
   UINT1                  au1IPRADest[ISIS_MAX_IP_ADDR_LEN];
                              /* Destination IP Address reachable
                               */
   UINT1                  u1IPRAAdminState;
                              /* Administrative state of the IPRA
                               */
   UINT1                  u1QFlag;
                              /* Quorum Flag used to check whether the mandatory
                               * parameter are set 
                               * bit 0 is for IPRADestType
                               * bit 1 is for IPRADest
                               * bit 2 is for IPRAMaskType
                               * bit 3 is for IPRAMask
                               */
   UINT1                  au1IPRANextHop[ISIS_MAX_IP_ADDR_LEN];
                              /* Next Hop IP address */    
   UINT1                  u1PrefixLen; 
                              /* Number of most significant bits set. This
                               * specifies a mask for the given address.
                               */
     
    UINT1                  u1IPRANextHopType;
                              /* Type of IPRA (IPv4 or IPv6)
                               */
};                     

/*
 * This structure holds the Manual Area address.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Manual Area Address table is the 
 * only structure referring to this and the reference is through a pointer. 
 */

struct IsisMAAEntry {

   tIsisAreaAddr          ManAreaAddr;
                              /* Manual area address configured by 
                               * manager
                               */
   struct IsisMAAEntry   *pNext;
                              /* Pointer to the next entry
                               */ 
   tRowStatus             u1ExistState;
                              /* Status of the entry, follows 
                               * the RowStatus behaviour
                               */
   UINT1                  au1Rsvd [3];
                              /* Included for Alignment
                               */ 
};

/*
 * This structure holds a list of all Manual area addresses configured by the 
 * manager.
 */

struct IsisMAATable {

   tIsisMAAEntry   *pMAARec;
                              /* Pointer to the first entry in the table
                               */
   UINT2           u2NumEntries;
                              /* Number of Entries in the table
                               */
   UINT2           u2Rsvd;
                              /* Included for Alignment
                               */
};

/*
 *  This structure holds the area address. 
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Area Address table is the 
 * only structure referring to this and the reference is through a pointer. 
 */

struct IsisAAEntry {

   tIsisAreaAddr          AreaAddr;
                              /* Area address as reported in L1 LSP
                               */
   struct IsisAAEntry    *pNext;
                              /* Pointer to the next area address entry
                               */
   UINT2                  u2UsgCnt;
                              /* Specifies the reference count for this entry.
                               * Since the Area Addresses can be referred to by
                               * several LSPs, this field ensures that Area
                               * Addresses are not deleted while some of the
                               * LSPs are still referring to it. For every LSP
                               * that refers to this address the count will be
                               * incremented by one. If the usage count becomes
                               * zero then the address will be removed.
                               */ 
   UINT2                  u2Rsvd;                           
};

/*
 * This structure holds a List of all Area address reported in L1 LSPs.
 */

struct IsisAATable {

   tIsisAAEntry   *pAARec;
                              /* pointer to the List of area address entries
                               */
   UINT2          u2NumEntries;
                              /* The number of entries present in the table
                               */
   UINT2          u2Rsvd;
                              /* Included for Alignment
                               */
};


/*
 * This structure holds IP Summary address information. 
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Summary Address table is the 
 * only structure referring to this and the reference is through a pointer. 
 */

struct IsisSAEntry {
   struct IsisSAEntry *pNext;
                              /* Pointer to the next summary address entry
                               */
   tIsisMetric         Metric;
                              /* Metric to reach the destination.
                               */

   UINT4               u4FullMetric;
                              /* The default wide metric value for
                               * this summary address entry. 
                               * This value is set only when 
                               * multi-topology is enabled */

   UINT1               au1SummAddr[ISIS_MAX_IP_ADDR_LEN];
                              /* The Summary IP address
                               */

   UINT2               u2L1UsageCnt;
                              /* The number of IPRA TLVs refering the 
                               * summary address in L1 Summary Address
                               * Admin State
                               */ 
   UINT2               u2L2UsageCnt;
                              /* The number of IPRA TLVs refering the 
                               * summary address in L2 Summary Address
                               * Admin State
                               */ 

   UINT1               u1AddrType;
                              /* IP Address type, possible values
                               * IPv4 or IPv6
                               */
   UINT1               u1PrefixLen;
                              /* Indicates the number of most significant bits
                               * that are set in the given IP address. This
                               * indicates the mask for the given address.
                               */
   tRowStatus          u1ExistState;
                              /* Status of the Entry, follows
                               * the RowStatus behaviour
                               */
   UINT1               u1AdminState;
                              /* Administrative state of the IP summary 
                               * address entry, possible values are
                               * L1 Summary, L2 Summary, L12 Summary or 
                               * ADMIN_OFF
                               */
#if ((ISIS_MAX_IP_ADDR_LEN) % 4)    
   UINT1     au1Rsvd[(4 - ((ISIS_MAX_IP_ADDR_LEN) % 4))];
                              /* Included for alignment
                               */
#endif    
#if ((ISIS_NUM_METRICS) % 4)    
   UINT1     au1Rsvd1[(4 - ((ISIS_NUM_METRICS) % 4))];
                              /* Included for alignment
                               */
#endif    
};

/*
 * This structure holds a list of all summary IP addresses configured by the 
 * manager
 */

struct IsisSATable {

   tIsisSAEntry   *pSAEntry;
                              /* Pointer to the first entry of summary IP
                               * address entries
                               */
   UINT2          u2NumEntries;
                              /* Number of entries in the table
                               */
   UINT2          u2Rsvd;
                              /* Included for Alignment
                               */
};

/*
 * This structure defines the LSP header which is used for holding the LSP in
 * the Transmission Queue. The difference between this header and the Database
 * header is that this header includes SRM flags which is required for handling
 * acknowledgements.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. These records are maintained a
 * linked chains in the Hash tables.
 */

struct IsisLSPTxEntry {

   tIsisLSPEntry      *pLSPRec;
                              /* Pointer to LSP entry
                               */
   UINT1              *pu1SRM;
                              /* Pointer to SRM flags. The number of bits
                               * in the SRM depends on number of circuits. The
                               * circuit indices are given in a sequential 
                               * order and the most significant bit of the 
                               * most significant byte gives the first circuit,
                               * and next bit is for next circuit and so on.
                               */
   UINT1              u1SRMLen;
                              /* Length of the SRM flags. The Length of the SRM
                               * flags may change dynamically based on current
                               * circuits over which the LSP is required to be
                               * transmitted.
                               */ 
   UINT1             au1Rsvd [3];                           
};

/*
 * This structure holds information regarding reachable destinations(routes)
 * calculated from the received LSPs. This structure is utilised by Update and 
 * Adjacency Modules to pass Adjacency information, pertaining to other systems,
 * during SPF calculation. In the TENT and PATH databases these nodes are held
 * in Hash tables. Equal Cost Nodes will have a separate node for each of the
 * direction, and the first among these nodes will indicate the number of such
 * nodes thorugh 'u1MaxPaths' field. All other equal cost nodes will have '0'
 * filled in 'u1MaxPaths'
 */

/* An example representation of Equal Cost Paths is shown below. Assumptions are
 * the system supports all four metrics. Metric[0] = DEFAULT, Metric[1] = DELAY
 * Metric[2] = ERROR and Metric[4] = EXPENSE.
 *
 * NODE-1              NODE-2              NODE-3              NODE-4
 * Dest ID   ABCDE1    Dest ID   ABCDE2    Dest ID   ABCDE2    Dest ID   ABCDE2
 * Metric[0]     10    Metric[0]     20    Metric[0]     15    Metric[0]     32
 * Metric[1]     12    Metric[1]      1    Metric[1]     24    Metric[1]      2
 * Metric[2]     14    Metric[2]      3    Metric[2]      3    Metric[2]      3
 * Metric[3]     17    Metric[3]      6    Metric[3]     26    Metric[3]     14
 * Dir[0]         1    Dir[0]         2    Dir[0]        16    Dir[0]        23 
 * Dir[1]         2    Dir[1]         5    Dir[1]        11    Dir[1]         3 
 * Dir[2]         3    Dir[2]         6    Dir[2]        26    Dir[2]        45 
 * Dir[3]         4    Dir[3]         3    Dir[3]        15    Dir[3]         3 
 * MaxPaths[0]    0    MaxPaths[0]    0    MaxPaths[0]    0    MaxPaths[0]    0 
 * MaxPaths[1]    0    MaxPaths[1]    0    MaxPaths[1]    0    MaxPaths[1]    0 
 * MaxPaths[2]    0    MaxPaths[2]    3    MaxPaths[2]    0    MaxPaths[2]    0 
 * MaxPaths[3]    0    MaxPaths[3]    0    MaxPaths[3]    0    MaxPaths[3]    0 
 * NextNode ---------> NextNode ---------> NextNode ---------> NextNode = NULL
 *
 * The above figure shows an example list where routes to two destinations
 * "ABCDE1" and "ABCDE2" are stored and "ABCDE2" has equal cost multipath
 * through ERROR metric. Hence the first node in theECMP list has u1MaxPaths set
 * to '1' and all other nodes set to '0'. In the above representation we have
 * shown the metric values (bits 0-9) directly and did not show the bits 10 
 * thorugh 15 for simplicity.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. SPT nodes are linked together and
 * held in hash tables which forms the Shortest Path Database.
 */

struct IsisSPTNode {

 struct IsisSPTNode   *pNext;  
                              /* Pointer to next node in the list   
                               */
   tRBNodeEmbd          RbNode;                           
   UINT4                 u4MetricVal;
   UINT4                 au4DirIdx[ISIS_NUM_METRICS];      
                              /* The index of the Direction table entry which
                               * holds interface information thorugh which the
                               * destination can be reached. This is an array
                               * and each entry in the array holds a direction
                               * corrresponding to a different metric.
                               */
      UINT2                 au2Metric[ISIS_NUM_METRICS];
                              /* Bit flag which has the following semantics:
                               * 
                               * Bis 15 & 14 are used in different ways
                               * depending on the system type as OSI Adjacency
                               * or IPRA System
                               * 
                               * In case of OSI System Adjacencies, the level of
                               * the system is stored in the Bits 15 & 14
                               * 
                               * Bit  15 14 : LEVEL of the SYSTEM
                               *       0  1    LEVEL  1
                               *       1  0    LEVEL  2
                               *       1  1    LEVEL 12
                               *       
                               * In case of IPRA Adjacencies,
                               * the bits 15 & 14 are used to represent the
                               * type of IPRA Address (IPv4 or IPv6) and
                               * the contents of 'au4DirIdx' in the
                               * 'tIsisSPTNode' respectively.
                               *
                               * In case of Self IPRA Adjacencies
                               * Bit   15  : Type of IP
                               *        0     IPv4
                               *        1     IPv6
                               *
                               * Bit   14  : Content of AdjDirIdx       
                               *        0  : au4DirIdx contains AdjDirIndex
                               *        1  : au4DirIdx contains CktIndex
                               *       
                               * Bit  13   : Adjacency Type
                               *       0      IP Reachable Address
                               *       1      IS Adjacency 
                               *       
                               * Bit  12   : Attached Status 
                               *       0      Not Attached
                               *       1      Attached
                               *
                               * Bit  11   : Flag for Deletion in
                               *             the previous path
                               *       0      To be deleted
                               *       1      Not to be deleted
                               * 
                               * Bit  10   : Metric Type 
                               *       0        Internal 
                               *       1        External
                               *
                               * Bits 9-0  : Metric Value
                               */
   UINT1                 au1MaxPaths[ISIS_NUM_METRICS];
                              /* Bit 7  6 : used to indicate the changed type
                               *     0  0   Not Modified 
                               *     0  1   Modified
                               *     1  0   New Node
                               * The bits 7 and 6 are used for above purpose 
                               * only in the default metric case.
                               * Bits  5-0 indicates the  
                               * No of equal cost paths available ot reach
                               * the destination. This is an array since each
                               * metric can have a different number of equal
                               * cost paths.
                               */

  UINT1                   au1Ipv6DirIdx[ISIS_MAX_IPV6_ADDR_LEN];
   UINT1                 au1DestID[ISIS_MAX ((ISIS_SYS_ID_LEN +
                                                   ISIS_PNODE_ID_LEN), 
                                              (2 * ISIS_MAX_IP_ADDR_LEN))];
                              /* The Destination ID can be SysID or IP address.
                               * If it is IP Address, then the Destination ID 
                               * occupies the size of IP Address and IP Address
                               * Mask. Hence (2 * ISIS_MAX_IP_ADDR_LEN) size is
                               * allocated.
                               * If the Destination ID is the OSI System ID then
                               * size of (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN) 
                               * is allocated.
                               * ISIS_MAX (x,y) macro returns the maximum of 
                               * x and y 
                               */
  UINT1                   u1AddrType;
  UINT1                   u1UpOrDownType; /*u1UpOrDownType carries both up/down info*/
  UINT1                   u1PrefixLen;
  UINT1      u1DefAddFlag;
};

/*
 * This structure defines the Self-LSPs header information. The structure holds
 * information viz the dirty flag (indicating the modification status of the
 * buffer), LSP number and all the TLVs that will hold Self Adjacency
 * information including IPRA. A separate such LSPInfo structure is maintained
 * per Self-LSP. Self-LSP buffers are constructed from the TLVs held in these
 * buffers during MinGen and MaxGen Intervals.
 */

struct IsisLSPInfo {

   UINT1                  u1DirtyFlag;
                              /* The flag Indicating the status of LSP. 
                               * This flag is set when ever any of the
                               * information in the header including TLVs is
                               * changed. If this flag is set, Self-LSPs are
                               * constructed from scratch during MinGen
                               * intervals.
                               */
   UINT1                  u1LSPNum;
                              /* The Unique number assigned for the LSP
                               */
   UINT1                  u1SNId; 
                              /* The pseudo-node ID for the LSP
                               */ 
   UINT1                  u1NumAATLV;
                              /* Number of Area Address TLVs. Used in the
                               * calculation of the Length of the LSP.
                               */
   UINT1                  u1NumPSTLV;
                              /* Number of Protocols Supported TLVs. Used in 
                               * the calculation of the Length of the LSP.
                               */
   UINT1                  u1NumISTLV;
                              /* Number of IS TLVs. Used in the
                               * calculation of the Length of the LSP.
                               */
   UINT1                  u1NumIPIfTLV;
                              /* Number of IP Interface Address TLVs. Used in \
                               * the calculation of the Length of the LSP.
                               */
   UINT1                  u1NumIPV6IfTLV;
   UINT1                  u1NumIPRATLV;
                              /* Number of IPRA TLVs. Used in the
                               * calculation of the Length of the LSP.
                               */
   UINT1                  u1NumIPV6RATLV;
                              /* Number of IPV6RA TLVs. Used in the
                               * calculation of the Length of the LSP.
                               */
   UINT1                  au1Res[2];
   UINT2                  u2LSPLen;
                              /* Length of the LSP buffer calculated from the
                               * Number of TLVs held in the pTLV list. The 
                               * length of the LSP cannot be greater than L1 
                               * or L2 Buffer size.
                               */ 
   UINT2                  u2AuthStatus;
                              /* Status of the Authentication added to the LSP
                               * PDU. 
                               * Whenever Authentication is added to the LSP
                               * PDU, this value is set to ISIS_TRUE.
                               * This is checked while sending the LSP after
                               * MAX_GEN_TIMEOUT. If the Value is ISIS_FALSE,
                               * and if Authentication is supported by the
                               * System, then the Auth Info TLV is added to the
                               * LSP PDU.
                               */
   UINT4                  u4SeqNum;
                              /* Sequence number of the LSP, which
                               * is incremented on every transmission. This
                               * value is included in the LSP everytime this 
                               * LSP is transmitted.
                               */
   tIsisTLV               *pTLV;
                              /* List of TLVs to be included in the LSP.
                               */
   struct IsisLSPInfo    *pNext;   
                              /* Pointer to next LSP
                               */
   tRBTree                IPRATLV; /*Pointer to IPRA TLV's RBTree */
   UINT1                  u1NumMTTLV;
   UINT1                  u1NumExtISReachTLV;
   UINT1                  u1NumMTISReachTLV;
   UINT1                  u1NumExtIpReachTLV;
   UINT1                  u1NumMTIpv6ReachTLV;
   UINT1                  u1NumHostNmeTLV; /*Number of Host Name TLVs*/
   UINT1                  au1Res1[2];
};

/*
 * This structure holds the information that will be used to construct any 
 * Non-Pseudonode LSPs that the IS generates.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Self LSP is the only structure 
 * referring to this and the reference is through a pointer. 
 */

struct IsisNSNLSP {

   tIsisLSPInfo          *pZeroLSP;
                              /* Pointer to LSP 0 
                               */
   tIsisLSPInfo          *pNonZeroLSP; 
                              /* Pointer to LSPs which store IPRA, IS Adjacency
                               * information 
                               */
   UINT1                 u1CurrLSPNum;
                              /* This field holds the current LSP number to
                               * be assigned to any new Non-Pseudonode LSPs 
                               * that the IS generates. This is incremented 
                               * after every assignment.
                               */
   UINT1                 au1Rsvd[3];
                              /* Included for Alignment
                               */
};


/*
 * This structure holds the information that will be used to construct any 
 * Pseudonode LSPs that the IS generates.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Self LSP is the only structure 
 * referring to this and the reference is through a pointer. 
 */

struct IsisSNLSP {

   tIsisLSPInfo            *pZeroLSP;
                              /* Pointer to LSP 0 
                               */
   tIsisLSPInfo            *pNonZeroLSP;
                              /* Pointer to LSP which stores IS adjacency 
                               * information
                               */ 
   tIsisCktEntry           *pCktEntry;
                              /* Pointer to Circuit for which the Pseudonode 
                               * LSP is constructed. Pseudonodes are required
                               * only for Broadcast circuits.
                               */
   struct IsisSNLSP       *pNext;
                              /* Since there can be more than one Broadcast
                               * circuit, there can be more than onePseudonode
                               * LSP. This field holds a pointer to next 
                               * Pseudonode LSP
                               */
   UINT1                   u1CurrLSPNum;
                              /* This field holds the current LSP number to
                               * be assigned to any new Pseudonode LSPs that 
                               * the IS generates. This is incremented after 
                               * every assignment.
                               */
   UINT1               au1Rsvd[3];
                              /* Included for Alignment
                               */
};

/*
 * This structure defines the Self-LSP header. This structure holds the LSP
 * flags and pointers to all Pseudonode and Non-Pseudonode LSPs that are
 * generated by the IS
 */

struct IsisSelfLSP {

   UINT1         u1L1LSPFlags;
                              /* The Level1 LSP flags, 
                               * Bit8     Set if partition repair is supported
                               * Bit7 - 4 Indicates issuing IS is attached
                               *          to other areas using atleast one of
                               *          the metrics
                               * Bit3     Set if the LSP data base is 
                               *          overloaded
                               * Bit2 - 1 Indiates the type of System 
                               *          (L1 of L2)
                               */ 
   UINT1         au1L1MTLspFlag [ISIS_MAX_MT]; /* This flag will be set for setting 
                                                  Attach bit & overload bit in L1 MT TLVs
                                                   other than MT ID 0*/
   UINT1         au1L2MTLspFlag [ISIS_MAX_MT]; /* This flag will be set for setting 
                                                  Attach bit & overload bit in L2 MT TLVs
                                                   other than MT ID 0*/
   UINT1         u1L2LSPFlags;
                              /* The Level2 LSP flags, 
                               * Bit8     Set if partition repair is supported
                               * Bit7 - 4 Indicates issuing IS is attached
                               *          to other areas using atleast one of
                               *          the metrics
                               * Bit3     Set if the LSP data base is 
                               *          overloaded
                               * Bit2 - 1 Indiates the type of System 
                               *          (L1 of L2)
                               */ 
   UINT1         u1L1SAFlag; 
                              /* The flag indicating the change of status in
                               * L1 summary address record, if set LSPs are
                               * regenerated accordingly
                               */
   UINT1         u1L2SAFlag; 
                              /* The flag indicating the change of status in
                               * L2 summary address record, if set LSPs are
                               * regenerated accordingly
                               */
   tIsisNSNLSP   *pL1NSNLSP;
                              /* Pointer to the List of L1 non-pseudo node LSPs
                               */
   tIsisNSNLSP   *pL2NSNLSP;
                              /* Pointer to the List of L2 non-pseudo node LSPs
                               */
   tIsisSNLSP    *pL1SNLSP;
                              /* Pointer to the List of L1 pseudo node LSPs
                               */
   tIsisSNLSP    *pL2SNLSP;
                              /* Pointer to the List of L2 pseudo node LSPs
                               */
};

/*
 * This structure defines a Multi-Data Type, which can hold different types of
 * data. This is used by the update module to update Self-LSPs whenever local
 * information (Adjacency or System information) gets modified.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. This structure is used only for
 * passing arguments and hence does not cause alignment problems.
 */

struct IsisMDT {
   tIsisAreaAddr    AreaAddress;
                              /* Area Address 
                               */
   tIsisIpAddr      IPAddr;
                              /* IP Reachable Address
                               */

   tIsisMetric      Metric;   

   UINT4                  u4FullMetric;
                              /* The wide metric value for
                               * this MDT. This value is set
                               * only when multi-topology is enabled */

   UINT1            au1AdjSysID [ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];       
                              /* Adjacent IS system Id. This field along with
                               * the Metric field is used for specifying IS
                               * neighbour information
                               */
   UINT1  au1Rsvd1[1];
   UINT2           u2MTId; /* Multi-topology ID */

   UINT1            u1Cmd;
                              /* This field specifies the type of action to be
                               * performed. Possible commands are ADD, MODIFY
                               * or DELETE
                               */
   UINT1            u1LSPType;
                              /* The type of LSP, possible values are L1 
                               * (pseudonode/nonpseudonode), L2
                               * (pseudonode/nonpseudonode)
                               */
   UINT1            u1TLVType;
                              /* Type of TLV, possible values 
                               * Area Address TLV, IPRA TLV, IS Nbr TLV, 
                               * Protocol Support TLV
                               */
   UINT1            u1ProtSupp;
                              /* Protocol Supported
                               */ 
   UINT1           u1CtrlOctet;
                             /* To identify the metric in case of IPV6
                              * support
                              * Bit 0 - Internal/External metric type
                              * 1 - External type
                              * 0 - Internal type
                              * Bit 1 - UP/DOWN type
                              * 1 - DOWN type
                              * 0 - UP type
                              */
   UINT1            u1SrcProtoId;  /* This is used to Fill IPReachable TLV 
                               * from which protocol redistributed into ISIS */
   UINT1            au1IsisHstNme[ISIS_HSTNME_LEN];
UINT1            u1IsisEnabled;
   UINT1           u1L1toL2Flag;/*Flag to indicate that a Route is summarized from L1 to L2*/ 
};

/*
 * This Structure holds supported protocol information
 */

struct IsisProtSupp {

   UINT1         u1ProtSupp;
                              /* The Protocol supported by the IS
                               */
   tRowStatus    u1ExistState;
                              /* Status of the entry, follows RowStatus
                               * behaviour
                               */
   UINT2         u2Rsvd;
                              /* Included for Alignment
                               */
};

/*
 * This structure holds all the system level statistics
 */

struct IsisSysStats {

   tCounter   u4SysDroppedPDUs;
                              /* No of PDUs dropped
                               */ 
   tCounter   u4SysAreaMismatches;
                              /* No. of times  PDUs are received 
                               * with wrong/different area address not
                               * recognized by this system
                               */
   tCounter   u4SysMaxAAMismatches;
                              /* No. of times Max Area Addresses mismatch
                               * occured
                               */                          
   tCounter  u4SysMAADropFromAreas;                              
                              /* No. of times the MAA is Drop from Areas
                               */
};                            

struct IsisSysLevelStats {

   tCounter   u4SysCorrLSPs;  
                              /* No. of corrupted LSPs detected   
                               */
   tCounter   u4SysLSPDBOLs;
                              /* No. of times the L1 LSP database has
                               * become overloaded
                               */
   tCounter   u4SysAttemptToExcdMaxSeqNo;
                              /* No. of times the IS has
                               * attempted to exceed maximum sequence number
                               */
   tCounter   u4SysSeqNumSkips;
                              /* No. of times a sequence number   
                               * skip has occured
                               */
   tCounter   u4SysOwnLSPPurges;
                              /* No. of times a zero-aged copy of
                               * the systems own LSP is received from
                               * some other IS      
                               */
   tCounter   u4SysIDFieldLenMismatches;
                              /* No. of times System ID length mismatch occured
                               */
   tCounter   u4SysAuthFails;   
                              /* No of times a PDU authentication failed
                               */
   tCounter   u4SysAuthTypeFails;   
                              /* No of times a PDU authentication type failed
                               */
   tCounter   u4SysPartChanges;
                              /* No. of Partition changes occured. This field is
                               * not currently used since the implementation
                               * does not support Partition Repair.
                               */
};                            

/*
 * This structure defines the Event record. The record holds information
 * regarding an Event in a Concatenated string form. The following is one such
 * event which will be held in pu1EventData field:
 *
 * "Event: DUP_SYS_DET; Cause: DUP_SYS_ID; SysId: 0A0B0C;" 
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. Event table is the only structure 
 * referring to this and the reference is through a pointer. 
 */ 

struct IsisEvent{

     struct IsisEvent *pNext;
                              /* The pointer to the Next Event 
                               */ 
     CHAR              au1EventData[ISIS_MAX_LOG_STR_SIZE];
                              /* Concatenated string containing the 
                               * information necessary for the particular 
                               * event 
                               */  
     UINT1             u1Event;
                              /* Defines the type of Event
                               */
     UINT1             u1EventIdx;
                              /* An unique value to identify the row. 
                               */ 

#if ((ISIS_MAX_LOG_STR_SIZE + 2) % 4)    
   UINT1     au1Rsvd[(4 - ((ISIS_MAX_LOG_STR_SIZE + 2) % 4))];
                              /* Included for alignment
                               */
#endif    
};

/*
 * This structure holds all the events posted by various modules during
 * protocol operations. 
 */

struct IsisEventTable {

   tIsisEvent          *pEvent;
                              /* the pointer to the event entries */
   
   tIsisEvent          *pLastEvtEntry; 
                                /* Pointer to the last entry in the list*/
   
   UINT1               u1NextEvtIdx;
                              /* The Next available Event index.
                               */
   UINT1               au1Rsvd[3];
                              /* Included for Alignment
                               */
};

/*
 * This structure defines the Adjacency Neigbor information required by 
 * decision module to construct route entries. 
 */

 struct IsisAdjDelDirEntry {
   struct IsisAdjDelDirEntry    *pNext;
                              /* Pointer to the next entry
                               *
                               */

   UINT1                      au1AdjNbrIpV4Addr[ISIS_MAX_IPV4_ADDR_LEN]; 
   UINT1                      au1AdjNbrIpV6Addr[ISIS_MAX_IPV6_ADDR_LEN]; 

   UINT4                      u4DirIdx;
                              /* An unique direction index
                               */
   UINT1                      u1Status;
                             /* Status indicating whether this can be deleted 
                     * or not */
   UINT1                      u1Count;
                             /* The number os route entries added in this
                              * direction
                              */
   UINT1                      u1Level;
                             /* The adjacency usage associated 
                              * with this recore
                              */

   UINT1                      u1Rsvd;
                              /* The reserved filed
                               */

};
/*
 * This structure defines the Adjacency direction information required by 
 * decision module to construct route entries. 
 */

 struct IsisAdjDirEntry {

   UINT4                      u4DirIdx;
                              /* An unique direction index
                               */
   tIsisAdjEntry              *pAdjEntry;
                              /* Pointer to Adjacency Entry
                               */
   struct IsisAdjDirEntry    *pNext;
                              /* Pointer to the next entry
                               *
                               */
};
/*
 * This structure defines the Adjacency Direction table. For every adjacency 
 * established by the IS, one entry will be added to the Direction Table. 
 * Direction index will be maintained in the SPT nodes during SPF calculation 
 * instead of CircuitIndex and SNPA. Both CircuitIndex and SNPA can be obtained
 * from Direction Table.
 */

 struct IsisAdjDirTable {

   UINT4                      u4NumEntries;
                              /* Number of valid entries in the table
                               */
   struct IsisAdjDirEntry    *pDirEntry;
                              /* Pointer to Adjacency Direction Entry
                               */
};

/*
 * This structure holds all the identifiers either configured or returned by
 * external modules (during registration) which will be used by the IS for any 
 * subsequent interactions with the modules.
 */

/*
 *  This structure defines the Equivalence Class (EC) Timer information. This
 *  structure holds all the 3 blocks of EC timers which runs timers of different
 *  granularities.
 */

struct IsisTCInfo {

   UINT4           u4TCount;
                              /* Timer count used for processing the timer
                               * records. EC1 timers are of one second
                               * granularity, EC2 of 20 secs and EC3 of 100 secs
                               * granularity. This field is incremented for 
                               * every EC timer expiry (which is a single timer 
                               * ticking continuously). if u4TCount % 20 or 
                               * u4TCount % 100 becomes zero the records in the
                               * EC2 and EC3 blocks will also be processed.
                               */ 

  tIsisTmrECBlk   *apTmrEC1 [ISIS_MAX_EC1_BLKS];
                              /* EC1 block of size ISIS_MAX_EC1_BLKS. Each
                               * index of the array holds various records which
                               * will be processed on timer expiries, if the 
                               * entry happens to be the u1EC1StartIdx.
                               */
   tIsisTmrECBlk   *apTmrEC2 [ISIS_MAX_EC2_BLKS];
                              /* EC2 block of size ISIS_MAX_EC2_BLKS. Each
                               * index of the array holds various records which
                               * will be processed on timer expiries, if the 
                               * eentry happens to be the u1EC2StartIdx.
                               */
   tIsisTmrECBlk   *apTmrEC3 [ISIS_MAX_EC3_BLKS];
                              /* EC3 block of size ISIS_MAX_EC3_BLKS. Each
                               * index of the array holds various records which
                               * will be processed on timer expiries, if the 
                               * entry happens to be the u1EC3StartIdx.
                               */
   UINT1           u1EC1StartIdx; 
                              /* The index for the first equivalence class 
                               * timer. On exipry of the timer, the records held
                               * at this index in EC1 block will be processed 
                               * and the field is incremented by one 
                               * (modulo size of EC1 Block).
                               */
   UINT1           u1EC2StartIdx; 
                              /* The index for the second equivalence class 
                               * timer. On exipry of the timer, if u4TCount % 20
                               * becomes zero, the records held at this index 
                               * in EC2 block will be processed and
                               * the field is incremented by one (modulo size of
                               * EC2 Block).
                               */
   UINT1           u1EC3StartIdx; 
                              /* The index for the third equivalence class 
                               * timer. On exipry of the timer,if u4TCount % 100
                               * becomes zero,  the records held at this index 
                               * in EC3 block will be processed and
                               * the field is incremented by one (modulo size of
                               * EC3 Block).
                               */
   UINT1           u1Rsvd;
                              /* Included for Alignment
                               */ 
 
};

/* 
 * This structure holds all the system timers which are not level specific. This
 * includes all the global timers which are not associated with any particular
 * database records and are activated globally per instance, and the Equivalence
 * Class Timers.
 */

struct IsisSysTimers {

   tIsisTimer               SysECTimer;
                              /* Equivalence Class timer - Global timer for
                               * all Equivalence Classes which ticks
                               * continuously every second. This tick is
                               * utilised for processing EC1, EC2 and EC3
                               * blocks.
                               */
   tIsisTimer               SysISSeqWrapTimer;
                              /* Timer started when Max Sequence number used
                               * for Self LSP generation wraps around.
                               */
   tIsisTimer               SysDbaseNormTimer;
                              /* Timer used for Normalization of LSP database.
                               * Normalization is a process of distributing
                               * the nodes evenly across all buckets in a hash
                               * table. This is done periodically to ensure
                               * insertions of nodes into database does not
                               * result in single long linear lists.
                               */ 
   tIsisTimer               SysSPFSchTimer;
                              /* The SPF Schedule timer. After every expiry of
                               * this timer the trigger counts are verified
                               * against the thresholds, and if required the
                               * SPF is scheduled.
                               */
   tIsisTCInfo              TmrECInfo;
                              /* Includes Equivalence Class timer
                               * information. Any Non-Global timer started
                               * will have a record inserted in one of the
                               * Equivalence Classes maintained in either
                               * EC1, EC2 or EC3 blocks.
                               */ 
   tIsisTimer               SysRestartT3Timer;

};

/* 
 * This structure maintains all level specific information.
 */

struct IsisSysLevelInfo {

   tRBTree                  PrevShortPath [ISIS_MAX_MT];
                              /* The Shortest Path Database calculated in the
                               * previous run. This database will be compared
                               * with the database calculated in the current
                               * run and routes will be updated
                               * incrementally.
                               *
                               * In case of Single Topology:
                               * PrevShortPath [0] is used.
                               *
                               * In case of Multi-topology:
                               * PrevShortPath [0] - MT#0 (IPv4 Unicast)
                               * PrevShortPath [1] - MT#2 (IPv6 Unicast)
                               *
                               * This can be further extended to 
                               * multiple topologies.
                               */
   tRBTree                  ShortPath [ISIS_MAX_MT];
                              /* Shortest Path database computed in the
                               * current run.
                               *
                               * In case of Single Topology:
                               * ShortPath [0] is used.
                               *
                               * In case of Multi-topology:
                               * ShortPath [0] - MT#0 (IPv4 Unicast)
                               * ShortPath [1] - MT#2 (IPv6 Unicast)
                               *
                               * This can be further extended to
                               * multiple topologies.
                               */
   tIsisTimer               SysLSPGenTimer;
                              /* Everytime this timer fires, the Self-LSP 
                               * database is verified for changes. If the
                               * local information has changed indicated by
                               * the dirty flag, then those LSPs will be
                               * propogated to the peer nodes and
                               * u1LSPGenCount incremented. If u1LSPGenCount
                               * reaches ISIS_MAX_LSP_GEN_COUNT then all
                               * Self-LSPs are propogated irrespective of the
                               * dirty flag
                               */
   tIsisTimer               SysWaitingTimer;
                              /* Timer started once the database is 
                               * overloaded. No LSPs will be processedor as 
                               * long as this timer is active.
                               */
   tIsisSortedHashTable     LspTxQ;
                              /* The List of LSPs which are marked for 
                               * transmission. LspTxQ holds LSPs along with
                               * the SRM flags which is used for handling
                               * acknowledgements.
                               */
   tIsisSortedHashTable     LspDB;
                              /* The sorted LSP database. The LSP database
                               * does not include SRM flags since these are
                               * required only for acknowledgements and are
                               * included in the LspTxQ
                               */

   /* ISIS Graceful Restart*/
   tIsisTimer              SysRestartT2Timer;
   tRBTree                 pGrCSNPTbl;
   tTMO_SLL          AckLst;
                         /* List holding the Ack rcvd info */
   tTMO_SLL          CSNPLst;
                         /* List holding the CSNP rcvd info */

   UINT2                    u2TrgCount;
                              /* Count updated by the Update module whenever  
                               * there is a change in the LSP Databse. Upon SPF 
                               * timer expiry the decision module checks this
                               * field and decide to run the SPF algorithm
                               */
  UINT1                    u1LSPGenCount;
                              /* Number of times the minimum LSP Generation
                               * timer expired. This is reset when it reaches 
                               * MAX_LSP_GEN_COUNT. This count helps to avoid
                               * an extra timer for MAX_LSP_GEN_INTERVAL. The
                               * MAX_LSP_GEN_INTERVAL is divided into equal
                               * sized MIN_LSP_GEN_INTERVALS and when the
                               * minimum timer fires for MAX_LSP_GEN_COUNT
                               * times, actions as per MAX_LSP_GEN_TIMER will 
                               * be initiated.
                               */ 
   UINT1                    u1SpfTrgCount;
                              /* This field indicates how many times SPF 
                               * was triggered since the last time Decision
                               * process was run. If u2TrgCount is within
                               * the range as specified by 
                               * interval[MinTH, MaxTH] which is
                               * configurable, SPF is scheduled. The SPF is
                               * deferred is otherwise. This field is reset
                               * after every SPF run.
                               */

   UINT1                        u1IsisDbFullState;
   UINT1                        au1Rsvd[3];
                            
};


typedef struct _LspInfo {

 tRBNodeEmbd         RbNode;

 UINT4                   u4SeqNum;
            /* Sequence Number */

 UINT1                   au1LSPId[ISIS_LSPID_LEN];
                              /* LSP Identifier, which is a concatenation of System Id, pseudonode Id and LSP Number  */


 UINT2                   u2RLTime;
                              /* Remaining Life time */

 UINT2                   u2CheckSum;
                              /* Check sum value of the corresponding LSP */
} tLspInfo;


/*The structure holds the most recent notification trap fields for this 
 * instance of ISIS protocol*/
 
struct IsisNotifyTable {

  UINT1                   au1TrapLSPId[ISIS_LSPID_LEN];
                              /* Trap LSP Identifier
                               */ 
  UINT1                   au1ProtSupp [ISIS_MAX_ADJ_PROT_SUPP];
                              /* The List of protocols supported by the 
                               * adjacent system. This may be empty
                               */
  UINT1                   au1IsisPduFrag[ISIS_TRAP_PDU_FRAG_SIZE];
                              /* Holds up to the first 64 bytes of PDU 
                               * that triggered the notification
                               */

   UINT2                   u2LSPSize;
                              /* The size of the LSP received that was 
                               * too big to forward
                               */
  UINT2                   u2BuffSize;
                              /* The size of the Buffer advertised by the peer
                               * in TLV
                               */
    
  UINT1                   u1SysLevel;
                              /* Identification of the Level of notification
                               */
  UINT1                   u1FieldLen;
                              /* The System ID Length reported in PDU received
                               */
  UINT1                   u1MaxAreaAddr;
                              /* The Maximum Area Address received in the PDU
                               */
  UINT1                   u1ProtVersion;            
                              /* The Protocol version received in the PDU
                               */
#if ((ISIS_LSPID_LEN + ISIS_MAX_ADJ_PROT_SUPP) % 4)
  UINT1                   au1Rsvd[(4 - (4 % (ISIS_LSPID_LEN +
                                             ISIS_MAX_ADJ_PROT_SUPP)))];
#endif

};

/*
 * This Structure holds all the necessary information pertaining to a specific 
 * instance of the IS. Each instance can be configured with a different set of
 * parameters.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. 
 */

struct IsisSysContext {                      
   tRBTree                  Tent;
                              /* Temporary set of SPT nodes which are
                               * used by the Decision to calculate 
                               * Shortest Path Database
                               */ 
   tRBTree                  OSIPath;
                              /* Temporary set of SPT nodes which are
                               * used for storing IS Adjacency systems
                               * by the Decision to calculate Shortest
                               * Path Database. All the nodes in this
                               * database are freed after the SPF run
                               */
   tIsisSPTNode             *pL1DelNodes;
                              /* The List of nodes which are to be deleted
                               * from Path Database after scheduling L1
                               * SPF. This is used by L2 update process to
                               * delete the corresponding entries in the Self
                               * LSPs.
                               */ 
   tIsisLSPTxEntry          *pL1PurgeQueue;                           
                              /* The Queue of the L1 LSPs to be purged from the
                               * entire network
                               */ 
   tIsisLSPTxEntry          *pL2PurgeQueue;                           
                              /* The Queue of the L2 LSPs to be purged from the
                               * entire network
                               */ 
   struct IsisSysContext   *pNext;
                              /* Pointer to next System context
                               */
   tIsisSPTNode             *pL2DelNodes;
                              /* The List of nodes which are to be deleted
                               * from Path Database after scheduling L2
                               * SPF. This is used by L1 update process to
                               * delete the corresponding entries in the Self
                               * LSPs.
                               */
   tIsisLLData              *pLLDataNode;
   tIsisAdjDelDirEntry      *pDelDirEntry;
  tIsisIPRAEntry           *pLastIPRAEntry;
                                 /* SNMP improvement:
                                  * Cache-poiter for table isisIPRATable.
                                  */


#ifdef ROUTEMAP_WANTED
   tFilteringRMap* pDistributeInFilterRMap;     /* for Distribute in filtering feature */
   tFilteringRMap* pDistanceFilterRMap;         /* for Distance filtering feature */
#endif /* ROUTEMAP_WANTED */

   tIsisSysTimers           SysTimers;
                              /* Holds all the System specific timers
                               */ 
   tIsisSysLevelInfo        SysL1Info;
                              /* Holds all Level 1 specific information
                               */ 
   tIsisSysLevelInfo        SysL2Info;
                              /* Holds all Level 2 specific information
                               */ 
   tIsisIpAddr              SysRouterID;
                              /* The Router ID which will be used for
                               * Unnumbered interfaces. Hellos sent on
                               * unnumbered interfaces will send the Router Id
                               * in the IP Interface Address TLV.
                               */ 
   tIsisSysActuals          SysActuals;
                              /* This structure holds all the System
                               * parameters that are currently used bythe IS
                               * for protocol operations.
                               */
   tIsisSysConfigs          SysConfigs;
                              /* This structure holds all the configured
                               * parameters. These parameters can be
                               * dynamically changed by the manager but will
                               * be effective only the next time the IS is
                               * booted up. At bootup time all the objects
                               * from this structure will be copied into the
                               * SysActuals and the protocol initiated.
                               */
   tIsisSysStats            SysStats;
                              /* Holds System statistics 
                               */
   tIsisSysLevelStats       SysL1Stats;
                              /* Holds System Level 1 statistics 
                               */
   tIsisSysLevelStats       SysL2Stats;
                              /* Holds System Level 2 statistics 
                               */
   tIsisAdjDirTable         AdjDirTable;
                              /* Holds the Adjacency Direction information
                               */
   tIsisAATable             AreaAddrTable;
                              /* Holds all the Area Addresses heard in the
                               * Level1 LSPs
                               */
   tIsisSATable             SummAddrTable;
                              /* Holds all the IP Summary Addresses configured
                               * by the manager
                               */
   tIsisMAATable            ManAddrTable;
                              /* Manual Area Addresses configured by the 
                               * manager
                               */
   tIsisEventTable          EventTable;
                              /* Holds all the Events logged by various
                               * modules during protocol operations
                               */
   tIsisSelfLSP             SelfLSP;
                              /* Holds information regarding all the LSPs
                               * generated by the local system
                               */
   tIsisCktTable            CktTable;
                              /* Holds information regarding all the circuits
                               * configured by the manager
                               */
   tIsisIPIfAddrTable       IPIfTable;
                              /* Holds information regarding all the IP
                               * addresses configured for various interfaces
                               * by the manager
                               */ 
   tIsisIPRAInfo            IPRAInfo;
                              /* List of all IP Reachable addresses configured
                               * for this Instance 
                               */
   tIsisNotifyTable           TrapNotifyTable;
                              /* Trap Notification table 
                               */
   tIsisProtSupp            aProtSupp [ISIS_MAX_PROTS_SUPP];
                              /* Holds all the protocols supported by the
                               * local IS
                               */
   tIsisRRDConfig           RRDInfo;
   tRBTree                  RRDRouteRBTree; /*RBTree to store Redistributed routes*/
   tRBTree          HostNmeList; /*List to hold the learnt dynamic host names*/
   UINT4            u4HostNmeTotEntries;/*Total Number of dynamic host names learnt*/
   tIsisSPTNode            *pDelNL2Nodes[ISIS_MAX_MT]; /* Nodes (per MT basis) corresponding to the nearest L2 systems
                            * that were computed during the previous SPF
                            * iterarion, and are required to be deleted
                            * at the end of the current iteration of SPF
                            */
   tIsisSPTNode            *pAddNL2Nodes[ISIS_MAX_MT]; /* Nodes (per MT basis) corresponding to the neatest L2 systems
                            * that are computed during the current iteration of SPF
                            */

   UINT4                    u4MaxNodes;
   UINT4                    u4SysInstIdx;
                              /* An unique index of the Instance
                               */
   UINT2                        u2IsisGRT2TimerL1Interval;
   UINT2                        u2IsisGRT2TimerL2Interval;
   UINT2                        u2IsisGRT3TimerInterval;
   UINT2                        u2IsisGRHelperTimeLimit;

   UINT1                    u1QFlag;
                              /* Quorum Flag used to check whether the mandatory
                               * parameter are set or not 
                               * bit1 is for System ID
                               * bit2 is for System Type
                               */
   UINT1                    u1OperState;
                              /* The operational state of the Instance
                               */ 

   UINT1           u1Distance;

  /*ISIS GR*/
   UINT1                        u1IsisGRRestartSupport;
   UINT1                        u1IsisGRRestartStatus;
   UINT1                        u1IsisGRRestartReason;
   UINT1                        u1IsisGRT1TimerInterval;
   UINT1                        u1IsisGRMaxT1RetryCount;
   UINT1                        u1IsisGRRestarterState;
   UINT1                        u1IsisGRRestartMode;
   UINT1                        u1IsisGRRestartExitReason;
   UINT1                        u1IsisGRHelperSupport;
   UINT1                        u1IsisGRShutState;
   UINT1                        u1IsisMTSupport;
   UINT1                        u1IsisBfdSupport;     /* Indicates whether BFD support 
                                                         for ISIS is enabled or disabled. */
   UINT1                        u1IsisBfdAllIfStatus; /* Indicates whether BFD is enabled*/
   UINT1                        u1MetricStyle;        /* Narrow style Metrics or wide metrics.*/

   tBool            bNetIdDotCompliance;
   UINT1            u1IsisDynHostNmeSupport;/* Indicates whether Dynamic hostname support is enabled*/
   UINT1            au1Pad[1];    /* Needed for 4-byte alignment */
};


/*
 * This structure defines the format of messages exchanged between various
 * modules of ISIS.
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. This structure is used for passing
 * messages across modules.
 */

struct IsisMsg {

   UINT4         u4CxtOrIfindex;
                              /* This field is used for passing the
                               * context information between modules. The
                               * usage may change depending upon the modules
                               * that are communicating. Lower Layer Modules
                               * (Data Link Layer Interface modules) pass
                               * CircuitIfIndex while transferring packets
                               * received over the physical networks. ISIS
                               * modules pass Circuit context (Circuit Index)
                               * while communicating with each other.
                               */
   UINT4         u4IfSubIdx;
                              /* Includes (VPI, VCI) in the case of ATM, or DLCI
                               * in the case of FR networks. Not used for
                               * Ethernet or PPP links. This field is also used
                               * or Indicating NODE ID in the case of Node
                               * Status indications
                               */ 
   UINT4         u4Length;
                              /* Length of the received/transmitted message
                               */
   UINT1         *pu1Msg;    
                              /* Pointer to actual Message 
                               */
   INT4          (*pMemFreeRoutine) (VOID *);                           
                              /* The routine that frees the memory received from
                               * Lower Layer Modules. 
                               */ 
   UINT1         au1PeerSNPAAddr[ISIS_SNPA_ADDR_LEN];
                              /* The MAC address of the peer which originated
                               * the message
                               */
   UINT1         u1LenOrStat;
                              /* Indicates the length of the SNP Address for
                               * packets delivered from lower layer modules.
                               * This field holds the Status of the Interface if
                               * u1MsgType is ISIS_MSG_IF_STAT_IND or
                               */ 
   UINT1         u1MsgType;
                              /* Type of Message Received/Transmitted. The
                               * Control Module multiplexes the received
                               * messages based on this field
                               */
   UINT1         u1DataType;
                              /* Indicates the structure of the data held in the
                               * pu1Msg field. It can take the following values:
                               *
                               * ISIS_DATA_NORMAL - means that the data held in
                               * the pu1Msg field does not include any DLL
                               * Headers including FE FE 03
                               *
                               * ISIS_DATA_RAW - means that the data held in 
                               * pu1Msg field includes DLL Headers including FE
                               * FE 03.
                               */ 
   UINT1         u1RmEvent;   /* RM Message Type */
#if ((ISIS_SNPA_ADDR_LEN) % 4)    
   UINT1     au1Rsvd[(4 - ((ISIS_SNPA_ADDR_LEN) % 4))];
                              /* Included for alignment
                               */
#endif    
};

/*
 * This structure defines the route information used for updating the routing
 * table. RTMI module parses this structure and formats a message as required by
 * the Route Table Manager and updates the routing table based on the
 * information passed in this structure
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. This structure is used for updating
 * routes to RTM.
 */

struct IsisRouteInfo {

   tIsisIpAddr       DestIpAddr;       
                              /* The IP address of the destination
                               */
   UINT4             u4CktIfIndex;
                              /* The circuit interface index over which the
                               * corresponding destination can be reached.
                               * This is the L3 port number.
                               */
   UINT4             u4MetricVal;
   UINT1             au1IPAddr[ISIS_MAX_IP_ADDR_LEN];
                              /* The next hop IP Address 
                               */
   UINT1             u1MetricType;
                              /* The type of metric, possible values
                               * 1 - Default Metric
                               * 2 - Delay Metric
                               * 3 - Error Metric
                               * 4 - Expense Metric
                               */
   UINT1             u1BitMask;
                              /* BitMask to be sent to the RTM
                               * to indicate the change in the Route parameter
                               */
   UINT1             u1Level;
   UINT1  au1Rsvd[1]; 
};

/*
 * This structure defines structure of the nodes held in the Timer Equivalence
 * Classes. Each of the EC timers will have a list of such nodes which will be
 * processed upon timer expiry. The timer expiry action depends upon the u1Type
 * field. pInfo points to the atcual data that should be processed during timer
 * expiries
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. However the
 * fields inside this structure are aligned. IsisTCInfo is the 
 * only structure referring to this and the reference is through a pointer. 
 */
 
struct IsisTmrECBlk {
 
   VOID                    *pInfo;
                              /* Pointer to actual information that should be
                               * processed
                               */
   struct IsisTmrECBlk    *pNext;
                              /* Pointer to the next Block
                               */
   UINT2                   u2RemTime;
                               /* The Time for which the timer block should be
                                * restarted after the expiry.
                                */

   UINT1                   u1Type;
                              /* Type of the timer block. This decides the type
                               * of processing upon timer expiry
                               */
   UINT1                   u1Rsvd;
                               /* Included for alignment
                                */
};


/* This structure forms the header for LSP related information held in the
 * Equivalence Class timer blocks. Whenever an LSP is transmitted its Remaining
 * Life Time must be decremented by the amount of time it was held in the
 * database. The pNxtTimerLink field holds the list of all LSPs which belong to
 * a particular timer class. To find the remaining life time for a particular
 * LSP, the pPrevTimerLink is traversed till the header block is reached.
 * The u4TStamp holds the time at which the list had been created. The
 * difference between the global timer counter and the u4TStamp will specify the
 * amount of time elapsed since the LSP was inserted into the list. This amount
 * of time can be decremented before transmitting the LSP. 
 * 
 * CAUTION :- The pNxtTimerLink and pPrevTimerLink field's offsets must be
 *            exactly similar to the offsets of these fields in tIsisLSPEntry
 */ 

typedef struct _IsisTmrHdr 
{

   UINT4            u4TStamp;
                              /* The time at which the LSP chain is formed
                               */ 
   tIsisLSPEntry    *pNxtTimerLink; 
                              /* This link points to the first LSP on the chain
                               */ 
   tIsisLSPEntry    *pPrevTimerLink;
                              /* Will be NULL since this is the header and there
                               * are no nodes before this. While traversing back
                               * the pPrevTimerLink from any LSP, this is the
                               * only way to find that the header is reached.
                               */ 

} tIsisTmrHdr;

/* CSNP spoecific timer structure. when a timer expires */
typedef struct _IsisTmrCSNPHdr
{
   UINT4            u4CktIdx; /* Thg Index to the circuit record over which the
                               * CSNPs are transmitted
                               */ 
   UINT1            au1LastTxId [ISIS_LSPID_LEN];
                              /* Identifier which will be used to retrieve 
                               * an appropriate LSP from both TxQ and DB
                               * during CSNP Time out. Construction of CSNP
                               * will start from these retrieved LSPs. 
                               *
                               * NOTE: This Field stores the Minimum Identifier 
                               * of the Last Processed LSPs from both Tx and DB
                               */
} tIsisTmrCSNPHdr;   

/* This structure defines the header used by the Fault Tolerance Interface
 * Module for constructing LSUs and Bulk Updates which are used for updating the
 * Standby node during Fault Tolerance updates
 */

struct IsisFLTHdr {
  
  UINT1                  u1Command;
                              /* Command indicates the type of action
                               * possible values are ADD, MODIFY and DELETE
                               */ 
  UINT1                  u1Type;
                              /* Type of Database being updated
                               */ 
  UINT2                  u2Length;
                              /* Length of the total message excluding the 
                               * Header
                               */ 
  UINT1                  u1DataFormat;
                              /* Indicates whether the update is a full update
                               * or a partial update.
                               */ 
  UINT1                  u1EntryCount;
                              /* Indictaes how many entries are included in the
                               * current buffer.
                               */ 
  UINT2                  u2Rsvd;
                              /* Included for Alignment. Set to 0 on Transmit 
                               * and ignored on receipt.
                               */ 
};

                              
struct IsisExtInfo {

#ifdef ISIS_FT_ENABLED

  UINT4                u4PeerCount;
                              /* No of peers attached to this active node */

  UINT4                u4FTSelfNodeID;     
                              /* The node identifier of the self, required by
                               * FTM
                               */
  UINT4                u4FTPeerNodeID;     
                              /* The node identifier of the Peer node, 
                               * required by FTM
                               */
  UINT4         u4DynBulkUpdatStatus;
                              /*Shows the bulk update status based on 
    * which dynamic updates are initiated*/
  UINT2                u2FTBufSize;
                              /* The maximum buffer size supported by the Fault
                               * Tolerance Module. Fault Tolerance
                               * packets cannot be greater than this size.
                               */ 
  UINT2                u2ModuleID;       
                              /* The ISIS Module identifier configured by
                               * manager
                               */
  UINT2                u2ProtocolID;
                              /* The routing protocol ID as defined by IANA
                               */ 
  UINT1                u1FTState; 
                              /* Indicates whethet the Node is ACTIVE, STANDBY,
                               * or Out Of Service
                               */
  UINT1                u1FTStatus;
                              /* Indicates the status of the Node whether it is
                               * Enabled or Disabled
                               */
#endif

  UINT2                u2RtmRegID;                             
                              /* The registration identifier given by the  
                               * RTM Module
                               */
  UINT2                u2Rtm6RegID;
  UINT1                u1NodeType;
                              /* The type of Node, used in the distributed 
                               * environment. Indicates ISIS_CTRL_NODE, if
                               * only Control plane is enabled, ISIS_DATA_NODE
                               * if only Data plane is enabled, 
                               * ISIS_CTRL_DATA_NODE if both Control and Data
                               * planes are enabled.
                               */ 

  UINT1                u1Rsvd[3];
                              /* Used for alignment
                               */
};

struct IsisLSPTxTable{

   tIsisLSPTxEntry      *pLSPTxRec;
                              /* Pointer to the first entry in the table
                               */
   UINT2                u2NumEntries;
                              /* Number of Entries in the table
                               */
   UINT2                u2Rsvd;
                              /* Included for alignment
                               */
};

/* The header includes information specific to the Link Layers.  u4SubIfIndex
 * will hold the VPI, VCI combination used for transmission of Data over ATM
 * networks, or it includes DLCI required for FR networks. au1DestMACAddr holds
 * the destination MAC address which is either ISIS_L1_MCAST or ISIS_L2_MCAST.
 */

struct IsisLLHdr {

   UINT4                u4SubIfIndex; 
                              /* Includes <VPI,VCI> in the case of ATM
                               * and DLCI in the case of FR
                               */ 
   UINT1                au1DestMACAddr[ISIS_SNPA_ADDR_LEN];
                              /* Includes Destination MAC address
                               */ 
   UINT1                au1SrcMACAddr[ISIS_SNPA_ADDR_LEN];
                              /* Includes Source MAC address
                               */
 
};


/*
 * This structure defines structure of the Lower Layer Interface information
 *
 * NOTE:
 * ----
 * The size of the structure is not adjusted for aligning on a 4 byte boundary
 * since this does not form a part of any other structure directly. 
 */
 
struct IsisLLInfo {
    
    UINT4                u4Mtu;
                              /* The Maximum Data Transfer uint of the Lower
                               * layer in Bytes
                               */
    UINT4                u4Speed;
                               /* The Link Speed of the Lower Layer
                                */
    UINT1                au1SNPA [ISIS_SNPA_ADDR_LEN];
    UINT1                u1IfType;
                               /* The Interface Type as registered by the
                                * Lower Layer
                                */ 
    UINT1                u1EncapType;
                               /* The type of the Encapsulation used by the
                                * Lower Layer
                                */ 
    UINT1                u1OperState;
                               /* The Operational State of the Interface
                                */
    UINT1                u1IpV6PrefixLen;
    UINT1                u1AddrType;
    UINT1                au1Rsvd1[1];
    UINT1                au1IpV6Addr[ISIS_MAX_IPV6_ADDR_LEN * ISIS_MAX_IPV6_ADDRESSES];
    UINT1                au1IpV4Addr[ISIS_MAX_IPV4_ADDR_LEN];

    UINT1                au1IpMask [ISIS_MAX_IP_ADDR_LEN];
                                /* The IP Address Mask of the Interface
                                 */ 
                                   /* The SNPA Address (Hardware MAC Address)                                  */ 
};

struct IsisLLData{
    
    UINT1                  *pu1Msg;
                              /* The pointer to the message of the LL Data 
                               */
    struct IsisLLData     *pNext;
                              /* The pointer to the next message in the list 
                               */
};

typedef struct IsisGrCheck{
    tTMO_SLL_NODE       next;
    UINT4                  u4CktId;
}tIsisGrCheck;

typedef struct IsisGrSemInfo{
  tIsisSysContext  *pContext; 
  tIsisCktEntry    *pCktRec;
  UINT2               u2RemainingTime;
  UINT1     u1Level;
  UINT1     u1PktType;
  UINT1     *pu1CSNP;
         /*UINT1               au1Rsvd[3];
                                Included for alignment
                                */

}tGrSemInfo;

/* RTM Event Structure info */
typedef struct RtmRouteDataInfo
{
    union
    {
        tNetIpv4RtInfo      RtInfo;
        tNetIpv6RtInfo      Rt6Info;
    }unRtInfo;
    UINT2               u2ProtoId;
    UINT1               au1Pad [2];
} tRtmRouteDataInfo;

typedef struct _tIsisHostNmeNodeInfo
{
    tRBNodeEmbd    HostNmeNode;
    UINT1          au1HostNme[ISIS_HSTNME_MAX_LEN + 1];
    UINT1          u1FragType;
    UINT1          au1SysID[ISIS_SYS_ID_LEN];
    UINT1          au1Pad[1];
}tIsisHostNmeNodeInfo;
 
#endif /* __ISTDFS_H__ */
