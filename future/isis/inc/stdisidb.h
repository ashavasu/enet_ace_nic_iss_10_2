/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdisidb.h,v 1.4 2014/11/11 13:01:01 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDISIDB_H
#define _STDISIDB_H

UINT1 IsisSysTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IsisManAreaAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 IsisAreaAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 IsisSysProtSuppTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 IsisSummAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 IsisSysStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 IsisCircTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IsisCircLevelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 IsisPacketCountTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 IsisISAdjTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IsisISAdjAreaAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 IsisISAdjIPAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IsisISAdjProtSuppTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 IsisIPRATableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IsisNotificationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 stdisi [] ={1,3,6,1,3,37};
tSNMP_OID_TYPE stdisiOID = {6, stdisi};


UINT4 IsisSysInstance [ ] ={1,3,6,1,3,37,1,1,1,1,1};
UINT4 IsisSysVersion [ ] ={1,3,6,1,3,37,1,1,1,1,2};
UINT4 IsisSysType [ ] ={1,3,6,1,3,37,1,1,1,1,3};
UINT4 IsisSysID [ ] ={1,3,6,1,3,37,1,1,1,1,4};
UINT4 IsisSysMaxPathSplits [ ] ={1,3,6,1,3,37,1,1,1,1,5};
UINT4 IsisSysMaxLSPGenInt [ ] ={1,3,6,1,3,37,1,1,1,1,6};
UINT4 IsisSysOrigL1LSPBuffSize [ ] ={1,3,6,1,3,37,1,1,1,1,7};
UINT4 IsisSysMaxAreaAddresses [ ] ={1,3,6,1,3,37,1,1,1,1,8};
UINT4 IsisSysMinL1LSPGenInt [ ] ={1,3,6,1,3,37,1,1,1,1,9};
UINT4 IsisSysMinL2LSPGenInt [ ] ={1,3,6,1,3,37,1,1,1,1,10};
UINT4 IsisSysPollESHelloRate [ ] ={1,3,6,1,3,37,1,1,1,1,11};
UINT4 IsisSysWaitTime [ ] ={1,3,6,1,3,37,1,1,1,1,12};
UINT4 IsisSysAdminState [ ] ={1,3,6,1,3,37,1,1,1,1,13};
UINT4 IsisSysL1State [ ] ={1,3,6,1,3,37,1,1,1,1,14};
UINT4 IsisSysOrigL2LSPBuffSize [ ] ={1,3,6,1,3,37,1,1,1,1,15};
UINT4 IsisSysL2State [ ] ={1,3,6,1,3,37,1,1,1,1,16};
UINT4 IsisSysLogAdjacencyChanges [ ] ={1,3,6,1,3,37,1,1,1,1,17};
UINT4 IsisSysMaxAreaCheck [ ] ={1,3,6,1,3,37,1,1,1,1,18};
UINT4 IsisSysNextCircIndex [ ] ={1,3,6,1,3,37,1,1,1,1,19};
UINT4 IsisSysExistState [ ] ={1,3,6,1,3,37,1,1,1,1,20};
UINT4 IsisSysL2toL1Leaking [ ] ={1,3,6,1,3,37,1,1,1,1,21};
UINT4 IsisSysSetOverload [ ] ={1,3,6,1,3,37,1,1,1,1,22};
UINT4 IsisSysL1MetricStyle [ ] ={1,3,6,1,3,37,1,1,1,1,23};
UINT4 IsisSysL1SPFConsiders [ ] ={1,3,6,1,3,37,1,1,1,1,24};
UINT4 IsisSysL2MetricStyle [ ] ={1,3,6,1,3,37,1,1,1,1,25};
UINT4 IsisSysL2SPFConsiders [ ] ={1,3,6,1,3,37,1,1,1,1,26};
UINT4 IsisSysTEEnabled [ ] ={1,3,6,1,3,37,1,1,1,1,27};
UINT4 IsisSysMaxAge [ ] ={1,3,6,1,3,37,1,1,1,1,28};
UINT4 IsisSysReceiveLSPBufferSize [ ] ={1,3,6,1,3,37,1,1,1,1,29};
UINT4 IsisManAreaAddr [ ] ={1,3,6,1,3,37,1,1,2,1,1};
UINT4 IsisManAreaAddrExistState [ ] ={1,3,6,1,3,37,1,1,2,1,2};
UINT4 IsisAreaAddr [ ] ={1,3,6,1,3,37,1,1,3,1,1};
UINT4 IsisSysProtSuppProtocol [ ] ={1,3,6,1,3,37,1,1,4,1,1};
UINT4 IsisSysProtSuppExistState [ ] ={1,3,6,1,3,37,1,1,4,1,2};
UINT4 IsisSummAddressType [ ] ={1,3,6,1,3,37,1,1,5,1,1};
UINT4 IsisSummAddress [ ] ={1,3,6,1,3,37,1,1,5,1,2};
UINT4 IsisSummAddrPrefixLen [ ] ={1,3,6,1,3,37,1,1,5,1,3};
UINT4 IsisSummAddrExistState [ ] ={1,3,6,1,3,37,1,1,5,1,4};
UINT4 IsisSummAddrAdminState [ ] ={1,3,6,1,3,37,1,1,5,1,5};
UINT4 IsisSummAddrMetric [ ] ={1,3,6,1,3,37,1,1,5,1,6};
UINT4 IsisSysStatLevel [ ] ={1,3,6,1,3,37,1,1,6,1,1};
UINT4 IsisSysStatCorrLSPs [ ] ={1,3,6,1,3,37,1,1,6,1,2};
UINT4 IsisSysStatAuthTypeFails [ ] ={1,3,6,1,3,37,1,1,6,1,3};
UINT4 IsisSysStatAuthFails [ ] ={1,3,6,1,3,37,1,1,6,1,4};
UINT4 IsisSysStatLSPDbaseOloads [ ] ={1,3,6,1,3,37,1,1,6,1,5};
UINT4 IsisSysStatManAddrDropFromAreas [ ] ={1,3,6,1,3,37,1,1,6,1,6};
UINT4 IsisSysStatAttmptToExMaxSeqNums [ ] ={1,3,6,1,3,37,1,1,6,1,7};
UINT4 IsisSysStatSeqNumSkips [ ] ={1,3,6,1,3,37,1,1,6,1,8};
UINT4 IsisSysStatOwnLSPPurges [ ] ={1,3,6,1,3,37,1,1,6,1,9};
UINT4 IsisSysStatIDFieldLenMismatches [ ] ={1,3,6,1,3,37,1,1,6,1,10};
UINT4 IsisSysStatMaxAreaAddrMismatches [ ] ={1,3,6,1,3,37,1,1,6,1,11};
UINT4 IsisSysStatPartChanges [ ] ={1,3,6,1,3,37,1,1,6,1,12};
UINT4 IsisCircIndex [ ] ={1,3,6,1,3,37,1,2,1,1,1};
UINT4 IsisCircIfIndex [ ] ={1,3,6,1,3,37,1,2,1,1,2};
UINT4 IsisCircIfSubIndex [ ] ={1,3,6,1,3,37,1,2,1,1,3};
UINT4 IsisCircLocalID [ ] ={1,3,6,1,3,37,1,2,1,1,4};
UINT4 IsisCircAdminState [ ] ={1,3,6,1,3,37,1,2,1,1,5};
UINT4 IsisCircExistState [ ] ={1,3,6,1,3,37,1,2,1,1,6};
UINT4 IsisCircType [ ] ={1,3,6,1,3,37,1,2,1,1,7};
UINT4 IsisCircExtDomain [ ] ={1,3,6,1,3,37,1,2,1,1,8};
UINT4 IsisCircAdjChanges [ ] ={1,3,6,1,3,37,1,2,1,1,9};
UINT4 IsisCircInitFails [ ] ={1,3,6,1,3,37,1,2,1,1,10};
UINT4 IsisCircRejAdjs [ ] ={1,3,6,1,3,37,1,2,1,1,11};
UINT4 IsisCircOutCtrlPDUs [ ] ={1,3,6,1,3,37,1,2,1,1,12};
UINT4 IsisCircInCtrlPDUs [ ] ={1,3,6,1,3,37,1,2,1,1,13};
UINT4 IsisCircIDFieldLenMismatches [ ] ={1,3,6,1,3,37,1,2,1,1,14};
UINT4 IsisCircLevel [ ] ={1,3,6,1,3,37,1,2,1,1,15};
UINT4 IsisCircMCAddr [ ] ={1,3,6,1,3,37,1,2,1,1,16};
UINT4 IsisCircPtToPtCircID [ ] ={1,3,6,1,3,37,1,2,1,1,17};
UINT4 IsisCircPassiveCircuit [ ] ={1,3,6,1,3,37,1,2,1,1,18};
UINT4 IsisCircMeshGroupEnabled [ ] ={1,3,6,1,3,37,1,2,1,1,19};
UINT4 IsisCircMeshGroup [ ] ={1,3,6,1,3,37,1,2,1,1,20};
UINT4 IsisCircSmallHellos [ ] ={1,3,6,1,3,37,1,2,1,1,21};
UINT4 IsisCircUpTime [ ] ={1,3,6,1,3,37,1,2,1,1,22};
UINT4 IsisCircLevelIndex [ ] ={1,3,6,1,3,37,1,3,1,1,1};
UINT4 IsisCircLevelMetric [ ] ={1,3,6,1,3,37,1,3,1,1,2};
UINT4 IsisCircLevelISPriority [ ] ={1,3,6,1,3,37,1,3,1,1,3};
UINT4 IsisCircLevelDesIS [ ] ={1,3,6,1,3,37,1,3,1,1,4};
UINT4 IsisCircLevelLANDesISChanges [ ] ={1,3,6,1,3,37,1,3,1,1,5};
UINT4 IsisCircLevelHelloMultiplier [ ] ={1,3,6,1,3,37,1,3,1,1,6};
UINT4 IsisCircLevelHelloTimer [ ] ={1,3,6,1,3,37,1,3,1,1,7};
UINT4 IsisCircLevelDRHelloTimer [ ] ={1,3,6,1,3,37,1,3,1,1,8};
UINT4 IsisCircLevelLSPThrottle [ ] ={1,3,6,1,3,37,1,3,1,1,9};
UINT4 IsisCircLevelMinLSPRetransInt [ ] ={1,3,6,1,3,37,1,3,1,1,10};
UINT4 IsisCircLevelCSNPInterval [ ] ={1,3,6,1,3,37,1,3,1,1,11};
UINT4 IsisCircLevelPartSNPInterval [ ] ={1,3,6,1,3,37,1,3,1,1,12};
UINT4 IsisPacketCountLevel [ ] ={1,3,6,1,3,37,1,4,1,1,1};
UINT4 IsisPacketCountDirection [ ] ={1,3,6,1,3,37,1,4,1,1,2};
UINT4 IsisPacketCountHello [ ] ={1,3,6,1,3,37,1,4,1,1,3};
UINT4 IsisPacketCountLSP [ ] ={1,3,6,1,3,37,1,4,1,1,4};
UINT4 IsisPacketCountCSNP [ ] ={1,3,6,1,3,37,1,4,1,1,5};
UINT4 IsisPacketCountPSNP [ ] ={1,3,6,1,3,37,1,4,1,1,6};
UINT4 IsisISAdjIndex [ ] ={1,3,6,1,3,37,1,5,1,1,1};
UINT4 IsisISAdjState [ ] ={1,3,6,1,3,37,1,5,1,1,2};
UINT4 IsisISAdjNeighSNPAAddress [ ] ={1,3,6,1,3,37,1,5,1,1,3};
UINT4 IsisISAdjNeighSysType [ ] ={1,3,6,1,3,37,1,5,1,1,4};
UINT4 IsisISAdjNeighSysID [ ] ={1,3,6,1,3,37,1,5,1,1,5};
UINT4 IsisISAdjUsage [ ] ={1,3,6,1,3,37,1,5,1,1,6};
UINT4 IsisISAdjHoldTimer [ ] ={1,3,6,1,3,37,1,5,1,1,7};
UINT4 IsisISAdjNeighPriority [ ] ={1,3,6,1,3,37,1,5,1,1,8};
UINT4 IsisISAdjUpTime [ ] ={1,3,6,1,3,37,1,5,1,1,9};
UINT4 IsisISAdjAreaAddrIndex [ ] ={1,3,6,1,3,37,1,5,2,1,1};
UINT4 IsisISAdjAreaAddress [ ] ={1,3,6,1,3,37,1,5,2,1,2};
UINT4 IsisISAdjIPAddrIndex [ ] ={1,3,6,1,3,37,1,5,3,1,1};
UINT4 IsisISAdjIPAddressType [ ] ={1,3,6,1,3,37,1,5,3,1,2};
UINT4 IsisISAdjIPAddress [ ] ={1,3,6,1,3,37,1,5,3,1,3};
UINT4 IsisISAdjProtSuppIndex [ ] ={1,3,6,1,3,37,1,5,4,1,1};
UINT4 IsisISAdjProtSuppProtocol [ ] ={1,3,6,1,3,37,1,5,4,1,2};
UINT4 IsisIPRAIndex [ ] ={1,3,6,1,3,37,1,7,1,1,1};
UINT4 IsisIPRAType [ ] ={1,3,6,1,3,37,1,7,1,1,2};
UINT4 IsisIPRADestType [ ] ={1,3,6,1,3,37,1,7,1,1,3};
UINT4 IsisIPRADest [ ] ={1,3,6,1,3,37,1,7,1,1,4};
UINT4 IsisIPRADestPrefixLen [ ] ={1,3,6,1,3,37,1,7,1,1,5};
UINT4 IsisIPRAExistState [ ] ={1,3,6,1,3,37,1,7,1,1,6};
UINT4 IsisIPRAAdminState [ ] ={1,3,6,1,3,37,1,7,1,1,7};
UINT4 IsisIPRAMetric [ ] ={1,3,6,1,3,37,1,7,1,1,8};
UINT4 IsisIPRAMetricType [ ] ={1,3,6,1,3,37,1,7,1,1,9};
UINT4 IsisIPRASNPAAddress [ ] ={1,3,6,1,3,37,1,7,1,1,10};
UINT4 IsisTrapLSPID [ ] ={1,3,6,1,3,37,1,8,1,1,1};
UINT4 IsisSystemLevel [ ] ={1,3,6,1,3,37,1,8,1,1,2};
UINT4 IsisPDUFragment [ ] ={1,3,6,1,3,37,1,8,1,1,3};
UINT4 IsisFieldLen [ ] ={1,3,6,1,3,37,1,8,1,1,4};
UINT4 IsisMaxAreaAddress [ ] ={1,3,6,1,3,37,1,8,1,1,5};
UINT4 IsisProtocolVersion [ ] ={1,3,6,1,3,37,1,8,1,1,6};
UINT4 IsisLSPSize [ ] ={1,3,6,1,3,37,1,8,1,1,7};
UINT4 IsisOriginatingBufferSize [ ] ={1,3,6,1,3,37,1,8,1,1,8};
UINT4 IsisProtocolsSupported [ ] ={1,3,6,1,3,37,1,8,1,1,9};


tMbDbEntry stdisiMibEntry[]= {

{{11,IsisSysInstance}, GetNextIndexIsisSysTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IsisSysTableINDEX, 1, 0, 0, NULL},

{{11,IsisSysVersion}, GetNextIndexIsisSysTable, IsisSysVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisSysTableINDEX, 1, 0, 0, "1"},

{{11,IsisSysType}, GetNextIndexIsisSysTable, IsisSysTypeGet, IsisSysTypeSet, IsisSysTypeTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "3"},

{{11,IsisSysID}, GetNextIndexIsisSysTable, IsisSysIDGet, IsisSysIDSet, IsisSysIDTest, IsisSysTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, NULL},

{{11,IsisSysMaxPathSplits}, GetNextIndexIsisSysTable, IsisSysMaxPathSplitsGet, IsisSysMaxPathSplitsSet, IsisSysMaxPathSplitsTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "2"},

{{11,IsisSysMaxLSPGenInt}, GetNextIndexIsisSysTable, IsisSysMaxLSPGenIntGet, IsisSysMaxLSPGenIntSet, IsisSysMaxLSPGenIntTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "900"},

{{11,IsisSysOrigL1LSPBuffSize}, GetNextIndexIsisSysTable, IsisSysOrigL1LSPBuffSizeGet, IsisSysOrigL1LSPBuffSizeSet, IsisSysOrigL1LSPBuffSizeTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1492"},

{{11,IsisSysMaxAreaAddresses}, GetNextIndexIsisSysTable, IsisSysMaxAreaAddressesGet, IsisSysMaxAreaAddressesSet, IsisSysMaxAreaAddressesTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "3"},

{{11,IsisSysMinL1LSPGenInt}, GetNextIndexIsisSysTable, IsisSysMinL1LSPGenIntGet, IsisSysMinL1LSPGenIntSet, IsisSysMinL1LSPGenIntTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "30"},

{{11,IsisSysMinL2LSPGenInt}, GetNextIndexIsisSysTable, IsisSysMinL2LSPGenIntGet, IsisSysMinL2LSPGenIntSet, IsisSysMinL2LSPGenIntTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "30"},

{{11,IsisSysPollESHelloRate}, GetNextIndexIsisSysTable, IsisSysPollESHelloRateGet, IsisSysPollESHelloRateSet, IsisSysPollESHelloRateTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "50"},

{{11,IsisSysWaitTime}, GetNextIndexIsisSysTable, IsisSysWaitTimeGet, IsisSysWaitTimeSet, IsisSysWaitTimeTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "60"},

{{11,IsisSysAdminState}, GetNextIndexIsisSysTable, IsisSysAdminStateGet, IsisSysAdminStateSet, IsisSysAdminStateTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1"},

{{11,IsisSysL1State}, GetNextIndexIsisSysTable, IsisSysL1StateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisSysTableINDEX, 1, 0, 0, NULL},

{{11,IsisSysOrigL2LSPBuffSize}, GetNextIndexIsisSysTable, IsisSysOrigL2LSPBuffSizeGet, IsisSysOrigL2LSPBuffSizeSet, IsisSysOrigL2LSPBuffSizeTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1492"},

{{11,IsisSysL2State}, GetNextIndexIsisSysTable, IsisSysL2StateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisSysTableINDEX, 1, 0, 0, NULL},

{{11,IsisSysLogAdjacencyChanges}, GetNextIndexIsisSysTable, IsisSysLogAdjacencyChangesGet, IsisSysLogAdjacencyChangesSet, IsisSysLogAdjacencyChangesTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "2"},

{{11,IsisSysMaxAreaCheck}, GetNextIndexIsisSysTable, IsisSysMaxAreaCheckGet, IsisSysMaxAreaCheckSet, IsisSysMaxAreaCheckTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1"},

{{11,IsisSysNextCircIndex}, GetNextIndexIsisSysTable, IsisSysNextCircIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisSysTableINDEX, 1, 0, 0, NULL},

/* The default value of the Row status object shoule not be 1 (ACTIVE), since the default values will not 
 * be set in the corresponding while MSR restoration */
{{11,IsisSysExistState}, GetNextIndexIsisSysTable, IsisSysExistStateGet, IsisSysExistStateSet, IsisSysExistStateTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 1, NULL},

{{11,IsisSysL2toL1Leaking}, GetNextIndexIsisSysTable, IsisSysL2toL1LeakingGet, IsisSysL2toL1LeakingSet, IsisSysL2toL1LeakingTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "2"},

{{11,IsisSysSetOverload}, GetNextIndexIsisSysTable, IsisSysSetOverloadGet, IsisSysSetOverloadSet, IsisSysSetOverloadTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "4"},

{{11,IsisSysL1MetricStyle}, GetNextIndexIsisSysTable, IsisSysL1MetricStyleGet, IsisSysL1MetricStyleSet, IsisSysL1MetricStyleTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1"},

{{11,IsisSysL1SPFConsiders}, GetNextIndexIsisSysTable, IsisSysL1SPFConsidersGet, IsisSysL1SPFConsidersSet, IsisSysL1SPFConsidersTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1"},

{{11,IsisSysL2MetricStyle}, GetNextIndexIsisSysTable, IsisSysL2MetricStyleGet, IsisSysL2MetricStyleSet, IsisSysL2MetricStyleTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1"},

{{11,IsisSysL2SPFConsiders}, GetNextIndexIsisSysTable, IsisSysL2SPFConsidersGet, IsisSysL2SPFConsidersSet, IsisSysL2SPFConsidersTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1"},

{{11,IsisSysTEEnabled}, GetNextIndexIsisSysTable, IsisSysTEEnabledGet, IsisSysTEEnabledSet, IsisSysTEEnabledTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "3"},

{{11,IsisSysMaxAge}, GetNextIndexIsisSysTable, IsisSysMaxAgeGet, IsisSysMaxAgeSet, IsisSysMaxAgeTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1200"},

{{11,IsisSysReceiveLSPBufferSize}, GetNextIndexIsisSysTable, IsisSysReceiveLSPBufferSizeGet, IsisSysReceiveLSPBufferSizeSet, IsisSysReceiveLSPBufferSizeTest, IsisSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSysTableINDEX, 1, 0, 0, "1492"},

{{11,IsisManAreaAddr}, GetNextIndexIsisManAreaAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IsisManAreaAddrTableINDEX, 2, 0, 0, NULL},

{{11,IsisManAreaAddrExistState}, GetNextIndexIsisManAreaAddrTable, IsisManAreaAddrExistStateGet, IsisManAreaAddrExistStateSet, IsisManAreaAddrExistStateTest, IsisManAreaAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisManAreaAddrTableINDEX, 2, 0, 1, "1"},

{{11,IsisAreaAddr}, GetNextIndexIsisAreaAddrTable, IsisAreaAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisAreaAddrTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysProtSuppProtocol}, GetNextIndexIsisSysProtSuppTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IsisSysProtSuppTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysProtSuppExistState}, GetNextIndexIsisSysProtSuppTable, IsisSysProtSuppExistStateGet, IsisSysProtSuppExistStateSet, IsisSysProtSuppExistStateTest, IsisSysProtSuppTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSysProtSuppTableINDEX, 2, 0, 1, "1"},

{{11,IsisSummAddressType}, GetNextIndexIsisSummAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IsisSummAddrTableINDEX, 4, 0, 0, NULL},

{{11,IsisSummAddress}, GetNextIndexIsisSummAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, IsisSummAddrTableINDEX, 4, 0, 0, NULL},

{{11,IsisSummAddrPrefixLen}, GetNextIndexIsisSummAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, IsisSummAddrTableINDEX, 4, 0, 0, NULL},

/* The default value of the Row status object shoule not be 1 (ACTIVE), since the default values will not be set 
 * in the corresponding entry during MSR restoration */
{{11,IsisSummAddrExistState}, GetNextIndexIsisSummAddrTable, IsisSummAddrExistStateGet, IsisSummAddrExistStateSet, IsisSummAddrExistStateTest, IsisSummAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSummAddrTableINDEX, 4, 0, 1, NULL},

{{11,IsisSummAddrAdminState}, GetNextIndexIsisSummAddrTable, IsisSummAddrAdminStateGet, IsisSummAddrAdminStateSet, IsisSummAddrAdminStateTest, IsisSummAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisSummAddrTableINDEX, 4, 0, 0, "4"},

{{11,IsisSummAddrMetric}, GetNextIndexIsisSummAddrTable, IsisSummAddrMetricGet, IsisSummAddrMetricSet, IsisSummAddrMetricTest, IsisSummAddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisSummAddrTableINDEX, 4, 0, 0, "20"},

{{11,IsisSysStatLevel}, GetNextIndexIsisSysStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatCorrLSPs}, GetNextIndexIsisSysStatsTable, IsisSysStatCorrLSPsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatAuthTypeFails}, GetNextIndexIsisSysStatsTable, IsisSysStatAuthTypeFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatAuthFails}, GetNextIndexIsisSysStatsTable, IsisSysStatAuthFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatLSPDbaseOloads}, GetNextIndexIsisSysStatsTable, IsisSysStatLSPDbaseOloadsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatManAddrDropFromAreas}, GetNextIndexIsisSysStatsTable, IsisSysStatManAddrDropFromAreasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatAttmptToExMaxSeqNums}, GetNextIndexIsisSysStatsTable, IsisSysStatAttmptToExMaxSeqNumsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatSeqNumSkips}, GetNextIndexIsisSysStatsTable, IsisSysStatSeqNumSkipsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatOwnLSPPurges}, GetNextIndexIsisSysStatsTable, IsisSysStatOwnLSPPurgesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatIDFieldLenMismatches}, GetNextIndexIsisSysStatsTable, IsisSysStatIDFieldLenMismatchesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatMaxAreaAddrMismatches}, GetNextIndexIsisSysStatsTable, IsisSysStatMaxAreaAddrMismatchesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisSysStatPartChanges}, GetNextIndexIsisSysStatsTable, IsisSysStatPartChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisSysStatsTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircIndex}, GetNextIndexIsisCircTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircIfIndex}, GetNextIndexIsisCircTable, IsisCircIfIndexGet, IsisCircIfIndexSet, IsisCircIfIndexTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircIfSubIndex}, GetNextIndexIsisCircTable, IsisCircIfSubIndexGet, IsisCircIfSubIndexSet, IsisCircIfSubIndexTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircLocalID}, GetNextIndexIsisCircTable, IsisCircLocalIDGet, IsisCircLocalIDSet, IsisCircLocalIDTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircAdminState}, GetNextIndexIsisCircTable, IsisCircAdminStateGet, IsisCircAdminStateSet, IsisCircAdminStateTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, "1"},

/* The default value of the Row status object shoule not be 1 (ACTIVE), since the default values will not be set 
 * in the corresponding entry during MSR restoration */
{{11,IsisCircExistState}, GetNextIndexIsisCircTable, IsisCircExistStateGet, IsisCircExistStateSet, IsisCircExistStateTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 1, NULL},

{{11,IsisCircType}, GetNextIndexIsisCircTable, IsisCircTypeGet, IsisCircTypeSet, IsisCircTypeTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircExtDomain}, GetNextIndexIsisCircTable, IsisCircExtDomainGet, IsisCircExtDomainSet, IsisCircExtDomainTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, "2"},

{{11,IsisCircAdjChanges}, GetNextIndexIsisCircTable, IsisCircAdjChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircInitFails}, GetNextIndexIsisCircTable, IsisCircInitFailsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircRejAdjs}, GetNextIndexIsisCircTable, IsisCircRejAdjsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircOutCtrlPDUs}, GetNextIndexIsisCircTable, IsisCircOutCtrlPDUsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircInCtrlPDUs}, GetNextIndexIsisCircTable, IsisCircInCtrlPDUsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircIDFieldLenMismatches}, GetNextIndexIsisCircTable, IsisCircIDFieldLenMismatchesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircLevel}, GetNextIndexIsisCircTable, IsisCircLevelGet, IsisCircLevelSet, IsisCircLevelTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, "3"},

{{11,IsisCircMCAddr}, GetNextIndexIsisCircTable, IsisCircMCAddrGet, IsisCircMCAddrSet, IsisCircMCAddrTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, "1"},

{{11,IsisCircPtToPtCircID}, GetNextIndexIsisCircTable, IsisCircPtToPtCircIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircPassiveCircuit}, GetNextIndexIsisCircTable, IsisCircPassiveCircuitGet, IsisCircPassiveCircuitSet, IsisCircPassiveCircuitTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, "2"},

{{11,IsisCircMeshGroupEnabled}, GetNextIndexIsisCircTable, IsisCircMeshGroupEnabledGet, IsisCircMeshGroupEnabledSet, IsisCircMeshGroupEnabledTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, "1"},

{{11,IsisCircMeshGroup}, GetNextIndexIsisCircTable, IsisCircMeshGroupGet, IsisCircMeshGroupSet, IsisCircMeshGroupTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, "1"},

{{11,IsisCircSmallHellos}, GetNextIndexIsisCircTable, IsisCircSmallHellosGet, IsisCircSmallHellosSet, IsisCircSmallHellosTest, IsisCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisCircTableINDEX, 2, 0, 0, "1"},

{{11,IsisCircUpTime}, GetNextIndexIsisCircTable, IsisCircUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisCircTableINDEX, 2, 0, 0, NULL},

{{11,IsisCircLevelIndex}, GetNextIndexIsisCircLevelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IsisCircLevelTableINDEX, 3, 0, 0, NULL},

{{11,IsisCircLevelMetric}, GetNextIndexIsisCircLevelTable, IsisCircLevelMetricGet, IsisCircLevelMetricSet, IsisCircLevelMetricTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "10"},

{{11,IsisCircLevelISPriority}, GetNextIndexIsisCircLevelTable, IsisCircLevelISPriorityGet, IsisCircLevelISPrioritySet, IsisCircLevelISPriorityTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "64"},

{{11,IsisCircLevelDesIS}, GetNextIndexIsisCircLevelTable, IsisCircLevelDesISGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisCircLevelTableINDEX, 3, 0, 0, NULL},

{{11,IsisCircLevelLANDesISChanges}, GetNextIndexIsisCircLevelTable, IsisCircLevelLANDesISChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisCircLevelTableINDEX, 3, 0, 0, NULL},

{{11,IsisCircLevelHelloMultiplier}, GetNextIndexIsisCircLevelTable, IsisCircLevelHelloMultiplierGet, IsisCircLevelHelloMultiplierSet, IsisCircLevelHelloMultiplierTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "10"},

{{11,IsisCircLevelHelloTimer}, GetNextIndexIsisCircLevelTable, IsisCircLevelHelloTimerGet, IsisCircLevelHelloTimerSet, IsisCircLevelHelloTimerTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "3000"},

{{11,IsisCircLevelDRHelloTimer}, GetNextIndexIsisCircLevelTable, IsisCircLevelDRHelloTimerGet, IsisCircLevelDRHelloTimerSet, IsisCircLevelDRHelloTimerTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "1000"},

{{11,IsisCircLevelLSPThrottle}, GetNextIndexIsisCircLevelTable, IsisCircLevelLSPThrottleGet, IsisCircLevelLSPThrottleSet, IsisCircLevelLSPThrottleTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "10"},

{{11,IsisCircLevelMinLSPRetransInt}, GetNextIndexIsisCircLevelTable, IsisCircLevelMinLSPRetransIntGet, IsisCircLevelMinLSPRetransIntSet, IsisCircLevelMinLSPRetransIntTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "5"},

{{11,IsisCircLevelCSNPInterval}, GetNextIndexIsisCircLevelTable, IsisCircLevelCSNPIntervalGet, IsisCircLevelCSNPIntervalSet, IsisCircLevelCSNPIntervalTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "10"},

{{11,IsisCircLevelPartSNPInterval}, GetNextIndexIsisCircLevelTable, IsisCircLevelPartSNPIntervalGet, IsisCircLevelPartSNPIntervalSet, IsisCircLevelPartSNPIntervalTest, IsisCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisCircLevelTableINDEX, 3, 0, 0, "2"},

{{11,IsisPacketCountLevel}, GetNextIndexIsisPacketCountTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IsisPacketCountTableINDEX, 4, 0, 0, NULL},

{{11,IsisPacketCountDirection}, GetNextIndexIsisPacketCountTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IsisPacketCountTableINDEX, 4, 0, 0, NULL},

{{11,IsisPacketCountHello}, GetNextIndexIsisPacketCountTable, IsisPacketCountHelloGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisPacketCountTableINDEX, 4, 0, 0, NULL},

{{11,IsisPacketCountLSP}, GetNextIndexIsisPacketCountTable, IsisPacketCountLSPGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisPacketCountTableINDEX, 4, 0, 0, NULL},

{{11,IsisPacketCountCSNP}, GetNextIndexIsisPacketCountTable, IsisPacketCountCSNPGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisPacketCountTableINDEX, 4, 0, 0, NULL},

{{11,IsisPacketCountPSNP}, GetNextIndexIsisPacketCountTable, IsisPacketCountPSNPGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IsisPacketCountTableINDEX, 4, 0, 0, NULL},

{{11,IsisISAdjIndex}, GetNextIndexIsisISAdjTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjState}, GetNextIndexIsisISAdjTable, IsisISAdjStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjNeighSNPAAddress}, GetNextIndexIsisISAdjTable, IsisISAdjNeighSNPAAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjNeighSysType}, GetNextIndexIsisISAdjTable, IsisISAdjNeighSysTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjNeighSysID}, GetNextIndexIsisISAdjTable, IsisISAdjNeighSysIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjUsage}, GetNextIndexIsisISAdjTable, IsisISAdjUsageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjHoldTimer}, GetNextIndexIsisISAdjTable, IsisISAdjHoldTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjNeighPriority}, GetNextIndexIsisISAdjTable, IsisISAdjNeighPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjUpTime}, GetNextIndexIsisISAdjTable, IsisISAdjUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisISAdjTableINDEX, 3, 0, 0, NULL},

{{11,IsisISAdjAreaAddrIndex}, GetNextIndexIsisISAdjAreaAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IsisISAdjAreaAddrTableINDEX, 5, 0, 0, NULL},

{{11,IsisISAdjAreaAddress}, GetNextIndexIsisISAdjAreaAddrTable, IsisISAdjAreaAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisISAdjAreaAddrTableINDEX, 5, 0, 0, NULL},

{{11,IsisISAdjIPAddrIndex}, GetNextIndexIsisISAdjIPAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IsisISAdjIPAddrTableINDEX, 4, 0, 0, NULL},

{{11,IsisISAdjIPAddressType}, GetNextIndexIsisISAdjIPAddrTable, IsisISAdjIPAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisISAdjIPAddrTableINDEX, 4, 0, 0, NULL},

{{11,IsisISAdjIPAddress}, GetNextIndexIsisISAdjIPAddrTable, IsisISAdjIPAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisISAdjIPAddrTableINDEX, 4, 0, 0, NULL},

{{11,IsisISAdjProtSuppIndex}, GetNextIndexIsisISAdjProtSuppTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IsisISAdjProtSuppTableINDEX, 4, 0, 0, NULL},

{{11,IsisISAdjProtSuppProtocol}, GetNextIndexIsisISAdjProtSuppTable, IsisISAdjProtSuppProtocolGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisISAdjProtSuppTableINDEX, 4, 0, 0, NULL},

{{11,IsisIPRAIndex}, GetNextIndexIsisIPRATable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IsisIPRATableINDEX, 3, 0, 0, NULL},

{{11,IsisIPRAType}, GetNextIndexIsisIPRATable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IsisIPRATableINDEX, 3, 0, 0, NULL},

{{11,IsisIPRADestType}, GetNextIndexIsisIPRATable, IsisIPRADestTypeGet, IsisIPRADestTypeSet, IsisIPRADestTypeTest, IsisIPRATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisIPRATableINDEX, 3, 0, 0, NULL},

{{11,IsisIPRADest}, GetNextIndexIsisIPRATable, IsisIPRADestGet, IsisIPRADestSet, IsisIPRADestTest, IsisIPRATableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IsisIPRATableINDEX, 3, 0, 0, NULL},

{{11,IsisIPRADestPrefixLen}, GetNextIndexIsisIPRATable, IsisIPRADestPrefixLenGet, IsisIPRADestPrefixLenSet, IsisIPRADestPrefixLenTest, IsisIPRATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IsisIPRATableINDEX, 3, 0, 0, NULL},

/* The default value of the Row status object shoule not be 1 (ACTIVE), since the default values will not be set 
 * in the corresponding entry during MSR restoration */
{{11,IsisIPRAExistState}, GetNextIndexIsisIPRATable, IsisIPRAExistStateGet, IsisIPRAExistStateSet, IsisIPRAExistStateTest, IsisIPRATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisIPRATableINDEX, 3, 0, 1, NULL},

{{11,IsisIPRAAdminState}, GetNextIndexIsisIPRATable, IsisIPRAAdminStateGet, IsisIPRAAdminStateSet, IsisIPRAAdminStateTest, IsisIPRATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisIPRATableINDEX, 3, 0, 0, "1"},

{{11,IsisIPRAMetric}, GetNextIndexIsisIPRATable, IsisIPRAMetricGet, IsisIPRAMetricSet, IsisIPRAMetricTest, IsisIPRATableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IsisIPRATableINDEX, 3, 0, 0, "20"},

{{11,IsisIPRAMetricType}, GetNextIndexIsisIPRATable, IsisIPRAMetricTypeGet, IsisIPRAMetricTypeSet, IsisIPRAMetricTypeTest, IsisIPRATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IsisIPRATableINDEX, 3, 0, 0, "1"},

{{11,IsisIPRASNPAAddress}, GetNextIndexIsisIPRATable, IsisIPRASNPAAddressGet, IsisIPRASNPAAddressSet, IsisIPRASNPAAddressTest, IsisIPRATableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IsisIPRATableINDEX, 3, 0, 0, "0"},

{{11,IsisTrapLSPID}, GetNextIndexIsisNotificationTable, IsisTrapLSPIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},

{{11,IsisSystemLevel}, GetNextIndexIsisNotificationTable, IsisSystemLevelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},

{{11,IsisPDUFragment}, GetNextIndexIsisNotificationTable, IsisPDUFragmentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},

{{11,IsisFieldLen}, GetNextIndexIsisNotificationTable, IsisFieldLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},

{{11,IsisMaxAreaAddress}, GetNextIndexIsisNotificationTable, IsisMaxAreaAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},

{{11,IsisProtocolVersion}, GetNextIndexIsisNotificationTable, IsisProtocolVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},

{{11,IsisLSPSize}, GetNextIndexIsisNotificationTable, IsisLSPSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},

{{11,IsisOriginatingBufferSize}, GetNextIndexIsisNotificationTable, IsisOriginatingBufferSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},

{{11,IsisProtocolsSupported}, GetNextIndexIsisNotificationTable, IsisProtocolsSupportedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, IsisNotificationTableINDEX, 1, 0, 0, NULL},
};
tMibData stdisiEntry = { 127, stdisiMibEntry };
#endif /* _STDISIDB_H */

