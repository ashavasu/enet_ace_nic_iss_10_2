/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: stdislow.h,v 1.1.1.1 2008/01/23 13:21:27 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for IsisSysTable. */
INT1
nmhValidateIndexInstanceIsisSysTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisSysTable  */

INT1
nmhGetFirstIndexIsisSysTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisSysTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisSysVersion ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisSysType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisSysMaxPathSplits ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysMaxLSPGenInt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysOrigL1LSPBuffSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysMaxAreaAddresses ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysMinL1LSPGenInt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysMinL2LSPGenInt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysPollESHelloRate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysWaitTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysAdminState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysL1State ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysOrigL2LSPBuffSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysL2State ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysLogAdjacencyChanges ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysMaxAreaCheck ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysNextCircIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysExistState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysL2toL1Leaking ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysSetOverload ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysL1MetricStyle ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysL1SPFConsiders ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysL2MetricStyle ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysL2SPFConsiders ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysTEEnabled ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysMaxAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisSysReceiveLSPBufferSize ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIsisSysType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIsisSysMaxPathSplits ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysMaxLSPGenInt ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysOrigL1LSPBuffSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysMaxAreaAddresses ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysMinL1LSPGenInt ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysMinL2LSPGenInt ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysPollESHelloRate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysWaitTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysAdminState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysOrigL2LSPBuffSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysLogAdjacencyChanges ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysMaxAreaCheck ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysExistState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysL2toL1Leaking ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysSetOverload ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysL1MetricStyle ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysL1SPFConsiders ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysL2MetricStyle ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysL2SPFConsiders ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysTEEnabled ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysMaxAge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIsisSysReceiveLSPBufferSize ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IsisSysType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IsisSysMaxPathSplits ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysMaxLSPGenInt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysOrigL1LSPBuffSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysMaxAreaAddresses ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysMinL1LSPGenInt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysMinL2LSPGenInt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysPollESHelloRate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysWaitTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysAdminState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysOrigL2LSPBuffSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysLogAdjacencyChanges ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysMaxAreaCheck ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysExistState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysL2toL1Leaking ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysSetOverload ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysL1MetricStyle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysL1SPFConsiders ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysL2MetricStyle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysL2SPFConsiders ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysTEEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysMaxAge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IsisSysReceiveLSPBufferSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for IsisManAreaAddrTable. */
INT1
nmhValidateIndexInstanceIsisManAreaAddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IsisManAreaAddrTable  */

INT1
nmhGetFirstIndexIsisManAreaAddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisManAreaAddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisManAreaAddrExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIsisManAreaAddrExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IsisManAreaAddrExistState ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Validate Index Instance for IsisAreaAddrTable. */
INT1
nmhValidateIndexInstanceIsisAreaAddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IsisAreaAddrTable  */

INT1
nmhGetFirstIndexIsisAreaAddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisAreaAddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for IsisSysProtSuppTable. */
INT1
nmhValidateIndexInstanceIsisSysProtSuppTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisSysProtSuppTable  */

INT1
nmhGetFirstIndexIsisSysProtSuppTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisSysProtSuppTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisSysProtSuppExistState ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIsisSysProtSuppExistState ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IsisSysProtSuppExistState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for IsisSummAddrTable. */
INT1
nmhValidateIndexInstanceIsisSummAddrTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisSummAddrTable  */

INT1
nmhGetFirstIndexIsisSummAddrTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisSummAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisSummAddrExistState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetIsisSummAddrAdminState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetIsisSummAddrMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIsisSummAddrExistState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetIsisSummAddrAdminState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetIsisSummAddrMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IsisSummAddrExistState ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2IsisSummAddrAdminState ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2IsisSummAddrMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Proto Validate Index Instance for IsisSysStatsTable. */
INT1
nmhValidateIndexInstanceIsisSysStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisSysStatsTable  */

INT1
nmhGetFirstIndexIsisSysStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisSysStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisSysStatCorrLSPs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatAuthTypeFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatAuthFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatLSPDbaseOloads ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatManAddrDropFromAreas ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatAttmptToExMaxSeqNums ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatSeqNumSkips ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatOwnLSPPurges ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatIDFieldLenMismatches ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatMaxAreaAddrMismatches ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisSysStatPartChanges ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for IsisCircTable. */
INT1
nmhValidateIndexInstanceIsisCircTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisCircTable  */

INT1
nmhGetFirstIndexIsisCircTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisCircTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisCircIfIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircIfSubIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLocalID ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircAdminState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircExistState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircExtDomain ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircAdjChanges ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisCircInitFails ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisCircRejAdjs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisCircOutCtrlPDUs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisCircInCtrlPDUs ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisCircIDFieldLenMismatches ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisCircLevel ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircMCAddr ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircPtToPtCircID ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisCircPassiveCircuit ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircMeshGroupEnabled ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircMeshGroup ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircSmallHellos ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircUpTime ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIsisCircIfIndex ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircIfSubIndex ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLocalID ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircAdminState ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircExistState ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircExtDomain ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevel ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircMCAddr ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircPassiveCircuit ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircMeshGroupEnabled ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircMeshGroup ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircSmallHellos ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IsisCircIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircIfSubIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLocalID ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircAdminState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircExistState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircExtDomain ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevel ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircMCAddr ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircPassiveCircuit ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircMeshGroupEnabled ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircMeshGroup ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircSmallHellos ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for IsisCircLevelTable. */
INT1
nmhValidateIndexInstanceIsisCircLevelTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisCircLevelTable  */

INT1
nmhGetFirstIndexIsisCircLevelTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisCircLevelTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisCircLevelMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLevelISPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLevelDesIS ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisCircLevelLANDesISChanges ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisCircLevelHelloMultiplier ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLevelHelloTimer ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLevelDRHelloTimer ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLevelLSPThrottle ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLevelMinLSPRetransInt ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLevelCSNPInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisCircLevelPartSNPInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIsisCircLevelMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevelISPriority ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevelHelloMultiplier ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevelHelloTimer ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevelDRHelloTimer ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevelLSPThrottle ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevelMinLSPRetransInt ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevelCSNPInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisCircLevelPartSNPInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IsisCircLevelMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevelISPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevelHelloMultiplier ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevelHelloTimer ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevelDRHelloTimer ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevelLSPThrottle ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevelMinLSPRetransInt ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevelCSNPInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisCircLevelPartSNPInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for IsisPacketCountTable. */
INT1
nmhValidateIndexInstanceIsisPacketCountTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisPacketCountTable  */

INT1
nmhGetFirstIndexIsisPacketCountTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisPacketCountTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisPacketCountHello ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisPacketCountLSP ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisPacketCountCSNP ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisPacketCountPSNP ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for IsisISAdjTable. */
INT1
nmhValidateIndexInstanceIsisISAdjTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisISAdjTable  */

INT1
nmhGetFirstIndexIsisISAdjTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisISAdjTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisISAdjState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisISAdjNeighSNPAAddress ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisISAdjNeighSysType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisISAdjNeighSysID ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisISAdjUsage ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisISAdjHoldTimer ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisISAdjNeighPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisISAdjUpTime ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for IsisISAdjAreaAddrTable. */
INT1
nmhValidateIndexInstanceIsisISAdjAreaAddrTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IsisISAdjAreaAddrTable  */

INT1
nmhGetFirstIndexIsisISAdjAreaAddrTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisISAdjAreaAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for IsisISAdjIPAddrTable. */
INT1
nmhValidateIndexInstanceIsisISAdjIPAddrTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisISAdjIPAddrTable  */

INT1
nmhGetFirstIndexIsisISAdjIPAddrTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisISAdjIPAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisISAdjIPAddressType ARG_LIST((INT4  , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisISAdjIPAddress ARG_LIST((INT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for IsisISAdjProtSuppTable. */
INT1
nmhValidateIndexInstanceIsisISAdjProtSuppTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisISAdjProtSuppTable  */

INT1
nmhGetFirstIndexIsisISAdjProtSuppTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisISAdjProtSuppTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for IsisIPRATable. */
INT1
nmhValidateIndexInstanceIsisIPRATable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisIPRATable  */

INT1
nmhGetFirstIndexIsisIPRATable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisIPRATable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisIPRADestType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisIPRADest ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisIPRADestPrefixLen ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetIsisIPRAExistState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisIPRAAdminState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisIPRAMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisIPRAMetricType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIsisIPRASNPAAddress ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIsisIPRADestType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisIPRADest ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIsisIPRADestPrefixLen ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetIsisIPRAExistState ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisIPRAAdminState ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisIPRAMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisIPRAMetricType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIsisIPRASNPAAddress ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IsisIPRADestType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisIPRADest ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IsisIPRADestPrefixLen ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2IsisIPRAExistState ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisIPRAAdminState ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisIPRAMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisIPRAMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IsisIPRASNPAAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for IsisNotificationTable. */
INT1
nmhValidateIndexInstanceIsisNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisNotificationTable  */

INT1
nmhGetFirstIndexIsisNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIsisNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIsisTrapLSPID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisSystemLevel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisPDUFragment ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIsisFieldLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisMaxAreaAddress ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisProtocolVersion ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisLSPSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisOriginatingBufferSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIsisProtocolsSupported ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
