
/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: bdyexp.h,v 1.2 2009/06/03 06:39:01 premap-iss Exp $
 *
 * Description: This file contains all the exported definitions
 * used by the buddy module
 *
 *****************************************************************************/

#ifndef __BDYEXP_H__
#define __BDYEXP_H__

#define BUDDY_SUCCESS            0
#define BUDDY_FAILURE           -1
#define BUDDY_ALLOC_FAILURE     -2
#define BUDDY_INVALID_SIZE      -3
#define BUDDY_NO_SUCH_BLK       -4
#define BUDDY_NO_FREE_INST      -5
#define BUDDY_CONT_BUF           0
#define BUDDY_NON_CONT_BUF       1

#define BUDDY_MAX_INST           2


PUBLIC VOID BuddyInit (VOID);
PUBLIC INT4 BuddyCreateInst (UINT4 , UINT4 , UINT4, UINT1);
PUBLIC VOID BuddyDestroyInst (UINT1);
PUBLIC UINT1* BuddyAlloc (UINT1 , UINT4);
PUBLIC INT4 BuddyFree (UINT1 , UINT1 *);


#endif /* __BDYEXP_H__ */
