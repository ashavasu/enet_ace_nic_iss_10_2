/*******************************************************************************
 *
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 * $Id: isdlli.h,v 1.3 2009/08/04 14:29:15 prabuc-iss Exp $
 *
 * Description: This file contains all the macros and constant definitions
 *              specific to the Data Link Layers
 *
 ******************************************************************************/

#ifndef __ISDLLI_H__
#define __ISDLLI_H__

#ifndef __ISLINUX__

#include "cfa.h"

#define ISIS_GET_MSG_LEN(pQbuf)\
        CRU_BUF_Get_ChainValidByteCount (pQBuf)

#define ISIS_COPY_TO_CHAIN_BUF(pQBuf, pu1Data, u2Offset, u4Length)\
        CRU_BUF_Copy_OverBufChain (pQBuf, pu1Data,u2Offset, u4Length)

#define ISIS_TX_DATA_TO_DLL(u2LLHandle, pChainBuf)\
        CfaHandlePktFromIsIs (pChainBuf, u2LLHandle, CFA_NLPID_ISIS)

#define ISIS_CFG_MAC_ADDR(u1OperCode, u2IfIndex, IPAddr, MACAddr)\
        CfaIfmEnetConfigMcastAddr (u1OperCode, u2IfIndex, IPAddr, MACAddr)

#define ISIS_DLL_IF_STATUS_IND(u2IfIndex, u1AdminStatus, u1OperStatus)\
        CfaHandleInterfaceStatusUpdate (u2IfIndex, u1AdminStatus, u1OperStatus)

#define ISIS_REG_ATM_VC(u4Port, u4VPI, u4VCI, pAtmCfg)\
        CfaIfmCreateDynamicAtmVcInterface (u4Port, u4VPI, u4VCI, pAtmCfg)

#define ISIS_DEREG_ATM_VC(u2AtmIfIndex)\
        CfaIfmDeleteDynamicAtmVcInterface (u2AtmIfIndex)

/* Currently CFA does not support FR links
 */

#define ISIS_REG_FR_DLCI(u4IfIndex, u4IfSubIndex, pAtmCfg)\
        0

#define ISIS_DEREG_FR_DLCI(u2IfIndex)\
        0

#define ISIS_GET_IF_INFO(u2IfIndex, pIfInfo)\
        CfaGetIfInfo (u2IfIndex, pIfInfo)

#define ISIS_GET_IF_TYPE(pIfInfo)\
        pIfInfo->u1IfType
        
#define ISIS_GET_IP_ADDR(pIfInfo)\
        pIfInfo->u4IpAddr

#define ISIS_GET_HW_ADDR(pIfInfo)\
        pIfInfo->au1MacAddr
#endif

/* DLLI Function PrototTypes */

PUBLIC INT4 IsisDlliTxData (tIsisCktEntry *, tIsisLLHdr *, UINT1 *, UINT4 , 
                            UINT1 );

#endif /* __ISDLLI_H__ */
