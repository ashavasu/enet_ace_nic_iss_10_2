/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisisdb.h,v 1.15.22.1 2018/03/08 14:01:40 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISISDB_H
#define _FSISISDB_H

UINT1 FsIsisExtSysTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIsisExtSysAreaRxPasswdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIsisExtSysDomainRxPasswdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIsisExtSummAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsIsisExtSysEventTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIsisExtSysIdToSysNameMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIsisExtCircTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIsisExtCircLevelTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsIsisExtIPRATableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIsisExtCircLevelRxPasswordTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIsisExtIPIfAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsIsisExtLogTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsIsisExtAdjTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIsisDistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIsisPreferenceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsIsisRRDMetricTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsisis [] ={1,3,6,1,4,1,2076,62};
tSNMP_OID_TYPE fsisisOID = {8, fsisis};


UINT4 FsIsisMaxInstances [ ] ={1,3,6,1,4,1,2076,62,1,1};
UINT4 FsIsisMaxCircuits [ ] ={1,3,6,1,4,1,2076,62,1,2};
UINT4 FsIsisMaxAreaAddrs [ ] ={1,3,6,1,4,1,2076,62,1,3};
UINT4 FsIsisMaxAdjs [ ] ={1,3,6,1,4,1,2076,62,1,4};
UINT4 FsIsisMaxIPRAs [ ] ={1,3,6,1,4,1,2076,62,1,5};
UINT4 FsIsisMaxEvents [ ] ={1,3,6,1,4,1,2076,62,1,6};
UINT4 FsIsisMaxSummAddr [ ] ={1,3,6,1,4,1,2076,62,1,7};
UINT4 FsIsisStatus [ ] ={1,3,6,1,4,1,2076,62,1,8};
UINT4 FsIsisMaxLSPEntries [ ] ={1,3,6,1,4,1,2076,62,1,9};
UINT4 FsIsisMaxMAA [ ] ={1,3,6,1,4,1,2076,62,1,10};
UINT4 FsIsisFTStatus [ ] ={1,3,6,1,4,1,2076,62,1,11};
UINT4 FsIsisFTState [ ] ={1,3,6,1,4,1,2076,62,1,12};
UINT4 FsIsisFactor [ ] ={1,3,6,1,4,1,2076,62,1,13};
UINT4 FsIsisMaxRoutes [ ] ={1,3,6,1,4,1,2076,62,1,14};
UINT4 FsIsisRestartState [ ] ={1,3,6,1,4,1,2076,62,1,15};
UINT4 FsIsisExtSysInstance [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,1};
UINT4 FsIsisExtSysAuthSupp [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,2};
UINT4 FsIsisExtSysAreaAuthType [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,3};
UINT4 FsIsisExtSysDomainAuthType [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,4};
UINT4 FsIsisExtSysAreaTxPasswd [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,5};
UINT4 FsIsisExtSysDomainTxPasswd [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,6};
UINT4 FsIsisExtSysMinSPFSchTime [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,7};
UINT4 FsIsisExtSysMaxSPFSchTime [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,8};
UINT4 FsIsisExtSysMinLSPMark [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,9};
UINT4 FsIsisExtSysMaxLSPMark [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,10};
UINT4 FsIsisExtSysDelMetSupp [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,11};
UINT4 FsIsisExtSysErrMetSupp [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,12};
UINT4 FsIsisExtSysExpMetSupp [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,13};
UINT4 FsIsisExtSysActSysType [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,14};
UINT4 FsIsisExtSysActMPS [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,15};
UINT4 FsIsisExtSysActMaxAA [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,16};
UINT4 FsIsisExtSysActSysIDLen [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,17};
UINT4 FsIsisExtSysActSysID [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,18};
UINT4 FsIsisExtSysActOrigL1LSPBufSize [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,19};
UINT4 FsIsisExtSysActOrigL2LSPBufSize [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,20};
UINT4 FsIsisExtSysRouterID [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,21};
UINT4 FsIsisExtSysCkts [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,22};
UINT4 FsIsisExtSysActiveCkts [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,23};
UINT4 FsIsisExtSysAdjs [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,24};
UINT4 FsIsisExtSysOperState [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,25};
UINT4 FsIsisExtSysDroppedPDUs [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,26};
UINT4 FsIsisExtRestartSupport [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,27};
UINT4 FsIsisExtGRRestartTimeInterval [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,28};
UINT4 FsIsisExtGRT2TimeIntervalLevel1 [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,29};
UINT4 FsIsisExtGRT2TimeIntervalLevel2 [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,30};
UINT4 FsIsisExtGRT1TimeInterval [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,31};
UINT4 FsIsisExtGRT1RetryCount [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,32};
UINT4 FsIsisExtGRMode [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,33};
UINT4 FsIsisExtRestartStatus [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,34};
UINT4 FsIsisExtRestartExitReason [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,35};
UINT4 FsIsisExtRestartReason [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,36};
UINT4 FsIsisExtHelperSupport [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,37};
UINT4 FsIsisExtHelperGraceTimeLimit [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,38};
UINT4 FsIsisRRDAdminStatus [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,39};
UINT4 FsIsisRRDProtoMaskForEnable [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,40};
UINT4 FsIsisRRDProtoMaskForDisable [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,41};
UINT4 FsIsisRRDRouteMapName [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,42};
UINT4 FsIsisRRDImportLevel [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,43};
UINT4 FsIsisMultiTopologySupport [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,44};
UINT4 FsIsisExtSysBfdSupport [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,45};
UINT4 FsIsisExtSysBfdAllIfStatus [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,46};
UINT4 FsIsisDotCompliance [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,47};
UINT4 FsIsisExtSysDynHostNameSupport [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,48};
UINT4 FsIsisMetricStyle [ ] ={1,3,6,1,4,1,2076,62,2,1,1,1,49};
UINT4 FsIsisExtSysAreaRxPasswd [ ] ={1,3,6,1,4,1,2076,62,2,1,2,1,1};
UINT4 FsIsisExtSysAreaRxPasswdExistState [ ] ={1,3,6,1,4,1,2076,62,2,1,2,1,2};
UINT4 FsIsisExtSysDomainRxPassword [ ] ={1,3,6,1,4,1,2076,62,2,1,3,1,1};
UINT4 FsIsisExtSysDomainRxPasswdExistState [ ] ={1,3,6,1,4,1,2076,62,2,1,3,1,2};
UINT4 FsIsisExtSummAddressType [ ] ={1,3,6,1,4,1,2076,62,2,3,1,1,1};
UINT4 FsIsisExtSummAddress [ ] ={1,3,6,1,4,1,2076,62,2,3,1,1,2};
UINT4 FsIsisExtSummAddrPrefixLen [ ] ={1,3,6,1,4,1,2076,62,2,3,1,1,3};
UINT4 FsIsisExtSummAddrDelayMetric [ ] ={1,3,6,1,4,1,2076,62,2,3,1,1,4};
UINT4 FsIsisExtSummAddrErrorMetric [ ] ={1,3,6,1,4,1,2076,62,2,3,1,1,5};
UINT4 FsIsisExtSummAddrExpenseMetric [ ] ={1,3,6,1,4,1,2076,62,2,3,1,1,6};
UINT4 FsIsisExtSummAddrFullMetric [ ] ={1,3,6,1,4,1,2076,62,2,3,1,1,7};
UINT4 FsIsisExtSysEventIdx [ ] ={1,3,6,1,4,1,2076,62,2,1,4,1,1};
UINT4 FsIsisExtSysEvent [ ] ={1,3,6,1,4,1,2076,62,2,1,4,1,2};
UINT4 FsIsisExtSysEventStr [ ] ={1,3,6,1,4,1,2076,62,2,1,4,1,3};
UINT4 FsIsisExtSysRouterInstance [ ] ={1,3,6,1,4,1,2076,62,2,1,5,1,1};
UINT4 FsIsisExtSysRouterSysID [ ] ={1,3,6,1,4,1,2076,62,2,1,5,1,2};
UINT4 FsIsisExtSysRouterHostName [ ] ={1,3,6,1,4,1,2076,62,2,1,5,1,3};
UINT4 FsIsisExtCircIndex [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,1};
UINT4 FsIsisExtCircIfStatus [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,2};
UINT4 FsIsisExtCircTxEnable [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,3};
UINT4 FsIsisExtCircRxEnable [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,4};
UINT4 FsIsisExtCircTxISHs [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,5};
UINT4 FsIsisExtCircRxISHs [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,6};
UINT4 FsIsisExtCircSNPA [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,7};
UINT4 FsIsisExtCircMTID [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,8};
UINT4 FsIsisExtCircBfdStatus [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,9};
UINT4 FsIsisExtCircExtendedCircID [ ] ={1,3,6,1,4,1,2076,62,2,2,1,1,10};
UINT4 FsIsisExtCircLevelIndex [ ] ={1,3,6,1,4,1,2076,62,2,2,2,1,1};
UINT4 FsIsisExtCircLevelDelayMetric [ ] ={1,3,6,1,4,1,2076,62,2,2,2,1,2};
UINT4 FsIsisExtCircLevelErrorMetric [ ] ={1,3,6,1,4,1,2076,62,2,2,2,1,3};
UINT4 FsIsisExtCircLevelExpenseMetric [ ] ={1,3,6,1,4,1,2076,62,2,2,2,1,4};
UINT4 FsIsisExtCircLevelTxPassword [ ] ={1,3,6,1,4,1,2076,62,2,2,2,1,5};
UINT4 FsIsisExtCircLevelWideMetric [ ] ={1,3,6,1,4,1,2076,62,2,2,2,1,6};
UINT4 FsIsisExtCircLevelAuthType [ ] ={1,3,6,1,4,1,2076,62,2,2,2,1,7};
UINT4 FsIsisExtIPRAType [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,1};
UINT4 FsIsisExtIPRAIndex [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,2};
UINT4 FsIsisExtIPRADelayMetric [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,3};
UINT4 FsIsisExtIPRAErrorMetric [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,4};
UINT4 FsIsisExtIPRAExpenseMetric [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,5};
UINT4 FsIsisExtIPRADelayMetricType [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,6};
UINT4 FsIsisExtIPRAErrorMetricType [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,7};
UINT4 FsIsisExtIPRAExpenseMetricType [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,8};
UINT4 FsIsisExtIPRANextHopType [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,9};
UINT4 FsIsisExtIPRANextHop [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,10};
UINT4 FsIsisExtIPRAFullMetric [ ] ={1,3,6,1,4,1,2076,62,2,2,3,1,11};
UINT4 FsIsisExtCircLevelRxPassword [ ] ={1,3,6,1,4,1,2076,62,2,2,4,1,1};
UINT4 FsIsisExtCircLevelRxPasswordExistState [ ] ={1,3,6,1,4,1,2076,62,2,2,4,1,2};
UINT4 FsIsisExtIPIfIndex [ ] ={1,3,6,1,4,1,2076,62,2,4,1,1,1};
UINT4 FsIsisExtIPIfSubIndex [ ] ={1,3,6,1,4,1,2076,62,2,4,1,1,2};
UINT4 FsIsisExtIPIfAddrType [ ] ={1,3,6,1,4,1,2076,62,2,4,1,1,3};
UINT4 FsIsisExtIPIfAddr [ ] ={1,3,6,1,4,1,2076,62,2,4,1,1,4};
UINT4 FsIsisExtIPIfExistState [ ] ={1,3,6,1,4,1,2076,62,2,4,1,1,5};
UINT4 FsIsisExtLogModId [ ] ={1,3,6,1,4,1,2076,62,2,5,1,1,1};
UINT4 FsIsisExtLogLevel [ ] ={1,3,6,1,4,1,2076,62,2,5,1,1,2};
UINT4 FsIsisExtAdjIndex [ ] ={1,3,6,1,4,1,2076,62,2,6,1,1,1};
UINT4 FsIsisExtAdjNeighSysID [ ] ={1,3,6,1,4,1,2076,62,2,6,1,1,2};
UINT4 FsIsisExtAdjHelperStatus [ ] ={1,3,6,1,4,1,2076,62,2,6,1,1,3};
UINT4 FsIsisExtAdjHelperExitReason [ ] ={1,3,6,1,4,1,2076,62,2,6,1,1,4};
UINT4 FsIsisExtAdjMTID [ ] ={1,3,6,1,4,1,2076,62,2,6,1,1,5};
UINT4 FsIsisExtAdjHostName [ ] ={1,3,6,1,4,1,2076,62,2,6,1,1,6};
UINT4 FsIsisExtISAdj3WayState [ ] ={1,3,6,1,4,1,2076,62,2,6,1,1,7};
UINT4 FsIsisExtISAdjNbrExtendedCircID [ ] ={1,3,6,1,4,1,2076,62,2,6,1,1,8};
UINT4 FsIsisDistInOutRouteMapName [ ] ={1,3,6,1,4,1,2076,62,4,1,1,1};
UINT4 FsIsisDistInOutRouteMapType [ ] ={1,3,6,1,4,1,2076,62,4,1,1,2};
UINT4 FsIsisDistInOutRouteMapValue [ ] ={1,3,6,1,4,1,2076,62,4,1,1,3};
UINT4 FsIsisDistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,62,4,1,1,4};
UINT4 FsIsisPreferenceValue [ ] ={1,3,6,1,4,1,2076,62,5,1,1,2};
UINT4 FsIsisPreferenceRowStatus [ ] ={1,3,6,1,4,1,2076,62,5,1,1,3};
UINT4 FsIsisRRDMetricProtocolId [ ] ={1,3,6,1,4,1,2076,62,6,1,1,1};
UINT4 FsIsisRRDMetricValue [ ] ={1,3,6,1,4,1,2076,62,6,1,1,2};




tMbDbEntry fsisisMibEntry[]= {

{{10,FsIsisMaxInstances}, NULL, FsIsisMaxInstancesGet, FsIsisMaxInstancesSet, FsIsisMaxInstancesTest, FsIsisMaxInstancesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxCircuits}, NULL, FsIsisMaxCircuitsGet, FsIsisMaxCircuitsSet, FsIsisMaxCircuitsTest, FsIsisMaxCircuitsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxAreaAddrs}, NULL, FsIsisMaxAreaAddrsGet, FsIsisMaxAreaAddrsSet, FsIsisMaxAreaAddrsTest, FsIsisMaxAreaAddrsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxAdjs}, NULL, FsIsisMaxAdjsGet, FsIsisMaxAdjsSet, FsIsisMaxAdjsTest, FsIsisMaxAdjsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxIPRAs}, NULL, FsIsisMaxIPRAsGet, FsIsisMaxIPRAsSet, FsIsisMaxIPRAsTest, FsIsisMaxIPRAsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxEvents}, NULL, FsIsisMaxEventsGet, FsIsisMaxEventsSet, FsIsisMaxEventsTest, FsIsisMaxEventsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxSummAddr}, NULL, FsIsisMaxSummAddrGet, FsIsisMaxSummAddrSet, FsIsisMaxSummAddrTest, FsIsisMaxSummAddrDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisStatus}, NULL, FsIsisStatusGet, FsIsisStatusSet, FsIsisStatusTest, FsIsisStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxLSPEntries}, NULL, FsIsisMaxLSPEntriesGet, FsIsisMaxLSPEntriesSet, FsIsisMaxLSPEntriesTest, FsIsisMaxLSPEntriesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxMAA}, NULL, FsIsisMaxMAAGet, FsIsisMaxMAASet, FsIsisMaxMAATest, FsIsisMaxMAADep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisFTStatus}, NULL, FsIsisFTStatusGet, FsIsisFTStatusSet, FsIsisFTStatusTest, FsIsisFTStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisFTState}, NULL, FsIsisFTStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsIsisFactor}, NULL, FsIsisFactorGet, FsIsisFactorSet, FsIsisFactorTest, FsIsisFactorDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisMaxRoutes}, NULL, FsIsisMaxRoutesGet, FsIsisMaxRoutesSet, FsIsisMaxRoutesTest, FsIsisMaxRoutesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsIsisRestartState}, NULL, FsIsisRestartStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "1"},

{{13,FsIsisExtSysInstance}, GetNextIndexFsIsisExtSysTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysAuthSupp}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysAuthSuppGet, FsIsisExtSysAuthSuppSet, FsIsisExtSysAuthSuppTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "4"},

{{13,FsIsisExtSysAreaAuthType}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysAreaAuthTypeGet, FsIsisExtSysAreaAuthTypeSet, FsIsisExtSysAreaAuthTypeTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "1"},

{{13,FsIsisExtSysDomainAuthType}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysDomainAuthTypeGet, FsIsisExtSysDomainAuthTypeSet, FsIsisExtSysDomainAuthTypeTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "1"},

{{13,FsIsisExtSysAreaTxPasswd}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysAreaTxPasswdGet, FsIsisExtSysAreaTxPasswdSet, FsIsisExtSysAreaTxPasswdTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysDomainTxPasswd}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysDomainTxPasswdGet, FsIsisExtSysDomainTxPasswdSet, FsIsisExtSysDomainTxPasswdTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysMinSPFSchTime}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysMinSPFSchTimeGet, FsIsisExtSysMinSPFSchTimeSet, FsIsisExtSysMinSPFSchTimeTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysMaxSPFSchTime}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysMaxSPFSchTimeGet, FsIsisExtSysMaxSPFSchTimeSet, FsIsisExtSysMaxSPFSchTimeTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysMinLSPMark}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysMinLSPMarkGet, FsIsisExtSysMinLSPMarkSet, FsIsisExtSysMinLSPMarkTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysMaxLSPMark}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysMaxLSPMarkGet, FsIsisExtSysMaxLSPMarkSet, FsIsisExtSysMaxLSPMarkTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysDelMetSupp}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysDelMetSuppGet, FsIsisExtSysDelMetSuppSet, FsIsisExtSysDelMetSuppTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysErrMetSupp}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysErrMetSuppGet, FsIsisExtSysErrMetSuppSet, FsIsisExtSysErrMetSuppTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysExpMetSupp}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysExpMetSuppGet, FsIsisExtSysExpMetSuppSet, FsIsisExtSysExpMetSuppTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysActSysType}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysActSysTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysActMPS}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysActMPSGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysActMaxAA}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysActMaxAAGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysActSysIDLen}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysActSysIDLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysActSysID}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysActSysIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysActOrigL1LSPBufSize}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysActOrigL1LSPBufSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysActOrigL2LSPBufSize}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysActOrigL2LSPBufSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysRouterID}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysRouterIDGet, FsIsisExtSysRouterIDSet, FsIsisExtSysRouterIDTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysCkts}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysCktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysActiveCkts}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysActiveCktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysAdjs}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysAdjsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysOperState}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtSysDroppedPDUs}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysDroppedPDUsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtRestartSupport}, GetNextIndexFsIsisExtSysTable, FsIsisExtRestartSupportGet, FsIsisExtRestartSupportSet, FsIsisExtRestartSupportTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "1"},

{{13,FsIsisExtGRRestartTimeInterval}, GetNextIndexFsIsisExtSysTable, FsIsisExtGRRestartTimeIntervalGet, FsIsisExtGRRestartTimeIntervalSet, FsIsisExtGRRestartTimeIntervalTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtGRT2TimeIntervalLevel1}, GetNextIndexFsIsisExtSysTable, FsIsisExtGRT2TimeIntervalLevel1Get, FsIsisExtGRT2TimeIntervalLevel1Set, FsIsisExtGRT2TimeIntervalLevel1Test, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "60"},

{{13,FsIsisExtGRT2TimeIntervalLevel2}, GetNextIndexFsIsisExtSysTable, FsIsisExtGRT2TimeIntervalLevel2Get, FsIsisExtGRT2TimeIntervalLevel2Set, FsIsisExtGRT2TimeIntervalLevel2Test, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "60"},

{{13,FsIsisExtGRT1TimeInterval}, GetNextIndexFsIsisExtSysTable, FsIsisExtGRT1TimeIntervalGet, FsIsisExtGRT1TimeIntervalSet, FsIsisExtGRT1TimeIntervalTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "3"},

{{13,FsIsisExtGRT1RetryCount}, GetNextIndexFsIsisExtSysTable, FsIsisExtGRT1RetryCountGet, FsIsisExtGRT1RetryCountSet, FsIsisExtGRT1RetryCountTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "3"},

{{13,FsIsisExtGRMode}, GetNextIndexFsIsisExtSysTable, FsIsisExtGRModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, "1"},

{{13,FsIsisExtRestartStatus}, GetNextIndexFsIsisExtSysTable, FsIsisExtRestartStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, "1"},

{{13,FsIsisExtRestartExitReason}, GetNextIndexFsIsisExtSysTable, FsIsisExtRestartExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtRestartReason}, GetNextIndexFsIsisExtSysTable, FsIsisExtRestartReasonGet, FsIsisExtRestartReasonSet, FsIsisExtRestartReasonTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtHelperSupport}, GetNextIndexFsIsisExtSysTable, FsIsisExtHelperSupportGet, FsIsisExtHelperSupportSet, FsIsisExtHelperSupportTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "1"},

{{13,FsIsisExtHelperGraceTimeLimit}, GetNextIndexFsIsisExtSysTable, FsIsisExtHelperGraceTimeLimitGet, FsIsisExtHelperGraceTimeLimitSet, FsIsisExtHelperGraceTimeLimitTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "0"},

{{13,FsIsisRRDAdminStatus}, GetNextIndexFsIsisExtSysTable, FsIsisRRDAdminStatusGet, FsIsisRRDAdminStatusSet, FsIsisRRDAdminStatusTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "0"},

{{13,FsIsisRRDProtoMaskForEnable}, GetNextIndexFsIsisExtSysTable, FsIsisRRDProtoMaskForEnableGet, FsIsisRRDProtoMaskForEnableSet, FsIsisRRDProtoMaskForEnableTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisRRDProtoMaskForDisable}, GetNextIndexFsIsisExtSysTable, FsIsisRRDProtoMaskForDisableGet, FsIsisRRDProtoMaskForDisableSet, FsIsisRRDProtoMaskForDisableTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisRRDRouteMapName}, GetNextIndexFsIsisExtSysTable, FsIsisRRDRouteMapNameGet, FsIsisRRDRouteMapNameSet, FsIsisRRDRouteMapNameTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisRRDImportLevel}, GetNextIndexFsIsisExtSysTable, FsIsisRRDImportLevelGet, FsIsisRRDImportLevelSet, FsIsisRRDImportLevelTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "2"},

{{13,FsIsisMultiTopologySupport}, GetNextIndexFsIsisExtSysTable, FsIsisMultiTopologySupportGet, FsIsisMultiTopologySupportSet, FsIsisMultiTopologySupportTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "2"},

{{13,FsIsisExtSysBfdSupport}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysBfdSupportGet, FsIsisExtSysBfdSupportSet, FsIsisExtSysBfdSupportTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "2"},

{{13,FsIsisExtSysBfdAllIfStatus}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysBfdAllIfStatusGet, FsIsisExtSysBfdAllIfStatusSet, FsIsisExtSysBfdAllIfStatusTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "2"},

{{13,FsIsisDotCompliance}, GetNextIndexFsIsisExtSysTable, FsIsisDotComplianceGet, FsIsisDotComplianceSet, FsIsisDotComplianceTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "2"},

{{13,FsIsisExtSysDynHostNameSupport}, GetNextIndexFsIsisExtSysTable, FsIsisExtSysDynHostNameSupportGet, FsIsisExtSysDynHostNameSupportSet, FsIsisExtSysDynHostNameSupportTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "2"},

{{13,FsIsisMetricStyle}, GetNextIndexFsIsisExtSysTable, FsIsisMetricStyleGet, FsIsisMetricStyleSet, FsIsisMetricStyleTest, FsIsisExtSysTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysTableINDEX, 1, 0, 0, "1"},

{{13,FsIsisExtSysAreaRxPasswd}, GetNextIndexFsIsisExtSysAreaRxPasswdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIsisExtSysAreaRxPasswdTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtSysAreaRxPasswdExistState}, GetNextIndexFsIsisExtSysAreaRxPasswdTable, FsIsisExtSysAreaRxPasswdExistStateGet, FsIsisExtSysAreaRxPasswdExistStateSet, FsIsisExtSysAreaRxPasswdExistStateTest, FsIsisExtSysAreaRxPasswdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysAreaRxPasswdTableINDEX, 2, 0, 1, NULL},

{{13,FsIsisExtSysDomainRxPassword}, GetNextIndexFsIsisExtSysDomainRxPasswdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIsisExtSysDomainRxPasswdTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtSysDomainRxPasswdExistState}, GetNextIndexFsIsisExtSysDomainRxPasswdTable, FsIsisExtSysDomainRxPasswdExistStateGet, FsIsisExtSysDomainRxPasswdExistStateSet, FsIsisExtSysDomainRxPasswdExistStateTest, FsIsisExtSysDomainRxPasswdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtSysDomainRxPasswdTableINDEX, 2, 0, 1, NULL},

{{13,FsIsisExtSysEventIdx}, GetNextIndexFsIsisExtSysEventTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtSysEventTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtSysEvent}, GetNextIndexFsIsisExtSysEventTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtSysEventTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtSysEventStr}, GetNextIndexFsIsisExtSysEventTable, FsIsisExtSysEventStrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsIsisExtSysEventTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtSysRouterInstance}, GetNextIndexFsIsisExtSysIdToSysNameMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtSysIdToSysNameMappingTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtSysRouterSysID}, GetNextIndexFsIsisExtSysIdToSysNameMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIsisExtSysIdToSysNameMappingTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtSysRouterHostName}, GetNextIndexFsIsisExtSysIdToSysNameMappingTable, FsIsisExtSysRouterHostNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsIsisExtSysIdToSysNameMappingTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtCircIndex}, GetNextIndexFsIsisExtCircTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtCircTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtCircIfStatus}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircIfStatusGet, FsIsisExtCircIfStatusSet, FsIsisExtCircIfStatusTest, FsIsisExtCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtCircTableINDEX, 2, 0, 0, "1"},

{{13,FsIsisExtCircTxEnable}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircTxEnableGet, FsIsisExtCircTxEnableSet, FsIsisExtCircTxEnableTest, FsIsisExtCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtCircTableINDEX, 2, 0, 0, "1"},

{{13,FsIsisExtCircRxEnable}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircRxEnableGet, FsIsisExtCircRxEnableSet, FsIsisExtCircRxEnableTest, FsIsisExtCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtCircTableINDEX, 2, 0, 0, "1"},

{{13,FsIsisExtCircTxISHs}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircTxISHsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtCircTableINDEX, 2, 0, 0, "0"},

{{13,FsIsisExtCircRxISHs}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircRxISHsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtCircTableINDEX, 2, 0, 0, "0"},

{{13,FsIsisExtCircSNPA}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircSNPAGet, FsIsisExtCircSNPASet, FsIsisExtCircSNPATest, FsIsisExtCircTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIsisExtCircTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtCircMTID}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircMTIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtCircTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtCircBfdStatus}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircBfdStatusGet, FsIsisExtCircBfdStatusSet, FsIsisExtCircBfdStatusTest, FsIsisExtCircTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtCircTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtCircExtendedCircID}, GetNextIndexFsIsisExtCircTable, FsIsisExtCircExtendedCircIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIsisExtCircTableINDEX, 2, 0, 0, NULL},

{{13,FsIsisExtCircLevelIndex}, GetNextIndexFsIsisExtCircLevelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIsisExtCircLevelTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtCircLevelDelayMetric}, GetNextIndexFsIsisExtCircLevelTable, FsIsisExtCircLevelDelayMetricGet, FsIsisExtCircLevelDelayMetricSet, FsIsisExtCircLevelDelayMetricTest, FsIsisExtCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtCircLevelTableINDEX, 3, 0, 0, "0"},

{{13,FsIsisExtCircLevelErrorMetric}, GetNextIndexFsIsisExtCircLevelTable, FsIsisExtCircLevelErrorMetricGet, FsIsisExtCircLevelErrorMetricSet, FsIsisExtCircLevelErrorMetricTest, FsIsisExtCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtCircLevelTableINDEX, 3, 0, 0, "0"},

{{13,FsIsisExtCircLevelExpenseMetric}, GetNextIndexFsIsisExtCircLevelTable, FsIsisExtCircLevelExpenseMetricGet, FsIsisExtCircLevelExpenseMetricSet, FsIsisExtCircLevelExpenseMetricTest, FsIsisExtCircLevelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtCircLevelTableINDEX, 3, 0, 0, "0"},

{{13,FsIsisExtCircLevelTxPassword}, GetNextIndexFsIsisExtCircLevelTable, FsIsisExtCircLevelTxPasswordGet, FsIsisExtCircLevelTxPasswordSet, FsIsisExtCircLevelTxPasswordTest, FsIsisExtCircLevelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIsisExtCircLevelTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtCircLevelWideMetric}, GetNextIndexFsIsisExtCircLevelTable, FsIsisExtCircLevelWideMetricGet, FsIsisExtCircLevelWideMetricSet, FsIsisExtCircLevelWideMetricTest, FsIsisExtCircLevelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIsisExtCircLevelTableINDEX, 3, 0, 0, "10"},

{{13,FsIsisExtCircLevelAuthType}, GetNextIndexFsIsisExtCircLevelTable, FsIsisExtCircLevelAuthTypeGet, FsIsisExtCircLevelAuthTypeSet, FsIsisExtCircLevelAuthTypeTest, FsIsisExtCircLevelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtCircLevelTableINDEX, 3, 0, 0, "0"},

{{13,FsIsisExtIPRAType}, GetNextIndexFsIsisExtIPRATable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIsisExtIPRATableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtIPRAIndex}, GetNextIndexFsIsisExtIPRATable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtIPRATableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtIPRADelayMetric}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRADelayMetricGet, FsIsisExtIPRADelayMetricSet, FsIsisExtIPRADelayMetricTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, "0"},

{{13,FsIsisExtIPRAErrorMetric}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRAErrorMetricGet, FsIsisExtIPRAErrorMetricSet, FsIsisExtIPRAErrorMetricTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, "0"},

{{13,FsIsisExtIPRAExpenseMetric}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRAExpenseMetricGet, FsIsisExtIPRAExpenseMetricSet, FsIsisExtIPRAExpenseMetricTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, "0"},

{{13,FsIsisExtIPRADelayMetricType}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRADelayMetricTypeGet, FsIsisExtIPRADelayMetricTypeSet, FsIsisExtIPRADelayMetricTypeTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtIPRAErrorMetricType}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRAErrorMetricTypeGet, FsIsisExtIPRAErrorMetricTypeSet, FsIsisExtIPRAErrorMetricTypeTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtIPRAExpenseMetricType}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRAExpenseMetricTypeGet, FsIsisExtIPRAExpenseMetricTypeSet, FsIsisExtIPRAExpenseMetricTypeTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtIPRANextHopType}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRANextHopTypeGet, FsIsisExtIPRANextHopTypeSet, FsIsisExtIPRANextHopTypeTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtIPRANextHop}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRANextHopGet, FsIsisExtIPRANextHopSet, FsIsisExtIPRANextHopTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtIPRAFullMetric}, GetNextIndexFsIsisExtIPRATable, FsIsisExtIPRAFullMetricGet, FsIsisExtIPRAFullMetricSet, FsIsisExtIPRAFullMetricTest, FsIsisExtIPRATableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIsisExtIPRATableINDEX, 3, 0, 0, "20"},

{{13,FsIsisExtCircLevelRxPassword}, GetNextIndexFsIsisExtCircLevelRxPasswordTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIsisExtCircLevelRxPasswordTableINDEX, 4, 0, 0, NULL},

{{13,FsIsisExtCircLevelRxPasswordExistState}, GetNextIndexFsIsisExtCircLevelRxPasswordTable, FsIsisExtCircLevelRxPasswordExistStateGet, FsIsisExtCircLevelRxPasswordExistStateSet, FsIsisExtCircLevelRxPasswordExistStateTest, FsIsisExtCircLevelRxPasswordTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtCircLevelRxPasswordTableINDEX, 4, 0, 1, NULL},

{{13,FsIsisExtSummAddressType}, GetNextIndexFsIsisExtSummAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIsisExtSummAddrTableINDEX, 4, 0, 0, NULL},

{{13,FsIsisExtSummAddress}, GetNextIndexFsIsisExtSummAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIsisExtSummAddrTableINDEX, 4, 0, 0, NULL},

{{13,FsIsisExtSummAddrPrefixLen}, GetNextIndexFsIsisExtSummAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsIsisExtSummAddrTableINDEX, 4, 0, 0, NULL},

{{13,FsIsisExtSummAddrDelayMetric}, GetNextIndexFsIsisExtSummAddrTable, FsIsisExtSummAddrDelayMetricGet, FsIsisExtSummAddrDelayMetricSet, FsIsisExtSummAddrDelayMetricTest, FsIsisExtSummAddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSummAddrTableINDEX, 4, 0, 0, "0"},

{{13,FsIsisExtSummAddrErrorMetric}, GetNextIndexFsIsisExtSummAddrTable, FsIsisExtSummAddrErrorMetricGet, FsIsisExtSummAddrErrorMetricSet, FsIsisExtSummAddrErrorMetricTest, FsIsisExtSummAddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSummAddrTableINDEX, 4, 0, 0, "0"},

{{13,FsIsisExtSummAddrExpenseMetric}, GetNextIndexFsIsisExtSummAddrTable, FsIsisExtSummAddrExpenseMetricGet, FsIsisExtSummAddrExpenseMetricSet, FsIsisExtSummAddrExpenseMetricTest, FsIsisExtSummAddrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtSummAddrTableINDEX, 4, 0, 0, "0"},

{{13,FsIsisExtSummAddrFullMetric}, GetNextIndexFsIsisExtSummAddrTable, FsIsisExtSummAddrFullMetricGet, FsIsisExtSummAddrFullMetricSet, FsIsisExtSummAddrFullMetricTest, FsIsisExtSummAddrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIsisExtSummAddrTableINDEX, 4, 0, 0, "20"},

{{13,FsIsisExtIPIfIndex}, GetNextIndexFsIsisExtIPIfAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtIPIfAddrTableINDEX, 5, 0, 0, NULL},

{{13,FsIsisExtIPIfSubIndex}, GetNextIndexFsIsisExtIPIfAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtIPIfAddrTableINDEX, 5, 0, 0, NULL},

{{13,FsIsisExtIPIfAddrType}, GetNextIndexFsIsisExtIPIfAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIsisExtIPIfAddrTableINDEX, 5, 0, 0, NULL},

{{13,FsIsisExtIPIfAddr}, GetNextIndexFsIsisExtIPIfAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIsisExtIPIfAddrTableINDEX, 5, 0, 0, NULL},

{{13,FsIsisExtIPIfExistState}, GetNextIndexFsIsisExtIPIfAddrTable, FsIsisExtIPIfExistStateGet, FsIsisExtIPIfExistStateSet, FsIsisExtIPIfExistStateTest, FsIsisExtIPIfAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisExtIPIfAddrTableINDEX, 5, 0, 1, NULL},

{{13,FsIsisExtLogModId}, GetNextIndexFsIsisExtLogTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIsisExtLogTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtLogLevel}, GetNextIndexFsIsisExtLogTable, FsIsisExtLogLevelGet, FsIsisExtLogLevelSet, FsIsisExtLogLevelTest, FsIsisExtLogTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisExtLogTableINDEX, 1, 0, 0, NULL},

{{13,FsIsisExtAdjIndex}, GetNextIndexFsIsisExtAdjTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisExtAdjTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtAdjNeighSysID}, GetNextIndexFsIsisExtAdjTable, FsIsisExtAdjNeighSysIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsIsisExtAdjTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtAdjHelperStatus}, GetNextIndexFsIsisExtAdjTable, FsIsisExtAdjHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIsisExtAdjTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtAdjHelperExitReason}, GetNextIndexFsIsisExtAdjTable, FsIsisExtAdjHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIsisExtAdjTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtAdjMTID}, GetNextIndexFsIsisExtAdjTable, FsIsisExtAdjMTIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsIsisExtAdjTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtAdjHostName}, GetNextIndexFsIsisExtAdjTable, FsIsisExtAdjHostNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsIsisExtAdjTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtISAdj3WayState}, GetNextIndexFsIsisExtAdjTable, FsIsisExtISAdj3WayStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsIsisExtAdjTableINDEX, 3, 0, 0, NULL},

{{13,FsIsisExtISAdjNbrExtendedCircID}, GetNextIndexFsIsisExtAdjTable, FsIsisExtISAdjNbrExtendedCircIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsIsisExtAdjTableINDEX, 3, 0, 0, NULL},

{{12,FsIsisDistInOutRouteMapName}, GetNextIndexFsIsisDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsIsisDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsIsisDistInOutRouteMapType}, GetNextIndexFsIsisDistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsIsisDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsIsisDistInOutRouteMapValue}, GetNextIndexFsIsisDistInOutRouteMapTable, FsIsisDistInOutRouteMapValueGet, FsIsisDistInOutRouteMapValueSet, FsIsisDistInOutRouteMapValueTest, FsIsisDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisDistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{12,FsIsisDistInOutRouteMapRowStatus}, GetNextIndexFsIsisDistInOutRouteMapTable, FsIsisDistInOutRouteMapRowStatusGet, FsIsisDistInOutRouteMapRowStatusSet, FsIsisDistInOutRouteMapRowStatusTest, FsIsisDistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisDistInOutRouteMapTableINDEX, 3, 0, 1, NULL},

{{12,FsIsisPreferenceValue}, GetNextIndexFsIsisPreferenceTable, FsIsisPreferenceValueGet, FsIsisPreferenceValueSet, FsIsisPreferenceValueTest, FsIsisPreferenceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsIsisPreferenceTableINDEX, 1, 0, 0, "120"},

{{12,FsIsisPreferenceRowStatus}, GetNextIndexFsIsisPreferenceTable, FsIsisPreferenceRowStatusGet, FsIsisPreferenceRowStatusSet, FsIsisPreferenceRowStatusTest, FsIsisPreferenceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsIsisPreferenceTableINDEX, 1, 0, 1, NULL},

{{12,FsIsisRRDMetricProtocolId}, GetNextIndexFsIsisRRDMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsIsisRRDMetricTableINDEX, 2, 0, 0, NULL},

{{12,FsIsisRRDMetricValue}, GetNextIndexFsIsisRRDMetricTable, FsIsisRRDMetricValueGet, FsIsisRRDMetricValueSet, FsIsisRRDMetricValueTest, FsIsisRRDMetricTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsIsisRRDMetricTableINDEX, 2, 0, 0, NULL},
};
tMibData fsisisEntry = { (sizeof (fsisisMibEntry) / sizeof (tMbDbEntry)) , fsisisMibEntry };
#endif /* _FSISISDB_H */

