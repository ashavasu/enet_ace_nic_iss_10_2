/*******************************************************************************
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 * $Id: istincl.h,v 1.6 2014/11/11 13:01:01 siva Exp $
 *
 * Description: This file contains the all the type definition used by ISIS
 *
 ******************************************************************************/

#ifndef __ISTINCL_H__
#define __ISTINCL_H__

typedef struct IsisSysCfg              tIsisSysCfg;
typedef struct IsisFLTHdr              tIsisFLTHdr;
typedef struct IsisSysLevelInfo        tIsisSysLevelInfo;
typedef struct IsisTCInfo              tIsisTCInfo ;
typedef struct IsisTmrECBlk            tIsisTmrECBlk ;
typedef struct IsisRouteInfo           tIsisRouteInfo ;
typedef struct IsisComHdr              tIsisComHdr ;
typedef struct IsisRestartTLV          tIsisRestartTLV ;
typedef struct IsisHashTable           tIsisHashTable ;
typedef struct IsisSortedHashTable     tIsisSortedHashTable ;
typedef struct IsisHashBucket          tIsisHashBucket ;
typedef struct IsisMsg                 tIsisMsg ;
typedef struct IsisTimer               tIsisTimer ;
typedef struct IsisSysTimers           tIsisSysTimers ;
typedef struct IsisEventTable          tIsisEventTable ;
typedef struct IsisEvent               tIsisEvent ;
typedef struct IsisSysContext          tIsisSysContext ;
typedef struct IsisProtSupp            tIsisProtSupp ;
typedef struct IsisMDT                 tIsisMDT ;
typedef struct IsisSelfLSP             tIsisSelfLSP ;
typedef struct IsisSNLSP               tIsisSNLSP ;
typedef struct IsisNSNLSP              tIsisNSNLSP ;
typedef struct IsisLSPInfo             tIsisLSPInfo ;
typedef struct IsisSPTNode             tIsisSPTNode ;
typedef struct IsisLSPTxEntry          tIsisLSPTxEntry ;
typedef struct IsisSATable             tIsisSATable ;
typedef struct IsisSAEntry             tIsisSAEntry ;
typedef struct IsisAATable             tIsisAATable ;
typedef struct IsisAAEntry             tIsisAAEntry ;
typedef struct IsisMAATable            tIsisMAATable ;
typedef struct IsisMAAEntry            tIsisMAAEntry ;
typedef struct IsisIPRAInfo            tIsisIPRAInfo ;
typedef struct IsisIPRAEntry           tIsisIPRAEntry ;
typedef struct IsisCktTable            tIsisCktTable ;
typedef struct IsisCktEntry            tIsisCktEntry ;
typedef struct IsisAdjEntry            tIsisAdjEntry ;
typedef struct IsisAdjAAEntry          tIsisAdjAAEntry ;
typedef struct IsisAdjDirTable         tIsisAdjDirTable ;
typedef struct IsisAdjDirEntry         tIsisAdjDirEntry ;
typedef struct IsisCktLevel            tIsisCktLevel ;
typedef struct IsisLSPEntry            tIsisLSPEntry ;
typedef struct IsisIPIfAddrTable       tIsisIPIfAddrTable ;
typedef struct IsisIPIfAddr            tIsisIPIfAddr;
typedef struct IsisCktStats            tIsisCktStats ;
typedef struct IsisPktCount            tIsisPktCount ;
typedef struct IsisSysStats            tIsisSysStats ;
typedef struct IsisSysLevelStats       tIsisSysLevelStats ;
typedef struct IsisSysConfigs          tIsisSysConfigs ;
typedef struct IsisSysActuals          tIsisSysActuals ;
typedef struct IsisMemConfigs          tIsisMemConfigs ;
typedef struct IsisTLV                 tIsisTLV ;
typedef struct IsisIPRATLV             tIsisIPRATLV ;
typedef struct IsisRRDRoutes           tIsisRRDRoutes ;
typedef struct IsisPasswd              tIsisPasswd ;
typedef struct IsisIpAddr              tIsisIpAddr ;
typedef struct IsisAreaAddrs           tIsisAreaAddr ;
typedef struct IsisLETLV               tIsisLETLV;
typedef struct IsisMemPoolId           tIsisMemPoolId ;
typedef struct IsisExtInfo             tIsisExtInfo;
typedef struct IsisLSPTxTable          tIsisLSPTxTable;
typedef struct IsisLLHdr               tIsisLLHdr;
typedef struct IsisNotifyTable         tIsisNotifyTable;
typedef struct IsisLLInfo              tIsisLLInfo;
typedef struct IsisLLData              tIsisLLData;
typedef struct IsisAdjDelDirEntry   tIsisAdjDelDirEntry;
typedef struct IsisRRDConfig       tIsisRRDConfig;

#endif
