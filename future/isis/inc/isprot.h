/*******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isprot.h,v 1.39 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file contains all the function prototypes
 *
 ******************************************************************************/

#ifndef __ISPROT_H__
#define __ISPROT_H__


#include  "rtm.h"
PUBLIC VOID IsisGetDataFromQBuf (tIsisQBuf *, UINT1 **, UINT4 *);

/* Debug Module Function Prototypes
 */

PUBLIC VOID IsisDbgPrintHelloPDU (tIsisSysContext*, UINT1*);
PUBLIC VOID IsisDbgPrintLinkStatePDU (tIsisSysContext*, UINT1*);
PUBLIC VOID IsisDbgPrintCSNP (tIsisSysContext*, UINT1*);
PUBLIC VOID IsisDbgPrintPSNP (tIsisSysContext*, UINT1*);
PUBLIC VOID IsisDbgPrintCktTable (tIsisSysContext *pContext);
PUBLIC VOID IsisDbgPrintOctets (UINT1*, UINT1, UINT1*, UINT1);
PUBLIC VOID IsisDbgPrintOctetString (UINT1*, UINT1, UINT1*, UINT1);
PUBLIC VOID IsisDbgPrintLSPDB (tIsisSysContext *, UINT1);
PUBLIC VOID IsisDbgPrintPath (tIsisSysContext *, UINT1, UINT1);
PUBLIC VOID IsisDbgPrintIsisInfo (UINT1);
PUBLIC VOID IsisDbgPrintSysTable (VOID);
PUBLIC VOID IsisDbgPrintIPRATable (tIsisSysContext *);
PUBLIC VOID IsisDbgPrintSATable (tIsisSysContext *);
PUBLIC VOID IsisDbgPrintAdjTable (tIsisSysContext *);
PUBLIC VOID IsisDbgPrintLSPInfo (tIsisSysContext *, UINT1 *);
PUBLIC VOID IsisDbgPrintCktLvlTable (tIsisCktLevel *);
PUBLIC VOID IsisDbgPrintTime (char *);
PUBLIC VOID IsisDbgPrintHEXDMP (UINT4, UINT1*);
PUBLIC VOID IsisDbgPrintCommHdr (tIsisSysContext *,  tIsisComHdr *);

/* Timer Module Function prototypes 
 */ 

PUBLIC VOID IsisTmrSetECBlk (tIsisSysContext *, UINT1 , tIsisTmrECBlk *);
PUBLIC INT4 IsisTmrCreateTimerList (VOID);
PUBLIC VOID IsisTmrStartTimer (tIsisTimer *, eIsisTimerId , UINT4);
PUBLIC VOID IsisTmrStopTimer (tIsisTimer *);
PUBLIC VOID IsisTmrRestartTimer (tIsisTimer *, eIsisTimerId , UINT4);
PUBLIC VOID IsisTmrProcTimeOut (VOID);
PUBLIC VOID IsisTmrProcECTimeOut (tIsisSysContext *); 
PUBLIC VOID IsisTmrStartECTimer (tIsisSysContext *, UINT1, UINT4, UINT4, 
                                 UINT1*);
PUBLIC VOID IsisTmrStopECTimer (tIsisSysContext *pContext, UINT1 u1Type, 
                                UINT4 u4Id, UINT1);
PUBLIC VOID IsisTmrRestartECTimer (tIsisSysContext *pContext, UINT1 u1Type, 
                                  UINT4 u4Id, UINT4 u4Dur, UINT1 *);
PUBLIC INT4 IsisTmrGetECBlk (tIsisSysContext *, UINT4, UINT1,
                             tIsisTmrECBlk **, UINT1 *);
PUBLIC VOID IsisTmrDeAllocTmrECB (tIsisTmrECBlk *[], UINT1);
PUBLIC VOID IsisTmrDelinkECTimer (tIsisTmrECBlk *[], UINT1);
PUBLIC VOID IsisTmrDelLSPTmrLinks (tIsisLSPEntry *);

/* Adjacency Module Function Prototypes
 */
PUBLIC INT1 IsisAdjUpdateIpAddr (UINT1 *pu1PDU,
                   tIsisAdjEntry * pAdjRec, tBool *pbLSUFlag);

PUBLIC INT4 IsisAdjGetCktRecWithLLHandle (UINT2 , tIsisCktEntry ** );
PUBLIC VOID IsisAdjProcCtrlPkt (tIsisMsg*);
PUBLIC VOID IsisAdjProcHelloTimeOut (tIsisSysContext*, tIsisTmrECBlk*, UINT1);
PUBLIC VOID IsisAdjProcISHTimeOut (tIsisSysContext*, tIsisTmrECBlk*);
PUBLIC VOID IsisAdjProcHoldingTimeOut (tIsisSysContext*, tIsisTmrECBlk*);
PUBLIC INT4 IsisAdjAreaAddrMatchCheck (tIsisSysContext*, UINT1*, UINT1);
PUBLIC INT4 IsisUtlMaxAreaAddrCheck (tIsisSysContext*, tIsisCktEntry *, UINT1*);
PUBLIC VOID IsisAdjBldAdjRecFromPdu (tIsisCktEntry*, UINT1*, UINT1, UINT1*, 
                                     UINT1, tIsisAdjEntry*);
PUBLIC VOID IsisAdjAddAdj (tIsisSysContext*, tIsisCktEntry*, tIsisAdjEntry*,
                           UINT1);
PUBLIC VOID IsisAdjTxHello (tIsisSysContext *, tIsisCktEntry *, UINT1, tBool);
PUBLIC VOID IsisAdjUpdtAdjRec (tIsisAdjEntry*, tIsisAdjEntry*); 
PUBLIC VOID IsisAdjAddDirEntry (tIsisSysContext*, tIsisAdjEntry*, 
                                tIsisAdjDirEntry*);
PUBLIC VOID IsisAdjDelAdj (tIsisSysContext*, tIsisCktEntry*, tIsisAdjEntry*, 
                           UINT1, UINT4*);
PUBLIC VOID IsisAdjDelP2PAdj (tIsisSysContext*, tIsisCktEntry*, tIsisAdjEntry*, 
                           UINT1, UINT4*);
PUBLIC VOID IsisAdjDelDirEntry (tIsisSysContext*, tIsisAdjEntry*, UINT4*);
PUBLIC VOID IsisAdjAddAdjAA (tIsisAdjEntry*, tIsisAdjAAEntry*); 
PUBLIC VOID IsisAdjDelAdjAA (tIsisAdjEntry*, UINT1*);
PUBLIC VOID IsisAdjModifyP2PAdj (UINT1*, tIsisCktEntry*, tIsisAdjEntry*, 
                                 UINT1); 
PUBLIC INT4 IsisAdjValP2PHelloSelfL1 (tIsisCktEntry*, UINT1*,  UINT1);
PUBLIC INT4 IsisAdjValP2PHelloSelfL12 (tIsisCktEntry*, UINT1*, UINT1);
PUBLIC INT4 IsisAdjValAAMismatchHello (tIsisCktEntry*, UINT1*, UINT1);
PUBLIC VOID IsisAdjModifyLANAdj (UINT1*, tIsisCktEntry*, tIsisAdjEntry*, 
                                 UINT1, UINT1*, UINT1*);
PUBLIC INT4 IsisAdjCheckLANAdjExist (tIsisCktEntry*, UINT1*, UINT1, 
                                     tIsisAdjEntry**);
PUBLIC tBool IsisAdjSelfLANAddrListed (tIsisCktEntry*, UINT1*);
PUBLIC VOID IsisAdjPruneAdjs (tIsisCktEntry*, tIsisAdjEntry**);
PUBLIC VOID IsisAdjEncodeISH (tIsisSysContext*, tIsisCktEntry*, UINT1*); 
PUBLIC INT4 IsisAdjEncodeHelloPdu (tIsisCktEntry*, tIsisCktLevel*,
                                   UINT1, UINT1*, UINT2, UINT2*);
PUBLIC VOID IsisAdjBldHelloHdr (tIsisSysContext*, tIsisCktEntry*, 
                                tIsisCktLevel*, UINT1*, UINT2*);
PUBLIC INT4 IsisAdjBldISNbrTlv (tIsisCktEntry*, UINT1*, UINT2,
                                UINT2*);
PUBLIC INT4 IsisAdjBldAreaAddrTlv (tIsisSysContext*, UINT1*, UINT2, UINT2*);
PUBLIC INT4 IsisAdjBldPSTlv (UINT1*, UINT2*, UINT1);
PUBLIC INT4 IsisAdjBldIPV4IfTlv (tIsisSysContext*, tIsisCktEntry *, UINT1*, 
                               UINT2, UINT2*);
PUBLIC INT4 IsisAdjBldIPV6IfTlv (tIsisSysContext*, tIsisCktEntry *, UINT1*, 
                               UINT2, UINT2*);
PUBLIC INT4 IsisAdjBldMTTlv (tIsisCktEntry *, UINT1*, UINT2, UINT2*);
PUBLIC INT4 IsisAdjBldP2PThreeWayHndShkTLV (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                 UINT2 u2BufSize, UINT2 *pu2Offset);
PUBLIC VOID IsisAdjUpdateNbrThreeWayInfoinAdj (tIsisAdjEntry *pAdjRec, UINT1 *pu1PDU);

#ifdef BFD_WANTED
PUBLIC INT4 IsisAdjBldBFDEnabledTlv (tIsisCktEntry *, UINT1 *, UINT2, UINT2 *);
#endif
PUBLIC VOID IsisAdjEnqueueAdjChgEvt (tIsisCktEntry*, tIsisCktLevel*, 
                                     tIsisAdjEntry*, UINT1);
PUBLIC VOID IsisAdjEnqueueWrongSysEvt (tIsisSysContext*, UINT1*);
PUBLIC VOID IsisAdjEnqueueIncompCktEvt (tIsisSysContext*, tIsisCktEntry*);
PUBLIC VOID IsisAdjRemAdj (tIsisCktEntry*, tIsisAdjEntry*); 
PUBLIC VOID IsisAdjEnqueueDupSysEvt (tIsisAdjEntry*);
PUBLIC VOID IsisAdjEnqueueDISChgEvt (tIsisCktEntry*, UINT1, UINT1*, 
                                     UINT1*);
PUBLIC VOID IsisAdjEnqueueDISStatChgEvt (tIsisCktEntry*,  UINT1, UINT1);
PUBLIC VOID IsisAdjEnqueueDISNotElectEvt (tIsisCktEntry*, tIsisAdjEntry*,
                                          UINT1);
PUBLIC INT4 IsisAdjGetCktRec (tIsisSysContext*, UINT4, tIsisCktEntry**); 
PUBLIC VOID IsisAdjDelCktAdjs (tIsisSysContext*, tIsisCktEntry *, UINT1); 
PUBLIC VOID IsisAdjAddCkt (tIsisSysContext*, tIsisCktEntry*); 
PUBLIC INT4 IsisAdjDelCkt (tIsisSysContext*, UINT4); 
PUBLIC VOID IsisAdjUpdtCktRec (tIsisCktEntry*, tIsisCktEntry*); 
PUBLIC VOID IsisAdjAddCktLvlRec (tIsisCktEntry*, tIsisCktLevel*); 
PUBLIC VOID IsisAdjDelCktLvlRec (tIsisSysContext*, tIsisCktEntry*, UINT1); 
PUBLIC VOID IsisAdjUpdtCktLvlRec (tIsisCktLevel*, tIsisCktLevel*);
PUBLIC INT4 IsisAdjGetAdjRec (tIsisCktEntry*, UINT4, tIsisAdjEntry**); 
PUBLIC INT4 IsisAdjGetSelfAdjs (tIsisSysContext*, UINT1, UINT1, 
                                tIsisSPTNode**, UINT1);
PUBLIC tIsisAdjEntry* IsisAdjGetAdjRecFromDirIdx (tIsisSysContext*, UINT4); 

PUBLIC INT4 IsisAdjGetCktRecWithIfIdx (UINT4, UINT4, tIsisCktEntry**);
PUBLIC VOID IsisAdjGetNextAdjIdx (tIsisCktEntry*, UINT4*);
PUBLIC VOID IsisAdjGetNextCktIdx (tIsisSysContext*, UINT4*);
PUBLIC VOID IsisAdjGetNextDirIdx (tIsisSysContext*, UINT4*);
PUBLIC INT4 IsisAdjGetCktAdjs (tIsisSysContext*, tIsisCktEntry*,
                               UINT1, tIsisSPTNode**);
PUBLIC UINT4 IsisAdjGetDirEntryWithAdjIdx (tIsisSysContext *, tIsisAdjEntry *);

PUBLIC INT4 IsisAdjAddAuthToPDU (tIsisCktLevel*, UINT1*, UINT2,
                                 UINT2*);
PUBLIC INT4 IsisAdjGetPNodes (tIsisSysContext*, UINT1, tIsisSPTNode**);
PUBLIC VOID IsisAdjDelCktLvlPSNPs (tIsisCktLevel *);
PUBLIC INT4 IsisAdjCopyCktDefVal (tIsisSysContext *, tIsisCktEntry *);
PUBLIC INT4 IsisAdjInitCktLvl (tIsisSysContext * , tIsisCktEntry *, UINT1);
PUBLIC INT4 IsisAdjActivateCktLvl (tIsisSysContext *, tIsisCktEntry *);
PUBLIC VOID IsisAdjInitHelloTmInt (tIsisCktEntry *);
PUBLIC VOID IsisAdjBldAdjRecFromISH (tIsisCktEntry *, UINT1 *,
                                     UINT1, UINT1, tIsisAdjEntry *);

PUBLIC VOID IsisAdjDelAdjAreaAddrs (tIsisAdjEntry *);
PUBLIC VOID IsisAdjEnqueueDISElectEvt(tIsisCktEntry *, tIsisAdjEntry *);
PUBLIC INT4 IsisAdjIsValidAdj (tIsisCktEntry *, UINT1 *, UINT1);
PUBLIC VOID IsisAdjDelPSNPNode (tIsisCktLevel *, UINT1 *);
PUBLIC VOID IsisAdjAddPSNP (tIsisCktEntry *, tIsisLETLV *, UINT1);
PUBLIC VOID IsisAdjProcISDownEvt (tIsisSysContext *);
PUBLIC VOID IsisP2PHandleThreeWayHandshakeStateChange (tIsisCktEntry *pCktRec, 
       UINT1 u1PeerThreeWayState);
PUBLIC VOID IsisP2PDelAdj (tIsisCktEntry * pCktRec);
PUBLIC VOID IsisAdjP2PHandleStateChange (tIsisCktEntry * pCktRec, tIsisAdjEntry *pAdjRec, UINT1 *pu1PDU);
PUBLIC VOID IsisAdjModifyAdjAreaAddr (UINT1 *, tIsisCktEntry *,
                                              tIsisAdjEntry *);
PUBLIC INT4 IsisAdjValAAMismatchHelloThreeWay (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
            UINT1 u1CktType);
PUBLIC INT4 IsisAdjValP2PThreewayHelloSelfL12MatAA (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                           UINT1 u1CktType);
PUBLIC INT4 IsisAdjValP2PThreewayHelloSelfL1MatAA (tIsisCktEntry * pCktRec, UINT1 *pu1PDU,
                           UINT1 u1CktType);
PUBLIC VOID IsisAdjPerformP2PTwoWayHndshk (tIsisCktEntry * pCktRec, UINT1 *pu1PDU, INT4 i4AAChkFlag);
PUBLIC VOID IsisAdjPerformP2PThreeWayHndshk (tIsisCktEntry * pCktRec, UINT1 *pu1PDU, INT4 i4AAChkFlag);
PUBLIC INT4 IsisAdjP2PChkandCmnProcIIH (tIsisCktEntry * pCktRec, UINT1 *pu1PDU, INT4 *pi4AAChkFlag);
PUBLIC VOID IsisAdjP2PTwoWayDecision (tIsisCktEntry * pCktRec, UINT1 *pu1PDU, INT4  i4AAChkFlag);
PUBLIC VOID IsisAdjP2PModifyThreeWayAdj (tIsisCktEntry * pCktRec, tIsisAdjEntry *pAdjEntry, UINT1 *pu1PDU);
PUBLIC VOID IsisAdjAddP2PThreewayAdj (tIsisSysContext  *pContext,tIsisCktEntry  *pCktRec, tIsisAdjEntry *pAdjRec);
/* Update Module Function Prototypes
 */ 

PUBLIC INT4        IsisAdjDISElect (tIsisSysContext *, tIsisCktEntry *, UINT1);
/* Enqueue Evnt*/
PUBLIC VOID IsisUpdEnqueueP2PCktChgEvt (tIsisSysContext * pContext, UINT1 u1State, UINT4 u4CktId);
/* Event Processing functions
 */

PUBLIC VOID IsisUpdNWPurgeLSP (tIsisSysContext *, UINT1 *);
PUBLIC VOID IsisUpdProcLSPDBCorp (tIsisSysContext *, tIsisEvtCorrLsp *);
PUBLIC VOID IsisUpdAddAuthToPDU (tIsisSysContext * , UINT1 *, UINT2 *, UINT1 );
PUBLIC VOID IsisUpdProcISUpEvt (tIsisSysContext *);
PUBLIC VOID IsisUpdProcDBOLEvt (tIsisSysContext *, tIsisEvtLSPDBOL *);
PUBLIC VOID IsisUpdProcDBOLClearEvt (tIsisSysContext *, tIsisEvtLSPDBOL *); 
PUBLIC VOID IsisUpdProcISDownEvt (tIsisSysContext *);
PUBLIC VOID IsisUpdProcMAAChgEvt (tIsisSysContext *,
                                  tIsisEvtManAAChange *);
PUBLIC VOID IsisUpdProcPSChgEvt (tIsisSysContext *,
                                 tIsisEvtPSChange *);
PUBLIC VOID IsisUpdProcSummAddrChgEvt (tIsisSysContext *,
                                       tIsisEvtSummAddrChg *);
PUBLIC VOID IsisUpdProcL1OrL2DecnCompleteEvt (tIsisSysContext *,UINT1);
PUBLIC VOID IsisUpdProcDistanceEvt(tIsisSysContext *,UINT1 ,UINT1 ,UINT1 );
PUBLIC VOID IsisUpdHandleP2PCircuitChange (tIsisSysContext * pContext, 
            UINT1 u1State, UINT4 u4CktId);
PUBLIC INT4 IsisUpdSchLSPsForTxOnP2PCirc (tIsisCktEntry  *pCktRec,UINT1 u1Level);
PUBLIC VOID IsisUpdRemoveAuthFromLSP (UINT1 *, UINT2 *);

/* TLV Processing functions
 */

PUBLIC VOID IsisUpdAddProtSuppTLV (tIsisSysContext *, tIsisMDT *);
PUBLIC INT4 IsisUpdAddLSPBufSizeTLV (tIsisSysContext *, UINT1 );
PUBLIC INT4 IsisUpdAddIPRATLV (tIsisSysContext *, tIsisMDT *, UINT1, UINT1);
PUBLIC VOID IsisUpdAddIPRAFromPath (tIsisSysContext *, UINT1);
PUBLIC VOID IsisUpdDelIPRAFromPath (tIsisSysContext *);
PUBLIC VOID IsisUpdSummarize (tIsisSysContext *pContext,
                              tIsisEvtSummAddrChg *, UINT1);
PUBLIC VOID IsisUpdDeSummarize (tIsisSysContext *, 
                                tIsisEvtSummAddrChg *, UINT1);
PUBLIC INT4 IsisUpdModIPRATLV (tIsisSysContext *, tIsisMDT *, UINT1);
PUBLIC UINT1 IsisUpdCompIPAddrWithTLV (UINT1 *, UINT1 *, UINT1,UINT1);

PUBLIC INT4 IsisUpdDelIPRATLV (tIsisSysContext *, tIsisMDT *, UINT1, UINT1);
PUBLIC VOID IsisUpdUpdateIPRA (tIsisSysContext *, tIsisIPRAEntry *, UINT1);
PUBLIC INT4 IsisUpdModISAdjTLV (tIsisSysContext *, tIsisMDT *, UINT4);
PUBLIC INT4 IsisUpdAddAreaAddrTLV (tIsisSysContext *, tIsisMDT *);


PUBLIC INT4 IsisUpdDelISAdjTLV (tIsisSysContext *, tIsisMDT *, UINT4);
PUBLIC INT4 IsisUpdAddISAdjTLV (tIsisSysContext *, tIsisMDT *, UINT4);

PUBLIC VOID IsisUpdPurgeLSPs (tIsisSysContext*, UINT1);

PUBLIC VOID IsisUpdStopCSNPTimer (tIsisSysContext *, tIsisCktEntry *, UINT1);
PUBLIC UINT1 IsisUpdCompAAFromBuf (tIsisSysContext *, UINT1 *, UINT1 ); 
PUBLIC VOID IsisUpdProcMSNExceeded (tIsisSysContext *, UINT1, UINT1 *);

PUBLIC INT4 IsisUpdConstructCSNP (tIsisSysContext *, tIsisLSPTxEntry **,
                                  tIsisLSPEntry **, UINT1 **, UINT1);

PUBLIC VOID IsisUpdChkAndTxSelfLSP (tIsisSysContext *, tIsisLSPInfo *, UINT1 );
PUBLIC UINT1
IsUpdAddAuthTlvPrgLSP(tIsisSysContext * pContext, UINT1 *pu1LSP,
                      UINT1 u1Level, UINT4 u4RcvdSeqNum);

PUBLIC UINT1 IsisUpdClearSRM (tIsisSysContext *, tIsisLSPTxEntry *, 
                              tIsisLSPTxEntry *, tIsisCktEntry *, UINT1 , 
                              tIsisHashBucket *, UINT1 ); 

PUBLIC INT4 IsisUpdGetAdjsWithSysId (tIsisSysContext *, UINT1 *, UINT1, UINT1,
                                     UINT1, tIsisSPTNode **pAdjList, 
                                     UINT1 *, UINT1); 
PUBLIC UINT1 IsisUpdStartCSNPTimer (tIsisSysContext *, tIsisCktEntry *, 
                                    tIsisLSPTxEntry *, tIsisLSPEntry *,
                                    UINT2, UINT1);

PUBLIC INT4 IsisUpdFindAndPurgeSelfLSP (tIsisSysContext*,tIsisLSPInfo *,UINT1);
PUBLIC VOID IsisUpdDelTlvs (tIsisLSPInfo *);
PUBLIC INT4 IsisUpdSchLSPForTx(tIsisSysContext *, VOID *,
                                VOID *, tIsisCktEntry *, 
                                UINT1 , tIsisHashBucket *, UINT1 );

PUBLIC VOID IsisUpdConstructAndTxPSNP(tIsisSysContext *,
                                       tIsisLETLV **, tIsisCktEntry *, UINT1 );
PUBLIC INT4 IsisUpdSetCktBitPat (tIsisSysContext*, UINT1 **, UINT1 *, UINT1 *, 
                                 UINT4 , UINT1 );
PUBLIC VOID IsisUpdAddCS (UINT1 *, UINT2);
PUBLIC VOID IsisUpdResetSeqNums (tIsisSysContext *);
PUBLIC VOID IsisUpdFillLSPHdr (tIsisSysContext *, tIsisLSPInfo *, UINT1 ,
                                UINT2 *, UINT1 *);
PUBLIC INT4 IsisUpdCompareLSPs (UINT1 *, UINT1 *);
PUBLIC INT4 IsisUpdCompLSPHdr (UINT1 *, UINT1 *);
PUBLIC INT4 IsisUpdValidateCS (tIsisSysContext *, UINT1 *);
PUBLIC VOID IsisUpdUpdateAA (tIsisSysContext *, UINT1 *, UINT1 );
PUBLIC VOID IsisUpdAddAA (tIsisSysContext *, UINT1 *, UINT1 );
PUBLIC VOID IsisUpdDelAA (tIsisSysContext *, UINT1 *, UINT1 );
PUBLIC VOID IsisUpdUpdateSelfLSPSeqNum (tIsisSysContext *, UINT1 *, UINT4 , 
                                         UINT1 );
PUBLIC VOID IsisUpdUpdateRLTime (tIsisSysContext *, tIsisLSPEntry *);
PUBLIC INT4 IsisUpdAddNbrInfo (tIsisSysContext *, UINT1 *, UINT1 , UINT1 ,
                               UINT1, tIsisSPTNode **,UINT1,UINT1, BOOL1);

PUBLIC INT4 IsisUtilRBTreeIpraEntryFree (tRBElem * pIpraEntryNode, UINT4 u4Arg);
PUBLIC VOID IsisGetSemName (tIsisSysContext * pContext, UINT1 au1SemName[]);

/*MISIS*/
PUBLIC INT4 IsisUpdAddNbrInfoFromMTEx (tIsisSysContext *, UINT1 *, UINT1 ,
                               tIsisSPTNode **, UINT1, UINT1, UINT1 *, UINT1, BOOL1);
PUBLIC INT4 IsisUpdValidatePDU (tIsisSysContext *, UINT1 *, tIsisCktEntry *, 
                                 UINT1 *, UINT1 );
PUBLIC VOID IsisUpdProcCtrlPkt (tIsisMsg *);
PUBLIC VOID IsisUpdProcLSP (tIsisSysContext *, tIsisMsg *, UINT1 ,
                            tIsisCktEntry *);
PUBLIC VOID IsisUpdProcPSNP (tIsisSysContext *, tIsisMsg *, tIsisCktEntry *,
                             UINT1 );
PUBLIC VOID IsisUpdProcCSNP (tIsisSysContext *, tIsisMsg *, tIsisCktEntry *,
                             UINT1);
PUBLIC INT4 IsisUpdIsTwoWayConnected (tIsisSysContext *, UINT1 , UINT1 *,
                                      UINT1 *, UINT1);
PUBLIC VOID IsisUpdProcIPIfChgEvt (tIsisSysContext *, tIsisEvtIPIFChange *);
PUBLIC VOID IsisUpdProcDISChgEvt (tIsisSysContext *, tIsisEvtDISChg *);
PUBLIC VOID IsisUpdProcDISStatChgEvt (tIsisSysContext *, tIsisCktEntry *, 
                                      UINT1,  UINT1 );
PUBLIC VOID IsisUpdProcIPRAChgEvt (tIsisSysContext *, tIsisEvtIPRAChange *);
PUBLIC VOID IsisUpdProcCktUp (tIsisSysContext *, tIsisCktEntry *, UINT1); 
PUBLIC INT4 IsisUpdProcAdjChange (tIsisSysContext *, tIsisCktEntry *, UINT1 *,
                                  UINT4, UINT1 ,  UINT1 *, UINT1 ); 
PUBLIC INT4 IsisUpdProcCktDown(tIsisSysContext *, tIsisCktEntry *, UINT1 );
PUBLIC INT4 IsisUpdProcCktModify (tIsisSysContext *, tIsisCktEntry *, UINT1 *, UINT4, UINT1);
PUBLIC INT4 IsisUpdProcCktMetModify (tIsisSysContext *, tIsisCktEntry *, UINT1 *, UINT4, UINT1);
PUBLIC VOID IsisUpdProcMaxAgeTimeOut (tIsisSysContext *, tIsisTmrECBlk *);
PUBLIC VOID IsisUpdProcZeroAgeTimeOut (tIsisSysContext *, tIsisTmrECBlk *);
PUBLIC VOID IsisUpdProcL1LSPGenTimeOut (tIsisSysContext *);
PUBLIC VOID IsisUpdProcL2LSPGenTimeOut (tIsisSysContext *);
PUBLIC VOID IsisUpdProcL1WaitingTimeOut(tIsisSysContext *);
PUBLIC VOID IsisUpdProcL2WaitingTimeOut(tIsisSysContext *);
PUBLIC VOID IsisUpdProcPSNPTimeOut (tIsisSysContext *, 
                                       tIsisTmrECBlk *, UINT1);
PUBLIC VOID IsisUpdProcLSPTxTimeOut (tIsisSysContext *, tIsisTmrECBlk *, UINT1);
PUBLIC VOID IsisUpdProcCSNPTimeOut (tIsisSysContext *, tIsisTmrECBlk *,
                                       UINT1 );
PUBLIC VOID IsisUpdProcSHNTimeOut (tIsisSysContext *);
PUBLIC INT4 IsisUpdUpdateSelfLSP (tIsisSysContext *, UINT4 , tIsisMDT *);
PUBLIC VOID IsisUpdStartLSPTimer (tIsisSysContext *pContext,
                                      tIsisLSPEntry *pLSPRec, 
                                      UINT1 u1Type, UINT2 u2Dur);
PUBLIC UINT1 *IsisUpdConstructSelfLSP (tIsisSysContext *, 
                                      tIsisLSPInfo *, UINT1 , UINT2 *);
PUBLIC VOID IsisUpdDelSelfLSPInfo (tIsisSysContext *, UINT1);
PUBLIC INT4 IsisUpdAllocLSPHdr (tIsisSysContext *, tIsisNSNLSP **, UINT1);
PUBLIC VOID IsisUpdDelLspTimerNode (tIsisLSPEntry *);
PUBLIC INT4 IsisUpdRBTreeIPRATLVCmp (tRBElem *, tRBElem *);
PUBLIC INT4 IsisUpdRRDRoutesRBTreeCmp (tRBElem *, tRBElem *);

/* lsp db fns */

PUBLIC INT4 IsisUpdAddLSP (tIsisSysContext *, UINT1 *, UINT1, UINT1);
PUBLIC VOID IsisUpdReplaceLSP (tIsisSysContext *, UINT1 *, tIsisLSPEntry *, 
                               UINT1);
PUBLIC VOID IsisUpdModifyLSP (tIsisSysContext *, tIsisLSPEntry *, UINT2, 
                              UINT4, UINT1);
PUBLIC VOID IsisUpdAddToDb (tIsisSysContext *, tIsisLSPEntry *, UINT1);
PUBLIC VOID IsisUpdAddToTxQ (tIsisSysContext *, tIsisLSPTxEntry *, UINT1);
PUBLIC VOID IsisUpdDeleteLSP (tIsisSysContext *, UINT1, UINT1*); 
PUBLIC VOID IsisUpdRemLSPFromDB (tIsisSysContext *, tIsisLSPEntry *, 
                                 tIsisLSPEntry *, tIsisHashBucket *, UINT1,
                                 UINT1);
PUBLIC VOID IsisUpdRemLSPFromTxQ (tIsisSysContext *, tIsisLSPTxEntry *, 
                                  tIsisLSPTxEntry *, tIsisHashBucket *,
                                  UINT1, UINT1);
PUBLIC tIsisLSPEntry *IsisUpdGetLSP (tIsisSysContext *, UINT1*, UINT1, 
                                     tIsisLSPEntry **,
                                     tIsisHashBucket **, UINT1 *);
PUBLIC tIsisLSPEntry *IsisUpdGetLSPFromDB (tIsisSysContext *, UINT1*, UINT1, 
                                           tIsisLSPEntry **, tIsisHashBucket **,
                                           UINT1 *, UINT1 *);
PUBLIC tIsisLSPTxEntry *IsisUpdGetLSPFromTxQ (tIsisSysContext *, UINT1*, UINT1, 
                                              tIsisLSPTxEntry **,
                                              tIsisHashBucket **, UINT1 *, 
                                              UINT1 *);
PUBLIC tIsisLSPEntry *IsisUpdGetNextDbRec (tIsisLSPEntry *,
                                           tIsisHashBucket **); 
PUBLIC tIsisLSPTxEntry *IsisUpdGetNextTxRec (tIsisLSPTxEntry *,
                                             tIsisHashBucket **); 
PUBLIC VOID IsisUpdValLSPDataBase (tIsisSysContext *, UINT1);
PUBLIC VOID IsisUpdSplitBucket (tIsisSysContext *, tIsisSortedHashTable *,
                                UINT1);
PUBLIC INT4 IsisRmGetNodeState PROTO ((VOID));

#ifdef ISIS_FT_ENABLED

/* Fault Tolerance Module Function ProtoTypes
 */
PUBLIC VOID IsisFltrFillHdr (tRmMsg *, UINT1, UINT1, UINT1, UINT2 *);
PUBLIC INT4 IsisFltiRegisterWithRM (VOID);
PUBLIC INT4 IsisFltiDeRegisterFromRM (VOID);
PUBLIC INT4 IsisFltiSendData (UINT1 *, UINT1 *, UINT2);
PUBLIC INT4 IsisFltrFormBulkUpdateReq (VOID); 
PUBLIC INT4 IsisFltrFormBulkData (VOID);
PUBLIC INT4 IsisFltrProcData (tRmMsg *pMsg);
PUBLIC INT4 IsisFltrGetNextIPIfRec(tIsisSysContext **, tIsisIPIfAddr** );
PUBLIC INT4 IsisFltrProcBulkDataComplete(VOID);

PUBLIC INT4 IsisFltrGetFirstCktEntry (tIsisSysContext**, tIsisCktEntry**); 
PUBLIC INT4 IsisFltrGetFirstAdjEntry (tIsisSysContext** ,tIsisCktEntry**,
                                           tIsisAdjEntry**); 

PUBLIC INT4 IsisFltrGetFirstIPRAEntry (tIsisSysContext** , tIsisIPRAEntry**); 
PUBLIC INT4 IsisFltrGetFirstIPIFRec(tIsisSysContext** , tIsisIPIfAddr**);
PUBLIC INT4 IsisFltrGetFirstSAEntry (tIsisSysContext** ,tIsisSAEntry** );
PUBLIC INT4 IsisFltrGetFirstMAAEntry (tIsisSysContext** ,tIsisMAAEntry** );
PUBLIC INT4 IsisFltrGetFirstAAEntry (tIsisSysContext** ,tIsisAAEntry** );
PUBLIC INT4 IsisFltrGetFirstAdjAreaEntry (tIsisSysContext** ,tIsisCktEntry**,
                                          tIsisAdjEntry**, tIsisAdjAAEntry**);
PUBLIC INT4 IsisFltrGetNextCktEntry (tIsisSysContext **, tIsisCktEntry **);
PUBLIC INT4 IsisFltrGetNextCktLvlEntry (tIsisSysContext **, tIsisCktEntry **, 
                                        tIsisCktLevel **);
PUBLIC INT4 IsisFltrGetNextAdjEntry (tIsisSysContext **, tIsisCktEntry **,
                                     tIsisAdjEntry **);
PUBLIC INT4  IsisFltrGetNextLspEntry (tIsisSysContext **, UINT1* , 
                                      tIsisLSPEntry **, UINT1*);
PUBLIC INT4 IsisFltrGetNextIPRAEntry (tIsisSysContext **, tIsisIPRAEntry **);
PUBLIC INT4 IsisFltrGetNextSAEntry (tIsisSysContext **, tIsisSAEntry **);
PUBLIC INT4 IsisFltrGetNextMAAEntry (tIsisSysContext **, tIsisMAAEntry **);
PUBLIC INT4 IsisFltrGetNextAdjAreaEntry (tIsisSysContext **, tIsisCktEntry **,
                                         tIsisAdjEntry **, tIsisAdjAAEntry **);
PUBLIC VOID IsisFltrAddSelfLSPTLV (tIsisSysContext *, tIsisTLV *, 
                                   tIsisLSPInfo *);
PUBLIC VOID IsisFltrConstructSelfLSPTLV (tIsisSysContext *, UINT1 *, UINT4, 
                                         UINT1);
PUBLIC VOID IsisFltrModifySelfLSPTLV (tIsisSysContext *, UINT1 *, UINT1, UINT4);
PUBLIC VOID IsisFltrDeleteSelfLSPTLV (tIsisSysContext *, UINT1 *, UINT1);
PUBLIC INT4 IsisFltrProcCtrlPkt (tIsisMsg *);
PUBLIC tRmMsg * IsisFltrFormSysContextBulkData (tIsisSysContext**, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormCktBulkData (tIsisSysContext **, tIsisCktEntry **,
                                       UINT1 *, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormCktLevelBulkData (tIsisSysContext **, 
                                            tIsisCktEntry **,
                                            tIsisCktLevel **, UINT1 *, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormAdjBulkData (tIsisSysContext **, tIsisCktEntry **,
                                      tIsisAdjEntry**, UINT1 *, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormLSPBulkData (tIsisSysContext **, tIsisLSPEntry **,
                                      UINT1* ,UINT1 *, UINT1 *, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormIPRABulkData (tIsisSysContext**, tIsisIPRAEntry **,
                                        UINT1*, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormIPIfBulkData (tIsisSysContext**, tIsisIPIfAddr**,
                                       UINT1*, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormSAABulkData (tIsisSysContext**, tIsisSAEntry**, 
                                      UINT1*, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormMAABulkData (tIsisSysContext**, tIsisMAAEntry**, 
                                      UINT1*, UINT2 *);
PUBLIC tRmMsg * IsisFltrFormAdjAABulkData (tIsisSysContext**, 
                                              tIsisCktEntry**, tIsisAdjEntry **,
                                              tIsisAdjAAEntry **, UINT1*, UINT2 *);
PUBLIC INT4 IsisFltrAdjLSU (tIsisSysContext *, UINT1, tIsisAdjEntry *); 
PUBLIC INT4 IsisFltrCktLSU (tIsisSysContext *, UINT1, tIsisCktEntry *); 
PUBLIC INT4 IsisFltrManAreaAddrLSU (tIsisSysContext *, UINT1, tIsisMAAEntry *); 
PUBLIC INT4 IsisFltrIPRALSU (tIsisSysContext *, UINT1,  tIsisIPRAEntry *); 
PUBLIC INT4 IsisFltrSummAddrLSU (tIsisSysContext *, UINT1, tIsisSAEntry *); 
PUBLIC INT4 IsisFltrIPIfLSU (tIsisSysContext *, UINT1, tIsisIPIfAddr *); 
PUBLIC INT4 IsisFltrAdjAALSU (tIsisSysContext *, UINT1, tIsisAdjEntry *, 
                              tIsisAdjAAEntry *); 
PUBLIC INT4 IsisFltrCktLevelLSU (tIsisSysContext *, UINT1, tIsisCktEntry *, 
                                 tIsisCktLevel *); 
PUBLIC INT4 IsisFltrSysContextLSU (tIsisSysContext *, UINT1); 
PUBLIC INT4 IsisFltrSysActLSU (tIsisSysContext *, UINT1); 
PUBLIC INT4 IsisFltrSysConfLSU (tIsisSysContext *, UINT1); 
PUBLIC INT4 IsisFltrLspLSU (tIsisSysContext *, UINT1, tIsisLSPEntry *); 
PUBLIC INT4 IsisFltrProtSuppLSU (tIsisSysContext *, UINT1, UINT1);
PUBLIC INT4 IsisFltrMemCfgLSU (VOID);
PUBLIC INT4 IsisFltrISStatLSU (UINT1 );
PUBLIC INT4 IsisFltrSchSPFLSU (tIsisSysContext *, UINT1);
PUBLIC VOID IsisFltProcLSUFailure (VOID);

/* Fault Tolerance Endian Conversion functions */

PUBLIC VOID IsisFltrBldCktLevelRec (tRmMsg *, tIsisCktLevel *, UINT2* );
PUBLIC VOID IsisFltrBldSysContextRec (tRmMsg *, tIsisSysContext *, UINT2* );
PUBLIC VOID IsisFltrBldCktRec (tRmMsg*, tIsisCktEntry *, UINT2* );
PUBLIC VOID IsisFltrBldAdjRec(tRmMsg*, tIsisAdjEntry *, UINT2* );
PUBLIC VOID IsisFltrBldIPRARec(tRmMsg*, tIsisIPRAEntry *, UINT2* );
PUBLIC VOID IsisFltrBldAdjAARec (tRmMsg*, tIsisAdjAAEntry *, UINT2* );
PUBLIC VOID IsisFltrBldSysActualsRec (tRmMsg*, tIsisSysContext *, UINT2*);
PUBLIC VOID IsisFltrBldSysConfRec (tRmMsg*, tIsisSysContext *, UINT2*);
PUBLIC VOID IsisFltrBldIPIfRec (tRmMsg*, tIsisIPIfAddr *, UINT2* );
PUBLIC VOID IsisFltrBldMAARec (tRmMsg*, tIsisMAAEntry *, UINT2* );
PUBLIC VOID IsisFltrBldSARec (tRmMsg*, tIsisSAEntry *, UINT2* );
PUBLIC VOID IsisFltrGetCktEntry (tRmMsg*, tIsisCktEntry *, UINT2 *pu2Offset);
PUBLIC VOID IsisFltrGetCktLevelRec (tRmMsg*, tIsisCktLevel *, UINT2 *pu2Offset);
PUBLIC VOID IsisFltrGetAdjRec(tRmMsg*, tIsisAdjEntry *, UINT2 *pu2Offset);
PUBLIC VOID IsisFltrGetMAARec (tRmMsg*, tIsisMAAEntry *, UINT2 *pu2Offset);
PUBLIC VOID IsisFltrGetSARec (tRmMsg*, tIsisSAEntry *, UINT2 *pu2Offset);
PUBLIC VOID IsisFltrGetIPIfRec (tRmMsg*, tIsisIPIfAddr *, UINT2 *pu2Offset);
PUBLIC VOID IsisFltrGetSysContextRec (tRmMsg*, tIsisSysContext *, UINT2 *pu2Offset);
PUBLIC VOID IsisFltrGetSysConfRec (tRmMsg*, tIsisSysConfigs *, UINT2 *);
PUBLIC VOID IsisFltrGetSysActualsRec (tRmMsg*, tIsisSysActuals *, UINT2 *);
PUBLIC VOID IsisFltrGetAdjAARec (tRmMsg*, tIsisAdjAAEntry *, UINT2 *pu2Offset);
PUBLIC VOID IsisFltrGetIPRARec (tRmMsg*, tIsisIPRAEntry *, UINT2 *pu2Offset);

PUBLIC VOID IsisCtrlGoStandby (VOID);
PUBLIC VOID IsisCtrlGoActive (VOID);
PUBLIC INT4 IsisFltiRegWithRM (VOID);
PUBLIC INT4 IsisFltiDeRegWithRM (VOID);
PUBLIC INT4 IsisRmSendMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen);
PUBLIC tRmMsg * IsisRmAllocForRmMsg (UINT2 u2Size);
PUBLIC INT4   IsisRmRelRmMsgMem (UINT1 *pu1Block);
PUBLIC VOID IsisCtrlHandleStandbyUp (tRmMsg * pRmMsg);
PUBLIC VOID IsisCtrlHandleStandbyDown (tRmMsg * pRmMsg);
PUBLIC VOID IsisCtrlConfigRestoreComplete (VOID);
PUBLIC VOID IsisCtrlProcessRmMessage (tRmMsg * pRmMsg, UINT2 u2DataLen);
PUBLIC UINT1 IsisRmGetStandbyNodeCount (VOID);
PUBLIC VOID IsisSendProtoAckToRM (UINT4 u4SeqNum);
PUBLIC VOID IsisRmSetBulkUpdateStatus (VOID);
PUBLIC INT4 IsisRmSendEventToRm (UINT4 u4Event, UINT4 u4Error);
PUBLIC INT4 IsisFltrFormBulkUpdateTail (VOID); 

#endif
 /* Decision Module Function ProtoTypes
 */

PUBLIC VOID IsisDecProcSPFSchTimeOut (tIsisSysContext *);
PUBLIC VOID IsisDecProcISDownEvt (tIsisSysContext *);
PUBLIC INT4 IsisDecSchedSPF (tIsisSysContext *, UINT1);
PUBLIC INT4 IsisDecRevSPF (tIsisSysContext *, UINT1, UINT1, UINT1, UINT1, UINT1, UINT1);
PUBLIC INT4 IsisUtlSPTNodeCmp (tRBElem *, tRBElem *);
PUBLIC VOID IsisDecDeAllocSptDB (tRBTree, tIsisSysContext *pContext, UINT1, UINT1, UINT1);
PUBLIC VOID IsisDecFormRouteInfo (tIsisSysContext * , UINT1 ,tIsisSPTNode * , UINT1 ,
                                  UINT1 , UINT1 );
PUBLIC UINT4 IsisDecUpdatePrevPath (tIsisSysContext *, tIsisSPTNode *, tIsisSPTNode *,
                                   tIsisSPTNode *, UINT1, UINT1, UINT1);
/* RTM Function Prototypes
 */

PUBLIC VOID IsisProcRtmPkt (tIsisMsg *);
PUBLIC VOID IsisProcRtm6Pkt (tIsisMsg *);
PUBLIC INT4 IsisRtmiRtmTxRtUpdate (tIsisSysContext*, tIsisRouteInfo *);
PUBLIC INT4 IsisRegisterWithRtm (tIsisSysContext *pContext);
PUBLIC INT4 IsisDeRegisterFromRtm (UINT4);
PUBLIC INT4 IsisRegisterWithIP (tIsisSysContext *pContext);
PUBLIC INT4 IsisDeRegisterFromIP (UINT4);
PUBLIC VOID IsisIfStateChgHdlr (tNetIpv4IfInfo * pNetIfInfo, UINT4 u4BitMap);
PUBLIC VOID IsisV6IfStateChgHdlr (tNetIpv6HliParams * pIp6HliParams);
PUBLIC VOID IsisV6IfAddrChgHdlr (tNetIpv6HliParams * pIp6HliParams);
PUBLIC INT4 IsisRegWithExtMod (tIsisSysContext *pContext);
PUBLIC INT4 IsisDeRegFromExtMod (UINT1, UINT4);
PUBLIC INT4 IsisRegWithVcm (VOID);
PUBLIC INT4 IsisDeRegFromVcm (VOID);
PUBLIC INT4 IsisRecvMsgFromRtm (tRtmRespInfo *, tRtmMsgHdr *);
PUBLIC INT4 IsisRecvMsgFromRtm6 (tRtm6RespInfo *, tRtm6MsgHdr *);

PUBLIC INT4 IsisInitModule (VOID);
PUBLIC INT4 IsisInit (VOID);
PUBLIC INT4 IsisMemInit (VOID);
PUBLIC VOID IsisInitGlobals (VOID);
PUBLIC INT4 IsisMemReInit (VOID);
PUBLIC VOID IsisShutDownAll (VOID);
PUBLIC VOID IsisDelSemandQue (VOID);
PUBLIC VOID IsisResetInst (tIsisSysContext *);
PUBLIC INT4 IsisResetAll (VOID);
PUBLIC VOID IsisProcLLData (tIsisMsg *);
PUBLIC VOID IsisDeAllocLSPTxBlock (tIsisLSPTxEntry *pLspTxBuff);
PUBLIC tIsisLSPTxEntry* IsisAllocLSPTxBlock(VOID);
PUBLIC VOID IsisStopAllTimers (tIsisSysContext *);
PUBLIC VOID IsisDeAllocEvent (tIsisSysContext *);
PUBLIC INT4 IsisEnqueueMessage (UINT1 *, UINT4 , tOsixQId , UINT4);
PUBLIC VOID IsisProcessEvent (tIsisMsg *);
PUBLIC VOID IsisProcessData (tIsisMsg * ,tIsisCktEntry *);

/* Control Module Function ProtoTypes
 */
PUBLIC VOID IsisCtrlAddIPRA (tIsisSysContext*, tIsisIPRAEntry*);
PUBLIC INT4 IsisCtrlDelIPRA (tIsisSysContext*, UINT4, UINT1);
PUBLIC VOID IsisCtrlAddAutoIPRA (tIsisSysContext *, UINT4, UINT4, UINT1 *,
                                 UINT1, tIsisMetric , UINT4, UINT1);

PUBLIC VOID IsisCtrlAddSecAutoIPRA (tIsisSysContext *, UINT4, UINT4, UINT1 *,
                                       UINT1, tIsisMetric , UINT4, UINT1);



PUBLIC VOID IsisCtrlDelAutoIPRA (tIsisSysContext *, UINT4 , UINT4, UINT1);
PUBLIC VOID IsisCtrlDelAutoSecIPRA (tIsisSysContext *, UINT4 , UINT4, UINT1, UINT1 *);
PUBLIC INT4 IsisCtrlGetIPRARec (tIsisSysContext*, UINT4, UINT1, 
                                tIsisIPRAEntry**);
PUBLIC VOID IsisCtrlUpdtIPRA (tIsisIPRAEntry*, tIsisIPRAEntry*);
PUBLIC VOID IsisCtrlCopyIPRADefVal (tIsisSysContext *, tIsisIPRAEntry *);
PUBLIC INT4 IsisCtrlGetIPIfWithCktIdx (tIsisSysContext *, UINT4 ,
                                       UINT1 , tIsisIPIfAddr ** );
PUBLIC VOID IsisCtrlUpdtIPIf (tIsisIPIfAddr *, tIsisIPIfAddr *);
PUBLIC VOID IsisCtrlLogEvent (tIsisSysContext *, UINT1*);
PUBLIC VOID IsisCtrlProcISDownEvt (tIsisSysContext *);
PUBLIC INT4  IsisCtrlInitSysContext (tIsisSysContext *, UINT4);
PUBLIC VOID IsisCtrlCopyConfigsToActuals (tIsisSysContext *);

PUBLIC INT4 IsisCtrlGetSysContext (UINT4, tIsisSysContext**);
PUBLIC VOID IsisCtrlAddSysContext (tIsisSysContext *);
PUBLIC INT4 IsisCtrlDelSysContext (UINT4);
PUBLIC INT4 IsisCtrlGetMAA (tIsisSysContext *, UINT1 *, UINT1, tIsisMAAEntry **);
PUBLIC VOID IsisCtrlAddMAA (tIsisSysContext *, tIsisMAAEntry *);
PUBLIC INT4 IsisCtrlDelMAA (tIsisSysContext *, UINT1, UINT1 *);
PUBLIC INT4 IsisCtrlGetSA (tIsisSysContext *, UINT1 *, UINT1, UINT1,
                           tIsisSAEntry**);
PUBLIC VOID IsisCtrlAddSA (tIsisSysContext *, tIsisSAEntry *);
PUBLIC INT4 IsisCtrlDelSA (tIsisSysContext *, UINT1 *, UINT1, UINT1);
PUBLIC INT4 IsisCtrlGetPSEntry (tIsisSysContext *, UINT1, tIsisProtSupp**);
PUBLIC INT4 IsisCtrlAddPSEntry (tIsisSysContext *, UINT1);
PUBLIC INT4 IsisCtrlDelPSEntry (tIsisSysContext *, UINT1);
PUBLIC INT4 IsisCtrlGetIPIf (tIsisSysContext *,UINT4 , UINT4, UINT1 *, UINT1,
                             tIsisIPIfAddr **);
PUBLIC VOID IsisCtrlAddIPIf (tIsisSysContext *, tIsisIPIfAddr *);
PUBLIC INT4 IsisCtrlDelIPIf (tIsisSysContext *, UINT4, UINT4, UINT1*, UINT1);
PUBLIC VOID IsisCtrlUpdtSA (tIsisSAEntry *, tIsisSAEntry *);
PUBLIC INT1 IsisCtrlUpdtSAUsageCnt (tIsisSysContext *, UINT1 *, UINT1 , UINT2 ,
                                    UINT1, UINT1 );
PUBLIC VOID IsisCtrlProcISDestroyEvt (tIsisMsg *);
PUBLIC VOID IsisCtrlProcISUpEvt (tIsisSysContext *, UINT1);
PUBLIC INT4 IsisCtrlAllocShortPath (tIsisSysContext * );
PUBLIC VOID IsisCtrlInitTimers (tIsisSysContext * );
/* Traffic Router Module prototype */
               
PUBLIC VOID IsisTrfrIfStatusInd (tIsisSysContext*, tIsisCktEntry*, UINT1);
PUBLIC INT4 IsisTrfrTxData (tIsisCktEntry*, UINT1*, UINT4, UINT1, UINT1, UINT1);

/* SNMP util function Prototypes
 */ 

PUBLIC INT4 nmhUtlGetNextPktCntTable (UINT4 , UINT4 , UINT1 , UINT1 , UINT1 *);
PUBLIC INT4 nmhUtlGetNextCktLvlRxPasswd (tIsisSysContext*, UINT4, UINT1*,
                                         tSNMP_OCTET_STRING_TYPE*,
                                         tSNMP_OCTET_STRING_TYPE*);
PUBLIC INT4 nmhUtlGetNextAreaRxPasswd (tIsisSysContext*, 
                                       tSNMP_OCTET_STRING_TYPE *, 
                                       tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT4 nmhUtlGetNextDomainRxPasswd (tIsisSysContext*, 
                                         tSNMP_OCTET_STRING_TYPE *, 
                                         tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT4 nmhUtlGetNextIndexIsisSysTable (UINT4 , UINT4*);
PUBLIC INT4 nmhUtlGetNextMAAddr (tIsisSysContext*, tSNMP_OCTET_STRING_TYPE*, 
                                 tSNMP_OCTET_STRING_TYPE*);
PUBLIC INT4 nmhUtlGetNextAreaAddr (tIsisSysContext*, tSNMP_OCTET_STRING_TYPE*, 
                                   tSNMP_OCTET_STRING_TYPE*);
PUBLIC INT4 nmhUtlGetNextPSEntry (tIsisSysContext*, UINT1, UINT1*);
PUBLIC INT4 nmhUtlGetNextSummAddr (tIsisSysContext*, UINT1, UINT1*, 
                                   tSNMP_OCTET_STRING_TYPE*,
                                   tSNMP_OCTET_STRING_TYPE*, UINT1, UINT1*);
PUBLIC INT4 nmhUtlGetNextIndexIsisCircTable (UINT4, UINT4, UINT4*);
PUBLIC INT4 nmhUtlGetNextIndexIsisCircLevelTable (UINT4, UINT4, UINT1, UINT1 *);
PUBLIC INT4 nmhUtlGetNextIndexIsisISAdjTable (INT4, INT4, INT4, INT4*);
PUBLIC INT4 nmhUtlGetNextIndexIsisIPRATable(INT4, INT4, INT4*, INT4, INT4*);
PUBLIC INT4 nmhUtlGetNextIndexIsisISAdjAreaAddrTable (INT4, INT4, INT4, INT4,
                                                      INT4 *, 
                                                      tSNMP_OCTET_STRING_TYPE*);
PUBLIC INT4 nmhUtlGetNextIndexIsisISAdjIPAddrTable (INT4, INT4, INT4, INT4*);
PUBLIC INT4 nmhUtlGetNextIndexIsisISAdjProtSuppTable (INT4, INT4, INT4, INT4, INT4*);

PUBLIC INT4 nmhUtlGNIfIdx (tIsisSysContext *, INT4 , INT4  *);
PUBLIC INT4 nmhUtlGNIfSubIdx (tIsisSysContext *, INT4 , INT4 , INT4  *);
PUBLIC INT4 nmhUtlGNIfAddr (tIsisSysContext *, INT4, INT4, INT4,
                            tSNMP_OCTET_STRING_TYPE *,
                            tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 IsisNmhValSysTable (UINT4 *, INT4);
PUBLIC INT1 IsisNmhValSATable (UINT4 *, INT4, INT4, tSNMP_OCTET_STRING_TYPE *,
                               INT4, UINT1);
PUBLIC INT1 IsisNmhValCircTable (UINT4 *, INT4, INT4, UINT1, UINT1);
PUBLIC INT1 IsisNmhValCircLevelTable (UINT4 *, INT4, INT4, INT4);
PUBLIC INT1 IsisNmhValIPRATable (UINT4 *, INT4, INT4, INT4, UINT1);
PUBLIC INT4 nmhUtlGNIfAddrType (tIsisSysContext *, INT4, INT4, INT4, INT4 *);
PUBLIC INT4 nmhUtlGetNextIndexIsisSysStatLevel (UINT4, UINT4, UINT4*);


/* General Utility functions 
 */

PUBLIC INT4  IsisUtlGetNextIdFromBPat (UINT1**, UINT4*);
PUBLIC VOID  IsisUtlSetBPat (UINT1 **, UINT4);
PUBLIC UINT2 IsisUtlCalcChkSum (UINT1 *, UINT2 , UINT1 *);
PUBLIC VOID  IsisUtlGetIPMask (UINT1 , UINT1 *);
PUBLIC INT4  IsisUtlComputePrefixLen (UINT1 *, UINT1 , UINT1 *);
PUBLIC VOID  IsisUtlSendEvent (tIsisSysContext *, UINT1 *, UINT1 );
PUBLIC VOID  IsisUtlFillComHdr (tIsisSysContext *, UINT1 *, UINT1);
PUBLIC INT4  IsisUtlAuthenticatePDU (tIsisSysContext *,tIsisCktEntry *,UINT1 *);
PUBLIC INT4  IsisUtlValidatePDU (tIsisSysContext *, tIsisCktEntry *, UINT1 *);
PUBLIC VOID  IsisUtlFormMetric (tIsisSysContext *, UINT1 *, UINT1 *);
PUBLIC UINT1 IsisUtlGetHashIdx (UINT1*, UINT1, UINT1);
PUBLIC UINT1 IsisUtlGetMetType (tIsisSysContext *, UINT1);
PUBLIC UINT1 IsisUtlGetMetIndex (tIsisSysContext *, UINT1);
PUBLIC INT4  IsisUtlGetNextMetric (INT1 , UINT1 *);
PUBLIC INT4  IsisUtlGetNextLevel (tIsisSysContext *, UINT1  , UINT1  *);
PUBLIC tIsisMsg *IsisUtlCloneMsg (tIsisMsg *);
PUBLIC VOID IsisUtlFormResFailEvt (tIsisSysContext *, UINT1);
PUBLIC VOID IsisUtlFillMetDefVal (tIsisSysContext *, VOID *, UINT1);
PUBLIC UINT1 IsisUtlReSetBPat (UINT1 **, UINT4);
PUBLIC INT4 IsisUtlGetNextTlvOffset (UINT1 *, UINT1 , UINT2 *, UINT1 *);
PUBLIC CHAR *IsisCtrlGetEventStr (UINT1);
PUBLIC INT4 IsisUtlChkProtSupported (tIsisCktEntry *, UINT1 *, UINT1 );
PUBLIC VOID IsisUtlFreePkt (UINT1 *);
PUBLIC INT4 IsisLock (VOID);
PUBLIC INT4 IsisUnlock (VOID);
PUBLIC VOID IsisUtlSetMTIdfromLSP (tIsisSysContext *,UINT1 *, tIsisLSPEntry *);
PUBLIC VOID IsisUtlUpdateMetrics (tIsisSysContext *);
PUBLIC VOID IsisUtlUpdateMetricStyle (tIsisSysContext *);
PUBLIC VOID IsisAdjUpdateMTId (UINT1 *pu1PDU, tIsisAdjEntry * pAdjRec, tBool *pbLSUFlag);
PUBLIC INT4 IsisUtlValidatePurgeLSP (tIsisSysContext * , UINT1 *);
#ifdef BFD_WANTED
PUBLIC VOID IsisUtlUpdateBfdRequired (tIsisCktEntry *, tIsisAdjEntry *);
PUBLIC VOID IsisUtlUpdateBfdNeighborUseable (tIsisCktEntry *, tIsisAdjEntry *);
PUBLIC VOID IsisUtlProcessAdjUpNotification (tIsisSysContext * pContext, tIsisAdjEntry * pAdjEntry, 
                                             tIsisCktEntry * pCktEntry, UINT1 u1MtId);
PUBLIC VOID IsisUtlProcessAdjDownNotification (tIsisSysContext * pContext, tIsisAdjEntry * pAdjEntry, 
                                               tIsisCktEntry * pCktEntry, UINT1 u1MtId);
PUBLIC VOID IsisUtlProcessAdjAdminDownNotification (tIsisSysContext * pContext, tIsisAdjEntry * pAdjEntry,
                                                    tIsisCktEntry * pCktEntry, UINT1 u1MtId);
PUBLIC INT4 IsisUtlGetCktRecWithIfIndex (tIsisSysContext * pContext, UINT4 u4IfIndex, 
                                         tIsisCktEntry ** pCktRec);
PUBLIC INT4 IsisUtlGetAdjRecWithIpAddr (tIsisIpAddr *pIpAddr, tIsisCktEntry * pCktRec, 
                                        tIsisAdjEntry ** pL1AdjRec, tIsisAdjEntry ** pL2AdjRec);
PUBLIC INT1 IsisAdjUpdateBfdEnabled (UINT1 * pu1PDU, tIsisAdjEntry * pAdjRec, 
                                     tBool * pbLSUFlag, UINT1 * u1AdjState);
PUBLIC VOID IsisUtlUpdateBfdState (tIsisAdjEntry * pAdjEntry, UINT1 u1MtId, UINT1 u1BfdState);
PUBLIC VOID IsisUtlComputeIfUpdateReqd (tIsisAdjEntry * pAdjRec, 
                                        UINT1 * pu1Mt0UpdReqd, UINT1 * pu1Mt2UpdReqd);
PUBLIC VOID IsisUtlBfdRegister (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec, 
                                tIsisCktEntry * pCktRec);
PUBLIC VOID IsisUtlBfdDeRegister (tIsisSysContext * pContext, tIsisAdjEntry * pAdjRec, 
                                  tIsisCktEntry * pCktRec, UINT1 u1SyncFlag);
PUBLIC INT4 IsisAdjProcBfdinP2PIIH(tIsisCktEntry *pCktRec, tIsisAdjEntry *pAdjRec, UINT1 *pu1PDU, tBool *pbLSUFlag);

#endif
PUBLIC INT4
IsisHostNmeListCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
PUBLIC INT4
IsisUpdAddToHostNmeTable (tIsisSysContext *pContext, tIsisHostNmeNodeInfo *p1IsisHostNmeNodeInfo);
PUBLIC VOID 
IsisUpdHostNmeTable (tIsisSysContext *pContext, UINT1 *pu1SysID, UINT1 *pu1HostNme, UINT1 u1cmd, UINT1 u1LSPDID);
PUBLIC VOID
IsisUpdReFillHstNmeTable (tIsisSysContext * pContext, UINT1 u1Level);
   PUBLIC VOID
IsisUtlGetHostnmefromLSP (tIsisSysContext * pContext, UINT1 *pu1LSP, UINT1 u1Cmd);
PUBLIC VOID
IsisHostNmeMDT (tIsisSysContext *pContext, tIsisMDT *pMDT, UINT1 u1Cmd);
PUBLIC VOID
IsisGetHostNme (tIsisSysContext *pContext, UINT1 *pu1SysID, UINT1 *pu1HostNme);
PUBLIC VOID
IsisDelAllHstNmeInCxt (tIsisSysContext * pOspfCxt);
PUBLIC INT1 IsisUtlIsDirectlyConnected (tIsisCktEntry * pCktRec, UINT1 * pu1Address);
#ifdef ROUTEMAP_WANTED
PUBLIC INT1 IsisApplyInFilter(tFilteringRMap*, tIsisRouteInfo*, UINT1);
PUBLIC INT4 IsisSendRouteMapUpdateMsg (UINT1*, UINT4);
#endif /* ROUTEMAP_WANTED */
PUBLIC UINT1 IsisIp4FilterRouteSource(tIsisSysContext * pContext, tNetIpv4RtInfo *);
PUBLIC UINT1 IsisIp6FilterRouteSource (tIsisSysContext *pContext, tNetIpv6RtInfo *);

#if (ISIS_MEM_DBG == 1)

PUBLIC VOID * IsisUtlAllocBuf (UINT1 , UINT4 , const CHAR *, const INT4);
PUBLIC VOID   IsisUtlReleaseBuf (UINT1 , UINT1 *, const CHAR *, const INT4 );

#else

PUBLIC UINT1* IsisUtlAllocBuf (UINT1 , UINT4);
PUBLIC VOID   IsisUtlReleaseBuf (UINT1 , UINT1 *);

#endif

PUBLIC UINT1 IsisUtlCompIPAddr (UINT1*, UINT1, UINT1*, UINT1);

/* DLLI Modules ProtoTypes */
PUBLIC INT4 IsisDlliProcIfStatusChange (tIsisCktEntry *, UINT1 );


/* Port module ProtoTypes*/

PUBLIC UINT1 * IsisFltrFillPeerNodeHdr (UINT2, UINT2, UINT2 *);
PUBLIC VOID IsisCopyFromDLLBuf (tIsisMsg *, UINT1 *, UINT4);
PUBLIC VOID IsisFreeDLLBuf (tIsisMsg *);
PUBLIC VOID IsisUpdateIfStatus (tIsisCktEntry *);
PUBLIC INT4 IsisGetIfInfo (UINT4 u4IfIndex, tIsisLLInfo *pIfInfo);
#ifdef BFD_WANTED
PUBLIC INT4 IsisRegisterWithBfd (UINT4 u4SysInstance, tIsisCktEntry * pCktEntry,
                                 tIsisAdjEntry * pAdjEntry, UINT1 u1MtId);
PUBLIC INT4 IsisDeRegisterWithBfd (UINT4 u4SysInstance, tIsisCktEntry * pCktEntry,
                                   tIsisAdjEntry * pAdjEntry, UINT1 u1MtId, UINT1 u1Flag);
PUBLIC INT1 IsisHandleAdjPathStatusChange (UINT4 u4ContextId, tBfdClientNbrIpPathInfo * pNbrPathInfo);
#endif
#ifdef ISS_WANTED
PUBLIC VOID IsisRegWithIss (VOID);
PUBLIC VOID IsisDeRegWithIss (VOID);
#endif
PUBLIC VOID IsisHostNmeHdlr (tIsisMsg * pBuf);
PUBLIC VOID IsisHostNmeSupportChgEvt (tIsisMsg * pBuf);
/* Buddy Memory*/
PUBLIC INT4 MemBuddyCreate(UINT4, UINT4, UINT4, UINT1);
PUBLIC VOID MemBuddyDestroy(UINT1);
PUBLIC UINT1 *MemBuddyAlloc(UINT1, UINT4);
PUBLIC INT4 MemBuddyFree(UINT1, UINT1 *);

PUBLIC VOID IsisVcmCallbackFn PROTO ((UINT4 u4IpIfIndex,
                                      UINT4 u4VcmCxtId, UINT1 u1BitMap));
PUBLIC VOID IsisVcmMsgHandler PROTO ((tIsisMsg *));

PUBLIC INT4       UtilIsisVcmIsVcExist PROTO ((UINT4 u4SysInstIdx));
PUBLIC INT4       UtilIsisIsVcmSwitchExist PROTO ((UINT1 *pu1Alias,
                                                    UINT4 *pu4VcNum));
PUBLIC INT4       UtilIsisGetVcmAliasName PROTO ((UINT4 u4SysInstIdx,
                                                  UINT1 *pu1Alias));
PUBLIC INT4       UtilIsisGetVcmSystemMode PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4       UtilIsisGetVcmSystemModeExt PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4       UtilIsisSetContext PROTO  ((UINT4 u4SysInstIdx));
PUBLIC INT4       UtilIsisGetFirstCxtId (UINT4 *pu4IsisCxtId);
PUBLIC INT4       UtilIsisGetNextCxtId (UINT4 u4ContextId, UINT4 *pu4NextContextId);




/* Graceful Restart Support*/
PUBLIC INT4 IsisGrInitiateRestart PROTO ((UINT1 u1RestartType, UINT1 u1RestartReason));
PUBLIC INT4 IsisAdjRestartTlv (tIsisSysContext *,tIsisCktEntry *, UINT1*, UINT2, UINT2*);

PUBLIC VOID IsisAdjProcT1TimeOut (tIsisSysContext*, tIsisTmrECBlk*);
PUBLIC VOID IsisGRUpdProcT3TimeOut (tIsisSysContext *);
PUBLIC VOID IsisGRUpdProcL1T2TimeOut (tIsisSysContext *);
PUBLIC VOID IsisGRUpdProcL2T2TimeOut (tIsisSysContext *);

PUBLIC INT4 RtmIsisGrNotifInd PROTO ((tIsisSysContext *));
PUBLIC INT4 Rtm6IsisGrNotifInd PROTO ((tIsisSysContext *));
PUBLIC INT4 IsisAdjProcRestartTlv PROTO (( tIsisSysContext *, tIsisCktEntry *,UINT1*, UINT1 *));
PUBLIC VOID IssRestoreGraceContent PROTO ((VOID *,UINT4 protocol));
PUBLIC VOID IssStoreGraceContent PROTO ((VOID *,UINT4 protocol));
PUBLIC VOID IssRemoveGraceContent PROTO ((UINT4 protocol));
PUBLIC INT4 IsisGrExitGracefultRestart PROTO((tIsisSysContext *, UINT1 ));

PUBLIC INT4 RbCompareLsp PROTO ((tRBElem * e1, tRBElem * e2));
PUBLIC VOID IsisGrRestoreRestartInfo (tIsisSysContext *);
PUBLIC INT4 IsisGrStoreRestartInfo (VOID);
PUBLIC UINT4 IsisGrGetSecondsSinceBase (tUtlTm utlTm);
PUBLIC VOID IsisGrCheckAndShutdown (VOID);
PUBLIC VOID IsisGrStartup (tIsisSysContext *pContext);
PUBLIC VOID IsisGrSelfLspGen (tIsisSysContext *pContext, UINT1 u1Level, UINT1 u1flag);
PUBLIC INT4  IsisGRHelloProcess (tIsisSysContext *pContext,tIsisCktEntry *pCktRec,UINT1 u1Level);


PUBLIC INT4 IsIsisGrHelping PROTO ((tIsisSysContext*,UINT1 u1Level));
PUBLIC VOID IsisGrExitHelper PROTO ((tIsisSysContext *,tIsisAdjEntry *,UINT1));
PUBLIC VOID IsisGrHelperTxLSP PROTO ((tIsisSysContext *,tIsisCktEntry *,UINT1 *,UINT1));

PUBLIC VOID IsisGrSelfOLLspGen PROTO ((tIsisSysContext*,UINT1));
PUBLIC VOID IsisGrSelfClearOLLspGen PROTO ((tIsisSysContext*,UINT1));
PUBLIC VOID IsisGRSendSelfLsp PROTO ((tIsisSysContext*,UINT1));
PUBLIC VOID IsisUpdGRCSNPCheck PROTO((tIsisSysContext *,
                                        tIsisCktEntry *, UINT1 *, UINT1));

#ifdef NPAPI_WANTED
PUBLIC INT1 IsisHwProgram PROTO ((UINT1 u1Status));
#endif

#ifdef MBSM_WANTED
PUBLIC UINT1 IsisFsMbsmHwProgram PROTO ((tMbsmSlotInfo * pSlotInfo, UINT1 u1Status));
#endif                                                                                                                                                       

/*ISIS GR SEM*/
PUBLIC VOID IsisGrInvalid              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrRestart              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrSeenRA              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrAdjustT3              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrSeenCsnp             PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrT1Cancel             PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrT1Expire              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrT1ExpireNth           PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrT2Expire              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrT3Expire              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrLspDbSync              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrRunning              PROTO((tGrSemInfo *));
PUBLIC VOID IsisGrCleanUp              PROTO((tIsisSysContext *));
PUBLIC VOID IsisGrRunRestartSem PROTO((UINT2 , UINT2 , tGrSemInfo *));
PUBLIC VOID IsisGrAdjUp              PROTO((tGrSemInfo *));

/* Prototypes for new MT related TLVs */
/* Addition of MT TLVs */
INT4 IsisUpdAddMTTLV (tIsisSysContext *, tIsisMDT *);
INT4 IsisUpdAddExtISReachTLV (tIsisSysContext *, tIsisMDT *, UINT4);
INT4 IsisUpdAddExtMtIPReachTLV (tIsisSysContext *, tIsisMDT *, UINT1, UINT1);
INT4 IsisUpdAddMTISReachTLV (tIsisSysContext *, tIsisMDT *, UINT4);

/* Deletion of MT TLVs */
INT4 IsisUpdDelMTTLV (tIsisSysContext *, tIsisMDT *);
INT4 IsisUpdDelExtISReachTLV (tIsisSysContext *, tIsisMDT *, UINT4);
INT4 IsisUpdDelExtMtIPReachTLV (tIsisSysContext *, tIsisMDT *, UINT1, UINT1);
INT4 IsisUpdDelMTISReachTLV (tIsisSysContext *, tIsisMDT *, UINT4);

/* Modification of MT TLVs */
INT4 IsisUpdModExtISReachTLV (tIsisSysContext *, tIsisMDT *, UINT4);
INT4 IsisUpdModExtMtIPReachTLV (tIsisSysContext *, tIsisMDT *, UINT1);
INT4 IsisUpdModMTISReachTLV (tIsisSysContext *, tIsisMDT *, UINT4);


INT1
IsisSendingMessageToRRDQueue (tIsisSysContext * pContext,
                          UINT4 u4RRDSrcProtoMaskEnable,
                          UINT1 u1MessageType);
INT1
IsisSendingMessageToRRDQueue6 (tIsisSysContext * pContext,
                          UINT4 u4RRDSrcProtoMaskEnable,
                          UINT1 u1MessageType);
VOID
IsisProtoIdFromRtm6 (UINT2 *pu2ProtoId);
INT4
IsisRtmProcessRtChange (tNetIpv4RtInfo * pNetIpRtInfo, UINT2 u2ProtoId);

INT4
IsisRtmProcessUpdate (tNetIpv4RtInfo * pNetIpRtInfo, UINT2 u2ProtoId);

UINT1
IsisGetSubnetmasklen (UINT4 u4Mask);

INT1 IsisRtm6ProcessUpdate (tNetIpv6RtInfo * pNetIpRtInfo, UINT2 u2ProtoId);
INT1 IsisRtm6ProcessRtChange (tNetIpv6RtInfo * pNetIpRtInfo,UINT2 u2ProtoId);

INT1 
IsisHandleImportLevelChange (tIsisSysContext * pContext, UINT1 u1SrcLevel, UINT1 u1Type);
INT1
IsisUpdFillMDTFromIPRATLV(tIsisSysContext * pContext, tIsisMDT  *pMDT, tIsisIPRATLV  *pTlv);
PUBLIC VOID
IsisUtlGetMetric (tIsisSysContext * pContext, UINT1 *pu1Metric,
                   UINT1 *pu1TLVMetric);
INT1
IsisHandleRRDProtoDisable(tIsisSysContext * pContext, UINT2  u2ProtoMask, UINT1 u1Level);

INT1
IsisHandleMetricChange (tIsisSysContext * pContext, UINT1 u1ProtoIndex, UINT4 u4Metric);

/* Utility to convert the given NET ID into Array */
VOID
IsisDotNetToArray(UINT1 *pu1NetId, UINT1 *pu1Val);

UINT4
IsisNetOctetLen (UINT1 *pu1DotStr);

VOID
IsisFormDotStr(UINT4 u4StrLen, UINT1 *pu1Src, CHAR *pcDest);

VOID
IsisDisplayAreaInDot(UINT4 u4StrLen, UINT1 *pu1Src, CHAR *pcDest);

#ifdef RM_WANTED
INT4
IsisRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));
INT4
IsisGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum);
INT4
IsisCliGetShowCmdOutputToFile (UINT1 *);
INT4
IsisCliCalcSwAudCheckSum (UINT1 *, UINT2 *);
INT1
IsisCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);
#endif
VOID IsisFltrHandleDynSyncAudit (VOID);
VOID IsisExecuteCmdAndCalculateChkSum  (VOID);

#endif
