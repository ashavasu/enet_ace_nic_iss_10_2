/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 * $Id: isll.h,v 1.8 2015/07/04 13:12:49 siva Exp $
 *
 * Description: This file contains  definition required for low level routines
 *
 *******************************************************************/

#ifndef __ISLL_H__
#define __ISLL_H__

#include "snmctdfs.h"
#include "snmcdefn.h"
#include "snmccons.h"
#include "stdisilw.h"
#include "fsislow.h"
#include "fsistlow.h"
#include "midconst.h"

/* Constants for Tables associated with Control Module
 */

#define ISIS_LL_LOW_INSTANCE                 0   /* The Minimum value of ISIS
                                                  * Instance
                                                  */ 
#define ISIS_LL_HIGH_INSTANCE             10000  /* The Minimum value of ISIS
                                                  * Instance
                                                  */ 
#define ISIS_LL_MAX_IDX              0x77359400  /* Maximum value as specified 
                                                  * by the MIB for the Indices
                                                  */
#define ISIS_LL_MAX_PATH_SPLITS              32  /* Maximum Path Splits as
                                                  * specified in the MIB
                                                  */
#define ISIS_LL_LOW_MAX_LSP_GEN_INT           1  /* Lower Range of Max Lsp Gen
                                                  * Interval
                                                  */
#define ISIS_LL_HIGH_MAX_LSP_GEN_INT      65535  /* Higher Range of Max LSP Gen
                                                  * Interval
                                                  */
#define ISIS_LL_MAX_PDU_SIZE              16000  /* Maximum Size of the PDU
                                                  * supported by the protocol
                                                  */
#define ISIS_LL_MIN_PDU_SIZE                512  /* Minimum Size of the PDU
                                                  * supported by the protocol
                                                  */
#define ISIS_LL_LOW_MIN_L1LSP_GEN_INT         1  /* Lower Range of Min L1 Lsp 
                                                  * Gen Interval 
                                                  */
#define ISIS_LL_HIGH_MIN_L1LSP_GEN_INT    65535  /* Higher Range of Min L1 LSP
                                                  * Gen Interval
                                                  */
#define ISIS_LL_HIGH_MAX_AREA_ADDRESS       254  /* Higher Range of Maximum
                                                  * number of Area Addresses 
                                                  */
#define ISIS_LL_LOW_MAX_AREA_ADDRESS          3  /* Lower Range of Maximum
                                                  * of Area Addresses
                                                  */
#define ISIS_LL_LOW_MIN_L2LSP_GEN_INT         1  /* Lower Range of Min L2 Lsp
                                                  * Gen Interval
                                                  */
#define ISIS_LL_HIGH_MIN_L2LSP_GEN_INT    65535  /* Higher Range of Min L2 LSP
                                                  * Gen Interval 
                                                  */
#define ISIS_LL_LOW_SYS_WAIT_TIME             1  /* Lower Range of System Wait 
                                                  * Time 
                                                  */
#define ISIS_LL_HIGH_SYS_WAIT_TIME        65535  /* Higher Range of System Wait
                                                  * Time 
                                                  */
#define ISIS_LL_DEF_METRIC_MASK               1  /* Mask to get the zeroth bit
                                                  */
#define ISIS_LL_DEL_METRIC_MASK               2  /* Mask to get the first bit 
                                                  */
#define ISIS_LL_ERR_METRIC_MASK               4  /* Mask to get the second bit
                                                  */
#define ISIS_LL_EXP_METRIC_MASK               8  /* Mask to get the third bit 
                                                  */
#define ISIS_LL_LOW_MIN_SPF_SCH_TIME          1  /* The Lower boundary of
                                                  * SPF Schedule Time
                                                  */ 
#define ISIS_LL_HIGH_MIN_SPF_SCH_TIME       100  /* The Upper boundary of
                                                  * SPF Schedule Time
                                                  */ 
#define ISIS_LL_LOW_MAX_SPF_SCH_TIME          1  /* The Lower boundary of
                                                  * SPF Schedule Time
                                                  */ 
#define ISIS_LL_HIGH_MAX_SPF_SCH_TIME      1000  /* The Upper boundary of
                                                  * SPF Schedule Time
                                                  */ 
#define ISIS_LL_LOW_MIN_LSP_CHGS              1  /* The Lower boundary of
                                                  * Minimum no. of changes
                                                  * in LSPs to trigger decision
                                                  */
#define ISIS_LL_HIGH_MIN_LSP_CHGS           100  /* The Upper boundary of
                                                  * Minimum no. of changes
                                                  * in LSPs to trigger decision
                                                  */
#define ISIS_LL_LOW_MAX_LSP_CHGS              1  /* The Lower boundary of
                                                  * Maximum no. of changes
                                                  * in LSPs to trigger decision
                                                  */
#define ISIS_LL_HIGH_MAX_LSP_CHGS          1000  /* The Upper boundary of
                                                  * Maximum no. of changes
                                                  * in LSPs to trigger decision
                                                  */
#define ISIS_OSI_SUPP                       129  /* Value for OSI Protocol 
                                                  * Support
                                                  */ 
/* Constants for CIRUIT TABLE as per STD. MIB */ 

#define ISIS_LL_MIN_CIRCUIT_IF_INDEX     0x00     
                                          /* Minimum Circuit IfIndex 
                                           * as specified in the MIB
                                           */
#define ISIS_LL_MAX_CIRCUIT_IF_INDEX     0x7FFFFFFF  
                                          /* Maximum Circuit IfIndex 
                                           * as specified in the MIB
                                           */
#define ISIS_LL_MIN_CIRCUIT_IF_SUBINDEX  0x00        
                                          /* Minimum Circuit IfSubIndex 
                                           * as specified in the MIB 
                                           */
#define ISIS_LL_MAX_CIRCUIT_IF_SUBINDEX  0x7FFFFFFF  
                                          /* Maximum Circuit IfSubIndex 
                                           * as specified in the MIB
                                           */
#define ISIS_LL_MIN_CIRCUIT_LOCAL_ID         0      
                                          /* Minimum Circuit Local ID
                                           * as specified in the MIB
                                           */
#define ISIS_LL_MAX_CIRCUIT_LOCAL_ID       255      
                                          /* Maximum Circuit Local ID
                                           * as specified in the MIB 
                                           */
#define ISIS_LL_MESH_INACTIVE                1      
                                          /* State of the Mesh Group in
                                           * this port
                                           */
#define ISIS_LL_MESH_BLOCKED                 2      
                                          /* State of the Mesh Group in
                                           * this port
                                           */
#define ISIS_LL_MESH_SET                     3      
                                          /* State of the Mesh Group in
                                           * this port
                                           */
#define ISIS_LL_MIN_MESH_GROUP               1      
                                          /* Minimum Value of the 
                                           * Circuit Mesh Group 
                                           */
#define ISIS_LL_MAX_MESH_GROUP      2000000000      
                                          /* Maximum Value of the 
                                           * Circuit Mesh Group 
                                           */


/* Circuit Low Level Constants defined as per the Standard MIB
 */

#define ISIS_LL_CKTL_SMALL_HELLO_ON                   1
#define ISIS_LL_CKTL_SMALL_HELLO_OFF                  0
#define ISIS_LL_CKTL_MAX_METRIC                      63
#define ISIS_LL_CKTL_MIN_METRIC                       1
#define ISIS_LL_MIN_METRIC         0
#define ISIS_LL_CKTL_MAX_PRIORITY                   127
#define ISIS_LL_CKTL_MIN_PRIORITY                     1
#define ISIS_LL_CKTL_MAX_HELLO_MULT                 100
#define ISIS_LL_CKTL_MIN_HELLO_MULT                   2
#define ISIS_LL_CKTL_MAX_HELLO_TIMER             600000
#define ISIS_LL_CKTL_MIN_HELLO_TIMER               1000
#define ISIS_LL_CKTL_MAX_DR_HELLO_TIMER          120000
#define ISIS_LL_CKTL_MIN_DR_HELLO_TIMER              10
#define ISIS_LL_CKTL_MAX_LSP_THROTTLE             65535
#define ISIS_LL_CKTL_MIN_LSP_THROTTLE                 1
#define ISIS_LL_CKTL_MAX_MINLSP_RETRANS_INT         300
#define ISIS_LL_CKTL_MIN_MINLSP_RETRANS_INT           1
#define ISIS_LL_CKTL_MAX_CSNP_INT                   600
#define ISIS_LL_CKTL_MIN_CSNP_INT                     1
#define ISIS_LL_CKTL_MAX_PSNP_INT                   120
#define ISIS_LL_CKTL_MIN_PSNP_INT                     1


/* Cicuit Level Table Constants
 */
#define ISIS_LL_CKTL_MIN_METRIC_MT                   0x1
#define ISIS_LL_CKTL_MAX_METRIC_MT                   0xFFFFFE
#define ISIS_LL_GROUP                     1         /* Type of MC address */
#define ISIS_LL_FUNCTIONAL                2         /* Type of MC address */
#define ISIS_LL_SENDING                   1         /* Direction of PDUs */
#define ISIS_LL_RECEIVING                 2         /* Direction of PDUs */
#define ISIS_LL_INITIALISING              1         /* Adjacency State */
#define ISIS_LL_UP                        2         /* Adjacency State */
#define ISIS_LL_FAILED                    3         /* Adjacency State */
#define ISIS_LL_DOWN                      4         /* Adjacency State */
#define ISIS_LL_UNDEFINED_USAGE           1         /* Adjacency Usage */
#define ISIS_LL_L1USAGE                   2         /* Adjacency Usage */
#define ISIS_LL_L2USAGE                   3         /* Adjacency Usage */
#define ISIS_LL_L12USAGE                  4         /* Adjacency Usage */
#define ISIS_LL_MANUAL                    1         /* Type of IPRA Configured by 
                                                     * MANAGER
                                                     */
#define ISIS_LL_AUTOMATIC                 2         /* Learned Through protocol */ 

#define ISIS_LL_ACTIVE                    1         

#define ISIS_LL_SET_L1OVERLOAD            1         /* Administratively sets the
                                                     * the overload bit for level1
                                                     */
#define ISIS_LL_SET_L2OVERLOAD            2         /* Administratively sets the
                                                     * the overload bit for level2
                                                     */
#define ISIS_LL_SET_L12OVERLOAD           3         /* Administratively sets the
                                                     * the overload bit for
                                                     * level12 */
#define ISIS_LL_CLEAR_OVERLOAD          4           /* Administratively clears 
                                                     * the overload bit
                                                     */

#define ISIS_LL_MAX_EVENT               0x82        /* Maximum possible Event
                                                     * that can occur
                                                     */
#define ISIS_LL_MAX_EVTIDX              0xff        /* Maximum index value of
                                                     * the Event Index
                                                     */
#define ISIS_LL_MAX_LOGLEVEL            0xff        /* Maximum Log level 
                                                     */
#endif
