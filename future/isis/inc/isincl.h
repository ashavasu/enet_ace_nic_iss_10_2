/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isincl.h,v 1.10 2016/02/29 11:07:14 siva Exp $
 *
 * Description: This file contains all the files to be included
 *
 *****************************************************************************/

#ifndef __ISINCL_H__
#define __ISINCL_H__

/* FSAP Include files
 */

#include "lr.h"

#include "rmap.h"
#ifdef ISIS_FT_ENABLED
#include "isfltmac.h"
#endif

#include "isdefs.h"
#include "ismacs.h"
#include "isport.h"
#include "istdfs.h"
#include "isevt.h"
#include "isll.h"
#include "bdyexp.h"
#include "isprot.h"
#include "isdlli.h"
#include "stdisiwr.h"
#include "fsisiswr.h"
#include "cli.h"
#include "iss.h"
#include "csr.h"
#include "vcm.h"
#include "rmgr.h" 
#include "isissz.h"
#include "fssyslog.h"
#include "utilalgo.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ipnpwr.h"
#endif

#ifdef ISS_WANTED
#include "msr.h"
#endif

#endif /* __ISINCL_H__ */
