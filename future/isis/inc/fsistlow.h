/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: fsistlow.h,v 1.2 2009/06/03 06:39:01 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsIsisExtSysShortestPathTable. */
INT1
nmhValidateIndexInstanceFsIsisExtSysShortestPathTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysShortestPathTable  */

INT1
nmhGetFirstIndexFsIsisExtSysShortestPathTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtSysShortestPathTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtSysShortestPathNextHopIPAddr ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
