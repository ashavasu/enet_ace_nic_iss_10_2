/*******************************************************************
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 * $Id: isdefs.h,v 1.29 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file contains  various hashdefs used in the 
 *              ISIS Module
 *
 *******************************************************************/

#ifndef __ISDEFS_H__
#define __ISDEFS_H__

#include "isis.h"


#define ISIS_LGST                     (CHAR *)gai1IsisLogStr
#define ISIS_LGTM                    (CHAR *)gai1IsisTmLogStr

/* Size of SNMPv2 trap id */
#define SNMP_V2_TRAP_OID_LEN            11

#define GR_ENABLED 1
#define GR_DISABLED 2

#define ISIS_SIZING_CONTEXT_COUNT   FsISISSizingParams[MAX_ISIS_INSTANCES_SIZING_ID].u4PreAllocatedUnits
/* IANA I/f Types
 */

#define ISIS_IF_ETH_CSMACD            6
#define ISIS_IF_L3IPVLAN              136
#define ISIS_IF_PPP                   23
#define ISIS_IF_FR_DTE                32
#define ISIS_IF_FR_DCE                44
#define ISIS_IF_ATM_AAL5              49
#define ISIS_IF_INVALID               0
                                      /* This is used to discriminate that
                                       * interface doesnot exists
                                       */

#define ISIS_MAX_SEM_VALUE        4095 /*Decimal Value for FFF*/

/*Macros for dynamic sync audit CLI implementation*/
#ifdef RM_WANTED
#define ISIS_AUDIT_FILE_ACTIVE        "/tmp/isis_output_file_active"
#define ISIS_AUDIT_FILE_STDBY         "/tmp/isis_output_file_stdby"
#define ISIS_DYN_MAX_CMDS             19
#define ISIS_CLI_EOF                  2
#define ISIS_CLI_NO_EOF               1
#define ISIS_CLI_RDONLY               OSIX_FILE_RO
#define ISIS_CLI_WRONLY               OSIX_FILE_WO
#define ISIS_CLI_MAX_GROUPS_LINE_LEN  200
#endif

/* The following constants refer to the global structure which gets updated
 * during boot up. These constants should not be modified.
 */

#define ISIS_MAX_INSTS                 gIsisMemActuals.u4MaxInsts 
#define ISIS_MAX_LSPS                  gIsisMemActuals.u4MaxLSP   
#define ISIS_MAX_CKTS                  gIsisMemActuals.u4MaxCkts     
#define ISIS_MAX_ARAD                  gIsisMemActuals.u4MaxAreaAddr
#define ISIS_MAX_ADJS                  gIsisMemActuals.u4MaxAdjs
#define ISIS_MAX_ROUTES                gIsisMemActuals.u4MaxRoutes
#define ISIS_MAX_IPRA                  gIsisMemActuals.u4MaxIPRA
#define ISIS_MAX_SADDR                 gIsisMemActuals.u4MaxSA
#define ISIS_MAX_IPIF                  gIsisMemActuals.u4MaxIPIF    
#define ISIS_MAX_EVTS                  gIsisMemActuals.u4MaxEvents
#define ISIS_MAX_TXQL                  gIsisMemActuals.u4MaxLspTxQLen

#define ISIS_MAX_MEM_POOLS             20
                                      /* Ideally this value would be the number
                                       * of elements in the structure
                                       * gIsisMemActuals
                                       */

/* Configuration Modes - Each of these modes defines the typical values for the
 * memory related configuration objects for ISIS. The values can be modified at
 * compile time in isport.h, or at run time, through SNMP
 */

#define ISIS_CFG_LOW                   0x01 /* Brings up the system with values
                                             * defined for Low-End System.
                                             * Refer to isport.h for the actaul
                                             * values
                                             */ 
#define ISIS_CFG_MEDIUM                0x02 /* Brings up the system with values
                                             * defined for Medium-End System.
                                             * Refer to isport.h for the actaul
                                             * values
                                             */ 
#define ISIS_CFG_HIGH                  0x03 /* Brings up the system with values
                                             * defined for High-End System.
                                             * Refer to isport.h for the actaul
                                             * values
                                             */ 
#define ISIS_CFG_CUSTOM_LOW            0x04 /* Brings up the system with values
                                             * defined by the user. If the user
                                             * does not include values for
                                             * certain objects, then Low-End
                                             * system values will be used for 
                                             * such objects
                                             * Refer to isport.h for the actaul
                                             * values
                                             */ 
#define ISIS_CFG_CUSTOM_MEDIUM         0x05 /* Brings up the system with values
                                             * defined by the user. If the user
                                             * does not include values for
                                             * certain objects, then Medium-End
                                             * system values will be used for 
                                             * such objects
                                             * Refer to isport.h for the actaul
                                             * values
                                             */ 
#define ISIS_CFG_CUSTOM_HIGH           0x06 /* Brings up the system with values
                                             * defined by the user. If the user
                                             * does not include values for
                                             * certain objects, then High-End
                                             * system values will be used for 
                                             * such objects
                                             * Refer to isport.h for the actaul
                                             * values
                                             */ 
#define ISIS_CFG_CUSTOM                0x07 /* Manager should provide values 
                                             * for all the memory variables. 
                                             * If values for one or more of
                                             * the variables is not provided
                                             * the implementation currently
                                             * considers the ISIS_CFG_LOW
                                             * values for such variables.
                                             * For all other variables the 
                                             * values provided by the manager
                                             * will be used
                                             */ 

/* Message Types that are encoded into the Interface Message exchanged across
 * various modules
 */

#define ISIS_MSG_LL_DATA               0xF1 
                                     /* Messages received from lower layer
                                      * should carry this tag in the Message
                                      * type field
                                      */ 
#define ISIS_MSG_EVENT                 0xF2
                                     /* All the Events posted by various modules
                                      * during protocol operations should carry
                                      * this tag in the Message type field
                                      */ 
#define ISIS_MSG_HL_DATA               0xF3
                                     /* Any Protocol Data generated by the IS
                                      * should carry this tag in the Message
                                      * type field
                                      */ 
#define ISIS_MSG_IF_STAT_IND           0xF5
                                     /* Interface status indications received
                                      * from Lower Layer drivers should carry
                                      * this tag in the Message type field
                                      */ 
#define ISIS_MSG_FTM_DATA              0xF6
                                     /* Fault Tolerance Messages received from
                                      * FSAR
                                      */ 
#define ISIS_MSG_ROUTE_INFO            0xF7
                                     /* Route Info message Sent by the 
                                      * Decision module, to be updated in Route
                                      * cache
                                      */
#define ISIS_MSG_RTM_DATA              0xF8
                                     /* Registration/Un-registration responses
                                      * from RTM module
                                      */
#define ISIS_MSG_RTM6_DATA              0xF9
                                     /* Registration/Un-registration responses
                                      * from RTM module
                                      */

#ifdef ROUTEMAP_WANTED
#define ISIS_MSG_ROUTEMAP_UPDATE       0xFA    
                                     /* Update Message from Route Map module. */
#endif /* ROUTEMAP_WANTED */
#define ISIS_MSG_IF_ADDR_CHG           0xFB
                                       /* For Interface address change indication
                                        * from the lower layer */

#define ISIS_MSG_NO_RLEAK               0xFC /*msg event for handling no 
                                               route leak*/

#define ISIS_SYS_MSG_DATA           0XFD /*MBSM */

#define ISIS_MSG_SEC_IF_STAT_IND           0xFE

#define ISIS_MSG_BFD_SESSION_STATE_NOTIFICATION 0xFF /* BFD session status change notification from BFD */

/* RTM EVENT */
#define ISIS_SYS_ROUTE_UPDATE            0X1F
#define ISIS_SYS_ROUTE6_UPDATE           0X2F
#define ISIS_SYS_ROUTE_CHANGE            0X3F
#define ISIS_SYS_ROUTE6_CHANGE           0X4F



#define  ISIS_SET_MASK                 0x80

/* Data Types
 */

#define ISIS_DATA_RAW                  0x01
                                     /* The received data includes the Ethernet
                                      * header. Control Module while processing
                                      * this type of data must make sure that
                                      * the header is skipped appropriately
                                      */ 
#define ISIS_DATA_NORMAL               0x02
                                     /* The received data does not include any
                                      * Ethernet headers
                                      */ 
/* Masks
 */

#define ISIS_SPT_L1_SYS_MASK           0x4000
                                     /* Used for setting bits in the higher 
                                      * order byte of the Metric maintained 
                                      * in the SPT nodes. Sets the flag to 
                                      * indicate that the Metric is associated 
                                      * with a L1 System
                                      */
#define ISIS_SPT_L2_SYS_MASK           0x8000
                                     /* Used for setting bits in the higher 
                                      * order byte of the Metric maintained 
                                      * in the SPT nodes. Sets the flag to 
                                      * indicate that the Metric is associated 
                                      * with a L2 System
                                      */ 
#define ISIS_SPT_L12_SYS_MASK          0xc000
                                     /* Used for setting bits in the higher 
                                      * order byte of the Metric maintained 
                                      * in the SPT nodes. Sets the flag to 
                                      * indicate that the Metric is associated 
                                      * with a L12 System
                                      */ 
#define ISIS_SPT_IS_FLAG               0x2000
                                     /* Used for setting bits in the higher
                                      * order byte of the Metric maintained
                                      * in SPT node. Set the flag to 
                                      * indicate an OSI Intermediate system
                                      * else reset the flag for IP Reachable
                                      * addresses
                                      */ 
#define ISIS_SPT_IPADDR_TYPE_MASK      0x8000
                                     /* Mask Used to Extract IPRA Address Type
                                      */
#define ISIS_SPT_SELF_IPRA_MASK        0x4000
                                     /* Mask Used to Extract Self IPRA
                                      */ 
#define ISIS_IPV4_ADDR_TYPE            0x0000
#define ISIS_IPV6_ADDR_TYPE            0x8000
                                     /* Indicates the IPRA Address Type.
                                      */
#define ISIS_SELF_IPRA_ADJ             0x4000
                                     /* Indicates Self IPRA Adjacency, which is
                                      * used in forming the Route Update to
                                      * send to Trie or RTM
                                      */
#define ISIS_NBR_IPRA_ADJ             0x0000
                                     /* Indicates Nbr IPRA Adjacency, which is
                                      * used in forming the Route Update to
                                      * send to Trie or RTM
                                      */
#define ISIS_SPT_ATT_FLAG              0x1000
                                     /* Used for setting bits in the higher
                                      * order byte of the Metric maintained
                                      * in SPT node. Set the flag to 
                                      * indicate an IS is attached
                                      */ 
                                       
#define ISIS_SPT_INV_METRIC            0xF7FF
                                     /* Indicates an Invalid Metric, which is
                                      * used for initialising SPT node's Metric
                                      * array.
                                      */

#define ISIS_SPT_RL_INV_METRIC         0xFFFFFFFF
                                    /* Indicates an invalid metric, which is
                                     * used for initializing SPT metric */


                                     /* bits---> fedc ba98 7654 3210 */
#define ISIS_SPT_MET_FLAG              0x03ff /* 0000 0011 1111 1111 */ 
                                    
                                     /* Used for extracting the Metric Value
                                      * including Internal/External Bit from
                                      * the PATH metric in SPT nodes
                                      */

#define ISIS_SPT_ECM_FLAG              0xc0 /* 1100 0000 */ 
                                    
                                     /* Used for extracting the route type 
                                      * from ECMP of SPT node 
                                      */

#define ISIS_SPT_ECM_RESET_FLAG       0x3f /* 0011 1111 */

                                     /* Used for resetting the route type 
                                      * from ECMP of SPT node 
                                      */

#define ISIS_SPT_UNCHG_FLAG            0x00 /* 0000 0000 */ 
                                    
                                     /* Flag indicating that this route is
                                      * not modified from the precious SPT run 
                                      * to current SPT run 
                                      */

#define ISIS_SPT_MOD_FLAG              0x40 /* 0100 0000 */ 
                                    
                                     /* Flag indicating that this route is
                                      * modified from the precious SPT run to
                                      * current SPT run 
                                      */

#define ISIS_SPT_NEW_FLAG              0x80 /* 1000 0000 */ 
                                    
                                     /* Flag indicating that this route is found
                                      * is  found first time 
                                      */

                                     

#define ISIS_SPT_EXT_MET_FLAG          0x0400 /* 0000 0100 0000 0000 */ 
                                     
                                     /* Used for setting the Metric Type
                                      * (Internal/External Bit) in 
                                      * the PATH metric in SPT nodes
                                      */

/* Constants to denote change in Route
 */

#define ISIS_SPT_ROUTE_ADD               1 /* 0000 0001 */
#define ISIS_SPT_ROUTE_MET_CHG           2
#define ISIS_SPT_ROUTE_IFIDX_CHG         3
#define ISIS_SPT_ROUTE_DELETE            4

/* Location of SPT Nodes            
 */

#define ISIS_SPT_LOC_NONE              0x00
                                     /* Used for indicating the absence of SPT
                                      * Node both in Tent, IP Path and OSI Path
                                      */
#define ISIS_SPT_LOC_TENT              0x01
                                     /* Indicates the presence of SPT Node in
                                      * Tent
                                      */

/* A node may be present in the PATH for one of the metrics and not all. The
 * following constants indicates whether the node found in the PATH is for the
 * required metric.
 */

#define ISIS_SPT_LOC_PATH_SAME_FIRST   0x02
                                     /* Indicates the presence of SPT Node in
                                      * Path either in OSI Path or IP Path for
                                      * the given metric as the First element in
                                      * the given Hash Bucket
                                      */

#define ISIS_SPT_LOC_PATH_SAME         0x03
                                     /* Indicates the presence of SPT Node in
                                      * Path either in OSI Path or IP Path for
                                      * the given metric
                                      */

#define ISIS_SPT_LOC_PATH_DIFF_FIRST   0x04
                                     /* Indicates the presence of SPT Node in
                                      * Path either in OSI Path or IP Path for a
                                      * different Metric as the First element in
                                      * the given Hash Bucket
                                      */

#define ISIS_SPT_LOC_PATH_DIFF         0x05
                                     /* Indicates the presence of SPT Node in
                                      * Path either in OSI Path or IP Path for a
                                      * different Metric
                                      */
#define ISIS_SPT_LOC_PATH_SAME_DIRIDX  0x06
                                     /* Indicates the presence of SPT Node is
                                      * either in OSI Path or IP Path for
                                      * the given metric
                                      */



/* Following flag is used for marking SPT nodes in the previous PATH Database.
 * After the SPF is completed, SPT nodes in the Previous PATH Database which are
 * not marked would be freed.
 */

#define ISIS_SPT_MARK                  0x0800
                                     /* Flag used for marking the SPT nodes in
                                      * the Previous PATH so that they do not
                                      * get deleted after the SPF run.
                                      */
/* ISIS PDU Types
 */

#define ISIS_ISH_PDU                   0x04 /* ISH Pdu Type (4) */
#define ISIS_L1LSP_PDU                 0x12 /* L1LSP Pdu Type */
#define ISIS_L2LSP_PDU                 0x14 /* L2LSP Pdu Type */
#define ISIS_L1PSNP_PDU                0x1A /* L1PSNP Pdu Type */
#define ISIS_L2PSNP_PDU                0x1B /* L2PSNP Pdu Type */
#define ISIS_L1CSNP_PDU                0x18 /* L1CSNP Pdu Type */
#define ISIS_L2CSNP_PDU                0x19 /* L2CSNP Pdu Type */
#define ISIS_P2P_IIH_PDU               0x11 /* P2P IIH Pdu Type */
#define ISIS_L1LAN_IIH_PDU             0x0F /* L1LAN IIH Pdu Type */ 
#define ISIS_L2LAN_IIH_PDU             0x10 /* L2LAN IIH Pdu Type */

/* TLV Types
 */

/* IS0 10589
 */ 

#define ISIS_AREA_ADDR_TLV              1 /* Area Address TLV */
#define ISIS_IS_ADJ_TLV                 2 /* IS ADJ TLV */
#define ISIS_IS_NEIGH_ADDR_TLV          6 /* LAN Addr of the IS, TLV */ 
#define ISIS_PADDING_TLV                8 /* Padding TLV */
#define ISIS_LSP_ENTRY_TLV              9 /* LSP Entry TLV in SNP*/
#define ISIS_AUTH_INFO_TLV             10 /* authentication TLV */
#define ISIS_LSP_BUF_SIZE_TLV          14 /* LSP Buffer Size TLV */

/* RFC 1195
 */

#define ISIS_IP_INTERNAL_RA_TLV        128 /* IP Internal RA TLV */
#define ISIS_PROT_SUPPORT_TLV          129 /* Protocol Support TLV */
#define ISIS_IP_EXTERNAL_RA_TLV        130 /* IP External RA TLV */
#define ISIS_IPV6_RA_TLV               236 /* IPV6 Reachability TLV */
#define ISIS_IDRP_INFO_TLV             131 /* InterDomain Routing Protocol
                                             * Identifier 
                                             */
#define ISIS_IPV4IF_ADDR_TLV           132 /* IP IF Address TLV */
#define ISIS_IPV6IF_ADDR_TLV           232 /* IPV6 IF Address TLV */

/* Multi-topology TLVs */
#define ISIS_MT_TLV                     229
#define ISIS_EXT_IS_REACH_TLV           22
#define ISIS_EXT_IP_REACH_TLV           135
#define ISIS_MT_IPV6_REACH_TLV          237
#define ISIS_MT_IS_REACH_TLV            222

/*RFC 5303 - P2P three way handshake*/
#define ISIS_P2P_THREE_WAY_ADJ_TLV  240

/* RFC 6213 - BFD for M-ISIS TLV */
#define ISIS_BFD_ENABLED_TLV          148

/*RFC 5301 - Dynamic hostname TLV */
#define ISIS_DYN_HOSTNME_TLV          137

#define ISIS_EXTERNAL_METRIC          0x01
#define ISIS_INTERNAL_METRIC          0x02


#define ISIS_IPV6_MAX_PREFIX_LEN        128
#define ISIS_IPV4_MAX_PREFIX_LEN        32

#define ISIS_IPV4_ADDR_INDEX 1
#define ISIS_IPV6_ADDR_INDEX 2


/* Password Type
 */
#define ISIS_CLR_TXT_PASS                1 /* Clear Text Password */
#define ISIS_MD5_PASS                    54 /* MD5 digest */

/* Setting SRM flags
 */

#define ISIS_ALL_CKT                   0x01 /* Set SRM flags for 
                                             * All Circuits 
                                             */
#define ISIS_SPEC_CKT                  0x02 /* Set SRM flags for the
                                             * specified circuit
                                             */
#define ISIS_ALL_BUT_SPEC_CKT          0x03 /* Set SRM flags for all
                                             * the circuits except
                                             * the specified one
                                             */

#define ISIS_IPV6_IPRA_TLV_LEN         (4 + 2)

#define ISIS_IPV6_ADDR_TLV_LEN         ISIS_MAX_IP_ADDR_LEN

#define ISIS_CTRL_OCTET_LEN            0x01
                                     /* The control octet len in IPV6 RA TLV
                                       */
#define ISIS_FLG_DOWN_SET              0x80 /* 1000 0000 */
                                     /* Used to extract the UP/DOWN bit*/



/* Metric Types
 */

#define ISIS_INTERNAL                  0x00  /* Metric Type is 
                                              * Internal
                                              */
#define ISIS_EXTERNAL                  0x01  /* Metric Type is 
                                              * External
                                              */

/* Self LSP Types
 */

#define ISIS_L1_NON_PSEUDO_LSP         0x01
#define ISIS_L2_NON_PSEUDO_LSP         0x02
#define ISIS_L1_PSEUDO_LSP             0x03
#define ISIS_L2_PSEUDO_LSP             0x04

/* LSP Comparison codes
 */

#define ISIS_NEW_LSP                   0x01 /* A New LSP which does not exist in
                                             * the database.
                                             */
#define ISIS_DIFF_CS_GRTR_SEQNO        0x02 /* An LSP with a different Checksum
                                             * and Greater Sequence Number than
                                             * the LSP that exist in the
                                             * database
                                             */
#define ISIS_SAME_CS_GRTR_SEQNO        0x03 /* An LSP with same Checksum
                                             * and Greater Sequence Number than
                                             * the LSP that exist in the
                                             * database
                                             */
#define ISIS_SAME_CS_LESS_SEQNO        0x04 /* An LSP with same Checksum
                                             * and Less Sequence Number than the
                                             * LSP that exist in the database
                                             */
#define ISIS_DIFF_CS_LESS_SEQNO        0x05 /* An LSP with different Checksum
                                             * and Less Sequence Number than the
                                             * LSP that exist in the database
                                             */
#define ISIS_SAME_CS_SAME_SEQNO        0x06 /* An LSP with same Checksum
                                             * and same Sequence Number as the
                                             * LSP that exist in the database
                                             */
#define ISIS_DIFF_CS_SAME_SEQNO        0x07 /* An LSP with different Checksum
                                             * and same Sequence Number as the
                                             * LSP that exist in the database
                                             */ 
/* LSP Locations
 */

#define ISIS_LOC_DB                    0x01 /* LSP located in DB
                                             */
#define ISIS_LOC_DB_FIRST              0x02 /* LSP is the very first entry
                                             * located in the LSP Database.
                                             */
#define ISIS_LOC_TX                    0x03 /* LSP located in Transmission
                                             * queue
                                             */
#define ISIS_LOC_TX_FIRST              0x04 /* LSP is the very first entry
                                             * located in the LSP Transmission
                                             * queue.
                                             */
#define ISIS_LOC_NONE                  0x05 /* LSP is not present
                                             * in the database
                                             */
#define ISIS_LOC_SELF                  0x06 /* LSP is a new SelfLSP being 
                                             * generated for the first time
                                             */

/* The Return codes after processing LETLV in SNPs
 */

#define ISIS_NOT_MOVED                 0x07 /* The Record has not moved
                                             */
#define ISIS_MOVED_TO_DB               0x08 /* The Tx Record has moved to DB
                                             * from TxQ
                                             */ 

#define ISIS_MOVED_TO_TXQ              0x09 /* The Db Record has moved to TxQ
                                             * from DB
                                             */ 

/* DIS Status Codes
 */

#define ISIS_DIS_LOCAL_ELECTED         0x01 /* After DIS election, local system 
                                             * has become the new DIs for a 
                                             * given circuit
                                             */
#define ISIS_DIS_LOCAL_RESIGNED        0x02 /* Local IS has resigned as DIS 
                                             * since a remote system is now
                                             * elected as DIS
                                             */ 
#define ISIS_DIS_REMOTE_CHANGED        0x03 /* DIS for a circuit has changed 
                                             * from some remote system X to 
                                             * remote system Y
                                             */
#define ISIS_DIS_NO_CHANGE             0x04 /* There is no change in DIS for the
                                             * specified circuit
                                             */ 
/* Area Locations
 */

#define ISIS_SAME_AREA                 0x01 /* Located in the same area
                                             */
#define ISIS_OTHER_AREA                0x02 /* Located in the different area
                                             */

/* Definitions to specify which entry is pruned from the adjacency database.
 */

#define ISIS_EXIST_ENTRY_PRUNED        0x01 /* An existing adjacency entry
                                             * is pruned.
                                             */
#define ISIS_NEW_ENTRY_PRUNED          0x02 /* New adjacency entry is pruned
                                             */

/* Authentication Status
 */

#define ISIS_AUTH_BOTH_DISABLED        0x04 /* Authentication in the transmit
                                             * and receive direction disabled
                                             */

#define ISIS_AUTH_TX_ENABLED           0x05 /* Authentication in the transmit
                                             * direction enabled
                                             */
#define ISIS_AUTH_RX_ENABLED           0x06 /* Authentication in the receive
                                             * direction enabled
                                             */

#define ISIS_AUTH_BOTH_ENABLED         0x07 /* Authentication in the transmit
                                             * and receive direction enabled
                                             */
/* Authentication Type
 */

#define ISIS_AUTH_PASSWD               0x01 /* Clear text Password 
                                             * authentication
                                             */ 
                                             

/* The Equivalence class timer types. Each type of timer will hold a list of
 * records which require a particular kind of timer expiry service. There may be
 * many such lists in the EC blocks depending on the duration for which a
 * paricular timer request is made.
 */

#define ISIS_ECT_P2P_ISH               0x01 /* Holds information regarding all
                                             * circuits whose Point-to-Point 
                                             * Hello timer is active.
                                             */
#define ISIS_ECT_P2P_HELLO             0x02 /* Holds information regarding all
                                             * circuits whose Point-to-Point 
                                             * Hello timer is active.
                                             */
#define ISIS_ECT_L1LANHELLO            0x03 /* Holds information regarding all
                                             * circuits whose LAN Hello timer 
                                             * is active.
                                             */
#define ISIS_ECT_L2LANHELLO            0x04 /* Holds information regarding all
                                             * circuits whose LAN Hello timer 
                                             * is active.
                                             */
#define ISIS_ECT_HOLDING               0x05 /* Holds information regarding all
                                             * Adjacencies whose Holding timer 
                                             * is active.
                                             */
#define ISIS_ECT_L1_LSP_TX             0x06 /* Holds information regarding all
                                             * L1 LSPs whose Propogation timer 
                                             * is active.
                                             */
#define ISIS_ECT_L2_LSP_TX             0x07 /* Holds information regarding all
                                             * L2 LSPs whose Propogation timer 
                                             * is active.
                                             */
#define ISIS_ECT_L1_CSNP               0x08 /* Holds information regarding all
                                             * circuits whose L1 CSNP timer is 
                                             * active.
                                             */
#define ISIS_ECT_L2_CSNP               0x09 /* Holds information regarding all
                                             * circuits whose L2 CSNP timer is
                                             * active
                                             */
#define ISIS_ECT_L1_PSNP               0x0A /* Holds information regarding all
                                             * circuits whose L1 PSNP timer is 
                                             * active.
                                             */
#define ISIS_ECT_L2_PSNP               0x0B /* Holds information regarding all
                                             * circuits whose L2 PSNP timer is
                                             * active
                                             */
#define ISIS_ECT_ZERO_AGE              0x0C /* Holds information regarding all
                                             * LSPS whose Zero Age timer is 
                                             * active
                                             */
#define ISIS_ECT_MAX_AGE               0x0D /* Holds information regarding all
                                             * LSPS whose MaxAge timer is active
                                             */


#define ISIS_ECT_BLK_1                 0x40
#define ISIS_ECT_BLK_2                 0x80
#define ISIS_ECT_BLK_3                 0xC0

/* These masks may have to be changed based on the values of 
 * ISIS_TMR_EC1_TIME_UNIT, ISIS_TMR_EC2_TIME_UNIT and ISIS_TMR_EC3_TIME_UNIT.
 * Read the CAUTION given for these definitions
 */

#define ISIS_EC_IDX_MASK               0x3F
#define ISIS_EC_BLK_MASK               0xC0

#define ISIS_EC1_MAX_DUR               30
#define ISIS_EC2_MAX_DUR               300
#define ISIS_EC3_MAX_DUR               1200

/* CAUTION: If the following values are modified please take care of the
 * following:
 *
 * We have an ID stored in LSP records, Adjacency Records which is '1' byte in
 * length, which is a combination of Equivalence Class ID and the Array Index.
 * Since we have 3 Equivalence Class of timers we have allocated 2 bits in the
 * ID for this purpose. Remaining 6 bits are used for noting down the array
 * index which can be a maximum of '64'. With the current values we have the
 * following for the 3 class of timers:
 *
 * EC-1, Max Duration is 30 secs., interval is 1 sec. and hence 30 blocks which
 * is less than 64
 * EC-2, Max Duration is 300 secs., interval is 10 secs., and hence 30 blocks
 * which is les than 64
 * EC-3, Max duration is 1200 secs., interval is 100 secs., and hence 12
 * blocks which is less than 64.
 *
 * All the block Indices (array indices) and EC IDs can be fit into a single
 * byte. 
 *
 * Suppose the values are modified as 1 for EC-1, 2 for EC-2 and 50 for EC-3,
 * then we will have 30 blocks in EC-1, 150 blocks in EC-2 and 24 blocks in
 * EC-3. EC-1 and EC-3 cases are fine where as EC-2 required 150 blocks which
 * requires at least 8 bits i.e. the entire byte to hold this index which is
 * unavailable in the ID stored in LSP records, Adjacency records etc. since 2
 * bits are already allocated for thre EC ID. Hence modifying these values
 * beyond the limits will require increasing the ID length in LSPs & 
 * Adjacencies.
 */
  
#define ISIS_TMR_EC1_TIME_UNIT         1
#define ISIS_TMR_EC2_TIME_UNIT         10
#define ISIS_TMR_EC3_TIME_UNIT         100


#define ISIS_MATCH_REC                 1
#define ISIS_NON_MATCH_REC             2

/* Protocol PDU field Masks used for reading fields from PDUs
 */
                                    /* bits--> 8765 4321 */

#define ISIS_ATT_MASK                  0x78 /* 0111 1000 */
#define ISIS_DBOL_MASK                 0x04 /* 0000 0100 */
#define ISIS_ISTYPE_MASK               0x03 /* 0000 0011 */
#define ISIS_METRIC_VAL_MASK           0x3F /* 0011 1111 */
#define ISIS_PDU_TYPE_MASK             0x1F /* 0001 1111 */
#define ISIS_CKT_TYPE_MASK             0x03 /* 0000 0011 */
#define ISIS_PRIORITY_MASK             0x7F /* 0111 1111 */
#define ISIS_PART_REPAIR_MASK          0x80 /* 1000 0000 */

#define ISIS_METRIC_TYPE_MASK          0x40 /* 0100 0000 */ 
                                            /* Metric Type is External if the
                                             * 7th bit is set in the Metric,
                                             * else it shall be internal
                                             */
                                             
#define ISIS_METRIC_SUPP_MASK          0x80 /* 1000 0000 */
                                            /* Metric is not supported for the
                                             * specified Metric Type if the 8th
                                             * bit is set else that metric 
                                             * type is supported
                                             */
/*MISIS*/
#define ISIS_PREFIX_MASK         0x3F /*0011 1111*/
           /*used in case of Extended IP Reachability TLV*/
#define ISIS_EXTN_SUBTLV_MASK         0x40 /*0100 0000*/
#define ISIS_MT_IPV6_SUBTLV_MASK       0x20 /*0010 0000*/
/* Field Masks used for setting fields in PDUs
 */

                                  /* bits--> 8765 4321 */

#define ISIS_FLG_ATT_DEF               0x08 /* 0000 1000 */
                                     /* Used for setting the Flags field of an
                                      * LSP when the local system is attached
                                      * through Default metric to other areas.
                                      */ 
#define ISIS_FLG_ATT_DEL               0x10 /* 0001 0000 */
                                     /* Used for setting the Flags field of an
                                      * LSP when the local system is attached
                                      * through Delay metric to other areas.
                                      */ 
#define ISIS_FLG_ATT_EXP               0x20 /* 0010 0000 */
                                     /* Used for setting the Flags field of an
                                      * LSP when the local system is attached
                                      * through Expense metric to other areas.
                                      */ 
#define ISIS_FLG_ATT_ERR               0x40 /* 0100 0000 */
                                     /* Used for setting the Flags field of an
                                      * LSP when the local system is attached
                                      * through Error metric to other areas.
                                      */ 

#define ISIS_FLG_DBOL                  0x04 /* 0000 0100 */
                                     /* Used for setting the Flags field of an
                                      * LSP when the local system is overloaded
                                      */ 
#define ISIS_FLG_L1IS                  0x01 /* 0000 0001 */
                                     /* Used for setting the Flags field of an
                                      * LSP when the local system is an Level 1
                                      * Intermediate System
                                      */ 
#define ISIS_FLG_L2IS                  0x03 /* 0000 0011 */
                                     /* Used for setting the Flags field of an
                                      * LSP when the local system is an Level 2
                                      * Intermediate System
                                      */ 
/* IP Address Types
 */

#define ISIS_ADDR_IPV4                 0x01
#define ISIS_ADDR_IPV6                 0x02
#define ISIS_ADDR_IPVX                 0x03

/* Lengths
 */

#define ISIS_RAW_DATA_HDR_LEN          0x11 
                                      /* 14 bytes Ethernet header + 3 bytes LLC
                                       */
#define ISIS_OFFSET_SRC_MAC_ADDR       0x6
                                      /* The Source MAC Address offset in
                                       * Ethernet header
                                       */ 
#define ISIS_ISH_HDR_LEN               0x09
                                      /* The Length of ISH PDU Header Length
                                       */
#define ISIS_ISH_CS_OFFSET             0x07
                                      /* The Offset at which Check sum is 
                                       * present in ISH PDU
                                       */ 
#define ISIS_ISH_NET_OFFSET            0x09
                                      /* The Offset at which the Network Entity
                                       * Title is present in the ISH PDU
                                       */ 
#define ISIS_LAN_ADDR_LEN              0x06 
                                     /* LAN Address refers to the
                                      * Ethernet Address Length which is 6 bytes
                                      */
#define ISIS_LSPID_LEN                 (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN + 1)
                                     /* LSP ID is comprised of System ID,
                                      * Pseudonode and LSP Number
                                      */ 
#define ISIS_LAN_IIH_HDR_LEN           (sizeof (tIsisComHdr) +\
                                        6 + ISIS_SYS_ID_LEN  +\
                                        ISIS_SYS_ID_LEN      +\
                                        ISIS_PNODE_ID_LEN)
                                     /* Size of LAN IIH Header which includes
                                      * Common Header, Circuit Type (2), Source
                                      * ID (ISIS_SYS_ID_LEN), Holding Time (2),
                                      * PDU Length (2), Priority (1), and LAN ID
                                      * (ISIS_SYS_ID_LEN + 1)
                                      */ 
#define ISIS_P2P_IIH_HDR_LEN           (sizeof (tIsisComHdr) +\
                                        6 + ISIS_SYS_ID_LEN)
                                     /* Size of P2P IIH Header which includes
                                      * Common Header, Circuit Type (1), Source
                                      * ID (ISIS_SYS_ID_LEN), Holding Time (2),
                                      * PDU Length (2), Local CircuitID (1)
                                      */ 
#define ISIS_LSP_HDR_LEN               (sizeof (tIsisComHdr) + 2 + 2 + \
                                        ISIS_LSPID_LEN + 4 + 2 + 1)
                                     /* Length of the LSP PDU Header, 
                                      * consisting of 
                                      * Common header, 
                                      * PDU Length (2), 
                                      * Remaining Life Time (2), 
                                      * LSP ID (ISIS_LSPID_LEN),
                                      * Sequence Number (4),
                                      * Checksum (2) and the 
                                      * LSP Flags (1)
                                      */
#define ISIS_PSNP_HDR_LEN             (sizeof (tIsisComHdr) + 2 + \
                                       ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN) 
                                     /* Length of the PSNP Header,
                                      * consisting of 
                                      * Common header,
                                      * PDU Length (2), 
                                      * SourceID Length (SysID + PNODE ID)
                                      */
#define ISIS_CSNP_HDR_LEN             (ISIS_PSNP_HDR_LEN + \
                                       (2 * ISIS_LSPID_LEN))
                                     /* Length of the CSNP PDU Header,
                                      */
#define ISIS_MAX_HDR_LEN               ISIS_CSNP_HDR_LEN

#define ISIS_MD5_DIGEST_LEN            16

#define ISIS_CIRC_ID_LEN               0x01
                                     /* Circuit ID Length 
                                      */
#define ISIS_DEF_MAX_AALEN             0x03
                                     /* The Default Maximum Area Addresses that
                                      * the system must support
                                      */
#define ISIS_DEF_SYS_ID_LEN            0x06
                                     /* The Default System ID Length
                                      */
#define ISIS_IFUP                      0x01
#define ISIS_IFDOWN                    0x02

#define ISIS_DECN_WAIT                 0x01
                                      /* This is the global status tells 
                                       * that the decision is pending 
                                       * used to set the global flag gu1RelFlag
                                       */
#define ISIS_DECN_COMP                 0x02
                                      /* This is the global status tells 
                                       * that the decision is completed 
                                       * used to set the global flag gu1RelFlag
                                       */

#define ISIS_DECN_RELQ                 0x03
                                      /* Used to set the local flag in Decison
                                       * process
                                       */
#define ISIS_SPT_DONE                 0x04
/* Types of Fault Tolerance Updates
 */

#define ISIS_LS_UPDATE                 0x01
#define ISIS_BULK_UPDATE               0x02

/* Commands for adding, modifying and deleting TLVs and also as the type of
 * action required during Fault Tolerance updates.
 */

#define ISIS_CMD_ADD                   0x01
#define ISIS_CMD_MODIFY                0x02
#define ISIS_CMD_DELETE                0x03

/* Data Formats used in LSUs and Bulk Updates
 */

#define ISIS_FMT_FULL                  0x00 /* Indicates that a complete
                                             * entry is being backed up.
                                             */ 
#define ISIS_FMT_PARTIAL               0x01 /* Indictaes that only the
                                             * changes are being backed up
                                             * incrementally.
                                             */ 

#define ISIS_SOD                       0x01 /* Flag used for indicating
                                             * Start of Data while updating
                                             * databases during Fault 
                                             * Tolerance Updates
                                             */
#define ISIS_EOD                       0x02 /* Flag indicating End of Data
                                             * during FT updates. If the
                                             * flag is set, the update will
                                             * continue with the next
                                             * Database
                                             */

/* Fault Tolerance Support Status
 */

#define ISIS_FT_SUPP_ENABLE            0x01
                                     /* Fault Tolerance support is enabled.
                                      * The protocol will start sending Lock
                                      * Step Updates to the standby node 
                                      */ 

#define ISIS_FT_SUPP_DISABLE           0x02
                                     /* Fault Tolerance support is disabled.
                                      * The protocol will not send any updates
                                      * to the standby node even if the database
                                      * gets updated
                                      */ 

/* Fault Tolerance State
 */

#define ISIS_FT_INIT                    0x01
                                     /* The protocol is not operational even
                                      * though it is ready to become
                                      * operational. GO_ACTIVE command from FT
                                      * will trigger the protocol to move to
                                      * operational state
                                      */ 
#define ISIS_FT_STANDBY                0x02
                                     /* The node is a standby node and is
                                      * required to process the Lock Step
                                      * Updates and not respond to any other
                                      * events
                                      */ 
#define ISIS_FT_ACTIVE                 0x03
                                     /* The node is the active node and is
                                      * required to send Lock Step updates as
                                      * and when any of the databases changes
                                      */

/*Bulk Update Status*/
#define ISIS_BULK_UPDT_NOT_STARTED        1
#define ISIS_BULK_UPDT_IN_PROGRESS        2
#define ISIS_BULK_UPDT_COMPLETED          3
#define ISIS_BULK_UPDT_ABORTED            4

/* Peer Node Message Field Offsets
 */

#define ISIS_MSG_LENGTH_POS            0x03
#define ISIS_PEERHDR_LENGTH_POS        0x01
                                     /* Position of this entry in Fault 
                                      * Tolerance message is at 1 byte from the
                                      * beginning of the message*/
#define ISIS_FT_ENTRYCOUNT_POS         0x08
                                     /* Position of this entry in Fault 
                                      * Tolerance message is at Peer Node 
                                      * Message Header + 5 bytes
                                      */ 
#define ISIS_FT_LENGTH_POS             0x05 
                                     /* Position of this entry in Fault 
                                      * Tolerance message is at Peer Node 
                                      * Message Header + 3 bytes
                                      */ 

/* The following define the size of memory needed to allocate bufer while
 * sending the Bulk Update and LockStep Updates for each of the Databases
 */

#define ISIS_INST_IDX_LEN    4

#define ISIS_CKTS_IDX_LEN    4

#define ISIS_CKTS_SIDX_LEN    4

#define ISIS_CKTL_IDX_LEN    1

#define ISIS_ADJN_IDX_LEN    4

#define ISIS_IPRA_IDX_LEN    3

#define ISIS_SAAT_IDX_LEN    2
#define ISIS_GR_OFFSET    2
#define ISIS_LSPID_OFFSET    8

                                     
/* Protocol Types 
 */
/* Metric Positions in the PDU. These values are used for constructing the IS
 * Neighbour TLVs. Also defines the various Metric types used by the ISIS.
 */

#define ISIS_DEF_MET                   0x00
                                     /* Indicates the position of Default Metric
                                      * in the IS Neighbour TLV included in
                                      * LSPS. Also used for identifying the
                                      * Default Metric
                                      */ 
#define ISIS_DEL_MET                   0x01
                                     /* Indicates the position of Delay Metric
                                      * in the IS Neighbour TLV included in 
                                      * LSPS. Also used for identifying the
                                      * Delay Metric
                                      */ 
#define ISIS_EXP_MET                   0x02
                                     /* Indicates the position of Expense Metric
                                      * in the IS Neighbour TLV included in 
                                      * LSPS. Also used for identifying the
                                      * Expense Metric
                                      */ 
#define ISIS_ERR_MET                   0x03
                                     /* Indicates the position of Error Metric
                                      * in the IS Neighbour TLV included in 
                                      * LSPS. Also used for identifying the
                                      * Error Metric
                                      */ 
#define ISIS_MET_INVALID               0x04
                                     /* Indicates an invalid metric type
                                      */ 
#define ISIS_STYLE_NARROW_METRIC       0x01
                                     /* Indicates that narrow Metric is used
                                      * in the LSPs
                                      */
#define ISIS_STYLE_WIDE_METRIC         0x02
                                     /* Indicates that wide Metric is used
                                      * in the LSPs
                                      */
#define ISIS_STYLE_BOTH_METRIC         0x03
                                     /* Indicates that both narrow and wide 
                                      * Metrics are used in the LSPs
                                      */

                                       

/* Metrics Flags - The Supported Metrics is indicated by a bit mask in the
 * System Context. Each bit indicates whether a particular metric is supported.
 */


#define ISIS_DEF_MET_SUPP_FLAG         0x01
                                     /* Used for setting the support for default
                                      * Metric
                                      */ 
#define ISIS_DEL_MET_SUPP_FLAG         0x02
                                      /* Used for extracting the Delay Metric
                                       * support status. If set the Metric is
                                       * supported.
                                       */ 
#define ISIS_ERR_MET_SUPP_FLAG         0x04
                                      /* Used for extracting the Error Metric
                                       * support status. If set the Metric is
                                       * supported.
                                       */ 
#define ISIS_EXP_MET_SUPP_FLAG         0x08
                                      /* Used for extracting the Expense Metric
                                       * support status. If set the Metric is
                                       * supported.
                                       */ 
/* Adjacency States
 */ 

#define ISIS_DOWN                      0x00
                                     /* Adjacency no more exists
                                      */ 
#define ISIS_UP                        0x01
                                     /* Adjacency is in Active state and
                                      * all protocol operations are being 
                                      * carried over the adjacency
                                      */ 
/* Adjacency Usage 
 */

#define ISIS_L1ADJ                     ISIS_LEVEL1 
                                     /* Established adjacency with the neighbour
                                      * is of Level1 type
                                      */ 
#define ISIS_L2ADJ                     ISIS_LEVEL2
                                     /* Established adjacency with the neighbour
                                      * is of Level2 type
                                      */ 
#define ISIS_L12ADJ                    ISIS_LEVEL12
                                     /* Established adjacency with the neighbour
                                      * is of both Level1 and Level2 type
                                      */ 
    
/* Events or Exceptions logged during protocol operation. Each Event will have
 * associated data which gives details about these events.
 */

#define ISIS_EVT_MEM_RES_FAIL          0x01
#define ISIS_EVT_SYS_FAIL              0x02

#define ISIS_EVT_REG_FAIL              0x0B
#define ISIS_EVT_DEREG_FAIL            0x0C
#define ISIS_EVT_TX_FAIL               0x0D
#define ISIS_EVT_RX_FAIL               0x0E
#define ISIS_EVT_FTM_DCOD_FAIL         0x0F
#define ISIS_EVT_FTM_ABORT             0x10
#define ISIS_EVT_RTM_UPDT_FAIL         0x11
#define ISIS_EVT_MIBII_LOOK_UP_FAIL    0x12
#define ISIS_EVT_DLL_CONN_FAIL         0x13
#define ISIS_EVT_DLL_IOCTL_FAIL        0x14
#define ISIS_EVT_INV_IFIDX             0x15

#define ISIS_EVT_INV_PDU               0x1F
#define ISIS_EVT_DUP_SYS_DET           0x20
#define ISIS_EVT_ID_LEN_MISMATCH       0x21
#define ISIS_EVT_AUTH_FAIL             0x22
#define ISIS_EVT_IS_UP                 0x23
#define ISIS_EVT_WRONG_SYS_TYPE        0x24
#define ISIS_EVT_INCOMP_CKT            0x25
#define ISIS_EVT_AA_MISMATCH           0x26
#define ISIS_EVT_MAN_ADDR_CHANGE       0x27
#define ISIS_EVT_MAX_AA_MISMATCH       0x28
#define ISIS_EVT_PROT_SUPP_CHANGE      0x29
#define ISIS_EVT_IP_IF_ADDR_CHANGE     0x2A
#define ISIS_EVT_IPRA_CHANGE           0x2B
#define ISIS_EVT_SUMM_ADDR_CHANGE      0x2C
#define ISIS_EVT_IS_DOWN               0x2D
#define ISIS_EVT_IS_DESTROY            0x2E
#define ISIS_EVT_MAN_ADDR_DROPPED      0x2F
#define ISIS_EVT_DISTANCE_CHANGE       0X30

#define ISIS_EVT_CKT_CHANGE            0x33
#define ISIS_EVT_ADJ_CHANGE            0x34
#define ISIS_EVT_REJECTED_ADJ          0x35
#define ISIS_EVT_DIS_NOT_ELECTED       0x36
#define ISIS_EVT_DIS_CHANGE            0x37
#define ISIS_EVT_CHG_IN_DIS_STAT       0x38
#define ISIS_EVT_PRIORITY_CHANGE       0x39
#define ISIS_EVT_ELECT_DIS             0x3A

#define ISIS_EVT_RESTARTER             0x3B
#define ISIS_EVT_HELPER                0x3C


#define ISIS_EVT_CORR_LSP_DET          0x41
#define ISIS_EVT_INV_LSP               0x42
#define ISIS_EVT_LSP_DBOL              0x43
#define ISIS_EVT_LSP_DBOL_RECOV        0x44
#define ISIS_EVT_OWN_LSP_PURGE         0x45
#define ISIS_EVT_LSP_DB_VALIDATED      0x46
#define ISIS_EVT_MAX_SEQNO_EXCEED      0x47
#define ISIS_EVT_SEQ_NO_SKIP           0x48
#define ISIS_EVT_TX_Q_FULL             0x49

#define ISIS_EVT_NEAR_L2_NOT_AVAIL     0x51
#define ISIS_EVT_L1_DECN_COMPLETE      0x52
#define ISIS_EVT_L2_DECN_COMPLETE      0x53
#define ISIS_EVT_ATT_STATUS            0x54
#define ISIS_EVT_SHUTDOWN              0x55
#define ISIS_EVT_RESETALL              0x56
#define ISIS_EVT_ATT_STAT_CHG          0x57

#define ISIS_EVT_VER_MISMATCH          0x58 
#define ISIS_EVT_LSP_TOO_LARGE         0x59 
#define ISIS_EVT_PROT_SUPP_MISMATCH    0x5A
#define ISIS_EVT_LSP_BUFSIZE_MISMATCH  0x5B
#define ISIS_EVT_DECN_RELQ             0x5C
#define ISIS_EVT_VCM                   0x5d

#ifdef ISIS_FT_ENABLED

#define ISIS_EVT_LSU_FAIL              0x70

#endif
#define ISIS_EVT_IMPORT_LEVEL_CHG      0x80
#define ISIS_EVT_RRD_PROTO_DISABLE     0x81
#define ISIS_EVT_RRD_METRIC_CHG        0x82
#define ISIS_EVT_P2P_CKT_CHG     0x83 
#define ISIS_EVT_HOST_NME_CHANGE       0x84
#define ISIS_EVT_HOST_NME_SUPPORT      0x85

/* The following codes form part of the data associated with aforementioned
 * Events.
 */

/* System Failure
 */ 

#define ISIS_TASK_FAIL                 0x01
#define ISIS_SEM_FAIL                  0x02
#define ISIS_QUE_FAIL                  0x03
#define ISIS_TIM_FAIL                  0x04

/* Registration Failure
 */

#define ISIS_RTM_REG_FAIL              0x01
#define ISIS_FTM_REG_FAIL              0x02
#define ISIS_DLL_REG_FAIL              0x04

/* DeRegistration Failure
 */

#define ISIS_RTM_DEREG_FAIL            0x01
#define ISIS_FTM_DEREG_FAIL            0x02
#define ISIS_DLL_DEREG_FAIL            0x04

/* Transmission Failure
 */

#define ISIS_LSU_TX_FAIL               0x01
#define ISIS_BU_TX_FAIL                0x02
#define ISIS_BPCM_TX_FAIL              0x03
#define ISIS_DLL_TX_FAIL               0x04

/* Reception Failure
 */

#define ISIS_BPCM_RX_FAIL              0x01
#define ISIS_DLL_RX_FAIL               0x02

/* Circuit Change
 */

#define ISIS_CKT_DOWN                  0x00
#define ISIS_CKT_UP                    0x01
#define ISIS_CKT_DESTROY               0x02
#define ISIS_CKT_MODIFY                0x03
#define ISIS_CKT_IP_IF_ADD             0x04
#define ISIS_CKT_IP_IF_DEL             0x05
#define ISIS_CKT_NO_EVT                0x06
#define ISIS_CKT_RESTART               0x07

/* Node Status Indication
 */

#define ISIS_NODE_STAT_DOWN            0x00
#define ISIS_NODE_STAT_UP              0x01

/* Adjacency Change
 */

#define ISIS_ADJ_INIT                  0x01 
#define ISIS_ADJ_UP                    0x00 
#define ISIS_ADJ_FAIL                  0x03 
#define ISIS_ADJ_DOWN                  0x02

/*Handshake mechanism*/
#define ISIS_P2P_TWO_WAY      0x02
#define ISIS_P2P_THREE_WAY      0x03

/*Versions of 240 TLV*/
#define ISIS_P2P_THREE_WAY_V1     0x01
#define ISIS_P2P_THREE_WAY_V2     0x02

#define ISIS_ADJ_STATE_NONE      0x00
#define ISIS_ADJ_STATE_UP      0x01
#define ISIS_ADJ_STATE_DOWN      0x02
#define ISIS_ADJ_STATE_ACCEPT     0x03
#define ISIS_ADJ_STATE_REJECT     0x04

/* IPRA Change
 */

#define ISIS_IPRA_DOWN                 0x00
#define ISIS_IPRA_UP                   0x01 
#define ISIS_IPRA_CHG                  0x02

/* IPRA Types
 */

#define ISIS_MANUAL_TYPE               0x01
#define ISIS_AUTO_TYPE                 0x02
/* DIS Status Change
 */

#define ISIS_DIS_RESIGNED              0x00
#define ISIS_DIS_ELECTED               0x01


/* Manual Area Address Change
 */

#define ISIS_MAA_ADDED                 0x00
#define ISIS_MAA_DROPPED               0x01

/* Protocol Support Change
 */

#define ISIS_PS_ADDED                  0x00
#define ISIS_PS_DELETED                0x01

/* IP Interface Address Change
 */

#define ISIS_IP_IF_ADDR_ADD            0x00
#define ISIS_IP_IF_ADDR_DEL            0x01

/* Summary Address Change
 */

#define ISIS_SUMM_ADDR_ADD             0x00
#define ISIS_SUMM_ADDR_DEL             0x01
#define ISIS_SUMM_ADDR_ADMIN_CHG       0x02
#define ISIS_SUMM_ADDR_METRIC_CHG      0x03

/* Authentication Failure
 */

#define ISIS_PASSWD_MISMTCH            0x01
#define ISIS_AUTH_MISSING              0x02
#define ISIS_AUTH_NOT_SUPP             0x03 


/* DIS Not Elected
 */

#define ISIS_DUP_SNPA                  0x04

/* Invalid PDU
 */

#define ISIS_CHKSUM_FAIL               0x05

/* Rejected Adjacency
 */

#define ISIS_PRUNE                     0x01
#define ISIS_BUF_LEN_MISMTCH           0x02

/* FTM Decode Failure
 */

#define ISIS_LSU_DECOD_FAIL            0x01
#define ISIS_BU_DECOD_FAIL             0x02
#define ISIS_OPCODE_DECOD_FAIL         0x03

/* FTM Aborted
 */

#define ISIS_LSU_ABORT                 0x01
#define ISIS_BU_ABORT                  0x02

/* RTM Update Failure
 */

#define ISIS_RTM_ADD                   0x01
#define ISIS_RTM_DEL                   0x02
#define ISIS_RTM_MOD                   0x03

/* DLL Conection Failure
 */

#define ISIS_DLL_OPEN                  0x01
#define ISIS_DLL_CLOSE                 0x02

/* DLL I/O Control Failure
 */

#define ISIS_IOCTL_ADD_ADDR            0x01
#define ISIS_IOCTL_DEL_ADDR            0x02

/* Log Level Masks
 */
                                   /* bits --> 8765 4321 */ 

#define ISIS_PT_MASK                   0x01 /* 0000 0001 */
#define ISIS_PD_MASK                   0x02 /* 0000 0010 */
#define ISIS_TD_MASK                   0x04 /* 0000 0100 */
#define ISIS_PI_MASK                   0x08 /* 0001 0000 */
#define ISIS_EE_MASK                   0x10 /* 0000 1000 */
#define ISIS_ALL_MASK                  0x1f /* 0001 1111 */

/* Debug string types
 */

#define ISIS_OCTET_STRING              0x01 
                                     /* Displays values in HEX
                                      */
#define ISIS_CHAR_STRING               0x02
                                     /* Displays characters
                                      */ 
#define ISIS_DISPLAY_STRING            0x03
                                     /* Displays a String
                                      */ 

#define ISIS_LOG_ENABLE                0x01
                                     /* For Compile time enabling of LOG
                                      * Messages
                                      */ 
#define ISIS_LOG_DISABLE               0x02
                                     /* For Compile time disabling of LOG
                                      * Messages
                                      */ 
/* Circuits types
 */

#define ISIS_BC_CKT                    0x01
                                     /* Broadcast Circuit */
#define ISIS_P2P_CKT                   0x02
                                     /* Point to Point */
#define ISIS_STATIC_IN                 0x03
                                     /* Static IN - only incoming frames 
                                      * allowed
                                      */
#define ISIS_STATIC_OUT                0x04
                                     /* Static OUT - only outgoing frames 
                                      * allowed
                                      */
#define ISIS_DYNAMICALLY_ASSIGNED      0x05
                                     /* Circuit has to be established
                                      * dynamically on demand.
                                      */

/* Quoram flags: These flags are defined for each of the database entries. These
 * flags indicate which of the mandatory parameters are set during Row Creation.
 * If one or more of the mandatory parameters are not set then Row Status cannot
 * be made ACTIVE.*/

/* System Table   
 */

#define ISIS_SYSTYPE_FLAG              0x01  
                                     /* Value with which the SysQFlag is to be 
                                      * OR'ed with to indicate that sysType is 
                                      * set in the table 
                                      */
#define ISIS_SYSID_FLAG                0x02   
                                     /* Value with which the SysQFlag is to be 
                                      * OR'ed with to indicate that sysType is 
                                      * set in the table 
                                      */
#define ISIS_SYSMAA_FLAG               0x04
                                     /* Value with which the SysQFlag is to be 
                                      * OR'ed with to indicate that atleast one
                                      * MAA is set in the table
                                      */
#define ISIS_SYSALLSET_FLAG            0x07   
                                     /* Value which indicates that all the
                                      * mandatory parameters are set
                                      */ 
/* Circuit Table
 */

#define ISIS_CIRCIFINDEX_FLAG          0x01
                                     /* Value with which the CircQFlag is to be 
                                      * OR'ed with to indicate that CircIfIndex
                                      * is set in the table 
                                      */
#define ISIS_CIRCIFSUBINDEX_FLAG       0x02
                                     /* Value with which the CircQFlag is to be 
                                      * OR'ed with to indicate that 
                                      * CircIfSubIndex is set in the table 
                                      */
#define ISIS_CIRCLOCALID_FLAG          0x04
                                     /* Value with which the CircQFlag is to be 
                                      * OR'ed with to indicate that 
                                      * Local ID is set in the table 
                                      */
#define ISIS_CIRCTYPE_FLAG             0x08
                                     /* Value with which the CircQFlag is to be 
                                      * OR'ed with to indicate that 
                                      * Circuit Type is set in the table 
                                      */
#define ISIS_CIRCALLSET_FLAG           0x0F
                                     /* Value with which the CircQFlag is to be 
                                      * OR'ed with to indicate that 
                                      * all the mandatory parameters are set 
                                      */ 

/* IPRA Table        
 */ 

#define ISIS_IPRADESTTYPE_FLAG         0x01
                                     /* Value with which the IPRAQflag is to 
                                      * be OR'ed with to indicate the 
                                      * IPRADest Type is set in the table
                                      */ 
#define ISIS_IPRADEST_FLAG             0x02
                                     /* Value with which the IPRAQflag is to
                                      * be OR'ed with to indicate thatthe
                                      * IPRA Dest is set in the table
                                      */
#define ISIS_IPRAPREFIXLEN_FLAG        0x04
                                     /* Value with which the IPRAflag is to be
                                      * OR'ed with to indicte that the IPRA
                                      * Prefix Length is set in the Table
                                      */

#define ISIS_IPRANEXTHOP_FLAG          0x08
                                     /* Value with which the IPRAQflag is to
                                      * be OR'ed with to indicate that the
                                      * IPRA Dest is set in the table
                                      */

#define  ISIS_IPRAALLSET_FLAG          0x0f
                                     /* Value with which the IPRAQflag is to
                                      * be OR'ed with to indicate that all the
                                      * mandatory parameters are set in the
                                      * table
                                      */

/* TLV Lengths
 */
#define ISIS_TLV_LEN                  30

#define ISIS_MAX_TLV_LEN               0xFF
                                     /* Maximum length of any TLV included in
                                      * the ISIS PDUs
                                      */ 
#define ISIS_PROT_SUPP_TLV_LEN         0x01
                                     /* NLPID Length - as per RFC 1195 
                                      */
#define ISIS_IPRA_TLV_LEN              ((2 * ISIS_IPV4_ADDR_LEN) + 4)
                                     /* Includes IP Address, IP Address Mask and
                                      * 4 one byte Metric fields
                                      */ 

#define ISIS_IP6RA_TLV_LEN           22
                                     /* Includes IP Address, IP Address Mask and
                                      * 4 one byte Metric fields
                                      */ 

#define ISIS_IS_ADJ_TLV_LEN            (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN + 4)
                                     /* IS Adjacency TLV includes System ID,
                                      * Pseudonode Id,  4 one byte metric
                                      * fields .
                                      */ 
#define ISIS_IS_NEIGH_TLV_LEN          ISIS_LAN_ADDR_LEN
                                     /* Ethernet Address Length - Upcoming
                                      * drafts may include one more TLV for
                                      * holding SNPA Addresses which are of
                                      * variable length. Current Specs has scope
                                      * only for 6 bytes LAN Address.
                                      */ 
#define ISIS_AREA_ADDR_TLV_LEN         (ISIS_AREA_ADDR_LEN + 1)
                                     /* Includes an Area Address and 1 byte
                                      * indicating the length of the address
                                      */ 
#define ISIS_IPV4_ADDR_TLV_LEN         ISIS_MAX_IP_ADDR_LEN
                                     /* Includes only the IP address
                                      */ 
#define ISIS_LSP_ENTRY_TLV_LEN         (ISIS_LSPID_LEN + 8)
                                     /* Includes System ID, Pseudonode ID, LSP
                                      * Number, 4 Byte Sequence Number, 2 Byte 
                                      * Checksum and 2 byte Remaining Life Time
                                      */ 
#define ISIS_LSP_BUF_SIZE_TLV_LEN      2
/*MISIS*/
#define ISIS_COM_EXTN_IP_REACH_TLV_LEN     (ISIS_EXT_DEF_METRIC_LEN + 1) 
#define ISIS_COM_MT_IPV6_REACH_TLV_LEN     (ISIS_EXT_DEF_METRIC_LEN + 1 + 1) /* excluding mt id length*/

/* The following definitions are used for differentiating between OSI system IDs
 * and IP Addresses. FutureISIS implementation maintains a separate path for OSI
 * System IDs and IP Addresses. Current implementation supports IP only
 * environments and hence IP Path is constructed separately from OSI paths.
 */ 

#define ISIS_IP_END_SYSTEM             0x01
                                      /* The retrieved SPT node refers to an IP
                                       * reachable address.
                                       */ 
#define ISIS_OSI_SYSTEM                0x02
                                      /* The retrieved SPT node refers to an OSI
                                       * Adjacent System
                                       */ 
/* Path definitions
 */

#define ISIS_OSI_PATH                  0x01
                                      /* Refers to OSI Path
                                       */ 
#define ISIS_CURR_IP_PATH              0x02
                                      /* Refers to IP Path
                                       */ 
#define ISIS_PREV_IP_PATH              0x03
#define ISIS_TENT                      0x04

/* General Definitions
 */

/* NOTE: Make sure that ISIS_MAX_HASH_MBRS * ISIS_MAX_BUCKETS is equal to or
 * greater than ISIS_MAX_LSPS. Each instance will then have hash tables with 
 * varying sizes, but normalization procedure uses these constants for 
 * normalising the Hash tables.
 */

#define ISIS_MAX_HASH_MBRS             100
                                      /* Maximum number of Elements per bucket 
                                       * in sorted hash table
                                       */ 
#define ISIS_MAX_SH_BKTS               (ISIS_MAX_LSPS/ISIS_MAX_HASH_MBRS)
                                      /* The maximum number of buckets in the
                                       * Sorted Hash Table
                                       */
#define ISIS_MAX_BUCKETS               16
                                      /* The maximum number of buckets in the
                                       * Hash Table
                                       */ 
#define ISIS_MAX_SEQNO                 0xFFFFFFFF
                                      /* Maximum Sequence number. If Sequence
                                       * Number exceeds this value, then the IS
                                       * shall halt for MAX_AGE + ZERO_AGE time
                                       * to ensure purging of old entries from
                                       * the Network
                                       */ 
#define ISIS_NOT_SET                   0x00
                                      /* Flag used for indicating that the Entry
                                       * was not added previously to the Buffer.
                                       * Currently used for Summary Address
                                       * Entries.
                                       * This is also used for indicating the
                                       * attached status of the system
                                       */ 
#define ISIS_SET                       0x01
                                      /* Flag used for indicating that the Entry
                                       * not added previously to the Buffer.
                                       * Currently used for Summary Address
                                       * Entries.
                                       */ 

#define ISIS_NOT_MODIFIED              0x00
                                      /* Flag indicating that the Dirty Flag is
                                       * not modified since the last time
                                       * SelfLSP buffer was transmitted
                                       */ 
#define ISIS_MODIFIED                  0xFF
                                      /* Flag indicating that the Dirty Flag is
                                       * modified since the last time
                                       * SelfLSP buffer was transmitted
                                       */ 
#define ISIS_SHT_SIZE                  0xff
                                      /* Number of buckets in the Sorted Hash
                                       * Table
                                       */

#define ISIS_CORRECT_CS                0x0
                                      /* The Value returned after validation
                                       * of Checksum
                                       */ 

/* System Status Values
 */

#define ISIS_IS_UP                     0x01
                                      /* The state of IS System when it is
                                       * operationally up
                                       */ 
#define ISIS_SHUTDOWN                  0x02
                                      /* The command to shutdown all the
                                       * instances, this command will retain the
                                       * memory allocated globally, will not
                                       * return to the system
                                       */ 
#define ISIS_RESET_ALL                 0x03
                                      /* The command to reset all the instances
                                       * and Releases all the memory held by
                                       * pools to the system
                                       */ 

/* Module Identifiers
 */

#define ISIS_MOD_CTRL                  0x01
                                      /* Module Id for the Control Module which
                                       * is used internally
                                       */ 
#define ISIS_MOD_FORW                  0x02
                                      /* Module Id for the Forwarding Module 
                                       * which is used internally
                                       */ 

/* Boolean comparison codes
 */

#define ISIS_LSP_GREATER               0x01 
                                      /* Indicating one LSP is greater
                                       * than the other
                                       */ 
#define ISIS_LSP_EQUAL                 0x00
                                      /* Indicating both LSPs are equal
                                       */ 
#define ISIS_LSP_NOT_EQUAL             0x02
                                      /* Indicating both LSPs are not equal
                                       */ 
/* LSP Validation Codes
 */

#define ISIS_SELF_LSP                  0x01
                                      /* Indicates that a received LSP is an LSP
                                       * that was originated by the local system
                                       */ 
#define ISIS_ZERO_RLT_LSP              0x02
                                      /* Indicates that a received LSP has Zero
                                       * Remaining Life Time
                                       */
#define ISIS_LSP_ACQUIRE               0x03
                                      /* Indicates a request for LSP
                                       * while building in PSNP
                                       */

#define ISIS_MSG_PRIORITY              0x00 
                                      /* Isis Messages priority to send
                                       * messages to the Queue
                                       * PRIORITY_NORMAL is defined in FSAP2
                                       */

#define ISIS_SAME_AREA                 0x01             
                                      /* Update module indicates using this flag
                                       * that the system whose adjancencies
                                       * are being processed belongs to the same
                                       * area as the processing system
                                       */
#define ISIS_OTHER_AREA                0x02
                                      /* Update module indicates using this flag
                                       * that the system whose adjancencies
                                       * are being processed belongs to a
                                       * different area than the processing 
                                       * system
                                       */

#define ISIS_CTRL_NODE_TYPE_MASK      0x01
                                       /* The Mask used to set the Node type to
                                        * Control Node
                                        */
#define ISIS_DATA_NODE_TYPE_MASK      0x02                                 
                                       /* The Mask used to set the Node type to
                                        * Data Node
                                        */

#define ISIS_CTRL_NODE                0x01
                                       /* The Value of Node type indicating 
                                        * Control Plane
                                        */ 
#define ISIS_DATA_NODE                0x02
                                       /* The Value of Node type indicating 
                                        * data Plane
                                        */ 
#define ISIS_CTRL_DATA_NODE           0x03
                                       /* The Value of Node type indicating 
                                        * both control and data Plane
                                        */ 


/* Flags specifying whether the module is allowed to release the Buffer after
 * transmission
 */ 

#define ISIS_FREE_BUF                  0x01
#define ISIS_HOLD_BUF                  0x02 
                                         
#define ISIS_MIB_PROT_ID               0x09
#define ISIS_IDRP_DISC                 0x83
#define ISIS_PROT_ID_EXT               0x01
#define ISIS_VERSION                   0x01

/* Flag specifying whether the FLTR Module is allowed ot release the Buffer
 * after processing FT Message. When the Primary Node issues SHUTDOWN / RESTART
 * LSU, the standby node after processing the LSU, releases all the memory to
 * system, Hence the buffer should not be freed.
 */

#define ISIS_NO_FREE                   0x02

#define ISIS_BDY_1500_BLK              0x00
#define ISIS_BDY_64_BLK                0x01

/* Constants for Miscellaneous memory initializations
 */

#define ISIS_MAX_ECT_TYPES             0x05   /* The Maximum Types of EC Timers
                                               */
#define ISIS_MAX_ECTI_TYPES            0x03   /* The Maximum Type of EC Timers
                                               * using Mask Information
                                               */
#define ISIS_MAX_NUM_EVTS_IN_Q         0x0A   /* The Maximum number of events
                                               * that will be held in Queue
                                               * at any point of time
                                               */
#define ISIS_MAX_EVT_SIZE              0x32   /* The Maximum size of any event
                                               * NOTE : if a new event is added
                                               *        and exceeds the size 
                                               *        defined here, then this
                                               *        has to be changed.
                                               */
#define ISIS_NUM_HASH_TBLS             0x06   /* The Number of Hash Tables
                                               * required per Instance
                                               */ 
#define ISIS_NUM_SHASH_TBLS            0x04   /* The Number of sorted Hash
                                               * tables required per instance
                                               */

#define ISIS_MAX_REG_MSG_LEN           0x32   /* The Maximum Registration
                                               * Message Length required
                                               */ 
/* Constants for Checksum Validations
 */ 

#define  ISIS_ITERMASK (ISIS_MAXITER - 1)  
                                /* mask to tell if we need mod or not */
#define  ISIS_INLINEMASK  (ISIS_INLINE - 1)    /* mask for uneven bits */
#define  ISIS_INSHIFT     3     /* i.e. inlined by 1<<INSHIFT */
#define  ISIS_ITERSHIFT   12    /* log2 of number of interations */
#define  ISIS_MAXINLINE   (ISIS_MAXITER/ISIS_INLINE)
                                /* number of inline iterations */
#define  ISIS_MODULUS     255   /* modulus for checksum */
#define  ISIS_MAXITER     4096
#define  ISIS_INLINE      8

/*MI Related Macros*/

#define ISIS_INVALID_CXT_ID        0xFFFFFFFF
#define ISIS_DEFAULT_CXT_ID        0
#define ISIS_INVALID_IFINDEX       -1

#define  ISIS_RTM_IF_GET_MSG_HDR_PTR(pMsg)   \
                              IP_GET_MODULE_DATA_PTR(pMsg)



/*PDU Fragments Size for trap mib-08.txt
 */
#define ISIS_TRAP_PDU_FRAG_SIZE    64
/*TRAP-IDS */
#define ISIS_TRAP_DBOL                   1
#define ISIS_TRAP_MAN_ADDR_DROPS         2
#define ISIS_TRAP_CORR_LSP_DETECT        3
#define ISIS_TRAP_EXCEED_MAX_SEQ         4
#define ISIS_TRAP_ID_LEN_MISMATCH        5
#define ISIS_TRAP_MAX_AA_MISMATCH        6
#define ISIS_TRAP_OWN_LSP_PURGE          7
#define ISIS_TRAP_SEQ_NO_SKIP            8
#define ISIS_TRAP_AUTH_TYPE_FAIL         9
#define ISIS_TRAP_AUTH_FAIL              10
#define ISIS_TRAP_VERSION_SKEW           11
#define ISIS_TRAP_AREA_MISMATCH          12
#define ISIS_TRAP_ADJ_REJECTED           13
#define ISIS_TRAP_LSP_LARGE_TO_PURGE     14
#define ISIS_TRAP_ORIG_BUFSIZE_MISMATCH  15
#define ISIS_TRAP_PROT_SUPP_MISMATCH     16 
#define ISIS_TRAP_GR_RESTARTER           17
#define ISIS_TRAP_GR_HELPER              18



/*Restart Support*/
#define ISIS_GR_RESTART_NONE    1       
#define ISIS_GR_RESTART_PLANNED         2
#define ISIS_GR_RESTART_BOTH    3


/*Restart default timer intervals*/
#define ISIS_GR_DEF_T1_INTERVAL 3
#define ISIS_GR_DEF_T2_INTERVAL  60
#define ISIS_GR_DEF_T3_INTERVAL  120    


#define ISIS_GR_DEF_ACK_RETRY_COUNT     3
/*Restart State*/
#define ISIS_GR_SHUTDOWN        2
#define ISIS_GR_RESTART          3      

/*ShutDown State*/

#define ISIS_GR_SHUT       1 
#define ISIS_GR_NO_SHUT    2

/*Gr mode*/
#define ISIS_GR_NONE         1
#define ISIS_GR_RESTARTER    2  
#define ISIS_GR_HELPER       3
#define ISIS_GR_HELPER_DOWN  4


#define ISIS_GR_DEF_HELPER_TIME         0
#define ISIS_GR_MIN_HELPER_TIME         180
#define ISIS_GR_MAX_HELPER_TIME         65535


#define ISIS_GR_MIN_T3_INTERVAL      1
#define ISIS_GR_MAX_T3_INTERVAL      65535

#define ISIS_GR_MIN_T2_INTERVAL      1
#define ISIS_GR_MAX_T2_INTERVAL      32767

#define ISIS_GR_MIN_T1_INTERVAL      1
#define ISIS_GR_MAX_T1_INTERVAL      180

#define ISIS_GR_MIN_RETRY_COUNT      1
#define ISIS_GR_MAX_RETRY_COUNT      200



#define ISIS_GR_RESTART_ACK_RCVD        1
#define ISIS_GR_ RESTART_NO_ACK_RCVD    2

/*Helper Support*/
#define ISIS_GR_HELPER_NONE             1
#define ISIS_GR_HELPER_RESTART   2
#define ISIS_GR_HELPER_BOTH         3

/*Helper reason*/
#define ISIS_GR_NOT_HELPING             1
#define ISIS_GR_HELPING                 2

/*Helper exit reason*/
#define ISIS_GR_HELPER_NONE             1
#define ISIS_GR_HELPER_COMPLETED        2
#define ISIS_GR_HELPER_TIMEDOUT  3
#define ISIS_GR_HELPER_TOP_CHG          4


/*ADj State*/
#define ISIS_DB_SYNC 1
#define ISIS_DB_NOT_SYNC 2


#define ISIS_GR_ECT_T1                  0x0E


#define ISIS_RESTART_TLV             211  /*As per RFC 5036*/


#define ISIS_RESTART_TLV_HDR   2
#define FLAG     1   /*Flag size*/
#define REM_TIME   2   /*Remaining time field length*/

#define OVERLOAD 1 /*Overload bit*/
#define CLEAR_OVERLOAD 2 /*Overload bit*/
#define SELF_LSP 3 /*Self Lsp generation*/



/*ISIS GR state*/
#define ISIS_HELPER_RA_SEND 0xa
#define ISIS_HELPER_NONE 0

enum{
ISIS_GR_ADJ_RESTART=1, /*Noticed when router comes with a restart reason*/
ISIS_GR_ADJ_SEEN_RA, /*Noticed once restart Ack from all the adjacent routers*/ 
ISIS_GR_ADJ_SEEN_CSNP,/*Noticed once CSNP from all the adjacent routers*/
ISIS_GR_SPF_WAIT/*Noticed when SPT complete is triggered*/
};

enum{
ISIS_GR_SPF_L1WAIT=5,/*Wait for LSP from L1 neighbours*/
ISIS_GR_SPF_L2WAIT,/*Wait for LSP from L2 neighbours*/
ISIS_GR_SPF_L1COMPLETE,/*L1 LSP Rcvd and SPT complete for L1 neighbours*/
ISIS_GR_SPF_L2COMPLETE,/*L1 LSP Rcvd and SPT complete for L2 neighbours*/
ISIS_GR_SPF_DONE,/*SPT Complete for all Levels L1 and L2*/
ISIS_GR_OVERLOAD,/*On T3 Expire sets overload bit and moves to this state*/
ISIS_GR_CLEAR_OVERLOAD,/*If OB is set - Clears overload bit on SPT complete*/
ISIS_GR_NORMAL_ROUTER/*Notice once it comes as normal router*/
};

/*ISIS GR Events*/
enum{
ISIS_GR_EVT_RESTART= 0x01,/*Restart event occurred*/
ISIS_GR_EVT_RX_RA,/*Rcvd Restart Ack on that circuit*/
ISIS_GR_EVT_RX_CSNP,/*Rcvd CSNP on that circuit*/
ISIS_GR_EVT_RX_IIH,/*RCvd Normal Hello*/
ISIS_GR_EVT_T1_EXP,/*Rcvd On T1 Expire*/
ISIS_GR_EVT_N_T1_EXP,/*Rcvd On T1 Nth Expire*/
ISIS_GR_EVT_T2_EXP,/*Rcvd T2 Expire*/
ISIS_GR_EVT_T3_EXP,/*Rcvd T3 Expire*/
ISIS_GR_EVT_LSP_DB_SYNC,/*LSP sync for all Levels on ISIS enabled circuits*/
ISIS_GR_EVT_ALL_SPF_DONE/*Completion is GR process*/
};
#define ISIS_GR_EVT_ADJ_UP 0x08

#define ISIS_GR_ACK_RCVD 0x01
#define ISIS_GR_CSNP_RCVD 0x02
#define ISIS_GR_ACK_CSNP_RCVD 0x03

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

#define  ISIS_OFFSET(x,y)    FSAP_OFFSETOF(x,y)


/* Macro used for RBTree Comparisions */
#define ISIS_RB_EQUAL             0
#define ISIS_RB_GREATER           1
#define ISIS_RB_LESS             -1

/*Flag Value in Restart TLV*/

#define RR_SA_RA_CLEAR   0
#define RR_SET           1
#define RA_SET           2
#define SA_SET           4
#define RR_SA_SET        5

/*Adjacent Neighbour Restart State*/

#define ISIS_ZERO 0

#define ISIS_ADJ_RUNNING  1
#define ISIS_ADJ_SUPPRESSED  2

#define ISIS_DUPLICATE_IPRA_TLV    1
#define ISIS_MOVE_IPRA_TLV         2
#define ISIS_DELETE_IPRA_TLV       3

#define ISIS_RRD_ENABLE    1
#define ISIS_RRD_DISABLE   2
#define ISIS_METRIC_INDEX_STATIC  0
#define ISIS_METRIC_INDEX_DIRECT  1
#define ISIS_METRIC_INDEX_OSPF    2
#define ISIS_METRIC_INDEX_RIP     3
#define ISIS_METRIC_INDEX_BGP     4
#define ISIS_MAX_METRIC_INDEX     4


#define ISIS_ALL_PROTO_MASK \
            (ISIS_IMPORT_DIRECT | ISIS_IMPORT_STATIC | \
             ISIS_IMPORT_RIP | ISIS_IMPORT_OSPF | ISIS_IMPORT_BGP )


#define  ISIS_RRD_DISABLE_MASK_VALUE(pContext) \
                    (ISIS_ALL_PROTO_MASK & \
                            (~(pContext->RRDInfo.u4RRDSrcProtoMask)))


/* Multi-topology macros */
#define ISIS_MAX_MT                     2

/* SPF calculation indices */
#define ISIS_ST_INDEX                   0 /* During Single topology - Index 0 is used for single topology */
#define ISIS_MT0_INDEX                  0 /* During Multi topology - Index 0 and 1 are used for MT 0 and MT2 */
#define ISIS_MT2_INDEX                  1

#define ISIS_IPV4_UNICAST_MT_ID         0
#define ISIS_IPV6_UNICAST_MT_ID         2


#define ISIS_MT_ID_LEN                  2
/* This length will be used in Ext-IP Reachability and MT-IPv6 Reachability*/ 
#define ISIS_EXT_DEF_METRIC_LEN         4
/* This length will be used in Ext-IS Reachability and MT-IS Reachability*/ 
#define ISIS_IS_DEF_METRIC_LEN          3
#define ISIS_EXT_IS_REACH_TLV_LEN      (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN + ISIS_IS_DEF_METRIC_LEN + 1)
#define ISIS_MT_IS_REACH_TLV_LEN       (ISIS_MT_ID_LEN + ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN \
                                         + ISIS_IS_DEF_METRIC_LEN + 1)
#define ISIS_EXT_IP_REACH_TLV_LEN      (ISIS_EXT_DEF_METRIC_LEN + 1 + ISIS_IPV4_ADDR_LEN)
#define ISIS_MT_IPV6_REACH_TLV_LEN     (ISIS_MT_ID_LEN + ISIS_EXT_DEF_METRIC_LEN + 1 + 1 + ISIS_IPV6_ADDR_LEN)

/* The bit map values corresponding to each MT */
#define ISIS_MT0_BITMAP                 0x01 /* 0th bit set for MT 0 (binary - 0001)*/
#define ISIS_MT2_BITMAP                 0x04 /* 2nd bit set for MT 2 (binary - 0100)*/


/* 3-way handshake field lengths*/
#define ISIS_P2P_THREE_WAY_HS_MIN_LEN     5
#define ISIS_P2P_THREE_WAY_HS_MAX_LEN     17
#define ISIS_P2P_THREE_WAY_STATE_LEN          1
#define ISIS_P2P_EXT_CKT_ID_LEN         4


/* Min & Max metric */
#define ISIS_MIN_METRIC_MT              1
#define ISIS_MAX_METRIC_MT              0xFE000000
#define ISIS_MAX_METRIC_ST              63

#define ISIS_CKT_LEVEL_REC              1
#define ISIS_IPRA_REC                   2
#define ISIS_SUMMARY_REC                3

/* BFD for M-ISIS macros */
#define ISIS_BFD_MT_NLPID_LEN           3
/* HOSTNAME macros*/
#define ISIS_HSTNME_LEN   50
#define ISIS_HSTNMEWITHID_LEN 20
#define ISIS_HSTNME_DISPLAY 14
#define ISIS_HSTNME_MAX_LEN 255
/* Function to convert given NetId into Array.*/
#define ISIS_CONVERT_DOT_NET_TO_ARRAY IsisDotNetToArray

#define ISIS_FORM_DOT_STR(StrLen, au1Src, au1Dest) IsisFormDotStr(StrLen, au1Src, au1Dest);
#endif  /* __ISDEFS_H__ */

