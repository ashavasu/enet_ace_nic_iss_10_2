#ifndef _STDISIWR_H
#define _STDISIWR_H
INT4 GetNextIndexIsisSysTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDISI(VOID);

VOID UnRegisterSTDISI(VOID);
INT4 IsisSysVersionGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysTypeGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysIDGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxPathSplitsGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxLSPGenIntGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysOrigL1LSPBuffSizeGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAreaAddressesGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMinL1LSPGenIntGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMinL2LSPGenIntGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysPollESHelloRateGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysWaitTimeGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysAdminStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL1StateGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysOrigL2LSPBuffSizeGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL2StateGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysLogAdjacencyChangesGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAreaCheckGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysNextCircIndexGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysExistStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL2toL1LeakingGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysSetOverloadGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL1MetricStyleGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL1SPFConsidersGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL2MetricStyleGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL2SPFConsidersGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysTEEnabledGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysReceiveLSPBufferSizeGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysTypeSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysIDSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxPathSplitsSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxLSPGenIntSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysOrigL1LSPBuffSizeSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAreaAddressesSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMinL1LSPGenIntSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMinL2LSPGenIntSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysPollESHelloRateSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysWaitTimeSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysAdminStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysOrigL2LSPBuffSizeSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysLogAdjacencyChangesSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAreaCheckSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysExistStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL2toL1LeakingSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysSetOverloadSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL1MetricStyleSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL1SPFConsidersSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL2MetricStyleSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysL2SPFConsidersSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysTEEnabledSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAgeSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysReceiveLSPBufferSizeSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxPathSplitsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxLSPGenIntTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysOrigL1LSPBuffSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAreaAddressesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysMinL1LSPGenIntTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysMinL2LSPGenIntTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysPollESHelloRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysWaitTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysAdminStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysOrigL2LSPBuffSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysLogAdjacencyChangesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAreaCheckTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysExistStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysL2toL1LeakingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysSetOverloadTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysL1MetricStyleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysL1SPFConsidersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysL2MetricStyleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysL2SPFConsidersTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysTEEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysMaxAgeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysReceiveLSPBufferSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIsisManAreaAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisManAreaAddrExistStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisManAreaAddrExistStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisManAreaAddrExistStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisManAreaAddrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIsisAreaAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisAreaAddrGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIsisSysProtSuppTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisSysProtSuppExistStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysProtSuppExistStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisSysProtSuppExistStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSysProtSuppTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIsisSummAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisSummAddrExistStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrAdminStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrMetricGet(tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrExistStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrAdminStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrMetricSet(tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrExistStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrAdminStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisSummAddrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIsisSysStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisSysStatCorrLSPsGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatAuthTypeFailsGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatAuthFailsGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatLSPDbaseOloadsGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatManAddrDropFromAreasGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatAttmptToExMaxSeqNumsGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatSeqNumSkipsGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatOwnLSPPurgesGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatIDFieldLenMismatchesGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatMaxAreaAddrMismatchesGet(tSnmpIndex *, tRetVal *);
INT4 IsisSysStatPartChangesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIsisCircTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisCircIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircIfSubIndexGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLocalIDGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircAdminStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircExistStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircTypeGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircExtDomainGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircAdjChangesGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircInitFailsGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircRejAdjsGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircOutCtrlPDUsGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircInCtrlPDUsGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircIDFieldLenMismatchesGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircMCAddrGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircPtToPtCircIDGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircPassiveCircuitGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircMeshGroupEnabledGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircMeshGroupGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircSmallHellosGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircIfSubIndexSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLocalIDSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircAdminStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircExistStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircTypeSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircExtDomainSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircMCAddrSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircPassiveCircuitSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircMeshGroupEnabledSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircMeshGroupSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircSmallHellosSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircIfSubIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLocalIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircAdminStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircExistStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircExtDomainTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircMCAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircPassiveCircuitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircMeshGroupEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircMeshGroupTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircSmallHellosTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIsisCircLevelTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisCircLevelMetricGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelISPriorityGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelDesISGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelLANDesISChangesGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelHelloMultiplierGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelHelloTimerGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelDRHelloTimerGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelLSPThrottleGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelMinLSPRetransIntGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelCSNPIntervalGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelPartSNPIntervalGet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelMetricSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelISPrioritySet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelHelloMultiplierSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelHelloTimerSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelDRHelloTimerSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelLSPThrottleSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelMinLSPRetransIntSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelCSNPIntervalSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelPartSNPIntervalSet(tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelISPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelHelloMultiplierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelHelloTimerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelDRHelloTimerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelLSPThrottleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelMinLSPRetransIntTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelCSNPIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelPartSNPIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisCircLevelTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIsisPacketCountTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisPacketCountHelloGet(tSnmpIndex *, tRetVal *);
INT4 IsisPacketCountLSPGet(tSnmpIndex *, tRetVal *);
INT4 IsisPacketCountCSNPGet(tSnmpIndex *, tRetVal *);
INT4 IsisPacketCountPSNPGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIsisISAdjTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisISAdjStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisISAdjNeighSNPAAddressGet(tSnmpIndex *, tRetVal *);
INT4 IsisISAdjNeighSysTypeGet(tSnmpIndex *, tRetVal *);
INT4 IsisISAdjNeighSysIDGet(tSnmpIndex *, tRetVal *);
INT4 IsisISAdjUsageGet(tSnmpIndex *, tRetVal *);
INT4 IsisISAdjHoldTimerGet(tSnmpIndex *, tRetVal *);
INT4 IsisISAdjNeighPriorityGet(tSnmpIndex *, tRetVal *);
INT4 IsisISAdjUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIsisISAdjAreaAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisISAdjAreaAddressGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIsisISAdjIPAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisISAdjIPAddressTypeGet(tSnmpIndex *, tRetVal *);
INT4 IsisISAdjIPAddressGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIsisISAdjProtSuppTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisISAdjProtSuppProtocolGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIsisIPRATable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisIPRADestTypeGet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRADestGet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRADestPrefixLenGet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRAExistStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRAAdminStateGet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRAMetricGet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRAMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRASNPAAddressGet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRADestTypeSet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRADestSet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRADestPrefixLenSet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRAExistStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRAAdminStateSet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRAMetricSet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRAMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRASNPAAddressSet(tSnmpIndex *, tRetVal *);
INT4 IsisIPRADestTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisIPRADestTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisIPRADestPrefixLenTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisIPRAExistStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisIPRAAdminStateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisIPRAMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisIPRAMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisIPRASNPAAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IsisIPRATableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIsisNotificationTable(tSnmpIndex *, tSnmpIndex *);
INT4 IsisTrapLSPIDGet(tSnmpIndex *, tRetVal *);
INT4 IsisSystemLevelGet(tSnmpIndex *, tRetVal *);
INT4 IsisPDUFragmentGet(tSnmpIndex *, tRetVal *);
INT4 IsisFieldLenGet(tSnmpIndex *, tRetVal *);
INT4 IsisMaxAreaAddressGet(tSnmpIndex *, tRetVal *);
INT4 IsisProtocolVersionGet(tSnmpIndex *, tRetVal *);
INT4 IsisLSPSizeGet(tSnmpIndex *, tRetVal *);
INT4 IsisOriginatingBufferSizeGet(tSnmpIndex *, tRetVal *);
INT4 IsisProtocolsSupportedGet(tSnmpIndex *, tRetVal *);
#endif /* _STDISIWR_H */
