/*****************************************************************************
 *
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 * $Id: isport.h,v 1.25 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file contains the various structures and definitions used
 * in the ISIS module and which are portable. Some of these definitions may
 * have to be modified based on the target modules requirements. 
 *
 ******************************************************************************/

#ifndef __ISPORT_H__
#define __ISPORT_H__

#ifndef SIZING_WANTED
#include "size.h"
#endif

#include "ip.h"
#include "ipv6.h"
#include "rmgr.h"
#include "cfa.h"
#include "trie.h"
#include "ip6util.h"
#include "rtm.h"
#include "rtm6.h"
#ifdef BFD_WANTED
#include "bfd.h"
#endif


/* NOTE: Make sure any addition (modification) or deletion of elements in the
 *       structures defined does not spoil the alignment. 
 */    

/*----------------------------------------------------------------------------*/
/* All the structures defined in this file are portable. Current implementation
 * assumes the following structures for interacting with the external modules.
 * These structures may have to be modified or defined afresh based on the
 * targets requirements
 */ 
/*----------------------------------------------------------------------------*/

/* 
 * PORTABLE STRUCTURE DEFINITIONS
 * ==============================
 */ 

/* TYPEDEFS related to FutureRM 
 * ============================
 */
typedef tCfaIfInfo       tIsisIfInfo;


/* TYPEDEFS
 * ========
 */

typedef tRtmMsgHdr tIsisRTMHdr;
                              /* Route Table Manager specific header to be
                               * appended to every message that ISIS sends. This
                               * header is also included in every message
                               * received by ISIS from RTM
                               */ 
                               
/* MEMORY CONFIGURATIONS
 * =====================
 */

#define ISIS_MAX_INSTS_SUPP              255
#define ISIS_MAX_CKT_SUPP                200000
#define ISIS_MAX_AREA_ADDR_SUPP          20000 
#define ISIS_MAX_ADJ_SUPP                200000
#define ISIS_MAX_IPRA_SUPP               200000
#define ISIS_MAX_EVTS_SUPP               200000
#define ISIS_MAX_SA_SUPP                 200000
#define ISIS_MAX_LSPS_SUPP               200000
#define ISIS_MAX_NODES                   255
#define ISIS_MAX_FACTOR                  100
#define ISIS_MAX_SUPP_ROUTES             200000
#define LSP_LEN_VALUE_ADD                 2
#define ISIS_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

/* FutureISIS is currently integrated with FutureRM. FutureRM and FutureISIS
 * uses the same message type values. Hence the macro returns the same argument.
 * If the message types used by the RM (third party) is different from the
 * message types used by FutureISIS, the macro must be appropriately modified to
 * convert the message types appropriately so that FutureISIS understands the
 * same
 */

#define ISIS_FT_GET_MSG_TYPE(u2Type)\
        u2Type 

#define ISIS_RTM_PKT_LEN(pRtmHdr)\
        pRtmHdr->u2MsgLen

#define ISIS_RTM_ACK_REQD                  0x80  /* 1000 0000 */
                                           /* Flag used to enable the
                                            * Acknowledgement from the FutureRTM
                                            */
#define ISIS_RTM_IFIDX_MASK                0x40  /* 0001 0000 */
                                           /* Flag to set the Bitmask in Route
                                            * Update Message to the FutureRTM
                                            * to denote the Change in the 
                                            * Interface Index Change of the
                                            * already existing Route
                                            */
#define ISIS_RTM_MET_MASK                  0x20  /* 0010 0000 */
                                           /* Flag to set the Bitmask in Route 
                                            * Update Message to the FutureRTM
                                            * to denote the Change in the Metric
                                            * of the already existing Route
                                            */
#define ISIS_RTM_ROW_STATUS_MASK           0x10  /* 0001 0000 */
                                           /* Flag to set the Bitmask in Route
                                            * Update Message to the FutureRTM
                                            * to denote the Change in the 
                                            * RowStatus of the already existing 
                                            * Route
                                            */

#define ISIS_RTMI_GET_BITMASK(u1BitMask)\
        ((u1BitMask == ISIS_SPT_ROUTE_MET_CHG) ? ISIS_RTM_MET_MASK :\
        ((u1BitMask == ISIS_SPT_ROUTE_DELETE) ? ISIS_RTM_ROW_STATUS_MASK :\
        ((u1BitMask == ISIS_SPT_ROUTE_IFIDX_CHG) ? ISIS_RTM_IFIDX_MASK : 0)))


#define ISIS_DEF_MAX_FACT            60 
                              /* 
                               * Buddy Buffers of size 1492 are allocated
                               * according to the following formula:
                               *
                               * Buddy Buffers = (ISIS_DEF_HMAX_LSPS *
                               *                  ISIS_DEF_HMAX_FACT) / 100,
                               *
                               *               = (2000 * 40)/100
                               *               = 800 buffers
                               *  Only a factor of the Max LSPs will be
                               *  allocated as buddy buffers because not all
                               *  LSPs are of size 1492 and buddy can fragment
                               *  buffers for allocation based on the request.
                               *  To increase the buddy buffers increase the
                               *  factor fixing the total number of LSPs.
                               */ 
#define ISIS_DEF_MAX_TXQL     1000 
                              /* Maximum LSPs that can be present in the
                               * Transmission Queue at any time. The LSPs in
                               * the Transmission Queue use a different header
                               * which includes SRM flags. This parameter
                               * defines the maximum number of such headers.
                               * If the queue is full, then additional memory
                               * is allocated to accomodate the temporary
                               * internal congestion and the extra memory
                               * allocated freed up subsequently when the
                               * congestion eases. Under normal conditions this
                               * value must be sufficient
                               */ 

/* System Configurations
 * =====================
 */

#define ISIS_SYS_SEM_NAME      (UINT1 *)"ISISS"
#define ISIS_TASK_NAME         (UINT1 *)"ISIS"
#define ISIS_SYS_FTM_TASK      (UINT1 *)"FTMT"
#define ISIS_SYS_PNI_TASK      (UINT1 *)"PNIT"

#define ISIS_CTRL_QUEUE        (UINT1 *)"CTRLQ"
#define ISIS_RTM_QUEUE         (UINT1 *)"ISRTMQ"
#define ISIS_FLT_QUEUE         (UINT1 *)"FLTQ"

#define ISIS_CTRLQ_DEPTH       4096
                              /* Defines the maximum number of messages that can
                               * be held in the queue
                               */ 
#define ISIS_RTMQ_DEPTH        100
                              /* Defines the maximum number of messages that can
                               * be held in the queue specific to RTM
                               */ 
#define ISIS_FLTQ_DEPTH        128
                              /* Defines the maximum number of messages that can
                               * be held in the queue specific to FTM
                               */ 

#define ISIS_SYS_TSK_PRIO      0x64
#define ISIS_SYS_STACK_SIZE    0x0FFF
                              /* Decimal 4096 bytes
                               */ 
#define ISIS_SYS_FT_DATA_EVT  0x01
                              /* This is the event posted by the Fault Tolerance
                               * module whenever a FT Message is posted to the
                               * Control Modules queue 
                               */ 
#define ISIS_SYS_LL_DATA_EVT  0x02
                              /* This is the event posted by the Lower Layer
                               * modules when a data frame received over
                               * physical interface is posted to the Control
                               * Modules queue
                               */ 
#define ISIS_SYS_TMR_EVT      0x04    
                              /* This is the event posted to ISIS module
                               * whenever any of the timers started by the
                               * protocol expires.
                               */ 
#define ISIS_SYS_RT_DATA_EVT  0x08
                              /* This is the event posted by the RTM 
                               * module whenever a RTM Message is posted to the
                               * Control Modules queue 
                               */ 
#define ISIS_SYS_INT_EVT      0x10
                              /* This event is posted by all the internal
                               * modules to indicate certain exceptions or
                               * protocol conditions to other internal modules.
                               * These events are logged into the event table
                               * and further actions taken based on the type of
                               * event
                               */
#define ISIS_SYS_FWD_EVT      0x20
                              /* Event to be posted to the Forwarding module
                               * whenever a message is enqueued to the
                               * Forwarding Module's Queue
                               */ 
#define ISIS_SYS_RT6_DATA_EVT  0x40
                              /* This is the event posted by the RTM6
                               * module whenever a RTM Message is posted to the
                               * Control Modules queue
                               */
#define ISIS_SYS_ROUTE_DATA_EVT 0x80
                              /* This is the event posted by the RTM
                               * module for route update 
                               */
#define ISIS_SYS_BFD_STATE_EVT 0x100
                              /* This is the event posted by the BFD
                               * module for session state change
                               */



/* Lengths - The following Constants must be set to proper values during 
 * compilation and cannot be modified since the data structures use these 
 * constants to define the respective arrays. Changing these values may alter 
 * memory requirements
 */
#define ISIS_IPV6RA_PREFIX_OFFSET 0x06
                              /* Prefix offset in the IPV6 RA TLV */


#define ISIS_IPV6RA_PREFIXLEN_OFFSET 0x05
                              /* PrefixLen offset in the IPV6 RA TLV */


#define ISIS_IPV4RA_PREFIX_OFFSET 0x04
                              /* Prefix offset in the IPV4 RA TLV */

 #define ISIS_EXT_IPV4RA_PREFIX_OFFSET 0x05
 /* Prefix offset in the EXT IPV4 RA TLV
 * In EXT IPRA TLV, Default Metric = 4 bytes +
 * Prefix length = 1 bytes */

 #define ISIS_EXT_IPV4RA_PREFIXLEN_OFFSET 0x04

 #define ISIS_MT_IPV6RA_PREFIX_OFFSET 0x08
 /* Prefix offset in the EXT IPV6 RA TLV
 * In MT IPRA TLV, MT ID = 2 bytes +
 * Default metric = 4 bytes +
 * Reserved = 1 byte
 * Prefix len = 1 byte*/
 #define ISIS_MT_IPV6RA_PREFIXLEN_OFFSET 0x07



#define ISIS_IPV6RA_PREFIX_LEN_ROUNDOFF  0x08
#define ISIS_PREFIX_LEN_ROUNDOFF         0x08
                              /* Prefix offset in the IPV4 RA TLV */
#define ISIS_NUM_METRICS       0x04 
                              /* Total number of metrics supported. By
                               * default one metric - the default metric is
                               * supported. This value indicates only
                               * the total number and does not specify 
                               * which metrics. The supported metrics can
                               * be dynamically configured restricted only
                               * by this value
                               */

#define ISIS_MAX_IPV4_PREFLEN  0x20
                              /* Maximum value of prefix length in IPV4
                               */
#define ISIS_MAX_IPV6_PREFLEN  0x80
                              /* Maximum value of prefix length in IPV6
                               */
#define ISIS_ETH_HDR_LEN       0x0E
#define ISIS_LLC_HDR_LEN       0x03
                              


/* DEFAULT VALUES
 * ==============
 */

/* Default Values assigned to the Data structures if Manager does not configure
 * any other values during configuration. When a Row is created these values get
 * updated into the Row, and remain as is unless manager modifies them
 * msubsequently. These values may have to be modified to suit the target system
 * based on the requirements
 */

#define ISIS_DEF_SYS_VER               0x01
                                            /* The Default System Version 
                                             */

#define ISIS_DEF_MPS                   0x02
                                            /* The Maximum number of paths with
                                             * equal routeing metric value 
                                             * which is permitted to split 
                                             * between
                                             */ 

#define ISIS_DEF_FT_BUF_SIZE           1522
#define ISIS_DEF_L1_LSPBUF_SIZE        1492
#define ISIS_DEF_L2_LSPBUF_SIZE        1492
                                            /* The Default L1 and L2 Buffer 
                                             * size which the system can handle
                                             */
#define ISIS_MIN_STD_RX_LSPBUF_SIZE    1492
#define ISIS_MAX_STD_RX_LSPBUF_SIZE    65535
                                            /* The Minium and Maximum receive buffer
                                               size that has to be supported as per
                                               stdisis.mib*/
#define ISIS_MIN_RX_LSPBUF_SIZE        512 
                                            /* Minimum Buffer size which the
                                             * system should handle
                                             */
#define ISIS_MAX_RX_LSPBUF_SIZE       16000 
                                            /* Maximum Buffer size which the
                                             * system can handle
                                             */

#define ISIS_DEF_WAIT_TIME               60 /* The Number of seconds to delay in
                                             * in waiting state before entering
                                             * ON state
                                             */ 
#define ISIS_DEF_ES_HELLORATE            50 /*The value, in seconds, to be used
                                             * for the suggested ES 
                                             * configuration timer in ISH PDUs 
                                             * when soliciting the ES 
                                             * configuration
                                             */
#define ISIS_IS_PRIORITY                 64 /* Default Value for the IS 
                                             * Priority in a LAN Network
                                             */
#define ISIS_HELLO_INT                 3000 /* Hello Time Interval in milli
                                             * seconds
                                             */
#define ISIS_ZERO_AGE_INT                60 /* Zero Age LifeTime interval in 
                                             * seconds
                                             */
#define ISIS_DR_HELLO_INT              1000 /* DIS Hello Time interval in 
                                             * milli seconds
                                             */
#define ISIS_PSNP_INT                     2 /* PSNP generation interval in
                                             * seconds 
                                             */
#define ISIS_CSNP_INT                    10 /* CSNP generation interval in 
                                             * seconds 
                                             */
#define ISIS_MIN_L1GEN_INT               30 /* L1 LSP minimum generation 
                                             * interval in seconds 
                                             */
#define ISIS_MIN_L2GEN_INT               30 /* L2 LSP minimum generation
                                             * interval in seconds 
                                             */
#define ISIS_MAX_LSP_GEN_INT            900 /* L1 and L2 LSP maximum generation
                                             * interval in seconds 
                                             */
#define ISIS_MAX_AGE_MIN_INT           1200 /* The Minimum value for the Max Age
                                             * interval
                                             */
#define ISIS_MAX_AGE_MAX_INT          65535 /* The Maximum value for the Max Age
                                             * interval
                                             */
#define ISIS_LSP_MAXAGE                1200 /* Maximum Age of the LSP beyond
                                             * which the LSP is purged from 
                                             * the ZERO AGE Timer is started.
                                             */
#define ISIS_LSP_THROTTLE                10 /* Minimal Interval in milli 
                                             * seconds between LSP 
                                             * transmissions of LSPs on a 
                                             * interface for a particular 
                                             * Circuit Level
                                             */
#define ISIS_LSP_RETXN_INT                5 /* Minimum interal in seconds 
                                             * between retransmission of LSPs 
                                             * in a particular Level for Point
                                             * to Point Circuits
                                             */
#define ISIS_MAX_LNK_MET                63 /* Maximum Metric per link */
#define ISIS_MAX_LNK_MET_MT             0xFE000000 /* Maximum Metric per link if 
                                                      multi-topology is enabled */
#define ISIS_MAX_PATH_MET            1023 /* Maximum Path Metric */
#define ISIS_MAX_PATH_IPV4_MET            1023 /* Maximum Path Metric */
#define ISIS_MAX_PATH_IPV6_MET           0xFE000000 /* Maximum Path Metric */
#define ISIS_CKTL_DEF_METRIC_VALUE       10 /* The default circuit Metric Value
                                             * of the metric supported 
                                             */
#define ISIS_IPRA_DEF_METRIC_VALUE       20 /* The default IPRA Metric Value
                                             * of the metric supported 
                                             */
#define ISIS_HELLO_MULT                  10 /* To be multiplied with Hello 
                                             * time to get the holding time
                                             * for adjacenies
                                             */
#define ISIS_MAX_IF_TYPES              0x04 /* Number of Interface Types.
                                             * Currently supported types are
                                             * Ethernet, PPP, ATM and FR. Modify
                                             * this value if any other
                                             * interfaces are added.
                                             */ 
#define ISIS_MIN_SPF_SCH_TIME          0x02
                                            /* Minimum time to schedule SPF
                                             */
#define ISIS_MAX_SPF_SCH_TIME          0x0A
                                            /* Maximum time after which the
                                             * SPF is forced to scheduled
                                             */
#define ISIS_MIN_LSP_MARK              0x01 
                                            /* Minimum Value of LSP Change 
                                             * above which the 
                                             * SPF is triggered
                                             */
#define ISIS_MAX_LSP_MARK              0x0A
                                            /* Maximum Value of LSP Change 
                                             * above which the SPF is not 
                                             * triggered
                                             */
#define ISIS_DB_NORM_TIME              0x0A
                                            /* The Time interval upon 
                                             * expiry the normalization
                                             * of LSP Databases are 
                                             * carried out
                                             */ 
/* Max Definitions 
 */

#define ISIS_MAX_PROTS_SUPP            0x03
                                     /* Currently supports IPV4. Modify 
                                      * this value if more protocols 
                                      * are added
                                      */
#define ISIS_MAX_ADJ_PROT_SUPP         0x04
                                     /* Currently supports 4 protocol types 
                                      * Increase this value if more protocols 
                                      * are added
                                      */
#define ISIS_MAX_EC1_BLKS              0x1E
                                     /* Defines the range of timers supported
                                      * by this block. Since the granularity of
                                      * this block is '1', this value defines a
                                      * ISIS_MAX_EC1_BLKS '1' second timers.
                                      */ 
#define ISIS_MAX_EC2_BLKS              0x1E
                                     /* Defines the range of timers supported
                                      * by this block. Since the granularity of
                                      * this block is '10', this value defines a
                                      * ISIS_MAX_EC2_BLKS '10' second timers
                                      */ 
#define ISIS_MAX_EC3_BLKS              0x0C
                                     /* Defines the range of timers supported
                                      * by this block. Since the granularity of
                                      * this block is '100', this value defines 
                                      * ISIS_MAX_EC3_BLKS '100' second timers
                                      */ 
#define ISIS_MAX_MODULES               0x14
                                     /* Defines the size of the gau1ModLevelMask
                                      * array. This array holds the Log status
                                      * of each of the modules. The value at a
                                      * particular index in this array defines
                                      * the LOG Levels enabled or disabled for a
                                      * particular module.
                                      */ 
#define ISIS_MAX_LOG_STR_SIZE           180 
                                     /* Defines the maximum size of the string
                                      * that can be logged.
                                      */ 
#define ISIS_MAX_LOG_SIZE               200 
                                     /* Defines the maximum size of the string
                                      * that can be logged.
                                      */ 
#define ISIS_MAX_LOG_TIME_SIZE          (100 + ISIS_MAX_LOG_SIZE)
                                     /* Defines the maximum size of the string
                                      * that can be logged.
                                      */

#define ISIS_MAX_RX_PASSWDS            0x03
                                     /* Maximum number of Authentication keys in
                                      * the receive direction.
                                      */ 
#define ISIS_MAX_ISH_TX_COUNT           0xA
                                     /* Maximum number of times ISH is sent on
                                      * point to point circuits
                                      */ 
#define ISIS_MAX_SPF_NODES              0x61a8 
                                      /* Maximum number of Nodes which can be
                                       * run in decn without relinquishing
                                       */
#define ISIS_MAX_HOSTNME                256

/* Fault Tolerance Message Types
 * 
 * NOTE: The values defined are as per FSAR implementation. If any other fault
 * tolerance module is used on the target these message type values may have to
 * be modified as per the actual definitions
 */

#define ISIS_GO_ACTIVE                 0x01
#define ISIS_GO_OOS                    0x02
#define ISIS_INIT_BULK_UPDATE          0x03
#define ISIS_ENABLE_LSU                0x04
#define ISIS_DISABLE_LSU               0x05
#define LOCK_STEP_UPDATE_MSG           0x10
#define ISIS_BULK_DATA                 0x11
#define ISIS_BULK_DATA_COMPLETE        0x12 
#define ISIS_BULK_DATA_REQ             0x13 
#define ISIS_SE_REG                    0x0d
#define ISIS_SE_DEREG                  0x0e
#define ISIS_BULK_UPDATE_COMPLETE      0x0f 

/*SNMP traps Interface defines */

#define ISIS_TRAPS_OID_LEN  8 
#define SNMP_VERSION        1


/* Declaration for other protocols supported by ISIS */
#define  ISIS_OTHERS_ID     OTHERS_ID
#define  ISIS_LOCAL_ID      CIDR_LOCAL_ID           
#define  ISIS_STATIC_ID     CIDR_STATIC_ID
#define  ISIS_RIP_ID        RIP_ID
#define  ISIS_OSPF_ID       OSPF_ID
#define  ISIS_BGP_ID        BGP_ID


PUBLIC VOID IsisSnmpIfSendTrap (UINT1 *, UINT1);

#endif /* __ISPORT_H__ */
