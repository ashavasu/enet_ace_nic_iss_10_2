/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isglob.h,v 1.10 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file contains all the global variable definitions
 *
 *****************************************************************************/

#ifndef __ISGLOB_H__
#define __ISGLOB_H__
INT1               gai1IsisLogStr [ISIS_MAX_LOG_SIZE];
INT1               gai1IsisTmLogStr [ISIS_MAX_LOG_TIME_SIZE];
UINT1              gu1IsisStatus;
UINT1              gau1IsisModLevelMask [ISIS_MAX_MODULES];
tMemPoolId         gau1IsisBdyId [BUDDY_MAX_INST];
UINT1              gu1RelqFlag;
UINT1              gu1IsisGrCsnp;
tIsisMemConfigs    gIsisMemConfigs;
tIsisMemConfigs    gIsisMemActuals;
tIsisSysContext    *gpIsisInstances;
tIsisMemPoolId     gIsisMemPoolId;
tIsisLSPTxTable    gIsisLspTxPool;
tIsisHashTable     gIsisCktHashTable;
tIsisExtInfo       gIsisExtInfo;
tTimerListId       gIsisTmrListID;
tOsixTaskId        gIsisTaskId;
tOsixQId           gIsisCtrlQId;
tOsixQId           gIsisRtmQId;
tIsisGrInfo       *gIsisGrinfo;
INT4               gi4IsisSysLogId;
UINT1              gau1IsisHstNme[ISIS_HSTNME_LEN];
#ifdef ISIS_FT_ENABLED
tOsixQId           gIsisFltQId;
#endif


UINT1              gu1IsisGrCsrFlag;
UINT1              gu1IsisGrRestoreFlag;
UINT1              gu1IsisGrState;
UINT1              gu1IsisState;
UINT1              gu1IsisGrOverLoad;
UINT4              gu4SemNameVar;

VOID (*gIsisTmrRoutines []) (tIsisSysContext *) = {
                                     IsisTmrProcECTimeOut,
                                     IsisUpdProcL1LSPGenTimeOut,
                                     IsisUpdProcL2LSPGenTimeOut,
                                     IsisDecProcSPFSchTimeOut,
                                     IsisUpdProcL1WaitingTimeOut,
                                     IsisUpdProcL2WaitingTimeOut,
                                     IsisUpdProcSHNTimeOut,
         IsisGRUpdProcT3TimeOut,
         IsisGRUpdProcL1T2TimeOut,
         IsisGRUpdProcL2T2TimeOut,
                                     NULL
                                 };
#endif
