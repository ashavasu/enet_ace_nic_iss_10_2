/********************************************************************
 * Copyright (C) Future Software Limited, 1997-98, 2001
 *
 * $Id: ismacs.h,v 1.22 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file contains  various macros used in the 
 *              ISIS Module
 *
 *******************************************************************/

#ifndef __ISMACS_H__
#define __ISMACS_H__

#ifndef PROTO
#define PROTO(x) x
#endif

/* Buffer allocation macros
 */

#if (ISIS_MEM_DBG == 1)

#define ISIS_MEM_ALLOC(u1Type, u4Size)\
        IsisUtlAllocBuf (u1Type, u4Size, __FILE__, __LINE__)

#define ISIS_MEM_FREE(u1Type, pBuf )\
        IsisUtlReleaseBuf (u1Type, pBuf, __FILE__, __LINE__)

#else

#define ISIS_MEM_ALLOC(u1Type, u4Size)\
        IsisUtlAllocBuf (u1Type, u4Size)

#define ISIS_MEM_FREE(u1Type, pBuf )\
        IsisUtlReleaseBuf (u1Type, pBuf)

#endif


#define ISIS_MEM_ALLOC_FAIL "Memory Allocation Failure"

/* Macro to get number of bytes (1-2-4) from the specified offset  
 */

#define ISIS_GET_1_BYTE(Buf,u2Offset,u1Val)\
        (u1Val) = (UINT1)(*(UINT1 *)(Buf+u2Offset))

#define ISIS_GET_2_BYTES(Buf,u2Offset,u2Val)\
        MEMCPY ((UINT1 *)&u2Val, (UINT1 *)(Buf + u2Offset), 2);\
        u2Val = (UINT2) OSIX_NTOHS (u2Val)

#define ISIS_GET_4_BYTES(Buf,u2Offset,u4Val)\
        MEMCPY ((UINT1 *)&u4Val, (UINT1 *)(Buf + u2Offset), 4);\
        u4Val = (UINT4) OSIX_NTOHL (u4Val)



#define ISIS_GET_DATA(dest,src,len) \
        MEMCPY(dest,src,len)   



/* Macro to set specified value (of bytes 1-2-4) from the specified offset
 */

#define ISIS_ASSIGN_1_BYTE(Buf, u2Offset, u1Val)\
        (*(UINT1 *)(Buf+u2Offset)) = (UINT1)u1Val

#define ISIS_ASSIGN_2_BYTES(Buf, u2Offset, Val)\
        {\
           UINT2 u2Val;\
           \
           u2Val = (UINT2) OSIX_HTONS (Val);\
           MEMCPY ((Buf + u2Offset), (UINT1 *)&u2Val, 2);\
        }


#define ISIS_ASSIGN_4_BYTES(Buf, u2Offset, Val)\
        {\
          UINT4 u4Val;\
          \
          u4Val = (UINT4) OSIX_HTONL (Val);\
          MEMCPY ((Buf + u2Offset), (UINT1 *)&u4Val, 4);\
        }

#define ISIS_ASSIGN_STRING(Buf,u2Offset,str,len) \
        MEMCPY(((UINT1*)Buf+u2Offset),str,len)


/* Common Header Access Macros
 */ 

#define ISIS_EXTRACT_PDU_TYPE(pPDU)\
        (UINT1)(((tIsisComHdr *)(pPDU))->u1PDUType & ISIS_PDU_TYPE_MASK)

#define ISIS_GET_PDU_HDR_LEN(u1PduType)\
        (((u1PduType == ISIS_L1LAN_IIH_PDU)\
         || (u1PduType == ISIS_L2LAN_IIH_PDU)) ? ISIS_LAN_IIH_HDR_LEN :\
        ((u1PduType == ISIS_P2P_IIH_PDU) ? ISIS_P2P_IIH_HDR_LEN :\
        (((u1PduType == ISIS_L1LSP_PDU)\
         || (u1PduType == ISIS_L2LSP_PDU)) ? ISIS_LSP_HDR_LEN :\
        (((u1PduType == ISIS_L1PSNP_PDU)\
         || (u1PduType == ISIS_L2PSNP_PDU)) ? ISIS_PSNP_HDR_LEN :\
        (((u1PduType == ISIS_L1CSNP_PDU)\
         || (u1PduType == ISIS_L2CSNP_PDU)) ? ISIS_CSNP_HDR_LEN : 0)))))
        
#define ISIS_IDLEN(x) (((x)==0)?ISIS_DEF_SYS_ID_LEN:(((x)==255)?0:(x)))

#define ISIS_EXTRACT_HELLO_PDU_LEN(pPDU, u2Len)\
        ISIS_GET_2_BYTES (pPDU, (sizeof (tIsisComHdr) +\
        ISIS_IDLEN(((tIsisComHdr*)pPDU)->u1IdLen) + 3), u2Len)

/*ISIS NSAP macro */

#define ISIS_DOT1_COUNT(u1Cnt,u1Pos)\
         (UINT1) ((u1Cnt%2==0)?(u1Pos + 2):(u1Pos + 3))
#define ISIS_DOT2_COUNT(u1Cnt,u1Pos)\
         (UINT1) ((u1Cnt%2==0)?(u1Pos + 3):(u1Pos + 2))

#define ISIS_MAX_FIRST_BYTE_AREA_ID  ((UINT1)0x9f)

/* Hello PDU Access Macros
 */

#define ISIS_EXTRACT_HELLO_CKT_TYPE(pPDU)\
        (UINT1)(*(pPDU + sizeof (tIsisComHdr)))

#define ISIS_GET_ADJ_USAGE(u1PDUType) \
        (UINT1)((u1PDUType == ISIS_L1LAN_IIH_PDU) ? ISIS_L1ADJ : ISIS_L2ADJ)
        
#define ISIS_GET_ADJ_USAGE_WITH_CKTLVL(u1CktLvl) \
        (UINT1)((u1CktLvl == ISIS_LEVEL1) ? ISIS_L1ADJ :\
                ((u1CktLvl == ISIS_LEVEL2) ? ISIS_L2ADJ : ISIS_LEVEL12)) 
        
#define ISIS_GET_LEVEL(u1PDUType) \
        (UINT1)((u1PDUType == ISIS_L1LAN_IIH_PDU) ? ISIS_LEVEL1 : ISIS_LEVEL2)

#define ISIS_GET_HELLO_INT(pCktLevel) \
        ((pCktLevel->bIsDIS == ISIS_TRUE) ? pCktLevel->DRHelloTimeInt \
                                          : pCktLevel->HelloTimeInt)

/* Bit Pattern Manipulation Macros
 */

#define ISIS_NUM_BYTES(x) (((x) - 1)/8 + 1)
#define ISIS_BYTE_POS(x) ((x - 1) / 8)
#define ISIS_BIT_POS(x) (((x) % 8) ? (((x) % 8) - 1) : 7)
#define ISIS_BIT_MASK(x,y) y = (UINT1) (0x80 >> ISIS_BIT_POS(x))

#define ISIS_GET_PREFIX_LEN(u1Mask)\
        ((u1Mask == 0xFF) ? 8 :\
         ((u1Mask == 0xFE) ? 7 :\
         ((u1Mask == 0xFC) ? 6 :\
         ((u1Mask == 0xF8) ? 5 :\
         ((u1Mask == 0xF0) ? 4 :\
         ((u1Mask == 0xE0) ? 3 :\
         ((u1Mask == 0xC0) ? 2 :\
         ((u1Mask == 0x80) ? 1 :\
         ((u1Mask == 0x00) ? 0 : -1)))))))))

#define ISIS_BIT_VALUE(ID)\
        ((ID == 1) ? 0 :\
         ((ID == 2) ? 1 :\
         ((ID == 4) ? 2 :\
         ((ID == 8) ? 3 :\
         ((ID == 16) ? 4 :\
         ((ID == 32) ? 5 :\
         ((ID == 64) ? 6 : 7)))))))

/* MISIS - GETMTID*/
#define ISIS_MTID_MASK         0x0FFF 
#define ISIS_MT_LSP_FLAG_MASK  0xF0 /* 1111 0000 */
#define ISIS_MT_LSP_OFLAG_MASK 0x80 /* 1000 0000 */
#define ISIS_MT_LSP_ATT_MASK   0x40 /* 0100 0000- This will be used for setting 
                                               attach flag in MT TLV*/

/* Link State PDU Access Macros
 */

#define ISIS_OFFSET_PDULEN  sizeof (tIsisComHdr)
#define ISIS_OFFSET_RLT    (ISIS_OFFSET_PDULEN + 2)
#define ISIS_OFFSET_LSPID  (ISIS_OFFSET_PDULEN + 4)
#define ISIS_OFFSET_PNODEID (ISIS_OFFSET_LSPID + ISIS_SYS_ID_LEN)
#define ISIS_OFFSET_LSPNUM  (ISIS_OFFSET_PNODEID + 1)
#define ISIS_OFFSET_SEQNO  (ISIS_OFFSET_LSPID + ISIS_LSPID_LEN)
#define ISIS_OFFSET_CHKSUM (ISIS_OFFSET_SEQNO + 4)

#define ISIS_LSP_HDR_SIZE  (sizeof (tIsisComHdr) - (ISIS_SYS_ID_LEN + 13))

#define ISIS_EXTRACT_LSPID_FROM_LSP(pu1LSP, au1LspId)  \
        MEMCPY (au1LspId, (pu1LSP + ((UINT2)ISIS_OFFSET_LSPID)), \
                (ISIS_LSPID_LEN))

#define ISIS_EXTRACT_RLT_FROM_LSP(pBuf, u2Rlt)       \
        ISIS_GET_2_BYTES(pBuf, (((UINT2)(ISIS_OFFSET_RLT))), u2Rlt)

#define ISIS_EXTRACT_PDU_LEN_FROM_LSP(pBuf, u2Len)            \
        ISIS_GET_2_BYTES(pBuf, ((UINT2)ISIS_OFFSET_PDULEN), u2Len)

#define ISIS_EXTRACT_SEQNUM_FROM_LSP(pBuf, u4SeqNum) \
        ISIS_GET_4_BYTES(pBuf, ((UINT2)ISIS_OFFSET_SEQNO), u4SeqNum)

#define ISIS_EXTRACT_CS_FROM_LSP(pBuf, u2Cs)  \
        ISIS_GET_2_BYTES(pBuf, ((UINT2)ISIS_OFFSET_CHKSUM), u2Cs)

#define ISIS_SET_LSP_CS(pBuf, u2Cs)    \
        ISIS_ASSIGN_2_BYTES(pBuf, ((UINT2)ISIS_OFFSET_CHKSUM), u2Cs)

#define ISIS_SET_CTRL_PDU_LEN(pBuf, u2Len)   \
        ISIS_ASSIGN_2_BYTES(pBuf, ((UINT2)ISIS_OFFSET_PDULEN), u2Len)

#define ISIS_SET_LSP_SEQNO(pBuf, u4SeqNum) \
        ISIS_ASSIGN_4_BYTES(pBuf, ((UINT2)ISIS_OFFSET_SEQNO), u4SeqNum)

#define ISIS_SET_LSP_RLT(pBuf, u2RLT)   \
        ISIS_ASSIGN_2_BYTES(pBuf, ((UINT2)(ISIS_OFFSET_PDULEN + 2)), u2RLT) 

#define ISIS_TRIGGER_DEC(pContext, u1Level)  \
        (u1Level == ISIS_LEVEL1) ? (pContext->SysL1Info.u2TrgCount++) : \
                                   (pContext->SysL2Info.u2TrgCount++)

#define ISIS_SLL_DYN_Scan(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First ((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(pNode))))))))


#define ISIS_GET_LVL_FROM_PTYPE(u1MsgType)\
        (((u1MsgType == ISIS_L1LSP_PDU) ||\
         (u1MsgType == ISIS_L1CSNP_PDU) ||\
         (u1MsgType == ISIS_L1PSNP_PDU)) ? ISIS_LEVEL1 :\
         (((u1MsgType == ISIS_L2LSP_PDU) ||\
         (u1MsgType == ISIS_L2CSNP_PDU) ||\
         (u1MsgType == ISIS_L2PSNP_PDU)) ? ISIS_LEVEL2 : 0))

/* LOGIC: Every '1 sec' we can transmit '1000 / u2Int' packets. Out of these 
 *        98% of the packets will be LSPs, 1% will be CSNP/PSNP and 1% will be
 *        Hello. 
 */        

#define ISIS_MAX_LSP_TX_COUNT(u4Int)\
        ISIS_MAX (((1000 * 98) / (u4Int * 100)), 1)

#define ISIS_GET_MAX_CSNP_COUNT(u4Int)\
        ISIS_MAX (((1000 * 1) / (u4Int * 100)), 1) 

#define ISIS_GET_REM_BYTES(Num, Len)\
        (((Num % (ISIS_MAX_TLV_LEN / Len)) == 0) ? 0 :\
        (ISIS_MAX_TLV_LEN - ((Num % (ISIS_MAX_TLV_LEN / Len)) * Len)))

#define ISIS_ROUNDOFF_PREFIX_LEN(x)  ((x+7)/8)

#define ISIS_GET_NUM_TLV(pLSP, u1TlvType)\
        ((u1TlvType == ISIS_IS_ADJ_TLV) ? pLSP->u1NumISTLV :\
        ((u1TlvType == ISIS_EXT_IS_REACH_TLV) ? pLSP->u1NumExtISReachTLV :\
        ((u1TlvType == ISIS_MT_IS_REACH_TLV) ? pLSP->u1NumMTISReachTLV :\
        ((u1TlvType == ISIS_EXT_IP_REACH_TLV) ? pLSP->u1NumExtIpReachTLV :\
        ((u1TlvType == ISIS_MT_IPV6_REACH_TLV) ? pLSP->u1NumMTIpv6ReachTLV :\
        (((u1TlvType == ISIS_IP_INTERNAL_RA_TLV)\
         || (u1TlvType == ISIS_IP_EXTERNAL_RA_TLV)) ? pLSP->u1NumIPRATLV :\
         ((u1TlvType == ISIS_IPV6_RA_TLV) ? pLSP->u1NumIPV6RATLV :\
        ((u1TlvType == ISIS_IPV4IF_ADDR_TLV) ? pLSP->u1NumIPIfTLV : \
        ((u1TlvType == ISIS_IPV6IF_ADDR_TLV) ? pLSP->u1NumIPV6IfTLV : 0)))))))))

#define ISIS_GET_TLV_LEN(u1TlvType)\
        ((u1TlvType == ISIS_IS_ADJ_TLV) ? ISIS_IS_ADJ_TLV_LEN :\
        (((u1TlvType == ISIS_IP_INTERNAL_RA_TLV)\
         || (u1TlvType == ISIS_IP_EXTERNAL_RA_TLV)) ? ISIS_IPRA_TLV_LEN :\
         ((u1TlvType == ISIS_IPV6_RA_TLV)?ISIS_IP6RA_TLV_LEN :\
         ((u1TlvType == ISIS_EXT_IP_REACH_TLV)?ISIS_EXT_IP_REACH_TLV_LEN :\
         ((u1TlvType == ISIS_MT_IPV6_REACH_TLV)?ISIS_MT_IPV6_REACH_TLV_LEN :\
         ((u1TlvType == ISIS_EXT_IS_REACH_TLV)?ISIS_EXT_IS_REACH_TLV_LEN:\
         ((u1TlvType == ISIS_MT_IS_REACH_TLV)?ISIS_MT_IS_REACH_TLV_LEN:\
         ((u1TlvType == ISIS_IPV6IF_ADDR_TLV)?ISIS_MAX_IPV6_ADDR_LEN:\
        ((u1TlvType == ISIS_IPV4IF_ADDR_TLV) ? ISIS_MAX_IPV4_ADDR_LEN :\
        ((u1TlvType == ISIS_DYN_HOSTNME_TLV) ? ISIS_HSTNME_TLV_LEN : 0))))))))))

#define ISIS_NUM_LSP_PER_CSNP(pContext, u1Level) \
        ((u1Level == ISIS_LEVEL1) ?  \
             ((pContext->SysActuals.u4SysOrigL1LSPBufSize -  \
              (3 * ISIS_SYS_ID_LEN) - sizeof (tIsisComHdr) - \
              10 - ISIS_MAX_PASSWORD_LEN) /                  \
              ((257 * ISIS_LSP_ENTRY_TLV_LEN ) / 255))        \
           : ((pContext->SysActuals.u4SysOrigL2LSPBufSize -  \
              (3 * ISIS_SYS_ID_LEN) - sizeof (tIsisComHdr) - \
              10 - ISIS_MAX_PASSWORD_LEN) /                  \
              ((257 * ISIS_LSP_ENTRY_TLV_LEN) / 255)))        \

#define ISIS_GET_MAX_BUF_LEN(pContext, u1Level)             \
        ((u1Level == ISIS_LEVEL1) ? pContext->SysActuals.u4SysOrigL1LSPBufSize \
                                  : pContext->SysActuals.u4SysOrigL2LSPBufSize)


#define COPY_U4_FROM_BUF(val,buf) do { \
     val = 0; \
     val |=buf[0]; \
     val <<= 8; \
     val |=buf[1]; \
     val <<= 8; \
     val |=buf[2]; \
     val <<= 8; \
     val |=buf[3]; \
    } while (0)


/* LSP Database Access Macros to get the first LSP Record for the given Level
 */

#define ISIS_UPD_FIRST_LSP_TX_ENTRY(pContext, u1Level) \
        (u1Level == ISIS_LEVEL1) ?   \
        ((pContext->SysL1Info.LspTxQ.pHashBkts == NULL) ? \
        NULL : pContext->SysL1Info.LspTxQ.pHashBkts->pFirstElem) : \
        ((pContext->SysL2Info.LspTxQ.pHashBkts == NULL) ? \
        NULL : pContext->SysL2Info.LspTxQ.pHashBkts->pFirstElem) 

#define ISIS_UPD_FIRST_LSP_DB_ENTRY(pContext, u1Level) \
        (u1Level == ISIS_LEVEL1) ?   \
        ((pContext->SysL1Info.LspDB.pHashBkts == NULL) ? \
        NULL : pContext->SysL1Info.LspDB.pHashBkts->pFirstElem) : \
        ((pContext->SysL2Info.LspDB.pHashBkts == NULL) ? \
        NULL : pContext->SysL2Info.LspDB.pHashBkts->pFirstElem) 

#define ISIS_UPD_TX_FLSP(pTxHash) \
        (((tIsisLSPTxEntry *)(pTxHash->pFirstElem))->pLSPRec)

#define ISIS_UPD_TX_NLSP(pTxElem) \
        ((pTxElem->pLSPRec->pNext != NULL ) \
        ? (((tIsisLSPTxEntry *)(pTxElem->pLSPRec->pNext))->pLSPRec) \
        : NULL)

#define ISIS_UPD_TX_FE(pTxHash) \
        ((tIsisLSPTxEntry *)(pTxHash->pFirstElem))

#define ISIS_UPD_TX_NE(pTxElem) \
        ((tIsisLSPTxEntry *)(pTxElem->pLSPRec->pNext))

#define ISIS_UPD_DB_FE(pDbHash) \
        ((tIsisLSPEntry *)(pDbHash->pFirstElem))
        
#define ISIS_UPD_DB_NE(pDbElem) \
        pDbElem->pNext

/* Timer Macros
 */

#define ISIS_TIME_DIFF(x,y)\
        ((x) > (y)) ? ((x) - (y)) \
                    : ((0xffffffff - (y)) + (x))

#define ISIS_CONV_MILLISEC_TO_SEC(x) ((x > 1) ? (((x - 1) / 1000) + 1) : 1)
#define ISIS_GET_CURR_TICK(pContext)\
        pContext->SysTimers.TmrECInfo.u4TCount

#define ISIS_INIT_TIMER(x)\
        ((x).TimerId = ISIS_INACTIVE_TIMER)

#define ISIS_IS_TIMER_ACTIVE(x)\
        (((x).TimerId == ISIS_INACTIVE_TIMER) ? ISIS_FALSE\
                                             : ISIS_TRUE)

#define ISIS_DEL_LSP_TIMER_NODE(x) \
         IsisUpdDelLspTimerNode((tIsisLSPEntry *) (x))

/* Divide and Round Off
 */

#define ISIS_DIVR(X, Y)\
         (((X % Y) == 0) ? (X/Y) : (((X % Y) >= Y/2) ? (X/Y) : ((X/Y) + 1)))

#define ISIS_TMR_INC(u1Idx, u2Dur, u2Int)\
        (u1Idx + ISIS_DIVR (u2Dur, u2Int))

#define ISIS_TMR_INC_EC1(u1Idx, u2Dur)\
        (u1Idx +  u2Dur)
                     
#define ISIS_MAX(x,y)\
        (((x) > (y)) ? (x) : (y))

#define ISIS_MIN(x,y)\
        (((x) < (y)) ? (x) : (y))


/* Sequence Number PDU Access Macros
 */

#define ISIS_EXTRACT_SNP_PDU_LEN(pPDU, u2Len)\
        ISIS_GET_2_BYTES (pPDU, sizeof (tIsisComHdr), u2Len)

/* Adjacency Access macros
 */

#define ISIS_ADJ_SET_ECTID(pCktLvlRec, u1Id)  \
        pCktLvlRec->u1ECTId = u1Id

#define ISIS_EXTRACT_HOLD_TIME(pu1PDU, u2HoldTime) \
        ISIS_GET_2_BYTES (pu1PDU, (sizeof (tIsisComHdr) + ISIS_SYS_ID_LEN + 1), u2HoldTime)
        
#define ISIS_EXTRACT_HELLO_SYS_ID(x)\
        ((UINT1 *)x + sizeof (tIsisComHdr) + 1)

#define ISIS_EXTRACT_NET_LEN(pPDU, NetLen)\
   NetLen = *((UINT1 *)pPDU + ISIS_ISH_NET_OFFSET)

#define ISIS_EXTRACT_SYSID_FROM_NET(pPDU, pSysId, u1NetLen)\
            MEMCPY (pSysId, ((UINT1 *)pPDU + ISIS_ISH_NET_OFFSET + \
                    1 + u1NetLen - (1+ISIS_SYS_ID_LEN)), ISIS_SYS_ID_LEN)
        
#define ISIS_EXTRACT_ISH_HOLD_TIME(u1Pdu, u2HoldTime)\
        ISIS_GET_2_BYTES (u1Pdu, 5, u2HoldTime)

#define ISIS_EXTRACT_PRIORITY(x)\
        (UINT1)(*((UINT1 *)x + sizeof (tIsisComHdr) + ISIS_SYS_ID_LEN + 5))

#define ISIS_EXTRACT_P2P_NEIGHBOR_SYS_ID(pu1PDU, u2Offset, pSysId)\
  MEMCPY(pSysId, ((UINT1 *)pu1PDU + u2Offset), ISIS_SYS_ID_LEN)

#define ISIS_EXTRACT_P2P_NEIGHBOR_EXT_CIRC_ID(pu1PDU, u2Offset, u4ExtCktId)\
  ISIS_GET_4_BYTES(pu1PDU, u2Offset, u4ExtCktId) 

/* Metric Extraction macros
 */

#define ISIS_MET_SUPP_STATUS(pu1Buf)\
        ((*pu1Buf & ISIS_METRIC_MASK) ?"Not Supported"\
                                      :"Supported")
#define ISIS_ADJ_SYS_TYPE(u2Metric) \
        ((u2Metric & ISIS_SPT_IS_FLAG) == ISIS_SPT_IS_FLAG) \
              ? ISIS_OSI_SYSTEM : ISIS_IP_END_SYSTEM

#define ISIS_GET_SYS_TYPE_MASK(u1Level)\
        ((u1Level == ISIS_LEVEL1) ? ISIS_SPT_L1_SYS_MASK :\
         ((u1Level == ISIS_LEVEL2) ? ISIS_SPT_L2_SYS_MASK : \
                                     ISIS_SPT_L12_SYS_MASK))
#define ISIS_GET_MET_TYPE(u1Val)\
        (((u1Val & ISIS_METRIC_TYPE_MASK) == ISIS_METRIC_TYPE_MASK) ? \
        ISIS_EXTERNAL : ISIS_INTERNAL)

#define ISIS_GET1_MET_TYPE(u1Val)\
        (((u1Val & ISIS_METRIC_TYPE_MASK) == ISIS_METRIC_TYPE_MASK) ? \
         ISIS_EXTERNAL : ISIS_INTERNAL)

#define  ISIS_SET_MET_TYPE(u1Byte,Type) \
         ((Type == ISIS_INTERNAL_METRIC) ? (u1Byte &= (~0x40)) :\
          (u1Byte |=  0x40))

/* Decision Module Macros
 */

#define  ISIS_MARK_PATH(pSPTNode, u1MetIdx, u1Flag) \
            pSPTNode->au2Metric[u1MetIdx] |= u1Flag

#define ISIS_DECN_MARK_CHG(pSPTNode, u1Flag) \
            pSPTNode->au1MaxPaths[0] |= u1Flag

#define ISIS_DECN_GET_MARK(pSPTNode) \
            (pSPTNode->au1MaxPaths[0] & ISIS_SPT_ECM_FLAG) 

#define ISIS_DECN_RESET_MARK(pSPTNode) \
            (pSPTNode->au1MaxPaths[0] &= ISIS_SPT_ECM_RESET_FLAG)

#define ISIS_RL_L1_UP                  0x08
#define ISIS_RL_L2_UP                  0x04
#define ISIS_RL_L1_DOWN                0x01
#define ISIS_RL_L2_DOWN                0x02

#define SPT_GET_UPDOWN_FLAG(node)            ((node)->u1UpOrDownType & (UINT1)0x0f)
#define SPT_SET_UPDOWN_FLAG(node, val)     ((node)->u1UpOrDownType =(UINT1) ((UINT1)(0x0f & val) + ((node)->u1UpOrDownType& (UINT1)0xf0)))

#define  ISIS_SET_UPDOWN_TYPE(u1Byte, Type) \
         (Type == ISIS_RL_L1_DOWN) ? (u1Byte |= 0x80) :\
          (u1Byte)

#define  ISIS_GET_UPDOWN_TYPE(u1Byte) \
         ((u1Byte & 0x80) == 0x80 ) ? ISIS_RL_L1_DOWN:\
             ISIS_RL_L1_UP


            
            
/* Maximum Generation count extraction macro
 */

#define ISIS_MAX_GEN_COUNT(pContext,u1Level) \
        ((u1Level == ISIS_LEVEL1) ? (pContext->SysActuals.u2SysMaxLSPGenInt / \
                                    pContext->SysActuals.u2SysMinL1LSPGenInt):\
                                   (pContext->SysActuals.u2SysMaxLSPGenInt / \
                                    pContext->SysActuals.u2SysMinL2LSPGenInt))
/* Macros to set the Attached Status in 
 * Self LSPs
 */

#define ISIS_GET_ATT_BIT(pContext, u1Type) \
         ((u1Type == ISIS_DEF_MET) \
          ? ((pContext->SelfLSP.u1L1LSPFlags & ISIS_FLG_ATT_DEF)) \
          : ((u1Type == ISIS_DEL_MET)  \
            ? ((pContext->SelfLSP.u1L1LSPFlags &  ISIS_FLG_ATT_DEL)) \
            : ((u1Type == ISIS_ERR_MET) \
              ? ((pContext->SelfLSP.u1L1LSPFlags & ISIS_FLG_ATT_ERR))\
              : ((pContext->SelfLSP.u1L1LSPFlags & ISIS_FLG_ATT_EXP)))))

#define ISIS_RESET_ATT_BIT(pContext, u1Type)\
         ((u1Type == ISIS_DEF_MET) \
          ? (pContext->SelfLSP.u1L1LSPFlags &= ~ISIS_FLG_ATT_DEF) \
          : ((u1Type == ISIS_DEL_MET)  \
            ? (pContext->SelfLSP.u1L1LSPFlags &= ~ISIS_FLG_ATT_DEL) \
            : ((u1Type == ISIS_ERR_MET) \
              ? (pContext->SelfLSP.u1L1LSPFlags &= ~ISIS_FLG_ATT_ERR)\
              : (pContext->SelfLSP.u1L1LSPFlags &= ~ISIS_FLG_ATT_EXP))))

#define ISIS_SET_ATT_BIT(pContext, u1Type) \
         ((u1Type == ISIS_DEF_MET) \
          ? (pContext->SelfLSP.u1L1LSPFlags |= ISIS_FLG_ATT_DEF) \
          : ((u1Type == ISIS_DEL_MET)  \
            ? (pContext->SelfLSP.u1L1LSPFlags |= ISIS_FLG_ATT_DEL) \
            : ((u1Type == ISIS_ERR_MET) \
              ? (pContext->SelfLSP.u1L1LSPFlags |= ISIS_FLG_ATT_ERR)\
              : (pContext->SelfLSP.u1L1LSPFlags |= ISIS_FLG_ATT_EXP))))

/* Macro to Get the Circuit Level
 */

#define ISIS_GET_P2P_CKT_LEVEL(pCktRec) \
        ((pCktRec->u1CktLevel == ISIS_LEVEL2) ? (pCktRec->pL2CktInfo)\
                                              : (pCktRec->pL1CktInfo))

#define ISIS_GET_ADJ_STATE(pAdjRec) \
  (((pAdjRec->pCktRec->u1CktType == ISIS_BC_CKT) || \
       ((pAdjRec->pCktRec->u1CktType == ISIS_P2P_CKT) && \
     (pAdjRec->pCktRec->u1P2PDynHshkMachanism == ISIS_P2P_TWO_WAY))) ? pAdjRec->u1AdjState \
                   : pAdjRec->u1P2PThreewayState)


/* Macro to check for the Circuit entry status. If Fault Tolerance is supported,
 * then ISIS can process protocol packets only if FT state is ACTIVE and ISIS is
 * Operationally UP. If Fault Tolerance is not supported, then ISIS Operational
 * state alone is required to be UP. These conditions hold for both Reception as
 * well as transmission of Data
 */

#ifndef ISIS_FT_ENABLED

#define ISIS_CONTEXT_ACTIVE(pContext) \
        ((pContext->u1OperState == ISIS_UP) ? ISIS_TRUE\
                                            : ISIS_FALSE)
#define ISIS_IS_SYSTEM_ACTIVE() ISIS_TRUE
#endif


/* Macros accessing the Global Hash Table of Circuits
 */

#define ISIS_INS_CKT_IN_HASHTBL(pCktEntry, u1HashIdx) \
  if (u1HashIdx < ISIS_MAX_BUCKETS)\
  {\
         if (gIsisCktHashTable.apHashBkts [u1HashIdx] == NULL) \
         {\
            gIsisCktHashTable.apHashBkts [u1HashIdx] = pCktEntry; \
            pCktEntry->pNextHashEntry = NULL;\
         }\
         else \
         { \
            pCktEntry->pNextHashEntry = \
                        gIsisCktHashTable.apHashBkts [u1HashIdx];\
            pCktEntry->pPrevHashEntry = NULL;\
            gIsisCktHashTable.apHashBkts [u1HashIdx] = pCktEntry;\
            pCktEntry->pNextHashEntry->pPrevHashEntry = pCktEntry;\
         } \
         }

#define ISIS_DEL_CKT_IN_HASHTBL(pCktEntry, u1HashIdx) \
 if (u1HashIdx < ISIS_MAX_BUCKETS)\
 {\
        if ((pCktEntry->pPrevHashEntry == NULL)\
           && (pCktEntry->pNextHashEntry == NULL))    \
        {\
            /* The only node in the chain */\
         \
            gIsisCktHashTable.apHashBkts [u1HashIdx] = NULL;\
        }\
        else if ((pCktEntry->pNextHashEntry != NULL)        \
                && (pCktEntry->pPrevHashEntry == NULL))\
        {\
            /* Very first node in the chain */\
         \
            gIsisCktHashTable.apHashBkts [u1HashIdx] = \
                            pCktEntry->pNextHashEntry;\
            pCktEntry->pNextHashEntry->pPrevHashEntry = NULL;\
            pCktEntry->pPrevHashEntry = NULL;\
            pCktEntry->pNextHashEntry = NULL;\
        }\
        else if ((pCktEntry->pPrevHashEntry != NULL)  \
                && (pCktEntry->pNextHashEntry == NULL))\
       {\
            /* Last node in the chain */\
            \
            pCktEntry->pPrevHashEntry->pNextHashEntry = NULL;\
            pCktEntry->pPrevHashEntry = NULL;\
            pCktEntry->pNextHashEntry = NULL;\
        }\
        else\
        {\
            /* Node somewhere in the middle of the chain */\
            \
            pCktEntry->pPrevHashEntry->pNextHashEntry = \
                pCktEntry->pNextHashEntry;\
            pCktEntry->pNextHashEntry->pPrevHashEntry = \
                pCktEntry->pPrevHashEntry;\
            pCktEntry->pPrevHashEntry = NULL;\
            pCktEntry->pNextHashEntry = NULL;\
        }\
        }

#define ISIS_SET_CKT_HASH_SIZE(u1Size)\
        gIsisCktHashTable.u1HashSize = u1Size

#define ISIS_GET_CKT_HASH_SIZE()\
        gIsisCktHashTable.u1HashSize 

#define ISIS_SET_SELF_HSTNME()\
    {\
        MEMSET (&gau1IsisHstNme, 0, ISIS_HSTNME_LEN);\
        STRCPY (&gau1IsisHstNme, CLI_GET_ISS_SWITCH_NAME ());\
    }
#define ISIS_SET_HSTNME(pHstNme)\
    {\
        MEMSET (&gau1IsisHstNme, 0, ISIS_HSTNME_LEN);\
        MEMCPY (&gau1IsisHstNme, pHstNme, STRLEN (pHstNme));\
    }
#define ISIS_GET_HSTNME(pHstNme)\
{\
   MEMSET (pHstNme, 0, ISIS_HSTNME_LEN);\
   MEMCPY (pHstNme, gau1IsisHstNme, STRLEN (gau1IsisHstNme));\
}
/*Macro to get Host name for the SYSTEM ID*/
#define ISIS_HST_FRMSYSID(pu1SysID, pu1HostNme,pContext)\
{\
   if(pContext->u1IsisDynHostNmeSupport == ISIS_DYNHOSTNME_ENABLE)\
   {\
       IsisGetHostNme(pContext,pu1SysID,pu1HostNme);\
   }\
}

#define ISIS_HSTNME_TLV_LEN   STRLEN (gau1IsisHstNme)

/* Macros to access global information pertaining to the external modules
 */

#define ISIS_SET_SELF_NODE_ID(u4Id) \
        gIsisExtInfo.u4SelfNodeID = u4Id

#define ISIS_GET_NODE_TYPE() \
        gIsisExtInfo.u1NodeType

#ifndef ISIS_FT_ENABLED

#define ISIS_EXT_SET_FT_SELF_NODEID(u4NodeID)
#define ISIS_EXT_SET_FT_STATE(u1State)
#define ISIS_EXT_SET_FT_STATUS(u1Status)
#define ISIS_EXT_SET_BLK_UPD_STATUS(u4Status)
#define ISIS_EXT_SET_FT_BUF_SIZE(u2BufSize)
#define ISIS_EXT_SET_FT_PEER_NODEID(u4NodeID)
#define ISIS_EXT_SET_MODULEID(u2ModuleID)
#define ISIS_EXT_IS_FT_STATE()
#define ISIS_EXT_IS_FT_STATUS()
#define ISIS_EXT_IS_BLK_UPD_STATUS()
#define ISIS_EXT_SELF_NODE_ID()
#define ISIS_EXT_PEER_NODE_ID()
#define ISIS_EXT_FT_BUF_SIZE()
#define ISIS_EXT_MOD_ID()
#define ISIS_EXT_PROTO_ID()
#define ISIS_EXT_REG_ID()

#endif

/* Macros to access global information pertaining to the forward Context
 */


/* Macros to update the Statistics
 */

#define ISIS_INCR_SYS_DROPPED_PDUS(pContext) \
        pContext->SysStats.u4SysDroppedPDUs++

#define ISIS_INCR_SYS_MAA_DROPPED(pContext) \
        pContext->SysStats.u4SysMAADropFromAreas++

#define ISIS_INCR_OWN_LSP_PURGE(pContext, u1Level)   \
        ((u1Level == ISIS_LEVEL1)\
          ? pContext->SysL1Stats.u4SysOwnLSPPurges++ \
          : pContext->SysL2Stats.u4SysOwnLSPPurges++)

#define ISIS_INCR_MAX_SEQ_EXCEED(pContext, u1Level)  \
        (u1Level == ISIS_LEVEL1)\
        ? (pContext->SysL1Stats.u4SysAttemptToExcdMaxSeqNo++)\
        : (pContext->SysL2Stats.u4SysAttemptToExcdMaxSeqNo++)

#define ISIS_INCR_RCVD_LSP(pCktRec, u1Level) \
         ((u1Level == ISIS_LEVEL1)\
           ? pCktRec->pL1CktInfo->RcvdPktStats.u4LinkStatePDUs++ \
           : pCktRec->pL2CktInfo->RcvdPktStats.u4LinkStatePDUs++)  

#define ISIS_INCR_SENT_LSP(pCktRec, u1Level) \
        ((u1Level == ISIS_LEVEL1)\
          ? pCktRec->pL1CktInfo->SentPktStats.u4LinkStatePDUs++ \
          : pCktRec->pL2CktInfo->SentPktStats.u4LinkStatePDUs++)  

#define ISIS_INCR_RCVD_CSNP(pCktRec, u1Level) \
        ((u1Level == ISIS_LEVEL1)\
          ? pCktRec->pL1CktInfo->RcvdPktStats.u4CSNPDUs++ \
          : pCktRec->pL2CktInfo->RcvdPktStats.u4CSNPDUs++)  

#define ISIS_INCR_SENT_CSNP(pCktRec, u1Level) \
        ((u1Level == ISIS_LEVEL1)\
          ? pCktRec->pL1CktInfo->SentPktStats.u4CSNPDUs++ \
          : pCktRec->pL2CktInfo->SentPktStats.u4CSNPDUs++)  

#define ISIS_INCR_RCVD_PSNP(pCktRec, u1Level) \
        ((u1Level == ISIS_LEVEL1) \
          ? pCktRec->pL1CktInfo->RcvdPktStats.u4PSNPDUs++ \
          : pCktRec->pL2CktInfo->RcvdPktStats.u4PSNPDUs++)  

#define ISIS_INCR_SENT_PSNP(pCktRec, u1Level) \
        ((u1Level == ISIS_LEVEL1)\
          ? pCktRec->pL1CktInfo->SentPktStats.u4PSNPDUs++ \
          : pCktRec->pL2CktInfo->SentPktStats.u4PSNPDUs++)  
         
#define ISIS_INCR_SENT_HELLO(pCktLevel) \
        pCktLevel->SentPktStats.u4HelloPDUs++  

#define ISIS_INCR_RCVD_HELLO(pCktLevel) \
        pCktLevel->RcvdPktStats.u4HelloPDUs++  

#define ISIS_INCR_RCVD_ISH(pCktRec) pCktRec->CktStats.u4RxISHPDUs++;

#define ISIS_INCR_TXED_ISH(pCktRec ) pCktRec->CktStats.u4TxISHPDUs++;

#define ISIS_INCR_SYS_AUTH_FAILS(pContext, u1Level) \
        ((u1Level == ISIS_LEVEL1) ?\
         pContext->SysL1Stats.u4SysAuthFails++ :\
         pContext->SysL2Stats.u4SysAuthFails++)

#define ISIS_INCR_ID_LEN_MISMATCH_STATS(pContext, u1Level) \
        ((u1Level == ISIS_LEVEL1) ?\
         pContext->SysL1Stats.u4SysIDFieldLenMismatches++ :\
         pContext->SysL2Stats.u4SysIDFieldLenMismatches++)

#define ISIS_INCR_CIRC_ID_LEN_MISMATCH_STATS(pCktRec) \
        pCktRec->CktStats.u4CktIdLenMsmtchs++

#define ISIS_INCR_L1DBOL_STAT(pContext, u1Level) \
        pContext->SysL1Stats.u4SysLSPDBOLs++

#define ISIS_INCR_L2DBOL_STAT(pContext) \
        pContext->SysL2Stats.u4SysLSPDBOLs++
        
#define ISIS_INCR_SEQNO_SKIP_STAT(pContext, u1Level) \
        (u1Level == ISIS_LEVEL1)\
         ?(pContext->SysL1Stats.u4SysSeqNumSkips++)\
         :(pContext->SysL2Stats.u4SysSeqNumSkips++)

#define ISIS_INCR_AA_MISMATCH_STAT(pContext)\
        pContext->SysStats.u4SysAreaMismatches++

#define ISIS_INCR_MAA_MISMATCH_STAT(pContext)\
        (pContext->SysStats.u4SysMaxAAMismatches)++\
   
#define ISIS_INCR_WRONG_SYS_STAT(x)

#define ISIS_INCR_DIS_CHG_STAT(x)\
        (x->u4CktLanDISChgs)++

#define ISIS_INCR_CKT_INIT_FAIL_STAT(x)\
        (x->CktStats.u4CktInitFails)++

#define ISIS_INCR_ADJ_CHG_STAT(x)\
        (x->CktStats.u4CktAdjChgs)++

#define ISIS_INCR_REJ_ADJ_STAT(x)\
        (x->CktStats.u4CktRejAdj)++

#define ISIS_INCR_CKT_MET_CHG_STAT(x)\
        (x->CktStats.u4CktMetChgs)++

#define ISIS_INCR_OUT_CTRL_PDU_STAT(x)\
        (x->CktStats.u4CktOutCtrlPDUs)++
        
#define ISIS_INCR_IN_CTRL_PDU_STAT(x)\
        (x->CktStats.u4CktInCtrlPDUs)++

/* Macro to get and insert the System Context in the Global Instance Table
 */

#define ISIS_GET_CONTEXT() gpIsisInstances

#define ISIS_PUT_CONTEXT(pContext) gpIsisInstances = pContext


#define ISIS_LOG_EVT_STR_LEN\
        ISIS_MAX (ISIS_MAX_PASSWORD_LEN,  \
                  (3 * (ISIS_MAX (ISIS_LSPID_LEN,\
                                  ISIS_SNPA_ADDR_LEN)) + 1))

#define ISIS_GET_ROUTE_TAG(u1Level)\
        ((u1Level == 0x01) ? "IL1" :\
         ((u1Level == 0x02) ? "IL2" :\
          ((u1Level == 0x03) ? "IIA" : "I*")))

#define ISIS_GET_LEVEL_STR(u1Level)\
        ((u1Level == 0x01) ? "ISIS_LEVEL1" :\
         ((u1Level == 0x02) ? "ISIS_LEVEL2" : "ISIS_UNKNOWN_LEVEL"))

#define ISIS_GET_SYNC_CMD_STR(u1Cmd)\
       ((u1Cmd == ISIS_CMD_ADD)? "ADD" :\
        ((u1Cmd == ISIS_CMD_DELETE)? "DELETE" :\
         ((u1Cmd == ISIS_CMD_MODIFY)? "MODIFY" :\
          "UNKNOWN")))

#define ISIS_GET_DECNC_EVT_STR(u1Event)\
        ((u1Event == ISIS_EVT_L1_DECN_COMPLETE) ? "L1_COMPLETE" :\
        ((u1Event == ISIS_EVT_L2_DECN_COMPLETE) ? "L2_COMPLETE" :\
                                                  "UNKNOWN_EVT"))
#define ISIS_GET_LSPCMP_CODE_STR(i4Code)\
        ((i4Code == ISIS_NEW_LSP) ? "NEW LSP" :\
        ((i4Code == ISIS_DIFF_CS_GRTR_SEQNO) ? "DIFF_CS_GRTR_SEQNO" :\
        ((i4Code == ISIS_SAME_CS_GRTR_SEQNO) ? "SAME_CS_GRTR_SEQNO" :\
        ((i4Code == ISIS_SAME_CS_LESS_SEQNO) ? "SAME_CS_LESS_SEQNO" :\
        ((i4Code == ISIS_DIFF_CS_LESS_SEQNO) ? "DIFF_CS_LESS_SEQNO" :\
        ((i4Code == ISIS_SAME_CS_SAME_SEQNO) ? "SAME_CS_SAME_SEQNO" :\
        ((i4Code == ISIS_DIFF_CS_SAME_SEQNO) ? "DIFF_CS_SAME_SEQNO" :\
                        "UNKNOWN_CODE")))))))

#define ISIS_GET_SYS_TYPE_STR(u1SysType)\
        ((u1SysType == 0x01) ? "ISIS_LEVEL1" :\
         ((u1SysType == 0x02) ? "ISIS_LEVEL2" :\
         ((u1SysType == 0x03) ? "ISIS_LEVEL12" :\
         ((u1SysType == 0x04) ? "UNKNOWN_SYSTEM_TYPE" : "ISIS_IS"))))

#define ISIS_GET_PDU_TYPE_STR(u1PDUType)\
        ((u1PDUType == 0x04) ? "ISIS_ISH_PDU" :\
         ((u1PDUType == 0x0F) ? "ISIS_L1LAN_IIH_PDU" :\
         ((u1PDUType == 0x10) ? "ISIS_L2LAN_IIH_PDU" :\
         ((u1PDUType == 0x11) ? "ISIS_P2P_IIH_PDU" :\
         ((u1PDUType == 0x12) ? "ISIS_LEVEL1_LSP" :\
         ((u1PDUType == 0x14) ? "ISIS_LEVEL2_LSP" :\
         ((u1PDUType == 0x18) ? "ISIS_LEVEL1_CSNP" :\
         ((u1PDUType == 0x19) ? "ISIS_LEVEL2_CSNP" :\
         ((u1PDUType == 0x1A) ? "ISIS_LEVEL1_PSNP" :\
         ((u1PDUType == 0x1B) ? "ISIS_LEVEL2_PSNP" : "UNKNOWN_PDU"))))))))))

#define ISIS_GET_PDU_TYPE_STR_FOR_TRACE(u1PDUType)\
        ((u1PDUType == 0x04) ? "ISH Hello" :\
         ((u1PDUType == 0x0F) ? "L1 IIH Hello" :\
         ((u1PDUType == 0x10) ? "L2 IIH Hello" :\
         ((u1PDUType == 0x11) ? "P2P IIH Hello" :\
         ((u1PDUType == 0x12) ? "L1 LSP" :\
         ((u1PDUType == 0x14) ? "L2 LSP" :\
         ((u1PDUType == 0x18) ? "L1CSNP" :\
         ((u1PDUType == 0x19) ? "L2 CSNP" :\
         ((u1PDUType == 0x1A) ? "L1 PSNP" :\
         ((u1PDUType == 0x1B) ? "L2 PSNP" : "UNKNOWN_PDU"))))))))))

#define ISIS_GET_TIMER_TYPE_STR(TimerId)\
        ((TimerId == 0x00) ? "ISIS_EQUV_CLSS_TIMER" :\
         ((TimerId == 0x01) ? "ISIS_L1LSP_GEN_TIMER" :\
         ((TimerId == 0x02) ? "ISIS_L2LSP_GEN_TIMER" :\
         ((TimerId == 0x03) ? "ISIS_SPF_SCHDL_TIMER" :\
         ((TimerId == 0x04) ? "ISIS_L1WAITING_TIMER" :\
         ((TimerId == 0x05) ? "ISIS_L2WAITING_TIMER" :\
         ((TimerId == 0x06) ? "ISIS_DBS_NORMN_TIMER" :\
         ((TimerId == 0x07) ? "ISIS_SEQ_REINIT_TIMER" :\
         ((TimerId == 0x08) ? "ISIS_INACTIVE_TIMER" \
                            : "ISIS_TMR_UNKNOWN")))))))))

#define ISIS_GET_ADJ_USAGE_STR(u1AdjUsage)\
        ((u1AdjUsage == 0x00) ? "ISIS_ADJ_UNKNOWN" :\
         ((u1AdjUsage == 0x01) ? "ISIS_ADJ_LEVEL1" :\
         ((u1AdjUsage == 0x02) ? "ISIS_ADJ_LEVEL2" :\
         ((u1AdjUsage == 0x03) ? "ISIS_ADJ_LEVEL12" : "ISIS_ADJ_ERROR"))))

#define ISIS_GET_CKT_LVL_STR(u1CktLvl)\
        ((u1CktLvl == 0x01) ? "ISIS_CKT_LEVEL1" :\
         ((u1CktLvl == 0x02) ? "ISIS_CKT_LEVEL2" :\
         ((u1CktLvl == 0x03) ? "ISIS_CKT_LEVEL12" : "ISIS_CKT_ERR")))

#define ISIS_GET_CKT_TYP_STR(u1CktTyp)\
        ((u1CktTyp == 0x01) ? "ISIS_CKT_BC" :\
         ((u1CktTyp == 0x02) ? "ISIS_CKT_P2P" :\
         ((u1CktTyp == 0x03) ? "ISIS_CKT_ST_IN" :\
         ((u1CktTyp == 0x04) ? "ISIS_CKT_ST_OUT" : "ISIS_CKT_DA"))))

#define ISIS_GET_LSP_COMP_STATUS_STR(Status)\
        ((Status == 0x01) ? "ISIS_NEW_LSP" :\
         ((Status == 0x02) ? "ISIS_DIFF_CS_GRTR_SEQNO" :\
         ((Status == 0x03) ? "ISIS_SAME_CS_GRTR_SEQNO" :\
         ((Status == 0x04) ? "ISIS_SAME_CS_LESS_SEQNO" :\
         ((Status == 0x05) ? "ISIS_DIFF_CS_LESS_SEQNO" :\
         ((Status == 0x06) ? "ISIS_SAME_CS_SAME_SEQNO" :\
                               "ISIS_DIFF_CS_SAME_SEQNO"))))))

#define ISIS_GET_METRIC_TYPE_STR(pContext, u1MetType)\
        ((u1MetType == 0x00) ? ((pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) ? "WIDE_MET" : "DEFAULT_MET"):\
         ((u1MetType == 0x01) ? "DELAY_MET" :\
         ((u1MetType == 0x02) ? "EXPENSE_MET" :\
         ((u1MetType == 0x03) ? "ERROR_MET" :\
                               "UNKNOWN_MET"))))
#define ISIS_GET_ISIS_CKT_STATUS(u1Status)\
        ((u1Status == 0x00) ? "DOWN" :\
         ((u1Status == 0x01) ? "UP" :\
         ((u1Status == 0x02) ? "DESTROY" :\
         ((u1Status == 0x03) ? "MODIFY" :\
         ((u1Status == 0x04) ? "IP_IF_ADD" :\
         ((u1Status == 0x05) ? "IP_IF_DEL" :\
         ((u1Status == 0x07) ? "RESTART" :\
                              "NO_EVT")))))))

#define ISIS_GET_ISIS_ADJ_STATUS(u1Status)\
        ((u1Status == 0x00) ? "UP" :\
         ((u1Status == 0x01) ? "INIT" :\
         ((u1Status == 0x02) ? "DOWN" :\
         ((u1Status == 0x03) ? "FAIL" :\
                               "UNKNOWN"))))   
         
#define ISIS_GET_PROT_SUPP(u1ProtSupp)\
        ((u1ProtSupp == 142) ? "IPv4" :\
          ((u1ProtSupp == 204) ? "IPv6" :\
                               "INVALID"))

#define ISIS_GET_SELF_LSP_TYPE(u1LSPType)\
        ((u1LSPType == 0x01) ? "L1_NSNLSP" :\
         ((u1LSPType == 0x02) ? "L2_NSNLSP" :\
         ((u1LSPType == 0x03) ? "L1_SNLSP" :\
         ((u1LSPType == 0x04) ? "L2_SNLSP" :\
                               "INVALID"))))

#define ISIS_GET_SYS_EVT_NAME(u1Event)\
        ((u1Event == 0x01) ? "ISIS_SYS_FT_DATA_EVT" :\
         ((u1Event == 0x02) ? "ISIS_SYS_LL_DATA_EVT" :\
         ((u1Event == 0x04) ? "ISIS_SYS_TMR_EVT" :\
         ((u1Event == 0x08) ? "ISIS_SYS_RT_DATA_EVT" :\
         ((u1Event == 0x10) ? "ISIS_SYS_INT_EVT" :\
         ((u1Event == 0x20) ? "ISIS_SYS_FWD_EVT" :\
         ((u1Event == 0x40) ? "ISIS_SYS_RT6_DATA_EVT" :\
         ((u1Event == 0x100) ? "ISIS_RM_FSW_EVT" :\
         ((u1Event == 0x80) ? "ISIS_SYS_ROUTE_DATA_EVT" :\
         ((u1Event == 0x200) ? "ISIS_SYS_BFD_STATE_EVT" :\
         ((u1Event == 0x800) ? "ISIS_HELLO_TMR_EVT" :\
         ((u1Event == 0x1000) ? "ISIS_HELLO_PKT_ARRV_EVT" :\
         ((u1Event == 0x2000) ? "ISIS_SPF_RELQ_EVT" :\
                              "UNKNOWN")))))))))))))

#define ISIS_GET_LSP_STR(u1LSPType)\
      ((u1LSPType == ISIS_L1_NON_PSEUDO_LSP)? "L1_NON_PSEUDO_LSP" :\
       ((u1LSPType == ISIS_L2_NON_PSEUDO_LSP)? "L2_NON_PSEUDO_LSP" :\
       ((u1LSPType == ISIS_L1_PSEUDO_LSP)? "L1_PSEUDO_LSP" :\
       ((u1LSPType == ISIS_L2_PSEUDO_LSP)? "L2_PSEUDO_LSP" :\
                                           "UNKNOWN_LSP"))))

#define ISIS_GET_TLV_TYPE_STR(u1TlvType)\
        ((u1TlvType == 1) ? "AREA_ADDR_TLV" :\
         ((u1TlvType == 2) ? "IS_ADJ_TLV" :\
         ((u1TlvType == 6) ? "IS_NEIGH_ADDR_TLV" :\
         ((u1TlvType == 8) ? "PADDING_TLV" :\
         ((u1TlvType == 9) ? "LSP_ENTRY_TLV" :\
         ((u1TlvType == 10) ? "AUTH_INFO_TLV" :\
         ((u1TlvType == 14) ? "LSP_BUF_SIZE_TLV" :\
         ((u1TlvType == 128) ? "IP_INTERNAL_RA_TLV" :\
         ((u1TlvType == 129) ? "PROT_SUPPORT_TLV" :\
         ((u1TlvType == 130) ? "IP_EXTERNAL_RA_TLV" :\
         ((u1TlvType == 236) ? "IPV6_RA_TLV" :\
         ((u1TlvType == 131) ? "IDRP_INFO_TLV" :\
         ((u1TlvType == 132) ? "IPV4IF_ADDR_TLV" :\
         ((u1TlvType == 232) ? "IPV6IF_ADDR_TLV" :\
         ((u1TlvType == 229) ? "MT_TLV" :\
         ((u1TlvType == 22) ? "EXT_IS_REACH_TLV" :\
         ((u1TlvType == 135) ? "EXT_IP_REACH_TLV" :\
         ((u1TlvType == 237) ? "MT_IPV6_REACH_TLV" :\
         ((u1TlvType == 222) ? "MT_IS_REACH_TLV" :\
         ((u1TlvType == 240) ? "P2P_THREE_WAY_ADJ_TLV" :\
         ((u1TlvType == 148) ? "BFD_ENABLED_TLV" :\
         ((u1TlvType == 137) ? "DYN_HOSTNME_TLV" :\
                              "UNKNOWN"))))))))))))))))))))))

#define ISIS_GET_CIRC_STATE_STR(u1State)\
        ((u1State == 0x00) ? "DOWN" :\
        ((u1State == 0x01) ? "UP" :\
        ((u1State == 0x02) ? "DESTROY" :\
        ((u1State == 0x03) ? "MODIFY" :\
        ((u1State == 0x06) ? "NO_EVT" :\
        ((u1State == 0x07) ? "RESTART" :\
                          "UNKNOWN"))))))

#define ISIS_GET_ADDR_TYPE_STR(u1AddrType)\
        ((u1AddrType == 0x01) ? "IPV4" :\
         ((u1AddrType == 0x02) ? "IPV6" :\
                              "UNKNOWN"))

#define ISIS_GET_RRD_PROTO_STR(u1Protocol)\
        ((u1Protocol == 0x02) ? "CONNECTED" :\
         ((u1Protocol == 0x03) ? "STATIC" :\
         ((u1Protocol == 0x08) ? "RIP" :\
         ((u1Protocol == 0x0d) ? "OSPF" :\
         ((u1Protocol == 0x0e) ? "BGP" :\
                            "UNKNOWN")))))

#define ISIS_GET_RRD_RTM_IMPORTMASK_STR(u2BitMask)\
         (((u2BitMask & ISIS_IMPORT_ALL) == ISIS_IMPORT_ALL) ? "ALL" :\
         (((u2BitMask & ISIS_IMPORT_DIRECT) == ISIS_IMPORT_DIRECT) ? "CONNECTED" :\
         (((u2BitMask & ISIS_IMPORT_STATIC) == ISIS_IMPORT_STATIC) ? "STATIC" :\
         (((u2BitMask & ISIS_IMPORT_RIP) == ISIS_IMPORT_RIP) ? "RIP" :\
         (((u2BitMask & ISIS_IMPORT_OSPF) == ISIS_IMPORT_OSPF) ? "OSPF" :\
         (((u2BitMask & ISIS_IMPORT_BGP) == ISIS_IMPORT_BGP) ? "BGP" :\
                                        "UNKNOWN"))))))
#define ISIS_GET_RRD_MSG_TYPE_STR(u1MsgType)\
        ((u1MsgType == RTM_REDISTRIBUTE_ENABLE_MESSAGE) ? "REGISTER" :\
        ((u1MsgType == RTM_REDISTRIBUTE_DISABLE_MESSAGE) ? "DEREGISTER" :\
                                        "UNKNOWN"))
#define ISIS_GET_FLTR_TYPE_STR(u1Type)\
       ((u1Type ==0x01 ) ? "ISIS_DB_LSPS" :\
        ((u1Type ==0x02 ) ? "ISIS_DB_CKTS" :\
        ((u1Type ==0x03 ) ? "ISIS_DB_CKTL" :\
        ((u1Type ==0x04 ) ? "ISIS_DB_ADJN" :\
        ((u1Type ==0x05 ) ? "ISIS_DB_ADAD" :\
        ((u1Type ==0x06 ) ? "ISIS_DB_IPRA" :\
        ((u1Type ==0x07 ) ? "ISIS_DB_MAAD" :\
        ((u1Type ==0x08 ) ? "ISIS_DB_SAAD" :\
        ((u1Type ==0x09 ) ? "ISIS_DB_IPIF" :\
        ((u1Type ==0x10 ) ? "ISIS_DB_CTXT" :\
        ((u1Type ==0x12 ) ? "ISIS_DB_CONF" :\
        ((u1Type ==0x13 ) ? "ISIS_DB_ACTS" :\
        ((u1Type ==0x14 ) ? "ISIS_DB_PRSU" :\
        ((u1Type ==0x15 ) ? "ISIS_DB_MCFG" :\
        ((u1Type ==0x15 ) ? "ISIS_IS_STAT" :\
        ((u1Type ==0x16 ) ? "ISIS_SD_DECN" :\
                            "UNKNOWN"))))))))))))))))

#define ISIS_GET_EC_TIMER_TYPE(u1Type)\
        ((u1Type == 0x01) ? "ECT_P2P_ISH" :\
         ((u1Type == 0x02) ? "ECT_P2P_HELLO" :\
         ((u1Type == 0x03) ? "ECT_L1_LAN_HELLO" :\
         ((u1Type == 0x04) ? "ECT_L2_LAN_HELLO" :\
         ((u1Type == 0x05) ? "ECT_HOLDING" :\
         ((u1Type == 0x06) ? "ECT_L1_LSP_TX" :\
         ((u1Type == 0x07) ? "ECT_L2_LSP_TX" :\
         ((u1Type == 0x08) ? "ECT_L1_CSNP" :\
         ((u1Type == 0x09) ? "ECT_L2_CSNP" :\
         ((u1Type == 0x0A) ? "ECT_L1_PSNP" :\
         ((u1Type == 0x0B) ? "ECT_L2_PSNP" :\
         ((u1Type == 0x0C) ? "ECT_ZERO_AGE" :\
         ((u1Type == 0x0D) ? "ECT_MAX_AGE" :\
                             "UNKNOWN")))))))))))))

#define ISIS_GET_DIS_STATE(u1DisState) \
        ((u1DisState == ISIS_DIS_ELECTED) ? "DIS Elected" : "DIS Resigned") 

#define ISIS_GET_MPS_VAL_STR(u1MPS)\
        ((u1MPS > 0x01) ? "[ECMP]" : "")

#define ISIS_GET_EVT_STR(u1Event)\
        IsisCtrlGetEventStr (u1Event)

/* LOG Macros used for printing debugging information
 */

#define ISIS_DBG_GET_PS_IDX(x) (((x) == 0xCC) ? 2 : (((x) == 0xCD) ? 3 :\
        ((x) == 129) ? 1 : 0))

#if (ADJ_LOG == ISIS_LOG_ENABLE) 
#define ISIS_PRINT_ID(pu1Buf, u1Len, pStr, u1Fmt)\
        if (gau1IsisModLevelMask [ISIS_ADJ_MODULE] & ISIS_PT_MASK){\
                   IsisDbgPrintTime(ISIS_LGTM);\
                   IsisDbgPrintOctets (pu1Buf, u1Len, (UINT1 *)pStr, u1Fmt);}\


#else

#define ISIS_PRINT_ID(pu1Buf, u1Len, pStr, u1Fmt)

#endif

#if (UPD_LOG == ISIS_LOG_ENABLE)
#define ISIS_UPD_PRINT_ID(pu1Buf, u1Len, pStr, u1Fmt)\
         if (gau1IsisModLevelMask [ISIS_UPD_MODULE] & ISIS_PT_MASK){\
                   IsisDbgPrintOctetString (pu1Buf, u1Len, (UINT1 *)pStr, u1Fmt);}\

#else

#define ISIS_UPD_PRINT_ID(pu1Buf, u1Len, pStr, u1Fmt)

#endif

#if (DBG_LOG == ISIS_LOG_ENABLE)

#define ISIS_DBG_PRINT_ID(pu1Buf, u1Len, pStr, u1Fmt)\
        IsisDbgPrintOctetString (pu1Buf, u1Len, (UINT1 *)pStr, u1Fmt)

#else

#define ISIS_DBG_PRINT_ID(pu1Buf, u1Len, pStr, u1Fmt)

#endif
        

#if (DBG_LOG == ISIS_LOG_ENABLE)

#define ISIS_DBG_PRINT_ADDR(pu1Buf, u1Len, pStr, u1Fmt)\
        IsisDbgPrintOctetString (pu1Buf, u1Len, (UINT1 *)pStr, u1Fmt)

#define ISIS_DBG_PRINT_AUTH_INFO(pu1Buf, u1Len, pStr, u1Fmt)\
        IsisDbgPrintOctetString (pu1Buf, u1Len, (UINT1 *)pStr, u1Fmt)
#else

#define ISIS_DBG_PRINT_ADDR(pu1Buf, u1Len, pStr, u1Fmt)

#define ISIS_DBG_PRINT_AUTH_INFO(pu1Buf, u1Len, pStr, u1Fmt)

#endif


#define ISIS_DEBUG_SNPRINTF(pStr)\
        IsisDbgPrintTime(ISIS_LGTM);\
        SNPRINTF pStr;\
        STRNCAT (ISIS_LGTM, ISIS_LGST, ISIS_MAX_LOG_SIZE);\
        UtlTrcPrint (ISIS_LGTM);\
        MEMSET (ISIS_LGST,0,ISIS_MAX_LOG_SIZE);\
        MEMSET (ISIS_LGTM,0,ISIS_MAX_LOG_TIME_SIZE);



#define ISIS_DEBUG_PRINT(x)\
        IsisDbgPrintTime(ISIS_LGTM);\
        SPRINTF x;\
        STRNCAT (ISIS_LGTM, ISIS_LGST, ISIS_MAX_LOG_SIZE);\
        UtlTrcPrint (ISIS_LGTM);\
        MEMSET (ISIS_LGST,0,ISIS_MAX_LOG_SIZE);\
        MEMSET (ISIS_LGTM,0,ISIS_MAX_LOG_TIME_SIZE);


#define PANIC(x)\
        SPRINTF x;\
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, (UINT4) gi4IsisSysLogId,ISIS_LGST));\
        if (gau1IsisModLevelMask [ISIS_CR_MODULE] & ISIS_ALL_MASK) {\
            ISIS_DEBUG_PRINT (x)\
        }


#define WARNING(x)\
        SPRINTF x;\
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, (UINT4) gi4IsisSysLogId,ISIS_LGST));\
        ISIS_DEBUG_PRINT (x)\


#if (DT_LOG == ISIS_LOG_ENABLE)
#define DETAIL_FAIL(x)\
        if ((gau1IsisModLevelMask [x] & ISIS_PT_MASK) && (gau1IsisModLevelMask [ISIS_DT_MODULE] & ISIS_PT_MASK)) {\
           IsisDbgPrintTime(ISIS_LGTM);\
           SPRINTF (ISIS_LGST,"\t\t Failure - Function [ %s ], Line [ %d ]\n\n", __func__, __LINE__);\
           STRNCAT (ISIS_LGTM, ISIS_LGST, ISIS_MAX_LOG_SIZE);\
           UtlTrcPrint (ISIS_LGTM);\
           MEMSET (ISIS_LGST,0,ISIS_MAX_LOG_SIZE);\
           MEMSET (ISIS_LGTM,0,ISIS_MAX_LOG_TIME_SIZE);\
        }

#else

#define DETAIL_FAIL(x)

#endif

#if (DT_LOG == ISIS_LOG_ENABLE)
#define DETAIL_INFO(x, pStr)\
        if ((gau1IsisModLevelMask [x] & ISIS_PT_MASK) && (gau1IsisModLevelMask [ISIS_DT_MODULE] & ISIS_PT_MASK)) {\
            ISIS_DEBUG_SNPRINTF (pStr)\
        }
        
#else

#define DETAIL_INFO(x, pStr)

#endif


#if (DBG_LOG == ISIS_LOG_ENABLE)

#define DBP_PT(x)\
        if (gau1IsisModLevelMask [ISIS_DBG_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define DBP_PI(x)\
        if (gau1IsisModLevelMask [ISIS_DBG_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define DBP_EE(x)\
        if (gau1IsisModLevelMask [ISIS_DBG_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define DBP_PD(x)\
        if (gau1IsisModLevelMask [ISIS_DBG_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define DBP_TD(x)\
        if (gau1IsisModLevelMask [ISIS_DBG_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define DBP_PT(x)
#define DBP_PI(x)
#define DBP_EE(x)
#define DBP_PD(x)
#define DBP_TD(x)

#endif

#if (ADJ_LOG == ISIS_LOG_ENABLE)

#define ADP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_ADJ_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define ADP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_ADJ_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define ADP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_ADJ_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define ADP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_ADJ_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else 
#define ADP_PT(x)
#define ADP_PI(x)
#define ADP_PD(x)
#define ADP_TD(x)
#endif

#if (EE_LOG == ISIS_LOG_ENABLE)

#define ADP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define UPP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define DEP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define TMP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define FTP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define RTP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define DLP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define TRP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define UTP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define NMP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define CTP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_EE_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#else

#define ADP_EE(x)
#define UPP_EE(x)
#define DEP_EE(x)
#define TMP_EE(x)
#define FTP_EE(x)
#define RTP_EE(x)
#define DLP_EE(x)
#define TRP_EE(x)
#define UTP_EE(x)
#define NMP_EE(x)
#define CTP_EE(x)

#endif

#if (UPD_LOG == ISIS_LOG_ENABLE)

#define UPP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_UPD_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define UPP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_UPD_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define UPP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_UPD_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define UPP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_UPD_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define UPP_PT(x)
#define UPP_PI(x)
#define UPP_PD(x)
#define UPP_TD(x)

#endif



#if (DEC_LOG == ISIS_LOG_ENABLE)

#define DEP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_DEC_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define DEP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_DEC_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define DEP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_DEC_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define DEP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_DEC_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#define ISIS_DEC_PRINT_ID(pu1Buf, u1Len, pStr, u1Fmt)\
        if (gau1IsisModLevelMask [ISIS_DEC_MODULE] & ISIS_PT_MASK) {\
           IsisDbgPrintOctets (pu1Buf, u1Len, (UINT1 *)pStr, u1Fmt);\
        }
#else

#define DEP_PT(x)
#define DEP_PI(x)
#define DEP_PD(x)
#define DEP_TD(x)
#define ISIS_DEC_PRINT_ID(pu1Buf, u1Len, (UINT1 *)pStr, u1Fmt)

#endif

#if (TMR_LOG == ISIS_LOG_ENABLE)

#define TMP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_TMR_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define TMP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_TMR_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define TMP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_TMR_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define TMP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_TMR_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define TMP_PT(x)
#define TMP_PI(x)
#define TMP_PD(x)
#define TMP_TD(x)

#endif

#if (FLT_LOG == ISIS_LOG_ENABLE)

#define FTP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_FLT_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define FTP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_FLT_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define FTP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_FLT_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define FTP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_FLT_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define FTP_PT(x)
#define FTP_PI(x)
#define FTP_PD(x)
#define FTP_TD(x)

#endif

#if (RTM_LOG == ISIS_LOG_ENABLE)

#define RTP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_RTM_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define RTP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_RTM_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define RTP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_RTM_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define RTP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_RTM_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define RTP_PT(x)
#define RTP_PI(x)
#define RTP_PD(x)
#define RTP_TD(x)

#endif

#if (DLL_LOG == ISIS_LOG_ENABLE)

#define DLP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_DLL_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define DLP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_DLL_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define DLP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_DLL_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define DLP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_DLL_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define DLP_PT(x)
#define DLP_PI(x)
#define DLP_PD(x)
#define DLP_TD(x)

#endif

#if (TRF_LOG == ISIS_LOG_ENABLE)

#define TRP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_TRF_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define TRP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_TRF_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define TRP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_TRF_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define TRP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_TRF_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define TRP_PT(x)
#define TRP_PI(x)
#define TRP_PD(x)
#define TRP_TD(x)

#endif

#if (CTL_LOG == ISIS_LOG_ENABLE)

#define CTP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_CTL_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define CTP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_CTL_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define CTP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_CTL_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define CTP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_CTL_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define CTP_PT(x)
#define CTP_PI(x)
#define CTP_PD(x)
#define CTP_TD(x)

#endif

#if (NMG_LOG == ISIS_LOG_ENABLE)

#define NMP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_NMG_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define NMP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_NMG_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define NMP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_NMG_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define NMP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_NMG_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define NMP_PT(x)
#define NMP_PI(x)
#define NMP_PD(x)
#define NMP_TD(x)

#endif

#if (BPC_LOG == ISIS_LOG_ENABLE)

#define BPP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_BPC_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define BPP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_BPC_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define BPP_EE(x) \
        if (gau1IsisModLevelMask [ISIS_BPC_MODULE] & ISIS_EE_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define BPP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_BPC_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define BPP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_BPC_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define BPP_PT(x)
#define BPP_PI(x)
#define BPP_EE(x)
#define BPP_PD(x)
#define BPP_TD(x)

#endif

#if (UTL_LOG == ISIS_LOG_ENABLE)

#define UTP_PT(x) \
        if (gau1IsisModLevelMask [ISIS_UTL_MODULE] & ISIS_PT_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define UTP_PI(x) \
        if (gau1IsisModLevelMask [ISIS_UTL_MODULE] & ISIS_PI_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }


#define UTP_PD(x) \
        if (gau1IsisModLevelMask [ISIS_UTL_MODULE] & ISIS_PD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }

#define UTP_TD(x) \
        if (gau1IsisModLevelMask [ISIS_UTL_MODULE] & ISIS_TD_MASK) {\
           ISIS_DEBUG_PRINT (x)\
        }
#else

#define UTP_PT(x)
#define UTP_PI(x)
#define UTP_PD(x)
#define UTP_TD(x)

#endif

#ifndef ISIS_FT_ENABLED

#define ISIS_FLTR_ADJ_LSU(pContext, u1Cmd, pRec)
#define ISIS_FLTR_MAA_LSU(pContext, u1Cmd, pRec)
#define ISIS_FLTR_IPRA_LSU(pContext, u1Cmd, pIPRARec, u1ExistState)
#define ISIS_FLTR_SA_LSU(pContext, u1Cmd, pIPRARec, u1ExistState)
#define ISIS_FLTR_IPIF_LSU(pContext, u1Cmd, pRec)
#define ISIS_FLTR_ADJ_AA_LSU(pContext, u1Cmd, pAdjRec, pAdjAARec)
#define ISIS_FLTR_CKT_LSU(pContext, u1Cmd, pCktRec, u1ExistState)
#define ISIS_FLTR_CKT_LVL_LSU(pContext, u1Cmd, pCktRec, pCktLvl, u1ExistState)
#define ISIS_FLTR_SYS_CTXT_LSU(pContext, u1Cmd, u1ExistState)
#define ISIS_FLTR_SYS_CFGS_LSU(pContext, u1Cmd)
#define ISIS_FLTR_SYS_ACTS_LSU(pContext, u1Cmd)
#define ISIS_FLTR_LSP_LSU(pContext, u1Cmd, pRec)
#define ISIS_FLTR_PROT_SUPP_LSU(pContext, u1ProtSupp, u1ExistState)
#define ISIS_FLTR_MEM_CFG_LSU()
#define ISIS_FLTR_IS_STAT_LSU(u1Status)
#define ISIS_FLTR_SCH_SPF_LSU(pContext, u1Level)

#endif


#define ISIS_IN6_IS_ADDR_LINKLOCAL(pAddr, Status) \
{ \
    tIp6Addr    Ipv6Addr; \
    MEMCPY((Ipv6Addr.u1_addr), pAddr, 16); \
    if (IN6_IS_ADDR_LINKLOCAL (Ipv6Addr))  { Status = ISIS_TRUE; } \
    else { Status = ISIS_FALSE; } \
}

#define  IN6_IS_ADDR_LINKLOCAL(a)     \
   (((a).u4_addr[0] & CRU_HTONL(0xFFC00000)) == CRU_HTONL(0xFE800000))


/* ISIS  modes of operation */
#define ISIS_SI_MODE         VCM_SI_MODE
#define ISIS_MI_MODE         VCM_MI_MODE


/*Traps Support
 */

#ifdef ISIS_TRAPS

#define ISIS_SEND_TRAP(pTrapNotifyTable, pu1IsisEvent) \
        { \
             IsisCtrlCreateNotifyTable(pTrapNotifyTable, pu1IsisEvent);\
             IsisSnmpIfSendTrap(pu1IsisEvent, pu1IsisEvent->u1EvtID); \
        }

#else

#define ISIS_SEND_TRAP(pTrapNotifyTable, pu1IsisEvent)

#endif 


/*ISIS Graceful Restart*/
#define CONTEXT_COUNT(pContext,pCxtcount)  while (pContext != NULL){ \
     (*pCxtcount)++; \
     pContext = pContext->pNext; \
    }

/* Multi-topology ISIS */
#define ISIS_SET_MT_ID(u1MtId, u1AddrType) \
 {\
     if (u1AddrType & ISIS_ADDR_IPV4)\
            {\
                u1MtId |= ISIS_MT0_BITMAP;\
            }\
     if (u1AddrType & ISIS_ADDR_IPV6)\
            {\
                u1MtId |= ISIS_MT2_BITMAP;\
            }\
        }
  
#define ISIS_RESET_MT_ID(u1MtId, u1AddrType) \
{\
    if (u1AddrType & ISIS_ADDR_IPV4)\
    {\
        u1MtId &= ~ISIS_MT0_BITMAP;\
    }\
    if (u1AddrType & ISIS_ADDR_IPV6)\
    {\
        u1MtId &= ~ISIS_MT2_BITMAP; \
    }\
}

#define ISIS_SET_METRIC(pContext, pDest, pSrc) \
{\
    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) \
    {\
        pDest->u4FullMetric = pSrc->u4FullMetric; \
    }\
    else \
    {\
        MEMCPY (pDest->Metric, pSrc->Metric, sizeof (tIsisMetric));\
    }\
}

#define ISIS_GET_METRIC(pContext, pRec, u1MetIndex) \
    (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) ? pRec->u4FullMetric : (UINT4)pRec->Metric [u1MetIndex]

#define ISIS_COPY_METRIC(pContext, u4Metric, pDest) \
{\
    u4Metric = (UINT4)u4Metric; \
    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC) \
    {\
        pDest->u4FullMetric = (UINT4)u4Metric; \
    }\
    else \
    {\
        MEMCPY (pDest->Metric, &u4Metric, sizeof (tIsisMetric));\
    }\
}

#define ISIS_PASTE_METRIC(pContext, pDest, u4Metric, u1Metric) \
{\
    if (pContext->u1MetricStyle == ISIS_STYLE_WIDE_METRIC)\
    {\
        pDest->u4FullMetric = u4Metric; \
    }\
    else \
    {\
        MEMCPY (pDest->Metric, u1Metric, sizeof (tIsisMetric));\
    }\
}

/* Dynamic Hostname Support */
#define ISIS_DYNHOSTNME_ENABLE                     1
#define ISIS_DYNHOSTNME_DISABLE                    2

/* BFD for M-ISIS macros */
#define ISIS_BFD_ENABLE         1
#define ISIS_BFD_DISABLE        2

#define ISIS_BFD_TRUE           1
#define ISIS_BFD_FALSE          0

#define ISIS_BFD_IPV4_ENABLED  1 /* 0th bit set */
#define ISIS_BFD_IPV6_ENABLED  2 /* 1st bit set */

#define ISIS_BFD_SESS_DOWN          1
#define ISIS_BFD_SESS_UP            2
#define ISIS_BFD_SESS_ADMIN_DOWN    3

#define ISIS_CHK_BFD_STATUS(pAdjRec) \
  (((ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) == ISIS_BFD_FALSE) || \
   ((ISIS_BFD_NEIGHBOR_USEABLE (pAdjRec) == ISIS_BFD_TRUE) && \
                     (pAdjRec->u1IsisMT0BfdState != ISIS_BFD_SESS_UP) && \
                     (pAdjRec->u1IsisMT2BfdState != ISIS_BFD_SESS_UP)))? ISIS_BFD_FALSE : ISIS_BFD_TRUE)

#define ISIS_BFD_REQUIRED(pAdjEntry)             pAdjEntry->u1IsisBfdRequired
#define ISIS_BFD_NEIGHBOR_USEABLE(pAdjEntry)     pAdjEntry->u1IsisBfdNeighborUseable

#define ISIS_COMPARE_IP_ADDR_IN_ADJ(pIpAddr, pAdjEntry) \
        (pIpAddr->u1AddrType == ISIS_ADDR_IPV4)? \
        MEMCMP (pIpAddr->au1IpAddr, pAdjEntry->AdjNbrIpV4Addr.au1IpAddr, ISIS_MAX_IPV4_ADDR_LEN): \
        MEMCMP (pIpAddr->au1IpAddr, pAdjEntry->AdjNbrIpV6Addr.au1IpAddr, ISIS_MAX_IPV6_ADDR_LEN)

#define ISIS_COMPARE_IP_ADDR_IN_CKT(IpAddr, pCktEntry, u1AddrType) \
        (u1AddrType == ISIS_ADDR_IPV4)? \
        MEMCMP (IpAddr, pCktEntry->au1IPV4Addr, ISIS_MAX_IPV4_ADDR_LEN): \
        MEMCMP (IpAddr, pCktEntry->au1IPV6Addr, ISIS_MAX_IPV6_ADDR_LEN)

/* Deregister from BFD, if registered for path monitoring */
#define ISIS_BFD_DEREGISTER(pContext, pAdjRec, pCktRec, u1SyncFlag) IsisUtlBfdDeRegister (pContext, pAdjRec, pCktRec, u1SyncFlag)

/* Register with BFD */
#define ISIS_BFD_REGISTER(pContext, pAdjRec, pCktRec) IsisUtlBfdRegister (pContext, pAdjRec, pCktRec)

#endif /* __ISMACS_H__ */
