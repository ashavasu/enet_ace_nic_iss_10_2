/********************************************************************
* Copyright (C) Future Software Limited,2003
*
* $Id: fsislow.h,v 1.12 2016/03/26 09:56:49 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisMaxInstances ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxCircuits ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxAreaAddrs ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxAdjs ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxIPRAs ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxEvents ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxSummAddr ARG_LIST((INT4 *));

INT1
nmhGetFsIsisStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxLSPEntries ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxMAA ARG_LIST((INT4 *));

INT1
nmhGetFsIsisFTStatus ARG_LIST((INT4 *));

INT1
nmhGetFsIsisFTState ARG_LIST((INT4 *));

INT1
nmhGetFsIsisFactor ARG_LIST((INT4 *));

INT1
nmhGetFsIsisMaxRoutes ARG_LIST((INT4 *));

INT1
nmhGetFsIsisRRDAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisRRDProtoMaskForEnable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisRRDProtoMaskForDisable ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisRRDRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisRRDImportLevel ARG_LIST((INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisMaxInstances ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxCircuits ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxAreaAddrs ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxAdjs ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxIPRAs ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxEvents ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxSummAddr ARG_LIST((INT4 ));

INT1
nmhSetFsIsisStatus ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxLSPEntries ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxMAA ARG_LIST((INT4 ));

INT1
nmhSetFsIsisFTStatus ARG_LIST((INT4 ));

INT1
nmhSetFsIsisFactor ARG_LIST((INT4 ));

INT1
nmhSetFsIsisMaxRoutes ARG_LIST((INT4 ));

INT1
nmhSetFsIsisRRDAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisRRDProtoMaskForEnable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisRRDProtoMaskForDisable ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisRRDRouteMapName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIsisRRDImportLevel ARG_LIST((INT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisMaxInstances ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxCircuits ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxAreaAddrs ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxAdjs ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxIPRAs ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxEvents ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxSummAddr ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxLSPEntries ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxMAA ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisFTStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisFactor ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisMaxRoutes ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsIsisRRDAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisRRDProtoMaskForEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisRRDProtoMaskForDisable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisRRDRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIsisRRDImportLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIsisMaxInstances ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxCircuits ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxAreaAddrs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxAdjs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxIPRAs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxEvents ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxSummAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxLSPEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxMAA ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisFTStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisFactor ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisMaxRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtSysTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisRestartState ARG_LIST((INT4 *));


INT1
nmhDepv2FsIsisExtSysAreaRxPasswdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtSysDomainRxPasswdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtSummAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtCircTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtCircLevelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtIPRATable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtCircLevelRxPasswordTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtIPIfAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsIsisExtLogTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FsIsisExtSysTable. */
INT1
nmhValidateIndexInstanceFsIsisExtSysTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysTable  */

INT1
nmhGetFirstIndexFsIsisExtSysTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtSysTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtSysAuthSupp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysAreaAuthType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysDomainAuthType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysAreaTxPasswd ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtSysDomainTxPasswd ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtSysMinSPFSchTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysMaxSPFSchTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysMinLSPMark ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysMaxLSPMark ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysDelMetSupp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysErrMetSupp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysExpMetSupp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysActSysType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysActMPS ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysActMaxAA ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysActSysIDLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysActSysID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtSysDroppedPDUs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysActOrigL1LSPBufSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysActOrigL2LSPBufSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysRouterID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtSysCkts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysActiveCkts ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysAdjs ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysOperState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtRestartSupport ARG_LIST((INT4 ,INT4 *));


INT1
nmhGetFsIsisExtGRRestartTimeInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtGRT2TimeIntervalLevel1 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtGRT2TimeIntervalLevel2 ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtGRT1TimeInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtGRT1RetryCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtGRMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtRestartStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtRestartExitReason ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtRestartReason ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtHelperSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtHelperGraceTimeLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisMultiTopologySupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysBfdSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisExtSysBfdAllIfStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisDotCompliance ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsIsisExtSysDynHostNameSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsIsisMetricStyle ARG_LIST((INT4 ,INT4 *)); 

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtSysAuthSupp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysAreaAuthType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysDomainAuthType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysAreaTxPasswd ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIsisExtSysDomainTxPasswd ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIsisExtSysMinSPFSchTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysMaxSPFSchTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysMinLSPMark ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysMaxLSPMark ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysDelMetSupp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysErrMetSupp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysExpMetSupp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysRouterID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIsisExtRestartSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtGRRestartTimeInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtGRT2TimeIntervalLevel1 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtGRT2TimeIntervalLevel2 ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtGRT1TimeInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtGRT1RetryCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtRestartReason ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtHelperSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtHelperGraceTimeLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisMultiTopologySupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysBfdSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysBfdAllIfStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisDotCompliance ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisExtSysDynHostNameSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsIsisMetricStyle ARG_LIST((INT4  ,INT4 ));  

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtSysAuthSupp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysAreaAuthType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysDomainAuthType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysAreaTxPasswd ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIsisExtSysDomainTxPasswd ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIsisExtSysMinSPFSchTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysMaxSPFSchTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysMinLSPMark ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysMaxLSPMark ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysDelMetSupp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysErrMetSupp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysExpMetSupp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysRouterID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIsisExtRestartSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtGRRestartTimeInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtGRT2TimeIntervalLevel1 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtGRT2TimeIntervalLevel2 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtGRT1TimeInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtGRT1RetryCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtRestartReason ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtHelperSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtHelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisMultiTopologySupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysBfdSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysBfdAllIfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisDotCompliance ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSysDynHostNameSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsIsisMetricStyle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));   

/* Proto Validate Index Instance for FsIsisExtSysAreaRxPasswdTable. */
INT1
nmhValidateIndexInstanceFsIsisExtSysAreaRxPasswdTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysAreaRxPasswdTable  */

INT1
nmhGetFirstIndexFsIsisExtSysAreaRxPasswdTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtSysAreaRxPasswdExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtSysAreaRxPasswdExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtSysAreaRxPasswdExistState ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtSysDomainRxPasswdTable. */
INT1
nmhValidateIndexInstanceFsIsisExtSysDomainRxPasswdTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysDomainRxPasswdTable  */

INT1
nmhGetFirstIndexFsIsisExtSysDomainRxPasswdTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtSysDomainRxPasswdExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtSysDomainRxPasswdExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtSysDomainRxPasswdExistState ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtSummAddrTable. */
INT1
nmhValidateIndexInstanceFsIsisExtSummAddrTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSummAddrTable  */

INT1
nmhGetFirstIndexFsIsisExtSummAddrTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtSummAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtSummAddrDelayMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsIsisExtSummAddrErrorMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsIsisExtSummAddrExpenseMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsIsisExtSummAddrFullMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtSummAddrDelayMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsIsisExtSummAddrErrorMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsIsisExtSummAddrExpenseMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsIsisExtSummAddrFullMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtSummAddrDelayMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSummAddrErrorMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSummAddrExpenseMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtSummAddrFullMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

/* Proto Validate Index Instance for FsIsisExtSysEventTable. */
INT1
nmhValidateIndexInstanceFsIsisExtSysEventTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysEventTable  */

INT1
nmhGetFirstIndexFsIsisExtSysEventTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtSysEventTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtSysEventStr ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsIsisExtSysHostNameTable. */
INT1
nmhValidateIndexInstanceFsIsisExtSysIdToSysNameMappingTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysHostNameTable  */

INT1
nmhGetFirstIndexFsIsisExtSysIdToSysNameMappingTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtSysIdToSysNameMappingTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtSysRouterHostName ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsIsisExtCircTable. */
INT1
nmhValidateIndexInstanceFsIsisExtCircTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtCircTable  */

INT1
nmhGetFirstIndexFsIsisExtCircTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtCircTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtCircIfStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircTxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircRxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircTxISHs ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircRxISHs ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircSNPA ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtCircMTID ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircBfdStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircExtendedCircID ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtCircIfStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtCircTxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtCircRxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtCircSNPA ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIsisExtCircBfdStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtCircIfStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtCircTxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtCircRxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtCircSNPA ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIsisExtCircBfdStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtCircLevelTable. */
INT1
nmhValidateIndexInstanceFsIsisExtCircLevelTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtCircLevelTable  */

INT1
nmhGetFirstIndexFsIsisExtCircLevelTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtCircLevelTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtCircLevelDelayMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircLevelErrorMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircLevelExpenseMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtCircLevelTxPassword ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtCircLevelWideMetric ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsIsisExtCircLevelAuthType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtCircLevelDelayMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtCircLevelErrorMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtCircLevelExpenseMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtCircLevelTxPassword ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIsisExtCircLevelWideMetric ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsIsisExtCircLevelAuthType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtCircLevelDelayMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtCircLevelErrorMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtCircLevelExpenseMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtCircLevelTxPassword ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIsisExtCircLevelWideMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsIsisExtCircLevelAuthType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtIPRATable. */
INT1
nmhValidateIndexInstanceFsIsisExtIPRATable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtIPRATable  */

INT1
nmhGetFirstIndexFsIsisExtIPRATable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtIPRATable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtIPRADelayMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtIPRAErrorMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtIPRAExpenseMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtIPRADelayMetricType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtIPRAErrorMetricType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtIPRAExpenseMetricType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtIPRANextHopType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtIPRANextHop ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtIPRAFullMetric ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtIPRADelayMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtIPRAErrorMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtIPRAExpenseMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtIPRADelayMetricType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtIPRAErrorMetricType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtIPRAExpenseMetricType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtIPRANextHopType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsIsisExtIPRANextHop ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsIsisExtIPRAFullMetric ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtIPRADelayMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtIPRAErrorMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtIPRAExpenseMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtIPRADelayMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtIPRAErrorMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtIPRAExpenseMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtIPRANextHopType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsIsisExtIPRANextHop ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsIsisExtIPRAFullMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

/* Proto Validate Index Instance for FsIsisExtCircLevelRxPasswordTable. */
INT1
nmhValidateIndexInstanceFsIsisExtCircLevelRxPasswordTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtCircLevelRxPasswordTable  */

INT1
nmhGetFirstIndexFsIsisExtCircLevelRxPasswordTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtCircLevelRxPasswordTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtCircLevelRxPasswordExistState ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtCircLevelRxPasswordExistState ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtCircLevelRxPasswordExistState ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtIPIfAddrTable. */
INT1
nmhValidateIndexInstanceFsIsisExtIPIfAddrTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtIPIfAddrTable  */

INT1
nmhGetFirstIndexFsIsisExtIPIfAddrTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtIPIfAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtIPIfExistState ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtIPIfExistState ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtIPIfExistState ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtLogTable. */
INT1
nmhValidateIndexInstanceFsIsisExtLogTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtLogTable  */

INT1
nmhGetFirstIndexFsIsisExtLogTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtLogTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtLogLevel ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisExtLogLevel ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisExtLogLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtAdjTable. */
INT1
nmhValidateIndexInstanceFsIsisExtAdjTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtAdjTable  */

INT1
nmhGetFirstIndexFsIsisExtAdjTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisExtAdjTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisExtAdjNeighSysID ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtAdjHelperStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtAdjHelperExitReason ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtAdjMTID ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtAdjHostName ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsIsisExtISAdj3WayState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsIsisExtISAdjNbrExtendedCircID ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsIsisDistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFsIsisDistInOutRouteMapTable ARG_LIST((
    INT4  , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4    ));

/* Proto Type for Low Level GET FIRST fn for FsIsisDistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFsIsisDistInOutRouteMapTable ARG_LIST((
    INT4 * , 
    tSNMP_OCTET_STRING_TYPE *  , 
    INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisDistInOutRouteMapTable ARG_LIST((
    INT4 , 
    INT4 * , 
    tSNMP_OCTET_STRING_TYPE *, 
    tSNMP_OCTET_STRING_TYPE *  , 
    INT4 ,
    INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisDistInOutRouteMapValue ARG_LIST((
    INT4  , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4 ,
    INT4 *));

INT1
nmhGetFsIsisDistInOutRouteMapRowStatus ARG_LIST((
    INT4  , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4 ,
    INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisDistInOutRouteMapValue ARG_LIST((
    INT4  , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4  ,
    INT4 ));

INT1
nmhSetFsIsisDistInOutRouteMapRowStatus ARG_LIST((
    INT4  , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4  ,
    INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisDistInOutRouteMapValue ARG_LIST((UINT4 *  ,
    INT4  , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4  ,
    INT4 ));

INT1
nmhTestv2FsIsisDistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,
    INT4  , 
    tSNMP_OCTET_STRING_TYPE * , 
    INT4  ,
    INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIsisDistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIsisPreferenceTable. */
INT1
nmhValidateIndexInstanceFsIsisPreferenceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisPreferenceTable  */

INT1
nmhGetFirstIndexFsIsisPreferenceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisPreferenceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisPreferenceValue ARG_LIST((
    INT4 ,
    INT4 *));

INT1
nmhGetFsIsisPreferenceRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisPreferenceValue ARG_LIST((
    INT4  ,
    INT4 ));

INT1
nmhSetFsIsisPreferenceRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisPreferenceValue ARG_LIST((UINT4 *  ,
    INT4  ,
    INT4 ));

INT1
nmhTestv2FsIsisPreferenceRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIsisPreferenceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsIsisRRDMetricTable. */
INT1
nmhValidateIndexInstanceFsIsisRRDMetricTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisRRDMetricTable  */

INT1
nmhGetFirstIndexFsIsisRRDMetricTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsIsisRRDMetricTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsIsisRRDMetricValue ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsIsisRRDMetricValue ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsIsisRRDMetricValue ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsIsisRRDMetricTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
