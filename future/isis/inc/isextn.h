
/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isextn.h,v 1.11 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file contains all the external variable declarations
 *
 *****************************************************************************/

#ifndef __ISEXTN_H__
#define __ISEXTN_H__

extern UINT1              gu1IsisStatus;
extern UINT1              gu1IsisGrCsnp;
extern UINT1              gu1RelqFlag;
extern UINT1              gau1IsisModLevelMask [ISIS_MAX_MODULES];
extern INT1               gai1IsisLogStr [ISIS_MAX_LOG_SIZE];
extern INT1               gai1IsisTmLogStr [ISIS_MAX_LOG_TIME_SIZE];
extern tMemPoolId         gau1IsisBdyId [BUDDY_MAX_INST];
extern tIsisMemConfigs    gIsisMemConfigs;
extern tIsisMemConfigs    gIsisMemActuals;
extern tIsisSysContext    *gpIsisInstances;
extern VOID               (*gIsisTmrRoutines[]) (tIsisSysContext *) ;
extern tIsisMemPoolId     gIsisMemPoolId;
extern tIsisLSPTxTable    gIsisLspTxPool;     
extern tIsisHashTable     gIsisCktHashTable;
extern tIsisExtInfo       gIsisExtInfo;
extern tTimerListId       gIsisTmrListID;
extern tOsixTaskId        gIsisTaskId;
extern tOsixQId           gIsisCtrlQId;
extern tOsixQId           gIsisRtmQId;
extern tIsisGrInfo       *gIsisGrinfo;
extern UINT1              gu1IsisGrOverLoad;
extern INT4               gi4IsisSysLogId;
extern UINT1              gau1IsisHstNme[ISIS_HSTNME_LEN];
extern UINT4              gu4SemNameVar;
extern UINT1              gu1IsisGrCsrFlag;
extern UINT1              gu1IsisGrRestoreFlag;
extern UINT1              gu1IsisGrState;
extern tRtmMsgHdr         gIsisRtmHdr;
extern tRtm6MsgHdr        gIsisRtm6Hdr;
#ifdef ISIS_FT_ENABLED
extern tOsixQId           gIsisFltQId;
#endif
#endif /* __ISEXTN_H__ */
