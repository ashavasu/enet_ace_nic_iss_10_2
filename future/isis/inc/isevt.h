/*******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isevt.h,v 1.16 2016/06/28 10:09:11 siva Exp $
 *
 * Description: This file contains typedefs for various events logged by 
 *              FutureISIS
 ******************************************************************************/

#ifndef __ISEVT_H__
#define __ISEVT_H__

/* System Errors
 */

typedef struct _IsisEvtResFail {

   UINT1         u1EvtID;
   UINT1         u1ResType;
   UINT2         u2Rsvd;
} tIsisEvtResFail;

typedef struct _IsisEvtSysFail {

   UINT1         u1EvtID;
   UINT1         u1SysRes;
   UINT2         u2Rsvd;
} tIsisEvtSysFail;

/* Interface Errors
 */ 

typedef struct _IsisEvtRegFail {

   UINT1         u1EvtID;
   UINT1         u1IfType;
   UINT2         u2Rsvd;
} tIsisEvtRegFail;

typedef struct _IsisEvtDeRegFail {

   UINT1         u1EvtID;
   UINT1         u1IfType;
   UINT2         u2Rsvd;
} tIsisEvtDeRegFail;

typedef struct _IsisEvtTxFail {

   UINT1         u1EvtID;
   UINT1         u1IfType;
   UINT2         u2Rsvd;
   UINT4         u4IfIdx;
} tIsisEvtTxFail;

typedef struct _IsisEvtRxFail {

   UINT1         u1EvtID;
   UINT1         u1IfType;
   UINT2         u2Rsvd;
   UINT4         u4IfIdx;
} tIsisEvtRxFail;

typedef struct _IsisEvtFTMDecFail {

   UINT1         u1EvtID;
   UINT1         u1Cause;
   UINT2         u2Rsvd;
} tIsisEvtFTMDecFail;

typedef struct _IsisEvtFTMAbort {

   UINT1         u1EvtID;
   UINT1         u1Cause;
   UINT2         u2Rsvd;
} tIsisEvtFTMAbort;

typedef struct _IsisEvtRTMUpdFail {

   UINT1         u1EvtID;
   UINT1         u1Cause;
   UINT2         u2Rsvd;
} tIsisEvtRTMUpdFail;

typedef struct _IsisEvtMIBIILookUpFail {

   UINT1         u1EvtID;
   UINT1         u1Cause;
   UINT2         u2Rsvd;
} tIsisEvtMIBIILookUpFail;

typedef struct _IsisEvtDllConnFail {

   UINT1         u1EvtID;
   UINT1         u1Cause;
   UINT2         u2Rsvd;
   UINT4         u4IfIdx;
} tIsisEvtDLLConnFail;

typedef struct _IsisEvtDllIOCTLFail {

   UINT1         u1EvtID;
   UINT1         u1Cause;
   UINT2         u2Rsvd;
   UINT4         u4IfIdx;
}tIsisEvtDllIOCTLFail;

typedef struct _IsisEvtInvIfIdx {

   UINT1         u1EvtID;
   UINT1         au1Rsvd[3];
   UINT4         u4IfIdx;
} tIsisEvtInvIfIdx;

/* Protocol Errors
 */ 

typedef struct _IsisEvtInvPdu {

   UINT1         u1EvtID;
   UINT1         u1OpCode;
   UINT2         u2Rsvd;
}tIsisEvtInvPdu;

typedef struct _IsisEvtDupSys {

   UINT1         u1EvtID;
   UINT1         u1Cause;
   UINT1         au1SysID [ISIS_SYS_ID_LEN];
   UINT1         au1SNPA [ISIS_SNPA_ADDR_LEN];
#if ((ISIS_SYS_ID_LEN + ISIS_SNPA_ADDR_LEN + 2) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_SYS_ID_LEN + ISIS_SNPA_ADDR_LEN + 2) %
   4)]; 
#endif
}tIsisEvtDupSys;

typedef struct _IsisEvtIDLenMismatch {

   UINT1         u1EvtID;
   UINT1         u1SelfIdLen;
   UINT1         u1RxdIdLen;
   UINT1         u1PduType;
   UINT4         u4CktIfIdx;
   UINT1         *pu1PduFrag; /*Added for traps */
}tIsisEvtIDLenMismatch;

typedef struct _IsisEvtAuthFail {

   UINT1         u1EvtID;
   UINT1         u1Cause;
   UINT1         u1AuthType;
   UINT1         u1SysLevel;
   tIsisPasswd   RxdPasswd;
   UINT4         u4CktIfIdx;
   UINT1         *pu1PduFrag;/*Added for traps */
}tIsisEvtAuthFail;   

typedef struct _IsisEvtISStatChange {

   UINT1         u1EvtID;
   UINT1         u1PrevState;
   UINT1         au1SysID [ISIS_SYS_ID_LEN];
#if ((ISIS_SYS_ID_LEN + 2) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_SYS_ID_LEN + 2) % 4)];
#endif
}tIsisEvtISStatChange;

typedef struct _IsisEvtWrongSys {
   
   UINT1         u1EvtID;
   UINT1         u1SysType;
   UINT1         au1AdjSysID [ISIS_SYS_ID_LEN];
#if ((ISIS_SYS_ID_LEN + 2) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_SYS_ID_LEN + 2) % 4)];
#endif
}tIsisEvtWrongSys;   

typedef struct _IsisEvtIncompCkt {
   
   UINT1         u1EvtID;
   UINT1         u1CktLevel;
   UINT2         u2Rsvd;
   UINT4         u4CktIdx;
   UINT4         u4CktIfIdx;
}tIsisEvtIncompCkt;

typedef struct _IsisEvtNodeStatInd {

   UINT1         u1EvtID;
   UINT1         u1NodeID;
   UINT1         u1Status;
   UINT1         u1Rsvd;
}tIsisEvtNodeStatInd;   

typedef struct _IsisEvtIFStatInd {

   UINT1         u1EvtID;
   UINT1         u1Status;
   UINT2         u2Rsvd;
   UINT4         u4CktIfIdx;
}tIsisEvtIfStatInd;

typedef struct _IsisEvtAAMismatch {

   UINT1         u1EvtID;
   UINT1         u1PduType;
   UINT1         u1MaxAA;
   UINT1         u1SysLevel;
   UINT2         u2LSPSize;
   UINT2         u2Rsvd;
   UINT4         u4CktIfIdx;
   UINT1         *pu1PduFrag;
}tIsisEvtAAMismatch;

typedef struct _IsisEvtManAAChange {

   UINT1         u1EvtID;
   UINT1         u1Status;
   UINT1         u1Length;
   UINT1         au1AreaAddr[ISIS_AREA_ADDR_LEN];
#if ((ISIS_AREA_ADDR_LEN + 3) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_AREA_ADDR_LEN + 3) % 4)];
#endif
}tIsisEvtManAAChange;

typedef struct _IsisEvtManAADropped {

   UINT1         u1EvtID;
   UINT1         u1MAAExistState;
   UINT1         u1Length;
   UINT1         au1AreaAddr[ISIS_AREA_ADDR_LEN];
#if ((ISIS_AREA_ADDR_LEN + 3) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_AREA_ADDR_LEN + 3) % 4)];
#endif
}tIsisEvtManAADropped;

typedef struct _IsisEvtMaxAAMismatch {

   UINT1         u1EvtID;
   UINT1         u1SelfMAA;
   UINT1         u1RxdMAA;
   UINT1         u1PduType;
   UINT4         u4CktIfIdx;
   UINT1         *pu1PduFrag;/*Added for traps */
}tIsisEvtMaxAAMismatch;

typedef struct _IsisEvtPSChange {

   UINT1         u1EvtID;
   UINT1         u1Status;
   UINT1         u1ProtSupp;
   UINT1         u1Rsvd;
}tIsisEvtPSChange;

typedef struct _IsisEvtIPIFChange {

   UINT1         u1EvtID;
   UINT1         u1Status;
   UINT1         u1PrefixLen;
   UINT1         u1AddrType;
   UINT4         u4CktIfIdx;
   UINT4         u4CktIfSubIdx;
   UINT1         au1IPAddr[ISIS_MAX_IP_ADDR_LEN];
#if ((ISIS_MAX_IP_ADDR_LEN) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_MAX_IP_ADDR_LEN) % 4)];
#endif
   UINT1         u1SecondaryFlag;
   UINT1         u1Pad[3];
}tIsisEvtIPIFChange;

typedef struct _IsisEvtIPRAChange {

  UINT1        u1EvtID;
  UINT1        u1Status;
  UINT1        u1IPRADestType;
  UINT1        u1PrefixLen;
  UINT1        u1MetricType;
  UINT1        u1IPRAType;
  UINT1        au1Pad[2];
  UINT4        u4IPRAIdx;
  UINT1        au1IPRADest[ISIS_MAX_IP_ADDR_LEN];
  UINT1        au1IPRASNPA[ISIS_SNPA_ADDR_LEN];
  tIsisMetric  Metric;
#if ((ISIS_MAX_IP_ADDR_LEN + ISIS_SNPA_ADDR_LEN + ISIS_NUM_METRICS) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_MAX_IP_ADDR_LEN + ISIS_SNPA_ADDR_LEN +
                                 ISIS_NUM_METRICS) % 4)];
#endif
  UINT4        u4FullMetric;
} tIsisEvtIPRAChange;

typedef struct _IsisEvtSummAddrChg {

    UINT1         u1EvtID;
    UINT1         u1Status;
    UINT1         u1MetricType;
    UINT1         u1PrefixLen;
    UINT1         u1PrevAdminState;
    UINT1         u1CurrAdminState;
    UINT1         au1IPAddr[ISIS_MAX_IP_ADDR_LEN];
    tIsisMetric   Metric;
    UINT1         u1AddrType;
    UINT1         au1Pad[1];
    UINT4         u4FullMetric;
}tIsisEvtSummAddrChg;


typedef struct _IsisEvtCktChange {

   UINT1         u1EvtID;
   UINT1         u1Status;
   UINT1         u1CktType;
   UINT1         u1CktLevel;
   UINT4         u4CktIdx;
   UINT1         u1MetricType;
   UINT1         u1Reserved;
   UINT2         u2Reserved;
   tIsisMetric   Metric;
#if ((ISIS_NUM_METRICS) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_NUM_METRICS) % 4)]; 
#endif
   UINT4         u4FullMetric;
} tIsisEvtCktChange;   

/*VCM Info Structure*/
typedef struct _IsisEvtVcmInfo {

    UINT4                   u4IpIfIndex;
    UINT4                   u4VcmCxtId;
    UINT1                   u1BitMap;
    UINT1                   u1EvtID;
    UINT1                   au1Rsvd[2];
} tIsisEvtVcmInfo;


typedef struct _IsisEvtAdjChange {

   UINT1         u1EvtID;
   UINT1         u1Status;
   UINT1         u1AdjType;
   UINT1         u1MetricType;
   UINT4         u4AdjIdx;
   UINT4         u4CktIdx;
   UINT1         u1AdjUsage;
   UINT1         u1Reserved;
   UINT2         u2Reserved;
   tIsisMetric   Metric;
   UINT1         au1AdjSysID [ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
#if ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN + ISIS_NUM_METRICS) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN +
                                 ISIS_NUM_METRICS) % 4)]; 
#endif
   UINT4         u4FullMetric;
} tIsisEvtAdjChange;

typedef struct _IsisEvtAdjRej {

   UINT1         u1EvtID;
   UINT1         u1SysLevel;
   UINT2         u2Rsvd;
   UINT4         u4CktIfIdx;
   UINT4         u4CktIdx;
   UINT1         au1AdjSysID [ISIS_SYS_ID_LEN];
#if ((ISIS_SYS_ID_LEN) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_SYS_ID_LEN) % 4)];
#endif
}tIsisEvtAdjRej;

typedef struct _IsisEvtNoDISElect {

   UINT1         u1EvtID;
   UINT1         u1Reason;
   UINT2         u2Rsvd;
   UINT4         u4CktIdx;
   UINT1         au1SNPA [ISIS_SNPA_ADDR_LEN];
#if ((ISIS_SYS_ID_LEN) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_SYS_ID_LEN) % 4)];
#endif
}tIsisEvtNoDISElect;

typedef struct _IsisEvtDISChange {

   UINT1         u1EvtID;
   UINT1         u1CktLevel;
   UINT2         u2Rsvd;
   UINT4         u4CktIdx;
   UINT1         au1PrevDIS [ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
   UINT1         au1CurrDIS [ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN];
#if ((2 * (ISIS_SYS_ID_LEN + ISIS_PNODE_ID_LEN)) % 4)
   UINT1         au1Rsvd [ 4 - ((2 * (ISIS_SYS_ID_LEN + 
                                      ISIS_PNODE_ID_LEN)) % 4)];
#endif
}tIsisEvtDISChg;

typedef struct _IsisEvtDISElect {

   UINT1         u1EvtID;
   UINT1         u1CktLevel;
   UINT2         u2Rsvd;
   UINT4         u4CktIdx;
}tIsisEvtDISElect;

typedef struct _IsisEvtDISStatChange {

  UINT1          u1EvtID;
  UINT1          u1Level;
  UINT1          u1Status;
  UINT1          u1Rsvd;
  UINT4          u4CktIdx;
} tIsisEvtDISStatChg;

typedef struct _IsisEvtPriorityChange {

  UINT1         u1EvtID;
  UINT1         u1Priority;
  UINT1         u1CktLevel;
  UINT1         u1Rsvd;
  UINT4         u4CktIdx;
}tIsisEvtPriorityChange;

typedef struct _IsisEvtCorrLsp{

  UINT1         u1EvtID;
  UINT1         u1Level;
  UINT2         u2Rsvd;
  UINT1         au1TrapLspID [ISIS_LSPID_LEN];
#if ((ISIS_LSPID_LEN) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_LSPID_LEN) % 4)];
#endif
}tIsisEvtCorrLsp;

typedef struct _IsisEvtInvalidLSP {

  UINT1         u1EvtID;
  UINT1         u1Reason;
  UINT2         u2Rsvd;
  UINT4         u4CktIdx;
  UINT1         au1LSPId [ISIS_LSPID_LEN];
#if ((ISIS_LSPID_LEN) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_LSPID_LEN) % 4)];
#endif
} tIsisEvtInvalidLSP;

typedef struct _IsisEvtLSPDBOL {

  UINT1         u1EvtID;
  UINT1         u1Level;
  UINT1         u1SysL1State;
  UINT1         u1SysL2State;
}tIsisEvtLSPDBOL;


typedef struct _IsisEvtRestarter {
  UINT2         u2IsisGRT3TimerInterval;
  UINT1         au1SysID [ISIS_SYS_ID_LEN];
  UINT1         u1EvtID;
  UINT1         u1IsisGRRestartStatus;
  UINT1         u1IsisGRRestartExitReason;
  UINT1         au1padding[1];
}tIsisEvtRestarter;

typedef struct _IsisEvtHelper {
  UINT2         u2AdjGRTime;
  UINT1         u1IsisGRHelperExitReason;
  UINT1         u1EvtID;
  UINT1         au1SysID [ISIS_SYS_ID_LEN];
  UINT1         au1AdjNbrSysID[ISIS_SYS_ID_LEN];
  UINT1         u1IsisGRHelperStatus;
  UINT1         au1padding[1];
}tIsisEvtHelper;


typedef struct _IsisEvtOwnLspPurge {

  UINT1         u1EvtID;
  UINT1         u1SysLevel;
  UINT2         u2Rsvd;
  UINT4         u4CktIfIdx;
  UINT1         au1TrapLspID [ISIS_LSPID_LEN];
#if ((ISIS_LSPID_LEN) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_LSPID_LEN) % 4)];
#endif
}tIsisEvtOwnLspPurge;

typedef struct _IsisEvtDBVal{

  UINT1         u1EvtID;
  UINT1         au1Rsvd[3];
}tIsisEvtDBVal;  

typedef struct _IsisEvtSeqNoExceed {

  UINT1         u1EvtID;
  UINT1         u1Level;
  UINT2         u2Rsvd;
  UINT1         au1TrapLspID [ISIS_LSPID_LEN];
}tIsisEvtSeqNoExceed;

typedef struct _IsisEvtSeqNoSkip {

  UINT1         u1EvtID;
  UINT1         u1SysLevel;
  UINT2         u2Rsvd;
  UINT4         u4CktIfIdx;
  UINT1         au1TrapLspID [ISIS_LSPID_LEN];
#if ((ISIS_LSPID_LEN) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_LSPID_LEN) % 4)];
#endif
}tIsisEvtSeqNoSkip;

typedef struct _IsisEvtTxQFull {

  UINT1         u1EvtID;
  UINT1         au1LspID [ISIS_LSPID_LEN];
#if ((ISIS_LSPID_LEN + 1) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_LSPID_LEN + 1) % 4)];
#endif
}tIsisEvtTxQFull;

typedef struct _IsisEvtNearL2NotAvail {

  UINT1         u1EvtID;
  UINT1         au1Rsvd[3];
}tIsisEvtNearL2NotAvail;

typedef struct _IsisEvtDecnComplete {
  
   UINT1       u1EvtID;
   UINT1         au1Rsvd[3];
} tIsisEvtDecnComplete;

typedef struct _IsisEvtAttStatChg {

  UINT1        u1EvtID;
  UINT1        u1MetType;
  UINT1        u1Attached;
  UINT1        u1MtIndex; /* 0 for ipv4, 1 for ipv6 (MT -2)*/
}tIsisEvtAttStatChg;

typedef struct _IsisEvtShutdown {

  UINT1        u1EvtID;
  UINT1         au1Rsvd[3];
}tIsisEvtShutdown;

typedef struct _IsisEvtResetAll {

  UINT1        u1EvtID;
  UINT1         au1Rsvd[3];
}tIsisEvtResetAll;

typedef struct _IsisEvtVerMismatch {

  UINT1        u1EvtID;
  UINT1        u1ProtVer;
  UINT1        u1SysLevel;
  UINT1        u1Rsvd;
  UINT4        u4CktIfIdx;
  UINT1        *pu1PduFrag;
}tIsisEvtVerMismatch;

typedef struct _IsisEvtLspSizeTooLarge {

  UINT1        u1EvtID;
  UINT1        u1SysLevel;
  UINT2        u2LSPSize;
  UINT4        u4CktIfIdx;
  UINT1        au1TrapLspID [ISIS_LSPID_LEN];
#if ((ISIS_LSPID_LEN) % 4)
  UINT1         au1Rsvd [ 4 - ((ISIS_LSPID_LEN) % 4)];
#endif
}tIsisEvtLspSizeTooLarge;

typedef struct _IsisEvtLspBuffSizeMismatch {

  UINT1        u1EvtID;
  UINT1        u1SysLevel;
  UINT2        u2BuffSize;
  UINT4        u4CktIfIdx;
  UINT1        au1TrapLspID [ISIS_LSPID_LEN];
#if ((ISIS_LSPID_LEN) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_LSPID_LEN) % 4)];
#endif

}tIsisEvtLspBuffSizeMismatch;

typedef struct _IsisEvtProtSuppMismatch {

  UINT1        u1EvtID;
  UINT1        u1SysLevel;
  UINT2        u2Rsvd;
  UINT4        u4CktIfIdx;
  UINT1        au1UProt [ISIS_MAX_PROTS_SUPP];
  UINT1        au1TrapLspID [ISIS_LSPID_LEN];
#if ((ISIS_LSPID_LEN + ISIS_MAX_PROTS_SUPP) % 4)
   UINT1         au1Rsvd [ 4 - ((ISIS_LSPID_LEN + ISIS_MAX_PROTS_SUPP) % 4)];
#endif
}tIsisEvtProtSuppMismatch;

typedef struct _IsisEvtDecnRelq {
  UINT1        u1EvtID;
  UINT1        u1MetIdx; 
  UINT1        u1Level; 
  UINT1        u1AttFlag;
  UINT1        u1NearL2Flag; 
  UINT1        u1AttStatChg;
  UINT1        u1MTIndex; /* The multi-topology index, for which SPF was run lastly */
  UINT1        u1Rsvd;
}tIsisEvtDecnRelq;

typedef struct _IsisEvtImportLevelChange {

   UINT1         u1EvtID;
   UINT1         u1PrevImportLevel;
   UINT1         u1ImportLevel;
   UINT1         u1Rsvd;
}tIsisEvtImportLevelChange;
typedef struct _IsisEvtRRDProtoDisable {
   UINT1         u1EvtID;
   UINT1         u1Level;
   UINT2         u2ProtoMask;
}tIsisEvtRRDProtoDisable;
typedef struct _IsisEvtDistChange{
   UINT1        u1EvtID;
   UINT1        au1Pad[3];
}tIsisEvtDistChange;
typedef struct _IsisEvtMetricChange {
   UINT1         u1EvtID;
   UINT1         u1MetricIndex;
   UINT2         u2Rsvd;
   UINT4         u4Metric;
}tIsisEvtMetricChange;

typedef struct _IsisEvtP2PCktState {
   UINT1         u1EvtID;
   UINT1         u1State;
   UINT2         u2Rsvd;
   UINT4         u4CktIndex;
}tIsisEvtP2PCktState;

typedef struct _IsisEvtHostNmeChange {

   UINT1         u1EvtID;
   UINT1         u1Status;
   UINT1         au1HstNme [ISIS_HSTNME_LEN];
   UINT1         u1Rsvd;
}tIsisEvtHostNmeChange;

#endif
