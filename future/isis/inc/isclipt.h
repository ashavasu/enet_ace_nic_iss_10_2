
/******************************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * $Id: isclipt.h,v 1.28 2017/09/11 13:44:07 siva Exp $
 *
 * Description: This file has prototype definitions for isis CLI submodule
 *
 *****************************************************************************/

#ifndef __ISCLIPT_H__
#define __ISCLIPT_H__

#define ISIS_CLI_INVALID_METRIC -1
#define ISIS_CLI_INVALID_INTERVAL 0
#define ISIS_CLI_INVALID_PREFIXLENGTH 0
#define ISIS_CLI_CLEAR_PRIORITY_INTERVAL 128

#define ISIS_HOST_MASK  0xffffffff
#define ISIS_MAX_PREFIXLEN 32
#define ISIS_PRINT_BUF_SIZE 200

extern UINT1 CfaIfIsBridgedInterface (INT4 i4IfIndex);

INT4 IsisCliAddContext PROTO ((tCliHandle CliHandle, UINT4 u4Inst));
INT4 IsisCliDelContext PROTO ((tCliHandle CliHandle, UINT4 u4Inst));
INT4 IsisCliSetSysType PROTO ((tCliHandle CliHandle, UINT4 u4Inst, UINT4 u4SysType));
INT4 IsisCliAddMAA PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1NetId,INT4 i4NetDotComp));
INT4 IsisCliDelMAA PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1NetId,INT4 i4NetDotComp));
INT4 IsisCliAddSa PROTO ((tCliHandle CliHandle, UINT4 u4Inst, VOID *pu4Address,
              VOID *pMask, UINT4, UINT4 u4Level));


INT4 IsisCliDelSa PROTO ((tCliHandle CliHandle, UINT4 u4Inst, VOID *pAddress, VOID *pMask));


INT4 IsisCliAddIpRa PROTO ((tCliHandle CliHandle, UINT4 u4Inst, UINT4 u4IfIndex, VOID *pAddress, VOID *pu1Mask, VOID *pNextHop,UINT4));



INT4 IsisCliUpdIPRA PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx, INT4  u4ipraIndex, INT4 i4default_metric, UINT4 u4full_metric));



INT4 IsisCliDelIpRa PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx, INT4  i4ipraIndex));

INT4 IsisCliSetDBOL PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx));
INT4 IsisCliResetDBOL PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx));

INT4 IsisCliSetRouteLeak (tCliHandle CliHandle, UINT4 u4InstIdx);
INT4 IsisCliResetRouteLeak (tCliHandle CliHandle, UINT4 u4InstIdx);


INT4 IsisCliSetAreaPasswd PROTO ((tCliHandle CliHandle,UINT4 u4InstIdx,UINT1 *pu1passwd));
INT4 IsisCliResetAreaPasswd PROTO ((tCliHandle CliHandle,UINT4 u4InstIdx, UINT1 *pu1passwd));
INT4 IsisCliSetDomainPasswd PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx ,UINT1 *pu1passwd));
INT4 IsisCliResetDomainPasswd PROTO ((tCliHandle CliHandle,UINT4 u4InstIdx, UINT1 *pu1passwd));
INT4 IsisCtrlUtlGetMAACnt PROTO ((UINT4 u4InstIdx, UINT2 *pu2MAACnt));
INT4 IsisCliChkOctetStr PROTO ((UINT1 *pu1OctetStr, UINT1 u1Len));

INT4 IsisCliDelCkt PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx,INT4 i4IfIndex));
INT4 IsisCliAddCkt PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx,INT4 i4IfIndex,UINT1 u1CircType));


INT4 IsisCliSetCktMetric PROTO ((tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4default_metric, INT4 i4delay_metric, INT4 i4error_metric, INT4 i4expense_metric,UINT4 u4wide_metric, UINT1 u1Level));

INT4 IsisCliSetCktType PROTO ((tCliHandle CliHandle,INT4 i4IfIndex,UINT1 u1Level));
INT4 IsisCliResetCktMetric PROTO ((tCliHandle CliHandle ,INT4 i4IfIndex,UINT1 u1Level));

INT4 IsisCliSetCktHelloInt PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT4 u4Interval, UINT1 u1Level));
INT4 IsisCliSetCktHelloMulti PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT4 u4Interval, UINT1 u1Level));

INT4 IsisCliSetCktAddPasswd PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT1 *pu1passwd, UINT1 u1Level));
INT4 IsisCliSetCktDelPasswd PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT1 *pu1passwd, UINT1 u1Level));

INT4 IsisCliSetCktLSPInt PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT4 u4Interval, UINT1 u1Level));
INT4 IsisCliSetCktRetrInt PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT4 u4Interval, UINT1 u1Level));

INT4 IsisCliSetCktPriority PROTO (( tCliHandle CliHandle,INT4 i4IfIndex, UINT4 u4Priority, UINT1 u1Level));

INT4 IsisCliSetCktCSNPInt PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT4 u4Interval, UINT1 u1Level));

INT4 IsisCliShowInsts PROTO ((tCliHandle CliHandle));

INT4 IsisCliShowCkts PROTO ((tCliHandle CliHandle));

INT4 IsisCliShowAdjs PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT4 u4Inst));
INT4 IsisCliShowIfAdjs PROTO ((tCliHandle CliHandle,tIsisCktEntry *pPrintCktRec, tIsisSysContext *pContext));

INT4 IsisCliShowRoutes PROTO ((tCliHandle CliHandle, UINT1 u1Level,UINT4 u4InstIdx));
INT4 IsisCliShowIPV6Routes PROTO ((tCliHandle CliHandle, UINT1 u1Level,UINT4 u4InstIdx));

INT4 IsisCliAddIPV6Sa PROTO ((tCliHandle CliHandle, UINT4 u4Inst, UINT1 *pu1Address,
              UINT4 u4PrefixLen, UINT4, UINT4 u4Level));

INT4 IsisCliDelIPV6Sa PROTO ((tCliHandle CliHandle, UINT4 u4Inst, UINT1 *pu1Address, UINT4 u4Prefixlength));

INT4 IsisCliAddIpV6Ra PROTO ((tCliHandle CliHandle, UINT4 u4Inst, UINT4 u4IfIndex, UINT1 *pu1Address, UINT4 u4PrefixLen, UINT1 *pu1NextHop,UINT4));
INT4 IsisCliShowLSPDB PROTO ((tCliHandle CliHandle,UINT4 u4InstIdx,UINT1 u1level,UINT4 u4Detail,UINT1 *u1lspid,INT4 i4NetDotComp));


VOID IsisCliShowL1DataBase PROTO ((tCliHandle CliHandle, tIsisSysContext * pContext, UINT1 bDetailed,INT4 i4DotCompliance));


VOID IsisCliShowL2DataBase PROTO ((tCliHandle CliHandle, tIsisSysContext * pContext, UINT1 bDetailed,INT4 i4DotCompliance));


VOID IsisCliPrintLSPInfo PROTO ((tCliHandle CliHandle, tIsisSysContext * pContext, tIsisLSPEntry * pLSP, UINT2 u2HoldTime,INT4 i4DotCompliance));
UINT2 IsisCliGetHoldTime PROTO ((tIsisSysContext * pContext, tIsisLSPEntry * pLSP));
VOID IsisCliFreeLSPRecords PROTO ((tIsisLSPEntry * pHead));

VOID IsisCliPrintLSPTLVs PROTO ((tCliHandle CliHandle, UINT1 *pPDU,INT4 i4DotCompliance));


VOID IsisCliFormIpAddrStr PROTO((UINT1 *pu1Src, CHR1 *pu1Dest,UINT1));


INT4 IsisCliShowCktInfo PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT1 u1Level));


INT4 IsisCliShowPktStats PROTO ((tCliHandle CliHandle,INT4 i4IfIndex));

VOID IsisCliShowPktCount PROTO ((tCliHandle CliHandle, tIsisCktLevel * pCktLevel, UINT1 u1Level, tIsisCktEntry *pCktRec));

VOID IsisCliNoDebugUpdt PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugDecn PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugAdjn PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugInterface PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugControl PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugTimer PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugFault PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugRtmi PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugDlli PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugTrfr PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugSnmp PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugUtil PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugEntryExit PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugPacketDump PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugCritical PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugPacketDump PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugAll PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugUpdt PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugInterface PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugControl PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugTimer PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugFault PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugRtmi PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugDlli PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugTrfr PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugSnmp PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugUtil PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugDecn PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugAdjn PROTO ((tCliHandle CliHandle));

VOID IsisiCliDebugControl PROTO ((tCliHandle CliHandle));

VOID IsisiCliDebugInterface PROTO ((tCliHandle CliHandle));

VOID IsisiCliDebugTimer PROTO ((tCliHandle CliHandle));

VOID IsisiCliDebugRtmi PROTO ((tCliHandle CliHandle));

VOID IsisiCliDebugDlli PROTO ((tCliHandle CliHandle));

VOID IsisiCliDebugTrfr PROTO ((tCliHandle CliHandle));

VOID IsisiCliDebugSnmp PROTO ((tCliHandle CliHandle));

VOID IsisiCliDebugUtil PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugCritical PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugEntryExit PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugAll PROTO ((tCliHandle CliHandle));

VOID IsisCliDebugDetail PROTO ((tCliHandle CliHandle));

VOID IsisCliNoDebugDetail PROTO ((tCliHandle CliHandle));

VOID IsisCliShowDbgInfo PROTO ((tCliHandle CliHandle));

INT4 IsIsShowRunningConfigInCxt PROTO ((tCliHandle CliHandle,UINT4 u4Module,INT4 i4Instances));

VOID IsIsShowRunningConfigScalarsInCxt PROTO ((tCliHandle CliHandle,INT4 i4Instances));

VOID IsIsShowRunningConfigTablesInCxt PROTO((tCliHandle CliHandle,INT4 i4Instances));


VOID IsIsSysTableInfo PROTO((tCliHandle CliHandle, INT4 i4Instance,UINT4 *pu4PagingStatus));

VOID IsIsAreaTableInfo PROTO((tCliHandle CliHandle, INT4 i4Instance,tSNMP_OCTET_STRING_TYPE *pOctetStr,UINT4 *pu4PagingStatus,UINT1 u1Flag));

VOID IsIsIpraTableInfo PROTO((tCliHandle CliHandle, INT4 i4Instance,INT4 i4IsisIPRAType,INT4 i4IsisIPRAIndex, UINT4 *pu4PagingStatus));

VOID IsIsIpraMetricTableInfo PROTO((tCliHandle CliHandle, INT4 i4Instance,INT4 i4IsisIPRAType,INT4 i4IsisIPRAIndex, UINT4 *pu4PagingStatus));

VOID IsIsCktEntryTableInfo PROTO((tCliHandle CliHandle, INT4 i4Instances,INT4 i4IsisCircIndex, UINT4 *pu4PagingStatus));

VOID IsIsCktLevelTableInfo PROTO((tCliHandle CliHandle, INT4 i4Instances,INT4 i4IsisCircIndex, UINT4 *pu4PagingStatus));

UINT1* IsIsPrintIpMask PROTO((UINT1 u1PrefixLen));

UINT4 IsIsGetSubnetmask PROTO((UINT1 u1Prefixlen));

INT4 IsisCliGetContext PROTO ((tCliHandle CliHandle, UINT4 u4Command,
                               UINT4 * pu4IsisCxtId, INT4 * pi4IfIndex,
                               UINT2 * pu2ShowCmdFlag));

INT4 IsisCliGetCxtIdFromCxtName PROTO ((tCliHandle CliContext, UINT1 * pu1IsisCxtName,
                          UINT4 * pu4IsisCxtId));

VOID IsIsDistanceTableInfo PROTO ((tCliHandle CliHandle));

INT4 IsisCliPassiveInterface PROTO ((tCliHandle CliHandle, INT4 i4IfIndex,UINT4 u4Inst,
                                    UINT4 i4ValIsisCircPassiveCircuit));

INT4 IsisCliPassiveInterfaceDefault PROTO ((tCliHandle CliHandle,UINT4 u4Inst,
                                            UINT4 i4ValIsisCircPassiveCircuit));

INT4 IsisCliSetMetricStyle PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx,
                                   INT4 i4IsisSetMetricStyle));

INT4 IsisIsAllinterfacePassive PROTO ((tIsisSysContext *pContext));

VOID IsIsCktTablePassiveInterfaceInfo PROTO ((tCliHandle CliHandle,tIsisSysContext *pContext));

VOID IsisRouterIsisInfo PROTO ((tCliHandle CliHandle,INT4 i4Inst));
#ifdef RM_WANTED
INT4 IsisCliShowIsisSyncStatus (tCliHandle CliHandle);
INT4 IsisCliShowActualDB(tCliHandle CliHandle,UINT4  u4InstIdx );
INT4 IsisCliShowConfDB(tCliHandle CliHandle, UINT4 u4InstIdx);
#endif

VOID
IsIsRRDImportLevelInfo(tCliHandle CliHandle, INT4 i4RrdImportType, INT4 i4ProtoIndex, INT4 i4Instance);
VOID
IsIsRedistributionInfo (tCliHandle CliHandle, INT4 i4Instance);
#ifdef ROUTEMAP_WANTED
INT4 IsisCliSetDistribute PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1RMapName, UINT1 u1Status));
INT4 IsisSetRouteDistance PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 u1Distance, UINT1 *pu1RMapName));
INT4 IsisSetNoRouteDistance PROTO ((tCliHandle CliHandle, UINT4 u4InstIdx, UINT1 *pu1RMapName));
VOID IsIsRMapFilterTableInfo PROTO ((tCliHandle CliHandle));
#endif /* ROUTEMAP_WANTED */

/* ISIS GR Prototypes*/


INT4 IsisCliSetGRRestartSupport (tCliHandle CliHandle,UINT4 u4InstIdx,INT4 i4IsisGRRestartSupport);
INT4 IsisCliSetGRRestartReason (tCliHandle CliHandle,UINT4 u4InstIdx,INT4 i4IsisGRRestartReason);
INT4 IsisCliSetGRT1Interval (tCliHandle CliHandle,UINT4 u4InstIdx,INT4 i4IsisGRT1TimerInterval);
INT4 IsisCliSetGRT2Interval (tCliHandle CliHandle,UINT4 u4InstIdx,INT4 i4IsisGRT2TimerInterval,UINT1 u1Level);
INT4 IsisCliSetGRT3Interval (tCliHandle CliHandle,UINT4 u4InstIdx,INT4 i4IsisGRT3TimerInterval);
INT4 IsisCliSetGRT1RetryCount (tCliHandle CliHandle,UINT4 u4InstIdx,INT4 i4IsisGRMaxT1RetryCount);
INT4 IsisCliSetGRHelperSupport (tCliHandle CliHandle,UINT4 u4InstIdx,INT4 i4IsisGRHelperSupport);
INT4 IsisCliSetGRHelperMaxGraceTime (tCliHandle CliHandle,UINT4 u4InstIdx,INT4 i4IsisGRHelperGRTimeLimit);
INT4 IsisCliShowRestartStatus (tCliHandle CliHandle,UINT4 u4InstIdx);

/* M-ISIS Prototypes */
INT4 IsisCliSetMultiTopology PROTO ((tCliHandle CliHandle,INT4 i4IfIndex, UINT1 u1IsisMTSupport));

/* BFD for M-ISIS Prototypes */
INT4 IsisCliSetBfdSupport (INT4 i4InstIdx, UINT1 u1IsisBfdSupport);
INT4 IsisCliSetBfdAllIfStatus (INT4 i4InstIdx, UINT1 u1IsisBfdAllIfStatus);
INT4 IsisCliSetBfdIfStatus (INT4 i4IfIndex, UINT1 u1IsisBfdIfStatus);

/*Dynamic Hostname Prototypes*/
INT4 IsisCliSetDynHostname PROTO ((tCliHandle CliHandle, INT4 i4InstIdx,
                         UINT1 u1IsisDynHostNmeSupport));
INT4 is_debug_cmd(UINT4 u4Command);

INT4 IsisConfigRedistribute (tCliHandle CliHandle, INT4 i4RedisProto, UINT1 u1Set,
              UINT1 *pu1RMapName, UINT4 u4Metric, INT4 i4ImportType,INT4 i4IfIndex);

INT4 IsisCliSetAuthMode PROTO ((tCliHandle CliHandle,UINT4 u4InstIdx,UINT4 u4AuthType,UINT1 u1Level));
INT4 IsisCliDelAuthMode PROTO ((tCliHandle CliHandle,UINT4 u4InstIdx,UINT1 u1Level));
INT4 IsisCliSetIfAuthMode PROTO ((tCliHandle CliHandle,UINT4 u4InstIdx,UINT4 u4AuthType,UINT1 u1Level));
INT4 IsisCliDelIfAuthMode PROTO ((tCliHandle CliHandle,UINT4 u4InstIdx,UINT1 u1Level));
 


#endif /* __ISCLIPT_H__ */
