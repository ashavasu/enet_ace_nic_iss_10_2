/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved]
*
* $Id: evcproprot.h,v 1.5 2011/09/24 06:58:39 siva Exp $
* *******************************************************************/
#ifndef _EVCPROPROT_H
#define _EVCPROPROT_H

INT4 EvcProLock (VOID);
INT4 EvcProUnLock (VOID);

INT4 EvcProInit (VOID);
INT4 EvcProTaskInit (VOID);
VOID EvcProTaskDeinit (VOID);

INT4 EvcProInitialize(VOID);
VOID EvcproAssignMemPoolsId(VOID);

INT4 EvcProModuleInit(VOID);
VOID EvcProModuleShutdown (VOID);
INT4 EvcProModuleEnable (VOID);
INT4 EvcProModuleDisable (VOID);

VOID EvcProGlobalTrace (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...);
VOID EvcProGlobalDebug (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...);
INT4 EvcProSelectContext (UINT4 u4ContextId);
INT4 EvcProReleaseContext (VOID);
INT4 EvcProGlobalMemoryInit(VOID);

INT4 EvcProCreateEvc(UINT1 *pu1EvcId);
INT4 EvcProDeleteEvc(UINT1 *pu1EvcId);
INT4 EvcProUpdateEvcAttributes(VOID);
INT4 EvcProSetDefaultEvc(VOID);

#endif
