/*********** * Copyright (C) 2006 Aricent Inc . All Rights Reserved] **
 * $Id: evcproinc.h,v 1.7 2011/09/24 06:58:39 siva Exp $       **
 * Description: This file contains all the header files to be        ** 
 *              included and used by EVCPRO Module.                  ***/
#ifndef _EVCPROHDRS_H
#define _EVCPROHDRS_H

#include "lr.h"
#include "cfa.h"
#include "snmctdfs.h"
#include "snmccons.h" 
#include "fssyslog.h"
#include "snmputil.h" 
#include "fsvlan.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "l2iwf.h"
#include "iss.h"
#include "cli.h"
#include "vcm.h"

#include "evcpro.h"
#include "elm.h"
#include "lcm.h"
#include "uniglobcli.h"

#include "evcproconst.h"
#include "evcpromacr.h"
#include "evcproprot.h"
#include "evcprocli.h"
#include "evcprotdfs.h"
#include "eprosz.h"
#include "evcproglob.h"
#include "uni.h"
#include "uniinc.h"

#endif
