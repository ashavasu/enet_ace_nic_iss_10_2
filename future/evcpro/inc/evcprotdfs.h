/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: evcprotdfs.h,v 1.3 2010/07/29 09:20:16 prabuc Exp $
 *
 * Description: This file contains data structures defined for
 *              EVCPRO module.
 *********************************************************************/

#ifndef _EVCPROTDFS_H
#define _EVCPROTDFS_H

/**************************************************************/
/*                For EVC Provisioning Module                 */
/******************************************************************************/
/*                             Global Structure                               */
/******************************************************************************/

typedef struct EvcProContextInfo tEvcProContextInfo;

typedef tCRU_BUF_CHAIN_HEADER     tEvcProBufChainHeader;
typedef tCRU_INTERFACE            tEvcProInterface;
typedef tTMO_SLL                  tEvcProSll;
typedef tTMO_SLL_NODE             tEvcProSllNode;
typedef UINT4                     tEvcProLocalMsgType;
typedef tCfaIfInfo                tEvcProCfaIfInfo;
typedef tMacAddr                  tEvcProMacAddr;




struct EvcProContextInfo {
   /*tEvcProBridgeEntry      BridgeEntry; */      /* Bridge Information that
                                             * is common irrespective
                                             * of the number of 
                                             * spanning trees 
                                             * supported 
                                             */
#ifdef MI_WANTED
   tRBTree              PerContextIfIndexTable;
#endif
   
   UINT4                u4ContextId;

   UINT4                u4TraceOption;      /* For enabling traces 
                                             */

   UINT4                u4DebugOption;      /* For enabling debug 
                                             * statements 
                                             */
   UINT4                u4ContextTrapOption;
                                            /* To set the traps 
                                             */
   UINT1                u1EvcProGenTrapType;
                                            /* Indicate the general trap set 
                                             */
   UINT1                u1GlobalModuleStatus;  
                                            /* Stores the protocol Admin status 
                                             */ 
   UINT1                u1SystemControl; 
                                        
   UINT1                au1Reserved[1];
};

typedef struct EvcProGlobalInfo 
{
   tEvcProQId           InputQId;   /* QueueId of the EVCI Task */

   tEvcProQId           CfgQId;     /* QueueId of the EVCI Configuration
                                  * Queue */
   tEvcProTaskId        TaskId;     /* EVCI Task Id */
   tEvcProOsixSemId     SemId;      /* EVCI Sem4 Id */
   
   tEvcProMemPoolId     ContextMemPoolId;   /* Memory Pool Id of
                                          * the pool used for 
                                          * allocating memory 
                                          * block for ContextInfo
                                          */


   tEvcProMemPoolId     LocalMsgMemPoolId;  /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Local
                                          * SNMP messages 
                                          */
   tEvcProMemPoolId     QMsgMemPoolId;      /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Messages 
                                          * posted to EVCI Q 
                                          */
   tEvcProMemPoolId     CfgQMsgMemPoolId;   /* Memory Pool Id of
                                          * the pool used for
                                          * allocating memory
                                          * blocks for Messages
                                          * posted to EVCI CFG Q 
                                          */

   tRBTree                  GlobalIfIndexTable;
   
   tEvcProContextInfo          *apContextInfo [EVCPRO_MAX_CONTEXTS];

   UINT4            u4GlobalTrapOption;  /* To set the traps 
                                              */
   UINT4            u4GlobalTraceOption; /* To set the global trace 
                                              */
   UINT4            u4GlobalDebugOption; /* To set the global debug 
                                              */
   UINT4            u4BufferFailureCount;
                                             /* Number of times Buffer 
                                              * Failure has occured 
                                              */
   UINT4            u4MemoryFailureCount;
                                             /* Number of times Memory 
                                              * Failure has occured 
                                              */
}tEvcProGlobalInfo;


typedef struct EvcProMsgNode
{
    tEvcProLocalMsgType    MsgType;
    UINT4               u4ContextId;
    UINT4               u4BridgeMode;
}tEvcProMsgNode;

typedef struct EvcProQMsg {
   tEvcProLocalMsgType      LocalMsgType;
   union {
      tEvcProMsgNode        *pMsgNode;
      tEvcProBufChainHeader *pPduInQ;
   }uQMsg;

}tEvcProQMsg;

/**************************************************************/

#endif
