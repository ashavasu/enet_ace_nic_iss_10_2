#ifndef _EVCPROCLI_H
#define _EVCPROCLI_H


INT4    CliHandleEvcCreate(UINT1 *pu1EvcId);
INT4    CliHandleEvcDelete(UINT1 *pu1EvcId);
INT4    CliHandleUniCount(UINT1 *pu1EvcId ,UINT2 u2UniCount,UINT4 u4UniType);
INT4    CliHandleCeVlanMap(UINT2 u2PortNo,UINT1 *pu1EvcId,UINT2 u2CeVlanId,UINT4 u4CeVlanType, UINT4 u4CommandType);
INT4    CliHandleEvcBwProfile(UINT1 *pu1EvcId,UINT4 u4Cm,UINT4 u4Cf,UINT4 u4PerCosBit,
                              UINT4 u4CirMagnitude,UINT4 u4CirMultiplier,UINT4 u4CbsMagnitude,
                              UINT4 u4CbsMultiplier,UINT4 u4EirMagnitude,UINT4 u4EirMultiplier,
                              UINT4 u4EbsMagnitude,UINT4 u4EbsMultiplier,UINT4 u4PriorityBits);
INT4    CliHandleEvcStatusChange(UINT1 *pu1EvcId,UINT4 u4EvcStatus);

typedef struct EvcProCliInfo
{
    UINT1    EvcName[100];
    UINT4    u4ServiceInstanceNo;
}tEvcProPromptInfo;


#endif
