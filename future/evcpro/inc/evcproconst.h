/******************************************************************** * Copyright (C) 2006 Aricent Inc . All Rights Reserved] * * $Id: evcproconst.h,v 1.4 2013/05/06 11:56:41 siva Exp $ * * Description: This file contains all the constants used by *              EVC module. * *******************************************************************/
#ifndef _EVCPROCONST_H
#define _EVCPROCONST_H

#define     MAX_NUM_EVC_SUPPORTED               1000

#define     EVCPRO_MAP_VLAN                     1 
#define     EVCPRO_NO_MAP_VLAN                  2

#define     EVCPRO_PDU_EVENT                       0x0001
#define     EVCPRO_MSG_EVENT                       0x0002

#define     EVCPRO_INIT_VAL                     0
#define     EVCPRO_MAX_CONTEXTS                SYS_DEF_MAX_NUM_CONTEXTS 
#define     EVCPRO_INVALID_CONTEXT                 (EVCPRO_MAX_CONTEXTS + 1)
#define     EVCPRO_CONTEXT_MEMPOOL_ID              gEvcProGlobalInfo.ContextMemPoolId

#define     tEvcProTaskId                          tOsixTaskId
#define     tEvcProMemPoolId                       tMemPoolId
#define     tEvcProOsixSemId                       tOsixSemId
#define     tEvcProQId                             tOsixQId

#define     EVCPRO_MUT_EXCL_SEM_NAME               (const UINT1 *) "EVC" 
#define     EVCPRO_GLOBAL_TRC                      EvcProGlobalTrace
#define     EVCPRO_GLOBAL_DBG                      EvcProGlobalDebug

#define     EVCPRO_SEM_INIT_COUNT                  1
#define     EVCPRO_SNMP_CONFIG_QMSG                1
#define     EVCPRO_PDU_RCVD_QMSG                   2

#define     EVCPRO_ALLOC_CRU_BUF                   CRU_BUF_Allocate_MsgBufChain
#define     EVCPRO_RELEASE_CRU_BUF                 CRU_BUF_Release_MsgBufChain
#define     EVCPRO_CRU_SUCCESS                     CRU_SUCCESS
#define     EVCPRO_CRU_FAILURE                     CRU_FAILURE

#define     EVCPRO_AVG_LOCAL_MSGS_PER_PORT         10
#define     EVCPRO_QMSG_MEMBLK_SIZE                (sizeof(tEvcProQMsg))
#define     EVCPRO_LOCAL_MSG_MEMBLK_SIZE           (sizeof(tEvcProMsgNode))
#define     EVCPRO_CFGQ_MSG_MEMBLK_SIZE            (sizeof(tEvcProQMsg))
#define     EVCPRO_QMSG_MEMBLK_COUNT               (EVCPRO_QUEUE_DEPTH)
#define     EVCPRO_CFGQ_MSG_MEMBLK_COUNT           (EVCPRO_CFG_Q_DEPTH)
#define     EVCPRO_LOCAL_MSG_MEMBLK_COUNT          ((SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG ) \
                                                 * EVCPRO_AVG_LOCAL_MSGS_PER_PORT )

#define     EVCPRO_QMSG_MEMPOOL_ID                 gEvcProGlobalInfo.QMsgMemPoolId
#define     EVCPRO_LOCALMSG_MEMPOOL_ID             gEvcProGlobalInfo.LocalMsgMemPoolId
#define     EVCPRO_CFG_QMSG_MEMPOOL_ID             gEvcProGlobalInfo.CfgQMsgMemPoolId

#define     EVCPRO_FAILURE                         SNMP_FAILURE
#define     EVCPRO_SUCCESS                         SNMP_SUCCESS
#define     EVCPRO_FALSE                           0
#define     EVCPRO_TRUE                            1
#define     EVCPRO_START                           1
#define     EVCPRO_SHUTDOWN                        2
#define     EVCPRO_ENABLED                         1
#define     EVCPRO_DISABLED                        0


#define     EVCPRO_INPUT_QID                       gEvcProGlobalInfo.InputQId
#define     EVCPRO_CFG_QID                         gEvcProGlobalInfo.CfgQId
#define     EVCPRO_TASK_ID                         gEvcProGlobalInfo.TaskId

#define     EVCPRO_GLOBAL_TRC                      EvcProGlobalTrace
#define     EVCPRO_GLOBAL_DBG                      EvcProGlobalDebug
#define     EVCPRO_DEFAULT_CONTEXT                 L2IWF_DEFAULT_CONTEXT
#define     EVCPRO_CONTEXT_STR_LEN                 64

/* For Traces */
#define   EVCPRO_INIT_SHUT_TRC      INIT_SHUT_TRC
#define   EVCPRO_MGMT_TRC           MGMT_TRC
#define   EVCPRO_DATA_PATH_TRC      DATA_PATH_TRC
#define   EVCPRO_CONTROL_PATH_TRC   CONTROL_PLANE_TRC
#define   EVCPRO_DUMP_TRC           DUMP_TRC
#define   EVCPRO_OS_RESOURCE_TRC    OS_RESOURCE_TRC
#define   EVCPRO_ALL_FAILURE_TRC    ALL_FAILURE_TRC
#define   EVCPRO_BUFFER_TRC         BUFFER_TRC

#endif
