#ifndef _CFAUNIINC_H
#define _CFAUNIINC_H

#define CFA_GET_PORTENTRY(u2PortNo) \
        ((u2PortNo <=0)||(u2PortNo>CFA_MAX_NUM_PORTS_PER_CONTEXT)?NULL:(&(gCfaGlobalInfo.aContextInfo[0].aPortEntry[u2PortNo - 1])))

#define CFA_UNUSED(x)                   {x = x;}
#define CFA_MAX_NUM_PORTS_PER_CONTEXT   64
#define CFA_MAX_CONTEXTS                1
#define CFA_INIT_VAL                    0

typedef struct CfaCliInfo
{
    UINT1    EvcName[100];
    UINT4    u4ServiceInstanceNo;
}tCfaPromptInfo;

/**************************************************************/
/*                  For Cfa                                   */



typedef struct CfaPortEntry
{
    tCfaUniInfo             CfaUniInfo;
    UINT2                   u2PortNo;
    UINT1                   au1Reserved[2];
}tCfaPortEntry;

typedef struct CfaContextInfo {
    tCfaPortEntry            aPortEntry[CFA_MAX_NUM_PORTS_PER_CONTEXT];
}tCfaContextInfo;


typedef struct CfaGlobalInfo {
   tCfaContextInfo         aContextInfo [CFA_MAX_CONTEXTS];
}tCfaGlobalInfo;
/**************************************************************/

#ifdef _CFA_UNI_C
tCfaGlobalInfo       gCfaGlobalInfo;
#else
extern tCfaGlobalInfo       gCfaGlobalInfo;
#endif

INT4 CfaCreateUni(UINT1 *pu1UniId, UINT2 u2PortNo);
INT4 CfaHandleCliUpdateUniInfo(UINT1 u1BundlingType, UINT2 u2PortNo,UINT4 
u4BundlingValue);

INT4 CfaDeleteUni(UINT1 *pu1UniId, UINT2 u2PortNo);
INT4 CfaInitialize(VOID);
VOID  CfaSetUniBwProfile(UINT2 u2PortNo,UINT1 *pu1EvcId,UINT4 u4Cm,UINT4 u4Cf,UINT4 u4PerCosBit,
                   UINT4 u4CirMagnitude,UINT4 u4CirMultiplier,UINT4 u4CbsMagnitude,
                   UINT4 u4CbsMultiplier,UINT4 u4EirMagnitude,UINT4 u4EirMultiplier,
                   UINT4 u4EbsMagnitude,UINT4 u4EbsMultiplier,UINT4 u4PriorityBits);


INT4    CliHandleUniCreate( UINT1 *pu1UniId,UINT2 u2PortNo);
INT4    CliHandleUniDelete( UINT1 *pu1UniId,UINT2 u2PortNo);
INT4    CliHandleUniBundling(UINT2 u2PortNo, UINT1 u1BundlingType,UINT4 u4Bundlingvalue);
INT4    CliHandleUniBwProfile(UINT2 u2PortNo,UINT1 *pu1EvcId,UINT4 u4Cm,UINT4 u4Cf,
                              UINT4 u4PerCosBit,UINT4 u4CirMagnitude,
                              UINT4 u4CirMultiplier,UINT4 u4CbsMagnitude,UINT4 u4CbsMultiplier,   
                              UINT4 u4EirMagnitude,UINT4 u4EirMultiplier,UINT4 u4EbsMagnitude,     
                              UINT4 u4EbsMultiplier,UINT4 u4PriorityBits);

#endif
