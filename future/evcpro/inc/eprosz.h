/*$Id: eprosz.h,v 1.2 2011/09/24 06:58:39 siva Exp $*/
enum {
    MAX_EVCPRO_CONFIG_MESG_SIZING_ID,
    MAX_EVCPRO_LOCAL_MESG_SIZING_ID,
    MAX_EVCPRO_Q_MESG_SIZING_ID,
    EVCPRO_MAX_SIZING_ID
};
INT4 IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams);


#ifdef  _EVCPROSZ_C
tMemPoolId EVCPROMemPoolIds[ EVCPRO_MAX_SIZING_ID];
INT4  EvcproSizingMemCreateMemPools(VOID);
VOID  EvcproSizingMemDeleteMemPools(VOID);
INT4  EvcproSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _EVCPROSZ_C  */
extern tMemPoolId EVCPROMemPoolIds[ ];
extern INT4  EvcproSizingMemCreateMemPools(VOID);
extern VOID  EvcproSizingMemDeleteMemPools(VOID);
extern INT4  EvcproSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _EVCPROSZ_C  */


#ifdef  _EVCPROSZ_C
tFsModSizingParams FsEVCPROSizingParams [] = {
{ "tEvcProQMsg", "MAX_EVCPRO_CONFIG_MESG", sizeof(tEvcProQMsg),MAX_EVCPRO_CONFIG_MESG, MAX_EVCPRO_CONFIG_MESG,0 },
{ "tEvcProMsgNode", "MAX_EVCPRO_LOCAL_MESG", sizeof(tEvcProMsgNode),MAX_EVCPRO_LOCAL_MESG, MAX_EVCPRO_LOCAL_MESG,0 },
{ "tEvcProQMsg", "MAX_EVCPRO_Q_MESG", sizeof(tEvcProQMsg),MAX_EVCPRO_Q_MESG, MAX_EVCPRO_Q_MESG,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _EVCPROSZ_C  */
extern tFsModSizingParams FsEVCPROSizingParams [];
#endif /*  _EVCPROSZ_C  */


