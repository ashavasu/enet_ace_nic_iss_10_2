/* $Id:						*/

#ifndef _EVCPROPROGLOB_H
#define _EVCPROPROGLOB_H

#ifdef _EVCPROSYS_C
tEvcProContextInfo              *gpEvcProContextInfo;
UINT4                           gEvcProCruAllocatedMemory;
UINT1                           gu1IsEvcProInitialised = EVCPRO_FALSE;
UINT1                           gau1EvcProSystemControl [EVCPRO_MAX_CONTEXTS];
UINT1                           gau1EvcProModuleStatus [EVCPRO_MAX_CONTEXTS];
tEvcProGlobalInfo               gEvcProGlobalInfo;
tEvcProContextInfo              gEvcProContextInfo;
#else
extern tEvcProContextInfo              *gpEvcProContextInfo;
extern UINT4                           gEvcProCruAllocatedMemory;
extern UINT1                           gu1IsEvcProInitialised;
extern UINT1                           gau1EvcProSystemControl [EVCPRO_MAX_CONTEXTS];
extern UINT1                           gau1EvcProModuleStatus [EVCPRO_MAX_CONTEXTS];
extern tEvcProGlobalInfo               gEvcProGlobalInfo;
extern tLcmPromptInfo                  LcmPromptInfo;
extern tEvcProContextInfo              gEvcProContextInfo;
#endif
#endif
