/******************************************************************** 
* Copyright (C) 2006 Aricent Inc . All Rights Reserved] * 
* $Id: evcpromacr.h,v 1.4 2010/08/03 11:45:12 prabuc Exp $ * 
* Description: This file contains all the macros used by EVC Module. * 
*******************************************************************/
#ifndef _EVCPROMACR_H_
#define _EVCPROMACR_H_

/******************************************************************************/
/*                             MACROS                                         */
/******************************************************************************/


#define     EVCPRO_CREATE_SEM(pu1Sem, u4Count, u4Flags, pSemId) \
                OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))
  
#define     EVCPRO_DELETE_SEM  OsixDeleteSem

#define     EVCPRO_TAKE_SEM  OsixSemTake

#define     EVCPRO_GIVE_SEM  OsixSemGive

#define     EVCPRO_UNUSED(x)                   {x = x;}
#define     EVCPRO_CREATE_QUEUE                OsixQueCrt
#define     EVCPRO_DELETE_QUEUE                OsixQueDel
#define     EVCPRO_GET_TASK_ID                 OsixGetTaskId
#define     EVCPRO_SEND_TO_QUEUE               OsixQueSend
#define     EVCPRO_RECV_FROM_QUEUE             OsixQueRecv
#define     EVCPRO_RECEIVE_EVENT               OsixEvtRecv
#define     EVCPRO_OSIX_EV_ANY                 OSIX_EV_ANY
#define     EVCPRO_OSIX_NO_WAIT                OSIX_NO_WAIT
#define     EVCPRO_DEF_MSG_LEN                 OSIX_DEF_MSG_LEN 

#define     EVCPRO_GET_PDU_FROM_EVC_QUEUE      EVCPRO_RECV_FROM_QUEUE
#define     EVCPRO_TASK_INPUT_QNAME            EVCPRO_QUEUE_NAME
#define     EVCPRO_SELF                        SELF
#define     EVCPRO_IS_EVC_INITIALISED()            gu1IsEvcProInitialised
#define     EVCPRO_OSIX_NO_WAIT                OSIX_NO_WAIT
#define     EVCPRO_QMSG_TYPE(pMsg)             pMsg->LocalMsgType
#define     EVCPRO_MEMCPY                      MEMCPY
#define     EVCPRO_MEMCMP                      MEMCMP
#define     EVCPRO_MEMSET                      MEMSET
#define     EVCPRO_SPRINTF                     SPRINTF
#define     EVCPRO_CURR_CONTEXT_INFO()         gpEvcProContextInfo
#define     EVCPRO_MEMORY_TYPE                 MEM_DEFAULT_MEMORY_TYPE
#define     EVCPRO_CREATE_LOCALMSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
                MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                (UINT4)(EVCPRO_MEMORY_TYPE),(tEvcProMemPoolId*)(pPoolId))
#define     EVCPRO_CREATE_QMSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
                MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                (UINT4)(EVCPRO_MEMORY_TYPE),(tEvcProMemPoolId*)(pPoolId))
#define     EVCPRO_CREATE_CFGQ_MSG_MEM_POOL(BlockSize,NumOfBlocks,pPoolId) \
                MemCreateMemPool((UINT4)(BlockSize),(UINT4)(NumOfBlocks), \
                (UINT4)(EVCPRO_MEMORY_TYPE),(tEvcProMemPoolId*)(pPoolId))
#define     EVCPRO_SYSTEM_CONTROL \
                gau1EvcProSystemControl[EVCPRO_CURR_CONTEXT_ID()]
#define     EVCPRO_CURR_CONTEXT_ID() \
                (EVCPRO_CURR_CONTEXT_INFO ())->u4ContextId

#define     EVCPRO_TRC_FLAG() \
                (EVCPRO_CURR_CONTEXT_INFO ())->u4TraceOption

#define     EVCPRO_DELETE_LOCALMSG_MEM_POOL(PoolId) \
                MemDeleteMemPool(PoolId)
#define     EVCPRO_DELETE_QMSG_MEM_POOL(PoolId) \
                MemDeleteMemPool(PoolId)
#define     EVCPRO_DELETE_CFGQ_MSG_MEM_POOL(PoolId) \
                MemDeleteMemPool(PoolId)

#define     EVCPRO_RELEASE_LOCALMSG_MEM_BLOCK(pNode) \
                MemReleaseMemBlock(EVCPRO_LOCALMSG_MEMPOOL_ID, (UINT1 *)pNode)
#define     EVCPRO_RELEASE_QMSG_MEM_BLOCK(pNode) \
                MemReleaseMemBlock(EVCPRO_QMSG_MEMPOOL_ID, (UINT1 *)pNode)
#define     EVCPRO_RELEASE_CFGQ_MSG_MEM_BLOCK(pNode) \
                MemReleaseMemBlock(EVCPRO_CFG_QMSG_MEMPOOL_ID, (UINT1 *)pNode)
#define     EVCPRO_CREATE_MEM_POOL(BlockSize, NumOfBlocks, MemoryType, pPoolId) \
                MemCreateMemPool((UINT4)BlockSize,(UINT4)NumOfBlocks, \
                MemoryType, (tEvcProMemPoolId *)(pPoolId))
#define     EVCPRO_DELETE_MEM_POOL(pPoolId)    MemDeleteMemPool(pPoolId)

#define EVCPRO_QMSG_TYPE(pMsg) pMsg->LocalMsgType

#define     EVCPRO_GET_PDU_FROM_EVCPRO_QUEUE      EVCPRO_RECV_FROM_QUEUE

#define     EVCPRO_CONTEXT_INFO(u4ContextId) \
                gEvcProGlobalInfo.apContextInfo[(u4ContextId)]

#endif

