/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved]
*
* $Id: evcpromsg.c,v 1.7 2013/12/16 15:26:30 siva Exp $
*
* Description: This file contains functions pertaining to the 
*              Message Processing Module which includes enqueued
*              PDUs and local SNMP messages and Task Initilisation
*              Routines.
* *******************************************************************/

#include "evcproinc.h"
/*****************************************************************************/
/* Function Name      : EvcProInit                                              */
/*                                                                           */
/* Description        : This function is called during system startup.       */
/*                      It calls routine to allocate resuorces such as Sems  */
/*                      Queus and mempools and also calls routine to create  */
/*                      default context                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS/EVCPRO_FAILURE                              */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : BridgeInitializeProtocol                             */
/*****************************************************************************/
INT4
EvcProInit (VOID)
{

    EVCPRO_MEMSET (&gEvcProGlobalInfo, EVCPRO_INIT_VAL,
                   sizeof (tEvcProGlobalInfo));

    if (EVCPRO_TASK_ID == (tEvcProTaskId) EVCPRO_INIT_VAL)
    {
        /* Initialize task related memory.
         * Create Sems, Queues, and mempools for local messages.
         */
        if (EvcProTaskInit () != (INT4) EVCPRO_SUCCESS)
        {
            return EVCPRO_FAILURE;
        }

    }
    EVCPRO_IS_EVC_INITIALISED () = EVCPRO_TRUE;
    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProTaskInit                                          */
/*                                                                           */
/* Description        : This function creates the semaphores, configuration  */
/*                      message queue, PDU message queue and also creates    */
/*                      the Memory Pool for the created queues               */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                            */
/*****************************************************************************/

INT4
EvcProTaskInit (VOID)
{

    /*EVCPRO_GLOBAL_TRC (EVCPRO_DEFAULT_CONTEXT,EVCPRO_INIT_SHUT_TRC,
       "MSG: Spawning EVCPRO Task...\n"); */

    if (EVCPRO_CREATE_SEM (EVCPRO_MUT_EXCL_SEM_NAME, EVCPRO_SEM_INIT_COUNT, 0,
                           &gEvcProGlobalInfo.SemId) != OSIX_SUCCESS)
    {
        /*EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,EVCPRO_ALL_FAILURE_TRC,
           "MSG: Failed to Create EVCPRO Mutual Exclusion Semaphore \n"); */
        return EVCPRO_FAILURE;
    }

    /* To create all memory pools required for the module */
    if (EvcproSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        EvcProTaskDeinit ();
        return EVCPRO_FAILURE;
    }
    EvcproAssignMemPoolsId ();

    if (EVCPRO_CREATE_QUEUE (EVCPRO_TASK_INPUT_QNAME, OSIX_MAX_Q_MSG_LEN,
                             EVCPRO_QUEUE_DEPTH,
                             &(EVCPRO_INPUT_QID)) != OSIX_SUCCESS)
    {
        /*EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,EVCPRO_ALL_FAILURE_TRC,
           "MSG: Failed To Create Message EVCPRO Module Input Queue \n"); */
        EvcProTaskDeinit ();
        return EVCPRO_FAILURE;
    }

    if (EVCPRO_CREATE_QUEUE (EVCPRO_CFG_QUEUE, OSIX_MAX_Q_MSG_LEN,
                             EVCPRO_CFG_Q_DEPTH,
                             &(EVCPRO_CFG_QID)) != OSIX_SUCCESS)
    {
        /*EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,EVCPRO_ALL_FAILURE_TRC,
           "MSG: Failed To Create Message EVCPRO Module CFG Queue \n"); */
        EvcProTaskDeinit ();
        return EVCPRO_FAILURE;
    }
    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProTaskDeinit                                        */
/*                                                                           */
/* Description        : This function deletes the local message memory pool. */
/*                      It releases all the stored/buffered messages in the  */
/*                        queue and then deletes the Queue.                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                            */
/*****************************************************************************/
VOID
EvcProTaskDeinit (VOID)
{
    tEvcProQMsg        *pQMsg = NULL;
    tEvcProBufChainHeader *pPdu = NULL;

    /*EVCPRO_GLOBAL_TRC (EVCPRO_DEFAULT_CONTEXT,EVCPRO_INIT_SHUT_TRC,
       "MSG: De-Initializing EVCPRO Task module...\n"); */

    if (EVCPRO_INPUT_QID != (tEvcProQId) EVCPRO_INIT_VAL)
    {
        /* Before DeInitializing the task, remove and release 
         * all messages in the Queue */
        while (EVCPRO_GET_PDU_FROM_EVC_QUEUE (EVCPRO_INPUT_QID,
                                              (UINT1 *) &pQMsg,
                                              EVCPRO_DEF_MSG_LEN,
                                              OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            if (pQMsg == NULL)
            {
                continue;
            }
            /* Processing of PDUs */
            if (EVCPRO_QMSG_TYPE (pQMsg) == EVCPRO_PDU_RCVD_QMSG)
            {
                pPdu = pQMsg->uQMsg.pPduInQ;
                if (EVCPRO_RELEASE_CRU_BUF (pPdu, EVCPRO_FALSE) ==
                    EVCPRO_CRU_FAILURE)
                {
                    /*EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,EVCPRO_ALL_FAILURE_TRC,
                       "MSG: Received CRU Buffer Release FAILED -");
                       EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,EVCPRO_ALL_FAILURE_TRC,
                       "in EVCPRO Task DeInit!\n"); */
                }
            }

            if (EVCPRO_RELEASE_QMSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
            {
                /*EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,
                   EVCPRO_ALL_FAILURE_TRC | EVCPRO_MGMT_TRC,
                   "MSG: Release of Local Msg Memory Block FAILED!\n"); */
            }
            pQMsg = NULL;
        }

        EVCPRO_DELETE_QUEUE (EVCPRO_INPUT_QID);
        EVCPRO_INPUT_QID = (tEvcProQId) EVCPRO_INIT_VAL;
    }

    if (EVCPRO_CFG_QID != (tEvcProQId) EVCPRO_INIT_VAL)
    {
        /* Before DeInitializing the task, remove and release 
         * all messages in the Configuration Queue */
        while (EVCPRO_RECV_FROM_QUEUE (EVCPRO_CFG_QID,
                                       (UINT1 *) &pQMsg,
                                       EVCPRO_DEF_MSG_LEN,
                                       OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            if (pQMsg == NULL)
            {
                continue;
            }

            if (EVCPRO_QMSG_TYPE (pQMsg) == EVCPRO_SNMP_CONFIG_QMSG)
            {
                if (EVCPRO_RELEASE_LOCALMSG_MEM_BLOCK
                    (pQMsg->uQMsg.pMsgNode) != MEM_SUCCESS)
                {
                    /*EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,
                       EVCPRO_ALL_FAILURE_TRC | EVCPRO_MGMT_TRC,
                       "MSG: Release of Local Msg Memory Block "
                       "FAILED!\n"); */

                }

                if (EVCPRO_RELEASE_CFGQ_MSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
                {
                    /*EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,
                       EVCPRO_ALL_FAILURE_TRC | EVCPRO_MGMT_TRC,
                       "MSG: Release of CFG Q Msg Memory Block "
                       "FAILED!\n"); */

                }
            }
        }

        EVCPRO_DELETE_QUEUE (EVCPRO_CFG_QID);
        EVCPRO_CFG_QID = (tEvcProQId) EVCPRO_INIT_VAL;
    }
    /* To Delete all memory pools used by the module */
    EvcproSizingMemDeleteMemPools ();
    EVCPRO_DELETE_SEM (EVCPRO_SELF, EVCPRO_MUT_EXCL_SEM_NAME);
    return;
}

/*****************************************************************************/
/* Function Name      : EvcProTaskMain                                          */
/*                                                                           */
/* Description        : This is the main entry point function for the EvcPro    */
/*                      Task. This waits continuously in a loop to receive   */
/*                      events that are posted to this task and it then calls*/
/*                      the corresponding functions to process the events.   */
/*                                                                           */
/* Input(s)           : pi1Param - Pointer to the parameter value that can be*/
/*                                 passed to this task entry point function. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
EvcProTaskMain (INT1 *pi1Param)
{
    UINT4               u4Events = EVCPRO_INIT_VAL;
    tEvcProQMsg        *pQMsg = NULL;
    EVCPRO_UNUSED (pi1Param);

    /* Create Sems, Queues, Mempools and the default context */
    if (EvcProInit () == EVCPRO_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (EvcProSelectContext (EVCPRO_DEFAULT_CONTEXT) == EVCPRO_FAILURE)
    {
        EvcProTaskDeinit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (EvcProGlobalMemoryInit () == EVCPRO_FAILURE)
    {
        EvcProTaskDeinit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }
    (EVCPRO_CURR_CONTEXT_INFO ())->u4TraceOption = 0xffffffff;

    EvcProReleaseContext ();

    if (EVCPRO_GET_TASK_ID
        (EVCPRO_SELF, (UINT1 *) EVCPRO_TASK_NAME,
         &(EVCPRO_TASK_ID)) != OSIX_SUCCESS)
    {
        EvcProReleaseContext ();
        EvcProTaskDeinit ();
        lrInitComplete (OSIX_FAILURE);
    }

    /* Indicate the status of initialization to the main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (EVCPRO_TRUE)
    {

        if (EVCPRO_RECEIVE_EVENT
            (EVCPRO_TASK_ID, EVCPRO_PDU_EVENT | EVCPRO_MSG_EVENT,
             (UINT4) OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if (u4Events & EVCPRO_PDU_EVENT)
            {
                while (EVCPRO_GET_PDU_FROM_EVC_QUEUE (EVCPRO_INPUT_QID,
                                                      (UINT1 *) &pQMsg,
                                                      EVCPRO_DEF_MSG_LEN,
                                                      OSIX_NO_WAIT)
                       == OSIX_SUCCESS)
                {
                    /* Processing of PDUs */
                    if ((pQMsg == NULL) ||
                        (EVCPRO_QMSG_TYPE (pQMsg) != EVCPRO_PDU_RCVD_QMSG))
                    {
                        continue;
                    }

                    EVCPRO_LOCK ();

                    /* Handle the packet */

                    EVCPRO_UNLOCK ();

                    if (EVCPRO_RELEASE_QMSG_MEM_BLOCK (pQMsg) != MEM_SUCCESS)
                    {
                        /*EVCPRO_GLOBAL_TRC (EVCPRO_INVALID_CONTEXT,
                           EVCPRO_ALL_FAILURE_TRC | EVCPRO_MGMT_TRC,
                           "MSG: Release of Local Msg Memory Block"
                           "FAILED!\n"); */

                    }

                    /* EVCPRO_GLOBAL_TRC (EVCPRO_DEFAULT_CONTEXT, EVCPRO_INIT_SHUT_TRC,
                       "MSG: PDU Received Event processed.\n"); */
                }
            }

            /*EVCPRO_GLOBAL_TRC (EVCPRO_DEFAULT_CONTEXT, EVCPRO_EVENT_HANDLING_TRC,
               "MSG: Completed processing the event(s).\n"); */

        }                        /* End of Receive Event If Loop */
    }                            /* End of While Loop */

}

/*****************************************************************************/
/* Function Name      : EvcProGlobalMemoryInit                               */
/*                                                                           */
/* Description        : This function initializes the global memory          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_FAILURE/EVCPRO_SUCCESS                        */
/*****************************************************************************/
INT4
EvcProGlobalMemoryInit (VOID)
{
    /* In the case of SI, all memory pools related to protocol data structures
     * will be intialised only when the Module is STARTED.
     *
     * Hence allocate memory for the default context alone and return. */
    MEMSET (&gEvcProContextInfo, EVCPRO_INIT_VAL, sizeof (tEvcProContextInfo));
    EVCPRO_CURR_CONTEXT_INFO () = &gEvcProContextInfo;

    if (EVCPRO_CURR_CONTEXT_INFO () == NULL)
    {
        return EVCPRO_FAILURE;
    }
    EVCPRO_CONTEXT_INFO (EVCPRO_DEFAULT_CONTEXT) = EVCPRO_CURR_CONTEXT_INFO ();
    MEMSET (EVCPRO_CURR_CONTEXT_INFO (), EVCPRO_INIT_VAL,
            sizeof (tEvcProContextInfo));
    (EVCPRO_CURR_CONTEXT_INFO ())->u4ContextId = EVCPRO_DEFAULT_CONTEXT;

    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcproAssignMemPoolsId                               */
/*                                                                           */
/* Description        : This function is used to assign the mempool ID's for */
/*                       memory pools created for the module                 */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
EvcproAssignMemPoolsId (VOID)
{
    EVCPRO_QMSG_MEMPOOL_ID = EVCPROMemPoolIds[MAX_EVCPRO_Q_MESG_SIZING_ID];
    EVCPRO_LOCALMSG_MEMPOOL_ID =
        EVCPROMemPoolIds[MAX_EVCPRO_LOCAL_MESG_SIZING_ID];
    EVCPRO_CFG_QMSG_MEMPOOL_ID =
        EVCPROMemPoolIds[MAX_EVCPRO_CONFIG_MESG_SIZING_ID];

    return;

}
