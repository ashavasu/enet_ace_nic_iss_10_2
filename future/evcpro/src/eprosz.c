#define _EVCPROSZ_C
#include "evcproinc.h"
INT4
EvcproSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < EVCPRO_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsEVCPROSizingParams[i4SizingId].u4StructSize,
                              FsEVCPROSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(EVCPROMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            EvcproSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
EvcproSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsEVCPROSizingParams);
    return OSIX_SUCCESS;
}

VOID
EvcproSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < EVCPRO_MAX_SIZING_ID; i4SizingId++)
    {
        if (EVCPROMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (EVCPROMemPoolIds[i4SizingId]);
            EVCPROMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
