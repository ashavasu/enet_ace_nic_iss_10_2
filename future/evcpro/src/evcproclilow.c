#include "evcproinc.h"
#include "evcproglobcli.h"

/* "ethernet evc <evc-id>" */
                                                                                                     
/*****************************************************************************/
/* Function Name      : CliHandleEvcCreate                                   */
/*                                                                           */
/* Description        : This function handles the EVC creation command       */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC ID                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
CliHandleEvcCreate(UINT1 *pu1EvcId)
{   if (EvcProCreateEvc(pu1EvcId) != EVCPRO_SUCCESS )
   {
       return CLI_FAILURE;
   }
   return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliHandleEvcDelete                                   */
/*                                                                           */
/* Description        : This function handles the EVC deletion command       */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC ID                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
CliHandleEvcDelete(UINT1 *pu1EvcId)
{   if (EvcProDeleteEvc(pu1EvcId) != EVCPRO_SUCCESS )
   {
       return CLI_FAILURE;
   }
   return CLI_SUCCESS;
}

/* "uni count <value>" */
/*****************************************************************************/
/* Function Name      : CliHandleUniCount                                    */
/*                                                                           */
/* Description        : This function handles the command to update the UNI  */
/*                      count updation for EVC                               */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC ID                                    */
/*                      u2UniCount - No of UNI connected                     */
/*                      u4UniType - Type of UNI                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
CliHandleUniCount(UINT1 *pu1EvcId ,UINT2 u2UniCount,UINT4 u4UniType)
{   if ( LcmUpdateEvcUniCount(pu1EvcId,u2UniCount,u4UniType) != LCM_SUCCESS )
   {
       return CLI_FAILURE;
   }
   return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CliHandleUniCount                                    */
/*                                                                           */
/* Description        : This function handles the command to update the UNI  */
/*                      count updation for EVC                               */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC ID                                    */
/*                      u2UniCount - No of UNI connected                     */
/*                      u4UniType - Type of UNI                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
CliHandleCeVlanMap(UINT2 u2PortNo,UINT1 *pu1EvcId,UINT2 u2CeVlanId, UINT4 u4CeVlanType, UINT4 u4CommandType)
{
   EVCPRO_UNUSED(u4CeVlanType);

   if (u4CommandType == CLI_MAP_CEVLAN)
   {
       u4CommandType = EVCPRO_MAP_VLAN;
   }
   else
   {
       u4CommandType = EVCPRO_NO_MAP_VLAN;
   }
   if ( LcmUpdateCVlanEvcMap(u2PortNo, pu1EvcId,u2CeVlanId,u4CommandType) != LCM_SUCCESS)
   {
       return CLI_FAILURE;
   }
   return CLI_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : CliHandleEvcBwProfile                                */
/*                                                                           */
/* Description        : This function handles the command to set the         */
/*                      bandwidth profile of the EVC                         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC ID                                    */
/*                      u4Cm - Coloring Mode                                 */
/*                      u4Cf - Coupling Flag                                 */
/*                      u4PerCosBit - Per Cos Bit                            */
/*                      u4CirMagnitude - CIR Magnitude                       */
/*                      u4CirMultiplier - CIR Multiplier                     */
/*                      u4CbsMagnitude - CBS Magnitude                       */
/*                      u4CbsMultiplier - CBS Multiplier                     */
/*                      u4EirMagnitude - EIR Magnitude                       */
/*                      u4EirMultiplier - EIR Multiplier                     */
/*                      u4EbsMagnitude - EBS Magnitude                       */
/*                      u4EbsMultiplier - EBS Multiplier                     */
/*                      u4PriorityBits - Priority Bits                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4  
CliHandleEvcBwProfile(UINT1 *pu1EvcId,UINT4 u4Cm,UINT4 u4Cf,UINT4 u4PerCosBit,UINT4 u4CirMagnitude,
                      UINT4 u4CirMultiplier,UINT4 u4CbsMagnitude,UINT4 u4CbsMultiplier,
                      UINT4 u4EirMagnitude,UINT4 u4EirMultiplier,UINT4 u4EbsMagnitude,
                      UINT4 u4EbsMultiplier,UINT4 u4PriorityBits)
{   if (LcmSetEvcBwProfile(pu1EvcId,u4Cm,u4Cf,u4PerCosBit,u4CirMagnitude,
                             u4CirMultiplier,u4CbsMagnitude,u4CbsMultiplier,
                             u4EirMagnitude,u4EirMultiplier,u4EbsMagnitude,
                             u4EbsMultiplier,u4PriorityBits) == LCM_FAILURE)
   {
       return CLI_FAILURE;
   }
   return CLI_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : CliHandleEvcStatusChange                             */
/*                                                                           */
/* Description        : This function handles the command to update EVC Status*/
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC ID                                    */
/*                      u4EvcStatus - EVC status                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
CliHandleEvcStatusChange(UINT1* pu1EvcId,UINT4 u4EvcStatus)
{

   LcmEvcStatusChange (pu1EvcId,(UINT1)u4EvcStatus);
   return CLI_SUCCESS;
}
