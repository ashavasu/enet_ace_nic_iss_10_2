/******************************************************************** 
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: evcprosys.c,v 1.3 2007/12/19 09:50:30 iss Exp $
 *
 * Description: This file contains initialisation routines for the 
 *              EVCI Module.
 * *******************************************************************/
#define _EVCPROSYS_C

#include "evcproinc.h"

/*****************************************************************************/
/* Function Name      : EvcProModuleInit                                        */
/*                                                                           */
/* Description        : This function is called at the time of starting up   */
/*                      of E-LMI. This function will reserve the memory pool */
/*                      for Port Entry and EVC information. This function is */
/*                      responsible for initializing the local database and  */
/*                      other protocol specific values                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : The local database gets initialized.                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                            */
/*****************************************************************************/

INT4
EvcProModuleInit (VOID)
{
    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProModuleShutDown                                    */
/*                                                                           */
/* Description        : This function gets the Timer submodule               */
/*                      deinitialization, deletes the memory pools.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gu1EvcProSystemControl                                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
EvcProModuleShutdown (VOID)
{
    EVCPRO_SYSTEM_CONTROL = (UINT1) EVCPRO_SHUTDOWN;
    return;
}

/*****************************************************************************/
/* Function Name      : EvcProModuleEnable                                      */
/*                                                                           */
/* Description        : Called by management when EVCPROI is enabled globally*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                      gEvcProGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gEvcProGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                            */
/*****************************************************************************/
INT4
EvcProModuleEnable (VOID)
{
    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProModuleDisable                                     */
/*                                                                           */
/* Description        : Called by management when EVCPROI is disabled globally*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                      gEvcProGlobalInfo.u1ModuleStatus                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gEvcProGlobalInfo.apPortEntry[]                         */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                            */
/*****************************************************************************/
INT4
EvcProModuleDisable (VOID)
{

    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProLock                                              */
/*                                                                           */
/* Description        : This function is used to take the EVCPRO mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP, EvcProTaskMain                                   */
/*****************************************************************************/
INT4
EvcProLock (VOID)
{
    if (EVCPRO_TAKE_SEM (gEvcProGlobalInfo.SemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the EVCPRO mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP, EvcProTaskMain                                   */
/*****************************************************************************/
INT4
EvcProUnLock (VOID)
{
    EVCPRO_GIVE_SEM (gEvcProGlobalInfo.SemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProCreateEvc                                      */
/*                                                                           */
/* Description        : This routines creates an EVC                         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC to be created                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                      */
/*****************************************************************************/
INT4
EvcProCreateEvc (UINT1 *pu1EvcId)
{
    LCM_LOCK ();
    if (LcmFindEvcId (pu1EvcId) == LCM_SUCCESS)
    {
        LCM_UNLOCK ();
        return EVCPRO_SUCCESS;
    }
    if (LcmCreateEvc (pu1EvcId) == LCM_FAILURE)
    {
        LCM_UNLOCK ();
        return EVCPRO_FAILURE;
    }
    LCM_UNLOCK ();
    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProDeleteEvc                                      */
/*                                                                           */
/* Description        : This routines deletes an EVC                         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC to be deleted                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                      */
/*****************************************************************************/
INT4
EvcProDeleteEvc (UINT1 *pu1EvcId)
{
    LCM_LOCK ();
    /* Check if EVCPRO Id does not exist */
    if (LcmFindEvcId (pu1EvcId) != LCM_SUCCESS)
    {
        LCM_UNLOCK ();
        return EVCPRO_FAILURE;
    }

    /* else Delete EVCPRO reference Id */
    LcmDeleteEvc (pu1EvcId);
    LCM_UNLOCK ();
    /* Delete EVCPRO entry LCM database */
    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProUpdateEvcAttributes                            */
/*                                                                           */
/* Description        : This routines updates EVC attributes                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                      */
/*****************************************************************************/
INT4
EvcProUpdateEvcAttributes (VOID)
{
    /* Call "Update EVCPRO Information API" of LCM to update EVCPRO information */
    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProSetDefaultEvc                                  */
/*                                                                           */
/* Description        : This routines sets a default EVC for the CVlans      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                      */
/*****************************************************************************/
INT4
EvcProSetDefaultEvc (VOID)
{
    UINT1              *pu1EvcId = NULL;
    LcmSetDefaultEvc (pu1EvcId);
    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProInitialize                                     */
/*                                                                           */
/* Description        : This routines initializes the EVC Provisioning module*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS / EVCPRO_FAILURE                      */
/*****************************************************************************/
INT4
EvcProInitialize (VOID)
{
    lrInitComplete (OSIX_SUCCESS);
    return EVCPRO_SUCCESS;
}

/*******For EVCPRO Provisioning end **********/
