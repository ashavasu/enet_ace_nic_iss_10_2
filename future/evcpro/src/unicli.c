/* SOURCE FILE HEADER :
*
*  ---------------------------------------------------------------------------
* |  FILE NAME             : elmcli.c                                       |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : Aricent Inc.                             |
* |                                                                           |
* |  SUBSYSTEM NAME        : CFA                                              |
* |                                                                           |
* |  MODULE NAME           : CFA                                     |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Action routines for CLI CFA Commands       |
* |                                                                           |
*  ---------------------------------------------------------------------------
* |   1    | 25th Jun 2007   |    Original                                    |
* |        |                 |                                                |
*  ---------------------------------------------------------------------------
*   
*/

#include "evcproinc.h"
#include "uniglobcli.h"

INT4
cli_process_cfauni_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    /*Third Argument is always passed as Interface Index */

    va_list             ap;
    UINT4              *args[CLI_CFA_UNI_MAX_ARGS];
    INT1                argno = (UINT1) CFA_INIT_VAL;
    UINT4               u4Index = CFA_INIT_VAL;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4CfgCmdFound = CLI_SUCCESS;
    UINT1              *pu1UniName = NULL;
    UINT4               u4BundlingType = CFA_INIT_VAL;
    UINT4               u4BundlingValue = CFA_INIT_VAL;
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    MEMSET (au1Cmd, EVCPRO_INIT_VAL, MAX_PROMPT_LEN);

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    u4Index = CLI_GET_IFINDEX ();

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_CFA_UNI_MAX_ARGS)
            break;
    }
    va_end (ap);

    /* CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
     */
    /* ELM_LOCK (); */
    switch (u4Command)
    {
        case CLI_CFA_UNI_ID:
            pu1UniName = (UINT1 *) args[0];
            i4RetVal = CliHandleUniCreate (pu1UniName, u4Index);
            break;
        case CLI_CFA_NO_UNI_ID:
            pu1UniName = (UINT1 *) args[0];
            i4RetVal = CliHandleUniDelete (pu1UniName, u4Index);
            break;
        case CLI_CFA_UNI_TYPE:
            u4BundlingType = *args[0];
            u4BundlingValue = *args[1];
            i4RetVal =
                CliHandleUniBundling (u4Index, u4BundlingType, u4BundlingValue);
            break;
        case CLI_CFA_NO_UNI_TYPE:
            u4BundlingType = EVC_BUNDLING;
            u4BundlingValue = CFA_INIT_VAL;
            i4RetVal =
                CliHandleUniBundling (u4Index, u4BundlingType, u4BundlingValue);
            break;
        case CLI_CFA_UNI_BW_PROFILE:
        {
            UINT4               u4Cm = *args[0];
            UINT4               u4Cf = *args[1];
            UINT4               u4PerCosBit = 0;
            UINT4               u4CirMagnitude = *args[2];
            UINT4               u4CirMultiplier = *args[3];
            UINT4               u4CbsMagnitude = *args[4];
            UINT4               u4CbsMultiplier = *args[5];
            UINT4               u4EirMagnitude = *args[6];
            UINT4               u4EirMultiplier = *args[7];
            UINT4               u4EbsMagnitude = *args[8];
            UINT4               u4EbsMultiplier = *args[9];
            UINT4               u4PriorityBits = *args[10];

            i4RetVal =
                CliHandleUniBwProfile (u4Index, NULL, u4Cm, u4Cf, u4PerCosBit,
                                       u4CirMagnitude, u4CirMultiplier,
                                       u4CbsMagnitude, u4CbsMultiplier,
                                       u4EirMagnitude, u4EirMultiplier,
                                       u4EbsMagnitude, u4EbsMultiplier,
                                       u4PriorityBits);
        }
            break;
        default:
            if (u4CfgCmdFound == CLI_FAILURE)
            {
                /* Given command does not match with any of the SET or SHOW 
                 * commands */
                CliPrintf (CliHandle, "\r%% Invalid Command !\r\n ");
                i4RetVal = CLI_FAILURE;
            }
            break;
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}
