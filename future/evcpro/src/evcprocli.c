/* SOURCE FILE HEADER :
*  $Id: evcprocli.c,v 1.5 2014/04/04 10:10:18 siva Exp $
*  ---------------------------------------------------------------------------
* |  FILE NAME             : evcprocli.c                                      |
* |                                                                           |
* |  PRINCIPAL AUTHOR      : Aricent Inc.                                     |
* |                                                                           |
* |  SUBSYSTEM NAME        : CLI                                              |
* |                                                                           |
* |  MODULE NAME           : EVCPRO                                           |
* |                                                                           |
* |  LANGUAGE              : C                                                |
* |                                                                           |
* |  TARGET ENVIRONMENT    :                                                  |
* |                                                                           |
* |  DATE OF FIRST RELEASE :                                                  |
* |                                                                           |
* |  DESCRIPTION           : Action routines for CLI EVCPRO Commands          |
* |                                                                           |
*  ---------------------------------------------------------------------------
* |   1    | 25th Jun 2007   |    Original                                    |
* |        |                 |                                                |
*  ---------------------------------------------------------------------------
*   
*/

#include "evcproinc.h"
#include "evcproglobcli.h"

INT4
cli_process_evcpro_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    /*Third Argument is always passed as Interface Index */

    va_list             ap;
    UINT4              *args[EVCPRO_CLI_MAX_ARGS];
    INT1                argno = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4Index = EVCPRO_INIT_VAL;
    INT4                i4RetVal = CLI_FAILURE;
    UINT4               u4CfgCmdFound = CLI_SUCCESS;
    UINT1              *pu1EvcName = NULL;
    UINT4               u4NoOfUniCount = EVCPRO_INIT_VAL;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4UniType = EVCPRO_INIT_VAL;
    UINT4               u4CeVlanType = EVCPRO_INIT_VAL;
    UINT4               u4Cm = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4Cf = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4PerCosBit = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4CirMagnitude = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4CirMultiplier = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4CbsMagnitude = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4CbsMultiplier = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4EirMagnitude = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4EirMultiplier = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4EbsMagnitude = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4EbsMultiplier = (UINT1) EVCPRO_INIT_VAL;
    UINT4               u4PriorityBits = (UINT1) EVCPRO_INIT_VAL;

    UINT4               u4CeVlanValue = EVCPRO_INIT_VAL;

    MEMSET (au1Cmd, EVCPRO_INIT_VAL, MAX_PROMPT_LEN);

    va_start (ap, u4Command);

    /* Third argument is always Interface Name Index */

    u4Index = va_arg (ap, UINT4);

    u4Index = CLI_GET_IFINDEX ();

    /* Walk through the rest of the arguments and store in args array. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == EVCPRO_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    /* CliRegisterLock (CliHandle, ElmLock, ElmUnLock);
     */
    /* ELM_LOCK (); */
    switch (u4Command)
    {
        case CLI_EVCPRO_ETH_EVC:
            pu1EvcName = (UINT1 *) args[0];
            i4RetVal = CliHandleEvcCreate (pu1EvcName);
            if (i4RetVal == CLI_SUCCESS)
            {
                SPRINTF ((CHR1 *) au1Cmd, "%s", CLI_EVC_MODE);
                if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to enter into Evc configuration"
                               "mode\r\n");
                    return CLI_FAILURE;
                }
                MEMSET (&(LcmPromptInfo.EvcName), 0x00,
                        LCM_MAX_EVC_IDENTIFIER_LENGTH);
                STRCPY (&(LcmPromptInfo.EvcName), pu1EvcName);
            }

            break;
        case CLI_EVCPRO_NO_ETH_EVC:
            pu1EvcName = (UINT1 *) args[0];
            i4RetVal = CliHandleEvcDelete (pu1EvcName);
            if (i4RetVal != CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% ERROR: EVC does not exist\r\n");
            }

            MEMSET (&(LcmPromptInfo.EvcName), 0x00,
                    EVCPRO_MAX_EVC_IDENTIFIER_LENGTH);

            break;
        case CLI_EVCPRO_SET_UNI_COUNT:
            u4NoOfUniCount = *args[0];
            u4UniType = *args[1];
            if (args[0] == EVCPRO_INIT_VAL)
            {
                u4UniType = POINT_TO_POINT_EVC;
            }
            if (u4NoOfUniCount > 2)
            {
                u4UniType = POINT_TO_MULTI_POINT_EVC;
            }
            pu1EvcName = (UINT1 *) &(LcmPromptInfo.EvcName);
            /*Evc id is taken from the mode */
            /*uni type is also required */
            i4RetVal =
                CliHandleUniCount (pu1EvcName, u4NoOfUniCount, u4UniType);
            break;

        case CLI_EVCPRO_SET_NO_UNI_COUNT:
            u4NoOfUniCount = 2;
            u4UniType = POINT_TO_POINT_EVC;
            pu1EvcName = (UINT1 *) &(LcmPromptInfo.EvcName);
            /*Evc id is taken from the mode */
            /*uni type is also required */
            i4RetVal =
                CliHandleUniCount (pu1EvcName, u4NoOfUniCount, u4UniType);
            break;

        case CLI_MAP_CEVLAN:
            u4CeVlanType = *args[0];
            u4CeVlanValue = *args[1];
            pu1EvcName = (UINT1 *) &(LcmPromptInfo.EvcName);
            i4RetVal =
                CliHandleCeVlanMap (u4Index, pu1EvcName, u4CeVlanValue,
                                    u4CeVlanType, CLI_MAP_CEVLAN);
            break;
        case CLI_NO_MAP_CEVLAN:
            u4CeVlanType = *args[0];
            u4CeVlanValue = *args[1];
            pu1EvcName = (UINT1 *) &(LcmPromptInfo.EvcName);
            i4RetVal =
                CliHandleCeVlanMap (u4Index, pu1EvcName, u4CeVlanValue,
                                    u4CeVlanType, CLI_NO_MAP_CEVLAN);
            break;
        case CLI_EVCPRO_EVC_BW_PROFILE:
        {
            u4Cm = *args[0];
            u4Cf = *args[1];
            u4PerCosBit = 0;
            u4CirMagnitude = *args[2];
            u4CirMultiplier = *args[3];
            u4CbsMagnitude = *args[4];
            u4CbsMultiplier = *args[5];
            u4EirMagnitude = *args[6];
            u4EirMultiplier = *args[7];
            u4EbsMagnitude = *args[8];
            u4EbsMultiplier = *args[9];
            u4PriorityBits = *args[10];

            pu1EvcName = (UINT1 *) &(LcmPromptInfo.EvcName);

            /* A Separate BW Profile Element should be maintained for EVC
               Current Implementation has BW profile elements based on Priority(Cos) */
            u4PerCosBit = 1;

            i4RetVal =
                CliHandleEvcBwProfile (pu1EvcName, u4Cm, u4Cf, u4PerCosBit,
                                       u4CirMagnitude, u4CirMultiplier,
                                       u4CbsMagnitude, u4CbsMultiplier,
                                       u4EirMagnitude, u4EirMultiplier,
                                       u4EbsMagnitude, u4EbsMultiplier,
                                       u4PriorityBits);
        }
            break;
        case CLI_EVCPRO_SET_EVC_STATUS:
        {
            UINT4               u4EvcStatus = *args[0];

            pu1EvcName = (UINT1 *) &(LcmPromptInfo.EvcName);

            i4RetVal = CliHandleEvcStatusChange (pu1EvcName, u4EvcStatus);
        }
            break;
        default:
            if (u4CfgCmdFound == CLI_FAILURE)
            {
                /* Given command does not match with any of the SET or SHOW 
                 * commands */
                CliPrintf (CliHandle, "\r%% Invalid Command !\r\n ");
                i4RetVal = CLI_FAILURE;
            }
            break;
    }
    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

/*******************************************************************************
 *
 *     FUNCTION NAME    : EvcProGetEvcConfigPromt
 *
 *     DESCRIPTION      : This function Checks for domain validity and
 *                        returns the prompt to be displayed.
 *
 *     INPUT            : pi1ModeName - Mode to be configured.
 *
 *     OUTPUT           : pi1DispStr  - Prompt to be displayed.
 *
 *     RETURNS          : ELM_TRUE or ELM_FALSE
 *
*******************************************************************************/

INT1
EvcProGetEvcConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT1              *pu1EvcName;
    UINT4               u4Len = STRLEN (CLI_EVC_MODE);

    if (!pi1DispStr || !pi1ModeName)
    {
        return EVCPRO_FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_EVC_MODE, u4Len) != 0)
    {
        return EVCPRO_FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    pu1EvcName = (UINT1 *) pi1ModeName;
    UNUSED_PARAM (pu1EvcName);
    /*u4Index = CLI_ATOI (pi1ModeName);
     *
     * No need to take lock here, since it is taken by
     * Cli in cli_process_ecfm_cmd.
     */

    STRCPY (pi1DispStr, "(config-evc)#");

    return EVCPRO_TRUE;
}
