#include "evcproinc.h"

/* "ethernet uni id <name>" */
INT4
CliHandleUniCreate (UINT1 *pu1UniId, UINT2 u2PortNo)
{
    if (CfaCreateUni (pu1UniId, u2PortNo) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/* "ethernet uni id <name>" */
INT4
CliHandleUniDelete (UINT1 *pu1UniId, UINT2 u2PortNo)
{
    if (CfaDeleteUni (pu1UniId, u2PortNo) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/* "ethernet uni {bundle [all-to-one] | multiplex}" */
INT4
CliHandleUniBundling (UINT2 u2PortNo, UINT1 u1BundlingType,
                      UINT4 u4BundlingValue)
{

    if (CfaHandleCliUpdateUniInfo (u1BundlingType, u2PortNo, u4BundlingValue) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
CliHandleUniBwProfile (UINT2 u2PortNo, UINT1 *pu1EvcId, UINT4 u4Cm, UINT4 u4Cf,
                       UINT4 u4PerCosBit, UINT4 u4CirMagnitude,
                       UINT4 u4CirMultiplier, UINT4 u4CbsMagnitude,
                       UINT4 u4CbsMultiplier, UINT4 u4EirMagnitude,
                       UINT4 u4EirMultiplier, UINT4 u4EbsMagnitude,
                       UINT4 u4EbsMultiplier, UINT4 u4PriorityBits)
{
    CfaSetUniBwProfile (u2PortNo, pu1EvcId, u4Cm, u4Cf, u4PerCosBit,
                        u4CirMagnitude, u4CirMultiplier, u4CbsMagnitude,
                        u4CbsMultiplier, u4EirMagnitude, u4EirMultiplier,
                        u4EbsMagnitude, u4EbsMultiplier, u4PriorityBits);
    return CLI_SUCCESS;
}
