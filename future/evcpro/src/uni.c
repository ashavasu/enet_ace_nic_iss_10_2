#define _CFA_UNI_C

#include "evcproinc.h"

/************** Stubs Code Start *********************************/

/*******For CFA Start **********/
INT4
CfaCreateUni (UINT1 *pu1UniId, UINT2 u2PortNo)
{
    tCfaPortEntry      *pCfaPortEntry = NULL;

    pCfaPortEntry = CFA_GET_PORTENTRY (u2PortNo);

    if (pCfaPortEntry == NULL)
    {
        return CLI_FAILURE;
    }

    /* Copy pu1UniId in CfaPortEntry */
    STRCPY (pCfaPortEntry->CfaUniInfo.au1UniName, pu1UniId);
    ElmUniInformationChangeIndication (u2PortNo);

    return CLI_SUCCESS;
}

INT4
CfaDeleteUni (UINT1 *pu1UniId, UINT2 u2PortNo)
{
    CFA_UNUSED (pu1UniId);
    CFA_UNUSED (u2PortNo);
    return CLI_SUCCESS;
}

INT4
CfaHandleCliUpdateUniInfo (UINT1 u1BundlingType, UINT2 u2PortNo,
                           UINT4 u4BundlingValue)
{
    tCfaPortEntry      *pCfaPortEntry = NULL;

    CFA_UNUSED (u4BundlingValue);

    pCfaPortEntry = CFA_GET_PORTENTRY (u2PortNo);

    if (pCfaPortEntry == NULL)
    {
        return CLI_FAILURE;
    }

    /* Update u1BundlingType in CfaPortEntry */
    pCfaPortEntry->CfaUniInfo.u1CeVlanMapType = u1BundlingType;
    ElmUniInformationChangeIndication (u2PortNo);

    return CLI_SUCCESS;
}

INT4
CfaInitialize (VOID)
{
    return CLI_SUCCESS;
}

/******* For CFA End **********/
/************** For Compilation Only **********************************/
INT4
CfaGetUniInfo (UINT2 u2PortNo, tCfaUniInfo * pCfaUniInfo)
{
    UINT1              *pu1UniId = NULL;
    CFA_UNUSED (u2PortNo);
    pu1UniId = (UINT1 *) "UNI1";
    pCfaUniInfo->u1CeVlanMapType = 1;
    STRCPY (pCfaUniInfo->au1UniName, pu1UniId);
    return CFA_FAILURE;
}

VOID
CfaUpdateUniInfo (tCfaUniInfo CfaUniInfo)
{
    CFA_UNUSED (CfaUniInfo);
    return;
}

/************** For Compilation End **********************************/
VOID
CfaSetUniBwProfile (UINT2 u2PortNo, UINT1 *pu1EvcId, UINT4 u4Cm, UINT4 u4Cf,
                    UINT4 u4PerCosBit, UINT4 u4CirMagnitude,
                    UINT4 u4CirMultiplier, UINT4 u4CbsMagnitude,
                    UINT4 u4CbsMultiplier, UINT4 u4EirMagnitude,
                    UINT4 u4EirMultiplier, UINT4 u4EbsMagnitude,
                    UINT4 u4EbsMultiplier, UINT4 u4PriorityBits)
{
    CFA_UNUSED (u2PortNo);
    CFA_UNUSED (pu1EvcId);
    CFA_UNUSED (u4Cm);
    CFA_UNUSED (u4Cf);
    CFA_UNUSED (u4PerCosBit);
    CFA_UNUSED (u4CirMagnitude);
    CFA_UNUSED (u4CirMultiplier);
    CFA_UNUSED (u4CbsMagnitude);
    CFA_UNUSED (u4CbsMultiplier);
    CFA_UNUSED (u4EirMagnitude);
    CFA_UNUSED (u4EirMultiplier);
    CFA_UNUSED (u4EbsMagnitude);
    CFA_UNUSED (u4EbsMultiplier);
    CFA_UNUSED (u4PriorityBits);

    if (ElmGetPortMode (u2PortNo) == ELM_NETWORK_SIDE)
    {
        ElmUniInformationChangeIndication (u2PortNo);
    }
    return;
}
