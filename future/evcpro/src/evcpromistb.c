/******************************************************************** *  * Copyright (C) 2006 Aricent Inc . All Rights Reserved] * * $Id: evcpromistb.c,v 1.2 2007/11/19 07:21:41 iss Exp $ * * Description: This file contains stub routines of Multiple instance *              specific functions for backward compatibility. * *******************************************************************/
#include "evcproinc.h"

#define EVCPRO_TRC_BUF_SIZE    500

/*****************************************************************************/
/* Function Name      : EvcProGlobalTrace                                    */
/*                                                                           */
/* Description        : This fucntion handles the global trace option for    */
/*                      EVC Pro module.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
EvcProGlobalTrace (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
    static CHR1         Str[EVCPRO_TRC_BUF_SIZE];

    if (EVCPRO_IS_EVC_INITIALISED () == EVCPRO_FALSE)
    {
        return;
    }
    UNUSED_PARAM (u4ContextId);
    va_start (ap, fmt);
    vsprintf (&Str[0], fmt, ap);
    va_end (ap);

    UtlTrcLog (EVCPRO_TRC_FLAG (), u4Flags, EVCPRO_MOD_NAME, Str);
}

/*****************************************************************************/
/* Function Name      : EvcProGlobalDebug                                    */
/*                                                                           */
/* Description        : This fucntion handles the global debug option for    */
/*                      EVC Pro module.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
EvcProGlobalDebug (UINT4 u4ContextId, UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
    static CHR1         Str[EVCPRO_TRC_BUF_SIZE];

    if (EVCPRO_IS_EVC_INITIALISED () == EVCPRO_FALSE)
    {
        return;
    }
    UNUSED_PARAM (u4ContextId);
    va_start (ap, fmt);
    vsprintf (&Str[0], fmt, ap);
    va_end (ap);

    UtlTrcLog (EVCPRO_TRC_FLAG (), u4Flags, EVCPRO_MOD_NAME, Str);
}

/*****************************************************************************/
/* Function Name      : EvcProSelectContext                                     */
/*                                                                           */
/* Description        : This function selects a virtual context as the       */
/*                      current context pointer before processing an event   */
/*                      for the context                                      */
/*                      It is stubbed here and is active only in the multiple*/
/*                      instance mode of operation.                          */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS/ EVCPRO_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
EvcProSelectContext (UINT4 u4ContextId)
{
    EVCPRO_UNUSED (u4ContextId);

    return EVCPRO_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EvcProReleaseContext                                    */
/*                                                                           */
/* Description        : This function releases the virtual context           */
/*                      previously selected as current after an event has    */
/*                      been processed for the context.                      */
/*                      It is stubbed here and is active only in the multiple*/
/*                      instance mode of operation.                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : EVCPRO_SUCCESS/ EVCPRO_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
EvcProReleaseContext (VOID)
{
    return EVCPRO_SUCCESS;
}
