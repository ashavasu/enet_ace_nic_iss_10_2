/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddmain.c,v 1.75 2016/10/03 10:34:40 siva Exp $
 *
 * Description:This file contains the routines for the     
 *             Gemeric Device Driver Module of the CFA.    
 *             These routines are called at various places     
 *             in the CFA modules for interfacing with the     
 *             device drivers in the system. These routines    
 *             have to be modified when moving onto different  
 *             drivers. Currently,only the SOCK_PACKET for     
 *             Ethernet and WANIC HDLC driver are supported.  
 *
 *******************************************************************/
#include "cfainc.h"
#include "ofcl.h"

/*****************************************************************************
 *
 *    Function Name        : CfaGddInitPort
 *
 *    Description        : The GDD structure is allocated only for the physical 
 *                         ports and if the itType is ATM_VC then the Gdd
 *                         structure of the parent AAL5 port is used.
 *                         This function 
 *                         initializes the structure with the default values 
 *                         depending on the interface type. 
 *                         This function also initializes the function pointers 
 *                         for Performing the open / close/ read/ write
 *                         operations on the device driver ports. 
 *
 *    Input(s)            : u4IfIndex - MIB-2 ifIndex
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (ifMain table) structure.
 *
 *    Global Variables Modified : gapIfTable (IfMain table) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddInitPort (UINT4 u4IfIndex)
{
    UINT2               u2GddType;
#ifndef MUX_SUPPORT
    INT4                i4Retval = CFA_SUCCESS;
#endif

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddInitPort\n");

    /* check if port already inited */
    if (CFA_GDD_ENTRY_USED (u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddInitPort - Interface %d already "
                  "Initialised - FAILURE.\n", u4IfIndex);
        return (CFA_SUCCESS);
    }

    /* allocate for the GDD phys entry in the ifTable */
    if ((CFA_GDD_ENTRY (u4IfIndex) =
         (tGddRegStruct *) MemAllocMemBlk (CFA_GDD_MEMPOOL)) == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaGddInitPort - Mempool alloc fail "
                  "for interface %d.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGddSetGddType (u4IfIndex);

    CFA_GDD_PORT (u4IfIndex) = CFA_INVALID_TYPE;
    CFA_GDD_REGSTAT (u4IfIndex) = CFA_INVALID;

#ifndef MUX_SUPPORT
    CFA_GDD_PORT_DESC (u4IfIndex) = CFA_INVALID_DESC;
#else
    CFA_GDD_BIND_COOKIE_DESC (u4IfIndex) = CFA_INVALID_DESC;
#endif
    CfaGetIfName (u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));

    CFA_GDD_HIGHTYPE (u4IfIndex) = CFA_INVALID_TYPE;

    u2GddType = CFA_GDD_TYPE (u4IfIndex);

    /* we first probe the port and see if it is present -
     * if not present, then we set the operstatus of
     * the port accordingly
     */
#ifndef MUX_SUPPORT
    switch (u2GddType)
    {
            /* Intentional Fall through for CFA_OTHER */
#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_WLAN_RADIO:
        case CFA_RADIO:
#endif
        case CFA_OTHER:
        case CFA_LAGG:
#ifdef HDLC_WANTED
        case CFA_HDLC:
#endif
        case CFA_ETHERNET:

            if (CfaIsMgmtPort (u4IfIndex) == TRUE)
            {
                /* If it is the mgmt port, it is system interface (linux port\
                 * VxWorks system Interface and the function ptrs should call
                 * corresponding API and not  NPAPI. */
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                CFA_GDD_OPEN_FNPTR (u4IfIndex) = &CfaGddOobOpen;
                CFA_GDD_CLOSE_FNPTR (u4IfIndex) = &CfaGddOobClose;
                CFA_GDD_READ_FNPTR (u4IfIndex) = &CfaGddOobRead;
                CFA_GDD_WRITE_FNPTR (u4IfIndex) = &CfaGddOobWrite;
#endif
            }
            else
            {
                CFA_GDD_OPEN_FNPTR (u4IfIndex) = &CfaGddEthOpen;
                CFA_GDD_CLOSE_FNPTR (u4IfIndex) = &CfaGddEthClose;
                CFA_GDD_READ_FNPTR (u4IfIndex) = &CfaGddEthRead;
                CFA_GDD_WRITE_FNPTR (u4IfIndex) = &CfaGddEthWrite;
                CFA_GDD_WRITE_FNPTR_WITH_PRIO (u4IfIndex) = 
					   &CfaGddEthWriteWithPri;
            }

            break;
#ifdef WLC_WANTED
        case CFA_CAPWAP_DOT11_BSS:
        {
            CFA_GDD_OPEN_FNPTR (u4IfIndex) = &CfaGddWssOpen;
            CFA_GDD_CLOSE_FNPTR (u4IfIndex) = &CfaGddWssClose;
            CFA_GDD_READ_FNPTR (u4IfIndex) = &CfaGddWssRead;
            CFA_GDD_WRITE_FNPTR (u4IfIndex) = &CfaGddWssWrite;
        }
            break;
#endif
        default:
            /* wrong type of device */
            i4Retval = CFA_FAILURE;
            break;

    }                            /* end of switch */

    if (i4Retval != CFA_SUCCESS)
    {
        if (MemReleaseMemBlock (CFA_GDD_MEMPOOL,
                                (UINT1 *) CFA_GDD_ENTRY (u4IfIndex)) !=
            MEM_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaGddInitPort - "
                      "MemBlock free fail for interface %d\n", u4IfIndex);
        }
        else
        {
            CFA_GDD_ENTRY (u4IfIndex) = NULL;
        }
        return CFA_FAILURE;
    }
    else
    {
        return CFA_SUCCESS;
    }
#else
    CFA_GDD_OPEN_FNPTR (u4IfIndex) = &CfaGddMuxOpen;
    CFA_GDD_CLOSE_FNPTR (u4IfIndex) = &CfaGddMuxClose;
    CFA_GDD_READ_FNPTR (u4IfIndex) = &CfaGddMuxRead;
    CFA_GDD_WRITE_FNPTR (u4IfIndex) = &CfaGddMuxWrite;

    return (CFA_SUCCESS);
#endif
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddDeInitPort
 *
 *    Description        : This function release the memory allocated to the
 *                         Gdd structure. This GDD structure is allocated only 
 *                         for physical ports and if the ifType is  ATM_VC, the 
 *                         GDD struct will not be de-allocated.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *
 *    Global Variables Modified : gapIfTable (Interface table) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaGddDeInitPort (UINT4 u4IfIndex)
{

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddDeInitPort\n");

    /* check if port already inited */
    if (!CFA_GDD_ENTRY_USED (u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddDeInitPort - Interface %d "
                  "already De-Initialized - FAILURE.\n", u4IfIndex);
        return (CFA_SUCCESS);
    }

    /* de-allocate for the GDD phys entry in the ifTable */
    if (MemReleaseMemBlock (CFA_GDD_MEMPOOL,
                            (UINT1 *) CFA_GDD_ENTRY (u4IfIndex)) != MEM_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaGddDeInitPort - "
                  "MemBlock free fail for interface %d\n", u4IfIndex);
    }
    else
    {
        CFA_GDD_ENTRY (u4IfIndex) = NULL;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddRegisterPort
 *
 *    Description        : Registers the given device driver port with the 
 *                         specified higher IWF/data-link layer modules. 
 *
 *    Input(s)            : u4IfIndex - MIB-2 ifIndex
 *                          u1HighIfType - IfType of the higher layer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                                gapIfTable.
 *
 *    Global Variables Modified : gapIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddRegisterPort (UINT4 u4IfIndex, UINT1 u1HighIfType)
{

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddRegisterPort\n");

    /* check if port already inited */
    if (!CFA_GDD_ENTRY_USED (u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddRegisterPort - "
                  "Interface %d not Initialized - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    /* check if already registered with HL */
    if (CFA_GDD_REGSTAT (u4IfIndex) == CFA_VALID)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddRegisterPort - "
                  "Interface %d already registered.\n", u4IfIndex);
        if (CFA_GDD_HIGHTYPE (u4IfIndex) != u1HighIfType)
            return (CFA_FAILURE);
        else
            return (CFA_SUCCESS);
    }

    CFA_GDD_HIGHTYPE (u4IfIndex) = u1HighIfType;

    switch (CFA_GDD_HIGHTYPE (u4IfIndex))
    {

        case CFA_ENET:
        case CFA_LAGG:
            CFA_GDD_HL_RX_FNPTR (u4IfIndex) = &CfaMuxIwfEnetProcessRxFrame;
            break;

#ifdef PPP_WANTED
        case CFA_PPP:
            CFA_GDD_HL_RX_FNPTR (u4IfIndex) = &CfaIwfPppHandleIncomingPkt;
            break;
#endif /* PPP_WANTED */

        default:
            return CFA_FAILURE;
    }                            /* end if switch */

    CFA_GDD_REGSTAT (u4IfIndex) = CFA_VALID;

    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGddDeRegisterPort
 *
 *    Description        : De-register the higher layre with the physical port. 
 *                         This function closes the port if it is opened already 
 *                         and removes the entry from the polling table and 
 *                         re-initializes the higher layer ifType and 
 *                         registration status of Gdd structure to INVALID.  
 *
 *    Input(s)            : u4IfIndex - MIB-2 ifIndex of the interface
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure.
 *
 *    Global Variables Modified : gapIfTable (Interface table) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddDeRegisterPort (UINT4 u4IfIndex)
{
    UINT1               u1OperStatus;
    UINT1               u1IfType;

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddDeRegisterPort\n");

    /* check if port already inited */
    if (!CFA_GDD_ENTRY_USED (u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddDeRegisterPort - "
                  "Interface %d not Initialized - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
    /* if the port is open then close it first */
    if (u1OperStatus != CFA_IF_DOWN)
    {
        if ((CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE) ||
            (CfaIsMgmtPort (u4IfIndex) == TRUE))
        {
            if ((CFA_GDD_CLOSE_FNPTR (u4IfIndex)) (u4IfIndex) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                          "Error in CfaGddDeRegisterPort - "
                          "Interface %d cannot be closed - FAILURE.\n",
                          u4IfIndex);
                CfaSetIfOperStatus (u4IfIndex, CFA_IF_DOWN);

                CfaGddOsRemoveIfFromList (u4IfIndex);

                return CFA_FAILURE;
            }
            if (CFA_GDD_PORT_DESC (u4IfIndex) != CFA_INVALID_DESC)
            {
                close (CFA_GDD_PORT_DESC (u4IfIndex));
            }
        }

        if (u1IfType == CFA_ENET)
        {
            CfaGddOsRemoveIfFromList (u4IfIndex);
        }
    }

    CFA_GDD_HIGHTYPE (u4IfIndex) = CFA_INVALID_TYPE;
    CFA_GDD_REGSTAT (u4IfIndex) = CFA_INVALID;
    CFA_GDD_HL_RX_FNPTR (u4IfIndex) = NULL;

    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGddOpenPort
 *
 *    Description        : Opens the device driver port based on the function pointer. 
 *                         If the interface type is ATM_VC, it uses the its lower layer 
 *                         ifIndex to open the device driver port. 
 *                         If the port open is SUCCESS make the Operstatus to UP.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable - (Interface table)
 *
 *    Global Variables Modified : gapIfTable - (Interface table)
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaGddOpenPort (UINT4 u4IfIndex)
{
    UINT1               u1OperStatus;

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddOpenPort\n");

    /* Removed check for the higher layer registration */
    if (!CFA_GDD_ENTRY_USED (u4IfIndex))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddOpenPort - "
                  "Interface %d not United - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_UP;
    CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_UP);

    /* invoke the driver call based on the type of port - use of function pointer */
    if ((CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE) ||
        (CfaIsMgmtPort (u4IfIndex) == TRUE))
    {
        if ((CFA_GDD_OPEN_FNPTR (u4IfIndex)) (u4IfIndex) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddOpenPort - "
                     "unsuccessful Driver Open -  FAILURE.\n");

            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
            if (u1OperStatus != CFA_IF_NP)
            {
                CfaSetIfOperStatus (u4IfIndex, CFA_IF_DOWN);
            }
            else
            {
                /* we have unsuccessful open only if the device is not present */
                CfaSetIfOperStatus (u4IfIndex, CFA_IF_NP);
            }
            return CFA_FAILURE;
        }
#ifndef OS_VXWORKS
#ifdef IP_WANTED
#if !defined( CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
        if ((CFA_MGMT_PORT == TRUE)
            && (u4IfIndex == CFA_DEFAULT_ROUTER_IFINDEX))
        {
            SelAddFd (CFA_GDD_PORT_DESC (CFA_DEFAULT_ROUTER_IFINDEX),
                      CfaOobDataRcvd);
        }
#endif
#endif
#endif
    }

    /* put the new port in the polling table */
    CfaGddOsAddIfToList (u4IfIndex);

    CfaSetIfOperStatus (u4IfIndex, CFA_IF_UP);
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGddClosePort
 *
 *    Description        : Closes the device driver port. It uses the function 
 *                         pointers to close the device driver port based upon 
 *                         the ifIndex. If the interface type is ATM_VC, its 
 *                         lower layer port function call is used to close the 
 *                         port. 
 *
 *    Input(s)            : u4IfIndex - MIB-2 ifIndex
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *
 *
 *    Global Variables Modified : gapIfTable (Interface table) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddClosePort (UINT4 u4IfIndex)
{
    UINT1               u1PromiscMode;
    UINT1               u1OperStatus;

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddClosePort\n");

/* check if port not inited */
    if ((!CFA_GDD_ENTRY_USED (u4IfIndex)) ||
        (CFA_GDD_REGSTAT (u4IfIndex) == CFA_INVALID))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddClosePort - "
                  "Interface %d not United or Registered - FAILURE.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

/* set the adminstatus as indicated - we set the operstatus after we try
opening */
    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_DOWN;
    CfaSetCdbPortAdminStatus (u4IfIndex, CFA_IF_DOWN);

    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
    if (u1OperStatus != CFA_IF_NP)
    {
        /* Store the promiscuous mode as the close would disable the */
        /* promiscuous mode and restore after the close call */
        u1PromiscMode = CFA_IF_PROMISC (u4IfIndex);

/* invoke the driver call based on the type of port - use of function pointer */
        if ((CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE) ||
            (CfaIsMgmtPort (u4IfIndex) == TRUE))
        {
            if ((CFA_GDD_CLOSE_FNPTR (u4IfIndex)) (u4IfIndex) != CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddClosePort - "
                         "unsuccessful Driver Close -  FAILURE.\n");
/* we dont expect this */
            }
            else
            {
                CfaSetIfOperStatus (u4IfIndex, CFA_IF_DOWN);
            }
        }
        /* restore the promiscuous mode */
        CFA_IF_PROMISC (u4IfIndex) = u1PromiscMode;
    }

/* remove the port from the polling table */
    CfaGddOsRemoveIfFromList (u4IfIndex);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddWrite
 *
 *    Description        : This function writes to the driver ports.
 *                It releases the CRU Buf regardless of the
 *                success or failure. the pBuf should not be
 *                accessed by the calling function after the
 *                completion of this function.
 *
 *    Input(s)            : tCRU_BUF_CHAIN_HEADER *pBuf,
 *                UINT4 u4IfIndex,
 *                UINT4 u4PktSize.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table),
 *
 *    Global Variables Modified : The statistic of ifTable (gapIfTable).
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddWrite (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, UINT4 u4PktSize,
             UINT1 u1VlanTagCheck, tCfaIfInfo * pIfInfo)
{
    UINT4               u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;
    UINT4               u4VlanOffset;
#ifndef WGS_WANTED
    UINT4               u4IfL2CxtId = 0;
#endif
    UINT1               u1IsBufferLinear = CFA_TRUE;
    UINT1              *pu1DataBuf = NULL;
    INT4                i4RetVal = CFA_SUCCESS;
#ifndef HDLC_WANTED
    UINT1              *pTempDataBuf = NULL;
#endif
    UINT1               u1PaddingDone = CFA_FALSE;
    UINT2               u2PhyPort = 0;
#ifndef WGS_WANTED
    tVlanId             VlanId;
#endif /*WGS_WANTED */

    UINT1 u1PMChannelType = 0;
    UINT1 u1PMPriority = 0;

    CFA_DBG1 (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddWrite for ifIndex %d.\n",
              u4IfIndex);

    /* Check if u4IfIndex is valid */
    if (u4IfIndex == 0 || u4IfIndex > SYS_DEF_MAX_INTERFACES)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddWrite - "
                 "Unsuccessful Driver Write -  FAILURE.\n");

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }

    /* if port is not open then discard the frame */
    if (pIfInfo->u1IfOperStatus != CFA_IF_UP)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CFA_IF_SET_OUT_DISCARD (u4IfIndex);

        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddWrite - Port Down - FAILURE.\n");

        return (CFA_FAILURE);
    }

    if (EoamMuxParActOnTx (u4IfIndex, pBuf) == EOAM_DISCARD)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CFA_IF_SET_OUT_DISCARD (u4IfIndex);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWrite: Mux Action DISCARD. Frame discarded.\n");
        return (CFA_SUCCESS);
    }

    if ((pu1DataBuf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, u4PktSize))
        == NULL)
    {
        /* allocate for FCS also - required for some HDLC cards */
        if ((pu1DataBuf = MemAllocMemBlk (gCfaPktPoolId)) == NULL)
        {

            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - No Memory -  FAILURE.\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_OUT_DISCARD (u4IfIndex);
            return (CFA_FAILURE);
        }

        /* for aysnc the copy would be done by the byte stuffing function */
        if (CRU_BUF_Copy_FromBufChain (pBuf, pu1DataBuf, 0, u4PktSize) ==
            CRU_FAILURE)
        {
            CFA_DBG (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_GDD,
                     "Error in CfaGddWrite - Copy_FromBufChain failed.\n");
            CFA_IF_SET_OUT_ERR (u4IfIndex);
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return CFA_FAILURE;
        }
        u1IsBufferLinear = CFA_FALSE;
    }                            /* end of check whether the data in linear in CRU Buf */

    /* If the packet is to be sent over ethernet interface, check if
     * the packet size is less than the minimum ethernet packet
     * size . If it is pad the rest of the frame with zeroes
     * to make it equal to min eth frame size.
     */

    if (pIfInfo->u1IfType == CFA_ENET)
    {
        if (u1VlanTagCheck == CFA_TRUE)
        {
            if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset) ==
                VLAN_SUCCESS)
            {
                if (u4VlanOffset != CFA_VLAN_TAG_OFFSET)
                {
                    u4MinFrameMtu =
                        CFA_ENET_MIN_UNTAGGED_FRAME_SIZE + u4VlanOffset -
                        CFA_VLAN_TAG_OFFSET;
                }
            }
        }
#ifndef HDLC_WANTED
        if (u4PktSize < u4MinFrameMtu)
        {
            pTempDataBuf = MemAllocMemBlk (gCfaPktPoolId);
            if (pTempDataBuf == NULL)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unable to allocate memory for Data Buffer - FAILURE.\n");

                if (u1IsBufferLinear == CFA_FALSE)
                {
                    MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
                }

                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_OUT_ERR (u4IfIndex);
                return (CFA_FAILURE);
            }

            MEMSET (pTempDataBuf, 0, u4MinFrameMtu);
            MEMCPY (pTempDataBuf, pu1DataBuf, u4PktSize);

            /* free pu1DataBuf only if it doesn't point to CRU data 
             * descriptor. If it points to CRU, it'll be freed when
             * CRU buffer is released.
             */
            if (u1IsBufferLinear == CFA_FALSE)
            {
                MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
            }

            pu1DataBuf = pTempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }
#endif /* HDLC_WANTED */
    }                            /* For EtherNet interface */

#ifdef OPENFLOW_WANTED
    if (OfcInbandCntlrTxPktCheck (pu1DataBuf, u4PktSize) == OFC_SUCCESS)
    {
        if ((u1IsBufferLinear == CFA_FALSE) || (u1PaddingDone == CFA_TRUE))
        {
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
        }

        /* Free the CRU buffer */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return CFA_SUCCESS;
    }
#endif

    /* When the interface is a VLAN interface, the packet should be given
     * to the hardware with the corresponding VLAN ID, if NPAPI is enabled.

     */

#ifdef WGS_WANTED
    if (pIfInfo->u1IfType == CFA_L2VLAN)
    {
        i4RetVal = CfaIvrProcessPktFromMgmtIface (pu1DataBuf, u4PktSize);
    }
#else
    if ((pIfInfo->u1IfType == CFA_L3IPVLAN)||
       (pIfInfo->u1IfType == CFA_L3SUB_INTF))
    {
        CfaGetIfIvrVlanId (u4IfIndex, &VlanId);
        if (VcmGetL2CxtIdForIpIface (u4IfIndex, &u4IfL2CxtId) == VCM_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_OUT_ERR (u4IfIndex);
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
            return (CFA_FAILURE);
        }

        if (CfaGetSecIvrIndex () != u4IfIndex)
        {
            i4RetVal =
                CfaGddTxPktOnVlanMemberPortsInCxt (u4IfL2CxtId,
                                                   pu1DataBuf, VlanId,
                                                   CFA_TRUE, u4PktSize);
        }
        else
        {
            CfaUtilTxPktOnSecVlanMemberPorts (u4IfL2CxtId, pu1DataBuf,
                                              u4PktSize);
        }
    }
#endif /* WGS_WANTED */
    if (pIfInfo->u1IfType == CFA_LAGG)
    {
        /* Get the physical port corresponding to the port-channel */
        if (L2IwfGetPotentialTxPortForAgg ((UINT2) u4IfIndex, &u2PhyPort)
            == L2IWF_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_OUT_ERR (u4IfIndex);
            /* release only if allocated */
            if (u1IsBufferLinear == CFA_FALSE)
            {
                MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
            }
            return CFA_FAILURE;
        }
        /* invoke the driver call based on the type of port - use of function pointer */
        if ((CFA_GDD_WRITE_FNPTR ((UINT4) u2PhyPort))
            (pu1DataBuf, (UINT4) u2PhyPort, u4PktSize) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "unsuccessful Driver Write -  FAILURE.\n");
            i4RetVal = CFA_FAILURE;
        }
    }

    else
    {
        u1PMChannelType = GET_MODULE_DATA_PTR(pBuf)->au1Reserved[0];
        /* invoke the driver call based on the type of port - use of
         * function pointer */
        if ((RFC6374_CHANNEL_DM == u1PMChannelType) ||
                       (RFC6374_CHANNEL_INFERED_LM == u1PMChannelType) ||
                       (RFC6374_CHANNEL_DIRECT_LM == u1PMChannelType))
        {
            u1PMPriority = GET_MODULE_DATA_PTR(pBuf)->au1Reserved[1];
            if (CFA_GDD_ENTRY (u4IfIndex) != NULL)
            {
                if ((CFA_GDD_WRITE_FNPTR_WITH_PRIO (u4IfIndex))
                         (pu1DataBuf, u4IfIndex, u4PktSize,u1PMPriority)
                          != CFA_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                             "Error in CfaGddWrite - "
                             "unsuccessful Driver Write -  FAILURE.\n");
                    i4RetVal = CFA_FAILURE;
                }
            }
       }  
       else
       {
        /* invoke the driver call based on the type of port - use of function pointer */
        if (CFA_GDD_ENTRY (u4IfIndex) != NULL)
        {
            if ((CFA_GDD_WRITE_FNPTR (u4IfIndex))
                (pu1DataBuf, u4IfIndex, u4PktSize) != CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "unsuccessful Driver Write -  FAILURE.\n");
                i4RetVal = CFA_FAILURE;
            }
        }
    }
    }

    if ((u1IsBufferLinear == CFA_FALSE) || (u1PaddingDone == CFA_TRUE))
    {
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
    }

    /* Free the CRU buffer */
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    if (i4RetVal == CFA_SUCCESS)
    {
        CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);
        CFA_DBG (CFA_TRC_ALL_TRACK, CFA_GDD,
                 "In CfaGddWrite - Attempt to Write Succeeds.\n");

    }
    else
    {
        CFA_IF_SET_OUT_ERR (u4IfIndex);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddWrite - could not write to driver - FAILURE.\n");
    }

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddProcessRecvEvent
 *
 *    Description        : This function reads from the driver ports.
 *                It allocates the CRU Buf. It is expected that
 *                in case of success the CRU buf would be
 *                released by the appropriate function. This
 *                function should be called whenever the
 *                POLL TIMER expires. In the future this
 *                function can be called from the interrupt
 *                handler too. Only one packet is taken from
 *                one port - processing is done in round-robin
 *                manner.
 *
 *                This function invokes the packet processing
 *                function of the higher layer registered over
 *                the specific port when there is some data
 *                available.
 *
 *    Input(s)            : UINT1 u1DevType (expected to be 0),
 *                UINT1 u1PortNum (expected to be 0).
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                gaPhysIfTable (GDD reg table) structure,
 *                ifTable (gapIfTable).
 *
 *    Global Variables Modified : The statistic of ifTable (gapIfTable).
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
INT4
CfaGddProcessRecvEvent ()
{
    /*
     * CFA Lock must be taken in the Message DqQueue while loop
     * in CfaGddOsProcessRecvEvent ().
     */
    if (CfaGddOsProcessRecvEvent () != CFA_SUCCESS)
        return (CFA_FAILURE);

    return CFA_SUCCESS;
}

#ifdef LNXIP4_WANTED

/*****************************************************************************
 *
 *    Function Name      : CfaGddGetPortName
 *
 *    Description        : This function gets the Port Name from the Gdd Entry
 *                         strucuture. 
 *
 *    Input(s)           : u2IfIndex - Interface Index.
 *
 *    Output(s)          : pu1IfName - Pointer to Port Name corresponding
 *                                     to u2IfIndex in Gdd Entry Structure.
 *
 *    Returns            : CFA_SUCCESS
 *
 *****************************************************************************/

INT4
CfaGddGetPortName (UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    STRCPY (pu1IfName, CFA_GDD_PORT_NAME (u4IfIndex));

    return (CFA_SUCCESS);
}
#endif
