/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfastubs.c,v 1.13 2012/02/13 12:12:08 siva Exp $ 
 *
 * Description: This file contains the stub routines of procedures 
 *              called by other modules. 
 *****************************************************************************/
#include "cfainc.h"
#include "firewall.h"

#ifndef EOAM_WANTED
/******************************************************************************
 * Function Name      : EoamApiNotifyIfCreate
 *
 * Description        : This function posts a message to EOAM task's message
 *                      queue notify  an interface creation
 *
 * Input(s)           : u4IfIndex - Interface index
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiNotifyIfCreate (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamApiNotifyIfDelete
 *
 * Description        : This function posts a message to EOAM task's message
 *                      queue notify  an interface deletion
 *
 * Input(s)           : u4IfIndex - Interface index
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiNotifyIfDelete (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function Name      : EoamApiNotifyIfOperChg
 *
 * Description        : This function is invoked from CFA to indicate link
 *                      status change. This function posts a message to EOAM
 *                      task's message queue
 *
 * Input(s)           : u4IfIndex - Interface index
 *                      u1OperStatus - Operational link status of the port
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiNotifyIfOperChg (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1OperStatus);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMuxParActOnTx
 *                                                                          
 *    DESCRIPTION      : This function checks the Multiplexer state and 
 *                       decides whether to 
 *                       - Forward to received frame to HL or
 *                       - Discard the received frame or
 *                       - Loopback the received frame or
 *                       - Deliver the loopback test data to FM module.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       pBuf  - CRU buffer pointer to be transmitted
 *                       NOTE: ISS code has multiple Tx flows for
 *                       - physical if(CfaGddWrite - all L2 & L3 frames) and 
 *                       - logical if(CfaTxIvrPktOnPort - L3 frames on IVR if). 
 *                       CRU buffer pointer is passed to this function when 
 *                       called from CfaGddWrite, to validate the PDU t be tx.
 *                       NULL is passed in other cases.
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : EOAM_FWD - packet to be txmd.
 *                       EOAM_DISCARD - packet to be discarded.
 ****************************************************************************/
PUBLIC INT4
EoamMuxParActOnTx (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pBuf);
    return (EOAM_FWD);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : EoamMuxParActOnRx
 *                                                                          
 *    DESCRIPTION      : This function checks the Parser state and 
 *                       decides whether to 
 *                       - Forward to received frame to HL or
 *                       - Discard the received frame or
 *                       - Loopback the received frame or
 *                       - Deliver the loopback test data to FM module.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       pBuf - pointer to received buffer
 *                       u4PktSize - size of the buffer 
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : EOAM_FWD - If the rcvd frame can be delivered to HL.
 *                       EOAM_DISCARD - If the rcvd frame has to be discarded
 *                                      as PARSER is in DISCARD state.            
 *                       EOAM_LOOPBACK - If the rcvd frame has to be ignored
 *                                       since the node is in local LOOPBACK 
 *                                       mode.
 *                       EOAM_REMOTE_LB - If the rcvd frame has to be ignored 
 *                                        since the node is in REMOTE_LOOPBACK
 *                                        mode.
 ****************************************************************************/
PUBLIC INT4
EoamMuxParActOnRx (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                   UINT4 u4PktSize)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4PktSize);
    return (EOAM_FWD);
}

/******************************************************************************
 * Function Name      : EoamApiIsOAMPDU                                       
 *                                                                            
 * Description        : This function is called to check if the received      
 *                      frame is OAMPDU or not                                
 *                                                                            
 * Input(s)           : pBuf - Received frame buffer                          
 *                                                                            
 * Output(s)          : None.                                                 
 *                                                                           
 * Return Value(s)    : OSIX_TRUE - If the frame is an OAMPDU                 
 *                      OSIX_FALSE - If the frame is not an OAMPDU.           
 *****************************************************************************/
PUBLIC INT4
EoamApiIsOAMPDU (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UNUSED_PARAM (pBuf);
    return (OSIX_FALSE);
}

/******************************************************************************
 * Function Name      : EoamApiEnqIncomingPdu
 *
 * Description        : This function receives the incoming frame from the
 *                      Packet Reception module and enqueues it to the EOAM
 *                      task's queue and an appropriate event is sent
 *
 * Input(s)           : pBuf - pointer to CRU buffer of the incoming packet
 *                      u4PortIndex - Interface Index 
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
EoamApiEnqIncomingPdu (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PortIndex)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4PortIndex);
    return (OSIX_FAILURE);
}
#endif /* #ifndef EOAM_WANTED */

#ifndef VPN_WANTED
/******************************************************************************
 * Function Name      : VpnGetAssoIntfForVpnc
 *
 * Description        : This function is used to get Associated IfIndex and
 *                      Destination IP Address from the VPNC IfIndex
 *
 * Input(s)           : pBuf - Buffer contains IP Secured packet.
 *
 * Output(s)          : pu4AssoIfIndex - Associated IfIndex of VPNC IfIndex
 *                      pu4Dest - Destination IP Address
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
UINT4
VpnGetAssoIntfForVpnc (tIP_BUF_CHAIN_HEADER * pCBuf,
                       UINT4 *pu4AssoIfIndex, UINT4 *pu4Dest)
{
    UNUSED_PARAM (pCBuf);
    UNUSED_PARAM (pu4AssoIfIndex);
    UNUSED_PARAM (pu4Dest);
    return OSIX_SUCCESS;
}
#endif

#if (!defined SMOD_WANTED)
/******************************************************************************
 * Function Name      : SecPortHandleLogMessages
 *
 * Description        : This is a stub function which is needed in case of
 *                         security module is disabled to release the CRU buffer 
 *                         allocated to copy the attack packet.
 *
 * Input(s)           : pBuf - pointer to CRU buffer of the incoming packet
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/

INT4
SecPortHandleLogMessages (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    /* Releasing the CRU buffer allocated to copy the attack packet,
     * since security module is not enabled*/
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    return OSIX_SUCCESS;
}

#endif /* !SMOD_WANTED */

#ifndef SMOD_WANTED
UINT4
SecProcessFrame (tCRU_BUF_CHAIN_HEADER * pBuf, tSecIntfInfo * pSecIntfInfo,
                 UINT1 u1Direction, BOOL1 * pbSecVerdict)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pSecIntfInfo);
    UNUSED_PARAM (pbSecVerdict);
    UNUSED_PARAM (u1Direction);
    return OSIX_SUCCESS;
}
#endif /* !SMOD_WANTED */

#if (!defined SMOD_WANTED) || (defined SECURITY_KERNEL_MAKE_WANTED)
PUBLIC VOID
FwlTmrHandleTmrExpiry (VOID)
{
    return;
}

VOID
Secv4ProcessTimeOut (VOID)
{
    return;
}

#endif /* !SMOD_WANTED */
#ifdef MPLS_WANTED                /*MPLS WANTED */
/*****************************************************************************
 *
 *    Function Name       : L2VpnGetMPLSPwIndex
 *
 *    Description         : This is stub function.
 *
 *
 *    Input(s)            : NONE
 *
 *    Output(s)           : NONE
 *
 *    Returns             : NONE
 *
 *****************************************************************************/
INT4
L2VpnGetMPLSPwIndex (UINT4 u4IfIndex, UINT4 *pu4PwIndex)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PwIndex);
    return 0;
}

/*****************************************************************************
 *
 *    Function Name       : L2VpnDelMPLSPwIfIndex
 *
 *    Description         : This is stub function.
 *
 *
 *    Input(s)            : NONE
 *
 *    Output(s)           : NONE
 *
 *    Returns             : NONE
 *
 *****************************************************************************/
VOID
L2VpnDelMPLSPwIfIndex (UINT4 u4PwIndex)
{
    UNUSED_PARAM (u4PwIndex);
}
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  cfastubs.c                     */
/*-----------------------------------------------------------------------*/
