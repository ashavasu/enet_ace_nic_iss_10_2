/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddintons.c,v 1.5 2016/07/23 11:41:25 siva Exp $
 *
 * Description: This file contains the function implementations  of INONS NP-API.
 ****************************************************************************/
#include "cfainc.h"
#include "gddapi.h"
#include "ofcl.h"
#include <signal.h>
#include "iss.h"
#include "lnxip.h"

extern UINT1       *gpu1InvalidPortList;
/**************************** GLOBAL DEFINITIONS *****************************/

tFdListStruct       FdTable;    /* list of all the file descriptors given to the select
                                   call during polling */

UINT1              *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
VOID               CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 * au1IfName);
INT4                CfaGddGetLnxIntPortForName (CHR1 *pName);

#ifdef  WLC_WANTED
extern INT4         VlanGetContextInfoFromIfIndex (UINT4 u4IfIndex,
                                                   UINT4 *pu4ContextId,
                                                   UINT2 *pu2LocalPort);
extern INT4         VlanClassifyFrame (tCRU_BUF_CHAIN_DESC * pFrame,
                                       UINT2 u2EtherType, UINT2 u2Port);
extern INT4         VlanSelectContext (UINT4 u4ContextId);
extern INT4         VlanReleaseContext (VOID);
extern INT4         WlcHdlrEnqueCtrlPkts (unWlcHdlrMsgStruct *);
#endif

INT4        gi4CpuFd = -1;
UINT1       gau1CpuPortName[10] = "sw0p0";
/* Array containing the Mapping between Slot and Eth Interfaces */
static UINT1        au1IntMapTable[SYS_DEF_MAX_PHYSICAL_INTERFACES][2][10] =
{
{"Slot0/1", "sw0p1"},
{"Slot0/2", "sw0p2"},
{"Slot0/3", "sw0p3"},
{"Slot0/4", "sw0p4"},
{"Slot0/5", "sw0p5"},
{"Slot0/6", "sw0p6"},
{"Slot0/7", "sw0p7"},
{"Slot0/8", "sw0p8"},
{"Slot0/9", "sw0p0"},
};


/*****************************************************************************
 *
 *    Function Name        : CfaGddInit
 *
 *    Description        : This function performs the initialisation of
 *                the Generic Device Driver Module of CFA. This
 *                initialization routine should be called
 *                from the init of IFM after we have obtained
 *                the number of physical ports after parsing of
 *                the Config file. This function initializes the FD list
 *                and ifIndex array of the polling table.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddInit (VOID)
{
    tMacAddr            au1SwitchMac;    /* Base MAC address */

    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddInit \n");

    /* Bug-ISS29 :
     * ISS System Test: Verify switch base MAC can be changed */
    CfaGetSysMacAddress (au1SwitchMac);

    CfaNpUpdateSwitchMac (au1SwitchMac);

    if (CfaNpInit () == FNP_FAILURE)
    {
        PRINTF ("ERROR[NP] - CfaNpInit returned failure\n\r");
        return (CFA_FAILURE);
    }
    /* allocate memory for the GDD File Descriptor - polling table */
    CFA_GDD_FDLIST () = &aGddFdList[0];

    /* allocate memory for the ifIndex array - polling table */
    CFA_GDD_IFINDEX_LIST () = &au4GddIfIndexList[0];

    CFA_GDD_FDLIST_LAST_INDEX () = 0;

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_GDD,
             "Exiting CfaGddInit - successful Init.\n");
    signal (SIGINT, (void *) IssSystemRestart);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddShutdown
 *
 *    Description        : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                routine frees the polling table structure.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable.
 *
 *    Global Variables Modified : FdTable.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaGddShutdown (VOID)
{
    CFA_DBG (CFA_TRC_ALL, CFA_GDD, "Entering CfaGddShutdown \n");

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_GDD, "Exiting CfaGddShutdown - SUCCESS.\n");
}

/**************************************************************************/
INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_TYPE (u4IfIndex) = u1IfType;

    return (CFA_SUCCESS);
}

/**************************************************************************/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    UINT2               u2PollTableIndex = 0;
/* remove the port from the polling table */
    for (; u2PollTableIndex < CFA_GDD_FDLIST_LAST_INDEX (); ++u2PollTableIndex)
    {
        if (u4IfIndex == CFA_GDD_FDLIST_IFINDEX (u2PollTableIndex))
        {
            CFA_GDD_FDLIST_DEINIT (u2PollTableIndex);
            break;
        }
    }
    return;
}

/**************************************************************************/
VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_FDLIST_INIT (u4IfIndex, CFA_GDD_PORT_DESC (u4IfIndex));
}

/**************************************************************************/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
    UINT2               u2PollTableIndex = 0;
    UINT1              *pu1DataBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4PktSize;
    UINT4               u4IfIndex = 0;
    UINT2               u2PortIfIndex = 0;
    INT4                i4ReadPortNum = 0;
    UINT1               u1IfType;
    UINT1               u1SubType = CFA_SUBTYPE_SISP_INTERFACE;
#ifdef OPENFLOW_WANTED
    UINT4               u4VlanIndex = 0;
#endif

/* MAX_DRIVER_MTU * 2 in order to allow for the worst case of byte stuffing
 * for async interfaces where the packet size doubles over the MTU */
    if ((pu1DataBuf = MemAllocMemBlk (gCfaDriverMtuPoolId)) == NULL)
    {

        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddProcessRecvEvent - " "No Memory - FAILURE.\n");

        return (CFA_FAILURE);
    }

    if ((i4ReadPortNum = poll (CFA_GDD_FDLIST (), CFA_GDD_FDLIST_LAST_INDEX (),
                               0)) < 0)
    {

        MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaGddProcessRecvEvent - Poll Call - FAILURE.\n");
        return (CFA_FAILURE);
    }

    while ((i4ReadPortNum > 0) &&
           (u2PollTableIndex < CFA_GDD_FDLIST_LAST_INDEX ()))
    {

        u4IfIndex = CFA_GDD_FDLIST_IFINDEX (u2PollTableIndex);

        /* Check the validity of the index */
        if (u4IfIndex == 0 || u4IfIndex > SYS_DEF_MAX_INTERFACES)
        {
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                      "Error in CfaGddProcessRecvEvent\n"
                      "Value at index %d in FdTable is 0.\n", u2PollTableIndex);

            MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);

            return (CFA_FAILURE);
        }

        CfaGetIfType (u4IfIndex, &u1IfType);

        u2PortIfIndex = (UINT2) u4IfIndex;

/* if data is not available then dont do read operation */
        if (!CFA_GDD_IS_DATA_AVAIL (u2PollTableIndex))
        {

            ++u2PollTableIndex;
            continue;
        }

        --i4ReadPortNum;

/* now read all the data from the interface */
        while ((CFA_GDD_READ_FNPTR (u2PortIfIndex))
               (pu1DataBuf, u4IfIndex, &u4PktSize) != CFA_FAILURE)
        {
            CFA_LOCK ();

            CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
                      "In CfaGddProcessRecvEvent - got data from interface %d.\n",
                      u4IfIndex);
            CFA_DBG1 (CFA_TRC_ALL, CFA_GDD,
                      "In CfaGddProcessRecvEvent - got %d bytes.\n", u4PktSize);

#ifdef RMON_WANTED
/* Forward the frames to RMON module only when SW_FWD is enabled */
#ifdef SW_FWD
/* send packet to RMON before converting the buffer to CRU */
            if (CFA_GDD_TYPE (u2PortIfIndex) == CFA_ETHERNET)
            {
                /*invoke rmon update tables routine */
                if (CfaIwfRmonUpdateTables (pu1DataBuf, u4IfIndex, u4PktSize)
                    != CFA_FAILURE)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_GDD,
                             "In CfaGddProcessRecvEvent - update RMON tables by invoking CfaIwfRmonUpdateTables \n");
                }
                else
                {
                    CFA_DBG (CFA_TRC_ERROR, CFA_GDD,
                             "In CfaGddProcessRecvEvent - invoke CfaIwfRmonUpdateTables function failed \n");
                }
            }
#endif /* SW_FWD */
#endif /* RMON_WANTED */
/* allocate CRU Buf */
            if ((pBuf = CRU_BUF_Allocate_MsgBufChain ((u4PktSize +
                                                       CFA_MAX_FRAME_HEADER_SIZE),
                                                      CFA_MAX_FRAME_HEADER_SIZE))
                == NULL)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddProcessRecvEvent - "
                         "No CRU Buf Available - FAILURE.\n");

                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
                CFA_UNLOCK ();
                return CFA_FAILURE;
            }

/* copy data from linear buffer into CRU buffer - it is expected that the
higher layers will fill the CRU interface struct if required */
            if (CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0, u4PktSize) ==
                CRU_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddProcessRecvEvent - "
                         "unable to copy to CRU Buf- FAILURE.\n");

                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CFA_UNLOCK ();
                continue;
            }

#ifdef OPENFLOW_WANTED
            if (OfcHandleVlanPkt (pBuf, &u4VlanIndex) == OSIX_SUCCESS)
            {
                if (OpenflowNotifyPktRecv (u4VlanIndex, pBuf) == OFC_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                }
                CFA_UNLOCK ();
                continue;
            }
#endif /* OPENFLOW_WANTED */

            CfaGetIfSubType ((UINT4) u4IfIndex, &u1SubType);
            if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
            {
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_IF_SET_IN_UCAST (u4IfIndex);
#ifdef OPENFLOW_WANTED
                /* 
                 * Make the openflow ports visble to the ISS context when the Client 
                 * context associated with the IfIndex is in fail stand alone mode 
                 * and also process the packets via CFA.Else process the 
                 * packets through the Openflow Pipeline Process.
                 */
                if (OfcHandleOpenflowSwModeProcess (&u4IfIndex, pBuf) !=
                    OFC_SUCCESS)
                {
                    if (OpenflowNotifyPktRecv (u4IfIndex, pBuf) == OFC_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    }
                    CFA_UNLOCK ();
                    continue;
                }
#else
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_UNLOCK ();
                continue;
#endif /* OPENFLOW_WANTED */
            }

/* send the buffer to the higher layer - the port is not opened unless
some layer is defined abover this */

            if (CFA_GDD_HL_RX_FNPTR (u2PortIfIndex) == NULL)
            {
/* Higher layer is not found. Release buffer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
                CFA_UNLOCK ();
                continue;
            }

            if ((CFA_GDD_HL_RX_FNPTR (u2PortIfIndex))
                (pBuf, u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
/* release buffer which were not successfully sent to higher layer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            }

            CFA_UNLOCK ();
        }                        /* end of while loop - read while data is available */

        ++u2PollTableIndex;

    }                            /* end of while loop - polling */

/* free the linear buffer allocated */
    MemReleaseMemBlock (gCfaDriverMtuPoolId, (UINT1 *) pu1DataBuf);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddConfigPort
 *
 *    Description        : Function is called for configuration of the
 *                Enet ports. No such support is available for
 *                Wanic at present. Only the Promiscuous mode
 *                and multicast addresses can be configured at
 *                present for Enet ports. This is called
 *                directly whenever the Manager configures the
 *                ifTable for Ethernet ports. It can also be
 *                called indirectly by Bridge or RIP, etc.
 *                through the CfaIfmEnetConfigMcastAddr API.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1ConfigOption,
 *                VOID *pConfigParam.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{

    INT4                i4RetVal = CFA_SUCCESS;
    UINT1              *pu1PortName = NULL;

/* For ETH_SOCK */
    struct ifreq        ifr;    /* struct which contains status of interface */
    INT4                i4CommandStatus;
/* end of for ETH_SOCK */

#ifdef LNXIP4_WANTED
    if (u1ConfigOption == CFA_IF_UP)
        u1ConfigOption = CFA_ENET_LNXIP_INT_CFA_UP;
    else if (u1ConfigOption == CFA_IF_DOWN)
        u1ConfigOption = CFA_ENET_LNXIP_INT_CFA_DOWN;

#endif

    CFA_DBG2 (CFA_TRC_ALL, CFA_GDD,
              "Entering CfaGddConfigPort for interface %d and config %d.\n",
              u4IfIndex, u1ConfigOption);

    /* check if the IfIndex is valid */
    if ((u4IfIndex > SYS_DEF_MAX_INTERFACES) || (u4IfIndex < 1))
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddConfigPort - "
                  "Interface %d not Valid - FAILURE.\n", u4IfIndex);

        return (CFA_FAILURE);
    }

    if ((!CFA_IF_ENTRY_USED (u4IfIndex)) || (!CFA_GDD_ENTRY_USED (u4IfIndex)))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_MAIN,
                  "Error in CfaGddConfigPort - un-United interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

/* dont configure for ports which are not registered - we dont have
socket/file descriptors for them */
    if (CFA_GDD_REGSTAT (u4IfIndex) == CFA_INVALID)
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddConfigPort - "
                  "Interface %d not Registered - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

    if (pu1PortName == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddConfigPort - "
                  "Not able to get Interface name for %d - FAILURE.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    switch (u1ConfigOption)
    {
        case CFA_ENET_EN_PROMISC:
/* get ethX interface flags */
            STRNCPY (ifr.ifr_name, pu1PortName, STRLEN (pu1PortName));
            ifr.ifr_name[STRLEN (pu1PortName)] = '\0';

            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCGIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in getting interface flags\n");
                i4RetVal = CFA_FAILURE;
                break;
            }
/* set the IFF_PROMISC flag to enter promiscuous mode and set ethX interface
flags */
            ifr.ifr_flags = ifr.ifr_flags | IFF_PROMISC;
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCSIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in setting promisc mode\n");
                i4RetVal = CFA_FAILURE;
            }
            else
            {
                CFA_IF_PROMISC (u4IfIndex) = CFA_ENABLED;
            }
            break;

        case CFA_ENET_DIS_PROMIS:
/* get ethX interface flags */
            STRNCPY (ifr.ifr_name, pu1PortName, STRLEN (pu1PortName));
            ifr.ifr_name[STRLEN (pu1PortName)] = '\0';

            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCGIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in getting interface flags\n");
                i4RetVal = CFA_FAILURE;
                break;
            }

/* reset the IFF_PROMISC flag to disable promiscuous mode and set ethX
interface flags */
            ifr.ifr_flags = ifr.ifr_flags & (~IFF_PROMISC);
            /* To avoid pkt queing when interface admin status is down */
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCSIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in reset   promiscuous mode\n");
                i4RetVal = CFA_FAILURE;
            }
            else
            {
                CFA_IF_PROMISC (u4IfIndex) = CFA_DISABLED;
            }
            break;

/* it is assumed that the multicast mode is enabled on the Ethernet
interfaces - this is the default setting */
        case CFA_ENET_ADD_MCAST:
/* add the address to the Enet port's multicast address list */
            STRNCPY (ifr.ifr_name, pu1PortName, STRLEN (pu1PortName));
            ifr.ifr_name[STRLEN (pu1PortName)] = '\0';
            ifr.ifr_hwaddr.sa_family = AF_UNSPEC;
            MEMCPY (ifr.ifr_hwaddr.sa_data, pConfigParam, CFA_ENET_ADDR_LEN);
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCADDMULTI, &ifr)) < 0)
            {
                perror ("Error in add mcast address\n");
                i4RetVal = CFA_FAILURE;
            }

            break;

        case CFA_ENET_DEL_MCAST:
/* delete the address from the Enet port's multicast address list */
            STRNCPY (ifr.ifr_name, pu1PortName, STRLEN (pu1PortName));
            ifr.ifr_name[STRLEN (pu1PortName)] = '\0';
            ifr.ifr_hwaddr.sa_family = AF_UNSPEC;
            MEMCPY (ifr.ifr_hwaddr.sa_data, pConfigParam, CFA_ENET_ADDR_LEN);
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCDELMULTI, &ifr)) < 0)
            {
                perror ("Error in delete mcast address\n");
                i4RetVal = CFA_FAILURE;
            }

            break;

        case CFA_ENET_ADD_ALL_MCAST:
/* receive all the mcast frames on the specified port */
/* get ethX interface flags */
            STRNCPY (ifr.ifr_name, pu1PortName, STRLEN (pu1PortName));
            ifr.ifr_name[STRLEN (pu1PortName)] = '\0';
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCGIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in getting interface flags\n");
                i4RetVal = CFA_FAILURE;
                break;
            }
/* set the IFF_ALLMULTI flag to enable all multicast addresses */
            ifr.ifr_flags = ifr.ifr_flags | IFF_ALLMULTI;
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCSIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in setting all multi ON mode\n");
                i4RetVal = CFA_FAILURE;
            }

            break;

        case CFA_ENET_DEL_ALL_MCAST:
/* stop the receiving of all the mcast frames on the specified port */
/* get ethX interface flags */
            STRNCPY (ifr.ifr_name, pu1PortName, STRLEN (pu1PortName));
            ifr.ifr_name[STRLEN (pu1PortName)] = '\0';
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCGIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in getting interface flags\n");
                i4RetVal = CFA_FAILURE;
                break;
            }
/* reset the IFF_ALLMULTI flag to disable all multicast addresses */
            ifr.ifr_flags = ifr.ifr_flags | (~IFF_ALLMULTI);
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCSIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in setting all multi OFF mode\n");
                i4RetVal = CFA_FAILURE;
            }

            break;
#ifdef LNXIP4_WANTED
        case CFA_ENET_LNXIP_INT_CFA_UP:
            STRCPY (ifr.ifr_name, pu1PortName);
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCGIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in getting interface flags\n");
                i4RetVal = CFA_FAILURE;
                break;
            }
            ifr.ifr_flags = ifr.ifr_flags | IFF_UP | IFF_RUNNING;
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCSIFFLAGS, &ifr)) < 0)
            {
                perror
                    ("Error in setting interface operational status to up\n");
                i4RetVal = CFA_FAILURE;
            }
            break;
        case CFA_ENET_LNXIP_INT_CFA_DOWN:
            STRCPY (ifr.ifr_name, pu1PortName);
            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCGIFFLAGS, &ifr)) < 0)
            {
                perror ("Error in getting interface flags\n");
                i4RetVal = CFA_FAILURE;
                break;
            }

            ifr.ifr_flags = (ifr.ifr_flags & ~(IFF_RUNNING));
            ifr.ifr_flags = (ifr.ifr_flags & ~(IFF_UP));

            if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex),
                                          SIOCSIFFLAGS, &ifr)) < 0)
            {
                perror
                    ("Error in setting interface operational status to down\n");
                i4RetVal = CFA_FAILURE;
            }
            break;

#endif
        default:
            i4RetVal = CFA_FAILURE;
            break;
    }

    if (i4RetVal == CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_GDD,
                  "In CfaGddConfigPort - Configured interface %d - SUCCESS.\n",
                  u4IfIndex);
    }
    else
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "In CfaGddConfigPort - Configuration of "
                  "interface %d - FAILURE.\n", u4IfIndex);
    }

    return (i4RetVal);

}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetOsHwAddr
 *
 *    Description        : Function is called for Opening the port
 *                         and getting the Hw Address of the port
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                gaPhysIfTable (GDD Reg Table) structure.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
/* For ETH_SOCK */
    struct ifreq        ifr;    /* struct which contains status of interface */
    INT4                i4CommandStatus;
    UINT1               u1IfType;
/* end of for ETH_SOCK */
    UINT1               au1IfHwAddr[CFA_ENET_ADDR_LEN];
    UINT1              *pu1PortName = NULL;

    CfaGetIfaceType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_PSEUDO_WIRE || u1IfType == CFA_PPP)
    {
        if (CfaGetPswHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitEnetIfEntry - "
                      "unable to get the socket or MAC address for interface %d\n",
                      u4IfIndex);
            return CFA_FAILURE;
        }
        return CFA_SUCCESS;
    }

    if (CfaGddLinuxEthSockOpen (u4IfIndex) != CFA_SUCCESS)
    {
        return (CFA_FAILURE);
    }

    if ((u4IfIndex > SYS_DEF_MAX_INTERFACES) || (u4IfIndex < 1))
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetOsHwAddr - "
                  "Interface %u not Valid - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    /* 
     * When the request comes from ARP for the hardware address 
     * of VLAN interface, this function should not return FAILURE. Instead, 
     * copy the MAC address of the default bridged Ethernet interface into 
     * the MAC address of VLAN interface and ARP/RARP will update the same 
     * for VLAN interfaces.
     */
    if (gu4IsIvrEnabled == CFA_ENABLED)
    {
        if ((u1IfType == CFA_L3IPVLAN)
#ifdef WGS_WANTED
            || (u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
            )
        {
            CfaGetIfHwAddr (CFA_DEFAULT_ROUTER_IFINDEX, au1IfHwAddr);
            MEMCPY (au1HwAddr, au1IfHwAddr, CFA_ENET_ADDR_LEN);

            return CFA_SUCCESS;
        }
    }

/* cant get HW address for ports which are not registered - we dont have
socket/file descriptors for them and only ETH_SOCK is supported currently */
    if ((CFA_GDD_REGSTAT (u4IfIndex) == CFA_INVALID) ||
        (CFA_GDD_TYPE (u4IfIndex) != CFA_ETHERNET))
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetOsHwAddr - "
                  "Interface %u not Registered/Enet - FAILURE.\n", u4IfIndex);

        return (CFA_FAILURE);
    }

/* invoke the IOCTL call on the socket */
    pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

    /*If No Mapping Exists,No Such Physical Interface can Exists.So Just
       return FAILURE */
    if (pu1PortName == NULL)
    {
        return (CFA_FAILURE);
    }

    STRNCPY (ifr.ifr_name, pu1PortName, STRLEN (pu1PortName));
    ifr.ifr_name[STRLEN (pu1PortName)] = '\0';

    if ((i4CommandStatus = ioctl (CFA_GDD_PORT_DESC (u4IfIndex), SIOCGIFHWADDR,
                                  &ifr)) < 0)
    {
        perror ("Error in getting MAC address\n");
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddGetOsHwAddr - "
                  "IOCTL on Interface %u failed - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    MEMCPY (au1HwAddr, ifr.ifr_hwaddr.sa_data, 6);
    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_GDD,
              "In CfaGddGetOsHwAddr - "
              "Got Address of interface %u - SUCCESS.\n", u4IfIndex);

    return CFA_SUCCESS;
}

INT4
CfaGddLinuxEthSockOpen (UINT4 u4IfIndex)
{
    struct sockaddr_ll  Enet;
    struct ifreq        ifr;
    INT4                i4PortDesc = 0;

    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT1              *pu1PortName = NULL;
    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));
    MEMSET (ai1IfName, 0, sizeof (ai1IfName));

    if (u4IfIndex > SYS_DEF_MAX_INTERFACES)
    {
        return CFA_FAILURE;
    }
    pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

    /*If No Mapping Exists,No Such Physical Interface can Exists.So Just
      return FAILURE */
    if (pu1PortName == NULL)
    {
        return (CFA_FAILURE);
    }

    if ((i4PortDesc = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0)
    {
        perror ("Error in opening port\n");
        return CFA_FAILURE;
    }
    CFA_GDD_PORT_DESC (u4IfIndex) = i4PortDesc;

    sprintf (ifr.ifr_name, "%s", pu1PortName);

    if (ioctl (i4PortDesc, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        if (gpu1InvalidPortList != NULL)
        {
            OSIX_BITLIST_SET_BIT (gpu1InvalidPortList, u4IfIndex,
                                  sizeof (tPortList));
        }
        /* Print the error message in case of restoration thread only
         * Because regular msimod already contains a error message
         */
        if (MsrIsMibRestoreInProgress () == SUCCESS)
        {
            /* In case of MI we need to print this. Non MI
             * case is already handled by gpu1InvalidPortList
             */
            if (VcmGetL2Mode () == VCM_MI_MODE)
            {
                CfaGetPhysIfName (u4IfIndex, ai1IfName);
                UtlTrcLog (1, 1, "CFA", "Interface %s cannot be restored\n",
                           ai1IfName);
            }
        }

        close (i4PortDesc);
        return CFA_FAILURE;
    }

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;
    /*Porting index to router port */
    CFA_CDB_IF_IP_IPPORT (u4IfIndex) = ifr.ifr_ifindex;
    if (bind (i4PortDesc, (struct sockaddr *) &Enet, sizeof (Enet)) < 0)
    {
        perror ("Bind error\n");
        close (i4PortDesc);
        return CFA_FAILURE;
    }

    /* set options for non-blocking mode */
    if (fcntl (i4PortDesc, F_SETFL, O_NONBLOCK) < 0)
    {
        perror ("Failure in setting ETH SOCK option\n");
        close (i4PortDesc);
    }

    KW_FALSEPOSITIVE_FIX3 (i4PortDesc);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *
 *    Description        : This function opens the Ethernet port and stores the
                           descriptor in the interface table. Since the port is 
                           opened already during the registration time, this 
                           function is dummy.
 *
 *    Input(s)            : u4IfIndex - Interface Index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure
 *
 *    Global Variables Modified :  gapIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if open succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{
/* socket already opened during register call */
    /* during admin status down promiscous mode is disabled, when we make the *
     * admin status up we have to enable the promiscuous mode */
    CfaGddConfigPort (u4IfIndex, CFA_ENET_EN_PROMISC, NULL);
    u4IfIndex = u4IfIndex;        /* for warning removal */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *
 *    Description        : First disables the promiscuous mode of 
 *                         the port, if enabled. The multicast address
 *                         list is lost when the port is closed.i
 *                         We dont need to check if the call is a 
 *                         success or not.
 *
 *    Input(s)            : u4IfIndex - Interface index.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : gapIfTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if close succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    /* first disable the promiscuous mode of the port,
     * if enabled. the multicast address list is lost
     * when the port is closed. We dont need to check if the
     * call is a success or not. 
     */

    CfaGddConfigPort (u4IfIndex, CFA_ENET_DIS_PROMIS, NULL);

    /* closing the SOCK PACKET results in deletion of
     * the ethX interface from Linux itself - hence
     * commented out close(CFA_GDD_PORT_DESC(u4IfIndex));
     */
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthWrite
 *
 *    Description        : Writes the data to the ethernet driver.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    struct sockaddr_ll  Enet;
    UINT4               u4ToAddrLen;
    INT4                i4WrittenBytes;
    struct ifreq        ifr;
    UINT1              *pu1PortName = NULL;
    UINT1               u1SubType = CFA_SUBTYPE_SISP_INTERFACE;
    INT4                i4SockFd = -1;

    if (u4IfIndex > SYS_DEF_MAX_INTERFACES)
    {
        return CFA_FAILURE;
    }

    pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

    if (pu1PortName == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddConfigPort - "
                  "Not able to get Interface name for %d - FAILURE.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    sprintf (ifr.ifr_name, "%s", pu1PortName);
    i4SockFd = CFA_GDD_PORT_DESC (u4IfIndex);
    if (ioctl (i4SockFd, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("Interface Index Get Failed");
        close (i4SockFd);
        return CFA_FAILURE;
    }

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;

    u4ToAddrLen = sizeof (Enet);

    if ((i4WrittenBytes =
         sendto (i4SockFd, (VOID *) pu1DataBuf, u4PktSize,
                 0, (struct sockaddr *) &Enet, (socklen_t) u4ToAddrLen)) < 0)
    {
        return CFA_FAILURE;
    }

    if (i4WrittenBytes != (INT4) u4PktSize)
        return CFA_FAILURE;

    CfaGetIfSubType ((UINT4) u4IfIndex, &u1SubType);

    if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
    {
        CFA_IF_SET_OUT_UCAST (u4IfIndex);
        CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4PktSize);
    }

    return CFA_SUCCESS;

}

/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *					        be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen, 
		       UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}
/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *
 *    Description        : This function reads the data from the simernet port.
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - IfIndex of the interface
 *                          pu4PktSize - Pointer to the packet size
 *
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    struct sockaddr_ll  Enet;
    struct sockaddr    *pPort;
    int                 i4FromAddrLen = 0;
    INT4                i4ReadBytes;
    tEnetV2Header      *pEthHdr;
    tEnetSnapHeader    *pSnapEthHdr;
    INT2                i2IpTotalLen = 0;
    UINT2               u2Protocol = 0xFF;
    UINT2               u2LenOrType;
    UINT4               u4ExtraBytes = 0;
    UINT1               u1EnetHeaderSize;
    UINT1              *pu1PortName = NULL;
    struct ifreq        ifr;
    INT4                i4SockFd = -1;

    if (u4IfIndex > SYS_DEF_MAX_INTERFACES)
    {
        return CFA_FAILURE;
    }

    pu1PortName = CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex);

    if (pu1PortName == NULL)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                  "Error in CfaGddConfigPort - "
                  "Not able to get Interface name for %d - FAILURE.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    i4SockFd = CFA_GDD_PORT_DESC (u4IfIndex);
    sprintf (ifr.ifr_name, "%s", pu1PortName);

    if (ioctl (i4SockFd, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("Interface Index Get Failed");
        close (i4SockFd);
        return CFA_FAILURE;
    }

    MEMSET (&Enet, 0, sizeof (Enet));
    MEMSET (&ifr, 0, sizeof (ifr));

    Enet.sll_family = AF_PACKET;
    Enet.sll_ifindex = ifr.ifr_ifindex;

    pPort = (struct sockaddr *) &Enet;

    if ((i4ReadBytes = recvfrom (i4SockFd,
                                 (VOID *) pu1DataBuf, CFA_MAX_DRIVER_MTU, 0,
                                 (struct sockaddr *) pPort,
                                 (socklen_t *) & i4FromAddrLen)) <= 0)
    {
        return CFA_FAILURE;
    }

    if (u4IfIndex != 0)
    {
        CFA_IF_SET_IN_OCTETS (u4IfIndex, (UINT4) i4ReadBytes);
    }

    pEthHdr = (tEnetV2Header *) (VOID *) pu1DataBuf;
    u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        u2Protocol = u2LenOrType;
        u1EnetHeaderSize = CFA_ENET_V2_HEADER_SIZE;
    }
    else
    {
        pSnapEthHdr = (tEnetSnapHeader *) (VOID *) pu1DataBuf;
        u1EnetHeaderSize = CFA_ENET_SNAP_HEADER_SIZE;

        /* check for LLC control frame first - we expect only LLC in SNAP now */
        if (pSnapEthHdr->u1Control == CFA_LLC_CONTROL_UI)
        {
            /*check for presence of SNAP which may carry IP/ARP/RARP after LLC */
            if ((pSnapEthHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1Oui1 == 0x00) ||
                (pSnapEthHdr->u1Oui2 == 0x00) || (pSnapEthHdr->u1Oui3 == 0x00))
            {
                /* determine which protocol is being carried */
                u2Protocol = OSIX_NTOHS (pSnapEthHdr->u2ProtocolType);
            }                    /* end of SNAP framing */

        }                        /* end of LLC framing */

    }

    if (u2Protocol == CFA_ENET_IPV4)
    {
        MEMCPY (&i2IpTotalLen, (pu1DataBuf + u1EnetHeaderSize
                                + IP_PKT_OFF_LEN), sizeof (INT2));
        i2IpTotalLen = (INT2) OSIX_NTOHS (i2IpTotalLen);

        if ((i2IpTotalLen + u1EnetHeaderSize) > i4ReadBytes)
        {
            return CFA_FAILURE;
        }

        u4ExtraBytes =
            (UINT4) i4ReadBytes - (UINT4) (i2IpTotalLen + u1EnetHeaderSize);
    }
#ifdef IP6_WANTED
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        MEMCPY (&i2IpTotalLen, (pu1DataBuf + u1EnetHeaderSize +
                                IPV6_OFF_PAYLOAD_LEN), sizeof (INT2));
        i2IpTotalLen = (INT2) OSIX_NTOHS (i2IpTotalLen);

        if ((i2IpTotalLen + u1EnetHeaderSize + IPV6_HEADER_LEN) > i4ReadBytes)
        {
            return CFA_FAILURE;
        }

        u4ExtraBytes =
            (UINT4) i4ReadBytes - ((UINT4) i2IpTotalLen +
                                   (UINT4) u1EnetHeaderSize +
                                   (UINT4) IPV6_HEADER_LEN);
    }
#endif /*IP6_WANTED */

    (*pu4PktSize) = (UINT4) i4ReadBytes - u4ExtraBytes;

    return CFA_SUCCESS;
}

UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    UINT1               u1IfType = 0;
    UINT1               u1OperStatus = CFA_IF_UP;

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_ENET)
    {
        return CfaCfaNpGetLinkStatus (u4IfIndex);
    }
    else
    {
        CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
        return (u1OperStatus);
    }
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetLnxIntfnameForPort
 *
 *    Description          : This function Returns LinuxEth Name Based on Slot 
 *                           Referred. 
 *    Input(s)            :  u4IfIndex - Interface index
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure 
 *                
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : EthName if Slot is found Else NULL
 *
 *****************************************************************************/
UINT1* CfaGddGetLnxIntfnameForPort (UINT2 u2Index)
{
    INT4                i4Index;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    if ((u2Index == 0) || (u2Index > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return NULL;
    }
    if (u2Index == 9)
    {
        /* FIX - at start up before creating CPU port, this function 
         * would be called. For ex: adding other ports in a VLAN makes CPU
         * port also to be added in that particular VLAN. In such case to avoid
         * crash, returing the CPU port name directly would be a work around. */
        return gau1CpuPortName; 
    }

    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {
        MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

        STRCPY (au1Temp, au1IntMapTable[i4Index]);

        if (MEMCMP (CFA_GDD_PORT_NAME (u2Index), au1Temp,
                    STRLEN (CFA_GDD_PORT_NAME (u2Index))) == 0)
        {
            return au1IntMapTable[i4Index][1];
        }
    }
    return NULL;
}
INT4
CfaGddGetLnxIntPortForName (CHR1 *pName)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_DEF_MAX_PHYSICAL_INTERFACES; i4Index++)
    {

        if (STRCMP (pName, au1IntMapTable[i4Index][1]) == 0)
        {
            return (i4Index + 1);
        }
 
    }
    return 0;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddSetLnxIntfnameForPort
 *
 *    Description          : This function sets Interface Name for the interface
 *                           index.
 *    Input(s)            :  u4IfIndex - Interface index
 *                           au1IfName - Interface name
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table) structure
 *
 *    Global Variables Modified : gaIfTable (Interface table) structure
 *
 *    Exceptions or Operating
 *    System Error Handling   : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns                 : NULL
 *
 *****************************************************************************/
VOID        
CfaGddSetLnxIntfnameForPort (UINT4 u4Index, UINT1 * au1IfName)
{

    if (u4Index == 0)
    {
        return;
    }

    STRNCPY (au1IntMapTable[u4Index], CFA_GDD_PORT_NAME (u4Index),
            CFA_MAX_PORT_NAME_LENGTH); 

    STRNCPY (au1IntMapTable[u4Index][1], au1IfName, CFA_MAX_PORT_NAME_LENGTH);
    CfaSetIfName (u4Index, au1IfName);
    return;
}
#ifdef MBSM_WANTED
/*****************************************************************************
 *    Function Name       : CfaMbsmGddInit
 *    Description         : This function performs the initialisation of
 *                          the  Device Driver Module of CFA. This routine
 *                          1. creates the Queue for reception of packets from
 *                             driver
 *                          2. creates the memory pool for the driver messages
 *                          3. initializes the driver to cfa mapping tables
 *    Input(s)            : None.
 *    Output(s)           : None.
 *
 *    Global Variables Referred : _devices, _ndevices
 *    Global Variables Modified : gFsDrvMemPoolId, gaCfaNpIndexMap
 *                                gaCfaNpDevMap
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/

PUBLIC INT4
CfaMbsmGddInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name        : CfaMbsmGddDeInit
 *    Description         : This function performs the shutdown of
 *                the Generic Device Driver Module of CFA. This
 *                shutdown routine should be called
 *                before the protocols are informed about the shutdown.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
PUBLIC INT4
CfaMbsmGddDeInit (tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pSlotInfo);
    return CFA_SUCCESS;
}
#endif /* MBSM_WANTED */

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId,
                                                (UINT1) bBcast, &u4OutPort,
                                                &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED 
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}

/* ============================= WSS related functions ================ */

#ifdef WLC_WANTED
INT4
CfaGddWssOpen (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

INT4
CfaGddWssClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddWssWrite
 *
 *    Description        : Writes the data to the ethernet driver
 *                         by constructing CAPWAP messages and posts from
 *                         CFA to WSS
 *
 *    Input(s)            : pu1DataBuf - Pointer to the linear buffer.
 *                          u4IfIndex - MIB-2 interface index
 *                          u4PktSize - Size of the buffer.
 *
 *    Output(s)            : None.
 *
 *    Returns            : CFA_SUCCESS if write succeeds,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaGddWssWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    UINT1               u1OperStatus = 0;
    unWlcHdlrMsgStruct  *pcapwapMsgStruct = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;

    UINT2               u2EtherType = 0;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);
    if (pBuf == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: Unable to Allocate CRU Buffer \n");
        return CFA_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0, u4PktSize) ==
        CRU_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: Unable to copy to CRU Buffer \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
    if (u1OperStatus != CFA_IF_UP)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: The VAP interface is not Operationally UP\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    /*Vlan tag is added when the packet is sent frmo WSS to CFA */
    /*Similarly Untag the frame before giving the packet to WSS */
    VLAN_LOCK ();
    if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPort) != VLAN_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: VlanGetContextInfoFromIfIndex FAILED\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
	
    } 

    if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "CfaGddWssWrite: VlanSelectContext FAILED\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        VLAN_UNLOCK ();
        return VLAN_FAILURE;
    }

    /*Fetch the etherType of the packet .Ethertype is present after Dest Mac + Src Mac */

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                               (CFA_ENET_ADDR_LEN * 2), sizeof (u2EtherType));

    u2EtherType = OSIX_NTOHS (u2EtherType);

    if ((VlanClassifyFrame (pBuf, u2EtherType, u2LocalPort) != VLAN_UNTAGGED))
    {
        VlanUnTagFrame (pBuf);
    }

    VlanReleaseContext ();
    VLAN_UNLOCK ();

    pcapwapMsgStruct =  (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();
    MEMSET (pcapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
    pcapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf = pBuf;
    pcapwapMsgStruct->WlcHdlrQueueReq.u2SessId = u4IfIndex;
    pcapwapMsgStruct->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CFA_RX_MSG;

    /* Invoke the WSSIF module to send the packet to MAC Handler module */
    /* Posting the packet from CFA to WSS for sending Tx capwap message */
    if (WlcHdlrEnqueCtrlPkts (pcapwapMsgStruct) != OSIX_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                 "L2 Packet processing in WSS failed for VAP Index\n");
        UtlShMemFreeWlcBuf ((UINT1 *) pcapwapMsgStruct);
        return CFA_FAILURE;
    }
    UtlShMemFreeWlcBuf ((UINT1 *) pcapwapMsgStruct);
    return CFA_SUCCESS;
}

INT4
CfaGddWssRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
    return CFA_SUCCESS;
}
#endif
