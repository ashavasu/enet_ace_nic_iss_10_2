/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfamain.c,v 1.211 2017/10/10 11:57:20 siva Exp $
 *
 * Description:This file contains the routines common to all
 *             the CFA modules. It also contains routines  
 *             for the init and shutdown of the entire CFA 
 *             and the main loop of the CFA.               
 *
 *******************************************************************/
#include "cfainc.h"

#ifdef SNMP_2_WANTED
#include "fscfawr.h"
#include "ifmibwr.h"
#include "stdethwr.h"
#include "stdmauwr.h"
#include "fstunlwr.h"
#include "fsmptuwr.h"
#endif

#include "cust.h"

#ifdef MPLS_WANTED
#include "mpls.h"
#endif
#include "cfaglob.h"
#include "mux.h"
#include "cfamux.h"
#include "natfragext.h"

#ifdef OPENFLOW_WANTED
#include "ofcl.h"
#endif /* OPENFLOW_WANTED */
extern tIssBool     MsrisRetoreinProgress;
#ifdef  WLC_WANTED
extern INT4         VlanGetContextInfoFromIfIndex (UINT4 u4IfIndex,
                                                   UINT4 *pu4ContextId,
                                                   UINT2 *pu2LocalPort);
extern INT4         VlanClassifyFrame (tCRU_BUF_CHAIN_DESC * pFrame,
                                       UINT2 u2EtherType, UINT2 u2Port);
extern INT4         VlanSelectContext (UINT4 u4ContextId);
extern INT4         VlanReleaseContext (VOID);
extern INT4         WlcHdlrEnqueCtrlPkts (unWlcHdlrMsgStruct *);
#endif
extern INT1         gu1MplsInitialised;

/**************************** GLOBAL DEFINITIONS *****************************/

 /* obtained during spawning of CFA task - currently
    there is only task which is the CFA fowarding task. In a dual processor
    scenario there will be another CFA task in the control processor also. */
UINT1               gu1CfaDefIpv6Initialised = TRUE;    /*Default ipv6 configuration Flag */
extern tOsixTaskId  gCfaPktTaskId;
extern tOsixTaskId  gCfaTxPktTaskId;
tOsixTaskId         gCfaTaskId = 0;
UINT1               gu1CfaInitialised = FALSE;
UINT4               gu4CfaTraceLevel = 0;
UINT4               gu4CfaDebugLevel = 0;
UINT4               gau4ModTrcEvents[MAX_MOD_EVENTS];
UINT4               u4CfaTaskPriority = CFA_TASK_PRIORITY;
/* for changing priority during debug */
tTimerListId        CfaGddTimerListId;
/*Link Up delay parameter*/
tTimerListId        CfaLinkUpDelayTimerListId;
tCfaTimerDesc       gGlobalTimerDesc;
tLnkUpSysControl    gLnkUpSystemControl;
INT4                gu4CfaLinkUpStatus;
tOsixSemId          gCfaSemId;
#ifndef CFA_INTERRUPT_MODE
/* CFA's timer list which has GDD poll timer */
tTmrAppTimer        GddPollTimer;    /* GDD poll timer */
#endif /* CFA_INTERRUPT_MODE */
tOsixQId            gCfaPktMuxQId;
tOsixQId            gCfaPktQId;
tOsixQId            gCfaTxPktQId;
tOsixQId            gCfaPktPlQId;
tOsixQId            gCfaExtTrgrQId;
tOsixQId            gCfaPktParamsQId;
tOsixQId            gCfaAttackPktQId;
UINT1               gu1CfaDataPktQueueStatus = CFA_DISABLED;
UINT1               gu1CfaPortListPktQueueStatus = CFA_DISABLED;
UINT1               gu1CfaExtTriggerQueueStatus = CFA_DISABLED;
UINT1               gu1CfaAttackPktQueueStatus = CFA_DISABLED;
UINT1               gu1IpForwardingEnable = CFA_FALSE;
UINT4               gu4IsIvrEnabled = CFA_ENABLED;    /* CFA_ENABLED */
UINT4               gu4SecModeForBridgedTraffic = CFA_SEC_BRIDGE_DISABLED;
UINT4               gu4SecIvrIfIndex = CFA_INVALID_INDEX;
UINT4               gu4StartIfIndex = 1;
UINT4               gu4EndIfIndex = SYS_DEF_MAX_PHYSICAL_INTERFACES;
tHwPortInfo         gLocalUnitHwPortInfo;
/* Default port parameter structure to be used after clear configuration */
tCfaDefaultPortParams gCfaDefaultPortInfo[SYS_DEF_MAX_PHYSICAL_INTERFACES];
UINT4               gu4ConnectingPortCount = 0;
UINT2               gCfaDefautVlanId;
UINT4               gu4CfaSysLogId = 0;
tMemPoolId          gCfaMbsmSlotMsgPoolId = 0;
tMemPoolId          gCfaMbsmProtoMsgPoolId = 0;
tMemPoolId          gCfaPktPoolId = 0;
tMemPoolId          gCfaDriverMtuPoolId = 0;
tMemPoolId          gCfaStackPoolId = 0;
tMemPoolId          gCfaILanPortPoolId = 0;
tMemPoolId          gCfaRcvAddrPoolId = 0;
tMemPoolId          gCfaIntfStatusPoolId = 0;
tMemPoolId          gCfaPortArrayPoolId = 0;
tMacAddr            gu1CfaUnNumMCastMac =
    { 0x01, 0x00, 0x5E, 0x90, 0x00, 0x00 };

UINT4               gu4IfOOBNode0SecondaryIpAddress;
UINT4               gu4IfOOBNode0SecondaryIpMask;
UINT4               gu4IfOOBNode1SecondaryIpAddress;
UINT4               gu4IfOOBNode1SecondaryIpMask;
UINT4               gu4DualOob;

/*****************************************************************************
 *
 *    Function Name        : CfaInit
 *
 *    Description        : This function performs the initialisation of
 *                         the CFA. It is to be called at the begining
 *                         of the root task before any other module
 *                         (i.e. IP, PPP, etc.) are initialised.
 *
 *    Input(s)            : None. 
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if initialisation succeeds,
 *                            otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaInit (VOID)
{
    INT4                i4RetVal;

#ifdef L2RED_WANTED
    /* Get the node status from RM. */
    CfaGetNodeStatusFromRm ();
#else
    CFA_NODE_STATUS () = CFA_NODE_ACTIVE;
    CFA_RED_IPINIT_COMPLETE_FLAG () = CFA_TRUE;
#endif

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Entering CfaInit.\n");
    CFA_DEFAULT_VLAN_ID = IssGetDefaultVlanIdFromNvRam ();
    L2IwfSetDefaultVlanId (CFA_DEFAULT_VLAN_ID);

#ifndef MBSM_WANTED
    /* initialise the IFM */
    CfaNpFillInterfaceName ();
#endif /* MBSM_WANTED */
    MEMSET (&gCfaDefaultPortInfo, 0, sizeof (tCfaDefaultPortParams));
    MEMSET (&gLocalUnitHwPortInfo, 0, sizeof (tHwPortInfo));

    if ((i4RetVal = CfaCdbInit ()) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaInit - IFM Init Fail - FAILURE.\n");

        return (i4RetVal);
    }

/* initialise the IFM */
    if ((i4RetVal = CfaIfmInit ()) != CFA_SUCCESS)
    {

        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaInit - IFM Init Fail - FAILURE.\n");

        return (i4RetVal);
    }

    /* Initialize the Protocol Registration Table */
    CfaMuxInitProtRegnTable ();
    /* Register the ARP and RARP modules with Protocol Registration Table */
    CfaMuxRegisterArpModule ();

    /* initialise cfa registration table */
    if ((i4RetVal = CfaInitHLRegnTbl ()) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaInit - Init RegTbl Fail - FAILURE.\n");
        return (i4RetVal);
    }

    if ((i4RetVal = CfaInitTnlTable ()) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaInit - CFA Tnl table init Fail -"
                 "FAILURE.\n");
        return (i4RetVal);

    }

#ifdef L2RED_WANTED
    if ((i4RetVal = CfaRedInitBufferPool ()) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaRedInitBufferPool - Error in MemPool "
                 "Alloc for Buffer Mempool.\n");
        return (i4RetVal);
    }
#endif

    /* Initializing Ufd module globals */
    CFA_UFD_SYSTEM_CONTROL () = CFA_UFD_SHUTDOWN;
    CFA_UFD_MODULE_STATUS () = CFA_UFD_DISABLED;
    /* To get if OOB is enabled in both nodes, 
     * based on a compilation flag */
    CFA_FETCH_DUAL_OOB_STATUS ();
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaTaskInitFailureShutdown
 *
 *    Description        : This function performs the shutdown of
 *                the CFA. It is to be called at end of
 *                the root task after other modules
 *                (i.e. IP, PPP, etc.) are shutdown. It is
 *                assumed that before IP and the other modules
 *                shutdown, they indicate this to CFA so that
 *                all the concerned interfaces can be brought
 *                down.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gu1CfaTaskStatus,
 *                CfaTaskId (CFA_TASK_ID),
 *
 *    Global Variables Modified : gu1CfaTaskStatus,
 *                CfaTaskId (CFA_TASK_ID),
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaTaskInitFailureShutdown (VOID)
{
    if (CFA_PACKET_QID != 0)
    {
        OsixQueDel (CFA_PACKET_QID);
        CFA_PACKET_QID = 0;
    }
    if (CFA_PACKET_PLIST_QID != 0)
    {
        OsixQueDel (CFA_PACKET_PLIST_QID);
        CFA_PACKET_PLIST_QID = 0;
    }
    if (CFA_EXT_TRIGGER_QID != 0)
    {
        OsixQueDel (CFA_EXT_TRIGGER_QID);
        CFA_EXT_TRIGGER_QID = 0;
    }
#ifdef CFA_INTERRUPT_MODE
    if (CFA_PACKET_MUX_QID != 0)
    {
        OsixQueDel (CFA_PACKET_MUX_QID);
        CFA_PACKET_MUX_QID = 0;
    }
#endif
    if (CFA_PACKET_PARAMS_QID != 0)
    {
        OsixQueDel (CFA_PACKET_PARAMS_QID);
        CFA_PACKET_PARAMS_QID = 0;
    }

    if (CFA_ATTACK_PACKET_QID != 0)
    {
        OsixQueDel (CFA_ATTACK_PACKET_QID);
        CFA_ATTACK_PACKET_QID = 0;
    }

    CfaPktGenShutdown ();

    if (gCfaSemId != 0)
    {
        OsixSemDel (gCfaSemId);
        gCfaSemId = 0;
    }
    CfaGddShutdown ();

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_MAIN,
             "In CfaTaskInitFailureShutdown - CFA Task init failure "
             " Shutdown completed.\n");
}

/*****************************************************************************
 *
 *    Function Name        : CfaHandleModuleStart
 *
 *    Description        : This function performs the CFA start, it allocates
 *                         the memory pools, creates the timer list and registration
 *                         with higher layer modules are done in this routine. 
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaHandleModuleStart (VOID)
{
#ifdef L2RED_WANTED
#ifdef RM_WANTED
    tRmRegParams        RmReg = { RM_CFA_APP_ID, CfaRmCallBk };
#endif
#endif
    if (CfaInit () != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }
#ifndef CFA_INTERRUPT_MODE
    /* create the timer lists for CFA */
    if (TmrCreateTimerList ((const UINT1 *) CFA_TASK_NAME,
                            CFA_GDD_POLL_TMR_EXP_EVENT,
                            NULL, &CFA_GDD_TMR_LIST) != TMR_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "Exiting CfaHandleModuleStart - create timer list for "
                 "GDD poll timer fail\n");
        return CFA_FAILURE;
    }
#else /* CFA_INTERRUPT_MODE end */

    if (CfaLinkUpDelayTmrInit () != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "Exiting CfaHandleModuleStart - create timer list for "
                 "LinkUp Delay Timer fail\n");

        return CFA_FAILURE;
    }
#endif
#ifdef L2RED_WANTED
    CFA_NODE_STATUS () = CFA_NODE_IDLE;
    CFA_IS_BULK_REQ_RECVD () = CFA_FALSE;
    CFA_NUM_STANDBY_NODES () = 0;
    gu4CfaBulkUptIfIndex = 1;
#ifdef RM_WANTED
    /* Register with RM */
    RmRegisterProtocols (&RmReg);
#endif /* RM_WANTED */
#endif /* L2RED_WANTED */
    CFA_INITIALISED = TRUE;
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaHandleShutdown
 *
 *    Description        : This function performs the dequeueing of CFA queues and 
 *                         deallocation/deletion timers and memory pools.
 *                         It shuts all except the GDD init related information.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaHandleShutdown (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pQueueDrainBuf;
    tCRU_BUF_CHAIN_HEADER *pAttackPkt = NULL;
    tCfaTxPktOnPortListMsg *pTxPktOnPortListMsg;
    tCfaExtTriggerMsg  *pExtTriggerMsg;
#ifndef CFA_INTERRUPT_MODE
    UINT4               u4GddTmrRemTime = 0;
#endif

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Entering CfaRmShutdownModule.\n");

    CFA_INITIALISED = FALSE;
    /* if CFA task is running, delete it first */
#ifndef CFA_INTERRUPT_MODE
    /* delete the timer lists for CFA */
    if (CFA_GDD_TMR_LIST != 0)
    {
        TmrGetRemainingTime (CFA_GDD_TMR_LIST, &GddPollTimer, &u4GddTmrRemTime);
        if (u4GddTmrRemTime != 0)
        {
            TmrStopTimer (CFA_GDD_TMR_LIST, &GddPollTimer);
        }
        if (TmrDeleteTimerList (CFA_GDD_TMR_LIST) != TMR_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                     "Error in CfaRmShutdownModule - GDD Timer release fail.\n");
        }
        CFA_GDD_TMR_LIST = 0;
    }
#else /* CFA_INTERRUPT_MODE */
    if (CFA_PACKET_MUX_QID != 0)
    {
        /* delete the Queue after dequeuing all packets from the queue */
        while (OsixQueRecv (CFA_PACKET_MUX_QID, (UINT1 *) &pQueueDrainBuf,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pQueueDrainBuf, FALSE);
        }
    }
    CfaLinkUpDelayTmrDeInit ();

#endif
    if (gu1CfaDataPktQueueStatus == CFA_ENABLED)
    {
        /* delete the Queue after dequeuing all packets from the queue */
        while (OsixQueRecv (CFA_PACKET_QID, (UINT1 *) &pQueueDrainBuf,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pQueueDrainBuf, FALSE);
        }
    }

    if (gu1CfaPortListPktQueueStatus == CFA_ENABLED)
    {
        while (OsixQueRecv (CFA_PACKET_PLIST_QID,
                            (UINT1 *) &pTxPktOnPortListMsg,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pTxPktOnPortListMsg->pFrame, FALSE);
            MemReleaseMemBlock (TxPktOnPortListMsgPoolId,
                                (UINT1 *) pTxPktOnPortListMsg);
        }
    }

    if (gu1CfaExtTriggerQueueStatus == CFA_ENABLED)
    {
        while (OsixQueRecv (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            MemReleaseMemBlock (ExtTriggerMsgPoolId, (UINT1 *) pExtTriggerMsg);
        }
    }

    if (gu1CfaAttackPktQueueStatus == CFA_ENABLED)
    {
        while (OsixQueRecv (CFA_ATTACK_PACKET_QID,
                            (UINT1 *) &pAttackPkt,
                            OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pAttackPkt, FALSE);
        }
    }

    CfaDeInitHLRegnTbl ();
    CfaDeInitTnlTable ();
    CfaIwfEnetShutdown ();
    CfaIfmShutdown ();
    CfaCdbDeInit ();
#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    if (CFA_AUDIT_TASK_ID () != 0)
    {
        CFA_RED_AUDIT_FLAG () = CFA_RED_AUDIT_STOP;

        CFA_SEND_EVENT (CFA_AUDIT_TASK_ID (), CFA_RED_AUDIT_TSK_DEL_EVENT);
    }
#endif
    CfaRedDeInitBufferPool ();
    CFA_NODE_STATUS () = CFA_NODE_IDLE;
    CFA_IS_BULK_REQ_RECVD () = CFA_FALSE;
    gu4CfaBulkUptIfIndex = 1;
    CFA_NUM_STANDBY_NODES () = 0;
#endif
    CFA_RED_IPINIT_COMPLETE_FLAG () = CFA_FALSE;
#ifdef RM_WANTED
    /* Deregister with RM */
    if (RmDeRegisterProtocols (RM_CFA_APP_ID) == RM_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "In CfaRmShutdownModule - RM deregistration failed.\n");
        return CFA_FAILURE;
    }
#endif /* RM_WANTED */

    SYS_LOG_DEREGISTER (gu4CfaSysLogId);
    gu4CfaSysLogId = 0;

    CFA_DBG (CFA_TRC_ALL_TRACK, CFA_MAIN,
             "In CfaRmShutdownModule - CFA Shutdown completed.\n");
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaMain
 *
 *    Description        : This function is the main function of
 *                the CFA. It handles all the events and
 *                posted to CFA and invokes routines
 *                for processing these events.
 *
 *    Input(s)            : Dummy parameter pParam 
 *
 *    Output(s)            : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaMain (INT1 *pParam)
{
    UINT4               u4EventMask = 0;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    tCRU_BUF_CHAIN_HEADER *pAttackPkt = NULL;
    tCfaTxPktOnPortListMsg *pTxPktOnPortListMsg;
    UINT1               u1Cmd;
    UINT4               u4PktSize;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex;
    UINT4               u4UfdGroupId = 0;
    UINT1               au1DestHwAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1OldOperStatus = CFA_IF_DOWN;
    UINT1               u1IfType;
    UINT1               u1IsFromMib;
    UINT1               u1EncapType;
    UINT2               u2Protocol;

    UINT2               VlanId = 0;
#ifdef IKE_WANTED
#ifdef IPSECv4_WANTED
    UINT4               u4BufSize = 0;
    tIkeIPSecQMsg       IkeMsg;
#endif
#endif
#if defined (WLC_WANTED) || defined (WTP_WANTED)
    UINT2               u2VlanId = 0;
#endif
    UINT1               u1OperStatus;
    tCfaLinkStatusMsg  *pLinkStatusMsg;
    tCfaExtTriggerMsg  *pExtTriggerMsg;
    tCfaSystemSize      CfaSizingStruct;
    UINT1               u1SubType = 0;
    UINT1               u1VipOperStatus;
    UINT1               u1SchOperStatus = 0;
    UINT1               u1IfEtherType = 0;
    UINT4               u4MaxFrontPanelPorts;
    UINT1              *pu1DataBuf = NULL;
    UINT1              *pu1TxPktInfo = NULL;
    UINT1               u1Status = 0;
    BOOL1               b1IsPacketAlreadyTx = OSIX_FALSE;
    INT4                i4RetValue = 0;
#ifdef VCPEMGR_WANTED
    tTapIfMsg          *pTapIfMsg = NULL;
#endif
    /* Dummy pointers for system sizing during run time */
    tCfaMbsmSlotMsgBlock *pCfaMbsmSlotMsgBlock = NULL;
    tCfaPktPoolBlock   *pCfaPktPoolBlock = NULL;
    tCfaDriverMtuBlock *pCfaDriverMtuBlock = NULL;
    tCfaBufferEntry    *pCfaBufferEntry = NULL;
    tEnetIwfStruct     *pEnetIwfStruct = NULL;

    UNUSED_PARAM (pCfaMbsmSlotMsgBlock);
    UNUSED_PARAM (pCfaPktPoolBlock);
    UNUSED_PARAM (pCfaDriverMtuBlock);
    UNUSED_PARAM (pCfaBufferEntry);
    UNUSED_PARAM (pEnetIwfStruct);

    UNUSED_PARAM (pParam);

    CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "Entering CfaMain.\n");

    i4RetValue = SYS_LOG_REGISTER ((CONST UINT1 *) "CFA", SYSLOG_ALERT_LEVEL);
    if (i4RetValue <= 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - sys log registration failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    else
    {
        gu4CfaSysLogId = (UINT4) i4RetValue;
    }
    i4RetValue = SYS_LOG_REGISTER ((CONST UINT1 *) "FM", SYSLOG_ALERT_LEVEL);
    if (i4RetValue <= 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - sys log registration failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    i4RetValue = SYS_LOG_REGISTER ((CONST UINT1 *) "FFM", SYSLOG_ALERT_LEVEL);
    if (i4RetValue <= 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - sys log registration failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    i4RetValue = SYS_LOG_REGISTER ((CONST UINT1 *) "IFM", SYSLOG_ALERT_LEVEL);
    if (i4RetValue <= 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - sys log registration failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    i4RetValue = SYS_LOG_REGISTER ((CONST UINT1 *) "IWF", SYSLOG_ALERT_LEVEL);
    if (i4RetValue <= 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - sys log registration failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    i4RetValue = SYS_LOG_REGISTER ((CONST UINT1 *) "GDD", SYSLOG_ALERT_LEVEL);
    if (i4RetValue <= 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - sys log registration failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    i4RetValue = SYS_LOG_REGISTER ((CONST UINT1 *) "CFAIF", SYSLOG_ALERT_LEVEL);
    if (i4RetValue <= 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - sys log registration failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    i4RetValue = SYS_LOG_REGISTER ((CONST UINT1 *) "TNL", SYSLOG_ALERT_LEVEL);
    if (i4RetValue <= 0)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - sys log registration failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixCreateSem (CFA_PROTOCOL_LOCK, 1, 0, &gCfaSemId) != OSIX_SUCCESS)
    {
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    FsCFASizingParams[MAX_CFA_PKTGEN_TX_INFO_SIZING_ID].u4StructSize
        = (sizeof (tCfaPktTxInfo) *
           FsCFASizingParams[MAX_CFA_PKTGEN_TX_INFO_SIZING_ID].
           u4PreAllocatedUnits);
    FsCFASizingParams[MAX_CFA_PKTGEN_TX_INFO_SIZING_ID].u4PreAllocatedUnits = 1;

    FsCFASizingParams[MAX_CFA_PKTGEN_RX_INFO_SIZING_ID].u4StructSize
        = (sizeof (tCfaPktRxInfo) *
           FsCFASizingParams[MAX_CFA_PKTGEN_RX_INFO_SIZING_ID].
           u4PreAllocatedUnits);
    FsCFASizingParams[MAX_CFA_PKTGEN_RX_INFO_SIZING_ID].u4PreAllocatedUnits = 1;

    /* Increment  MAX_CFA_MAU_INTERFACES by 1 if MGMT Port Enabled */
    if (CfaIsMgmtPortEnabled () == TRUE)
    {
        FsCFASizingParams[MAX_CFA_MAU_INTF_SIZING_ID].u4PreAllocatedUnits
            = MAX_CFA_MAU_INTERFACES + 1;
    }
    else
    {
        FsCFASizingParams[MAX_CFA_MAU_INTF_SIZING_ID].u4PreAllocatedUnits
            = MAX_CFA_MAU_INTERFACES;
    }

    if (CfaSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain -  MemPool Creation Failed \n");
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    TxPktOnPortListMsgPoolId =
        CFAMemPoolIds[MAX_CFA_PKT_PORTLIST_Q_DEPTH_SIZING_ID];
    ExtTriggerMsgPoolId =
        CFAMemPoolIds[MAX_CFA_EXT_TRIGGER_QUEUE_DEPTH_SIZING_ID];
    gCfaMbsmSlotMsgPoolId =
        CFAMemPoolIds[MAX_CFA_MBSM_SLOT_MSG_BLOCKS_SIZING_ID];
    gCfaMbsmProtoMsgPoolId =
        CFAMemPoolIds[MAX_CFA_MBSM_PROTO_MSG_BLOCKS_SIZING_ID];
    gCfaPktPoolId = CFAMemPoolIds[MAX_CFA_PKT_POOL_BLOCKS_SIZING_ID];
    gCfaDriverMtuPoolId = CFAMemPoolIds[MAX_CFA_DRIVER_MTU_BLOCKS_SIZING_ID];
    gCfaStackPoolId = CFAMemPoolIds[MAX_CFA_STACK_BLOCKS_SIZING_ID];
    gCfaILanPortPoolId = CFAMemPoolIds[MAX_CFA_SISP_INTERFACES_SIZING_ID];
    gCfaRcvAddrPoolId = CFAMemPoolIds[MAX_CFA_RCV_ADDR_BLOCKS_SIZING_ID];
    gCfaIntfStatusPoolId =
        CFAMemPoolIds[MAX_CFA_IFSTATUS_PROP_BLOCKS_SIZING_ID];
    gCfaPortArrayPoolId = CFAMemPoolIds[MAX_CFA_PORT_ARRAY_SIZING_ID];

    /* To receive outgoing packets from Higher layers */
    if (OsixQueCrt ((UINT1 *) CFA_PACKET_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    CFA_PACKET_QUEUE_DEPTH, &(CFA_PACKET_QID)) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - CFA Queue Creation Fail -"
                 "FAILURE.\n");
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    gu1CfaDataPktQueueStatus = CFA_ENABLED;

    if (OsixQueCrt ((UINT1 *) CFA_PACKET_PORTLIST_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    MAX_CFA_PKT_PORTLIST_Q_DEPTH,
                    &(CFA_PACKET_PLIST_QID)) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - CFA Queue Creation Fail -"
                 "FAILURE.\n");
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) CFA_PKT_PARAMS_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    CFA_PACKET_QUEUE_DEPTH,
                    &(CFA_PACKET_PARAMS_QID)) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - CFA Queue Creation Fail -"
                 "FAILURE.\n");
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    gu1CfaPortListPktQueueStatus = CFA_ENABLED;

    if (OsixQueCrt ((UINT1 *) CFA_EXT_TRIGGER_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    MAX_CFA_EXT_TRIGGER_QUEUE_DEPTH,
                    &(CFA_EXT_TRIGGER_QID)) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - CFA Queue Creation Fail -"
                 "FAILURE.\n");
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    gu1CfaExtTriggerQueueStatus = CFA_ENABLED;

#ifdef CFA_INTERRUPT_MODE
    if (OsixQueCrt ((UINT1 *) CFA_PACKET_QUEUE_MUX, OSIX_MAX_Q_MSG_LEN,
                    CFA_PACKET_QUEUE_DEPTH, &(CFA_PACKET_MUX_QID))
        != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - CFA Queue Creation Fail -"
                 "FAILURE.\n");
        /* Indicate the status of initialization to the main routine */
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif

    if (OsixQueCrt ((UINT1 *) CFA_ATTACK_PKT_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    CFA_ATTACK_PKT_Q_DEPTH,
                    &(CFA_ATTACK_PACKET_QID)) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - CFA Queue Creation for processing "
                 "Attack packet failed\n");
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    gu1CfaAttackPktQueueStatus = CFA_ENABLED;

    GetCfaSizingParams (&CfaSizingStruct);
    CFA_MAX_INTERFACES () = (UINT4) (CfaSizingStruct.u4SystemMaxIface +
                                     (UINT4) CFA_MAX_NUM_OF_VIP +
                                     (UINT4) CFA_MAX_NUM_OF_OF +
                                     (UINT4) CFA_MAX_SISP_INTERFACES);
    CFA_MAX_TUNL_INTERFACES () = (UINT2) CfaSizingStruct.u4SystemMaxTunlIface;
    CFA_PHYS_NUM () = (UINT2) CfaSizingStruct.u4SystemMaxPhysicalIface;

#ifdef ISS_WANTED
    u4MaxFrontPanelPorts = IssGetFrontPanelPortCountFromNvRam ();
#endif

#ifdef NPAPI_WANTED
#ifdef MBSM_WANTED
    /*In case of MBSM, Dynamic Fron Panel Ports in invalid */
    NpCfaFrontPanelPorts (SYS_DEF_MAX_PHYSICAL_INTERFACES);
    /* Update the Stacking Model to NP Layer. Stacking Model 
       is applicable only in MBSM case */
    CfaNpSetStackingModel (gu4StackingModel);
#else
    CfaNpCfaFrontPanelPorts (u4MaxFrontPanelPorts);
#endif
#else
    UNUSED_PARAM (u4MaxFrontPanelPorts);
#endif

    /* initialise the GDD */
    if (CfaGddInit () != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_MAIN,
                 "Fatal Error in CfaMain - GDD Init Fail - FAILURE.\n");
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (CfaHandleModuleStart () != CFA_SUCCESS)
    {
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (CfaPktGenInit () != CFA_SUCCESS)
    {
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    /* Register the MIBs with SNMP */
    RegisterFSCFA ();
    RegisterIFMIB ();
    RegisterSTDETH ();
    RegisterSTDMAU ();
    RegisterFSTUNL ();
    RegisterFSMPTU ();
#endif

    if (OsixTskIdSelf (&CFA_TASK_ID) != OSIX_SUCCESS)
    {
        CfaHandleShutdown ();
        CfaTaskInitFailureShutdown ();
        CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                 "Exiting CfaMain - OsixTskIdSelf Failed\n");
        /* Indicate the status of initialization to the main routine */
        CFA_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    CFA_INIT_COMPLETE (OSIX_SUCCESS);

    /* Wait for all the tasks to finish their initializations
     * before starting packet RX.
     */
    OsixEvtRecv (CFA_TASK_ID, CFA_PACKET_PROCESS_START_EVENT,
                 CFA_EVENT_WAIT_FLAGS, &u4EventMask);

    if (CFA_PKT_TASK_ID == 0)
    {
        if (OsixGetTaskId (SELF, (const UINT1 *) CFA_PKT_TASK_NAME,
                           &CFA_PKT_TASK_ID) != OSIX_SUCCESS)
        {
            CFA_DBG (ALL_FAILURE_TRC, CFA_MAIN,
                     "Event send to CFA PKT TASK failed\n");
            return;
        }

    }
    OsixEvtSend (CFA_PKT_TASK_ID, CFA_PACKET_START_EVENT);

    /* start the main loop only after all the tasks are initialized */
    while (CFA_ALWAYS)
    {

        OsixEvtRecv (CFA_TASK_ID,
                     (CFA_GDD_POLL_TMR_EXP_EVENT |
                      CFA_LINKUP_DELAY_TMR_EXP_EVENT | CFA_OOB_DATA_RCVD_EVENT |
#ifdef CFA_INTERRUPT_MODE
                      CFA_GDD_INTERRUPT_EVENT |
#endif
                      CFA_SUB_BULK_UPDT_EVENT |
                      CFA_EXT_TRIGGER_EVENT |
                      CFA_PACKET_PORTLIST_ARRIVAL_EVENT |
                      CFA_PACKET_ARRIVAL_EVENT | CFA_IP_UP_EVENT
#ifdef NAT_WANTED
                      | NAT_TIMER_EXPIRY_EVENT
/* NAPT TIMER */
                      | NAPT_TIMER_EXPIRY_EVENT
/* END */
/* SIPALG TIMER */
                      | NAT_SIPALG_TIMER_EXPIRY_EVENT
                      | NAT_FRAG_TIMER_EXPIRY_EVENT
/* END */
#endif /* NAT_WANTED */
#ifdef FIREWALL_WANTED
                      | FWL_TIMER_EXPIRY_EVENT
#endif
#ifdef  IPSECv4_WANTED
                      | IPSEC_TIMER_EXPIRY_EVENT
#endif
#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
                      | CFA_TUNL_PKT_RECV_EVENT | CFA_GRE_TUNL_PKT_RECV_EVENT
#endif
                      | CFA_ATTACK_PACKET_RX_EVENT
                      | CFA_PKT_GEN_TMR_EVENT | CFA_PKT_GEN_TX_EVENT
#ifdef VCPEMGR_WANTED
                      | TAP_IF_PKT_EVENT
#endif
                     ), CFA_EVENT_WAIT_FLAGS, &u4EventMask);

#ifdef CFA_INTERRUPT_MODE
        if (u4EventMask & CFA_GDD_INTERRUPT_EVENT)
        {
            /*
             * NOTE: CFA Lock MUST be taken in CfaGddProcessRecvInterruptEvent() 
             * in the Message DeQueue while loop. 
             */
            CfaGddProcessRecvInterruptEvent ();
        }
#else /* CFA_INTERRUPT_MODE */
/* Poll the Device Drivers */
        if (u4EventMask & CFA_GDD_POLL_TMR_EXP_EVENT)
        {
            /*
             * CFA Lock must be taken in the Message DqQueue while loop
             * in CfaGddProcessRecvEvent ().
             */
            if (CFA_INITIALISED == TRUE)
            {
                CfaGddProcessRecvEvent ();
                /* restart the timer */
                if (TmrStartTimer
                    (CFA_GDD_TMR_LIST, &GddPollTimer,
                     CFA_GDD_POLL_TIMEOUT) != TMR_SUCCESS)
                {
                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                             "In CfaMain GDD poll timer restart fail.\n");
                }
            }
        }
#endif /* ! CFA_INTERRUPT_MODE */
        /*process the signal indicating the timer expiry for LinkUp Delay */
        if (u4EventMask & CFA_LINKUP_DELAY_TMR_EXP_EVENT)
        {
            CfaLinkUpDelayProcessTimerExpiryEvent ();
        }
        /* Process the signal indicating the completion of IP init */
        if (u4EventMask & CFA_IP_UP_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaMain recd IP UP event.\n");
            CFA_LOCK ();

            if ((CFA_NODE_STATUS () != CFA_NODE_IDLE &&
                 CFA_RED_IPINIT_COMPLETE_FLAG () == CFA_TRUE) ||
                (CFA_RED_IPINIT_COMPLETE_FLAG () ==
                 (UINT1) CFA_RED_STADNBY_RCVD))
            {
                if ((IssGetRestoreFlagFromNvRam () != ISS_CONFIG_RESTORE) ||
                    (IssGetRestoreOptionFromNvRam () ==
                     ISS_CONFIG_REMOTE_RESTORE) || (CFA_MGMT_PORT == TRUE))

                {
                    CfaProcessIpInitComplete ();
                }
                else
                {
                    /* Start the GDD Timer */
                    CfaStartGddTimer ();
                }

            }

            if (CFA_RED_IPINIT_COMPLETE_FLAG () == CFA_FALSE)
            {
                CFA_RED_IPINIT_COMPLETE_FLAG () = CFA_TRUE;
            }
#ifdef NPAPI_WANTED
#ifndef MBSM_WANTED
            /* Call To Register with NP, by defaults  only for Pizza box, 
             * for Multi-board systems this Register routine will be invoked during
             * the card insert.
             */
            /* HITLESS RESTART */
            if (IssGetHRFlagFromNvRam () == ISS_HITLESS_RESTART_DISABLE)
            {
                CfaCfaRegisterWithNpDrv ();
            }

            /* Update the link status of all the interfaces in those units. */
            CfaUtilUpdateLinkStatus (ISS_MIN_PORTS, ISS_MAX_PORTS);

#endif /*MBSM_WANTED */
#endif /* NPAPI_WANTED */

#ifdef MSR_WANTED
            if (LrSendEventToLrTask (ISS_SPAWN_MSR_TASK) != OSIX_SUCCESS)
            {
            }
#endif
            CFA_UNLOCK ();
        }

        /* some module has sent a packet to CFA - currently the module could be
           IP or PPP only */
        if (u4EventMask & CFA_PACKET_ARRIVAL_EVENT)
        {

            /* we process all the packets from other modules */
            while (OsixQueRecv (CFA_PACKET_QID, (UINT1 *) &pBuf,
                                OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
            {
                CFA_LOCK ();

                /* This flag is set to true when EOAM packet receieved.
                 * This need to be re initiliaized every time. So 
                 * Re initialization is done here */

                b1IsPacketAlreadyTx = OSIX_FALSE;

                u1Cmd = CFA_GET_COMMAND (pBuf);

                switch (u1Cmd)
                {

                        /* handle outgoing packet from IP */
                    case IP_TO_CFA:

                        u4IfIndex = CFA_GET_IFINDEX (pBuf);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain recd IP pkt for interface %d"
                                  "sending to FFM.\n", u4IfIndex);

                        /* send the packet to the IP handler/FFM - all conditions handled there */
                        CfaHandleIpPktFromIp (pBuf, u4IfIndex);
                        break;

                    case ARP_TO_CFA:
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u2Protocol = CFA_GET_PROTOCOL (pBuf);

                        CFA_GET_DEST_MEDIA_ADDR (pBuf, au1DestHwAddr);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain recd ARP pkt for interface %d"
                                  "sending to IWF.\n", u4IfIndex);

                        /* send the packet to the Enet IWF module */
                        if (gu4IsIvrEnabled == CFA_DISABLED)
                        {
                            if (CfaIwfEnetProcessTxFrame
                                (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                                 u2Protocol, CFA_ENCAP_ENETV2) != CFA_SUCCESS)
                            {
                                /* release of the pkt in IWF/GDD */
                                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                         "In CfaMain ARP pkt dispatch to IWF fail.\n");
                                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            }
                        }
                        else
                        {
                            CfaProcessPktFromArpForIvr
                                (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                                 u2Protocol, CFA_ENCAP_ENETV2);
                        }
                        break;

#ifdef IP6_WANTED
                    case IP6_TO_CFA:
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u2Protocol = CFA_GET_PROTOCOL (pBuf);

                        CFA_GET_DEST_MEDIA_ADDR (pBuf, au1DestHwAddr);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain recd IPv6 pkt for interface %d sending to IWF.\n",
                                  u4IfIndex);
                        /* send the packet to the Enet IWF module */
                        CfaHandlePktFromIpv6
                            (pBuf, u4IfIndex, au1DestHwAddr,
                             u4PktSize, u2Protocol, CFA_ENCAP_ENETV2);
                        break;

#endif /* IP6_WANTED  */

#ifdef PPP_WANTED

#ifndef PPP_BYPASS_WANTED
/* handle incoming packets from PPP - to be sent to HL .i.e IP */
                    case PPP_TO_CFA_IN:
                        u4IfIndex = (UINT2) CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u2Protocol = CFA_GET_PROTOCOL (pBuf);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain sending incoming PPP Pkt "
                                  "on interface %d to IWF.\n", u4IfIndex);

                        CfaIwfPppHandleIncomingPktFromPpp (pBuf, u4IfIndex,
                                                           u4PktSize,
                                                           u2Protocol);
                        break;
#endif /* PPP_BYPASS_WANTED */

/* handle incoming packets from PPP - to be sent to LL .i.e HDLC. */
                    case PPP_TO_CFA_OUT:
                        u4IfIndex = (UINT2) CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u2Protocol = CFA_GET_PROTOCOL (pBuf);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain sending outgoing PPP Pkt"
                                  "on interface %d to IWF.\n", u4IfIndex);

                        CfaIwfPppHandleOutGoingPktFromPpp (pBuf, u4IfIndex,
                                                           u4PktSize,
                                                           u2Protocol);
                        break;

                    case PPPOE_PKT_OUT:
                        u4IfIndex = (UINT2) CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u2Protocol = CFA_GET_PROTOCOL (pBuf);
                        CFA_GET_DEST_MEDIA_ADDR (pBuf, au1DestHwAddr);

                        CfaIwfPPPoEHandleOutGoingPkt (pBuf, u4IfIndex,
                                                      u4PktSize, u2Protocol,
                                                      au1DestHwAddr);

                        break;

#endif /* PPP_WANTED */

#ifdef MPLS_WANTED
                    case MPLS_TO_CFA:
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain sending outgoing Mpls Pkt"
                                  "on interface %d to IWF.\n", u4IfIndex);

                        if (gu1MplsInitialised == FALSE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            break;
                        }

                        CfaIwfMplsHandleOutGoingPkt (pBuf, u4IfIndex,
                                                     u4PktSize);
                        break;

                    case L2PKT_MPLS_TO_CFA:
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain sending outgoing L2 Pkt"
                                  "on interface %d to IWF.\n", u4IfIndex);

                        /* For pseudowire visibility for L2 modules -
                         * CFA receives the packet from MPLS after removing
                         * pseudowire label. This Packet is destined L2
                         * modules */
                        if (CfaMuxIwfEnetProcessRxFrame (pBuf, u4IfIndex,
                                                         u4PktSize, 0, 0)
                            != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                      "In CfaMain sending outgoing L2 Pkt"
                                      "on interface %d to IWF failed \n",
                                      u4IfIndex);
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        }

                        break;
#ifdef MPLS_L3VPN_WANTED
                    case MPLSL3VPN_TO_CFA:
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain sending outgoing Mpls Pkt"
                                  "on interface %d to IWF.\n", u4IfIndex);

                        CfaIwfMplsHandleOutGoingPkt (pBuf, u4IfIndex,
                                                     u4PktSize);
                        break;
#endif
#endif /* MPLS_WANTED */

#ifdef ISIS_WANTED
                    case ISIS_TO_CFA:
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u4IfIndex = (UINT2) CFA_GET_IFINDEX (pBuf);
                        u2Protocol = CFA_GET_PROTOCOL (pBuf);
                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain sending outgoing ISIS Pkt"
                                  "on interface %d to IWF.\n", u4IfIndex);

                        if (CfaIwfHandleOutgoingIsisPkt (pBuf, u4IfIndex,
                                                         u4PktSize, u2Protocol)
                            != CFA_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "IsIs dispatch to IWF fail.\n");
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        }
                        break;
#endif /* ISIS_WANTED */

#ifdef IKE_WANTED
#ifdef IPSECv4_WANTED
                    case IKE_IPSEC_MESSAGE:

                        u4BufSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

                        /* Copy the message to a local variable */
                        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IkeMsg, 0,
                                                   sizeof (tIkeIPSecQMsg));

                        /* Release the rcvd buffer */
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

                        Secv4ProcessIKEMsg (&IkeMsg);
                        break;
                        UNUSED_PARAM (u4BufSize);
#endif
#endif

                    case SEC_TO_CFA:
                        u4IfIndex = SEC_GET_IFINDEX (pBuf);
                        u2Protocol = SEC_GET_PROTOCOL (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

                        if (CFA_FAILURE ==
                            CfaIwfEnetProcessRxFrame (pBuf, u4IfIndex,
                                                      u4PktSize, u2Protocol,
                                                      CFA_ENCAP_ENETV2))
                        {
                            /* release of the pkt in IWF/GDD */
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "In Security dispatch to " "IWF fail.\n");
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

                        }
                        break;

                    case L2_TO_CFA:

                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);

                        u2Protocol = CFA_GET_PROTOCOL (pBuf);

                        u1EncapType = CFA_GET_ENCAP_TYPE (pBuf);

                        CRU_BUF_Copy_FromBufChain (pBuf, au1DestHwAddr,
                                                   0, CFA_ENET_ADDR_LEN);

                        if (u2Protocol == CFA_ENET_SLOW_PROTOCOL)
                        {
                            /* Get the first byte after ethernet header. */
                            CRU_BUF_Copy_FromBufChain (pBuf, &u1SubType,
                                                       CFA_ENET_V2_HEADER_SIZE,
                                                       1);
                            if (u1SubType == EOAM_PROTOCOL_SUBTYPE)
                            {
                                b1IsPacketAlreadyTx = OSIX_TRUE;
                                if (CfaHandlePktFromEoam (pBuf, u4IfIndex,
                                                          u4PktSize, u2Protocol,
                                                          au1DestHwAddr,
                                                          u1EncapType) !=
                                    CFA_SUCCESS)
                                {
                                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                             "In CfaMain EOAM Control packet dispatch "
                                             "to IWF failed.\n");
                                }
                            }
                        }

                        if (b1IsPacketAlreadyTx != OSIX_TRUE)
                        {
                            if (CfaHandlePktFromL2 (pBuf, u4IfIndex, u4PktSize,
                                                    u2Protocol, au1DestHwAddr,
                                                    u1EncapType) != CFA_SUCCESS)
                            {
                                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                         "In CfaMain LA Control packet dispatch "
                                         "to IWF failed.\n");
                            }
                        }
                        break;

                    case VLAN_TO_CFA:

                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u4ContextId = CFA_GET_IFINDEX (pBuf);
                        VlanId = CFA_GET_PROTOCOL (pBuf);

                        /* The packet needs to be transmitted over the VLAN 
                         * member ports. The Packet should not be tailored and
                         * forwarded to all the member ports as such
                         * */
                        pu1DataBuf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                                   u4PktSize);

                        if (pu1DataBuf != NULL)
                        {
                            CfaGddTxPktOnVlanMemberPortsInCxt (u4ContextId,
                                                               pu1DataBuf,
                                                               VlanId,
                                                               OSIX_FALSE,
                                                               u4PktSize);
                        }
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        break;

#ifdef OPENFLOW_WANTED
                    case OFC_TO_CFA:
                        /*
                         * all the details are expected to be filled in the
                         * Openflow module itself.
                         */
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u2Protocol = CFA_GET_PROTOCOL (pBuf);
                        u1EncapType = CFA_GET_ENCAP_TYPE (pBuf);

                        if (CFA_FAILURE ==
                            CfaIwfEnetProcessRxFrame (pBuf, u4IfIndex,
                                                      u4PktSize, u2Protocol,
                                                      u1EncapType))
                        {
                            /* release of the pkt in IWF/GDD */
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "Ofcl dispatch to ISS failed\n");
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        }
                        break;
#endif /* OPENFLOW_WANTED */

#ifdef MBSM_WANTED
                    case MBSM_MSG_BOOTUP_DISC:
                        /* Assumption is that all the available slots are
                         * present in a single message at Bootup so that we
                         * can proceed with MSR */
                        MsrisRetoreinProgress = ISS_TRUE;
                        CfaHandleIfUpdatePktFromMbsm (pBuf);
#ifdef RM_WANTED
                        if (RmSendEventToRmTask (CFA_PORT_CREATION_COMPLETED)
                            != RM_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "Failed to Sent CFA_PORT_CREATION_COMPLETED "
                                     "event.\n");
                        }
#endif
                        /* All the interfaces have been created, so trigger
                         * MSR */
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        /*wait till IP_UP indication received by LR 
                         * also wait for the successful creation
                         * of MSR Task.Without MSR Task creation
                         * the function CfaNotifyBootComplete, should
                         * not be called.
                         * */
                        OsixEvtRecv (CFA_TASK_ID, CFA_BOOTUP_START_EVENT,
                                     CFA_EVENT_WAIT_FLAGS, &u4EventMask);
                        CfaNotifyBootComplete ();
                        MsrisRetoreinProgress = ISS_FALSE;
                        break;

                        /* Interface Control handle routine:
                         * Called for every interface status 
                         * change notifications like creation/deletion
                         */

                    case MBSM_MSG_INTF_CTRL:
                        CfaHandleIfUpdatePktFromMbsm (pBuf);
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        break;

                        /* These card insert and remove events are only
                         * for ISS system features related H/W updates.
                         * For other protocols, the indications given
                         * from MBSM itself.
                         */

                    case MBSM_MSG_CARD_INSERT:
                        /* Flag indicted it is a Insertion event */
                        CfaHandleCardUpdateFromMbsm ((UINT1 *) pBuf, TRUE);
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        break;

                    case MBSM_MSG_CARD_REMOVE:
                        /* Flag indicted it is a Removel event */
                        CfaHandleCardUpdateFromMbsm ((UINT1 *) pBuf, FALSE);
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        break;

                    case MBSM_MSG_PRECONFIG_DEL:
                        /* Flag indicted it is a Removel event */
                        CfaHandlePreConfigDelMsgFromMbsm ((UINT1 *) pBuf);
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        break;
                    case MBSM_MSG_LINK_STATUS_UPDATE:
                        /* Initiate link status detect 
                         * and update to protocols 
                         */
                        CfaHandleLinkStatusUpdateMsgFromMbsm ((UINT1 *) pBuf);
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        break;
#endif /* MBSM_WANTED */

#ifdef WTP_WANTED
                    case WSS_CFA_TX_DOT11_PKT:

                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

                        if (CfaIwfWssProcessTxFrame (pBuf, u4IfIndex,
                                                     u4PktSize) != CFA_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                     "In CfaMain wireless packet dispatch "
                                     "to IWF failed.\n");
                        }
                        break;
#endif

#if defined (WLC_WANTED) || defined (WTP_WANTED)
                    case WSS_TO_CFA:
                    {
                        /* This event is sent from WSS to CFA whenever a DOT3
                         * packet is received */
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u2VlanId = CFA_GET_PROTOCOL (pBuf);
                        /*Check if the packet is received on the WAP
                         * interface */
                        if ((u4IfIndex >= CFA_MIN_WSS_BSSID)
                            && (u4IfIndex <= CFA_MAX_WSS_BSSID))
                        {
                            if (CfaWssProcessRxFrame (u4IfIndex, pBuf, u2VlanId)
                                != CFA_SUCCESS)
                            {
                                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                         "\r\nCfaWssWlcProcessTxFrame :"
                                         "Processing the frame from WSS to CFA failed \n");
                            }
                        }
                    }
                        break;
#endif
#ifdef VXLAN_WANTED
                    case L2PKT_VXLAN_TO_CFA:    /* Value 50 is used */
                        u4IfIndex = CFA_GET_IFINDEX (pBuf);
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

                        CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                  "In CfaMain sending outgoing L2 Pkt"
                                  "on interface %d to IWF.\n", u4IfIndex);

                        /* CFA receives the packet from VXLAN module after removing
                         * VXLAN header. This Packet is destined L2
                         * modules */
                        if (CfaMuxIwfEnetProcessRxFrame (pBuf, u4IfIndex,
                                                         u4PktSize, 0, 0)
                            != CFA_SUCCESS)
                        {
                            CFA_DBG1 (CFA_TRC_ALL, CFA_MAIN,
                                      "In CfaMain sending outgoing L2 Pkt"
                                      "on interface %d to IWF failed \n",
                                      u4IfIndex);
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        }

                        break;
#endif
                    default:
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

                        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                 "In CfaMain recd invalid command - "
                                 "discarded packet.\n");

                        break;
                }                /* end of switch statement */

                CFA_UNLOCK ();
            }                    /* end of dequeuing from the queue */
        }                        /* end of processing packet arrival event */

        /* When LNXIP is enabled, LNXIP handles packet reception on OOB 
         * interface and hand over to the management applications. So this 
         * portion of code is not needed in case of LNXIP. */
#ifndef LNXIP4_WANTED
#ifdef IP_WANTED
        /* Packet is received on OOB Interface.Process it */
        if (u4EventMask & CFA_OOB_DATA_RCVD_EVENT)
        {
            if (CfaGddProcessOobDataRcvdEvent () != CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode failed to deliver to HL.\n");
            }
            else
            {
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt recvd in interrupt mode delivered to HL.\n");
            }
        }
#endif /* IP_WANTED */
#endif /* LNXIP4_WANTED */

        if (u4EventMask & CFA_PACKET_PORTLIST_ARRIVAL_EVENT)
        {
            /* Frames from IGS module that are to be transmitted on more
             * than one ports are posted to the below queue along with a
             * port list */

            while (OsixQueRecv (CFA_PACKET_PLIST_QID,
                                (UINT1 *) &pTxPktOnPortListMsg,
                                OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
            {
                CFA_LOCK ();

                CfaUtilTxL2PktOnPortList (pTxPktOnPortListMsg);

                MemReleaseMemBlock (TxPktOnPortListMsgPoolId,
                                    (UINT1 *) pTxPktOnPortListMsg);
                CFA_UNLOCK ();
            }
        }

        if (u4EventMask & CFA_EXT_TRIGGER_EVENT)
        {
            /* 
             * Indications from external modules are posted to the below 
             * queue. Currently this is being used for -
             * a) Link status change indication from hardware
             * b) Port channel deletion indication from LA 
             *    (when LA module is shutdown)
             c) Configure Dynamically acquired IP information for the 
             Interface
             d) Configure the switchport as Router port and vice-versa for
             Transparent Bridging.
             */
            while (OsixQueRecv (CFA_EXT_TRIGGER_QID, (UINT1 *) &pExtTriggerMsg,
                                OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
            {
                CFA_LOCK ();

                switch (pExtTriggerMsg->i4MsgType)
                {
                    case CFA_LINK_STATUS_CHANGE_MSG:
                        pLinkStatusMsg =
                            &(pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg);
                        u4IfIndex = pLinkStatusMsg->u4IfIndex;

                        /*
                         * No need to check the interface Validity, since
                         * it is done in CfaGetIfOperStatus ()
                         */
                        if (CfaGetIfOperStatus (u4IfIndex,
                                                &u1OldOperStatus)
                            == CFA_SUCCESS)
                        {
                            CfaGetIfType (u4IfIndex, &u1IfType);
                            CfaGetIfSubType (u4IfIndex, &u1SubType);

                            if ((pLinkStatusMsg->u1LinkStatus == CFA_IF_UP)
                                && (u1IfType == CFA_ENET)
                                && (CfaIsMgmtPort (u4IfIndex) == FALSE))
                            {
                                if (CFA_IS_NP_PROGRAMMING_ALLOWED () ==
                                    CFA_TRUE)
                                {
                                    CfaUpdateHwParams (u4IfIndex);
                                }
                            }

                            if (((CFA_IF_ADMIN (u4IfIndex) == CFA_IF_UP)
                                 || (CFA_IF_ADMIN (u4IfIndex) ==
                                     CFA_LOOPBACK_LOCAL))
                                && (pLinkStatusMsg->u1LinkStatus == CFA_IF_UP))
                            {
                                u1OperStatus = CFA_IF_UP;
                            }
                            else
                            {
                                u1OperStatus = CFA_IF_DOWN;
                            }
                            CfaCheckForLinkUpDelay ((INT4) u4IfIndex,
                                                    u1OperStatus);
                            if (u1OperStatus != u1OldOperStatus)
                            {
#if defined (WLC_WANTED) || defined (WTP_WANTED)
                                if ((u1IfType == CFA_ENET)
                                    || (u1IfType == CFA_RADIO)
                                    || (u1IfType == CFA_PSEUDO_WIRE)
                                    || (u1IfType == CFA_VXLAN_NVE)
                                    || (u1IfType == CFA_TAP)
                                    || ((u1IfType == CFA_PROP_VIRTUAL_INTERFACE)
                                        && (u1SubType ==
                                            CFA_SUBTYPE_AC_INTERFACE)))
#else
                                if ((u1IfType == CFA_ENET)
                                    || (u1IfType == CFA_PSEUDO_WIRE)
                                    || (u1IfType == CFA_VXLAN_NVE)
                                    || (u1IfType == CFA_TAP)
                                    || ((u1IfType == CFA_PROP_VIRTUAL_INTERFACE)
                                        && (u1SubType ==
                                            CFA_SUBTYPE_AC_INTERFACE)))
#endif
                                {
                                    /*
                                     * u1IsFromMib can take 3 values -
                                     *    CFA_TRUE  - Status change is from Mgmt 
                                     *    CFA_IF_LINK_STATUS_CHANGE - 
                                     *          Status change is from Ethernet
                                     *          Link Layer.
                                     *    CFA_FALSE - Otherwise      
                                     */
                                    u1IsFromMib = CFA_IF_LINK_STATUS_CHANGE;
                                }
                                else
                                {
                                    u1IsFromMib = CFA_FALSE;
                                }
                                if ((gu4CfaLinkUpStatus == CFA_DEFAULT_ENABLE)
                                    && (IssGetRestoreFlagFromNvRam () !=
                                        ISS_CONFIG_RESTORE)
                                    && (u1OperStatus == CFA_IF_UP)
                                    && (CFA_DEFAULT_ROUTER_IFINDEX ==
                                        u4IfIndex))
                                {
                                    CfaProcessLinkUpDelayStartUp ();
                                }
                                else
                                {
                                    CfaProcessPortOperStatus (pLinkStatusMsg->
                                                              u4IfIndex,
                                                              CFA_IF_ADMIN
                                                              (u4IfIndex),
                                                              u1OperStatus,
                                                              u1IsFromMib,
                                                              u1OldOperStatus);
                                }
                            }
#ifdef MPLS_WANTED
                            if (u1IfType == CFA_MPLS)
                            {
                                CfaUtilL3IfStatusHandleForMplsIf (u4IfIndex,
                                                                  u1OperStatus);
                            }
#endif
                            if (IssGetColdStandbyFromNvRam () ==
                                ISS_COLDSTDBY_ENABLE)
                            {
                                CfaGetEthernetType (u4IfIndex, &u1IfEtherType);
                                if (u1IfEtherType == CFA_STACK_ENET)
                                {
                                    if (CfaIfmHandleInterfaceStatusChange
                                        (CFA_DEFAULT_STACK_IFINDEX,
                                         CFA_IF_UP,
                                         pLinkStatusMsg->u1LinkStatus,
                                         CFA_FALSE) != CFA_SUCCESS)
                                    {
                                        printf ("[ERROR]:To make up default"
                                                "stack interface");
                                    }
                                }
                            }
                        }
                        break;
                    case CFA_PORT_CHANNEL_DELETE_MSG:
                        u4IfIndex = pExtTriggerMsg->uExtTrgMsg.u2PortChannelId;

                        CfaUtilDeletePortChannelInterface (u4IfIndex);

                        break;
                    case CFA_ADD_PORT_TO_PORT_CHANNEL_MSG:
                        /*Before adding the Uplink/downlink interface to the port channel
                         *LA sending this MSG to CFA*/
                        u4IfIndex =
                            pExtTriggerMsg->uExtTrgMsg.IfUpdateMsg.u4IfIndex;

                        if (u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
                        {
                            CfaGetIfUfdGroupId (u4IfIndex, &u4UfdGroupId);
                            if (u4UfdGroupId != 0)
                            {
                                /*If the adding port is present in the UFD group, 
                                 *Remove the port from the UFD group and addit to the port channel*/
                                if (CfaUfdRemovePortFromGroup
                                    (u4IfIndex, u4UfdGroupId) != CFA_SUCCESS)
                                {
                                    CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                                             "In CfaMain port add to port channel notification "
                                             "to UFD failed.\n");
                                }
                            }
                        }
                        break;
                    case CFA_PORT_CHANNEL_OPER_MSG:
                        /*while changing the port channel oper status
                         *LA sending this MSG to CFA*/
                        pLinkStatusMsg =
                            &(pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg);
                        u4IfIndex = pLinkStatusMsg->u4IfIndex;
                        u1OperStatus = pLinkStatusMsg->u1LinkStatus;

                        CfaGetIfType (u4IfIndex, &u1IfType);
                        if (u1IfType == CFA_LAGG)
                        {
                            /*If the port channel present in the UFD group, 
                             *UFD has to get the port channel oper status change
                             *Notification from LA*/
                            if (CfaNotifyUfdOperStatusUpdate
                                (u4IfIndex, u1OperStatus) != CFA_SUCCESS)
                            {
                                CFA_DBG1 (ALL_FAILURE_TRC, CFA_IFM,
                                          "Error in CfaNotifyUfdOperStatusUpdate - "
                                          "UFD Oper Status notify of the interface %d failed\n",
                                          u4IfIndex);
                            }
                        }
                        break;
                    case CFA_IPINFO_CONFIG_MSG:
                        u4IfIndex =
                            pExtTriggerMsg->uExtTrgMsg.IpInfoMsg.u4IfIndex;
                        CfaDynamicInterfaceIpConfig (u4IfIndex,
                                                     pExtTriggerMsg->uExtTrgMsg.
                                                     IpInfoMsg.IpInfo);
                        break;
                    case CFA_VIP_OPER_STATUS_MSG:

                        u4IfIndex = pExtTriggerMsg->uExtTrgMsg.
                            VipOperStatusMsg.u4IfIndex;

                        u1VipOperStatus = pExtTriggerMsg->uExtTrgMsg.
                            VipOperStatusMsg.u1VipOperStatus;

                        CfaSetVipOperStatus (u4IfIndex, u1VipOperStatus);

                        break;
#ifdef L2RED_WANTED
                    case CFA_RM_MESSAGE:
                        CfaHandleRmEvents (pExtTriggerMsg->uExtTrgMsg.CfaRmMsg);
                        break;
#endif

                    case CFA_BASE_BRIDGE_MODE_MSG:
                        CfaHandleBaseBridgeModeMsg
                            (pExtTriggerMsg->uExtTrgMsg.BaseBridgeModeMsg);
                        break;
                    case CFA_IF_MTU_UPDATE_CHANGE_MSG:
                        CfaSetIfMainMtu
                            (pExtTriggerMsg->uExtTrgMsg.IfUpdateMsg.u4IfIndex,
                             pExtTriggerMsg->uExtTrgMsg.IfUpdateMsg.u4Mtu);
                        break;
#ifdef ICCH_WANTED
                    case CFA_MCLAG_STATUS_CHANGE_MSG:
                        CfaHandleMclagIvrStatusChange
                            (pExtTriggerMsg->uExtTrgMsg.MclagOperStatusMsg.
                             u4IfIndex,
                             pExtTriggerMsg->uExtTrgMsg.MclagOperStatusMsg.
                             u1MclagIvrOperStatus);
                        break;
#endif
                    case CFA_HL_PORT_OPER_IND_MSG:
                        u4IfIndex =
                            pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u4IfIndex;
                        u1OperStatus =
                            pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.
                            u1LinkStatus;

                        CfaSetInterfaceHLPortOperIndication (u4IfIndex,
                                                             u1OperStatus);
                        break;

                    case CFA_LA_PORT_CREATE_IND_MSG:
                        u4IfIndex =
                            pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.u4IfIndex;
                        u1OperStatus =
                            pExtTriggerMsg->uExtTrgMsg.LinkStatusMsg.
                            u1LinkStatus;

                        CfaSetInterfaceLAPortCreateIndication (u4IfIndex,
                                                               u1OperStatus);
                        break;

                    case CFA_CREATE_DYN_SCH_IF:
                        CfaUtilCreateSChannelInterface
                            (pExtTriggerMsg->uExtTrgMsg.DynSchannelIf.
                             u4UapIfIndex,
                             pExtTriggerMsg->uExtTrgMsg.DynSchannelIf.
                             u4SChIfIndex, u1Status);
                        break;

                    case CFA_DELETE_DYN_SCH_IF:
                        CfaUtilDeleteSChannelInterface
                            (pExtTriggerMsg->uExtTrgMsg.DynSchannelIf.
                             u4SChIfIndex);
                        break;
                    case CFA_SCH_OPER_STATUS_MSG:

                        u4IfIndex = pExtTriggerMsg->uExtTrgMsg.
                            LinkStatusMsg.u4IfIndex;
                        u1SchOperStatus = pExtTriggerMsg->uExtTrgMsg.
                            LinkStatusMsg.u1LinkStatus;
                        CfaUtilSetSChannelOperStatus (u4IfIndex,
                                                      u1SchOperStatus);
                        break;

                    default:
                        break;
                }

                MemReleaseMemBlock (ExtTriggerMsgPoolId,
                                    (UINT1 *) pExtTriggerMsg);

                CFA_UNLOCK ();
            }
        }
#ifdef NAT_WANTED
        if (u4EventMask & NAT_TIMER_EXPIRY_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaMain NAT timer expiry.\n");
            CFA_LOCK ();
            NatProcessTimeOut ();
            CFA_UNLOCK ();
        }
        /* NAPT TIMER */
        if (u4EventMask & NAPT_TIMER_EXPIRY_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaMain NAPT timer expiry.\n");
            CFA_LOCK ();
            NaptProcessTimeOut ((tTimerListId) NAPT_TIMER_LIST_ID);
            CFA_UNLOCK ();
        }
        /* END */
        /* SIPALG TIMER */
        if (u4EventMask & NAT_SIPALG_TIMER_EXPIRY_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain SIPALG timer expiry.\n");
            CFA_LOCK ();
            NatSipAlgProcessTimeOut ((tTimerListId) NAT_SIPALG_TIMER_LIST_ID);
            CFA_UNLOCK ();
        }
        /* END */
        if (u4EventMask & NAT_FRAG_TIMER_EXPIRY_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain NAT Fragmentation timer expiry.\n");
            CFA_LOCK ();
            NatTmrHandleExpiry ();
            CFA_UNLOCK ();
        }

#endif

#ifdef FIREWALL_WANTED
/* Process the Expiry of the Firewall timer */
        if (u4EventMask & FWL_TIMER_EXPIRY_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Firewall timer expiry.\n");
            CFA_LOCK ();
            FwlTmrHandleTmrExpiry ();
            CFA_UNLOCK ();
        }
#endif

#ifdef IPSECv4_WANTED
        if (u4EventMask & IPSEC_TIMER_EXPIRY_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "In CfaMain NAT timer expiry.\n");
            CFA_LOCK ();
            Secv4ProcessTimeOut ();
            CFA_UNLOCK ();
        }
#endif
#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
        if (u4EventMask & CFA_TUNL_PKT_RECV_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain recd tunnel pkt Arrival event.\n");
            CFA_LOCK ();
            IpifRecvTnlPkt ();
            CFA_UNLOCK ();
            SelAddFd (gi4RawSockId, CfaNotifyTnlTask);
        }
        if (u4EventMask & CFA_GRE_TUNL_PKT_RECV_EVENT)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain recd Gre tunnel pkt Arrival event.\n");
            CFA_LOCK ();
            IpifRecvGreTnlPkt ();
            CFA_UNLOCK ();
            SelAddFd (gi4GreRawSockId, CfaNotifyGreTnlTask);
        }
#endif

#ifdef L2RED_WANTED
        if (u4EventMask & CFA_SUB_BULK_UPDT_EVENT)
        {
            CFA_LOCK ();
            CfaHandleBulkReqEvent (NULL);
            CFA_UNLOCK ();
        }
#endif
        if (u4EventMask & CFA_ATTACK_PACKET_RX_EVENT)
        {
            while (OsixQueRecv (CFA_ATTACK_PACKET_QID,
                                (UINT1 *) &pAttackPkt,
                                OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
            {
                CFA_LOCK ();
                SecPortHandleLogMessages (pAttackPkt);
                CFA_UNLOCK ();
            }
        }
        /* Event handling for generating user defined packet patterns 
         * from CFA */

        if (u4EventMask & CFA_PKT_GEN_TX_EVENT)
        {
            while (OsixQueRecv (CFA_PKT_GEN_TX_QID, (UINT1 *) &pu1TxPktInfo,
                                OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
            {
                CFA_LOCK ();
                CfaPktGenTxPkt (pu1TxPktInfo);
                MemReleaseMemBlock (CFA_PKT_GEN_QUE_POOL_ID,
                                    (UINT1 *) pu1TxPktInfo);
                CFA_UNLOCK ();
            }
        }

        /* Timer Event handling for generating user defined packet patterns 
         * from CFA */
        if (u4EventMask & CFA_PKT_GEN_TMR_EVENT)
        {
            CFA_LOCK ();
            CfaPktGenTmrExpHdlr ();
            CFA_UNLOCK ();
        }
#ifdef VCPEMGR_WANTED
        /* TAP Interface event handling */
        if (u4EventMask & TAP_IF_PKT_EVENT)
        {
            while (OsixQueRecv (gTapIfQId, (UINT1 *) &pTapIfMsg,
                                OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
            {
                CFA_LOCK ();
                CfaGddTunTapRead (pTapIfMsg->i4TapFd);
                INT4                i4TaprdFd = pTapIfMsg->i4TapFd;
                MemReleaseMemBlock (gTapIfMemPoolId, (UINT1 *) pTapIfMsg);
                CFA_UNLOCK ();
                SelAddFd (i4TaprdFd, TapIfCallbackFn);
            }
        }
#endif

    }                            /* end of while(1) loop */
}

/*****************************************************************************/
/* Function Name      : CfaUpdateHwParams                                    */
/*                                                                           */
/* Description        : This function updates the interface  parameters in CFA*/
/*                      common database                                      */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaUpdateHwParams (UINT4 u4IfIndex)
{
    INT4                i4RetVal = CFA_SUCCESS;
    INT4                i4DuplexStatus = ETH_STATS_DUP_ST_UNKNOWN;
    UINT4               u4Speed = 0;
    UINT4               u4HighSpeed = 0;
    UINT4               u4PortCount = 0;
    INT4                i4MauIndex = 1;
    INT4                i4Support = 0;
    INT4                i4Status = 0;
    UINT4               u4OperMauType = 0;
    UINT2               u2AdvtCapability = 0;
    tCfaIfInfo          IfInfo;
    tHwPortInfo         HwPortInfo;
#ifdef NPAPI_WANTED
    tEthStats           EthStats;
#endif

    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    /*
     * Obtain the interface parameters and update them in
     * CFA common database table.
     */

    /* Get the default values for interface parameters from CFA */
    if ((CfaGetIfInfo (u4IfIndex, &IfInfo)) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }

    CfaDetermineSpeed (u4IfIndex, &u4Speed, &u4HighSpeed);

    if ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) ||
        (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL))
    {
        u4PortCount = (SYS_DEF_MAX_DATA_PORT_COUNT +
                       (HwPortInfo.u4NumConnectingPorts) * 2);
        /* Store boot up port properities in default structure */
        if ((u4IfIndex < u4PortCount)
            && (gCfaDefaultPortInfo[u4IfIndex].u4IfSpeed == 0)
            && (gCfaDefaultPortInfo[u4IfIndex].u4IfHighSpeed == 0))
        {
            gCfaDefaultPortInfo[u4IfIndex].u4IfSpeed = u4Speed;
            gCfaDefaultPortInfo[u4IfIndex].u4IfHighSpeed = u4HighSpeed;
        }
    }

    /* check for default invalid value. If value is not invalid(default),
     * CFA structure is updated through MSR */
    if (IfInfo.u4IfSpeed == 0)
    {
        /* Update the Speed Value */
        CfaSetIfSpeed (u4IfIndex, u4Speed);
        CfaSetIfHighSpeed (u4IfIndex, u4HighSpeed);
    }

#ifdef NPAPI_WANTED
    MEMSET (&EthStats, 0, sizeof (tEthStats));
    EthStats.u4ReqType = NP_STAT_PORT_DUPLEX_STATUS;
    CfaFsEtherHwGetStats (u4IfIndex, &EthStats);
    i4DuplexStatus = EthStats.u4DuplexStatus;
#endif
    if ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) ||
        (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL))
    {
        /* Store boot up port properities in default structure */
        if ((u4IfIndex < u4PortCount) &&
            (gCfaDefaultPortInfo[u4IfIndex].u4DuplexStatus == 0))
        {
            gCfaDefaultPortInfo[u4IfIndex].u4DuplexStatus =
                (UINT4) i4DuplexStatus;
        }
    }

    /* check for default invalid value. If value is not invalid,
     * CFA structure is updated through MSR */
    if (IfInfo.u1DuplexStatus == 0)
    {
        /* Update the Duplex status */
        CfaSetIfDuplexStatus (u4IfIndex, i4DuplexStatus);
    }

    /* 
     * For MAU low level routines, where REPEATER connection is not 
     * supported, the i4MauIndex for the MAU Tables is always "1". 
     * Hence second argument is 1. 
     */
    CfaGetIfAutoNegSupport (u4IfIndex, i4MauIndex, &i4Support);
    CfaGetIfAdvtCapability (u4IfIndex, i4MauIndex, &u2AdvtCapability);
    CfaGetIfOperMauType (u4IfIndex, i4MauIndex, &u4OperMauType);
    CfaSetIfAutoNegSupport (u4IfIndex, i4Support);
    CfaSetIfAdvtCapability (u4IfIndex, u2AdvtCapability);
    CfaSetIfOperMauType (u4IfIndex, (UINT2) u4OperMauType);

    CfaGetIfAutoNegStatus ((INT4) u4IfIndex, i4MauIndex, &i4Status);
    if ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) ||
        (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL))
    {
        /* Store boot up port properities in default structure */
        if ((u4IfIndex < u4PortCount) &&
            (gCfaDefaultPortInfo[u4IfIndex].u4AutoNegStatus == 0))
        {
            gCfaDefaultPortInfo[u4IfIndex].u4AutoNegStatus = (UINT4) i4Status;
        }
    }

    /* check for default invalid value. If value is not invalid,
     * CFA structure is updated through MSR */
    if (IfInfo.u1AutoNegStatus == 0)
    {
        /* Update the Negotiation status */
        CfaSetIfAutoNegStatus (u4IfIndex, i4Status);
    }

    /*
     * Flow control is not stored in CFA common database
     * and hence the CfaSetIf...() routine is not called for these
     * objects.
     */
    CfaGetIfFlowControl (u4IfIndex);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : CfaLock                                              */
/*                                                                           */
/* Description        : This function is used to take the CFA  protocol      */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      CFA database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
CfaLock (VOID)
{
    if (OsixSemTake (gCfaSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the CFA                */
/*                      SEMa4 to avoid simultaneous access to                */
/*                      CFA database                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaUnlock (VOID)
{
    OsixSemGive (gCfaSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaProcessIpInitComplete
 *
 *    Description        : This function handles the signal from IP
 *                indicating that the IP task is UP. It then
 *                registers the interfaces with IP, sends IP
 *                a signal indicating Interface Creation
 *                Complete and finally starts the GDD poll
 *                timer.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 * 
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaProcessIpInitComplete (VOID)
{

    tIpConfigInfo       IpCfgInfo;
    UINT4               u4Flag = 0;
    UINT4               u4DefIfIndex;
    UINT4               u4IpAddress;
    UINT4               u4IpAddrAllocProto;
#if defined (LNXIP4_WANTED) || defined (IP_WANTED)
    UINT1               u1AllocMethod = 0;
#endif
    UINT4               u4SubnetMask;
    UINT1               u1LinkStatus = 0;
    tCfaDefPortParams   CfaDefPortParams;
    UINT1               u1IfType;
    UINT4               u4StackIp = 0;
    UINT4               u4StackMask = 0;
    UINT1               u1OldOperStatus = CFA_IF_DOWN;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    UINT1               u1AdminStatus = 0;
    /* Determine the Interface Index which is to be registered with IP */
    u4DefIfIndex = CfaGetDefaultRouterIfIndex ();

    CfaGetDefaultPortParams (&CfaDefPortParams);

    u4IpAddress = CfaDefPortParams.u4IpAddress;
    u4SubnetMask = CfaDefPortParams.u4SubnetMask;
    u4IpAddrAllocProto = CfaDefPortParams.u4IpAddrAllocProto;

#if defined (LNXIP4_WANTED) || defined (IP_WANTED)
    if (CfaDefPortParams.u4ConfigMode == ISS_CFG_DYNAMIC)
    {
        /* Register the IP Interface with IP Addres 0.0.0.0
         * IP Address will be acquired by RARP / BOOTP / DHCP
         * when interface oper status notification is given */

        u1AllocMethod = CFA_IP_ALLOC_POOL;
        u4IpAddress = 0;
        u4SubnetMask = 0;

    }
    else
    {
        /* Mode Manual */
        u1AllocMethod = CFA_IP_ALLOC_MAN;

        /* Same subnet address shouldn't be allowed for multiple 
         * interfaces */
        if (CfaIpIfValidateIfIpSubnet (u4DefIfIndex, u4IpAddress,
                                       u4SubnetMask) == CFA_FAILURE)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "CfaProcessIpInitComplete - FAIL "
                     "same subnet address shouldn't be configured for"
                     " multiple interfaces.\n");
            return;
        }
    }

    /* Set all the IP interface params */
    u4Flag = CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD |
        CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK |
        CFA_IP_IF_FORWARD | CFA_IP_IF_BCASTADDR;

    IpCfgInfo.u4Addr = u4IpAddress;
    IpCfgInfo.u4NetMask = u4SubnetMask;
    IpCfgInfo.u4BcastAddr = (~u4SubnetMask | u4IpAddress);
    IpCfgInfo.u1AddrAllocMethod = u1AllocMethod;
    IpCfgInfo.u1AddrAllocProto = (UINT1) u4IpAddrAllocProto;
    IpCfgInfo.u1IpForwardEnable = CFA_ENABLED;
    if (CfaDefPortParams.u4ConfigMode != ISS_CFG_DYNAMIC)
    {
        IpCfgInfo.Ip6Addr = CfaDefPortParams.localIp6Addr;
        IpCfgInfo.u1PrefixLen = CfaDefPortParams.u1PrefixLen;
        gu1CfaDefIpv6Initialised = FALSE;    /*After setting default ipv6 address set tha flag False */
    }
    else
    {
        IpCfgInfo.u1PrefixLen = 0;
    }
    /* Create mapping in VCM */
    if (VcmCfaCreateIPIfaceMapping (VCM_DEFAULT_CONTEXT, u4DefIfIndex) ==
        VCM_FAILURE)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "CfaProcessIpInitComplete - "
                 "Def interface mapping in VCM - FAIL.\n");
        return;
    }
    if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW, u4DefIfIndex,
                                      u4Flag, &IpCfgInfo) != CFA_SUCCESS)
    {

        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "CfaProcessIpInitComplete - "
                 "Def interface registration with IP - FAIL.\n");

        return;

    }
#endif
    /* Get the Link Status from Driver if it is not a OOB Interface */
    if (CFA_MGMT_PORT == FALSE)
    {
        u1LinkStatus = CfaGddGetLinkStatus (CFA_DEFAULT_ROUTER_IFINDEX);

        CfaGetIfType (CFA_DEFAULT_ROUTER_IFINDEX, &u1IfType);

        if ((u1LinkStatus == CFA_IF_UP) && (u1IfType == CFA_ENET)
            && (CfaIsMgmtPort (CFA_DEFAULT_ROUTER_IFINDEX) == FALSE))
        {

            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                CfaUpdateHwParams (CFA_DEFAULT_ROUTER_IFINDEX);
            }
        }
    }
    else
    {
        u1LinkStatus = CFA_IF_UP;

        if (CFA_OOB_MGMT_INTF_ROUTING == FALSE)
        {
            /* Disable the IP Forwarding for OOB Interface 
               when routing over the oob is not wanted */
            IpCfgInfo.u1IpForwardEnable = CFA_DISABLED;
        }
        else
        {
            /* Enable the IP Forwarding for OOB Interface 
               when routing over the oob is  wanted */
            IpCfgInfo.u1IpForwardEnable = CFA_ENABLED;

        }
        u4Flag = CFA_IP_IF_FORWARD;

        if (CfaIpIfSetIfInfo (CFA_DEFAULT_ROUTER_IFINDEX,
                              u4Flag, &IpCfgInfo) == CFA_FAILURE)
        {
            return;
        }
#if defined(IP6_WANTED) && (defined(CLI_LNXIP_WANTED) || defined(SNMP_LNXIP_WANTED))
        /* Linux programming of OOB Mgmt port during ISS boot-up. Linux interface 
         * programming is done only in active node. In case of standby, programming
         * is taken care in GO_ACTIVE event during switchover. */
#ifdef RM_WANTED
        if (RmGetNodeState () == RM_ACTIVE)
        {
#endif
            CfaIpUpdateInterfaceParams (CFA_CDB_IF_ALIAS
                                        (CFA_DEFAULT_ROUTER_IFINDEX), u4Flag,
                                        &IpCfgInfo);
            CfaSetIfIp6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpCfgInfo.Ip6Addr,
                             (INT4) IpCfgInfo.u1PrefixLen, OSIX_TRUE);
#ifdef RM_WANTED
        }
        else
        {
            /* During boot-up of standby, the default mgmt port ipv6 params need to updated to cfa db 
             * but not to be programmed in linux. So that same can be used for linux programming when
             * GO_ACTIVE event is received on switchover. */
            CfaSetIfIp6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpCfgInfo.Ip6Addr,
                             (INT4) IpCfgInfo.u1PrefixLen, OSIX_TRUE);
            CfaLinuxIpUpdateIpv6Addr (CFA_DEFAULT_ROUTER_IFINDEX, &IpCfgInfo.
                                      Ip6Addr, (INT4) IpCfgInfo.u1PrefixLen,
                                      OSIX_FALSE);
            CfaIpUpdateInterfaceParams (CFA_CDB_IF_ALIAS
                                        (CFA_DEFAULT_ROUTER_IFINDEX),
                                        CFA_IP_IF_NOFORWARD, NULL);
        }
#endif
#endif
    }
    if ((CFA_DEFAULT_IFINDEX_SUPPORTED) || (CFA_MGMT_PORT == TRUE) ||
        ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) &&
         ((VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE) ? 1 : 0)))
    {
        if ((IssGetRestoreFlagFromNvRam () != ISS_CONFIG_RESTORE) ||
            (IssGetRestoreFlagFromNvRam () != ISS_CONFIG_REMOTE_RESTORE))
        {
            u1AdminStatus = CFA_IF_UP;
        }
        else
        {
            u1AdminStatus = CFA_IF_DOWN;
        }

        if (CfaIfmHandleInterfaceStatusChange (CFA_DEFAULT_ROUTER_IFINDEX,
                                               u1AdminStatus, u1LinkStatus,
                                               CFA_IF_LINK_STATUS_CHANGE) !=
            CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "FATAL ERROR in CfaProcessIpInitComplete"
                     "make default interface UP - FAIL.\n");
            return;
        }
#ifndef OS_VXWORKS
#ifdef IP_WANTED
#if !defined(CLI_LNXIP_WANTED) && !defined(SNMP_LNXIP_WANTED)
        if (CFA_MGMT_PORT == TRUE)
        {
            SelAddFd (CFA_GDD_PORT_DESC (CFA_DEFAULT_ROUTER_IFINDEX),
                      CfaOobDataRcvd);
        }
#endif
#endif
#endif
    }
    if ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) &&
        ((VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE) ? 1 : 0))
    {
        /* Update the link status of the default interface in the unit. */
        if (CfaGetIfOperStatus (CFA_DEFAULT_ROUTER_IFINDEX, &u1OldOperStatus) ==
            CFA_SUCCESS)
        {
            u1LinkStatus = CfaGddGetLinkStatus (CFA_DEFAULT_ROUTER_IFINDEX);

            if (u1LinkStatus == CFA_IF_UP)
            {
                u1OperStatus = CFA_IF_UP;
            }
            else if (u1LinkStatus == CFA_IF_NP)
            {
                u1OperStatus = CFA_IF_NP;
            }
            else
            {
                u1OperStatus = CFA_IF_DOWN;
            }

            if (u1OperStatus != u1OldOperStatus)
            {
                CfaIfmHandleInterfaceStatusChange (CFA_DEFAULT_ROUTER_IFINDEX,
                                                   CFA_IF_ADMIN
                                                   (CFA_DEFAULT_ROUTER_IFINDEX),
                                                   u1OperStatus,
                                                   CFA_IF_LINK_STATUS_CHANGE);
            }
        }
    }
#if defined (LNXIP4_WANTED) || defined (IP_WANTED)
    if ((gu4IsIvrEnabled == CFA_ENABLED) && (CFA_MGMT_PORT == FALSE))
    {
        VlanIvrGetVlanIfOperStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                                    &u1LinkStatus);

        if (CfaIfmHandleInterfaceStatusChange
            (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_IF_UP, u1LinkStatus,
             CFA_FALSE) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                     "FATAL ERROR in CfaProcessIpInitComplete"
                     "make default interface UP - FAIL.\n");
            return;
        }
    }
#endif

    /* Start the GDD Timer */
    CfaStartGddTimer ();

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* Creating Stack IP.StackIp & Mask will be taken from RM */
#ifdef RM_WANTED
        RmGetSelfNodeIp (&u4StackIp);
        RmGetSelfNodeMask (&u4StackMask);
#endif /* RM_WANTED */
        /* Set all the IP interface params */
        u4Flag = CFA_IP_IF_ALLOC_PROTO | CFA_IP_IF_ALLOC_METHOD |
            CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK |
            CFA_IP_IF_FORWARD | CFA_IP_IF_BCASTADDR;

        IpCfgInfo.u4Addr = u4StackIp;
        IpCfgInfo.u4NetMask = u4StackMask;
        IpCfgInfo.u4BcastAddr = (~u4StackMask | u4StackIp);
        IpCfgInfo.u1AddrAllocMethod = u1AllocMethod;
        IpCfgInfo.u1AddrAllocProto =
            (UINT1) IssGetIpAddrAllocProtoFromNvRam ();;
        IpCfgInfo.u1IpForwardEnable = CFA_DISABLED;

        if (CfaIpIfValidateIfIpSubnet (CFA_DEFAULT_STACK_IFINDEX, u4StackIp,
                                       u4StackMask) == CFA_FAILURE)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "CfaProcessIpInitComplete - FAIL "
                     "same subnet address shouldn't be configured for"
                     " multiple interfaces.\n");
            return;
        }
        /* Create mapping in VCM */
        VcmCfaCreateIPIfaceMapping (VCM_DEFAULT_CONTEXT, u4DefIfIndex);
        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW,
                                          (UINT2) CFA_DEFAULT_STACK_IFINDEX,
                                          u4Flag, &IpCfgInfo) != CFA_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "CfaProcessIpInitComplete - "
                     "Def interface registration with IP - FAIL.\n");
            return;
        }
    }
}

/*****************************************************************************/
/* Function Name      : CfaHandlePktFromL2                                   */
/*                                                                           */
/* Description        : This function is called by L2 modules to send out the*/
/*                      pkts through CFA                                     */
/* Input(s)           : pBuf - Pointer to the CRU Buffer                     */
/*                      u4IfIndex - ifIndex of the driver port.              */
/*                      u4PktSize - Size of the data buffer                  */
/*                      u2Protocol - Protocol type                           */
/*                      au1DestHwAddr-Destination Mac Address                */
/*                      u1EncapType - Encapsulation Type                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaHandlePktFromL2 (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                    UINT4 u4PktSize, UINT2 u2Protocol,
                    UINT1 *au1DestHwAddr, UINT1 u1EncapType)
{
    tCfaIfInfo          IfInfo;
    UINT1               u1IfType;
    UINT4               u4OutPort;
#ifdef WLC_WANTED
    unWlcHdlrMsgStruct *pcapwapMsgStruct = NULL;
    UINT1               u1OperStatus = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    UINT2               u2EtherType = 0;
#endif

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ALL, CFA_IWF,
                  "Get interface info for Interface %d failed.\n", u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);    /* buffer is released by GDD */
    }

    /* Check whether the interface is Valid */
    if (IfInfo.i4Valid != CFA_TRUE)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2, "Invalid Interface Index. \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    u1IfType = IfInfo.u1IfType;

    switch (u1IfType)
    {
        case CFA_LAGG:
        case CFA_ENET:
        case CFA_PROP_VIRTUAL_INTERFACE:
        case CFA_PSEUDO_WIRE:
        case CFA_BRIDGED_INTERFACE:
#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
#endif
#ifdef VCPEMGR_WANTED
        case CFA_TAP:
#endif
            /* Fall through */
            if (L2IwfWrPreprocessOutgoingFrameFromCfa
                (pBuf, u4PktSize, u4IfIndex, u2Protocol, u1EncapType,
                 &u4OutPort, u1IfType) == L2IWF_FAILURE)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                         "Packet Validation failed in L2-IWF module.\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }

            u4IfIndex = u4OutPort;

            if (CfaIwfEnetProcessTxFrame
                (pBuf, u4IfIndex, au1DestHwAddr, u4PktSize,
                 u2Protocol, u1EncapType) != CFA_SUCCESS)
            {
                /* release of the pkt in IWF/GDD */
                CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                         "Eth pkt from bridge dispatch to IWF fail.\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
            break;

#ifdef PPP_WANTED
        case CFA_PPP:

            if (CfaIwfPppHandleOutgoingPkt
                (pBuf, u4IfIndex, u4PktSize, u2Protocol) != CFA_SUCCESS)
            {

                CFA_DBG (CFA_TRC_ALL, CFA_IWF,
                         "PPP pkt from bridge dispatch to " "IWF fail.\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
            break;
#endif /* PPP_WANTED */
#ifdef WLC_WANTED
        case CFA_CAPWAP_DOT11_BSS:
        {
            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);

            if (u1OperStatus != CFA_IF_UP)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                         "The CFA_CAPWAP_DOT11_BSS interface is not Operationally UP\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);

            }
            /*PNAC Validation is done here for CAPWAP interfaces */
            if (L2IwfWrPreprocessOutgoingFrameFromCfa
                (pBuf, u4PktSize, u4IfIndex, u2Protocol, u1EncapType,
                 &u4OutPort, u1IfType) == L2IWF_FAILURE)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                         "Packet Validation failed for DOT11_BSS  in L2-IWF module.\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return (CFA_FAILURE);
            }
            u4IfIndex = u4OutPort;

            /*Get the Local port for the BSS if index */
            VLAN_LOCK ();
            if (VlanGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                               &u2LocalPort) != VLAN_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "CfaHandlePktFromL2: VlanGetContextInfoFromIfIndex FAILED\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }

            if (VlanSelectContext (u4ContextId) != VLAN_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "CfaHandlePktFromL2: VlanSelectContext FAILED\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                VLAN_UNLOCK ();
                return VLAN_FAILURE;
            }

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                                       (CFA_ENET_ADDR_LEN * 2),
                                       sizeof (u2EtherType));

            if ((VlanClassifyFrame (pBuf, OSIX_NTOHS (u2EtherType), u2LocalPort)
                 != VLAN_UNTAGGED))
            {
                VlanUnTagFrame (pBuf);
            }

            VlanReleaseContext ();
            VLAN_UNLOCK ();

            pcapwapMsgStruct =
                (unWlcHdlrMsgStruct *) (VOID *) UtlShMemAllocWlcBuf ();

            MEMSET (pcapwapMsgStruct, 0, sizeof (unWlcHdlrMsgStruct));
            pcapwapMsgStruct->WlcHdlrQueueReq.pRcvBuf = pBuf;
            pcapwapMsgStruct->WlcHdlrQueueReq.u2SessId = u4IfIndex;
            pcapwapMsgStruct->WlcHdlrQueueReq.u4MsgType = WLCHDLR_CFA_RX_MSG;

            /* Invoke the WSSIF module to send the packet to MAC Handler module */
            /* On success / Failure cases, Buffer is released by WSS */

            if (WlcHdlrEnqueCtrlPkts (pcapwapMsgStruct) != OSIX_SUCCESS)
            {
                CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                         "Packet forwarding from L2 to WSS failed for CAPWAP Index\n");
                UtlShMemFreeWlcBuf ((UINT1 *) pcapwapMsgStruct);
                return CFA_FAILURE;
            }
            UtlShMemFreeWlcBuf ((UINT1 *) pcapwapMsgStruct);
        }
            break;
#endif
        default:
            CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                     "Lower layer interface is not  "
                     "Ethernet or PPP or AtmVc \n");

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : CfaHandlePktFromEoam                                 */
/*                                                                           */
/* Description        : This function is called from CFA to send out OAMPDUs */
/*                      through CFA                                          */
/* Input(s)           : pBuf - Pointer to the CRU Buffer                     */
/*                      u4IfIndex - ifIndex of the driver port.              */
/*                      u4PktSize - Size of the data buffer                  */
/*                      u2Protocol - Protocol type                           */
/*                      au1DestHwAddr-Destination Mac Address                */
/*                      u1EncapType - Encapsulation Type                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS or CFA_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
CfaHandlePktFromEoam (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT4 u4PktSize, UINT2 u2Protocol,
                      UINT1 *au1DestHwAddr, UINT1 u1EncapType)
{
    UINT1               u1IfType = 0;
    INT4                i4RetVal = CFA_SUCCESS;

    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, CFA_L2, "Invalid Interface Index. \n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }

    CfaGetIfType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_ENET)
    {
        if (CfaIwfEnetProcessTxFrame (pBuf, u4IfIndex, au1DestHwAddr,
                                      u4PktSize, u2Protocol,
                                      u1EncapType) != CFA_SUCCESS)
        {
            /* release of the pkt in IWF/GDD */
            CFA_DBG (ALL_FAILURE_TRC, CFA_L2,
                     "Eth pkt from EOAM dispatch to IWF fail.\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            i4RetVal = CFA_FAILURE;
        }
    }

    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : CfaHandleBaseBridgeModeMsg                           */
/*                                                                           */
/* Description        : This function handles the Base BridgeMode messages   */
/*                                                                           */
/* Input(s)           : CfaBaseBridgeModeMsg                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaHandleBaseBridgeModeMsg (tCfaBaseBridgeModeMsg CfaBaseBridgeModeMsg)
{
    tCfaDefPortParams   CfaDefPortParams;
    UINT4               u4DefIfIndex = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4SubnetMask = 0;
    UINT1               u1OperStatus = 0;
    UNUSED_PARAM (CfaBaseBridgeModeMsg);

    /* Map the default interface 0/1 to default context. */
    CfaIfmMapDefaultInterfaceForDefaultContext ();

    /* For Dot1d-Bridge Mode, delete all IVR interfaces in teh switch.
     * For Dot1q-vlan Mode, create the default IVR interface */

    CfaCreateDefaultIVRInterface ();

    /* Determine the Default IVR Interface */
    u4DefIfIndex = CfaGetDefaultRouterIfIndex ();

    CfaGetDefaultPortParams (&CfaDefPortParams);

    u4IpAddress = CfaDefPortParams.u4IpAddress;
    u4SubnetMask = CfaDefPortParams.u4SubnetMask;

    if (CfaDefPortParams.u4ConfigMode == ISS_CFG_DYNAMIC)
    {
        /* Register the IP Interface with IP Addres 0.0.0.0
         * IP Address will be acquired by RARP / BOOTP / DHCP
         * when interface oper status notification is given */
        u4IpAddress = 0;
        u4SubnetMask = 0;
    }

    /* Set the IP Address to the Default IVR Interface */
    CfaIPVXSetIpAddress (u4DefIfIndex, u4IpAddress, u4SubnetMask);

    /*Make the Default IVR Port as Admin Up */
    CfaGetIfOperStatus (u4DefIfIndex, &u1OperStatus);

    CfaIfmHandleInterfaceStatusChange (u4DefIfIndex, (UINT1) CFA_IF_UP,
                                       u1OperStatus, CFA_TRUE);
    return;
}

/*****************************************************************************/
/* Function Name      : CfaHandleMclagIvrStatusChange                        */
/*                                                                           */
/* Description        : This function posts the Mclag port membership changes*/
/*                      to VRRP module                                       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Vlan Index                               */
/*                      u1IvrOperStatus - IVR Oper status                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
CfaHandleMclagIvrStatusChange (UINT4 u4IfIndex, UINT1 u1IvrOperStatus)
{

#ifdef VRRP_WANTED
    VrrpHandleMclagIvrOperStatus ((INT4) u4IfIndex, u1IvrOperStatus);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1IvrOperStatus);
#endif

    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaStartGddTimer
 *
 *    Description          : This function used to start GDD timer.    
 *
 *    Input(s)             : None 
 *
 *    Output(s)            : None
 *
 *    Returns              : CFA_SUCCESS/CFA_FAILURE
 *****************************************************************************/
INT4
CfaStartGddTimer (VOID)
{
    /* GDD Poll timer will be started only when CFA_INTERRUPT_MODE 
     * is undefined */
#ifndef CFA_INTERRUPT_MODE
#ifdef RM_WANTED
    /* GDD Poll timer is started only when the RM node state
     * is ACTIVE. STANDBY node should not do any protocol 
     * operation. So the GddPoll timer is not started 
     * in order to avoid packet processing. */
    if (RmGetNodeState () == RM_ACTIVE)
    {
#endif /* RM_WANTED */
        /* start the GDD poll timer */
        if (TmrStartTimer (CFA_GDD_TMR_LIST, &GddPollTimer,
                           CFA_GDD_POLL_TIMEOUT) != TMR_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN, "CfaStartGddTimer -Starting the "
                     "GDD poll timer - FAILURE.\n");
            return CFA_FAILURE;
        }
#ifdef RM_WANTED
    }
#endif /* RM_WANTED */
#endif /* CFA_INTERRUPT_MODE */

    return CFA_SUCCESS;
}
