/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: gddvx.c,v 1.22 2016/07/23 11:41:26 siva Exp $ 
 *
 * Description: This file contains the routines for the
 *             Device Driver modules of VxWorks.
 *******************************************************************/
#include "cfainc.h"

UINT4               u4PktCount = 0;
UINT4               u4PktSendCount = 0;
static UINT4        au4Spare[SYS_DEF_MAX_INTERFACES];

/*Create the Cookie list and allocate the memory here*/

INT4
CfaGddInit (VOID)
{
    MEMSET (au4Spare, 0, (SYS_DEF_MAX_INTERFACES * sizeof (UINT4 *)));

    return (CFA_SUCCESS);
}

/*-----------------------------------------------------------------------*/

VOID
CfaGddShutdown (VOID)
{
    return;
}

/*-----------------------------------------------------------------------*/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
    return (CFA_SUCCESS);
}

#ifdef MUX_SUPPORT
/*-----------------------------------------------------------------------*/
/* Static function definitions */
STATUS CfaGddShutdownRtn PROTO ((VOID *pCookie, VOID *pSpare));
STATUS CfaGddRestartRtn PROTO ((VOID *pCookie, VOID *pSpare));
VOID CfaGddErrorRtn PROTO ((END_OBJ * pEnd, END_ERR * pErr, VOID *pSpare));
BOOL CfaSendPktToMux PROTO ((VOID *pCookiePtr, UINT4 u4PktType,
                             M_BLK_ID pMuxBuf, LL_HDR_INFO * pLinkHdrInfo,
                             VOID *pSpare));
BOOL                CfaRcvPktFromMux
PROTO ((VOID *pCookiePtr, UINT4 u4PktType, M_BLK_ID pMuxBuf,
        LL_HDR_INFO * pLinkHdrInfo, VOID *pSpare));
INT4 CfaGddMuxDetachVxIp PROTO ((INT4 i4UnitNo, UINT1 *pDevName));
INT4 CfaGddMuxBind  PROTO ((UINT4 u4IfIndex));
INT4 CfaGddMuxIsVxIpAttached PROTO ((UINT1 *pu1PortName, UINT4 u4IfIndex));
M_BLK_ID
    CfaGddNetMblkFromBufCopy
PROTO ((UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize));
extern ipDetach     PROTO ((INT4, UINT1 *));
/*-----------------------------------------------------------------------*/
BOOL
CfaSendPktToMux (VOID *pCookiePtr, UINT4 u4PktType, M_BLK_ID pMuxBuf,
                 LL_HDR_INFO * pLinkHdrInfo, VOID *pSpare)
{
    return FALSE;
}

/*-----------------------------------------------------------------------*/
BOOL
CfaRcvPktFromMux (VOID *pCookiePtr, UINT4 u4PktType, M_BLK_ID pMuxBuf,
                  LL_HDR_INFO * pLinkHdrInfo, VOID *pSpare)
{

    UINT1              *pu1DataBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4PktSize;
    UINT4               u4Len;
    UINT2               u4IfIndex;
    UINT2               u2LenOrType;
    UINT2               u2Protocol = 0;
    UINT2               u2ExtraBytes = 0;
    UINT1               u1EnetHeaderSize;
    INT2                i2IpTotalLen;
    tEnetV2Header      *pEthHdr = NULL;
    tEnetSnapHeader    *pSnapEthHdr = NULL;

    CFA_DBG (CFA_DRIVER_TRC, "CfaRcvPktFromMux: Pkt Arrived: \n");

    if (CFA_INITIALISED != TRUE)
    {
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        CFA_DBG (ALL_FAILURE_TRC,
                 "CfaRcvPktFromMux: CFA is not initialized - FAILURE!\n");
        return FALSE;
    }

    u4IfIndex = (*(UINT4 *) pSpare & 0x0000ffff);

    if (CFA_GDD_HL_RX_FNPTR (u4IfIndex) == NULL)
    {
        /* Higher layer is not found. Release Mux buffer */
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        CFA_DBG (ALL_FAILURE_TRC,
                 "CfaRcvPktFromMux: HL not registered - FAILURE!\n");
        return FALSE;
    }

    u4PktSize = pMuxBuf->mBlkPktHdr.len;

    pu1DataBuf = MemAllocMemBlk (gCfaPktPoolId);
    if (pu1DataBuf == NULL)
    {
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        CFA_DBG (ALL_FAILURE_TRC,
                 "CfaRcvPktFromMux: Linear Buffer Allocation - FAILURE!\n");
        return FALSE;
    }

    if ((u4Len = netMblkToBufCopy (pMuxBuf, pu1DataBuf, NULL)) != u4PktSize)
    {
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        CFA_DBG (ALL_FAILURE_TRC,
                 "CfaRcvPktFromMux: mBuf To Linear Buffer Copy - FAILURE!\n");
        return FALSE;
    }

    CFA_IF_SET_IN_OCTETS (u4IfIndex, u4Len);

    pEthHdr = (tEnetV2Header *) pu1DataBuf;
    u2LenOrType = OSIX_NTOHS (pEthHdr->u2LenOrType);

    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        u2Protocol = u2LenOrType;
        u1EnetHeaderSize = CFA_ENET_V2_HEADER_SIZE;
    }
    else
    {
        pSnapEthHdr = (tEnetSnapHeader *) pu1DataBuf;
        u1EnetHeaderSize = CFA_ENET_SNAP_HEADER_SIZE;

        /* check for LLC control frame first - we expect only LLC in SNAP now */
        if (pSnapEthHdr->u1Control == CFA_LLC_CONTROL_UI)
        {
            /*check for presence of SNAP which may carry IP/ARP/RARP after LLC */
            if ((pSnapEthHdr->u1DstLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1SrcLSap == CFA_LLC_SNAP_SAP) ||
                (pSnapEthHdr->u1Oui1 == 0x00) ||
                (pSnapEthHdr->u1Oui2 == 0x00) || (pSnapEthHdr->u1Oui3 == 0x00))
            {
                /* determine which protocol is being carried */
                u2Protocol = OSIX_NTOHS (pSnapEthHdr->u2ProtocolType);
            }                    /* end of SNAP framing */

        }                        /* end of LLC framing */

    }

    if (u2Protocol == CFA_ENET_IPV4)
    {
        i2IpTotalLen = OSIX_NTOHS (*(INT2 *) (pu1DataBuf + u1EnetHeaderSize +
                                              IP_PKT_OFF_LEN));

        u2ExtraBytes = u4Len - (i2IpTotalLen + u1EnetHeaderSize);
    }
#ifdef IP6_WANTED
    else if (u2Protocol == CFA_ENET_IPV6)
    {
        i2IpTotalLen = OSIX_NTOHS (*(INT2 *) (pu1DataBuf + u1EnetHeaderSize +
                                              IPV6_OFF_PAYLOAD_LEN));
        u2ExtraBytes =
            u4Len - (i2IpTotalLen + u1EnetHeaderSize + IPV6_HEADER_LEN);
    }
#endif /*IP6_WANTED */

    u4PktSize = u4Len - u2ExtraBytes;

    if ((pBuf = CRU_BUF_Allocate_MsgBufChain ((u4PktSize +
                                               CFA_MAX_FRAME_HEADER_SIZE),
                                              CFA_MAX_FRAME_HEADER_SIZE))
        == NULL)
    {
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        CFA_DBG (ALL_FAILURE_TRC,
                 "CfaRcvPktFromMux: CRU Buffer Allocation - FAILURE!\n");
        return FALSE;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, pu1DataBuf, 0,
                                   u4PktSize) != CRU_SUCCESS)
    {
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CFA_DBG (ALL_FAILURE_TRC,
                 "CfaRcvPktFromMux: CRU Buffer Copy BufChain - FAILURE!\n");
        return FALSE;
    }

    /* Free the Net Buffer and Linear Buffer as CRU_BUFFER is
     * ready at this point of time 
     */
    netMblkClChainFree (pMuxBuf);
    MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1DataBuf);

    CFA_SET_IFINDEX (pBuf, u4IfIndex);

    if (OsixQueSend (CFA_PACKET_MUX_QID, (UINT1 *) &pBuf,
                     OSIX_DEF_MSG_LEN) == OSIX_SUCCESS)

    {
        /* send the event to the CFA task */
        OsixEvtSend (CFA_TASK_ID, CFA_GDD_INTERRUPT_EVENT);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CFA_DBG (ALL_FAILURE_TRC,
                 "CfaRcvPktFromMux: Enqueuing to CFA Q - FAILURE!\n");
        return FALSE;
    }

    return TRUE;
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddMuxOpen (UINT4 u4IfIndex)
{
    INT4                i4Type = MUX_PROTO_OUTPUT;

    if (CFA_GDD_BIND_COOKIE_DESC (u4IfIndex) != CFA_INVALID_DESC)
    {
        UtlTrcPrint
            ("CfaGddMuxOpen ... Valid CFA_GDD_BIND_COOKIE_DESC Found!\n");
        return (CFA_SUCCESS);
    }

    au4Spare[u4IfIndex - 1] = (int) u4IfIndex;

    i4Type = MUX_PROTO_PROMISC;

    if (((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex) =
         muxBind (CFA_GDD_PORT_NAME (u4IfIndex), CFA_GDD_PORT (u4IfIndex),
                  (FUNCPTR) CfaRcvPktFromMux, CfaGddShutdownRtn,
                  CfaGddRestartRtn, CfaGddErrorRtn, i4Type, "MuxCfa",
                  (VOID *) &au4Spare[u4IfIndex - 1])) == NULL)
    {
        perror ("#2 Error in binding port :\n");
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddMuxClose (UINT4 u4IfIndex)
{
    if (CFA_GDD_BIND_COOKIE_DESC (u4IfIndex) == CFA_INVALID_DESC)
    {
        UtlTrcPrint
            ("CfaGddMuxClose... invalid CFA_GDD_BIND_COOKIE_DESC Found!\n");
        return (CFA_SUCCESS);
    }

    if (muxUnbind
        ((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex), MUX_PROTO_PROMISC,
         CfaRcvPktFromMux) != MUX_SUCCESS)
    {
        perror ("Error in Unbinding the device for PROTO_SNARF\n");
        return (CFA_FAILURE);
    }

    au4Spare[u4IfIndex - 1] = 0;

    CFA_GDD_BIND_COOKIE_DESC (u4IfIndex) = CFA_INVALID_DESC;

    return (CFA_SUCCESS);
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddMuxRead (UINT1 *pu1Buf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    /* This function is not required as the packets are received in interrupt mode 
     * If Polling mode is used this function should call muxPollReceive to capture
     * the packet from the driver */
    return (CFA_SUCCESS);
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddMuxWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    M_BLK_ID            pNetBuf = NULL;

    CFA_DBG (CFA_DRIVER_TRC, " Entering CfaGddMuxWrite \n");

    if (u4IfIndex == 0)
    {
        CFA_DBG (ALL_FAILURE_TRC,
                 "u4IfIndex is zero - CfaGddMuxWrite Failure!\n");
        return CFA_FAILURE;
    }

    if ((pNetBuf = CfaGddNetMblkFromBufCopy (pu1DataBuf, u4IfIndex, u4PktSize))
        == NULL)
    {
        UtlTrcPrint ("-E- CfaGddMuxWrite: CfaGddNetMblkFromBufCopy Failure!\n");
        return CFA_FAILURE;
    }

    if (muxSend ((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex), pNetBuf) !=
        MUX_SUCCESS)
    {
        perror ("Error while Sending. Device not found(ENETDOWN).\n");
        netMblkClChainFree (pNetBuf);
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

M_BLK_ID
CfaGddNetMblkFromBufCopy (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    NET_POOL_ID         pNetPool;
    END_OBJ            *pEnd;
    M_BLK_ID            pMbuf = NULL;
    M_BLK_ID            pNextMbuf = NULL;
    M_BLK_ID            pCurMbuf = NULL;
    UINT4               u4ThisPktLen;
    UINT1               u1FirstCopy;

    /* Reference Pointer to the linear Buffer,
     * as it's required to move the pointer,
     * we need to have a reference pointer, otherwise,
     * the caller cann't free the linear buffer from where it's
     * originated
     */
    UINT1              *pu1Data = pu1DataBuf;

    /* Get the Device (END_OBJ) Structure 
     * for retrieval of the Driver Net Pool
     * to construct mBlk-clBlk Net buffer
     */
    pEnd =
        endFindByName (CFA_GDD_PORT_NAME (u4IfIndex), CFA_GDD_PORT (u4IfIndex));

    /* When there is no such device OR
     * the Pkt size is less than or equal
     * to zero, then no need to construct
     * a Net buffer
     */
    if (pEnd == NULL || u4PktSize <= 0)
    {
        return NULL;
    }

    pNetPool = pEnd->pNetPool;

    /* Get the MBLK-CL Construct from the NetPool 
     * It's assumed that enough buffer will always
     * get allocated when the call to netTupleGet
     * is made, if not available, this function wil
     * return failure, so in such case, we won't handoff
     * the packet, simply discarded
     */
    pMbuf = netTupleGet (pNetPool, pNetPool->clSizeMax, M_DONTWAIT,
                         MT_DATA, TRUE);

    if (pMbuf == NULL)
    {
        UtlTrcPrint
            ("-E- CfaGddNetMblkFromBufCopy - pMbuf - netTupleGet failed to allocate\n");
        return NULL;
    }

    /* Some Physical devices demands us to reserve
     * space for stuffing FCS
     */
    if ((u4PktSize + FCS_EXTRA_BYTES) < pNetPool->clSizeMax)
    {
        MEMCPY (pMbuf->mBlkHdr.mData, pu1Data, u4PktSize);
        pMbuf->mBlkHdr.mLen = u4PktSize;
    }
    else
    {
        /* For the very first time, no need
         * to allocate mBuf, as it's already done.
         * So to keep track this, u1FirstCopy is set 
         */
        u1FirstCopy = 1;
        while (u4PktSize > 0)
        {
            /* Determine the length of the current pkt
             * At any point of time, the Pkt can hold 
             * only Maximum of the Cluster Size Bytes.
             * 
             * Some Physical devices demands us to reserve
             * space for stuffing FCS, so 4 bytes were
             * reserved.
             */
            u4ThisPktLen = (u4PktSize + FCS_EXTRA_BYTES) < pNetPool->clSizeMax ?
                u4PktSize : (pNetPool->clSizeMax - FCS_EXTRA_BYTES);

            if (u1FirstCopy)
            {
                MEMCPY (pMbuf->mBlkHdr.mData, pu1Data, u4ThisPktLen);
                pMbuf->mBlkHdr.mLen = u4ThisPktLen;
                /* Reset this flag  so that next
                 * time onwards, it can allocate new mBuf
                 * for copying the remaining data
                 */
                u1FirstCopy = 0;
                /* Set Reference to pMbuf for further chaining */
                pCurMbuf = pMbuf;
            }
            else
            {
                /* Get the MBLK-CL Construct from the NetPool 
                 * It's assumed that enough buffer will always
                 * get allocated when the call to netTupleGet
                 * is made, if not available, this function wil
                 * return failure, so in such case, we won't handoff
                 * the packet, simply discarded
                 */
                pNextMbuf = NULL;
                pNextMbuf =
                    netTupleGet (pNetPool, pNetPool->clSizeMax, M_DONTWAIT,
                                 MT_DATA, TRUE);
                if (pNextMbuf == NULL)
                {
                    UtlTrcPrint
                        ("-E- CfaGddNetMblkFromBufCopy - pNextMbuf - netTupleGet failed to allocate\n");
                    netMblkClChainFree (pMbuf);
                    return NULL;
                }
                MEMCPY (pNextMbuf->mBlkHdr.mData, pu1Data, u4ThisPktLen);
                pNextMbuf->mBlkHdr.mLen = u4ThisPktLen;

                /* Form the Chain */
                pCurMbuf->mBlkHdr.mNext = pNextMbuf;

                /* Update pu1Data (linear Buffer), 
                 * PktSize, pCurMbuf
                 */
                pCurMbuf = pNextMbuf;
            }

            /*
             * Update u4PktSize and Move pointer by u4ThisPktLen
             * for pu1Data for successive iterations
             */
            u4PktSize -= u4ThisPktLen;
            pu1Data += u4ThisPktLen;
        }                        /* more bytes remaining to copy */
    }

    /* Calculate the Total Length of the Chained
     * mBuffer
     */
    pNextMbuf = pMbuf;
    pMbuf->mBlkPktHdr.len = 0;
    while (pNextMbuf != NULL)
    {
        pMbuf->mBlkPktHdr.len += pNextMbuf->mBlkHdr.mLen;
        pNextMbuf = pNextMbuf->mBlkHdr.mNext;
    }                            /* Traverse thru' mBuf till end to calculate the Total Pkt Len */

    pMbuf->mBlkHdr.mFlags |= M_PKTHDR;

    return pMbuf;
}                                /* CfaGddNetMblkFromBufCopy */

/*-----------------------------------------------------------------------*/
STATUS
CfaGddShutdownRtn (VOID *pCookie, VOID *pSpare)
{

    return MUX_SUCCESS;

}

/*-----------------------------------------------------------------------*/
STATUS
CfaGddRestartRtn (VOID *pCookie, VOID *pSpare)
{
    return MUX_SUCCESS;

}

/*-----------------------------------------------------------------------*/
VOID
CfaGddErrorRtn (END_OBJ * pEnd, END_ERR * pErr, VOID *pSpare)
{
    return;
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddGetHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{

    UINT1               au1Addr[CFA_ENET_ADDR_LEN];
    UINT2               u4IfIndex;

    u4IfIndex = u4IfIndex;

    if (u4IfIndex == 0)
    {
        CFA_DBG (ALL_FAILURE_TRC,
                 "CfaGddGetHwAddr: Unknown Interface Index \n");
        return CFA_FAILURE;
    }

    if (CFA_GDD_BIND_COOKIE_DESC (u4IfIndex) == CFA_INVALID_DESC)
    {
        if (!CFA_ENET_IWF_LOCAL_MAC (u4IfIndex))
        {
            CFA_DBG (ALL_FAILURE_TRC,
                     "CfaGddGetHwAddr: Interface Not yet initialized \n");
            return (CFA_FAILURE);
        }
        MEMCPY (au1HwAddr, CFA_ENET_IWF_LOCAL_MAC (u4IfIndex),
                CFA_ENET_ADDR_LEN);
        return (CFA_SUCCESS);
    }

    if (muxIoctl ((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex),
                  EIOCGADDR, au1Addr) != MUX_SUCCESS)
    {
        perror (" Unable to Get the Hardware Address \n");
        return (CFA_FAILURE);
    }

    MEMCPY ((VOID *) au1HwAddr, (VOID *) au1Addr, CFA_ENET_ADDR_LEN);

    return (CFA_SUCCESS);
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddMuxDetachVxIp (INT4 i4UnitNo, UINT1 *pDevName)
{
    if (ipDetach (i4UnitNo, pDevName) != MUX_SUCCESS)
    {
        perror ("Error in unbinding the device name");
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);
}

/*-----------------------------------------------------------------------*/
VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{

    if (CfaGddMuxBind (u4IfIndex) != CFA_SUCCESS)
        return (CFA_FAILURE);
    if (CfaGddGetHwAddr (u4IfIndex, au1HwAddr) != CFA_SUCCESS)
    {
        CFA_DBG (ALL_FAILURE_TRC, "CfaGddGetHwAddr: FAIL\n");
    }

    return (CFA_SUCCESS);
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4               u4PktSize;
    UINT2               u4IfIndex;

    while (OsixQueRecv (CFA_PACKET_MUX_QID, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        u4IfIndex = CFA_GET_IFINDEX (pBuf);
        if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex))
            (pBuf, u4IfIndex, u4PktSize, CFA_IF_IFTYPE (u4IfIndex),
             CFA_ENCAP_NONE) != CFA_SUCCESS)
        {
            UtlTrcPrint ("-E- Pkt Discarded by HL\n");
            /* release buffer which were not successfully sent to higher layer */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);
            return (CFA_FAILURE);
        }
    }
    return (CFA_SUCCESS);
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddMuxBind (UINT4 u4IfIndex)
{
    INT4                i4Type = MUX_PROTO_PROMISC;
    UINT1               u1AliasLen;
    UINT1               u1Card;
    UINT1               u1Index;
    UINT1               au1CardNum[CFA_MAX_PORT_NAME_LENGTH];

    u1AliasLen = STRLEN (CFA_GDD_PORT_NAME (u4IfIndex));

    for (u1Index = 0; u1Index <= u1AliasLen; u1Index++)
    {
        u1Card = CFA_GDD_PORT_NAME (u4IfIndex)[u1Index];
        if (u1Card >= '0' && u1Card <= '9')
            break;
    }

    STRCPY (au1CardNum, &CFA_GDD_PORT_NAME (u4IfIndex)[u1Index]);

    CFA_GDD_PORT (u4IfIndex) = u1Card = atoi (au1CardNum);

    CFA_GDD_PORT_NAME (u4IfIndex)[u1Index] = '\0';

    if (CfaGddMuxIsVxIpAttached
        (CFA_GDD_PORT_NAME (u4IfIndex), CFA_GDD_PORT (u4IfIndex)) == CFA_TRUE)
    {
        if (CfaGddMuxDetachVxIp
            (CFA_GDD_PORT (u4IfIndex),
             CFA_GDD_PORT_NAME (u4IfIndex)) != CFA_SUCCESS)
        {
            perror ("Error in unbinding tcp/ip stack on EndOne");
            return (CFA_FAILURE);
        }
    }

    au4Spare[u4IfIndex - 1] = (int) u4IfIndex;

    if (((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex) =
         muxBind (CFA_GDD_PORT_NAME (u4IfIndex), CFA_GDD_PORT (u4IfIndex),
                  (FUNCPTR) CfaRcvPktFromMux, NULL, NULL, NULL,
                  i4Type, "MuxCfa", (VOID *) &au4Spare[u4IfIndex - 1])) == NULL)
    {
        perror ("#4 Error in binding port :\n");
        return CFA_FAILURE;
    }

    CfaGddConfigPort (u4IfIndex, CFA_ENET_EN_PROMISC, NULL);

    return (CFA_SUCCESS);
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddMuxIsVxIpAttached (UINT1 *pu1PortName, UINT4 u4IfIndex)
{
    END_OBJ            *pEndObject = NULL;

    NET_PROTOCOL       *pProtoType;

    pEndObject = (END_OBJ *) endFindByName (pu1PortName, u4IfIndex);

    if (pEndObject == NULL)
        return CFA_FALSE;

    for (pProtoType = (NET_PROTOCOL *) lstFirst (&pEndObject->protocols);
         pProtoType != NULL;
         pProtoType = (NET_PROTOCOL *) lstNext (&pProtoType->node))
    {
        if (MEMCMP (pProtoType->name, "IP 4.4 ARP", 10) == 0 ||
            MEMCMP (pProtoType->name, "IP 4.4 TCP/IP", 13) == 0)
        {
            return CFA_TRUE;
        }
    }
    return CFA_FALSE;
}

/*-----------------------------------------------------------------------*/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{

    INT4                i4RetVal = CFA_SUCCESS;
    UINT4               u4flags;

    CFA_DBG2 (CFA_DRIVER_TRC,
              " CfaGddConfigPort for interface %d and config %d.\n",
              u4IfIndex, u1ConfigOption);

    /* check if the IfIndex is valid */
    if ((u4IfIndex > SYS_DEF_MAX_INTERFACES) || (u4IfIndex < 1))
    {

        CFA_DBG1 (ALL_FAILURE_TRC,
                  "Error in CfaGddConfigPort - "
                  "Interface %d not Valid - FAILURE.\n", u4IfIndex);

        return (CFA_FAILURE);
    }

    if ((!CFA_IF_ENTRY_USED (u4IfIndex)) || (!CFA_GDD_ENTRY_USED (u4IfIndex)))
    {
        CFA_DBG1 (ALL_FAILURE_TRC,
                  "Error in CfaGddConfigPort - un-United interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

/* dont configure for ports which are not registered - we dont have
socket/file descriptors for them */
    if (CFA_GDD_REGSTAT (u4IfIndex) == CFA_INVALID)
    {

        CFA_DBG1 (ALL_FAILURE_TRC,
                  "Error in CfaGddConfigPort - "
                  "Interface %d not Registered - FAILURE.\n", u4IfIndex);
        return (CFA_FAILURE);
    }

    switch (u1ConfigOption)
    {

        case CFA_ENET_EN_PROMISC:
            if (muxIoctl ((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex),
                          EIOCGFLAGS, &u4flags) != MUX_SUCCESS)
            {
                perror ("Error in getting interface flags\n");
                return (MUX_FAILURE);
            }
            /* set the IFF_PROMISC flag to enter promiscuous mode and
               set ethX interface flags
             */
            u4flags = u4flags | IFF_PROMISC;
            if (muxIoctl ((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex),
                          EIOCSFLAGS, u4flags) != MUX_SUCCESS)
            {
                perror ("Error in setting promisc mode\n");
                return (MUX_FAILURE);
            }
            else
            {
                CFA_IF_PROMISC (u4IfIndex) = CFA_ENABLED;
            }
            break;

        case CFA_ENET_DIS_PROMIS:

            break;

/* it is assumed that the multicast mode is enabled on the Ethernet
interfaces - this is the default setting */
        case CFA_ENET_ADD_MCAST:
/* add the address to the Enet port's multicast address list */
            if (muxMCastAddrAdd
                ((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex),
                 pConfigParam) != MUX_SUCCESS)
                i4RetVal = CFA_FAILURE;

            break;

        case CFA_ENET_DEL_MCAST:
/* delete the address from the Enet port's multicast address list */
            if (muxMCastAddrDel
                ((INT4 *) CFA_GDD_BIND_COOKIE_DESC (u4IfIndex),
                 pConfigParam) != MUX_SUCCESS)
                i4RetVal = CFA_FAILURE;

            break;

        case CFA_ENET_ADD_ALL_MCAST:
/* receive all the mcast frames on the specified port */
/* get ethX interface flags */

            break;

        case CFA_ENET_DEL_ALL_MCAST:
/* stop the receiving of all the mcast frames on the specified port */
/* get ethX interface flags */

            break;

        default:
            i4RetVal = CFA_FAILURE;
            break;
    }

    if (i4RetVal == CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_DRIVER_TRC,
                  "In CfaGddConfigPort - Configured interface %d - SUCCESS.\n",
                  u4IfIndex);
    }
    else
    {
        CFA_DBG1 (ALL_FAILURE_TRC,
                  "In CfaGddConfigPort - Configuration of "
                  "interface %d - FAILURE.\n", u4IfIndex);
    }

    return (i4RetVal);
}

INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    CFA_GDD_TYPE (u4IfIndex) = CFA_IF_IFTYPE (u4IfIndex);
    return (CFA_SUCCESS);
}
#else
VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);
    UNUSED_PARAM (pConfigParam);
    return (CFA_SUCCESS);
}

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (au1HwAddr);
    return (CFA_SUCCESS);
}

UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_IF_UP;
}

PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    return (CFA_SUCCESS);
}

INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return (CFA_SUCCESS);
}

INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
    return CFA_SUCCESS;
}

INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);
    return CFA_SUCCESS;
}
/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *					        be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen, 
		       UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}
#endif

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaUtilTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId, bBcast,
                                                &u4OutPort, &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}
