/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: cfatnlmgr.c,v 1.48 2018/01/22 10:27:12 siva Exp $
*
* Description: Tunnel manager implementation. 
*********************************************************************/
#include "cfainc.h"
#include "cfatnlmgr.h"
#include "fssocket.h"
/* Tunnel If Table */
tCfaTnlGblInfo      gCfaTnlGblInfo;
/* MemPool for TunlTbl */
tMemPoolId          CfaTnlCxtMemPoolId;

#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
/* Socket Id for ISATAP, 6to4, IPv6Ip and Auto Compat tunnels */
INT4                gi4RawSockId = -1;

/* Socket Id for GRE tunnels */
INT4                gi4GreRawSockId = -1;
#endif

tOsixSemId          gCfaTnlSemId;

/******************************************************************************
 * Function           : CfaInitTnlTable
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS
 * Action             : Routine to create a tunnel table (SLL).       
 ******************************************************************************/
INT4
CfaInitTnlTable (VOID)
{
    tVcmRegInfo         VcmRegInfo;

    gCfaTnlGblInfo.CfaTnlCxtMemPoolId =
        CFAMemPoolIds[MAX_CFA_TUNL_CONTEXT_SIZING_ID];
    gCfaTnlGblInfo.CfaTnlMemPoolId =
        CFAMemPoolIds[MAX_CFA_TUNL_INTERFACES_SIZING_ID];

    if (OsixCreateSem (CFA_TNL_SEMAPHORE, 1, 0, &gCfaTnlSemId) != OSIX_SUCCESS)
    {
        return CFA_FAILURE;
    }

#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
    if (CfaTnlCreateRawSock () != CFA_SUCCESS)
    {
        return (CFA_FAILURE);
    }

    if (CfaSetTnlRawSockOptions () != CFA_SUCCESS)
    {
        CfaTnlCloseRawSock ();
        return (CFA_FAILURE);
    }

    SelAddFd (gi4RawSockId, CfaNotifyTnlTask);

    SelAddFd (gi4GreRawSockId, CfaNotifyGreTnlTask);
#endif

    /* Register With VCM Module */
    MEMSET ((UINT1 *) &VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = CfaVcmCallbackFn;
    VcmRegInfo.u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
    VcmRegInfo.u1ProtoId = CFA_PROTOCOL_ID;
    VcmRegisterHLProtocol (&VcmRegInfo);

    if (CfaTnlMgrRegisterLL () == CFA_FAILURE)
    {
        CFA_DBG (ALL_FAILURE_TRC | OS_RESOURCE_TRC, CFA_TUNL,
                 "Tunnel Registration with LL  failed\r\n");
        return CFA_FAILURE;
    }
    /* Create Default context for Tunl entry */
    CfaTunlCreateContext (VCM_DEFAULT_CONTEXT);
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaDeInitTnlTable
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS
 * Action             : Routine to de-init the tunnel table (SLL).       
 ******************************************************************************/
INT4
CfaDeInitTnlTable (VOID)
{
    /* Delete Default context for Tunl entry */
    CfaTunlDeleteContext (VCM_DEFAULT_CONTEXT);

    if (gCfaTnlSemId != 0)
    {
        OsixSemDel (gCfaTnlSemId);
        gCfaTnlSemId = 0;
    }
#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
    CfaTnlCloseRawSock ();
#endif

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaTnlMgrRegWithIp4
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine is used to register Tunnel Manager with 
 *                      IP. CfaTnlMgrRxFrame is a call-back function from IP
 *                      to tunnel manager. This routine has to be ported when
 *                      interfacing with third party IP.      
 ******************************************************************************/
INT4
CfaTnlMgrRegWithIp4InCxt (UINT4 u4ContextId)
{
    INT4                i4RetVal = CFA_SUCCESS;
#ifdef IP_WANTED
    tNetIpRegInfo       NetIpRegInfo;

    MEMSET (&NetIpRegInfo, 0, sizeof (tNetIpRegInfo));
    /* copy the Vrf Id */
    NetIpRegInfo.u4ContextId = u4ContextId;
    /* Copy the callback functions for packet Rx and Rt chnage indication */
    NetIpRegInfo.u2InfoMask |= (NETIPV4_ROUTECHG_REQ | NETIPV4_PROTO_PKT_REQ);
    NetIpRegInfo.pRtChng = CfaTnlMgrRtChgHdlr;
    NetIpRegInfo.pProtoPktRecv = CfaTnlMgrRxFrame;
    /* Register with IP for TNL_IPV6IP_PROTO */
    NetIpRegInfo.u1ProtoId = TNL_IPV6IP_PROTO;
    NetIpv4RegisterHigherLayerProtocol (&NetIpRegInfo);

    /* Register with IP for TNL_GRE_PROTO */
    MEMSET (&NetIpRegInfo, 0, sizeof (tNetIpRegInfo));
    /* copy the Vrf Id */
    NetIpRegInfo.u4ContextId = u4ContextId;
    /* Copy the callback functions for packet Rx and Rt chnage indication */
    NetIpRegInfo.u2InfoMask |= (NETIPV4_ROUTECHG_REQ | NETIPV4_PROTO_PKT_REQ);
    NetIpRegInfo.pRtChng = CfaTnlMgrRtChgHdlr;
    NetIpRegInfo.pProtoPktRecv = CfaTnlMgrRxFrame;
    NetIpRegInfo.u1ProtoId = TNL_GRE_PROTO;
    NetIpv4RegisterHigherLayerProtocol (&NetIpRegInfo);
#else
    UNUSED_PARAM (u4ContextId);
#endif

    return (i4RetVal);
}

/******************************************************************************
 * Function           : CfaTnlMgrRxFrame
 * Input(s)           : pBuf - CRU Buf received, 
 *                      u2Len - Length of the rcvd buf.
 *                      u4IfIndex - IfIndex over which the pkt is rcvd
 *                      IfaceId - Interface struct 
 *                      u1Flag - Flag (unused) 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This is the call-back function registered to 
 *                      receive packets from Ipv4. 
 ******************************************************************************/
VOID
CfaTnlMgrRxFrame (tIP_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
                  UINT4 u4IfIndex, tIP_INTERFACE IfaceId, UINT1 u1Flag)
{
    UINT4               u4ByteCount;
    tTnlIpAddr          TnlSrcAddr;
    tTnlIpAddr          TnlDestAddr;
    UINT4               u4GreHdrLen;
    UINT4               u4ContextId = 0;
#ifdef IP_WANTED
    UINT4               u4CfaIfIndex;
#endif
    UINT2               u2GreProtoType;
    UINT2               u2GreHdrFlag = 0;
    tCfaRegInfo         CfaInfo;
    UINT1              *pu1FirstValidByte = NULL;
    UINT1               u1OperStatus;
    INT4                i4IpHdrLen;
    INT4                i4RetVal = CFA_SUCCESS;
    INT4                i4ReturnValue = CFA_SUCCESS;

    tTnlIfEntry         TnlEntry;
    tTnlIfEntry        *pTnlEntry = NULL;

    UNUSED_PARAM (IfaceId);
    UNUSED_PARAM (u1Flag);

    if (VcmGetContextInfoFromIpIfIndex (u4IfIndex, &u4ContextId) == VCM_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }
    /* Assumption: Pkt rcvd from IPv4 has IP header */

    /* Get the Src IP addr from the rcvd pkt */
    CRU_BUF_Copy_FromBufChain (pBuf,
                               (UINT1 *) &(TnlSrcAddr.Ip4TnlAddr),
                               IP_PKT_OFF_SRC, sizeof (UINT4));
    TnlSrcAddr.Ip4TnlAddr = OSIX_NTOHL (TnlSrcAddr.Ip4TnlAddr);

    /* Get the Dest IP addr from the rcvd pkt */
    CRU_BUF_Copy_FromBufChain (pBuf,
                               (UINT1 *) &(TnlDestAddr.Ip4TnlAddr),
                               IP_PKT_OFF_DEST, sizeof (UINT4));
    TnlDestAddr.Ip4TnlAddr = OSIX_NTOHL (TnlDestAddr.Ip4TnlAddr);

    /* Scan the TnlIfTbl to find the Src & Dest Ip matching entry. The
     * Dest Address in received pkt will match against the configured Tunnel
     * source address and viceversa */
    pTnlEntry = &TnlEntry;
    i4ReturnValue = CfaTnlFindEntryInCxt (u4ContextId,
                                          ADDR_TYPE_IPV4, &TnlDestAddr,
                                          &TnlSrcAddr, TNL_INCOMING, pTnlEntry);

    if (i4ReturnValue == CFA_SUCCESS)
    {
        CfaGetIfOperStatus (pTnlEntry->u4IfIndex, &u1OperStatus);
    }
    if ((i4ReturnValue == CFA_FAILURE) || (u1OperStatus == CFA_IF_DOWN))
    {
        /* No entry in TnlIfTbl with matching Src & Dest Ip addr. 
         * Search for matching Configured Source Address (destination in
         * received buffer) alone */
        pTnlEntry = &TnlEntry;
        i4ReturnValue =
            CfaTnlFindEntryInCxt (u4ContextId, ADDR_TYPE_IPV4, &TnlDestAddr,
                                  NULL, TNL_INCOMING, pTnlEntry);

        if (i4ReturnValue == CFA_SUCCESS)
        {
            CfaGetIfOperStatus (pTnlEntry->u4IfIndex, &u1OperStatus);
        }

        if ((i4ReturnValue == CFA_FAILURE) || (u1OperStatus == CFA_IF_DOWN))
        {
            /* No SrcIp matching entry. Pkt Dropped... */

            /* Get the CFA If Index from the given Ip Interface Index */
#ifdef IP_WANTED
            if (NetIpv4GetCfaIfIndexFromPort (u4IfIndex, &u4CfaIfIndex) ==
                NETIPV4_SUCCESS)
            {
                CFA_IF_SET_IN_ERR ((UINT2) u4CfaIfIndex);
            }
#endif

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return;
        }
    }

    /* Incr stats of TnlIf */
    u4ByteCount = CRU_BUF_Get_ChainValidByteCount (pBuf);
    CFA_IF_SET_IN_OCTETS ((UINT2) pTnlEntry->u4IfIndex, u4ByteCount);

    if ((pTnlEntry->u1DirFlag == TNL_UNIDIRECTIONAL) &&
        (pTnlEntry->u1Direction == TNL_OUTGOING))
    {
        /* Drop the pkt... */
        CFA_IF_SET_IN_ERR ((UINT2) pTnlEntry->u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    /* Get the first byte of pBuf. i.e., IP version & Header length */
    pu1FirstValidByte = CRU_BUF_GetDataPtr (CRU_BUF_GetFirstDataDesc (pBuf));
    i4IpHdrLen = (((*pu1FirstValidByte) & 0x0F) * 4);

    /* Strip off IP header */
    if (CRU_BUF_Move_ValidOffset (pBuf, (UINT4) i4IpHdrLen) != CRU_SUCCESS)
    {
        CFA_IF_SET_IN_ERR ((UINT2) pTnlEntry->u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (pTnlEntry->u4EncapsMethod == TNL_TYPE_GRE)
    {
        /* Get the first byte of GRE Header i.e - Flags for checksum etc. */
        pu1FirstValidByte =
            CRU_BUF_GetDataPtr (CRU_BUF_GetFirstDataDesc (pBuf));
        PTR_FETCH2 (u2GreHdrFlag, pu1FirstValidByte);
        if (pTnlEntry->bCksumFlag == TNL_CKSUM_ENABLE)
        {
            if ((u2GreHdrFlag & GRE_SET_CKSUM_FLAG) != GRE_SET_CKSUM_FLAG)
            {
                /* Checksum not present in Received Packet. Drop it. */
                CFA_IF_SET_IN_ERR ((UINT2) pTnlEntry->u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return;
            }
            u4GreHdrLen = MAX_GRE_HDR_LEN;

            /* Validate GRE checksum */
            if (OSIX_HTONS (UtlIpCSumCruBuf (pBuf, (UINT4) (u2Len), 0)) != 0)
            {
                /* GRE checksum error. Pkt dropped!!! */
                CFA_IF_SET_IN_ERR ((UINT2) pTnlEntry->u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return;
            }
        }
        else
        {
            if ((u2GreHdrFlag & GRE_SET_CKSUM_FLAG) == GRE_SET_CKSUM_FLAG)
            {
                /* Checksum present in Received Packet. Drop it. */
                CFA_IF_SET_IN_ERR ((UINT2) pTnlEntry->u4IfIndex);
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return;
            }
            u4GreHdrLen = MIN_GRE_HDR_LEN;
        }

        /* Get the Protocol type from GRE hdr */
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   (UINT1 *) &u2GreProtoType,
                                   GRE_PROTO_TYPE_OFFSET,
                                   sizeof (u2GreProtoType));

        /* convert u2ProtoType to host byte order as Ipv4 sends 
         * it in network byte order */
        u2GreProtoType = OSIX_NTOHS (u2GreProtoType);

        /* Strip off GRE header */
        if (CRU_BUF_Move_ValidOffset (pBuf, u4GreHdrLen) != CRU_SUCCESS)
        {
            CFA_IF_SET_IN_ERR ((UINT2) pTnlEntry->u4IfIndex);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return;
        }
    }

    switch (pTnlEntry->u4EncapsMethod)
    {
        case TNL_TYPE_GRE:
            switch (u2GreProtoType)
            {
                case CFA_ENET_IPV6:
                case CFA_ENET_IPV4:
                    break;

                default:
                    /* Protocol type IPV6 alone is supported now */
                    /* No matching GRE ProtoType */
                    i4RetVal = CFA_FAILURE;
                    break;
            }
            break;
        case TNL_TYPE_IPV6IP:
        case TNL_TYPE_SIXTOFOUR:
        case TNL_TYPE_COMPAT:
        case TNL_TYPE_ISATAP:
            break;

        default:
            /* No matching tunnel encap type */
            i4RetVal = CFA_FAILURE;
            break;
    }

    if (i4RetVal == CFA_FAILURE)
    {
        i4RetVal = CFA_FAILURE;
        CFA_IF_SET_IN_ERR ((UINT2) pTnlEntry->u4IfIndex);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    else
    {
        /* Store Src & Dest Ip addr in ModuleData of CRU BUF */
        CRU_BUF_Set_U4Reserved1 (pBuf, TnlSrcAddr.Ip4TnlAddr);
        CRU_BUF_Set_U4Reserved2 (pBuf, TnlDestAddr.Ip4TnlAddr);

        CfaInfo.u4IfIndex = pTnlEntry->u4IfIndex;
        MEMSET (&(CfaInfo.CfaPktInfo), 0, sizeof (tCfaPktInfo));
        CfaInfo.CfaPktInfo.pBuf = pBuf;
        if ((TNL_TYPE_GRE == pTnlEntry->u4EncapsMethod) &&
            (CFA_ENET_IPV4 == u2GreProtoType))
        {
#ifdef IP_WANTED
            if (IpHandlePktFromCfa (pBuf,
                                    (UINT2) CFA_IF_IPPORT (CfaInfo.u4IfIndex),
                                    CfaInfo.CfaPktInfo.u1PktType) != IP_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ALL, CFA_FFM,
                         "Error in delivering the message to IP "
                         "Exiting CfaTnlMgrRxFrame \n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return;
            }
#endif
        }
        else
        {
            CfaNotifyIfPktRcvd (CFA_ENET_IPV6, &CfaInfo);
        }
        return;
    }
}

/******************************************************************************
 * Function           : CfaTnlEnqPktToIp
 * Input(s)           : pTnlPktInfo - struct fileld by CfaTnlMgrTxFrame  
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine enque the packet to Ipv4 
 ******************************************************************************/
INT4
CfaTnlEnqPktToIp (tTnlPktTxInfo * pTnlPktInfo)
{
    t_IP_SEND_PARMS    *pIpSendParams = NULL;
    INT4                i4RetVal = CFA_SUCCESS;

    pIpSendParams = (t_IP_SEND_PARMS *)
        IP_GET_MODULE_DATA_PTR (pTnlPktInfo->pBuf);

    if (pIpSendParams == NULL)
    {
        i4RetVal = CFA_FAILURE;
    }

    if (i4RetVal == CFA_SUCCESS)
    {
        /* Fill in the Ip hdr info */
        pIpSendParams->u1Cmd = IP_LAYER4_DATA;
        pIpSendParams->u1Tos = pTnlPktInfo->IpHdr.u1Tos;
        pIpSendParams->u2Len = pTnlPktInfo->IpHdr.u2Len;
        pIpSendParams->u2Id = pTnlPktInfo->IpHdr.u2Id;
        pIpSendParams->u1Df = pTnlPktInfo->IpHdr.u1Df;
        pIpSendParams->u1Olen = pTnlPktInfo->IpHdr.u1Olen;
        pIpSendParams->u1Ttl = pTnlPktInfo->IpHdr.u1Ttl;
        pIpSendParams->u1Proto = pTnlPktInfo->IpHdr.u1Proto;

        pIpSendParams->u2Port = (UINT2)
            CFA_IF_IPPORT ((UINT2) pTnlPktInfo->u4IfIndex);

        pIpSendParams->u4Src = pTnlPktInfo->TnlSrcAddr.Ip4TnlAddr;
        pIpSendParams->u4Dest = pTnlPktInfo->TnlDestAddr.Ip4TnlAddr;
        pIpSendParams->u4ContextId = pTnlPktInfo->u4ContextId;

#ifdef IP_WANTED
        IpEnquePktToIpFromHLWithCxtId (pTnlPktInfo->pBuf);
#endif /* IP_WANTED */
    }
    return (i4RetVal);
}

/******************************************************************************
 * Function           : CfaTnlMgrPmtuRegWithV4
 * Input(s)           : pTnlIfEntry - TnlEntry 
 * Output(s)          : None. 
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Each tunnel configured registers with V4 PmtuD for 
 *                      getting the Pmtu information from V4.
 ******************************************************************************/
INT4
CfaTnlMgrPmtuRegWithV4 (tTnlIfNode * pTnlIfEntry)
{
    INT4                i4TunlSd = 0;
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4PmtuFlag = IP_PMTUDISC_DO;
#endif
    /* FutureSLI expects i4PmtuFlag to be IPPMTUDISC_DO */
#ifdef SLI_WANTED
    INT4                i4PmtuFlag = IPPMTUDISC_DO;
    UINT1               u1Mode = SOCK_UNIQUE_MODE;
#endif
#if defined (BSDCOMP_SLI_WANTED) || defined (SLI_WANTED)
    struct sockaddr_in  DestAddr;

    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = OSIX_HTONL (pTnlIfEntry->RemoteAddr.Ip4TnlAddr);

    if ((i4TunlSd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0)
    {
        return CFA_FAILURE;
    }

    if (setsockopt
        (i4TunlSd, IPPROTO_IP, IP_MTU_DISCOVER, &i4PmtuFlag,
         sizeof (INT4)) != OSIX_SUCCESS)
    {
        close (i4TunlSd);
        return CFA_FAILURE;
    }

#ifdef SLI_WANTED
    /* Each tunnel interface will have separate socket */
    setsockopt (i4TunlSd, IPPROTO_IP, IP_PKT_RX_CXTID,
                (VOID *) &pTnlIfEntry->u4ContextId, sizeof (UINT4));

    setsockopt (i4TunlSd, IPPROTO_IP, IP_PKT_TX_CXTID,
                (VOID *) &pTnlIfEntry->u4ContextId, sizeof (UINT4));

    setsockopt (i4TunlSd, IPPROTO_IP, IP_SOCK_MODE,
                (VOID *) &u1Mode, sizeof (UINT1));

    if (SliRegisterPmtuHandler (i4TunlSd, CfaTnlMgrNotifyIpv4PMtuChgInCxt) !=
        OSIX_SUCCESS)
    {
        close (i4TunlSd);
        return CFA_FAILURE;
    }
#endif /* SLI_WANTED */

    if (((connect (i4TunlSd, (struct sockaddr *) &DestAddr,
                   sizeof (struct sockaddr)))) != OSIX_SUCCESS)
    {
        close (i4TunlSd);
        return CFA_FAILURE;
    }
#endif /* BSDCOMP_SLI_WANTED || SLI_WANTED */

    /* Update the Sock Desc value in the TnlTbl. */
    pTnlIfEntry->i4PathMtuSock = i4TunlSd;

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaTnlMgrPmtuDeregWithV4
 * Input(s)           : pTnlIfEntry - TnlEntry 
 * Output(s)          : None. 
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Tunnel configured de-registers with V4 PmtuD when 
 *                      Pmtu discovery is disabled on that interface.
 ******************************************************************************/
INT4
CfaTnlMgrPmtuDeregWithV4 (tTnlIfNode * pTnlIfEntry)
{
#if defined (BSDCOMP_SLI_WANTED) || defined (SLI_WANTED)
    close (pTnlIfEntry->i4PathMtuSock);
#endif /* BSDCOMP_SLI_WANTED || SLI_WANTED */

    pTnlIfEntry->i4PathMtuSock = CFA_INVALID_DESC;
    return CFA_SUCCESS;
}

 /******************************************************************************
 * Function           : CfaTnlCopyEntry
 * Input(s)           : pDestTnl - Dest TnlEntry to which the elements 
 *                                 have to be copied
 *                      pSrcTnl - Src Tnl from which the elements have
 *                                to be copied           
 * Output(s)          : None. 
 * Returns            : void 
 * Action             : This function copies the TnlEntry elements from
 *                      SrcTnl to DestTnl.
 ******************************************************************************/
VOID
CfaTnlCopyEntry (tTnlIfEntry * pDestTnl, tTnlIfNode * pSrcTnl)
{
    pDestTnl->u4ContextId = pSrcTnl->u4ContextId;
    pDestTnl->u4AddrType = pSrcTnl->u4AddrType;
    if (pDestTnl->u4AddrType == ADDR_TYPE_IPV4)
    {
        pDestTnl->LocalAddr.Ip4TnlAddr = pSrcTnl->LocalAddr.Ip4TnlAddr;
        pDestTnl->RemoteAddr.Ip4TnlAddr = pSrcTnl->RemoteAddr.Ip4TnlAddr;
    }
    else
    {
        MEMCPY (pDestTnl->LocalAddr.Ip6TnlAddr,
                pSrcTnl->LocalAddr.Ip6TnlAddr, IP6ADDR_SIZE);
        MEMCPY (pDestTnl->RemoteAddr.Ip6TnlAddr,
                pSrcTnl->RemoteAddr.Ip6TnlAddr, IP6ADDR_SIZE);
    }

    pDestTnl->u4EncapsMethod = pSrcTnl->u4EncapsMethod;
    pDestTnl->u4ConfigId = pSrcTnl->u4ConfigId;
    pDestTnl->u4Mtu = pSrcTnl->u4Mtu;
    pDestTnl->u4EncapLmt = pSrcTnl->u4EncapLmt;
    pDestTnl->u4PhyIfIndex = pSrcTnl->u4PhyIfIndex;
    pDestTnl->u4IfIndex = pSrcTnl->u4IfIndex;
    pDestTnl->i4FlowLabel = pSrcTnl->i4FlowLabel;
    pDestTnl->u2HopLimit = pSrcTnl->u2HopLimit;
    pDestTnl->i2TOS = pSrcTnl->i2TOS;
    pDestTnl->u1Security = pSrcTnl->u1Security;
    pDestTnl->u1DirFlag = pSrcTnl->u1DirFlag;
    pDestTnl->u1Direction = pSrcTnl->u1Direction;
    pDestTnl->u1EncapOption = pSrcTnl->u1EncapOption;
    pDestTnl->bCksumFlag = pSrcTnl->bCksumFlag;
    pDestTnl->bPmtuFlag = pSrcTnl->bPmtuFlag;

    return;
}

/******************************************************************************
 * Function           : CfaTnlMgrRegisterLL
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine is used to register Tunnel Manager with 
 *                      lower layer (CFA). This routine has to be ported when
 *                      interfacing with third party agent.      
 ******************************************************************************/
INT4
CfaTnlMgrRegisterLL (VOID)
{
    tCfaRegParams       CfaRegParams;
    INT4                i4RetVal = CFA_SUCCESS;

    CfaRegParams.u2LenOrType = 0xFFFF;
    CfaRegParams.u2RegMask = CFA_IF_DELETE | CFA_IF_UPDATE | CFA_IF_OPER_ST_CHG;
    CfaRegParams.pIfCreate = NULL;
    CfaRegParams.pIfDelete = CfaTnlPhyIfDelete;
    CfaRegParams.pIfUpdate = CfaTnlPhyIfUpdate;
    CfaRegParams.pTnlIfUpdate = NULL;
    CfaRegParams.pIfOperStChg = CfaTnlPhyIfOperStChg;
    CfaRegParams.pIfRcvPkt = NULL;
    if (CfaRegisterHL (&CfaRegParams) == CFA_FAILURE)
    {
        i4RetVal = CFA_FAILURE;
    }

    return (i4RetVal);
}

/******************************************************************************
 * Function           : CfaGetTnlNode
 * Input(s)           : u4AddrType
 *                      u4SrcIpAddr
 *                      u4DestIpAddr
 *                      pu1SrcIp6Addr
 *                      pu1DestIp6Addr
 *                      u4EncapsMethod
 *                      u4ConfigId 
 * Output(s)          : pointer to the tnlIfNode of TnlIfSLL
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This routine is used to get the TMO SLL pointer.
 *                      Caller should make sure of taking the tunnel lock.      
 ******************************************************************************/
tTnlIfNode         *
CfaGetTnlNodeInCxt (tCfaTnlCxt * pTunlCxt, UINT4 u4AddrType, UINT4 u4SrcIpAddr,
                    UINT4 u4DestIpAddr, UINT1 *pu1SrcIp6Addr,
                    UINT1 *pu1DestIp6Addr, UINT4 u4EncapsMethod,
                    UINT4 u4ConfigId)
{
    tTnlIfNode         *pTnlIfNode = NULL;

    TMO_SLL_Scan (&(pTunlCxt->TnlIfSLL), pTnlIfNode, tTnlIfNode *)
    {
        if ((pTnlIfNode->u4AddrType == u4AddrType) &&
            (pTnlIfNode->u4EncapsMethod == u4EncapsMethod) &&
            (pTnlIfNode->u4ConfigId == u4ConfigId) &&
            (pTnlIfNode->u4ContextId == pTunlCxt->u4ContextId))
        {
            if (u4AddrType == ADDR_TYPE_IPV4)
            {
                if ((pTnlIfNode->LocalAddr.Ip4TnlAddr == u4SrcIpAddr) &&
                    (pTnlIfNode->RemoteAddr.Ip4TnlAddr == u4DestIpAddr))
                {
                    break;
                }
            }
            else if (u4AddrType == ADDR_TYPE_IPV6)
            {
                if ((MEMCMP (&(pTnlIfNode->RemoteAddr.Ip6TnlAddr),
                             pu1SrcIp6Addr, IP6ADDR_SIZE) == 0) &&
                    (MEMCMP (&(pTnlIfNode->RemoteAddr.Ip6TnlAddr),
                             pu1DestIp6Addr, IP6ADDR_SIZE) == 0))
                {
                    break;
                }
            }
        }
    }

    return pTnlIfNode;
}

/*-------------------------------------------------------------------+
 *
 * Function           : CfaTnlUtilIsValidCxtId
 *
 * Input(s)           : u4TunlIfConfigID - Identifier of Tunnel
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : This function validates the ConfigId
 *
+-------------------------------------------------------------------*/
INT4
CfaTnlUtilIsValidCxtId (UINT4 u4TunlIfConfigID)
{
    if (u4TunlIfConfigID < CFA_MAX_TUNL_CONTEXT)
    {
        if (gCfaTnlGblInfo.apCfaTnlCxt[u4TunlIfConfigID] != NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/*-------------------------------------------------------------------+
 *
 * Function           : CfaUtilSetTnlCxt
 *
 * Input(s)           : u4ContextId - Context Identifier
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : This function will set Tunnel global context
 *                      pointer.
 *
+-------------------------------------------------------------------*/

INT4
CfaUtilSetTnlCxt (UINT4 u4ContextId)
{
    if (VcmIsL3VcExist (u4ContextId) != VCM_FALSE)
    {
        gCfaTnlGblInfo.pTunlGblCxt = CfaUtilGetCxtEntryFromCxtId (u4ContextId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : CfaUtilResetTnlCxt
 *
 * Input(s)           : None.
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action             : This function will reset Tunnel global context
 *                      pointer.
 *
+-------------------------------------------------------------------*/
VOID
CfaUtilResetTnlCxt (VOID)
{
    gCfaTnlGblInfo.pTunlGblCxt = NULL;
    return;
}

/*-------------------------------------------------------------------+
 *
 * Function           : CfaUtilGetCxtEntryFromCxtId
 *
 * Input(s)           : u4ContextId - Context Identifier
 *
 * Output(s)          : None.
 *
 * Returns            : pTunlCxt - Tunnel Context entry
 *
 * Action             : This function will return Tunnel Context entry 
 *                      of the given Context Identifier.
 *
+-------------------------------------------------------------------*/
tCfaTnlCxt         *
CfaUtilGetCxtEntryFromCxtId (UINT4 u4ContextId)
{
    tCfaTnlCxt         *pTunlCxt = NULL;
    if (u4ContextId >= (UINT4) CFA_MAX_TUNL_CONTEXT)
    {
        return NULL;
    }
    pTunlCxt = gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId];
    return pTunlCxt;
}

/*-------------------------------------------------------------------+
 *
 * Function           : CfaTnlUtilGetFirstCxtId
 *
 * Input(s)           : pu4FirstCxtId - Context Identifier
 *
 * Output(s)          : None.
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : This function will return first valid VRF id.
 *
+-------------------------------------------------------------------*/

INT4
CfaTnlUtilGetFirstCxtId (UINT4 *pu4FirstCxtId)
{
    UINT4               u4ContextId = 0;

    for (u4ContextId = 0; u4ContextId < CFA_MAX_TUNL_CONTEXT; u4ContextId++)
    {
        if (gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId] != NULL)
        {
            *pu4FirstCxtId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/*-------------------------------------------------------------------+
 *
 * Function           : CfaTnlUtilGetNextCxtId
 *
 * Input(s)           : u4ContextId - Context Identifier
 *
 * Output(s)          : pu4NextCxtId - Next valid Context Identifier
 *
 * Returns            : SNMP_SUCCESS/SNMP_FAILURE
 *
 * Action             : This function will return next valid VRF id.
 *
+-------------------------------------------------------------------*/
INT4
CfaTnlUtilGetNextCxtId (UINT4 u4ContextId, UINT4 *pu4NextCxtId)
{
    UINT4               u4CxtId = 0;

    u4ContextId++;
    for (u4CxtId = u4ContextId; u4CxtId < CFA_MAX_TUNL_CONTEXT; u4CxtId++)
    {
        if (gCfaTnlGblInfo.apCfaTnlCxt[u4CxtId] != NULL)
        {
            *pu4NextCxtId = u4CxtId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
/*-------------------------------------------------------------------+
 *
 * Function           : CfaNotifyTnlTask
 *
 * Input(s)           : i4SockFd - Sock desc
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action             : This function will notify the CFA task about the
 *                      reception of the Tunnel packet from Linux kernel IP
 *
+-------------------------------------------------------------------*/
VOID
CfaNotifyTnlTask (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    if (CFA_INITIALISED != TRUE)
    {
        return;
    }
    OsixEvtSend (CFA_TASK_ID, CFA_TUNL_PKT_RECV_EVENT);
}

/*-------------------------------------------------------------------+
 *
 * Function           : CfaNotifyGreTnlTask
 *
 * Input(s)           : i4SockFd - Sock desc
 *
 * Output(s)          : None.
 *
 * Returns            : None.
 *
 * Action             : This function will notify the CFA task about the
 *                      reception of the GRE Tunnel packet from Linux kernel IP
 *
+-------------------------------------------------------------------*/
VOID
CfaNotifyGreTnlTask (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    if (CFA_INITIALISED != TRUE)
    {
        return;
    }
    OsixEvtSend (CFA_TASK_ID, CFA_GRE_TUNL_PKT_RECV_EVENT);
}

/******************************************************************************
 * Function           : CfaTnlCreateRawSock
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This function creates raw sockets for tunnels..
 ******************************************************************************/
PUBLIC INT4
CfaTnlCreateRawSock (VOID)
{

    INT4                i4SockFd = 0;
    INT4                i4GreSockFd = 0;

    /* Socket Id for ISATAP, 6to4, IPv6Ip and Auto Compat tunnels */

    i4SockFd = socket (AF_INET, SOCK_RAW, TNL_IPV6IP_PROTO);

    if (i4SockFd < 0)
    {
        return CFA_FAILURE;
    }

    gi4RawSockId = i4SockFd;

    /* Socket Id for GRE tunnels */

    i4GreSockFd = socket (AF_INET, SOCK_RAW, TNL_GRE_PROTO);

    if (i4GreSockFd < 0)
    {
        return CFA_FAILURE;
    }

    gi4GreRawSockId = i4GreSockFd;

    return CFA_SUCCESS;

}

/******************************************************************************
 * Function           : CfaTnlCloseRawSock
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : VOID 
 * Action             : This function closes raw sockets for tunnels..
 ******************************************************************************/

PUBLIC VOID
CfaTnlCloseRawSock (VOID)
{
    SelRemoveFd (gi4RawSockId);
    close (gi4RawSockId);
    SelRemoveFd (gi4GreRawSockId);
    close (gi4GreRawSockId);
    gi4RawSockId = -1;
    gi4GreRawSockId = -1;
}

/******************************************************************************
 * Function           : CfaSetTnlRawSockOptions
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This function sets the raw socket options for tunnels.
 ******************************************************************************/
PUBLIC INT4
CfaSetTnlRawSockOptions (VOID)
{
    INT4                i4OptnVal = 1;

    if ((fcntl ((gi4RawSockId), F_SETFL, O_NONBLOCK)) < 0)
    {
        return CFA_FAILURE;
    }

    if (setsockopt (gi4RawSockId, IPPROTO_IP, IP_HDRINCL,
                    (&i4OptnVal), (sizeof (INT4))) < 0)
    {
        return CFA_FAILURE;
    }

    if ((fcntl ((gi4GreRawSockId), F_SETFL, O_NONBLOCK)) < 0)
    {
        return CFA_FAILURE;
    }

    if (setsockopt (gi4GreRawSockId, IPPROTO_IP, IP_HDRINCL,
                    (&i4OptnVal), (sizeof (INT4))) < 0)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;

}

/******************************************************************************
 * Function           : CfaTnlEnqPktToLinuxIp 
 * Input(s)           : pTnlPktInfo - struct fileld by CfaTnlMgrTxFrame
 * Output(s)          : None
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : This function fills the IP Header for the IPv6 tunnel 
 *                      packet and sends the packet to Linux IP
 ******************************************************************************/

PUBLIC INT4
CfaTnlEnqPktToLinuxIp (tTnlPktTxInfo * pTnlPktInfo, UINT4 u4EncapsMethod)
{
    t_IP_HEADER        *pIpHdr;
    UINT1              *pRawPkt;
    struct sockaddr_in  DestAddr;
    INT4                i4SendBytes;

    if ((pRawPkt = MemAllocMemBlk (gCfaPktPoolId)) == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pTnlPktInfo->pBuf, 0);
        return CFA_FAILURE;
    }

    pIpHdr = (t_IP_HEADER *) (VOID *) pRawPkt;

    pIpHdr->u1Ver_hdrlen = IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    pIpHdr->u1Tos = pTnlPktInfo->IpHdr.u1Tos;
    pIpHdr->u2Totlen = CRU_HTONS (pTnlPktInfo->IpHdr.u2Len + IP_HDR_LEN);
    pIpHdr->u2Id = pTnlPktInfo->IpHdr.u2Id;
    pIpHdr->u2Fl_offs = 0;
    pIpHdr->u1Ttl = IP_DEF_TTL;
    if (u4EncapsMethod == TNL_TYPE_GRE)
    {
        pIpHdr->u1Proto = TNL_GRE_PROTO;
    }
    else
    {
        pIpHdr->u1Proto = TNL_IPV6IP_PROTO;
    }
    pIpHdr->u2Cksum = 0;

    pIpHdr->u4Src = OSIX_HTONL (pTnlPktInfo->TnlSrcAddr.Ip4TnlAddr);
    pIpHdr->u4Dest = OSIX_HTONL (pTnlPktInfo->TnlDestAddr.Ip4TnlAddr);

    if (CRU_BUF_Copy_FromBufChain (pTnlPktInfo->pBuf, pRawPkt + IP_HDR_LEN, 0,
                                   pTnlPktInfo->IpHdr.u2Len) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pTnlPktInfo->pBuf, 0);
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pRawPkt);
        return CFA_FAILURE;
    }

    MEMSET (&(DestAddr), 0, sizeof (struct sockaddr_in));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = pIpHdr->u4Dest;
    DestAddr.sin_port = 0;

    if (u4EncapsMethod == TNL_TYPE_GRE)
    {
        if ((i4SendBytes = sendto (gi4GreRawSockId, pRawPkt,
                                   (pTnlPktInfo->IpHdr.u2Len + IP_HDR_LEN), 0,
                                   ((struct sockaddr *) &DestAddr),
                                   (sizeof (struct sockaddr_in))))
            == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pTnlPktInfo->pBuf, 0);
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pRawPkt);
            return CFA_FAILURE;
        }
    }
    else
    {
        if ((i4SendBytes = sendto (gi4RawSockId, pRawPkt,
                                   (pTnlPktInfo->IpHdr.u2Len + IP_HDR_LEN), 0,
                                   ((struct sockaddr *) &DestAddr),
                                   (sizeof (struct sockaddr_in))))
            == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pTnlPktInfo->pBuf, 0);
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pRawPkt);
            return CFA_FAILURE;
        }
    }
    CRU_BUF_Release_MsgBufChain (pTnlPktInfo->pBuf, 0);
    MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pRawPkt);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifRecvTnlPkt                                             */
/*                                                                           */
/* Description  : This function receives the IPv6 tunnel packet from socket  */
/*                and decapsulates the Ipv6 Tunnel packet. The ipv6 packet   */
/*                is sent to FS IPv6 Module through ipv6 call back function  */
/*                registered with cfa                                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IpifRecvTnlPkt (VOID)
{
    socklen_t           i4RecvNodeLen;
    INT4                i4RecvBytes;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT1              *pRecvPkt;
    UINT4               u4IfIndex;
    tTnlIpAddr          TnlDestAddr;
    struct sockaddr_in  Recv_Node;
    tCRU_INTERFACE      IfaceId;

    i4RecvNodeLen = sizeof (Recv_Node);
    MEMSET (&Recv_Node, 0, i4RecvNodeLen);
    MEMSET (&(IfaceId), 0, sizeof (tCRU_INTERFACE));

    if ((pRecvPkt = MemAllocMemBlk (gCfaPktPoolId)) == NULL)
    {
        return;
    }

    /* Call Recvfrom to recv Tunl pkts */
    while ((i4RecvBytes = recvfrom (gi4RawSockId, pRecvPkt,
                                    CFA_ENET_MTU,
                                    CFA_TNL_RAWSOCK_NON_BLOCK,
                                    ((struct sockaddr *) &Recv_Node),
                                    (&i4RecvNodeLen))) > 0)
    {
        /* Copy the recvd linear buf to cru buf */
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
        {
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) (pRecvPkt));
            return;
        }
        CRU_BUF_Copy_OverBufChain (pBuf, pRecvPkt, 0, i4RecvBytes);

        /* Assumption: Pkt rcvd from IPv4 has IP header */

        /* Get the Dest IP addr from the rcvd pkt */
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   (UINT1 *) &(TnlDestAddr.Ip4TnlAddr),
                                   IP_PKT_OFF_DEST, sizeof (UINT4));
        TnlDestAddr.Ip4TnlAddr = OSIX_NTOHL (TnlDestAddr.Ip4TnlAddr);

        /* Get the Ip Port Number for the Dest Addr */
        /* This function is used in case of linuxip. 
         * Hence use NetIpv4GetIfIndexFromAddr that works in default VRF
         */
        if (NetIpv4GetIfIndexFromAddr (TnlDestAddr.Ip4TnlAddr, &u4IfIndex)
            == NETIPV4_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) (pRecvPkt));
            return;
        }

        CfaTnlMgrRxFrame (pBuf, i4RecvBytes, u4IfIndex, IfaceId, 0);

    }                            /* end of while loop recvfrom - gi4RawSockId */

    MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) (pRecvPkt));
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IpifRecvGreTnlPkt                                          */
/*                                                                           */
/* Description  : This function receives the Gre tunnel  packet from socket  */
/*                and decapsulates the Gre Tunnel packet.  The ipv6 packet   */
/*                is sent to FS IPv6 Module through ipv6 call back function  */
/*                registered with cfa                                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IpifRecvGreTnlPkt (VOID)
{
    socklen_t           i4RecvNodeLen;
    INT4                i4RecvBytes;
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT1              *pRecvPkt;
    UINT4               u4IfIndex;
    tTnlIpAddr          TnlDestAddr;
    struct sockaddr_in  Recv_Node;
    tCRU_INTERFACE      IfaceId;

    i4RecvNodeLen = sizeof (Recv_Node);
    MEMSET (&Recv_Node, 0, i4RecvNodeLen);
    MEMSET (&(IfaceId), 0, sizeof (tCRU_INTERFACE));

    if ((pRecvPkt = MemAllocMemBlk (gCfaPktPoolId)) == NULL)
    {
        return;
    }
    /* Call Recvfrom to recv Gre Tunl pkts */
    while ((i4RecvBytes = recvfrom (gi4GreRawSockId, pRecvPkt,
                                    CFA_ENET_MTU,
                                    CFA_TNL_RAWSOCK_NON_BLOCK,
                                    ((struct sockaddr *) &Recv_Node),
                                    (&i4RecvNodeLen))) > 0)
    {
        /* Copy the recvd linear buf to cru buf */
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (i4RecvBytes, 0)) == NULL)
        {
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) (pRecvPkt));
            return;
        }
        CRU_BUF_Copy_OverBufChain (pBuf, pRecvPkt, 0, i4RecvBytes);

        /* Assumption: Pkt rcvd from IPv4 has IP header */

        /* Get the Dest IP addr from the rcvd pkt */
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   (UINT1 *) &(TnlDestAddr.Ip4TnlAddr),
                                   IP_PKT_OFF_DEST, sizeof (UINT4));
        TnlDestAddr.Ip4TnlAddr = OSIX_NTOHL (TnlDestAddr.Ip4TnlAddr);

        /* Get the Ip Port Number for the Dest Addr */

        /* This function is used in case of linuxip. 
         * Hence use NetIpv4GetIfIndexFromAddr that works in default VRF
         */
        if (NetIpv4GetIfIndexFromAddr (TnlDestAddr.Ip4TnlAddr, &u4IfIndex)
            == NETIPV4_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) (pRecvPkt));
            return;
        }

        CfaTnlMgrRxFrame (pBuf, i4RecvBytes, u4IfIndex, IfaceId, 0);

    }                            /* end of while loop recvfrom - gi4GreRawSockId */

    MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) (pRecvPkt));
    return;
}
#endif
/*****************************************************************************/
/*                                                                           */
/* Function     : CfaTunlCreateContext                                       */
/*                                                                           */
/* Description  : This function creates a virtual context when a virtual     */
/*                router is created in VCM                                   */
/*                                                                           */
/* Input        : u4ContextId                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
CfaTunlCreateContext (UINT4 u4ContextId)
{
    tCfaTnlCxt         *pTnlCxt = NULL;

    if (gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId] == NULL)
    {
        pTnlCxt = (tCfaTnlCxt *) MemAllocMemBlk
            (gCfaTnlGblInfo.CfaTnlCxtMemPoolId);
        if (pTnlCxt != NULL)
        {
            TMO_SLL_Init (&(pTnlCxt->TnlIfSLL));
            pTnlCxt->u4ContextId = u4ContextId;
            pTnlCxt->u1TunlIfExists = FALSE;
            gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId] = pTnlCxt;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CfaTunlDeleteContext                                       */
/*                                                                           */
/* Description  : This function deletes a virtual context when a virtual     */
/*                router is deleted in VCM                                   */
/*                                                                           */
/* Input        : u4ContextId                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
CfaTunlDeleteContext (UINT4 u4ContextId)
{
    tCfaTnlCxt         *pTnlCxt = NULL;

    if (gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId] != NULL)
    {
        pTnlCxt = gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId];

        /* All the tunnel interfaces should have been deleted 
         * before context deletion 
         */
        MemReleaseMemBlock (gCfaTnlGblInfo.CfaTnlCxtMemPoolId,
                            (UINT1 *) pTnlCxt);
        gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId] = NULL;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CfaTnlGetCxt                                               */
/*                                                                           */
/* Description  : This function fetches ans returns the tunnel context       */
/*                pointer for the given context Id                           */
/*                                                                           */
/* Input        : u4ContextId                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : returns valid context pointer or NULL                      */
/*                                                                           */
/*****************************************************************************/
tCfaTnlCxt         *
CfaTnlGetCxt (UINT4 u4ContextId)
{
    tCfaTnlCxt         *pTnlCxt = NULL;
    if (u4ContextId < CFA_MAX_TUNL_CONTEXT)
    {
        if (gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId] != NULL)
        {
            pTnlCxt = gCfaTnlGblInfo.apCfaTnlCxt[u4ContextId];
        }
    }
    return pTnlCxt;
}

/****************************************************************************
* Function     : CfaVcmCallbackFn 
*                                            
* Description  : This function is invoked by the VCM module to notify the 
*                 context creation / context deletion event.
*
* Input        : u4ContextId - The virtual router Id                      
*                u4Event     - Context create / delete event.
*
* Output       : None 
*
* Returns      : None.
*
***************************************************************************/

VOID
CfaVcmCallbackFn (UINT4 u4IfIndex, UINT4 u4ContextId, UINT1 u1Event)
{
    UNUSED_PARAM (u4IfIndex);

    CFA_LOCK ();
    if (u1Event == VCM_CONTEXT_CREATE)
    {
        /* Create Cfa tunnel context */
        CfaTunlCreateContext (u4ContextId);
    }
    else
    {
        /* Delete Cfa tunnel context */
        CfaTunlDeleteContext (u4ContextId);
    }
    CFA_UNLOCK ();
    return;
}

/****************************************************************************
* Function     : CfaTnlIfaceUnMapping
*                                            
* Description  : This function is invoked by the VCM module to handle the 
*                 VRF unmapping for the tunl interface.
*
* Input        : u4ContextId - The virtual router Id                      
*                u4IfIndex - Interface index
*
* Output       : None 
*
* Returns      : None.
*
***************************************************************************/

VOID
CfaTnlIfaceUnMapping (UINT4 u4IfIndex, UINT4 u4ContextId)
{
    tStackInfoStruct   *pLowStackListScan = NULL;
    tTnlIfEntry         TnlIfEntry;
    tTnlIfEntry        *pTnlIfEntry = NULL;
    tCfaTnlCxt         *pTunlCxt = NULL;
    tCfaRegInfo         CfaInfo;
    INT4                i4RetVal;

    pTnlIfEntry = &TnlIfEntry;
    i4RetVal = CfaFindTnlEntryFromContextId (u4ContextId, u4IfIndex,
                                             pTnlIfEntry);
    if (i4RetVal == CFA_SUCCESS)
    {
        /* Notify Higher Layers for the deletion of Tunnel parameters
         * while remapping the tunl interface */
        MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));
        CfaInfo.u4IfIndex = pTnlIfEntry->u4IfIndex;
        CfaInfo.CfaTnlIfInfo.u4TnlMask = (TNL_DIR_FLAG_CHG
                                          | TNL_DIRECTION_CHG |
                                          TNL_ENCAP_OPT_CHG |
                                          TNL_ENCAP_LIMIT_CHG |
                                          TNL_ENCAP_ADDR_CHG);

        CfaInfo.CfaTnlIfInfo.u4TnlDirFlag = 0;
        CfaInfo.CfaTnlIfInfo.u4TnlDir = 0;
        CfaInfo.CfaTnlIfInfo.u4TnlEncapLmt = 0;
        CfaInfo.CfaTnlIfInfo.u4TnlEncapOpt = 0;
        CfaInfo.CfaTnlIfInfo.u4LocalAddr = 0;
        CfaInfo.CfaTnlIfInfo.u4RemoteAddr = 0;
        CfaInfo.CfaTnlIfInfo.u4TnlType = 0;
        CfaInfo.CfaTnlIfInfo.u4TnlHopLimit = 0;

        CfaNotifyTnlIfUpdate (&CfaInfo);
        CfaSetIfTnlEntry (pTnlIfEntry->u4IfIndex, NULL);
        pTunlCxt = CfaTnlGetCxt (u4ContextId);
        CfaDeleteTnlIfEntryInCxt (pTunlCxt, pTnlIfEntry->u4IfIndex);
        while (CFA_IF_STACK_LOW_ENTRY (pTnlIfEntry->u4IfIndex))
        {
            pLowStackListScan = (tStackInfoStruct *) CFA_IF_STACK_LOW_ENTRY
                (pTnlIfEntry->u4IfIndex);

            if (pLowStackListScan == NULL)
                return;
            if (CFA_IF_STACK_IFINDEX (pLowStackListScan) != CFA_NONE)
            {
                /* First Delete this entry from the lower layer's stack */
                CfaIfmDeleteStackEntry
                    (CFA_IF_STACK_IFINDEX
                     (pLowStackListScan),
                     CFA_HIGH, (UINT2) pTnlIfEntry->u4IfIndex);

                CfaIfmDeleteStackEntry ((UINT2) pTnlIfEntry->u4IfIndex,
                                        CFA_LOW, CFA_IF_STACK_IFINDEX
                                        (pLowStackListScan));
            }
            else
            {
                CfaIfmUpdateStackForDeletion (&CFA_IF_STACK_LOW
                                              (pTnlIfEntry->u4IfIndex),
                                              CFA_FALSE, CFA_NONE);
            }
        }

    }
    return;
}
