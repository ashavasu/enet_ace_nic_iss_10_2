/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: cfareg.c,v 1.25 2014/11/14 12:12:23 siva Exp $
 *
 * Description:This file has the functions related to
 *             registration/deregistration of HL applications
 *             with CFA.
 *
 *******************************************************************/
#include "cfainc.h"

/* MemPool for CfaRegTbl */
tMemPoolId          CfaRegMemPoolId;

/* Cfa HL registration tbl */
tCfaRegParams      *gCfaRegTbl[MAX_CFA_HL_APPLS_LIMIT + 1];

/******************************************************************************
 * Function           : CfaInitHLRegnTbl
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine to initialize the CFA registration table 
 *                      used to store the HL functin pointers.       
 ******************************************************************************/
INT4
CfaInitHLRegnTbl (VOID)
{
    UINT2               u2RegTblIndx;

    CFA_REG_MEMPOOL = CFAMemPoolIds[MAX_CFA_HL_APPLS_SIZING_ID];;

    for (u2RegTblIndx = 0; u2RegTblIndx <= MAX_CFA_HL_APPLS_LIMIT;
         u2RegTblIndx++)
    {
        gCfaRegTbl[u2RegTblIndx] = NULL;
    }

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaDeInitHLRegnTbl
 * Input(s)           : None. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine to de-initialize the CFA registration table. 
 ******************************************************************************/
INT4
CfaDeInitHLRegnTbl (VOID)
{
    UINT2               u2RegTblIndx;

    for (u2RegTblIndx = 1; u2RegTblIndx <= MAX_CFA_HL_APPLS_LIMIT;
         u2RegTblIndx++)
    {
        if (gCfaRegTbl[u2RegTblIndx] != NULL)
        {
            if (MemReleaseMemBlock (CFA_REG_MEMPOOL,
                                    (UINT1 *) gCfaRegTbl[u2RegTblIndx]) !=
                MEM_SUCCESS)
            {
                CFA_DBG (ALL_FAILURE_TRC | OS_RESOURCE_TRC, "CFA",
                         "Releasing MemBlock in Registration table failed\r\n");
                return CFA_FAILURE;
            }
            else
            {
                gCfaRegTbl[u2RegTblIndx] = NULL;
            }
        }
    }

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaRegisterHL
 * Input(s)           : pCfaRegParams - Reg params to be filled-in by HL. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by HL to register with CFA. 
 ******************************************************************************/
INT4
CfaRegisterHL (tCfaRegParams * pCfaRegParams)
{
    UINT2               u2RegTblIndx;
    tCfaRegInfo         CfaInfo;
    UINT4               u4Index;
    tStackInfoStruct   *pScanNode = NULL;

    u4Index = 1;

    if (pCfaRegParams == NULL)
    {
        return CFA_FAILURE;
    }

    u2RegTblIndx = CfaGetRegTblIndx (pCfaRegParams->u2LenOrType);

    if (u2RegTblIndx == 0)
    {
        /* RegTbl index 0 is used by TnlMgr */
        if (gCfaRegTbl[u2RegTblIndx] != NULL)
        {
            /* Index 0 is already being used */
            return CFA_FAILURE;
        }
    }

    /* allocate for the registration table entry */
    if ((gCfaRegTbl[u2RegTblIndx] = (tCfaRegParams *)
         MemAllocMemBlk (CFA_REG_MEMPOOL)) == NULL)
    {
        CFA_DBG (ALL_FAILURE_TRC | OS_RESOURCE_TRC, "CFA",
                 "Allocating MemBlock in HLRegistration table failed\r\n");
        return CFA_FAILURE;
    }

    MEMSET (gCfaRegTbl[u2RegTblIndx], 0, sizeof (tCfaRegParams));
    gCfaRegTbl[u2RegTblIndx]->u1RegFlag = CFA_REGISTER;
    if (pCfaRegParams->u2RegMask & CFA_IF_CREATE)
    {
        gCfaRegTbl[u2RegTblIndx]->pIfCreate = pCfaRegParams->pIfCreate;
    }
    if (pCfaRegParams->u2RegMask & CFA_IF_DELETE)
    {
        gCfaRegTbl[u2RegTblIndx]->pIfDelete = pCfaRegParams->pIfDelete;
    }
    if (pCfaRegParams->u2RegMask & CFA_IF_UPDATE)
    {
        gCfaRegTbl[u2RegTblIndx]->pIfUpdate = pCfaRegParams->pIfUpdate;
    }
    if (pCfaRegParams->u2RegMask & CFA_TNL_IF_UPDATE)
    {
        gCfaRegTbl[u2RegTblIndx]->pTnlIfUpdate = pCfaRegParams->pTnlIfUpdate;
    }
    if (pCfaRegParams->u2RegMask & CFA_IF_OPER_ST_CHG)
    {
        gCfaRegTbl[u2RegTblIndx]->pIfOperStChg = pCfaRegParams->pIfOperStChg;
    }
    if (pCfaRegParams->u2RegMask & CFA_IF_RCV_PKT)
    {
        gCfaRegTbl[u2RegTblIndx]->pIfRcvPkt = pCfaRegParams->pIfRcvPkt;
    }
    gCfaRegTbl[u2RegTblIndx]->u2RegMask = pCfaRegParams->u2RegMask;
    gCfaRegTbl[u2RegTblIndx]->u2LenOrType = pCfaRegParams->u2LenOrType;
    /* Notifications for all interfaces already created is sent 
     * to all registered HLs */

    CFA_GAP_IF_SCAN (u4Index, SYS_DEF_MAX_INTERFACES)
    {
        if (CFA_IF_ENTRY_USED (u4Index))
        {
            pScanNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY
                ((UINT2) u4Index);
            CFA_CHECK_IF_NULL (pScanNode, CFA_FAILURE);

            if ((CFA_IF_STACK_IFINDEX (pScanNode) == CFA_NONE) &&
                (CFA_IF_STACK_STATUS (pScanNode) == CFA_RS_ACTIVE) &&
                (CFA_IF_IPPORT (u4Index) != CFA_INVALID_INDEX))

            {
                /* Notify interface creation to all HLs */
                CfaInfo.u4IfIndex = u4Index;
                CfaGetIfInfo (u4Index, &(CfaInfo.CfaIntfInfo));
                CfaNotifyIfCreate (&CfaInfo);
            }
        }
    }

    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaDeregisterHL
 * Input(s)           : u2LenOrType - Length or Type in Ethernet header. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by HL to de-register with CFA. 
 ******************************************************************************/
INT4
CfaDeregisterHL (UINT2 u2LenOrType)
{
    UINT2               u2RegTblIndx;
    INT4                i4RetVal = CFA_SUCCESS;

    u2RegTblIndx = CfaGetRegTblIndx (u2LenOrType);

    if (gCfaRegTbl[u2RegTblIndx] == NULL)
    {
        i4RetVal = CFA_FAILURE;
    }
    else
    {
        if (MemReleaseMemBlock (CFA_REG_MEMPOOL,
                                (UINT1 *) gCfaRegTbl[u2RegTblIndx]) !=
            MEM_SUCCESS)
        {
            CFA_DBG (ALL_FAILURE_TRC | OS_RESOURCE_TRC, "CFA",
                     "Releasing MemBlock in HL Registration table failed\r\n");
            i4RetVal = CFA_FAILURE;
        }
        else
        {
            gCfaRegTbl[u2RegTblIndx] = NULL;
        }
    }
    return (i4RetVal);
}

/******************************************************************************
 * Function           : CfaNotifyIfCreate 
 * Input(s)           : pCfaInfo - Info to be sent to HLs.
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by CFA to send the Interface creation
 *                      notification to HLs. 
 ******************************************************************************/
VOID
CfaNotifyIfCreate (tCfaRegInfo * pCfaInfo)
{
    UINT2               u2RegTblIndx;

    for (u2RegTblIndx = 0; u2RegTblIndx <= MAX_CFA_HL_APPLS_LIMIT;
         u2RegTblIndx++)
    {
        if (gCfaRegTbl[u2RegTblIndx] != NULL)
        {
            if (gCfaRegTbl[u2RegTblIndx]->pIfCreate != NULL)
            {
                (*(gCfaRegTbl[u2RegTblIndx]->pIfCreate)) (pCfaInfo);
            }
        }
    }
}

/******************************************************************************
 * Function           : CfaNotifyIfDelete
 * Input(s)           : pCfaInfo - IfInfo to be sent to HLs. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by CFA to send the Interface deletion 
 *                      notification to HLs. 
 ******************************************************************************/
VOID
CfaNotifyIfDelete (tCfaRegInfo * pCfaInfo)
{
    UINT2               u2RegTblIndx;

    for (u2RegTblIndx = 0; u2RegTblIndx <= MAX_CFA_HL_APPLS_LIMIT;
         u2RegTblIndx++)
    {
        if (gCfaRegTbl[u2RegTblIndx] != NULL)
        {
            if (gCfaRegTbl[u2RegTblIndx]->pIfDelete != NULL)
            {
                (*(gCfaRegTbl[u2RegTblIndx]->pIfDelete)) (pCfaInfo);
            }
        }
    }
}

/******************************************************************************
 * Function           : CfaNotifyIfUpdate
 * Input(s)           : pCfaInfo - IfInfo to be sent to HLs. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by CFA to send the Interface parameter
 *                      change notification to HLs. 
 ******************************************************************************/
VOID
CfaNotifyIfUpdate (tCfaRegInfo * pCfaInfo)
{
    UINT2               u2RegTblIndx;

    for (u2RegTblIndx = 0; u2RegTblIndx <= MAX_CFA_HL_APPLS_LIMIT;
         u2RegTblIndx++)
    {
        if (gCfaRegTbl[u2RegTblIndx] != NULL)
        {
            if (gCfaRegTbl[u2RegTblIndx]->pIfUpdate != NULL)
            {
                (*(gCfaRegTbl[u2RegTblIndx]->pIfUpdate)) (pCfaInfo);
            }
        }
    }
}

/******************************************************************************
 * Function           : CfaNotifyTnlIfUpdate
 * Input(s)           : pCfaInfo - IfInfo related to Tunnel interface. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by CFA to send the Tunnel interface 
 *                      parameter change to HLs. 
 ******************************************************************************/
VOID
CfaNotifyTnlIfUpdate (tCfaRegInfo * pCfaInfo)
{
    UINT2               u2RegTblIndx;

    for (u2RegTblIndx = 0; u2RegTblIndx <= MAX_CFA_HL_APPLS_LIMIT;
         u2RegTblIndx++)
    {
        if (gCfaRegTbl[u2RegTblIndx] != NULL)
        {
            if (gCfaRegTbl[u2RegTblIndx]->pTnlIfUpdate != NULL)
            {
                (*(gCfaRegTbl[u2RegTblIndx]->pTnlIfUpdate)) (pCfaInfo);
            }
        }
    }
}

/******************************************************************************
 * Function           : CfaNotifyIfOperStChg
 * Input(s)           : pCfaInfo - IfInfo to be sent to HLs. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by CFA to notify the oper sts chg to HLs. 
 ******************************************************************************/
VOID
CfaNotifyIfOperStChg (tCfaRegInfo * pCfaInfo)
{
    UINT2               u2RegTblIndx;
    UINT1               u1Ipv4AddressChange = CFA_FALSE;

    CfaGetIfIpv4AddressChange(pCfaInfo->u4IfIndex,&u1Ipv4AddressChange);

    for (u2RegTblIndx = 0; u2RegTblIndx <= MAX_CFA_HL_APPLS_LIMIT;
         u2RegTblIndx++)
    {
	    if (gCfaRegTbl[u2RegTblIndx] != NULL)
	    {
		    /* If ipv4 address of an interface
		     * is changed then no need to notify ipv6 module*/

		     if (!((gCfaRegTbl[u2RegTblIndx]->u2LenOrType == CFA_ENET_IPV6) 
                     	&& (u1Ipv4AddressChange == CFA_TRUE)))
		    {
			    if (gCfaRegTbl[u2RegTblIndx]->pIfOperStChg != NULL)
			    {
				    (*(gCfaRegTbl[u2RegTblIndx]->pIfOperStChg)) (pCfaInfo);
			    }
		    }
	    }
    }
}

/******************************************************************************
 * Function           : CfaNotifyIfPktRvcd
 * Input(s)           : u2LenOrType - Length or Type in Ethernet header. 
 *                      pCfaInfo - IfInfo to be sent to HLs
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by CFA to notify the packet arrived 
 *                      in CFA to HL. 
 ******************************************************************************/
INT4
CfaNotifyIfPktRcvd (UINT2 u2LenOrType, tCfaRegInfo * pCfaInfo)
{
    UINT2               u2RegTblIndx;

    u2RegTblIndx = CfaGetRegTblIndx (u2LenOrType);

    if (gCfaRegTbl[u2RegTblIndx] != NULL)
    {
        if (gCfaRegTbl[u2RegTblIndx]->pIfRcvPkt != NULL)
        {
            /* CRU buf rcvd by CFA is delivered to HL. HL will be 
             * the owner of the buf. Buf release has to be handled 
             * by HL in case of failure. */
            (*(gCfaRegTbl[u2RegTblIndx]->pIfRcvPkt)) (pCfaInfo);
            /* the registered call back function takes care of
             * failure contions buffer release.
             *  Caution, if CFA_SUCCESS is changed to CFA_FAILURE it will result in
             * double release of Buffer */
            return CFA_SUCCESS;
        }
        else
        {
#ifdef LNXIP6_WANTED
            CfaHandleInBoundPktToLnxIpTap (pCfaInfo->u4IfIndex,
                                           pCfaInfo->CfaPktInfo.pBuf,
                                           &(pCfaInfo->CfaPktInfo.EthHdr),
                                           IPV6_ID);
            /* We have already taken a copy of Cru Buf.
             * Release the Buffer */
            CRU_BUF_Release_MsgBufChain (pCfaInfo->CfaPktInfo.pBuf, FALSE);
            return CFA_SUCCESS;
#endif
        }
    }
    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IWF,
              "Exiting CfaNotifyIfPktRcvd - Cfa2 Enqueuing on interface %d - \
              FAILURE\n", pCfaInfo->u4IfIndex);
    return CFA_FAILURE;

}

/******************************************************************************
 * Function           : CfaGetRegTblIndx
 * Input(s)           : u2LenOrType - Length or Type in Ethernet header. 
 * Output(s)          : None.
 * Returns            : CFA_SUCCESS/FAILURE
 * Action             : Routine used by CFA to get the index of the CFA 
 *                      registration table based on u2LenOrType. 
 ******************************************************************************/
UINT2
CfaGetRegTblIndx (UINT2 u2LenOrType)
{
    UINT2               u2RegTblIndx = 0;
#ifdef SYNCE_WANTED
    UINT2               u2ReservedRegTblIndx = 13;
#else
    UINT2               u2ReservedRegTblIndx = 12;
#endif
    UINT2               u2Indx = 0;

    /* RegTblIndex 0 is used by Cfa Tunnel manager */

    /* Determine the encapsulation type */
    if (CFA_ENET_IS_TYPE (u2LenOrType))
    {
        /* ENET V2 encap type */
        switch (u2LenOrType)
        {
            case CFA_ENET_IPV4:
                u2RegTblIndx = 1;
                break;
            case CFA_ENET_ARP:
                u2RegTblIndx = 2;
                break;
            case CFA_ENET_RARP:
                u2RegTblIndx = 3;
                break;
            case CFA_ENET_IPV6:
                u2RegTblIndx = 4;
                break;
            case CFA_ENET_MPLS:
                u2RegTblIndx = 7;
                break;
            case CFA_ENET_PTP:
                u2RegTblIndx = 11;
                break;
#ifdef SYNCE_WANTED
            case CFA_ENET_SYNCE:
                u2RegTblIndx = 13;
                break;
#endif
            default:
                break;
        }
    }
    else
    {
        switch (u2LenOrType)
        {
                /* LLC-SNAP encap type */
            case CFA_LLC_ROUTED_SAP:
                /* IS-IS frame */
                u2RegTblIndx = 8;
                break;
            case CFA_LLC_STAP_SAP:
                /* BRIDGE control frame */
                u2RegTblIndx = 9;
                break;
            case CFA_LLC_SNAP_SAP:
                /* BRIDGE data frame */
                u2RegTblIndx = 10;
                break;
            case CFA_TELINK:
                /* TE LINK Type */
                u2RegTblIndx = 12;
                break;

            default:
                /* Get the free TblIndex for the following protocols: 
                 * PROTO_RMON, PROTO_L2TP, PROTO_QoS, PROTO_DHCP, PROTO_PPP */
                for (u2Indx = (UINT2) (u2ReservedRegTblIndx + 1);
                     u2Indx <= MAX_CFA_HL_APPLS_LIMIT; u2Indx++)
                {
                    if (gCfaRegTbl[u2Indx] == NULL)
                    {
                        u2RegTblIndx = u2Indx;
                        break;
                    }
                }
                break;
        }
    }
    return u2RegTblIndx;
}

/******************************************************************************
 * Function           : CfaRegNotifyHigherLayer
 *
 * Input(s)           : u4IfIndex - Interface Index to be notified to higher 
 *                                  Layers.
 *                      u1Action  - Interface Create (CFA_IF_CREATE)/
 *                                  Interface Delete (CFA_IF_DEL)
 *
 * Output(s)          : None.
 *
 * Returns            : None.                 
 *
 * Action             : This routine notifies the higher layer modules regarding
 *                      the event provided by the CFA.           
 ******************************************************************************/
VOID
CfaRegNotifyHigherLayer (UINT4 u4IfIndex, UINT1 u1Action)
{
    tCfaRegInfo         CfaInfo;

    MEMSET (&CfaInfo, 0, sizeof (tCfaRegInfo));

    /* Notify interface delete info to all HLs */
    CfaInfo.u4IfIndex = u4IfIndex;

    CfaGetIfType (u4IfIndex, &(CfaInfo.CfaIntfInfo.u1IfType));
    CfaGetIfMtu (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfMtu));
    CfaGetIfOperStatus (u4IfIndex, &(CfaInfo.CfaIntfInfo.u1IfOperStatus));
    CfaGetIfSpeed (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfSpeed));
    CfaGetIfHighSpeed (u4IfIndex, &(CfaInfo.CfaIntfInfo.u4IfHighSpeed));
    CfaGetIfName (u4IfIndex, CfaInfo.CfaIntfInfo.au1IfName);
    CfaGetIfIvrVlanId (u4IfIndex, &CfaInfo.CfaIntfInfo.u2VlanId);
    CfaGetIfWanType (u4IfIndex, &CfaInfo.CfaIntfInfo.u1WanType);
    CfaGetIfNwType (u4IfIndex, &CfaInfo.CfaIntfInfo.u1NwType);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &CfaInfo.CfaIntfInfo.u1BridgedIface);
    /* if the Lag port is a router port dont send the delete
     * notification to higher layers */
    if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_LAGG) &&
        (CfaInfo.CfaIntfInfo.u1BridgedIface == CFA_DISABLED))
    {
        return;
    }

    if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_ENET)
        || (CfaInfo.CfaIntfInfo.u1IfType == CFA_LAGG))
    {
        CfaGetIfHwAddr (u4IfIndex, CfaInfo.CfaIntfInfo.au1MacAddr);
    }
    else if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_L3IPVLAN)
             || (CfaInfo.CfaIntfInfo.u1IfType == CFA_L2VLAN))
    {
        CfaGetIfHwAddr (u4IfIndex, CfaInfo.CfaIntfInfo.au1MacAddr);
    }

    switch (u1Action)
    {
        case CFA_IF_CREATE:
            CfaNotifyIfCreate (&CfaInfo);
            break;

        case CFA_IF_DELETE:
            CfaNotifyIfDelete (&CfaInfo);
            break;

        case CFA_IF_UPDATE:
            CfaNotifyIfUpdate (&CfaInfo);
            break;

        default:
            break;

    }                            /* End of switch */
    return;
}
