/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ifmutils.c,v 1.206 2018/01/08 12:28:33 siva Exp $
 *
 * Description:This file contains the utility routines for    
 *             the Interface Management Module of the CFA.     
 *
 *******************************************************************/
#include "cfainc.h"
#include "vcm.h"
#include "ifmibcli.h"
#ifdef NPAPI_WANTED
#include "rstminp.h"
#include "mbsnp.h"
#endif
#include "ofcl.h"

#ifdef LNXIP4_WANTED
extern UINT1       *CfaGddGetLnxIntfnameForPort (UINT2 u2Index);
#endif

/* SNMP Functions for TRAP generation */
extern INT4 AstIsMstStartedInContext PROTO ((UINT4 u4ContextId));
extern INT4 AstIsPvrstStartedInContext PROTO ((UINT4 u4ContextId));
#ifdef SYSLOG_WANTED
extern UINT4        SyslogNotifyIfStatusChange (UINT4 u4IfIndex);
#endif
#ifdef LNXIP6_WANTED
extern INT4         Lip6RAConfig (UINT4 u4ContextId);
#define ADV_DEF_LIFETIME 1800
#endif
/* CFA Functions for TRAP Support */
VOID CfaMemFreeVarBindList PROTO ((tSNMP_VAR_BIND * pVarBindLst));
VOID CfaFreeVarbind PROTO ((tSNMP_VAR_BIND * pVarBind));

/***************************************************************************n
 *
 *    Function Name        : CfaIfmUpdateHighCounters
 *
 *    Description        : This function is for updating the high
 *                counters of ifXTable.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *                UINT4 u4Octets,
 *                UINT1 u1Direction
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIfmUpdateHighCounters (UINT4 u4IfIndex, UINT4 u4Octets, UINT1 u1Direction)
{

    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmUpdateHighCounters for interface %d in Dir %d.\n",
              u4IfIndex, u1Direction);

    if (u1Direction == CFA_INCOMING)
    {
        if ((CFA_IF_GET_IN_OCTETS (u4IfIndex) + u4Octets) <
            CFA_IF_GET_IN_OCTETS (u4IfIndex))
        {
            CFA_IF_INCR_IN_HC_OCTETS (u4IfIndex);
        }
        CFA_IF_SET_IN_OCTETS (u4IfIndex, u4Octets);
    }
    else
    {
        if ((CFA_IF_GET_OUT_OCTETS (u4IfIndex) + u4Octets) <
            CFA_IF_GET_OUT_OCTETS (u4IfIndex))
        {
            CFA_IF_INCR_OUT_HC_OCTETS (u4IfIndex);
        }
        CFA_IF_SET_OUT_OCTETS (u4IfIndex, u4Octets);
    }

}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmAddStackEntry
 *
 *    Description        : This function is for creating entry in the
 *                stack table and updating the affected stack
 *                entries. Not to be called from the low level
 *                routines.
 *
 *                If this call results (e.g.) in the layering
 *                of interface A over interface B - then
 *                whatever was layer above B previously would
 *                be left dangling if the interface B does not
 *                support multiplexing. Hence, this function
 *                must be used with care. It should be checked
 *                in the calling function if B is either a
 *                multiplexing layer or there is currently no
 *                layer above B (dangling) - if not then this
 *                operation should not be performed.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *                UINT1 u1Direction
 *                UINT4 u4StackIfIndex,
 *                UINT1 u1StackStatus
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *                gIfGlobal (Interface's global struct)
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if entry is added
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmAddStackEntry (UINT4 u4IfIndex, UINT1 u1Direction,
                     UINT4 u4StackIfIndex, UINT1 u1StackStatus)
{
    tStackInfoStruct   *pStackLayer = NULL;
    tStackInfoStruct   *pStackAffectedLayer = NULL;
    tTMO_SLL           *pIfStack = NULL;
    UINT1               u1Replace = CFA_TRUE;
    UINT1               u1IfType = CFA_NONE;
    UINT1               u1StackIfType = CFA_ENET_UNKNOWN;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    tTMO_SLL_NODE      *pNode = NULL;
#ifdef MPLS_WANTED
    /* STATIC_HLSP */
    UINT4               u4L3IfIndex = 0;
#endif
    CFA_DBG4 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmAddStackEntry for "
              "interface %d in Dir %d layer %d status %d\n",
              u4IfIndex, u1Direction, u4StackIfIndex, u1StackStatus);

/* allocate for the new node and the new stack entry caused by the creation of
this node - if unsuccessful then return failure. We always allocate for the
indirectly affected layer also since we cannot roll-back the change if we
are not able to allocate memory later. */
    if ((pStackLayer = MemAllocMemBlk (gCfaStackPoolId)) == NULL)
    {

        CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmAddStackEntry - "
                  "Malloc fail - interface %d in Dir %d\n",
                  u4IfIndex, u1Direction);

        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfType ((UINT4) u4StackIfIndex, &u1StackIfType);

/* if the there is no higher or lower layer then we need not allocate another
structure */
    if (u4StackIfIndex != 0)
    {
        if ((pStackAffectedLayer = MemAllocMemBlk (gCfaStackPoolId)) == NULL)
        {

            CFA_DBG2 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmAddStackEntry - "
                      "Malloc fail - aff by interface %d in Dir %d\n",
                      u4IfIndex, u1Direction);

            MemReleaseMemBlock (gCfaStackPoolId, (UINT1 *) pStackLayer);
            return (CFA_FAILURE);
        }
    }

/* check which direction stack is to change. based on this we have to see
if we have to delete the existing entry. */
    if (u1Direction == CFA_HIGH)
    {
        pIfStack = &CFA_IF_STACK_HIGH (u4IfIndex);
        if (u1StackIfType == CFA_MPLS_TUNNEL)
        {
            /* Many Mpls Tunnel(150) interfaces can be stacked over the
             * same MPLS(166) interface. Replace the higher layer entry only
             * if it is the only entry.
             */
            pNode = (tTMO_SLL_NODE *) TMO_SLL_First (pIfStack);
            if (pNode == NULL)
            {
                MemReleaseMemBlock (gCfaStackPoolId, (UINT1 *) pStackLayer);
                if (pStackAffectedLayer != NULL)
                {
                    MemReleaseMemBlock (gCfaStackPoolId,
                                        (UINT1 *) pStackAffectedLayer);
                }
                return CFA_FAILURE;
            }
            if (CFA_IF_STACK_IFINDEX (pNode) != CFA_NONE)
            {
                u1Replace = CFA_FALSE;
            }
        }
        if ((u1StackIfType == CFA_BRIDGED_INTERFACE)
            || (u1StackIfType == CFA_PIP))
        {
            u1Replace = CFA_FALSE;
        }
    }
    else
    {
        pIfStack = &CFA_IF_STACK_LOW (u4IfIndex);

        if ((u1IfType == CFA_LAGG) || (u1IfType == CFA_TELINK))
        {
            pNode = (tTMO_SLL_NODE *) TMO_SLL_First (pIfStack);
            /*CFA_CHECK_IF_NULL (pNode, CFA_FAILURE); */
            if (pNode == NULL)
            {
                MemReleaseMemBlock (gCfaStackPoolId, (UINT1 *) pStackLayer);
                if (pStackAffectedLayer != NULL)
                {
                    MemReleaseMemBlock (gCfaStackPoolId,
                                        (UINT1 *) pStackAffectedLayer);
                }
                return CFA_FAILURE;
            }
        }

        /* One TE Link can be stacked over many TE links (in case of 
         * TE Link Bundling), So, replace the existing lower stack entry
         * only if it is the only entry */
        if (((u1IfType == CFA_LAGG) || (u1IfType == CFA_TELINK)) &&
            (CFA_IF_STACK_IFINDEX (pNode) != CFA_NONE))
        {
            u1Replace = CFA_FALSE;
        }

        u1Direction = CFA_LOW;
    }

/* initialise the node with the values provided */
    TMO_SLL_Init_Node (&pStackLayer->NextEntry);
    pStackLayer->u4IfIndex = u4StackIfIndex;
    pStackLayer->u1StackStatus = u1StackStatus;

    CfaIfmUpdateStackForAddition (u4IfIndex, pStackLayer, pIfStack, u1Replace,
                                  u1Direction);

/* We now update the stack for the u4StackIfIndex layer which was
affected by this change, if required.we do not perform recursion hence
the function's body is some-what repeated below.*/
    if (u4StackIfIndex == 0)
    {
/* we update the stack status change time */
        OsixGetSysTime (&CFA_IFSTK_CHNG ());

        CFA_DBG4 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmAddStackEntry Success - "
                  "interface %d in dir %d layer %d status %d\n",
                  u4IfIndex, u1Direction, u4StackIfIndex, u1StackStatus);
        return (CFA_SUCCESS);
    }

    /* re-initialise the variable */
    pIfStack = NULL;
    u1Replace = CFA_TRUE;

/* the direction would now be reversed for the affected entry. */
    if (u1Direction == CFA_LOW)
    {
        pIfStack = &CFA_IF_STACK_HIGH (u4StackIfIndex);
        u1Direction = CFA_HIGH;
        CfaGetIfBridgedIfaceStatus ((UINT4) u4StackIfIndex,
                                    &u1BridgedIfaceStatus);
        if (((u1StackIfType == CFA_ENET)
             && (u1BridgedIfaceStatus == CFA_DISABLED))
            || (u1StackIfType == CFA_L3IPVLAN)
            || (u1StackIfType == CFA_BRIDGED_INTERFACE)
            || (u1StackIfType == CFA_PIP))
        {
            /* Here Stack if index is Router port or L3 IP interface */
            u1Replace = CFA_FALSE;
        }

        /* We need to update the higher stack entries of the lower 
         * layer interface.
         *
         * So, if the lower layer if type is L3 Interface or CFA_TELINK
         * it should be replaced.
         */
        if ((u1IfType == CFA_MPLS) || (u1IfType == CFA_TELINK))
        {
            if (((u1StackIfType == CFA_ENET) &&
                 (u1BridgedIfaceStatus == CFA_DISABLED)) ||
                (u1StackIfType == CFA_L3IPVLAN))
            {
                u1Replace = CFA_TRUE;
                u1Direction = CFA_LOW;
            }
            else if (u1StackIfType == CFA_TELINK)
            {
                u1Replace = CFA_TRUE;
            }
        }
    }
    else
    {
        pIfStack = &CFA_IF_STACK_LOW (u4StackIfIndex);

        u1Direction = CFA_LOW;
        if ((u1IfType == CFA_BRIDGED_INTERFACE) || (u1IfType == CFA_PIP)
            || (u1IfType == CFA_ILAN))
        {
            u1Replace = CFA_FALSE;
        }
    }

/* initialise the node with the values provided */
    TMO_SLL_Init_Node (&pStackAffectedLayer->NextEntry);
    pStackAffectedLayer->u4IfIndex = u4IfIndex;
    pStackAffectedLayer->u1StackStatus = u1StackStatus;

    CfaIfmUpdateStackForAddition (u4StackIfIndex, pStackAffectedLayer, pIfStack,
                                  u1Replace, u1Direction);

/* we update the stack status change time */
    OsixGetSysTime (&CFA_IFSTK_CHNG ());

    CFA_DBG4 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmAddStackEntry Success - "
              "interface %d in Dir %d layer %d status %d\n",
              u4IfIndex, u1Direction, u4StackIfIndex, u1StackStatus);

#ifdef MPLS_WANTED
    if ((u1StackIfType == CFA_MPLS_TUNNEL) && (u1Direction == CFA_LOW))
    {
        /* STATIC_HLSP */
        if (u1IfType == CFA_MPLS_TUNNEL)
        {
            u4L3IfIndex = CFA_IF_ENTRY (u4IfIndex)->u4HighIfIndex;
        }
        else if (u1IfType == CFA_MPLS)
        {
            CfaGetL3IfFromMplsIf (u4IfIndex, &u4L3IfIndex);
        }

        if (u4L3IfIndex != 0)
        {
            /* In the ifTable of MPLS, store u4LowerIfIndex as 
             * the u1HighType Added for the refernce of mpls purpose */
            CFA_IF_ENTRY (u4StackIfIndex)->u4HighIfIndex = (UINT2) u4L3IfIndex;
        }
    }

#endif
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmUpdateStackForAddition
 *
 *    Description        : This function performs the operations on the
 *                stack table linked lists for addition of
 *                entries.
 *
 *    Input(s)            : tStackInfoStruct *pStackLayer,
 *                tTMO_SLL *pIfStack,
 *                UINT1 u1Replace,
 *                UINT4 u4StackIfIndex,
 *                UINT1 u1StackStatus
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIfmUpdateStackForAddition (UINT4 u4IfIndex, tStackInfoStruct
                              * pStackLayer, tTMO_SLL * pIfStack,
                              UINT1 u1Replace, UINT1 u1Direction)
{
    tStackInfoStruct   *pScanNode = NULL;
    tStackInfoStruct   *pScanPrevNode = NULL;

    CFA_DBG3 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Entering CfaIfmUpdateStackForAddition "
              "for layer %d status %d replace %d\n",
              pStackLayer->u4IfIndex, pStackLayer->u1StackStatus, u1Replace);

    /* delete the existing entry if required and if it exists */
    if ((u1Replace == CFA_TRUE) && (TMO_SLL_Count (pIfStack) > 0))
    {
/* we pass a temporary pointer as the delete function changes the pIfStack
pointer */
        CfaIfmDeleteStackEntry (u4IfIndex, u1Direction,
                                CFA_IF_STACK_IFINDEX (TMO_SLL_First
                                                      (pIfStack)));

/* insert the new entry */
/* update in the existing node which was left dangling by the delete call and
release the newly allocated node */

        CFA_IF_STACK_IFINDEX (TMO_SLL_First (pIfStack)) =
            pStackLayer->u4IfIndex;
        CFA_IF_STACK_STATUS (TMO_SLL_First (pIfStack)) =
            pStackLayer->u1StackStatus;
        MemReleaseMemBlock (gCfaStackPoolId, (UINT1 *) pStackLayer);
    }
    else
    {
/* we have insert into the stack in a sorted manner */
        TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
        {
            if (pScanNode->u4IfIndex > pStackLayer->u4IfIndex)
            {
                break;
            }
            else
            {
                pScanPrevNode = pScanNode;
            }
        }

        /* check if the node is to be put in the first or last position */
        if (pScanPrevNode == NULL)
        {
            TMO_SLL_Insert (pIfStack, NULL, &pStackLayer->NextEntry);
        }
        else
        {
            if (pScanNode == NULL)
            {
                TMO_SLL_Insert (pIfStack, &pScanPrevNode->NextEntry,
                                &pStackLayer->NextEntry);
            }
            else
            {
                TMO_SLL_Insert_In_Middle (pIfStack, &pScanPrevNode->NextEntry,
                                          &pStackLayer->NextEntry,
                                          &pScanNode->NextEntry);
            }
        }
    }

}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteStackEntry
 *
 *    Description        : This function is for deleting entry in the
 *                stack table and updating the affected stack
 *                entries. Not to be called from the low level
 *                routines. If there is no other entry
 *                remaining, a entry for CFA_NONE is created.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *                UINT1 u1Direction
 *                UINT4 u4StackIfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *                gIfGlobal (Interface's global struct)
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIfmDeleteStackEntry (UINT4 u4IfIndex, UINT1 u1Direction,
                        UINT4 u4StackIfIndex)
{
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;
    UINT1               u1Replace = CFA_TRUE;
    UINT1               u1IfType = CFA_NONE;
    UINT1               u1StackIfType;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT1               u1UpdateStackStatus = CFA_FALSE;
    CFA_DBG3 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmDeleteStackEntry "
              "for interface %d in Dir %d layer %d\n",
              u4IfIndex, u1Direction, u4StackIfIndex);

    CfaGetIfType (u4IfIndex, &u1IfType);
    CfaGetIfType ((UINT4) u4StackIfIndex, &u1StackIfType);

/* check which direction stack is to change. based on this we have to see
if we have to delete the existing entry or just update the entry with
CFA_NONE. */
    if (u1Direction == CFA_HIGH)
    {
        pIfStack = &CFA_IF_STACK_HIGH (u4IfIndex);
/* we have to delete the existing entry - multiplexing is possible only for
FR/ATM/X.25 ports */
        CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
        if (((u1IfType == CFA_ENET) && (u1BridgedIfaceStatus == CFA_DISABLED))
            || (u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_BRIDGED_INTERFACE)
            || (u1IfType == CFA_PIP) || (u1IfType == CFA_ILAN)
            || (u1IfType == CFA_L3SUB_INTF))
        {
            u1Replace = CFA_FALSE;
        }

        /* Many MPLS Tunnel interfaces can be stacked over a single
         * MPLS interface. When deletion of this stacking is done through this
         * function, u1IfType is CFA_MPLS and u1StackIfType is CFA_MPLS_TUNNEL. 
         *
         * We should replace only if the higher stack entry is the last
         * entry. */
        if ((u1IfType == CFA_MPLS) && (TMO_SLL_Count (pIfStack) > 1))
        {
            u1Replace = CFA_FALSE;
        }
#ifdef HDLC_WANTED
        if (u1IfType == CFA_HDLC)
        {
            u1UpdateStackStatus = CFA_TRUE;
        }
#endif
    }
    else
    {
        pIfStack = &CFA_IF_STACK_LOW (u4IfIndex);

        /* de-multiplexing interface is possible only for MP. but if the
           interface is dangling currently - there is no interface under it right now
           then we should replace and not delete. */

        /* One TE Link interface can be stacked over many TE Link interfaces.
         * When deletion of this stacking is done through this function, 
         * u1IfType is CFA_TELINK and u1StackIfType is CFA_TELINK.
         *
         * We should replace only if the higher stack entry is the last
         * entry. */
        if (((u1IfType == CFA_RFC1483) || (u1IfType == CFA_TELINK)) &&
            (TMO_SLL_Count (pIfStack) > 1))
        {
            u1Replace = CFA_FALSE;
        }
        else
        {
            if ((u1IfType == CFA_PPP))
            {
                u1UpdateStackStatus = CFA_TRUE;
            }
        }
    }

/* now we delete the entry from the stack */
    CfaIfmUpdateStackForDeletion (pIfStack, u1Replace, u4StackIfIndex);

/* after deletion, certain types of layering have to be set to NOTREADY
state as a default */
    pScanNode = (tStackInfoStruct *) TMO_SLL_First (pIfStack);
    if (pScanNode != NULL && u1UpdateStackStatus == CFA_TRUE)
    {
        CFA_IF_STACK_STATUS (pScanNode) = CFA_RS_NOTREADY;
    }

/* there is no affected interface layer if this layer was the highest or
the lowest one */
    if (u4StackIfIndex == CFA_NONE)
    {
/* we update the stack status change time */
        OsixGetSysTime (&CFA_IFSTK_CHNG ());
        return;
    }

/* now we update the stack similarly for the affected interface layer */
    pIfStack = NULL;
    u1Replace = CFA_TRUE;

/* the direction would now be reversed for the affected entry. */
    if (u1Direction == CFA_LOW)
    {
        pIfStack = &CFA_IF_STACK_HIGH (u4StackIfIndex);
/* we have to change the direction as we would have to use the u1Direction
for the deletion of the interface. */
        u1Direction = CFA_HIGH;

        if ((u1IfType == CFA_MPLS_TUNNEL) && (TMO_SLL_Count (pIfStack) > 1))
        {
            u1Replace = CFA_FALSE;
        }
    }
    else
    {
        pIfStack = &CFA_IF_STACK_LOW (u4StackIfIndex);
        if ((u1IfType == CFA_BRIDGED_INTERFACE) || (u1IfType == CFA_PIP)
            || (u1IfType == CFA_ILAN))
        {
            u1Replace = CFA_FALSE;
        }
    }

/* now we delete the entry from the stack */
    CfaIfmUpdateStackForDeletion (pIfStack, u1Replace, u4IfIndex);

    pScanNode = (tStackInfoStruct *) TMO_SLL_First (pIfStack);
    if (pScanNode != NULL && u1UpdateStackStatus == CFA_TRUE)
    {
        CFA_IF_STACK_STATUS (pScanNode) = CFA_RS_NOTREADY;
    }

/* we update the stack status change time */
    OsixGetSysTime (&CFA_IFSTK_CHNG ());

#ifdef NPAPI_WANTED
    if ((u1StackIfType == CFA_PIP) || (u1StackIfType == CFA_BRIDGED_INTERFACE))
    {
        FsCfaHwRemovePortFromILan (u4IfIndex, u4StackIfIndex);
    }
#endif

}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmUpdateStackForDeletion
 *
 *    Description        : This function performs the operations on the
 *                stack table linked lists for deletion of
 *                entries.
 *
 *    Input(s)            : tTMO_SLL *pIfStack,
 *                UINT1 u1Replace,
 *                UINT4 u4StackIfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIfmUpdateStackForDeletion (tTMO_SLL * pIfStack, UINT1 u1Replace,
                              UINT4 u4StackIfIndex)
{
    tStackInfoStruct   *pScanNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;

    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "Entering CfaIfmUpdateStackForDeletion for layer %d replace %d\n",
              u4StackIfIndex, u1Replace);

/* now we delete or update the entry as the case may be */
    if (u1Replace == CFA_TRUE)
    {
        pTempNode = (tTMO_SLL_NODE *) TMO_SLL_First (pIfStack);
/* we make ifIndex as CFA_NONE and RowStatus as Not in service */
        if (pTempNode == NULL)
        {
            return;
        }

        CFA_IF_STACK_IFINDEX (pTempNode) = CFA_NONE;
        CFA_IF_STACK_STATUS (pTempNode) = CFA_RS_NOTINSERVICE;
    }
    else
    {
/* we have to delete the node */
/* we have to scan the stack to find the node */
        TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
        {
            if (pScanNode->u4IfIndex == u4StackIfIndex)
            {
                break;
            }
        }

        if (pScanNode == NULL)
        {
            return;
        }

        TMO_SLL_Delete (pIfStack, &pScanNode->NextEntry);
        if (pScanNode != NULL)
        {
            MemReleaseMemBlock (gCfaStackPoolId, (UINT1 *) pScanNode);
        }
    }
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmAddRcvAddrEntry
 *
 *    Description        : This function is for creating entry in the
 *                Receive Address table. Not to be called from
 *                the low level routines.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *                UINT1 *au1HwAddr,
 *                UINT1 u1AddrType
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if entry is added
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmAddRcvAddrEntry (UINT4 u4IfIndex, UINT1 *au1HwAddr, UINT1 u1AddrType)
{
    tRcvAddressInfo    *pRcvAddr = NULL;
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;
    tRcvAddressInfo    *pScanPrevNode = NULL;

    CFA_DBG1 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmAddRcvAddrEntry for interface %d \n", u4IfIndex);

/* allocate for the new node - if unsuccessful then return failure. */
    if ((pRcvAddr = MemAllocMemBlk (gCfaRcvAddrPoolId)) == NULL)
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmAddRcvAddrEntry - "
                  "Malloc fail - interface %d\n", u4IfIndex);

        return (CFA_FAILURE);
    }

/* initialise the node with the values provided */
    TMO_SLL_Init_Node (&pRcvAddr->NextEntry);
    pRcvAddr->u1AddressStatus = CFA_RS_ACTIVE;
    pRcvAddr->u1AddressType = u1AddrType;
    MEMCPY (pRcvAddr->au1Address, au1HwAddr, CFA_MAX_MEDIA_ADDR_LEN);

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

/* we have insert into the table in a sorted manner */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, au1HwAddr,
                    CFA_MAX_MEDIA_ADDR_LEN) > 0)
            break;
        else
            pScanPrevNode = pScanNode;
    }

    /* check if the node is to be put in the first or last position */
    if (pScanPrevNode == NULL)
        TMO_SLL_Insert (pIfRcvAddrTab, NULL, &pRcvAddr->NextEntry);
    else
    {
        if (pScanNode == NULL)
            TMO_SLL_Insert (pIfRcvAddrTab, &pScanPrevNode->NextEntry,
                            &pRcvAddr->NextEntry);
        else
            TMO_SLL_Insert_In_Middle (pIfRcvAddrTab, &pScanPrevNode->NextEntry,
                                      &pRcvAddr->NextEntry,
                                      &pScanNode->NextEntry);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmAddRcvAddrEntry Success - interface %d\n", u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeleteRcvAddrEntry
 *
 *    Description        : This function is for deleting entry from the
 *                Receive Address table. Not to be called from
 *                the low level routines.
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *                UINT1 *au1HwAddr,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
VOID
CfaIfmDeleteRcvAddrEntry (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    tTMO_SLL           *pIfRcvAddrTab = NULL;
    tRcvAddressInfo    *pScanNode = NULL;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeleteRcvAddrEntry for interface %d \n", u4IfIndex);

    pIfRcvAddrTab = &CFA_IF_RCVADDRTAB (u4IfIndex);

/* we have to scan the stack to find the node */
    TMO_SLL_Scan (pIfRcvAddrTab, pScanNode, tRcvAddressInfo *)
    {
        if (MEMCMP (pScanNode->au1Address, au1HwAddr,
                    CFA_MAX_MEDIA_ADDR_LEN) == 0)
            break;
    }

    if (pScanNode == NULL)
    {
        return;
    }

    TMO_SLL_Delete (pIfRcvAddrTab, &pScanNode->NextEntry);
    if (pScanNode != NULL)
        MemReleaseMemBlock (gCfaRcvAddrPoolId, (UINT1 *) pScanNode);

}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmChangeInterfaceStatus
 *
 *    Description        : This function identifies the admin and oper
 *                status to be set for a particular interface
 *                on the basis of the existing status and the
 *                property of the interface (e.g. demand and
 *                automatic ckt.).
 *
 *                Currently we do not support testing and
 *                not-present states of OperStatus and the
 *                testing state of AdminStatus
 *
 *    Input(s)            : UINT4 u4IfIndex.
 *                UINT1 u1AdminStatus,
 *                UINT1 u1OperStatus,
 *                UINT1 u1IsFromMib,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if status is updated
 *                successfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmChangeInterfaceStatus (UINT4 u4IfIndex, UINT1 u1AdminStatus,
                             UINT1 *pu1InOperStatus, UINT1 u1IsFromMib)
{
    tStackInfoStruct   *pScanNode;
    tCfaIfInfo          IfInfo;
    INT4                i4RetVal = CFA_SUCCESS;
    UINT1               u1IsLowerLayerDown = CFA_TRUE;
    UINT1               u1IsLowerLayerDormant = CFA_FALSE;
    UINT1               u1IsRegToIp = CFA_FALSE;
    UINT1               u1OperStatus = *pu1InOperStatus;
    UINT1               u1TmpOperStatus = CFA_IF_DOWN;
    UINT1               u1IfType;

    CFA_DBG3 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmChangeInterfaceStatus for "
              "interface %d admin %d oper %d\n",
              u4IfIndex, u1AdminStatus, u1OperStatus);

    CfaGetIfType (u4IfIndex, &u1IfType);

    /* Get the interface information for all the calls inside */
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }

    if (IfInfo.i4IpPort != CFA_INVALID_INDEX)
    {
        u1IsRegToIp = CFA_TRUE;
    }

/* if admin is down then the oper has to be down too. we make the oper down
immediately without waiting for the pending packets to be sent - the pending
packets may be discarded. */
    if (u1AdminStatus == CFA_IF_DOWN)
    {
        if (CFA_IF_ADMIN (u4IfIndex) == CFA_IF_DOWN)
        {
            /* No change in admin status */

            if (u1OperStatus != IfInfo.u1IfOperStatus)
            {
                /* Change in Oper Status */
                /* Oper Status can be either 'Down' or 'Not Present' when
                 * Admin Status is down */
                if ((u1OperStatus == CFA_IF_DOWN) ||
                    (u1OperStatus == CFA_IF_NP))
                {
                    CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
                }

                if (u1OperStatus == CFA_IF_DOWN)
                {
                    /* CFA_IF_NP -> CFA_IF_DOWN - this case cannot occur */
                }
                else if (u1OperStatus == CFA_IF_NP)
                {
                    /* IF_DOWN -> IF_NP - Notify Protocols as Oper Down */
                    if ((i4RetVal = CfaIfmNotifyInterfaceStatus (u4IfIndex,
                                                                 u1AdminStatus,
                                                                 &u1OperStatus,
                                                                 u1IsFromMib,
                                                                 u1IsRegToIp,
                                                                 &IfInfo))
                        != CFA_SUCCESS)
                    {
                        /* error in making link down */
                    }
                }

            }

            return CFA_SUCCESS;
        }
        if (IfInfo.u1IfType == CFA_L3IPVLAN)
        {
#ifdef SYSLOG_WANTED
            SyslogNotifyIfStatusChange (u4IfIndex);
#endif
        }
        /* API for LinkDown trap generation - Before bringing the interface
           to down state. Both Admin and Oper status of the interface is DOWN */
        if ((IfInfo.u1IfType != CFA_LAGG)
            && (CFA_IF_TRAP_EN (u4IfIndex) == CFA_ENABLED)
            && (u1OperStatus == CFA_IF_UP))
        {
            CfaSnmpifSendTrap (u4IfIndex, CFA_IF_DOWN, u1AdminStatus);
        }

        if (IfInfo.u1IfType != CFA_LAGG)
        {
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CfaSysLogId,
                          "%s Link Status [DOWN]", IfInfo.au1IfName));
        }

        /* if Oper status is 'not Present', then don't change it */
        if (IfInfo.u1IfOperStatus != CFA_IF_NP)
        {
            u1OperStatus = CFA_IF_DOWN;
        }
        CfaSetIfOperStatus (u4IfIndex, u1OperStatus);

        if ((i4RetVal = CfaIfmNotifyInterfaceStatus (u4IfIndex, u1AdminStatus,
                                                     &u1OperStatus, u1IsFromMib,
                                                     u1IsRegToIp, &IfInfo)) !=
            CFA_SUCCESS)
        {
/* error in making link down */
        }
        *pu1InOperStatus = u1OperStatus;
    }
    else
    {
/* admin is UP - we now have to decide the operstatus
on the basis of the type of interface (for dormant) and the
previous state of Admin and Oper status */

/* if admin has become up from a previous state of down, then the oper
should change to up (normally), dormant (for demand ckts or when lower
interface is dormant) or lower-layer-down
(if all the interfaces immediately below are also oper down). 
This updation is done after the respective protocol modules notify CFA of
the success, till then the oper is UNK. If there
is any problem then some other mechanism should bring the oper down
through a explicit operation. */
        if (u1AdminStatus != CFA_IF_ADMIN (u4IfIndex))
        {
            /* CHK this part of Chassis change, could not comment why the
             * changes are! Eerlier we were returing from here after setting
             * *pu1InOperStatus.
             */
            if (IfInfo.u1IfOperStatus == CFA_IF_NP)
            {
                *pu1InOperStatus = IfInfo.u1IfOperStatus;
            }

/* we now check for the rest of the conditions */
            TMO_SLL_Scan (&CFA_IF_STACK_LOW (u4IfIndex), pScanNode,
                          tStackInfoStruct *)
            {
                if (pScanNode->u4IfIndex == CFA_NONE)
                {
/* this is the lowest layer and we dont need to check - its up */
                    u1IsLowerLayerDown = CFA_FALSE;
                    break;
                }

                CfaGetIfOperStatus (pScanNode->u4IfIndex, &u1TmpOperStatus);
                if (u1TmpOperStatus == CFA_IF_UP)
                {
/* at least one lower interface is up - we can make it up */
                    u1IsLowerLayerDown = CFA_FALSE;
                    break;
                }
                if (u1TmpOperStatus == CFA_IF_DORM)
                {
/* at least one lower interface is in dormant state - but we continue to
look for an interface which is up */
                    u1IsLowerLayerDormant = CFA_TRUE;
                    continue;
                }
            }                    /* end of lower stack scan */

            if (u1IsFromMib != CFA_IF_LINK_STATUS_CHANGE)
            {
                if (u1IsLowerLayerDown == CFA_FALSE)
                {
                    if (u1IfType == CFA_PPP)
                    {
                        /* Lower layer is UP.
                         *                          * Yet to see if this layer is UP */
                        u1OperStatus = CFA_IF_UNK;
                    }
                    else if ((u1IfType == CFA_PSEUDO_WIRE)
                             || (u1IfType == CFA_CAPWAP_DOT11_BSS))
                    {
                        u1OperStatus = CFA_IF_DOWN;
                    }
                    else
                    {
                        /* If lower layers are up this layer is
                         *                          * automatically up */
                        u1OperStatus = CFA_IF_UP;
                    }
                }
                else if (u1IsLowerLayerDormant == CFA_TRUE)
                {
                    u1OperStatus = CFA_IF_DORM;
                }
                else
                {
                    u1OperStatus = CFA_IF_LLDOWN;
                }
            }

            if ((i4RetVal =
                 CfaIfmNotifyInterfaceStatus (u4IfIndex, u1AdminStatus,
                                              &u1OperStatus, u1IsFromMib,
                                              u1IsRegToIp,
                                              &IfInfo)) != CFA_SUCCESS)
            {
/* error in making link up/down/dormant/etc/ */
            }
            else
            {
/* if Oper was made up then send linkup trap */
                if ((IfInfo.u1IfType != CFA_LAGG) &&
                    (CFA_IF_TRAP_EN (u4IfIndex) == CFA_ENABLED) &&
                    (u1OperStatus == CFA_IF_UP))
                {
/* API for LinkUp trap generation here - after bringing the interface
out of down state. */

                    CfaSnmpifSendTrap (u4IfIndex, CFA_IF_UP, u1AdminStatus);
                }
                if (IfInfo.u1IfType != CFA_LAGG)
                {
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CfaSysLogId,
                                  "%s Link Status [%s]", IfInfo.au1IfName,
                                  ((u1OperStatus ==
                                    CFA_IF_UP) ? "UP" : "DOWN")));
                }
            }                    /* successfully notified status to protocol module */

            *pu1InOperStatus = u1OperStatus;
        }                        /* end of change in admin status */
        else
        {
/* this implies that the admin status has not changed and this is
only a oper status update. we simply copy the new oper status. Hence,
we dont need to do anything with the oper status here - we simply have
to generate the link traps. */

            if (u1OperStatus == IfInfo.u1IfOperStatus)
            {
/* we dont have to do anything in this case - just return success. we dont
expect this to happen */
                i4RetVal = CFA_SUCCESS;
            }
            else
            {
/* Oper Status change will come only from some protocol module or driver. */

                if ((u1OperStatus == CFA_IF_DOWN) ||
#ifdef MBSM_WANTED
                    (u1OperStatus == CFA_IF_NP) ||
#endif
                    (u1OperStatus == CFA_IF_LLDOWN))
                {
/* Generate trap before bringing the interface down */
                    if (CFA_IF_TRAP_EN (u4IfIndex) == CFA_ENABLED)
                    {
/* API for LinkDown trap generation here - before bringing the interface
down. */
                        CfaSnmpifSendTrap (u4IfIndex, CFA_IF_DOWN,
                                           u1AdminStatus);

                    }

                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CfaSysLogId,
                                  "%s Link Status [DOWN]", IfInfo.au1IfName));

                    if ((i4RetVal = CfaIfmNotifyInterfaceStatus (u4IfIndex,
                                                                 u1AdminStatus,
                                                                 &u1OperStatus,
                                                                 u1IsFromMib,
                                                                 u1IsRegToIp,
                                                                 &IfInfo))
                        != CFA_SUCCESS)
                    {
/* error in making link up/down/dormant/etc/ */
                    }
                }
                else
                {
                    if (u1OperStatus == CFA_IF_UP)
                    {
/* the interface is moving out of down state - generate Trap after making
the status change */
                        if ((i4RetVal = CfaIfmNotifyInterfaceStatus (u4IfIndex,
                                                                     u1AdminStatus,
                                                                     &u1OperStatus,
                                                                     u1IsFromMib,
                                                                     u1IsRegToIp,
                                                                     &IfInfo))
                            != CFA_SUCCESS)
                        {
/* error in making link up/down/dormant/etc/ */
                        }
                        else
                        {
                            if ((CFA_IF_TRAP_EN (u4IfIndex) == CFA_ENABLED) &&
                                (u1OperStatus == CFA_IF_UP))
                            {
/* API for LinkUp trap generation here - after bringing the interface
out of down state. */
                                CfaSnmpifSendTrap (u4IfIndex, CFA_IF_UP,
                                                   u1AdminStatus);
                            }
                            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4CfaSysLogId,
                                          "%s Link Status [UP]",
                                          IfInfo.au1IfName));
                        }        /* end of successfully notified status to protocol module */
                    }
                    else
                    {
/* no need to generate trap - just update as this is some status other than
up or down */
                        if ((i4RetVal = CfaIfmNotifyInterfaceStatus (u4IfIndex,
                                                                     u1AdminStatus,
                                                                     &u1OperStatus,
                                                                     u1IsFromMib,
                                                                     u1IsRegToIp,
                                                                     &IfInfo))
                            != CFA_SUCCESS)
                        {
/* error in making link up/down/dormant/etc/ */
                        }
                    }            /* end of just update */
                }                /* end of bringing the interface up */

                *pu1InOperStatus = u1OperStatus;

            }                    /* end of oper status change */
        }                        /* end of no change in admin status */
    }                            /* end of find the admin and oper status to be updated */

    if (i4RetVal == CFA_SUCCESS)
    {
        CFA_DBG3 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmChangeInterfaceStatus "
                  "for interface %d admin %d oper %d - Success\n",
                  u4IfIndex, u1AdminStatus, u1OperStatus);
    }
    else
    {
        CFA_DBG3 (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_IFM,
                  "In CfaIfmChangeInterfaceStatus "
                  "for interface %d admin %d oper %d - Failure\n",
                  u4IfIndex, u1AdminStatus, u1OperStatus);
    }

    return (i4RetVal);

}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmNotifyInterfaceStatus
 *
 *    Description        : This function is for updating the status of
 *                given interface. It also calls the
 *                appropriate API for updating the status in
 *                IP and the data-link layer protocol modules.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1AdminStatus,
 *                UINT1 u1OperStatus,
 *                UINT1 u1IsFromMib,
 *                UINT1 u1IsRegToIp
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *                gIfGlobal (Interface's global struct)
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if status is updated
 *                successfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmNotifyInterfaceStatus (UINT4 u4IfIndex, UINT1 u1AdminStatus,
                             UINT1 *pu1InOperStatus, UINT1 u1IsFromMib,
                             UINT1 u1IsRegToIp, tCfaIfInfo * pIfInfo)
{
    INT4                i4RetVal = CFA_SUCCESS;
    UINT1               u1CurOperStatus = CFA_NONE;
    UINT1               u1CurAdminStatus = 0;
    UINT1               u1OperStatus = *pu1InOperStatus;
    UINT1               u1PromiscMode;
    UINT1               u1SubType = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4ACPort = 0;
    UINT2               u2LocalPortId;
    UINT1               u1IfType = 0;
    tCfaIfInfo         *pCfaIfSbpInfo = NULL;
    UINT2               u2VlanId = 0;
    UINT4               u4ParentIfIndex = 0;
    tVlanEvbSbpArray    SbpArray;
    UINT1               u1BrgPortType = 0;
    UINT1               u1LoopIndex = 0;
    UINT1               u1ParentOperStatus = 0;
    UINT1               u1TempOperStatus = 0;
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    UINT4               u4LnxVlanIfIndex;
    UINT1               u1OldOperStatus;
    UINT1               u1NewOperStatus;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

#endif
#ifdef NPAPI_WANTED
    tCfaIfInfo          IfInfo;
    UINT4               u4IfSpeed = 0;
    UINT4               u4Value = 0;
    tEthStats           EthStats;

#endif

    tCfaRegInfo         CfaInfo;
#ifdef PPP_WANTED
    UINT1               u1BridgedIfaceStatus;
#endif

#if defined (WLC_WANTED) || defined (WTP_WANTED)
    tRadioIfMsgStruct   RadioIfMsgStruct;
#endif
#ifdef MPLS_WANTED
    UINT4               u4MplsIfIndex = 0;
#endif
#if !defined (IP_WANTED) && !defined (LNXIP4_WANTED)
    UNUSED_PARAM (u1IsRegToIp);
#endif
#ifdef LNXIP6_WANTED
    tLip6If            *pIf6Entry = NULL;
    UINT4               u4TmpDefLifetime = 0;
#endif
#ifdef MBSM_WANTED
    UINT4               u4IfUfdGroupId = 0;
    UINT1               u1IfUfdOperStatus = 0;
#endif

    CFA_DBG3 (CFA_TRC_ALL, CFA_IFM,
              "Entering CfaIfmNotifyInterfaceStatus "
              "for interface %d admin %d oper %d\n",
              u4IfIndex, u1AdminStatus, u1OperStatus);
#ifdef NPAPI_WANTED

    /* Bring up the Hardware port before intimating upper layers */
    if ((((pIfInfo->u1IfType == CFA_ENET)
          || (pIfInfo->u1IfType == CFA_LAGG)
          || (pIfInfo->u1IfType == CFA_PSEUDO_WIRE)
          || (pIfInfo->u1IfType == CFA_VXLAN_NVE)
          || ((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE)
              && (pIfInfo->u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)))
         || (CfaIsVirtualInterface (u4IfIndex) == CFA_TRUE))
        && (CfaIsMgmtPort (u4IfIndex) == FALSE)
#ifdef WLC_WANTED
        || (pIfInfo->u1IfType == CFA_RADIO)
#endif
        )
    {
#ifdef MBSM_WANTED
        if (gu4UfdPortFlag != 1)
        {
            if (pIfInfo->u1IfType == CFA_ENET || pIfInfo->u1IfType == CFA_LAGG)
            {
                /* Check if port already present in group */
                if (CfaGetIfUfdGroupId (u4IfIndex, &u4IfUfdGroupId) !=
                    CFA_SUCCESS)
                {
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_UFD,
                              "CfaIfmNotifyInterfaceStatus - Given port %d is not existing\r\n",
                              u4IfIndex);
                }
                if (u4IfUfdGroupId != 0)
                {
                    /* Get ufd operstatus of port */
                    if (CfaGetIfUfdOperStatus (u4IfIndex, &u1IfUfdOperStatus) !=
                        CFA_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR, CFA_UFD,
                                 "CfaIfmNotifyInterfaceStatus - Failure in getting "
                                 "UFD port oper status\n");
                    }
                    if (u1IfUfdOperStatus == CFA_IF_UFD_ERR_DISABLED)
                    {
                        u1AdminStatus = CFA_IF_UP;
                    }
                }
            }
        }
#endif

        if (u1AdminStatus == CFA_IF_UP)
        {
#ifdef MBSM_WANTED
            if (u1OperStatus != CFA_IF_NP)
            {
#endif
                if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
                {
                    /* 
                     * When a node becomes active from standby, config will
                     * be restored before GO_ACTIVE event is received.
                     * At this time CFA node status is CFA_NODE_STANDBY and 
                     * here NP programming should not be done. 
                     */
                    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                    {
#ifdef MBSM_WANTED
                        /* Stack Ports admin state will be always UP in BCM.
                         * No need to set the same status*/
                        if ((u4IfIndex < (UINT4) CFA_STACK_PORT_START_INDEX) ||
                            (u4IfIndex >
                             (UINT4) (MBSM_MAX_PORTS_PER_SLOT +
                                      ISS_MAX_STK_PORTS)))
                        {
                            /* NOTE:Hardware updation of Admin status is allowed even if
                               there is no change with the existing status,
                               considering certain scenarios, where the port is pre -
                               configured when the card is unavailable.
                               This avoids restricting hardware programming for the
                               card, when it is inserted later. Hence,this hardware call 
                               cannot be blocked, when the Oper-status has alone been changed 
                               and no change in admin status. */

                            if (CfaFsHwUpdateAdminStatusChange
                                (u4IfIndex, FNP_TRUE) != FNP_SUCCESS)
                            {

                                /* if Hardware port could not bring up then no need to 
                                 * intimate Admin status change to the upper layer 
                                 */
                                CfaGetIfType (u4IfIndex, &u1IfType);
                                if (u1IfType != CFA_VXLAN_NVE)
                                {
                                    return CFA_FAILURE;
                                }
                            }
                        }
#endif
                    }
                    else
                    {
                        if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,
                                                            FNP_TRUE) !=
                            FNP_SUCCESS)
                        {
                            /* if Hardware port could not bring up then no need to 
                             * intimate Admin status change to the upper layer 
                             */
                            CfaGetIfType (u4IfIndex, &u1IfType);
                            if (u1IfType != CFA_VXLAN_NVE)
                            {
                                return CFA_FAILURE;
                            }
                        }
                    }
                }

            }
#ifdef MBSM_WANTED
        }
#endif
    }

#endif

    if ((pIfInfo->u1BrgPortType == CFA_UPLINK_ACCESS_PORT) &&
        (CFA_UFD_MODULE_STATUS () == CFA_UFD_ENABLED))
    {
        CFA_DBG1 (ALL_FAILURE_TRC, CFA_UFD,
                  "OperStatus Indication should be sent to other modules "
                  " when the Downlink port goes to Err Disabled state - %d \n",
                  u4IfIndex);
        u1IsFromMib = CFA_TRUE;
    }

    CfaGetIfOperStatus (u4IfIndex, &u1CurOperStatus);
    CfaGetIfBrgPortType (u4IfIndex, &u1BrgPortType);

    switch (pIfInfo->u1IfType)
    {

        case CFA_TUNNEL:
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);

            if (u1AdminStatus == CFA_IF_UP)
            {
                if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD, u4IfIndex, 0,
                                                  NULL) != CFA_SUCCESS)
                {
                    /* unsuccessful in registration IP -
                     * but we dont fail the interface creation*/
                    CFA_DBG1 (ALL_FAILURE_TRC, CFA_IFM,
                              "Error in CfaIfmNotifyInterfaceStatus - "
                              "IP Registration fail interface %d\n", u4IfIndex);
                }
            }
            break;
        case CFA_LAGG:
            if (pIfInfo->u1BridgedIface == CFA_ENABLED)
            {
                CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
                CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
                /* Check with la if any of the ports in the
                 * port channel is active.
                 */
                LaUpdatePortChannelAdminStatus ((UINT2) u4IfIndex,
                                                u1AdminStatus, &u1OperStatus);

                CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
                *pu1InOperStatus = u1OperStatus;
            }
            else
            {
                CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
                CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
                CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
                *pu1InOperStatus = u1OperStatus;
            }
            /* FIX for Bug 7329 - The admin status of the LTD interface is
             * not down when the LTM interface is down and the UFD group is
             * down */
            if (u1IsFromMib == CFA_TRUE && gu4UfdPortFlag != 1)
            {
                CfaGetIfOperStatus (u4IfIndex, &u1CurOperStatus);
                CfaGetCdbPortAdminStatus (u4IfIndex, &u1CurAdminStatus);
                if (u1CurAdminStatus == CFA_IF_UP &&
                    u1CurOperStatus == CFA_IF_UP)
                {
                    if (CfaNotifyUfdOperStatusUpdate (u4IfIndex, CFA_IF_UP)
                        != CFA_SUCCESS)
                    {
                        CFA_DBG1 (ALL_FAILURE_TRC, CFA_IFM,
                                  "Error in CfaNotifyUfdOperStatusUpdate - "
                                  "UFD Oper Status notify of the interface %d failed\n",
                                  u4IfIndex);
                    }
                }
                else
                {
                    if (CfaNotifyUfdOperStatusUpdate (u4IfIndex, CFA_IF_DOWN)
                        != CFA_SUCCESS)
                    {
                        CFA_DBG1 (ALL_FAILURE_TRC, CFA_IFM,
                                  "Error in CfaNotifyUfdOperStatusUpdate - "
                                  "UFD Oper Status notify of the interface %d failed\n",
                                  u4IfIndex);
                    }
                }
            }
            break;
        case CFA_ENET:

#ifdef LNXIP6_WANTED
            /*  Need to send RA by setting lifetime to 0 before disabling the interface
             *  1. With the index, try resetting the Lifetime to 0.
             *  2. Manually trigger to send RA
             */
            pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
            if (pIf6Entry != NULL)
            {
                u4TmpDefLifetime = pIf6Entry->u4DefRaLifetime;
                if (u1AdminStatus == CFA_IF_DOWN)
                {
                    pIf6Entry->u4DefRaLifetime = 0;
                }
                Lip6RAConfig (pIf6Entry->u4ContextId);
            }
#endif

            /* Currently, ENET alone supports Link status handling */
            if (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE)
            {
                /*Admin status set to UP, now update Oper status with the
                 *vlaue provided. u1AdminStatus will always have value = 
                 CFA_IF_UP.*/

                CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
                CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
                CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);

                if (u1OperStatus == CFA_IF_UP)
                {
                    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
                    {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                        /*Enable promiscous mode forOOB Interface */
                        CfaGddOobConfigPort (u4IfIndex, CFA_ENET_EN_PROMISC);
#endif
                    }
                    else
                    {
                        /*Enable promiscous mode for the port */
                        CfaGddConfigPort (u4IfIndex, CFA_ENET_EN_PROMISC, NULL);
                    }

                    /* put the new port in the polling table */
                    CfaGddOsAddIfToList (u4IfIndex);
#ifdef NPAPI_WANTED
                    /* Retrieving the interface info from hardware is
                     * done at active node and synced to the standby unit. */
                    if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
                    {
                        /* In case of Hardwares with half duplexity ports, the speed 
                         * update to CFA is done here when the port becomes full
                         * duplexity on connectivity */

                        /* Update the speed for the interface */
                        CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4Value);
                        /* if auto-neg is enabled set the speed-else donot set it in datbase */
                        MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
                        if ((CfaGetIfInfo (u4IfIndex, &IfInfo)) == CFA_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }

                        if (IfInfo.u1AutoNegStatus == CFA_ENABLED)

                        {
                            CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
                            CfaSetIfHighSpeed (u4IfIndex, u4Value);
                        }
                        /* Update the Duplex status */
                        MEMSET (&EthStats, 0, sizeof (tEthStats));
                        EthStats.u4ReqType = NP_STAT_PORT_DUPLEX_STATUS;
                        CfaFsEtherHwGetStats (u4IfIndex, &EthStats);
                        u4Value = EthStats.u4DuplexStatus;
                        CfaSetIfDuplexStatus (u4IfIndex, (INT4) u4Value);
                    }
#endif /* NPAPI_WANTED */
                }
                else
                {
                    /* Store the promiscuous mode as the close would disable the */
                    /* promiscuous mode and restore after the close call */
                    u1PromiscMode = CFA_IF_PROMISC (u4IfIndex);
                    if (CfaIsMgmtPort (u4IfIndex) == TRUE)
                    {
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                        /*Disable promiscous mode forOOB Interface */
                        CfaGddOobConfigPort (u4IfIndex, CFA_ENET_DIS_PROMIS);
#endif
                    }
                    else
                    {
                        /*Disable promiscous mode for the port */
                        CfaGddConfigPort (u4IfIndex, CFA_ENET_DIS_PROMIS, NULL);
                    }

                    /* remove the port from the polling table */
                    CfaGddOsRemoveIfFromList (u4IfIndex);
                    /* restore the promiscuous mode */
                    CFA_IF_PROMISC (u4IfIndex) = u1PromiscMode;
                }
                if (CfaIsParentPort (u4IfIndex) == CFA_TRUE)

                {
                    CfaL3SubIfShutDownNotify (u4IfIndex, u1OperStatus);
                }

                if (gu4UfdPortFlag != 1)
                {
                    CfaGetIfOperStatus (u4IfIndex, &u1CurOperStatus);
                    CfaGetCdbPortAdminStatus (u4IfIndex, &u1CurAdminStatus);
                    if (u1CurAdminStatus == CFA_IF_UP &&
                        u1CurOperStatus == CFA_IF_UP)
                    {
                        if (CfaNotifyUfdOperStatusUpdate (u4IfIndex, CFA_IF_UP)
                            != CFA_SUCCESS)
                        {
                            CFA_DBG1 (ALL_FAILURE_TRC, CFA_IFM,
                                      "Error in CfaNotifyUfdOperStatusUpdate - "
                                      "UFD Oper Status notify of the interface %d failed\n",
                                      u4IfIndex);
                        }
                    }
                    else
                    {
                        if (CfaNotifyUfdOperStatusUpdate
                            (u4IfIndex, CFA_IF_DOWN) != CFA_SUCCESS)
                        {
                            CFA_DBG1 (ALL_FAILURE_TRC, CFA_IFM,
                                      "Error in CfaNotifyUfdOperStatusUpdate - "
                                      "UFD Oper Status notify of the interface %d failed\n",
                                      u4IfIndex);
                        }
                    }
                }
                break;
            }
            else if (u1IsFromMib == CFA_TRUE)
            {
#ifdef NPAPI_WANTED
                if (CFA_NODE_STATUS () == CFA_NODE_ACTIVE)
                {
                    /* In case of Hardwares with half duplexity ports, the speed 
                     * update to CFA is done here when the port becomes full
                     * duplexity on connectivity */

                    /* Update the speed for the interface */
                    CfaDetermineSpeed (u4IfIndex, &u4IfSpeed, &u4Value);
                    /* if auto-neg is enabled set the speed-else donot set it in datbase */
                    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
                    if ((CfaGetIfInfo (u4IfIndex, &IfInfo)) == CFA_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    if (IfInfo.u1AutoNegStatus == CFA_ENABLED)
                    {
                        CfaSetIfSpeed (u4IfIndex, u4IfSpeed);
                        CfaSetIfHighSpeed (u4IfIndex, u4Value);
                    }
                }
#endif
                if (CfaIsParentPort (u4IfIndex) == CFA_TRUE)

                {
                    CfaL3SubIfShutDownNotify (u4IfIndex, u1OperStatus);
                }

            }

/* These interfaces' operstatus will be updated directly by the
GDD */
            if (u1AdminStatus == CFA_IF_UP)
            {
                if (CfaGddOpenPort (u4IfIndex) != CFA_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
            }
            else
            {
                if (CfaGddClosePort (u4IfIndex) != CFA_SUCCESS)
                {
                    i4RetVal = CFA_FAILURE;
                }
            }
            CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
            /* Attachment Circuit interface's oper status depends on the 
             * oper status of underlying physical port. So whenever, oper
             * status of physical port changes, it is necessary to update 
             * the oper status of respective attachment circuit interface(s)
             */
            CfaUpdateACOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);

            /* FIX for Bug 7329 - The admin status of the LTD interface is
             * not down when the LTM interface is down and the UFD group is
             * down */

            if (u1IsFromMib == CFA_TRUE && gu4UfdPortFlag != 1)
            {
                CfaGetIfOperStatus (u4IfIndex, &u1CurOperStatus);
                CfaGetCdbPortAdminStatus (u4IfIndex, &u1CurAdminStatus);
                if (u1CurAdminStatus == CFA_IF_UP &&
                    u1CurOperStatus == CFA_IF_UP)
                {
                    if (CfaNotifyUfdOperStatusUpdate (u4IfIndex, CFA_IF_UP)
                        != CFA_SUCCESS)
                    {
                        CFA_DBG1 (ALL_FAILURE_TRC, CFA_IFM,
                                  "Error in CfaNotifyUfdOperStatusUpdate - "
                                  "UFD Oper Status notify of the interface %d failed\n",
                                  u4IfIndex);
                    }
                }
                else
                {
                    if (CfaNotifyUfdOperStatusUpdate (u4IfIndex, CFA_IF_DOWN)
                        != CFA_SUCCESS)
                    {
                        CFA_DBG1 (ALL_FAILURE_TRC, CFA_IFM,
                                  "Error in CfaNotifyUfdOperStatusUpdate - "
                                  "UFD Oper Status notify of the interface %d failed\n",
                                  u4IfIndex);
                    }
                }
            }

            if (u1IsFromMib == CFA_TRUE)
            {
                /* Flush all the Split-Horizon Rules and Reconfigure */

                if (CFA_SH_MODULE_STATUS == CFA_SH_ENABLED)
                {
                    /* Remove all th entries from the table
                     * Passing the Macro CFA_SH_DISABLED - to remove the entries */
                    if (CfaShDisable (CFA_SH_DISABLED) != CFA_SUCCESS)
                    {
                        return CFA_FAILURE;
                    }
                    /* Reconfigure all the split-horizon rules */

                    if (CfaShEnable (CFA_SH_ENABLED) != CFA_SUCCESS)
                    {
                        return CFA_FAILURE;
                    }

                }

            }

            CfaGetIfSubType (u4IfIndex, &u1SubType);
            if (u1SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
            {
#ifdef OPENFLOW_WANTED
                if ((OpenflowNotifyPortStatus (u4IfIndex, (INT1) u1OperStatus))
                    == OFC_FAILURE)
                {
                    return CFA_FAILURE;
                }
#endif
            }
#ifdef LNXIP6_WANTED
            if (pIf6Entry != NULL)
            {
                pIf6Entry->u4DefRaLifetime = u4TmpDefLifetime;
            }
#endif
            break;

#ifdef MPLS_WANTED
        case CFA_MPLS:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            if (u1AdminStatus == CFA_IF_DOWN)
            {
                u1OperStatus = CFA_IF_DOWN;
            }
            else
            {
                /* Get the status of underlying layers (L3 Interface or 
                 * TE Link Interface) */
                u1OperStatus = CfaGetLLIfOperStatusFromMplsIf (u4IfIndex);
                if (u1OperStatus == CFA_IF_NP)
                {
                    u1OperStatus = CFA_IF_DOWN;
                }
            }
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;
        case CFA_MPLS_TUNNEL:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            if (u1AdminStatus == CFA_IF_DOWN)
            {
                u1OperStatus = CFA_IF_DOWN;
            }
            else
            {
                if (CfaUtilGetMplsIfFromMplsTnlIf (u4IfIndex,
                                                   &u4MplsIfIndex, FALSE) ==
                    CFA_FAILURE)
                {
                    u1OperStatus = CFA_IF_DOWN;
                }
                else
                {
                    CfaGetIfOperStatus (u4MplsIfIndex, &u1OperStatus);
                }
            }
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;
#endif /* MPLS_WANTED */

        case CFA_PIP:
        case CFA_BRIDGED_INTERFACE:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;

        case CFA_LOOPBACK:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            /* Oper status is the same as admin status for loopback 
             * interface*/
            u1OperStatus = u1AdminStatus;
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;

        case CFA_L3SUB_INTF:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            /* Incrementing the ports count in the respective L2 VLAN */
            VlanIvrGetVlanIfOperStatus (u4IfIndex, &u1TempOperStatus);
            if (CfaGetVlanId (u4IfIndex, &u2VlanId) == CFA_SUCCESS)
            {
                /* Get the Parent(Physical port) interface index from logical index */
                if (CfaGetL3SubIfParentIndex (u4IfIndex, &u4ParentIfIndex)
                    == CFA_FAILURE)
                {
                    CFA_DBG (CFA_TRC_ERROR, CFA_MAIN,
                             "CfaGetL3SubIfParentIndex: Unable to get Parent IfIndex");
                    return CFA_FAILURE;
                }
                /* Check whether the Parent port is the member of this
                 * VLAN. If not a member , then make the respective
                 * l3subinterface index */

                if ((u1OperStatus == CFA_IF_UP) &&
                    (CfaVlanIsMemberPort ((UINT4) u2VlanId,
                                          u4ParentIfIndex) == CFA_SUCCESS))
                {
                    /* OperStatus of L3Sub-If should be made up only if the Parent
                     * Interface is up */
                    if ((CfaGetParentPortOperStatus
                         (u4IfIndex, &u1ParentOperStatus) == CFA_SUCCESS)
                        && (u1ParentOperStatus == CFA_IF_UP))
                    {
                        u1OperStatus = CFA_IF_UP;
                    }
                    else
                    {
                        u1OperStatus = CFA_IF_DOWN;
                    }
                }
                else
                {
                    u1OperStatus = CFA_IF_DOWN;
                }
            }
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;

        case CFA_L3IPVLAN:
#ifdef WGS_WANTED
        case CFA_L2VLAN:
#endif /* WGS_WANTED */
#ifdef LNXIP6_WANTED
            /*  Need to send RA by setting lifetime to 0 before disabling the interface
             *  1. With the index, try resetting the Lifetime to 0.
             *  2. Manually trigger to send RA
             */
            pIf6Entry = Lip6UtlGetIfEntry (u4IfIndex);
            if (pIf6Entry != NULL)
            {
                u4TmpDefLifetime = pIf6Entry->u4DefRaLifetime;
                if (u1AdminStatus == CFA_IF_DOWN)
                {
                    pIf6Entry->u4DefRaLifetime = 0;
                }
                Lip6RAConfig (pIf6Entry->u4ContextId);
            }
#endif

            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);

            if (CfaIsLinuxVlanIntf (u4IfIndex) != TRUE)
            {
                if (u1AdminStatus == CFA_IF_UP)
                {
                    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                    {
#ifdef MBSM_WANTED
                        if (u4IfIndex == CFA_DEFAULT_STACK_IFINDEX)
                        {
                            /* Oper status depends on stack port Oper status */
                            CfaGetStackIvrOperStatus (&u1OperStatus);
                        }
                        else
                        {
                            VlanIvrGetVlanIfOperStatus (u4IfIndex,
                                                        &u1OperStatus);
                        }
#endif
                    }

                    else
                    {
                        VlanIvrGetVlanIfOperStatus (u4IfIndex, &u1OperStatus);
                    }
                }
                else
                {
                    u1OperStatus = CFA_IF_DOWN;
                }
#ifdef PORT_MAC_WANTED
                /* If the compilation switches UNIQUE_VLAN_MAC_WANTED 
                   & PORT_MAC_WANTED are defined, assign active port 
                   MAC as vlan interace MAC when L3 VLAN interface 
                   oper status as up */
                if (u1OperStatus == CFA_IF_UP)
                {
                    CfaIvrVlanIfAssignMac (u4IfIndex);
                }
#endif
            }
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
#ifdef LNXIP6_WANTED
            if (pIf6Entry != NULL)
            {
                pIf6Entry->u4DefRaLifetime = u4TmpDefLifetime;
            }
#endif

            break;
#ifdef PPP_WANTED
        case CFA_PPP:            /* notify the status to PPP/MP module and update the
                                   values in the ifTable if successful */
            CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);

            if (u1IsFromMib == CFA_TRUE)
            {

                if (u1OperStatus == CFA_IF_UNK)
                {

                    /* CFA_IF_IDLE_LAST_COUNTERS (u4IfIndex) = (UINT4)
                       (CFA_IF_GET_IN_BCAST (u4IfIndex) +
                       CFA_IF_GET_OUT_BCAST (u4IfIndex)); */
                }

                /* if the interface is a demand circuit
                 * then we dont intimate the DORM operstatus to PPP
                 * - it will be indicated ON DEMAND */
                /* DSL_ADD-S- */
                if ((u1AdminStatus == CFA_IF_DOWN) &&
                    (CFA_IF_PERST (u4IfIndex) == CFA_PERS_DEMAND))
                {
                    CFA_IF_DEMAND_CKT_STATUS (u4IfIndex) = CFA_DEMAND_CKT_CLOSE;
                }
                /* DSL_ADD-E- */
                if (u1OperStatus != CFA_IF_DORM)
                {
                    if (PppUpdateInterfaceStatus (u4IfIndex, u1AdminStatus,
                                                  u1OperStatus) != PPP_SUCCESS)
                    {
                        i4RetVal = CFA_FAILURE;
                    }
                }
                CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
                CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            }                    /* end of status change from MIB */
            else
            {
                /* if we are getting a Lower layer UP event - we send it as a trigger
                 * to PPP - UNK status */

                if ((u1OperStatus == CFA_IF_UP) &&
                    (u1CurOperStatus == CFA_IF_LLDOWN))
                {
                    u1OperStatus = CFA_IF_UP;
                }

                /* if the PPP interface is going down
                 * - restore the configured values */
                if (u1OperStatus == CFA_IF_DOWN)
                {
                    CfaSetIfMtu (u4IfIndex, CFA_IF_CONFIG_MTU (u4IfIndex));

                    /* if the DOWN notification has come from the protocol,
                     * check if this transition is from UP->DORM
                     * or UP->DOWN */
                    if ((CFA_IF_PERST (u4IfIndex) == CFA_PERS_DEMAND) &&
                        (CFA_IF_IDLE_LAST_COUNTERS (u4IfIndex) == 0))
                    {
                        if (u1CurOperStatus != CFA_IF_UNK)
                        {
                            if (u1BridgedIfaceStatus == CFA_DISABLED)
                            {
                                /* Down the interface in IP so that routes are
                                 * deleted
                                 */
                                if (IpUpdateInterfaceStatus
                                    ((UINT2) (CFA_IF_IPPORT (u4IfIndex)),
                                     NULL, CFA_IF_DOWN) != IP_SUCCESS)
                                {
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                              "Error in CfaIfmNotifyInterfaceStatus "
                                              "for interface %d IP upd - Failure\n",
                                              u4IfIndex);
                                }

                                if (IpUpdateInterfaceStatus
                                    ((UINT2) (CFA_IF_IPPORT (u4IfIndex)),
                                     NULL, CFA_IF_UP) != IP_SUCCESS)
                                {
                                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                                              "Error in CfaIfmNotifyInterfaceStatus "
                                              "for interface %d IP upd - Failure\n",
                                              u4IfIndex);
                                }
                            }
                        }

                        /* this is a UP->DORM transition as a result of idle timer expiry */
                        *pu1InOperStatus = u1OperStatus = CFA_IF_DORM;
                        CFA_IF_DEMAND_CKT_STATUS (u4IfIndex) =
                            CFA_DEMAND_CKT_CLOSE;
                    }
                }

                CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
                CfaSetIfOperStatus (u4IfIndex, u1OperStatus);

                /* Indicate PPP if the lower layer status changes */
                if ((u1OperStatus == CFA_IF_LLDOWN)
                    || (u1OperStatus == CFA_IF_UNK))
                {
                    PppUpdateInterfaceStatus (u4IfIndex, u1AdminStatus,
                                              u1OperStatus);
                }
                /* we now update the link state info */
                if (u1OperStatus != CFA_IF_UP)
                    CFA_PPP_IWF_BYPASS (u4IfIndex) |=
                        (NON_PLAIN_RX | NON_PLAIN_TX | ACFC_PLAIN_RX);
            }                    /* end of NOT a demand CKT going UP/DOWN */

            break;
#else /* PPP_WANTED */
            UNUSED_PARAM (u1IsFromMib);
#endif /* PPP_WANTED  */
#ifdef VPN_WANTED
#ifdef IKE_WANTED
        case CFA_VPNC:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;

            if (u1AdminStatus == CFA_IF_UP)
            {
                u1OperStatus = CFA_IF_UP;
            }
            else
            {
                u1OperStatus = CFA_IF_DOWN;
            }
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;
#endif /* IKE_WANTED */
#endif /* VPN_WANTED */

        case CFA_TELINK:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);

            /* Need to send interface status change for TE Link Interfaces to HL */

            /* If Admin Status of the TE Link interface itself is down,
             * indicate CFA_IF_DOWN as Oper Status to HL */
            if (u1AdminStatus == CFA_IF_DOWN)
            {
                u1OperStatus = CFA_IF_DOWN;
            }
            else
            {
                /* Admin Status of TE Link Interface is UP.
                 *
                 * TE link interface now operate over L3 Interfaces, 
                 * Need to send the oper status values of the L3 Interface
                 * to HL */
#ifdef TLM_WANTED
                u1OperStatus = CfaGetL3IfOperStatusFromTeLinkIf (u4IfIndex);
#endif
            }
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;
        case CFA_PSEUDO_WIRE:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            /*Need to set admin status only OperStatus will take care from mpls */
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            break;

#ifdef VXLAN_WANTED
        case CFA_VXLAN_NVE:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            break;
#endif

#ifdef VCPEMGR_WANTED
        case CFA_TAP:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            if (UpdateTapInterfaceStatus (u4IfIndex, u1OperStatus) ==
                OSIX_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaIfmNotifyInterfaceStatus - "
                         "UpdateTapInterfaceStatus - FAILURE.\n");
                return CFA_FAILURE;
            }
            break;
#endif

        case CFA_PROP_VIRTUAL_INTERFACE:
            CfaGetIfSubType (u4IfIndex, &u1SubType);
            if (u1SubType != CFA_SUBTYPE_AC_INTERFACE)
            {
                break;
            }
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            if (u1AdminStatus == CFA_IF_DOWN)
            {
                /* If Admin status is DOWN, then oper status should 
                 * also be down. */
                u1OperStatus = CFA_IF_DOWN;
                CfaSetIfOperStatus (u4IfIndex, CFA_IF_DOWN);
            }
            else if (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE)
            {
                /* Whenever physical port oper status chnages, correspondigly
                 * attachment circuit interface's oper status gets changed.
                 */
                CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            }
            else
            {
                /* When making AC interface admin status UP, then its oper
                 * status should also be modified based on the following 
                 * criteria.
                 * 1) If AC interface has underlying physical port, then
                 *    AC interface status is set based on the oper status
                 *    of physical port.
                 * 2) If AC interface does not have underlying physical port,
                 *    then its oper status should be down. 
                 */
                CfaGetIfACPortIdentifier (u4IfIndex, &u4ACPort);

                if (u4ACPort != 0)
                {
                    CfaGetIfOperStatus (u4ACPort, &u1OperStatus);
                    CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
                }
                CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
            }
            *pu1InOperStatus = u1OperStatus;

            break;
#if defined (WLC_WANTED) || defined (WTP_WANTED)
        case CFA_WLAN_RADIO:
        case CFA_RADIO:
            if (u4IfIndex < SYS_DEF_MAX_PHYSICAL_INTERFACES)
            {
                if (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE)
                {
                    /*Admin status set to UP, now update Oper status with the
                     *value provided. u1AdminStatus will always have value = 
                     CFA_IF_UP.*/

                    CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
                    CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
                    CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);

                    if (u1OperStatus == CFA_IF_UP)
                    {
                        /* put the new port in the polling table */
                        CfaGddOsAddIfToList (u4IfIndex);
                    }
                    else
                    {
                        /* remove the port from the polling table */
                        CfaGddOsRemoveIfFromList (u4IfIndex);
                    }
                    break;
                }
                CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
                *pu1InOperStatus = u1OperStatus;
                CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
                CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);

                break;
            }

        case CFA_CAPWAP_DOT11_BSS:
        case CFA_CAPWAP_DOT11_PROFILE:
        case CFA_CAPWAP_VIRT_RADIO:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;
#endif
#ifdef HDLC_WANTED
        case CFA_HDLC:
            CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
            CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
            *pu1InOperStatus = u1OperStatus;
            break;
#endif
        default:
            i4RetVal = CFA_FAILURE;
            break;
    }

    if (i4RetVal == CFA_SUCCESS)
    {
/* IP doesnt understand status lower-layer-down */
        if (u1BrgPortType != CFA_UPLINK_ACCESS_PORT)
        {
            if ((u1OperStatus == CFA_IF_LLDOWN)
#ifdef MBSM_WANTED
                || (u1OperStatus == CFA_IF_NP)
#endif
                )
                u1OperStatus = CFA_IF_DOWN;
        }
        else
        {
            if (u1OperStatus == CFA_IF_LLDOWN)
                u1OperStatus = CFA_IF_DOWN;
        }
/* No need to tell IP if Oper is UNK - we will tell when its up/down */
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
        if ((u1IsRegToIp == CFA_TRUE) && ((u1OperStatus == CFA_IF_UP) ||
                                          (u1OperStatus == CFA_IF_DOWN)
                                          || (u1OperStatus == CFA_IF_DORM)))
        {
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            STRNCPY (au1IfName, pIfInfo->au1IfName,
                     CFA_MAX_PORT_NAME_LENGTH - CFA_STR_DELIM_SIZE);
            au1IfName[CFA_MAX_PORT_NAME_LENGTH - 1] = '\0';

            if (u1OperStatus == CFA_IF_UP)
            {
                i4RetVal = CfaUpdtIvrInterface (u4IfIndex, CFA_IF_CREATE);
            }
            else if (u1OperStatus == CFA_IF_DOWN)
            {
                i4RetVal = CfaUpdtIvrInterface (u4IfIndex, CFA_IF_DELETE);
            }
            if (IpUpdateInterfaceStatus
                ((UINT2) (CFA_IF_IPPORT (u4IfIndex)), au1IfName, u1OperStatus)
                != IP_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmNotifyInterfaceStatus "
                          "for interface %d IP updating - Failure\n",
                          u4IfIndex);
            }

            /* Change the interface status of all the linux vlan
               Interfaces, based on the interface status of the oob port */
            if (CfaIsMgmtPort (u4IfIndex) == TRUE)
            {
                u4LnxVlanIfIndex = CFA_OOB_MGMT_IFINDEX + 1;
                CFA_CDB_SCAN_WITH_LOCK (u4LnxVlanIfIndex,
                                        SYS_DEF_MAX_INTERFACES, CFA_L3IPVLAN)
                {
                    /*
                     * No need to check the interface Validity, since
                     * it is done in CfaGetIfOperStatus ()
                     */

                    /* check whther the interface is linux vlan interface */

                    if ((CfaIsLinuxVlanIntf (u4LnxVlanIfIndex) == TRUE)
                        && (CfaGetIfOperStatus (u4LnxVlanIfIndex,
                                                &u1OldOperStatus) ==
                            CFA_SUCCESS))
                    {
                        if ((CFA_IF_ADMIN (u4LnxVlanIfIndex) == CFA_IF_UP)
                            && (u1AdminStatus == CFA_IF_UP))
                        {
                            u1NewOperStatus = CFA_IF_UP;
                        }
                        else
                        {
                            u1NewOperStatus = CFA_IF_DOWN;
                        }

                        if (u1NewOperStatus != u1OldOperStatus)
                        {

                            /* change the operstatus of the Linux vlan interface
                               based on the oper status of the oob Mgmt Port */

                            CfaIfmHandleInterfaceStatusChange
                                ((UINT4) u4LnxVlanIfIndex,
                                 CFA_IF_ADMIN ((UINT4) u4LnxVlanIfIndex),
                                 u1NewOperStatus, CFA_FALSE);
                        }

                    }

                }                /* End of For loop */
            }
        }
#endif

        if ((pIfInfo->u1IfType == CFA_L3IPVLAN)
            || (pIfInfo->u1IfType == CFA_L3SUB_INTF)
            || (pIfInfo->u1IfType == CFA_TUNNEL)
            || (pIfInfo->u1IfType == CFA_ENET)
            || (pIfInfo->u1IfType == CFA_LAGG)
            || (pIfInfo->u1IfType == CFA_TELINK)
            || (pIfInfo->u1IfType == CFA_PSEUDO_WIRE)
            || (pIfInfo->u1IfType == CFA_VXLAN_NVE)
            || (pIfInfo->u1IfType == CFA_TAP)
#ifdef WGS_WANTED
            || (pIfInfo->u1IfType == CFA_L2VLAN)
#endif
            || ((pIfInfo->u1IfType == CFA_ENET)
                && (pIfInfo->u1BridgedIface == CFA_DISABLED))
            || (pIfInfo->u1IfType == CFA_LOOPBACK))
        {
            /* Notify the oper status change to HL */
            CfaInfo.u4IfIndex = u4IfIndex;
            CfaInfo.CfaIntfInfo.u1IfType = pIfInfo->u1IfType;
            CfaInfo.CfaIntfInfo.u4IfMtu = pIfInfo->u4IfMtu;
            CfaInfo.CfaIntfInfo.u1BridgedIface = pIfInfo->u1BridgedIface;
            /* Operstatus needs to be got dynamically */
            CfaGetIfOperStatus (u4IfIndex,
                                &(CfaInfo.CfaIntfInfo.u1IfOperStatus));

            CfaInfo.CfaIntfInfo.u4IfSpeed = pIfInfo->u4IfSpeed;
            CfaInfo.CfaIntfInfo.u4IfHighSpeed = pIfInfo->u4IfHighSpeed;
            STRNCPY (CfaInfo.CfaIntfInfo.au1IfName, pIfInfo->au1IfName,
                     STRLEN (pIfInfo->au1IfName));
            CfaInfo.CfaIntfInfo.au1IfName[STRLEN (pIfInfo->au1IfName)] = '\0';
            STRNCPY (CfaInfo.CfaIntfInfo.au1IfName, pIfInfo->au1IfName,
                     CFA_MAX_PORT_NAME_LENGTH - CFA_STR_DELIM_SIZE);
            CfaInfo.CfaIntfInfo.au1IfName[CFA_MAX_PORT_NAME_LENGTH - 1] = '\0';
            CfaInfo.CfaIntfInfo.u2VlanId = pIfInfo->u2VlanId;

            if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_ENET)
                || (CfaInfo.CfaIntfInfo.u1IfType == CFA_LAGG))
            {
                MEMCPY (CfaInfo.CfaIntfInfo.au1MacAddr, pIfInfo->au1MacAddr,
                        CFA_ENET_ADDR_LEN);
            }
            else if ((CfaInfo.CfaIntfInfo.u1IfType == CFA_L3IPVLAN)
                     || (CfaInfo.CfaIntfInfo.u1IfType == CFA_L3SUB_INTF)
#ifdef WGS_WANTED
                     || (CfaInfo.CfaIntfInfo.u1IfType == CFA_L2VLAN)
#endif /* WGS_WANTED */
                )
            {
                MEMCPY (CfaInfo.CfaIntfInfo.au1MacAddr, pIfInfo->au1MacAddr,
                        CFA_ENET_ADDR_LEN);
            }
            CfaNotifyIfOperStChg (&CfaInfo);
        }

        /* If the port is Management Port then,update the Lastchange time
         * for OOB interface and return */
        if (CfaIsMgmtPort (u4IfIndex) == TRUE)
        {
            OsixGetSysTime (&CFA_IF_CHNG (u4IfIndex));

            CFA_DBG3 (CFA_TRC_ALL_TRACK, CFA_IFM,
                      "CHANGING STATUS for interface %d TO admin %d oper %d\n",
                      u4IfIndex, u1AdminStatus, u1OperStatus);

            return i4RetVal;
        }

#if defined (WLC_WANTED) || defined (WTP_WANTED)
        if (((u1IsFromMib == CFA_TRUE) ||
             (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE))
            && (pIfInfo->u1IfType == CFA_RADIO))

        {
            MEMSET (&RadioIfMsgStruct, 0, sizeof (tRadioIfMsgStruct));

            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.u4IfIndex =
                u4IfIndex;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.
                u1OperStatus = u1OperStatus;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.u1IfType =
                pIfInfo->u1IfType;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.
                u1AdminStatus = u1AdminStatus;
            RadioIfMsgStruct.unRadioIfMsg.RadioIfSetAdminOperStatus.
                u1IsFromMib = u1IsFromMib;

            if ((i4RetVal =
                 WssIfProcessRadioIfMsg (WSS_RADIOIF_OPER_STATUS_CHG_MSG,
                                         &RadioIfMsgStruct)) != OSIX_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmNotifyInterfaceStatus "
                          "for interface %d sending message - Failure\n",
                          u4IfIndex);

                return i4RetVal;
            }
        }

        if (((u1IsFromMib == CFA_TRUE) ||
             (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE))
            && ((pIfInfo->u1IfType != CFA_RADIO) &&
                (pIfInfo->u1IfType != CFA_CAPWAP_VIRT_RADIO) &&
                (pIfInfo->u1IfType != CFA_CAPWAP_DOT11_PROFILE) &&
                (pIfInfo->u1IfType != CFA_WLAN_RADIO)))
#else
        if ((u1IsFromMib == CFA_TRUE) ||
            (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE))
#endif

        {
            EoamApiNotifyIfOperChg (u4IfIndex, u1OperStatus);
        }

        if (gu4IsIvrEnabled == CFA_ENABLED)
        {

            if (((u1IsFromMib == CFA_TRUE) ||
                 (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE))
                && (pIfInfo->u1BridgedIface != CFA_ENABLED))

            {
                L2IwfL3LAGPortOperIndication (u4IfIndex, u1OperStatus);

            }
            else if (((u1IsFromMib == CFA_TRUE) ||
                      (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE))
                     && (pIfInfo->u1BridgedIface == CFA_ENABLED))

            {
#ifdef RM_WANTED
                /* In case of the port type is UAP, even in stanby node too,
                 * its status is sent to Higher modules as well to VLAN.
                 * So that S-Channels are set as CONFIRMED state in hybrid mode VLAN.
                 * Then it will be used to set the S-Channel interface oper status 
                 * in the below code */

                if ((RmGetNodeState () == RM_ACTIVE) ||
                    (u1BrgPortType == CFA_UPLINK_ACCESS_PORT))
#endif
                {
                    L2IwfPortOperStatusIndication (u4IfIndex, u1OperStatus);

                    /* When changing the UAP port down to up . 
                     * update for all SCh will happen in CfaIfmNotifyInterfaceStatus.
                     * Since CfaVlanApiEvbGetSbpPortsOnUap gives the CONFIRMED 
                     * state entries which are made confirmed by 
                     * L2IwfPortOperStatusIndication (above) */

                    if (u1BrgPortType == CFA_UPLINK_ACCESS_PORT)
                    {
                        MEMSET (&SbpArray, 0, sizeof (tVlanEvbSbpArray));
                        CfaVlanApiEvbGetSbpPortsOnUap (u4IfIndex, &SbpArray);
                        while (u1LoopIndex < VLAN_EVB_MAX_SBP_PER_UAP)
                        {
                            if (SbpArray.au4SbpArray[u1LoopIndex] == 0)
                            {
                                /* No more SBPs are present on this interface */
                                break;
                            }
                            CFA_DS_LOCK ();
                            pCfaIfSbpInfo = CfaIfUtlCdbGetIfDbEntry
                                (SbpArray.au4SbpArray[u1LoopIndex]);

                            if (NULL != pCfaIfSbpInfo)
                            {
                                CFA_CDB_IF_OPER (SbpArray.
                                                 au4SbpArray[u1LoopIndex]) =
                                    u1OperStatus;
                            }
                            CFA_DS_UNLOCK ();
                            u1LoopIndex++;
                        }
                    }
                }
#ifndef RM_WANTED
            }
#else
                else
                {
                    if (!(AstIsRstStartedInContext (u4ContextId) ||
                          AstIsMstStartedInContext (u4ContextId) ||
                          AstIsPvrstStartedInContext (u4ContextId)))
                    {
                        if (u1AdminStatus == CFA_IF_UP)
                        {

                            L2IwfSetInstPortState ((UINT2)
                                                   RST_DEFAULT_INSTANCE,
                                                   u4IfIndex,
                                                   (UINT1)
                                                   AST_PORT_STATE_FORWARDING);
                        }
                        else if (u1AdminStatus == CFA_IF_DOWN)
                        {
                            L2IwfSetInstPortState ((UINT2)
                                                   RST_DEFAULT_INSTANCE,
                                                   u4IfIndex,
                                                   (UINT1)
                                                   AST_PORT_STATE_DISCARDING);
                        }

                    }
                }

            }
#endif
            /* CFA has to update the Routed Port status */
            if (((u1IsFromMib == CFA_TRUE) ||
                 (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE)) &&
                (pIfInfo->u1BridgedIface == CFA_DISABLED) &&
                (pIfInfo->u1IfType == CFA_ENET))

            {
                if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                                  &u2LocalPortId)
                    == VCM_SUCCESS)
                {
#ifdef NPAPI_WANTED
#ifdef RSTP_WANTED
                    CfaFsMiRstpNpSetPortState (u4ContextId,
                                               u4IfIndex,
                                               AST_PORT_STATE_FORWARDING);
#endif
#endif
                }

            }
        }
        else
        {
#if defined (WLC_WANTED) || defined (WTP_WANTED)
            if (((u1IsFromMib == CFA_TRUE) ||
                 (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE))
                && ((pIfInfo->u1IfType != CFA_RADIO) &&
                    (pIfInfo->u1IfType != CFA_CAPWAP_VIRT_RADIO) &&
                    (pIfInfo->u1IfType != CFA_CAPWAP_DOT11_PROFILE) &&
                    (pIfInfo->u1IfType != CFA_WLAN_RADIO)))
#else
            if ((u1IsFromMib == CFA_TRUE) ||
                (u1IsFromMib == CFA_IF_LINK_STATUS_CHANGE))
#endif
            {
#ifdef RM_WANTED
                if (RmGetNodeState () == RM_ACTIVE)
#endif
                {
                    L2IwfPortOperStatusIndication (u4IfIndex, u1OperStatus);
                }
            }
        }

#ifdef MPLS_WANTED
        if ((u1OperStatus == CFA_IF_DOWN) || (u1OperStatus == CFA_IF_UP))
        {
            MplsHandleIfStatusChange (u4IfIndex, u1OperStatus);
        }
#endif
#ifdef RMON_WANTED
        /* If the Socket is Ethernet, then inform RMON */
        if ((u1IsFromMib == CFA_TRUE) && (pIfInfo->u1IfType == CFA_ENET))
        {
            RmonUpdateInterfaceStatus (u4IfIndex, u1OperStatus);
        }
#endif /* RMON_WANTED */

#ifdef RMON2_WANTED
        /* If the Socket is Ethernet, then inform RMON2 */
        if (pIfInfo->u1IfType == CFA_ENET)
        {
            Rmon2CfaIndicateIfUpdate (u4IfIndex, u1OperStatus);
        }
#endif /* RMON2_WANTED */
#ifdef ISS_WANTED
        if ((u1OperStatus == CFA_IF_DOWN) || (u1OperStatus == CFA_IF_UP))
        {
            IssUpdatePortLinkStatus (u4IfIndex, pIfInfo->u1IfType,
                                     u1OperStatus);
        }
#endif
        OsixGetSysTime (&CFA_IF_CHNG (u4IfIndex));

        CFA_DBG3 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "CHANGING STATUS for interface %d TO admin %d oper %d\n",
                  u4IfIndex, u1AdminStatus, u1OperStatus);
    }                            /* end of i4RetVal == CFA_SUCCESS */
    else
    {
        CFA_DBG3 (CFA_TRC_ALL_TRACK | CFA_TRC_ERROR, CFA_IFM,
                  "In CfaIfmNotifyInterfaceStatus "
                  "for interface %d admin %d oper %d - Failure\n",
                  u4IfIndex, u1AdminStatus, u1OperStatus);
    }

#ifdef NPAPI_WANTED
    if (((((pIfInfo->u1IfType == CFA_ENET)
           || (pIfInfo->u1IfType == CFA_LAGG)
           || (pIfInfo->u1IfType == CFA_PSEUDO_WIRE)
           || (pIfInfo->u1IfType == CFA_VXLAN_NVE)
           || ((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE)
               && (pIfInfo->u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)))
          || (CfaIsVirtualInterface (u4IfIndex) == CFA_TRUE))
         && (CfaIsMgmtPort (u4IfIndex) == FALSE))
#ifdef MBSM_WANTED
        && (u1OperStatus != CFA_IF_NP)
#endif
        )
    {
        if (u1AdminStatus == CFA_IF_DOWN)
        {

            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if (CfaFsHwUpdateAdminStatusChange (u4IfIndex,
                                                    FNP_FALSE) != FNP_SUCCESS)
                {
                    CfaGetIfType (u4IfIndex, &u1IfType);
                    if (u1IfType != CFA_VXLAN_NVE)
                    {
                        i4RetVal = CFA_FAILURE;
                    }
                }
            }

        }
    }

#endif

    if ((pIfInfo->u1IfType == CFA_LAGG) &&
        (pIfInfo->u1BridgedIface != CFA_ENABLED))
    {
        CFA_IF_ADMIN (u4IfIndex) = u1AdminStatus;
        CfaSetCdbPortAdminStatus (u4IfIndex, u1AdminStatus);
        /* Check with la if any of the ports in the
         *  port channel is active.    */
        LaUpdatePortChannelAdminStatus ((UINT2) u4IfIndex, u1AdminStatus,
                                        &u1OperStatus);

        CfaSetIfOperStatus (u4IfIndex, u1OperStatus);
        if (u1OperStatus == CFA_IF_DOWN)
        {
            if (IpUpdateInterfaceStatus
                ((UINT2) (CFA_IF_IPPORT (u4IfIndex)),
                 au1IfName, CFA_IF_DOWN) != IP_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmNotifyInterfaceStatus "
                          "for interface %d IP upd - Failure\n", u4IfIndex);
            }

        }
        *pu1InOperStatus = u1OperStatus;
    }
    UNUSED_PARAM (u1IfType);

    return (i4RetVal);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmAddDynamicStackEntry
 *
 *    Description        : This function is for creating entry in the
 *                         stack table and updating the affected stack
 *
 *    Input(s)            : u4IfStackHigherLayer, u2IfStacklowerLayer 
 *                
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *                gIfGlobal (Interface's global struct)
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if entry is added
 *                succeessfully, otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmAddDynamicStackEntry (UINT4 u4IfStackHigherLayer,
                            UINT4 u4IfStackLowerLayer)
{
    UINT4               u4ErrorCode;
    UINT4               u4HighIfIndex = u4IfStackHigherLayer;
    UINT4               u4LowIfIndex = u4IfStackLowerLayer;

    if ((nmhTestv2IfStackStatus (&u4ErrorCode, (INT4) u4HighIfIndex,
                                 (INT4) u4LowIfIndex,
                                 CFA_RS_CREATEANDWAIT)) == SNMP_SUCCESS)
    {
        if ((nmhSetIfStackStatus
             ((INT4) u4HighIfIndex, (INT4) u4LowIfIndex,
              CFA_RS_CREATEANDWAIT)) == SNMP_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmAddDynamicStackEntry - "
                     "stack entry creation - FAILURE.\n");
            return (CFA_FAILURE);
        }
    }
    else
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmAddDynamicStackEntry - "
                 "stack entry creation - FAILURE.\n");
        return (CFA_FAILURE);
    }

    if ((nmhTestv2IfStackStatus (&u4ErrorCode, (INT4) u4HighIfIndex,
                                 (INT4) u4LowIfIndex,
                                 CFA_RS_ACTIVE)) == SNMP_SUCCESS)
    {
        if ((nmhSetIfStackStatus
             ((INT4) u4HighIfIndex, (INT4) u4LowIfIndex,
              CFA_RS_ACTIVE)) == SNMP_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmAddDynamicStackEntry - "
                     "stack entry creation - FAILURE.\n");
            return (CFA_FAILURE);
        }
    }
    else
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmAddDynamicStackEntry - "
                 "stack entry creation - FAILURE.\n");
        return (CFA_FAILURE);
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmUpdateExtSubType
 *
 *    Description          : This function is used to register/deregister the 
 *                           interface from openflow based on the subtype.
 *                           When this function is called from CLI, port 
 *                           creation and deletion notification should be sent.
 *                           When this function is called from OFCL, 
 *                           notification should not be sent.
 * 
 *    Input(s)             : u4IfIndex, u2SubType, u1Notify 
 *                
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *                                gIfGlobal (Interface's global struct)
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                                gIfGlobal (Interface's global struct)
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns               : CFA_SUCCESS on Success otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaIfmUpdateExtSubType (UINT4 u4IfIndex, UINT2 u2SubType, UINT1 u1Notify)
{
    tCfaIfInfo          IfInfo;
    tIpConfigInfo       IpInfo;
    UINT1               u1BridgedIfaceStatus = 0;
    UINT1               u1CurrSubType = 0;
    UINT4               u4ContextId = 0;
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    tRcvAddressInfo    *pRcvAddrNode = NULL;
    UINT4               u4Flag = 0;

#if ((defined NPAPI_WANTED) && (defined RM_WANTED) && (defined MBSM_WANTED))
    UINT4               u4SlotId = 0;
#endif

#ifdef LNXIP4_WANTED
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
#endif

    CfaGetIfBridgedIfaceStatus ((UINT4) u4IfIndex, &u1BridgedIfaceStatus);
    CfaGetIfSubType ((UINT4) u4IfIndex, &u1CurrSubType);

    if (u2SubType == CFA_SUBTYPE_OPENFLOW_INTERFACE)
    {
        if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }

        if (u1BridgedIfaceStatus == CFA_DISABLED)
        {
            if (CfaIfmConfigNetworkInterface
                (CFA_NET_IF_DEL, u4IfIndex, 0, NULL) != CFA_SUCCESS)
            {
                return CFA_FAILURE;
            }

            /* 
             * No need to maintain IP interface records for Bridged interface..
             * So just remove the ip interface record 
             */
            if (CfaIpIfDeleteIpInterface (u4IfIndex) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmInitEnetIfEntry - "
                          "IP Entry Mempool alloc fail interface %d.\n",
                          u4IfIndex);
                return (CFA_FAILURE);
            }
            IfInfo.i4IpPort = CFA_INVALID_INDEX;
            CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &IfInfo);
        }
        else if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            if (CfaIsMgmtPort (u4IfIndex) == FALSE)
            {
                L2IwfPortDeleteIndication (u4IfIndex);
                EoamApiNotifyIfDelete (u4IfIndex);
                CfaRegNotifyHigherLayer (u4IfIndex, CFA_IF_DELETE);

                if (!(CFA_ENET_CREATE_DELETE_ALLOWED))
                {
                    VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
                    VcmUnMapPortFromContext (u4IfIndex);
                }
            }
        }

        /* 
         * If u1Notify is TRUE,send the Port Creation Notification 
         */

        if (u1Notify == TRUE)
        {
#ifdef OPENFLOW_WANTED
            if ((OpenflowNotifyPortCreateDelete (u4IfIndex, OFC_INVALID_CONTEXT,
                                                 OF_PORT_CREATE)) ==
                OFC_FAILURE)
            {
                return CFA_FAILURE;
            }
#endif /* OPENFLOW_WANTED */
        }
    }
    else if ((u2SubType != CFA_SUBTYPE_OPENFLOW_INTERFACE) &&
             (u1CurrSubType == CFA_SUBTYPE_OPENFLOW_INTERFACE))
    {
        if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }

        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            CfaGddGetOsHwAddr (u4IfIndex, au1HwAddr);
            nmhSetIfMainExtMacAddress ((INT4) u4IfIndex, au1HwAddr);

            if (IfInfo.u1IfType == CFA_ENET)
            {
                if ((pRcvAddrNode =
                     (tRcvAddressInfo *) CFA_IF_RCVADDRTAB_ENTRY (u4IfIndex)) ==
                    NULL)
                {
                    return CFA_FAILURE;
                }
                MEMCPY (pRcvAddrNode->au1Address, au1HwAddr, CFA_ENET_ADDR_LEN);
            }

#ifdef LNXIP4_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                /* 
                 * We will change the Alias Name for Linux IP case so,
                 * revert back while chaning from IP interface to Bridged Interface 
                 */
                CfaSetIfName (u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));
            }
#endif
            IfInfo.i4IpPort = CFA_INVALID_INDEX;
            CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &IfInfo);

            /* 
             * Indicate about port creation to L2 modules if the port is 
             * NOT management port
             */
            if (CfaIsMgmtPort (u4IfIndex) == FALSE)
            {
                if (!(CFA_ENET_CREATE_DELETE_ALLOWED))
                {
                    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
                        == VCM_FAILURE)
                    {
                        VcmCfaCreateIfaceMapping (VCM_DEFAULT_CONTEXT,
                                                  u4IfIndex);
                    }
                    L2IwfPortCreateIndication (u4IfIndex);
                    EoamApiNotifyIfCreate (u4IfIndex);
                }
            }
        }
        else if (u1BridgedIfaceStatus == CFA_DISABLED)
        {
#ifdef LNXIP4_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                          CfaGddGetLnxIntfnameForPort ((UINT2) u4IfIndex));
                CfaSetIfName (u4IfIndex, au1IfName);
            }
#endif

#if ((defined NPAPI_WANTED) && (defined RM_WANTED) && (defined MBSM_WANTED))
            if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
            {
                /* 
                 * The stack interface uses the stack mac address
                 * which is different from the system mac address
                 */
                if (u4IfIndex == CFA_DEFAULT_STACK_IFINDEX)
                {
                    RmGetNodeSwitchId (&u4SlotId);
                    MbsmNpGetStackMac (u4SlotId, au1HwAddr);
                }
                else
                {
                    VcmGetVRMacAddr (VCM_DEFAULT_CONTEXT, au1HwAddr);
                }
            }
            else
#endif
            {
                VcmGetVRMacAddr (VCM_DEFAULT_CONTEXT, au1HwAddr);
            }

            nmhSetIfMainExtMacAddress ((INT4) u4IfIndex, au1HwAddr);

            /* Create IP reocrd required for router interface */
            if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmInitEnetIfEntry - "
                          "IP Entry Mempool alloc fail interface %d.\n",
                          u4IfIndex);
#ifdef LNXIP4_WANTED
                if (IfInfo.u1IfType == CFA_ENET)
                {
                    /* 
                     * We will change the Alias Name for Linux IP case so,
                     * revert back while chaning from IP interface to Bridged 
                     * Interface 
                     */
                    CfaSetIfName ((UINT4) u4IfIndex,
                                  CFA_GDD_PORT_NAME (u4IfIndex));
                }
#endif
                if ((CfaIsMgmtPort ((UINT4) u4IfIndex) == FALSE)
                    && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
                {
                    VcmCfaCreateIfaceMapping (u4ContextId, u4IfIndex);
                    L2IwfPortCreateIndication (u4IfIndex);
                    EoamApiNotifyIfCreate ((UINT4) u4IfIndex);
                }
                return (CFA_FAILURE);
            }

            IfInfo.i4IpPort = CFA_INVALID_INDEX;
            CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &IfInfo);

            u4Flag =
                CFA_IP_IF_FORWARD | CFA_IP_IF_ALLOC_PROTO |
                CFA_IP_IF_ALLOC_METHOD;
            IpInfo.u1IpForwardEnable = CFA_ENABLED;
            IpInfo.u1AddrAllocMethod = CFA_IP_ALLOC_MAN;
            IpInfo.u1AddrAllocProto = CFA_PROTO_DHCP;
            if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpInfo) == CFA_FAILURE)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Setting IP Param for Interface %d failed\n",
                          u4IfIndex);
#ifdef LNXIP4_WANTED
                if (IfInfo.u1IfType == CFA_ENET)
                {
                    /* We will change the Alias Name for Linux IP case so,
                     * revert back while chaning from IP interface to Bridged Interface */
                    CfaSetIfName ((UINT4) u4IfIndex,
                                  CFA_GDD_PORT_NAME (u4IfIndex));
                }
#endif
                if ((CfaIsMgmtPort ((UINT4) u4IfIndex) == FALSE)
                    && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
                {
                    VcmCfaCreateIfaceMapping (u4ContextId, u4IfIndex);
                    L2IwfPortCreateIndication (u4IfIndex);
                    EoamApiNotifyIfCreate ((UINT4) u4IfIndex);
                }
                CfaIpIfDeleteIpInterface (u4IfIndex);
                return CFA_FAILURE;
            }

            if (VcmCfaCreateIPIfaceMapping (VCM_DEFAULT_CONTEXT, u4IfIndex) ==
                VCM_FAILURE)
            {
                /* Unsuccessful in making  the interface
                 * mapping in VCM active */
#ifdef LNXIP4_WANTED
                if (IfInfo.u1IfType == CFA_ENET)
                {
                    /* We will change the Alias Name for Linux IP case so,
                     * revert back while chaning from IP interface to Bridged Interface */
                    CfaSetIfName ((UINT4) u4IfIndex,
                                  CFA_GDD_PORT_NAME (u4IfIndex));
                }
#endif
                if ((CfaIsMgmtPort ((UINT4) u4IfIndex) == FALSE)
                    && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
                {
                    VcmCfaCreateIfaceMapping (u4ContextId, u4IfIndex);
                    L2IwfPortCreateIndication (u4IfIndex);
                    EoamApiNotifyIfCreate ((UINT4) u4IfIndex);
                }
                CfaIpIfDeleteIpInterface (u4IfIndex);
                return CFA_FAILURE;
            }

            if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW, u4IfIndex,
                                              0, NULL) == CFA_FAILURE)
            {
                CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                          "Error in CfaIfmNotifyInterfaceStatus - "
                          "IP Registration fail interface %d\n", u4IfIndex);
#ifdef LNXIP4_WANTED
                if (IfInfo.u1IfType == CFA_ENET)
                {
                    /* We will change the Alias Name for Linux IP case so,
                     * revert back while chaning from IP interface to Bridged Interface */
                    CfaSetIfName ((UINT4) u4IfIndex,
                                  CFA_GDD_PORT_NAME (u4IfIndex));
                }
#endif
                if ((CfaIsMgmtPort ((UINT4) u4IfIndex) == FALSE)
                    && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
                {
                    VcmCfaCreateIfaceMapping (u4ContextId, u4IfIndex);
                    L2IwfPortCreateIndication (u4IfIndex);
                    EoamApiNotifyIfCreate ((UINT4) u4IfIndex);
                }
                CfaIpIfDeleteIpInterface (u4IfIndex);
                return CFA_FAILURE;
            }
        }

        /* 
         * If u1Notify is TRUE,send the Port Deletion Notification 
         */
        if (u1Notify == TRUE)
        {
#ifdef OPENFLOW_WANTED
            if ((OpenflowNotifyPortCreateDelete (u4IfIndex, OFC_INVALID_CONTEXT,
                                                 OF_PORT_DELETE)) ==
                OFC_FAILURE)
            {
                return CFA_FAILURE;
            }
#endif
        }
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmUpdateInterfaceStatus
 *
 *    Description          :  This function is used to register/deregister the 
 *                            interface from IP/Bridge based on the status.
 * 
 *    Input(s)            : u4IfIndex, u2BridgedIface 
 *                
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *                gIfGlobal (Interface's global struct)
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *                gIfGlobal (Interface's global struct)
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS on Success,
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaIfmUpdateInterfaceStatus (UINT4 u4IfIndex, UINT2 u2BridgedIface)
{
    tCfaIfInfo          IfInfo;
    tIpConfigInfo       IpInfo;
    tRcvAddressInfo    *pRcvAddrNode = NULL;
    UINT4               u4Flag;
    UINT4               u4ContextId = 0;
#if ((defined NPAPI_WANTED) && (defined RM_WANTED) && (defined MBSM_WANTED))
    UINT4               u4SlotId;
#endif
#ifdef LNXIP4_WANTED
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Type = 0;
#endif
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];
    UINT1               u1IfType = 0;

    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2Index = 0;
    UINT2               u2NumPorts = 0;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return CFA_FAILURE;
    }

    /*
     * Deregister the interface from IP if it is registered 
     * already.If not, delete the port from Bridge. Because in IVR the interface
     * will be registered with IP or Bridge and not both.
     * If the request is to enable bridging on a particular interface, then
     * port create is indication to Bridge. 
     */
    if (u2BridgedIface == CFA_ENABLED)
    {
        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_DEL, u4IfIndex, 0, NULL)
            != CFA_SUCCESS)
        {
            return CFA_FAILURE;
        }

        /* No need to maintain IP interface records for Bridged interface..
         * So just remove the ip interface record */
        if (CfaIpIfDeleteIpInterface (u4IfIndex) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitEnetIfEntry - "
                      "IP Entry Mempool alloc fail interface %d.\n", u4IfIndex);
            return (CFA_FAILURE);
        }
        if (IfInfo.u1IfType == CFA_ENET)
        {
            L2IwfDeleteEntry (u4IfIndex, IfInfo.u1IfType, L2IWF_FALSE);
        }
        CfaGetIfType (u4IfIndex, &u1IfType);
        if (u1IfType == CFA_LAGG)
        {
            MEMCPY (au1HwAddr, IfInfo.au1MacAddr, CFA_ENET_ADDR_LEN);
        }
        else
        {
            CfaGddGetOsHwAddr (u4IfIndex, au1HwAddr);
        }

        if (IfInfo.u1IfType == CFA_ENET)
        {
            if ((pRcvAddrNode =
                 (tRcvAddressInfo *) CFA_IF_RCVADDRTAB_ENTRY (u4IfIndex)) !=
                NULL)
            {
                MEMCPY (pRcvAddrNode->au1Address, au1HwAddr, CFA_ENET_ADDR_LEN);
            }
            else
            {
                return CFA_FAILURE;
            }
        }

#ifdef LNXIP4_WANTED
        if (IfInfo.u1IfType == CFA_ENET)
        {
            /* We will change the Alias Name for Linux IP case so,
             * revert back while chaning from IP interface to Bridged Interface */
            CfaSetIfName (u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));
        }
#endif
        IfInfo.i4IpPort = CFA_INVALID_INDEX;
        CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &IfInfo);

        /* Indicate about port creation to L2 modules if the port is 
         * NOT management port
         * */
        if (CfaIsMgmtPort (u4IfIndex) == FALSE)
        {
            if (!(CFA_ENET_CREATE_DELETE_ALLOWED))
            {
                if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
                    == VCM_FAILURE)
                {
                    VcmCfaCreateIfaceMapping (VCM_DEFAULT_CONTEXT, u4IfIndex);
                }
                L2IwfPortCreateIndication (u4IfIndex);
                EoamApiNotifyIfCreate (u4IfIndex);
            }
        }

        /* Need to create IfaceMapping for the member ports of the L2 LAG */

        if (IfInfo.u1IfType == CFA_LAGG)
        {
            if (L2IwfGetConfiguredPortsForPortChannel ((UINT2) u4IfIndex,
                                                       au2ConfPorts,
                                                       &u2NumPorts)
                != L2IWF_FAILURE)
            {
                for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
                {
                    VcmCfaCreateIfaceMapping (VCM_DEFAULT_CONTEXT,
                                              au2ConfPorts[u2Index]);
                }
            }
        }

    }

    else
    {
#ifdef LNXIP4_WANTED
        if (ISS_HW_SUPPORTED !=
            IssGetHwCapabilities (ISS_HW_KNET_IFACE_SUPPORT))
        {
            if (IfInfo.u1IfType == CFA_ENET)
            {
                SNPRINTF ((CHR1 *) au1IfName, CFA_MAX_PORT_NAME_LENGTH, "%s",
                          CfaGddGetLnxIntfnameForPort (u4IfIndex));
                CfaSetIfName (u4IfIndex, au1IfName);
            }
        }
#endif
        if ((IfInfo.u1IfType == CFA_ENET) || IfInfo.u1IfType == CFA_LAGG)
        {
            if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId)
                == VCM_FAILURE)
            {
                VcmCfaCreateIfaceMapping (VCM_DEFAULT_CONTEXT, u4IfIndex);
            }
            L2IwfPortCreateIndication (u4IfIndex);
        }
        /* Port is getting changed as router interface.Router interface
         * should not be visible to L2 modules.So give port delete
         * indication to all L2 modules if the port is NOT Management Port
         * */
        if (CfaIsMgmtPort (u4IfIndex) == FALSE)
        {
            L2IwfPortDelIndication (u4IfIndex, CFA_DISABLED);
            EoamApiNotifyIfDelete (u4IfIndex);
            CfaRegNotifyHigherLayer (u4IfIndex, CFA_IF_DELETE);

            if (!(CFA_ENET_CREATE_DELETE_ALLOWED))
            {
                VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId);
                VcmUnMapPortFromContext (u4IfIndex);
            }
        }
#if ((defined NPAPI_WANTED) && (defined RM_WANTED) && (defined MBSM_WANTED))
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            /* The stack interface uses the stack mac address
             * which is different from the system mac address*/

            if (u4IfIndex == CFA_DEFAULT_STACK_IFINDEX)
            {
                RmGetNodeSwitchId (&u4SlotId);
                CfaMbsmNpGetStackMac (u4SlotId, au1HwAddr);
            }
            else
            {
                VcmGetVRMacAddr (VCM_DEFAULT_CONTEXT, au1HwAddr);
            }
        }
        else
#endif
        {
#ifndef LNXIP4_WANTED
            VcmGetVRMacAddr (VCM_DEFAULT_CONTEXT, au1HwAddr);
#else
#if defined (WLC_WANTED) || defined (WTP_WANTED)
            CfaGddGetOsHwAddr (u4IfIndex, au1HwAddr);
#endif
#endif
        }

        nmhSetIfMainExtMacAddress ((INT4) u4IfIndex, au1HwAddr);
        /* Even if the Port is changed as Router Port, it should be visible 
         * SYSTEM Modules such as QOS. So give Port create Indication to QOS 
         * */
#ifdef QOSX_WANTED
        if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
              (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
        {
            QosCreatePort (u4IfIndex);
        }
#endif /* QOSX_WANTED */

        /* Create IP reocrd required for router interface */
        if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmInitEnetIfEntry - "
                      "IP Entry Mempool alloc fail interface %d.\n", u4IfIndex);
#ifdef LNXIP4_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                /* We will change the Alias Name for Linux IP case so,
                 * revert back while chaning from IP interface to Bridged Interface */
                CfaSetIfName ((UINT4) u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));
            }
#endif
            if ((CfaIsMgmtPort ((UINT4) u4IfIndex) == FALSE)
                && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
            {
                VcmCfaCreateIfaceMapping (u4ContextId, u4IfIndex);
                L2IwfPortCreateIndication (u4IfIndex);
                EoamApiNotifyIfCreate ((UINT4) u4IfIndex);
            }
            return (CFA_FAILURE);
        }

        IfInfo.i4IpPort = CFA_INVALID_INDEX;
        CfaSetIfInfo (IF_IP_PORTNUM, u4IfIndex, &IfInfo);

        u4Flag = CFA_IP_IF_FORWARD | CFA_IP_IF_ALLOC_PROTO |
            CFA_IP_IF_ALLOC_METHOD;
        IpInfo.u1IpForwardEnable = CFA_ENABLED;
        IpInfo.u1AddrAllocMethod = CFA_IP_ALLOC_MAN;
        IpInfo.u1AddrAllocProto = CFA_PROTO_DHCP;
        if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpInfo) == CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Setting IP Param for Interface %d failed\n", u4IfIndex);
#ifdef LNXIP4_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                /* We will change the Alias Name for Linux IP case so,
                 * revert back while chaning from IP interface to Bridged Interface */
                CfaSetIfName ((UINT4) u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));
            }
#endif
            if ((CfaIsMgmtPort ((UINT4) u4IfIndex) == FALSE)
                && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
            {
                VcmCfaCreateIfaceMapping (u4ContextId, u4IfIndex);
                L2IwfPortCreateIndication (u4IfIndex);
                EoamApiNotifyIfCreate ((UINT4) u4IfIndex);
            }
            CfaIpIfDeleteIpInterface (u4IfIndex);
            return CFA_FAILURE;
        }

        /* Need to create IPIfaceMapping for the member ports of the L3 LAG */

        if (IfInfo.u1IfType == CFA_LAGG)
        {
            if (L2IwfGetConfiguredPortsForPortChannel ((UINT2) u4IfIndex,
                                                       au2ConfPorts,
                                                       &u2NumPorts)
                != L2IWF_FAILURE)
            {
                for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
                {
                    if (VcmCfaCreateIPIfaceMapping
                        (VCM_DEFAULT_CONTEXT,
                         au2ConfPorts[u2Index]) == VCM_FAILURE)
                    {
                        /* Unsuccessful in making  the interface
                         * mapping in VCM active */
#ifdef LNXIP4_WANTED
                        CfaGetIfType ((UINT4) au2ConfPorts[u2Index], &u1Type);
                        if (u1Type == CFA_ENET)
                        {
                            /* We will change the Alias Name for Linux IP case so,
                             * revert back while chaning from IP interface to Bridged Interface */
                            CfaSetIfName ((UINT4) au2ConfPorts[u2Index],
                                          CFA_GDD_PORT_NAME (au2ConfPorts
                                                             [u2Index]));
                        }
#endif
                        if ((CfaIsMgmtPort ((UINT4) au2ConfPorts[u2Index]) ==
                             FALSE) && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
                        {
                            VcmCfaCreateIfaceMapping (u4ContextId,
                                                      au2ConfPorts[u2Index]);
                            L2IwfPortCreateIndication ((UINT4)
                                                       au2ConfPorts[u2Index]);
                            EoamApiNotifyIfCreate ((UINT4)
                                                   au2ConfPorts[u2Index]);
                        }
                        CfaIpIfDeleteIpInterface ((UINT4)
                                                  au2ConfPorts[u2Index]);
                        return CFA_FAILURE;
                    }

                }
            }
            if (u2NumPorts > L2IWF_MAX_PORTS_PER_CONTEXT)
            {
                return CFA_FAILURE;
            }
        }

        if (VcmCfaCreateIPIfaceMapping (VCM_DEFAULT_CONTEXT, u4IfIndex)
            == VCM_FAILURE)
        {
            /* Unsuccessful in making  the interface
             * mapping in VCM active */
#ifdef LNXIP4_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                /* We will change the Alias Name for Linux IP case so,
                 * revert back while chaning from IP interface to Bridged Interface */
                CfaSetIfName ((UINT4) u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));
            }
#endif
            if ((CfaIsMgmtPort ((UINT4) u4IfIndex) == FALSE)
                && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
            {
                VcmCfaCreateIfaceMapping (u4ContextId, u4IfIndex);
                L2IwfPortCreateIndication (u4IfIndex);
                EoamApiNotifyIfCreate ((UINT4) u4IfIndex);
            }
            CfaIpIfDeleteIpInterface (u4IfIndex);
            return CFA_FAILURE;
        }

        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_NEW, u4IfIndex,
                                          0, NULL) == CFA_FAILURE)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmNotifyInterfaceStatus - "
                      "IP Registration fail interface %d\n", u4IfIndex);
#ifdef LNXIP4_WANTED
            if (IfInfo.u1IfType == CFA_ENET)
            {
                /* We will change the Alias Name for Linux IP case so,
                 * revert back while chaning from IP interface to Bridged Interface */
                CfaSetIfName ((UINT4) u4IfIndex, CFA_GDD_PORT_NAME (u4IfIndex));
            }
#endif
            if ((CfaIsMgmtPort ((UINT4) u4IfIndex) == FALSE)
                && (!(CFA_ENET_CREATE_DELETE_ALLOWED)))
            {
                VcmCfaCreateIfaceMapping (u4ContextId, u4IfIndex);
                L2IwfPortCreateIndication (u4IfIndex);
                EoamApiNotifyIfCreate ((UINT4) u4IfIndex);
            }
            CfaIpIfDeleteIpInterface (u4IfIndex);
            return CFA_FAILURE;
        }
    }
    /* The notification is done here because this routine is called from the
     * low level routine and also during the bridge mode change
     */
    MsrNotifyPortDeletion (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CfaSnmpifSendTrap                                          */
/*                                                                           */
/* Description  : Routine to send trap to SNMP .                             */
/*                                                                           */
/* Input        : u4IfIndex  : Interface index for which Trap is generated   */
/*                u1OperStatus : Oper Info which will be filled in Trap      */
/*                u1AdminStatus : Admin Info which will be filled in Trap    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
CfaSnmpifSendTrap (UINT4 u4IfIndex, UINT1 u1OperStatus, UINT1 u1AdminStatus)
{
#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4GenTrapType = 0;
    UINT4               u4SpecTrapType = 0;

    UINT1               au1Buf[CFA_OBJECT_NAME_LEN];

    /* ColdStart trap is the first trap to go out from the switch
     * Hence, if trying to send link up/down trap while restoration is in 
     * progress, stop sending this trap now.
     * Link up/down trap will be sent from MSR module as soon as MSR is successfully
     * completed and ColdStart trap is triggered
     */

#ifdef ISS_WANTED
#ifdef MSR_WANTED
    if (MIB_RESTORE_IN_PROGRESS == gi4MibResStatus)
    {
        return;
    }
#endif
#endif
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Check for the OperStatus */
    if (u1OperStatus == CFA_IF_DOWN)
    {
        u4GenTrapType = CFA_TRAP_LINK_DOWN;    /* Value 2 of snmpTraps */
    }
    else if (u1OperStatus == CFA_IF_UP)
    {
        u4GenTrapType = CFA_TRAP_LINK_UP;    /* Value 3 of snmpTraps */
    }
    else
    {
        return;
    }

    u4SpecTrapType = 0;

    if ((u4GenTrapType == CFA_TRAP_LINK_DOWN)
        || (u4GenTrapType == CFA_TRAP_LINK_UP))
    {
        /* For a IF LINK UP/DOWN Trap, ifIndex, ifAdminStatus and */
        /* ifOperStatus have to be notified through the TRAP. */
        SNPRINTF ((char *) au1Buf, CFA_OBJECT_NAME_LEN, "ifIndex");

        /* Pass the Object Name ifIndex to get the SNMP Oid */
        pOid = CfaMakeObjIdFromDotNew ((UINT1 *) au1Buf);
        if (pOid == NULL)
        {
            /* Release the memory Assigned for Entrerprise Oid */
            return;
        }
        /* Concatenate the OID with the ifIndex value - table indexing */
        pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = u4IfIndex;
        pOid->u4_Length = SNMP_TRAP_OID_LEN;

        /* For the SNMP Variable Binding of OID and associated Value
         * for ifIndex */
        pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                           SNMP_DATA_TYPE_INTEGER,
                                                           0,
                                                           (INT4) u4IfIndex,
                                                           NULL, NULL,
                                                           SnmpCnt64Type);

        if (pVbList == NULL)
        {
            SNMP_FreeOid (pOid);
            CfaMemFreeVarBindList (pStartVb);
            return;
        }

        /* Starting of Variable Binding */
        pStartVb = pVbList;

        SNPRINTF ((char *) au1Buf, CFA_OBJECT_NAME_LEN, "ifAdminStatus");
        /* Pass the Object Name ifAdminStatus to get the SNMP Oid */
        pOid = CfaMakeObjIdFromDotNew ((UINT1 *) au1Buf);
        if (pOid == NULL)
        {
            CfaMemFreeVarBindList (pStartVb);
            return;
        }

        /* Concatenate the OID with the ifIndex value - table indexing */
        pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = u4IfIndex;
        pOid->u4_Length = SNMP_TRAP_OID_LEN;

        /* For the SNMP Variable Binding of OID and associated Value
         * for ifAdminStatus*/
        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER,
                                                     0, u1AdminStatus, NULL,
                                                     NULL, SnmpCnt64Type);

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pOid);
            CfaMemFreeVarBindList (pStartVb);
            return;
        }

        /* Start of Next Variable Binding */
        pVbList = pVbList->pNextVarBind;

        SNPRINTF ((char *) au1Buf, CFA_OBJECT_NAME_LEN, "ifOperStatus");
        /* Pass the Object Name ifOperStatus to get the SNMP Oid */
        pOid = CfaMakeObjIdFromDotNew ((UINT1 *) au1Buf);
        if (pOid == NULL)
        {
            CfaMemFreeVarBindList (pStartVb);
            return;
        }

        /* Concatenate the OID with the ifIndex value - table indexing */
        pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = u4IfIndex;
        pOid->u4_Length = SNMP_TRAP_OID_LEN;

        /* For the SNMP Variable Binding of OID and associated Value
         * for ifOperStatus */
        pVbList->pNextVarBind =
            (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                     SNMP_DATA_TYPE_INTEGER,
                                                     0L, u1OperStatus, NULL,
                                                     NULL, SnmpCnt64Type);

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_FreeOid (pOid);
            CfaMemFreeVarBindList (pStartVb);
            return;
        }

        pVbList = pVbList->pNextVarBind;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (NULL, u4GenTrapType, u4SpecTrapType, pStartVb);

#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1AdminStatus);
    UNUSED_PARAM (u1OperStatus);
#endif /* SNMP_3_WANTED */

}

/*****************************************************************************/
/* Function     : CfaSnmpifUfdSendTrap                                       */
/*                                                                           */
/* Description  : Routine to send trap to SNMP .                             */
/*                                                                           */
/* Input        : u4IfIndex  : Interface index for which Trap is generated   */
/*                u1UfdStatus : Port status Info which will be filled in Trap*/
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
VOID
CfaSnmpifUfdSendTrap (UINT4 u4IfIndex, UINT1 u1UfdOperStatus)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4GenTrapType = 0;
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[CFA_OBJECT_NAME_LEN];

    UINT4               au4IfMainUfdOperStatus[] =
        { 1, 3, 6, 1, 4, 1, 2076, 27, 1, 4, 1, 16 };
#ifdef ISS_WANTED
#ifdef MSR_WANTED
    if (MIB_RESTORE_IN_PROGRESS == gi4MibResStatus)
    {
        return;
    }
#endif
#endif
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    u4GenTrapType = 0;
    u4SpecTrapType = 0;

    SNPRINTF ((char *) au1Buf, CFA_OBJECT_NAME_LEN, "ifIndex");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = CfaMakeObjIdFromDotNew ((UINT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        return;
    }
    /* Concatenate the OID with the ifIndex value - table indexing */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = u4IfIndex;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for ifIndex */
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                       SNMP_DATA_TYPE_INTEGER,
                                                       0,
                                                       (INT4) u4IfIndex,
                                                       NULL, NULL,
                                                       SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_FreeOid (pOid);
        CfaMemFreeVarBindList (pStartVb);
        return;
    }

    /* Starting of Variable Binding */
    pStartVb = pVbList;
    if ((pOid =
         alloc_oid ((sizeof (au4IfMainUfdOperStatus) / sizeof (UINT4)) + 1)) ==
        NULL)
    {
        CfaMemFreeVarBindList (pStartVb);
        return;
    }
    MEMCPY (pOid->pu4_OidList, au4IfMainUfdOperStatus,
            sizeof (au4IfMainUfdOperStatus));
    /* Concatenate the OID with the ifIndex value - table indexing */
    pOid->pu4_OidList[(sizeof (au4IfMainUfdOperStatus) / sizeof (UINT4))] =
        u4IfIndex;
    pOid->u4_Length = ((sizeof (au4IfMainUfdOperStatus) / sizeof (UINT4)) + 1);
    /* For the SNMP Variable Binding of OID and associated Value
     * for ifUfdOperStatus*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, u1UfdOperStatus, NULL,
                                                 NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        CfaMemFreeVarBindList (pStartVb);
        return;
    }

    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_Trap (NULL, u4GenTrapType, u4SpecTrapType, pStartVb);

#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1UfdOperStatus);
#endif /* SNMP_2_WANTED or SNMP_3_WANTED */
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CfaMemFreeVarBindList                                      */
/*                                                                           */
/* Description  : Frees the memory allocated for SNMP VarBind list,          */
/*                by scanning the list                                       */
/*                                                                           */
/* Input        : pVarBindLst  : pointer to the Varbind list                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
CfaMemFreeVarBindList (tSNMP_VAR_BIND * pVarBindLst)
{
    SNMP_AGT_FreeVarBindList (pVarBindLst);
    return;
}

/**********************************************************************
* Function             : CfaMakeObjIdFromDotNew                      *
*                                                                     *
* Role of the function : This function returns the pointer to the     *
*                         object id of the passed object to it.       * 
* Formal Parameters    : textStr  :  pointer to the object whose      *
                                      object id is to be found        *
* Global Variables     : None                                         *
* Side Effects         : None                                         *
* Exception Handling   : None                                         *
* Use of Recursion     : None                                         *
* Return Value         : pOid : pointer to object id.                 *
**********************************************************************/

tSNMP_OID_TYPE     *
CfaMakeObjIdFromDotNew (UINT1 *textStr)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    INT1               *pTemp = NULL, *pDot = NULL;
    UINT1               au1tempBuffer[CFA_OBJECT_NAME_LEN + 1];

    UINT4               au4IfIndex[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 1 };
    UINT4               au4IfAdminStatus[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 7 };
    UINT4               au4IfOperStatus[] = { 1, 3, 6, 1, 2, 1, 2, 2, 1, 8 };

    /* NOTE: Currently this function is hardcoded to return the Oid of 
     * the Object names. Once ifmib.mib is compiled with latest MIB 
     * compiler, this function will be modified to generate the OID 
     * from the given string .*/

    /* Is there an alpha descriptor at begining ?? */
    if (CFA_ISALPHA (*textStr))
    {
        pDot = ((INT1 *) STRCHR ((char *) textStr, '.'));

        /* if no dot, point to end of string */
        if (pDot == NULL)
        {
            pDot = ((INT1 *) (textStr + STRLEN ((char *) textStr)));
        }
        pTemp = ((INT1 *) textStr);
        MEMSET (au1tempBuffer, 0, CFA_OBJECT_NAME_LEN + 1);
        STRNCPY (au1tempBuffer, pTemp, (pDot - pTemp));
        au1tempBuffer[(pDot - pTemp)] = '\0';

        if ((pOid = alloc_oid (SNMP_TRAP_OID_LEN)) == NULL)
        {
            return (NULL);
        }

        if (STRCMP ("ifIndex", (INT1 *) au1tempBuffer) == 0)
        {
            MEMCPY (pOid->pu4_OidList, au4IfIndex,
                    (SNMP_TRAP_OID_LEN - 1) * sizeof (UINT4));
            pOid->u4_Length = SNMP_TRAP_OID_LEN;
            return (pOid);

        }
        else if (STRCMP ("ifAdminStatus", (INT1 *) au1tempBuffer) == 0)
        {
            MEMCPY (pOid->pu4_OidList, au4IfAdminStatus,
                    (SNMP_TRAP_OID_LEN - 1) * sizeof (UINT4));
            pOid->u4_Length = SNMP_TRAP_OID_LEN;
            return (pOid);

        }
        else if (STRCMP ("ifOperStatus", (INT1 *) au1tempBuffer) == 0)
        {
            MEMCPY (pOid->pu4_OidList, au4IfOperStatus,
                    (SNMP_TRAP_OID_LEN - 1) * sizeof (UINT4));
            pOid->u4_Length = SNMP_TRAP_OID_LEN;
            return (pOid);

        }
    }

    if (pOid != NULL)
    {
        free_oid (pOid);
    }
    return NULL;

}

VOID
CfaGetIfDescr (UINT4 u4IfIndex, UINT1 *pu1Descr)
{

    UINT4               u4DescrLen;
    UINT1               au1VlanIfDesc[] = "Vlan Interface";
    UINT1               au1EnetIfDesc[] = "Ethernet Interface Port";
    UINT1              *pu1InterfaceDescr = NULL;
    UINT1               au1PortChannelIfDesc[] = "Ieee802.3ad LAG";
    UINT1               u1IfType = CFA_NONE;
    UINT1               au1Buf[CFA_MAX_DESCR_LENGTH];

    MEMSET (au1Buf, 0, CFA_MAX_DESCR_LENGTH);

    CfaGetIfType ((u4IfIndex), &u1IfType);

    if (u1IfType == CFA_ENET)
    {
        SPRINTF ((CHR1 *) au1Buf, "%s %02u", au1EnetIfDesc, u4IfIndex);
        pu1InterfaceDescr = au1Buf;
    }
    else if (u1IfType == CFA_LAGG)
    {
        SPRINTF ((CHR1 *) au1Buf, "%s %02u", au1PortChannelIfDesc, u4IfIndex);
        pu1InterfaceDescr = au1Buf;
    }
    else if ((u1IfType == CFA_L2VLAN) || (u1IfType == CFA_L3IPVLAN))
    {
        SPRINTF ((CHR1 *) au1Buf, "%s %02u", au1VlanIfDesc, u4IfIndex);
        pu1InterfaceDescr = au1Buf;
    }

    if (pu1InterfaceDescr != NULL)
    {
        u4DescrLen = STRLEN (pu1InterfaceDescr);
        MEMCPY (pu1Descr, pu1InterfaceDescr, u4DescrLen);
    }
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetHLIfFromLLIf
 *
 *    Description          : This function gets the first valid 
 *                           Higher Layer Interface value and its type from
 *                           the Lower Interface passed.
 *                           This is not an API and it should not be invoked
 *                           directly from the external modules.
 *
 *    Input(s)             : u4LLIf - Lower Layer Interface Value
 *
 *    Output(s)            : pu4HLIf   - Higher Layer Interface Value
 *                           pu1IfType - Higher Layer Interface Type
 *
 *    Returns              : CFA_SUCCESS if succeeds,
 *                           otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGetHLIfFromLLIf (UINT4 u4LLIf, UINT4 *pu4HLIf, UINT1 *pu1IfType)
{
    tTMO_SLL           *pIfStack = NULL;
    tStackInfoStruct   *pScanNode = NULL;

    *pu4HLIf = CFA_NONE;

    pIfStack = &CFA_IF_STACK_HIGH (u4LLIf);

    TMO_SLL_Scan (pIfStack, pScanNode, tStackInfoStruct *)
    {
        if (CfaValidateCfaIfIndex (pScanNode->u4IfIndex) == CFA_SUCCESS)
        {
            CfaGetIfType (pScanNode->u4IfIndex, pu1IfType);

            if (*pu1IfType != CFA_NONE)
            {
                *pu4HLIf = pScanNode->u4IfIndex;
                break;
            }
        }
    }

    if (*pu4HLIf == CFA_NONE)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateACOperStatus
 *
 *    Description          : This function sets the oper status to the AC
 *                           interface based on the physical interface and its
 *                           operational status. 
 *
 *    Input(s)             : u4PhyIndex   - Physical interface index.
 *                           u1OperStatus - Operational Status of phy iface.
 *
 *    Output(s)            : None
 *
 *****************************************************************************/
VOID
CfaUpdateACOperStatus (UINT4 u4PhyIndex, UINT1 u1OperStatus)
{
    UINT4               u4StartIndex = CFA_MIN_AC_IF_INDEX;
    UINT4               u4LocalIndex = 0;

    CFA_CDB_SCAN_WITH_LOCK (u4StartIndex, CFA_MAX_AC_IF_INDEX,
                            CFA_PROP_VIRTUAL_INTERFACE)
    {
        if (CfaValidateCfaIfIndex (u4StartIndex) != CFA_SUCCESS)
        {
            continue;
        }
        if (CfaGetIfACPortIdentifier (u4StartIndex, &u4LocalIndex)
            != CFA_SUCCESS)
        {
            continue;
        }
        /* u4LocalIndex - underlying physical index of an AC interface.
         * If incoming u4PhyIndex matches with the u4LocalIndex, then
         * the oper status of u4PhyIndex is given to set the oper status
         * of  AC interface (u4StartIndex).
         */

        if (u4LocalIndex == u4PhyIndex)
        {
            CfaInterfaceStatusChangeIndication (u4StartIndex, u1OperStatus);
        }
    }
    return;
}

/*****************************************************************************
 *
 *    Function Name        :  CfaSetBrgPortFromBrgMode
 *
 *    Description          : This function sets the Bridge Port  to the 
 *                           interface based on the Bridge Mode 
 *
 *    Input(s)             : u4PhyIndex   - Physical interface index.
 *
 *    Output(s)            : None
 *
 *****************************************************************************/
VOID
CfaSetBrgPortFromBrgMode (UINT4 u4IfIndex)
{
    UINT4               u4BridgeMode = 0;
    UINT4               u4ContextId = 0;

    L2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);

    if (u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        CfaSetIfBrgPortType (u4IfIndex, CFA_CUSTOMER_BRIDGE_PORT);
    }
    else if ((u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE) ||
             (u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE) ||
             (u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE) ||
             (u4BridgeMode == VLAN_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        CfaSetIfBrgPortType (u4IfIndex, CFA_PROVIDER_NETWORK_PORT);
    }
    else if (u4BridgeMode == VLAN_PBB_ICOMPONENT_BRIDGE_MODE)
    {
        CfaSetIfBrgPortType (u4IfIndex, CFA_CNP_STAGGED_PORT);
    }
    else
    {
        /* Set the Brg port type as Customer Bridge port. This can be set
         * for all types of ports. In case of non-switch this bridge port type
         * will not be used. */
        CfaSetIfBrgPortType (u4IfIndex, CFA_CUSTOMER_BRIDGE_PORT);
    }
    return;
}
