/****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2011                                          */
/*                                                                           */
/* $Id: gddksbcm.c,v 1.3 2014/11/25 13:04:47 siva Exp $                                                  */
/*                                                                           */
/*  FILE NAME             : gddksbcm.c                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Packet transmission and reception in the kernel  */
/*  MODULE NAME           : Security Module                                  */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE :                                                  */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains BCM target specific           */
/*                          functions used by kernel                         */
/*****************************************************************************/
#ifndef _GDDKSEC_BCM_C_
#define _GDDKSEC_BCM_C_

#include "cfa.h"
#include "secmod.h"
#include "gddksec.h"
#include "gddksbcm.h"

/*****************************************************************************
 *
 *    Function Name     : GddKSecMoveOffSet
 *
 *    Description       : This functin moves the Offset based on target.
 *                        Making DSA tag invisible in case of marvell boards
 *
 *    Input(s)          : pSkb - The packet buffer
 *
 *    Output(s)         : pSkb - Modifies the offset
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
GddKSecMoveOffSet (tSkb *pSkb)
{
    UNUSED_PARAM(pSkb);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name     : GddKSecPortGetDirection
 *
 *    Description       : This function is used to parse target specific tag,
 *                        retrieve ifindex, vlanid and remove the tag.
 *
 *    Input(s)          : pBuf - The packet buffer
 *                        pNetDev - Device on which packet is recieved
 *
 *    Output(s)         : pBuf - Modifies the packet with specific tag removed.
 *                        pu1Direction - The direction of the packet.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
GddKSecPortGetDirection (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Direction, tNetDevice * pNetDev)
{
    UNUSED_PARAM(pBuf);

/* Get Direction based on pNetDev since it 
 * is valid in BCM target */

    if (MEMCMP (pNetDev->name, SEC_DEF_BOND_DEV_0,
                STRLEN (SEC_DEF_BOND_DEV_0)) == 0)
    {
        /* Packet from WAN */
        *pu1Direction = SEC_INBOUND;
    }
    else if (MEMCMP (pNetDev->name, SEC_DEF_BOND_DEV_1,
                     STRLEN (SEC_DEF_BOND_DEV_1)) == 0)
    {
        /* Packet to WAN */
        *pu1Direction = SEC_OUTBOUND;
    }
    else
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name     : GddKSecUpdateOutTag
 *
 *    Description       : This function updates the Tag required for sending 
 *                        out the packet
 *                        
 *                        
 *
 *    Input(s)          : u1Direction - Direction of the packet.(INBOUND/OUTBOUND)
 *                        u2DsaVlanId - VlanId for the packet.
 *
 *    Output(s)         : None.
 *
 *    Global Variables Referred : gau1DSATag
 *
 *    Global Variables Modified : gau1DSATag
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/
INT4
GddKSecUpdateOutTag(UINT1 u1Direction, UINT2 u2DsaVlanId)
{

    UNUSED_PARAM(u1Direction);
    UNUSED_PARAM(u2DsaVlanId);

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name     : GddKSecAddSpecificTag
 *
 *    Description       : This function adds the specific tag based on 
 *                        the target used.
 *                        
 *
 *    Input(s)          : pBuf - The packet buffer
 *
 *    Output(s)         : pBuf - Adds specific tag to the buffer.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            : CFA_SUCCESS / CFA_FAILURE.
 *
 *****************************************************************************/

INT4
GddKSecAddSpecificTag (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UNUSED_PARAM(pBuf);
    return CFA_SUCCESS;
}
#endif
