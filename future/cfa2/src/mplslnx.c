/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id : mplslnx.c
*
* Description:This file contains the routine for sending the MPLS packets 
* to Linux L3 Interface from CFA.
*******************************************************************/
#include "cfainc.h"
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/ip.h>
#include <net/if.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
/*****************************************************************************
 *    Function Name             : CfaLinuxMplsSockOpenAndSend
 *
 *    Description               : This function sends the mpls packet from  
 *                                cfa to the linux l3 interface
 *
 *    Input(s)                  : PBuf - Pointer to the CRU Buffer.
 *                                u4IfIndex - L3 Interface Index.
 *                                u4PktSize - Size of the buffer to be TX
 *
 *    Output(s)                 : None.
 *
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/

INT4
CfaLinuxMplsSockOpenAndSend (tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4IfIndex, UINT4 u4PktSize)
{
    INT4                i4SockFd;
    struct sockaddr_ll  ll;
    struct ifreq        ifr;
    UINT4               u4ToAddrLen;
    INT4                i4WrittenBytes;
    UINT1               au1Name[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1Data = NULL;
    tEnetV2Header       EnetV2Header;

    if (CfaGetInterfaceNameFromIndex (u4IfIndex, au1Name) == OSIX_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }
    MEMCPY (&EnetV2Header, &CFA_ENET_IWF_V2_IPHDR ((UINT2) u4IfIndex),
            sizeof (tEnetV2Header));
    EnetV2Header.u2LenOrType = OSIX_HTONS (CFA_ENET_MPLS);
    CFA_GET_DEST_MEDIA_ADDR (pBuf, EnetV2Header.au1DstAddr);
    if (CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) &EnetV2Header,
                                  CFA_ENET_V2_HEADER_SIZE) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return (CFA_FAILURE);
    }
    pu1Data = MemAllocMemBlk (gCfaPktPoolId);
    if (pu1Data == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return CFA_FAILURE;
    }
    CRU_BUF_Copy_FromBufChain (pBuf, pu1Data, 0, u4PktSize);
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    if ((i4SockFd = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_ALL))) < 0)
    {
        perror ("Error in opening socket");
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1Data);
        return CFA_FAILURE;
    }
    sprintf (ifr.ifr_name, "%s", au1Name);
    if (ioctl (i4SockFd, SIOCGIFINDEX, (char *) &ifr) < 0)
    {
        perror ("Interface Index Get Failed");
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1Data);
        close (i4SockFd);
        return CFA_FAILURE;
    }

    memset (&ll, 0, sizeof (ll));
    ll.sll_family = AF_PACKET;
    ll.sll_ifindex = ifr.ifr_ifindex;

    if (bind (i4SockFd, (struct sockaddr *) &ll, sizeof (ll)) < 0)
    {
        perror ("Bind error");
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1Data);
        close (i4SockFd);
        return CFA_FAILURE;
    }

    u4ToAddrLen = sizeof (ll);

    if ((i4WrittenBytes = sendto (i4SockFd, (void *) pu1Data, u4PktSize, 0,
                                  (struct sockaddr *) &ll,
                                  (socklen_t) u4ToAddrLen)) != (INT4) u4PktSize)
    {
        perror ("sendto failed");
        MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1Data);
        close (i4SockFd);
        return CFA_FAILURE;
    }
    MemReleaseMemBlock (gCfaPktPoolId, (UINT1 *) pu1Data);
    close (i4SockFd);
    return CFA_SUCCESS;
}
