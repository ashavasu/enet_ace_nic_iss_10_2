/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmptulw.c,v 1.4 2014/02/24 11:43:34 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "cfainc.h"
#include  "fsmptulw.h"

/* LOW LEVEL Routines for Table : FsMITunlIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMITunlIfTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMITunlIfTable (INT4 i4FsMIStdIpContextId,
                                         INT4 i4FsMITunlIfAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMITunlIfLocalInetAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMITunlIfRemoteInetAddress,
                                         INT4 i4FsMITunlIfEncapsMethod,
                                         INT4 i4FsMITunlIfConfigID)
{
    INT1                i1RetValue = 0;

    UNUSED_PARAM (i1RetValue);

    if ((i4FsMIStdIpContextId < CFA_DEF_TUNL_CONTEXT) ||
        ((UINT4) i4FsMIStdIpContextId >= CFA_MAX_TUNL_CONTEXT))
    {
        return SNMP_FAILURE;
    }
    if (CfaTnlUtilIsValidCxtId ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1RetValue = nmhValidateIndexInstanceFsTunlIfTable (i4FsMITunlIfAddressType,
                                                        pFsMITunlIfLocalInetAddress,
                                                        pFsMITunlIfRemoteInetAddress,
                                                        i4FsMITunlIfEncapsMethod,
                                                        i4FsMITunlIfConfigID);
    CfaUtilResetTnlCxt ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMITunlIfTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMITunlIfTable (INT4 *pi4FsMIStdIpContextId,
                                 INT4 *pi4FsMITunlIfAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMITunlIfLocalInetAddress,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMITunlIfRemoteInetAddress,
                                 INT4 *pi4FsMITunlIfEncapsMethod,
                                 INT4 *pi4FsMITunlIfConfigID)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4FsMIStdIpContextId = CFA_DEF_TUNL_CONTEXT;

    if (CfaTnlUtilGetFirstCxtId ((UINT4 *) pi4FsMIStdIpContextId)
        == SNMP_FAILURE)
    {
        return i1RetVal;
    }
    do
    {
        i4FsMIStdIpContextId = *pi4FsMIStdIpContextId;
        if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal = nmhGetFirstIndexFsTunlIfTable (pi4FsMITunlIfAddressType,
                                                  pFsMITunlIfLocalInetAddress,
                                                  pFsMITunlIfRemoteInetAddress,
                                                  pi4FsMITunlIfEncapsMethod,
                                                  pi4FsMITunlIfConfigID);

        CfaUtilResetTnlCxt ();

        if (i1RetVal == SNMP_SUCCESS)
        {
            return i1RetVal;
        }

    }
    while (CfaTnlUtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                   (UINT4 *) pi4FsMIStdIpContextId) !=
           SNMP_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMITunlIfTable
 Input       :  The Indices
                FsMIStdIpContextId
                nextFsMIStdIpContextId
                FsMITunlIfAddressType
                nextFsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                nextFsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                nextFsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                nextFsMITunlIfEncapsMethod
                FsMITunlIfConfigID
                nextFsMITunlIfConfigID
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMITunlIfTable (INT4 i4FsMIStdIpContextId,
                                INT4 *pi4NextFsMIStdIpContextId,
                                INT4 i4FsMITunlIfAddressType,
                                INT4 *pi4NextFsMITunlIfAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMITunlIfLocalInetAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextFsMITunlIfLocalInetAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMITunlIfRemoteInetAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextFsMITunlIfRemoteInetAddress,
                                INT4 i4FsMITunlIfEncapsMethod,
                                INT4 *pi4NextFsMITunlIfEncapsMethod,
                                INT4 i4FsMITunlIfConfigID,
                                INT4 *pi4NextFsMITunlIfConfigID)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (CfaTnlUtilIsValidCxtId ((UINT4) i4FsMIStdIpContextId) == SNMP_SUCCESS)
    {
        if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }
        i1RetVal = nmhGetNextIndexFsTunlIfTable (i4FsMITunlIfAddressType,
                                                 pi4NextFsMITunlIfAddressType,
                                                 pFsMITunlIfLocalInetAddress,
                                                 pNextFsMITunlIfLocalInetAddress,
                                                 pFsMITunlIfRemoteInetAddress,
                                                 pNextFsMITunlIfRemoteInetAddress,
                                                 i4FsMITunlIfEncapsMethod,
                                                 pi4NextFsMITunlIfEncapsMethod,
                                                 i4FsMITunlIfConfigID,
                                                 pi4NextFsMITunlIfConfigID);
        CfaUtilResetTnlCxt ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while (CfaTnlUtilGetNextCxtId ((UINT4) i4FsMIStdIpContextId,
                                       (UINT4 *) pi4NextFsMIStdIpContextId) !=
               SNMP_FAILURE)
        {
            if (CfaUtilSetTnlCxt ((UINT4) *pi4NextFsMIStdIpContextId) ==
                SNMP_FAILURE)
            {
                return i1RetVal;
            }
            i1RetVal =
                nmhGetFirstIndexFsTunlIfTable (pi4NextFsMITunlIfAddressType,
                                               pNextFsMITunlIfLocalInetAddress,
                                               pNextFsMITunlIfRemoteInetAddress,
                                               pi4NextFsMITunlIfEncapsMethod,
                                               pi4NextFsMITunlIfConfigID);

            CfaUtilResetTnlCxt ();
            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdIpContextId = *pi4NextFsMIStdIpContextId;
        }

    }
    else
    {
        *pi4NextFsMIStdIpContextId = i4FsMIStdIpContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMITunlIfHopLimit
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfHopLimit (INT4 i4FsMIStdIpContextId,
                          INT4 i4FsMITunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsMITunlIfRemoteInetAddress,
                          INT4 i4FsMITunlIfEncapsMethod,
                          INT4 i4FsMITunlIfConfigID,
                          INT4 *pi4RetValFsMITunlIfHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfHopLimit (i4FsMITunlIfAddressType,
                                pFsMITunlIfLocalInetAddress,
                                pFsMITunlIfRemoteInetAddress,
                                i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                pi4RetValFsMITunlIfHopLimit);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfSecurity
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfSecurity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfSecurity (INT4 i4FsMIStdIpContextId,
                          INT4 i4FsMITunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsMITunlIfRemoteInetAddress,
                          INT4 i4FsMITunlIfEncapsMethod,
                          INT4 i4FsMITunlIfConfigID,
                          INT4 *pi4RetValFsMITunlIfSecurity)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfSecurity (i4FsMITunlIfAddressType,
                                pFsMITunlIfLocalInetAddress,
                                pFsMITunlIfRemoteInetAddress,
                                i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                pi4RetValFsMITunlIfSecurity);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfTOS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfTOS
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfTOS (INT4 i4FsMIStdIpContextId, INT4 i4FsMITunlIfAddressType,
                     tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                     tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                     INT4 i4FsMITunlIfEncapsMethod, INT4 i4FsMITunlIfConfigID,
                     INT4 *pi4RetValFsMITunlIfTOS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfTOS (i4FsMITunlIfAddressType, pFsMITunlIfLocalInetAddress,
                           pFsMITunlIfRemoteInetAddress,
                           i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                           pi4RetValFsMITunlIfTOS);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfFlowLabel
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfFlowLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfFlowLabel (INT4 i4FsMIStdIpContextId,
                           INT4 i4FsMITunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfRemoteInetAddress,
                           INT4 i4FsMITunlIfEncapsMethod,
                           INT4 i4FsMITunlIfConfigID,
                           INT4 *pi4RetValFsMITunlIfFlowLabel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfFlowLabel (i4FsMITunlIfAddressType,
                                 pFsMITunlIfLocalInetAddress,
                                 pFsMITunlIfRemoteInetAddress,
                                 i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                 pi4RetValFsMITunlIfFlowLabel);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfMTU
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfMTU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfMTU (INT4 i4FsMIStdIpContextId, INT4 i4FsMITunlIfAddressType,
                     tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                     tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                     INT4 i4FsMITunlIfEncapsMethod, INT4 i4FsMITunlIfConfigID,
                     INT4 *pi4RetValFsMITunlIfMTU)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfMTU (i4FsMITunlIfAddressType, pFsMITunlIfLocalInetAddress,
                           pFsMITunlIfRemoteInetAddress,
                           i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                           pi4RetValFsMITunlIfMTU);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfDirFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfDirFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfDirFlag (INT4 i4FsMIStdIpContextId,
                         INT4 i4FsMITunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                         INT4 i4FsMITunlIfEncapsMethod,
                         INT4 i4FsMITunlIfConfigID,
                         INT4 *pi4RetValFsMITunlIfDirFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfDirFlag (i4FsMITunlIfAddressType,
                               pFsMITunlIfLocalInetAddress,
                               pFsMITunlIfRemoteInetAddress,
                               i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                               pi4RetValFsMITunlIfDirFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfDirection
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfDirection (INT4 i4FsMIStdIpContextId,
                           INT4 i4FsMITunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfRemoteInetAddress,
                           INT4 i4FsMITunlIfEncapsMethod,
                           INT4 i4FsMITunlIfConfigID,
                           INT4 *pi4RetValFsMITunlIfDirection)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfDirection (i4FsMITunlIfAddressType,
                                 pFsMITunlIfLocalInetAddress,
                                 pFsMITunlIfRemoteInetAddress,
                                 i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                 pi4RetValFsMITunlIfDirection);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfEncaplmt
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfEncaplmt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfEncaplmt (INT4 i4FsMIStdIpContextId,
                          INT4 i4FsMITunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsMITunlIfRemoteInetAddress,
                          INT4 i4FsMITunlIfEncapsMethod,
                          INT4 i4FsMITunlIfConfigID,
                          UINT4 *pu4RetValFsMITunlIfEncaplmt)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfEncaplmt (i4FsMITunlIfAddressType,
                                pFsMITunlIfLocalInetAddress,
                                pFsMITunlIfRemoteInetAddress,
                                i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                pu4RetValFsMITunlIfEncaplmt);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfEncapOption
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfEncapOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfEncapOption (INT4 i4FsMIStdIpContextId,
                             INT4 i4FsMITunlIfAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfLocalInetAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfRemoteInetAddress,
                             INT4 i4FsMITunlIfEncapsMethod,
                             INT4 i4FsMITunlIfConfigID,
                             INT4 *pi4RetValFsMITunlIfEncapOption)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfEncapOption (i4FsMITunlIfAddressType,
                                   pFsMITunlIfLocalInetAddress,
                                   pFsMITunlIfRemoteInetAddress,
                                   i4FsMITunlIfEncapsMethod,
                                   i4FsMITunlIfConfigID,
                                   pi4RetValFsMITunlIfEncapOption);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfIndex
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfIndex (INT4 i4FsMIStdIpContextId, INT4 i4FsMITunlIfAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                       tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                       INT4 i4FsMITunlIfEncapsMethod, INT4 i4FsMITunlIfConfigID,
                       INT4 *pi4RetValFsMITunlIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfIndex (i4FsMITunlIfAddressType,
                             pFsMITunlIfLocalInetAddress,
                             pFsMITunlIfRemoteInetAddress,
                             i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                             pi4RetValFsMITunlIfIndex);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfAlias
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfAlias
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfAlias (INT4 i4FsMIStdIpContextId, INT4 i4FsMITunlIfAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                       tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                       INT4 i4FsMITunlIfEncapsMethod, INT4 i4FsMITunlIfConfigID,
                       tSNMP_OCTET_STRING_TYPE * pRetValFsMITunlIfAlias)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfAlias (i4FsMITunlIfAddressType,
                             pFsMITunlIfLocalInetAddress,
                             pFsMITunlIfRemoteInetAddress,
                             i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                             pRetValFsMITunlIfAlias);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfCksumFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfCksumFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfCksumFlag (INT4 i4FsMIStdIpContextId,
                           INT4 i4FsMITunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfRemoteInetAddress,
                           INT4 i4FsMITunlIfEncapsMethod,
                           INT4 i4FsMITunlIfConfigID,
                           INT4 *pi4RetValFsMITunlIfCksumFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfCksumFlag (i4FsMITunlIfAddressType,
                                 pFsMITunlIfLocalInetAddress,
                                 pFsMITunlIfRemoteInetAddress,
                                 i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                 pi4RetValFsMITunlIfCksumFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfPmtuFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfPmtuFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfPmtuFlag (INT4 i4FsMIStdIpContextId,
                          INT4 i4FsMITunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsMITunlIfRemoteInetAddress,
                          INT4 i4FsMITunlIfEncapsMethod,
                          INT4 i4FsMITunlIfConfigID,
                          INT4 *pi4RetValFsMITunlIfPmtuFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfPmtuFlag (i4FsMITunlIfAddressType,
                                pFsMITunlIfLocalInetAddress,
                                pFsMITunlIfRemoteInetAddress,
                                i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                pi4RetValFsMITunlIfPmtuFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhGetFsMITunlIfStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                retValFsMITunlIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMITunlIfStatus (INT4 i4FsMIStdIpContextId, INT4 i4FsMITunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                        INT4 i4FsMITunlIfEncapsMethod,
                        INT4 i4FsMITunlIfConfigID,
                        INT4 *pi4RetValFsMITunlIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhGetFsTunlIfStatus (i4FsMITunlIfAddressType,
                              pFsMITunlIfLocalInetAddress,
                              pFsMITunlIfRemoteInetAddress,
                              i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                              pi4RetValFsMITunlIfStatus);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMITunlIfHopLimit
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfHopLimit (INT4 i4FsMIStdIpContextId,
                          INT4 i4FsMITunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsMITunlIfRemoteInetAddress,
                          INT4 i4FsMITunlIfEncapsMethod,
                          INT4 i4FsMITunlIfConfigID,
                          INT4 i4SetValFsMITunlIfHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfHopLimit (i4FsMITunlIfAddressType,
                                pFsMITunlIfLocalInetAddress,
                                pFsMITunlIfRemoteInetAddress,
                                i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                i4SetValFsMITunlIfHopLimit);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfTOS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfTOS
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfTOS (INT4 i4FsMIStdIpContextId, INT4 i4FsMITunlIfAddressType,
                     tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                     tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                     INT4 i4FsMITunlIfEncapsMethod, INT4 i4FsMITunlIfConfigID,
                     INT4 i4SetValFsMITunlIfTOS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfTOS (i4FsMITunlIfAddressType, pFsMITunlIfLocalInetAddress,
                           pFsMITunlIfRemoteInetAddress,
                           i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                           i4SetValFsMITunlIfTOS);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfFlowLabel
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfFlowLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfFlowLabel (INT4 i4FsMIStdIpContextId,
                           INT4 i4FsMITunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfRemoteInetAddress,
                           INT4 i4FsMITunlIfEncapsMethod,
                           INT4 i4FsMITunlIfConfigID,
                           INT4 i4SetValFsMITunlIfFlowLabel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfFlowLabel (i4FsMITunlIfAddressType,
                                 pFsMITunlIfLocalInetAddress,
                                 pFsMITunlIfRemoteInetAddress,
                                 i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                 i4SetValFsMITunlIfFlowLabel);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfDirFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfDirFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfDirFlag (INT4 i4FsMIStdIpContextId,
                         INT4 i4FsMITunlIfAddressType,
                         tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                         tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                         INT4 i4FsMITunlIfEncapsMethod,
                         INT4 i4FsMITunlIfConfigID,
                         INT4 i4SetValFsMITunlIfDirFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfDirFlag (i4FsMITunlIfAddressType,
                               pFsMITunlIfLocalInetAddress,
                               pFsMITunlIfRemoteInetAddress,
                               i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                               i4SetValFsMITunlIfDirFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfDirection
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfDirection (INT4 i4FsMIStdIpContextId,
                           INT4 i4FsMITunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfRemoteInetAddress,
                           INT4 i4FsMITunlIfEncapsMethod,
                           INT4 i4FsMITunlIfConfigID,
                           INT4 i4SetValFsMITunlIfDirection)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfDirection (i4FsMITunlIfAddressType,
                                 pFsMITunlIfLocalInetAddress,
                                 pFsMITunlIfRemoteInetAddress,
                                 i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                 i4SetValFsMITunlIfDirection);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfEncaplmt
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfEncaplmt
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfEncaplmt (INT4 i4FsMIStdIpContextId,
                          INT4 i4FsMITunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsMITunlIfRemoteInetAddress,
                          INT4 i4FsMITunlIfEncapsMethod,
                          INT4 i4FsMITunlIfConfigID,
                          UINT4 u4SetValFsMITunlIfEncaplmt)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfEncaplmt (i4FsMITunlIfAddressType,
                                pFsMITunlIfLocalInetAddress,
                                pFsMITunlIfRemoteInetAddress,
                                i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                u4SetValFsMITunlIfEncaplmt);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfEncapOption
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfEncapOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfEncapOption (INT4 i4FsMIStdIpContextId,
                             INT4 i4FsMITunlIfAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfLocalInetAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfRemoteInetAddress,
                             INT4 i4FsMITunlIfEncapsMethod,
                             INT4 i4FsMITunlIfConfigID,
                             INT4 i4SetValFsMITunlIfEncapOption)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfEncapOption (i4FsMITunlIfAddressType,
                                   pFsMITunlIfLocalInetAddress,
                                   pFsMITunlIfRemoteInetAddress,
                                   i4FsMITunlIfEncapsMethod,
                                   i4FsMITunlIfConfigID,
                                   i4SetValFsMITunlIfEncapOption);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfAlias
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfAlias
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfAlias (INT4 i4FsMIStdIpContextId, INT4 i4FsMITunlIfAddressType,
                       tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                       tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                       INT4 i4FsMITunlIfEncapsMethod, INT4 i4FsMITunlIfConfigID,
                       tSNMP_OCTET_STRING_TYPE * pSetValFsMITunlIfAlias)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfAlias (i4FsMITunlIfAddressType,
                             pFsMITunlIfLocalInetAddress,
                             pFsMITunlIfRemoteInetAddress,
                             i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                             pSetValFsMITunlIfAlias);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfCksumFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfCksumFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfCksumFlag (INT4 i4FsMIStdIpContextId,
                           INT4 i4FsMITunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfRemoteInetAddress,
                           INT4 i4FsMITunlIfEncapsMethod,
                           INT4 i4FsMITunlIfConfigID,
                           INT4 i4SetValFsMITunlIfCksumFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfCksumFlag (i4FsMITunlIfAddressType,
                                 pFsMITunlIfLocalInetAddress,
                                 pFsMITunlIfRemoteInetAddress,
                                 i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                 i4SetValFsMITunlIfCksumFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfPmtuFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfPmtuFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfPmtuFlag (INT4 i4FsMIStdIpContextId,
                          INT4 i4FsMITunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsMITunlIfRemoteInetAddress,
                          INT4 i4FsMITunlIfEncapsMethod,
                          INT4 i4FsMITunlIfConfigID,
                          INT4 i4SetValFsMITunlIfPmtuFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfPmtuFlag (i4FsMITunlIfAddressType,
                                pFsMITunlIfLocalInetAddress,
                                pFsMITunlIfRemoteInetAddress,
                                i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                i4SetValFsMITunlIfPmtuFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhSetFsMITunlIfStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                setValFsMITunlIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMITunlIfStatus (INT4 i4FsMIStdIpContextId, INT4 i4FsMITunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                        INT4 i4FsMITunlIfEncapsMethod,
                        INT4 i4FsMITunlIfConfigID,
                        INT4 i4SetValFsMITunlIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        return i1Return;;
    }

    i1Return =
        nmhSetFsTunlIfStatus (i4FsMITunlIfAddressType,
                              pFsMITunlIfLocalInetAddress,
                              pFsMITunlIfRemoteInetAddress,
                              i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                              i4SetValFsMITunlIfStatus);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfHopLimit
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfHopLimit (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                             INT4 i4FsMITunlIfAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfLocalInetAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfRemoteInetAddress,
                             INT4 i4FsMITunlIfEncapsMethod,
                             INT4 i4FsMITunlIfConfigID,
                             INT4 i4TestValFsMITunlIfHopLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfHopLimit (pu4ErrorCode, i4FsMITunlIfAddressType,
                                   pFsMITunlIfLocalInetAddress,
                                   pFsMITunlIfRemoteInetAddress,
                                   i4FsMITunlIfEncapsMethod,
                                   i4FsMITunlIfConfigID,
                                   i4TestValFsMITunlIfHopLimit);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfTOS
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfTOS
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfTOS (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                        INT4 i4FsMITunlIfAddressType,
                        tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                        tSNMP_OCTET_STRING_TYPE * pFsMITunlIfRemoteInetAddress,
                        INT4 i4FsMITunlIfEncapsMethod,
                        INT4 i4FsMITunlIfConfigID, INT4 i4TestValFsMITunlIfTOS)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfTOS (pu4ErrorCode, i4FsMITunlIfAddressType,
                              pFsMITunlIfLocalInetAddress,
                              pFsMITunlIfRemoteInetAddress,
                              i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                              i4TestValFsMITunlIfTOS);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfFlowLabel
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfFlowLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfFlowLabel (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              INT4 i4FsMITunlIfAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMITunlIfLocalInetAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMITunlIfRemoteInetAddress,
                              INT4 i4FsMITunlIfEncapsMethod,
                              INT4 i4FsMITunlIfConfigID,
                              INT4 i4TestValFsMITunlIfFlowLabel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfFlowLabel (pu4ErrorCode, i4FsMITunlIfAddressType,
                                    pFsMITunlIfLocalInetAddress,
                                    pFsMITunlIfRemoteInetAddress,
                                    i4FsMITunlIfEncapsMethod,
                                    i4FsMITunlIfConfigID,
                                    i4TestValFsMITunlIfFlowLabel);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfDirFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfDirFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfDirFlag (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                            INT4 i4FsMITunlIfAddressType,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsMITunlIfLocalInetAddress,
                            tSNMP_OCTET_STRING_TYPE *
                            pFsMITunlIfRemoteInetAddress,
                            INT4 i4FsMITunlIfEncapsMethod,
                            INT4 i4FsMITunlIfConfigID,
                            INT4 i4TestValFsMITunlIfDirFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfDirFlag (pu4ErrorCode, i4FsMITunlIfAddressType,
                                  pFsMITunlIfLocalInetAddress,
                                  pFsMITunlIfRemoteInetAddress,
                                  i4FsMITunlIfEncapsMethod,
                                  i4FsMITunlIfConfigID,
                                  i4TestValFsMITunlIfDirFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfDirection
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfDirection (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              INT4 i4FsMITunlIfAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMITunlIfLocalInetAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMITunlIfRemoteInetAddress,
                              INT4 i4FsMITunlIfEncapsMethod,
                              INT4 i4FsMITunlIfConfigID,
                              INT4 i4TestValFsMITunlIfDirection)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfDirection (pu4ErrorCode, i4FsMITunlIfAddressType,
                                    pFsMITunlIfLocalInetAddress,
                                    pFsMITunlIfRemoteInetAddress,
                                    i4FsMITunlIfEncapsMethod,
                                    i4FsMITunlIfConfigID,
                                    i4TestValFsMITunlIfDirection);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfEncaplmt
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfEncaplmt
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfEncaplmt (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                             INT4 i4FsMITunlIfAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfLocalInetAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfRemoteInetAddress,
                             INT4 i4FsMITunlIfEncapsMethod,
                             INT4 i4FsMITunlIfConfigID,
                             UINT4 u4TestValFsMITunlIfEncaplmt)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfEncaplmt (pu4ErrorCode, i4FsMITunlIfAddressType,
                                   pFsMITunlIfLocalInetAddress,
                                   pFsMITunlIfRemoteInetAddress,
                                   i4FsMITunlIfEncapsMethod,
                                   i4FsMITunlIfConfigID,
                                   u4TestValFsMITunlIfEncaplmt);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfEncapOption
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfEncapOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfEncapOption (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                                INT4 i4FsMITunlIfAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMITunlIfLocalInetAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMITunlIfRemoteInetAddress,
                                INT4 i4FsMITunlIfEncapsMethod,
                                INT4 i4FsMITunlIfConfigID,
                                INT4 i4TestValFsMITunlIfEncapOption)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfEncapOption (pu4ErrorCode, i4FsMITunlIfAddressType,
                                      pFsMITunlIfLocalInetAddress,
                                      pFsMITunlIfRemoteInetAddress,
                                      i4FsMITunlIfEncapsMethod,
                                      i4FsMITunlIfConfigID,
                                      i4TestValFsMITunlIfEncapOption);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfAlias
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfAlias
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfAlias (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                          INT4 i4FsMITunlIfAddressType,
                          tSNMP_OCTET_STRING_TYPE * pFsMITunlIfLocalInetAddress,
                          tSNMP_OCTET_STRING_TYPE *
                          pFsMITunlIfRemoteInetAddress,
                          INT4 i4FsMITunlIfEncapsMethod,
                          INT4 i4FsMITunlIfConfigID,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsMITunlIfAlias)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfAlias (pu4ErrorCode, i4FsMITunlIfAddressType,
                                pFsMITunlIfLocalInetAddress,
                                pFsMITunlIfRemoteInetAddress,
                                i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                pTestValFsMITunlIfAlias);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfCksumFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfCksumFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfCksumFlag (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                              INT4 i4FsMITunlIfAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMITunlIfLocalInetAddress,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMITunlIfRemoteInetAddress,
                              INT4 i4FsMITunlIfEncapsMethod,
                              INT4 i4FsMITunlIfConfigID,
                              INT4 i4TestValFsMITunlIfCksumFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfCksumFlag (pu4ErrorCode, i4FsMITunlIfAddressType,
                                    pFsMITunlIfLocalInetAddress,
                                    pFsMITunlIfRemoteInetAddress,
                                    i4FsMITunlIfEncapsMethod,
                                    i4FsMITunlIfConfigID,
                                    i4TestValFsMITunlIfCksumFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfPmtuFlag
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfPmtuFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfPmtuFlag (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                             INT4 i4FsMITunlIfAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfLocalInetAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMITunlIfRemoteInetAddress,
                             INT4 i4FsMITunlIfEncapsMethod,
                             INT4 i4FsMITunlIfConfigID,
                             INT4 i4TestValFsMITunlIfPmtuFlag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfPmtuFlag (pu4ErrorCode, i4FsMITunlIfAddressType,
                                   pFsMITunlIfLocalInetAddress,
                                   pFsMITunlIfRemoteInetAddress,
                                   i4FsMITunlIfEncapsMethod,
                                   i4FsMITunlIfConfigID,
                                   i4TestValFsMITunlIfPmtuFlag);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/****************************************************************************
 Function    :  nmhTestv2FsMITunlIfStatus
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID

                The Object 
                testValFsMITunlIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMITunlIfStatus (UINT4 *pu4ErrorCode, INT4 i4FsMIStdIpContextId,
                           INT4 i4FsMITunlIfAddressType,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfLocalInetAddress,
                           tSNMP_OCTET_STRING_TYPE *
                           pFsMITunlIfRemoteInetAddress,
                           INT4 i4FsMITunlIfEncapsMethod,
                           INT4 i4FsMITunlIfConfigID,
                           INT4 i4TestValFsMITunlIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (CfaUtilSetTnlCxt ((UINT4) i4FsMIStdIpContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;;
    }

    i1Return =
        nmhTestv2FsTunlIfStatus (pu4ErrorCode, i4FsMITunlIfAddressType,
                                 pFsMITunlIfLocalInetAddress,
                                 pFsMITunlIfRemoteInetAddress,
                                 i4FsMITunlIfEncapsMethod, i4FsMITunlIfConfigID,
                                 i4TestValFsMITunlIfStatus);
    CfaUtilResetTnlCxt ();
    return i1Return;;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMITunlIfTable
 Input       :  The Indices
                FsMIStdIpContextId
                FsMITunlIfAddressType
                FsMITunlIfLocalInetAddress
                FsMITunlIfRemoteInetAddress
                FsMITunlIfEncapsMethod
                FsMITunlIfConfigID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMITunlIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
