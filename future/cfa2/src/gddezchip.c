/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 *  $Id: gddezchip.c,v 1.8 2016/07/23 11:41:25 siva Exp $
 *
 *  Description:This file contains the routines for the     
 *              Gemeric Device Driver Module of the CFA.    
 *              These routines are called at various places     
 *              in the CFA modules for interfacing with the    
 *              device drivers in the system. These routines   
 *              have to be modified when moving onto different  
 *              drivers. 
 *              Drivers currently supported are                  
 *                  SOCK_PACKET for Ethernet                      
 *                  WANIC HDLC driver                             
 *                  SANGOMA wanpipe driver                        
 *                  ETINC driver                                  
 *                  ATMVC's support                               
 *                              
 *******************************************************************/

#include "cfainc.h"
#include "cfaport.h"
#include "gddipdx.h"
#include "issnvram.h"
#include "fsvlan.h"

PRIVATE INT4        NpCfaChangeVlanTag (tCRU_BUF_CHAIN_HEADER * pBuf);

PUBLIC INT4         CfaShutdownDeleteDmaPool (VOID);

/*****************************************************************************
 *
 *    Function Name      : CfaGddInit
 *
 *    Description        : This function performs the initialisation of
 *                         the Generic Device Driver Module of CFA. This
 *                         initialization routine should be called
 *                         from the init of IFM after we have obtained
 *                         the number of physical ports after parsing of
 *                         the Config file. This function initializes the FD list
 *                         and ifIndex array of the polling table.
 *
 *    Input(s)           : None.
 *
 *    Output(s)          : None.
 *
 *    Global Variables Referred : FdTable
 *
 *    Global Variables Modified : FdTable
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddInit (VOID)
{
    tMacAddr            au1MacAddr;

    CfaGetSysMacAddress (au1MacAddr);
    CfaNpUpdateSwitchMac (au1MacAddr);
    /* Initialize the Borad and UnitInfo structures */
    if (CfaNpInit () != FNP_SUCCESS)
    {
        PRINTF ("ERROR[NP] - CfaNpInit returned failure\n\r");
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      :  CfaGddGetLinkStatus                             */
/*                                                                          */
/*    Description        : Gets the Physical Link Status of the device      */
/*    Input(s)           : Logical Port ,Vlan and the Data Buffer           */
/*                                                                          */
/*    Output(s)          : Link Status for the specific IfIndex             */
/*                                                                          */
/****************************************************************************/

UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    UINT1               u1IfType;
    UINT1               u1OperStatus;

    CfaGetIfType (u4IfIndex, &u1IfType);
    if (u1IfType == CFA_ENET)
    {
        return CfaCfaNpGetLinkStatus (u4IfIndex);
    }
    else
    {
        CfaGetIfOperStatus (u4IfIndex, &u1OperStatus);
        return (u1OperStatus);
    }
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthWrite
 *    Description          : Writes the data to the ethernet driver.
 *    Input(s)             : pu1DataBuf - Pointer to the linear buffer.
 *                           u4IfIndex - MIB-2 interface index
 *                           u4PktSize - Size of the buffer.
 *    Output(s)            : None.
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{

    if (FNP_SUCCESS != CfaHwL2VlanIntfWrite (pu1DataBuf, u4IfIndex, u4PktSize))
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *					        be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen, 
		       UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}
/*****************************************************************************
 *    Function Name      : CfaGddProcessRecvInterruptEvent ()
 *    Description        : This function is invoked when an driver
 *                         event occurs. This routine invokes the packet
 *                         processing function if a packet is received
 *                         else it invokes the function which process the
 *                         driver status if status indication message is
 *                         received
 *    Input(s)           : VOID
 *    Output(s)          : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : None.
 ******************************************************************************/
PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               u1IfType;

    while (OsixReceiveFromQ (SELF, CFA_PACKET_QUEUE_MUX, OSIX_NO_WAIT, 0,
                             (tCRU_BUF_CHAIN_HEADER **) & pBuf) == OSIX_SUCCESS)
    {
        CFA_LOCK ();
        u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        if (pBuf == NULL)
        {
            printf ("pBuff Empty: OsixReceiveFromQ Failed\n");
        }
        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Pkt received in interrupt mode failed to deliver to HL.\n");
        }
        else
        {
            /* EtherType check */
            if (NpCfaChangeVlanTag (pBuf) != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher 
                 * layer */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
            }
            if ((CFA_GDD_HL_RX_FNPTR (u4IfIndex)) (pBuf, u4IfIndex, u4PktSize,
                                                   u1IfType, CFA_ENCAP_NONE)
                != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher 
                 * layer */

                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CFA_IF_SET_IN_DISCARD (u4IfIndex);
            }
            else
            {
                /* increment after successful handling of packet */
                CFA_IF_SET_IN_OCTETS (u4IfIndex, u4PktSize);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt received in interrupt mode delivered to HL.\n");
            }
        }
        CFA_UNLOCK ();
    }
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name      : NpCfaChangeVlanTag
 *    Description        : This function will check, whether VLAN tag 
 *                         needs to be changed in the incoming packet.
 *    Input(s)           : pBuf - Pointer to the linear buffer.
 *    Output(s)          : None. 
 *
 *    Global Variables Referred : None.
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_TRUE or CFA_FALSE.
 *
 *****************************************************************************/
PRIVATE INT4
NpCfaChangeVlanTag (tCRU_BUF_CHAIN_HEADER * pBuf)
{

    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;
    UINT4               u4IfIndex;
    UINT2               u2LenOrType = 0;
    UINT2               u2EtherType = 0;
    UINT2               u2AggId;
    tEnetV2Header      *pEthHdr = NULL;
    UINT1               au1GmrpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x20 };
    UINT4               u4TempIfIndex = 0;
    BOOL1               bIsTunnelPort;

    /* Marvell XCAT will not add any extra tag to the packet coming
     * to CPU. Whatever tag cassified in the packet during ingress
     * will be given to CPU with ethertype changed to 8100.
     * In case of provider bridging this is giving problem.Hence
     * the packet ethertype is changed to the port ethertype if the 
     * packet contains even one tag.
     * * */

    u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                               u4VlanOffset, CFA_ENET_TYPE_OR_LEN);
    u2LenOrType = OSIX_NTOHS (u2LenOrType);

    pEthHdr = (tEnetV2Header *) (VOID *) CRU_BMC_Get_DataPointer (pBuf);

    /* If the port belongs to port-channel, then get the port-channel id.
     * If the port is not part of any port-channel, then u2AggId will be
     * same as the given u4IfIndex. */
    if (L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId) == L2IWF_SUCCESS)
    {
        u4TempIfIndex = u2AggId;
    }
    else
    {
        u4TempIfIndex = u4IfIndex;
    }

    L2IwfGetPortVlanTunnelStatus (u4TempIfIndex, &bIsTunnelPort);

    if (bIsTunnelPort == OSIX_TRUE)
    {
        if (MEMCMP (pEthHdr->au1DstAddr, au1GmrpAddr, CFA_ENET_ADDR_LEN) == 0)
        {
            /* Incase of GMRP Tunnling do not change ether type present
             * in the packet 
             * */
            return CFA_SUCCESS;
        }
    }

    if (u2LenOrType == CFA_VLAN_PROTOCOL_ID)
    {
        VlanGetIngressPortEtherType (u4TempIfIndex, &u2EtherType);

        u2EtherType = OSIX_NTOHS (u2EtherType);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EtherType,
                                   u4VlanOffset, CFA_ENET_TYPE_OR_LEN);
    }

    return CFA_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CfaGddSendIntrEvent                              */
/*                                                                          */
/*    Description        :  Sends an event to CFA to indicate packet arrival*/
/*                          at the driver                                   */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/****************************************************************************/
UINT4
CfaGddSendIntrEvent (VOID)
{
    OsixSendEvent (SELF, (UINT1 *) "CFA", CFA_GDD_INTERRUPT_EVENT);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name      : CfaGddConfigPort
 *    Description        : Function is called for configuration of the
 *                         Enet ports. No such support is available for
 *                         Wanic at present. Only the Promiscuous mode
 *                         and multicast addresses can be configured at
 *                         present for Enet ports. This is called
 *                         directly whenever the Manager configures the
 *                         ifTable for Ethernet ports. It can also be
 *                         called indirectly by Bridge or RIP, etc.
 *                         through the CfaIfmEnetConfigMcastAddr API.
 *    Input(s)           : UINT4 u4IfIndex,
 *                         UINT1 u1ConfigOption,
 *                         VOID *pConfigParam.
 *    Output(s)          : None.
 *
 *    Global Variables Referred : gaPhysIfTable (GDD Reg Table) structure.
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if configuration succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ConfigOption);
    UNUSED_PARAM (pConfigParam);
    return (CFA_SUCCESS);
}

/**************************************************************************/
VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/**************************************************************************/
VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddGetHwAddr
 *    Description          : Function is called for getting the hardware
 *                           address of the Enet ports. This function can
 *                           be called only after the opening of the ports
 *                           - the ports should be open but Admin/Oper
 *                           Status can be down.
 *    Input(s)             : UINT4 u4IfIndex.
 *    Output(s)            : UINT1 *au1HwAddr.
 *
 *    Global Variables Referred : gIfGlobal (Interface's global struct),
 *                                gaPhysIfTable (GDD Reg Table) structure.
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if address is obtained,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (au1HwAddr);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddShutdown
 *    Description          : This function performs the shutdown of
 *                           the Generic Device Driver Module of CFA. This
 *                           routine frees the polling table structure.
 *    Input(s)             : None.
 *    Output(s)            : None.
 *
 *    Global Variables Referred : FdTable.
 *    Global Variables Modified : FdTable.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : None.
 *
 *****************************************************************************/
VOID
CfaGddShutdown (VOID)
{
    CfaShutdownDeleteDmaPool ();
}

/**************************************************************************/
INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);
    CFA_GDD_TYPE (u4IfIndex) = u1IfType;

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthOpen
 *    Description          : This function opens the Ethernet port and stores the
 *                           descriptor in the interface table. Since the port is 
 *                           opened already during the registration time, this 
 *                           function is dummy.
 *    Input(s)             : u4IfIndex - Interface Index
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure
 *    Global Variables Modified :  gapIfTable (Interface table) structure
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if open succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthClose
 *    Description          : First disables the promiscuous mode of 
 *                           the port, if enabled. The multicast address
 *                           list is lost when the port is closed.i
 *                           We dont need to check if the call is a 
 *                           success or not.
 *    Input(s)             : u4IfIndex - Interface index.
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *    Global Variables Modified : gapIfTable
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if close succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGddEthRead
 *    Description          : This function reads the data from the ethernet port.
 *    Input(s)             : pu1DataBuf - Pointer to the linear buffer.
 *                           u4IfIndex - IfIndex of the interface
 *                           pu4PktSize - Pointer to the packet size
 *    Output(s)            : pu1DataBuf, pu1PktSize
 *
 *    Global Variables Referred : gapIfTable (Interface table)
 *                                structure gapIfTable
 *    Global Variables Modified : None
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS or CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    UNUSED_PARAM (pu1DataBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4PktSize);
    return CFA_SUCCESS;
}

/**************************************************************************/
INT4
CfaGddOsProcessRecvEvent (VOID)
{
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId, bBcast,
                                                &u4OutPort, &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}
