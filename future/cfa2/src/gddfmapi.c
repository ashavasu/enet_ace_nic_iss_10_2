/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: gddfmapi.c,v 1.12 2016/07/23 11:41:25 siva Exp $
 *
 * Description: This file contains the function implementations  of FS NP-API.
 ****************************************************************************/
#include "cfainc.h"
#include "fsvlan.h"
#include "vlangarp/garp.h"
#include "igs.h"
tOsixSemId          gNpapiSemId;
UINT1               au1NpapiSemName[] = "NPSM";

#ifdef NPAPI_WANTED
#ifndef CFA_INTERRUPT_MODE
#error "Enable INTERRUPT_MODE in CFA"
#endif
#include "cfanp.h"
#endif
#include "gddfmapi.h"

PRIVATE INT4        NpCfaIsVlanTagRequest (tCRU_BUF_CHAIN_HEADER * pBuf);

/*****************************************************************************
 *    Function Name      : CfaGddProcessRecvInterruptEvent ()
 *    Description        : This function is invoked when an driver 
 *                         event occurs. This routine invokes the packet
 *                         processing function if a packet is received 
 *                         else it invokes the function which process the
 *                         driver status if status indication message is
 *                         received
 *    Input(s)           : VOID
 *    Output(s)          : None.
 *
 *    Globals Referred         : None
 *    Globals Modified         : gu1CfaTaskStatus,
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Returns                  : None.
 *****************************************************************************/

PUBLIC INT4
CfaGddProcessRecvInterruptEvent (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf;
    UINT4               u4IfIndex;
    UINT4               u4PktSize;
    UINT1               au1EnetHdr[VLAN_TAG_OFFSET];
    UINT1               u1IfType;

#ifdef ICCH_WANTED
    UINT2               u2EtherType = 0;
#endif

    while (OsixQueRecv (CFA_PACKET_MUX_QID, (UINT1 *) &pCruBuf,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        CFA_LOCK ();

#ifdef ICCH_WANTED
        /*Fetch the Ethertype from packet*/
        CRU_BUF_Copy_FromBufChain (pCruBuf, (UINT1 *) &u2EtherType,
                                  CFA_HB_MSG_ETHERTYPE_OFFSET,
                                   CFA_HB_MSG_ETHERTYPE_LEN);

        u2EtherType = OSIX_NTOHS(u2EtherType);
        if (u2EtherType == CFA_HB_MSG_ETHERTYPE)
        {
            /*Move the valid offset to 18 Bytes */
            /*18 Bytes = dst mac,src mac,vlan tag, hb ethertype*/

            /*Post the packet to HB task*/
            CfaHbIcchPostHBPktToIcch (pCruBuf);
            CFA_UNLOCK ();
            return (CFA_SUCCESS);
        }
#endif

        /* extract the interface index and packet size from buffer */
        if (NpCfaIsVlanTagRequest (pCruBuf) == CFA_FALSE)
        {
            CRU_BUF_Copy_FromBufChain (pCruBuf, au1EnetHdr, 0, VLAN_TAG_OFFSET);
            CRU_BUF_Move_ValidOffset (pCruBuf,
                                      VLAN_TAG_OFFSET + VLAN_TAG_PID_LEN);
            CRU_BUF_Prepend_BufChain (pCruBuf, au1EnetHdr, VLAN_TAG_OFFSET);
        }

        u4IfIndex = pCruBuf->ModuleData.InterfaceId.u4IfIndex;
        u4PktSize = pCruBuf->pFirstValidDataDesc->u4_ValidByteCount;

        if (CfaGetIfType (u4IfIndex, &u1IfType) == CFA_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
            CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                     "In CfaMain Pkt received in interrupt mode failed to deliver to HL.\n");
        }
        else
        {
            /* Handover the Packet to Receive module */
            if ((CFA_GDD_HL_RX_FNPTR ((UINT2) u4IfIndex))
                (pCruBuf, u4IfIndex, u4PktSize, u1IfType,
                 CFA_ENCAP_NONE) != CFA_SUCCESS)
            {
                /* release buffer which were not successfully sent to higher layer
                 */
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt received in interrupt mode failed to deliver to HL.\n");
            }
            else
            {
                /* increment after successful handling of packet */
                CFA_IF_SET_IN_OCTETS ((UINT2) u4IfIndex, u4PktSize);
                CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                         "In CfaMain Pkt received in interrupt mode delivered to HL.\n");
            }
        }
        CFA_UNLOCK ();
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name      : CfaGddInit
 *    Description        : This function performs the initialisation of
 *                         the  Device Driver Module of CFA. This routine
 *                         1. creates the Queue for reception of packets from
 *                            driver
 *                         2. creates the memory pool for the driver messages
 *                         3. initializes the driver to cfa mapping tables
 *    Input(s)           : None.
 *    Output(s)          : None.
 *
 *    Globals Referred         : None
 *    Globals Modified         : gaCfaNpIndexMap 
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion         : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
PUBLIC INT4
CfaGddInit (VOID)
{
    tMacAddr            au1SwitchMac;    /* Base MAC address */

    CfaGetSysMacAddress (au1SwitchMac);

    CfaNpUpdateSwitchMac (au1SwitchMac);

    /* Create semaphore for NPAPI module */
    if (OsixSemCrt (au1NpapiSemName, &gNpapiSemId) != OSIX_SUCCESS)
    {
        PRINTF ("Failed to created semaphore for NPAPI module.\n");
        return CFA_FAILURE;
    }
    /* After creation semaphore must first be given before it can be taken,
     * because semafore is initialized by 0 on creation */
    OsixSemGive (gNpapiSemId);

    /* Initialize the Borad and UnitInfo structures */
    if (CfaNpInit () != FNP_SUCCESS)
    {
        PRINTF ("ERROR[NP] - CfaNpInit returned failure\n\r");
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name      : CfaGddShutdown
 *    Description        : This function performs the initialisation of
 *                         the Generic Device Driver Module of CFA. This
 *                         initialization routine should be called
 *                         from the init of IFM after we have obtained
 *                         the number of physical ports after parsing of
 *                         the Config file. This function initializes the FD list
 *                         and ifIndex array of the polling table.
 *    Input(s)           : None.
 *    Output(s)          : None.
 *
 *    Globals Referred         : FdTable
 *    Globals Modified         : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion         : None.
 *    Returns                  : CFA_SUCCESS if initialisation succeeds,
 *                               otherwise CFA_FAILURE.
 *****************************************************************************/

PUBLIC VOID
CfaGddShutdown (VOID)
{
    return;
}

VOID
CfaGddOsRemoveIfFromList (UINT4 u4IfIndex)
{
    (VOID) u4IfIndex;
}

INT4
CfaGddOsProcessRecvEvent (VOID)
{
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name      : CfaGddGetOsHwAddr
 *
 *    Description        : Function is called for getting the hardware
 *                         address of the Enet ports at Interface initialisation.
 *                         This function can be called only at port creation time.
 *                         The MAC address is obtained in the same way as it is used
 *                         for IndexMap table initialisation at GddInit.
 *
 *    Input(s)           : UINT2 u2IfIndex.
 *
 *    Output(s)          : UINT1 *au1HwAddr.
 *
 *    Returns            : CFA_SUCCESS if address is obtained,
 *                         otherwise CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddGetOsHwAddr (UINT4 u4IfIndex, UINT1 *au1HwAddr)
{
    if (CfaIsL3IpVlanInterface (u4IfIndex) == CFA_SUCCESS)
    {
        /* For IVR interfaces, Get the Switch Mac Address from CDB table */
        CfaGetIfHwAddr (u4IfIndex, au1HwAddr);
        return CFA_SUCCESS;
    }

    /* Get the port mac address from NP for each port
     * at port creation & store it in CFA CDB table.
     * Here onwards protocol should refer CDB table 
     * for port mac retrieval.
     */
    CfaNpGetHwAddr (u4IfIndex, au1HwAddr);

    return CFA_SUCCESS;
}

INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pConfigParam);
    UNUSED_PARAM (u1ConfigOption);
    return CFA_SUCCESS;
}

INT4
CfaGddSetGddType (UINT4 u4IfIndex)
{
    UINT1               u1IfType;

    CfaGetIfType (u4IfIndex, &u1IfType);

    CFA_GDD_TYPE (u4IfIndex) = u1IfType;
    return (CFA_SUCCESS);
}

PUBLIC VOID
CfaGddOsAddIfToList (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
}

/* Link up/down changes start */
/*****************************************************************************
 *    Function Name       : CfaGddGetLinkStatus
 *    Description         : This function  retrives the status of the Link
 *    Input(s)            : None.
 *    Output(s)           : None.
 *    Globals Referred         : FdTable
 *    Globals Modified         : FdTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion         : None.
 *    Returns                  : CFA_IF_DOWN if the Link is Down
 *                               else, CFA_IF_UP.
 *****************************************************************************/

PUBLIC UINT1
CfaGddGetLinkStatus (UINT4 u4IfIndex)
{
    UINT4               u4LinkStatus = CFA_IF_UP;

    if ((CFA_MGMT_PORT == TRUE) && (u4IfIndex == CFA_OOB_MGMT_IFINDEX))
    {
        return u4LinkStatus;
    }
    return CfaCfaNpGetLinkStatus (u4IfIndex);
}

/*****************************************************************************
 *    Function Name      : CfaGddEthOpen
 *    Description        : This function opens the Ethernet port and stores the
                           descriptor in the interface table. Since the port is 
                           opened already during the registration time, this 
                           function is dummy.
 *    Input(s)           : u2IfIndex - Interface Index
 *    Output(s)          : None.
 *
 *    Globals Referred         : gapIfTable (Interface table)
 *                               structure gapIfTable (Interface table)
 *                               structure
 *    Globals Modified         :  gapIfTable (Interface table) structure
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion         : None.
 *    Returns                  : CFA_SUCCESS if open succeeds,
 *                               otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthOpen (UINT4 u4IfIndex)
{
    if (CfaNpPortOpen (u4IfIndex) != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name      : CfaGddEthClose
 *    Description        : First disables the promiscuous mode of 
 *                         the port, if enabled. The multicast address
 *                         list is lost when the port is closed.i
 *                         We dont need to check if the call is a 
 *                         success or not.
 *    Input(s)           : u2IfIndex - Interface index.
 *    Output(s)          : None.
 *
 *    Globals Referred         : gapIfTable (Interface table)
 *                               structure gapIfTable
 *    Globals Modified         : gapIfTable
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion         : None.
 *    Returns                  : CFA_SUCCESS if close succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthClose (UINT4 u4IfIndex)
{
    if (CfaNpPortClose (u4IfIndex) != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }

    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name      : CfaGddEthWrite
 *    Description        : Writes the data to the ethernet driver.
 *    Input(s)           : pu1DataBuf - Pointer to the linear buffer.
 *                         u2IfIndex - MIB-2 interface index
 *                         u4PktSize - Size of the buffer.
 *    Output(s)          : None.
 *
 *    Globals Referred         : gapIfTable (Interface table)
 *                                structure gapIfTable (Interface table)
 *                                structure,
 *    Globals Modified         : None.
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion         : None.
 *    Returns                  : CFA_SUCCESS if write succeeds,
 *                               otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWrite (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 u4PktSize)
{
    if (CfaNpPortWrite (pu1DataBuf, u4IfIndex, u4PktSize) != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name             : CfaGddEthWriteWithPri
 *    Description               : Writes the data to the ethernet driver.
 *    Input(s)                  : pu1DataBuf - Pointer to the linear buffer.
 *                                u4IfIndex  - MIB-2 interface index
 *                                u4PktSize  - Size of the buffer.
 *                                u1Priority - Traffic class on which Pkt to
 *					        be TXed
 *    Output(s)                 : None.
 *    Global Variables Referred : None
 *    Global Variables Modified : None.
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *    Use of Recursion          : None.
 *    Returns                   : CFA_SUCCESS if write succeeds,
 *                                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthWriteWithPri (UINT1 *pData, UINT4 u4IfIndex, UINT4 u4PktLen, 
		       UINT1 u1Priority)
{
    if(CfaGddEthWrite(pData,u4IfIndex,u4PktLen) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    UNUSED_PARAM (u1Priority);
    return CFA_SUCCESS;
}
/*****************************************************************************
 *
 *    Function Name      : CfaGddEthRead
 *    Description        : This function reads the data from the ethernet port.
 *    Input(s)           : pu1DataBuf - Pointer to the linear buffer.
 *                         u2IfIndex - IfIndex of the interface
 *                         pu4PktSize - Pointer to the packet size
 *    Output(s)          : pu1DataBuf, pu1PktSize
 *
 *    Globals Referred         : gapIfTable (Interface table)
 *                               structure gapIfTable
 *    Globals Modified         : None
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion         : None.
 *    Returns                  : CFA_SUCCESS or CFA_FAILURE.
 *****************************************************************************/
INT4
CfaGddEthRead (UINT1 *pu1DataBuf, UINT4 u4IfIndex, UINT4 *pu4PktSize)
{
    if (CfaNpPortRead (pu1DataBuf, u4IfIndex, pu4PktSize) != FNP_SUCCESS)
    {
        return CFA_FAILURE;
    }
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : NpCfaIsVlanTagRequest
 *    Description        : This function will check, whether VLAN tag is
 *                         required for the incoming packet.
 *    Input(s)           : pBuf - Pointer to the linear buffer.
 *    Output(s)          : None. 
 *
 *    Globals Referred         : None.
 *    Globals Modified         : None.
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *    Use of Recursion         : None.
 *    Returns                  : CFA_TRUE or CFA_FALSE.
 *
 *****************************************************************************/
PRIVATE INT4
NpCfaIsVlanTagRequest (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tEnetV2Header       EthHdr;
    tEnetV2Header      *pEthHdr = &EthHdr;
    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;
    UINT4               u4ContextId = VLAN_DEF_CONTEXT_ID;
    UINT4               u4IfIndex;
    INT4                i4RetVal;
    UINT4               u4TempIfIndex = 0;
    UINT2               u2AggId = 0;
    UINT2               u2LenOrType = 0;
    UINT1               au1GmrpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x20 };
    UINT1               au1PvrstAddr[6] =
        { 0x01, 0x00, 0x0c, 0xcc, 0xcc, 0xcd };
    tMacAddr            ProviderGmrpAddr;
    tMacAddr            ProviderLacpAddr;
    tMacAddr            ProviderDot1xAddr;
    tMacAddr            ProviderGvrpAddr;
    tMacAddr            ProviderStpAddr;
    UINT1               au1GvrpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x21 };
    UINT1               au1StpAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00 };
    UINT1               au1LaAddr[6] = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x02 };
    BOOL1               bIsTunnelPort;

    /* CFA_GET_IFINDEX should not be used here, because in the packet
     * processing thread, Cfa Interface index is filled in
     * "ModuleData.InterfaceId" data structure.
     */
    u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;

    /* If the port belongs to port-channel, then get the port-channel id.
     * If the port is not part of any port-channel, then u2AggId will be
     * same as the given u4IfIndex. */
    if (L2IwfGetPortChannelForPort ((UINT2) u4IfIndex, &u2AggId) ==
        L2IWF_SUCCESS)
    {
        u4TempIfIndex = u2AggId;
    }
    else
    {
        u4TempIfIndex = u4IfIndex;
    }

    L2IwfGetPortVlanTunnelStatus ((UINT2) u4TempIfIndex, &bIsTunnelPort);
    if (bIsTunnelPort == OSIX_TRUE)
    {
        /* If it is a Tunnel Port - retain VLAN tag */
        return CFA_TRUE;
    }

    MEMCPY (pEthHdr, CRU_BMC_Get_DataPointer (pBuf), sizeof (tEnetV2Header));

    MEMSET (ProviderGmrpAddr, 0, sizeof (tMacAddr));

    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_GMRP_PROTO_ID,
                              ProviderGmrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_DOT1X_PROTO_ID,
                              ProviderDot1xAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_LACP_PROTO_ID,
                              ProviderLacpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_GVRP_PROTO_ID,
                              ProviderGvrpAddr);
    VlanGetTunnelProtocolMac (u4ContextId, VLAN_NP_STP_PROTO_ID,
                              ProviderStpAddr);

    if ((MEMCMP (pEthHdr->au1DstAddr, au1GmrpAddr, CFA_ENET_ADDR_LEN) == 0) ||
        (MEMCMP (pEthHdr->au1DstAddr, au1PvrstAddr, CFA_ENET_ADDR_LEN) == 0) ||
        (MEMCMP (pEthHdr->au1DstAddr, ProviderGvrpAddr, CFA_ENET_ADDR_LEN) == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderStpAddr, CFA_ENET_ADDR_LEN) ==
            0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderLacpAddr, CFA_ENET_ADDR_LEN) ==
            0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderDot1xAddr, CFA_ENET_ADDR_LEN)
            == 0)
        || (MEMCMP (pEthHdr->au1DstAddr, ProviderGmrpAddr, CFA_ENET_ADDR_LEN) ==
            0))
    {
        /* GMRP packet - retain VLAN tag */
        return CFA_TRUE;
    }

    if (VlanGetTagLenInFrame (pBuf, (UINT2) u4IfIndex, &u4VlanOffset)
        == VLAN_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   u4VlanOffset, CFA_ENET_TYPE_OR_LEN);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }

    /* Fulcrum gives protocol PDUs for STP, LA, GVRP, 802.1X without tag.
       So no need to strip any information from packet. */
    /* Provider Bridge is currently not handled */
    if ((0 == MEMCMP (pEthHdr->au1DstAddr, au1GvrpAddr, CFA_ENET_ADDR_LEN))
        || (0 == MEMCMP (pEthHdr->au1DstAddr, au1StpAddr, CFA_ENET_ADDR_LEN))
        || (0 == MEMCMP (pEthHdr->au1DstAddr, au1LaAddr, CFA_ENET_ADDR_LEN))
        || (PNAC_PAE_ENET_TYPE == u2LenOrType)
        || (PNAC_PAE_RSN_PREAUTH_ENET_TYPE == u2LenOrType))
    {
        return CFA_TRUE;
    }

    VlanNpRetainTag (u4IfIndex, pEthHdr->au1DstAddr, u2LenOrType, &i4RetVal);
    if (i4RetVal == VLAN_TRUE)
    {
        return CFA_TRUE;
    }

    /* Retain tag for all the L3 protocols  and Dot1x packets as well */
    if ((u2LenOrType == CFA_ENET_IPV4) || (u2LenOrType == CFA_ENET_ARP) ||
        (u2LenOrType == CFA_ENET_RARP) || (u2LenOrType == CFA_ENET_IPV6))
    {
        return CFA_TRUE;
    }

    return CFA_FALSE;
}

/*****************************************************************************
 *
 *    Function Name       :CfaGddTxPktOnVlanMemberPorts 
 *
 *    Description         :This routine transmits the given Frame on the member
 *                         ports of the given Vlan.
 *
 *    Input(s)            : u4L2ContextId - L2 Context Id to which VlanId is 
 *                                          associated
 *                        : pu1DataBuf  - Pointer to the frame buffer 
 *                          u4PktSize   - Size of the frame 
 *                          VlanId      - Vlan on which the frame is to be 
 *                                        transmitted
 *                          bBcast      - Whether this frame is to be 
 *                                        broadcasted  
 *
 *    Output(s)           : None
 *
 *    Global Variables Referred : None 
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/ CFA_FAILURE.
 *****************************************************************************/

INT4
CfaGddTxPktOnVlanMemberPortsInCxt (UINT4 u4L2ContextId,
                                   UINT1 *pu1DataBuf, tVlanId VlanId,
                                   BOOL1 bBcast, UINT4 u4PktSize)
{
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4BridgeMode;
#ifdef NPAPI_WANTED
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
#endif
    UINT4               u4OutPort;
    BOOL1               bIsTag;
    INT4                i4RetVal;
    UINT1              *pu1TempBuf = NULL;
    UINT1               TempDataBuf[CFA_ENET_MIN_UNTAGGED_FRAME_SIZE];
    UINT4               u4MinFrameMtu = 0;
    INT4                i4RetStat = CFA_SUCCESS;
    UINT1               u1PaddingDone = CFA_FALSE;
    MEMCPY (DestAddr, pu1DataBuf, CFA_ENET_ADDR_LEN);

    MEMSET (TempDataBuf, 0, sizeof (TempDataBuf));
    VlanInfo.u2VlanId = VlanId;
#ifdef NPAPI_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
#ifdef MBSM_WANTED
        /* If the packet belongs to STACK IVR, then send the
         * packet via stack port using ATP*/
        if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
        {
            if (CfaMbsmNpTxOnStackInterface (pu1DataBuf, u4PktSize) ==
                FNP_FAILURE)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddTxPktOnVlanMemberPorts - "
                         "Unable to tx on stack interface.\n");
                return CFA_FAILURE;
            }
            return CFA_SUCCESS;
        }
#endif
    }
#endif

    pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        return CFA_FAILURE;
    }

    pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        return CFA_FAILURE;
    }

    VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in Allocating memory for bitlist\n");
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        return CFA_FAILURE;
    }

    MEMSET (*pTagPorts, 0, sizeof (tPortList));
    MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
    i4RetVal = VlanIvrGetTxPortOrPortListInCxt (u4L2ContextId,
                                                DestAddr, VlanId, bBcast,
                                                &u4OutPort, &bIsTag, *pTagPorts,
                                                *pUnTagPorts);

    if (i4RetVal == VLAN_FORWARD)
    {
        /*ASSUMPTION: The Packet reached here will be untagged always.
         * Since bcoz this thread is called only for the outgoing packet
         * from IP.*/

        u4MinFrameMtu = CFA_ENET_MIN_UNTAGGED_FRAME_SIZE;

        if (u4PktSize < u4MinFrameMtu)
        {
            pu1TempBuf = pu1DataBuf;
            MEMCPY (TempDataBuf, pu1DataBuf, u4PktSize);
            pu1DataBuf = TempDataBuf;
            u4PktSize = u4MinFrameMtu;
            u1PaddingDone = CFA_TRUE;
        }

        CfaTxFillVlanInfo (DestAddr, u4OutPort,
                           bIsTag, *pTagPorts, *pUnTagPorts, &VlanInfo);
        if (L2IwfGetBridgeMode (L2IWF_DEFAULT_CONTEXT, &u4BridgeMode) ==
            L2IWF_FAILURE)
        {
            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                     "Error in CfaGddWrite - "
                     "L2IWF-GetBridgeMode -  FAILURE.\n");
            i4RetStat = CFA_FAILURE;
        }
        else
        {
#ifdef NPAPI_WANTED
            if (CFA_IS_NP_PROGRAMMING_ALLOWED () == CFA_TRUE)
            {
                if ((u4BridgeMode == L2IWF_CUSTOMER_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE))
                {
                    if (CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo)
                        != FNP_SUCCESS)
                    {
                        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                 "Error in CfaGddWrite - "
                                 "Unsuccessful Driver Write -  FAILURE.\n");
                        i4RetStat = CFA_FAILURE;
                    }
                }
                else if ((u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    if (u4OutPort != VLAN_INVALID_PORT)
                    {
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                                     "Error in CfaGddWrite - "
                                     "Unsuccessful Driver Write -  FAILURE.\n");
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                    else
                    {
                        for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE;
                             u2ByteInd++)
                        {
                            if ((*pTagPorts)[u2ByteInd] == 0)
                            {
                                continue;
                            }

                            u1PortFlag = (*pTagPorts)[u2ByteInd];

                            for (u2BitIndex = 0;
                                 ((u2BitIndex < BITS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & 0x80) != 0)
                                {
                                    VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                                    VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    u4OutPort =
                                        VlanInfo.unPortInfo.TxUcastPort.
                                        u2TxPort;

#ifdef VLAN_WANTED
                                    VlanGetPortEtherType (u4OutPort,
                                                          &u2EtherType);
#endif
                                    VlanInfo.unPortInfo.TxUcastPort.
                                        u2EtherType = u2EtherType;

                                    VlanInfo.unPortInfo.TxUcastPort.u1Tag =
                                        CFA_NP_TAGGED;

                                    CfaHwL3VlanIntfWrite (pu1DataBuf, u4PktSize,
                                                          VlanInfo);
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }
                        MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                                sizeof (tPortList));
                        VlanInfo.u2PktType = CFA_NP_UNKNOWN_UCAST_PKT;
                        /* 
                         * In 802.1ad Bridges, for untagged ports, the tag ethertype 
                         * need not be filled in based on the port ethertype. 
                         * Hence calling the API for packet transmission on L3 
                         * interfaces directly.
                         */
                        if (CfaHwL3VlanIntfWrite
                            (pu1DataBuf, u4PktSize, VlanInfo) != FNP_SUCCESS)
                        {
                            i4RetStat = CFA_FAILURE;
                        }
                    }
                }
            }
#else
            if (CfaL3VlanIntfWrite (pu1DataBuf, u4PktSize, VlanInfo) !=
                CFA_SUCCESS)
            {
                CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                         "Error in CfaGddWrite - "
                         "Unsuccessful Driver Write -  FAILURE.\n");
                i4RetStat = CFA_FAILURE;

            }
#endif
        }
        if (u1PaddingDone == CFA_TRUE)
        {
            pu1DataBuf = pu1TempBuf;
        }
        FsUtilReleaseBitList ((UINT1 *) pTagPorts);
        FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pTagPorts));
        FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                         pUnTagPorts));
        return i4RetStat;
    }

    FsUtilReleaseBitList ((UINT1 *) pTagPorts);
    FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pTagPorts));
    FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.pUnTagPorts));
    return CFA_FAILURE;
}
