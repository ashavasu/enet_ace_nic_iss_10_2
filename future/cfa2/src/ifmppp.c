/********************************************************************
 * Copyright (C) Future Sotware,1997-98,2001
 *
 * $Id: ifmppp.c,v 1.27 2014/12/09 12:48:13 siva Exp $
 *
 *
 * Description:This file contains the routines for the     
 *             Interface Management Module PPP/MP interfaces   
 *             of the CFA.                     
 *
 *******************************************************************/
#include "cfainc.h"
#ifdef DIFFSRV_WANTED
#include  "diffsrv.h"
#endif /* DIFFSRV_WANTED */
#ifdef PPP_WANTED
/**************************** GLOBAL DEFINITIONS *****************************/
/* the count of the PPP interfaces in the system */
UINT2               gu1NumofPppInterface = 0;

/* the count of the MP interfaces in the system */
UINT2               gu1NumofMpInterface = 0;
extern tMemPoolId   CFA_IPIF_MEMPOOL;

/*****************************************************************************
 *
 *    Function Name             : CfaIfmInitPppIfEntry
 *
 *    Description               : This function initialises the PPP ifEntries.
 *
 *    Input(s)                  : UINT2 u2IfIndex, UINT1 *au1PortName.
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if initialisation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmInitPppIfEntry (UINT4 u4IfIndex, UINT1 *pu1PortName)
#else /* __STDC__ */
INT4
CfaIfmInitPppIfEntry (u4IfIndex, *pu1PortName)
     UINT4               u4IfIndex;
     UINT1              *pu1PortName;
#endif /* __STDC__ */
{
    UINT4               u4Flag;
    tIpConfigInfo       IpConfigInfo;

    MEMSET (&IpConfigInfo, 0, sizeof (tIpConfigInfo));

    CfaSetIfType (u4IfIndex, CFA_PPP);

    /* allocate memory for PPP/MP IWF struct - if failure then interface cant be
       created */
    if ((CFA_IF_IWF (u4IfIndex) =
         MEM_MALLOC (sizeof (tPppIwfStruct), tPppIwfStruct)) == NULL)
    {

        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Exiting CfaIfmInitPppIfEntry - IWF struct malloc fail for interface %d\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    /* allocate for the WAN entry in the ifTable */
    if (MemAllocateMemBlock (CFA_WAN_MEMPOOL, (UINT1 **) (VOID *)
                             &(CFA_WAN_ENTRY (u4IfIndex))) != MEM_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPppIfEntry - Wan Mempool alloc fail for interface %d.\n",
                  u4IfIndex);

        return (CFA_FAILURE);
    }

    /* allocate for the IP config entry in the ifTable */
    if (CfaIpIfCreateIpInterface (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitVpncIfEntry - "
                  "IP Entry Mempool alloc fail for vpn interface %d.\n",
                  u4IfIndex);
        /* de-allocate memory taken from WAN Mempool */
        if (MemReleaseMemBlock (CFA_WAN_MEMPOOL,
                                (UINT1 *) CFA_WAN_ENTRY (u4IfIndex)) !=
            MEM_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmInitPppIfEntry - Cant releasing WAN entry MemBlock.\n");
        }
        else
        {
            CFA_WAN_ENTRY (u4IfIndex) = NULL;
        }
        /* Do cleanup of PPP interface entries */
        MEM_FREE (CFA_IF_IWF (u4IfIndex));

        return (CFA_FAILURE);
    }

/* Allocating the MemAllocate and later it has to be cleaned up 
 * to be implemented as per iss */

    if (MemAllocateMemBlock (CFA_IPIF_MEMPOOL, (UINT1 **)
                             (VOID *) &CFA_IP_ENTRY (u4IfIndex)) != MEM_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmInitPppIfEntry - IP-If Mempool alloc fail for interface %d.\n",
                  u4IfIndex);

        /* de-allocate memory taken from WAN Mempool */
        if (MemReleaseMemBlock (CFA_WAN_MEMPOOL,
                                (UINT1 *) CFA_WAN_ENTRY (u4IfIndex)) !=
            MEM_SUCCESS)
        {
            CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                     "Error in CfaIfmInitPppIfEntry - Cant releasing WAN entry MemBlock.\n");
        }
        else
        {
            CFA_WAN_ENTRY (u4IfIndex) = NULL;
        }

        return (CFA_FAILURE);
    }

    MEMSET (CFA_IF_IWF (u4IfIndex), 0, sizeof (tPppIwfStruct));

    /* Initialise the Wan config entry */
    CFA_IF_CONN_TYPE (u4IfIndex) = CFA_WAN_CONN_TYPE_OTHER;
    MEMSET (CFA_IF_PEER_MEDIA (u4IfIndex), 0, CFA_MAX_MEDIA_ADDR_LEN);
    CFA_IF_QOS_INDEX (u4IfIndex) = 0;
    CFA_IF_PERST (u4IfIndex) = CFA_PERS_OTHER;
    CFA_IF_DEMAND_CKT_STATUS (u4IfIndex) = CFA_DEMAND_CKT_CLOSE;
    CFA_IF_COMPR_EN (u4IfIndex) = CFA_FALSE;

    u4Flag = CFA_IP_IF_ALLOC_METHOD;
    IpConfigInfo.u1AddrAllocMethod = CFA_IP_ALLOC_NEGO;
    if (CfaIpIfSetIfInfo (u4IfIndex, u4Flag, &IpConfigInfo) == CFA_FAILURE)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Setting IP Param for Interface %d failed\n", u4IfIndex);
        return CFA_FAILURE;
    }

    CfaSetIfSpeed (u4IfIndex, 0);
    CfaSetIfNwType (u4IfIndex, CFA_NETWORK_TYPE_WAN);
    CfaSetIfWanType (u4IfIndex, CFA_WAN_TYPE_PUBLIC);

    STRCPY (CFA_IF_DESCR (u4IfIndex), CFA_PPP_DESCR);
    CfaSetIfName (u4IfIndex, pu1PortName);

    CFA_IF_ENCAP (u4IfIndex) = CFA_ENCAP_OTHER;
    /* update the PPP interface count for the next interface */
    ++CFA_PPP_IF_COUNT;
    /* Enable SNMP trap generation for PPP & MP interfaces */
    CFA_IF_TRAP_EN (u4IfIndex) = CFA_ENABLED;

    /* PPP interfaces are router ports by default */
    CfaSetIfBridgedIfaceStatus (u4IfIndex, CFA_DISABLED);

    /* there is no RCV addr table for PPP - so we dont add any nodes */
    TMO_SLL_Init (&CFA_IF_RCVADDRTAB (u4IfIndex));

    /* RS will be not-ready. After layering it, the status becomes not-in-service */
    CFA_IF_RS (u4IfIndex) = CFA_RS_NOTINSERVICE;
    CfaSetCdbRowStatus (u4IfIndex, CFA_RS_NOTINSERVICE);

    /* we can layer This interface below IP without any further config */
    CFA_IF_STACK_STATUS (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
        = CFA_RS_NOTINSERVICE;

    CFA_PPP_IWF_BYPASS (u4IfIndex) = (NON_PLAIN_RX | NON_PLAIN_TX |
                                      ACFC_PLAIN_RX);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name             : CfaIfmCreatePppInterface
 *
 *    Description               : This function is for registration of a
 *                               new PPP interface with PPP/MP.
 *
 *    Input(s)                  : UINT2 u2IfIndex,
 *                                tIfLayerInfo *pLowInterface,
 *                                tIfLayerInfo *pHighInterface,
 *                                tPppIpInfo *pIpInfo,
 *                                UINT4 u4IdleTimer.
 *
 *    Output(s)                 : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns                   : CFA_SUCCESS if creation succeeds,
 *                                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmCreatePppInterface (UINT4 u4IfIndex)
#else /* __STDC__ */
INT4
CfaIfmCreatePppInterface (u4IfIndex)
     UINT4               u4IfIndex;
#endif /* __STDC__ */
{
    tIfLayerInfo        LowInterface;
    tIfLayerInfo        HighInterface;
    tIpConfigInfo       IpInfo;
    UINT4               u4CurMtu;
    UINT4               u4Speed;
    UINT1               u1LowIfType;
    UINT1               u1CurType;
    UINT1               au1HwAddr[CFA_ENET_ADDR_LEN];

    /* initialize the config structures */
    LowInterface.u4IfIndex =
        CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY (u4IfIndex));
    if (LowInterface.u4IfIndex == CFA_NONE)
    {
        LowInterface.u1IfType = CFA_NONE;
    }
    else
    {
        CfaGetIfType (LowInterface.u4IfIndex, &u1CurType);
        LowInterface.u1IfType = u1CurType;

        /* Added for PPPOAAL5 support */
        LowInterface.u1EncapsType = CFA_IF_ENCAP (LowInterface.u4IfIndex);

    }

    HighInterface.u4IfIndex =
        CFA_IF_STACK_IFINDEX (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex));
    if (HighInterface.u4IfIndex == CFA_NONE)
    {
        HighInterface.u1IfType = CFA_NONE;
    }
    else
    {
        CfaGetIfType (HighInterface.u4IfIndex, &u1CurType);
        HighInterface.u1IfType = u1CurType;
    }

    /* Ideally the higher layer information may be provided after the
       link (LCP( is up. Currently, only IP is expected to run over these interfaces -
       CFA_NONE implies IP. Hence, if the Manager specifies the IP address info then
       we configure the higher layer information at the time of creation itself.
       Later we can use the u1IfType to indicate the higher layer. Even PPP/MP can
       inform the IP address info after negotiation.
       If required the Manager can specify the higher interface info at the
       time of creation itself.

       The information about lower layer is mandatory for creation of the PPP
       interface - currently it would only be a HDLC port. It is expected that the
       calling function provides the lower layer info. */

    /* register with the lower HDLC port first - this is must so that the link
       can be made up. In future the registration should be done in a switch
       statement - to enable PPP over other interfaces. */

    CfaGetIfType (LowInterface.u4IfIndex, &u1LowIfType);

    if ((LowInterface.u4IfIndex != CFA_NONE)
        && (u1LowIfType != CFA_ENET) /*PPPOE*/
        && CfaGddRegisterPort (LowInterface.u4IfIndex, CFA_PPP) != CFA_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmCreatePppInterface - GDD Registration fail for interface %d\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmCreatePppInterface - PPP-GDD Registration success - interface %d\n",
              u4IfIndex);

    /* Copy the speed of the lower layer physical interace to the
     * PPP interface */
    CfaGetIfSpeed (LowInterface.u4IfIndex, &u4Speed);
    CfaSetIfSpeed (u4IfIndex, u4Speed);
    CfaGetIfHighSpeed (LowInterface.u4IfIndex, &u4Speed);
    CfaSetIfHighSpeed (u4IfIndex, u4Speed);

    CfaGetIfMtu (u4IfIndex, &u4CurMtu);

    CfaGetIfHwAddr (LowInterface.u4IfIndex, au1HwAddr);
    CfaSetIfHwAddr (u4IfIndex, au1HwAddr, CFA_ENET_ADDR_LEN);

    CfaIpIfGetIfInfo (u4IfIndex, &IpInfo);
    /* Create the PPP interface in the PPP module */
    if (PppCreatePppInterface (u4IfIndex, &HighInterface, &LowInterface,
                               IpInfo.u4Addr,
                               CFA_IF_PEER_IPADDR (u4IfIndex),
                               u4CurMtu) != PPP_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmCreatePppInterface - PPP Registration fail for interface %d\n",
                  u4IfIndex);

        /* de-register with the lower HDLC port first */
        /**** Added check for Lower Interface index is not 0 ****/
        CfaGetIfType (LowInterface.u4IfIndex, &u1CurType);
        if ((LowInterface.u4IfIndex != CFA_NONE)
            && (u1CurType != CFA_ENET) /*PPPOE*/
            && CfaGddDeRegisterPort (LowInterface.u4IfIndex) != CFA_SUCCESS)
        {

            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmCreatePppInterface - GDD De-Registration fail interface %d\n",
                      u4IfIndex);
        }                        /* end of unsuccessful de-registration */
        return (CFA_FAILURE);
    }                            /* end of unsuccessful creation of PPP if */

    /**** Added check for Lower Interface index is not 0 ****/

    CfaGetIfType (LowInterface.u4IfIndex, &u1CurType);
    if ((LowInterface.u4IfIndex != CFA_NONE))
    {
        CFA_INIT_ASYNC_PARAMS (LowInterface.u4IfIndex);
    }

    /* By default the interface which is created would be down and has
       to be made up later explicitly. */

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmUpdatePppInterface
 *
 *    Description        : This function is for updation of PPP
 *                interface and updation of registration
 *                information in the PPP IWF struct whenever
 *                there is any change in the configuration of
 *                a PPP interface.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                tIfLayerInfo *pLowInterface,
 *                tIfLayerInfo *pHighInterface,
 *                tPppIpInfo *pIpInfo,
 *                UINT4 u4IdleTimer.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if update succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmUpdatePppInterface (UINT4 u4IfIndex, tIfLayerInfo * pLowInterface,
                          tIfLayerInfo * pHighInterface, tPppIpInfo * pIpInfo,
                          UINT4 u4IdleTimer, UINT4 u4Mtu)
#else /* __STDC__ */
INT4
CfaIfmUpdatePppInterface (u4IfIndex, pLowInterface, pHighInterface,
                          pIpInfo, u4IdleTimer, u4Mtu)
     UINT4               u4IfIndex;
     tIfLayerInfo       *pLowInterface;
     tIfLayerInfo       *pHighInterface;
     tPppIpInfo         *pIpInfo;
     UINT4               u4IdleTimer;
     UINT4               u4Mtu;
#endif /* __STDC__ */
{
    UINT1               u1CurType;
    UINT1               u1LowIfType;
    tIpConfigInfo       IpConfigInfo;
    tStackInfoStruct   *pNode = NULL;

    MEMSET (&IpConfigInfo, 0, sizeof (tIpConfigInfo));

    /* Find the HL to be given to PPP */
    if (pHighInterface->u1Command == CFA_LAYER_IGNORE)
    {
        /* we fill the struct with current values */
        pHighInterface->u4IfIndex =
            CFA_IF_STACK_IFINDEX (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex));
        if (pHighInterface->u4IfIndex == CFA_NONE)
            pHighInterface->u1IfType = CFA_NONE;
        else
            CfaGetIfType (pHighInterface->u4IfIndex,
                          &(pHighInterface->u1IfType));
    }

    /* Find the LL to be given to PPP */
    if (pLowInterface->u1Command == CFA_LAYER_IGNORE)
    {
        /* we fill the struct with current values */
        pLowInterface->u4IfIndex = CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY
                                                         (u4IfIndex));
        if (pLowInterface->u4IfIndex == CFA_NONE)
            pLowInterface->u1IfType = CFA_NONE;
        else
        {
            CfaGetIfType (pLowInterface->u4IfIndex, &u1CurType);
            pLowInterface->u1IfType = u1CurType;
            /* Added for PPP Over AAL5 */
            pLowInterface->u1EncapsType =
                CFA_IF_ENCAP (pLowInterface->u4IfIndex);
        }
    }

    /* de-register with the old lower interface first if the lower interface has
       changed and then register with the new interface. */

    /* the command in LowInterface struct should be CFA_LAYER_ADD or CFA_LAYER_DEL
       only if the lower interface is to be updated - else it should be
       CFA_LAYER_IGNORE. */
    if (pLowInterface->u1Command != CFA_LAYER_IGNORE)
    {

        /* doing de-registration - from existing lower layer. We need a switch
           statement here if the lower ifType can be FR VC, etc. - currently we
           support only HDLC port. we dont need to De-register if the StackStatus is
           not in service - i.e. registration was not done in the first place. */
        if (CFA_IF_STACK_STATUS (CFA_IF_STACK_LOW_ENTRY (u4IfIndex))
            == CFA_RS_ACTIVE)
        {
            CfaGetIfType (CFA_IF_STACK_IFINDEX
                          (CFA_IF_STACK_LOW_ENTRY (u4IfIndex)), &u1LowIfType);

            /* If the LL type was ENET or PVC we would not have registered
             * it with PPP as HL since they run PPpoE */
            if ((u1LowIfType != CFA_ENET))
            {
                if (CfaGddDeRegisterPort
                    (CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY (u4IfIndex)))
                    != CFA_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                              "Error in CfaIfmUpdatePppInterface - GDD De-Registration fail interface %d\n",
                              u4IfIndex);
                    return (CFA_FAILURE);
                }
            }
        }                        /* end of need to delete LL */

        if (pLowInterface->u1Command == CFA_LAYER_ADD)
        {
            /* a new lower layer was added - doing registration */
            if (CfaGddRegisterPort (pLowInterface->u4IfIndex, CFA_PPP)
                != CFA_SUCCESS)
            {
                /* Error in registering */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                          "Error in CfaIfmUpdatePppInterface - GDD Registration fail for interface %d\n",
                          u4IfIndex);
                return CFA_FAILURE;
            }
        }                        /* end of successful registration with new lower layer */
        else
        {
            /* we have already deleted the entry - if command was LAYER_DEL. We just make
               the PPP RS notready */
            CFA_IF_RS (u4IfIndex) = CFA_RS_NOTREADY;
        }
        CfaGetIfType (pLowInterface->u4IfIndex, &u1CurType);

    }                            /* end of change in lower if */

    /* if higher layer info is provided then enter it in stack table. If the
       higher interface is IP we can register/de-register the interface with IP,
       else we have to add/delete this link under a MP bundle. */
    if (pHighInterface->u1Command != CFA_LAYER_IGNORE)
    {
        /* first we need to de-register from the higher layer */

        /* check if higher layer is IP */
        if (CFA_IF_STACK_IFINDEX (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
            == CFA_NONE)
        {
            pNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
            if (pNode == NULL)
            {
                return (CFA_FAILURE);
            }

            /* now check if it needs to be de-registered */
            if ((pHighInterface->u1Command == CFA_LAYER_DEL) &&
                CFA_IF_ENTRY_REG_WITH_IP (u4IfIndex, pNode))
            {
                /* we have to de-register it now */
                if (CfaIfmConfigNetworkInterface (CFA_NET_IF_DEL, u4IfIndex,
                                                  0, NULL) != CFA_SUCCESS)
                {
                    /* unsuccessful in de-registration IP */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                              "Error in CfaIfmUpdatePppInterface - IP De-Register fail for interface %d\n",
                              u4IfIndex);
                    return CFA_FAILURE;
                }
                /* Inform ISIS of this interface deletion */
#ifdef ISIS_WANTED
                IsisDlliIfStatusIndication (u4IfIndex, CFA_IF_NP);
#endif /* ISIS_WANTED */
            }                    /* end of need for de-registration */
        }                        /* end of higher layer is IP */

        else
        {
            /* we assume that if higher layer is not IP then its MP. */
            /* now check if it needs to be de-registered from the bundle */
            if (((pHighInterface->u1Command == CFA_LAYER_DEL)) &&
                CFA_IF_STACK_STATUS (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
                == CFA_RS_ACTIVE)
            {

                /* check if the MP interface is registered with PPP module first */
                if (CFA_IF_RS
                    (CFA_IF_STACK_IFINDEX (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex)))
                    == CFA_RS_ACTIVE)
                {
                    /* we have to de-register it now */
                    if (PppComposeMpBundle
                        (CFA_IF_STACK_IFINDEX
                         (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex)), u4IfIndex,
                         CFA_LAYER_DEL) != PPP_SUCCESS)
                    {
                        /* error in deleting from MP bundle */
                        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                                  "Error in CfaIfmUpdatePppInterface - Delete from MP fail for interface %d\n",
                                  u4IfIndex);
                        return CFA_FAILURE;
                    }
                }                /* end of MP interface active check */
            }
        }                        /* end of deletion/de-registration from HL */

        /* we now have to add the new higher layer - i.e. do the registration. if
           the command was to add a layer. We do the updation only if the previous
           high layer was updated successfully. */
        if (pHighInterface->u1Command == CFA_LAYER_ADD)
        {

            if ((pHighInterface->u1IfType == CFA_MP) &&
                (CFA_IF_RS (pHighInterface->u4IfIndex) == CFA_RS_ACTIVE))
            {
                /* add this link to a MP bundle */
                if (PppComposeMpBundle (pHighInterface->u4IfIndex, u4IfIndex,
                                        CFA_LAYER_ADD) != PPP_SUCCESS)
                {
                    /* error in adding to a MP bundle */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                              "Error in CfaIfmUpdatePppInterface - Add to MP fail for interface %d\n",
                              u4IfIndex);
                    return CFA_FAILURE;
                }
            }                    /* higher interface was MP */
            /* we assume that if the higher interface is not MP then its IP */
            /* we dont need to register with IP - the reg/update will be done after
               the PPP link comes up */

        }                        /* end of addition of a higher if */

    }                            /* end of change in higher if */

    /* we update the ifTable and PPP IWF struct only after the updation is
       successful in PPP */
    if (PppUpdatePppInterface (u4IfIndex, pHighInterface, pLowInterface,
                               pIpInfo->u4LocalIpAddr, pIpInfo->u4PeerIpAddr,
                               u4Mtu) != PPP_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmUpdatePppInterface - PPP update fail for interface %d\n",
                  u4IfIndex);

        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmUpdatePppInterface - PPP update Register success - interface %d\n",
              u4IfIndex);

    /* is the idle timer enabled - if yes then enter the value in PPP IWF struct */
    CFA_IF_IDLE_TIMEOUT (u4IfIndex) = u4IdleTimer;
    CfaSetIfMtu (u4IfIndex, u4Mtu);

    CfaIpIfGetIfInfo (u4IfIndex, &IpConfigInfo);
    /* copy the rest of the information into the PPP - IWF struct */
    IpConfigInfo.u4Addr = pIpInfo->u4LocalIpAddr;
    CFA_IF_PEER_IPADDR (u4IfIndex) = pIpInfo->u4PeerIpAddr;
    IpConfigInfo.u4NetMask = (UINT4) pIpInfo->u1SubnetMask;

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmDeletePppInterface
 *
 *    Description        : This function is for for de-registration of
 *                interface with PPP/MP. Used for both PPP and
 *                MP interfaces.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmDeletePppInterface (UINT4 u4IfIndex, UINT1 u1DelIfEntry)
#else /* __STDC__ */
INT4
CfaIfmDeletePppInterface (u4IfIndex, u1DelIfEntry)
     UINT4               u4IfIndex;
     UINT1               u1DelIfEntry;
#endif /* __STDC__ */
{
    UINT4               u4LowIfIndex;
    UINT4               u4Flag;
    UINT1               u1CurType;
    UINT1               u1LowIfType;
    UINT1               u1BridgedStatus;
    tIpConfigInfo       IpConfigInfo;
    tStackInfoStruct   *pNode = NULL;

    MEMSET (&IpConfigInfo, 0, sizeof (tIpConfigInfo));

    CfaGetIfType (u4IfIndex, &u1CurType);
    /* we have to make the interface admin status down to enable ppp to resume */
    /* when we make ppp interface status to notinsrevice to active */

    CFA_IF_ADMIN (u4IfIndex) = CFA_IF_DOWN;

    /* first we de-register the interface from IP or higher layer if it was
       registered earlier */
    if (CFA_IF_STACK_STATUS (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex))
        == CFA_RS_ACTIVE)
    {
        pNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
        if (pNode == NULL)
        {
            return (CFA_FAILURE);
        }

        if (CFA_IF_ENTRY_REG_WITH_IP (u4IfIndex, pNode))
        {
            CfaIpIfGetIfInfo (u4IfIndex, &IpConfigInfo);
            u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK;
            /* this interface is registered with IP */
            if (CfaIfmConfigNetworkInterface (CFA_NET_IF_DEL, u4IfIndex,
                                              u4Flag,
                                              &IpConfigInfo) != CFA_SUCCESS)
            {
                /* unsuccessful in de-registering from IP - we cant delete the interface.
                   we dont expect this to fail - just for handling unforeseen exceptions. */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                          "Error in CfaIfmDeletePppInterface - IP De-reg fail for interface %d\n",
                          u4IfIndex);
                return (CFA_FAILURE);
            }

        }                        /* end of interface was registered with IP */
        else
        {
            /* if not registered with IP - then this must be a PPP link which is registered
               under a MP bundle - we dont support anything else currently. */
            if (PppComposeMpBundle
                (CFA_IF_STACK_IFINDEX (CFA_IF_STACK_HIGH_ENTRY (u4IfIndex)),
                 u4IfIndex, CFA_LAYER_DEL) != PPP_SUCCESS)
            {
                /*  error in deleting from MP bundle */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                          "Error in CfaIfmDeletePppInterface - Delete from MP fail for interface %d\n",
                          u4IfIndex);
            }
            /* This routines returns success only if the interface doesn't exist *
             * at PPP level, we should not return failure here */
        }                        /* end of interface was registered with MP */

        /* the higher interface was invalidated - so we just delete the stack entry */
        CfaIfmDeleteStackEntry (u4IfIndex, CFA_HIGH,
                                CFA_IF_STACK_IFINDEX (CFA_IF_STACK_HIGH_ENTRY
                                                      (u4IfIndex)));

    }                            /* end of interface was registered to some higher module */

    /* Inform ISIS of this interface deletion */
#ifdef ISIS_WANTED
    IsisDlliIfStatusIndication (u4IfIndex, CFA_IF_NP);
#endif /* ISIS_WANTED */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeletePppInterface - Un-reg with HL success - interface %d\n",
              u4IfIndex);

    /* now we de-register all the lower interfaces and delete the stack entries
       for lower layers */

    /* see if it is a MP interface - if so, delete all the PPP links from the
       bundle */
    if (u1CurType == CFA_MP)
    {
        /* we delete all the PPP links from this bundle and update the stack table */
        while ((u4LowIfIndex =
                CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY (u4IfIndex)))
               != CFA_NONE)
        {
            if (PppComposeMpBundle (u4IfIndex, u4LowIfIndex,
                                    CFA_LAYER_DEL) != PPP_SUCCESS)
            {
                /* error in deleting from MP bundle */
                CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                          "Error in CfaIfmDeletePppInterface - delete of PPP links fail for interface %d\n",
                          u4IfIndex);
            }
            /* we now delete this entry from the stack table */
            CfaIfmDeleteStackEntry (u4IfIndex, CFA_LOW, u4LowIfIndex);

            /* Register the underlying PPP interfaces with IP again */
            CfaGetIfBridgedIfaceStatus (u4LowIfIndex, &u1BridgedStatus);

            CfaIpIfGetIfInfo (u4IfIndex, &IpConfigInfo);
            u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK;

            if (u1BridgedStatus == CFA_DISABLED)
            {
                if (CfaIfmConfigNetworkInterface
                    (CFA_NET_IF_NEW, u4LowIfIndex,
                     u4Flag, &IpConfigInfo) != CFA_SUCCESS)
                {
                    /* unsuccessful in registering with IP 
                       we dont expect this to fail - just for handling unforeseen exceptions. */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                              "Error in CfaIfmDeletePppInterface - IP reg fail for interface %d\n",
                              u4LowIfIndex);
                }
            }
        }                        /* end of while loop */
    }                            /* end of deletion of all PPP links under the MP bundle */
    else
    {
        /* This is a PPP interface. De-register from the lower phys port - thats the
           only lower interface which we support currently. */

        if (CFA_IF_STACK_STATUS (CFA_IF_STACK_LOW_ENTRY (u4IfIndex))
            == CFA_RS_ACTIVE)
        {
            CfaGetIfType (CFA_IF_STACK_IFINDEX
                          (CFA_IF_STACK_LOW_ENTRY (u4IfIndex)), &u1LowIfType);

            if ((u1LowIfType != CFA_ENET))
            {
                if (CfaGddDeRegisterPort
                    (CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY (u4IfIndex)))
                    != CFA_SUCCESS)
                {

                    CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                              "Error in CfaIfmUpdatePppInterface - GDD De-Registration fail interface %d\n",
                              u4IfIndex);
                    return (CFA_FAILURE);
                }
            }
        }                        /* end of PPP interface which was registered with HDLC */

        u4LowIfIndex =
            CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY (u4IfIndex));

        /* Delete the lower stack layer */
        CfaIfmDeleteStackEntry (u4IfIndex, CFA_LOW,
                                CFA_IF_STACK_IFINDEX (CFA_IF_STACK_LOW_ENTRY
                                                      (u4IfIndex)));

        /* If the PPP iterface was stacked over an ethernet router port
         * register the ethernet port with IP now */
        if (u4LowIfIndex != CFA_NONE)
        {
            CfaGetIfType (u4LowIfIndex, &u1LowIfType);
            CfaGetIfBridgedIfaceStatus (u4LowIfIndex, &u1BridgedStatus);

            CfaIpIfGetIfInfo (u4IfIndex, &IpConfigInfo);
            u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK;

            if ((u1LowIfType == CFA_ENET) && (u1BridgedStatus == CFA_DISABLED))
            {
                if (CfaIfmConfigNetworkInterface
                    (CFA_NET_IF_NEW, u4LowIfIndex,
                     u4Flag, &IpConfigInfo) != CFA_SUCCESS)
                {
                    /* unsuccessful in registering with IP 
                       we dont expect this to fail - just for handling unforeseen exceptions. */
                    CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                              "Error in CfaIfmDeletePppInterface - IP reg fail for interface %d\n",
                              u4LowIfIndex);
                }
                /*Change the admin status of the lower layer from DOWN-UP, so that 
                 * it will start the DHCP IP address Aquisition*/
                nmhSetIfMainAdminStatus (u4LowIfIndex, CFA_IF_DOWN);
                nmhSetIfMainAdminStatus (u4LowIfIndex, CFA_IF_UP);
            }
        }
    }                            /* end of de-registration of PPP interface with LL */

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeletePppInterface - Un-reg with LL success - interface %d\n",
              u4IfIndex);

    /* de-register the new interface from PPP it the row status is in active state */
    if (CFA_IF_RS (u4IfIndex) == CFA_RS_ACTIVE)
    {
        if (PppDeRegisterInterface (u4IfIndex) != PPP_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmUpdatePppInterface - PPP De-Registration fail interface %d\n",
                      u4IfIndex);
            return (CFA_FAILURE);
        }
    }
    /* check whether the entry is to be deleted from IfTable itself */
    if (u1DelIfEntry == CFA_FALSE)
    {
        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmDeletePppInterface - disabling success - interface %d\n",
                  u4IfIndex);

        return (CFA_SUCCESS);
    }

    /* delete the interface from the IfTable */
    if (CfaIfmDeleteIfEntry (u4IfIndex, u1CurType) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmUpdatePppInterface - ifEntry delete fail for interface %d\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmDeletePppInterface - Full Delete success - interface %d\n",
              u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmHandlePppConfigUpdate
 *
 *    Description        : This function processes interface management
 *                notifications from PPP/MP. This function is
 *                used both from PPP and MP interface. The MTU
 *                indicated by PPP should be the max IP packet
 *                size. This function should be called whenever
 *                there is a change in the configuration.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT4 u4LocalIpAddr,
 *                UINT4 u4PeerIpAddr,
 *                UINT4 u4Mtu,
 *                UINT4 u4IdleTimeout,
 *                UINT1 u1PppType
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if update succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmHandlePppConfigUpdate (UINT4 u4IfIndex, UINT4 u4LocalIpAddr,
                             UINT4 u4PeerIpAddr, UINT4 u4Mtu,
                             UINT4 u4IdleTimeout)
#else /* __STDC__ */
INT4
CfaIfmHandlePppConfigUpdate (u4IfIndex, u4LocalIpAddr, u4PeerIpAddr, u4Mtu,
                             u4IdleTimeout)
     UINT4               u4IfIndex;
     UINT4               u4LocalIpAddr;
     UINT4               u4PeerIpAddr;
     UINT4               u4Mtu;
     UINT4               u4IdleTimeout;
#endif /* __STDC__ */
{
    UINT4               u4CurMtu;
    UINT4               u4Flag;
    UINT1               u1TempSubnetMask = 32;
    UINT1               u1CurType;
    tIpConfigInfo       IpConfigInfo;
    tStackInfoStruct   *pNode = NULL;

    MEMSET (&IpConfigInfo, 0, sizeof (tIpConfigInfo));

    /* first check if the index is valid and if interface is PPP or MP */
    if ((u4IfIndex > CFA_MAX_INTERFACES ()) || (u4IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmHandlePppConfigUpdate - invalid interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    CfaGetIfType (u4IfIndex, &u1CurType);
    if ((u1CurType != CFA_PPP) && (u1CurType != CFA_MP))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmHandlePppConfigUpdate - invalid type - interface %d.\n",
                  u4IfIndex);
        return (CFA_FAILURE);
    }

    /* we now update the MTU given by PPP/MP for the interface - this is done
       regardless of if this function returns success or failure */
    CfaGetIfMtu (u4IfIndex, &u4CurMtu);
    CFA_IF_CONFIG_MTU (u4IfIndex) = u4CurMtu;
    CfaSetIfMtu (u4IfIndex, u4Mtu);

    CfaIpIfGetIfInfo (u4IfIndex, &IpConfigInfo);
    IpConfigInfo.u4NetMask = (UINT4) u1TempSubnetMask;

    /* we update the local IP address in the PPP IWF struct. If the IP address
       has changed then we notify IP of this change and also assign a default
       subnet mask */
    if (u4LocalIpAddr != IpConfigInfo.u4Addr)
    {
        /* we find the default subnet mask according to the class of the IP
           address */
        if (CFA_IS_ADDR_CLASS_A (u4LocalIpAddr))
            u1TempSubnetMask = 8;    /* 255.0.0.0 */
        else if (CFA_IS_ADDR_CLASS_B (u4LocalIpAddr))
            u1TempSubnetMask = 16;    /* 255.255.0.0 */
        else if (CFA_IS_ADDR_CLASS_C (u4LocalIpAddr))
            u1TempSubnetMask = 24;    /* 255.255.255.0 */
        else
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmHandlePppConfigUpdate - invalid IP Addr - interface %d.\n",
                      u4IfIndex);
            return (CFA_FAILURE);    /* invalid IP addr - this is not expected
                                       to happen */
        }
    }
    pNode = (tStackInfoStruct *) CFA_IF_STACK_HIGH_ENTRY (u4IfIndex);
    if (pNode == NULL)
    {
        return (CFA_FAILURE);
    }

    /* check if this interface is registered with IP */
    if (CFA_IF_ENTRY_REG_WITH_IP (u4IfIndex, pNode))
    {
        /* DSL_ADD -S- 
         * IP address of Peer PPP session should be known to the ppp client
         */
        CFA_IF_NEGO_PEER_IPADDR (u4IfIndex) = u4PeerIpAddr;
        /* DSL_ADD -E- */
        /* we update the IP address  info */
        CfaIpIfGetIfInfo (u4IfIndex, &IpConfigInfo);
        IpConfigInfo.u4Addr = u4LocalIpAddr;
        IpConfigInfo.u4NetMask = (UINT4) u1TempSubnetMask;
        u4Flag = CFA_IP_IF_PRIMARY_ADDR | CFA_IP_IF_NETMASK;
        if (CfaIfmConfigNetworkInterface (CFA_NET_IF_UPD, u4IfIndex,
                                          u4Flag, &IpConfigInfo) != CFA_SUCCESS)
        {
            /* unsuccessful in updating in IP - we dont expect this to fail - just for
               handling unforeseen exceptions. */
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmHandlePppConfigUpdate - IP config upd fail - interface %d.\n",
                      u4IfIndex);
            return (CFA_FAILURE);
        }
        CFA_DBG2 (CFA_TRC_ALL, CFA_IFM,
                  "In CfaIfmHandlePppConfigUpdate - IP updating success interface %d, address %d\n",
                  u4IfIndex, u4LocalIpAddr);

    }                            /* end of IP the interface was already registered to IP */
    else
    {
        /* interface not already registered with IP - so we store the negotiated
           values and reg when interface is up. */
        CfaIpIfGetIfInfo (u4IfIndex, &IpConfigInfo);
        IpConfigInfo.u4Addr = u4LocalIpAddr;
        IpConfigInfo.u4NetMask = (UINT4) u1TempSubnetMask;
        CFA_IF_NEGO_PEER_IPADDR (u4IfIndex) = u4PeerIpAddr;
    }

    /* we update the idle timeout in the PPP IWF struct */
    CFA_IF_IDLE_TIMEOUT (u4IfIndex) = u4IdleTimeout;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmHandlePppConfigUpdate - Full Update success - interface %d\n",
              u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmHandleMpBundleUpdate
 *
 *    Description        : This function processes processes
 *                notifications from PPP/MP about the state of
 *                the interface - DATA_OPEN or DATA_CLOSE.
 *
 *    Input(s)            : UINT2 u2IfIndex,
 *                UINT1 u1PppLinkState
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmHandleMpBundleUpdate (UINT2 u2MpIfIndex, UINT2 u2PppIfIndex,
                            UINT1 u1Command)
#else /* __STDC__ */
INT4
CfaIfmHandleMpBundleUpdate (u2MpIfIndex, u2PppIfIndex, u1Command)
     UINT2               u2MpIfIndex;
     UINT2               u2PppIfIndex;
     UINT1               u1Command;
#endif /* __STDC__ */
{
    UINT1               u1CurType;
    UINT1               u1MpCurType;

    CFA_DBG3 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmHandleMpBundleUpdate for MP interface %d PPP interface %d - command %d\n",
              u2MpIfIndex, u2PppIfIndex, u1Command);

    /* first check if the index is valid and if interface is PPP or MP */
    if ((u2MpIfIndex > CFA_MAX_INTERFACES ()) || (u2MpIfIndex == 0) ||
        (u2PppIfIndex > CFA_MAX_INTERFACES ()) || (u2PppIfIndex == 0))
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                 "Error in CfaIfmHandleMpBundleUpdate - invalid interface index.\n");
        return CFA_FAILURE;
    }

    /* first check if interface is PPP or MP */
    CfaGetIfType (u2PppIfIndex, &u1CurType);
    CfaGetIfType (u2MpIfIndex, &u1MpCurType);
    if ((u1CurType != CFA_PPP) && (u1MpCurType != CFA_MP))
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                 "Error in CfaIfmHandleMpBundleUpdate - invalid type.\n");
        return CFA_FAILURE;
    }

    if (u1Command == CFA_LAYER_DEL)
    {
        /* the PPP link under this bundle is to be deleted - we dont need to
           indicate this info to the PPP module */

        /* the higher interface was invalidated for PPP - so we just delete the stack entry */
        CfaIfmDeleteStackEntry (u2PppIfIndex, CFA_HIGH, u2MpIfIndex);

        CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                  "In CfaIfmUpdateMpInterface - PPP link delete & LowStack updating success - interface %d\n",
                  u2MpIfIndex);
    }                            /* end of deletion of ppp link  */

    if (u1Command == CFA_LAYER_ADD)
    {
        /* a new PPP link is to be added - we dont need to
           indicate this info to the PPP module */
        if (CfaIfmAddStackEntry
            (u2PppIfIndex, CFA_HIGH, u2MpIfIndex, CFA_RS_ACTIVE) == CFA_SUCCESS)
        {

            CFA_PPP_IWF_BYPASS (u2PppIfIndex) |= (NON_PLAIN_RX | NON_PLAIN_TX | ACFC_PLAIN_RX);    /* resetting */
            CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
                      "In CfaIfmUpdateMpInterface - PPP link add success for interface %d\n",
                      u2MpIfIndex);
        }                        /* successfully added link to MP bundle */
        else
        {
            /* error in adding to MP bundle */
            CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                      "Error in CfaIfmUpdateMpInterface - PPP link add fail for interface %d\n",
                      u2MpIfIndex);
            return CFA_FAILURE;
        }

    }                            /* end of addition of lower ppp link */

    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmHandleBcpConfigUpdate
 *
 *    Description        : This function processes 
 *                notifications from PPP/BCP about the state of
 *                the interface - DATA_OPEN or DATA_CLOSE.
 *
 *    Input(s)            : UINT2 u2IfIndex,
 *                          UINT1 LocalMediaAddr,
 *                          UINT1 u1PeerMediaAddr
 *                          UINT1 u1Stp,
 *                          UINT1 u1TinyGramFlag,
 *                          UINT1 u1MgmtInlineSupport,
 *                          UINT1 u1IEEE802TagFrameSupport,
 *                          UINT1 u1BridgeLineId,
 *                          UINT4 u4Mtu,
 *                          UINT4 u4IdleTimeOut
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/

#ifdef BRIDGE_WANTED
#ifdef __STDC__
INT4
CfaIfmHandleBcpConfigUpdate (tBcpConfigStruct PppBcpConfigUpdate)
#endif                            /* __STDC__ */
{
    UINT4               u4CurMtu;
    UINT1               u1CurType;

    /* first check if the index is valid and if interface is PPP or MP */
    if (((UINT2) PppBcpConfigUpdate.u4Index > CFA_MAX_INTERFACES ())
        || ((UINT2) PppBcpConfigUpdate.u4Index == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmHandleBcpConfigUpdate - invalid interface %d.\n",
                  (UINT2) PppBcpConfigUpdate.u4Index);
        return (CFA_FAILURE);
    }

    CfaGetIfType ((UINT2) PppBcpConfigUpdate.u4Index, &u1CurType);
    if ((u1CurType != CFA_PPP) && (u1CurType != CFA_MP))
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmHandleBcpConfigUpdate - invalid type - interface %d.\n",
                  (UINT2) PppBcpConfigUpdate.u4Index);
        return (CFA_FAILURE);
    }

    /* we now update the MTU given by PPP/MP for the interface - this is done
       regardless of if this function returns success or failure */

    CfaGetIfMtu ((UINT2) PppBcpConfigUpdate.u4Index, &u4CurMtu);
    CFA_IF_CONFIG_MTU ((UINT2) PppBcpConfigUpdate.u4Index) = u4CurMtu;
    CfaSetIfMtu ((UINT2) PppBcpConfigUpdate.u4Index, PppBcpConfigUpdate.u4Mtu);
#ifdef TINYGRAM_COMPRESSION
    CFA_IF_TINYGRAM_COMPRESS_FLAG ((UINT2) PppBcpConfigUpdate.u4Index) =
        PppBcpConfigUpdate.u1TinygramFlag;
#endif /* TINYGRAM_COMPRESSION */

    L2IwfPppUpdateInfo (PppBcpConfigUpdate.u4Index,
                        PppBcpConfigUpdate.u1MgmtInlineSupport,
                        PppBcpConfigUpdate.u1IEEE802TagFrameSupport);

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmHandleBcpConfigUpdate - Full Update success - interface %x\n",
              PppBcpConfigUpdate.u1LocalMediaAddr);

    MEMCPY (CFA_IF_PEER_MEDIA ((UINT2) PppBcpConfigUpdate.u4Index),
            PppBcpConfigUpdate.u1PeerMediaAddr, CFA_ENET_ADDR_LEN);

    /* we update the idle timeout in the PPP IWF struct */
    CFA_IF_IDLE_TIMEOUT ((UINT2) PppBcpConfigUpdate.u4Index) =
        PppBcpConfigUpdate.u4IdleTimeout;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmHandleBcpConfigUpdate - Full Update success - interface %d\n",
              (UINT2) PppBcpConfigUpdate.u4Index);

    return (CFA_SUCCESS);

}
#endif
#ifdef PPP_BYPASS_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIfmHandlePppBypassStateUpdate
 *
 *    Description        : This function processes processes
 *                notifications from PPP/MP about the state of
 *                the interface - DATA_OPEN or DATA_CLOSE.
 *
 *    Input(s)            : UINT2 u2IfIndex,
 *                UINT1 u1PppLinkState
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
#ifdef __STDC__
VOID
CfaIfmHandlePppBypassStateUpdate (UINT2 u2IfIndex, UINT1 u1PppLinkBypassState)
#else /* __STDC__ */
VOID
CfaIfmHandlePppBypassStateUpdate (u2IfIndex, u1PppLinkBypassState)
     UINT2               u2IfIndex;
     UINT1               u1PppLinkBypassState;
#endif /* __STDC__ */
{
    UINT1               u1CurType;

    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmHandlePppBypassStateUpdate for PPP/MP interface %d state %x\n",
              u2IfIndex, u1PppLinkBypassState);

    /* first check if the index is valid and if interface is PPP or MP */
    if ((u2IfIndex > CFA_MAX_INTERFACES ()) || (u2IfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmHandlePppBypassStateUpdate - invalid interface %d.\n",
                  u2IfIndex);
        return;
    }

    /* first check if interface is PPP or MP */
    CfaGetIfType (u2IfIndex, &u1CurType);
    if ((u1CurType != CFA_PPP) && (u1CurType != CFA_MP))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmHandlePppBypassStateUpdate - invalid type for interface %d.\n",
                  u2IfIndex);
        return;
    }

    /* we now update the link state info */
    CFA_PPP_IWF_BYPASS (u2IfIndex) = u1PppLinkBypassState;

}
#endif /* PPP_BYPASS_WANTED */

/*****************************************************************************
 *
 *    Function Name        : CfaIfmHandleAsyncParamsUpdate
 *
 *    Description        : This function processes processes
 *                notifications from PPP/MP about the state of
 *                the interface - DATA_OPEN or DATA_CLOSE.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1PppLinkState
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmHandleAsyncParamsUpdate (UINT4 u4IfIndex, VOID *pAsyncParams)
#else /* __STDC__ */
INT4
CfaIfmHandleAsyncParamsUpdate (u4IfIndex, pAsyncParams)
     UINT4               u4IfIndex;
     VOID               *pAsyncParams;
#endif /* __STDC__ */
{
    tLlAsyncParams     *TempAsyncParams = (tLlAsyncParams *) pAsyncParams;
    UINT1               u1CurType;

    CFA_DBG1 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmHandleAsyncParamsUpdate for Phys interface %d\n",
              u4IfIndex);

    /* first check if the index is valid and if interface is PPP or MP */
    if (CfaValidateCfaIfIndex (u4IfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmHandleAsyncParamsUpdate - invalid interface %d.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    /* first check if interface is PPP and the params are valid */
    CfaGetIfType (u4IfIndex, &u1CurType);
    if ((u1CurType != CFA_ASYNC) || (TempAsyncParams == NULL))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmHandleAsyncParamsUpdate - invalid type for interface %d.\n",
                  u4IfIndex);
        return CFA_FAILURE;
    }

    /* assign the FCS */
    CFA_GDD_ASYNC_PARAMS (u4IfIndex).u1TxFcsSize = TempAsyncParams->u1TxFcsSize;
    CFA_GDD_ASYNC_PARAMS (u4IfIndex).u1RxFcsSize = TempAsyncParams->u1RxFcsSize;

    /* if the ACCM given is not NULL then we have to allocate and take the new
       ACCM */
    if (TempAsyncParams->pTxACCMap != NULL)
    {

        if (CFA_GDD_ASYNC_PARAMS (u4IfIndex).pTxACCMap != gCfaDefaultTxACCM)
        {
            MEM_FREE (CFA_GDD_ASYNC_PARAMS (u4IfIndex).pTxACCMap);
        }

        if ((CFA_GDD_ASYNC_PARAMS (u4IfIndex).pTxACCMap =
             MEM_MALLOC (CFA_PPP_TX_ACCM_SIZE, UINT1)) == NULL)
        {

            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Exiting CfaIfmHandleAsyncParamsUpdate - Tx ACCM malloc fail for interface %d\n",
                      u4IfIndex);

            /* we set to default ACCM */
            CFA_GDD_ASYNC_PARAMS (u4IfIndex).pTxACCMap = gCfaDefaultTxACCM;
            return (CFA_FAILURE);
        }
        else
        {
            /* copy the ACCM */
            MEMCPY (CFA_GDD_ASYNC_PARAMS (u4IfIndex).pTxACCMap,
                    TempAsyncParams->pTxACCMap, CFA_PPP_TX_ACCM_SIZE);
        }
    }                            /* end of Tx ACCM */

    if (TempAsyncParams->pRxACCMap != NULL)
    {

        if (CFA_GDD_ASYNC_PARAMS (u4IfIndex).pRxACCMap != gCfaDefaultRxACCM)
        {
            MEM_FREE (CFA_GDD_ASYNC_PARAMS (u4IfIndex).pRxACCMap);
        }

        if ((CFA_GDD_ASYNC_PARAMS (u4IfIndex).pRxACCMap =
             MEM_MALLOC (CFA_PPP_RX_ACCM_SIZE, UINT1)) == NULL)
        {

            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Exiting CfaIfmHandleAsyncParamsUpdate - Rx ACCM malloc fail for interface %d\n",
                      u4IfIndex);

            /* we set to default ACCM */
            CFA_GDD_ASYNC_PARAMS (u4IfIndex).pRxACCMap = gCfaDefaultRxACCM;
            return (CFA_FAILURE);
        }
        else
        {
            /* copy the ACCM */
            MEMCPY (CFA_GDD_ASYNC_PARAMS (u4IfIndex).pRxACCMap,
                    TempAsyncParams->pRxACCMap, CFA_PPP_RX_ACCM_SIZE);
        }
    }                            /* end of Tx ACCM */

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmPppCreatePppLinkForMpBundle
 *
 *    Description        : This function processes processes
 *                notifications from PPP/MP about the state of
 *                the interface - DATA_OPEN or DATA_CLOSE.
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *                UINT1 u1PppLinkState
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmPppCreatePppLinkForMpBundle (UINT4 u4MpIfIndex, UINT4 u4PhysIfIndex)
#else /* __STDC__ */
INT4
CfaIfmPppCreatePppLinkForMpBundle (u4MpIfIndex, u4PhysIfIndex)
     UINT2               u4MpIfIndex;
     UINT2               u4PhysIfIndex;
#endif /* __STDC__ */
{
    UINT2               u2IfIndex = CFA_AVAIL_IFINDEX ();
    UINT1               u1CurType;

    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmPppCreatePppLinkForMpBundle for MP interface %d, Phys interface %d\n",
              u4MpIfIndex, u4PhysIfIndex);

    /* first check if the index is valid and if interface is MP */
    if ((u4MpIfIndex > CFA_MAX_INTERFACES ()) || (u4MpIfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmPppCreatePppLinkForMpBundle - invalid interface %d.\n",
                  u4MpIfIndex);
        return CFA_FAILURE;
    }
    if ((u4PhysIfIndex > CFA_MAX_INTERFACES ()) || (u4PhysIfIndex == 0))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmPppCreatePppLinkForMpBundle - invalid interface %d.\n",
                  u4PhysIfIndex);
        return CFA_FAILURE;
    }

    CfaGetIfType (u4MpIfIndex, &u1CurType);
    if (u1CurType != CFA_MP)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmPppCreatePppLinkForMpBundle - invalid type for interface %d.\n",
                  u4MpIfIndex);
        return CFA_FAILURE;
    }

    /* Create the entry in the interface table for the PPP interface */
    if (CfaIfmCreateAndInitIfEntry (u2IfIndex, CFA_PPP, NULL) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmPppCreatePppLinkForMpBundle - interface creation - FAILURE.\n");
        return (CFA_FAILURE);
    }

    /* create the entries in the stack table for the LL-PPP binding */
    if (CfaIfmAddStackEntry (u2IfIndex, CFA_LOW, u4PhysIfIndex,
                             CFA_RS_NOTINSERVICE) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmPppCreatePppLinkForMpBundle - lower stack layer - FAILURE.\n");

        /* delete the interface from the IfTable */
        CfaGetIfType (u2IfIndex, &u1CurType);
        if (CfaIfmDeleteIfEntry (u2IfIndex, u1CurType) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmPppCreatePppLinkForMpBundle - ifEntry delete fail for iface %d\n",
                      u2IfIndex);
        }

        return (CFA_FAILURE);
    }
    else
    {
        CFA_IF_RS (u2IfIndex) = CFA_RS_ACTIVE;
    }

    /* register the PPP interface with the PPP module */
    if (CfaIfmCreatePppInterface (u2IfIndex) != CFA_SUCCESS)
    {

        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmPppCreatePppLinkForMpBundle - Reg with PPP fail for iface %d\n",
                  u2IfIndex);

        CfaIfmDeleteStackEntry (u2IfIndex, CFA_LOW, u4PhysIfIndex);

        CfaGetIfType (u2IfIndex, &u1CurType);
        if (CfaIfmDeleteIfEntry (u2IfIndex, u1CurType) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmPppCreatePppLinkForMpBundle - ifEntry delete fail for iface %d\n",
                      u2IfIndex);
        }

        return CFA_FAILURE;
    }

    /* create the entries in the stack table for the MP-PPP binding */
    if (CfaIfmAddStackEntry (u4MpIfIndex, CFA_LOW, u2IfIndex,
                             CFA_RS_NOTINSERVICE) != CFA_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ERROR, CFA_IFM,
                 "Error in CfaIfmPppCreatePppLinkForMpBundle - Higher stack layer - FAILURE.\n");

        if (CfaIfmDeletePppInterface (u2IfIndex, CFA_TRUE) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmPppCreatePppLinkForMpBundle - PPP Iface delete fail for %d\n",
                      u2IfIndex);
        }

        CfaIfmDeleteStackEntry (u2IfIndex, CFA_LOW, u4PhysIfIndex);

        CfaGetIfType (u2IfIndex, &u1CurType);
        if (CfaIfmDeleteIfEntry (u2IfIndex, u1CurType) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmPppCreatePppLinkForMpBundle - ifEntry delete fail for iface %d\n",
                      u2IfIndex);
        }

        return (CFA_FAILURE);
    }

    /* add this PPP link to a MP bundle */
    if (PppComposeMpBundle (u4MpIfIndex, u2IfIndex,
                            CFA_LAYER_ADD) != PPP_SUCCESS)
    {

        /* error in adding to a MP bundle */
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmPppCreatePppLinkForMpBundle - Add to MP fail for iface %d\n",
                  u2IfIndex);

        CfaIfmDeleteStackEntry (u2IfIndex, CFA_HIGH, u4MpIfIndex);

        if (CfaIfmDeletePppInterface (u2IfIndex, CFA_TRUE) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmPppCreatePppLinkForMpBundle - PPP Iface delete fail for %d\n",
                      u2IfIndex);
        }

        CfaIfmDeleteStackEntry (u2IfIndex, CFA_LOW, u4PhysIfIndex);

        CfaGetIfType (u2IfIndex, &u1CurType);
        if (CfaIfmDeleteIfEntry (u2IfIndex, u1CurType) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmPppCreatePppLinkForMpBundle - ifEntry delete fail for iface %d\n",
                      u2IfIndex);
        }

        return CFA_FAILURE;
    }

    /* we now make the lower (physical) link and the PPP interface UP - we dont
     * delete the interface if the PPP link cant come UP */
    nmhSetIfAdminStatus (u4PhysIfIndex, CFA_IF_UP);
    nmhSetIfAdminStatus (u2IfIndex, CFA_IF_UP);

    CFA_DBG3 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "CfaIfmPppCreatePppLinkForMpBundle - SUCCESS - PPP iface %d - MP iface %d LL iface %d.\n",
              u2IfIndex, u4MpIfIndex, u4PhysIfIndex);
    return (CFA_SUCCESS);
}

/*****************************************************************************
 *    Function Name        : CfaCreateNewBundleIf
 *
 *    Description        : 
 *    Input(s)            :Link ifIndex and Old bundle ifIndex 
 *    Output(s)            : None.
 *
 *    Global Variables Referred : 
 *    Global Variables Modified :
 *    Returns            : None.
 *
 *****************************************************************************/

VOID
CfaCreateNewBundleIf (UINT4 LinkIndex, UINT4 OldBundleIndex)
{
    UINT4               NewBundleIndex;

    NewBundleIndex = CFA_AVAIL_IFINDEX ();

    nmhSetIfMainRowStatus (NewBundleIndex, CFA_RS_CREATEANDWAIT);
    nmhSetIfMainType (NewBundleIndex, CFA_MP);
    nmhSetIfMainRowStatus (NewBundleIndex, CFA_RS_ACTIVE);
    nmhSetIfStackStatus (OldBundleIndex, LinkIndex, CFA_RS_DESTROY);
    PppComposeMpBundle ((UINT2) OldBundleIndex, LinkIndex, CFA_LAYER_DEL);
    nmhSetIfStackStatus (NewBundleIndex, LinkIndex, CFA_RS_CREATEANDWAIT);
    nmhSetIfStackStatus (NewBundleIndex, LinkIndex, CFA_RS_ACTIVE);
    nmhSetIfAdminStatus (NewBundleIndex, CFA_IF_UP);
    nmhSetIfAdminStatus (LinkIndex, CFA_IF_UP);
}

/*****************************************************************************
 *    Function Name        : CfaIfmDeletePppLink
 *
 *    Description          :This function makes the PPP interface status down 
 *    Input(s)             :u4PppIndex 
 *    Output(s)            : None.
 *
 *    Returns              :None   
 *
 *****************************************************************************/
VOID
CfaIfmDeletePppLink (UINT4 u4PppIndex)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IfAdminStatus (&u4ErrorCode, u4PppIndex, CFA_IF_DOWN) ==
        SNMP_SUCCESS)
    {
        nmhSetIfAdminStatus (u4PppIndex, CFA_IF_DOWN);
    }

}

/*****************************************************************************
 *     Function Name        : CfaPppInitForIfType
 *   
 *     Description          :This function initializes the PPP interface when
 *                           status down
 *     Input(s)             :u4PppIndex
 *     Output(s)            : None.
 *        
 *     Returns              :None
 *          
 *******************************************************************************/
INT4
CfaPppInitForIfType (UINT4 u4IfIndex)
{

    if (PppInitForIfType (u4IfIndex) != SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error in CfaPppInitForIfType initializing PPP interface type\n");
        return FAILURE;
    }

    return SUCCESS;
}

INT4
CfaPppCheckPortBinding (UINT4 u4PortIndex)
{
    UINT4               u4Index = 0;
    UINT1               u1IfType = 0;
    PPPoEBindingGetIndex ((UINT2) u4PortIndex, &u4Index);
    if (u4Index == 0)
    {
        return CFA_FAILURE;
    }
    CfaGetIfType (u4Index, &u1IfType);
    if (u1IfType == CFA_PPP)
    {
        return CFA_SUCCESS;
    }
    return CFA_FAILURE;
}

#endif /* PPP_WANTED */

#ifdef S_A_CONV_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaIfmSAConvRegisterInterface
 *
 *    Description        : This function processes processes
 *                notifications from PPP/MP about the state of
 *                the interface - DATA_OPEN or DATA_CLOSE.
 *
 *    Input(s)            : UINT2 u2IfIndex,
 *                UINT1 u1PppLinkState
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmSAConvRegisterInterface (UINT2 u2SyncIfIndex, UINT2 u2AsyncIfIndex)
#else /* __STDC__ */
INT4
CfaIfmSAConvRegisterInterface (u2SyncIfIndex, u2AsyncIfIndex)
     UINT2               u2SyncIfIndex;
     UINT2               u2AsyncIfIndex;
#endif /* __STDC__ */
{
    UINT1               u1CurType;

    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmSAConvRegisterInterface for Sync iface %d Async iface %d\n",
              u2SyncIfIndex, u2AsyncIfIndex);

    /* first check if the index is valid */
    CfaGetIfType (u2SyncIfIndex, &u1CurType);

    if ((u2SyncIfIndex > CFA_MAX_INTERFACES ()) || (u2SyncIfIndex == 0) ||
        (u1CurType != CFA_HDLC))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmSAConvRegisterInterface - invalid sync interface %d.\n",
                  u2SyncIfIndex);
        return CFA_FAILURE;
    }

    CfaGetIfType (u2AsyncIfIndex, &u1CurType);

    if ((u2AsyncIfIndex > CFA_MAX_INTERFACES ()) || (u2AsyncIfIndex == 0) ||
        (u1CurType != CFA_ASYNC))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmSAConvRegisterInterface - invalid Async interface %d.\n",
                  u2AsyncIfIndex);
        return CFA_FAILURE;
    }

    /* register the HL for there ports as the Sync-Async convertor module */
    if (CfaGddRegisterPort (u2SyncIfIndex, CFA_SYNC_ASYNC) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmSAConvRegisterInterface - GDD Registration fail for iface %d\n",
                  u2SyncIfIndex);
        return (CFA_FAILURE);
    }

    if (CfaGddRegisterPort (u2AsyncIfIndex, CFA_SYNC_ASYNC) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmSAConvRegisterInterface - GDD Registration fail for iface %d\n",
                  u2AsyncIfIndex);

        /* remove the registration for sync also */
        if (CfaGddDeRegisterPort (u2SyncIfIndex) != CFA_SUCCESS)
        {
            CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                      "Error in CfaIfmSAConvRegisterInterface - GDD De-Registration fail interface %d\n",
                      u2SyncIfIndex);
        }
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmSAConvDeRegisterInterface
 *
 *    Description        : This function processes processes
 *                notifications from PPP/MP about the state of
 *                the interface - DATA_OPEN or DATA_CLOSE.
 *
 *    Input(s)            : UINT2 u2IfIndex,
 *                UINT1 u1PppLinkState
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None.
 *
 *****************************************************************************/
#ifdef __STDC__
INT4
CfaIfmSAConvDeRegisterInterface (UINT2 u2SyncIfIndex, UINT2 u2AsyncIfIndex)
#else /* __STDC__ */
INT4
CfaIfmSAConvDeRegisterInterface (u2SyncIfIndex, u2AsyncIfIndex)
     UINT2               u2SyncIfIndex;
     UINT2               u2AsyncIfIndex;
#endif /* __STDC__ */
{
    UINT1               u1CurType;

    CFA_DBG2 (CFA_TRC_ALL_TRACK, CFA_IFM,
              "In CfaIfmSAConvDeRegisterInterface for Sync iface %d Async iface %d\n",
              u2SyncIfIndex, u2AsyncIfIndex);

    /* first check if the index is valid */
    CfaGetIfType (u2SyncIfIndex, &u1CurType);
    if ((u2SyncIfIndex > CFA_MAX_INTERFACES ()) || (u2SyncIfIndex == 0) ||
        (u1CurType != CFA_HDLC))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmSAConvDeRegisterInterface - invalid sync interface %d.\n",
                  u2SyncIfIndex);
        return CFA_FAILURE;
    }

    CfaGetIfType (u2AsyncIfIndex, &u1CurType);
    if ((u2AsyncIfIndex > CFA_MAX_INTERFACES ()) || (u2AsyncIfIndex == 0) ||
        (u1CurType != CFA_ASYNC))
    {
        CFA_DBG1 (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_IFM,
                  "Error in CfaIfmSAConvDeRegisterInterface - invalid Async interface %d.\n",
                  u2AsyncIfIndex);
        return CFA_FAILURE;
    }

    /* de-register the HL for there ports as the Sync-Async convertor module */
    if (CfaGddDeRegisterPort (u2SyncIfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmSAConvRegisterInterface - GDD De-Reg for Sync iface FAIL %d\n",
                  u2SyncIfIndex);
        return (CFA_FAILURE);
    }

    if (CfaGddDeRegisterPort (u2AsyncIfIndex) != CFA_SUCCESS)
    {
        CFA_DBG1 (CFA_TRC_ERROR, CFA_IFM,
                  "Error in CfaIfmSAConvRegisterInterface - GDD De-Reg for Async iface FAIL %d\n",
                  u2AsyncIfIndex);
        return (CFA_FAILURE);
    }

    return (CFA_SUCCESS);
}

#endif /* S_A_CONV_WANTED */

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateInterfaceStatus
 *
 *    Description          : This function updates the interface status.
 *
 *    Input(s)             : UINT4 u4IfIndex,
 *                           UINT1 u1IfStatus
 *
 *    Output(s)            : None.
 *
 *
 *    Returns            : None.
 *
 *****************************************************************************/

VOID
CfaUpdateInterfaceStatus (UINT4 u4IfIndex, UINT1 u1IfStatus)
{
    tCRU_BUF_CHAIN_HEADER *pBuf;
    if ((pBuf = CRU_BUF_Allocate_MsgBufChain (4, 0)) == NULL)
    {
        CFA_DBG (CFA_TRC_ERROR | CFA_TRC_ALL_TRACK, CFA_GDD,
                 "Error in CfaUpdateInterfaceStatus - No CRU Buf Available "
                 "- FAILURE.\n");
        return;
    }

    CFA_SET_COMMAND (pBuf, UPDATE_IF_STATUS);
    CFA_SET_IFINDEX (pBuf, u4IfIndex);
    /* Using pkt type for interface status info */
    CFA_SET_PKT_TYPE (pBuf, u1IfStatus);

    if (OsixSendToQ (SELF, (const UINT1 *) CFA_PACKET_QUEUE, pBuf,
                     OSIX_MSG_URGENT) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaUpdateInterfaceStatus-Pkt Enqueue to CFA FAIL\n");
        return;
    }

    /* send an explicit event to the task */
    if (OsixSendEvent
        (SELF, (const UINT1 *) CFA_TASK_NAME,
         CFA_PACKET_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        CFA_DBG (CFA_TRC_ALL, CFA_MAIN,
                 "Error In CfaUpdateInterfaceStatus-event send to CFA FAIL.\n");
        /* dont return failure as buffer has already been enqueued */
    }

    return;
}
