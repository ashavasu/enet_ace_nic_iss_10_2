/* SOURCE FILE HEADER :
 * $Id: cfanpwr.c,v 1.12 2018/02/02 09:47:31 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : cfanpwr.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                                     |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CFA                                              |
 * |                                                                           |
 * |  MODULE NAME           : CFA NP Wrapper Configurations                    |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : This file contains the CFA NPAPI related         |
 * |                          wrapper routines.                                |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#ifndef __CFA_NP_WR_C__
#define __CFA_NP_WR_C__

#include "cfainc.h"
#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : CfaNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tCfaNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
CfaNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    INT4                i4RetVal = FNP_SUCCESS;
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tCfaNpModInfo      *pCfaNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pCfaNpModInfo = &(pFsHwNp->CfaNpModInfo);

    if (NULL == pCfaNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case CFA_NP_GET_LINK_STATUS:
        {
            tCfaNpWrCfaNpGetLinkStatus *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaNpGetLinkStatus;
            pEntry->u1PortLinkStatus = CfaNpGetLinkStatus (pEntry->u4IfIndex);
            i4RetVal = FNP_SUCCESS;
            break;
        }
        case CFA_REGISTER_WITH_NP_DRV:
        {
            i4RetVal = CfaRegisterWithNpDrv ();
            break;
        }
        case FS_CFA_HW_CLEAR_STATS:
        {
            tCfaNpWrFsCfaHwClearStats *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwClearStats;
            i4RetVal = FsCfaHwClearStats (pEntry->u4Port);
            break;
        }
        case FS_CFA_HW_GET_IF_FLOW_CONTROL:
        {
            tCfaNpWrFsCfaHwGetIfFlowControl *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwGetIfFlowControl;
            i4RetVal =
                FsCfaHwGetIfFlowControl (pEntry->u4IfIndex,
                                         pEntry->pu4PortFlowControl);
            break;
        }
        case FS_CFA_HW_GET_MTU:
        {
            tCfaNpWrFsCfaHwGetMtu *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwGetMtu;
            i4RetVal =
                FsCfaHwGetMtu ((UINT2) pEntry->u4IfIndex, pEntry->pu4MtuSize);
            break;
        }
        case FS_CFA_HW_SET_MAC_ADDR:
        {
            tCfaNpWrFsCfaHwSetMacAddr *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwSetMacAddr;
            i4RetVal = FsCfaHwSetMacAddr (pEntry->u4IfIndex, pEntry->PortMac);
            break;
        }
        case FS_CFA_HW_SET_MTU:
        {
            tCfaNpWrFsCfaHwSetMtu *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwSetMtu;
            i4RetVal = FsCfaHwSetMtu (pEntry->u4IfIndex, pEntry->u4MtuSize);
            break;
        }
        case FS_CFA_HW_L3_SET_MTU:
        {
            tCfaNpWrFsCfaHwL3SetMtu *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwL3SetMtu;
            i4RetVal = FsCfaHwL3SetMtu (pEntry->L3Mtu);
            break;

        }
        case FS_CFA_HW_SET_WAN_TYE:
        {
            tCfaNpWrFsCfaHwSetWanTye *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwSetWanTye;
            i4RetVal = FsCfaHwSetWanTye (pEntry->pCfaWanInfo);
            break;
        }
        case FS_ETHER_HW_GET_CNTRL:
        {
            tCfaNpWrFsEtherHwGetCntrl *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetCntrl;
            i4RetVal =
                FsEtherHwGetCntrl (pEntry->u4IfIndex, pEntry->pElement,
                                   pEntry->i4Code);
            break;
        }
        case FS_ETHER_HW_GET_COLL_FREQ:
        {
            tCfaNpWrFsEtherHwGetCollFreq *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetCollFreq;
            i4RetVal =
                FsEtherHwGetCollFreq (pEntry->u4IfIndex,
                                      pEntry->i4dot3CollCount,
                                      pEntry->pElement);
            break;
        }
        case FS_ETHER_HW_GET_PAUSE_FRAMES:
        {
            tCfaNpWrFsEtherHwGetPauseFrames *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetPauseFrames;
            i4RetVal =
                FsEtherHwGetPauseFrames (pEntry->u4IfIndex, pEntry->pElement,
                                         pEntry->i4Code);
            break;
        }
        case FS_ETHER_HW_GET_PAUSE_MODE:
        {
            tCfaNpWrFsEtherHwGetPauseMode *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetPauseMode;
            i4RetVal =
                FsEtherHwGetPauseMode (pEntry->u4IfIndex, pEntry->pElement,
                                       pEntry->i4Code);
            break;
        }
        case FS_ETHER_HW_GET_STATS:
        {
            tCfaNpWrFsEtherHwGetStats *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsEtherHwGetStats;
            i4RetVal = FsEtherHwGetStats (pEntry->u4IfIndex, pEntry->pEthStats);
            break;
        }
        case FS_ETHER_HW_SET_PAUSE_ADMIN_MODE:
        {
            tCfaNpWrFsEtherHwSetPauseAdminMode *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsEtherHwSetPauseAdminMode;
            i4RetVal =
                FsEtherHwSetPauseAdminMode (pEntry->u4IfIndex,
                                            pEntry->pElement);
            break;
        }
        case FS_HW_GET_ETHERNET_TYPE:
        {
            tCfaNpWrFsHwGetEthernetType *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsHwGetEthernetType;
            i4RetVal =
                FsHwGetEthernetType (pEntry->u4IfIndex, pEntry->pu1EtherType);
            break;
        }
        case FS_HW_GET_STAT:
        {
            tCfaNpWrFsHwGetStat *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsHwGetStat;
            i4RetVal =
                FsHwGetStat (pEntry->u4IfIndex, pEntry->i1StatType,
                             pEntry->pu4Value);
            break;
        }
        case FS_HW_GET_STAT64:
        {
            tCfaNpWrFsHwGetStat64 *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsHwGetStat64;
            i4RetVal =
                FsHwGetStat64 (pEntry->u4IfIndex, pEntry->i1StatType,
                               pEntry->pValue);
            break;
        }
        case FS_HW_SET_CUST_IF_PARAMS:
        {
            tCfaNpWrFsHwSetCustIfParams *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsHwSetCustIfParams;
            i4RetVal =
                FsHwSetCustIfParams (pEntry->u4IfIndex, pEntry->HwCustParamType,
                                     pEntry->CustIfParamVal,
                                     pEntry->EntryAction);
            break;
        }
        case FS_HW_UPDATE_ADMIN_STATUS_CHANGE:
        {
            tCfaNpWrFsHwUpdateAdminStatusChange *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsHwUpdateAdminStatusChange;
            i4RetVal =
                FsHwUpdateAdminStatusChange (pEntry->u4IfIndex,
                                             pEntry->u1AdminEnable);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_CONFIG:
        {
            tCfaNpWrFsMauHwGetAutoNegAdminConfig *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegAdminConfig;
            i4RetVal =
                FsMauHwGetAutoNegAdminConfig (pEntry->u4IfIndex,
                                              pEntry->i4IfMauId,
                                              pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_ADMIN_STATUS:
        {
            tCfaNpWrFsMauHwGetAutoNegAdminStatus *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegAdminStatus;
            i4RetVal =
                FsMauHwGetAutoNegAdminStatus (pEntry->u4IfIndex,
                                              pEntry->i4IfMauId,
                                              pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_ADVT_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapAdvtBits *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapAdvtBits;
            i4RetVal =
                FsMauHwGetAutoNegCapAdvtBits (pEntry->u4IfIndex,
                                              pEntry->i4IfMauId,
                                              pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapBits *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapBits;
            i4RetVal =
                FsMauHwGetAutoNegCapBits (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                          pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_CAP_RCVD_BITS:
        {
            tCfaNpWrFsMauHwGetAutoNegCapRcvdBits *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegCapRcvdBits;
            i4RetVal =
                FsMauHwGetAutoNegCapRcvdBits (pEntry->u4IfIndex,
                                              pEntry->i4IfMauId,
                                              pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_ADVT:
        {
            tCfaNpWrFsMauHwGetAutoNegRemFltAdvt *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemFltAdvt;
            i4RetVal =
                FsMauHwGetAutoNegRemFltAdvt (pEntry->u4IfIndex,
                                             pEntry->i4IfMauId,
                                             pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REM_FLT_RCVD:
        {
            tCfaNpWrFsMauHwGetAutoNegRemFltRcvd *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemFltRcvd;
            i4RetVal =
                FsMauHwGetAutoNegRemFltRcvd (pEntry->u4IfIndex,
                                             pEntry->i4IfMauId,
                                             pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_REMOTE_SIGNALING:
        {
            tCfaNpWrFsMauHwGetAutoNegRemoteSignaling *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRemoteSignaling;
            i4RetVal =
                FsMauHwGetAutoNegRemoteSignaling (pEntry->u4IfIndex,
                                                  pEntry->i4IfMauId,
                                                  pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_RESTART:
        {
            tCfaNpWrFsMauHwGetAutoNegRestart *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegRestart;
            i4RetVal =
                FsMauHwGetAutoNegRestart (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                          pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_AUTO_NEG_SUPPORTED:
        {
            tCfaNpWrFsMauHwGetAutoNegSupported *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetAutoNegSupported;
            i4RetVal =
                FsMauHwGetAutoNegSupported (pEntry->u4IfIndex,
                                            pEntry->i4IfMauId,
                                            pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_FALSE_CARRIERS:
        {
            tCfaNpWrFsMauHwGetFalseCarriers *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetFalseCarriers;
            i4RetVal =
                FsMauHwGetFalseCarriers (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                         pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_JABBER_STATE:
        {
            tCfaNpWrFsMauHwGetJabberState *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetJabberState;
            i4RetVal =
                FsMauHwGetJabberState (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                       pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_JABBERING_STATE_ENTERS:
        {
            tCfaNpWrFsMauHwGetJabberingStateEnters *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetJabberingStateEnters;
            i4RetVal =
                FsMauHwGetJabberingStateEnters (pEntry->u4IfIndex,
                                                pEntry->i4IfMauId,
                                                pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_JACK_TYPE:
        {
            tCfaNpWrFsMauHwGetJackType *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetJackType;
            i4RetVal =
                FsMauHwGetJackType (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                    pEntry->i4ifJackIndex, pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_MAU_TYPE:
        {
            tCfaNpWrFsMauHwGetMauType *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetMauType;
            i4RetVal =
                FsMauHwGetMauType (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                   pEntry->pu4Val);
            break;
        }
        case FS_MAU_HW_GET_MEDIA_AVAIL_STATE_EXITS:
        {
            tCfaNpWrFsMauHwGetMediaAvailStateExits *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetMediaAvailStateExits;
            i4RetVal =
                FsMauHwGetMediaAvailStateExits (pEntry->u4IfIndex,
                                                pEntry->i4IfMauId,
                                                pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_MEDIA_AVAILABLE:
        {
            tCfaNpWrFsMauHwGetMediaAvailable *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetMediaAvailable;
            i4RetVal =
                FsMauHwGetMediaAvailable (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                          pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_TYPE_LIST_BITS:
        {
            tCfaNpWrFsMauHwGetTypeListBits *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetTypeListBits;
            i4RetVal =
                FsMauHwGetTypeListBits (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                        pEntry->pElement);
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_ADMIN_STATUS:
        {
            tCfaNpWrFsMauHwSetAutoNegAdminStatus *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegAdminStatus;
            i4RetVal =
                FsMauHwSetAutoNegAdminStatus (pEntry->u4IfIndex,
                                              pEntry->i4IfMauId,
                                              pEntry->pElement);
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_CAP_ADVT_BITS:
        {
            tCfaNpWrFsMauHwSetAutoNegCapAdvtBits *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegCapAdvtBits;
            i4RetVal =
                FsMauHwSetAutoNegCapAdvtBits (pEntry->u4IfIndex,
                                              pEntry->i4IfMauId,
                                              pEntry->pElement);
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_REM_FLT_ADVT:
        {
            tCfaNpWrFsMauHwSetAutoNegRemFltAdvt *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegRemFltAdvt;
            i4RetVal =
                FsMauHwSetAutoNegRemFltAdvt (pEntry->u4IfIndex,
                                             pEntry->i4IfMauId,
                                             pEntry->pElement);
            break;
        }
        case FS_MAU_HW_SET_AUTO_NEG_RESTART:
        {
            tCfaNpWrFsMauHwSetAutoNegRestart *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetAutoNegRestart;
            i4RetVal =
                FsMauHwSetAutoNegRestart (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                          pEntry->pElement);
            break;
        }
        case FS_MAU_HW_SET_MAU_STATUS:
        {
            tCfaNpWrFsMauHwSetMauStatus *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetMauStatus;
            i4RetVal =
                FsMauHwSetMauStatus (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                     pEntry->pElement);
            break;
        }
        case FS_MAU_HW_GET_STATUS:
        {
            tCfaNpWrFsMauHwGetStatus *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetStatus;
            i4RetVal =
                FsMauHwGetStatus (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                  pEntry->pElement);
            break;
        }
        case FS_MAU_HW_SET_DEFAULT_TYPE:
        {
            tCfaNpWrFsMauHwSetDefaultType *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwSetDefaultType;
            i4RetVal =
                FsMauHwSetDefaultType (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                       pEntry->u4Val);
            break;
        }
        case FS_MAU_HW_GET_DEFAULT_TYPE:
        {
            tCfaNpWrFsMauHwGetDefaultType *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsMauHwGetDefaultType;
            i4RetVal =
                FsMauHwGetDefaultType (pEntry->u4IfIndex, pEntry->i4IfMauId,
                                       pEntry->pu4Val);
            break;
        }
        case NP_CFA_FRONT_PANEL_PORTS:
        {
            tCfaNpWrNpCfaFrontPanelPorts *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpNpCfaFrontPanelPorts;
            i4RetVal = NpCfaFrontPanelPorts (pEntry->u4MaxFrontPanelPorts);
            break;
        }
        case FS_CFA_HW_MAC_LOOKUP:
        {
            tCfaNpWrFsCfaHwMacLookup *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwMacLookup;
            i4RetVal =
                FsCfaHwMacLookup (pEntry->pu1MacAddr, pEntry->VlanId,
                                  pEntry->pu2Port);
            break;
        }
        case CFA_NP_SET_HW_PORT_INFO:
        {
            tCfaNpWrCfaNpSetHwPortInfo *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaNpSetHwPortInfo;
            CfaNpSetHwPortInfo (pEntry->HwPortInfo);
            break;
        }
        case CFA_NP_GET_HW_PORT_INFO:
        {
            tCfaNpWrCfaNpGetHwPortInfo *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaNpGetHwPortInfo;
            CfaNpGetHwPortInfo (pEntry->pHwPortInfo);
            break;
        }
        case FS_CFA_HW_DELETE_PKT_FILTER:
        {
            tCfaNpWrFsCfaHwDeletePktFilter *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwDeletePktFilter;
            i4RetVal = FsCfaHwDeletePktFilter (pEntry->u4FilterId);
            break;
        }
        case FS_CFA_HW_CREATE_PKT_FILTER:
        {
            tCfaNpWrFsCfaHwCreatePktFilter *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwCreatePktFilter;
            i4RetVal =
                FsCfaHwCreatePktFilter (pEntry->pFilterInfo,
                                        pEntry->pu4FilterId);
            break;
        }
        case FS_NP_CFA_SET_DLF_STATUS:
        {
            tCfaNpWrFsNpCfaSetDlfStatus *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsNpCfaSetDlfStatus;
            i4RetVal = FsNpCfaSetDlfStatus (pEntry->u1Status);
            break;
        }
        case CFA_NP_SET_STACKING_MODEL:
        {
            tCfaNpWrCfaNpSetStackingModel *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaNpSetStackingModel;
            i4RetVal = CfaNpSetStackingModel (pEntry->u4StackingModel);
            break;
        }
        case CFA_NP_GET_HW_INFO:
        {
            tCfaNpWrCfaNpGetHwInfo *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaNpGetHwInfo;
            CfaNpGetHwInfo (pEntry->pHwIdInfo);
            break;
        }
        case CFA_NP_REMOTE_SET_HW_INFO:
        {
            tCfaNpWrCfaNpRemoteSetHwInfo *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaNpRemoteSetHwInfo;
            CfaNpRemoteSetHwInfo (pEntry->pHwIdInfo);
            break;
        }
        case FS_CFA_HW_REMOVE_IP_NET_RCVD_DLF_IN_HASH:
        {
            tCfaNpWrFsCfaHwRemoveIpNetRcvdDlfInHash *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIpNetRcvdDlfInHash;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsCfaHwRemoveIpNetRcvdDlfInHash (pEntry->u4ContextId,
                                                 pEntry->u4IpNet,
                                                 pEntry->u4IpMask);
            break;
        }
#ifdef IP6_WANTED
        case FS_CFA_HW_REMOVE_IP6_NET_RCVD_DLF_IN_HASH:
        {
            tCfaNpWrFsCfaHwRemoveIp6NetRcvdDlfInHash *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaHwRemoveIp6NetRcvdDlfInHash;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsCfaHwRemoveIp6NetRcvdDlfInHash (pEntry->u4ContextId,
                                                  pEntry->Ip6Addr,
                                                  pEntry->u4Prefix);
            break;
        }
#endif

#ifdef MBSM_WANTED
        case CFA_MBSM_NP_SLOT_DE_INIT:
        {
            tCfaNpWrCfaMbsmNpSlotDeInit *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaMbsmNpSlotDeInit;
            i4RetVal = CfaMbsmNpSlotDeInit (pEntry->pSlotInfo);
            break;
        }
        case CFA_MBSM_NP_SLOT_INIT:
        {
            tCfaNpWrCfaMbsmNpSlotInit *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaMbsmNpSlotInit;
            i4RetVal = CfaMbsmNpSlotInit (pEntry->pSlotInfo);
            break;
        }
        case CFA_MBSM_REGISTER_WITH_NP_DRV:
        {
            tCfaNpWrCfaMbsmRegisterWithNpDrv *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaMbsmRegisterWithNpDrv;
            i4RetVal = CfaMbsmRegisterWithNpDrv (pEntry->pSlotInfo);
            break;
        }
        case CFA_MBSM_UPDATE_PORT_MAC_ADDRESS:
        {
            tCfaNpWrCfaMbsmUpdatePortMacAddress *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpCfaMbsmUpdatePortMacAddress;
            CfaMbsmUpdatePortMacAddress (pEntry->u1SlotId, pEntry->u4PortIndex,
                                         pEntry->u4IfIndex,
                                         pEntry->pu1SwitchMac);
            break;
        }
        case FS_CFA_MBSM_SET_MAC_ADDR:
        {
            tCfaNpWrFsCfaMbsmSetMacAddr *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsCfaMbsmSetMacAddr;
            i4RetVal =
                FsCfaMbsmSetMacAddr (pEntry->u4IfIndex, pEntry->PortMac,
                                     pEntry->pSlotInfo);
            break;
        }
        case FS_HW_MBSM_SET_CUST_IF_PARAMS:
        {
            tCfaNpWrFsHwMbsmSetCustIfParams *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsHwMbsmSetCustIfParams;
            i4RetVal =
                FsHwMbsmSetCustIfParams (pEntry->u4IfIndex,
                                         pEntry->HwCustParamType,
                                         pEntry->CustIfParamVal,
                                         pEntry->EntryAction,
                                         pEntry->pSlotInfo);
            break;
        }
        case FS_NP_CFA_MBSM_ARP_MODIFY_FILTER:
        case FS_NP_CFA_MBSM_OSPF_MODIFY_FILTER:
        {
            tCfaNpWrFsNpCfaModifyFilter *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsNpCfaModifyFilter;
            u1RetVal =
                FsNpCfaModifyFilter (pEntry->u1Action, pFsHwNp->u4Opcode);
            break;
        }

#endif /* MBSM_WANTED */

        case FS_HW_GET_VLAN_INTF_STAT:
        {
            tCfaNpWrFsHwGetVlanIntfStats *pEntry = NULL;
            pEntry = &pCfaNpModInfo->CfaNpFsHwGetVlanIntfStats;

            i4RetVal =
                FsHwGetVlanIntfStats (pEntry->u4IfIndex, pEntry->i1StatType,
                                      pEntry->pu4Value);
            break;
        }
        default:
            i4RetVal = FNP_FAILURE;
            break;

    }                            /* switch */
    u1RetVal = (UINT1) i4RetVal;
    return (u1RetVal);
}

#endif /* __CFA_NP_WR_C__ */
